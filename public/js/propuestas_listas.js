	var objFiltro = {cantidad: 0, filtros: []};

	$(document).ready(function(){

	$("#btn-filtros").click(function(){
		mostrarFiltro();
	});

	$("#btn-buscar").click(function(){
		cargarFiltros();
		aplicarFiltro();
	});

	});

	function mostrarFiltro()
	{
		console.log("mostrarFiltro");
	}

	function aplicarFiltro()
	{
		
		var url = BASE_URL + '/propuestas/obtenerLista';
		$.get(url , objFiltro)
		  	.done(function( data ) {		  		
		    	console.log("aplicarFiltro");
		    	$("#tbLista > tbody").empty();
		    	mostrar(data.lista);
		});
	}

	function cargarFiltros()
	{
		var obj = {};

		if( $("#tipo_servicio_id").val() != 0 ){			
			obj.tipo = "tipo_servicio_id";
			obj.val = $("#tipo_servicio_id").val();			
		}

		objFiltro.filtros.push(obj);

	}

	function mostrar(lista)
	{
		for (var i = 0; i < lista.length; i++) {
			var row = "";
			row = "<tr class='proyecto' data-propuesta_id='"+ lista[i].id +"'>";
			row += "<td>"+ (i+1) +"</td>";
			row += "<td>"+ lista[i].propuesta + "</td>";
			row += "<td>"+ lista[i].codigo + "</td>";
			row += "<td>"+ lista[i].cliente.nombre + "</td>";
			row += "<td>"+ lista[i].created_at + "</td>";
			row += "<td>"+ lista[i].estado_toString + "</td>";
			row += "</tr>";
			$("#tbLista > tbody").append(row);
		}
	}