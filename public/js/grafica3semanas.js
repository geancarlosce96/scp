function grafico3semanas_chart (data) {
    // console.log("grafico3semanas_chart", data);

    var fact = [];
    var nofact = [];
    var admin = [];
    var interna = [];
    var semana = [];

    for (var i = 0; i < data.length; i++) {
        fact[i] = data[i].horasFF;
        nofact[i] = data[i].horasFN;
        admin[i] = data[i].horasAND;
        interna[i] = data[i].horasINT;
        semana[i] = data[i].semana;
        // horas ejecutadas
    }

    Highcharts.chart('carga_semanal_3', {
        chart: {
            text: 'Combination chart'
        },
        title: {
           // text: 'HH de '+$("#careas option:selected").text()+' semana '+data['semana']+'
            text: 'HH de '+$("#careas option:selected").text()+' por semanas ' + semana
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: [{
            categories: semana,

            title: {
                text: 'Semanas',
                className: 'highcharts-color-1',
                crosshair: true
            },

        }],
        yAxis: {
            title: {
                text: 'Horas'
            }
        },
        credits: {
            enabled: false,
        },
        colors: [
            '#CD201A','#11B15B','#0069AA','#BABDB6FF'
        ],
        series: [
            {
                type: 'column',
                name: 'Proyecto Facturable',
                data:fact,
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }

            },
            {
                type: 'column',
                name: 'General No Facturable',
                data: nofact,
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }

            } ,
            {
                type: 'column',
                name: 'AND Administrativo',
                data: admin,
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }

            } ,
            {
                type: 'column',
                name: 'Proyectos internos',
                data: interna,
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }

            }
        ],
    });
}