function validarcheck(){
  var check_activos=[];
  var contador_check = 0;
   $('#checkbox_ent:checked').each(
      function() {
          check_activos[contador_check] =$(this).val();
          contador_check++;
      });
    grabaruteo(check_activos,'check');
 }

 function grabaruteo(le,valor){
  $.ajax({
     url: 'grabarEntregablePrySelecRuteo',
     type: 'GET',
     // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
     data: {'check_entre': le, 'valor':valor},
   })
   .done(function(data) {
     console.log("success",data.length);
     $('#cantidadentregablessel').show();
     if (data == 'false') {
      swal("Hay entregables que no tienen fecha B o se encuentran sin codificación");
     }else{
       if (data.length > 100) {
        $('#cantidadentregablessel').text(" 0 entregables para validación");
       }else{
        $('#cantidadentregablessel').text(data+" entregables para validación");
       }
    }
   })
   .fail(function() {
     console.log("error");
   })
   .always(function() {
     console.log("complete");
   });
 }


$('.verentregableruteo').click(function() {

    var cproyecto = $('#cproyecto').val();
    var ruteo = $('#ruteo').val();
    // alert(cproyecto);

    $.ajax({
      url: 'verentregableruteo',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'cproyecto': cproyecto, 'ruteo': ruteo,'vista':'tableResumenEntregablesProyecto', 'estado':['REG']},
    })
    .done(function(data) {
      // console.log("verentregableruteo",data);
      $('#tablaEntregableruteo').html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    

    $('#modalresumenLE').modal({
        backdrop:"static"
      });
  });


 function actualizar_ruteo(obj) {
  console.log($(obj).val(),$(obj).attr("id"),"actualizar_ruteo");
  var ruta = $(obj).val();
  var cproyectoentregables = $(obj).attr("id")
  if (ruta) {
    $.ajax({
      url: 'actualizarruteo',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'cproyectoentregables': cproyectoentregables, 'ruta': ruta},
    })
    .done(function(data) {
      console.log("success","actualizar_ruteo",data);
      $('#grabadoruta_'+cproyectoentregables).show();
      $('#grabadoruta_'+cproyectoentregables).text('Ruta guardada');
      $('#grabadoruta_'+cproyectoentregables).css('color', 'green');
      setTimeout(function(){
        $('#grabadoruta_'+cproyectoentregables).hide();
      }, 5000);
      $("#"+cproyectoentregables).hide();
      $("#editarruta_"+cproyectoentregables).show();
      $("#cerrarruta_"+cproyectoentregables).hide();
      $("#ruta_"+cproyectoentregables).show();
      $("#ruta_"+cproyectoentregables).text(ruta);
    })
    .fail(function(data) {
      console.log("error","actualizar_ruteo");
      $('#grabadoruta_'+cproyectoentregables).show();
      $('#grabadoruta_'+cproyectoentregables).text('Ruta No guardada');
      $('#grabadoruta_'+cproyectoentregables).css('color', 'red');
      setTimeout(function(){
        $('#grabadoruta_'+cproyectoentregables).hide();
      }, 5000);
    })
    .always(function() {
      console.log("complete");
    });
  } else {
    $('#grabadoruta_'+cproyectoentregables).show();
    $('#grabadoruta_'+cproyectoentregables).text('Ingrese una ruta');
    $('#grabadoruta_'+cproyectoentregables).css('color', 'red');
    setTimeout(function(){
      $('#grabadoruta_'+cproyectoentregables).hide();
    }, 5000);
  }
   
 }  
    function vercomentarioentregable(id,ruteo,rev) {
      // alert('msg');

    $.ajax({
      url: 'verentregableruteocomentario',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'cproyectoentregable': id, 'ruteo': ruteo,'vista':'tableComentarioEntregablesProyecto', 'estado':['REG'],'rev':rev},
    })
    .done(function(data) {
      console.log("tablaEntregableRuteoComentario",data);
      // $('#tablaEntregableRuteoComentario').html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

      $('#modalresumenLE').modal({
        backdrop:"static"
      });
    }
   
