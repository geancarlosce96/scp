/*
$(".descripcion_ent").on("click",function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();

    mostrar_input_ent(id,desc);

    //tabla_eventos();
  });

  $(".observacion").on("click",function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();

    mostrar_input_ent(id,desc);

  });


  $(".tipo").on("click",function() {

    var id=$(this).find("select").attr("id");
    var desc=$(this).find("span").text();  

    mostrar_input_ent(id,desc);

    //actualizar_entregable($(this),'tipo');

  });

  $(".fechasTodEnt").on("click",function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();

    mostrar_input_ent(id,desc);

  });


  $(".descripcionEnt").focusout(function() {
    //console.log($(this));

    actualizar_entregable($(this),'descripcion');

  });

  $(".observacionEnt").focusout(function() {
    //console.log($(this));

    actualizar_entregable($(this),'observacion');

  });
  


  $(".tipoEnt").focusout(function() {

    //.on("change",function() {
    //console.log($(this).val());

    actualizar_entregable($(this),'tipo');

  });


  $('.fechaEnt').datepicker({
      format: "dd-mm-yy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "<?php echo date('d/m/Y'); ?>",
      todayHighlight: true,

  }).on("input changeDate", function (e) {
      actualizar_entregable($(this),'fecha')
  });

  $("a.verActivProy").off('click');
  $("a.verActivProy").on("click",function() {

    var id=$(this).parent().parent().parent().data("ident");

    verActividadesProyecto(id);

  });

  $("a.anularEnt").off('click');
  $("a.anularEnt").on("click",function() {
    var id=$(this).parent().parent().parent().data("ident");

    activarEnt(id,'1');

  });

  $("a.activarEnt").off('click');
  $("a.activarEnt").on("click",function() {
    var id=$(this).parent().parent().parent().data("ident");

    activarEnt(id,'0');

  });

  $("a.fechasEnt").off('click');
  $("a.fechasEnt").on("click",function() {
    var id=$(this).parent().parent().parent().data("ident");
    var descripcion=$(this).parent().parent().parent().find(".descripcion_ent").text();    

    verfechasEntregable(id,descripcion,'entregable');

  });

  $(".faseEnt").off('click')
  $(".faseEnt").on("click",function() {


    var idSpanFase=$(this).find("select").attr("id"); 
    var valSpanFase=$(this).find("select").text(); 
   
    mostrar_input_ent(idSpanFase);

  });
  $(".faseEntregable").focusout(function() {

      actualizar_entregable($(this),'fase');

    }); */


  $(".codigoEnt").off('click')
  $(".codigoEnt").dblclick(function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();
   
    mostrar_input_ent(id,desc);

  });
  $(".codigo").focusout(function() {

     actualizar_entregable($(this),'codigo');

  });

  $(".codigoEntCli").off('click')
  $(".codigoEntCli").dblclick(function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();
   
    mostrar_input_ent(id,desc);

  });
  $(".codigoCli").focusout(function() {

     actualizar_entregable($(this),'codigoCli');

  });


  $(".responsable").on("click",function() {

    var id=$(this).find("select").attr("id");
    var desc=$(this).find("span").text();  

    mostrar_input_ent(id,desc);
  });

  $(".responsableEnt").focusout(function() {

    //.on("change",function() {
    //console.log($(this).val());

    actualizar_entregable($(this),'responsable');

  });

function actualizar_entregable(obj,act){


  var objEnt={cproyectoentregables: "", observacion: "", descripcion: "", responsable: "", actualizar:"", tipo: "", edt: "" ,tipoFecha: "",fecha: "", fase: "",codigo: "",codigoCli: "" };
  objEnt.cproyectoentregables=$(obj).parent().parent().data("ident");
  objEnt.edt=$(obj).parent().parent().data("edt");

  if(act=='codigo'){
    objEnt.codigo=$(obj).val();
  }
  if(act=='codigoCli'){
    objEnt.codigoCli=$(obj).val();
  }
  if(act=='observacion'){
    objEnt.observacion=$(obj).val();
  }
  if(act=='descripcion'){
    objEnt.descripcion=$(obj).val();      
  }
  if(act=='responsable'){
    objEnt.responsable=$(obj).val();      
  }
  if(act=='tipo'){
    objEnt.tipo=$(obj).val();      
  }

  if(act=='fecha'){
    
    //console.log($(obj));
    objEnt.fecha=$(obj).val(); 
    objEnt.tipoFecha=$(obj).parent().data("ctipo");   
  }

  if(act=='fase'){
    objEnt.cproyectoentregables=$(obj).parent().parent().parent().data("ident");

    objEnt.edt=$(obj).parent().parent().parent().data("edt");
    objEnt.fase=$(obj).val();      
  }

  objEnt.actualizar=act;


  

  var idInput=$(obj).attr("id");

  $.ajax({
    url: 'actualizarEntregable',
    type:'GET',
    data: objEnt,

    success : function (data){
      
      $('#div_carga').hide(); 
      //$("#tableEntdivLEntregable").html(data);
      //$("a.ocultarAnexos").hide();
      //console.log(idInput);
      if(act=='responsable' || act=='fase'){
        ocultar_input(idInput,$("#"+idInput+" option:selected").text()); 
      }

      else{
        ocultar_input(idInput,$(obj).val());  
      }
      //ocultar_input_ent(idInput,$(obj).val());
          

    },
  }); 

}

function ocultar_input_ent(idInput,valor){

  $("#"+idInput).css("display", "none"); 
  $("#sp_"+idInput).html(valor);
  $("#sp_"+idInput).css("display", "block"); 

}

function mostrar_input_ent(idInput){


  // alert(idInput);
	 
  $("#sp_"+idInput).css("display", "none"); 
  //$("#"+idInput).val(valor);
  $("#"+idInput).css("display", "block");

}


function setCheck(chk){
  //alert($(":checkbox").length);
  $("input[name='checkbox_ent[]']").each(function(){
      /*if (this.checked) {
          
      }*/
      //alert(this.checked);
      this.checked = chk.checked;
    }); 
  
  return;
}  





$("a.mostrarAnexos").on("click",function(){

  var id=$(this).parent().parent().data("ident");
  var edt=$(this).parent().parent().data("edt");

  obtenerAnexos(id,edt);
  obtenerCabeceraFechasXProyecto();

});


$("a.ocultarAnexos").on("click",function(){
  var id=$(this).parent().parent().data("ident");
  var anexos=$(this).parent().parent().data("anexos");
  ocultarAnexos(id,anexos);

});


var fechasCab=[];

function obtenerCabeceraFechasXProyecto(){

  $.ajax({
          url: 'obtenerCabeceraFechasXProyecto/'+$('#cproyecto').val(),
          type:'get',
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){
            console.log(data);

            $('#div_carga').hide(); 
            fechasCab=data;

          },
  });  
}


function obtenerAnexos(idEnt,idEdt){
  
  $.ajax({
          url: 'obtenerAnexos/'+idEnt+'/'+idEdt+'/Proyecto',
          type:'get',
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){
            $('#div_carga').hide();          
            mostrarAnexos(data,idEnt);
           
          },
  });  

}



function mostrarAnexos(obj,id){

  $("#btnmostrar_"+id).hide(); 
  $("#btnocultar_"+id).show(); 
  //var indice=$("#fila_"+id).index(); 
  //console.log(indice);
  //$('#tableEntregablesTodosPry').dataTable().fnDestroy();
  for (var i = 0; i < obj.length; i++) {
    var row = "";
    row = "<tr id='fila_"+id+"_"+i+"'"; 
    row += "data-edt='"+obj[i].cproyectoedt+"' data-ident='"+obj[i].cproyectoentregables+"' data-anexos='"+obj[i].anexos+"'>";  
    row += "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='input-group'><div class='pull-left'> "
    if(obj[i].cantidadFechas<=4){
      row += "<a href='#' title='¡Las fechas no están completas!' style='color: red'><i class='fa fa-exclamation' aria-hidden='true'></i>&nbsp;<i class='fa fa-calendar-times-o' aria-hidden='true'></i></a>&nbsp;";
    }
    else{
      row += "<a href='#' title='¡Fechas completas!' style='color: gray'>&nbsp;&nbsp;&nbsp;<i class='fa  fa-calendar-check-o' aria-hidden='true'></i></a>&nbsp;";
    }


    row += "</div><div class='pull-right'><label style='color: black;font-size: 12px;'><strong></strong></label>"
    row += "</div></div></td>";  
    row += "<td>"+obj[i].estado+"</td>";  
    row += "<td>"+obj[i].cedt+"</td>"; 
    row += "<td>"+obj[i].des_edt+"</td>"; 

     row += "<td class='faseEnt'><span id='sp_fase_"+obj[i].cproyectoentregables+"'>"+obj[i].fase+"</span>";
     row += " <div id='divfase_"+obj[i].cproyectoentregables+"'></div></td>";

    row += "<td>"+obj[i].tipoentregable+"</td>"; 
    row += "<td>"+obj[i].tipoproyentregable+"</td>";

    //alert(obj[i].codigo);

    var codigoCli='';
   
    if (obj[i].codigoCliente!=null) {

      codigoCli=obj[i].codigoCliente;
    }

    row += "<td class='codigoEntCli'>"+'<input id="codcli_'+obj[i].cproyectoentregables+'"class="input-sm codigoCli" type="text" name="" value="'+codigoCli+'" style="display:none" >';
    row += "<span id='sp_codcli_"+obj[i].cproyectoentregables+"'</span>"+codigoCli+"</td>";



    var codigo='';
    if (obj[i].codigo!=null) {

      codigo=obj[i].codigo;
    }

    row += "<td class='codigoEnt'>"+'<input id="cod_'+obj[i].cproyectoentregables+'"class="input-sm codigo" type="text" name="" value="'+codigo+'" style="display:none" >';
    row += "<span id='sp_cod_"+obj[i].cproyectoentregables+"'</span>"+codigo+"</td>";

/*    row += "<td class='codAnexo'>"+'<input id="codAn_'+obj[i].cproyectoentregables+'"class="form-control" type="text" name="" style="display:none">';
    row += "<span id='sp_codAn_"+obj[i].cproyectoentregables+"' style='display:none'></span></td>";*/


    row += "<td>"+obj[i].des_dis+"</td>";
    row += "<td class='descripcion_ent'>"+'<input id="tdesc_'+obj[i].cproyectoentregables+'"class="form-control descripcionEnt" type="text" name="" value="'+obj[i].descripcion+'" style="display:none">';
    row += "<span id='sp_tdesc_"+obj[i].cproyectoentregables+"'>"+obj[i].descripcion+"</span></td>";
    // row += "<span id='sp_tdesc_"+obj[i].cproyectoentregables+"'>"+obj[i].revision+"</span></td>";

/////////////////////////// INICIO FECHAS ///////////////////////////
    if (obj[i].fechas!='SinFechas') {
      var fechas =obj[i].fechas;
      for (var f = 0; f < fechas.length; f++) {
        row += "<td class='fechasTodEnt' data-ctipo='"+fechas[f].ctipo+"'>";
        row += "<span id='sp_tfec_"+obj[i].cproyectoentregables+"_"+fechas[f].ctipo+"'>"+fechas[f].fecha+"</span>";
        row += "<input id='tfec_"+obj[i].cproyectoentregables+"_"+fechas[f].ctipo+"' class='input-sm fechaEnt' type='text'  name='' style='display:none' value='"+fechas[f].fecha+"'></td>";
      }
    }
    else{
      for (var c = 0; c < fechasCab.length; c++) {
        row += "<td class='fechasTodEnt' data-ctipo='"+fechasCab[c].ctipofechasentregable+"'>";
        row += "<span id='sp_tfec_"+obj[i].cproyectoentregables+"_"+fechasCab[c].ctipofechasentregable+"'></span>";
        row += "<input id='tfec_"+obj[i].cproyectoentregables+"_"+fechasCab[c].ctipofechasentregable+"' class='input-sm fechaEnt' type='text'  name='' style='display:none' value=''></td>";
      }
    }
/////////////////////////// FIN FECHAS ///////////////////////////
    row += "<td class='observacion'>"+'<input id="tobs_'+obj[i].cproyectoentregables+'"class="input-sm observacionEnt" type="text" name="" value="'+obj[i].observacion+'" style="display:none" >';
    row += "<span id='sp_tobs_"+obj[i].cproyectoentregables+"'</span>"+obj[i].observacion+"</td>";
    row += "<td class='fase'><span id='sp_respAn_"+obj[i].cproyectoentregables+"'>"+obj[i].responsable+"</span>";
    row += " <div id='divrespAn_"+obj[i].cproyectoentregables+"'></div></td>";
    row += "<td>"+obj[i].elaborador+"</td>";
    row += "<td>"+obj[i].revisor+"</td>";
    row += "<td>"+obj[i].aprobador+"</td>";
    /*row += "<td><center>";
      row += "<a class='verActivProy' href='#' title='Asignar actividades' ><i class='fa fa-list-ul' aria-hidden='true'></i></a> &nbsp;";
      if (obj[i].activo=='1') {
        row += "<a class='anularEnt' href='#' title='Anular' style='color: red'><i class='fa fa-minus-square' aria-hidden='true' onclick='activarEnt("+obj[i].cproyectoentregables+",'1')'></i></a> &nbsp;";
      }
      else{
        row += "<a href='#' title='Activar' style='color: green'><i class='fa fa-check-square' aria-hidden='true' onclick='activarEnt("+obj[i].cproyectoentregables+",'0')'></i></a> &nbsp;";
      }
      row += "<a class='fechasEnt' href='#' title='Agregar fechas' ><i class='fa fa-calendar' aria-hidden='true'></i></a>";
    row += "</center></td>";*/
    row += "<td><input type='checkbox' name='checkbox_ent[]' id='checkbox_ent' value='"+obj[i].cproyectoentregables+"'</td>";
    row += "</tr>";
    $("#fila_"+id).after(row);
    //$('#tableEntregablesTodosPry').dataTable().fnAddData( $(row)[0]);
    //obtenerFasesProy(obj[i].cproyectoentregables,obj[i].cfaseproyecto);
  }
    //tabla_eventos_EntregablesTodos();
    //initDataTable();

  $(".codigoEnt").off('click')
  $(".codigoEnt").on("click",function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();
   
    mostrar_input_ent(id,desc);

  });
  $(".codigo").focusout(function() {

     actualizar_entregable($(this),'codigo');

    });


  $(".codigoEntCli").off('click')
  $(".codigoEntCli").on("click",function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();
   
    mostrar_input_ent(id,desc);

  });
  $(".codigoCli").focusout(function() {

     actualizar_entregable($(this),'codigoCli');

  });


}

function ocultarAnexos(id,anexos){

  $("#btnmostrar_"+id).show(); 
  $("#btnocultar_"+id).hide();  

  for (var i = 0; i < anexos; i++) {

    $("#fila_"+id+"_"+i).remove();

  }
}
  


