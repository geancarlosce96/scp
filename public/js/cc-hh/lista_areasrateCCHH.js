function chart_listadp_areasCCHH (data,color,descripcionactividad,idactividad) {
    console.log("datas",data);

    var chartSeriesData = [];
    var chartDrilldownData = [];

    var consumMonto =  data[0].consumidas;
    for (var i = 0; i < consumMonto.length; i++) {

        // horas ejecutadas
        var serienameArea = consumMonto[i].descripcion;
        var drill_idArea = consumMonto[i].carea;
        var sumYArea = consumMonto[i].sumatorio;
        var cat = consumMonto[i].categorias;
        //var emplea = data[i].categorias.empleadoCategoria;

        // if (cat.length>0)
        //{
        chartSeriesData.push({
            name: serienameArea,      // nombre de areas
            y: sumYArea,              // horas
            drilldown : drill_idArea,  // carea
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: -20, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif',
                    textDecoration: 'none'
                }
            }
        });

        // console.log("serienameArea",serienameArea);
        for (var y = 0; y < cat.length; y++)
        {

            var serienameAreaCategoria = cat[y].drilldown;
            var  nombredelaCategoria = cat[y].name;

            //console.log("categoria",cat);
            for (var z = 0; z < cat[y].empleadoCategoria.length; z++)
            {
                var emplea = cat[y].empleadoCategoria;




                chartDrilldownData.push({
                        data : cat, // id / name / y / carea
                        id: drill_idArea, //   carea
                        //name: nombredelaCategoria,
                        name: 'Consumidas',
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#000',
                            align: 'center',
                            format: '{point.y:.2f}', // one decimal
                            y: -5, // 10 pixels down from the top
                            style: {
                                fontSize: '10px',
                                fontFamily: 'Verdana, sans-serif',
                                textDecoration: 'none'
                            }
                        }


                    },
                    {
                        id: serienameAreaCategoria,
                        //name: nombredelaCategoria,
                        name: 'Consumidas',
                        data: emplea,
                        dataLabels: {
                            enabled: true,
                            rotation: -90,
                            color: '#000',
                            align: 'center',
                            format: '{point.y:.2f}', // one decimal
                            y: -5, // 10 pixels down from the top
                            style: {
                                fontSize: '10px',
                                fontFamily: 'Verdana, sans-serif',
                                textDecoration: 'none'
                            }
                        }

                    } );

            }

        }

        //}


    }

    var chartSeriesDataPresupuestadas = [];
    var chartDrilldownDataPresupuestadas = [];
    var presup =  data[0].presupuestadas;
    for (var a = 0; a < presup.length; a++)
    {
        var serienameAreaP = presup[a].descripcion;
        var drill_idAreaP = presup[a].carea;
        var sumYAreaP = presup[a].sumatorio;
        var rol = presup[a].roles;

        chartSeriesDataPresupuestadas.push({
            name: serienameAreaP,      // nombre de areas
            y: sumYAreaP,              // horas
            drilldown : drill_idAreaP,  // carea,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: -20, // 10 pixels down from the top
                style: {
                    fontSize: '10px',
                    fontFamily: 'Verdana, sans-serif',
                    textDecoration: 'none'
                }
            }
        });
        for (var b = 0; b < rol.length; b++)
        {
            var  nombredelaRol = rol[b].name;

            chartDrilldownDataPresupuestadas.push({
                data : rol, // id / name / y / carea
                id: drill_idAreaP, //   carea
               // name: nombredelaRol,
                name: 'Presupuestadas',
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: -5, // 10 pixels down from the top
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif',
                        textDecoration: 'none'
                    }
                },
                animation: {
                    duration: 1500,
                    // Uses simple function
                    easing: easeOutBounce
                }

            });

        }

    }


    var merged =chartDrilldownData.concat(chartDrilldownDataPresupuestadas);
   // console.log("drillmerge",merged);



    var easeOutBounce = function (pos) {
        if ((pos) < (1 / 2.75)) {
            return (7.5625 * pos * pos);
        }
        if (pos < (2 / 2.75)) {
            return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
        }
        if (pos < (2.5 / 2.75)) {
            return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
        }
        return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
    };
    Math.easeOutBounce = easeOutBounce;

    var chart = new Highcharts.chart('listadehorasporsemanaCCHH', {
        chart: {
            type: 'column',
            // width: 500,
            height: 550,
            options3d: {
                enabled: true,
                alpha: 0,
                beta: 0,
                depth: 100,
                viewDistance: 25
            }
        },
        title: {
            text: idactividad +' '+ descripcionactividad
        },
        subtitle: {
            text: '',
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total de Monto Consumidas | Presupuestadas'
            }

        },
        credits: {
            enabled: false
        },
        legend: {
            enabled: false
        },
        colors:[
            color, '#f79646'
        ],
        plotOptions: {

            series: {
                borderColor: '#FFF',

                borderWidth:1,
                dataLabels: {
                    enabled: true,
                     rotation: -90,
                    color: '#000000',
                    align: 'center',
                   // 'text-decoration': 'none',

                    format: '{point.y:.2f}', // one decimal
                    y: 15, // 10 pixels down from the top
                    style: {
                        fontSize: '10px !important',
                        fontFamily: 'Verdana, sans-serif',
                        //textDecoration: 'none !important',
                        //color: '#000000'
                    }
                },
            },
            /*column: {
                borderWidth: 2
            }*/

        },
        tooltip: {
            enabled: true,
            headerFormat: '<span>{point.name}</span><table style="font-size:10px">',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' + '<td style="padding:0"><b>{point.y:.2f}</b> HH</td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },

        series: [{

            //name: serienameArea,
            name: "Consumidas",
            data: chartSeriesData,
            animation: {
                duration: 1500,
                // Uses simple function
                easing: easeOutBounce
            }

        },{
            //name: serienameAreaP,
            name: "Presupuestadas",
            data: chartSeriesDataPresupuestadas,
            animation: {
                duration: 1500,
                // Uses simple function
                easing: easeOutBounce
            }
        }],
        drilldown: {
            allowPointDrilldown: false,
            series: merged

        }

    });

    function showValues() {
        $('#alpha-value').html(chart.options.chart.options3d.alpha);
        $('#beta-value').html(chart.options.chart.options3d.beta);
        $('#depth-value').html(chart.options.chart.options3d.depth);
    }

// Activate the sliders
    $('#sliders input').on('input change', function () {
        chart.options.chart.options3d[this.id] = parseFloat(this.value);
        showValues();
        chart.redraw(false);
    });

}

//showValues();