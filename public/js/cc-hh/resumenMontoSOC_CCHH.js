function chart_listado_resumenMontoSOC (data,color) {
    console.log("color 1", color);
        var chartSeriesData = [];
        var chartDrilldownData = [];
    
        var consum =  data[0].consumidas;
        // var consum =  data;
    
        for (var i = 0; i < consum.length; i++) {
    
            //console.log(data[0].consumidas[i].descripcion,"descripcion");
            // horas ejecutadas
            var serienameArea = consum[i].descripcion;
            var drill_idArea = consum[i].carea;
            var sumYArea = consum[i].sumatorio;
            var cat = consum[i].categorias;
            // if (cat.length>0)
            // console.log("categoria",cat);
    
            //var emplea = data[i].categorias.empleadoCategoria;
            // console.log("serienameArea",serienameArea);
    
            //{
            chartSeriesData.push({
                name: serienameArea,      // nombre de areas
                y: sumYArea,              // horas
                drilldown : drill_idArea,  // carea
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: -20, // 10 pixels down from the top
                    style: {
                       fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif',
                        textDecoration: 'none'
                    }
                }
            });
    
            for (var y = 0; y < cat.length; y++)
            {
                var serienameAreaCategoria = cat[y].drilldown;
    
                var  nombredelaCategoria = cat[y].name;
    
                for (var z = 0; z < cat[y].empleadoCategoria.length; z++)
                {
                    var emplea = cat[y].empleadoCategoria;
    
                    chartDrilldownData.push({
                            data : cat, // id / name / y / carea
                            id: drill_idArea, //   carea
                            //name: nombredelaCategoria,
                            name: "Consumido",
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#000',
                                align: 'center',
                                format: '{point.y:.2f}', // one decimal
                                y: 5, // 10 pixels down from the top
                                style: {
                                   fontSize: '10px',
                                    fontFamily: 'Verdana, sans-serif',
                                    textDecoration: 'none'
                                }
                            }
                        },
                        {
                            id: serienameAreaCategoria,
                           // name: nombredelaCategoria,
                            name: "Consumido",
                            data: emplea,
                            dataLabels: {
                                enabled: true,
                                rotation: -90,
                                color: '#000',
                                align: 'center',
                                format: '{point.y:.2f}', // one decimal
                                y: 5, // 10 pixels down from the top
                                style: {
                                   fontSize: '10px',
                                    fontFamily: 'Verdana, sans-serif',
                                    textDecoration: 'none'
                                }
                            }
                        } );
    
                }
    
            }
    
            //}
    
    
        }
    
    
        // Presupuestadas
    
    
        var chartSeriesDataPresupuestadas = [];
        var chartDrilldownDataPresupuestadas = [];
        var presup =  data[0].presupuestadas;
        for (var a = 0; a < presup.length; a++)
        {
            var serienameAreaP = presup[a].descripcion;
            var drill_idAreaP = presup[a].carea;
            var sumYAreaP = presup[a].sumatorio;
            var rol = presup[a].roles;
    
            chartSeriesDataPresupuestadas.push({
                name: serienameAreaP,      // nombre de areas
                y: sumYAreaP,              // horas
                drilldown : drill_idAreaP,  // carea,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: -20, // 10 pixels down from the top
                    style: {
                       fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif',
                        textDecoration: 'none'
                    }
                }
            });
            for (var b = 0; b < rol.length; b++)
            {
                var  nombredelaRol = rol[b].name;
    
                chartDrilldownDataPresupuestadas.push({
                    data : rol, // id / name / y / carea
                    id: drill_idAreaP, //   carea
                    //name: nombredelaRol,
                    name: 'Presupuestado',
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000',
                        align: 'center',
                        format: '{point.y:.2f}', // one decimal
                        y: 10, // 10 pixels down from the top
                        style: {
                           fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif',
                            textDecoration: 'none'
                        }
                    }
    
                });
    
            }
    
        }
    
    
        var merged =chartDrilldownData.concat(chartDrilldownDataPresupuestadas);
        //console.log("drillmerge",merged);
    
        // console.log("Drill",chartDrilldownData);
        // console.log("Drill2",chartDrilldownDataPresupuestadas);
        // console.log("OLA",chartSeriesData,chartDrilldownData,chartSeriesDataPresupuestadas,chartDrilldownDataPresupuestadas);
        var easeOutBounce = function (pos) {
            if ((pos) < (1 / 2.75)) {
                return (7.5625 * pos * pos);
            }
            if (pos < (2 / 2.75)) {
                return (7.5625 * (pos -= (1.5 / 2.75)) * pos + 0.75);
            }
            if (pos < (2.5 / 2.75)) {
                return (7.5625 * (pos -= (2.25 / 2.75)) * pos + 0.9375);
            }
            return (7.5625 * (pos -= (2.625 / 2.75)) * pos + 0.984375);
        };
        Math.easeOutBounce = easeOutBounce;
    
    
        var chart = new  Highcharts.chart('resumenmontoConsumidosPropuestasProyectoSOC', {
            chart: {
                type: 'column',
                // width: 500,
                height: 550,
                options3d: {
                    enabled: true,
                    alpha: 0,
                    beta: 0,
                    depth: 100,
                    viewDistance: 25
                }
    
            },
            title: {
               // text: $("#proyectos option:selected").text()
                text: 'Resumen total de los montos'
                //text: $("#proyectos").val()
            },
            subtitle: {
                text: '',
            },
            xAxis: {
                type: 'category',
                labels: {
                    // rotation: -45,
                    color: '#000000',
                    style: {
                        //fontSize: '13px',
                        //fontFamily: 'Verdana, sans-serif',
                        textDecoration: 'none'
                    }
                }
            },
            yAxis: {
                title: {
                    text: 'Costo Consumido | Presupuestado'
                }
    
            },
            credits: {
                enabled: false
            },
    
            colors:[
                color, '#f79646'
            ],
            plotOptions: {
    
                column: {
                    borderWidth: 2
                },
                series: {
                    borderColor: '#FFF',
                    borderWidth:1,
                    dataLabels: {
                        enabled: true,
                        rotation: -90,
                        color: '#000000',
                        align: 'center',
                        format: '{point.y:.2f}', // one decimal
                        y: 15, // 10 pixels down from the top
                        style: {
                           fontSize: '10px',
                            fontFamily: 'Verdana, sans-serif'
                        }
                    }
                }
            },
    
            tooltip: {
                enabled: true,
                headerFormat: '<span>{point.name}</span><table style="font-size:10px">',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name} : </td>' + '<td style="padding:0"><b>{point.y:.2f}</b> HH</td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
    
            series: [{
    
                /*name: serienameAreaP,
                data: chartSeriesDataPresupuestadas,*/
    
              //  name: serienameArea,
                name: "Consumido",
                data: chartSeriesData,
                animation: {
                    duration: 1500,
                    // Uses simple function
                    easing: easeOutBounce
                }
    
    
    
            },{
                //name: serienameAreaP,
                name: "Presupuestado",
                data: chartSeriesDataPresupuestadas,
                animation: {
                    duration: 1500,
                    // Uses simple function
                    easing: easeOutBounce
                }
            }],
            drilldown: {
                allowPointDrilldown: false,
                //series: chartDrilldownDataPresupuestadas
                series: merged
    
            },
    
        });
    
        function showValues() {
            $('#alpha-value_monto').html(chart.options.chart.options3d.alpha);
            $('#beta-value_monto').html(chart.options.chart.options3d.beta);
            $('#depth-value_monto').html(chart.options.chart.options3d.depth);
        }
    
    // Activate the sliders
    
        $('#sliders_monto input').on('input change', function () {
            chart.options.chart.options3d[this.id] = parseFloat(this.value);
            showValues();
            chart.redraw(false);
        });
    
    }
   // showValues();