agregarchosen('select')

function faddrEnt(id,desc,codigo){


    limpiarEntregables();
    limpiarlistaEntregables();

    $('#divEntAgregados').css("display", "none");

    $('#tipoentregable').prop("disabled",false);   
    $('#disciplina').prop("disabled",false);
    $("input[name='radio_ent']").prop('checked',false);


    $('#cproyectoedt_m').val(id);
    //$('#descripcionedt').val(desc);
    $('#descripcionedt').html(codigo+' - '+desc);
    $('#cproyecto_ent').val($('#cproyecto').val());
    $('#crevisionDoc').val($('#cod_revisionInt').val());

    verEntregablesEDT(id);



    $('#divLE').modal({
        backdrop:"static"
    });
    $('#tipoproyent').val('2').trigger('chosen:updated');
            //$('#divLE').modal('show');

      obtenerEntregablesCombo($('#tipoproyent').val());

    

}

function saveFechas(){
      $.ajaxSetup({
        header: document.getElementById('_token').value
    });

      

    $.ajax({
        url: 'grabarFechasEntregablePry',
        type:'POST',
        data: $("#frmfechasentproy").serialize(),
        beforeSend: function () {
          $('#div_carga').show(); 
      },              
      success : function (data){

          limpiarfechas();
         
          $("#divFechaEntregable").html(data);
          $('#div_carga').hide(); 
      },
    });
}


$('#tipoentregable').change(function(){

    if(this.value==''){
      return;
    }

    obtenerDisciplina(this.value,0);

    if($('#tipoproyent').val()=='1'){

      todosEntregableAgregado();
    }
      $('#tablaEntregablesAgregados').dataTable().fnDestroy();
      $("#tablaEntregablesAgregados > tbody tr").remove();

});




function obtenerDisciplina(tipoEnt,disciplina){

    $.ajax({
      type:'GET',
      url: 'obtenerDisciplinasEntregables/'+tipoEnt,
      success: function (data) {
        $('#entregableN0').find('option').remove();
        $('#entregableN1').find('option').remove();
        $('#disciplina').find('option').remove();
        $('#searchEnt').val('');
        limpiarlistaEntregables();

        $('#disciplina').chosen('destroy')

        for (var i = 0; i < data.length; i++) {

          $('#disciplina').append($('<option></option>').attr('value',data[i].cdisciplina).text(data[i].descripcion));

        } 

        agregarchosen('#disciplina')

       
        //$('#disciplina').val(disciplina);

        listado();     

        //buscarEntregableHijo($('#entregableN1').val());
        },
        error: function(data){

        }
    });

}

function listado(){ 
    // swal($('#disciplina').val()+'/'+$('#tipoentregable').val());
    $.ajax({
      type:'GET',
      url: 'Selentregable/'+$('#disciplina').val()+'/'+$('#tipoentregable').val(),
      success: function (data) {
              //Listaentregables(data);
              limpiarlistaEntregables();
              $('#entregableN0').chosen('destroy');
              $('#entregableN1').chosen('destroy');
              $('#entregableN0').find('option').remove();
              $('#entregableN1').find('option').remove();
              $('#searchEnt').val('');
              var str = JSON.stringify(data);
              var pushedData = jQuery.parseJSON(str); 

              $.each(pushedData, function(i, serverData)
              {
                  $('#entregableN0').append($('<option></option>').attr('value',i).text(serverData));

              });

              agregarchosen('#entregableN0')



              //$('#entregableN0').append($('<option style="color: blue"></option>').attr('value','nuevoN0').text('-- Agregar nuevo entregable Nivel 0 --'));

              listaEntregableN1($('#entregableN0').val());

              /*$( '#entregableN0' ).chosen(
                {
                    allow_single_deselect: true,width:"100%"
                });*/

             // $('#entregableN0').val(entregableTemp);
             //$('#tipoproyent').val('2');
         },
         error: function(data){

         }
    });
}




function listaEntregableN1(idEntN0){

  $.ajax({
      type:'GET',
      url: 'SelentregableHijo/'+idEntN0+'/'+$('#cproyecto').val()+'/'+$('#cproyectoedt_m').val(),
      success: function (data) {
        $('#entregableN1').chosen('destroy');
        $('#entregableN1').find('option').remove();
        $('#searchEnt').val('');
        limpiarlistaEntregables();

        for (var i = 0; i < data.length; i++) {

          $('#entregableN1').append($('<option></option>').attr('value',data[i].centregables).text(data[i].descripcion));

        }   

        agregarchosen('#entregableN1');

        //$('#entregableN1').append($('<option><p style="background-color:#FF0055;"></p></option>').attr('value','nuevoN1').text('-- Agregar nuevo entregable Nivel 1 --'));   

      buscarEntregableHijo($('#entregableN1').val());
  },
  error: function(data){

    $('#entregableN1').find('option').remove();

    $("#tablaEntregables > tbody tr").remove();

  }
});

}


$('#entregableN0').change(function(){
  if(this.value==''){
      return;
  }
  listaEntregableN1(this.value);

})

$('#entregableN1').change(function(){
    if(this.value==''){
        return;
    }
    limpiarlistaEntregables();

    buscarEntregableHijo(this.value);

})  

$('#searchEnt').keyup(function(){

    limpiarlistaEntregables();
    if($('#searchEnt').val()==""){

        buscarEntregableHijo($('#entregableN1').val());
    }

    else{ 

        searchEntregableHijo($('#entregableN1').val());
    }
})  

function buscarEntregableHijo(idEnt){
 $('#searchEnt').val('');

 $.ajax({
    type:'GET',
    url: 'SelentregableHijo/'+idEnt+'/'+$('#cproyecto').val()+'/'+$('#cproyectoedt_m').val(),
    success: function (data) {
      mostrarSeleccionarEntregables(data);

  },
  error: function(data){

  }
});

}

function searchEntregableHijo(idEnt){

  $.ajax({
    type:'GET',
    url: 'SearchEntregableHijo/'+idEnt+'/'+$('#searchEnt').val()+'/'+$('#cproyecto').val()+'/'+$('#cproyectoedt_m').val(),

    beforeSend: function () {

    },
    success: function (data) {

      mostrarSeleccionarEntregables(data);

    },
    error: function(data){

    }
  });

}

function mostrarSeleccionarEntregables(obj){

  limpiarlistaEntregables();

  for (var i = 0; i < obj.length; i++) {

    var row = "";
    row = "<tr ";  
    row += "data-identregable='"+obj[i].centregables+"'"; 
    row += "data-proyectoentregable='"+obj[i].cproyectoentregables+"'>"; 
    //row += "<td class='' title=''>"+"<input type='text' class='descripcionEnt form-control' value='"+obj[i].descripcion+"'></td>";


    row += "<td class='descripcionEntMaster'>"+'<input  id="desc_ent_master_'+obj[i].centregables+'" class="form-control descripcionEnt" type="text" value="'+obj[i].descripcion+'" style="display:none">';
    row += "<span id='sp_desc_ent_master_"+obj[i].centregables+"'> "+obj[i].descripcion+"</span></td>";

    //row += "<td class='' title=''>"+"<input type='text' class='descripcionEnt form-control' value='"+obj[i].descripcion+"'></td>";
    





    row += "<td width='10%' title='Codificacón según SIG'>"+"<input size='1' type='text' class='form-control' value='"+obj[i].tipodoc+"' readonly></td>";

    var habilitado='readonly';
    var clase_agregar='agregarEnt';

    if (obj[i].tipo_paquete==1) {
       //habilitado='';
       //clase_agregar='agregarEntGrupo'

    }
    
    row += "<td width='10%' class='cantEntregable' title='Ingresar cantidad del entregable *doble clic para activar este campo*'>"+"<input "+habilitado+" size='1' type='text' maxlength='2' onkeypress='return soloNumeros(event,this);' class='form-control cantidadEnt' value=''></td>";


    row += "<td width='5%' class='"+clase_agregar+"' title='Agregar entregable'><button type='button' class='btn-success'><i class='fa fa-plus' ></i></button></td>";

    if (obj[i].agregado=='si') {
      row +="<td width='5%' title='Entregable agregado'><a href='#' style='color: rgba(47, 172, 53, 1)'><i class='fa fa-check-square' aria-hidden='true' ></i></a></td>";
      //row += "<td class='agregarEntCOMENTAR_ESTO' title=''>"+"<span class='descripcionEnt'>"+obj[i].descripcion+"</span>"+"</td>";
    }
    else{

        row +="<td width='5%' title='Entregable no agregado'><a href='#' style='color: rgba(0, 105, 173, 1)'><i class='fa fa-square-o' aria-hidden='true' ></i></a></td>";
        //row += "<td class='quitarEntCOMENTAR_ESTO' title=''>"+"<span class='descripcionEnt'>"+obj[i].descripcion+"</span>"+"</td>";
    }


    /*if (obj[i].agregado=='no') {
      row +="<td class='agregarEnt' title='Agregar'><a href='#' style='color: rgba(0, 105, 173, 1)'><i class='fa fa-circle-o' aria-hidden='true' ></i></a></td>";
      //row += "<td class='agregarEntCOMENTAR_ESTO' title=''>"+"<span class='descripcionEnt'>"+obj[i].descripcion+"</span>"+"</td>";
    }
    else{

        row +="<td class='quitarEntCOMENTAR_ESTO' title=''><a href='#' style='color: rgba(0, 105, 173, 1)'><i class='fa fa-check-circle' aria-hidden='true' ></i></a></td>";
        //row += "<td class='quitarEntCOMENTAR_ESTO' title=''>"+"<span class='descripcionEnt'>"+obj[i].descripcion+"</span>"+"</td>";
    }*/


    row += "<td width='5%'><input title='Seleccionar entregable' type='checkbox' name='chkent[]' id='chkent' value='"+obj[i].centregables+'_'+obj[i].cproyectoentregables+"'></td>";   

    row += "</tr>";  


    $("#tablaEntregables > tbody").append(row);

    /*$(".agregarEntGrupo").off('click');
    $(".agregarEntGrupo").on("click",function() {

        var objEntregable=$(this).parent(); 

        //$('#cproyectoentregables').val($(this).data("proyectoentregable"));
        enviarFilaGrabar(objEntregable);

    });*/

    $(".descripcionEntMaster").off('click')
    $(".descripcionEntMaster").dblclick(function() {

      var idSpanDesc=$(this).find("input").attr("id"); 
      var valSpanDesc=$(this).text(); 

      //console.log(idSpanDesc+'    '+valSpanDesc);
      mostrar_input(idSpanDesc,valSpanDesc);

    }); 

    $(".descripcionEnt").focusout(function() {

      ocultar_input($(this).attr("id"),$(this).val());

    }); 



    $(".agregarEnt").off('click');
    $(".agregarEnt").on("click",function() {

        var objEntregable=$(this).parent(); 

        //$('#cproyectoentregables').val($(this).data("proyectoentregable"));
        enviarFilaGrabar(objEntregable);

    });
    $(".quitarEnt").off('click');
    $(".quitarEnt").on("click",function() {

        var objEntregable=$(this).parent(); 

        //activarEnt(objEntregable.data('proyectoentregable'),'1');

        eliminarEntregable(objEntregable.data('proyectoentregable'));

        //console.log(objEntregable.data('identregable'));

        //$('#cproyectoentregables').val($(this).data("proyectoentregable"));
        
        //saveEnt(objEntregable);

    });

    $(".cantidadEnt").off('click')
    $(".cantidadEnt").dblclick(function() {

      $(this).prop('readonly',null);

    }); 

    $(".cantidadEnt").focusout(function() {

      $(this).prop('readonly',true);

    }); 

    


  }
}

$("#btnSaveEnt").click(function(){

    $.ajax({
      url: 'grabarEntregablesSeleccionados',
      type:'POST',
      data: $("#frmentregable").serialize(),
      beforeSend: function () {
        $('#div_carga').show(); 
    },              
    success : function (data){

        $('#marcarTodo').prop('checked',false);

        limpiarlistaEntregables();
        buscarEntregableHijo($('#entregableN1').val())
        
            //limpiarEntregables();  
            $("#divLEntregable").html(data);
            $('#div_carga').hide(); 
            $('#tipoproyent').val('2').trigger('chosen:updated');
        },
    });

});



function limpiarlistaEntregables(){

  $("#tablaEntregables > tbody tr").remove();
  $('#marcarTodo').prop('checked',false);
  //$('#divEntAgregados').css("display", "none");
  //$("#tablaEntregablesAgregados > tbody tr").remove();

}


function enviarFilaGrabar(obj){

  if($('#tipoproyent').val()=='1'){
    var padre=$("input[name='radio_ent']:checked").val();

    if (!padre) {
      swal('Seleccione un entregable contractual');
      return;
    }
  }

  var objEnt={cproyecto: "", centregable: "", cproyectoedt_m: "", tipoproyent: "", observacionent: "", responsable: "", cproyectoentregables: "", cproyectoentregables_parent: "", fase: "", descripcion_entregable: "", cantidad_entregable: ""};    

  objEnt.cproyecto=$('#cproyecto').val();
  objEnt.centregable=$(obj).data("identregable");
  objEnt.cproyectoedt_m=$('#cproyectoedt_m').val();
  objEnt.tipoproyent=$('#tipoproyent').val();
  objEnt.observacionent=$('#observacionent').val();
  //objEnt.responsable=$('#responsable').val();
  objEnt.descripcion_entregable=$(obj).find(".descripcionEnt").val();
  objEnt.fase=$('#fasesEdt').val();
  objEnt.cproyectoentregables_parent=$("input[name='radio_ent']:checked").val();
  objEnt.cantidad_entregable=$(obj).find(".cantEntregable").find("input").val();
  objEnt._token = $('input[name="_token"]').first().val();


  if (objEnt.cantidad_entregable.length<=0) {
    saveEnt(objEnt);
  }

  else {
    saveEntGrupo(objEnt);
  }

} 

function saveEnt(objEnt){ 


  $.ajaxSetup({
    header: document.getElementById('_token').value
  });

  $.ajax({
    url: 'grabarEntregablesProyecto',
    type:'POST',
    data: objEnt,
    beforeSend: function () {
      $('#div_carga').show(); 
  },              
    success : function (data){

      $('#marcarTodo').prop('checked',false);
      limpiarlistaEntregables();
      buscarEntregableHijo($('#entregableN1').val())

      //limpiarEntregables();  
      $("#divLEntregable").html(data);
      $('#div_carga').hide(); 

      //$('#tipoproyent').val('2');
    },
  });
}

function saveEntGrupo(objEnt){ 


  $.ajaxSetup({
    header: document.getElementById('_token').value
  });

  $.ajax({
    url: 'agregarEntregablesGrupos',
    type:'POST',
    data: objEnt,
    beforeSend: function () {
      $('#div_carga').show(); 
  },              
    success : function (data){

      $('#marcarTodo').prop('checked',false);
      limpiarlistaEntregables();
      buscarEntregableHijo($('#entregableN1').val())

      //limpiarEntregables();  
      $("#divLEntregable").html(data);
      $('#div_carga').hide(); 

      //$('#tipoproyent').val('2');
    },
  });
}

function eliminarEntregable(idEntPry){

  $.ajax({
    url: 'eliminarEntregablePry/'+idEntPry,
    type:'GET',
    beforeSend: function () {
        $('#div_carga').show(); 


    },              
    success : function (data){              
      $('#marcarTodo').prop('checked',false);
      limpiarlistaEntregables();
      buscarEntregableHijo($('#entregableN1').val())

      //limpiarEntregables();  
      $("#divLEntregable").html(data);
      $('#div_carga').hide();  


  },
});

}


function verEntregablesEDT(id){

   $.ajax({
    url: 'viewEntregables/'+id,
    type:'GET',
    beforeSend: function () {
        $('#div_carga').show(); 


    },              
    success : function (data){    
      $("#divLEntregable").html(data);
      $('#div_carga').hide();   


  },
});
}

function activarEnt(id,act){
    //swal('1');
    $.ajax({
      url: 'activarEntregable/'+id+'/'+act,
      type:'GET',
      beforeSend: function () {
        $('#div_carga').show(); 
    },              
    success : function (data){

        $("#divLEntregable").html(data);
        $('#div_carga').hide(); 
    },
});
  //});
}

function limpiarEntregables(){

  $('#cproyectoentregables').val('');
  $('#disciplina').val('');
  $('#tipoentregable').val('');   
  $('#entregableN0').val('');

  $('#entregableN1').val('').trigger('chosen:updated');

  $('#codigoent').val('');
  //$('#tipoproyent').val('');
  $('#observacionent').html('');
  $('#marcarTodo').prop('checked',false);

  $('#responsable').val('');

}

function limpiarfechas(){
  $('#fecha').val('');
  $('#tipofecha').val('');
  $('#cproyectoentfecha').val('');
}



/********************** TABLA EDT *********************************/



  // $("a.agregarnivel").on("click",function() {
  //   tabla_eventos_edt(); 
  // });  

  // $("a.agregarEntregables").on("click",function() {
  //   tabla_eventos_edt();
  // }); 

  // $("a.editarEdt").on("click",function() {
  //   tabla_eventos_edt();
  // }); 

  // $(".descripcion").on("click",function() {
  //   tabla_eventos_edt();
  // });

  // $(".codigo").on("click",function() {
  //   tabla_eventos_edt();
  // });

  // $(".fase").on("click",function() {
  //   tabla_eventos_edt();
  // });

  // $(".faseEdt").on("click",function() {
  //     tabla_eventos_edt();
  // }); 
  // $("a.verLE").on("click",function() {
    tabla_eventos_edt(); 
  // }); 




  function tabla_eventos_edt(){     

    $("a.agregarnivel").off('click');

    $("a.agregarnivel").on("click",function() {    
      var indice=$(this).parent().parent().index();  
      grabar_edt($(this),indice);      

    });

    $("a.deleteEDT").off('click');
    $("a.deleteEDT").on("click",function() {    
      var idEdt=$(this).parent().parent().data("edt");
      delete_update_edt(idEdt);

    }); 

    $("a.agregarEntregables").off('click');
    $("a.agregarEntregables").on("click",function() {
      
      var idEdt=$(this).parent().parent().data("edt"); 
      var descripcion=$(this).parent().parent().find(".descripcion").first().text();
      var codigo=$(this).parent().parent().find(".codigo").first().text();
     
      faddrEnt(idEdt,descripcion,codigo);
    
    });  

    $("a.editarEdt").off('click');
    $("a.editarEdt").on("click",function() {
      
      var idEdt=$(this).parent().parent().data("edt"); 
      EditarEDT(idEdt);
     
    }); 

    $(".codigoEdt").focusout(function() {

      actualizar_edt($(this),'codigo');

    }); 

    $(".nombreEdt").focusout(function() {

      actualizar_edt($(this),'descripcion');

    }); 

    $(".faseEdt").focusout(function() {

      actualizar_edt($(this),'fase');

    }); 

    $(".descripcion").off('click')
    $(".descripcion").on("dblclick",function() {

      var idSpanDesc=$(this).find("input").attr("id"); 
      var valSpanDesc=$(this).text(); 
      mostrar_input(idSpanDesc,valSpanDesc);

    }); 

    $(".codigo").off('click')
    $(".codigo").on("dblclick",function() {

      var idSpanCod=$(this).find("input").attr("id"); 
      var valSpanCod=$(this).text(); 

      mostrar_input(idSpanCod,valSpanCod);
   
    });

    $(".fase").off('click')
    $(".fase").on("click",function() {


      var idSpanFase=$(this).find("select").attr("id"); 
      var valSpanFase=$(this).find("select").text(); 
     
      mostrar_input(idSpanFase,valSpanFase);

    });

    $("a.verLE").off('click');
    $("a.verLE").on("click",function() {    
      var idEdt=$(this).parent().parent().data("edt"); 
      verLEporEDT(idEdt);
      var nomEDT= $(this).parent().parent().find(".descripcion").find("span").text();
      var codigo= $(this).parent().parent().find(".codigo").find("span").text();
      if (codigo.length>0) {
        codigo+=' - ';

      }
      var encabezado= codigo+nomEDT;

      
      $('#cpryedt').val(idEdt);
      $('#nombreEDTSeleccionado').html(encabezado);
      $('#collapseEnt').addClass('in');


    }); 

    $(".descripcionEntMaster").off('click');
    $(".descripcionEntMaster").on("click",function() {

      var idSpanCod=$(this).find("input").attr("descripcionEnt"); 
      var valSpanCod=$(this).text(); 

      mostrar_input(idSpanCod,valSpanCod);
    
    });


  } 

  function grabar_edt(obj,indice){  
 
    var objEDT={cproyecto: "", cproyectoedt_parent: 0, cproyectoent_rev: ""};    

    objEDT.cproyecto=$('#cproyecto').val();
    objEDT.cproyectoedt_parent=$(obj).parent().parent().data("edt");
    objEDT.cproyectoent_rev=$('#cod_revisionInt').val();
    objEDT._token = $('input[name="_token"]').first().val();

    $.ajax({
            url: 'grabarEDTFila',
            type:'POST',
            data: objEDT,
            beforeSend: function () {
              $('#div_carga').hide(); 
            },              
            success : function (data){
              $('#div_carga').hide(); 

              var proyectoEdt=data;             

              agregar_fila_edt(proyectoEdt,indice);

            },
    });   
   
  }

  function delete_update_edt(idEdt){
    console.log(idEdt);
    $.ajax({
      url: 'delEDTPy/'+idEdt,
      type: 'GET',
    })
    .done(function(data) {
      console.log("delete_update_edt",data);

      swal({
        title: "¿Estas seguro que deseas eliminar este EDT?",
        text: "Este EDT cuenta con "+data+ " entregables",
        type: "info",
        className: "red-bg",
        showCancelButton: true,
        dangerMode: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Eliminar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          if (data > 0) {updateestadoedt(idEdt);} else {delete_edt(idEdt);};
        } else {
          swal("Cancelado", "No se ha elimina el EDT con "+data+" entregables.", "error");
        }
      });

    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }

  function updateestadoedt(idedt){
    $.ajax({
      url: 'updateestadoedt/'+idedt,
      type: 'GET',
    })
    .done(function(data) {
      console.log("updateestadoedt",data);
      swal({
            title: "Eliminado",
            text: "Se ha eliminado el EDT con "+data+" entregables.",
            type: "success",
            timer: 3000,
            buttons: false,
          });
      verDetalleEDT();
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }

  function delete_edt(idedt){
    $.ajax({
      url: 'delete_edt/'+idedt,
      type: 'GET',
    })
    .done(function(data) {
      console.log("delete_edt",data);
      swal({
            title: "Eliminado",
            text: "Se ha eliminado el EDT con "+data+" entregables.",
            type: "success",
            timer: 3000,
            buttons: false,
          });
      verDetalleEDT();
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }

  
  function agregar_fila_edt(proyectoEdt,indice){

    var row = "";
    var colorNivel='';
    var colorLetra='';


    if (proyectoEdt.nivel==1) { colorNivel='rgb(0,105,170)';  colorLetra='white';}
    else if(proyectoEdt.nivel==2){ colorNivel='rgb(128,128,128)';  colorLetra='black';}
    else if(proyectoEdt.nivel==3){ colorNivel='rgb(166,166,166)';  colorLetra='black';}
    //else { colorNivel='rgb(166,166,166)';}

    row = "<tr data-edt='"+proyectoEdt.cproyectoedt+"'";
    row+="style='background-color:"+colorNivel+";color:"+colorLetra+"'>";
    row += "<td><a class='verLE' data-toggle='tooltip' data-placement='top' title='Ver LE' style='color: red;font-size: 13px'><i class='fa fa-eye' aria-hidden='true'></i></a></td>";
    row += "<td><a class='agregarnivel' data-toggle='tooltip' data-placement='top' title='Añadir nivel EDT' style='color: black;font-size: 13px'><i class='fa fa-level-down' aria-hidden='true'></i></a></td>";
    row += "<td >"+proyectoEdt.nivel+"</td>";
    // row += "<td class='fase'><span id='sp_fase_"+proyectoEdt.cproyectoedt+"'></span>";
    // row += " <div id='divfase_"+proyectoEdt.cproyectoedt+"'></div></td>";
    row += "<td class='codigo'>"+'<input id="cod_'+proyectoEdt.cproyectoedt+'" class="form-control codigoEdt" type="text" size="3" name="">';
    row += "<span id='sp_cod_"+proyectoEdt.cproyectoedt+"' style='display:none'></span></td>";
    row += "<td class='descripcion'>"+'<input id="des_'+proyectoEdt.cproyectoedt+'"class="form-control nombreEdt" type="text" name="">';
    row += "<span id='sp_des_"+proyectoEdt.cproyectoedt+"' style='display:none'></span></td>";
    row += "<td style='text-align: center;'>0</td>";
    row += "<td style='text-align: center;'>0</td>";
    row += "<td style='text-align: center;'>0</td>";
    row += "<td style='text-align: center;'>0</td>";
    row += "<td style='text-align: center;'>0</td>";
    row += "<td style='text-align: center;'>"+'<a data-toggle="tooltip" data-placement="top" class="agregarEntregables" title="Añadir LE" style="color: black;font-size: 13px"><i class="fa fa-plus" aria-hidden="true"></i></a>';
    //row += '<a href="#" class="editarEdt" title="Editar EDT" style="color: yellow;font-size: 13px"><i class="fa fa-pencil" aria-hidden="true" ></i></a> &nbsp;'+"</td>"
    row += "&nbsp;&nbsp;&nbsp; <a data-toggle='tooltip' data-placement='top' title='Eliminar EDT' class='deleteEDT' style='color: red;font-size: 13px'><i class='fa fa-trash'></i></a></td>"
    row += "</tr>";


   
    numeroFila= $("#tablaEDT > tbody > tr")[indice];

    $(numeroFila).after(row);

    if (proyectoEdt.ctipoedt!=1) {
      
    obtenerFasesProy(proyectoEdt.cproyectoedt);
    }
    tabla_eventos_edt();
    
    
  }

  function actualizar_edt(obj,act){

    var objEDT={cproyectoedt: "", codigo: "", descripcion: "", actualizar:"", fase: ""};
    if(act=='codigo'){
      objEDT.codigo=$(obj).val();
      objEDT.cproyectoedt=$(obj).parent().parent().data("edt");
    }
    if(act=='descripcion'){
    objEDT.descripcion=$(obj).val(); 
    objEDT.cproyectoedt=$(obj).parent().parent().data("edt");     
    }

    if(act=='fase'){
      objEDT.cproyectoedt=$(obj).parent().parent().parent().data("edt");
    objEDT.fase=$(obj).val();      
    }

    objEDT.actualizar=act;

    var idInput=$(obj).attr("id");

    $.ajax({
            url: 'actualizarEDT',
            type:'GET',
            data: objEDT,

            success : function (data){
              $('#div_carga').hide(); 

              if(act=='fase'){
                ocultar_input(idInput,$("#"+idInput+" option:selected").text()); 
              }

              else{
                ocultar_input(idInput,$(obj).val());  
              }
            },
    }); 
    
  }

  function ocultar_input(idInput,valor){
 
    $("#"+idInput).css("display", "none"); 
    $("#sp_"+idInput).html(valor);
    $("#sp_"+idInput).css("display", "block"); 

  }

  function mostrar_input(idInput,valor){

   
    $("#sp_"+idInput).css("display", "none"); 
    //$("#"+idInput).val(valor);
    $("#"+idInput).css("display", "block");
    $("#"+idInput).focus();

  }

  function obtenerFasesProy(idEnt,cfase){

    $.ajax({
            url: 'getFasesProy',
            type:'GET',

            success : function (data){
             
              $('#div_carga').hide(); 
               listarFasesProy(data,idEnt,cfase);
            },
    });

  }

  function listarFasesProy(obj,idEnt,cfase){

    var select= '';
    select = '<select class="form-control faseEntregable" name="faseEntr" id="fase_'+idEnt+'" style="display: none">';
    select+= '<option value=""></option>';

    for (var i = 0 ; i < obj.length; i++) {
      var fase='';

      if (obj[i].cfaseproyecto_gestion==cfase) {
          fase=' selected="'+obj[i].cfaseproyecto_gestion+'"';
      }

      select+= '<option'+fase+' value="'+obj[i].codigo+'">'+obj[i].descripcion+'</option>';
      
    }
    select+='</select>';


    $("#divfase_"+idEnt).html(select);

    tabla_eventos_EntregablesTodos();

  }

  function verLEporEDT(edt){

    var cproyecto=$('#cproyecto').val();

    $.ajax({
      url: 'verTodosEntregablesPorEDT/'+edt,
      type:'GET',
      beforeSend: function () {
          //$('#div_carga').show(); 
      },              
      success : function (data){              
          $('#div_carga').hide();     
          $("#tableEntdivLEntregable").html(data);
          $("a.ocultarAnexos").hide();
          obtenerFechaCabEDT(); 
        },
      });


  }

  $("#divLE").on("hidden.bs.modal", function () {
    verDetalleEDT();
  });

  function verDetalleEDT(){


    $.ajax({
      url: 'verDetalleEDT/'+$('#cproyecto').val()+'/'+$('#cod_revisionInt').val(),
      type:'GET',
      beforeSend: function () {
          $('#div_carga').show(); 

        },              
      success : function (data){              
          $('#div_carga').hide();     
          $("#divTableEDT").html(data);
          tabla_eventos_edt();
        },
      });

  }
/*******************************************************************/


$('#tipoproyent').change(function(){
  limpiarEntregables();

  $('#tablaEntregablesAgregados').dataTable().fnDestroy();
  $("#tablaEntregablesAgregados > tbody tr").remove();

  $("#tablaEntregables > tbody tr").remove();
  $('#marcarTodo').prop('checked',false);

  //$('#tipoentregable').val('');   
  //$('#disciplina').val('');

  if($('#tipoproyent').val()=='1'){

    $('#divEntAgregados').css("display", "block");

    todosEntregableAgregado();

    obtenerEntregablesCombo('1');


    //$('#tipoentregable').prop("disabled",true);   
    //$('#disciplina').prop("disabled",true);

    //todosEntregableAgregado();
        
  }
  else{
    $('#divEntAgregados').css("display", "none");

    $('#tipoentregable').prop("disabled",false);   
    $('#disciplina').prop("disabled",false);
    $("input[name='radio_ent']").prop('checked',false);

    obtenerEntregablesCombo('2');

  }

})

function obtenerEntregablesCombo(tipoEnt){

  $.ajax({
        type:'GET',
        url: 'listarComboEntregablesXTipo/'+tipoEnt,
        success: function (data) {  
          listarEntregablesCombo(data);
        },
        error: function(data){

        }
  });

}

function listarEntregablesCombo(obj){

  $('#tipoentregable').chosen('destroy')
  $('#tipoentregable').find('option').remove();
         
  for (var i = 0; i < obj.length; i++) {

    $('#tipoentregable').append($('<option></option>').attr('value',obj[i].ctiposentregable).text(obj[i].descripcion));

  } 

  agregarchosen('#tipoentregable')

  obtenerDisciplina($('#tipoentregable').val(),'');
   
}



function obtenerEntregAgregadosXEDT(val,descripcion){

   $.ajax({
        type:'GET',
        url: 'obtenerEntregAgregadosXEDT/'+val+'/'+descripcion+'/'+$('#cproyectoedt_m').val(),
        success: function (data) {  


          //$('#tablaEntregablesAgregados').dataTable().fnDestroy();

          mostrarEntregablesAgregados(data);

      },
      error: function(data){

      }
    });
} 


function todosEntregableAgregado(){

  obtenerEntregAgregadosXEDT('todos','ninguno');
}


function searchEntregableAgregado(descripcion){


  obtenerEntregAgregadosXEDT('search',descripcion);

}

$('#searchEntAgregado').keyup(function(){

    //limpiarlistaEntregables();
    if($('#searchEntAgregado').val()==""){

        todosEntregableAgregado();
    }

    else{ 

        searchEntregableAgregado($('#searchEntAgregado').val());
    }
}) 





function mostrarEntregablesAgregados(obj){

  


  for (var i = 0; i < obj.length; i++) {

    var row = "";
    row = "<tr>";  
    row += "<td>"+"<span'>"+obj[i].descripcion_entregable+"</span>"+"</td>";
    row += "<td>"+obj[i].disciplina+"</td>";
    row += "<td>"+obj[i].tipoentregable+"</td>"
    row += "<td><input onclick='seleccionarContractual($(this))' type='radio' data-ctipo='"+obj[i].ctiposentregable+"' data-cdisciplina='"+obj[i].cdisciplina+"' name='radio_ent' value='"+obj[i].cproyectoentregables+"'></td></tr>";   

    $("#tablaEntregablesAgregados > tbody").append(row);


  }
   
    agregardatatable();
}

function seleccionarContractual(obj){
 
  //$('#tipoentregable').val($(obj).data('ctipo'));   
  //$('#disciplina').val($(obj).data('cdisciplina'));
  //obtenerDisciplina($(obj).data('ctipo'),$(obj).data('cdisciplina'));
  //$('#tipoentregable').prop("disabled",false);   
  //$('#disciplina').prop("disabled",false);
}

var fechasCab=[];

function obtenerFechaCabEDT(){

  
  $.ajax({
          url: 'obtenerCabeceraFechasXEDT/'+$('#cpryedt').val(),
          type:'get',
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){
            $('#div_carga').hide(); 

            fechasCab=data;

          },
  });  
}

function mostrarFormularioNuevoEnt($nivel){

}


function soloNumeros(e,obj) {

    // Backspace = 8, Enter = 13, ’0' = 48, ’9' = 57, ‘.’ = 46
    
    var field = obj;

    key = e.keyCode ? e.keyCode : e.which;
    
    if (key == 8 || key ==9 || key ==11 || (key > 47 && key < 58)) return true;
    if (key > 47 && key < 58) {
      if (field.value === "") return true;
      var existePto = (/[.]/).test(field.value);
      
      if (existePto === false){
          regexp = /.[0-9]{2}$/; //PARTE ENTERA 10
      }
      else {
        regexp = /.[0-9]{2}$/; //PARTE DECIMAL2
      }
      return !(regexp.test(field.value));
    }
    if (key == 46) {
      if (field.value === "") return false;
      regexp = /^[0-9]+$/;
      return regexp.test(field.value);
    }

    return false;

}
$("#btnPrintEDT").on('click',function(){  
    printEDT();        
});   

function printEDT(){

  var id =$('#cproyecto').val();

  console.log(id);

  if (id.length > 0 )
  {
      win = window.open('printEDTProy/'+id,'_blank');
  }
  else
  {
      $('.swal').show();
  }
  
}

function agregarchosen(id_elemento){

  $(id_elemento).chosen({
            allow_single_deselect: true,width:"100%"
  });

}

