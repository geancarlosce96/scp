$(document).ready(function() {

		table_eventos()	;
		addchosen('cliente');
		addchosen('minera');
		addchosen('contacto');
		addchosen('select_cliente');
		addchosen('select_minera');
		addchosen('select_contacto');
		addchosen('select_cliente_pro');
		addchosen('select_minera_pro');
		addchosen('select_contacto_pro');
		addchosen('select_portafolio');
		addchosen('select_tipoproyecto');
		addchosen('aniocod');
		addchosen('sedecod');

	});

var objResponse = {};

	function table_eventos(){

		$("#cliente").on("change",function(){
			$('#cliente_nombre').val($('#cliente option:selected').text());
			if ($('#cliente').val()=="") {
				$('#minera').val('');
			}
			else{
				minera($('#cliente').val(),'nuevo');
			};
		})

		$("#minera").on("change",function(){ 
			console.log($('#minera').val());
			$('#mineraid').val($('#minera').val())
			$('#minera_nombre').val($('#minera option:selected').text());
			if ($('#minera').val()=="") {
				$('#contacto').val('');
			}
			else{
				contacto($('#minera').val(),'nuevo');
				$('#cpersona_u').val($('#cliente').val());
			};
		})

		$("#select_portafolio").on("change",function(){ 
			console.log($('#select_portafolio').val());
			// $('#mineraid').val($('#select_portafolio').val())
			// $('#minera_nombre').val($('#select_portafolio option:selected').text());
			if ($('#select_portafolio').val()=="") {
				$('#select_tipoproyecto').val('');
			}
			else{
				tipoproyecto($('#select_portafolio').val());
				// $('#cpersona_u').val($('#cliente').val());
			};
		})

		// $('#fecha').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		// $('#fecha_visita').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_visita').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		// $('#fecha_consulta').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_consulta').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		// $('#fecha_absolucion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_absolucion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});

		$('#fecha_presentacion_pro').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"
			}).on("input changeDate", function (e) {
				console.log($(this).val());
				$("#fecha_interna").val(moment($(this).val()).subtract(1, 'days').format('YYYY-MM-DD')); 
			});

		$('.fecha_interna').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});

		$('.fecha_presentacion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});

		$("#divUmineraContacto").on("hidden.bs.modal", function () {
			$('#divUminera').modal('hide');
		});

		
	}

	function limpiar(){
		$('#minera').val('');
		$('#cargo').val('');
		$('#contacto').val('');
		$('#telefono').val('');
		$('#anexo').val('');
		$('#correo').val('');
	}

	function tipoproyecto(id){
		$.ajax({
            type:'GET',
            url: 'listTipoPry/'+id,
            success: function (data) {
            	console.log(data);
                var str = JSON.stringify(data);
                var pushedData = jQuery.parseJSON(str);              
                $('#select_tipoproyecto').find('option').remove();
                $('#select_tipoproyecto').append($('<option></option>').attr('value','').text(''));
                $.each(pushedData, function(i, serverData)
                {
                    $('#select_tipoproyecto').append($('<option></option>').attr('value',i).text(serverData));
	                $('#select_tipoproyecto').val(i).trigger('chosen:updated');
                });
            },
            error: function(data){

            }

        });
		
	}

	function minera(id,valor){ 

		$.ajax({
			url: 'minera/'+id,
			type: 'GET',
		})
		.done(function(data) {
			console.log("success");
			console.log(data);

			$('#minera').find('option').remove();
 			$('#contacto').find('option').remove();

			limpiar();

			var cunidadminera_temp = 0;
			for (var i = 0; i < data.length; i++) {
				$("#minera").append('<option value="'+data[i].cunidadminera+'">'+data[i].nombre+'</option>');
				$("#select_minera").append('<option value="'+data[i].cunidadminera+'">'+data[i].nombre+'</option>');
				$("#select_minera_pro").append('<option value="'+data[i].cunidadminera+'">'+data[i].nombre+'</option>');
			};
				cunidadminera_temp = data[0].cunidadminera;
			if (valor=='nuevo') {
				$('#minera').val(cunidadminera_temp).trigger('chosen:updated');
				$('#mineraid').val(cunidadminera_temp)
				$('#minera_nombre').val(data[0].nombre)

			};
			if (valor=='edit') {
				$('#minera').val(objResponse.cunidadminera).trigger('chosen:updated');
			};
			if (valor=='pre') {
				$('#select_minera').val(objResponse.cunidadminera).trigger('chosen:updated');
			};
			if (valor=='pro') {
				$('#select_minera_pro').val(objResponse.cunidadminera).trigger('chosen:updated');
			};

			contacto(objResponse.cunidadminera,valor);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}

	function contacto(id,valor){ 
		console.log(valor);
		$.ajax({
			url: 'contacto/'+id,
			type: 'GET',
		})
		.done(function(data) {
			console.log("success");
			$('#contacto').find('option').remove();
			limpiar();
			var cunidadmineracontacto_temp = 0;
			for (var i = 0; i < data.length; i++) {
				var contacto = data[i].nombres +' '+ data[i].apaterno +' '+ data[i].amaterno;
				$("#contacto").append('<option value="'+data[i].cunidadmineracontacto+'">'+contacto+'</option>');
				$('#cargo').val(data[i].descripcion);
				$('#telefono').val(data[i].telefono);
				$('#anexo').val(data[i].anexo);
				$('#correo').val(data[i].email);

				$("#select_contacto").append('<option value="'+data[i].cunidadmineracontacto+'">'+contacto+'</option>');
				$('#cargo_pre').val(data[i].descripcion);
				$('#telefono_pre').val(data[i].telefono);
				$('#anexo_pre').val(data[i].anexo);
				$('#correo_pre').val(data[i].email);

				$("#select_contacto_pro").append('<option value="'+data[i].cunidadmineracontacto+'">'+contacto+'</option>');
				$('#cargo_pro').val(data[i].descripcion);
				$('#telefono_pro').val(data[i].telefono);
				$('#anexo_pro').val(data[i].anexo);
				$('#correo_pro').val(data[i].email);			


				cunidadmineracontacto_temp = data[i].cunidadmineracontacto;
			};

			console.log(cunidadmineracontacto_temp);

			if (valor=='pre') {
				$('#select_contacto').val(cunidadmineracontacto_temp).trigger("chosen:updated");
			};
			if (valor=='pro') {
				$('#select_contacto_pro').val(cunidadmineracontacto_temp).trigger("chosen:updated");
			};
			if (valor=='edit') {
				$('#contacto').val(objResponse.cunidadmineracontacto).trigger("chosen:updated");
			};
			if (valor=='nuevo'){
			$('#contacto').val(cunidadmineracontacto_temp).trigger("chosen:updated");
			};
			

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}

	function verModals(){ 
		var cli = $('#cliente').val();
		var um = $('#minera').val();
		if (cli && um) {
			// alert($('#cpersona_u').val(cli));
		$('#cunidadminera_c').val(um);
		$('#nombre_umc').val($('#uminera_nombre').val());
		$('#cpersona_u').val(cli);
		$('#divUmineraContacto').on('show.bs.modal',function(e){

			console.log(um);
			contactoUminera(um);
         
          });
		
		}
		if (cli && !um) {
		$('#cpersona_u').val(cli);
		$('#divUminera').modal('show');
		$('#ModalUminera').css('display','none');
		}
		if (cli == '') {
		$('#modal').modal('show');
		$('#ModalUminera').css('display','none');
		}

	}


	function verCliente(){
		$.ajax({
			url: 'verCliente',
			type: 'GET',
		})
		.done(function(data) {
			console.log("success");
			console.log(data)
			removerchosen('cliente');
			limpiar();
			$('#cliente').find('option').remove();
			for (var i = 0; i < data.length; i++) {
				$("#cliente").append('<option value="'+data[i].cpersona+'">'+data[i].nombre+'</option>');
			};

			addchosen('cliente');
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}

	function selectcliente(idcliente,valor){
		// alert(idcliente+'--'+valor);
		if (valor=='edit') {
			$('#cpersona_u').val(idcliente);
			$('#cliente').val(objResponse.cpersona).trigger('chosen:updated');
		};
		if (valor=='pre') {
			$('#select_cliente').val(objResponse.cpersona).trigger('chosen:updated');
		};
		if (valor=='pro') {
			$('#select_cliente_pro').val(objResponse.cpersona).trigger('chosen:updated');
		};
	}

	function getEdit(id,valor){

		$.ajax({
			url: 'getEdit/'+id,
			type: 'GET',
		})
		.done(function(data) {
			objResponse = data[0];
			console.log(objResponse);
			console.log("success");
			var contacto = objResponse.nombres +' '+ objResponse.apaterno +' '+ objResponse.amaterno

			$('#collapseOne').addClass('panel-collapse collapse in');
			$('#collapseOne').attr('aria-expanded', 'true');
			$('#collapseOne').css('height', '186px');
			$('.collapsed').attr('aria-expanded', 'true');

			limpiar();

			selectcliente(objResponse.cpersona,valor);
			minera(objResponse.cpersona,valor);

			$('#invitacion').val(objResponse.nombre_invitacion);
			$('#contacto').val(contacto);
			$('#cargo').val(objResponse.descripcion);
			$('#telefono').val(objResponse.telefono);
			$('#anexo').val(objResponse.anexo);
			$('#correo').val(objResponse.email);
			$('#fecha').val(objResponse.fecha_invitacion);


		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}

// Inicio de preliminar
    function codigo(valor){
    	if (valor=='pre') {
    	$('#propuesta').css('display', 'none');
    	$('#preliminar').css('display', 'block');
    	$('#titulo').text("Generar Preliminar")
    	var codigopre = $('#anio').val()+'.'+$('#correlativopre').val();
    	$('#codigopre').val(codigopre);
    	};
		if (valor=='pro') {
		$('#preliminar').css('display', 'none');
		$('#propuesta').css('display', 'block');
		$('#titulo').text("Generar Propuesta")
		var codigopro=$('#anio').val()+'.'+$('#sedecod').val()+'.'+$('#correlativopro').val();
		$('#codigopro').val(codigopro);
		};
    }
	
	function generarPre(valor,id){
		// alert(id);
		$.ajax({
			url: 'generarPre/'+id,
			type: 'GET',
		})
		.done(function(data) {
			objResponse = data[0];
			console.log("success");
			console.log(objResponse);
			var contacto = objResponse.nombres +' '+ objResponse.apaterno +' '+ objResponse.amaterno

			limpiar();

			selectcliente(objResponse.cpersona,valor);
			minera(objResponse.cpersona,valor);

			$('#generar').modal('show');
			codigo(valor);
			$('#invitacion_id').val(objResponse.id)
			$('#cliente_id').val(objResponse.cpersona)
			$('#cliente_nombre').val(objResponse.nombre)
			$('#minera_id').val(objResponse.cunidadminera)
			$('#minera_nombre').val(objResponse.uminera)

			$('#nombre_preliminar').val(objResponse.nombre_invitacion)

			$('#contacto_id').val(objResponse.contacto_id)
			$('#contacto_nombre').val(contacto)

			$('#cargo_pre').val(objResponse.descripcion)
			$('#telefono_pre').val(objResponse.telefono)
			$('#anexo_pre').val(objResponse.anexo)
			$('#correo_pre').val(objResponse.email)

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}
		

    function generarPro(valor,id){
    	// alert(valor+"-"+id);

    	$.ajax({
    		url: 'generarPro/'+id,
    		type: 'GET',
    	})
    	.done(function(data) {
    		objResponse = data[0];
    		console.log("success");
    		console.log(data)
    		var contacto = objResponse.nombres +' '+ objResponse.apaterno +' '+ objResponse.amaterno

			limpiar();

			selectcliente(objResponse.cpersona,valor);
			minera(objResponse.cpersona,valor);

    		$('#generar').modal('show');
			codigo(valor);
			$('#invitacion_id_pro').val(objResponse.id)
			$('#cliente_id').val(objResponse.cpersona)
			$('#cliente_nombre').val(objResponse.nombre)
			$('#minera_id').val(objResponse.cunidadminera)
			$('#minera_nombre').val(objResponse.uminera)

			$('#nombre_propuesta').val(objResponse.nombre_invitacion)

			$('#contacto_id').val(objResponse.contacto_id)
			$('#contacto_nombre').val(contacto)
			
			$('#cargo_pro').val(objResponse.descripcion)
			$('#telefono_pro').val(objResponse.telefono)
			$('#anexo_pro').val(objResponse.anexo)
			$('#correo_pro').val(objResponse.email)

    	})
    	.fail(function() {
    		console.log("error");
    	})
    	.always(function() {
    		console.log("complete");
    	});
    	
    }


