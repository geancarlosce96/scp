$(document).ready(function(){
	actualizar_perido_semana();
	obtenerProyectos();

	$("#listaProyectos").change(function(){
		cproyecto = $("#listaProyectos").val();
		actualizar_perido_semana();
		obtenerActividades();
	});

	$("#comboListaActividades").change(function(){
		var filtro = filtrarActividad();
		mostrarActividades(filtro);
		
		if(filtro.length>0){
			if(filtro.length == 1)
			{
				if(filtro[0].permitirRegistro == false)
					$("#tablaUsuarios tbody").empty();
				else
					obtenerUsuarios();				
			}
			else
				obtenerUsuarios();
		}
	});

	$("#chkUsuariosHorasRegistradas").click(function(){
		cambiarOpcionMostrarUsuariosConHoras();
		obtenerActividades();
	});

	$("#chkActividadesHorasRegistradas").click(function(){
		cambiarOpcionMostrarActividadesConHoras();
		obtenerActividades();
	});

	$("#chkUsuarios").click(function(){
		obtenerUsuarios();
	});

	$(".btn-ver-resumen").click(function(){		
		mostrarResumen(this);
	});

	$('#fecha').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es", startDate: "<?php echo date('d/m/Y'); ?>",
	todayHighlight: true,}).change(function(){
		cproyecto = $("#listaProyectos").val();
		actualizar_perido_semana();
		obtenerActividades();
	});
});

var cproyecto = 0;
var cproyectoactividades = 0;
var mostrarUsuariosConHoras = false;
var mostrarActividadesConHoras = false;
var listaActividades = [];
var listaUsuarios = [];

function mostrarResumen(obj)
{
	var url = BASE_URL + '/planificacion/resumen/diaria';
	var objRequest = {};
	objRequest.cproyecto = cproyecto;
	objRequest.cproyectoactividades = cproyectoactividades;
	objRequest.fecha = $("#fecha").val();
	objRequest.tipo = $(obj).data("tipo");

	$.get(url , objRequest)
		.done(function( data ) {
		$("#divTablaResumen").html(data);
		$("#modalResumen").modal("show");
	});
}

function cambiarOpcionMostrarUsuariosConHoras()
{
	mostrarUsuariosConHoras = $("#chkUsuariosHorasRegistradas").prop("checked");
}

function cambiarOpcionMostrarActividadesConHoras()
{
	mostrarActividadesConHoras = $("#chkActividadesHorasRegistradas").prop("checked");	
}

function obtenerProyectos()
{
		var url = BASE_URL + '/planificacion/obtenerProyectos';
		$.get(url , {})
		  	.done(function( data ) {		  		
		    	//console.log("obtenerProyectos", data);
		    	mostrarProyectos(data.lista);
		    	 $(".chosen-select").chosen({no_results_text: "No se ha encontrado un código o proyecto."});
		});
}

function mostrarProyectos(lista)
{
		for (var i = 0; i < lista.length; i++) {
			$("#listaProyectos").append("<option value='"+ lista[i].cproyecto +"'>"+ lista[i].nombre + "</option>");
		}

		/*eventos_lista_proyectos();
		actualizar_perido_semana();
		obtenerUsuarios();
		obtenerLogs();*/
}

function obtenerActividades()
{
		var cproyecto = $("#listaProyectos").val();		
		var url = BASE_URL + '/planificacion/obtenerActividades/' + cproyecto;
		var obj = {};
		obj.mostrarActividadesConHoras = mostrarActividadesConHoras;
		obj.fecha = $("#fecha").val();

		if(cproyecto=="0") return;

		$("#imgLoad").show();

		$.get(url , obj)
		  	.done(function( data ) {		  		
		    	//console.log("obtenerActividades", data);
		    	listaActividades = data.lista;
		    	mostrarActividades(data.lista);
		    	mostrarComboActividades(data.lista);
		    	$("#imgLoad").hide();

		    	if(data.lista.length>0)
					obtenerUsuarios();
				else
					$("#tablaUsuarios tbody").empty();
		});
}

function mostrarActividades(lista)
{
		$("#listaProyectos2 tbody").empty();
		var posPrimerElemento = -1;
		cproyectoactividades = 0;

		for (var i = 0; i < lista.length; i++) {
			var row = "";

			if(lista[i].permitirRegistro == true && posPrimerElemento == -1){
				posPrimerElemento = i;
				cproyectoactividades = lista[i].cproyectoactividades;
				row = "<tr class='proyecto-seleccionado'";
			}
			else if(lista[i].permitirRegistro == false)
				row = "<tr class='proyecto-nopermitido'";
			else
				row = "<tr class='proyecto'";
			
			row += "data-cproyectoactividades='"+ lista[i].cproyectoactividades +"'";			
			row += "data-permitirregistro='"+ lista[i].permitirRegistro +"'>";
			row += "<td>"+ lista[i].codigoactvidad + "</td>";
			row += "<td>"+lista[i].descripcionactividad+"</td>";
			$("#listaProyectos2 > tbody").append(row);
		}

		eventos_lista_proyectos();
}

function obtenerUsuarios()
{
		if($("#fecha").val() == "")
			return;

		var usuarioRelacionados = $("#chkUsuarios").prop("checked");
		$("#imgLoad").show();
		if(usuarioRelacionados)
			obtenerUsuariosdeProyecto();
		else
			obtenerUsuariosDeArea();
}

function obtenerUsuariosdeProyecto()
	{
		var url = BASE_URL + '/planificacion/obtenerUsuariosdeProyecto/diarias/' + cproyecto;
		var obj = {cproyecto: cproyecto, periodo: $("#periodo").val(), semana: $("#semana").val()}
		obj.fecha = $("#fecha").val();
		obj.cproyectoactividades = cproyectoactividades;
		obj.mostrarUsuariosConHoras = mostrarUsuariosConHoras;

		$.get(url , obj)
		  	.done(function( data ) {		  		
		    	//console.log("obtenerUsuariosdeProyecto", data);		    	
		    	$("#imgLoad").hide();
		    	listaUsuarios = data.lista;
		    	mostrarUsuarios(data.lista);
		    	//enviar_lista();
		    	//listacol=data.lista;
		    	//chart(listacol);
		});
	}

function obtenerUsuariosDeArea()
{
	var url = BASE_URL + '/planificacion/obtenerUsuariosDeArea/diarias/' + cproyecto;
	var obj = {cproyecto: cproyecto, periodo: $("#periodo").val(), semana: $("#semana").val(), mostrarUsuariosConHoras: mostrarUsuariosConHoras}
	obj.fecha = $("#fecha").val();
	obj.cproyectoactividades = cproyectoactividades;
	obj.mostrarUsuariosConHoras = mostrarUsuariosConHoras;

	$.get(url , obj)
		  	.done(function( data ) {		  		
		    	$("#imgLoad").hide();
		    	//console.log("obtenerUsuariosDeArea", data);
		    	listaUsuarios = data.lista;
		    	mostrarUsuarios(data.lista);
		    	listacol=data.lista;
		    	chart(listacol);
	});
}

function mostrarUsuarios(lista)
	{
		//console.log("mostrarUsuarios");		
		$("#tablaUsuarios tbody").empty();

		if(lista.length == 0){
			$("#divMensajeSinRegistros").show();
			//$("#divTabla").hide();
		}
		else{
			$("#divMensajeSinRegistros").hide();
			//$("#divTabla").show();
		}

		for (var i = 0; i < lista.length; i++) {
			var row = "";
			row = "<tr ";
			if(lista[i].horas.disponibles < 0)
				row += "class='danger'";
			row += "data-usuario='"+ lista[i].user +"' data-cpersona='" + lista[i].cpersona + "' >"
			row += "<td>"+ (i+1) +"</td><td>"+ lista[i].nombre + "</td>";
			row += "<td>"+ lista[i].profesion + "</td>";

			if(lista[i].usuarioProyecto==true || lista[i].usuarioProyecto==null){
				row += "<td><input type='text' class='form-control planificacion' data-dia='lun' value='" + lista[i].horas.lun + "'></td>";
				row += "<td><input type='text' class='form-control planificacion' data-dia='mar' value='" + lista[i].horas.mar + "'></td>";
				row += "<td><input type='text' class='form-control planificacion' data-dia='mie' value='" + lista[i].horas.mie + "'></td>";
				row += "<td><input type='text' class='form-control planificacion' data-dia='jue' value='" + lista[i].horas.jue + "'></td>";
				row += "<td><input type='text' class='form-control planificacion' data-dia='vie' value='" + lista[i].horas.vie + "'></td>";
				row += "<td><input type='text' class='form-control planificacion' data-dia='sab' value='" + lista[i].horas.sab + "'></td>";
				row += "<td><input type='text' class='form-control planificacion' data-dia='dom' value='" + lista[i].horas.dom + "'></td>";				
				//row += "<td><input type='text' class='form-control comentario' value='"+ lista[i].horas.comentarios +"'></td>";
			}
			else{
				for (var x = 0; x < 7; x++)
					row += "<td></td>";
			}			
			
			row += "</tr>";
			$("#tablaUsuarios > tbody").append(row); 
		}

		tabla_campos_eventos();
	}

function actualizar_perido_semana()
	{	var dia = moment($("#fecha").val() ).add(1, 'days').format('YYYY-MM-DD');
		var semana = obtenerNumeroSemana( dia );
		var periodo = $("#fecha").val();
		periodo = periodo.substring(0,4);
		$("#semana").val(semana);
		$("#periodo").val(periodo);
		$("#num_semana").text(semana);

		var url = $("#urlResumenPanoramico").val() + "/" + periodo + "/" + semana;
		$("#linkPanoramico").prop("href", url);
	}

function guardar_planificacion(obj)
{
	var url = BASE_URL + "/planificacion/guardar/horas/diarias";
	var objHoras = {proyecto: "", fecha: "", usuario: 0, horas: 0, comentario: "", cpersona: 0};
	objHoras._token = $('input[name="_token"]').first().val();
	//objHoras.comentario = $(obj).parent().parent().find(".comentario").val();
	objHoras.cproyectoactividades = cproyectoactividades;
	objHoras.horas = $(obj).val();
	objHoras.cproyecto = cproyecto;
	objHoras.usuario = $(obj).parent().parent().data("usuario");
	objHoras.cpersona = $(obj).parent().parent().data("cpersona");
	objHoras.fecha = $("#fecha").val();
	objHoras.periodo = $("#periodo").val();
	objHoras.semana = $("#semana").val();
	objHoras.dia = $(obj).data("dia");

	if(objHoras.horas == "")
		return;

	$("#imgLoad").show();

	$.post(url, objHoras)
		  	.done(function( data ) {		  		
		    	console.log("guardarHora", data);		    	
		    	$("#imgLoad").hide();
		});
}

function mostrarComboActividades(lista)
{
	//eliminar datos combo *
	$('#comboListaActividades option').remove();
	$("#comboListaActividades").append("<option value='0'>mostrar todas las actividades</option>");

	for (var i = 0; i < lista.length; i++) {
		$("#comboListaActividades").append("<option value='"+ lista[i].cproyectoactividades +"'>"+ lista[i].codigoactvidad + " " +  lista[i].descripcionactividad + "</option>");
	}

	$("#comboListaActividades").trigger("chosen:updated");
}

function filtrarActividad()
{
	var cproyectoactividades = $("#comboListaActividades").val();
	var filtro = [];

	if(cproyectoactividades == 0)
		return listaActividades;

	for (var i = 0; i < listaActividades.length; i++) {
		if(listaActividades[i].cproyectoactividades == cproyectoactividades){
			filtro.push(listaActividades[i]);
			break;
		}
	}

	return filtro;
}

	//eventos
	
	function eventos_lista_proyectos()
	{
		$("#listaProyectos2 > tbody > tr").click(function(){

			var permitirregistro = $(this).data("permitirregistro");
			if(permitirregistro == false)
				return;

			cproyectoactividades = $(this).data("cproyectoactividades");
			//retirar class, agregar class
			$(this).removeClass( "proyecto" );
			$(this).parent().find("tr").removeClass( "proyecto-seleccionado" );			
			$(this).addClass( "proyecto-seleccionado" );

			obtenerUsuarios();
			//obtenerLogs();
		});
	}

	function tabla_campos_eventos()
	{
		$(".planificacion").focusout(function() {
			guardar_planificacion(this);			
		});
		$(".comentario").focusout(function() {
			//guardar_planificacion(this);			
		});
	}