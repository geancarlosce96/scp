function chart_curva(data){
     // console.log("datazo",data);

var semana = [];
var hc = [];
/*var hd = [];
var hp = [];
var he = [];
 var ha = [];*/
 var hp = [];
 var rate = [];
 // var acumulativo = [];
 var performancehora = [];
 // var performancehorarate = [];
 var cpihora = [];
 // var cpirate = [];


for (var i = 0; i < data.length; i++) {
    semana[i] = data[i].semana;
    // horas ejecutadas
    hp[i] = data[i].suma_horas_ejecutadas;
    hc[i] = data[i].acumulado;

    //rate 

    rate[i] = data[i].rate;
    // acumulativo[i] =data[i].acumulativo;

    // avance

    performancehora[i] = data[i].performance_hora;
    // performancehorarate[i] = data[i].performance_hora_rate;

    //cpi

    cpihora[i] = data[i].cpi_hora;
    // cpirate[i] = data[i].cpi_hora_rate;

    // console.log("sem",semana);

}



// alert(hd);


Highcharts.chart('curva-s', {
    chart: {
        text: 'Combination chart'
    },
    title: {
        text: 'Curva S - tiempo'
    },
    subtitle: {
        text: 'Semana '
    },
    xAxis: [{
        title: {
            text: 'Semanas',
            className: 'highcharts-color-1',
        },
        categories: semana,
    },
        ],
    // yAxis: {
    //     title: {
    //         text: 'Horas Acumuladas'
    //     }
    // },
    yAxis: [{
        min: 0,
        title: {
            text: 'Horas'
        }
    }, 
    // {
    //     title: {
    //         text: 'CPI',
    //         className: 'highcharts-color-1',
    //     },
    //     opposite: true
    // }
    ],
    credits: {
        enabled: false,
    },
    language : 'es',
     colors: ['green','blue','red','black'],
    series: [
    /*{
        name: 'Planificadas',
        data: hp,
        type: 'column',
    },
    {
        name: 'Disponibles',
        data: hd,
        type: 'column',
    },*/
    {
        type: 'spline',
        name: 'Valor Actual (AC) - (Hoja de tiempo)',
        data: hc,
        marker: {
            lineWidth: 3,
            lineColor: 'green',
            fillColor: 'white'
        }
    }, 
    {
        type: 'spline',
        name: 'Ejecutadas',
        data: hp,
        visible: false,
        marker: {
            lineWidth: 3,
            lineColor: 'blue',
            fillColor: 'white'
     }
    },
    {
        type: 'spline',
        name: 'Valor Ganado (EV) - (Performance)',
        data: performancehora,
        marker: {
            lineWidth: 3,
            lineColor: 'red',
            fillColor: 'white'
        }
    },

    //     {
    //     type: 'spline',
    //     name: 'CPI',
    //     data: cpihora,
    //     marker: {
    //         lineWidth: 3,
    //         lineColor: 'black',
    //         fillColor: 'white'
    //     },
    //     yAxis: 1,
    // },  
    ],
});




}