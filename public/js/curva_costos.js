function chart_curvas_costos(data){
     // console.log("datazo",data);

var semana = [];
// var hc = [];
/*var hd = [];
var hp = [];
var he = [];
 var ha = [];*/
 // var hp = [];
 var rate = [];
 var acumulativo = [];
 // var performancehora = [];
 var performancehorarate = [];
 // var cpihora = [];
 var cpirate = [];


for (var i = 0; i < data.length; i++) {
    semana[i] = data[i].semana;
    // horas ejecutadas
    // hp[i] = data[i].suma_horas_ejecutadas;
    // hc[i] = data[i].acumulado;

    //rate 

    rate[i] = data[i].rate;
    acumulativo[i] =data[i].acumulativo;

    // avance

    // performancehora[i] = data[i].performance_hora;
    performancehorarate[i] = data[i].performance_hora_rate;

    //cpi

    // cpihora[i] = data[i].cpi_hora;
    cpirate[i] = data[i].cpi_hora_rate;

    // console.log("sem",semana);

}



Highcharts.chart('curvarate-s', {
    chart: {
        text: 'Combination chart'
    },
    title: {
        text: 'Curva S - costos'
    },
    subtitle: {
        text: 'Semana '
    },
    xAxis: {
        categories: semana
    },
    // yAxis: {
    //     title: {
    //         text: 'Rate Acumuladas'
    //     }
    // },
    yAxis: [{
        min: 0,
        title: {
            text: 'Costo'
        }
    }, 
    // {
    //     title: {
    //         text: 'CPI'
    //     },
    //     opposite: true
    // }
    ],
    credits: {
        enabled: false,
    },
   colors: ['green','blue','red','black'],
    series: [
    /*{
        name: 'Planificadas',
        data: hp,
        type: 'column',
    },
    {
        name: 'Disponibles',
        data: hd,
        type: 'column',
    },
    */
    {
        type: 'spline',
        name: 'Costo Real (AC) - (Hoja de tiempo)',
        data: acumulativo,
        marker: {
            lineWidth: 3,
            lineColor: 'green',
            fillColor: 'white'
        }
    }, 
    {
        type: 'spline',
        name: 'ejecutadas',
        data: rate,
        visible: false,
        marker: {
            lineWidth: 3,
            lineColor: 'blue',
            fillColor: 'white'
     }
    },
    {
        type: 'spline',
        name: 'Valor Ganado (EV) - (Performance)',
        data: performancehorarate,
        marker: {
            lineWidth: 3,
            lineColor: 'red',
            fillColor: 'white'
        }
    }, 
    //     {
    //     type: 'spline',
    //     name: 'CPI',
    //     data: cpirate,
    //     marker: {
    //         lineWidth: 3,
    //         lineColor: 'black',
    //         fillColor: 'white'
    //     },
    //     yAxis: 1,
    // },
    ],
});



}