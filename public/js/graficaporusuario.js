function grafico_porusuario_chart (data) {
    // console.log("data_user", data);


    var user = [];
    var fact = [];
    var nofac = [];
    var admin = [];
    var interna = [];
    var sem = [];



    for(var x = 0; x < data.length; x++)
    {

        user[x] = data[x].abreviatura;
        fact[x] = data[x].sumuser_fac;
        nofac[x] = data[x].sumuser_nofac;
        admin[x] = data[x].sumuser_admin;
        interna[x] = data[x].sumuser_admin_interna;
        sem[x] = data[x].semana;

    }


      Highcharts.chart('carga-semanal-user', {
          chart: {
              type: 'bar'
          },
          title: {
              //text: 'HH de '+$("#careas option:selected").text()+' semana '+semana+' por personal'
              text: 'HH de '+$("#careas option:selected").text()+' semana '+sem[0],
          },
          xAxis: {
              categories: user,

          },
          yAxis: {
              min: 0,
              title: {
                  text: 'Horas'
              },
              stackLabels: {
                  enabled: true,
                  style: {
                      // fontWeight: 'bold',
                      color: '#000000',
                      fontSize: '13px',
                      fontFamily: 'Verdana, sans-serif'
                  }
              }
          },
          colors: [
              '#0069AA','#11B15B','#CD201A','#BABDB6FF'
          ],
          credits: {
              enabled: false,
          },
          plotOptions: {
              series: {
                  stacking: 'normal'
              }
          },
          tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
          },
          series: [  {
              name: 'AND Admnistrativo',
              data: admin,
              dataLabels: {
                enabled: false,
                // rotation: -90,
                color: '#000000',
                align: 'top',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
          },
          {
              name: 'General no Facturable',
              data: nofac,
              dataLabels: {
                enabled: false,
                // rotation: -90,
                color: '#000000',
                align: 'top',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
          },
          {
              name: 'Proyecto Facturable',
              data: fact,
              dataLabels: {
                enabled: false,
                // rotation: -90,
                color: '#000000',
                align: 'top',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
          },
          {
              name: 'Proyectos Internos',
              data: interna,
              dataLabels: {
                enabled: false,
                // rotation: -90,
                color: '#000000',
                align: 'top',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
          },
          ]
      });

}