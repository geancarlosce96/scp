function graficnofacturable_chart (data) {
    // console.log("graficnofacturable", data)


    var descnofac = [];
    var sumnofac = [];
    var semana = [];


    for(var x = 0; x < data.length; x++)
    {
        descnofac[x] = data[x].descripcion;
        sumnofac[x] = data[x].sumatodos;
        semana[x] = data[x].semana;

    }





// Create the chart
    Highcharts.chart('carga-general-nofacturable', {
        chart: {
            text: 'Combination chart'
        },
        title: {
            text: 'HH de '+$("#careas option:selected").text()+' semana '+semana[0]+' General No Facturables'
           // text: 'HH de '+$("#careas option:selected").text()+' General No Facturables',
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: {
            categories: descnofac,
        },
        yAxis: [{
            min: 0,
            title: {
                  text: 'Horas'
            }
        }],
        credits: {
            enabled: false,
        },
        colors: [
            '#11B15B'
        ],
        series: [
            {
                type: 'column',
                name: 'No Facturables',
                data: sumnofac,
                dataLabels: {
                    enabled: true,
                    // rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                        }
                }
            }
        ],

    });



}