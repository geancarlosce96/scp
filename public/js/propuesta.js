
$(document).ready(function() {

		even();

		addchosen('select_cliente_pro');
		addchosen('select_minera_pro');
		addchosen('select_contacto_pro');
		addchosen('select_portafolio');
		addchosen('select_tipoproyecto');
		addchosen('aniocod');
		addchosen('sedecod');

		var table = $('#listapropuesta').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     true,
        lengthChange: false,
        buttons: [ 'excel', 'pdf', 'colvis' ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   {
            leftColumns: 1
        }
	    } ); 

	    table.buttons().container()
        .appendTo( '#listapropuesta_wrapper .col-sm-6:eq(0)' );

		$("#btn-obtenerPrepropuesta").click(function(){		
			obtenerInfo();
		});

		$.get( BASE_URL + '/obtenerTagEstructura', { })
	    .done(function( data ) {    
	    	tagsEstructura(data);
	  	});

	  	$.get( BASE_URL + '/obtenerTagServicio', { })
	    .done(function( data ) {    
	    	tagsServicio(data);
	  	});

	  	$(".btn-revision").click(function(){		
			mostrarModalRevision(this);
		});

		$("#btn-generarRevision").click(function(){		
			generarRevision();
		});
});

function even (){

	$('#cerrar_modal').css('display', 'none');

	$("#select_cliente").on("change",function(){
			
			if ($('#select_cliente').val()=="") {
				$('#select_minera').val('');
			}
			else{
				select_minera($('#select_cliente').val(),'nuevo');
			};
		})

		$("#select_minera").on("change",function(){ 
			alert($('#select_minera').val());
			if ($('#select_minera').val()=="") {
				$('#select_contacto').val('');
			}
			else{
				select_contacto($('#select_minera').val(),'nuevo');
				$('#cpersona_u').val($('#select_cliente').val());
			};
		})

		$('#fecha_preliminar').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('#fecha_visita').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('#fecha_consulta').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('#fecha_absolucion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('#fecha_presentacion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_preliminar').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_visita').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_consulta').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_absolucion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_presentacion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});

		$('#nombre_preliminar').removeAttr('readonly');

		$("#divUmineraContacto").on("hidden.bs.modal", function () {
			$('#divUminera').modal('hide');
		});
}

function select_minera(id,valor){ 
		$.ajax({
			url: 'minera/'+id,
			type: 'GET',
		})
		.done(function(data) {

			console.log("success");
			console.log(data);

			$('#select_minera').find('option').remove();
 			$('#select_contacto').find('option').remove();

			limpiar();
			var cunidadminera_temp = 0;
			for (var i = 0; i < data.length; i++) {
				$("#select_minera").append('<option value="'+data[i].cunidadminera+'">'+data[i].nombre+'</option>');
			};
				cunidadminera_temp = data[0].cunidadminera;
			if (valor=='nuevo') {
				$('#select_minera').val(cunidadminera_temp).trigger('chosen:updated');
			};
			if (valor=='edit') {
				$('#select_minera').val(objResponse.cunidadminera).trigger('chosen:updated');
			};

			select_contacto(objResponse.cunidadminera,valor);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}


function select_contacto(id,valor){ 
		alert(valor);
		$.ajax({
			url: 'contacto/'+id,
			type: 'GET',
		})
		.done(function(data) {
			console.log("success");
			$('#select_contacto').find('option').remove();
			limpiar();
			var cunidadmineracontacto_temp = 0;
			for (var i = 0; i < data.length; i++) {
				var select_contacto = data[i].nombres +' '+ data[i].apaterno +' '+ data[i].amaterno;
				$("#select_contacto").append('<option value="'+data[i].cunidadmineracontacto+'">'+select_contacto+'</option>');
				$('#cargo_pre').val(data[i].descripcion);
				$('#telefono_pre').val(data[i].telefono);
				$('#anexo_pre').val(data[i].anexo);
				$('#correo_pre').val(data[i].email);

				cunidadmineracontacto_temp = data[i].cunidadmineracontacto;
			};

			console.log(cunidadmineracontacto_temp);

			if (valor=='edit') {
				$('#select_contacto').val(objResponse.cunidadmineracontacto).trigger("chosen:updated");
			};
			if (valor=='nuevo'){
			$('#select_contacto').val(cunidadmineracontacto_temp).trigger("chosen:updated");
			};
			

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}

function obtenerInfo()
{
	console.log("obtenerInfo");
	var url = BASE_URL + '/obtenerPrepropuesta/' + $("#prepropuesta_id").val();
		$.get(url , {})
		  	.done(function( data ) {		  		
		    	console.log(obtenerInfo, data);
				mostrarInfo(data);
		});
}

function mostrarInfo(data)
{
	console.log("mostrarInfo");
	$("input[name=apellidos]").val(data.contacto.apellidos);
	$("input[name=cargo]").val(data.contacto.cargo);
	$("input[name=codigo]").val(data.codigo);
	$("input[name=correo]").val(data.contacto.correo);
	$("input[name=nombres]").val(data.contacto.nombres);
	$("input[name=numero]").val(data.contacto.numero);
	$("input[name=propuesta]").val(data.propuesta);
	$("input[name=unidad]").val(data.unidad);
	$("select[name=cliente_id]").val(data.cliente_id);
}


// autocompletado

function tagsEstructura(data){

    var availableTags = data;

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#tag_estructura" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
}

function tagsServicio(data){

    var availableTags = data;

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#tag_servicio" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
}

// revision

var cpropuesta_nueva = 0;

function mostrarModalRevision(obj)
{
	console.log("mostrarModalRevision");
	var revision_actual = $(obj).parent().parent().parent().data("revision_num");
	var propuesta_codigo = $(obj).parent().parent().parent().data("propuesta_codigo");
	var cpropuesta_revision = $(obj).parent().parent().parent().data("cpropuesta");
	$("#revision_actual").val(revision_actual);
	$("#propuesta_codigo").text(propuesta_codigo);
	$("#cpropuesta_revision").val(cpropuesta_revision);

	$("#modalRevision").modal("show");
}

function generarRevision()
{
	console.log("generarRevision");
	var cpropuesta = $("#cpropuesta_revision").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/revision/generar";

	$.get(url, {})
  		.done(function( data ) {
  			console.log(data);
  			cpropuesta_nueva = data.cpropuesta;
  			redireccionar();
  	});
}

function mostrarConfirmacion()
{
	// mostrar modal 
}

function redireccionar()
{
	window.location = BASE_URL + '/propuesta/'+ cpropuesta_nueva +'/ejecutar/equipo';
}
