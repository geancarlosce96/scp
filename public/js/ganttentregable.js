function ganttentregable(data){
//console.log("gantt", data);

if (data.length > 0) {
var entregable = [];
var nombres = [];
var codproy = [];
var id = 0;
var fecha_ini = [];
var fecha_fin = [];
var total_dias = [];
var dias = [];
var avance = [];
var cant_fech = 0;
var hoy = new Date();
    dia_ac = hoy.getDate(); 
    mes_ant = hoy.getMonth()+1;
    anio_ac= hoy.getFullYear();
    fecha_ac = moment(String(anio_ac+"/"+mes_ant+"/"+dia_ac));
    
    for (var i = 0; i < data.length; i++) {
        // cant_fech = data[i].fechas.length - 1;
        
        if (data[i].fechas == 'SinFechas' || data[i].fechas == '' || data[i].fechas == 0 || data[i].fechas == null) {
            fecha_ini[i] = String(anio_ac+","+mes_ant+","+dia_ac);
            fecha_fin[i] = String(anio_ac+","+mes_ant+","+dia_ac);
        }else { 
            var cont = data[i].fechas.length-1;
            while (cont === 0) {
                cont-=1;
            };
        cant_fech = cont -1;
        fecha_ini[i] = data[i].fechas[0].fecha_original;
        // moment(data[i].fechas[0].fecha_original, "YYYY,MM,DD").add('days', 1); 
        fecha_fin[i] = 
        // data[i].fechas[cant_fech].fecha_original;
        // moment(data[i].fechas[cant_fech].fecha_original, "YYYY,MM,DD").add('days', 1); 
        data[i].fechas[cant_fech].fecha_original == ''? data[i].fechas[cant_fech-1].fecha_original : data[i].fechas[cant_fech].fecha_original;
    };
    // console.log(fecha_ini[i]," al ",fecha_fin[i])
        nombres[i] =  data[i].descripcion;
        codproy[i] = data[i].codproy;

        total_dias[i] = moment(fecha_fin[i]).diff(moment(fecha_ini[i]),'days');
        dias[i] = fecha_ac.diff(moment(fecha_ini[i]),'days');
        avance[i] = dias[i]/total_dias[i];
        if (avance[i] > 1) {
            avance[i]=1.0;
        } else if (total_dias[i]== 0) {
            avance[i] = 0.0;
        }
        else {
            avance[i]=avance[i];
        }
        // estado[i] = data[i].estado;
        id+=1;
    // if (estado[i] != "Anulado") {
        // console.log(fecha_ini[i], data[i]);

        entregable.push({
           id : id,
           name: data[i].descripcion,
           y: i,
           start : Date.UTC(fecha_ini[i].substr(0,4), fecha_ini[i].substr(5,2)-1, fecha_ini[i].substr(8,2)),
           end : Date.UTC(fecha_fin[i].substr(0,4), fecha_fin[i].substr(5,2)-1, fecha_fin[i].substr(8,2)),
           fechastart: data[i].fechas[0].fecha_original,
           fechastartdesc: data[i].fechas[0].fechaDesc,
           fechaend: data[i].fechas[cant_fech].fecha_original==''? '':data[i].fechas[cant_fech].fecha_original,
           fechaenddesc: data[i].fechas[cant_fech].fecha_original == '' ? data[i].fechas[cant_fech-1].fechaDesc : data[i].fechas[cant_fech].fechaDesc,
           assignee : data[i].elaborador,
           imagen : data[i].imagen,
           completed: avance[i],
       });

    // }
};

Highcharts.ganttChart('gantt-entregable', {

    title: {
        text: 'Roadmap de entregables del proyecto ' +codproy[0],
    },
    xAxis: {
        tickPixelInterval: 60,
        minPadding: 1,
        maxPadding: 1,
        currentDateIndicator: {
            width: 1,
            dashStyle: 'dot',
            color: 'red',
            label: {
                format: '%d-%m-%y'
            }
        },
    },
    credits: {
        enabled: false
    },
    navigator: {
        enabled: true,
        liveRedraw: true,
        series: {
          type: 'gantt',
          pointPlacement: 0.5,
          pointPadding: 0.25
        },
        yAxis: {
          min: 0,
          max: 5,
          reversed: true,
          categories: []
            }
    },
    yAxis: {
        type: 'category',
        grid: {
            enabled: true,
            borderColor: 'rgba(0,0,0,0.3)',
            borderWidth: 1,
            columns: [
                {
                    title: {
                        text: '<b>Id</b>',
                        useHTML: true,
                    },
                    labels: {
                        format: '<b>{point.id}</b>'
                    }
                },
                {
                title: {
                    text: '<b>Entregables</b>',
                    useHTML: true,
                },
                labels: {
                    format: '{point.name}',
                    align: 'left',
                    width: 100,
                }
            }, {
                title: {
                    text: '<b>Responsable</b>',
                    useHTML: true,
                },
                labels: {
                    format: '{point.assignee}'
                }
            }, 
            {
                title: {
                    text: '<b>Días</b>',
                    useHTML: true,
                },
                labels: {
                    formatter: function () {
                        var point = this.point,
                            days = (1000 * 60 * 60 * 24),
                            number = (point.x2 - point.x) / days;
                            if (isNaN(number)) {
                                var diferencia = "-";
                            } else {
                                var diferencia = Math.round(number * 100) / 100;
                            }
                            return diferencia;
                    }
                }
            },
            {
                title: {
                    text: '<b>Rango de fechas</b>',
                    useHTML: true,
                },
                labels: {
                    format: '{point.fechastartdesc}-{point.fechaenddesc}'
                }
            },
            {
                labels: {
                    format: '{point.start:%d-%m-%y}'
                },
                title: {
                    text: '<b>Primera fecha</b>',
                    useHTML:true,
                }
            }, {
                title: {
                    text: '<b>Última fecha</b>',
                    useHTML:true,
                },
                offset: 0,
                labels: {
                    format: '{point.end:%d-%m-%y}'
                }
            }
            ]
        }
    },
    scrollbar: {
    enabled: true
    },
    rangeSelector: {
    enabled: true,
    selected: 5
    },
    tooltip: {
        xDateFormat: '%d-%m-%y'
    },

    series: [{
        name: codproy[0],
        data: entregable,
        dataLabels: [{
            enabled: true,
            format: '<div style="width: 20px; height: 20px; overflow: hidden; border-radius: 50%; margin-left: 0">' + '<img src="'+BASE_URL+'/images/{point.imagen}.jpg" ' + 'style="width: 30px; margin-left: -5px; margin-top: -2px"></div>',
            useHTML: true,
            align: 'left'
        }, {
            enabled: true,
            format: '<i class="fa fa-{point.fontSymbol}" style="font-size: 1.5em"></i>',
            useHTML: true,
            align: 'right'
            }]
        }],

    exporting: {
        sourceWidth: 1000000
    }
});

$('#gantt-entregable').css({
    'margin': '1em auto',
});

$('.box-body').css({
   'padding' : '0px', 
});

} else {
    $('#gantt-entregable').text("No hay entregables para mostrar");
    $('#gantt-entregable').css({
        'text-align': 'center',
        'font-size': '15px',
        'color': '#0069aa',
    });
}
}

