var periodo = "";
var semana = "";
var arrayLideresOcultos = []; //elemento {nombreLider}

$(document).ready(function(){
	
	goheadfixed('table.fixed');
	periodo = $("#periodo").val();
	semana = $("#semana").val();

	$(".planificacion_horas").change(function(){
		actualizar_horas(this);
	});

	$(".btn-ocultar").click(function(){
		ocultar_lider(this);
	});

	$(".btn-agregar-persona").click(function(){		
		$("#modalAgregarPersona").modal("show");		
		setTimeout(function(){ $(".chosen-select").chosen({no_results_text: "No se ha encontrado un código o proyecto."}); }, 1000);
	});

	$("#btn-registrar-persona").click(function(){
		registrar_persona();
	});

});


function actualizar_horas(objE)
{
	var codigoproyecto = $(objE).parent().parent().data("codigoproyecto");
	var cpersona = $(objE).data("cpersona");
	var _token = $("input[name=_token]").val();
	var horas = $(objE).val();
	var obj = {periodo: periodo, semana: semana, codigoproyecto: codigoproyecto, cpersona: cpersona, _token: _token, horas: horas};

	$.post( BASE_URL + "/planificacion/resumen/panoramico/guardar", obj)
		.done(function( data ) {		
		$("#msjActualizacion").text(data.msj);
	});	
}

function ocultar_lider(obj)
{
	var liderNombre = $(obj).data("liderocultar");
	$('tr[data-lider="' + liderNombre + '"]').hide();
	var elemento = '<span class="btn-mostrar-lider" title="Volver a mostrar al lider" data-lider="'+ liderNombre +'">' + liderNombre + "</span> ";
	$("#listaLideresOcultos").append(elemento);
	$("#spanCantidadLideresOcultos").text($("#listaLideresOcultos > span").length);
	agregar_evento();
}

function registrar_persona()
{
	var objRequest = {};
	objRequest.cpersona = $("#cpersona").val();
	objRequest.cproyecto = $("#cproyecto").val();
	objRequest.ccategoriaprofesional = $("#ccategoriaprofesional").val();
	objRequest._token = $("input[name=_token]").val();

	$.post( BASE_URL + "/planificacion/resumen/panoramico/agregarPersona", objRequest)
		.done(function( data ) {
		$("#modalAgregarPersona").modal("hide");
		alert("Se ha registrado a la persona");
		window.location.reload();
	});
	
}

function mostrar_lider(obj)
{
	var liderNombre = $(obj).data("lider");	
	$('tr[data-lider="' + liderNombre + '"]').show();
	$(obj).remove();
	$("#spanCantidadLideresOcultos").text($("#listaLideresOcultos > span").length);	
}

function agregar_evento(){
	$(".btn-mostrar-lider:last").click(function(){
		mostrar_lider(this);
	});
}

