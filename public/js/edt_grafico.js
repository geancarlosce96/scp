var cproyecto = 0;
var list = [];
var posElementoSeleccionado = -1;

$(document).ready(function(){
	
	// $("#btn-generaredt"cproyectp).click(function(){
	// 	generarEDT();
	// });

	// $('#btn-generaredt').on('hidden.bs.modal', function () { alert("hola");
           // generarEDT();
 //        });

	$("#btn-agregarHijo").click(function(){
		agregarHijo();
	});

	$("#btn-retirarElemento").click(function(){
		retirarElemento();
	});

	$("#btn-guardarHijo").click(function(){
		guardarHijo();
	});

});



function verEDTgrafico(cproyecto) {
	// alert("Hola mundo");
	// var cproyecto = $("#cproyecto").val();
	console.log(cproyecto);
	generarEDT(cproyecto);
	$('#modalEDTgrafico').modal('show');
}

function generarEDT(cproyecto)
{
	console.log("generarEDT",cproyecto);
	$("#opciones").hide();

	var url = BASE_URL + '/edt/obtener/' + cproyecto;
		$.get(url , {})
		  	.done(function( data ) {
		  		// verEDTgrafico();
		  		console.log("data", data);
		  		list = data;
		  		drawChart();
		});	
}

google.charts.load('current', {packages:["orgchart"]});

function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');
        var rows = [];
        for (var i = 0; i < list.length; i++) {
        	var element = [];
        	var name = {};
        	name.v = ""+list[i].cproyectoedt+"";


        	// if (list[i].nivel==0) { color == "rgb(0,176,80)"; letra="white";};
        	// if (list[i].nivel==1) { color == "rgb(0,105,170)"; letra="white";};
        	// if (list[i].nivel==2) { color == "rgb(128,128,128)"; letra="black";};
    		// if (list[i].nivel==3) { color == "rgb(166,166,166)"; letra="black";};

    		var codigo=' - ';

    		if (list[i].codigo!=null) {
    			codigo=list[i].codigo;
    		}
    		
        	name.f = codigo + '<div>' +list[i].descripcion +'</div>';
        	element.push(name);

        	if(list[i].cproyectoedt_parent == null)
        		element.push("");
        	else
        		element.push(""+list[i].cproyectoedt_parent+"");
        	element.push(list[i].descripcion);

        	rows.push(element);
        

        }
        console.log(rows);
        data.addRows(rows);

        var chart = new google.visualization.OrgChart(document.getElementById('chart_div'));        

        google.visualization.events.addListener(chart, 'select', function showAdd(){

        	if(chart.getSelection().length > 0)
        		posElementoSeleccionado = chart.getSelection()[0].row;        	
        	mostrarOpciones();

        });

        chart.draw(data, {allowHtml:true});

	        $('.google-visualization-orgchart-node').css({
	        	"background": "rgb(0,105,170)",
	        	"color": "white",
	        	"border-width":"0px",
	        	"border-style":"solid",
	        	"font-size":"12px"});
}

function mostrarOpciones()
{ 
	var cantidadSeleccionados = $(".google-visualization-orgchart-nodesel").length;
	
	if(cantidadSeleccionados == 0){
		$("#opciones").hide();
		return;
	}

	//mostrar en posicion cercana al elemento seleccionado
	var objPosicion = $(".google-visualization-orgchart-nodesel").first().position();
	var objPosicionChart = $("#chart_div").parent().position();
	$("#opciones").css("left", (objPosicion.left + objPosicionChart.left + 0) + "px");
	$("#opciones").css("top", (objPosicion.top + objPosicionChart.top) + "px");
	$("#opciones").show();
}

function retirarElemento(cproyecto)
{

	var objRequest = {};
	objRequest.cproyectoedt = list[posElementoSeleccionado].cproyectoedt;
	objRequest._token = $('input[name="_token"]').first().val();
	
	var url = BASE_URL + '/edt/eliminarElemento';
	$.post(url, objRequest)
		.done(function( data ) {
			alert("Elemento retirado");
			generarEDT(cproyecto);
			verDetalleEDT();
	});	
}

function agregarHijo(cproyecto)
{

	var codigo=' -';
	if (list[posElementoSeleccionado].codigo!=null) {
		codigo=list[posElementoSeleccionado].codigo;
	}

	$("#codigo").val("");
	$("#descripcion").val("");
	$("#padre").val(codigo + " " + list[posElementoSeleccionado].descripcion);
	$("#modalAgregar").modal("show");
}

function guardarHijo(cproyecto)
{
	var objRequest = {};
	objRequest.cproyecto = cproyecto;
	objRequest.codigo = $("#codigo").val();
	objRequest.descripcion = $("#descripcion").val();
  	objRequest.cproyectoedt_parent = list[posElementoSeleccionado].cproyectoedt;
  	objRequest._token = $('input[name="_token"]').first().val();
	
	var url = BASE_URL + '/edt/guardarHijo';
	$.post(url, objRequest)
		.done(function( data ) {
			console.log("data", data);
			$("#modalAgregar").modal("hide");
			alert("Elemento hijo agregado");
			generarEDT(cproyecto);
			verDetalleEDT();
	});	
}

