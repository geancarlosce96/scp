

/*********  Tabla Entregables  ***********/

tabla_eventos_EntregablesTodos();
/*
$(".descripcion_ent").on("click",function() {

  tabla_eventos_EntregablesTodos();
  //tabla_eventos();
});

$(".observacion").on("click",function() {

  tabla_eventos_EntregablesTodos();
});

$(".responsable").on("click",function() {

  tabla_eventos_EntregablesTodos();});

$(".tipo").on("click",function() {

  tabla_eventos_EntregablesTodos();
  //actualizar_entregable($(this),'tipo');

});

$(".fechasTodEnt").on("click",function() {

  tabla_eventos_EntregablesTodos();
});


$(".descripcionEnt").focusout(function() {
  //console.log($(this));

  tabla_eventos_EntregablesTodos();
});

$(".observacionEnt").focusout(function() {
  //console.log($(this));

  tabla_eventos_EntregablesTodos();
});

$(".responsableEnt").focusout(function() {

  tabla_eventos_EntregablesTodos();
});


$(".tipoEnt").focusout(function() {

  tabla_eventos_EntregablesTodos();
});

$('.addruteo').click(function() {
    tabla_eventos_EntregablesTodos();
  });

// $('.fechaEnt').datepicker({
//     format: "dd-mm-yy",
//     language: "es",
//     autoclose: true,
//     firstDay: 1,
//     calendarWeeks:true,
//     startDate: "<?php echo date('d/m/Y'); ?>",
//     todayHighlight: true,

// }).on("input changeDate", function (e) {
//     tabla_eventos_EntregablesTodos();
// });

$("a.verActivProy").on("click",function() {

    tabla_eventos_EntregablesTodos();

});

$("a.anularEnt").on("click",function() {

    tabla_eventos_EntregablesTodos();

});

$("a.activarEnt").on("click",function() {

    tabla_eventos_EntregablesTodos();

});

$("a.fechasEnt").on("click",function() {

    tabla_eventos_EntregablesTodos();

});

$(".faseEnt").on("click",function() {
    tabla_eventos_EntregablesTodos();
});

$(".faseEntregable").on("click",function() {
      tabla_eventos_EntregablesTodos();
  }); 
*/

/*function enviarFecha(obj){

  var validacionFecha = validarFechasTabla($(obj).attr("id"),$(obj).val()); 
    var idFec=$(obj).attr("id");
    var fechaspan=$("#sp_"+idFec).text();

    if (validacionFecha!='ok') {
      alert('Ingrese una fecha posterior');


      console.log(idFec);
      console.log(fechaspan);
      console.log($("#"+idFec).val("'"+fechaspan+"'"));
      
     
      //ocultar_input(idFec,fechaspan.toString());  
      $("#"+idFec).val('123');

      return;

    }

    else{
      actualizar_entregable(obj,'fecha');
    }

}*/

function actualizar_entregable(obj,act){



  var objEnt={cproyectoentregables: "", observacion: "", descripcion: "", responsable: "", actualizar:"", tipo: "", edt: "" ,tipoFecha: "",fecha: "", fase: "" };
  objEnt.cproyectoentregables=$(obj).parent().parent().data("ident");
  objEnt.edt=$(obj).parent().parent().data("edt");
  if(act=='observacion'){
    objEnt.observacion=$(obj).val();
  }
  if(act=='descripcion'){
    objEnt.descripcion=$(obj).val();      
  }
  if(act=='responsable'){
    objEnt.responsable=$(obj).val();      
  }
  if(act=='tipo'){
    objEnt.tipo=$(obj).val();      
  }

  if(act=='fecha'){
    var validacionFecha = validarFechasTabla($(obj).attr("id"),$(obj).val()); 
    var idFec=$(obj).attr("id");
    var fechaspan=$("#sp_"+idFec).text();

    if (validacionFecha!='ok') {

      ocultar_input(idFec,fechaspan.toString());  
      alert('Ingrese una fecha posterior');
      verLEporEDT($('#cpryedt').val());
     // $("#"+idFec).val('123');

      return;

    }

    objEnt.fecha=$(obj).val(); 
    objEnt.tipoFecha=$(obj).parent().data("ctipo");   
  }

  if(act=='fase'){
    objEnt.cproyectoentregables=$(obj).parent().parent().parent().data("ident");

    objEnt.edt=$(obj).parent().parent().parent().data("edt");
    objEnt.fase=$(obj).val();      
  }

  objEnt.actualizar=act;


  

  var idInput=$(obj).attr("id");

  $.ajax({
    url: 'actualizarEntregable',
    type:'GET',
    data: objEnt,

    success : function (data){
      
      $('#div_carga').hide(); 
      //$("#tableEntdivLEntregable").html(data);
      //$("a.ocultarAnexos").hide();
      //console.log(idInput);
      if(act=='responsable' || act=='fase'){
        ocultar_input(idInput,$("#"+idInput+" option:selected").text()); 
      }

      else{
        ocultar_input(idInput,$(obj).val());  

      }
      if(act=='fecha'){
        agregarClaseFecha(idInput);
      }

      //ocultar_input_ent(idInput,$(obj).val());
          

    },
  }); 

}


function validarFechasTabla(id,fechaIng){

  var tipoFecha=id.split("_");
  var tipoActual=parseInt(tipoFecha[2]);
  var tipoanterior=(tipoActual-1);
  var idFechaAnterior=tipoFecha[0]+'_'+tipoFecha[1]+'_'+tipoanterior;
  var fechaAnterior = $('#'+idFechaAnterior).val();
  

  if (typeof fechaAnterior!='undefined') {

    fechaIngresada=fechaIng;
    fechaIngresada=fechaIngresada.substr(3,2)+'/'+fechaIngresada.substr(0,2)+'/'+fechaIngresada.substr(6,2);
    fechaIngresada=new Date(fechaIngresada);

    fechaAnterior=fechaAnterior.substr(3,2)+'/'+fechaAnterior.substr(0,2)+'/'+fechaAnterior.substr(6,2);
    fechaAnterior=new Date(fechaAnterior);

    if (fechaIngresada<fechaAnterior) {
    
      return 'menor';
    }

    else{
     
      return 'ok';

    }
  }

  else{
    return 'ok';

  }


}

function agregarClaseFecha(id){

  var tipoFecha=id.split("_");
  var tipoActual=parseInt(tipoFecha[2]);
  var tiposiguiente=(tipoActual+1);
  var idFechaSiguiente='td_fecha_'+tipoFecha[1]+'_'+tiposiguiente;

  $('#'+idFechaSiguiente).addClass('fechasTodEnt');

  /*$(".fechasTodEnt").on("click",function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();

    mostrar_input_ent(id,desc);

  });*/

  //tabla_eventos_EntregablesTodos();
}

function ocultar_input_ent(idInput,valor){

  $("#"+idInput).css("display", "none"); 
  $("#sp_"+idInput).html(valor);
  $("#sp_"+idInput).css("display", "block"); 

}

function mostrar_input_ent(idInput){
	 
  $("#sp_"+idInput).css("display", "none"); 
  //$("#"+idInput).val(valor);
  $("#"+idInput).css("display", "block");
  $("#"+idInput).focus();

}


function setCheck(chk){
  console.log(chk);
  //alert($(":checkbox").length);
  $("input[name='checkbox_ent[]']").each(function(){
      /*if (this.checked) {
          
      }*/
      //alert(this.checked);
      this.checked = chk.checked;
    }); 
  
  return;
}  





$("a.mostrarAnexos").on("click",function(){

  var id=$(this).parent().parent().data("ident");
  obtenerAnexos(id);

});


$("a.ocultarAnexos").on("click",function(){
  var id=$(this).parent().parent().data("ident");
  var anexos=$(this).parent().parent().data("anexos");
  ocultarAnexos(id,anexos);

});

function obtenerAnexos(idEnt){
  
  $.ajax({
          url: 'obtenerAnexos/'+idEnt+'/'+$('#cpryedt').val()+'/EDT',
          type:'get',
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){
            $('#div_carga').hide(); 
            mostrarAnexos(data,idEnt);

          },
  });  

}


function mostrarAnexos(obj,id){

  console.log('anexos tabla_lista_entregbable');
  
  $("#btnmostrar_"+id).hide(); 
  $("#btnocultar_"+id).show(); 
  //var indice=$("#fila_"+id).index(); 
  console.log(obj);
  //$('#tableEntregablesTodosPry').dataTable().fnDestroy();
  for (var i = 0; i < obj.length; i++) {
    var row = "";
    row = "<tr id='fila_"+id+"_"+i+"'"; 
    row += "data-edt='"+obj[i].cproyectoedt+"' data-ident='"+obj[i].cproyectoentregables+"' data-anexos='"+obj[i].anexos+"'>";  
    row += "<td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<div class='input-group'><div class='pull-left'> "
    if(obj[i].cantidadFechas<=4){
      row += "<a href='#' title='¡Las fechas no están completas!' style='color: red'><i class='fa fa-exclamation' aria-hidden='true'></i>&nbsp;<i class='fa fa-calendar-times-o' aria-hidden='true'></i></a>&nbsp;";
    }
    else{
      row += "<a href='#' title='¡Fechas completas!' style='color: gray'>&nbsp;&nbsp;&nbsp;<i class='fa  fa-calendar-check-o' aria-hidden='true'></i></a>&nbsp;";
    }
    row += "</div><div class='pull-right'><label style='color: black;font-size: 12px;'><strong>"+/*'.'+obj[i].item+*/"</strong></label>"
    row += "</div></div></td>";  
    row += "<td>"+obj[i].estado+"</td>";  

     row += "<td class='faseEnt'><span id='sp_fase_"+obj[i].cproyectoentregables+"'>"+obj[i].fase+"</span>";
     row += " <div id='divfase_"+obj[i].cproyectoentregables+"'></div></td>";

    row += "<td>"+obj[i].tipoentregable+"</td>"; 
    row += "<td>"+obj[i].tipoproyentregable+"</td>";

    row += "<td class='codAnexo'>"+'<input id="codAn_'+obj[i].cproyectoentregables+'"class="form-control" type="text" name="" style="display:none">';
    row += "<span id='sp_codAn_"+obj[i].cproyectoentregables+"' style='display:block'>"+obj[i].codigo+"</span></td>";

    var revision=obj[i].des_revision;
    if (obj[i].des_revision==null) {
      revision = '';
    }

    row += "<td><center>"+revision+"</center></td>";
    row += "<td>"+obj[i].des_dis+"</td>";
    row += "<td class='descripcion_ent'>"+'<input id="tdesc_'+obj[i].cproyectoentregables+'"class="form-control descripcionEnt" type="text" name="" value="'+obj[i].descripcion+'" style="display:none">';
    row += "<span id='sp_tdesc_"+obj[i].cproyectoentregables+"'>"+obj[i].descripcion+"</span></td>";

/////////////////////////// INICIO FECHAS ///////////////////////////
    if (obj[i].fechas!='SinFechas') {

      var fechas = obj[i].fechas;
      var contFecVacia = 0;

      for (var f = 0; f < fechas.length; f++) {

        var fechaAct=fechas[f].fecha;
        var claseMostrarInput='fechasTodEnt';
       
        if (fechaAct.length<1) {
          contFecVacia++;

          if (contFecVacia==1) {
            console.log(fechas[f].fecha);
            claseMostrarInput='fechasTodEnt';
          }

          if (contFecVacia>1) {
            claseMostrarInput='';
          }
        }

        row += "<td id='td_fecha_"+obj[i].cproyectoentregables+"_"+fechas[f].ctipo+"' class='"+claseMostrarInput+"' data-ctipo='"+fechas[f].ctipo+"'>";
        row += "<span id='sp_tfec_"+obj[i].cproyectoentregables+"_"+fechas[f].ctipo+"'>"+fechas[f].fecha+"</span>";
        row += "<input id='tfec_"+obj[i].cproyectoentregables+"_"+fechas[f].ctipo+"' class='input-sm fechaEnt' type='text'  name='' style='display:none' value='"+fechas[f].fecha+"'></td>";
      }
    }
    else{
      for (var c = 0; c < fechasCab.length; c++) {

        var claseMostrarInput='';
        if (fechasCab[0]==fechasCab[c]) {

          claseMostrarInput='fechasTodEnt';

        }
        row += "<td id='td_fecha_"+obj[i].cproyectoentregables+"_"+fechasCab[c].ctipofechasentregable+"' class='"+claseMostrarInput+"' data-ctipo='"+fechasCab[c].ctipofechasentregable+"'>";
        row += "<span id='sp_tfec_"+obj[i].cproyectoentregables+"_"+fechasCab[c].ctipofechasentregable+"'></span>";
        row += "<input id='tfec_"+obj[i].cproyectoentregables+"_"+fechasCab[c].ctipofechasentregable+"' class='input-sm fechaEnt' type='text'  name='' style='display:none' value=''></td>";
      }
    }
/////////////////////////// FIN FECHAS ///////////////////////////
    row += "<td class='observacion'>"+'<input id="tobs_'+obj[i].cproyectoentregables+'"class="input-sm observacionEnt" type="text" name="" value="'+obj[i].observacion+'" style="display:none" >';
    row += "<span id='sp_tobs_"+obj[i].cproyectoentregables+"'</span>"+obj[i].observacion+"</td>";

    var responsable=obj[i].des_revision;
    if (obj[i].responsable==null) {
      responsable = '';
    }



    row += "<td class='fase'><span id='sp_respAn_"+obj[i].cproyectoentregables+"'>"+responsable+"</span>";
    row += " <div id='divrespAn_"+obj[i].cproyectoentregables+"'></div></td>";
    row += "<td>"+obj[i].elaborador+"</td>";
    row += "<td style='text-decoration:line-through; color: white; font-size: 12px'><center>";
      // row += "<a class='verActivProy' href='#' title='Asignar actividades' ><i class='fa fa-list-ul' aria-hidden='true'></i></a> &nbsp;";
      if (obj[i].activo=='1') {
        row += "<a class='anularEnt' href='#' title='Anular' style='color: red'><i class='fa fa-minus-square' aria-hidden='true' onclick='activarEnt("+obj[i].cproyectoentregables+",'1')'></i></a> &nbsp;";
      }
      else{
        row += "<a href='#' title='Activar' style='color: green'><i class='fa fa-check-square' aria-hidden='true' onclick='activarEnt("+obj[i].cproyectoentregables+",'0')'></i></a> &nbsp;";
      }
      row += "<a class='fechasEnt' href='#' title='Agregar fechas' ><i class='fa fa-calendar' aria-hidden='true'></i></a> &nbsp;";
      row += "<a class='addruteo' title='Agregar entregable para revisar o emitir'><i class='fa fa-cart-plus'></i></a>";
    row += "</center></td>";
    row += "<td><center><input type='checkbox' name='checkbox_ent[]' id='checkbox_ent' value='"+obj[i].cproyectoentregables+"'></center></td>";
    row += "</tr>";
    $("#fila_"+id).after(row);

    //$('#tableEntregablesTodosPry').dataTable().fnAddData( $(row)[0]);
    obtenerFasesProy(obj[i].cproyectoentregables,obj[i].cfaseproyecto);
  }
   
    //initDataTable();
}

function ocultarAnexos(id,anexos){

  $("#btnmostrar_"+id).show(); 
  $("#btnocultar_"+id).hide();  

  for (var i = 0; i < anexos; i++) {

    $("#fila_"+id+"_"+i).remove();

  }
}
  


