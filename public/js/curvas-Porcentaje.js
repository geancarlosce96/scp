function chart_curva_porcentaje(data){
     console.log("eeee",data);

    var semana = [];
    var resumenPorcentajeFinal = [];



    for (var i = 0; i < data.length; i++) {
        semana[i] = data[i].semana;
        resumenPorcentajeFinal[i] =data[i].resumenPorcentajeFinal;
    }



    Highcharts.chart('curva-s-porcentaje', {
        chart: {
            text: 'Combination chart'
        },
        title: {
            text: 'Curva S - costos'
        },
        subtitle: {
            text: 'Semana '
        },
        xAxis: {
            categories: semana
        },
        // yAxis: {
        //     title: {
        //         text: 'Rate Acumuladas'
        //     }
        // },
        yAxis: [{
            min: 0,
            title: {
                text: 'Porcentaje'
            }
        },
            // {
            //     title: {
            //         text: 'CPI'
            //     },
            //     opposite: true
            // }
        ],
        credits: {
            enabled: false,
        },
        language : 'es',
        colors: ['#CD201A'],
        series: [
            /*{
                name: 'Planificadas',
                data: hp,
                type: 'column',
            },
            {
                name: 'Disponibles',
                data: hd,
                type: 'column',
            },
            */
            {
                type: 'spline',
                name: '% Ejecutado(%EV)',
                data: resumenPorcentajeFinal,
                marker: {
                    lineWidth: 3,
                    lineColor: '#CD201A',
                    fillColor: 'white'
                }
            },

        ],
    });



}