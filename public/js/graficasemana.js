function graficasemana (data) {
    // console.log("datazograficasemana", data);

    Highcharts.chart('carga_semanal', {
        chart: {
            text: 'Combination chart'
        },
        title: {
            text: 'HH de '+$("#careas option:selected").text()+' semana '+data[0]
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: [{
        title: {
            text: 'Semanas',
            className: 'highcharts-color-1',
        },
        categories: [data[0]],
        }],
        yAxis: {
        title: {
            text: 'Horas'
            }
        },
        credits: {
            enabled: false,
        },
        colors: [
            '#CD201A','#11B15B','#0069AA','#BABDB6FF'
        ],
        series: [
            {
                type: 'column',
                name: 'Proyecto Facturable',
                data: [data[1]],
                tooltip: {
                    headerFormat: '<em>Semana : '+data[0]+'</em><br/>'
                },
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            {
                type: 'column',
                name: 'General No Facturable',
                data: [data[2]],
                tooltip: {
                    headerFormat: '<em>Semana : '+data[0]+'</em><br/>'
                },
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
            } ,
            {
                type: 'column',
                name: 'AND Administrativo',
                data: [data[3]],
                tooltip: {
                    headerFormat: '<em>Semana : '+data[0]+'</em><br/>'
                },
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            {
                type: 'column',
                name: 'Proyectos Internos',
                data: [data[4]],
                tooltip: {
                    headerFormat: '<em>Semana : '+data[0]+'</em><br/>'
                },
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }
            }
        ],
    });

}