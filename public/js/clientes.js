        function Valida(){

            var pais = $("#pais").val();

            var result =true;

            if (pais=='PER') {
            
              if($('#dpto').val()=='' || $('#dpto').val()==null){
                  alert('Especifique el Departamento de residencia');
                  result=false;
              }         
              if($('#prov').val()=='' || $('#prov').val()==null){
                  alert('Especifique la provincia de residencia');
                  result=false;
              }                 
              if($('#dis').val()=='' || $('#dis').val()==null){
                  alert('Especifique el distrito de residencia');
                  result=false;
              }

            }
           
            return result;
        }

        function ValidaUM(){

            var pais = $("#pais_um").val();

            var result =true;

            if (pais=='PER') {
            
              if($('#dpto_um').val()=='' || $('#dpto_um').val()==null){
                  alert('Especifique el Departamento de residencia');
                  result=false;
              }         
              if($('#prov_um').val()=='' || $('#prov_um').val()==null){
                  alert('Especifique la provincia de residencia');
                  result=false;
              }                 
              if($('#dis_um').val()=='' || $('#dis_um').val()==null){
                  alert('Especifique el distrito de residencia');
                  result=false;
              }

            }
           
            return result;
        }
        
        $('#frmcliente').on('submit',function(e){
            if(!Valida()){
                return false;
            }
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            //$('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            
            $('select').parent().parent().removeClass('has-error');

            $.ajax({

                type:"POST",
                url:'grabarCliente',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
                    
            
                },
                success: function(data){
                     console.log(data);
                     $('#div_carga').hide(); 
                     if ($('#vista').val()=='invitacion') {
                      $('#cpersona_u').val(data);
                      alert('Cliente registrado, registre UM');
                      selectcliente(data);
                      $('#divUminera').modal('show');
                      $('#modal').modal('hide');
                      return;
                     }else{
                     $(".alert-success").prop("hidden", false);
                     $("#resultado").html(data);
                     $("#div_msg").show();
                     $('.modal-backdrop').remove()
                     }

                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });

       /* $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });*/

        $('#pais').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDpto/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    
                    $('#dpto').find('option').remove();
                    $('#prov').find('option').remove();
                    $('#dis').find('option').remove();
                    $('#dpto').append($('<option></option>').attr('value','').text(''));
                    
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dpto').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){

                }

            });
            
        });

        $('#dpto').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listProv/'+$('#pais').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);

                    $('#prov').find('option').remove();
                    $('#dis').find('option').remove();
                    $('#prov').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#prov').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){

                }

            });
            
        });

        $('#prov').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDis/'+$('#pais').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    $('#dis').find('option').remove();
                    $('#dis').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dis').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){

                }

            });
            
        })                
        
        /* ubigeo unidad minera */
        $('#pais_um').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDpto/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    
                    $('#dpto_um').find('option').remove();
                    $('#prov_um').find('option').remove();
                    $('#dis_um').find('option').remove();
                    $('#dpto_um').append($('<option></option>').attr('value','').text(''));
                    
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dpto_um').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){

                }

            });
            
        });

        $('#dpto_um').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listProv/'+$('#pais_um').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);

                    $('#prov_um').find('option').remove();
                    $('#dis_um').find('option').remove();
                    $('#prov_um').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#prov_um').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){

                }

            });
            
        });

        $('#prov_um').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDis/'+$('#pais_um').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    $('#dis_um').find('option').remove();
                    $('#dis_um').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dis_um').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){

                }

            });
            
        })   
        /* fin de ubogeo de Unidad Minera */

        /* Agregar Unidad Minera*/
        $("#btnAddUm").click(function(e){
           if(!ValidaUM()){
                return false;
            }

          $.ajax({
            url: 'addUmineraCliente',
            type:'POST',
            data: $('#frmuminera').serialize() ,
            beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divMinera').html(data);
              limpiarUminera();

              

            },
          });
        });
        /* Fin de agregar unidad Minera*/

        function limpiarUminera(){

          $("#cuminera_u").val('');
          $("#codigo_um").val('');
          $("#codigo_um").removeAttr("readonly");
          $("#nombre_um").val('');
          $("#umineraestado").val('');
          $("#telefono_um").val('');
          $("#direccion_um").val('');
          $("#pais_um").val('');
          $('#dpto_um').val(0);
          $("#prov_um").val(0);
          $("#dis_um").val(0);
      
        }

        
        function eliminarUminera(id){

          $.ajax({

            url: 'getRespuestaEliminarUminera/'+ id,
            type:'GET',
            beforeSend: function () {                
               
            },              
            success : function (data){
              if(data=='1'){
                alert('Esta unidad minera no se puede eliminar');
              }
              else{
                $('#divMinera').html(data);
              }
              
              

            },

           
          });          

        }

        $('#ModalUminera').on('click',function(){
            $('#divUminera').modal('show');
            $('#modal').modal('hide');
        })



        function contactoUminera(id){

          limpiarUmineraContacto();
         
          $.ajax({
            url: 'verUmineraContacto/'+id,
            type:'GET',
              beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){              
              $('#divUmineraContacto').modal({backdrop: "static"});
              $('#divMineraCon').html(data);  
              $('#cuminera_con').val(id);     

             //          

              },
          });

          $.ajax({
            url: 'editUmineraCliente/'+id,
            type:'GET',
              beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){

              var str = JSON.stringify(data);

              var pushedData = jQuery.parseJSON(str);
              //$('#div_carga').hide(); 
              console.log(pushedData['cunidadminera']);

              var nombre = pushedData['nombre'];  

              $('#nombre_umc').val(nombre);
              $("#nombre_umc").attr("readonly","readonly"); 
             
              
            },
          });
        
           
        }

         /* Agregar Unidad Minera de Contacto*/

        
         $("#btnAddContactos").click(function(e){

          $.ajax({
            url: 'AddContactosCliente',
            type:'POST',
            data: $('#frmcuminera').serialize() ,
              beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divMineraCon').html(data);

              limpiarUmineraContacto();
            },
          });
        })

        //Fin de Agregar Unidad Minera Contactos

         function eliminarUmineraCon(id){


          $.ajax({
            url: 'eliminarUmineraContacto/' + id,
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show(); 

        
            },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divMineraCon').html(data);
            },
          });          

        }

        function editarUminera(id){
          $.ajax({
            url: 'editUmineraCliente/'+id,
            type:'GET',
              beforeSend: function () {
                                  
              },              
            success : function (data){

              var str = JSON.stringify(data);

              var pushedData = jQuery.parseJSON(str);
              //$('#div_carga').hide(); 
              console.log(pushedData['cunidadminera']);
              
              $("#cuminera_u").val(pushedData['cunidadminera']);
              $("#cuminera_u_1").val(pushedData['cunidadminera']);
              $("#imagenlogo").attr('src','images/logounidadminera/'+pushedData['logo_unidadminera']);
              $("#codigo_um").val(pushedData['codigo']);
              $("#codigo_um").attr("readonly","readonly");
              $("#nombre_um").val(pushedData['nombre']);
              $("#umineraestado").val(pushedData['cumineraestado']);
              $("#telefono_um").val(pushedData['telefono']);
              $("#direccion_um").val(pushedData['direccion']);
              $("#pais_um").val(pushedData['cpais']);
              $("#btnlogominera").show();

                if (pushedData['cubigeo']==null) { 
                
                $('#dpto_um').val(0);
                $("#prov_um").val(0);
                $("#dis_um").val(0);
              }
            
              else{              

                var ubigeo=pushedData['cubigeo'];  
                var dpto=ubigeo.substring(0,2)+'0000';

                var prv=ubigeo.substring(0,4)+'00';
                $("#dpto_um").val(dpto);
                $("#prov_um").val(prv);
                $("#dis_um").val(ubigeo);              

              }      
              
            },
          });
        }

         function editarUmineraCon(id){

          $.ajax({
            url: 'editUmineraContacto/'+id,
            type:'GET',
              beforeSend: function () {
                       
              },              
            success : function (data){

              var str = JSON.stringify(data);

              var pushedData = jQuery.parseJSON(str);

              console.log(pushedData['cunidadmineracontacto']);     
              
              $("#cunidadminera_c").val(pushedData['cunidadmineracontacto']);
              $("#cuminera_con").val(pushedData['cunidadminera']);
              $("#apaterno").val(pushedData['apaterno']);
              $("#amaterno").val(pushedData['amaterno']);
              $("#nombres").val(pushedData['nombres']);
              $("#email").val(pushedData['email']);
              $("#telefono").val(pushedData['telefono']);
              $("#cel1").val(pushedData['cel1']);
              $("#cel2").val(pushedData['cel2']);              
              $("#anexo").val(pushedData['anexo']);
              
             
              if (pushedData['ctipocontacto']==null) {                 
              
                $("#ctipocontacto").val(0);               
              }
            
              else{              

               $("#ctipocontacto").val(pushedData['ctipocontacto']);            

              }      

               if (pushedData['ccontactocargo']==null) {                 
              
                $("#ccontactocargo").val(0);               
              }
            
              else{              

               $("#ccontactocargo").val(pushedData['ccontactocargo']);            

              }                       
             
              
            },
          });
        }


        function limpiarUmineraContacto(){

          $("#cunidadminera_c").val('');
          $("#apaterno").val('');
          $("#amaterno").val('');
          $("#nombres").val('');
          $("#email").val('');
          $("#telefono").val('');
          $("#cel1").val('');
          $("#cel2").val('');              
          $("#anexo").val('');
          $("#ctipocontacto").val(0);   
          $("#ccontactocargo").val(0);    
  
        }