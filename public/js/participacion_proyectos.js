$(document).ready(function(){
	$("#cbPeriodo").val($("#periodo").val());
	$("#semanaini").val($("#semana_ini").val());
	$("#semanafin").val($("#semana_fin").val());
	obtenerElementos();



	console.log("semanafin",$("#semanafin").val());

	$("#cbPeriodo").change(function(){
		obtenerElementos();
	});

	// $("#semanaini").onkeyup(function(){
	// 	obtenerElementos();
	// })

	// $("#semanafin").onkeyup(function(){
	// 	obtenerElementos();
	// })

});
	

function semanaini(){
	obtenerElementos();
}

function semanafin(){
	obtenerElementos();
}


function obtenerElementos()
{
		console.log("obtenerElementos");
		$("#imgLoad").show();
		var url = BASE_URL + '/obtenerListadoParticipacion';
		var objRequest = {};
		objRequest.periodo = $("#cbPeriodo").val();
		objRequest.semanaini = $("#semanaini").val();
		objRequest.semanafin = $("#semanafin").val();
		console.log("objRequest",objRequest);
		$.get(url , objRequest)
		  	.done(function( data ) {		  		
		    	console.log("obtenerListadoParticipacion", data);
		    	mostrarElementos(data.lista);
		});
}

function mostrarElementos(lista)
{
	$("#tbListado tbody").empty();

	for (var i = 0; i < lista.length; i++) {
			var row = "";
			row = "<tr>";			
			row += "<td>"+ (i+1) + "</td>";
			row += "<td>"+ lista[i].codigoproyecto + "</td>";
			row += "<td>"+ lista[i].codigoactividad + "</td>";
			row += "<td>"+ lista[i].descripcion + "</td>";
			// row += "<td>"+ lista[i].periodo + "</td>";
			row += "<td>"+ lista[i].semana + "</td>";
			row += "<td>"+ lista[i].horas + "</td>";
			// row += "<td>"+ lista[i].tipo + "</td>";			
			row += "<td>"+ lista[i].lider + "</td>";	
			row += "<td>"+ lista[i].gerente + "</td>";
			// row += "<td>"+ lista[i].updated_at + "</td>";
			row += "</tr>";
			$("#tbListado > tbody").append(row);
	}

	$("#cantidadResultados").text(lista.length);

	$("#imgLoad").hide();
}
