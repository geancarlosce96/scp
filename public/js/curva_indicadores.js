/*
The purpose of this demo is to demonstrate how multiple charts on the same page
can be linked through DOM and Highcharts events and API methods. It takes a
standard Highcharts config with a small variation for each data set, and a
mouse/touch event handler to bind the charts together.
*/



/**
 * In order to synchronize tooltips and crosshairs, override the
 * built-in events with handlers defined on the parent element.
 */
['mousemove', 'touchmove', 'touchstart'].forEach(function (eventType) {
    document.getElementById('curva-s-indicadores').addEventListener(
        eventType,
        function (e) {
            var chart,
                point,
                i,
                event;

            for (i = 0; i < Highcharts.charts.length; i = i + 1) {
                chart = Highcharts.charts[i];
                // Find coordinates within the chart
                event = chart.pointer.normalize(e);
                // Get the hovered point
                point = chart.series[0].searchPoint(event, true);

                if (point) {
                    point.highlight(e);
                }
            }
        }
    );
});

/**
 * Override the reset function, we don't need to hide the tooltips and
 * crosshairs.
 */
Highcharts.Pointer.prototype.reset = function () {
    return undefined;
};

/**
 * Highlight a point by showing tooltip, setting hover state and draw crosshair
 */
Highcharts.Point.prototype.highlight = function (event) {
    event = this.series.chart.pointer.normalize(event);
    this.onMouseOver(); // Show the hover marker
    this.series.chart.tooltip.refresh(this); // Show the tooltip
    this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
};

/**
 * Synchronize zooming through the setExtremes event handler.
 */
function syncExtremes(e) {
    var thisChart = this.chart;

    if (e.trigger !== 'syncExtremes') { // Prevent feedback loop
        Highcharts.each(Highcharts.charts, function (chart) {
            if (chart !== thisChart) {
                if (chart.xAxis[0].setExtremes) { // It is null while updating
                    chart.xAxis[0].setExtremes(
                        e.min,
                        e.max,
                        undefined,
                        false,
                        { trigger: 'syncExtremes' }
                    );
                }
            }
        });
    }
};

function chart_indicadores(data){
    console.log("chart_indicadores",data);

    var semana = [];
    var hc = [];
     // var hp = [];
     var rate = [];
     var acumulativo = [];
     var performancehora = [];
     var performancehorarate = [];
     var cpihora = [];
     var cpirate = [];
     var cv = [];

    var arrary = [];

    for (var i = 0; i < data.length; i++) {
        semana[i] = data[i].semana;
        // horas ejecutadas
        // hp[i] = data[i].suma_horas_ejecutadas;
        // hc[i] = data[i].acumulado;

        //rate 

        rate[i] = data[i].rate;
        acumulativo[i] =data[i].acumulativo;

        // avance

        // performancehora[i] = data[i].performance_hora;
        performancehorarate[i] = data[i].performance_hora_rate;

        //cpi

        cpihora[i] = data[i].cpi_hora;
        cpirate[i] = data[i].cpi_hora_rate;

        cv[i] = data[i].performance_hora_rate - data[i].acumulativo;

        arrary[i] = data[i].performance_hora_rate / data[i].acumulativo;

        // console.log("sem",semana);

    }

    var activity = {
        "xData": semana,

        "datasets": [{
            "name": "CPI",
                "data": cpirate,
                "unit": "%",
                "type": "line",
                "valueDecimals": 2
        }, 
        {
            "name": "CV",
                "data": cv,
                "unit": "$",
                "type": "area",
                "valueDecimals": 2
        }, 
        // {
        //     "name": "Chart 2 series 1",
        //         "data": arrary,
        //         "unit": "%",
        //         "type": "area",
        //         "valueDecimals": 2
        //     }, 
        ]
    };

    charindicadores(activity);
};
// Get the data. The contents of the data file can be viewed at

    // url: 'https://cdn.jsdelivr.net/gh/highcharts/highcharts@v7.0.0/samples/data/activity.json',
        // lastChart,
    // dataType: 'text',
    function charindicadores(activity) {
console.log('activity',activity);
        // activity = JSON.parse(activity);
        activity.datasets.forEach(function (dataset, i) {

            // Add X values
            dataset.data = Highcharts.map(dataset.data, function (val, j) {
                return [activity.xData[j], val];
            });

            var chartDiv = document.createElement('div');
            chartDiv.className = 'chart';
            document.getElementById('curva-s-indicadores').appendChild(chartDiv);

            Highcharts.chart(chartDiv, {
                chart: {
                    marginLeft: 40, // Keep all charts left aligned
                    spacingTop: 20,
                    spacingBottom: 20
                },
                title: {
                    text: dataset.name,
                    align: 'left',
                    margin: 0,
                    x: 30
                },
                credits: {
                    enabled: false
                },
                legend: {
                    enabled: false
                },
                xAxis: {
                    crosshair: true,
                    events: {
                        setExtremes: syncExtremes
                    },
                    labels: {
                        format: '{value}'
                    }
                },
                yAxis: {
                    title: {
                        text: null
                    }
                },
                tooltip: {
                    positioner: function () {
                        return {
                            // right aligned
                            x: this.chart.chartWidth - this.label.width,
                            y: 10 // align to title
                        };
                    },
                    borderWidth: 0,
                    backgroundColor: 'none',
                    pointFormat: '{point.y}',
                    headerFormat: '',
                    shadow: false,
                    style: {
                        fontSize: '18px'
                    },
                    valueDecimals: dataset.valueDecimals
                },
                series: [{
                    data: dataset.data,
                    name: dataset.name,
                    type: dataset.type,
                    color: Highcharts.getOptions().colors[i],
                    fillOpacity: 0.3,
                    tooltip: {
                        valueSuffix: ' ' + dataset.unit
                    }
                }]
            });
        });
    }

