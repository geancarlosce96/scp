
	var cantidad_preguntas = 0;
	var preguntas = [];
	var preguntas_data = [];

	$(document).ready(function(){

		obtenerPreguntas();	

		$( "#form" ).submit(function( event ) {  			
  			event.preventDefault();
		});

		$("#btn-guardar").click(function(){
			guardarPreguntas();
		});

		$("#btn-aceptar").click(function(){
			redireccion();
		});

	});

	function cargaInicial()
	{		
		cantidad_preguntas = ( preguntas_data.length > 0) ? preguntas_data.length : $("#cantidad_preguntas").val();
		for (var i = 0; i < cantidad_preguntas; i++)
			agregarPregunta(null, preguntas_data[i]);
		completarNumeros();
	}	

	function redireccion()
	{
		window.location = BASE_URL + '/capacitaciones/propuestas';
	}

	function obtenerPreguntas(){
		var url = BASE_URL + '/obtenerPreguntasEvaluacion/' + $("#evaluacion_id").val();
		$.get(url , {})
		  	.done(function( data ) {		  		
		    	preguntas_data = data;		    	
				cargaInicial();
				validarOpcion();
		});
	}

	function validarOpcion()
	{
		var opcion = $("#opcion").val();
		if(opcion == "ver"){
			$("input").prop("readonly", true);
			$(":checkbox").prop("disabled", true)
			$(".btn").prop("disable", true);
		}
	}

	function completarNumeros()
	{
		$.each( $(".grupo"), function( key, value ) {
			$(value).find(".pregunta > .numero").html(key);
		});
	}

	//guardar

	function guardarPreguntas()
	{		
		preguntas = [];
		//retirar el grupo de ejemplo
		$(".grupo.example").first().remove()
		//formar
		$.each( $(".grupo"), function( key, value ) {
  			var pregunta = {};
  			pregunta.descripcion = $(value).find(".pregunta > .descripcion > input").first().val();
  			//alternativas
  			pregunta.alternativas = guardarAlternativas(value);
  			preguntas.push(pregunta);
		});
		//enviar
		var url = $("#form").prop("action");
		var _redirect = $("#_redirect").val();
		var _token = $('input[name="_token"]').first().val();
		$.post(url, { preguntas: preguntas, _token: _token, _redirect: _redirect })
		  	.done(function( data ) {  		
		    	$("#msjRespuesta").text(data.msj);
		    	$("#modalConfimacion").modal("show");
		});
	}

	function guardarAlternativas(obj)
	{
		var alternativas = [];
		
		$.each( $(obj).find(".alternativa"), function( key, value ) {			
			var alternativa = {};
			alternativa.descripcion = $(value).find(".descripcion > input").first().val();
			alternativa.checked =  $(value).find(".chk > input").first().prop("checked");
			alternativas.push(alternativa);
		});

		return alternativas;
	}

	//preguntas
	
	function agregarPregunta(objPos = null, objData = null)
	{		
		//copiar
		var obj = $(".grupo.example").clone();
		//preparar
		$(obj).removeClass("example");
		$(obj).css("display", "block");
		//data
		if(objData!=null)
			$(obj).find(".pregunta > .descripcion > input").first().val(objData.descripcion);
		//agregar
		if(objPos == null)
			$(".grupo").last().after(obj);
		else
			$(objPos).parent().parent().parent().after(obj);
		
		agregarEventosPregunta(obj);
		if(objData!=null)
			agregarAlternativas(obj, objData.alternativas.length, objData.alternativas);
		else
			agregarAlternativas(obj);

		completarNumeros();
	}	

	function retirarPregunta(obj)
	{
		var parent = $(obj).parent().parent().parent();
		$(parent).remove();
		completarNumeros();
	}

	//alternativas
	
	function agregarAlternativas(objGrupo, cantidad = 3, objData = null)
	{
		var obj = $(objGrupo).find(".alternativa").first();
		if(objData!=null){			
			$(obj).find(".descripcion > input").first().val(objData[0].descripcion);
			if(objData[0].correcta == 1)
				$(obj).find(".chk > input").first().prop("checked", true);
		}
		agregarEventosAlternativa(obj);

		for (var i = 1; i < cantidad; i++) {
			obj = $(objGrupo).find(".alternativa").first().clone();
			$(obj).find("input[type=checkbox]").prop("checked", false);
			
			if(objData!=null){

				$(obj).find(".descripcion > input").first().val(objData[i].descripcion);
				if(objData[i].correcta == 1)
					$(obj).find(".chk > input").first().prop("checked", true);
			}

			$(objGrupo).find(".alternativa").last().after(obj);
			agregarEventosAlternativa(obj);
		}
	}

	function agregarAlternativa(obj)
	{		
		var parent = $(obj).parent().parent();
		//copiar
		var obj = $(parent).clone();
		$(obj).find("input").val("");
		$(obj).find("input[type=checkbox]").prop("checked", false);
		//agregar
		$(parent).after(obj);
		agregarEventosAlternativa(obj);
	}

	function retirarAlternativa(obj)
	{
		var parent = $(obj).parent().parent();
		$(parent).remove();
	}

	//eventos

	function agregarEventosPregunta(obj)
	{
		var objAgregar = $(obj).find(".pregunta >.opciones >.btn-agregar").first();
		var objRetirar = $(obj).find(".pregunta >.opciones >.btn-retirar").first();

		$(objAgregar).click(function(){
			agregarPregunta(this);
		});

		$(objRetirar).click(function(){
			retirarPregunta(this);
		});
	}

	function agregarEventosAlternativa(obj)
	{
		var objAgregar = $(obj).find(".opciones >.btn-agregar").first();
		var objRetirar = $(obj).find(".opciones >.btn-retirar").first();

		$(objAgregar).click(function(){
			agregarAlternativa(this);
		});

		$(objRetirar).click(function(){
			retirarAlternativa(this);
		});
	}
	