$(document).ready(function() {

		eventos();

		addchosen('select_cliente');
		addchosen('select_minera');
		addchosen('select_contacto');

		codigo('pre');

	});

function eventos (){

	$('#cerrar_modal').css('display', 'none');

	$("#select_cliente").on("change",function(){
			
			if ($('#select_cliente').val()=="") {
				$('#select_minera').val('');
			}
			else{
				select_minera($('#select_cliente').val(),'nuevo');
			};
		})

		$("#select_minera").on("change",function(){ 
			alert($('#select_minera').val());
			if ($('#select_minera').val()=="") {
				$('#select_contacto').val('');
			}
			else{
				select_contacto($('#select_minera').val(),'nuevo');
				$('#cpersona_u').val($('#select_cliente').val());
			};
		})

		// $('#fecha_preliminar').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		// $('#fecha_visita').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		// $('#fecha_consulta').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		// $('#fecha_absolucion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		// $('#fecha_presentacion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_preliminar').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_visita').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_consulta').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_absolucion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
		$('.fecha_presentacion').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});

		$('#nombre_preliminar').removeAttr('readonly');

		$("#divUmineraContacto").on("hidden.bs.modal", function () {
			$('#divUminera').modal('hide');
		});
}

function codigo(valor){
    	if (valor=='pre') {
    	$('#propuesta').css('display', 'none');
    	$('#preliminar').css('display', 'block');
    	$('#titulo').text("Generar Preliminar")
    	var codigopre = $('#anio').val()+'.'+$('#correlativopre').val();
    	$('#codigopre').val(codigopre);
    	};
		if (valor=='pro') {
		$('#preliminar').css('display', 'none');
		$('#propuesta').css('display', 'block');
		$('#titulo').text("Generar Propuesta")
		var codigopro=$('#anio').val()+'.'+$('#sedecod').val()+'.'+$('#correlativopro').val();
		$('#codigopro').val(codigopro);
		};
    }

function select_minera(id,valor){ alert(id+'-'+valor) ;
		$.ajax({
			url: 'minera/'+id,
			type: 'GET',
		})
		.done(function(data) {

			console.log("success");
			console.log(data);

			$('#select_minera').find('option').remove();
 			$('#select_contacto').find('option').remove();

			limpiar();
			var cunidadminera_temp = 0;
			for (var i = 0; i < data.length; i++) {
				$("#select_minera").append('<option value="'+data[i].cunidadminera+'">'+data[i].nombre+'</option>');
			};
				cunidadminera_temp = data[0].cunidadminera;
			if (valor=='nuevo') {
				$('#select_minera').val(cunidadminera_temp).trigger('chosen:updated');
				$('#mineraid').val(cunidadminera_temp)
				$('#minera_nombre').val(data[0].nombre)
			};
			if (valor=='edit') {
				$('#select_minera').val(objResponse.cunidadminera).trigger('chosen:updated');
			};

			select_contacto(objResponse.cunidadminera,valor);
		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}


function select_contacto(id,valor){ 
		alert(valor);
		$.ajax({
			url: 'contacto/'+id,
			type: 'GET',
		})
		.done(function(data) {
			console.log("success");
			$('#select_contacto').find('option').remove();
			limpiar();
			var cunidadmineracontacto_temp = 0;
			for (var i = 0; i < data.length; i++) {
				var select_contacto = data[i].nombres +' '+ data[i].apaterno +' '+ data[i].amaterno;
				$("#select_contacto").append('<option value="'+data[i].cunidadmineracontacto+'">'+select_contacto+'</option>');
				$('#cargo_pre').val(data[i].descripcion);
				$('#telefono_pre').val(data[i].telefono);
				$('#anexo_pre').val(data[i].anexo);
				$('#correo_pre').val(data[i].email);

				cunidadmineracontacto_temp = data[i].cunidadmineracontacto;
			};

			console.log(cunidadmineracontacto_temp);

			if (valor=='edit') {
				$('#select_contacto').val(objResponse.cunidadmineracontacto).trigger("chosen:updated");
			};
			if (valor=='nuevo'){
			$('#select_contacto').val(cunidadmineracontacto_temp).trigger("chosen:updated");
			};
			

		})
		.fail(function() {
			console.log("error");
		})
		.always(function() {
			console.log("complete");
		});
		
	}