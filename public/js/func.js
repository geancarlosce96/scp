
/**
 * Created by Graciano on 12/09/2016.
 */

function getUrl(url,parametros){

    
     
    $.ajax({
        data:  parametros,
        url:   url,
        type:  'get',
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            $('#div_carga').show();
        },
        success:  function (data) {

            var dhtml="";
            dhtml=data;
            $("#resultado").html(dhtml);
            $('#div_carga').hide();
        },
        statusCode: {
            500: function() {
                //alert("Encontre el error 500");
                //console.log('500');
               //$("#resultado").html(rutaerror);
               var url = BASE_URL + '/int?url=error500';
               //$(location).attr('href',url);
            }
        }

    });
}
function postUrl(url,parametros,idObject){
    
    
    $.ajax({
        data:  parametros,
        url:   url,
        type:  'post',
        beforeSend: function () {
            $("#resultado").html("Procesando, espere por favor...");
            $('#div_carga').show(); 
            
        },
        success:  function (data) {
            var dhtml="";
            dhtml=data;
            $(idObject).fadeIn(1000).html(dhtml);
            $('#div_carga').hide();
        }
    });
}

function postUrlTimeSheet(tok){
    var fecha = $('#divCalendario').datepicker('getFormattedDate');
    // alert(fecha+tok);
    $.ajax({

        type:"POST",
        url:'editarTodosRegistrodeHoras',
        data:{
            fecha: fecha,
            "_token": tok,
            },
        beforeSend: function () {
            $('#div_carga').show(); 

        },
        success: function(data){
                $('#div_carga').hide(); 
                $("#resultado").html(data);

        },
        error: function(data){
            $('#div_carga').hide();
            //$('#detalle_error').html(data);
            //$("#div_msg_error").show();
        }
    });      
}

function removerchosen(id){

    $('#'+id).chosen('destroy');

}

function addchosen(id){

    $('#'+id).chosen(
    {
        allow_single_deselect: true,width:"100%"
    });

}

Highcharts.setOptions({
    lang: {
      loading: "CARGANDO...",
      contextButtonTitle: "Menú "+"contextual "+"del "+"gráfico",
      downloadPNG: "Descargar "+"PNG",
      downloadCSV: "Descargar "+"CSV",
      downloadPDF: "Descargar "+"PDF",
      downloadJPEG: "Descargar "+"JPEG",
      downloadXLS:"Descargar "+"XLS",
      printChart: "Imprimir "+"Gráfico",
      downloadSVG:"Descargar "+"imagen "+"vectorial "+"SVG",
      viewFullscreen: "Ver "+"en "+"pantalla "+"completa",
      viewData: "Ver "+"gráfico "+"en "+"tabla",
      openInCloud: "",
      decimalPoint:".",
      noData: "No "+"hay "+"datos",
      months: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
      shortMonths: ["Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
      weekdays: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
      week : ["Semana"],
      drillUpText: '◁ Regresar',
      },
    global : {
      useUTC : false,
      },
  });

