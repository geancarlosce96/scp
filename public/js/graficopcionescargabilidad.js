function graficosemana(carea,fecha_final) {
    $("#tabla-show").hide();
    $.ajax({
        url: 'graficosemana',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_ini': fecha_final,
        },
    })
    .done(function(data) {
        graficasemana(data);
        $("#tabla-show").show();
        // console.log("graficasemana",data);
        $("#horas_facturables").html(data[1]);
        $("#horas_no_facturables").html(data[2]);
        $("#horas_admin").html(data[3]);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}


function grafico3semanas(carea,fecha_ini,fecha_final) {
    $.ajax({
        url: 'grafico3semanas',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_ini': fecha_ini,
            'fecha_final': fecha_final
            },
    })
    .done(function(data) {
        grafico3semanas_chart(data);
        // console.log("grafico3semanas",data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function graficonofacturable(carea,fecha_final) {
    $.ajax({
        url: 'graficnofacturable',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_ini': fecha_final,
        },
    })
    .done(function(data) {
        graficnofacturable_chart(data);
        // console.log("graficonofacturable",data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function graficoporusuario(carea,fecha_final) {
    $("#tabla-show2").hide();
    $.ajax({
        url: 'grafico_porusuario',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_ini': fecha_final,
        },
    })
    .done(function(data) {
        grafico_porusuario_chart(data);
        $("#tabla-show2").show();

        $(".reset").remove();

        var user = [];
        var fact = [];
        var nofact = [];
        var admin = [];
       // var carga = [];
        var cabecera='<th class="reset"></th>';
        var acumulado_ff='';
        var acumulado_nf='';
        var acumulado_adm='';
        //var carga_u='';
        for(var x = 0; x < data.length; x++)

        {
            user[x] = data[x].abreviatura;
            fact[x] = data[x].sumuser_fac;
            nofact[x] =data[x].sumuser_nofac;
            admin[x] = data[x].sumuser_admin;
           // carga[u] = data[1][u].cargabilidad;


            cabecera +=  '<th class="reset">'+user[x]+'</th>';
            acumulado_ff +=  '<th class="reset">'+fact[x]+'</th>';
            acumulado_nf +=  '<th class="reset">'+nofact[x]+'</th>';
            acumulado_adm +=  '<th class="reset">'+admin[x]+'</th>';
           // carga_u +=  '<th class="reset">'+carga[u]+'</th>';
        }
        $("#abreviatura").append(cabecera);
        $("#ff").append(acumulado_ff);
        $("#nf").append(acumulado_nf);
        $("#adm").append(acumulado_adm);
        //$("#carga").append(carga_u);

        // console.log("graficonofacturable",data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function graficoadmin(carea,fecha_final) {
    $.ajax({
        url: 'graficoadmin',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_ini': fecha_final,
        },
    })
    .done(function(data) {
        graficoadmin_chart(data);
        // console.log("graficonofacturable",data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
}

function graficainternos(carea,fecha_final) {
    $.ajax({
        url: 'graficointernos',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_ini': fecha_final,
        },
    })
    .done(function(data) {
        // console.log("proyectosinternos",data);
        graficointernos_chart(data);
    })
    .fail(function() {
        console.log("error");
    })
    .always(function() {
        console.log("complete");
    });
    
}

function graficaCargaProductividad(carea,fecha_final) {
    $.ajax({
        url: 'graficosemanalCargaProd',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_final': fecha_final,
        },
    })
        .done(function(data) {
            // console.log("proyectosinternos",data);
            graficosemanalCargaProd_chart(data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

}