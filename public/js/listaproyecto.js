

  $(function(){
     $('.chosen').chosen();  
   });
   var table = $('#cargalistaproyecto').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        lengthChange: false,
        buttons: [ 'excel', 'pdf', 'colvis' ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   {
            leftColumns: 6
        }
    } ); 

    table.buttons().container()
        .appendTo( '#cargalistaproyecto_wrapper .col-sm-6:eq(0)' );

   $('.chosen').chosen();  
  // Fin Hoja Resumen
  //Codigo de inputs y cambios registros

  $(".ingsenior").change(function() {

      var cproyecto = $(this).find('option[value="'+$(this).val()+'"]').data('cproyecto');
      var cpersona =  $(this).val();
      var objEnt={cproyecto: cproyecto, cpersona: cpersona };
      $.ajax({
        url: 'ActualizaListaProyecto',
        type:'GET',
        data: objEnt,
        success : function (data){
          $('#alert-actualizo').show();
          setTimeout(function(){
            $('#alert-actualizo').hide();
          }, 2000);
        },
      });

    })

  $("#estadoproyecto").change(function() {
      var estadoproyecto =  $(this).val();
      var objEnt={estadoproyecto: estadoproyecto};
      $.ajax({
        url: 'listaProyectos',
        type:'GET',
        data: objEnt,
        beforeSend: function () {
          $('#div_carga').show();  
        },
        success : function (data){
          $('#div_carga').hide();
          $("#resultado").html(data);
        },
      });
    })
