
//var dataEntregables=[];
var idFila=0;
var selectMotivo = '';
var selectRevision = '';
var selectEntRecepcion = '';
var datamotivo='';
var datarevisiones='';

$(document).ready(function(){

    obtenerTodosEntregablesPry();
    obtenerComboMotivo();
    obtenerRevisionesCombo();
    var item=0;
    var entregablesAgregados=[];

    $('#btnGuardar').click(function(){  

      var objTransmittal = obtenerTR_Detalle();

        var url = BASE_URL + '/generar_transmittal_nuevo/'+$('#cproyecto').val()+'/transmittalGuardar';

        $.post(url , objTransmittal)
        .done(function( data ) { 

            var url_return= BASE_URL + '/listaentregablestr/'+$('#cproyecto').val()+'/vertransmittal/C-A/'+data;
            $(location).attr('href',url_return);

        });

    })


    $('#btnEnviar').click(function(){  
      swal({
        title: "¿Estás seguro que deseas enviar este Tranmittal?",
        text: "Ya no podrá editar el documento si lo envía",
        type: "info",
        className: "red-bg",
        showCancelButton: true,
        dangerMode: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Enviar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
            var objTransmittal = obtenerTR_Detalle();
            var url = BASE_URL + '/generar_transmittal_nuevo/'+$('#cproyecto').val()+'/transmittalEnviar';

            $.post(url , objTransmittal)
            .done(function( data ) { 
                var url_return= BASE_URL + '/listaentregablestr/'+$('#cproyecto').val()+'/vertransmittal/C-A/'+data;
                goPrintTR();
                $(location).attr('href',url_return);
            });
        } else {
          swal("Cancelado", "No se ha enviado el Transmittal", "error");
        }
      });
      return;
    })

    function obtenerTR_Detalle(){

      var detalleTR=[];

      $('.entregablesAgregadosTR').each(
          function(){

            if ($(this).data('agregado')=='si') {
              var c_entregable = $(this).find('.codigo_ent_TR').find('input').data('id');
            }

            else{
              var c_entregable = $('#entregables').find('option[value="'+$(this).find('.codigo_ent_TR').find('input').val()+'"]').data('id');
            }


                if (!c_entregable) {
                  c_entregable='nuevoEntCli';
                 
                }

            if ($(this).find('.codigo_ent_TR').find('input').val()!='') {

              var objEntregable = 
                  {     
                        codigo:                     $(this).find('.codigo_ent_TR').find('input').val(),
                        ctransmittaldetalle:        $(this).data('ctransmittaldetalle'),
                        cproyectoentregables:       c_entregable,
                        //revision:                   $('#revisiones').find('option[value="'+$(this).find('.revision_ent_TR').find('input').val()+'"]').data('id'),
                        revision:                   $(this).find('.revision_ent_TR').children().find('select').val(),
                        descripcion:                $(this).find('.descripcion_ent_TR').find('input').val(),
                        // motivo:                     $('#motivos').find('option[value="'+$(this).find('.motivo_ent_TR').find('input').val()+'"]').data('id'),
                        motivo:                     $(this).find('.motivo_ent_TR').children().find('select').val(),
                        fecha:                      '',
                  };

                  detalleTR.push(objEntregable);
            }

          }
      );


      //Cabecera
      objTransmittal = 
                  {     
                        cproyecto:                  $('#cproyecto').val(),  
                        ctransmittal:               $('#ctransmittal').val(),                  
                        numeroTransmittal:          $('#numTransmittal').val(),
                        tipoTransmittal:            $('#tipoTransmittal').val(),
                        tipoenvio:                  $('#tipoenvio').val(),
                        fechaenvio:                 $('#fechaenvio').val(),
                        cont_atencion:              '',
                        cont_cc:                    '',
                        comentario:                 $('#comentario').val(),
                        detalleTransmittal:         detalleTR,
                        _token :                    $('input[name="_token"]').first().val()
                  };

                  return objTransmittal;

    }


    $('#agregarentregables').click(function(){
        $('#selecTodosEntr').prop('checked',false); 
        $('#searchEntregable').modal({backdrop: "static"});
    })

    $('#searchEntregable').on('show.bs.modal', function() {

      setTimeout(iniDatatableAddEnt,2000);

    })

    $('.select-box').chosen({
        allow_single_deselect: true,width:"100%"
    }); 

    $('#addEntTR').mouseover(function(){
      $('#addEntTR').css({'border': '2px solid rgb(0,176,80)'})
    })

    $('#addEntTR').mouseout(function(){
      $('#addEntTR').css({'border': '0px solid'})
    })

    $('#addEntTR').click(function(){

        var totalFilas =totalFilasTabla();
        var fila = totalFilas+1;

        idFila ++

      
        var row='';
          row +=  "<tr data-centregable='' id='CA_TR_"+idFila+"'class='entregablesAgregadosTR' data-ctransmittaldetalle='nuevo'>";  
          row += "<td class='item_ent'>"+fila+"</td>";
          row += "<td class='codigo_ent_TR'>"+selectEntRecepcion+"</td>"; 
          row += "<td class='revision_ent_TR'><div>"+selectRevision+"</div></td>"; 
          row += "<td class='descripcion_ent_TR'><input class='form-control' type='text' name=''></td>"; 
          row += "<td class='motivo_ent_TR'><div>"+selectMotivo+"</div></td>"; 
          // row +="<td></td>";
          row += "<td><a title='Eliminar' class='eliminarEnt' id='eliminar_"+idFila+"'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>"; 
          row += "</tr>";


          $("#addEntTR").before(row);

          $('#eliminar_'+idFila).click(function(){

              input= $(this).parent().siblings(".codigo_ent_TR").find(".inputCodEntreg").val();
              codEnt = $('#entregables').find('option[value="'+input+'"]').data('id');

              mostrar_ocultar('mostrar',codEnt);
              removerEnt($(this));
              
          })

          $("input").focus(function(){    
            this.select();
          });

          eventoBuscarEntregable();

  
    })



    $('#aceptarEnt').click(function(){

      $('input[name="centregables[]"]:checked').each(
            function() {

                var validar=validarEntregableAgregado($(this).parent().parent().parent().data('centregable'));

                if (validar == 'ok'){
                  
                    item++;

                    var objEntregable = 
                    {     
                          centregable_transmittal:    '',
                          item:                       item,
                          centregable:                $(this).parent().parent().parent().data('centregable'),
                          codigo:                     $(this).parent().parent().siblings('.codigo_ent').text(),
                          revision:                   $(this).parent().parent().siblings('.revision_ent').text(),
                          crevision:                  $(this).parent().parent().siblings('.revision_ent').data('crevision'),
                          descripcion:                $(this).parent().parent().siblings('.decripcion_ent').text(),
                          fecha:                      $(this).parent().parent().siblings('.fechaEmision_ent').text(),
                    };


                    agregarEntregablesSeleccionados(objEntregable);

                    entregablesAgregados.push($(this).parent().parent().parent().data('centregable'));
                    
                    mostrar_ocultar('ocultar',$(this).parent().parent().parent().data('centregable'));
                }
            }
      );

      $('#entregablesagregados_input').val(entregablesAgregados);

    })
    function agregarEntregablesSeleccionados(obj){

        var totalFilas =totalFilasTabla();
        var fila = totalFilas+1;

        idFila++;

        var row ='';
        row +=  "<tr  id='CA_TR_"+idFila+"' data-centregable='"+obj.centregable+"'class='entregablesAgregadosTR' data-ctransmittaldetalle='nuevo'>";  
        row += "<td class='item_ent'>"+fila+"</td>";  
        row += "<td class='codigo_ent_TR'>"+selectEntRecepcion+"</td>"; 
        row += "<td class='revision_ent_TR'><div id='div_revisiones_"+obj.centregable+"'></div></td>"; 
        row += "<td class='descripcion_ent_TR'><input class='form-control' type='text' value='"+obj.descripcion+"'></td>"; 
        row += "<td class='motivo_ent_TR'><div id='div_motivo_"+obj.centregable+"'></div></td>"; 
        row += "<td><a title='Eliminar' class='eliminarEnt' id='eliminar_"+idFila+"'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>"; 
        row += "</tr>";





        // row +=  "<tr data-centregable='' id='CA_TR_"+idFila+"'class='entregablesAgregadosTR' data-ctransmittaldetalle='nuevo'>";  
        // row += "<td class='item_ent'>"+fila+"</td>";
        // row += "<td class='codigo_ent_TR'>"+selectEntRecepcion+"</td>"; 
        // row += "<td class='revision_ent_TR'>"+selectRevision+"</td>"; 
        // row += "<td class='descripcion_ent_TR'><input class='form-control' type='text' name=''></td>"; 
        // row += "<td class='motivo_ent_TR'>"+selectMotivo+"</td>"; 
        // row += "<td><a title='Eliminar' class='eliminarEnt' id='eliminar_"+idFila+"'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>"; 
        // row += "</tr>";

        eventoBuscarEntregable();

        //$('tr[data-centregable="' + liderNombre + '"]').hide();
        $("#addEntTR").before(row);
        $('#CA_TR_'+idFila).find('.codigo_ent_TR').find('input').val(obj.codigo)
        $('#CA_TR_'+idFila).find('.codigo_ent_TR').find('input').prop('readonly',true)
        $('#CA_TR_'+idFila).find('.descripcion_ent_TR').find('input').prop('readonly',true)
        //$("#tableRecepcionEntregablesTR > tbody").append(row);
        $('#eliminar_'+idFila).click(function(){

              input= $(this).parent().siblings(".codigo_ent_TR").find(".inputCodEntreg").val();
              codEnt = $('#entregables').find('option[value="'+input+'"]').data('id');

            mostrar_ocultar('mostrar',codEnt);
            removerEnt($(this));
        })

        comboMotivo(obj.centregable);
        comboRevisiones(obj.centregable,obj.crevision);

    }

    function validarEntregableAgregado(centregable){

      var mensaje='ok'; 

      var entregables_todos=$('#entregablesagregados_input').val();

      entregables_todos=entregables_todos.split(",");


      for (var i = 0; i < entregables_todos.length; i++) {

          if (centregable==entregables_todos[i]) {

             mensaje='YaFueAgregado';

             return mensaje;
            
          }
      }

        return mensaje;
   }

      //$('#entregablesagregados_input').val(entregablesAgregados);


    // $('.eliminarEnt').click(function(){

    //     codEnt = $(this).parent().siblings(".codigo_ent_TR").find(".inputCodEntreg").find($('#entregables')).find('option[value="'+$(this).val()+'"]').data('id');
    //     mostrar_ocultar('mostrar',codEnt);
    //     removerEnt($(this));
    // })


    /*$('#motivos').find('option[value="'+$('#motivo').val()+'"]').data('value');*/

    //comboEntregablesRecepcion();

    $('#fechaenvio').datepicker({
          format: "dd-mm-yyyy",
          language: "es",
          autoclose: true
    });  


    $('#btnBuscarTR').click(function(){

        $('#modalBuscarTR').modal({backdrop: "static"});

        obtenerListaTRProyecto();

    })


    $('#activarCodigoTR').click(function(){

        $('#numTransmittal').prop('readonly','true');
        if ($('#activarCodigoTR:checked').val()=='on') {
            $('#numTransmittal').prop('readonly',null);
        }

    })

     $('#numTransmittal').keyup(function(){

      var url = BASE_URL + '/buscaTRporNumero/'+$('#numTransmittal').val();
      $.get(url)
        .done(function( data ) { 
          if (data=='true') {
            $('#validarTR').css('display','block');
            $('#btnGuardar').hide();
            $('#btnGuardarTemp').show();

          }
          else{
            $('#validarTR').css('display','none');
            $('#btnGuardar').show();
            $('#btnGuardarTemp').hide();
          }

      });


    })

      $("#btnPrint").on('click',function(){    
        goPrintTR();
    });


/****************************************************************/


});
          var idEnt='';

    function eventoBuscarEntregable(){

          $(".inputCodEntreg").click(function(){
              idEnt = $('#entregables').find('option[value="'+$(this).val()+'"]').data('id');

          })

          $(".inputCodEntreg").on('input',function(){
             
              var c_entregable = $('#entregables').find('option[value="'+$(this).val()+'"]').data('id');
              var des_entregable = $('#entregables').find('option[value="'+$(this).val()+'"]').data('descripcion');
              var revision = $('#entregables').find('option[value="'+$(this).val()+'"]').data('revision');
              //var revision = $('#entregables').parent.find('option[value="'+$(this).val()+'"]').data('revision');

              
              $(this).parent().siblings('.descripcion_ent_TR').find('input').prop('readonly','true');
              $(this).parent().siblings('.descripcion_ent_TR').find('input').val(des_entregable);

              //$(this).parent().siblings('.revision_ent_TR').find('input').prop('readonly','true');
              $(this).parent().siblings('.revision_ent_TR').children().find('select').val(revision);
              $(this).parent().siblings('.motivo_ent_TR').children().find('select').val('1');

              mostrar_ocultar('ocultar',c_entregable);

              if (!c_entregable) {

              mostrar_ocultar('mostrar',idEnt);

              c_entregable='nuevoEntCli';
              $(this).parent().siblings('.descripcion_ent_TR').find('input').prop('readonly',null);

            }

          })

          // $(".inputCodEntreg").on('keyup',function(){ 

          //   var c_entregable = $('#entregables').find('option[value="'+$(this).val()+'"]').data('id');
          //   var des_entregable = $('#entregables').find('option[value="'+$(this).val()+'"]').data('descripcion');
          //   var revision = $('#entregables').find('option[value="'+$(this).val()+'"]').data('revision');
          //   //var revision = $('#entregables').parent.find('option[value="'+$(this).val()+'"]').data('revision');

            
          //   $(this).parent().siblings('.descripcion_ent_TR').find('input').prop('readonly','true');
          //   $(this).parent().siblings('.descripcion_ent_TR').find('input').val(des_entregable);

          //   //$(this).parent().siblings('.revision_ent_TR').find('input').prop('readonly','true');
          //   $(this).parent().siblings('.revision_ent_TR').children().find('select').val(revision);
          //   $(this).parent().siblings('.motivo_ent_TR').children().find('select').val('1');

          //   mostrar_ocultar('ocultar',c_entregable);



          //   if (!c_entregable) {

          //     mostrar_ocultar('mostrar',idEnt);

          //     c_entregable='nuevoEntCli';
          //     $(this).parent().siblings('.descripcion_ent_TR').find('input').prop('readonly',null);

          //   }

          // });

    }

    function comboMotivo(id){

        select = '<select class="form-control select-box" name="motivo" id="cbo_motivo_'+id+'">';
        select+= '<option value=""></option>';

        for (var i = 0 ; i < datamotivo.length; i++) {
          select+= '<option value="'+datamotivo[i].cmotivoenvio+'">'+datamotivo[i].descripcion+'</option>';
        }
        select+='</select>';
        $("#div_motivo_"+id).html(select);
        $("#cbo_motivo_"+id).val('1');

        /*$("#cbo_motivo_"+id).chosen(
        {
            allow_single_deselect: true,width:"100%"

        }); */

    }

    function comboRevisiones(id,revision){

        select = '<select class="form-control select-box" name="revision" id="cbo_revisiones_'+id+'">';
        select+= '<option value=""></option>';

        for (var i = 0 ; i < datarevisiones.length; i++) {
          select+= '<option value="'+datarevisiones[i].digide+'">'+datarevisiones[i].valor+'</option>';
        }
        select+='</select>';
        $("#div_revisiones_"+id).html(select);
        $("#cbo_revisiones_"+id).val(revision);

        /*$("#cbo_revisiones_"+id).chosen(
        {
            allow_single_deselect: true,width:"100%"

        });*/ 

    }

    function obtenerComboMotivo(){

      var url = BASE_URL + '/obtenerMotivoCombo';
      $.get(url)
        .done(function( data ) { 
          listarMotivoRecepcion(data);
          datamotivo=data;
        });

    }

    function obtenerRevisionesCombo(){

      var url = BASE_URL + '/obtenerRevisionesCombo';
      $.get(url)
        .done(function( data ) { 
          listarRevisionesRecepcion(data);
          datarevisiones=data;
      });

    }

    function obtenerTodosEntregablesPry(){
      // var  dataEntregables=[];
        var url = BASE_URL + '/listarTodosEntregables/'+$('#cproyecto').val()+'/'+'modal_recepcion_ent/sin';

      $.get(url)
      .done(function( data ) { 


        listarEntregablesRecepcion(data);
         
      });
    }

    function listarMotivoRecepcion(datamotivo){

        selectMotivo = '<select class="form-control select-box" name="motivo">';
        selectMotivo+= '<option value=""></option>';

        for (var i = 0 ; i < datamotivo.length; i++) {
          selectMotivo+= '<option value="'+datamotivo[i].cmotivoenvio+'">'+datamotivo[i].descripcion+'</option>';
        }
        selectMotivo+='</select>';

        return selectMotivo;


        /*selectMotivo = '<input list="motivos" class="form-control">';
        selectMotivo += '<datalist id="motivos">';
            selectMotivo+= '<option value=""></option>';

            for (var i = 0 ; i < datamotivo.length; i++) {
              selectMotivo+= '<option data-id="'+datamotivo[i].cmotivoenvio+'" value="'+datamotivo[i].descripcion+'">';
            }
         selectMotivo += '</datalist>';*/
            //selectMotivo+='</select>';

        //return selectMotivo;

        //$('#motivos').find('option[value="'+$('#motivo_1').val()+'"]').data('id');



    }

    function listarRevisionesRecepcion(datarevisiones){

        selectRevision = '<select class="form-control select-box" name="motivo">';
        selectRevision+= '<option value=""></option>';

        for (var i = 0 ; i < datarevisiones.length; i++) {
          selectRevision+= '<option value="'+datarevisiones[i].digide+'">'+datarevisiones[i].valor+'</option>';
        }
        selectRevision+='</select>';

        return selectRevision;


        /*selectRevision = '<input list="revisiones" class="form-control">';
        selectRevision += '<datalist id="revisiones">';
            selectRevision+= '<option value=""></option>';

            for (var i = 0 ; i < datarevisiones.length; i++) {
              selectRevision+= '<option data-id="'+datarevisiones[i].digide+'" value="'+datarevisiones[i].valor+'">';
            }
         selectRevision += '</datalist>';*/
            //selectMotivo+='</select>';

       // return selectRevision;
        //$('#revisiones').find('option[value="'+$('#revision_1').val()+'"]').data('id');

    }

    function listarEntregablesRecepcion(dataEntregables){

        selectEntRecepcion = '<input list="entregables" class="form-control inputCodEntreg">';
        selectEntRecepcion += '<datalist id="entregables">';
            selectEntRecepcion+= '<option value=""></option>';

            for (var i = 0 ; i < dataEntregables.length; i++) {
              selectEntRecepcion+= '<option class="datalistEnt" id="opcion_entregable_'+dataEntregables[i].cproyectoentregables+'" data-revision="'+dataEntregables[i].crevision+'" data-id="'+dataEntregables[i].cproyectoentregables+'" data-descripcion="'+dataEntregables[i].descripcion+'" value="'+dataEntregables[i].codigo+'">';
            }
         selectEntRecepcion += '</datalist>';
            //selectMotivo+='</select>';*/

        //return entregablesTR;
        //$('#entregables').find('option[value="'+$('#entregable_1').val()+'"]').data('id');

    }

  function obtenerListaTRProyecto(){

    var url = BASE_URL + '/obtenerListaTRProyecto/'+$('#cproyecto').val();
    $.get(url)
      .done(function( data ) { 

        $("#divListaTR").html(data);

     
    });
  }

  function removerEnt(obj){

    idEnt=$(obj).parent().parent().data('centregable')


    $(obj).parent().parent().remove()

    var entregables=[];

    $('.entregablesAgregadosTR').each(
        function()
        {
          var c_entregable = $('#entregables').find('option[value="'+$(this).find('.codigo_ent_TR').find('input').val()+'"]').data('id');
              if (c_entregable) {
                entregables.push(c_entregable);
              }
        }
    );

    $('#check_'+idEnt).prop('checked',false);

    $('#entregablesagregados_input').val(entregables);

    actualizarItem();

  }

  

  function obtenerFilasAgregadas(){
    var total=0;
      $('.agregados_TR').each(
            function(){

              total=total+1;

            }
      );

      return total;
  }

  function obtenerFilasNuevasAgregadas(){
    var total=0;
      $('.entregablesAgregadosTR').each(
            function(){
              total=total+1;
            }
      );

      return total;
  }

  function totalFilasTabla(){
    agregadas = obtenerFilasAgregadas();
    nuevas = obtenerFilasNuevasAgregadas();
    totalFilas = agregadas + nuevas;

    return totalFilas;
  }

  function actualizarItem(){

    var total=0;
    $("#tableRecepcionEntregablesTR > tbody >tr").each(
        function() {
          total=total+1;
          $(this).find('.item_ent').html(total);
        }
      )
      return total;
  }

  function goPrintTR(){

        var id =$('#ctransmittal').val();

        var url = BASE_URL + '/imprimirTransmittal/'+id;

     
        if (id.length > 0 ){

             win = window.open(url,'_blank');
           
                 
            
        }else{
            $('.alert').show();
        }

      /*  if(almacenar=='1'){
            saveHR();
        }*/

  }

  function mostrar_ocultar(accion,id){

    if (accion == 'mostrar') {
        // $('tr[data-centregable="'+id+'"]').show();
        $('#agregar_ent_'+id).show();
        $('option[data-id="'+id+'"]').prop('disabled','');
    }

    if (accion == 'ocultar') {
        // $('tr[data-centregable="'+id+'"]').hide();
        $('option[data-id="'+id+'"]').prop('disabled','disabled');
        $('#agregar_ent_'+id).hide();
    }

    $('#check_'+id).prop('checked',false);
}







   
