var datamotivo='';
var datarevisiones='';

$(document).ready(function(){

  $('.select-box').chosen({
      allow_single_deselect: true,width:"100%"
  }); 

  $('#agregarentregables').click(function(){

      $('#selecTodosEntr').prop('checked',false); 

      $('#searchEntregable').modal({backdrop: "static"});

  })

  $('#searchEntregable').on('show.bs.modal', function() {
      setTimeout(iniDatatableAddEnt,2000);
  })

  var item=0;
  var entregablesAgregados=[];

  obtenerComboMotivo();
  obtenerRevisionesCombo();

  $('#btnGuardar').click(function(){  


      var objTransmittal = obtenerTR_Detalle();

      var url = BASE_URL + '/generar_transmittal_nuevo/'+$('#cproyecto').val()+'/transmittalGuardar';

      $.post(url , objTransmittal)
      .done(function( data ) { 

          var url_return= BASE_URL + '/listaentregablestr/'+$('#cproyecto').val()+'/vertransmittal/A-C/'+data;
          $(location).attr('href',url_return);

      });
      

  })


  $('#btnEnviar').click(function(){  
      swal({
        title: "¿Estás seguro que deseas enviar este Tranmittal?",
        text: "Ya no podrá editar el documento si lo envía",
        type: "info",
        className: "red-bg",
        showCancelButton: true,
        dangerMode: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Enviar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
            var objTransmittal = obtenerTR_Detalle();
            var url = BASE_URL + '/generar_transmittal_nuevo/'+$('#cproyecto').val()+'/transmittalEnviar';
 
            $.post(url , objTransmittal)
            .done(function( data ) { 
                var url_return= BASE_URL + '/listaentregablestr/'+$('#cproyecto').val()+'/vertransmittal/A-C/'+data;
                goPrintTR();
               // $(location).attr('href',url_return);
            });
        } else {
          swal("Cancelado", "No se ha enviado el Transmittal", "error");
        }
      });
      return;
   })



  function obtenerTR_Detalle(){

    var detalleTR=[];

      $('.entregablesAgregadosTR').each(
          function(){

            var objEntregable = 
                {     
                      codigo:                     $(this).find('.codigo_ent_TR').text(),
                      ctransmittaldetalle:        $(this).data('ctransmittaldetalle'),
                      cproyectoentregables:       $(this).data('cproyectoentregables'),
                      revision:                   $(this).find('.revision_ent_TR').find('select').val(),
                      // crevision:                  $(this).find('.revision_ent_TR').data('crevision'),
                      descripcion:                $(this).find('.descripcion_ent_TR').text(),
                      motivo:                     $(this).find('.motivo_ent_TR').find('select').val(),
                      fecha:                      $(this).find('.fecha_ent_TR').html(),
                };

                detalleTR.push(objEntregable);

          }
      );

    //Cabecera
    objTransmittal = 
                {     
                      cproyecto:                  $('#cproyecto').val(),  
                      ctransmittal:               $('#ctransmittal').val(),                  
                      numeroTransmittal:          $('#numTransmittal').val(),
                      tipoTransmittal:            $('#tipoTransmittal').val(),
                      tipoenvio:                  $('#tipoenvio').val(),
                      fechaenvio:                 $('#fechaenvio').val(),
                      cont_atencion:              $('#id_CA').val(),
                      cont_cc:                    $('#id_CC').val(),
                      comentario:                  '',
                      detalleTransmittal:         detalleTR,
                      _token :                    $('input[name="_token"]').first().val()
                };

                return objTransmittal;

  }

  $('#aceptarEnt').click(function(){


      $('input[name="centregables[]"]:checked').each(
            function() {

                var validar=validarEntregableAgregado($(this).parent().parent().parent().data('centregable'));

                if (validar == 'ok'){
                  
                    item++;

                    var objEntregable = 
                    {     
                          centregable_transmittal:    '',
                          item:                       item,
                          centregable:                $(this).parent().parent().parent().data('centregable'),
                          codigo:                     $(this).parent().parent().siblings('.codigo_ent').text(),
                          revision:                   $(this).parent().parent().siblings('.revision_ent').text(),
                          crevision:                  $(this).parent().parent().siblings('.revision_ent').data('crevision'),
                          descripcion:                $(this).parent().parent().siblings('.decripcion_ent').text(),
                          fecha:                      $(this).parent().parent().siblings('.fechaEmision_ent').text(),
                    };


                    agregarEntregablesSeleccionados(objEntregable);

                    entregablesAgregados.push($(this).parent().parent().parent().data('centregable'));

                    mostrar_ocultar('ocultar',$(this).parent().parent().parent().data('centregable'));
                }

            }
      );

      $('#entregablesagregados_input').val(entregablesAgregados);

  })

  function validarEntregableAgregado(centregable){

      var mensaje='ok'; 

      var entregables_todos=$('#entregablesagregados_input').val();

      entregables_todos=entregables_todos.split(",");


      for (var i = 0; i < entregables_todos.length; i++) {

          if (centregable==entregables_todos[i]) {

             mensaje='YaFueAgregado';

             return mensaje;
            
          }
      }

        return mensaje;
  }

  function agregarEntregablesSeleccionados(obj){

      var totalFilas =totalFilasTabla();
      var fila = totalFilas+1;

      var row ='';
      row +=  "<tr data-cproyectoentregables='"+obj.centregable+"'class='entregablesAgregadosTR' data-ctransmittaldetalle='nuevo'>";  
      row += "<td class='item_ent'>"+fila+"</td>";  
      row += "<td class='codigo_ent_TR'>"+obj.codigo+"</td>"; 
      row += "<td class='revision_ent_TR'><div id='div_revisiones_"+obj.centregable+"'></div></td>"; 
      row += "<td class='descripcion_ent_TR'>"+obj.descripcion+"</td>"; 
      row += "<td class='motivo_ent_TR'><div id='div_motivo_"+obj.centregable+"'></div></td>"; 
      row += "<td class='fecha_ent_TR'>"+obj.fecha+"</td>"; 
      row += "<td><a title='Eliminar' class='eliminarEnt' id='eliminar_"+obj.centregable+"'><i class='fa fa-trash-o' aria-hidden='true'></i></a></td>"; 
      row += "</tr>";

      //$('tr[data-centregable="' + liderNombre + '"]').hide();
      $("#tableEntregablesTR > tbody").append(row);
      $('#eliminar_'+obj.centregable).click(function(){

          mostrar_ocultar('mostrar',$(this).parent().parent().data('cproyectoentregables'));
          removerEnt($(this));

      })

      comboMotivo(obj.centregable);
      comboRevisiones(obj.centregable,obj.crevision);

  }

  $('.eliminarEnt').click(function(){

          mostrar_ocultar('mostrar',$(this).parent().parent().data('centregable'));
          removerEnt($(this));
    })

  $('#fechaenvio').datepicker({
          format: "dd-mm-yyyy",
          language: "es",
          autoclose: true
  });  


  $('#btnBuscarCA').click(function(){
      
      $('#tipocontacto').val('001');

        mostrarContactosgregados( $('#id_CA').val())
        limpiarFormContactos();

     $('#modalContacto').modal('show');

  })

  $('#btnBuscarCC').click(function(){

      $('#tipocontacto').val('002');

        mostrarContactosgregados( $('#id_CC').val())
        limpiarFormContactos();

      $('#modalContacto').modal('show');

  })

  $('#btnaceptarContacto').click(function(){
    agregarContactos();
  })


  mostrarContactos();

  $('#btnGuardarContacto').click(function(){

    var objContacto = 
                    {     
                          apaterno:         $('#apaterno').val(),
                          amaterno:         $('#amaterno').val(),             
                          nombres:          $('#nombres').val(),          
                          telefono:         $('#telefono').val(),                
                          email:            $('#email').val(),  
                          cunidadminera:    $('#cunidadminera').val(),
                          _token :          $('input[name="_token"]').first().val()        
                    };


    var url = BASE_URL + '/guardar_contacto';

      $.post(url , objContacto)
      .done(function( data ) { 

          mostrarContactos();
          limpiarFormContactos();


      });

  })

  $('#btnBuscarTR').click(function(){

      $('#modalBuscarTR').modal({backdrop: "static"});

      obtenerListaTRProyecto();

  })


  $('#activarCodigoTR').click(function(){

        $('#numTransmittal').prop('readonly','true');
        if ($('#activarCodigoTR:checked').val()=='on') {
            $('#numTransmittal').prop('readonly',null);
        }

  })

  $('#numTransmittal').keyup(function(){

      var url = BASE_URL + '/buscaTRporNumero/'+$('#numTransmittal').val();
      $.get(url)
        .done(function( data ) { 
            if (data=='true') {
              $('#validarTR').css('display','block');
              $('#btnGuardar').hide();
              $('#btnGuardarTemp').show();

            }
            else{
              $('#validarTR').css('display','none');
              $('#btnGuardar').show();
              $('#btnGuardarTemp').hide();
            }
            
        });

  })

   $("#btnPrint").on('click',function(){    
        goPrintTR();
    });  

  /**********************************************************************/

});

function limpiarFormContactos(){

  $('#apaterno').val('');
  $('#amaterno').val('');
  $('#nombres').val('');
  $('#telefono').val('');
  $('#email').val('');
  
}


function mostrarContactosgregados(ccontactos){

    var contactos = ccontactos.split(',')
    $('input[name="contactos"]:checked').each(
          function() {
            $(this).prop('checked',false); 
          }
    );
    for (var i = 0; i < contactos.length; i++) {
      $('#cont_'+contactos[i]).prop('checked',true);
    }

}


function agregarContactos(){
  var contacto_id=[];
  var contacto_nom=[];

  $('input[name="contactos"]:checked').each(

        function() {

          contacto_id.push($(this).parent().parent().data('cumcontacto'));
          contacto_nom.push($(this).parent().siblings('.contactoNombre').text()+' '+$(this).parent().siblings('.contactoApaterno').text());
        }
  );

  if($('#tipocontacto').val()=='001'){
        $('#id_CA').val(contacto_id);
        $('#contactosAtencion').val(contacto_nom);

  }

  if($('#tipocontacto').val()=='002'){
        $('#id_CC').val(contacto_id);
        $('#contactosCC').val(contacto_nom);
        
  }

}

function mostrarContactos(){

  var url = BASE_URL + '/obtenerListaClientes/'+$('#cproyecto').val()+'/'+$('#ctransmittal').val()+'/'+$('#tipocontacto').val();

  $.get(url)
  .done(function( data ) { 
    $('#divContactosTR').html(data);

    mostrarContactosgregados( $('#id_CC').val());
  });
//$('#cproyecto').val();*/
}

function comboMotivo(id){

    select = '<select class="form-control select-box" name="motivo" id="cbo_motivo_'+id+'">';
    select+= '<option value=""></option>';

    for (var i = 0 ; i < datamotivo.length; i++) {
      select+= '<option value="'+datamotivo[i].cmotivoenvio+'">'+datamotivo[i].descripcion+'</option>';
    }
    select+='</select>';
    $("#div_motivo_"+id).html(select);
    $("#cbo_motivo_"+id).val('1');

    /*$("#cbo_motivo_"+id).chosen(
    {
        allow_single_deselect: true,width:"100%"

    }); */

}

function comboRevisiones(id,revision){

    select = '<select class="form-control select-box" name="revision" id="cbo_revisiones_'+id+'" disabled>';
    select+= '<option value=""></option>';

    for (var i = 0 ; i < datarevisiones.length; i++) {
      select+= '<option value="'+datarevisiones[i].digide+'">'+datarevisiones[i].valor+'</option>';
    }
    select+='</select>';
    $("#div_revisiones_"+id).html(select);
    $("#cbo_revisiones_"+id).val(revision);

    /*$("#cbo_revisiones_"+id).chosen(
    {
        allow_single_deselect: true,width:"100%"

    });*/ 

}

function obtenerComboMotivo(){

  var url = BASE_URL + '/obtenerMotivoCombo';
  $.get(url)
    .done(function( data ) { 
      datamotivo=data;
    });

}

function obtenerRevisionesCombo(){

  var url = BASE_URL + '/obtenerRevisionesCombo';
  $.get(url)
    .done(function( data ) { 
      datarevisiones=data;
  });

}

function obtenerListaTRProyecto(){

  var url = BASE_URL + '/obtenerListaTRProyecto/'+$('#cproyecto').val();
  $.get(url)
    .done(function( data ) { 

      $("#divListaTR").html(data);

     
  });

} 

function removerEnt(obj){

  idEnt=$(obj).parent().parent().data('centregable')


  $(obj).parent().parent().remove()

  var entregables=[];

  $('.entregablesAgregadosTR').each(
      function()
      {
        var c_entregable = $('#entregables').find('option[value="'+$(this).find('.codigo_ent_TR').find('input').val()+'"]').data('id');
            if (c_entregable) {
              entregables.push(c_entregable);
            }
      }
  );

  $('#check_'+idEnt).prop('checked',false);
  
  $('#entregablesagregados_input').val(entregables);
  



  actualizarItem();

}

function obtenerFilasAgregadas(){
  var total=0;
    $('.agregados_TR').each(
          function(){

            total=total+1;

          }
    );

    return total;
}

function obtenerFilasNuevasAgregadas(){
  var total=0;
    $('.entregablesAgregadosTR').each(
          function(){
            total=total+1;
          }
    );

    return total;
}

function totalFilasTabla(){
  agregadas = obtenerFilasAgregadas();
  nuevas = obtenerFilasNuevasAgregadas();
  totalFilas = agregadas + nuevas;

  return totalFilas;
}

function actualizarItem(){

  var total=0;
  $("#tableEntregablesTR > tbody >tr").each(
      function() {
        total=total+1;
        $(this).find('.item_ent').html(total);
      }
  )
    return total;
}


    function goPrintTR(){

        var id =$('#ctransmittal').val();

        var url = BASE_URL + '/imprimirTransmittal/'+id;

     
        if (id.length > 0 ){

             win = window.open(url,'_blank');
           
                 
            
        }else{
            $('.alert').show();
        }

      /*  if(almacenar=='1'){
            saveHR();
        }*/

  }

function mostrar_ocultar(accion,id){

  if (accion == 'mostrar') {
      // $('tr[data-centregable="'+id+'"]').show();
      $('#agregar_ent_'+id).show();

  }

  if (accion == 'ocultar') {
      // $('tr[data-centregable="'+id+'"]').hide();
      $('#agregar_ent_'+id).hide();
  }
  $('#check_'+id).prop('checked',false);
}

function actualizar_tabla_entregables(){

  var url = BASE_URL + '/actualizarTablaDetalleTR/'+$('#ctransmittal').val();
  $.get(url)
    .done(function( data ) { 

        $("#tableEntdivLEntregable").html(data);
     
  });

}




