
	$(document).ready(function(){

		actualizar_perido_semana();
		obtenerProyectos();
		// horas_totales();
		/*$("#listaProyectos").change(function(){
			obtenerUsuarios();
			obtenerLogs();
		});*/

		$("#fecha").change(function(){
			actualizar_perido_semana();
			obtenerUsuarios();
			obtenerLogs();
		});
		
		$('#fecha').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});

		$("#chkUsuarios").click(function(){
      		obtenerUsuarios();
		});

		$("#chkUsuariosHorasRegistradas").click(function(){
			cambiarOpcionMostrarUsuariosConHoras();
			obtenerUsuarios();
		});

		// $("#chkUsuariosxGerencia").click(function(){
		// 	// cambiarOpcionMostrarUsuariosxGerencia();
		// 	obtenerUsuariosDeGerencia();
		// });

		$("#chkProyectosHorasRegistradas").click(function(){
			cambiarOpcionMostrarProyectosConHoras();
			obtenerProyectos();
		});

		$("#btn-verResumen").click(function(){
      		verResumen();
		});

		var table = $('#tablaUsuarios').DataTable( {
		"bFilter": false,
		"bSort": false,
		"bPaginate": false,
		"bLengthChange": false,
		"bInfo": false,
        lengthChange: false,
        scrollY:        "600px",
        scrollCollapse: true,
	    } ); 

	    var table = $('#listaProyectos2').DataTable( {
		"bFilter": false,
		"bSort": false,
		"bPaginate": false,
		"bLengthChange": false,
		"bInfo": false,
        "order": [ 1, "asc" ],
        lengthChange: false,
        scrollY:        "600px",
        scrollCollapse: true,
	    } );

});

	var cproyecto = 0;
	var mostrarUsuariosConHoras = false;
	var mostrarProyectosConHoras = false;
	var mostrarUsuariosGerencia = false;


	function cambiarOpcionMostrarUsuariosConHoras()
	{
		mostrarUsuariosConHoras = $("#chkUsuariosHorasRegistradas").prop("checked");
	}

	function cambiarOpcionMostrarProyectosConHoras()
	{
		mostrarProyectosConHoras = $("#chkProyectosHorasRegistradas").prop("checked");	
	}

	// function cambiarOpcionMostrarUsuariosxGerencia()
	// {
	// 	mostrarUsuariosGerencia = $("#chkUsuariosxGerencia").prop("checked");
	// }

	function obtenerProyectos()
	{
		var url = BASE_URL + '/planificacion/obtenerProyectos';		
		objRequest = {mostrarProyectosConHoras: mostrarProyectosConHoras, periodo: $("#periodo").val(), semana: $("#semana").val()}

		$.get(url , objRequest)
		  	.done(function( data ) {		  		
		    	//console.log("obtenerProyectos", data);
		    	mostrarProyectos(data.lista);
		});
	}

	function obtenerLogs()
	{		
		var url = BASE_URL + '/planificacion/obtenerLogsdeProyecto/' + cproyecto;		
		$.get(url , {})
		  	.done(function( data ) {		  		
		    	//console.log("obtenerLogs", data);
		    	mostrarLogs(data.lista);		    	
		});
	}

	function obtenerUsuarios()
	{
		if($("#fecha").val() == "")
			return;

		var usuarioRelacionados = $("#chkUsuarios").prop("checked");
		$("#imgLoad").show();
		if(usuarioRelacionados)
			obtenerUsuariosdeProyecto();
		else
			obtenerUsuariosDeArea();
	}

	// function obtenerUsuariosxGerencia()
	// {
	// 	if($("#fecha").val() == "")
	// 		return;

	// 	var usuarioRelacionadosGerencia = $("#chkUsuarios").prop("checked");
	// 	$("#imgLoad").show();
	// 		obtenerUsuariosDeGerencia();
	// }

var listacol = [];
	function obtenerUsuariosdeProyecto()
	{
		var url = BASE_URL + '/planificacion/obtenerUsuariosdeProyecto/' + cproyecto;
		var obj = {cproyecto: cproyecto, periodo: $("#periodo").val(), semana: $("#semana").val(), mostrarUsuariosConHoras: mostrarUsuariosConHoras}
		$.get(url , obj)
		  	.done(function( data ) {		  		
		    	console.log("obtenerUsuariosdeProyecto", data);		    	
		    	$("#imgLoad").hide();
		    	mostrarUsuarios(data.lista);
		    	enviar_lista();
		    	listacol=data.lista;
		    	chart(listacol);
		});
	}

	function obtenerUsuariosDeArea()
	{
		var url = BASE_URL + '/planificacion/obtenerUsuariosDeArea/' + cproyecto;
		var obj = {cproyecto: cproyecto, periodo: $("#periodo").val(), semana: $("#semana").val(), mostrarUsuariosConHoras: mostrarUsuariosConHoras}
		$.get(url , obj)
		  	.done(function( data ) {		  		
		    	$("#imgLoad").hide();
		    	console.log("obtenerUsuariosDeArea", data);
		    	mostrarUsuarios(data.lista);
		    	listacol=data.lista;
		    	chart(listacol);
		});
	}

	// function obtenerUsuariosDeGerencia()
	// {
	// 	var url = BASE_URL + '/planificacion/obtenerUsuariosDeGerencia/' + cproyecto;
	// 	var obj = {cproyecto: cproyecto, periodo: $("#periodo").val(), semana: $("#semana").val(), mostrarUsuariosConHoras: mostrarUsuariosConHoras}
	// 	$.get(url , obj)
	// 	  	.done(function( data ) {		  		
	// 	    	$("#imgLoad").hide();
	// 	    	console.log("obtenerUsuariosDeGerencia", data);
	// 	    	mostrarUsuarios(data.lista);
	// 	    	listacol=data.lista;
	// 	    	chart(listacol);
	// 	});
	// }

	function obtenerUltimasActualizaciones()
	{
		var url = BASE_URL + '/planificacion/obtenerUltimasActualizaciones';
		$.get(url , {})
		  	.done(function( data ) {		  		
		    	console.log("obtenerUltimasActualizaciones")
		    	console.log(data);
		});
	}

	//mostrar

	function mostrarProyectos(lista)
	{
		$("#listaProyectos2 tbody").empty();
		
		for (var i = 0; i < lista.length; i++) {
			//$("#listaProyectos").append("<option value='"+ lista[i].cproyecto +"'>"+ lista[i].nombre + "</option>");

			var row = "";
			if(i==0){
				cproyecto = lista[i].cproyecto;
				row = "<tr class='proyecto-seleccionado'";
			}else
			row = "<tr class='proyecto'";
			
			row += "data-cproyecto='"+ lista[i].cproyecto +"'>";
			row += "<td>"+ (i+1) +"</td><td>"+ lista[i].nombre + "</td>";
			// row += "<td>"+lista[i].suma_eje+"</td><td></td>";
			// row += "<td></td><td></td>";
			$("#listaProyectos2 > tbody").append(row);
		}

		eventos_lista_proyectos();
		actualizar_perido_semana();
		obtenerUsuarios();
		obtenerLogs();
	}

	function mostrarUsuarios(lista)
	{
		//console.log("mostrarUsuarios");		
		$("#tablaUsuarios tbody").empty();

		if(lista.length == 0){
			$("#divMensajeSinRegistros").show();
			//$("#divTabla").hide();
		}
		else{
			$("#divMensajeSinRegistros").hide();
			//$("#divTabla").show();
		}
		console.log("mostrarUsuarios",lista);
		for (var i = 0; i < lista.length; i++) {
			var row = "";
			row = "<tr ";
			if(lista[i].horas.disponibles < 0)
				row += "class='danger'";
			row += "data-usuario='"+ lista[i].user +"'>"
			row += "<td>"+ (i+1) +"</td><td>"+ lista[i].nombre + "</td>";
			row += "<td>"+ lista[i].profesion + "</td>";
			// if(lista[i].usuarioProyecto==true || lista[i].usuarioProyecto==null)
				row += "<td>"+ '<input class="form-control planificacion" type="text" name="" data-planificacionanterior="'+ lista[i].horas.planificadas +'" value="'+lista[i].horas.planificadas+'" >' +"</td>";
			// else
				// row += "<td></td>";
			row += "<td><span class='disponibilidad' > "+ lista[i].horas.disponibles +" </span></td>";
			row += "<td><span class='ejecucion'>"+ lista[i].horas.ejecutadas +"</span></td>";
			row += "</tr>";
			$("#tablaUsuarios > tbody").append(row); 
		}

		tabla_campos_eventos();
	}

	function mostrarLogs(lista)
	{
		//console.log("mostrarLogs");
		$("#listaActualizaciones").html("");

		if(lista.length <= 0){
			$("#listaActualizaciones").html("<p>No hay actualizaciones.</p>");
			return;
		}

		for (var i = 0; i < lista.length; i++) {
			var actualizacion = '<p><span class="label label-default">'+ lista[i].created_at +'</span> '+ lista[i].mensaje +'</p>';
			$("#listaActualizaciones").append(actualizacion);
		}

	}

	function verResumen()
	{
		console.log("verResumen");
		var url = BASE_URL + '/planificacion/resumen';
		var objRequest = {};
		objRequest.periodo = $("#periodo").val();
		objRequest.semana = $("#semana").val();
		
		$.get(url , objRequest)
		  	.done(function( data ) {		  		
		    	$("#divTablaResumen").html(data);
		    	$("#modalResumen").modal("show");
		});
	}	

	//eventos
	
	function eventos_lista_proyectos()
	{
		$("#listaProyectos2 > tbody > tr").click(function(){		

			cproyecto = $(this).data("cproyecto");
			//retirar class, agregar class
			$(this).removeClass( "proyecto" );
			$(this).parent().find("tr").removeClass( "proyecto-seleccionado" );			
			$(this).addClass( "proyecto-seleccionado" );

			obtenerUsuarios();
			obtenerLogs();
		});
	}

//tiempo real

var socket = null;
var objLista = {};
var SERVIDOR_NODE =  document.getElementById("SERVIDOR_NODE").value;

	$(document).ready(function(){
		conectar();
	});

	function conectar(){
		//console.log("conectar");
		// socket = io("http://localhost:3000");
		socket = io(SERVIDOR_NODE);		
		socket_eventos();
		tabla_campos_eventos();
	}

	function tabla_campos_eventos()
	{
		$(".planificacion").focusout(function() {
			guardar_planificacion(this);			
		});
	}

	//enviar 1 

	function enviar_lista(){
		objLista.periodo = $("#periodo").val();
		objLista.semana = $("#semana").val();
		var usuarios = [];

		$("#tablaUsuarios > tbody > tr").each(function(e){
			var usuario = $(this).data("usuario");	
			usuarios.push(usuario);
		});
		objLista.lista = usuarios;
		socket.emit('enviar lista', objLista);
		console.log('enviar lista', objLista);
	}

	// avisar 2

	function guardar_planificacion(obj)
	{
		var url = BASE_URL + "/planificacion/guardar/horas";
		var objHoras = {proyecto: "", periodo: 0, semana: 0, usuario: 0, planificadas: 0};
		var disponibilidad_actual = $(obj).parent().parent().find(".disponibilidad").first().text();

		objHoras.proyecto = cproyecto;
		objHoras.periodo = $("#periodo").val();
		objHoras.semana = $("#semana").val();
		objHoras.usuario = $(obj).parent().parent().data("usuario");
		objHoras.planificadas = $(obj).val();
		objHoras.planificadas_anterior = $(obj).data("planificacionanterior");
		objHoras._token = $('input[name="_token"]').first().val();

		if(objHoras.planificadas_anterior == objHoras.planificadas)
			return;

		//objHoras.total = parseInt(disponibilidad_actual) + parseInt(objHoras.planificadas_anterior) - parseInt(objHoras.planificadas);

		//Dactual - planificacion*
		$.post( url, objHoras)
			.done(function( data ) {
				console.log("guardar_planificacion",obj, data);
				enviar_actualizar(obj, data);		    	
		});	
		
	}

	function enviar_actualizar(obj, data)
	{		
		console.log("enviar_actualizar ...");
		var planificacion = $(obj).val();
		var usuario = $(obj).parent().parent().data("usuario");
		var usuario_notificacion = $("#usuario").val();
		var disponibilidad_actual = $(obj).parent().parent().find(".disponibilidad").first().text();
		var planificacionanterior = $(obj).data("planificacionanterior");		
		//var total = parseInt(disponibilidad_actual) + parseInt(planificacionanterior) - parseInt(planificacion);

		if(planificacion == "")
			return;

		$(obj).data("planificacionanterior", planificacion);

		var objUsuario = {usuario: usuario, disponibilidad: data.disponibles, planificacion: planificacion, usuario_notificacion: usuario_notificacion };
		objUsuario.periodo = $("#periodo").val();
		objUsuario.semana = $("#semana").val();
		console.log("enviar_actualizar", objUsuario);
		socket.emit('guardar disponibilidad', objUsuario);		
	}

	// actualizar 3

	function actualizar_disponibilidad(obj)
	{
		console.log("actualizar_disponibilidad");

		$("#tablaUsuarios > tbody > tr").each(function(e){
			if( $(this).data("usuario") == obj.usuario )
			{
				var fila = $(this).find("td > .disponibilidad").first();
				$(fila).text(obj.disponibilidad);
				
				if(obj.disponibilidad<0)
					$(this).addClass("danger");
				else
					$(this).removeClass("danger");

				//actualizar array*				
				chart_actualizar_datos(obj.usuario, obj.disponibilidad, parseInt(obj.planificacion), obj.ejecucion);
			}
		});
	}

	function actualizar_mensaje(obj)
	{	
		var hoy = new Date;
		var ahora = hoy.getDate()+"-"+hoy.getMonth()+"-"+hoy.getFullYear()+" "+hoy.getHours()+":"+hoy.getMinutes()+":"+hoy.getSeconds();
		console.log("actualizar_mensaje", obj);
		if($("#usuario").val() == obj.usuario_notificacion) return;
		
		$("#mensajes_tiempo_real").show();
		$("#tr_mensaje").text(" En estos momento el usuario "+ obj.usuario_notificacion +" ha registrado horas planificadas al usuario  "+ obj.usuario + " " + ahora);
	}

	//eventos 

	function socket_eventos()
	{

	socket.on('actualizar disponibilidad', function(obj){
		actualizar_disponibilidad(obj);
	});

	socket.on('actualizar mensaje', function(obj){
		actualizar_mensaje(obj);
	});
		
	}

	function actualizar_perido_semana()
	{
		var dia = moment($("#fecha").val() ).add(1, 'days').format('YYYY-MM-DD');
		var semana = obtenerNumeroSemana( dia );
		var periodo = $("#fecha").val();
		periodo = periodo.substring(0,4);
		$("#semana").val(semana);
		$("#periodo").val(periodo);
		$("#num_semana").text(semana);

		var url = $("#urlResumenPanoramico").val() + "/" + periodo + "/" + semana;
		$("#linkPanoramico").prop("href", url);
	}


/*charts*/
function chart (lista){

var user = [];
var hd = [];
var hp = [];
var he = [];
// var ha = [];
var re = [];
var dia = moment($("#fecha").val() ).add(1, 'days').format('YYYY-MM-DD');
var sem = obtenerNumeroSemana( dia );
for (var i = 0; i < lista.length; i++) {
	user[i] = lista[i].nombre;
	hd[i] = lista[i].horas.disponibles;
	hp[i] = lista[i].horas.planificadas;
	he[i] = lista[i].horas.ejecutadas;
	// ha[i] = lista[i].horas.ejecutadas;
	re[i] = he[i]-hp[i];
	console.log("grafico",sem);

}
// alert(hd);


Highcharts.chart('container', {
    chart: {
        text: 'Combination chart'
    },
    title: {
        text: 'Programación'
    },
    subtitle: {
        text: 'Semana '+sem
    },
    xAxis: {
        categories: user
    },
    yAxis: {
        title: {
            text: 'Horas'
        }
    },
    credits: {
        enabled: false,
    },
    colors: ['yellow','green','blue','red'],
    series: [
    {
        name: 'Planificadas',
        data: hp,
        type: 'column',
    },
    {
        name: 'Disponibles',
        data: hd,
        type: 'column',
    },
    {
        type: 'spline',
        name: 'Reprogramadas',
        data: re,
        marker: {
            lineWidth: 2,
            lineColor: 'blue',
            fillColor: 'white'
        }
    }, 
    {
        type: 'spline',
        name: 'Ejecutadas',
        data: he,
        marker: {
            lineWidth: 2,
            lineColor: 'red',
            fillColor: 'white'
     }
    },
    ],
});
}

function chart_actualizar_datos(user, disponibles, planificadas, ejecucion)
{
	
	for (var i = 0; i < listacol.length; i++) {
		if(listacol[i].user == user)
		{
			listacol[i].horas.disponibles = disponibles;
			listacol[i].horas.planificadas = planificadas;
			listacol[i].horas.ejecutadas = ejecucion;
			console.log("chart_actualizar_datos");
			break;
		}
	}
	chart(listacol);
}

// function horas_totales() {
// 	var url = BASE_URL + '/planificacion/horas_totales';
// 	var obj = {periodo: $("#periodo").val(), semana: $("#semana").val()}
// 	$.get(url , obj)
// 	.done(function(data) {
// 		console.log("horas_totales",data);
// 	});
// }