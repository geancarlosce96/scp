function graficaentregablexcd(data) {
    // console.log("grafico3semanas_chart", data);

    var cantidad_plani = [];
    var cantidad_entre_apr = [];
    var cantidad_entre_rec = [];
    var cumplio = [];
    var nocumplio = [];
    var suma_cumplio = [];
    var suma_nocumplio = [];
    var fecha = [];
    var abreviatura = [];

    for (var i = 0; i < data.length; i++) {
        cantidad_plani[i] = data[i].cantidad_plani;
        cantidad_entre_apr[i] = data[i].cantidad_entre_apr;
        cantidad_entre_rec[i] = data[i].cantidad_entre_rec;
        cumplio[i] = data[i].cumplio;
        nocumplio[i] = data[i].nocumplio;
        suma_cumplio[i] = data[i].suma_cumplio;
        suma_nocumplio[i] = data[i].suma_nocumplio;
        abreviatura[i] = data[i].abreviatura;
        fecha[i] = moment(data[i].fecha).format("DD-MM-YY");
        // horas ejecutadas
    }

    Highcharts.chart('entrexcd', {
        chart: {
            text: 'Combination chart'
        },
        title: {
           // text: 'HH de '+$("#careas option:selected").text()+' semana '+data['semana']+'
            text: 'Cumplimiento de entregables al '+ fecha[0]
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: [{
            categories: abreviatura,

            title: {
                // text: ,
                className: 'highcharts-color-1',
                crosshair: true
            },

        }],
        yAxis: {
            title: {
                text: 'Horas'
            }
        },
        credits: {
            enabled: false,
        },
        colors: [
            '#CD201A','#11B15B','#0069AA'
        ],
        series: [
            {
                type: 'column',
                name: 'Entregables planificados',
                data:cantidad_plani,
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }

            },
            {
                type: 'column',
                name: 'Entregables por emitidos',
                data: cantidad_entre_apr,
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }

            } ,
            {
                type: 'column',
                name: 'Entregables emitidos',
                data: cantidad_entre_rec,
                dataLabels: {
                enabled: true,
                // rotation: -90,
                color: '#000000',
                align: 'center',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                    }
                }

            } ,
            // {
            //     type: 'column',
            //     renderTo: 'container',
            //     name: 'Entregables cumplió',
            //     data: cumplio,
            //     stack: 'male',
            //     dataLabels: {
            //     enabled: true,
            //     // rotation: -90,
            //     color: '#000000',
            //     align: 'center',
            //     format: '{point.y:.2f}', // one decimal
            //     y: 10, // 10 pixels down from the top
            //     style: {
            //         fontSize: '13px',
            //         fontFamily: 'Verdana, sans-serif'
            //         }
            //     }

            // } ,
            // {
            //     type: 'column',
            //     renderTo: 'container',
            //     name: 'Entregables no cumplió',
            //     data: nocumplio,
            //     stack: 'fecm',
            //     dataLabels: {
            //     enabled: true,
            //     // rotation: -90,
            //     color: '#000000',
            //     align: 'center',
            //     format: '{point.y:.2f}', // one decimal
            //     y: 10, // 10 pixels down from the top
            //     style: {
            //         fontSize: '13px',
            //         fontFamily: 'Verdana, sans-serif'
            //         }
            //     }

            // } ,
            {
                type: 'column',
                renderTo: 'container',
                // plotBackgroundColor: null,
                // plotBorderWidth: null,
                // plotShadow: false,
                name: 'Cumplimiento',
                data: [{
                    name: 'Cumplió',
                    y: cumplio,
                    color: 'blue',
                    // sliced: true,
                    // selected: true,
                    // colorByPoint: true,
                }, {
                    name: 'No cumplió',
                    y: nocumplio,
                    color: 'yellow',
                    // colorByPoint: true,
                    // sliced: true,
                    // selected: true
                }],
                // center: [10, 10],
                // size: 100,
                // showInLegend: false,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            },
            {
                type: 'pie',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: true,
                name: 'Cumplimiento',
                data: [{
                    name: 'Cumplió',
                    y: suma_cumplio[0],
                    color: 'green',
                    // sliced: true,
                    // selected: true,
                    colorByPoint: true,
                }, {
                    name: 'No cumplió',
                    y: suma_nocumplio[0],
                    color: 'red',
                    colorByPoint: true,
                    // sliced: true,
                    // selected: true
                }],
                center: [10, 10],
                size: 100,
                showInLegend: false,
                dataLabels: {
                    enabled: false,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                    color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        ],
    });
}