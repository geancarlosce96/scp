function graficomensualadmin_chart (data) {

    var descnofacadmin = [];
    var sumnofacadmin = [];
    var mes = [];




    for(var z = 0; z < data.length; z++)
    {
        descnofacadmin[z] = data[z].descripcion;
        sumnofacadmin[z] = data[z].sumatodos;
        mes[z] = data[z].mes;

    }



    Highcharts.chart('carga-mensual-administrativo', {
        chart: {
            text: 'Combination chart'
        },
        title: {
            text: 'HH de '+$("#careas option:selected").text()+' mes '+mes[0]+' Anddes Administrativo'
            // text: 'HH de '+$("#careas option:selected").text()+' Anddes Administrativo',
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: {
            categories: descnofacadmin,
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Horas'
            }
        }],
        credits: {
            enabled: false,
        },
        colors: [
            '#0069AA'
        ],
        series: [
            {
                type: 'column',
                name: 'Administrativas',
                data: sumnofacadmin,
                dataLabels: {
                    enabled: true,
                    // rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }
        ],

    });

}