function graficomensualinternos_chart (data) {
    // console.log("graficointernos_chart", data);


    var codigo = [];
    var suma_horas = [];
    var mes = [];
    var nombre = [];


    for (var i = data.length - 1; i >= 0; i--)
    {
        codigo[i] = data[i].codigo;
        suma_horas[i] = data[i].suma_horas;
        mes[i] = data[i].mes;
        nombre[i] = data[i].nombre;
    }

    // console.log(suma_horas)

// Create the chart
    Highcharts.chart('carga-mensual-interno', {
        chart: {
            text: 'Combination chart'
        },
        title: {
            text: 'HH de '+$("#careas option:selected").text()+' mes '+mes[0]+' Proyectos internos'
            // text: 'HH de '+$("#careas option:selected").text()+' General No Facturables',
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: {
            categories: codigo,
        },
        yAxis: [{
            min: 0,
            title: {
                text: 'Horas'
            }
        }],
        credits: {
            enabled: false,
        },
        colors: [
            '#BABDB6FF'
        ],
        series: [
            {
                type: 'column',
                name: 'Proyectos internos',
                data: suma_horas,
                // tooltip: {
                //     headerFormat: '<em>Nombre del proyecto : '+nombre+'</em><br/>'
                // },
                dataLabels: {
                    enabled: true,
                    // rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }
        ],

    });



}