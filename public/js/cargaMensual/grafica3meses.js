function grafico3meses_chart (data) {
    // console.log("grafico3semanas_chart", data);

    var fact = [];
    var nofact = [];
    var admin = [];
    var interna = [];
    var mes = [];

    for (var i = 0; i < data.length; i++) {
        fact[i] = data[i].horasFF;
        nofact[i] = data[i].horasFN;
        admin[i] = data[i].horasAND;
        interna[i] = data[i].horasINT;
        mes[i] = data[i].mes;
        // horas ejecutadas
    }

    Highcharts.chart('carga_mensual_3', {
        chart: {
            text: 'Combination chart'
        },
        title: {
            // text: 'HH de '+$("#careas option:selected").text()+' semana '+data['semana']+'
            text: 'HH de '+$("#careas option:selected").text()+' por meses '
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: [{
            categories: mes,

            title: {
                text: 'Meses',
                className: 'highcharts-color-1',
                crosshair: true
            },

        }],
        yAxis: {
            title: {
                text: 'Horas'
            }
        },
        credits: {
            enabled: false,
        },
        colors: [
            '#CD201A','#11B15B','#0069AA','#BABDB6FF'
        ],
        series: [
            {
                type: 'column',
                name: 'Proyecto Facturable',
                data:fact,
                dataLabels: {
                    enabled: true,
                    // rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }

            },
            {
                type: 'column',
                name: 'General No Facturable',
                data: nofact,
                dataLabels: {
                    enabled: true,
                    // rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }

            } ,
            {
                type: 'column',
                name: 'AND Administrativo',
                data: admin,
                dataLabels: {
                    enabled: true,
                    // rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }

            } ,
            {
                type: 'column',
                name: 'Proyectos internos',
                data: interna,
                dataLabels: {
                    enabled: true,
                    // rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '10px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }

            }
        ],
    });
}