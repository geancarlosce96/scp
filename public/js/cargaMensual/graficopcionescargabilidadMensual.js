function graficomensual(carea,fecha_final) {
    $("#tabla-show").hide();
    $.ajax({
        url: 'graficomensual',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_final': fecha_final,
        },
    })
        .done(function(data) {
            graficamensual(data);
            $("#tabla-show").show();
            // console.log("graficasemana",data);
            $("#horas_facturables").html(data[1]);
            $("#horas_no_facturables").html(data[2]);
            $("#horas_admin").html(data[3]);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
}

function grafico3meses(carea,fecha_ini,fecha_final) {
    $.ajax({
        url: 'grafico3meses',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_ini': fecha_ini,
            'fecha_final': fecha_final
        },
    })
        .done(function(data) {
            grafico3meses_chart(data);
            // console.log("grafico3semanas",data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
}

function graficomensualporusuario(carea,fecha_final) {
    $("#tabla-show2").hide();
    $.ajax({
        url: 'graficomensualporusuario',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_final': fecha_final,
        },
    })
        .done(function(data) {
            graficomensualporusuario_chart(data);
            $("#tabla-show2").show();

            $(".reset").remove();

            var user = [];
            var fact = [];
            var nofact = [];
            var admin = [];
            // var carga = [];
            var cabecera='<th class="reset"></th>';
            var acumulado_ff='';
            var acumulado_nf='';
            var acumulado_adm='';
            //var carga_u='';
            for(var x = 0; x < data.length; x++)

            {
                user[x] = data[x].abreviatura;
                fact[x] = data[x].sumuser_fac;
                nofact[x] =data[x].sumuser_nofac;
                admin[x] = data[x].sumuser_admin;
                // carga[u] = data[1][u].cargabilidad;


                cabecera +=  '<th class="reset">'+user[x]+'</th>';
                acumulado_ff +=  '<th class="reset">'+fact[x]+'</th>';
                acumulado_nf +=  '<th class="reset">'+nofact[x]+'</th>';
                acumulado_adm +=  '<th class="reset">'+admin[x]+'</th>';
                // carga_u +=  '<th class="reset">'+carga[u]+'</th>';
            }
            $("#abreviatura").append(cabecera);
            $("#ff").append(acumulado_ff);
            $("#nf").append(acumulado_nf);
            $("#adm").append(acumulado_adm);
            //$("#carga").append(carga_u);

            // console.log("graficonofacturable",data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
}

function graficomensualnofacturable(carea,fecha_final) {
    $.ajax({
        url: 'graficomensualnofacturable',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_final': fecha_final,
        },
    })
        .done(function(data) {
            graficoMensualnofacturable_chart(data);
            // console.log("graficonofacturable",data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
}

function graficomensualadmin(carea,fecha_final) {
    $.ajax({
        url: 'graficomensualadmin',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_final': fecha_final,
        },
    })
        .done(function(data) {
            graficomensualadmin_chart(data);
            // console.log("graficonofacturable",data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });
}

function graficamensualinternos(carea,fecha_final) {
    $.ajax({
        url: 'graficomensualinternos',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_final': fecha_final,
        },
    })
        .done(function(data) {
            // console.log("proyectosinternos",data);
            graficomensualinternos_chart(data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

}
function graficamensualprodCargabilidad(carea,fecha_final) {
    $.ajax({
        url: 'graficomensualproductividadCargabilidad',
        type: 'GET',
        // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
        data: {
            'careas': carea,
            'fecha_final': fecha_final,
        },
    })
        .done(function(data) {
            // console.log("proyectosinternos",data);
            graficamensualprodCargabilidad_chart(data);
        })
        .fail(function() {
            console.log("error");
        })
        .always(function() {
            console.log("complete");
        });

}



