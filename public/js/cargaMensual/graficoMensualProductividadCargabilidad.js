function graficamensualprodCargabilidad_chart (data) {

     var user = [];
     var mes = [];
     var prod = [];
     var carga = [];

     for(var x = 0; x < data.length; x++)
     {

          user[x] = data[x].abreviatura;
          mes[x] = data[x].mes;
          prod[x] = data[x].productividad_user;
          carga[x] = data[x].cargabilidad_user;

     }

    Highcharts.chart('carga-mensual-cargabilidad__productividad', {
        chart: {
            text: 'Combination chart'
        },
        title: {
            text: 'HH del '+$("#careas option:selected").text()+' mes '+mes[0]
        },
        subtitle: {
            // text: 'Semana '
        },
        xAxis: [{
            title: {
                text: '',
                className: 'highcharts-color-1',
            },
            categories: user,
        }],
        yAxis: {
            title: {
                text: 'Porcentaje'
            }
        },
        credits: {
            enabled: false,
        },
        colors: [
            '#f79646','#11B15B','#CD201A','#BABDB6FF'
        ],
        series: [
            {
                type: 'column',
                name: 'Productividad',
                data: prod,
                tooltip: {
                    headerFormat: '<em>Mes : '+mes[0]+'</em><br/>'
                },
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}%', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            {
                type: 'column',
                name: 'Cargabilidad',
                data: carga,
                tooltip: {
                    headerFormat: '<em>Mes : '+mes[0]+'</em><br/>'
                },
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#000000',
                    align: 'center',
                    format: '{point.y:.2f}%', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '12px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }
        ],
    });

}