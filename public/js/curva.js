$(document).ready(function(){

    /*==============================
    =            Chosen            =
    ==============================*/
        $('.select-box').chosen(
        {
            allow_single_deselect: true,width:"100%"
        });

      $('.area-chosen').chosen(
        {
            allow_single_deselect: true,width:"100%"
        });
    
    /*=====  End of Chosen  ======*/
    

/*====================================================================
=            Combo por area enviando el proyecto indicado            =
====================================================================*/
          $('#fdesde').datepicker({
              format: "dd/mm/yyyy",
              language: "es",
              calendarWeeks: true,
              todayHighlight: true,
              // startDate: finicio1,
              // endDate: fcierre1,
              autoclose: true,
              });

          $('#fhasta').datepicker({
              format: "dd/mm/yyyy",
              language: "es",
              calendarWeeks: true,
              todayHighlight: true,
              // startDate: finicio1,
              // endDate: fcierre1,
              autoclose: true,
              });

  $('#proyectos').change(function(){

    var cproyeco = $("#proyectos option:selected").val();

    // var finicio = '';
    // var fcierre = '';

    finicio = $("#proyectos option:selected").data('finicio');
    fcierre = $("#proyectos option:selected").data('fcierre');


    console.log(finicio,fcierre);

// $('#fdesde').datepicker('update','');
// $('#fhasta').datepicker('update','');
             $('#fdesde').datepicker({
              format: "dd/mm/yyyy",
              language: "es",
              calendarWeeks: true,
              todayHighlight: true,
              startDate: finicio,
              endDate: fcierre,
              autoclose: true,
            }).datepicker("update",finicio);

             $('#fhasta').datepicker({
              format: "dd/mm/yyyy",
              language: "es",
              calendarWeeks: true,
              todayHighlight: true,
              startDate: finicio,
              endDate: fcierre,
              autoclose: true,
            }).datepicker("update",fcierre);

    /*getareas*/
      $.ajax({
        type:'GET',
        url: 'getAreas/'+cproyeco,

        success: function (data) {

          console.log('getAreas',$("#proyectos option:selected").val());

          $('#cbo_areas').find('option').remove();
        //  $('#tablaactiv').html(data)
        if($("#lider_or_gerente").val() == 'gerente'){

            $('#cbo_areas').append($('<option></option>').attr('value','Nuevo').text('Todos'));
            $('#cbo_areas').trigger("chosen:updated");
        
        }

          for (var i = 0; i < data.length; i++) {
            $('#cbo_areas').append($('<option></option>').attr('value',data[i].disciplina).text(data[i].area));
              $('#cbo_areas').trigger("chosen:updated");

          };



          // obtenerfechas(data[0].cproyecto);
          // console.log("data",fechas);
    },
    error: function(data){

    

    }

  });

      });

});

      /*getfehca*/
function obtenerfechas(id){


 $.ajax({
          type:'GET',
          url: 'getfecha/'+id,

          success: function (data) {

            var finicio = '';
            var fcierre = '';
            
            // var fechas = {};
            
            var finicio = data[0].finicio;
            var fcierre = data[0].fcierre;

            console.log("fechas",finicio,fcierre);

          
            //var conver_finicio1 =   moment(finicio)
            moment.locale('ES_es_Es');
            
            var conver_finicio =   moment(finicio).format("DD/MM/YYYY");
            var conver_fcierre=   moment(fcierre).format("DD/MM/YYYY");

            var fechas = [conver_finicio,conver_fcierre];

            console.log("fechas array,",fechas);
          
            // fechas.push(conver_finicio,conver_fcierre);
            // fechas.push();

            // var conver_finicio =   moment(finicio);
            // var conver_fcierre=   moment(fcierre);

            console.log("fecha INICIO",conver_finicio,"fecha CIERRE",conver_fcierre);
           
            if (data[0].finicio != null) {

              $('#fdesde').datepicker({
              format: "dd/mm/yyyy",
              language: "es",
              calendarWeeks: true,
              todayHighlight: true,
              startDate: conver_finicio,
              endDate: conver_fcierre,
              autoclose: true,
              }).datepicker("update", conver_finicio);

            } else {
              $('#fdesde').datepicker('update', '');
            }


          if (data[0].finicio != null) {

              $('#fhasta').datepicker({
              format: "dd/mm/yyyy",
              language: "es",
              calendarWeeks: true,
              todayHighlight: true,
              startDate: conver_finicio,
              endDate: conver_fcierre,
              autoclose: true,
              }).datepicker("update", conver_fcierre);
           
          } else {
            $('#fhasta').datepicker('update', '');
          }

            
          },
          error: function(data){




          }
    
      });

    }    
    

  

/*
function getfecha(){
  
}
*/


/*
$('#proyectos').change(function(){


});*/


/*=====  End of Combo por area enviando el proyecto indicado  ======*/

/*========================================================
=            Listar data dentro de una grilla            =
========================================================*/

  $('#btngenerar').click(function(){
    verCurvaS();


});

 

/*=====  End of Listar data dentro de una grilla  ======*/

  /*=======================================
  =            Rango de Fechas            =
  =======================================*/
  
      

  
  /*=====  End of Rango de Fechas  ======*/





      



   function verCurvaS(){
            if ($("#fhasta").val()=='' || $("#fdesde").val()==''){
                alert('Especifique rango de fechas');
                return;
            }
            else{
                var fechai=$("#fdesde").val();
                var fechaf=$("#fhasta").val();
                var fi_string=fechai.substr(3,2)+'/'+fechai.substr(0,2)+'/'+fechai.substr(6,4);
                var ff_string=fechaf.substr(3,2)+'/'+fechaf.substr(0,2)+'/'+fechaf.substr(6,4);
                var fec_des1= new Date(fi_string);
                var fec_has2= new Date(ff_string);
                var fec_has= new Date(fi_string);
                var fec_des= new Date(ff_string);
                var anio_has=fec_has.getFullYear(); 
                var anio_des=fec_des.getFullYear();

                var difM = fec_has2 - fec_des1; // diferencia en milisegundos
                var difD = ((difM / (1000 * 60 * 60 * 24)) + 1); // diferencia en dias

              
                if(fec_has>fec_des){
                    alert('Verifique que la fecha inicio sea menor a la fecha final');
                    return;
                }

                /*if(difD>=35){
                    alert('Seleccione un rango de fechas menor a 5 semanas');
                    return;
                }*/
    
            }  

             $("#div_carga").show(); 

            var obj = {cproyecto: $('#proyectos').val(), disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()}

            $('#curva-s').empty();
            $('#curvarate-s').empty();
            $('#curva-s-indicadores').empty();
            $('#curva-s-porcentaje').empty();

            $.ajax({
                  type:'GET',
                  url: 'curva_s_array/'+ $('#proyectos').val(),
                  data: obj,
                  success: function (data) {

                    //$('#prueba').html(data);
                    $("#div_carga").hide();  
                     console.log(data);
                      chart_curva(data);
                       chart_curvas_costos(data);
                      chart_indicadores(data);
                      chart_curva_porcentaje(data);
                   // $('#lista').html(data);

                   
              },
              error: function(data){

              $("#div_carga").hide(); 

              }
            });

  }


/*=========================================================================================*/


