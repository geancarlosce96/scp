$(document).ready(function() {
	obtenerHoras();	
});

function obtenerHoras()
{
	var cpropuesta = $("#cpropuesta").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/ejecutar/obtenerHorasHH";

	$.get(url, {})
  		.done(function( data ) {
    		mostrarHoras(data);
  	});
}

function mostrarHoras(lista)
{
	for (var i = 0; i < lista.length; i++) {

		if(lista[i].disciplina_id > 0 && lista[i].profesion_id != null){
			var identificacion = lista[i].profesion_id + "_" + lista[i].disciplina_id;
			$("#" + identificacion).text(lista[i].valor);
			sumarTotal(lista[i].profesion_id, lista[i].valor);	
		}
	}
}

function sumarTotal(profesion_id, valor)
{
	if (valor == null)
		return;
	
	var total = parseInt($("#" + profesion_id + "_total").text());
	total += parseInt(valor);
	$("#" + profesion_id + "_total").text(total);
}
