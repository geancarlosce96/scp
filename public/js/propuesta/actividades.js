$(document).ready(function() {
	obtenerActividadesTotales();
	obtenerActividadesActuales();

	$("#btn-agregar").click(function(){
		agregarSeleccion();
	});

	$("#btn-limpiar").click(function(){
		limpiarSeleccion();
		mostrarArbol([]);
	});

	$("#btn-guardar").click(function(){
		guardar();
	});

	$("#s").submit(function(e) {
		e.preventDefault();
		$("#arbolTotal").jstree(true).search($("#q").val());
	});

	$('#arbolTotal').on("changed.jstree", function (e, data) {
		//console.log("seleccionados", data.instance.get_selected());
		eSeleccionadosID = data.instance.get_selected(); //data.instance.get_selected(true); //objeto completo
	});

});

var arbolTotal = [];
var arbolTotalActual = [];
var eSeleccionados = [];
var eSeleccionadosID = [];

//ajax

function obtenerActividadesTotales() //temp*
{
	console.log("obtenerActividades");
	var url = BASE_URL + "/propuesta/ejecutar/obtenerActividades";

	$.get(url, {})
  		.done(function( data ) {
    		arbolTotal = data;
    		mostrarArbolTotal(data)
  	});
}

function obtenerActividadesActuales()
{
	var cpropuesta = $("#cpropuesta").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/ejecutar/obtenerActividadesActuales";

	$.get(url, {})
  		.done(function( data ) {    		
    		arbolTotalActual = data;
    		mostrarArbol(data);
  	});
}

function guardar()
{
	console.log("guardar");
	var elementos = $('#arbol').jstree().get_json();
	var cpropuesta = $("#cpropuesta").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/ejecutar/actividadesGuardar";	
	var objRequest = {};
	objRequest.elementos = elementos;	
	objRequest._token = $('input[name="_token"]').first().val();

	$.post(url, objRequest)
  		.done(function( data ) {
    		window.location.href = BASE_URL + "/propuesta/" + cpropuesta + "/ejecutar/horas";
  	});
}

//functions

function limpiarSeleccion()
{
	console.log("limpiarSeleccion");
	$("#arbol").remove();
	$("#divArbol").append("<div id='arbol'></div>");
}

//tree

function mostrarArbolTotal(dataWithFormat)
{
	$('#arbolTotal').jstree({
		"plugins" : [
			    "checkbox",  
			    "wholerow",
			    "dnd", 
			    "state",
			    "contextmenu",
			    "search"
			],
		'core' : {
			"check_callback" : true, 
			'data' : dataWithFormat
		}
	});
}

function mostrarArbol(dataWithFormat)
{
	limpiarSeleccion();
	$('#arbol').jstree({
		"plugins" : [
			    "checkbox",  
			    "wholerow",
			    "dnd", 
			    "state",
			    "contextmenu",
			    "search"
			],
		'core' : {
			"check_callback" : true, 
			'data' : dataWithFormat
		}
	});
}

function agregarSeleccion()
{
	var arbolActual = $('#arbol').jstree().get_json();
	var arbolTotal = $('#arbolTotal').jstree().get_json();

	for (var i = 0; i < eSeleccionadosID.length; i++) {
		var elemento = obtenerElementoPorIDenArbolTotal(eSeleccionadosID[i]);
		if(elemento.parent != "#")
		{
			var pos = posPadre(arbolActual, elemento.parent);
			if(pos != -1){					
				agregarHijoAPadre(arbolActual[pos], elemento);
			}else{				
				var padre = obtenerElementoPorIDenArbolTotal(elemento.parent);
				padre = {id: padre.id, text: padre.text, icon: 'glyphicon glyphicon-pushpin'};
				agregarHijoAPadre(padre, elemento);
				arbolActual.push(padre);
			}			
		}
		else{			
			arbolActual.push({id: elemento.id, text: elemento.text, icon: 'glyphicon glyphicon-pushpin'});
		}		
	}

	mostrarArbol(arbolActual);
	
	$('#arbolTotal').jstree('deselect_all');
	setTimeout(function(){ $('#arbol').jstree('deselect_all'); }, 1000);
}

function posPadre(lista, id)
{
	//posicion del elemento padre en la lista
	for (var i = 0; i < lista.length; i++) {
		if(lista[i].id == id)
			return i;
	}

	return -1;
}

function agregarHijoAPadre(padre, hijo)
{
	if(padre.children == null)
		padre.children = [];

	padre.children.push({id: hijo.id, text: hijo.text, icon: 'glyphicon glyphicon-pushpin'});
}

function obtenerElementoPorIDenArbolTotal(id)
{
	return $('#arbolTotal').jstree(true).get_node(id).original;
}
