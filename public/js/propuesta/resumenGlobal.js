$(document).ready(function() {
	
	calcular();

	$(".parcial, .porcentaje").change(function(){
		calcular();
	});

});

// fun

function calcular()
{
	console.log("calcular");

	$.each( $(".actividad"), function( key, objTemp ) {
		var horas = parseFloat($(objTemp).find('.horas').val());
		var parcial = parseFloat($(objTemp).find('.parcial').val());
		var porcentaje = parseFloat($(objTemp).find('.porcentaje').val());
		var subtotal = parcial * (1 + porcentaje);
		var total = horas + subtotal;
		$(objTemp).find('.total').val(total);
	});

}
