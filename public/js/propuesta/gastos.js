$(document).ready(function() {
	obtenerGastos();

	$("#btn-guardar").click(function(){
		guardarGastos(this);
	});

	$("#btn-agregarItem").click(function(){
		modalItem();
	});

	$("#btn-registrarItem").click(function(){
		registrarItem();
	});
});

//ajax 

function obtenerGastos()
{
	console.log("obtenerActividades");
	var cpropuesta = $("#cpropuesta").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/ejecutar/obtenerGastos";

	$.get(url, {})
  		.done(function( data ) {
    		console.log(data);
    		$("#tbLista > tbody").empty();
    		mostrar(data.lista)
    		cantidadElementos = data.lista.length;
    		if(data.estado == 0)
    			$("#divInfoNuevo").show();
    		eventos();
  	});
}

function guardarGastos()
{
	console.log("obtenerActividades");
	var cpropuesta = $("#cpropuesta").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/ejecutar/gastosGuardar";
	var objRequest = {};
	objRequest.elementos = obtenerElementos();
	objRequest._token = $('input[name="_token"]').first().val();

	$.post(url, objRequest)
  		.done(function( data ) {
    		window.location.href = BASE_URL + "/propuesta/" + cpropuesta + "/ejecutar/gastosLaboratorios";
  	});
}

//func

var nivel = 0;
var subnivel = 0;
var cantidadElementos = 0;

function mostrar(lista)
{
	for (var i = 0; i < lista.length; i++) {
		
		if(lista[i].cconcepto_parent == null){
			subnivel=0;
			nivel++;
		}

		agregarFila(lista, i, nivel, subnivel)

		subnivel++;
	}
}

function retirarFila(obj)
{
	$(obj).parent().parent().remove();
}

function agregarFila(lista, i)
{
	var row = "";
			row = "<tr class='gasto' data-gasto_id='"+ lista[i].cconcepto +"'>";
			row += "<td>"+ (i+1) +"</td>";
			row += "<td>"+ nivel + "." + subnivel + "</td>";
			row += "<td>"+ lista[i].descripcion + "</td>";
			row += "<td class='unidad' data-unidad='"+ lista[i].unidad +"'>"+ lista[i].unidad + "</td>";
			
			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><input type='text' class='cantidad' value='"+ lista[i].cantidad +"'></td>";

			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><input type='text' class='costounitario' value='"+ lista[i].costou +"'></td>";
			
			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><input type='text' class='subtotal' readonly value='"+ lista[i].subtotal +"'></td>";

			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><input type='text' class='comentario' value='"+ lista[i].comentario +"'></td>";

			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><button class='btn-eliminar'>x</button></td>";

			row += "</tr>";
			$("#tbLista > tbody").append(row);
}

function obtenerElementos()
{
	var elementos = [];

	$.each( $(".gasto"), function( key, objTemp ) {
  		var elemento = {};
  		elemento.concepto = $(objTemp).data("gasto_id");
  		elemento.unidad = $(objTemp).find(".unidad").data("unidad");
  		elemento.cantidad = $(objTemp).find(".cantidad").val();
  		elemento.costounitario = $(objTemp).find(".costounitario").val();
  		elemento.comentario = $(objTemp).find(".comentario").val();  		

  		elementos.push(elemento);
	});

	return elementos;
}

function calcularSubtotal(obj)
{
	console.log("calcularSubtotal");
	var padre = $(obj).parent().parent();
	var cantidad = $(padre).find('.cantidad').val();
	var costounitario = $(padre).find('.costounitario').val();
	var subtotal = parseFloat(cantidad) * parseFloat(costounitario);
	$(padre).find('.subtotal').val(subtotal);
}

function modalItem()
{
	$("#modalAgregarItem").modal("show");
}

function registrarItem()
{
	var url = BASE_URL + "/item/gasto/registrar";
	var objRequest = {};	
	objRequest.descripcion = $("#txtdescripcion").val();
	objRequest.unidad = $("#txtunidad").val();
	objRequest.padre = $("#cbpadre").val();
	objRequest._token = $('input[name="_token"]').first().val();

	$.post(url, objRequest)
  		.done(function( data ) {
    		$("#modalAgregarItem").modal("hide");
    		agregarItem(data);
  	});
}

function agregarItem(item)
{
	nivel++;
	subnivel = 0;
	var lista = [];
	item.cconcepto_parent = 1; //temp
	lista.push(item);
	agregarFila(lista, cantidadElementos); //temp
}

//eventos

function eventos()
{
	$(".btn-eliminar").click(function(){
		retirarFila(this);
	});

	$(".cantidad").change(function(){
		calcularSubtotal(this);
	});

	$(".costounitario").change(function(){
		calcularSubtotal(this);
	});
}
