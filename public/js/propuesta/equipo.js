$(document).ready(function() {

	addchosen('gerentes');
	addchosen('responsable');
	addchosen('revisor');
	addchosen('coordinador');
	addchosen('responsables');
	addchosen('revisor_propuesta');
	addchosen('revisor_proyecto');

	$("#btn-guardar").click(function(e) {
  		e.preventDefault();
  		guardar();
	});

});

function guardar(){
	console.log("guardar");
	var objRequest = $('#form').serialize();
	var url = $('#form').attr("action");
	var cpropuesta = $("#cpropuesta").val();

	$.post(url, objRequest)
  		.done(function( data ) {
    		console.log("data", data);
    		window.location.href = BASE_URL + "/propuesta/" + cpropuesta + "/ejecutar/disciplinas";
  	});
}
