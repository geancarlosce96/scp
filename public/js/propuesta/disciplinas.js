$(document).ready(function() {
    
	$( ".disciplina" ).dblclick(function(e) {
  		mover(this);
	});

	$("#btn-guardar").click(function(e) {
  		e.preventDefault();
  		guardar();
	});

});

var disciplinasSeleccionadas = [];

function guardar()
{
	console.log("guardar");
	var cpropuesta = $("#cpropuesta").val();
	var url = $("#url").val();
	var objRequest = {};
	
	objRequest._token = $('input[name="_token"]').first().val();
	objRequest.cpropuesta = cpropuesta;
	obtenerDisciplinasSeleccionadas();
	objRequest.disciplinas = disciplinasSeleccionadas;

	$.post(url, objRequest)
  		.done(function( data ) {
    		console.log("data", data);
    		window.location.href = BASE_URL + "/propuesta/" + cpropuesta + "/ejecutar/tags";
  	});
}

function mover(obj)
{
	var parent = $(obj).parent();

	if( $(parent).attr("id") == "ListaDisciplinas" )
		$("#ListaSeleccionadas").append(obj);
	else
		$("#ListaDisciplinas").append(obj);
}

function obtenerDisciplinasSeleccionadas()
{
	var disciplinas = $("#ListaSeleccionadas > .disciplina");
	disciplinasSeleccionadas = [];

	for (var i = 0; i < disciplinas.length; i++) {
		var id = $(disciplinas[i]).data("id");
		disciplinasSeleccionadas.push(id);
	}
}
