$(document).ready(function() {
    obtenerTablaHoras();
});

var columnas_propiedad = []; // cada  elemento { type: 'text', readOnly:true||false }
var tabla = null;

//ajax
function obtenerTablaHoras()
{
    var cpropuesta = $("#cpropuesta").val();
    var url = BASE_URL + "/propuesta/"+ cpropuesta +"/tabla/obtenerTablaHoras";

    $.get(url, {})
        .done(function( data ) {            
            generarTabla(data.tabla);
            tabla = data.tabla;
    });
}

//func
function generarTabla(tabla)
{
    columnas = mostrarColumnas(tabla.columnas);
    filas = obtenerFilas(tabla.filas, tabla.columnas, tabla.celdas);
    mostrarTabla(columnas, filas);
}

function mostrarColumnas(lista)
{
    var columnas = [];    

    for (var i = 0; i < lista.length; i++) {
        columnas.push(lista[i].descripcion);
        columnas_propiedad.push({type: 'text', readOnly: (lista[i].editable==0) ? true : false })
    }

    return columnas;
}

function obtenerFilas(filas, columnas, celdas)
{
    var lista_filas = [];

    for (var f = 0; f < filas.length; f++) {        
        var fila = [];
        fila.push(filas[f].descripcion);

        for (var c = 1; c < columnas.length; c++)
            fila.push(obtenerValorCelda(celdas, filas[f].id, columnas[c].id) );

        lista_filas.push(fila);
    }

    return lista_filas;
}

function obtenerValorCelda(celdas, fila_codigo, columna_codigo)
{
    for (var i = 0; i < celdas.length; i++) {
        if(celdas[i].fila_codigo == fila_codigo && celdas[i].columna_codigo == columna_codigo)
            return celdas[i].valor;
    }

    return "";
}

function mostrarTabla(columnas, filas){
    $('#my').jexcel({
        data: filas,
        colHeaders: columnas,
        colWidths: [ 200, 100],
        columns: columnas_propiedad,
        minDimensions: [0, 15],
        onchange:update,
    });
}

function update(obj, cel, valor)
{
    //evento de la tabla
    var id = $(cel).prop('id').split('-');
    var indiceColumna = id[0];
    var indiceFila = id[1];
    var idFila = obtenerIdSegunArray(tabla.filas, indiceFila);
    var idColumna = obtenerIdSegunArray(tabla.columnas, indiceColumna);
    grabarCelda(idColumna, idFila, valor)
}

function obtenerIdSegunArray(lista, indice)
{
    return lista[indice].id;
}

function agregarValorACeldas(celdas, celda)
{
    var encontrado = false;
    
    for (var i = 0; i < celdas.length; i++) {
        if(celdas[i].columna_codigo == celda.columna_codigo && celdas[i].fila_codigo == celda.fila_codigo){
            encontrado = true;
            celdas[i].valor = celda.valor;            
        }
    }

    if(!encontrado)
        tabla.celdas.push(celda);
}

function grabarCelda(columna, fila, valor)
{
    var cpropuesta = $("#cpropuesta").val();
    var url = BASE_URL + "/propuesta/"+ cpropuesta +"/tabla/grabarCelda";
    objRequest = {};
    objRequest._token = $('input[name="_token"]').first().val();
    objRequest.celda = {columna, fila, valor};

    $.post(url, objRequest)
        .done(function( celda ) {
            agregarValorACeldas(tabla.celdas, celda)
    });
}
