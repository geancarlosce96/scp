$(document).ready(function() {
	obtenerGastos();

	$("#btn-guardar").click(function(){
		guardarGastos(this);
	});
});

//ajax 

function obtenerGastos()
{
	console.log("obtenerActividades");
	var cpropuesta = $("#cpropuesta").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/ejecutar/obtenerGastosLab";

	$.get(url, {})
  		.done(function( data ) {
    		console.log(data);
    		$("#tbLista > tbody").empty();
    		mostrar(data.lista)
    		if(data.estado == 0)
    			$("#divInfoNuevo").show();
    		eventos();
  	});
}

function guardarGastos()
{
	console.log("obtenerActividades");
	var cpropuesta = $("#cpropuesta").val();
	var url = BASE_URL + "/propuesta/"+ cpropuesta +"/ejecutar/gastosLabGuardar";
	var objRequest = {};
	objRequest.elementos = obtenerElementos();
	objRequest._token = $('input[name="_token"]').first().val();

	$.post(url, objRequest)
  		.done(function( data ) {
    		window.location.href = BASE_URL + "/propuesta/" + cpropuesta + "/ejecutar/resumenGlobal";
  	});
}

//func

function mostrar(lista)
{
	var nivel = 0;
	var subnivel = 0;

	for (var i = 0; i < lista.length; i++) {
		
		if(lista[i].cconcepto_parent == null){
			subnivel=0;
			nivel++;
		}

			var row = "";
			row = "<tr class='gasto' data-gasto_id='"+ lista[i].cconcepto +"'>";
			row += "<td>"+ (i+1) +"</td>";
			row += "<td>"+ nivel + "." + subnivel + "</td>";
			row += "<td>"+ lista[i].descripcion + "</td>";
			row += "<td class='unidad' data-unidad='"+ lista[i].unidad +"'>"+ lista[i].unidad + "</td>";
			
			if(lista[i].cconcepto_parent == null){
				row += "<td></td>";
				row += "<td></td>";
			}
			else{
				row += "<td><input type='text' class='cantcimentacion' value='"+ lista[i].cantcimentacion +"'></td>";
				row += "<td><input type='text' class='cantcanteras' value='"+ lista[i].cantcanteras +"'></td>";
			}

			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><input type='text' class='costounitario' value='"+ lista[i].preciou +"'></td>";

			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><input type='text' class='subtotal' readonly value='"+ lista[i].subtotal +"'></td>";

			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><input type='text' class='comentario' value='"+ lista[i].comentario +"'></td>";

			if(lista[i].cconcepto_parent == null)
				row += "<td></td>";
			else
				row += "<td><button class='btn-eliminar'>x</button></td>";
			row += "</tr>";
			$("#tbLista > tbody").append(row);

		subnivel++;
	}
}

function retirarFila(obj)
{
	$(obj).parent().parent().remove();
}

function obtenerElementos()
{
	var elementos = [];

	$.each( $(".gasto"), function( key, objTemp ) {  		
  		var elemento = {};
  		elemento.concepto = $(objTemp).data("gasto_id");
  		elemento.unidad = $(objTemp).find(".unidad").data("unidad");
  		elemento.cantcanteras = $(objTemp).find('.cantcanteras').val();	
		elemento.cantcimentacion = $(objTemp).find('.cantcimentacion').val();
  		elemento.costounitario = $(objTemp).find(".costounitario").val();
  		elemento.comentario = $(objTemp).find(".comentario").val();  		

  		elementos.push(elemento);
	});

	return elementos;
}

function calcularSubtotal(obj)
{
	console.log("calcularSubtotal");
	var padre = $(obj).parent().parent();
	var cantcimentacion = $(padre).find('.cantcimentacion').val();
	var cantcanteras = $(padre).find('.cantcanteras').val();	
	var costounitario = $(padre).find('.costounitario').val();
	var subtotal = ( parseFloat(cantcimentacion) + parseFloat(cantcanteras) )  * parseFloat(costounitario);
	$(padre).find('.subtotal').val(subtotal);
}

//eventos

function eventos()
{
	$(".btn-eliminar").click(function(){
		retirarFila(this);
	});

	$(".cantcimentacion").change(function(){
		calcularSubtotal(this);
	});

	$(".cantcanteras").change(function(){
		calcularSubtotal(this);
	});

	$(".costounitario").change(function(){
		calcularSubtotal(this);
	});
}
