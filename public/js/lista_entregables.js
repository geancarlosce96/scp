
function verActividadesProyecto(idEntregable){
	//swal(idEntregable);

	$.ajax({

		type:"GET",
		url:'verActividadesProyecto',
		data:{
			"cproyecto" : $("#cproyecto").val(),
			"centregableproyecto": idEntregable

		},

		beforeSend: function () {                   
			$('#div_carga').show();             
		},
		success: function(data){

			$('#div_carga').hide(); 
			var tree = [];
			res = "tree = [" + data + "]";
			eval(res);

			$('#treeActividadesTodos').highCheckTree({
				data: tree,
				onCheck: function (node){
					selected_actividad_todos.push(node.attr('rel'));
				},
				onUnCheck: function(node){
					var idx = $.inArray(node.attr('rel'),selected_actividad_todos);
					if(idx != -1){
						selected_actividad_todos.splice(idx,1);
					}
				}
			}); 

			$('#modalAsignarAct').modal({
				backdrop:"static"
			});                 
               //$("#divPrySel").html(data);
           }
       });

}

function asignarActividadesEntregables(){

	$.ajax({
		url: 'asignarActividadesEntregables',
		type:'POST',
		data: {"_token": "{{ csrf_token() }}",
		selected_actividad_todos: selected_actividad_todos,


	},
	beforeSend: function () {
		$('#div_carga').show(); 
	},              
	success : function (data){

		$("#divLEntregable").html(data);
		$('#div_carga').hide(); 
	},
});

}

function activarEnt(id,act){
      //swal('1');
      $.ajax({
      	url: 'activarEntregable/'+id+'/'+act,
      	type:'GET',
      	beforeSend: function () {
      		$('#div_carga').show(); 
      	},              
      	success : function (data){

      		$("#tableEntdivLEntregable").html(data);
      		$('#div_carga').hide(); 
      	},
      });
    //});
}

function verModalfechas(formulario){

	limpiarfechas();
	
	listarTodasLasFechas();

	$("#formulario").val(formulario);


	$('#modalfechas').modal({
		backdrop:"static"
	});


}

function verModalresumenLE(){
	$('#modalresumenLE').modal({
		backdrop:"static"
	});
}


function verfechasEntregable(id,desc_ent,formulario){
	limpiarfechas();
	$("#formulario").val(formulario);
	verHorasAnteriores(id);
	listaFechasNoAgregadas(id);

	$.ajax({
		url: 'viewFechasEntregablesPry/'+id,
		type:'GET',
		beforeSend: function () {
			$('#div_carga').show(); 


		},              
		success : function (data){              
			$('#div_carga').hide();     
			$("#divFechaEntregable").html(data);
			$('#centregableproy_fecha').val(id);
			//$('#nomb_ent').val(desc_ent);
			$('#nomb_ent').html(desc_ent);
			$('#modalfechas').modal('show');


		},
	});
}


function saveFechas(){

	$("#fechaform").val($("#fecha").val());
	$("#tipofechaform").val($("#tipofecha").val());

	var url = '';
	var obj = '';

	if($("#formulario").val()=='entregable'){

		url ='grabarFechasEntregablePry';
		obj=$("#frmfechasentproy").serialize();
	}

	if($("#formulario").val()=='general'){

		var confirmarFecha =confirm('¿Desea que todas las fechas de los entregables seleccionados se actualicen?');

		if (confirmarFecha){
			url ='grabarFechasEntregablePrySelec';
			//obj=$("#frmproyectoentregables").serialize();
			obj=$("#frmproyectoedt").serialize();
			$("#desc_fecha").val($("#tipofecha option:selected").text());
		}
		else{
			
			return;
		}
	}


	$.ajax({
		url: url,
		type:'POST',
		data: obj,
		beforeSend: function () {
			$('#div_carga').show(); 
		},              
		success : function (data){
			$('#div_carga').hide(); 
		      //$("#divFechaEntregable").html(data);

		      if($("#formulario").val()=='entregable'){

		      	$("#divFechaEntregable").html(data);
		      	verHorasAnteriores($('#centregableproy_fecha').val());
		      	listaFechasNoAgregadas($('#centregableproy_fecha').val());

		      }

		      if($("#formulario").val()=='general'){

		      	verFechasTodos();

		      }
		      
		      $('#fecha').val('');
		      $('#tipofecha').val('');
		      $("#desc_fecha").val('');

		  },
	});
}

function verTodosEntregables(){

	var cproyecto=$('#cproyecto').val();

	$.ajax({
		url: 'verTodosEntregables/'+cproyecto,
		type:'GET',
		beforeSend: function () {
			$('#div_carga').show(); 

		},              
		success : function (data){              
			$('#div_carga').hide();     
			$("#tableEntdivLEntregable").html(data);
		},
	});
}

function verTodosEntregablesEDT(){

	var pryEDT=$('#cpryedt').val();

	$.ajax({
		url: 'verTodosEntregablesPorEDT/'+pryEDT,
		type:'GET',
		beforeSend: function () {
			$('#div_carga').show(); 

		},              
		success : function (data){              
			$('#div_carga').hide();     
			$("#tableEntdivLEntregable").html(data);
			obtenerFechaCabEDT();
		},
	});
}

//$("#btnSaveFechas").on("click", function () {
$("#modalfechas").on("hidden.bs.modal", function () {

	$("#tablaFechasEnt > tbody tr").remove();
	$('#desc_fecha').val('');
	//verTodosEntregables();
	verTodosEntregablesEDT();


});

function verFechasTodos(){

	var row ='';
	row =  "<tr>";  
	row += "<td>"+$("#desc_fecha").val()+"</td>";  
	row += "<td>"+$("#fecha").val()+"</td>"; 
	row += "</tr>";

	$("#tablaFechasEnt > tbody").append(row);
}

$('#fecha').datepicker({
	format: "dd-mm-yy",
	language: "es",
	autoclose: true,
	firstDay: 1,
	calendarWeeks:true,
	// startDate: "<?php echo date('d/m/Y'); ?>",
	todayHighlight: true,
});

function limpiarfechas(){
	$('#fecha').val('');
	$('#tipofecha').val('');
	$('#cproyectoentfecha').val('');
	$("#fechaform").val('');
	$("#tipofechaform").val('');
	$("#formulario").val('');
	$("#fechasAnteriores").val('');
	$("#ultimoTipoFecha").val('');
}

 
 var table = $('#tableEntregablesTodos').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     true,
        lengthChange: false,
        buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ],
        searching: false,
    } );


function verHorasAnteriores(idEnt){
	
	$.ajax({
		url: 'getFechaEntAnteriores/'+idEnt,
		type:'GET',
		//data:$("#frmfechasentproy").serialize(),
		beforeSend: function () {
			$('#div_carga').hide(); 

		},              
		success : function (data){              
			$('#div_carga').hide(); 

			var fechasAnt='';

			for (var i = 0; i < data.length; i++) {

				fechasAnt+=data[i].ctipofechasentregable+','+data[i].fecha+','+data[i].tipofec+','+data[i].cproyectoentregables;

				if(i<data.length-1){
					fechasAnt+=';';
				}				
			}
			$("#fechasAnteriores").val(fechasAnt);
			  
		    //$("#tableEntdivLEntregable").html(data);
		},
	});
}

function listaFechasNoAgregadas(idEnt){
	//var idEnt= $("#centregableproy_fecha").val();

	$.ajax({
		url: 'getFechasNoAgregadas/'+idEnt,
		type:'GET',
		//data:$("#frmfechasentproy").serialize(),
		beforeSend: function () {
			$('#div_carga').show(); 

		},              
		success : function (data){  
		$('#div_carga').hide();
		$('#tipofecha').find('option').remove();

			for (var i = 0; i < data.length; i++) {
			
				$('#tipofecha').append($('<option></option>').attr('value',data[i].ctipofechasentregable).text(data[i].descripcion));            
			}
 			
		},
	});
}

function listarTodasLasFechas(){
	$.ajax({
		url: 'listarTodasLasFechas',
		type:'GET',
		//data:$("#frmfechasentproy").serialize(),
		beforeSend: function () {
			$('#div_carga').show(); 

		},              
		success : function (data){  
		$('#div_carga').hide();
		$('#tipofecha').find('option').remove();

			for (var i = 0; i < data.length; i++) {
			
				$('#tipofecha').append($('<option></option>').attr('value',data[i].ctipofechasentregable).text(data[i].descripcion));            
			}
 			
		},
	});

}



function validarFechaIngresada(){


	var tipofecha=buscarValidacion('tipoFecha',$("#tipofecha").val());
	var fecha=buscarValidacion('fecha',$("#tipofecha").val());
	var tipofechaIngresado=$("#tipofecha").val();
	var Ultimotipofecha=$("#ultimoTipoFecha").val();


	var validarTipoMayor=tipofechaIngresado-Ultimotipofecha;

	if ($("#fecha").val()=="") {
		swal('Ingrese una fecha');
		return;
	}

	if($("#formulario").val()=='general'){
		
		saveFechas();

	}

	else{

		if (validarTipoMayor>1) {

			if (parseInt(Ultimotipofecha)< 5  || parseInt(tipofechaIngresado)>9) {
				swal('Ingrese un tipo de fecha válido');
				return;
				
			}
		}

		if ($("#fechasAnteriores").val()==""){

			if ($("#tipofecha").val()!=1) {
			swal('Ingrese Fecha Ini Diseño');
			return;
			}

		}


		if(tipofecha!='ninguna'){
			var confirmar =confirm('El tipo de fecha '+tipofecha +' ya fue ingresado, desea actualizarlo?');

			if (confirmar){

				if(fecha!='ninguna'){
					swal('Seleccione una fecha posterior a : '+fecha);
					return;
				}
				else{
					saveFechas();
				}
				
			}
			else{
				$('#fecha').val('');
				$('#tipofecha').val('');
				return;
			}
		}

		if(fecha!='ninguna'){
			swal('Seleccione una fecha posterior a : '+fecha);
			return;
		}
		else{
			saveFechas();
		}

	}


}

function buscarValidacion(valor,tipoFecha){


	var tipoIngresado=$("#tipofecha").val();
	var fechaIngresada=$("#fecha").val();
	fechaIngresada=fechaIngresada.substr(3,2)+'/'+fechaIngresada.substr(0,2)+'/'+fechaIngresada.substr(6,2);
	fechaIngresada=new Date(fechaIngresada);

	var fechasAnt=$("#fechasAnteriores").val().split(";");

	if ($("#fechasAnteriores").val()!="") {


		for (var i = 0; i < fechasAnt.length; i++) {

			var fechas=fechasAnt[i].split(",");

			var ctipo=fechas[0];
			var desc=fechas[2];
			var fec=fechas[1];
			fec=fec.substr(3,2)+'/'+fec.substr(0,2)+'/'+fec.substr(8,2);

			var fechaAgregada=new Date(fec);

			if (valor=='tipoFecha'){

				if(tipoIngresado==ctipo){
					return desc;
				}

			}

			if (valor=='fecha'){
				if(fechaAgregada>fechaIngresada){
					return fechas[1];
				}

			}

		}
	}

	return 'ninguna';
}

function listafechasAgregadasTodos(){

	var idEdt=$("#cpryedt").val();

	$.ajax({
		url: 'getFechaEntAnterioresTodos/'+idEdt,
		type:'GET',
		//data:$("#frmfechasentproy").serialize(),
		beforeSend: function () {
			$('#div_carga').hide(); 

		},              
		success : function (data){  
		$('#div_carga').hide();
		
			/*for (var i = 0; i < data.length; i++) {
			
				$('#tipofecha').append($('<option></option>').attr('value',data[i].ctipofechasentregable).text(data[i].descripcion));            
			}*/
 			
		},
	});

}

function verModalNuevoEntregable(nivel){
	if (nivel==0) {
		limpiarFormEntregableNuevo();
	}

	var descripcionModal='';
	if (nivel==0){ descripcionModal='Componente'; }
	if (nivel==1){ descripcionModal='Instalación'; }
	if (nivel==2){ descripcionModal='Entregable'; }

	$('#modalNuevoEntregable').modal('show');  
	$('#tituloNuevoEntregable').html('Agregar '+descripcionModal);  
	$('#nivelNuevo').val(nivel); 
	$('#disciplinaNuevoEnt').val($('#disciplina').val()).trigger('chosen:updated');
	$('#descNivel').html(descripcionModal);
	$('#tiposDocumento').val("").trigger('chosen:updated');
	$('#paquete').val("");
	//$('#tiposDocumento').prop("disabled",true);
	$('#paquete').prop("disabled",true);
	$('#disciplinaNuevoEnt').prop("disabled",true);



}

$('#nuevoEntN0').on('click',function(){

	if ($('#tipoentregable').val()=='') {
		swal('Seleccione si es un Documento, Figura, Mapa o Plano');
		return;
	}
	$('#disciplinaNuevoEnt').prop("disabled",false);

	verModalNuevoEntregable(0);



})


$('#nuevoEntN1').on('click',function(){

	if ($('#entregableN0').val()==null) {
		swal('Seleccione un componente');
		return;
	}
	verModalNuevoEntregable(1);

})

var tipodocumento='';

$('#nuevoEntN2').on('click',function(){

		$('#tiposDocumento').prop("disabled",false);

	if ($('#entregableN1').val()==null) {
		swal('Seleccione una instalación');
		return;
	}

	if($('#tipoentregable').val()=='001'){
	}

	if($('#tipoentregable').val()=='002'){
		$('#tiposDocumento').val('9').trigger('chosen:updated');

		tipodocumento='9';
	}

	if($('#tipoentregable').val()=='004'){
		$('#tiposDocumento').val('22').trigger('chosen:updated');

		tipodocumento='22';
	}

	$('#paquete').prop("disabled",false);
	verModalNuevoEntregable(2);

})



$('#btnGrabarNuevoEnt').on('click',function(){
	grabarEntregableNuevo();
})

function limpiarFormEntregableNuevo(){

	$('#nuevoEntregable').val("");
	$('#disciplinaNuevoEnt').val("").trigger('chosen:updated');
	$('#tiposDocumento').val("").trigger('chosen:updated');
	$('#paquete').val("");

}


function grabarEntregableNuevo(){

  var objNuevoEnt={descripcion: "", ctipoentregable: "", cdisciplina: "", nivel: "", entregable_parent: "", cdocumentoparapry: "", paquete: "", tipo_paquete: ""};    

  objNuevoEnt.descripcion=$('#nuevoEntregable').val();
  objNuevoEnt.ctipoentregable=$('#tipoentregable').val();
  objNuevoEnt.cdisciplina=$('#disciplinaNuevoEnt').val();
  objNuevoEnt.nivel=$('#nivelNuevo').val();

  /*if ($('#nivelNuevo').val()==0) {

  	objNuevoEnt.entregable_parent=null;
  }*/

  if ($('#nivelNuevo').val()==1) {

  	objNuevoEnt.entregable_parent=$('#entregableN0').val();
  }

  if ($('#nivelNuevo').val()==2) {

  	objNuevoEnt.entregable_parent=$('#entregableN1').val();
  }



  objNuevoEnt.cdocumentoparapry=$('#tiposDocumento').val();
  objNuevoEnt.paquete=$('#paquete').val();
  objNuevoEnt.tipo_paquete=$("input[name='tipopaquete']:checked").val();
  objNuevoEnt._token = $('input[name="_token"]').first().val();



  $.ajax({
    url: 'grabarEntregablesNuevo',
    type:'POST',
    data: objNuevoEnt,
    beforeSend: function () {
      $('#div_carga').show(); 
  },              
    success : function (data){

    	if ($('#nivelNuevo').val()==0) {
    		obtenerDisciplina($('#tipoentregable').val(),$('#disciplinaNuevoEnt').val());
		}

		if ($('#nivelNuevo').val()==1) {
			//listado();

			listaEntregableN1($('#entregableN0').val());
		}

		if ($('#nivelNuevo').val()==2) {
			
			buscarEntregableHijo($('#entregableN1').val());
		}

		$('#modalNuevoEntregable').modal('hide');

    },
  });



}





