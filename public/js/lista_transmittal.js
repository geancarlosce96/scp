 
$(document).ready(function(){

 var selected =[];
        
  var table=$("#tProyecto").DataTable({
      "processing":true,
      "serverSide": true,
     //"lengthChange": false,

      "ajax": {
          "url": "listarProyectos",
          "type": "GET",
      },
      "columns":[
          {data : 'cproyecto', name: 'tproyecto.cproyecto'},
          {data : 'codigo', name: 'tproyecto.codigo'},
          {data : 'GteProyecto' , name : 'p.abreviatura'},
          {data : 'cliente' , name : 'tper.nombre'},
          {data : 'uminera' , name : 'tu.nombre'}, 
          {data : 'nombre' , name : 'tproyecto.nombre'},
          {data : 'descripcion' , name : 'te.descripcion'}

      ],
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Sin Resultados",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No existe registros disponibles",
          "infoFiltered": "(filtrado de un _MAX_ total de registros)",
          "search":         "Buscar:",
          "processing":     "Procesando...",
          "paginate": {
              "first":      "Inicio",
              "last":       "Ultimo",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "loadingRecords": "Cargando..."
      }
  });
  $('#tProyecto tbody').on('click', 'tr', function () {
      var id = this.id;
      var index = $.inArray(id, selected);
      table.$('tr.selected').removeClass('selected');
      selected.splice(0,selected.length);
      if ( index === -1 ) {
          selected.push( id );

      }

      $(this).toggleClass('selected');
  } );


  var tableEntTR = $('#tableEntregablesTR').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     false,
       // "order":     [[0,"desc"]],
        lengthChange: true,
        //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
    } );
 
    tableEntTR.buttons().container()
        .appendTo( '#tableEntregablesTR_wrapper .col-sm-6:eq(0)' );

    var tableTR = $('#tablaTR').DataTable( {
    "paging":   true,
    "ordering": true,
    "info":     false,
   // "order":     [[0,"desc"]],
    lengthChange: true,
    //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
    } );
 
    tableTR.buttons().container()
        .appendTo( '#tablaTR_wrapper .col-sm-6:eq(0)' );

   
  $('#searchProyecto').on('hidden.bs.modal', function () {

    var id =0;

    if (selected.length > 0 ){
      id = selected[0];

      var url = BASE_URL + '/listaentregablestr/'+id.substring(4);

      window.location.href = url;

    }

    else
    {
      $('.alert').show();
    }

 
  });

});