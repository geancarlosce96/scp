

{{ csrf_field() }}




<div class="modal fade" id="modalAgregar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color: white">Agregar EDT</h4>
      </div>
      <div class="modal-body">

		<form class="form-horizontal">
		  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Padre: </label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="padre" readonly>
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Codigo: </label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="codigo">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="" class="col-sm-2 control-label">Descripcion: </label>
		    <div class="col-sm-10">
		      <input type="text" class="form-control" id="descripcion">
		    </div>
		  </div>
		</form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="guardarHijo({{isset($proyecto)==1?$proyecto->cproyecto:''}})">Guardar</button>
      </div>
    </div>
  </div>
</div>

