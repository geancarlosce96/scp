

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="modalEDTgrafico">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel" style="color: white">EDT GRÁFICO</h4>
      </div>
      <div class="modal-body">
        
        <div id="chart_div"></div>

        <div id="opciones" style="position: absolute; display: none;">
          <a onclick="agregarHijo({{isset($proyecto)==1?$proyecto->cproyecto:''}})" title="Añadir nivel EDT" class="btn btn-default" style="color: black;font-size: 13px"><i class="fa fa-plus" aria-hidden="true" ></i></a>
          <a onclick="retirarElemento({{isset($proyecto)==1?$proyecto->cproyecto:''}})" title="Eliminar EDT" class="btn btn-default" style="color: red;font-size: 13px"><i class="fa fa-trash" aria-hidden="true" ></i></a>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

