<table class="table table-striped">
                <thead>
                <tr class="clsCabereraTabla">
                  <th>Item</th>
                  <th>Apellidos Nombre</th>
                  <th>Cargo</th>
                  @if(isset($dias))
                  @foreach($dias as $d)
                    <th>{{ $d }}</th>
                  @endforeach
                  @endif
                 </tr> 
                </thead>
                <tbody>
                @if(isset($crono) )
                <?php $i=-1; ?>
                @foreach($crono as $c)
                <?php $i++; ?>
                <tr>
                    <td ><?php echo $i+1; ?>
                    <input type="hidden" name="rooster[{{ $i }}][0]" value="{{ $c['cpersonaempleado'] }}" />
                    <input type="hidden" name="rooster[{{ $i }}][1]" value="{{ $c['tcronogramaconstruccionid'] }}" />
                    <input type="hidden" name="rooster[{{ $i }}][2]" value="" />
                    
                    </td>
                    <td >{{ $c['empleado'] }} </td>
                    <td ></td>                
                        @if(isset($dias))
                            @foreach($dias as $d)
                                <?php $key=str_replace('-','',$d) ?>
                                <td>
                                <input type='hidden' name='rooster[{{ $i }}][3][{{ $key }}][0]' value="{{ (isset($c['deta'][$key]['cpropuestaconstrucciondetafecha'])==1?$c['deta'][$key]['cpropuestaconstrucciondetafecha']:'') }}"  />
                                <input type='hidden' name='rooster[{{ $i }}][3][{{ $key }}][1]' value="{{ (isset($c['deta'][$key]['fecha'])==1?$c['deta'][$key]['fecha']:'') }}"  />
                                <input type='text' name='rooster[{{ $i }}][3][{{ $key }}][3]' value="{{ (isset($c['deta'][$key]['nrohoras'])==1?$c['deta'][$key]['nrohoras']:'') }}"  size="2" maxlength="2" />
                                <select name="rooster[{{ $i }}][3][{{ $key }}][2]">
                                <option value="">&nbsp;</option>
                                  @foreach($leyenda as $ley)
                                    <option value="{{ $ley['cod'] }}"
                                    @if(isset($c['deta'][$key]['codactividad']))
                                        @if($c['deta'][$key]['codactividad']==$ley['cod'] )
                                            selected
                                        @endif
                                    @endif
                                    >{{ $ley['cod'] }}</option>
                                  @endforeach
                                </select>
                                <input type='hidden' name='rooster[{{ $i }}][3][{{ $key }}][4]' value="{{ (isset($c['deta'][$key]['observacion'])==1?$c['deta'][$key]['observacion']:'') }}"  />
                                </td>
                            @endforeach
                        @endif
                    <td></td>                                                              
                </tr>
                @endforeach  
                @endif                                                 
                </tbody>
                
                
</table>