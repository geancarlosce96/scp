<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Propuesta
        <small>Generación de Revisión de Propuesta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuesta</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoPropuesta')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>    
    {!! Form::open(array('url' => 'grabarRevisionPropuesta','method' => 'POST','id' =>'frmpropuesta')) !!}
    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
    {!! Form::hidden('cpropuesta',(isset($propuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta')) !!}
    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">
    	<div class="row">
      
                <div class="col-lg-8 col-xs-10 clsPadding">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
                    {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
                </div>
                <div class="col-lg-2 col-xs-1 clsPadding"><button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#searchPropuesta"><b>...</b></button><button class="btn btn-primary btn-sm" disabled=""><b>+</b></button></div>
                <div class="col-lg-2 col-xs-1 clsPadding"></div>
    </div>
    <div class="row clsPadding2">
    <div class="col-lg-2 col-xs-2">Unidad minera:</div>
    <div class="col-lg-10 col-xs-10">
		  {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}   
    </div>
    </div>
    <div class="row">
    <div class="col-lg-1 col-xs-2">Código:</div>
    <div class="col-lg-4 col-xs-3">
        {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)==1?$propuesta->ccodigo:(isset($codigoProp)==1?$codigoProp:'' ) ),array('class'=>'form-control input-sm','disabled'=>'true')) !!}
    </div>
    <div class="col-lg-1 col-xs-2"></div>
    <div class="col-lg-4 col-xs-3">
		  
    </div>
    <div class="col-lg-2 col-xs-2"></div> 
    </div>    
<div class="row clsPadding">
<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>


<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-3">Nombre de la Propuesta:</div>
    <div class="col-lg-9 col-xs-9">
        {!! Form::text('propuestanombre',(isset($propuesta->nombre)==1?$propuesta->nombre:''),array('class' => 'form-control input-sm', 'placeholder' => 'Nombre de la Propuesta')) !!} 
    </div>
       
</div>
<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-3">Descripción de la propuesta:</div>
    <div class="col-lg-9 col-xs-9">
                        {!! Form::textarea('propuestadescripcion',(isset($propuesta->descripcion)==1?$propuesta->descripcion:''), array('class' => 'form-control', 'rows'=> '3','placeholder' => 'Descripción de la Propuesta')) !!}
    </div> 
</div> 
<div class="row clsPadding2">
    <div class="col-lg-6 col-xs-6">
    <div class="radio">
        @if(isset($formacotiza))
                    @foreach($formacotiza as $forma)
                    {!! Form::radio('formacotiza',$forma->cformacotizacion,(isset($propuesta->cformacotizacion)==1?($forma->cformacotizacion == $propuesta->cformacotizacion):false)) !!}
                    {{ $forma->descripcion }}
                    <br>
                    @endforeach
        @endif
    </div>
    </div>
    <div class="col-lg-6 col-xs-6">
    <div class="radio">
                        {!! Form::radio('tipopropuesta','1',(isset($propuesta->ftipo)==1?$propuesta->ftipo == '1':false), array('id' => 'tipopropuesta-0')) !!}Propuesta
                        <br>
                        {!! Form::radio('tipopropuesta','2',(isset($propuesta->ftipo)==1?$propuesta->ftipo == '2':false), array('id' => 'tipopropuesta-1')) !!}Licitación
    </div>
    </div>
</div> 
<div class="row clsPadding">
<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>
<div class="row clsPadding2 radio">
    <div class="col-lg-6 col-xs-6">
        TAG: {!! Form::text('propuestatag',(isset($propuesta->tag)==1?$propuesta->tag:''),array('class'=>'form-control input-sm','placeholder' => 'Palabra Clave')) !!}
    </div> 
</div>
<div class="row clsPadding2">
    <div class="col-lg-4 col-xs-12">
    <label>Servicio</label>
{!! Form::select('servicio',(isset($servicio)==1?$servicio:array()),(isset($propuesta->cservicio)==1?$propuesta->cservicio:''),array('class' => 'form-control','multiple'=>'true')) !!}    
    </div>
    <div class="col-lg-4 col-xs-12">
    <label>Estado:</label>
{!! Form::select('estadopropuesta',(isset($estadopropuesta)==1?$estadopropuesta:array()),(isset($propuesta->cestadopropuesta)==1?$propuesta->cestadopropuesta:''),array('class' => 'form-control')) !!}
    <label>Rev. Principal:</label>
{!! Form::select('revprincipal',(isset($personal)==1?$personal:array()),(isset($propuesta->cpersona_revprincipal)==1?$propuesta->cpersona_revprincipal:''),array('class' => 'form-control')) !!}
    <label>Coor. Propuesta:</label>
      {!! Form::select('coorpropuesta',(isset($personal)==1?$personal:array()),(isset($propuesta->cpersona_coordinadorpropuesta)==1?$propuesta->cpersona_coordinadorpropuesta:''),array('class' => 'form-control')) !!}<br>
     <button class="btn btn-primary btn-block"><b>Elegir contactos</b></button>
    </div>
    <div class="col-lg-4 col-xs-12">
    <label>Resp. de propuesta:</label>
      {!! Form::select('respropuesta',(isset($personal)==1?$personal:array()),(isset($propuesta->cpersona_resppropuesta)==1?$propuesta->cpersona_resppropuesta:''),array('class' => 'form-control')) !!}
    <label>Gte de Prpyecto:</label>
      {!! Form::select('gtepropuesta',(isset($personal)==1?$personal:array()),(isset($propuesta->cpersona_gteproyecto)==1?$propuesta->cpersona_gteproyecto:''),array('class' => 'form-control')) !!} <br>
      <button  class="btn btn-primary btn-block" disabled><b>Paquetes de propuesta</b></button>   
    </div>
</div>
<div class="row clsPadding">
<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>
<div class="row clsPadding2">
    <div class="col-lg-4 col-xs-12">
    Detalle de Propuesta:
    @if(isset($tipopropresentacion))
                        @foreach($tipopropresentacion as $tipo)
                            <?php $activado=false; ?> 
                            <div class="checkbox">
                                @if (isset($propresentacion))
                                    @foreach($propresentacion as $p)
                                        @if($tipo->cpresentacion == $p->cpresentacion)
                                            <?php $activado=true; ?>
                                        @endif
                                    @endforeach
                                @endif
                                {!! Form::checkbox('presentacion[]',$tipo->cpresentacion,$activado) !!}
                                
                                
                                {{ $tipo->descripcion }}
                            </div>
                        @endforeach   
    @endif
    </div>
    <div class="col-lg-4 col-xs-12">
    Observaciones:
    {!! Form::textarea('propuestaobservaciones',(isset($propuesta->observaciones)==1?$propuesta->observaciones:''), array('class' => 'form-control', 'rows' => '6', 'placeholder' => 'Observaciones a la Propuesta')) !!} 
    </div>
    <div class="col-lg-4 col-xs-12">
        Moneda:
            <div class="radio">
            @if(isset($monedas))
            @foreach($monedas as $mon)
                {!! Form::radio('moneda',$mon->cmoneda,(isset($propuesta->cmoneda)==1?($mon->cmoneda == $propuesta->cmoneda):false)) !!}
                {{ $mon->descripcion }}
                <br>
            @endforeach
            @endif
            </div><br>
        Medio de entrega:    
            <div class="radio">
            @if(isset($medioentrega))
            @foreach($medioentrega as $medio)
                            {!! Form::radio('mentrega',$medio->cmedioentrega,(isset($propuesta->cmedioentrega)==1?($medio->cmedioentrega == $propuesta->cmedioentrega):false)) !!}
                            {{ $medio->descripcion }}
                            <br>
            @endforeach   
            @endif
            </div>
        Revisión Actual: {!! Form::text('propuestarevision',(isset($propuesta->revision)==1?$propuesta->revision:(isset($revision)==1?$revision:'')),array('class'=>'form-control input-sm','placeholder' => '','disabled'=>'true')) !!} 
        Nueva Revisión:{!! Form::select('nuevarevision',(isset($revisiones)==1?$revisiones:array()),'',array('class' => 'form-control')) !!}
        
            
               
    </div>
</div>

<div class="row clsPadding2">
<div class="col-lg-12 col-xs-12 clsTitulo">
Fechas
</div>
</div>
<div class="row clsPadding2">
    <div class="col-lg-4 col-xs-12">
                <label>Entrega de bases(*):</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('propuestafbases',(isset($propuesta->fentregabases)==1?$propuesta->fentregabases:''), array('class'=>'form-control pull-right')) !!}
                </div>   
                <!--  ***************************  -->
                <label>Presentación(*):</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fpresentacion',(isset($propuesta->fpresentacion)==1?$propuesta->fpresentacion:''), array('class'=>'form-control pull-right')) !!}
                </div> 
                <!--  ***************************  -->
                <label>Visita técnica(*):</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fvtecnica',(isset($propuesta->fvtecnica)==1?$propuesta->fvtecnica:''), array('class'=>'form-control pull-right')) !!} 
                </div>                                   
                
    </div>
    <div class="col-lg-4 col-xs-12">
                <label>consulta(*):</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fconsulta',(isset($propuesta->fconsulta)==1?$propuesta->fconsulta:''), array('class'=>'form-control pull-right')) !!} 
                </div>   
                <!--  ***************************  -->
                <label>Respuesta(*):</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('frespuesta',(isset($propuesta->frespuesta)==1?$propuesta->frespuesta:''), array('class'=>'form-control pull-right')) !!} 
                </div> 
                <!--  ***************************  -->    
    </div>
    <div class="col-lg-4 col-xs-12">
                <label>Absolución de Consulta (*):</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fabsolucionconsultas',(isset($propuesta->fabsolucionconsultas)==1?$propuesta->fabsolucionconsultas:''), array('class'=>'form-control pull-right')) !!}
                </div>   
                <!--  ***************************  -->
                <label>Charla informativa(*):</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fcharla',(isset($propuesta->fcharla)==1?$propuesta->fcharla:''), array('class'=>'form-control pull-right')) !!} 
                </div> 
                <!--  ***************************  -->    
    </div>
</div> 
<div class="row clsPadding">
<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>

<div class="row clsPadding2">
    <div class="col-lg-4 col-xs-4 cls-centered"></div>
        <button class="btn btn-primary btn-block" style="width:200px;"><b>Generar revisión</b></button> 
    </div>
</div>
    
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






    {!! Form::close() !!}
    </section>
    <!-- /.content -->

    <!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuestas</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuestas" class="table">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Cliente</th>
                  <th>Unidad Minera</th>
                  <th>Propuesta</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal -->


  </div>




<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';        
        var table=$("#tPropuestas").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarPropuestas",
                "type": "GET",
            },
            "columns":[
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tPropuestas tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            
            if ( index === -1 ) {
                selected.push( id );

            } else {
                selected.splice( index, 1 );
            }
     
            $(this).toggleClass('selected');
        } );     
        $('#searchPropuesta').on('hidden.bs.modal', function () {
           goEditar();
        });

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('editarRevisionPropuesta/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $('#propuestafbases').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });       

        $('#fpresentacion').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        $('#fvtecnica').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        $('#fconsulta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });       

        $('#frespuesta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        $('#fabsolucionconsultas').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 
        
         $('#fcharla').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        $('#frmpropuesta').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);


            $.ajax({

                type:"POST",
                url:'grabarRevisionPropuesta',
                data: $(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });            
</script>
