<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Propuesta
      <small>Importar Propuesta</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Propuesta</li>
    </ol>
  </section>
  <section class="content">
    <div class="row" id="verppa">
      <div class="col-lg-12 col-xs-12">

        <div class="box box-primary">
          <div class="box-header">
            <h3 class="box-title">Importar PPA</h3>
            <div class="pull-right">
                <button type='button' class="btn btn-info" id="leyenda" style="color:black"><b>Ver abreviaturas</b></button>
            </div>
            <div class="pull-right">
                <button type='button' class="btn btn-info" id="leyendasig" style="color:black"><b>Ver código SIG</b></button>
            </div>
          </div>

          <form  id="f_cargar_datos" name="f_cargar_datos" method="post"  action="importarppa" class="formarchivo" enctype="multipart/form-data" >                

           <input type="hidden" name="_token" id="_token"  value="<?= csrf_token(); ?>"> 

           <div class="col-lg-12 col-xs-12">
            <label>Propuestas : </label>
            <select class="form-control" name="cpropuesta" id="comboListaPropuestas">
              <option value="">Seleccione propuestas</option>
              @foreach ($propuestas as $key => $ppa)
              <option value="{{$ppa->cpropuesta}}">{{$ppa->ccodigo}} {{$ppa->nombre}}</option>
              @endforeach
            </select>
          </div>
          <br>
          <div class="col-lg-6 col-xs-6">
            <label>Cliente : </label>
            <input type="text" id="cliente" readonly="true" class="form-control">
          </div>
          <div class="col-lg-6 col-xs-6">
            <label>Unidad minera : </label>
            <input type="text" id="uminera" readonly="true" class="form-control">
          </div>

          <div class="box-body">
            <div class="form-group col-lg-12 col-xs-12"  >
             <label>Agregar Archivo de Excel </label>
             <input name="archivo" id="archivo" type="file" class="archivo form-control"  required/><br /><br />
           </div>

           <div class="box-footer">
            <div class="pull-left">
              <button type="submit" class="btn btn-primary" id="cargar_datos">Cargar archivo</button>
            </div>
            <div class="pull-left">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <button type="button" class="btn btn-danger" id="limpiar">Limpiar</button>
            </div>
            <div class="pull-right">
              <button type='button' class="btn btn-block btn-primary input-sm"  id="aso_act"><b>Asociar actividades</b></button>
            </div>
            <br><br><br><br>

            <div class="col-lg-12 col-xs-12">
              <label>Seleccionar hoja a importar : </label>
              <select class="form-control" name="nombrehoja" id="comboListaHojas">

              </select>
            </div>
          </div>

        </div>

        <input type="hidden" name="datos" value='sindatos' id="datos">
        <div class="col-lg-4 col-xs-4 pull-left">
          <label>Separador de cabecera : </label>
          <input type="text" name="separador" value='_' id="separador">
        </div> 

        <div class="col-lg-4 col-xs-4 pull-right">
          <label>Suma total de horas : </label>
          <input type="text" name="sumatotal" value='0' id="sumatotal" readonly="true" >
        </div>

        <!-- <br><br><br> -->
        <div class="col-lg-12 col-xs-12">
          <label>Vista previa</label>
        </div>
        <br><br><br>
        <div class="col-lg-12 col-xs-12">
          <div class="col-lg-12 col-xs-12 table-responsive" id="tablaproyectoslista" style="height: 700px;width: 100%;"></div>
        </div>
      </form>
      

    </div>
  </div>
</div>

<div id="aso_actividades">
  @include('propuesta.modalasociaractividades',array(
    'act_ppa'=>(isset($act_ppa)==1?$act_ppa:array() ) ,
    'act_pry'=>(isset($act_pry)==1?$act_pry:array() ),
    'cproyecto'=>(isset($cproyecto)==1?$cproyecto:null ),
    'cpropuesta'=>(isset($cpropuesta)==1?$cpropuesta:null )
                                                      )
          )
</div>


</section>
</div>  

<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>
<script type="text/javascript">

  $("#limpiar").click(function(event) {
    $("#comboListaPropuestas").val('').trigger("chosen:updated");
    $('#comboListaHojas option').remove();
    $("#comboListaHojas").append("<option value='0'>Seleccione hoja a cargar</option>");
    $("#comboListaHojas").trigger("chosen:updated");
    $('#cliente').val('');
    $('#uminera').val('');
    $('#tablaproyectoslista').empty();
    $("#datos").val('sindatos');
    $("#archivo").val('');
    $("#cargar_datos").text('Cargar archivo');
  });


  $('#aso_act').click(function(event) {
    var cpropuesta = $('#comboListaPropuestas').val();
    console.log(cpropuesta);
    if (cpropuesta == '') { swal('Seleccione una propuesta')}
    else{
    $.ajax({
      url: 'actividades_ppa_proy',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'cpropuesta': cpropuesta},
    })
    .done(function(data) {
      // console.log("success",data);
      $('#aso_actividades').html(data);
      $('#searchActividades').modal('show');
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    }
    
  });


  addchosen('comboListaPropuestas');
  addchosen('comboListaHojas');

  $('#comboListaPropuestas').change(function(e) {
    var cpropuesta = $('#comboListaPropuestas').val();
    $.ajax({
      url: 'verimportarppavista',
      type: 'GET',
    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    data: {'cpropuesta': cpropuesta},
  })
    .done(function(data) {
      console.log("propuesta",data);
      $('#cliente').val(data[0][0].nombre);
      $('#uminera').val(data[0][1].nombre);
      $('#tablaproyectoslista').empty();
      $("#datos").val('sindatos');
      $("#archivo").val('');
      $("#cargar_datos").text('Cargar archivo');
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

  });

  $(document).on("submit",".formarchivo",function(e){

    e.preventDefault();

        //información del formulario
        var formData = new FormData($("#f_cargar_datos")[0]);
        $('#div_carga').show();
        $.ajax({
          url: "importarppa",  
          type: 'POST',
          // Form data
          //datos del formulario
          data: formData,
          //necesario para subir archivos via ajax
          cache: false,
          contentType: false,
          processData: false,
          //mientras enviamos el archivo
        })
        .done(function(data) {
          $('#div_carga').hide();
          console.log("hojas",data);
          if (data[0] == 'hojas') {          
            mostrarComboHojas(data[1],'');
            $("#datos").val('sindatos');
            $("#cargar_datos").text('Mostrar datos');
            swal('Archivo cargado, seleccione hoja a importar');
          }
          if (data[0] == 'matriz') {
            mostrarComboHojas(data[1],data[2]);
            matrizppa(data[3])
            $("#datos").val('condatos');
            $("#cargar_datos").text('Guardar datos');
            swal('Se cargaron los datos de la hoja seleccionada');
          }
          if (data[0] == 'cargado') {
            mostrarComboHojas(data[1],data[2]);
            // $('#tablaproyectoslista').empty();
            $("#datos").val('condatos');
            $("#sumatotal").val(data[3])
            swal('Se guardaron los datos correctamente');
          }
        })
        .fail(function() {
          // console.log("error");
          $('#div_carga').hide();
          swal("ha ocurrido un error") ;
        })
        .always(function() {
          $('#div_carga').hide();
          console.log("complete");
        });

      });

  function mostrarComboHojas(lista,sele)
  {
      //eliminar datos combo *
      $('#comboListaHojas option').remove();
      $("#comboListaHojas").append("<option value='0'>Seleccione hoja a cargar</option>");

      for (var i = 0; i < lista.length; i++) {
        if (sele==lista[i]) {seleccion='selected';} else {seleccion='';};
        $("#comboListaHojas").append("<option value='"+ lista[i] +"' "+seleccion+">"+lista[i]+"</option>");
      }

      $("#comboListaHojas").trigger("chosen:updated");
    };


    function matrizppa(lista){
      // $('#tablactividades').dataTable().fnDestroy();
      $('#tablaproyectoslista').empty();
      var sumatotal = 0;
      tamano = '';
      detalle = '<table class="table table-striped table-hover table-bordered table-responsive" style="font-size: 12px; width:100%; text-align: center;" id="tablactividades">';
        // <caption>table title and/or explanatory text</caption>
        detalle += '<thead>';
        detalle += '<tr class="clsCabereraTabla">';
        for (var i = 0; i < lista[0].length; i++) {
          // detalle += '<th>'+lista[0][i]+'<input type="hidden" value="'+lista[0][i]+'" id="cab_'+i+'" name="cab_'+i+'"></th>';
          detalle += '<th>'+lista[0][i]+'<input type="hidden" value="'+lista[0][i]+'" id="cab_'+i+'" name="cab[]"></th>';
        }
        detalle += '</tr>';
        detalle += '</thead>';
        detalle += '<tbody>';
        for (var x = 1; x < lista.length; x++) {
          detalle += '<tr>';
          for (var y = 0; y < lista[x].length; y++) {

            if (y == 0)       {tamañno = 6; nombre='item';  color=''; alineacion=''}
            if (y==2 || y==3) {tamañno = 1; nombre='ent';  color=''; alineacion='center'}
            if (y == 1)       {tamañno = 50; nombre='act';  color=''; alineacion=''}
            if (y>3)          {tamañno = 1; nombre='hrs'; lista[x][y]>0?color='#8AE234FF':color=''; alineacion='center'; sumatotal+=lista[x][y]}

            // detalle += '<td><input type="text" size="'+tamañno+'" value="'+lista[x][y]+'" id="'+nombre+'"_'+x+'_'+y+' name="det_'+nombre+'_'+x+'_'+y+'" style="border-radius: 5px;"></td>';
            // detalle += '<td><input type="text" size="'+tamañno+'" value="'+lista[x][y]+'" id="'+nombre+'"_'+x+'_'+y+' name="'+nombre+'" style="border-radius: 5px;"></td>';
            detalle += '<td><input type="text" class="focusable" size="'+tamañno+'" value="'+lista[x][y]+'" id="valor_'+x+y+'" name="det[]" style="border-radius: 5px; text-align:'+alineacion+'; background-color:'+color+';"></td>';
          }
          detalle += '</tr>';
        }
        detalle += '</tbody>';
        detalle += '</table>';

        $("#tablaproyectoslista").append(detalle);
        $("#sumatotal").val(sumatotal);

        $("#tablactividades").tableHeadFixer(
          {'left' : 4},
          
          );

        $("input.focusable").keyup(function(tecla){
          id = $(this).attr('id');
          valor = $('#'+id).val();

          if (valor > 0) {
            $('#'+id).css({
              'background-color': 'red',
            });
          }else {
            $('#'+id).val(0);
            $('#'+id).css({
              'background-color': 'white',
            });
          }
        });

        $("input.focusable").click(function(event) {
          $(this).select();
        });

      }

   $("#leyenda").click(function(event) {
     $.ajax({
       url: 'leyendappa',
       type: 'GET',
       // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
       // data: {param1: 'value1'},
     })
     .done(function(data) {
       //console.log("leyendappa",data);
       mostrarleyenda(data);
     })
     .fail(function() {
       console.log("error");
     })
     .always(function() {
       console.log("complete");
     });
     
   });

   $("#leyendasig").click(function(e){
    $.ajax({
      url: 'leyendasig',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      // data: {param1: 'value1'},
    })
    .done(function(data) {
      console.log("leyendasig",data);
      mostrarleyendasig(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
   });

   function mostrarleyenda(lista){
      $('.leyend').remove();

          detalle = '<div class="box box-widget widget-user leyend" style="width:auto;height:auto;border-radius: 5px 5px 5px 5px;position:absolute;z-index:10001;padding:10px 10px 10px 10px;line-height: 200%;">'
          detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarleyenda() style="color:black"><i class="fa fa-times"></i></button>'
            detalle += '<div class="widget-user-header" style="background:#06AF;color:white">';
              detalle += '<h3 class="widget-user-username">Leyenda</h3>';
              detalle += '<h4 class="widget-user-desc">*Verifique que la cabecera tenga algunas de estas abreviaturas</h4>';
              // detalle += '<h5 class="widget-user-desc">'+data.codigo+'</h5>';
              // detalle += '<h6 class="widget-user-desc">'+data.nombre+'</h6>';
            detalle += '</div>';
            detalle += '<div class="box-footer no-padding">';
              detalle += '<ul class="nav nav-stacked">';
              detalle += '<table class="table table-striped table-hover table-bordered table-responsive display" style="font-size: 12px; width:100%; text-align: center;" id="tableleyenda">';
                detalle += '<thead>';
                  detalle += '<tr>';
                    detalle += '<th style="text-align: center;">Ítem</th>';
                    detalle += '<th style="text-align: center;">Descripción del rol</th>';
                    detalle += '<th style="text-align: center;">Abreviatura</th>';
                  detalle += '</tr>';
                detalle += '</thead>';
                detalle += '<tfoot>';
                  detalle += '<tr>';
                    detalle += '<th style="text-align: center;" colspan="3">** En caso de no encontrar un rol por favor de comomunicarse con el área de TI</th>';
                  detalle += '</tr>';
                detalle += '</tfoot>';
                detalle += '<tbody>';
                for (var i = lista.length - 1; i >= 0; i--) {
                  detalle += '<tr>';
                    detalle += '<td style="text-align: left;">'+(lista.length - i)+'</td>';
                    detalle += '<td style="text-align: center;">'+lista[i].descripcionrol+'</td>';
                    detalle += '<td style="text-align: center;">'+lista[i].abreviatura+'</td>';
                  detalle += '</tr>';
                }
                detalle += '</tbody>';
              detalle += '</table>';
              detalle += '</ul>';
            detalle += '</div>';
          detalle += '</div>';

      $("#verppa").append(detalle);
      $(".leyend").draggable();
      $(".leyend").resizable();

      var table = $('#tableleyenda').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     false,
        lengthChange: false,
        scrollY:        "400px",
        scrollX:        false,
        scrollCollapse: true,
        paging:         false,
        buttons: ['excel'],
        // fixedColumns:   {
        //     leftColumns: 2
        // }
        // "order": [[1, 'asc']],
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "_MAX_ roles",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando...",
          },
        } );

      table.buttons().container()
        .appendTo( '#tableleyenda_wrapper .col-sm-6:eq(0)' );
   };

  function cerrarleyenda(){
    $('.leyend').remove();
  };

  function mostrarleyendasig(lista){
    $('.leyendsig').remove();

        detalle = '<div class="box box-widget widget-user leyendsig" style="width:auto;height:auto;border-radius: 5px 5px 5px 5px;position:absolute;z-index:10001;padding:10px 10px 10px 10px;line-height: 200%;">'
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarleyendasig() style="color:black"><i class="fa fa-times"></i></button>'
          detalle += '<div class="widget-user-header" style="background:#06AF;color:white">';
            detalle += '<h3 class="widget-user-username">Leyenda</h3>';
            detalle += '<h4 class="widget-user-desc">*Verifique que la cabecera tenga el código SIG correcto</h4>';
            // detalle += '<h5 class="widget-user-desc">'+data.codigo+'</h5>';
            // detalle += '<h6 class="widget-user-desc">'+data.nombre+'</h6>';
          detalle += '</div>';
          detalle += '<div class="box-footer no-padding">';
            detalle += '<ul class="nav nav-stacked">';
            detalle += '<table class="table table-striped table-hover table-bordered table-responsive display" style="font-size: 12px; width:100%; text-align: center;" id="tableleyendasig">';
              detalle += '<thead>';
                detalle += '<tr>';
                  detalle += '<th style="text-align: center;">Ítem</th>';
                  detalle += '<th style="text-align: center;">Área / Disciplina</th>';
                  detalle += '<th style="text-align: center;">Código SIG</th>';
                detalle += '</tr>';
              detalle += '</thead>';
              detalle += '<tfoot>';
                detalle += '<tr>';
                  detalle += '<th style="text-align: center;" colspan="3">** En caso de no encontrar una area/disciplina por favor de comomunicarse con el área de TI</th>';
                detalle += '</tr>';
              detalle += '</tfoot>';
              detalle += '<tbody>';
              for (var i = lista.length - 1; i >= 0; i--) {
                detalle += '<tr>';
                  detalle += '<td style="text-align: left;">'+(lista.length - i)+'</td>';
                  detalle += '<td style="text-align: center;">'+lista[i].descripcion+'</td>';
                  detalle += '<td style="text-align: center;">'+lista[i].codigo_sig+'</td>';
                detalle += '</tr>';
              }
              detalle += '</tbody>';
            detalle += '</table>';
            detalle += '</ul>';
          detalle += '</div>';
        detalle += '</div>';

    $("#verppa").append(detalle);
    $(".leyendsig").draggable();
    $(".leyendsig").resizable();

    var table = $('#tableleyendasig').DataTable( {
      "paging":   false,
      "ordering": true,
      "info":     false,
      lengthChange: false,
      scrollY:        "400px",
      scrollX:        false,
      scrollCollapse: true,
      paging:         false,
      buttons: ['excel'],
      // fixedColumns:   {
      //     leftColumns: 2
      // }
      // "order": [[1, 'asc']],
      "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Sin Resultados",
          "info": "_MAX_ roles",
          "infoEmpty": "No existe registros disponibles",
          "infoFiltered": "(filtrado de un _MAX_ total de registros)",
          "search":         "Buscar:",
          "processing":     "Procesando...",
          "paginate": {
              "first":      "Inicio",
              "last":       "Ultimo",
              "next":       "Siguiente",
              "previous":   "Anterior"
          },
          "loadingRecords": "Cargando...",
        },
      } );

    table.buttons().container()
      .appendTo( '#tableleyendasig_wrapper .col-sm-6:eq(0)' );
 }

 function cerrarleyendasig(){
  $('.leyendsig').remove();
};

</script>