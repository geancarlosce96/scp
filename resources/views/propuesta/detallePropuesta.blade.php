<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Propuesta
        <small>Detalle de Propuesta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuesta</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoPropuesta')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>
            
        {!! Form::open(array('url' => 'grabarDetallePropuesta','method' => 'POST','id' =>'frmpropuesta')) !!}
        {!! Form::hidden('cpersona',(isset($usuario->cpersona)==1?$usuario->cusuario:''),array('id'=>'cpersona')) !!} 

        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    
    <div class="col-lg-12 col-xs-12">
    	<div class="row" style="margin-top: 15px;">
          <div class="col-md-2 col-xs-2">
            <label>
            Cliente:
            </label>
          </div>
          <div class="col-md-9 col-xs-9">
            {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta')) !!}
            {!! Form::text('nombrecliente',(isset($persona->nombre)?$persona->nombre:''),array('class'=>'form-control input-sm','disabled' => 'true')) !!}
          </div>
          <div class="col-md-1 col-xs-1">
            <button type='button' class="btn btn-block btn-primary input-sm" data-toggle="modal" data-target="#searchPropuesta"><b>...</b></button>
          </div>
          <!--<div class="col-md-1 col-xs-1">
            <button type="button" class="btn btn-primary  input-sm" disabled=""><b>+</b></button>
          </div>-->
      </div>
      
      <div class="row" style="margin-top: 15px;">
        <div class="col-md-2 col-xs-2">
          <label>Unidad minera:</label>
        </div>
        <div class="col-md-10 col-xs-10">
            {!! Form::text('nombreuminera',(isset($uminera->nombre)==1?$uminera->nombre:''),array('disabled'=>'','class'=> 'form-control input-sm','placeholder'=>'Nombre Unidad Minera')) !!}    
        </div>
      </div>
      <div class="row" style="margin-top: 15px;">
        <div class="col-md-2 col-xs-2">
          <label>
          Código:
          </label>
        </div>
        <div class="col-md-4 col-xs-3">
                {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)?$propuesta->ccodigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}          
        </div>

        <div class="col-md-2 col-xs-2">
          <label>
          Nombre de la propuesta:
          </label>
        </div>
        <div class="col-md-4 col-xs-3">
          {!! Form::text('nombrepropuesta',(isset($propuesta->nombre)==1?$propuesta->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de la Propuesta')) !!}     
        </div>
      
      </div>    

      <div class="row clsPadding" style="margin-top: 15px;">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
      </div>
      <div class="row " style="margin-top: 15px;">
          <div class="col-md-2 col-xs-6">
           <label>Responsable de propuesta:</label>
          </div>
          <div class="col-md-4 col-xs-6">
                {!! Form::text('propuestaresp',(isset($personaresp->nombre)==1?$personaresp->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Responsable de Propuesta')) !!} 
          </div>
          <div class="col-md-2 col-xs-6">
            <label>Revisión:</label>
          </div>
          <div class="col-md-2 col-xs-6">
          <input class="form-control input-sm" type="text" placeholder="A" disabled>  </label>

          </div>       
          <div class="col-md-2 col-xs-6">
          </div>
      </div>
      <div class="row" style="margin-top: 15px;">
          <div class="col-md-2 col-xs-6">
            <label>Tipo de Servicio:</label>
          </div>
          <div class="col-md-3 col-xs-6">
              {!! Form::select('servicio',$servicios,(isset($propuesta->cservicio)==1?$propuesta->cservicio:''),array('id' => 'servicio','disabled'=>'true','class'=>'form-control')) !!}        
          </div>
      
          <div class="col-md-2 col-xs-6">
            <label>Paq. de  Propuesta:</label>
          </div>
          <div class="col-md-3 col-xs-6">
              {!! Form::select('paqpropuesta',(isset($paqpropuesta)==1?$paqpropuesta:array()),null,array('id' => 'paqpropuesta','class'=>'form-control')) !!}  
          </div>
          <div class="col-md-2 col-xs-6">
                <button type="button" class="btn btn-primary btn-sm" id="btnver" onclick="verHorasDetalle()"> Visualizar</button>
          </div>
      </div>
      <div class="row" style="margin-top: 15px;">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
      </div>
      <div class="row ">
        <div class="col-lg-12 col-xs-12 clsTitulo">
        Importar:
        </div>
      </div>
      <div class="row " style="margin-top: 7px; margin-bottom: 7px;">
          <div class="col-lg-2 col-xs-2"><p style="padding-top: 12px;">Tipo de Hoja:</p></div>
          <div class="col-lg-4 col-xs-4">
            <select  name="tipohoja" id="tipohoja" class="form-control">
              <option value="HH">Honorarios</option>
              <option value="GAS">Gastos</option>
              <option value="LAB">Laboratorio</option>
              <option value="ROO">Rooster Construcción</option>        
            </select>
          </div>

          <div class="col-lg-6 col-xs-6">
            <button type="button" class="btn btn-primary btn-sm" data-toggle='modal' data-target="#uploadFile" style="margin-top:4px;"><b>Seleccionar archivo a Importar</b>
            </button>
          </div>   
      </div>

      <div class="row ">
        <div class="col-lg-12 col-xs-12 clsTitulo">
        Actividades:
        </div>
      </div>
<!--  ***************************************   -->
      <div class="row clsPadding2">
          <div class="col-lg-12 col-xs-12 table-responsive" id="divActi">
              @include('partials.tableDetalleActividad',array('listaAct' => (isset($listaAct)==1?$listaAct:array()), 'listEstr'=> (isset($listEstr)==1?$listEstr:array() ),'listEstr_apo' => (isset($listEstr_apo)==1?$listEstr_apo:array()), 'listEstr_dis' => (isset($listEstr_dis)==1?$listEstr_dis:array() ),'listDis' => (isset($listDis)==1?$listDis:array() )  ) )
          </div>
      </div>
<!--  ***************************************   -->
      <div class="row clsPadding2">
        <div class="col-lg-12 col-xs-12 clsTitulo">
        Rooster (Solo para Construcción)
        </div>
      </div>
      <!--<div class="row clsPadding2">
          <div class="col-lg-2 col-xs-4">Leyenda:</div>
          <div class="col-lg-2 col-xs-4 clsCampo">Campo</div>
          <div class="col-lg-2 col-xs-4 clsOficina">Oficina</div>
          <div class="col-lg-6  hidden-xs"></div>
      </div>-->
<!--  ***************************************   -->
      <div class="row clsPadding2">
          <div class="col-lg-12 col-xs-12 table-responsive" idf="viewCrono">
            @include('propuesta.tableCronogramaRoosterPropuesta', array('crono' => (isset($crono)==1?$crono:array() ),'dias' => (isset($dias)==1?$dias:array() ),'personal' => (isset($personal)==1?$personal:array() )  ) )
          </div>
      </div>
<!--  ***************************************   -->
      <div class="row clsPadding2">
          <div class="col-lg-9 col-xs-9 clsTitulo">Gastos:</div>
          <div class="col-lg-1 col-xs-1" style="text-align:right;">Acciones:</div>
          <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-target="#divGastos" id="btnAddGasto"> <b>+</b></button></div>
          <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " id="btnDelGasto"> <i class="fa fa-trash-o" aria-hidden="true"></i></button></div>        
      </div>
<!--  *****************************************  -->
      <div class="row clsPadding2">
          <div class="col-lg-12 col-xs-12 table-responsive" id="tableGastos">
            @include('partials.tableGastosPropuesta',array('gastos'=> (isset($gastos)==1?$gastos:array() ) ))    
          
          
          </div>
      </div>    
<!-- ****************************************** -->
      <div class="row clsPadding2">
          <div class="col-lg-9 col-xs-9 clsTitulo">Laboratorio:</div>
          <div class="col-lg-1 col-xs-1" style="text-align:right;">Acciones:</div>
          <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-target="#divGastosLab" id="btnAddGastoLab"> <b>+</b></button></div>
          <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " id="btnDelGastoLab"> <i class="fa fa-trash-o" aria-hidden="true"></i> </button></div>        
      </div>
<!--  *****************************************  -->
      <div class="row clsPadding2">
          <div class="col-lg-12 col-xs-12 table-responsive" id="tableGastosLab">
          
          @include('partials.tableGastosLabPropuesta',array('gastosLab'=> (isset($gastosLab)==1?$gastosLab:array() ) ))  

          
          
          </div>
      </div>    
<!-- ****************************************** -->
      <div class="row clsPadding">
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
      </div>
      <div class="row clsPadding">
          <div class="col-lg-4 col-xs-4">
              <button class="btn btn-primary btn-block" style="width:200px;" ><b>Grabar</b></button> 
          </div>
          <div class="col-lg-4 col-xs-4">
              <button type="button" class="btn btn-primary btn-block" style="width:200px;" disabled><b>Vista Previa</b></button> 
          </div>            
      </div>
   </div>
   </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
   
    {!! Form::close() !!}
    </section>
    <!-- /.content -->
    <!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuestas</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuestas" class="table">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Cliente</th>
                  <th>Unidad Minera</th>
                  <th>Propuesta</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal -->

    <!-- Modal Upload Archivo-->
     
    <div id="uploadFile" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Subir Archivos</h4>
          </div>
          <div class="modal-body">
          {!! Form::open(
              array(
                  'route' => 'uploadFile', 
                  'class' => 'form', 
                  'novalidate' => 'novalidate', 
                  'files' => true,
                  'id' => 'frmUploadFiles'
                  )) !!}
            {!! Form::hidden('cpropuesta_file',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:'')) !!}
            {!! Form::hidden('tipohoja','',array('id'=>'tipohoja_up')) !!}
            {!! Form::hidden('paquete','',array('id'=>'paquete_up')) !!}
              <div id="opciones">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingOne">
                          <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                              Honorarios
                            </a>
                          </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          <div class="panel-body">
                            <label>Fila de Inicio de Datos</label>
                            <input type="text" name="ho_inicioDatos" size="2" maxlength="3" value="9" />
                            <label>Fila de Fin de Datos</label>
                            <input type="text" name="ho_finDatos" size="2" maxlength="3" value="21" />
                            <label>Columna Inicio Senior</label>
                            <input type="text" name="ho_inicioSenior" size="3" maxlength="3" value="E" />
                            <label>Columna Fin Senior</label>
                            <input type="text" name="ho_finSenior" size="3" maxlength="3" value="K" />
                            <br />
                            <label>Columna Inicio Profesionales Disciplinas</label>
                            <input type="text" name="ho_inicioDis" size="3" maxlength="3" value="L" />
                            <label>Columna Fin Profesionales Disciplinas</label>
                            <input type="text" name="ho_finDis" size="3" maxlength="3" value="AY" />

                            <label>Columna Inicio Apoyo</label>
                            <input type="text" name="ho_inicioApoyo" size="3" maxlength="3" value="AZ" />
                            <label>Columna Fin Apoyo</label>
                            <input type="text" name="ho_finApoyo" size="3" maxlength="3" value="BA" />                                                        



                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingTwo">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                              Gastos
                            </a>
                          </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                          <div class="panel-body">
                            <label>Fila de Inicio de Datos</label>
                            <input type="text" name="ga_inicioDatos" size="2" maxlength="3" value="7" />
                            <label>Fila de Fin de Datos</label>
                            <input type="text" name="ga_finDatos" size="2" maxlength="3" value="25" />

                            <label>Columna Item</label>
                            <input type="text" name="ga_colItem" size="3" maxlength="3" value="A" />
                            <label>Columna Descripcion</label>
                            <input type="text" name="ga_colDes" size="3" maxlength="3" value="B" />
                            <br />
                            <label>Columna Unidad</label>
                            <input type="text" name="ga_colUni" size="3" maxlength="3" value="C" />
                            <label>Columna Cantidad</label>
                            <input type="text" name="ga_colCant" size="3" maxlength="3" value="D" />
                            <br />
                            <label>Columna Costo</label>
                            <input type="text" name="ga_colCos" size="3" maxlength="3" value="E" />
                            <label>Columna SubTotal</label>
                            <input type="text" name="ga_colSub" size="3" maxlength="3" value="F" />  
                            <label>Columna Comentario</label>
                            <input type="text" name="ga_colCom" size="3" maxlength="3" value="G" />                              
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingThree">
                          <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                              Gastos de Laboratorio
                            </a>
                          </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                          <div class="panel-body">
                           <label>Fila de Inicio de Datos</label>
                            <input type="text" name="gal_inicioDatos" size="2" maxlength="3" value="8" />
                            <label>Fila de Fin de Datos</label>
                            <input type="text" name="gal_finDatos" size="2" maxlength="3" value="36" />

                            <label>Columna Item</label>
                            <input type="text" name="gal_colItem" size="3" maxlength="3" value="A" />
                            <label>Columna Descripcion</label>
                            <input type="text" name="gal_colDes" size="3" maxlength="3" value="B" />
                            <br />
                            <label>Columna Canteras</label>
                            <input type="text" name="gal_colCantCant" size="3" maxlength="3" value="D" />
                            <label>Columna Cimentación</label>
                            <input type="text" name="gal_colCantCimen" size="3" maxlength="3" value="E" />
                            <br />
                            <label>Columna Botadero</label>
                            <input type="text" name="gal_colCantBot" size="3" maxlength="3" value="" />
                            <label>Columna Costo</label>
                            <input type="text" name="gal_colCos" size="3" maxlength="3" value="C" />  
                            <label>Columna Subtotal</label>
                            <input type="text" name="gal_colSub" size="3" maxlength="3" value="F" />  
                          </div>
                        </div>
                      </div>
                    </div>

              </div>
              <div class="form-group">
                {!! Form::label('Ingrese nombre de hoja:  ') !!}
                <input type="text" name="nombrehoja" size="20" maxlength="20" placeholder="Nombre de Hoja" />  
              </div>
              <div class="form-group">
                  {!! Form::label('Especificar el Archivo Excel') !!}
                  {!! Form::file('fileXls', null,array('class'=>'form-control')) !!}
              </div>

              <div class="form-group">
                  {!! Form::submit('Subir Archivos',array('class'=>'btn btn-sucess')) !!}
              </div>
           {!! Form::close() !!} 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal Upload Archivo-->

  <!-- Modal Editar Datos de Horas-->
      <div id="divEditarHoras" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: yellow">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:black">Registro de Horas de Propuesta</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmhoras')) !!}
              {!! Form::hidden('_token_horas',csrf_token(),array('id'=>'_token_horas')) !!}
              
              {!! Form::hidden('cpropuesta_h',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta_h')) !!}            
                <div id="divConteHoras">
                @include('partials.tableInputHorasActividad',array('listaAct'=> (isset($listaAct)==1?$listaAct:array()) ,'cestructurapropuesta'=> (isset($cestructurapropuesta)==1?$cestructurapropuesta:''), 'propuesta' => (isset($propuesta)==1?$propuesta:null )   ) )
                </div>
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="save_profesional_senior()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Editar Datos de Horas -->

    <!-- Modal Editar Datos de Horas x Disciplina-->
      <div id="divEditarHorasDis" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: yellow">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:black">Registro de Horas de Propuesta - Disciplina</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmhorasDis')) !!}
              {!! Form::hidden('_token_horas_dis',csrf_token(),array('id'=>'_token_horas_dis')) !!}
              
              {!! Form::hidden('cpropuesta_hdis',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta_hdis')) !!}            
                <div id="divConteHorasDis">
                @include('partials.tableInputHorasActividadDis',array('listaAct'=> (isset($listaAct)==1?$listaAct:array()) ,'cestructurapropuesta'=> (isset($cestructurapropuesta)==1?$cestructurapropuesta:''), 'propuesta' => (isset($propuesta)==1?$propuesta:null ) ,'cdisciplina'=> (isset($cdisciplina)==1?$cdisciplina:'')  ) )
                </div>
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="save_profesional_disciplina()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Editar Datos de Horas x Disciplina-->

    <!-- Modal Editar Datos de Horas Apoyo-->
      <div id="divEditarHorasApo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: blue">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:black">Registro de Horas de Propuesta</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmhorasApo')) !!}
              {!! Form::hidden('_token_horas_apo',csrf_token(),array('id'=>'_token_horas_apo')) !!}
              
              {!! Form::hidden('cpropuesta_hapo',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta_hapo')) !!}            
                <div id="divConteHorasApo">
                @include('partials.tableInputHorasActividad',array('listaAct'=> (isset($listaAct)==1?$listaAct:array()) ,'cestructurapropuesta'=> (isset($cestructurapropuesta)==1?$cestructurapropuesta:''), 'propuesta' => (isset($propuesta)==1?$propuesta:null )   ) )
                </div>
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="save_profesional_apoyo()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Editar Datos de Horas Apoyo-->

  <!-- Modal Seleccionar Gastos-->
      <div id="divGastos" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: blue">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:white">Seleccionar Gastos</h4>
            </div>
            <div class="modal-body">
            <div id="treeGastos">
            </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Seleccionar Gastos-->


  <!-- Modal Seleccionar Gastos Laboratorio-->
      <div id="divGastosLab" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: blue">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:white">Seleccionar Gastos de Laboratorio</h4>
            </div>
            <div class="modal-body">
            <div id="treeGastosLab">
            </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Seleccionar Gastos Laboratorio-->

    <!-- ******************** MOdal Comentario******************************* -->
      <div class="modal fade" id="modalObs" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="ObsModalLabel">Observaciones</h4>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="message-text" class="control-label">Ingrese comentario:</label>
                  <textarea class="form-control" id="message-text"></textarea>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="saveObs()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>


    <!-- *************************************************** -->  
    <!-- ********************* Modal Sugerencia****************************** -->
      <div class="modal fade" id="modalSug" tabindex="-1" role="dialog" aria-labelledby="SugModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="SugModalLabel">Sugerencia de Tarea</h4>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="message-text-sug" class="control-label">Ingrese Sugerencia:</label>
                  <textarea class="form-control" id="message-text-sug"></textarea>
                  <label for="chkSug" class="control-label">UtilizarSugerencia:</label>
                  <input type="checkbox" name="chkSug" id="chkSug" value="1" />
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
              <button type="button" class="btn btn-primary" onclick="saveSug()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>


    <!-- *************************************************** -->    

  </div>




<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        var selected_gastos = [];
        var selected_gastos_lab = [];
        var i_col=0;
        var i_fila=0;        
        $.fn.dataTable.ext.errMode = 'throw';        
        var table=$("#tPropuestas").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarPropuestas",
                "type": "GET",
            },
            "columns":[
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tPropuestas tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            }
     
            $(this).toggleClass('selected');
        } );     

        $('#searchPropuesta').on('hidden.bs.modal', function () {
           goEditar();
        });       
        function viewObs(fila,col){
          //obj = document.getElementsByName('ejecu['+col+']['+fila+']');
          obj = document.querySelector("[name='hora["+(fila)+"]["+(col)+"]']");
          document.getElementById('message-text').value=obj.value;
          i_col=col;
          i_fila=fila;
          $('#modalObs').modal('show');
          
        }
        function saveObs(){
            obj = document.querySelector("[name='hora["+i_fila+"]["+i_col+"]']");
            obj.value=document.getElementById('message-text').value;
            $('#modalObs').modal('hide');
        }     
        function viewSug(fila,col){
          //obj = document.getElementsByName('ejecu['+col+']['+fila+']');
          obj = document.querySelector("[name='hora["+(fila)+"]["+(col)+"]']");
          objChk = document.querySelector("[name='hora["+(fila)+"]["+(3)+"]']");
          document.getElementById('message-text-sug').value=obj.value;
          if(objChk.value=='1'){
            document.getElementById('chkSug').checked = true;
          }else{
            document.getElementById('chkSug').checked = false;
          }

          i_col=col;
          i_fila=fila;
          $('#modalSug').modal('show');
          
        }
        function saveSug(){
            obj = document.querySelector("[name='hora["+i_fila+"]["+i_col+"]']");
            obj.value=document.getElementById('message-text-sug').value;
            objChk = document.querySelector("[name='hora["+i_fila+"]["+3+"]']");
            objChk.value=document.getElementById('chkSug').checked?'1':'0';
            
            $('#modalSug').modal('hide');
        }             
        /* Agregar Eliminar Gastos */
        $("#divGastos").on('shown.bs.modal', function(){
          selected_gastos.splice(0,selected_gastos.length);
          $('#treeGastos').highCheckTree({
            data: getTree(),
            onCheck: function (node){
              selected_gastos.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr('rel'));
              if(idx != -1){
                selected_gastos.splice(idx,1);
              }
            }
          });
        });
        $("#divGastos").on('hidden.bs.modal',function(){
          /*$.each(selected_gastos,function(index,value){
            alert(value);
          });*/
          if(selected_gastos.length <=0){
            return;
          }
          $.ajax({
            type:'GET',
            url: 'addGastosPropuesta',
            data: {selected_gastos:selected_gastos},
            success: function(data){
              $("#tableGastos").html(data);
              $('#div_carga').hide();
            },
            error: function(data){
              $('#div_carga').hide();
            },
            beforeSend: function(){
              $('#div_carga').show(); 
            }
          });
        });

        $('#btnDelGasto').click(function(){
            var searchIDs = $("#tableListGastos input:checkbox:checked").map(function(){
              return $(this).val();
            }).toArray();
            console.log(searchIDs);
            $.ajax({
              type: 'GET',
              url :'delGastosPropuesta',
              data: {chkselec:searchIDs},
              success :function(data){
                $("#tableGastos").html(data);
                $('#div_carga').hide();
              },
              error : function(data){
                 $('#div_carga').hide();
              },
              beforeSend : function(){
                $('#div_carga').show(); 
              }
            });
        });
        /* Fin Agregar Eliminar Gastos */

        /* Agregar Eliminar Gastos Lab*/
        $("#divGastosLab").on('shown.bs.modal', function(){
          selected_gastos_lab.splice(0,selected_gastos_lab.length);
          $('#treeGastosLab').highCheckTree({
            data: getTreeLab(),
            onCheck: function (node){
              selected_gastos_lab.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr('rel'));
              if(idx != -1){
                selected_gastos_lab.splice(idx,1);
              }
            }
          });
        });
        $("#divGastosLab").on('hidden.bs.modal',function(){
          /*$.each(selected_gastos,function(index,value){
            alert(value);
          });*/
          if(selected_gastos_lab.length <=0){
            return;
          }
          $.ajax({
            type:'GET',
            url: 'addGastosLabPropuesta',
            data: {selected_gastos_lab:selected_gastos_lab},
            success: function(data){
              $("#tableGastosLab").html(data);
              $('#div_carga').hide();
            },
            error: function(data){
              $('#div_carga').hide();
            },
            beforeSend: function(){
              $('#div_carga').show(); 
            }
          });
        });

        $('#btnDelGastoLab').click(function(){
            var searchIDs = $("#tableListGastosLab input:checkbox:checked").map(function(){
              return $(this).val();
            }).toArray();
            console.log(searchIDs);
            $.ajax({
              type: 'GET',
              url :'delGastosLabPropuesta',
              data: {chkselecLab:searchIDs},
              success :function(data){
                $("#tableGastosLab").html(data);
                $('#div_carga').hide();
              },
              error : function(data){
                 $('#div_carga').hide();
              },
              beforeSend : function(){
                $('#div_carga').show(); 
              }
            });
        });
        /* Fin Agregar Eliminar Gastos Lab*/

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('editarDetallePropuesta/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        };     

        $('#frmUploadFiles').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $("#tipohoja_up").val($("#tipohoja").val());
            $("#paquete_up").val($("#paqpropuesta").val());
            var fd = new FormData(document.getElementById('frmUploadFiles'));
            
            
            
            $.ajax({

                type:"POST",
                url:'uploadFile',
                data: fd,
                processData : false,
                contentType :  false,
                beforeSend: function () {
                  $("#uploadFile").modal('hide');
                  $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     
                },
                error: function(data){
                    $('#div_carga').hide();
                }
            });
        });
        function getTree() {
          // Some logic to retrieve, or generate tree structure
          var tree = [
          <?php echo (isset($estructura)==1?$estructura:'') ?>
          
          ];          
          return tree;
        }

        function getTreeLab() {
          // Some logic to retrieve, or generate tree structure
          var tree = [
          <?php echo (isset($estructuraLab)==1?$estructuraLab:'') ?>
          
          ];          
          return tree;
        }        

        $("#frmpropuesta").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarDetallePropuesta',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });        
        function profesional_senior(id){
          $.ajaxSetup({
              header: document.getElementById('_token').value
          });          
          $.ajax({
            type: 'GET',
            url : 'editHorasSeniorPropuesta',
            data: "cpropuesta="+$("#cpropuesta").val()+"&cestructurapropuesta="+id,
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divConteHoras").html(data);
              $("#divEditarHoras").modal();
            },
            error: function(data){
              $('#div_carga').hide();
            }
          });
          
        }
        function save_profesional_senior(){
          $.ajaxSetup({
              header: document.getElementById('_token_horas').value
          });         
          //alert($("#frmhoras").serialize());
          $.ajax({
            type: 'POST',
            url : 'saveHorasSeniorPropuesta',
            data: $("#frmhoras").serialize(),
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divEditarHoras").modal('hide');
              verHorasDetalle();
            },
            error: function(data){
               $('#div_carga').hide();
               $("#divEditarHoras").modal('hide');
            }
          });
          
        }

        function profesional_disciplina(id,id_dis){
          $.ajaxSetup({
              header: document.getElementById('_token').value
          });  
          //alert(id_dis);
          $.ajax({
            type: 'GET',
            url : 'editHorasDisciplinaPropuesta',
            data: "cpropuesta="+$("#cpropuesta").val()+"&cestructurapropuesta="+id+"&cdisciplina="+id_dis,
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divConteHorasDis").html(data);
              $("#divEditarHorasDis").modal();
            },
            error: function(data){
              $('#div_carga').hide();
            }
          });
        }
        function save_profesional_disciplina(){
          $.ajaxSetup({
              header: document.getElementById('_token_horas_dis').value
          });         
          
          $.ajax({
            type: 'POST',
            url : 'saveHorasDisciplinaPropuesta',
            data: $("#frmhorasDis").serialize(),
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divEditarHorasDis").modal('hide');
              verHorasDetalle();
            },
            error: function(data){
               $('#div_carga').hide();
               $("#divEditarHorasDis").modal('hide');
            }
          });
          
        }        
        function profesional_apoyo(id){
          $.ajaxSetup({
              header: document.getElementById('_token').value
          });          
          $.ajax({
            type: 'GET',
            url : 'editHorasApoyoPropuesta',
            data: "cpropuesta="+$("#cpropuesta").val()+"&cestructurapropuesta="+id,
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divConteHorasApo").html(data);
              $("#divEditarHorasApo").modal();
            },
            error: function(data){
              $('#div_carga').hide();
            }
          });
        }

        function save_profesional_apoyo(){
          $.ajaxSetup({
              header: document.getElementById('_token_horas_apo').value
          });         
          
          $.ajax({
            type: 'POST',
            url : 'saveHorasApoyoPropuesta',
            data: $("#frmhorasApo").serialize(),
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divEditarHorasApo").modal('hide');
              verHorasDetalle();
            },
            error: function(data){
               $('#div_carga').hide();
               $("#divEditarHorasApo").modal('hide');
            }
          });
          
        }   

        function verHorasDetalle(){
          $.ajax({
            type: 'GET',
            url : 'verHorasDetalleActividadPropuesta/'+$("#cpropuesta").val(),
            data: "",
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divActi").html(data);
            },
            error: function(data){
               $('#div_carga').hide();
               
            }
          });          
        }     
        

        function verHorasDetalle(){
          $.ajax({
            type: 'POST',
            url : 'verDetalleActividadPropuesta',
            data: $("#frmpropuesta").serialize(),
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divActi").html(data);
            },
            error: function(data){
               $('#div_carga').hide();
               
            }
          });          
        }           
        function setCheck(chk){
            //alert($(":checkbox").length);
            $("input[name='sel_cpropuestaactividades[]']").each(function(){
                /*if (this.checked) {
                    
                }*/
                //alert(this.checked);
                this.checked = chk.checked;
            }); 
            
            return;
        }   
        $("#btnDelAct").on('click',function(e){
            $("input[name='sel_cpropuestaactividades[]']").each(function(){
                if (this.checked) {
                  $("#"+this.value).css('display','none');
                  $("#remove_"+this.value).val("S");
                }
                
                
            });          
        });

        function autoSave(){
            $.ajax({

                type:"POST",
                url:'grabarDetallePropuesta',
                data:$("#frmpropuesta").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     setTimeout(autoSave,900000);
                     //$("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    /*$('#detalle_error').html(data);
                    $("#div_msg_error").show();*/
                }
            });          
        }

        //setTimeout(autoSave,900000);
        
</script>
