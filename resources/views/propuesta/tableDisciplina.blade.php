          <table class="table table-striped">
            <thead>
              <tr class="clsCabereraTabla">
                <th>Disciplina</th>
                <th>RDP</th>
              </tr>
            </thead>
              <tbody>
              <?php $i=0; ?>
              @if(isset($propuestadisciplina)==1)
              @foreach($propuestadisciplina as $d)
                <?php $activado=false; ?>
                <?php $cpersona_rdp=''; ?>
                <?php $descripcion=''; ?>
                <?php $name_rdp=''; ?>
                <?php $i++; ?>
                <tr>
                  <td>
                        <?php $cpersona_rdp = $d['cpersona_rdp']; ?>
                        <?php $name_rdp = $d['name_rdp']; ?>
                        {{ $d['descripcion'] }}
                  </td>              
                  <td>
                        {{ $name_rdp }}
                  </td>
                </tr>
              @endforeach        
              @endif                	
              </tbody>
            </table> 