@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<input type="hidden" id="cpropuesta" name="cpropuesta" value="{{ $propuesta->cpropuesta }}">
{{ csrf_field() }}

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Gastos Laboratorios</h3>
  </div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-10 col-md-offset-1 alert alert-info" id="divInfoNuevo" style="display: none">
  			Info: No se ha tiene registro de gastos, por lo tanto, se está mostrando la plantilla completa. Puede eliminar los elementos que no necesite.
  		</div>
  	</div>  	
  	<div class="row">
	  	<div class="col-md-12" id="divGastos">
	  		<table id="tbLista" class="table">
	  			<thead>
	  				<th>#</th>
	  				<th>Ítem</th>
	  				<th>Descripción</th>
	  				<th>Unidad</th>
	  				<th>C Cimentación</th>
	  				<th>C Canteras</th>
	  				<th>Costo unitario US$</th>
	  				<th>Subtotal</th>
	  				<th>Comentarios</th>
	  				<th>Acciones</th>
	  			</thead>
	  			<tbody></tbody>
	  		</table>
	  	</div>
	</div>
	@include('propuesta.ejecutar.adicional.botones_inferiores')
  </div>
</div>

@stop

@section('extra-css')
@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuesta/gastosLab.js') }}" ></script>
@stop
