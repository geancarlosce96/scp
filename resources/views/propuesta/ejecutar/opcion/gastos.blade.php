@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<input type="hidden" id="cpropuesta" name="cpropuesta" value="{{ $propuesta->cpropuesta }}">
{{ csrf_field() }}

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Gastos</h3>
  </div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-2">
	  		<button type="button" class="btn btn-primary" id="btn-agregarItem" >Agregar otro Item</button>
		</div>
  		<div class="col-md-9 alert alert-info" id="divInfoNuevo" style="display: none">
  			Info: No se ha tiene registro de gastos, por lo tanto, se está mostrando la plantilla completa. Puede eliminar los elementos que no necesite.
  		</div>
  	</div>	
  	<div class="row">
	  	<div class="col-md-12" id="divGastos">
	  		<table id="tbLista" class="table">
	  			<thead>
	  				<th>#</th>
	  				<th>Ítem</th>
	  				<th>Descripción</th>
	  				<th>Unidad</th>
	  				<th>Cantidad</th>
	  				<th>Costo unitario US$</th>
	  				<th>Subtotal US$</th>
	  				<th>Comentarios</th>
	  				<th>Acciones</th>
	  			</thead>
	  			<tbody></tbody>
	  		</table>
	  	</div>
	</div>
	@include('propuesta.ejecutar.adicional.botones_inferiores')
  </div>
</div>

<div class="modal fade" id="modalAgregarItem" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Agregar Item</h4>
      </div>
      <div class="modal-body">
        
        <form class="form-horizontal">
		  <div class="form-group">
		    <label for="descripcion" class="col-sm-2 control-label">descripcion</label>
		    <div class="col-sm-10">
		      <input type="email" class="form-control" id="txtdescripcion" placeholder="descripcion">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="unidad" class="col-sm-2 control-label">unidad</label>
		    <div class="col-sm-10">
		      <input type="email" class="form-control" id="txtunidad" placeholder="unidad">
		    </div>
		  </div>
		  <div class="form-group">
		    <label for="padre" class="col-sm-2 control-label">padre</label>
		    <div class="col-sm-10">		      
		      <select id="cbpadre">
		      	<option value="0">(Ninguno)</option>
		      	@foreach ($gastos as $g)
		      	<option value="{{$g->cconcepto}}">{{$g->descripcion}}</option>
		      	@endforeach
		      </select>
		    </div>
		  </div>
		</form>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn-registrarItem">Guardar</button>
      </div>
    </div>
  </div>
</div>

@stop

@section('extra-css')
@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuesta/gastos.js') }}" ></script>
@stop
