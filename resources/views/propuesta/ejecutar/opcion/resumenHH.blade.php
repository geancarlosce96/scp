@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<input type="hidden" id="cpropuesta" name="cpropuesta" value="{{ $propuesta->cpropuesta }}">

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Resumen HH</h3>
  </div>
  <div class="panel-body">
  	
  	<table class="table table-striped table-hover" id="tbList">
  			<thead>
  				<tr>	  				
	  				<th>Categorias</th>
	  				
	  				@foreach($disciplinas as $disciplina)
	  				<th>{{ $disciplina->descripcion }}</th>
	  				@endforeach

	  				<th>TOTAL HORAS</th>
	  				<th>COSTO TOTAL</th>
  				</tr>
  			</thead>
  			<tbody>
  				@foreach($profesiones as $profesion)
  				<tr>  					
  					<td>{{ $profesion->nombre }}</td>
  					
  					@foreach($disciplinas as $disciplina)
  					<td>              
              <label id="{{ $profesion->id }}_{{ $disciplina->cdisciplina }}" >0</label>
            </td>
  					@endforeach

  					<td>
                 <label id="{{ $profesion->id }}_total" >0</label>  
            </td>
  					<td>0</td>
  				</tr>
  				@endforeach  				
  			</tbody>
  		</table>

  </div>
</div>

@stop

@section('extra-css')
@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuesta/resumenHH.js') }}" ></script>
@stop
