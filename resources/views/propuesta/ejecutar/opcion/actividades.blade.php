@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<input type="hidden" id="cpropuesta" name="cpropuesta" value="{{ $propuesta->cpropuesta }}">
{{ csrf_field() }}

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Actividades</h3>
  </div>
  <div class="panel-body">
  	<div class="row">
  		<div class="col-md-5">
  			Actividad Disponibles  			
  		</div>
  		<div class="col-md-5 col-md-offset-2">
  			Actividades Seleccionadas
  		</div>
  	</div>
  	<div class="row">
	  	<div class="col-md-5">
	  		<form id="s">
			  <input type="search" id="q" />
			  <button type="submit">buscar</button>
			  *puede también presionar enter para buscar
			</form>
	  		<div id="arbolTotal" class="demo"></div>
	  	</div>	  	
	  	<div class="col-md-2">
	  		<button class="btn btn-default" id="btn-agregar" >agregar seleccionadas >></button>
	  		<button class="btn btn-default" id="btn-limpiar" >limpiar selección</button>
	  	</div>
	  	<div class="col-md-5" id="divArbol">
	  		<div id="arbol" class="demo"></div>
	  	</div>
  	</div>
  	@include('propuesta.ejecutar.adicional.botones_inferiores')
  </div>
</div>

@stop

@section('extra-css')
	<link rel="stylesheet" href="{{ asset('tree/style.min.css') }}"/>
@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuesta/actividades.js') }}" ></script>
	<script type="text/javascript" src="{{ asset('js/jstree.js') }}" ></script>
@stop
