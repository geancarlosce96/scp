@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<input type="hidden" id="cpropuesta" name="cpropuesta" value="{{ $propuesta->cpropuesta }}">
{{ csrf_field() }}

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Horas</h3>
  </div>
  <div class="panel-body">
    <p>Se está grabando automaticamente.</p>
  	<div id="divTabla" style="overflow: scroll">
      <div id="my"></div>
  	</div>

    @include('propuesta.ejecutar.adicional.botones_inferiores')

  </div>
</div>

@stop

@section('extra-css')
<link rel="stylesheet" type="text/css" href="{{ asset('css/propuestas/horas.css') }}">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jexcel/1.5.7/css/jquery.jexcel.min.css" type="text/css" />
@stop

@section('js')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jexcel/1.5.7/js/jquery.jexcel.js"></script>
  <script type="text/javascript" src="{{ asset('js/propuesta/horas.js') }}" ></script>
@stop
