@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Resumen Global</h3>
  </div>
  <div class="panel-body">
  	
  	<div>
  		<table class="table table-striped table-hover" id="tbList">
  			<thead>
  				<tr>	  				
	  				<th>Descripción de la actividad</th>
	  				<th>Horas</th>
	  				<th>Unidad</th>
	  				<th>Parcial US$</th>
	  				<th>Gastos Administrativos</th>
	  				<th>Subtotal</th>
	  				<th>Total</th>
  				</tr>
  			</thead>
  			<tbody>
  				@foreach($actividades as $actividad)
  				<tr class="actividad">			
  					<td>{{ $actividad->descripcionactividad }}</td>  					
            <td><input class="horas" type="text" style="width: 35px" readonly value="{{ $actividad->total }}" ></td>
  					<td>Global</td>
  					<td><input class="parcial" type="text" value="0" style="width: 35px" ></td>
  					<td><input class="porcentaje" type="text" value="0" style="width: 35px"> %</td>
  					<td><input class="subtotal" type="text" value="0" style="width: 35px" readonly></td>
  					<td><input class="total" type="text" style="width: 35px" readonly></td>            
  				</tr>
  				@endforeach
  			</tbody>
  		</table>
  	</div>

  </div>
</div>

@stop

@section('extra-css')
@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuesta/resumenGlobal.js') }}" ></script>
@stop
