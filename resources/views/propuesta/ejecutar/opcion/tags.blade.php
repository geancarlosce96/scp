@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Tags</h3>
  </div>
  <div class="panel-body">
  	
  	<div class="row">
						<div class="col-md-1 col-md-offset-1" >
							Estructuras
						</div>
						<div class="col-md-3" >
						</div>				
						<div class="col-md-1 col-md-offset-1" >
							Servicios
						</div>
						<div class="col-md-2" >							 
						</div>
						<div class="col-md-1" >
						</div>
					</div>
				
				<form id="form" method="POST" action='{{ url("propuesta/$propuesta->cpropuesta/ejecutar/tagsGuardar") }}'>
					<input type="hidden" id="cpropuesta" name="cpropuesta" value="{{ $propuesta->cpropuesta }}">
					{{ csrf_field() }}

					<div class="row">
						<div class="col-md-3 col-md-offset-2" id="divTagEstructuras" style="margin-top: 20px">
							<div class="panel panel-primary">							    
							    
							    <div class="panel-group" id="accordionT" role="tablist" aria-multiselectable="true">
									
									<div class="panel panel-default">
									    <div class="panel-heading" role="tab" id="divT">
									      <h4 class="panel-title">
									        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#divTag" aria-expanded="true" aria-controls="divTag">
									          Tags
									        </a>
									      </h4>
									    </div>
									    <div id="divTag" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="divT">
									      <div class="panel-body">
									        @foreach($tag_estructuras as $tag)
												
												<?php if(in_array($tag->id, $tag_estructuras_seleccionados)) {?> 
													<input type="checkbox" name="chk_tag_estructuras[]" value="{{ $tag->id }}" checked="checked">
												<?php } else { ?>
													<input type="checkbox" name="chk_tag_estructuras[]" value="{{ $tag->id }}">
												<?php } ?>

												{{ $tag->descripcion }} ({{ $tag->tag }}) <br>
											@endforeach
									      </div>
									    </div>
									</div>
									
								</div>

							</div>
						</div>
						<div class="col-md-3 col-md-offset-1" id="divTagServicios" style="margin-top: 20px" >

							<br>

							<div class="panel panel-primary">

								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									@foreach($tag_servicios as $grupo)
									@if( count($grupo->items)>0 )
									<div class="panel panel-default">
									    <div class="panel-heading" role="tab" id="{{ $grupo->abrevia }}">
									      <h4 class="panel-title">
									        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$grupo->cdisciplina}}" aria-expanded="true" aria-controls="{{$grupo->cdisciplina}}">
									          {{ $grupo->descripcion }}
									        </a>
									      </h4>
									    </div>
									    <div id="{{$grupo->cdisciplina}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ $grupo->abrevia }}">
									      <div class="panel-body">
									        @foreach($grupo->items as $tag)
												<?php if(in_array($tag->id, $tag_servicios_seleccionados)) {?> 
													<input type="checkbox" name="chk_tag_servicios[]" value="{{ $tag->id }}" checked="checked">
												<?php } else { ?>
													<input type="checkbox" name="chk_tag_servicios[]" value="{{ $tag->id }}">
												<?php } ?>
												{{ $tag->descripcion }} ({{ $tag->tag }}) <br>
											@endforeach
									      </div>
									    </div>
									</div>
									@endif
									@endforeach
								</div>

							</div>

						</div>

						<div class="col-md-2" id="divTagServicios" style="margin-top: 20px" >
							<p>Tags seleccionados: </p>
							@foreach($tag_servicios as $grupo)
								@foreach($grupo->items as $tag)
									<?php if(in_array($tag->id, $tag_servicios_seleccionados)) {?> 
									{{ $tag->descripcion }} ({{ $tag->tag }}) <br>
									<?php } ?>
								@endforeach
							@endforeach
						</div>
					</div>

					@include('propuesta.ejecutar.adicional.botones_inferiores')
				</form>

  </div>
</div>

@stop

@section('extra-css')
@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuesta/tags.js') }}" ></script>
@stop
