@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<input type="hidden" id="url" value='{{ url("propuesta/$propuesta->cpropuesta/ejecutar/disciplinasGuardar") }}'>
<input type="hidden" id="cpropuesta" value="{{ $propuesta->cpropuesta }}">
{{ csrf_field() }}

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Disciplinas</h3>
  </div>
  <div class="panel-body">
  	<div class="col-md-12" style="margin-bottom: 10px">
  		*Para agregar una disciplina, hacer doble click.
  	</div>
  	<div class="col-md-6">
  		<b>Disciplinas Actuales</b>
  	</div>
  	<div class="col-md-6">
  		<b>Disciplinas Seleccionadas</b>
  	</div>
  	
    <div class="col-md-6">
  		<div class="list-group" id="ListaDisciplinas">
  		@foreach ($disciplinas as $disciplina)
		  <?php if(!in_array($disciplina->cdisciplina, $disciplinasSeleccionadas)) { ?>
        <button type="button" data-id="{{ $disciplina->cdisciplina }}" class="list-group-item disciplina">
          {{ $disciplina->codigo }} - 
          {{ $disciplina->descripcion }}
        </button>
        <?php } ?>
  		@endforeach
  		</div>
  	</div>

  	<div class="col-md-6">
  		<div class="list-group" id="ListaSeleccionadas">
        @foreach ($disciplinas as $disciplina)
        <?php if(in_array($disciplina->cdisciplina, $disciplinasSeleccionadas)) { ?>
        <button type="button" data-id="{{ $disciplina->cdisciplina }}" class="list-group-item disciplina">
          {{ $disciplina->codigo }} - 
          {{ $disciplina->descripcion }}
        </button>
        <?php } ?>
        @endforeach
  		</div>
  	</div>
    
    @include('propuesta.ejecutar.adicional.botones_inferiores')

  </div>
</div>

@stop

@section('extra-css')	
	
@stop

@section('js')	
	<script src="{{ asset('js/propuesta/disciplinas.js') }}" ></script>
@stop
