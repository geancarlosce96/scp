@extends('propuesta.ejecutar.adicional.plantilla')

@section('content_plantilla')

<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Selección de Equipo</h3>
  </div>
  <div class="panel-body">
    <form id="form" method="POST" action='{{ url("propuesta/$propuesta->cpropuesta/ejecutar/equipoGuardar") }}'>		
		<input type="hidden" id="cpropuesta" name="cpropuesta" value="{{ $propuesta->cpropuesta }}">
		{{ csrf_field() }}

					<div class="col-md-4">
						<label>Gerente del proyecto</label>
						<select class="form-control" id="gerentes" name="gerentes">
							<option value="0">Seleccionar</option>
							@foreach($gerentes as $lista)
								@if($propuesta->cpersona_gteproyecto == $lista->cpersona)
								<option value="{{ $lista->cpersona }}" selected>{{ $lista->abreviatura }}</option>
								@else
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<label>Responsable de Propuesta</label>
						<select class="form-control" id="responsable" name="responsable">
							<option value="0">Seleccionar</option>
							@foreach($responsable as $lista)
								@if($propuesta->cpersona_resppropuesta == $lista->cpersona)
								<option value="{{ $lista->cpersona }}" selected>{{ $lista->abreviatura }}</option>
								@else
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<label>Coordinador de Propuesta</label>
						<select class="form-control" id="coordinador" name="coordinador">
							<option value="0">Seleccionar</option>
							@foreach($coordinador as $lista)
								@if($propuesta->cpersona_coordinadorpropuesta == $lista->cpersona)
								<option value="{{ $lista->cpersona }}" selected>{{ $lista->abreviatura }}</option>
								@else
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<label>Revisor Final</label>
						<select class="form-control" id="revisor" name="revisor">
							<option value="0">Seleccionar</option>
							@foreach($revisor as $lista)
								@if($propuesta->cpersona_revprincipal == $lista->cpersona)
								<option value="{{ $lista->cpersona }}" selected>{{ $lista->abreviatura }}</option>
								@else
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<label>Responsables Técnicos</label>
						<select class="form-control" multiple="multiple" id="responsables" name="responsables[]">
							<option value="0">Seleccionar</option>
							@foreach($responsable as $lista)
								<?php if(in_array( $lista->cpersona, $propuesta_tecnicos)) { ?>
								<option value="{{ $lista->cpersona }}" selected>{{ $lista->abreviatura }}</option>
								<? } else { ?>
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
								<?php } ?>
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<label>Revisor Propuesta</label>
						<select class="form-control" id="revisor_propuesta" name="revisor_propuesta">
							<option value="0">Seleccionar</option>
							@foreach($revisor as $lista)
								@if($propuesta->cpersona_revisor_propuesta == $lista->cpersona)
								<option value="{{ $lista->cpersona }}" selected>{{ $lista->abreviatura }}</option>
								@else
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
								@endif
							@endforeach
						</select>
					</div>
					<div class="col-md-4">
						<label>Revisor Proyecto</label>
						<select class="form-control" id="revisor_proyecto" name="revisor_proyecto">
							<option value="0">Seleccionar</option>
							@foreach($revisor as $lista)
								@if($propuesta->cpersona_revisor_proyecto == $lista->cpersona)
								<option value="{{ $lista->cpersona }}" selected>{{ $lista->abreviatura }}</option>
								@else
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
								@endif
							@endforeach
						</select>
					</div>

			@include('propuesta.ejecutar.adicional.botones_inferiores')
	</form>
  </div>
</div>

@stop

@section('extra-css')
@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuesta/equipo.js') }}" ></script>
@stop
