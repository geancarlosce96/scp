<div class="row" style="font-size: 12px; margin-bottom: 20px;">
<div class="col-md-12">
		
<div class="btn-group btn-group-justified" role="group" aria-label="...">  
  <div class="btn-group" role="group">
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/equipo') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->equipo==1) btn-primary @else btn-default @endif btn-lg">
      @if($propuesta->opciones != null and $propuesta->opciones->equipo==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br>   EQUIPO
   </a>
  </div>
  <div class="btn-group" role="group">    
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/disciplinas') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->disciplinas==1) btn-primary @else btn-default @endif btn-lg">
      @if($propuesta->opciones != null and $propuesta->opciones->disciplinas==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br>   DISCIPLINAS
   </a>
  </div>
  <div class="btn-group" role="group">    
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/tags') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->tags==1) btn-primary @else btn-default @endif btn-lg">
    	@if($propuesta->opciones != null and $propuesta->opciones->tags==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br> 	TAGS
	 </a>
  </div>
   <div class="btn-group" role="group">    
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/actividades') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->actividades==1) btn-primary @else btn-default @endif btn-lg">
    	@if($propuesta->opciones != null and $propuesta->opciones->actividades==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br> 	ACTIVIDADES
	 </a>
  </div>
   <div class="btn-group" role="group">    
   <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/horas') }}" 
   class="btn @if($propuesta->opciones != null and $propuesta->opciones->horas==1) btn-primary @else btn-default @endif btn-lg">
    	@if($propuesta->opciones != null and $propuesta->opciones->horas==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br> 	HORAS
	 </a>
  </div>
   <div class="btn-group" role="group">    
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/gastos') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->gastos==1) btn-primary @else btn-default @endif btn-lg">
      @if($propuesta->opciones != null and $propuesta->opciones->gastos==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br>   GASTOS
   </a>
  </div>
  <div class="btn-group" role="group">    
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/gastosLaboratorios') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->gastoslaboratorios==1) btn-primary @else btn-default @endif btn-lg">
      @if($propuesta->opciones != null and $propuesta->opciones->gastoslaboratorios==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br>   GASTOS LAB
   </a>
  </div>
   <div class="btn-group" role="group">    
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/resumenGlobal') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->resumenGlobal==1) btn-primary @else btn-default @endif btn-lg">
      @if($propuesta->opciones != null and $propuesta->opciones->resumenGlobal==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br>   RESUMEN G.
   </a>
  </div>
   <div class="btn-group" role="group">    
    <a href="{{ url('propuesta/'.$propuesta->cpropuesta.'/ejecutar/resumenHH') }}" 
    class="btn @if($propuesta->opciones != null and $propuesta->opciones->resumenHH==1) btn-primary @else btn-default @endif btn-lg">
      @if($propuesta->opciones != null and $propuesta->opciones->resumenHH==1)
        <span class="glyphicon glyphicon-check" aria-hidden="true"></span>
      @else
        <span class="glyphicon glyphicon-unchecked" aria-hidden="true"></span>
      @endif
      <br>   RESUMEN HH
   </a>
  </div>
</div>

</div>

</div>