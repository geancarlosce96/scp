<div class="row" style="margin-bottom: 10px;">
	<div class="col-md-2">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Código</label>
	       </span>
	       <input class="form-control input-sm" readonly="true" name="codigo" value="1010.10.05" type="text">
	   </div>
	</div>
	<div class="col-md-4">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Nombre</label>
	       </span>
	       <input class="form-control input-sm" readonly="true" name="nombre" value="Redacción Anddes" type="text">
	   </div>
	</div>
	<div class="col-md-3">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Cliente</label>
	       </span>
	       <input class="form-control input-sm" readonly="true" name="cliente" value="Anddes Asociados SAC" type="text">
	   </div>
	</div>
	<div class="col-md-3">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Unidad Minera</label>
	       </span>
	       <input class="form-control input-sm" readonly="true" name="uminera" value="Anddes Perú" type="text">
	   </div>
	</div>	
</div>
