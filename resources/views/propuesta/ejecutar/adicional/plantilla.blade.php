@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
		<h1>
			Propuesta
			<small>Ejecutar</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
			<li>Propuestas</li>
			<li class="active">Ejecutar</li>
		</ol>
</section>

<section class="content">
	@include('propuesta.ejecutar.adicional.opcion')
	@include('propuesta.ejecutar.adicional.info')	
	@yield('content_plantilla')	
</section>

</div>
	
@stop
