<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Propuesta
        <small>Listado de Propuesta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active">Propuesta</li>
      </ol>
    </section>


    <div class="alert alert-info alert-dismissable" hidden>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
      <strong>¡Información!</strong> Seleccione LA PROPUESTA  para Editar.
    </div>


    <!-- Main content -->
    <section class="content">
    <div class="row">
      <!--<div class="col-lg-1 hidden-xs"></div>-->
      <div class="col-lg-12 col-xs-12">

      	<!--<div class="row">
              <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
              <div class="col-lg-6 col-xs-12 clsPadding"><input id="txtbuscar" class="form-control input-sm" type="text" placeholder="Cliente"></div>
              <div class="col-lg-3 col-xs-12 clsPadding"><a href="#" onclick="postUrl('buscarPropuesta',getParametrosBusqueda(),'#resultado');" class="btn btn-primary btn-block "> <b>Buscar</b></a></div>
          </div>-->

          <div class="row">
              <div class="col-lg-2 col-xs-12 clsPadding"><button type="button" onclick="getUrl('registroPropuesta');" class="btn btn-primary btn-block"><b>Nuevo</b></button> </div>
              <div class="col-lg-2 col-xs-12 clsPadding"><button type="button" onclick="goEditar();" class="btn btn-primary btn-block"><b>Editar</b></button> </div>
              <div class="col-lg-2 col-xs-12 clsPadding"><button type="button" href="#" class="btn btn-primary btn-block"><b>Nueva Revisión</b></button> </div>
              <div class="col-lg-2 col-xs-12 clsPadding"><button type="button" class="btn btn-primary btn-block" disabled><b>Transmittals</b></button> </div>
              <div class="col-lg-2 col-xs-12 clsPadding"><button type="button" class="btn btn-primary btn-block" disabled><b>Imprimir</b></button> </div> 
              <div class="col-lg-2 col-xs-12 clsPadding"></div>           
          </div>

      </div>
      <!--<div class="col-lg-1 hidden-xs"></div>-->
    </div>
    <!--  tabla -->
      <!-- Table row -->
      <div class="row clsPadding" style="margin-top:10px;">
      <div class="row clsPadding" style="background-color:#FFF;">
      	<!--<div class="col-lg-1 hidden-xs"></div>-->
        <div class="col-lg-12 col-xs-12 table-responsive">
          <table class="table table-striped" id="listado">
            <thead>
            <tr class="clsCabereraTabla">
              <th>ID</th>	
              <th>Código</th>
              <th>Cliente</th>
              <th>Unidad Minera</th>
              <th>Nombre</th>
              <th>Disciplina</th>
              <th>Estado</th>
              <th>Revisión</th>
              <th>GP</th>
            </tr>
            </thead>
            <tfoot>
            <tr class="clsCabereraTabla">
              <th>ID</th>  
              <th>Código</th>
              <th>Cliente</th>
              <th>Unidad Minera</th>
              <th>Nombre</th>
              <th>Disciplina</th>
              <th>Estado</th>
              <th>Revisión</th>
              <th>GP</th>
            </tr>
            </tfoot>

          </table>
        </div>
        <!--<div class="col-lg-1 hidden-xs"></div>-->
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->    
    
    <!-- Fin de tabla -->
    </section>
    <!-- /.content -->
  </div>

  <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    var selected =[];
    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarPropuestas',
      "columns" : [
        {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
        {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
        {data : 'cliente' , name : 'tper.nombre'},
        {data : 'uminera' , name : 'tu.nombre'}, 
        {data : 'nombre' , name : 'tpropuesta.nombre'},
        {data : 'disciplina' , name : 'tpropuesta.revision'},
        {data : 'descripcion', name : 'ep.descripcion'},
        {data : 'revision' , name : 'tpropuesta.revision'},
        {data : 'gteProyecto' , name : 'p.nombre'}
      ],

      "rowCallback" : function( row, data ) {
          
          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });

    
    $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/
        
        $(this).toggleClass('selected');
    });

  function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(4));
        getUrl('editarPropuesta/'+id.substring(4),'');
    }else{
        $('.alert').show();
    }
  }
</script>