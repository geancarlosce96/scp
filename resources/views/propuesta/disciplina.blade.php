<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Propuesta
        <small>Seleccionar disciplinas participantes</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuesta</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoPropuesta')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>    
        {!! Form::open(array('url' => 'grabarDisciplinaPropuesta','method' => 'POST','id' =>'frmDisciplina')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('sel_rdp','',array('id'=>'sel_rdp')) !!}
        {!! Form::hidden('sel_name_rdp','',array('id'=>'sel_name_rdp')) !!}
        {!! Form::hidden('sel_prov','',array('id'=>'sel_prov')) !!}
        {!! Form::hidden('sel_name_prov','',array('id'=>'sel_name_prov')) !!}

    <div class="row clsPadding2">
      <!--<div class="col-lg-1 hidden-xs"></div>-->
      <div class="col-lg-12 col-xs-12">
      <div class="row">
        {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:'')) !!}
        <div class="col-md-2 col-xs-2 ">Cliente:</div>
        <div class="col-md-9 col-xs-9 ">{!! Form::text('nombrecliente',(isset($persona->nombre)?$persona->nombre:''),array('class'=>'form-control input-sm','disabled' => 'true')) !!}</div>
        
        
        <div class="col-md-1 col-xs-1 ">
        <button type='button' class="btn btn-primary btn-sm" data-toggle='modal' data-target='#searchPropuesta'><b>...</b></button>
        </div>
      </div>
      <div class="row">
        <div class="col-lg-2 col-xs-2 ">Unidad minera:</div>
        <div class="col-lg-4 col-xs-4 ">{!! Form::text('nombreuminera',(isset($uminera->nombre)==1?$uminera->nombre:''),array('disabled'=>'','class'=> 'form-control input-sm','placeholder'=>'Nombre Unidad Minera')) !!}</div>
         
        <div class="col-lg-2 col-xs-2 ">Código Propuesta:</div>
        <div class="col-lg-4 col-xs-4 ">
            {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)?$propuesta->ccodigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}    
        </div>
        
      </div> 
      <div class="row ">
          <div class="col-lg-2 col-xs-2">Nombre de la Propuesta:</div>
          <div class="col-lg-4 col-xs-4">
              {!! Form::text('nombrepropuesta',(isset($propuesta->nombre)==1?$propuesta->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de la Propuesta')) !!}
          </div>	
          <div class="col-lg-2 col-xs-2">Responsable Propuesta:</div>
          <div class="col-lg-4 col-xs-4">
              {!! Form::text('propuestaresp',(isset($personaresp->nombre)==1?$personaresp->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Responsable de Propuesta')) !!} 
          </div>	         
      </div>   
      <div class="row ">
          <div class="col-lg-2 col-xs-2">Tipo de servicio:</div>
          <div class="col-lg-4 col-xs-4">
            {!! Form::select('servicio',$servicios,(isset($propuesta->cservicio)==1?$propuesta->cservicio:''),array('class' => 'form-control','disabled'=>'true')) !!}
          </div>   
          <div class="col-lg-2 col-xs-2">Revisión:</div>
          <div class="col-lg-4 col-xs-4">
            {!! Form::text('revision',(isset($propuesta->revision)==1?$propuesta->revision:''),array('class'=>'form-control input-sm','disabled' =>'')) !!}         
          </div>            
      </div>
       
      <div class="row clsPadding">
      	<div class="col-md-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
      </div>
      <div class="row ">
      	<div class="col-md-12 col-xs-12" ><button type="button" class="btn btn-primary btn-sm" id="btnAdd"
              @if(isset($bsave))
                @if($bsave)
                @else
                  disabled   
                @endif
              @else 
                disabled
              @endif        
        >Agregar Disciplinas</button></div>
      </div>
    
      <div class="row">	
        <div class="col-lg-12 col-xs-12 table-responsive" id="divTable">
          @include('propuesta.tableDisciplina',array('propuestadisciplina'=>(isset($propuestadisciplina)==1?$propuestadisciplina:null) ) )
      	</div>
      </div>

    
    
    
    
    
      <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
      </div>
      <div class="row clsPadding2">
          <div class="col-lg-4 col-xs-4">
              <button class="btn btn-primary btn-block" style="width:200px;" 
              @if(isset($bsave))
                @if($bsave)
                @else
                  disabled   
                @endif
              @else 
                disabled
              @endif
              id='btnSave'><b>Grabar</b></button> 
          </div>        
      </div>
    
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






  </div>
  {!! Form::close() !!}
    </section>

    <!-- /.content -->

    <!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuestas</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuestas" class="table">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Cliente</th>
                  <th>Unidad Minera</th>
                  <th>Propuesta</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Modal Buscar Persona RDP-->
    <div id="searchRDP" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Asignar RDP</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tEmpleado" class="table">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Nombre</th>
                </thead>
              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="selRDP();">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
    <!-- Fin Buscar Personas RDP -->

    <div id="agregarDisciplina" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar Disciplinas</h4>
          </div>
          <div class="modal-body">
        {!! Form::open(array('url' => 'agregarDisciplina','method' => 'POST','id' =>'frmDisciplinaPersona')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_h')) !!}
		    {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta_h' )) !!}   
        <div class="row">
          <div class="col-md-5">
            <div class="panel-group" id="accordion_disciplinas">      <!--  Div Principal --> 
            @if(isset($disciplinas))
            <?php $i=0; ?>
            @foreach($disciplinas as $dis)
              <?php $i++; ?>
              <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion_disciplinas" href="#collapse{{ $i }}">
                    {{ $dis['descripcion'] }}</a>
                  </h4>
                </div>            
                <div id="collapse{{ $i }}" class="panel-collapse collapse">
                  <table class="table table-bordered">
                  <thead>
                    <th>Colaborador</th>
                    <th>Sel</th>
                  </thead>
                  <tbody>
                  <?php $da = $dis['da']; ?>
                  @foreach($da as $d)
                  <tr id="col_{{ $dis['cdisciplina'] }}_{{ $d->cpersona }}">
                    <td>
                    {{ $d->nombre }}
                    </td>
                    <td>
                    <input type='radio' name="rad_{{ $dis['cdisciplina'] }}" id="rad_{{ $dis['cdisciplina'] }}_{{ $d->cpersona }}" value="{{ $dis['cdisciplina'] }}_{{ $d->cpersona }}" />
                    </td>
                  </tr>
                  @endforeach
                  </tbody>
                  </table>
                </div>
              </div>
            @endforeach
            @endif
            </div> <!-- Fin Panel Principal -->
          </div> <!-- col-md-6  de disciplinas-->
          <div class="col-md-2">
              <br /><br /><br /><br /><br />
              <button type="button" id="btnAgregar" onclick="agregarDisciplinas()"><b>></b></button><br />
              <button type="button" id="btnBorrar" onclick="borrarDisciplinas()"><b>X</b></button>
          </div>
          <div class="col-md-5">
              <div class="rounded" style="width:100%;height:100% ;border: solid 1px blue" id="divDisSel">
                @include('propuesta.tableDisciplinaSeleccionado',array('propuestadisciplina'=>(isset($propuestadisciplina)==1?$propuestadisciplina:null) ) )
              </div>
          </div>
          </div> <!-- div row -->
          {!! Form::close() !!}
          </div>    
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
          </div>            
        </div>
      </div>
    </div>


    <!-- Modal Buscar Persona Proveedor-->
    <div id="searchProveedor" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Seleccionar Proveedor</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tProveedor" class="table">
                <thead>
                  <th>Item</th>
                  <th>Proveedor</th>
                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="selProv();">Aceptar</button>
          </div>
        </div>

      </div>
    </div>
    <!-- Fin Modal Buscar Persona Proveedor-->

</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
        /* buscar Propuestas */
        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';

        var table=$("#tPropuestas").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarPropuestas",
                "type": "GET",
            },
            "columns":[
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });


        $('#tPropuestas tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            var table = $('#tPropuestas').DataTable();
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        /* Fin Buscar Propuestas*/

        /*
        * @Descripcion 
        * @param
        * @return
        */
        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              //alert(id.substring(4));
              //$('#searchPropuesta').modal("hide");

              getUrl('editarDisciplinaPropuesta/'+id.substring(4),'');
              
              //alert(id.substring(4));
              
          }else{
              $('.alert').show();
          }
        }   
        $('#searchPropuesta').on('hidden.bs.modal', function () {
            
           goEditar();
        });
        /* Fin de Buscar Propuestas*/        

        /* Buscar RDP*/
        var selected_rdp =[];
        
        var table=$("#tEmpleado").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "buscarEmpleadosRDP",
                "type": "GET",
            },
            "columns":[
                {data : 'cpersona', name: 'tpersona.cpersona'},
                {data : 'codigo', name: 'emp.codigosig'},
                {data : 'nombre' , name : 'tpersona.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });


        $('#tEmpleado tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected_rdp);
            var table = $('#tEmpleado').DataTable();
            $('#' + this.id).each(function(indice, elemento) {
                //console.log('El elemento con el índice '+indice+' contiene '+$(elemento).html());
                $.each(elemento.cells, function (name , valor) { 
                        //console.log(name + '_' + valor.innerHTML);

                        if(name==0){
                            document.getElementById('sel_rdp').value=valor.innerHTML ;
                        }else if(name==2){
                            document.getElementById('sel_name_rdp').value=valor.innerHTML ;
                        }
                });
            });
            table.$('tr.selected').removeClass('selected');
            selected_rdp.splice(0,selected_rdp.length);
            if ( index === -1 ) {
                selected_rdp.push( id );

            } /*else {
                selected_rdp.splice( index, 1 );
            }*/
            

            $(this).toggleClass('selected');
        } ); 
        var objid='';
        function showRDP(id){
          objid=id;
          $('#searchRDP').modal('show');
        }
        function selRDP(){
          if (selected_rdp.length > 0 ){
            document.getElementById('cpersona_'+objid).value=document.getElementById('sel_rdp').value;
            document.getElementById('nomrdp_'+objid).value=document.getElementById('sel_name_rdp').value;
            $('#searchRDP').modal('hide');
          }

        }
        /* Fin Buscar RDP*/

        /* Buscar Proveedor*/
        var selected_prov =[];
        
        var table=$("#tProveedor").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "buscarProveedor",
                "type": "GET",
            },
            "columns":[
                {data : 'cpersona', name: 'tpersona.cpersona'},
                {data : 'nombre' , name : 'tpersona.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });


        $('#tProveedor tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected_prov);
            var table = $('#tProveedor').DataTable();
            $('#' + this.id).each(function(indice, elemento) {
                //console.log('El elemento con el índice '+indice+' contiene '+$(elemento).html());
                $.each(elemento.cells, function (name , valor) { 
                        //console.log(name + '_' + valor.innerHTML);

                        if(name==0){
                            document.getElementById('sel_prov').value=valor.innerHTML ;
                        }else if(name==1){
                            document.getElementById('sel_name_prov').value=valor.innerHTML ;
                        }
                });
            });
            table.$('tr.selected').removeClass('selected');
            selected_prov.splice(0,selected_prov.length);
            if ( index === -1 ) {
                selected_prov.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );

        var objidProv='';
        function showProv(id){
          objidProv=id;
          $('#searchProveedor').modal('show');
        }
        function selProv(){
          if (selected_prov.length > 0 ){
            document.getElementById('cpersona_prov_'+objidProv).value=document.getElementById('sel_prov').value;
            document.getElementById('nomprov_'+objidProv).value=document.getElementById('sel_name_prov').value;
            $('#searchProveedor').modal('hide');
          }

        }         
        /* Fin Buscar Proveedor*/

        /* Grabar frmDisciplina*/
        $('#frmDisciplina').on('submit',function(e){
          
            /*$.ajaxSetup({
                header: document.getElementById('_token').value
            });*/
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            
            
            $.ajax({

                type:"POST",
                url:'grabarDisciplinaPropuesta',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $.each(data.responseJSON, function (key, value) {
                            var input = '#frmDisciplina input[name=' + key + ']';
                            $(input + '+span>strong').text(value);
                            $(input).parent().parent().addClass('has-error');

                    });
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });        
        /* Fin Grabar FrmDisciplina*/
        $("#btnAdd").on('click',function (){
            $('#agregarDisciplina').modal('show');
        });
        $("#agregarDisciplina").on('show.bs.modal',function(){
              $('#frmDisciplinaPersona :input[type=checkbox]').each(function() 
              { 
                      this.checked = false; 
              });   
              $('#frmDisciplinaPersona :input[type=radio]').each(function() 
              { 
                      this.checked = false; 
              });                         
        });
        
        
        $("#agregarDisciplina").on('hidden.bs.modal',function(){
           $.ajax({

                type:"GET",
                url:'verDisciplinasPropuesta',
                
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     
                     $("#divTable").html(data);
                     
                }
            });                        
        });        
        function agregarDisciplinas(){
            $.ajax({

                type:"POST",
                url:'agregarDisciplinasPropuesta',
                data:$("#frmDisciplinaPersona").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     
                     $("#divDisSel").html(data);
                     
                }
            });
        }
        function borrarDisciplinas(){
            $.ajax({

                type:"POST",
                url:'borrarDisciplinasPropuesta',
                data:$("#frmDisciplinaPersona").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     
                     $("#divDisSel").html(data);
                     
                }
            });
        }
        
</script>