<!-- Modal -->
<div id="searchActividades" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color: white">Asociar actividades</h4>
      </div>
      <div class="modal-body">
        <div class="table-responsive" >
          <div class="pull-left col-lg-6 col-xs-6">
            <input type="hidden" id="cpropuesta" value="{{$cpropuesta}}">
            <table id="act_prop" class="table table-striped table-hover table-bordered table-responsive" style="font-size: 12px; width:100%;">
              <caption>Actividades de propuestas</caption>
              <thead>
                <tr class="clsCabereraTabla">
                  <th width="10%">Código de Asociación</th>
                  <th width="10%">Id</th>
                  <th width="80%">Descripción de la Actividad y/o tarea</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($act_ppa as $key => $ppa)
                <tr class="c_actpropuesta" data-actpropuesta='{{$ppa->cpropuestaactividades}}' data-acti_padre = '{{$ppa->padre}}' @if($ppa->padre == 'true') style="background-color:grey" @endif>
                  <td width="10%">{{$ppa->cpropuestaactividades}}</td>
                  <td width="10%">{{$ppa->codigoactividad}}</td>
                  <td width="80%">{{$ppa->descripcionactividad}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
            </div>
            <div class="pull-right col-lg-6 col-xs-6">
            @if($cproyecto)
              <input type="hidden" id="cproyecto" value="{{$cproyecto->cproyecto}}">
            @endif
            <table id="act_proy" class="table table-striped table-hover table-bordered table-responsive" style="font-size: 12px; width:100%;">
              <caption>Actividades de proyecto</caption>
              <thead>
                <tr class="clsCabereraTabla">
                  <th width="10%">Código de Asociación</th>
                  <th width="10%">Id</th>
                  <th width="80%">Descripción de la Actividad y/o tarea</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($act_pry as $key => $pry)
                <tr @if($pry->padre == 'disabled') style="background-color:grey" @endif>
                  <td width="10%">
                    @if($pry->padre == '') 
                    <input type="text" title="Ingrese código de la actividad para asociar" name="cod_asociado" class="asocia" id="{{$pry->cproyectoactividades}}" {{$pry->padre}} >
                    @endif
                  </td>
                  <td width="10%">{{$pry->codigoactvidad}}</td>
                  <td width="80%">{{$pry->descripcionactividad}}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="asociarahora">Aceptar</button>
      </div>
    </div>
  </div>
</div>
<!-- Fin Modal -->

<script type="text/javascript">

  var tachar = '';
  setTimeout(function datatableactividades(){
  var table = $('#act_prop').DataTable( {
    "bFilter": true,
    "bSort": true,
    "bPaginate": false,
    "bLengthChange": false,
    "bInfo": false,
    "order": [ 1, "asc" ],
    lengthChange: false,
    scrollY:        "600px",
    scrollCollapse: true,
      } ); 

  var table = $('#act_proy').DataTable( {
    "bFilter": true,
    "bSort": true,
    "bPaginate": false,
    "bLengthChange": false,
    "bInfo": false,
    "order": [ 1, "asc" ],
    lengthChange: false,
    scrollY:        "600px",
    scrollCollapse: true,
      } );
  },2000);

  $('#asociarahora').click(function(e) {
    asociar();
  });

  $('.c_actpropuesta').click(function(e) {
    tachar = $(this).data('actpropuesta');
    padre = $(this).data('acti_padre');
    $('#act_prop > tbody > tr').css('background', 'white');
    $(this).css('background', '#B8D3FFFF');
    if (padre === true) {
      swal("No puede asociar una actividad principal");
      $(this).css('background', 'white');
      tachar = '';
    } else {
      validarimput(tachar);
    }
  });

  $('.asocia').click(function(e) {
    $(this).val(tachar);
    if (tachar == '') {
      $('#act_prop > tbody > tr').css('background', 'white');
    }
    tachar = '';
  });

  function validarimput(valor){
    $('.asocia').each(function(index, el) {
      val = $(this).val();
      if (valor == val ) {
        swal('La actividade ya ha sido asociada');
      }
    });
  }

  $('.asocia').blur(function(event) {
    valor = $(this).val();
    cont = 0;
    $('.asocia').each(function(index, el) {
      val = $(this).val();
      if (valor == val ) {
        cont +=1;
        console.log(cont);
        if (cont > 1 ) {
          swal('La actividade ya ha sido asociada');
          $(this).val('');
        }
      }
    });
  });

  function asociar(){

    var actividades = [];
    var cpropuesta = $('#cpropuesta').val();
    var cproyecto = $('#cproyecto').val();

    $('.asocia').each(function(index, el) {
      valor = $(this).val()
      id = $(this).attr('id');
      if (valor > 0) {
        actividades.push(valor+'_'+id);
      }
    });
    console.log('asociar',cproyecto)
    guardar_asoc(cpropuesta,actividades,cproyecto);
  }

  function guardar_asoc(cpropuesta,actividades,cproyecto){
    $.ajax({
      url: 'guardar_ascocidos',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: { 'actividades': actividades,
              'cpropuesta' : cpropuesta,
              'cproyecto' : cproyecto
            },
    })
    .done(function(data) {
      console.log("guardar_ascocidos",data);
      swal('asociación de actividades guardadas')
      $('#searchActividades').modal('hide');
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }

</script>