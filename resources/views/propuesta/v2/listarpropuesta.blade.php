
<table class="table table-responsive table-hover table-bordered" style="font-size: 12px;" id="listapropuesta">
	<caption>Listado de Propuestas</caption>
	<thead>
		<tr style="background-color: #0069aa; color: white" >
			<th>Código Propuesta</th>
			<th>Cliente</th>
			<th>Unidad Minera</th>
			<th>Nombre Propuesta</th>
			<th>Fecha de Propuesta</th>
			<th>Gerente</th>
			<th width="11%">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($listarpropuestas as $lista)
		<tr data-revision_num="{{ $lista->revision_num }}" data-propuesta_codigo="{{ $lista->ccodigo }}" 
			data-cpropuesta="{{ $lista->cpropuesta }}" >
			<td>{{ $lista->ccodigo }} - <i>r{{ $lista->revision_num }}</i> </td>
			<td>{{ $lista->cliente }}</td>
			<td>{{ $lista->uminera }}</td>
			<td>{{ $lista->nombre }}</td>
			<td>{{ $lista->fpropuesta }}</td>
			<td>{{ $lista->gteProyecto }}</td>
			<td>
				<div class="btn-group">
					<button title="Ver contacto" type="button" class="btn bg-primary" data-toggle="modal" data-target="#modal"><i class="fa fa-fw fa-briefcase"></i></button>
					<button title="Editar" type="button" class="btn bg-yellow" onclick="getEdit('{{ $lista->ccodigo }}','edit')"><i class="fa fa-fw fa-edit"></i></button>
					<a title="Ejecutar" type="button" class="btn bg-faded" href='{{ url("propuesta/$lista->cpropuesta/ejecutar") }}' ><i class="fa fa-fw fa-plus-square-o"></i></a>
					<!-- <div class="btn-group">
						<button title="Registrar" type="button" class="btn bg-faded dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-fw fa-plus-square-o"></i>
						</button>
						<ul class="dropdown-menu" style="font-size: 12px; font-family: bold">
							<li><a href="#" data-toggle="modal" data-target="#modalcodigo" onclick="generarcodigoProp('pro')">Propuesta</a></li>
						</ul>
					</div> -->
					<button type="button" class="btn bg-default btn-revision" title="generar revision"><span class="glyphicon glyphicon-saved" aria-hidden="true"></span></button>
					<div class="btn-group">
						<button title="Estados" type="button" class="btn bg-red dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-fw  fa-star-half-o"></i>
						</button>
						<ul class="dropdown-menu" style="font-size: 12px; font-family: bold">
							<li><a href="#">Cancelada</a></li>
							<li><a href="#">Declinada</a></li>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

