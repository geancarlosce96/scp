
<table class="table table-responsive table-hover table-bordered" style="font-size: 12px;">
	<caption>Listado de Invitaciones</caption>
	<thead>
		<tr style="background-color: #0069aa; color: white" >
			<th>Código Preliminar</th>
			<th>Cliente</th>
			<th>Unidad Minera</th>
			<th>Nombre Preliminar</th>
			<th>Fecha de Preliminar</th>
			<th>Estado</th>
			<th width="11%">Acciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($listarpreliminar as $lista)
		<tr>
			<td>{{ $lista->codigo_preliminar }}</td>
			<td>{{ $lista->nombre }}</td>
			<td>{{ $lista->uminera }}</td>
			<td>{{ $lista->nombre_preliminar }}</td>
			<td>{{ $lista->fecha_preliminar }}</td>
			<td>{{ $lista->estado }}</td>
			<td>
				<div class="btn-group">
					<button title="Ver contacto" type="button" class="btn bg-primary" data-toggle="modal" data-target="#modal"><i class="fa fa-fw fa-briefcase"></i></button>
					<button title="Editar" type="button" class="btn bg-yellow" onclick="getEdit('{{ $lista->id }}','edit')"><i class="fa fa-fw fa-edit"></i></button>
					<div class="btn-group">
						<button title="Registrar" type="button" class="btn bg-faded dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-fw fa-plus-square-o"></i>
						</button>
						<ul class="dropdown-menu" style="font-size: 12px; font-family: bold">
							<li><a href="#" data-toggle="modal" data-target="#modalcodigo" onclick="generarcodigoProp('pro')">Propuesta</a></li>
						</ul>
					</div>
					<div class="btn-group">
						<button title="Estados" type="button" class="btn bg-red dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-fw  fa-star-half-o"></i>
						</button>
						<ul class="dropdown-menu" style="font-size: 12px; font-family: bold">
							<li><a href="#">Cancelada</a></li>
							<li><a href="#">Declinada</a></li>
						</ul>
					</div>
				</div>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

