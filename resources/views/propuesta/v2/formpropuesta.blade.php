
<form action="{{ url('grabarpropuesta') }}" style="font-size: 12px;" method="post">
 {{ csrf_field() }}
 <input type="text" id="invitacion_id_pro" name="invitacion_id_pro">
 <div class="row">
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
      <label>Código</label>
  </span>
  <input type="hidden" name="anio" id="anio" value="{{ Date('y') }}">

{!! Form::select('sedecod',(isset($tsedes)==1?$tsedes:array()),(isset($codsede)==1?$codsede:''),array('class' => 'form-control','id' => 'sedecod','onchange' => 'codigo("pro")')) !!}     

<input size="8" type="text" id="codigopro" name="codigopro" readonly="true" class="input-sm" value="{{ isset($codigopropuesta)==1? $codigopropuesta:'' }}">
<input size="5" type="text" id="correlativopro" name="correlativopro" class="input-sm" onkeyup="codigo('pro')" maxlength="7" title="Codigo a iniciar"value="{{ isset($codigopropuesta)==1? $codigopropuesta:'' }}">


</div>
</div>
     <div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Cliente</label>
           </span>
           <select class="form-control" id="select_cliente_pro" name="select_cliente" >
               <option value="">Seleccionar</option>
               @foreach($cliente as $cli)
               <option value="{{ $cli->cpersona }}">{{ $cli->nombre }}</option>
               @endforeach
           </select>
       </div>
   </div>
   <div class="col-xs-2 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Unidad Minera</label>
       </span>
       <select class="form-control" id="select_minera_pro" name="select_minera_pro"></select>
   </div>
</div>
<div class="col-xs-5 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Propuesta</label>
   </span>
   <input type="text" class="form-control" id="nombre_propuesta" name="nombre_propuesta" readonly="true" required>
</div>
</div>
</div>
<br>
<div class="row">
<!-- <div class="col-xs-2 col-xs-12"></div> -->
 <div class="col-xs-2 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Contacto</label>
       </span>
       <select class="form-control" id="select_contacto_pro" name="select_contacto_pro" ></select>
   </div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Cargo</label>
   </span>
   <input type="text" class="form-control" id="cargo_pro" name="cargo_pro" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Teléfono</label>
   </span>
   <input type="text" class="form-control" id="telefono_pro" name="telefono_pro" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Anexo</label>
   </span>
   <input type="text" class="form-control" id="anexo_pro" name="anexo_pro" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Correo</label>
   </span>
   <input type="text" class="form-control" id="correo_pro" name="correo_pro" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Propuesta</label>
         </div>
         <input class="form-control pull-right " id="fecha_preliminar" readonly="true" name="fecha_preliminar" value="{{ Date('Y-m-d') }}" type="text">
     </div>
 </div>
</div>
</div>
<br>
<div class="row">
<!-- <div class="col-xs-2 col-xs-12"></div> -->
  <div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_visita">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>visita</label>
         </div>
         <input class="form-control pull-right " id="fecha_visita" readonly="true" name="fecha_visita" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_consulta">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Consultas</label>
         </div>
         <input class="form-control pull-right " id="fecha_consulta" readonly="true" name="fecha_consulta" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_absolucion">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Absolución</label>
         </div>
         <input class="form-control pull-right " id="fecha_absolucion" readonly="true" name="fecha_absolucion" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Presentación</label>
         </div>
         <input class="form-control pull-right " id="fecha_presentacion_pro" readonly="true" name="fecha_presentacion_pro" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_interna">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Interna</label>
         </div>
         <input class="form-control pull-right " id="fecha_interna" readonly="true" name="fecha_interna" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
  <div class="input-group">
        <span class="input-group-addon">
          <input type="checkbox" name="comunicar_propuesta" id="comunicar_propuesta" checked="true">
        </span>
        <input type="text" class="form-control" name="email_propuesta" id="email_propuesta" value="fabio.colan@anddes.com" >
  </div>
</div>
</div>
<div class="row">
  <div class="col-xs-3 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Portafolio de Servicios</label>
       </span>
       <select class="form-control" id="select_portafolio" name="select_portafolio" >
               <option value="">Seleccionar</option>
               @foreach($portafolio as $porta)
               <option value="{{ $porta->cservicio }}">{{ $porta->descripcion }}</option>
               @endforeach
           </select>
   </div>
</div>
<div class="col-xs-3 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Tipo de Proyecto</label>
       </span>
       <select class="form-control" id="select_tipoproyecto" name="select_tipoproyecto"></select>
   </div>
</div>
<div class="col-xs-6 col-xs-12">
    <div class="form-group">
       <div class="input-group date">
          <div class="input-group-addon">
             <label>Comentario</label>
         </div>
         <input type="text" name="comentario_propuesta" id="comentario_propuesta" class="form-control" required>
     </div>
 </div>
</div>
</div> 
<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
  <button type="submit" class="btn btn-success bg-green">Generar</button>
</div>
</form>

