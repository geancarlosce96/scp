@extends('layouts.master')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="{{ url('css/timeline.css') }}"/>
<style type="text/css">
	#equipotime{color: green;}
</style>
@endsection
@section('content')
<input type="hidden" id="urldisciplinas" value="{{ url('disciplinas') }}">
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Propuesta
			<small>Propuesta</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
			<li class="active">Propuestas</li>
		</ol>
	</section>

	<section class="content">
		<div class="box-body">
			<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
			@include('propuesta.v2.ejecucion')
			<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:15px;"></div>
			<div class="timeline-panel">
				<div class="timeline-heading">
					<h4 class="timeline-title">Seleccion de equipo</h4>
				</div>
				<div class="timeline-body">
		        {!! Form::open(array('url' => 'grabarpropuestaequipo','method' => 'POST','id' =>'frmpropuestaequipo')) !!}
		        {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta')) !!}
		        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
					<div class="col-lg-6">
						<label>Gerente del proyecto</label>
						<select class="form-control" id="gerentes" name="gerentes">
							@foreach($gerentes as $lista)
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Responsable de Propuesta</label>
						<select class="form-control" id="responsable" name="responsable">
							@foreach($responsable as $lista)
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Coordinador de Propuesta</label>
						<select class="form-control" id="coordinador" name="coordinador">
							@foreach($coordinador as $lista)
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Revisor Final</label>
						<select class="form-control" id="revisor" name="revisor">
							@foreach($revisor as $lista)
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Responsables Técnicos</label>
						<select class="form-control" multiple="multiple" id="responsables" name="responsables[]">
							@foreach($responsable as $lista)
								<option value="{{ $lista->cpersona }}">{{ $lista->abreviatura }}</option>
							@endforeach
						</select>
					</div>
				<div class="row">
					<div class="pull-right">
						<a title="Siguiente" type="submit" class="btn bg-primary" href="{{ url('disciplinas') }}/{{$propuesta->cpropuesta}}" id="linkDisciplinas">Siguiente</a>
						<button type="submit">Siguiente</button>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
	$(document).ready(function() {
		addchosen('gerentes');
		addchosen('responsable');
		addchosen('revisor');
		addchosen('coordinador');
		addchosen('responsables');
	});

	var url = $("#urldisciplinas").val() + "/" + periodo + "/" + semana;
		$("#linkDisciplinas").prop("href", url);
</script>
@stop

