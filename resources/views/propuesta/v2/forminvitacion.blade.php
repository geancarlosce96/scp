<form action="{{ url('grabarinvitacion') }}" style="font-size: 12px;" method="post">
 {{ csrf_field() }}
 <div class="row">
     <div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Cliente</label>
           </span>
           <input type="hidden" id="cliente_nombre" name="cliente_nombre" required>
           <select class="form-control" id="cliente" name="cliente" required>
               <option value="">Seleccionar</option>
               @foreach($cliente as $cli)
               <option value="{{ $cli->cpersona }}">{{ $cli->nombre }}</option>
               @endforeach
           </select>
       </div>
   </div>
   <div class="col-xs-3 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Unidad Minera</label>
       </span>
       <input type="hidden" name="mineraid" id="mineraid">
       <input type="hidden" name="minera_nombre" id="minera_nombre">
       <select class="form-control" id="minera" name="minera"></select>
   </div>
</div>
<div class="col-xs-5 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Servicio Solicitado</label>
   </span>
   <input type="text" class="form-control" id="invitacion" name="invitacion" required>
</div>
</div>
<div class="col-xs-1 col-xs-12">
 <div class="input-group">
    <span class="input-group-btn">
      <button type="button" class="btn btn-primary" onclick="verModals()" title="Cliente, Unidad Minera o Contacto según se seleccione">Nuevo</button>
  </span>
</div>
</div>
</div>
<br>
<div class="row">
 <div class="col-xs-2 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Contacto</label>
       </span>
       <select class="form-control" id="contacto" name="contacto" required></select>
   </div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Cargo</label>
   </span>
   <input type="text" class="form-control" id="cargo" name="cargo" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Telefono</label>
   </span>
   <input type="text" class="form-control" id="telefono" name="telefono" readonly="true" required>
</div>
</div>
<div class="col-xs-1 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Anexo</label>
   </span>
   <input type="text" class="form-control" id="anexo" name="anexo" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Correo</label>
   </span>
   <input type="text" class="form-control" id="correo" name="correo" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha">
          <div class="input-group-addon ">
             <i class="fa fa-calendar"></i>
             <label>Invitacion</label>
         </div>
         <input class="form-control pull-right " id="fecha" readonly="true" name="fecha" type="text" value="{{ Date('Y-m-d') }}" required>
     </div>
 </div>
</div>
</div>
<br>
<div class="row">
<div class="col-xs-9 col-xs-12">
    <div class="form-group">
       <div class="input-group">
          <div class="input-group-addon">
             <label>Comentario</label>
         </div>
         <input type="text" name="comentario_invitacion" id="comentario_invitacion" class="form-control" required>
     </div>
 </div>
</div>

<div class="col-xs-2 col-xs-12">
  <div class="input-group">
        <span class="input-group-addon">
          <input type="checkbox" name="comunicar_invitacion" id="comunicar_invitacion" checked="true">
        </span>
    <input type="text" class="form-control" name="email_invitacion" id="email_invitacion" value="fabio.colan@anddes.com">
  </div>
  <!-- /input-group -->
</div>
  
<div class="col-xs-1 col-xs-12">
 <div class="input-group">
    <span class="input-group-btn">
       <button type="submit" class="btn btn-success bg-green" id="add_up">Registrar</button>
   </span>
</div>
</div>
</div>
</form>