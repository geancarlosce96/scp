@extends('layouts.master')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="{{ url('css/timeline.css') }}"/>
@endsection
<style type="text/css">
	#disciplinastime{color: green;}
	#equipotime{color: green;}
	#tagtime{background: green;}
</style>
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Propuesta
			<small>Propuesta</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
			<li class="active">Propuestas</li>
		</ol>
	</section>

	<section class="content">
		<div class="box-body">
			<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
			@include('propuesta.v2.ejecucion')
			<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:15px;"></div>
			<div class="timeline-panel">
				<div class="timeline-heading">
					<h4 class="timeline-title">Seleccion de TAG</h4>
				</div>
				<div class="timeline-body">
					<div class="col-lg-6">
						<label>Gerente del proyecto</label>
						<select class="form-control">
							@foreach($disciplina as $lista)
								<option value="{{ $lista->cdisciplina }}">{{ $lista->codigo }} - {{ $lista->descripcion }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Responsable de Propuesta</label>
						<select class="form-control">
							@foreach($disciplina as $lista)
								<option value="{{ $lista->cdisciplina }}">{{ $lista->codigo }} - {{ $lista->descripcion }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Revisor Final</label>
						<select class="form-control">
							@foreach($disciplina as $lista)
								<option value="{{ $lista->cdisciplina }}">{{ $lista->codigo }} - {{ $lista->descripcion }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Responsable Civil</label>
						<select class="form-control">
							@foreach($disciplina as $lista)
								<option value="{{ $lista->cdisciplina }}">{{ $lista->codigo }} - {{ $lista->descripcion }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Responsable Geotécnia</label>
						<select class="form-control">
							@foreach($disciplina as $lista)
								<option value="{{ $lista->cdisciplina }}">{{ $lista->codigo }} - {{ $lista->descripcion }}</option>
							@endforeach
						</select>
					</div>
					<div class="col-lg-6">
						<label>Responsable Hidráulica</label>
						<select class="form-control">
							@foreach($disciplina as $lista)
								<option value="{{ $lista->cdisciplina }}">{{ $lista->codigo }} - {{ $lista->descripcion }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<br>
				<div class="pull-right">
					<a class="btn btn-primary" href="{{ url('equipo') }}" role="button">Atras</a>
					<a class="btn btn-primary" href="#" role="button">Siguiente</a>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@stop

