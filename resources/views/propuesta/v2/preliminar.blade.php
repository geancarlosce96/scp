@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
      <h1>
        Propuesta
        <small>Preliminar</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuestas</li>
      </ol>
    </section>
    <section class="content">
    	<div class="box-body">
    		<div class="box-group" id="accordion">
    			<!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
    			<div class="panel panel-primary">
    				<div class="box-header with-border panel-heading">
    					<h4 class="box-title panel-title">
    						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed"> <span class="glyphicon glyphicon-hand-up"></span> Agregar preliminar</a>
    					</h4>
    				</div>
    				<div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
    					<div class="box-body">
    						@include('propuesta.v2.formpreliminar')
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
			@include('propuesta.v2.listarpreliminar')
    	</div>
    </section>
            
</div>

@include('partials.modalRegistroUnidadMinera')
@include('partials.modalRegistroContactoUnidadMinera')

        <div class="modal fade" id="modal">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header" style="background-color: #0069AA">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:white">Registrar Cliente</h4>
              </div>
              <div class="modal-body">
                @include('cliente.fromClientes')
              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary">Aceptar</button>
              </div> -->
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <div class="modal fade" id="generar">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header" style="background-color: #0069AA">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" style="color:white">Preliminar</h4>
              </div>
              <div class="modal-body">
              	<div class="row clsPadding2" id="propuesta">
              		<center>
              			<select name="aniocod" id="aniocod" class="form-control" onchange="generarcodigoProp('pro')">
              				<?php
              				$anio=date("Y");
              				for($t=$anio ; $t>=2010;$t--){
              					$selected="";
              					$selected = ($t==$aniocod?"selected":"");
              					?>
              					<option value="{{ $t }}" {{ $selected }}>{{ $t }}</option>
              					<?php
              				}
              				?>
              			</select>

              			{!! Form::select('sedecod',(isset($tsedes)==1?$tsedes:array()),(isset($codsede)==1?$codsede:''),array('class' => 'form-control','id' => 'sedecod','onchange' => 'generarcodigoProp("pro")')) !!}     

              			{!! Form::text('corcod',(isset($correlativo)==1?$correlativo:''),array('class' => 'input-sm','id' => 'corcod','onKeyUp' => 'generarcodigoProp("pro")','placeholder' => 'Número')) !!} 

              			{!! Form::text('codigoprop',(isset($propuesta->ccodigo)==1?$propuesta->ccodigo:''),array('class' => 'input-sm','readonly'=>'true','id' => 'codigoprop','placeholder' => 'Código de propuesta')) !!}   
					</center>
              	</div>
              	<div id="preliminar">
              	@include('propuesta.v2.formpreliminar')
              	</div>
              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-success">Generar</button>
              </div> -->
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
 								
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script src="js/clientes.js"></script>

<script src="js/preliminar.js"></script>

@stop

