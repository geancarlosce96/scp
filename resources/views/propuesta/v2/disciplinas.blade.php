@extends('layouts.master')
@section('extra-css')
<link rel="stylesheet" type="text/css" href="{{ url('css/timeline.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ url('multi-select.css') }}"/>
<style type="text/css">
	#disciplinastime{color: green;}
	#equipotime{color: green;}
</style>
@endsection
@section('content')
<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Propuesta
			<small>Propuesta</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
			<li class="active">Propuestas</li>
		</ol>
	</section>

	<section class="content">
		<div class="box-body">
			<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
			@include('propuesta.v2.ejecucion')
			<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:15px;"></div>
			<div class="timeline-panel">
				<div class="timeline-heading">
					<h4 class="timeline-title">Selección de disciplinas</h4>
				</div>
				<div class="timeline-body">
					<!-- <div class="col-lg-6"> -->
						<select id="disciplinas" name="disciplinas" multiple="multiple">
							@foreach($disciplina as $lista)
								<option value="{{ $lista->cdisciplina }}">{{ $lista->codigo }} - {{ $lista->descripcion }}</option>
							@endforeach
						</select>
					<!-- </div> -->
					<a class="btn btn-primary" href="{{ url('tag') }}"" role="button">Siguiente</a>
				</div>
			</div>
		</div>
	</section>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="{{ url('js/jquery.multi-select.js') }}"></script>

<script>
	$(document).ready(function() {

		addchosen('disciplinas');

	});
</script>

@stop

