
<form action="{{ url('grabarpreliminar') }}" style="font-size: 12px;" method="post">
 {{ csrf_field() }}
 <input type="text" id="invitacion_id" name="invitacion_id">


 <div class="row">
  <div class="col-xs-1 col-xs-12"></div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
      <label>Código</label>
  </span>
  <input type="hidden" name="anio" id="anio" value="{{ Date('y') }}">
  <input size="7" type="text" id="codigopre" name="codigopre" readonly="true" class="input-sm" value="{{ isset($codigopreliminar)==1? $codigopreliminar:'' }}">
  <input size="2" type="{{ isset($cod)==1? $cod:'text' }}" id="correlativopre" name="correlativopre" class="input-sm" onkeyup="codigo('pre')" maxlength="3" title="Codigo a iniciar"value="{{ isset($codigopreliminar)==1? $codigopreliminar:'' }}">
</div>
</div>
     <div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Cliente</label>
           </span>
           <input type="text" id="cliente_nombre_pre" name="cliente_nombre_pre" required>
           <select class="form-control" id="select_cliente" name="select_cliente" >
               <option value="">Seleccionar</option>
               @foreach($cliente as $cli)
               <option value="{{ $cli->cpersona }}">{{ $cli->nombre }}</option>
               @endforeach
           </select>
       </div>
   </div>
   <div class="col-xs-2 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Unidad Minera</label>
       </span>
       <input type="text" name="mineraid_pre" id="mineraid_pre">
       <input type="text" name="minera_nombre_pre" id="minera_nombre_pre">
       <select class="form-control" id="select_minera" name="select_minera"></select>
   </div>
</div>
<div class="col-xs-3 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Nombre Preliminar</label>
   </span>
   <input type="text" class="form-control" id="nombre_preliminar" name="nombre_preliminar" readonly="true" required>
</div>
</div>
</div>
<br>
<div class="row">
<div class="col-xs-1 col-xs-12"></div>
 <div class="col-xs-2 col-xs-12">
     <div class="input-group">
        <span class="input-group-addon">
           <label>Contacto</label>
       </span>
       <select class="form-control" id="select_contacto" name="select_contacto" ></select>
   </div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Cargo</label>
   </span>
   <input type="text" class="form-control" id="cargo_pre" name="cargo_pre" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Teléfono</label>
   </span>
   <input type="text" class="form-control" id="telefono_pre" name="telefono_pre" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Anexo</label>
   </span>
   <input type="text" class="form-control" id="anexo_pre" name="anexo_pre" readonly="true" required>
</div>
</div>
<div class="col-xs-2 col-xs-12">
 <div class="input-group">
    <span class="input-group-addon">
       <label>Correo</label>
   </span>
   <input type="text" class="form-control" id="correo_pre" name="correo_pre" readonly="true" required>
</div>
</div>
</div>
<br>
<div class="row">
<div class="col-xs-1 col-xs-12"></div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_preliminar">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Preliminar</label>
         </div>
         <input class="form-control pull-right " id="fecha_preliminar" readonly="true" name="fecha_preliminar" value="{{ Date('Y-m-d') }}" type="text">
     </div>
 </div>
</div>
  <div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_visita">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Visita</label>
         </div>
         <input class="form-control pull-right " id="fecha_visita" readonly="true" name="fecha_visita" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_consulta">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Consultas</label>
         </div>
         <input class="form-control pull-right " id="fecha_consulta" readonly="true" name="fecha_consulta" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_absolucion">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Absolución</label>
         </div>
         <input class="form-control pull-right " id="fecha_absolucion" readonly="true" name="fecha_absolucion" type="text" >
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
    <div class="form-group">
       <div class="input-group date fecha_presentacion">
          <div class="input-group-addon">
             <i class="fa fa-calendar"></i>
             <label>Presentación</label>
         </div>
         <input class="form-control pull-right " id="fecha_presentacion" readonly="true" name="fecha_presentacion" type="text" >
     </div>
 </div>
</div>
</div>
<div class="row">
  <div class="col-xs-1 col-xs-12"></div>
<div class="col-xs-8 col-xs-12">
    <div class="form-group">
       <div class="input-group date">
          <div class="input-group-addon">
             <label>Comentario</label>
         </div>
         <input type="text" name="comentario_preliminar" id="comentario_preliminar" class="form-control" required>
     </div>
 </div>
</div>
<div class="col-xs-2 col-xs-12">
  <div class="input-group">
        <span class="input-group-addon">
          <input type="checkbox" name="comunicar_preliminar" id="comunicar_preliminar" checked="true">
        </span>
        <input type="text" class="form-control" name="email_preliminar" id="email_preliminar" value="fabio.colan@anddes.com" >
  </div>
  <!-- /input-group -->
</div>
<!-- <div class="col-xs-1 col-xs-12">
 <div class="input-group">
    <span class="input-group-btn">
      <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar_modal">Cerrar</button>
      <button type="submit" class="btn btn-success bg-green">Registrar</button>
   </span>
</div>
</div> -->
</div>  
<!-- <br> -->
<div class="modal-footer">
  <button type="button" class="btn btn-danger" data-dismiss="modal" id="cerrar_modal">Cerrar</button>
  <button type="submit" class="btn btn-success bg-green">Registrar</button>
</div>
</form>

