@extends('layouts.master')
@section('content')
<div class="content-wrapper">
	<section class="content-header">
      <h1>
        Propuesta
        <small>Propuesta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuestas</li>
      </ol>
    </section>
    <section class="content">
    	<div class="box-body">
    		<div class="box-group" id="accordion">
    			<!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
    			<div class="panel panel-primary">
    				<div class="box-header with-border panel-heading">
    					<h4 class="box-title panel-title">
    						<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed"> <span class="glyphicon glyphicon-hand-up"></span> Agregar propuesta</a>
    					</h4>
    				</div>
    				<div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
    					<div class="box-body">
    						@include('propuesta.v2.formpropuesta')
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
			@include('propuesta.v2.listarpropuesta')
    	</div>
    </section>
            
</div>

@include('partials.modalRegistroUnidadMinera')
@include('partials.modalRegistroContactoUnidadMinera')

<div class="modal fade" id="modalRevision" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Generar Revisión para <span id="propuesta_codigo"></span> </h4>
      </div>
      <div class="modal-body">
        <p>¿Dé qué revisión quieres generar otra revisión?</p>
        <p><input type="radio" name="revision_opcion" value="actual" checked="true" > Revisión Actual: 
            <input type="text" id="revision_actual" readonly> </p>
        <p><input type="radio" name="revision_opcion" value="seleccion" disabled> De ésta revisión: 
            <select disabled>
                <option value="0">Elegir</option>        
            </select>
        </p>
        <p><i>Se redireccionará a la revisión creada luego de confirmar la nueva revisión.</i> </p>
        <input type="hidden" id="cpropuesta_revision" >
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" id="btn-generarRevision">Generar</button>
      </div>
    </div>
  </div>
</div>
 								
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/propuesta.js"></script>

@stop

