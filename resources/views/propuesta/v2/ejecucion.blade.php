
<ul class="timeline timeline-horizontal">
	<li class="timeline-item">
		<div class="timeline-badge" title="Asignación de Equipo"><a href="{{ url('equipo') }}/{{$propuesta->cpropuesta}}" id="equipotime"><i class="glyphicon glyphicon-edit"></i></a></div>
	</li>
	<li class="timeline-item">
		<div class="timeline-badge" title="Selección de Disciplinas"><a href="{{ url('disciplinas') }}/{{$propuesta->cpropuesta}}" id="disciplinastime"><i class="glyphicon glyphicon-edit"></i></a></div>
	</li>
	<li class="timeline-item">
		<div class="timeline-badge" title="Selección de TAG"><a href="{{ url('tag') }}/{{$propuesta->cpropuesta}}" id="tagtime"><i class="glyphicon glyphicon-edit"></i></a></div>
	</li>
	<li class="timeline-item">
		<div class="timeline-badge" title="Estructura de PPA"><a href="{{ url('estructura') }}/{{$propuesta->cpropuesta}}" id="estructuratime"><i class="glyphicon glyphicon-edit"></i></a></div>
	</li>
	<li class="timeline-item">
		<div class="timeline-badge" title="Registro de HH"><a><i class="glyphicon glyphicon-edit"></i></a></div>
	</li>
</ul>

<div class="row">
	<div class="col-xs-2 col-xs-12">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Código de propuesta</label>
	       </span>
	       {!! Form::text('codigo',(isset($propuesta->ccodigo)==1?$propuesta->ccodigo:''),array('class'=>'form-control input-sm','readonly' => 'true')) !!}
	   </div>
	</div>
	<div class="col-xs-4 col-xs-12">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Nombre de la propuesta</label>
	       </span>
	       {!! Form::text('nombre',(isset($propuesta->nombre)==1?$propuesta->nombre:''),array('class'=>'form-control input-sm','readonly' => 'true')) !!}
	   </div>
	</div>
	<div class="col-xs-4 col-xs-12">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Nombre del cliente</label>
	       </span>
	       {!! Form::text('cliente',(isset($propuesta->cliente)==1?$propuesta->cliente:''),array('class'=>'form-control input-sm','readonly' => 'true')) !!}
	   </div>
	</div>
	<div class="col-xs-2 col-xs-12">
	     <div class="input-group">
	        <span class="input-group-addon">
	           <label>Nombre de la unidad minera</label>
	       </span>
	       {!! Form::text('uminera',(isset($propuesta->uminera)==1?$propuesta->uminera:''),array('class'=>'form-control input-sm','readonly' => 'true')) !!}
	   </div>
	</div>
</div>

<br><br>