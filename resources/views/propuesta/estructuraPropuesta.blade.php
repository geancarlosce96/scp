<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Propuesta
        <small>Estructura de Propuesta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuesta</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoPropuesta')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>    
        {!! Form::open(array('url' => 'grabarEstructuraPropuesta','method' => 'POST','id' =>'frmEstructura')) !!}
        {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:''),array('id'=>'cpropuesta')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}

    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">
        <div class="row">
          
            <div class="col-md-2 col-xs-2">Cliente:</div>
            <div class="col-md-8 col-xs-8">
            {!! Form::text('nombrecliente',(isset($persona->nombre)?$persona->nombre:''),array('class'=>'form-control input-sm col-md-8 col-xs-12','disabled' => 'true')) !!}
            </div>
            <div class="col-md-1 col-xs-1">
                <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#searchPropuesta"><b>...</b></button>
            </div>
            <div class="col-md-1 col-xs-1">
              <button type="button" class="btn btn-primary btn-sm" disabled=""><b>+</b></button>
            </div>
          
        </div>
        <div class="row">
            <div class="col-lg-2 col-xs-2">Unidad minera:</div>
            <div class="col-lg-10 col-xs-10">
        		    {!! Form::text('nombreuminera',(isset($uminera->nombre)==1?$uminera->nombre:''),array('disabled'=>'','class'=> 'form-control input-sm','placeholder'=>'Nombre Unidad Minera')) !!}   
            </div>
        </div>
        <div class="row">
          <div class="col-lg-2 col-xs-2">Código:</div>
          <div class="col-lg-4 col-xs-3">
            {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)?$propuesta->ccodigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}          
          </div>
          <div class="col-lg-2 col-xs-2">Nombre de la propuesta:</div>
          <div class="col-lg-4 col-xs-3">
              {!! Form::text('nombrepropuesta',(isset($propuesta->nombre)==1?$propuesta->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de la Propuesta')) !!}  
          </div>
        </div>    
        <div class="row">
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>


        <div class="row">
            <div class="col-lg-6 col-xs-6">
        	       <label>Coordinador de propuesta:</label>
                  {!! Form::text('propuestacoor',(isset($personacoor->nombre)==1?$personacoor->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Coordinador de Propuesta')) !!} 
            </div>
            <div class="col-lg-6 col-xs-6">
                <label>Revisión:</label>
                {!! Form::text('revision',(isset($propuesta->revision)==1?$propuesta->revision:''),array('class'=>'form-control input-sm','disabled' =>'')) !!}   
            </div>       
        </div>
        <div class="row">
            <div class="col-lg-6 col-xs-12">
        	<label>Tipo de Servicio:</label>
            {!! Form::select('servicio',$servicios,(isset($propuesta->cservicio)==1?$propuesta->cservicio:''),array('class' => 'form-control','disabled'=>'true')) !!}     
            </div>
            <div class="col-lg-6  hidden-xs"></div> 
        </div> 
 
        <div class="row">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>
        <div class="row">
            <div class="col-lg-2 col-xs-2">Paquete de  Propuesta:</div>
            <div class="col-lg-6 col-xs-6">
            {!! Form::select('paqpropuesta',(isset($paqpropuesta)==1?$paqpropuesta:array()),null,array('class' => 'form-control','id'=>'paqpropuesta')) !!} 
            </div> 
            <div class="col-lg-2 col-xs-2">
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addActividad"> <b>Agregar</b></button>
            </div>
            <div class="col-lg-2 col-xs-2">
                &nbsp;
                <!--<a href="#" class="btn btn-primary btn-block "> <b>Eliminar</b></a>-->
            </div>    
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12 table-responsive" id="idActividades">
              @include('partials.tableActividadPropuesta',array('actividadesp' => (isset($actividadesp)==1?$actividadesp:array())))
            </div>
        </div>
        <div class="row">
        		<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>
        <div class="row ">

            <div class="col-lg-3 col-xs-6">
            <label>Categoría:
            {!! Form::select('categorias',(isset($categorias)==1?$categorias:array()),null,array('class'=>'form-control','id'=>'categorias')) !!}
              </label> 
            
            </div> 
            <div class="col-lg-3 col-xs-6">
            <label>Profesional:
                  {!! Form::select('profesional',(isset($personal)==1?$personal:array()),null,array('class' => 'form-control','id'=>'profesional')) !!}
              </label> 
            </div> 
            <div class="col-lg-2 col-xs-6">
            <label>Rate:
              {!! Form::text('rate','',array('id'=>'rate','class'=>'form-control')) !!}</label> 
            </div>         
            <div class="col-lg-2 col-xs-3">
                <button type='button' class="btn btn-primary " id="btnAddEst"> <b>Agregar</b></button>
            </div>
            <div class="col-lg-2 col-xs-3">
                &nbsp;
            </div>    
        </div>

        <div class="row">
            <div class="col-lg-12 col-xs-12 table-responsive" id="idPersonal">
              @include('partials.tableEstructura',array('participantes' => (isset($participantes)==1?$participantes:array())))
            </div>
        </div>

        <div class="row">
        	<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>
        <div class="row clsPadding">
            <div class="col-lg-4 col-xs-4 cls-centered"></div>
                <button class="btn btn-primary btn-block" style="width:200px;" id="btnSave"><b>Grabar</b></button> 
        </div>
    </div>

   
    <!--<div class="col-lg-1 hidden-xs"></div>-->
   





    </div>
    {!! Form::close() !!}
    </section>
    <!-- /.content -->

    <!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuestas</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuestas" class="table">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Cliente</th>
                  <th>Unidad Minera</th>
                  <th>Propuesta</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>
    <!-- Fin Modal Buscar Propuesta  -->


    <!-- Modal buscar actividades -->
    <div id="addActividad" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Actividades</h4>
          </div>
          <div class="modal-body">
            <div class="table-responsive" id="divAct">
              <?php /*  @include('partials.panelActividad',array('actividades' => (isset($actividades)==1?$actividades:array()))) */ ?>
                <div id="treeActividades">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddAct">Aceptar</button>
          </div>
        </div>

      </div>
    </div>
    <!-- Fin Modal buscar actividades-->





  </div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        var selected_actividad =[];
        var personal_rate =<?php echo (isset($personal_rate)==1?json_encode($personal_rate):'[]'); ?>;
        $.fn.dataTable.ext.errMode = 'throw';
        var table=$("#tPropuestas").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarPropuestas",
                "type": "GET",
            },
            "columns":[
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tPropuestas tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        });     
        $('#searchPropuesta').on('hidden.bs.modal', function () {
           goEditar();
        });

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('editarEstructuraPropuesta/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        };   
        $("#addActividad").on('show.bs.modal',function(e){
          
          if ($("#paqpropuesta").val()=='' || $("#paqpropuesta").val()==null){
            alert('Seleccione el paquete...');
            e.preventDefault();
            
          }

          selected_actividad.splice(0,selected_actividad.length);
          $('#treeActividades').highCheckTree({
            data: getTreeActividad(),
            onCheck: function (node){
              selected_actividad.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr('rel'));
              if(idx != -1){
                selected_actividad.splice(idx,1);
              }
            }
          });          
          
        }); 
        $("#idAddAct").click(function(e){
          $.ajax({
                url:'addActividad',
                type: 'POST',
                data : {
                  "_token": "{{ csrf_token() }}",
                  selected_actividad:selected_actividad,
                  paqpropuesta:$('#paqpropuesta').val()
                  },
                /*$('input[name="acti[]"]').serialize() + "&paqpropuesta="+ $('#paqpropuesta').val(),*/
                beforeSend: function(){
                  //$("input[name='acti[]']").attr('checked',false);
                  $('#div_carga').show(); 
                },
                success: function(data){
                    $("#idActividades").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        });  
        function fdelActi(id){
          $.ajax({
                url:'delActividad/'+id,
                type: 'GET',
                beforeSend: function(){
                    $('#div_carga').show(); 
                },                
                success: function (data){
                  $("#idActividades").html(data);
                  $('#div_carga').hide(); 
                },
          });

        };     
        $("#btnAddEst").click(function(e){
          if($("#categorias").val()==''){
            alert('Especifique la Categoria.');
            return ;
          }
          $.ajax({
            url: 'addEstructura',
            type:'GET',
            data: "categorias=" + $("#categorias").val()+"&profesional=" + $("#profesional").val() + "&rate=" + $("#rate").val()+"&cpropuesta=" + $("#cpropuesta").val() ,
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
            success : function (data){
              $("#idPersonal").html(data);
              $('#div_carga').hide(); 
            },
          });
        })
        function fdelEstru(id){
          $.ajax({
              url: 'delEstructura/'+id,
              type: 'GET',
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){
                $("#idPersonal").html(data);
                 $('#div_carga').hide(); 
              },

          });
        }

        $("#frmEstructura").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarEstructuraPropuesta',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $.each(data.responseJSON, function (key, value) {
                            var input = '#frmEstructura input[name=' + key + ']';
                            if(!($(input).length > 0)){
                                input ='#frmEstructura select[name=' + key + ']';
                            }
                            $(input).parent().parent().addClass('has-error');

                    });
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });
        $('#profesional').on('change', function() {
          sel= this.value; // or $(this).val()
          $.each(personal_rate, function( index, value ) {
            //alert( index + ": " + value['cpersona'] + "--" + value['rate1'] );
            if(value['cpersona']==sel){
              $("#rate").val(value['rate1']);
            }
          });          
        });
        
        function getTreeActividad() {
          
          var tree = [
          <?php echo (isset($estructura)==1?$estructura:'') ?>
          
          ];          
          return tree;
        }
</script>