<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Propuesta
        <small>Registro de Propuesta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Registro de Propuesta</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoPropuesta')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>
        <div class="row">
            <div class="col-lg-10 col-xs-1"></div>
            <div class="col-lg-1 col-xs-1">
            @if(isset($aprobacion))
                @if($aprobacion)
                <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Aprobaciones</button>
                @endif
            @endif
            </div>
            <div class="col-lg-1 col-xs-1"></div>
        </div>
        {!! Form::open(array('url' => 'grabarPropuesta','method' => 'POST','id' =>'frmpropuesta')) !!}
        {!! Form::hidden('cpropuesta',(isset($propuesta)==1?$propuesta->cpropuesta:''), array('id'=> 'cpropuesta' ) ) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('name_cliente','',array('id'=>'name_cliente')) !!}
        {!! Form::hidden('name_uminera','',array('id'=>'name_uminera')) !!}
        {!! Form::hidden('cod_uminera','',array('id'=>'cod_uminera')) !!}

        <div class="row">
        <!--<div class="col-lg-1 hidden-xs"></div>-->
        <div class="col-lg-12 col-xs-12">
            <div class="row">
                <div class="col-lg-8 col-xs-10">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
                    {!! Form::hidden('cpersona',(isset($persona)==1?$persona->cpersona:''),array('id'=>'idcpersona')) !!}
                    {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
                
                    
                </div>
                <div class="col-lg-2 col-xs-1 "><a href="#" onclick="$('#searchClientes').modal('show');" class="btn btn-primary btn-sm open-modal"> <b>...</b></a>
                </div>
                <div class="col-lg-2 col-xs-1 ">                    
                    <button  type="button" class="btn btn-info"
                        @if ($editar)
                            
                        @endif
                        disabled
                        >Nuevo Cliente</button></div>
                </div>
                <div class="row ">
                    <div class="col-lg-2 col-xs-2">Unidad minera:</div>
                    <div class="col-lg-10 col-xs-10">
                        {!! Form::hidden('cunidadminera',(isset($uminera->cunidadminera)==1?$uminera->cunidadminera:''),array('id'=>'iduminera')) !!}

                        {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-1 col-xs-2">Código:</div>
                    <div class="col-lg-4 col-xs-3">
                      {!! Form::text('umineracodigo',(isset($uminera->codigo)==1?$uminera->codigo:'' ),array('class'=>'form-control input-sm','disabled'=>'true','id'=>'umineracodigo')) !!}
                    </div>
                    <div class="col-lg-1 col-xs-2">Copiar desde...</div>
                    <div class="col-lg-4 col-xs-3">
                      <input class="form-control input-sm" type="text" placeholder="Copiar desde..." disabled>   
                    </div>
                    <div class="col-lg-2 col-xs-2">
                        
                        <button  type="button" class="btn btn-info"
                        @if ($editar)
                            disabled
                        @else 
                            disabled
                        @endif
                        >...</button>
                    </div>
                </div>    
                <div class="row ">
                    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                </div>

                <div class="row ">
                    <div class="col-lg-3 col-xs-3">Código de la propuesta(*):</div>
                    <div class="col-lg-9 col-xs-9">
                        {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)==1?$propuesta->ccodigo:(isset($codigoProp)==1?$codigoProp:'' ) ),array('class'=>'form-control input-sm', 'placeholder' => 'Código de la Propuesta')) !!}
                    </div>
                       
                </div>
                <div class="row ">
                    <div class="col-lg-3 col-xs-3">Nombre de la Propuesta(*):</div>
                    <div class="col-lg-9 col-xs-9">
                        {!! Form::text('propuestanombre',(isset($propuesta->nombre)==1?$propuesta->nombre:''),array('class' => 'form-control input-sm', 'placeholder' => 'Nombre de la Propuesta')) !!}   
                    </div>
                       
                </div>
                
                <div class="row ">
                    <div class="col-lg-3 col-xs-3">Descripción de la propuesta:</div>
                    <div class="col-lg-9 col-xs-9">
                        {!! Form::textarea('propuestadescripcion',(isset($propuesta->descripcion)==1?$propuesta->descripcion:''), array('class' => 'form-control', 'rows'=> '3','placeholder' => 'Descripción de la Propuesta')) !!}
                    </div> 
                </div> 
                <div class="row ">
                    <div class="col-lg-6 col-xs-6">
                    <div >
                    @foreach($formacotiza as $forma)
                        {!! Form::radio('formacotiza',$forma->cformacotizacion,(isset($propuesta->cformacotizacion)==1?($forma->cformacotizacion == $propuesta->cformacotizacion):false)) !!}
                        {{ $forma->descripcion }}
                        <br>
                    @endforeach
                    </div>
                    </div>
                    <div class="col-lg-6 col-xs-6">
                        <div >
                        
                        {!! Form::radio('tipopropuesta','1',(isset($propuesta->ftipo)==1?$propuesta->ftipo == '1':false), array('id' => 'tipopropuesta-0')) !!}Propuesta
                        <br>
                        {!! Form::radio('tipopropuesta','2',(isset($propuesta->ftipo)==1?$propuesta->ftipo == '2':false), array('id' => 'tipopropuesta-1')) !!}Licitación
                        
                        </div>
                    </div>
                </div> 
                <div class="row ">
                <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                </div>
                <div class="row ">
                    <div class="col-lg-6 col-xs-6">
                    TAG: {!! Form::text('propuestatag',(isset($propuesta->tag)==1?$propuesta->tag:''),array('class'=>'form-control input-sm','placeholder' => 'Palabra Clave')) !!}
                    </div> 
                    <div class="col-lg-6 col-xs-6">
                        Revisión: {!! Form::text('propuestarevision',(isset($propuesta->revision)==1?$propuesta->revision:(isset($revision)==1?$revision:'')),array('class'=>'form-control input-sm','placeholder' => '','readonly'=>'true')) !!} 
                    </div>
                </div>
                <div class="row ">
                    <div class="col-lg-4 col-xs-12">
                    <label>Servicio(*):</label>
                    {!! Form::select('servicio',$servicio,(isset($propuesta->cservicio)==1?$propuesta->cservicio:''),array('class' => 'form-control select-box','multiple'=>'true')) !!}
                      
                    </div>
                    <div class="col-lg-4 col-xs-12">
                    <label>Estado(*):</label>
                    {!! Form::select('estadopropuesta',$estadopropuesta,(isset($propuesta->cestadopropuesta)==1?$propuesta->cestadopropuesta:''),array('class' => 'form-control select-box')) !!}
                    <label>Rev. Principal(*):</label>
                    {!! Form::select('revprincipal',(isset($personal_rfp)==1?$personal_rfp:null),(isset($propuesta->cpersona_revprincipal)==1?$propuesta->cpersona_revprincipal:''),array('class' => 'form-control select-box','id'=>'revprincipal')) !!}
                    <label>Coor. Propuesta(*):</label>
                    {!! Form::select('coorpropuesta',(isset($personal_cp)==1?$personal_cp:null),(isset($propuesta->cpersona_coordinadorpropuesta)==1?$propuesta->cpersona_coordinadorpropuesta:''),array('class' => 'form-control select-box','id'=>'coorpropuesta')) !!}
                    <br>
                    <br>
                     <button type="button" class="btn btn-primary btn-block" disabled><b>Elegir contactos</b></button>
                    </div>
                    <div class="col-lg-4 col-xs-12">
                    <label>Resp. de propuesta(*):</label>
                    {!! Form::select('respropuesta',(isset($personal_rp)==1?$personal_rp:null),(isset($propuesta->cpersona_resppropuesta)==1?$propuesta->cpersona_resppropuesta:''),array('class' => 'form-control select-box','id'=>'respropuesta')) !!}
                    <label>Gte Proyecto(*):</label>
                    {!! Form::select('gtepropuesta',(isset($personal_gp)==1?$personal_gp:null),(isset($propuesta->cpersona_gteproyecto)==1?$propuesta->cpersona_gteproyecto:''),array('class' => 'form-control select-box','id'=>'gtepropuesta')) !!} 
                    <br>
                    <br>
                      <button type="button" class="btn btn-primary btn-block" id="idPaq"
                        @if ($editar)
                            
                        @else 
                            disabled
                        @endif                      
                      ><b>Paquetes de propuesta</b></button>   
                    </div>
                </div>
                <div class="row ">
                <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                </div>
                <div class="row ">
                    <div class="col-lg-4 col-xs-12">
                        Detalle de Propuesta:
                        @foreach($tipopropresentacion as $tipo)
                            <?php $activado=false; ?> 
                            <div>
                                @if (isset($propresentacion))
                                    @foreach($propresentacion as $p)
                                        @if($tipo->cpresentacion == $p->cpresentacion)
                                            <?php $activado=true; ?>
                                        @endif
                                    @endforeach
                                @endif
                                {!! Form::checkbox('presentacion[]',$tipo->cpresentacion,$activado) !!}
                                
                                
                                {{ $tipo->descripcion }}
                            </div>
                        @endforeach
                    </div>
                    <div class="col-lg-4 col-xs-12">
                    Observaciones (*):
                    {!! Form::textarea('propuestaobservaciones',(isset($propuesta->observaciones)==1?$propuesta->observaciones:''), array('class' => 'form-control', 'rows' => '6', 'placeholder' => 'Observaciones a la Propuesta')) !!}
                    </div>
                    <div class="col-lg-4 col-xs-12">
                        Moneda:
                            <div>
                            @foreach($monedas as $mon)
                            {!! Form::radio('moneda',$mon->cmoneda,(isset($propuesta->cmoneda)==1?($mon->cmoneda == $propuesta->cmoneda):false)) !!}
                            {{ $mon->descripcion }}
                            <br>
                            @endforeach
                            </div><br>
                        Medio de entrega:    
                            <div>
                            @foreach($medioentrega as $medio)
                            {!! Form::radio('mentrega',$medio->cmedioentrega,(isset($propuesta->cmedioentrega)==1?($medio->cmedioentrega == $propuesta->cmedioentrega):false)) !!}
                            {{ $medio->descripcion }}
                            <br>
                            @endforeach   
                            </div>    
                    </div>
                </div>

                <div class="row ">
                <div class="col-lg-12 col-xs-12 clsTitulo">
                Fechas
                </div>
                </div>
                <div class="row ">
                    <div class="col-lg-4 col-xs-12">
                                <label>Entrega de bases:</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <!--<input type="text" class="form-control pull-right" id="datepicker">-->
                                  {!! Form::text('propuestafbases',(isset($propuesta->fentregabases)==1?$propuesta->fentregabases:''), array('class'=>'form-control pull-right','id'=>'propuestafbases')) !!}
                                  
                                </div>   
                                <!--  ***************************  -->
                                <label>Presentación:</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <!--<input type="text" class="form-control pull-right" id="datepicker">-->
                                  {!! Form::text('fpresentacion',(isset($propuesta->fpresentacion)==1?$propuesta->fpresentacion:''), array('class'=>'form-control pull-right','id'=>'fpresentacion')) !!}                  
                                </div> 
                                <!--  ***************************  -->
                                <label>Visita técnica:</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <!--<input type="text" class="form-control pull-right" id="datepicker">-->
                                  {!! Form::text('fvtecnica',(isset($propuesta->fvtecnica)==1?$propuesta->fvtecnica:''), array('class'=>'form-control pull-right ','id'=>'fvtecnica')) !!}                  
                                </div>                                   
                                
                    </div>
                    <div class="col-lg-4 col-xs-12">
                                <label>consulta:</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <!--<input type="text" class="form-control pull-right" id="datepicker">-->
                                  {!! Form::text('fconsulta',(isset($propuesta->fconsulta)==1?$propuesta->fconsulta:''), array('class'=>'form-control pull-right ','id'=>'fconsulta')) !!}                  
                                </div>   
                                <!--  ***************************  -->
                                <label>Respuesta:</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <!--<input type="text" class="form-control pull-right" id="datepicker">-->
                                  {!! Form::text('frespuesta',(isset($propuesta->frespuesta)==1?$propuesta->frespuesta:''), array('class'=>'form-control pull-right ','id'=>'frespuesta')) !!}                  
                                </div> 
                                <!--  ***************************  -->    
                    </div>
                    <div class="col-lg-4 col-xs-12">
                                <label>Absolución de Consulta:</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <!--<input type="text" class="form-control pull-right" id="datepicker">-->
                                  {!! Form::text('fabsolucionconsultas',(isset($propuesta->fabsolucionconsultas)==1?$propuesta->fabsolucionconsultas:''), array('class'=>'form-control pull-right ','id'=>'fabsolucionconsultas')) !!}                  
                                </div>   
                                <!--  ***************************  -->
                                <label>Charla informativa:</label>

                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <!--<input type="text" class="form-control pull-right" id="datepicker">-->
                                  {!! Form::text('fcharla',(isset($propuesta->fcharla)==1?$propuesta->fcharla:''), array('class'=>'form-control pull-right','id'=>'fcharla')) !!}                  
                                </div> 
                                <!--  ***************************  -->    
                    </div>
                </div> 

                <div class="row ">
                    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                </div>

                <div class="row ">
                    <div class="col-lg-4 col-xs-4 cls-centered"></div>
                        
                        <button class="btn btn-primary btn-block" style="width:200px" 
                        @if(isset($bsave))
                            @if($bsave)
                            @else
                                disabled   
                            @endif
                        @else 
                            disabled
                        @endif 
                        id='btnSave'><b>Grabar</b></button>
                    </div>
                </div>
        

        </div>
        <!--<div class="col-lg-1 hidden-xs"></div>-->
        {!! Form::close() !!}


    </section>
    <!-- Modal Buscar Cliente-->
    <div class="modal fade" id="searchClientes" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" id="myModalLabel">Buscar Clientes</h4>
                </div>
                <div class="modal-body">
                    
                    <table id="table_id" class="table table-bordered table-hover" style="font-size: 12px" width="100%">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Cliente</th>
                            <th>Unidad Minera</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Código</th>
                            <th>Cliente</th>
                            <th>Unidad Minera</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btn-save" >Aceptar</button>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- Fin Modal Buscar Clientes -->
    
    <!-- Modal agregar Paquete -->
    <div id="addPaquete" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Agregar Paquetes</h4>
          </div>
          <div class="modal-body">
            <div class="row">
                <label for="txtcodpaq" class="col-md-2 control-label">Codigo:</label> 
                <div class="col-md-3">
                    <input type='text' id='txtcodpaq' id="txtcodpaq" class="form-control" />
                </div>
                <label for="txtcodpaq" class="col-md-2 control-label">Nombre:</label> 
                <div class="col-md-3">
                    <input type='text' id='txtnompaq' id="txtnompaq" class="form-control" />
                </div>
                <button type="button" class="btn btn-primary" id="btnAdd">Agregar</button>
            </div>
            <div class="table-responsive" id="divPaq">
                @include('partials.tablePaquetes')
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Aceptar</button>
          </div>
        </div>

      </div>
    </div>
    <!-- Fin Modal agregar Paquete-->


    <!-- Aporbaciones -->


@include('partials.modalAprobaciones')

</div>

<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>




<script>
        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
        var table=$("#table_id").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "buscarClientes",
                "type": "GET",
            },
            "columns":[
                {data: "codigosig" , name : "tuni.codigo"},
                {data: "nombre" , name : "tpersona.nombre"},
                {data: "uminera", name : "tuni.nombre"}

            ],
            "rowCallback" : function(row, data ){
              if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

                  $(row).addClass('selected');
              }                    
            },
          "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#table_id tbody').on( 'click', 'tr', function () {

            var id = this.id;
            var index = $.inArray(id, selected);
            var table = $('#table_id').DataTable();
            $('#' + this.id).each(function(indice, elemento) {
                //console.log('El elemento con el índice '+indice+' contiene '+$(elemento).html());
                $.each(elemento.cells, function (name , valor) { 
                        //console.log(name + '_' + valor.innerHTML);
                        if(name==1){
                            document.getElementById('name_cliente').value=valor.innerHTML ;
                        }else if(name==2){
                            document.getElementById('name_uminera').value=valor.innerHTML ;
                        }
                        else{
                            document.getElementById('cod_uminera').value=valor.innerHTML ;
                        }
                        
                });
            });
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);
            if ( index === -1 ) {
                selected.push( id );

            }
     
            $(this).toggleClass('selected');
        });
        var cli=0;
        var umin=0;
        $('#btn-save').on('click',function (){
            if(selected.length>0){
                temp= selected[0].substring(4);
                cli = temp.substring(0,temp.lastIndexOf('_'));
                umin = temp.substring(temp.lastIndexOf('_')+1);
                document.getElementById('idcpersona').value=cli;
                document.getElementById('iduminera').value=umin;
                document.getElementById('personanombre').value = document.getElementById('name_cliente').value;
                document.getElementById('umineranombre').value = document.getElementById('name_uminera').value;
                document.getElementById('umineracodigo').value = document.getElementById('cod_uminera').value;
                $('#searchClientes').modal('hide');
                $('#btnSave').prop('disabled',false);
            }else{
                alert('Seleccione un Cliente');
            }
        });



        $('#frmpropuesta').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            //$('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            
            $('select').parent().parent().removeClass('has-error');

            $.ajax({

                type:"POST",
                url:'grabarPropuesta',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     
                     $("#div_msg").show();

                     
                },
                error: function(data){
                    
                    
                    $.each(data.responseJSON, function (key, value) {
                            var input = '#frmpropuesta input[name=' + key + ']';
                            //$(input + '+span>strong').text(value);*/
                            if(!($(input).length > 0)){
                                input ='#frmpropuesta select[name=' + key + ']';
                            }
                            $(input).parent().parent().addClass('has-error');

                    });
                    $('#div_carga').hide();
                    
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });

        $('#propuestafbases').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        $('#fpresentacion').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });

        $('#fvtecnica').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        $('#fconsulta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        $('#frespuesta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        $('#fabsolucionconsultas').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        $('#fcharla').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });

        $('#idPaq').click(function(event){

            $("#addPaquete").modal({backdrop:"static"});
        });

        $('#addPaquete').on('show.bs.modal',function(){
            var id= $("#cpropuesta").val();

            $.ajax({
                url: 'listarPaquetes/'+id,
                type: 'GET',
                success : function(data){
                    $("#divPaq").html(data);
                },
                beforeSend : function(){

                }
                });
            
        });
        $("#btnAdd").click(function(event){
            event.preventDefault();

            $.ajax({
                url:'addPaquete',
                type: 'GET',
                data : 'codpaq='+ $("#txtcodpaq").val()+'&nompaq='+$("#txtnompaq").val()+'&cpropuesta='+$("#cpropuesta").val(),
                beforeSend: function(){

                },
                success: function(data){
                    $("#divPaq").html(data);
                    $("#txtcodpaq").val('');
                    $("#txtnompaq").val('');
                },

                });
            

        });
        function eliminarpaq(paq){
            $.ajax({
                url:'delPaquete',
                type: 'GET',
                data : 'cpropuestapaquete='+paq+'&cpropuesta='+$("#cpropuesta").val(),
                beforeSend: function(){

                },
                success: function(data){
                    $("#divPaq").html(data);


                },

                });            
        }
        $('#frmaprobacion').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token_aprob').value
            });
            e.preventDefault(e);  
            $("#myModal").modal('hide');
            $.ajax({

                type:"POST",
                url:'sgteFlujoPropuesta',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);


                },
                error: function(data){
                    $('#div_carga').hide(); 
                }                

            });          
        });
        

</script>

<!--Inicion del Script para funcionamiento de Chosen -->
<script>
    $('.select-box').chosen(
        {
            allow_single_deselect: true
        });
</script>
<!-- Fin del Script para funcionamiento de Chosen -->