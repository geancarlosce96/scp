<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Aprobaciones de entregables del Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div  id=tablaEntregableruteoap class="table table-reposive">
        
      @include('partials.ruteotable')
      
      </div>

    </section>
</div>

@include('partials.modalruteoLE')
@include('partials.modalcomentariosLE')
<script src="{{ url('js/ruteoedtproyecto.js') }}"></script>



