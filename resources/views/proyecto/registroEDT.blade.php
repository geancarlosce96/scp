<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>EDT del Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>    
        <div class="row">
        <div class="col-lg-10 col-xs-1"></div>
        <div class="col-lg-1 col-xs-1">
        <!-- <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Aprobaciones</button> -->
        </div>
        <div class="col-lg-1 col-xs-1"></div>
        </div>  
        {!! Form::open(array('url' => 'grabarEdtProyecto','method' => 'POST','id' =>'frmproyectoedt')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('ruteo',(isset($flujo)==1?$flujo:''),array('id'=>'ruteo')) !!}
        {!! Form::hidden('cproyectoedt','',array('id'=>'cproyectoedt')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="box box-primary">
        <div class="row">
            <div class="row"><br></div>
            <div class="col-lg-1 col-xs-1">Cliente:</div>
            <div class="col-lg-3 col-xs-3">
                {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
                {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','readonly' => 'true','id' => 'personanombre')) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Unidad minera:</div>
            <div class="col-lg-3 col-xs-3">
                {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','readonly' => 'true','id'=>'umineranombre') ) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Estado:</div>
            <div class="col-lg-1 col-xs-1">
                {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','readonly'=>'true')) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Buscar Proyecto:</div>
            <div class="col-lg-1 col-xs-1">
                <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto' title="Buscar proyecto">
                    <b>...</b>
                </button>
            </div> 
        </div>
        <div class="row">
            <div class="row"><br></div>
            <div class="col-lg-1 col-xs-1">Código Proyecto:</div>
            <div class="col-lg-2 col-xs-2">
                {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('readonly'=>'true','class'=> 'form-control input-sm' ,'id'=>'codigoproyecto')) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Nombre del Proyecto:</div>
            <div class="col-lg-6 col-xs-6">
                {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','readonly' =>'true','placeholder'=>'Nombre de Proyecto')) !!}   
            </div>
            
            <div class="col-lg-1 col-xs-1">
                <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='' onclick="getUrl('editarEntregablesProyecto/{{ (isset($proyecto)==1?$proyecto->cproyecto:'') }}','')" title="Dirigirse a la Lista de entregables">
                    <b>Ir a LE</b>
                </button>
            </div> 
            <div class="col-lg-1 col-xs-1">
                <!-- <button type="button" class="btn btn-success btn-app" onclick="verEDTgrafico({{isset($proyecto)==1?$proyecto->cproyecto:''}})"><b> <i class="glyphicon glyphicon-object-align-top"></i> EDT gráfico</b></button> -->
                <a class="btn btn-app" onclick="verEDTgrafico({{isset($proyecto)==1?$proyecto->cproyecto:''}})" style="background: #0069aa; color: white" title="Visualizar el EDT en modo gráfico"><i class="glyphicon glyphicon-object-align-top"></i>EDT gráfico</a>
            </div>
        </div>


        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>

        <div class="row">
          <div class="col-lg-12 col-xs-12">Registro EDT:</div>
        </div>

        <div class="row"><br></div>

        <div class="row">
          <div class="col-lg-1 col-xs-1">Tipo</div>
          <div class="col-lg-2 col-xs-2">
            {!! Form::select('tipo',(isset($tipoedt)==1?$tipoedt:array()),(isset($tipoedt->ctipoedt)==1?$tipoedt->ctipoedt:''),array('class' => 'form-control','id' => 'tipo','title'=>'Se refiere al tipo de documento')) !!}
          </div> 

          <div class="col-lg-1 col-xs-1">
            <button type="submit" class="btn btn-primary" id="btnSave" title="Agregar tipo de documento">
                <b>+</b>
            </button>
          </div>


            <div class="col-lg-2 col-xs-2">
              <div class="input-group input-group-normal">
                  {!! Form::text('revisionInt',(isset($revisiones_internas)==1?$revisiones_internas->revision_interna:''),array('class'=>'form-control input-sm','readonly' =>'true','placeholder'=>'Revisión interna ','id'=>'revisionInt','title'=>'Se refiere a generar una revisión cada vez que se modifique la LE sin ser emitida al cliente')) !!}   


                  {!! Form::hidden('cod_revisionInt',(isset($revisiones_internas)==1?$revisiones_internas->cproyectoent_rev:''),array('class'=>'form-control input-sm','readonly' =>'true','placeholder'=>'Revisión interna ','id'=>'cod_revisionInt')) !!}   

                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary btn-xl" id="btnRevInterna" title="Generar revisión interna"><b>Nueva revisión interna</b></button>
                  </span>                                            
              </div>  
            </div>  

            <div class="col-lg-2 col-xs-2">
              <div class="input-group input-group-normal">
                  {!! Form::text('revisionCli',(isset($documento->revision)==1?$documento->revision:''),array('class'=>'form-control input-sm','readonly' =>'true','placeholder'=>'Revisión cliente ','id'=>'revisionCli','title'=>'Se refiere a generar una revisión que cada vez que se actualizan y va ser emitida al cliente')) !!} 


                  {!! Form::hidden('cod_revisioncli',(isset($documento)==1?$documento->cproyectodocumentos:''),array('class'=>'form-control input-sm','readonly' =>'true','placeholder'=>'Revisión cliente ','id'=>'cod_revisioncli')) !!} 

                
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary btn-xl" id="btnRevCliente" title="Generar revisión para el cliente"><b>Nueva revisión cliente</b></button>
                  </span>                                            
              </div>  
            </div>

            <div class="col-lg-1 col-xs-1">Ver Revisiones</div>
            <div class="col-lg-2 col-xs-2">
              {!! Form::select('revisiones',(isset($revisiones_anteriores)==1?$revisiones_anteriores:array()),(isset($crevision)==1?$crevision:''),array('class' => 'form-control','id' => 'revisiones','title'=>'Se refiere al tipo de documento')) !!}
            </div> 

                   
          <!--
           <div class="input-group col-lg-5">
            <div class="input-group-prepend">
              <span class="input-group-text">Nivel</span>
              <input type="text" class="form-control">
              <button>sdfsdf</button>
            </div>           
          </div>

          <div class="col-lg-1 col-xs-1">Nivel</div>
          <div class="col-lg-1 col-xs-1">
            <select class="form-control" name="nivel" id="nivel">
                <option value="0">0</option>
             
            </select>
          </div>
          <div class="col-lg-1 col-xs-1">Código</div>
          <div class="col-lg-2 col-xs-2">
            <input type="text" class="form-control" name="codigo" id="codigo" readonly="true">
          </div>      
          <div class="col-lg-1 col-xs-1">Nombre</div>      
          <div class="col-lg-2 col-xs-2">
            <input type="text" class="form-control" name="nombre" id="nombre" readonly="true">
          </div>
          <div class="col-lg-1 col-xs-1">Insertar EDT:</div>-->


            
        </div>

        <div class="row"><br></div>

        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>

        <div class="row"><br></div>

        <div class="row">   
            <div class="col-lg-12 col-xs-12 table-responsive" id="divTableEDT">
              @include('partials.tableEDTProyecto',array('listaEDT' => (isset($listaEDT)==1?$listaEDT:array())))
            </div>

            <div class="col-lg-12 col-xs-12" > &nbsp;</div>
        </div>

        <div class="col-lg-12 col-xs-12" style="background-color:rgba(0, 105, 170, 1); max-height:1px;"></div>

        <div class="row">
            <div class="box-body">
                <div class="box-group" id="accordion">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel panel-primary">
                        <div class="box-header with-border panel-heading">
                            <h4 class="box-title panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseEnt" aria-expanded="false" class="collapsed"> <span class="glyphicon glyphicon-hand-up"></span>&nbsp;&nbsp;&nbsp;Ver entregables : <span id="nombreEDTSeleccionado"></span>  </a>
                            </h4>
                            <!--<button type="submit" class="btn btn-success bg-green pull-right">Registrar</button>-->
                            <div class="box-tools pull-right">
                              <button type="button" class="btn btn-success btn-xl verentregableruteo" id="cantidadentregablessel" title="Cantidad de entregables para validar" style="display: none; font-size: 12px; font-weight: bold; background: green;"></button>
                            </div>
                        </div>
                        <div id="collapseEnt" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="box-body">

                              <div class="col-lg-12 col-xs-12 table-responsive" id='tableEntdivLEntregable'>
                                @include('partials.tableTodosEntregablesProyecto',array('listEnt'=> (isset($listEnt)==1?$listEnt:array() )))
                              </div>
                                                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="box-footer pull-left"> -->
            <!-- <button type="submit" class="btn btn-primary" id="btnSave">Grabar</button> -->
            <!-- <button type="button" class="btn btn-primary" disabled="disabled"><b>Imprimir</b></button>  -->
            <!-- <button type="button"  class="btn btn-primary" disabled="disabled"><b>Vista Gráfica</b></button>  -->
        <!-- </div> -->
        <input type="hidden" name="fechaform" id="fechaform">
        <input type="hidden" name="tipofechaform" id="tipofechaform">
        <input type="hidden" name="formulario" id="formulario">
        <input type="hidden" name="cpryedt" id="cpryedt">
        <input type="hidden" name="tipoRevision" id="tipoRevision">
        <input type="hidden" name="nuevaRevision" id="nuevaRevision">
        <input type="hidden" id="revision_actual" value="{{isset($crevision)==1?$crevision:''}}">

        <div class="row clsPadding">
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
          <br>
          <div class="col-lg-4 col-xs-4"></div>
          <div class="col-lg-2 col-xs-2">
                <!-- <button type="button"  id="btnPrintEDT" class="btn btn-primary btn-block"><b>Imprimir</b></button>  -->
          </div>
        </div>




        
    </div>
 {!! Form::close() !!}
    </section>
    <!-- /.content -->
    @include('partials.searchProyecto')
    @include('partials.modalAprobaciones')
    @include('partials.modalistaEntregables')
    @include('partials.modalAgregarFechas')
    @include('partials.modalAsignarActividades')
  </div>


    @include('edt.modalgrafico')
    @include('edt.grafico')

    @include('partials.modalAlertAgregarRevisiones')
    @include('partials.modalAgregarRevisiones')
    @include('partials.modalAgregarNuevoEntregable')
    @include('partials.modalresumenLE')
    @include('partials.modalcomentariosLE')
    @include('partials.modallinkLE')


<!-- Script para modo grafico -->
<script src="{{ url('js/edt_grafico.js') }}"></script>

<!--<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>-->
<!-- <link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet"> -->
<script src="js/edt.js"></script>
<script src="js/lista_entregables.js"></script>
<script src="js/tabla_lista_entregables.js"></script>

<script src="{{ url('js/ruteoentregable.js') }}"></script>

<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>

<script type="text/javascript">
  var cproyecto = $('#cproyecto').val();
  var ruteo = $('#ruteo').val();
  var estado = 'REG';
  $.ajax({
    url: 'verentregableruteo',
    type: 'GET',
    data: {'cproyecto': cproyecto,'ruteo':ruteo,'estado':estado},
  })
  .done(function(data) {
    console.log("success",data.length);
    $('#cantidadentregablessel').show();
    $('#cantidadentregablessel').text("Enviar "+data+" entregables para validación");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  

  $('#btnruteo').click(function() {
    var entregables=[];
    var contador_entregable = 0;
     $('.entregable_ruteo').each(
      function() {
        entregables[contador_entregable] =$(this).data('entregable')+"_APR";
        contador_entregable++;
      });
     console.log(entregables);
     grabaruteo(entregables,'ruteo');
     $('#modalresumenLE').modal('toggle');
     $('#cantidadentregablessel').hide();
   });

  $('#btnlink').click(function() {
    var rutatodos = $("#rutatodos").val();
    var entregables= validarcheck_link();

    if (rutatodos == '') {
      $("#rutatodos").focus();
      swal("Ingrese ruta de entregable");
    }else{

    // console.log("rutatodos",rutatodos,entregables);

     $.ajax({
       url: 'actualizarruteo',
       type: 'GET',
       // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
       data: {'entregables': entregables, 'rutatodos': rutatodos},
     })
     .done(function(data) {
       console.log("rutatodos link",data.entregables);

       // for (var i = 0; i < data.entregables.length; i++) {
       for (var i = data.entregables.length - 1; i >= 0; i--) {
          console.log(data.entregables[i],data.rutatodos);
          $("#ruta_"+data.entregables[i]).show();
          $("#ruta_"+data.entregables[i]).text(data.rutatodos);
          $("#rutatodos").val('');
          $('.checkbox_link').prop('checked', false);
          $('.checkbox_link').hide();
       }

     })
     .fail(function() {
       console.log("error");
     })
     .always(function() {
       console.log("complete");
     });
     
     $('#modallinkLE').modal('toggle');
   }
     // $('#cantidadentregablessel').hide();
   });


</script>


<script>

        var selected =[];
        
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyLider",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'GteProyecto' , name : 'p.abreviatura'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'},
                {data : 'descripcion' , name : 'te.descripcion'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);
            if ( index === -1 ) {
                selected.push( id );

            }
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarEdtProyecto/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $('#fechaA').datepicker({
            format: "dd-mm-yy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });

        $('#fechaB').datepicker({
            format: "dd-mm-yy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });    

        $('#fecha0').datepicker({
            format: "dd-mm-yy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        $("#btnAddEDT").click(function(e){
          $.ajax({
            url: 'addEDTPy',
            type:'GET',
            data: "cod=" + $("#cod").val()+"&nom=" + $("#nom").val() + "&cproyecto=" + $("#cproyecto").val() ,
              beforeSend: function () {
                  $("#cod").val('');
                  $("#nom").val('');
                  $('#div_carga').show(); 
          
              },              
            success : function (data){
              $("#divTableEDT").html(data);
              $('#div_carga').hide(); 
            },
          });
        });

        function fdelEDT(id){
          $.ajax({
              url: 'delEDTPy/'+id,
              type: 'GET',
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){
                $("#divTableEDT").html(data);
                 $('#div_carga').hide(); 
              },

          });
        }

        function EditarEDT(id){
            $.ajax({
            url: 'editEDTProyecto/'+id,
            type:'GET',
              beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){

              var str = JSON.stringify(data);

              var pushedData = jQuery.parseJSON(str);
              
              $('#cproyectoedt').val(pushedData['cproyectoedt']);
              $('#tipo').val(pushedData['ctipoedt']);
              $('#nivel').val(pushedData['nivel']);
              $('#codigo').val(pushedData['codigo']);
              $('#nombre').val(pushedData['descripcion']);

              $('#nombre').removeAttr('readonly');
              $('#codigo').removeAttr('readonly');

              $('#tipo').prop('disabled',true);
              
            },
          });
        }


        $("#frmproyectoedt").on('submit',function(e){ 
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarEdtProyecto',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                    // $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     //$("#div_msg").show();

                },
                error: function(data){
                    
                    
                    $.each(data.responseJSON, function (key, value) {
                            var input = '#frmproyectoedt input[name=' + key + ']';
                            if(!($(input).length > 0)){
                                input ='#frmproyectoedt select[name=' + key + ']';
                            }
                            $(input).parent().parent().addClass('has-error');

                    });
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });


        
        //Guardar
        //$("#btnSaveEnt").click(function(e){
        


        

/*        function verfechas(id,desc_ent){

            limpiarfechas();
            $.ajax({
            url: 'viewFechasEntregablesPry/'+id,
            type:'GET',
            beforeSend: function () {
                $('#div_carga').show(); 

          
              },              
            success : function (data){              
                $('#div_carga').hide();     
                $("#divFechaEntregable").html(data);
                $('#centregableproy_fecha').val(id);
                $('#nomb_ent').val(desc_ent);
                $('#modalfechas').modal('show');

             //          

              },
            });
            
      
        }

        var idDashFecha= '';
        function verDashFec(id){
          $("#"+id).show();

          idDashFecha=id;

      

        }
        function ocultarDashFec(){
        
          $("#"+idDashFecha).hide();
         
        }  
       

       var entregableTemp='';


  

function Listaentregables(data){ 
   

    var availableTags = data;
   
    function split( val ) {
      return val.split( /,\s );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }

    if($( "#entregableN0" ).val().substring($( "#entregableN0" ).val().length-1,$( "#entregableN0" ).val().length) !='.'){
 
        $( "#entregableN0" )
          // don't navigate away from the field on tab when selecting an item
          .on( "keydown", function( event ) {
            if ( event.keyCode === $.ui.keyCode.TAB &&
                $( this ).autocomplete( "instance" ).menu.active ) {
              event.preventDefault();
            }
          })

          .autocomplete({
            minLength: 0,
            source: function( request, response ) {
              // delegate back to autocomplete, but extract the last term
              response( $.ui.autocomplete.filter(
                availableTags, extractLast( request.term ) ) );
            },
            focus: function() {
              // prevent value inserted on focus
              return false;
            },
            select: function( event, ui ) {
              var terms = split( this.value );
              // remove the current input
              terms.pop();
              // add the selected item
              terms.push( ui.item.value );
              // add placeholder to get the comma-and-space at the end
              terms.push( "" );
             
              this.value = terms.join( "." );
              return false;
            }
          });
      }
}


$('.select-box').chosen(
    {
        allow_single_deselect: true,width:"100%"
    });

$('#fecha').datepicker({
    format: "dd-mm-yyyy",
    language: "es",
    autoclose: true,
    calendarWeeks:true,
});


function editarEntregable(id){
    $.ajax({
    url: 'getEntregableProyecto/'+id,
    type:'GET',
      beforeSend: function () {
  
      },              
    success : function (data){


      var str = JSON.stringify(data);

      var pushedData = jQuery.parseJSON(str);

      //alert(str);
         
      entregableTemp=pushedData['centregable'];
      $('#cproyectoentregables').val(pushedData['cproyectoentregables']);
      $('#disciplina').val(pushedData['disciplina']);
      $('#tipoentregable').val(pushedData['tipoent']);   

      //

      $('#codigoent').val(pushedData['codigo']);
      $('#tipoproyent').val(pushedData['ctipoproyectoentregable']);
      $('#observacionent').html(pushedData['observacion']);
      $('#responsable').val(pushedData['cpersona_responsable']);

      listado();

      $('#entregableN0').val(entregableTemp);
    

      
    },
  });
}*/

$("#nuevarevisionModal").on("change",function() {

  $("#nuevaRevision").val($(this).val());

 //alert($("#nuevarevisionModal").val());

 
});

$("#btnRevCliente").on("click",function() {
  $("#tipoRevision").val('cliente');

  vermodalAlertRev();

 
});

$("#closeRevision").on("click",function() {

    $('#modalRevisiones').modal('hide');

 
});

$("#btnRevInterna").on("click",function() {
  $("#tipoRevision").val('interna');
  //grabarRevision();
  vermodalAlertRev();
 
});

$("#revisiones").on("change",function() {

    revision = $(this).val();

    if (revision == $('#revision_actual').val()) {
      getUrl('editarEdtProyecto/'+$('#cproyecto').val());
    }

    var url = BASE_URL + '/verDetalleEDT/'+$('#cproyecto').val()+'/'+revision;
      $.get(url)
        .done(function( data ) { 
           
              $('#divTableEDT').html(data);

              $("a.verLE").on("click",function() {    
                var idEdt=$(this).parent().parent().data("edt"); 
                verLEporEDT(idEdt);
                var nomEDT= $(this).parent().parent().find(".descripcion").find("span").text();
                var codigo= $(this).parent().parent().find(".codigo").find("span").text();
                if (codigo.length>0) {
                  codigo+=' - ';

                }
                var encabezado= codigo+nomEDT;

                
                $('#cpryedt').val(idEdt);
                $('#nombreEDTSeleccionado').html(encabezado);


              }); 
            
            
        });

   
});

function grabarRevision(){
  $.ajax({
    url: 'grabarRevisionEntregable',
    type:'POST',
    data: $("#frmproyectoedt").serialize(),
    beforeSend: function () {
      $('#div_carga').show(); 
    },              
    success : function (data){
      $('#div_carga').hide(); 
      $("#tipoRevision").val('');

      $("#resultado").html(data);
      $('.modal-backdrop').remove();
         
      },
  });
}

$("#btnSI").on("click",function() {
    if ($("#tipoRevision").val()=='interna'){
      grabarRevision();
    }

    else{
      verModalRevisiones();
    }
 
});

$("#btnNO").on("click",function() {
   cerrarModalAlerRev();
 
});

$("#saveRevision").on("click",function() {
     grabarRevision();
 
});

$("#cancelRevision").on("click",function() {
    cerrarModalRevisiones();
 
});

function vermodalAlertRev(){

    $('#mensajeRev').html('¿Desea pasar a una nueva revisión?');

    $('#modalAlertRev').modal({
        backdrop:"static"
    });

}

function cerrarModalAlerRev(){
    
    $('#modalAlertRev').modal('hide');
}

function verModalRevisiones(){

    $('#modalAlertRev').modal('hide');
    $('#modalRevisiones').modal({
        backdrop:"static"
    });

}

function cerrarModalRevisiones(){          

    $('#modalRevisiones').modal('hide');
    $("#nuevaRevision").val('');
    $("#nuevarevisionModal").val('');
}

</script>  


