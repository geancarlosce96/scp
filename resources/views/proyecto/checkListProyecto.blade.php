<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Checklist del Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoProyecto')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>
            
        {!! Form::open(array('url' => 'grabarChecklistProy','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('cproyectochecklist','',array('id'=>'cproyectochecklist')) !!}



    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
   <div class="row">
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
    <div class="col-lg-9 col-xs-9 clsPadding">{!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
    {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
    </div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}</div> 
    <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
   </div>    
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
        <div class="col-lg-9 col-xs-9">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}   
        </div>	 
    </div> 
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
        <div class="col-lg-4 col-xs-4">
        {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right','disabled' =>'true')) !!}</div> 
        <div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>
        <div class="col-lg-4 col-xs-4">{!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}</div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
            <label class="clsTxtNormal">Estado:</label>
            
        {!! Form::select( 'cestadoproyecto',(isset($testado)==1?$testado:array() ),'',array('class' => 'form-control','id'=>'cestadoproyecto')) !!} 
             
        </div>
        <div class="col-lg-6 col-xs-6">
            <label class="clsTxtNormal">Fase del Proyecto:</label>
              
        {!! Form::select( 'cfaseproyecto',(isset($tfase)==1?$tfase:array() ),'',array('class' => 'form-control','id'=>'faseproy')) !!} 
                     
        </div>    
    </div>
    

    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-6">
        Código de Documento:
        	<input class="form-control input-sm" type="text" placeholder="Cod. documento">
        
        </div>
        <div class="col-lg-3 col-xs-6">
        Revisión:
        	<input class="form-control input-sm" type="text" placeholder="Rev.">
                
        </div>
        <div class="col-lg-3 col-xs-6 clsPadding2">
        	<a href="#" class="btn btn-primary btn-block "> <b>Cambiar Rev - Cod</b></a>        
        </div>  
        <div class="col-lg-3 col-xs-6 clsPadding2">
        	<a href="#" class="btn btn-primary btn-block "> <b>Historial Rev.</b></a>        
        </div>                 

    </div>
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
		<div class="col-lg-1 col-xs-2 clsPadding">Rol</div>
        <div class="col-lg-5 col-xs-4 clsPadding">
              
                {!! Form::select( 'croldespliegue',(isset($trol)==1?$trol:array() ),'',array('class' => 'form-control','id'=>'croldespliegue')) !!} 
          
        </div> 

        <div class="row clsPadding2">    
      <div class="col-lg-2 col-xs-2"><button type='button' class="btn btn-primary btn-block" data-toggle="modal" data-target="#btnAddcheckEnt" > <b>Agregar</b></button></div>      
    </div>      

        <div class="col-lg-6 col-xs-6"></div>  
    
    </div>

<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive" id="idEnt">
        @include('partials.modalcheckListProyecto',array('checkEnt'=>(isset($checkEnt)==1?$checkEnt:null )  , 'checkEnt' => (isset($checkEnt)==1?$checkEnt:null ) ))
    </div>
</div>   

    
 
<div class="row">
    <div class="col-lg-3 col-xs-12 clsPadding">
        <button type="submit" class="btn btn-primary btn-block"><b>Grabar</b>
        </button>
    </div>
    <div class="col-lg-3 col-xs-12 clsPadding"><a href="#" class="btn btn-primary btn-block"><b>Cancelar</b></a> 
    </div>
    <div class="col-lg-3 col-xs-12 clsPadding"><a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> 
    </div>
</div>   
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
    {!! Form::close() !!}
    </section>
      @include('partials.searchProyecto')
    <!-- /.content -->
    @include('partials.addcheckList')
  </div>


  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
      

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );  

        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarChecklistProy/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });          

       

         //Para el boton Grabar CheckList
         $('#frmproyecto').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            

            $.ajax({

                type:"POST",
                url:'grabarChecklistProy',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
                
                },
                success: function(data){

                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });  

         
         //Agregar CheckList
         $("btnAddcheckEnt").on('show.bs.modal',function(e){

         });

       
       //Para el boton Aceptar CheckList  
       $("#idcheckAct").click(function(e){
        $.ajax({
            url:'btnAddcheckEntPy',
            type:'POST',
            data: $('#frmentregable').serialize(),
            beforeSend: function(){
                $('#div_carga').show();
            },
            success: function(data){
                $('#idEnt').html(data);
                $('#div_carga').hide();
            },
        });

    }); 




</script>  