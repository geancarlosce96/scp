<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Cronograma del Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoProyecto')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>

        <div class="row">
        <div class="col-lg-10 col-xs-1"></div>
        <div class="col-lg-1 col-xs-1">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Aprobaciones</button>
        </div>
        <div class="col-lg-1 col-xs-1"></div>
        </div>  
        {!! Form::open(array('url' => 'grabarCronogramaProyecto','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12  col-xs-12">
    
   <div class="row">
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
    <div class="col-lg-9 col-xs-9 clsPadding">{!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
      {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}</div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block btn-sm" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 clsPadding"> {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}</div> 
    <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
   </div>    
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
        <div class="col-lg-9 col-xs-9">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}   
        </div>	 
    </div> 
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
        <div class="col-lg-4 col-xs-4"><div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right','disabled' =>'true')) !!}</div> </div> 
        <div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>
        <div class="col-lg-4 col-xs-4"><div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}</div></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
            <label>Estado:</label>
              {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!} 
        </div>
        <div class="col-lg-6 col-xs-6">
          Código del Documento:
              {!! Form::text('coddocumento',(isset($documento->codigo)==1?$documento->codigo:''), array('class'=>'form-control input-sm','placeholder' => 'Código de Documento','disabled' => 'true','id'=>'coddocumento') ) !!} 
        </div>    
    </div>
 <div class="row clsPadding2">   
    <div class="col-lg-2 col-xs-2">Revisión del Documento:</div>
    <div class="col-lg-4 col-xs-4">
    {!! Form::text('revisiondocumento',(isset($documento->revision)==1?$documento->revision:''),array('class'=>'form-control pull-right','disabled'=>'disabled')) !!}
    </div>
    
  </div>     
    <!--<div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">Importar:</div>
    </div>        
    <div class="row clsPadding2">
        <div class="col-lg-1 col-xs-1">Ruta: </div>
        <div class="col-lg-10 col-xs-10"><input class="form-control input-sm" type="text" placeholder="Ruta"></div>
        <div class="col-lg-1 col-xs-1">
        	<button type="button" class="btn btn-primary btn-block btn-sm" data-toggle='modal' data-target="#uploadFile"><b>...</b></button></a>
        </div>
    </div>
    <div class="row clsPadding2">
	    <div class="col-lg-2 col-xs-2">Versión Actual:</div>
      <div class="col-lg-2 col-xs-2"><input class="form-control input-sm" type="text" placeholder="Ruta" disabled></div>
      <div class="col-lg-2 col-xs-2">Versión Nueva:</div>
      <div class="col-lg-2 col-xs-2"><input class="form-control input-sm" type="text" placeholder="Ruta" disabled></div>
      <div class="col-lg-2 col-xs-2">Generar nueva versión:</div>
      <div class="col-lg-1 col-xs-1"><input type="checkbox" name="chkVersion" id="version" value="op1" checked=""></div> <div class="col-lg-1 col-xs-1">
        	<a href="#" class="btn btn-primary btn-block"><b><i class="fa fa-floppy-o" aria-hidden="true"></i></b></a>
      </div>        
    </div>    -->

    <!--
    <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">Exportar:</div>
    </div>        
    <div class="row clsPadding2">
        <div class="col-lg-1 col-xs-1">Ruta: </div>
        <div class="col-lg-10 col-xs-10"><input class="form-control input-sm" type="text" placeholder="Ruta" disabled></div>

        <div class="col-lg-1 col-xs-1">
        	<a href="#" class="btn btn-primary btn-block"><b>...</b></a>
        </div>
    </div>-->
    <!--<div class="row clsPadding2">
	    <div class="col-lg-2 col-xs-2">Versión Actual:</div>
        <div class="col-lg-2 col-xs-2"><input class="form-control input-sm" type="text" placeholder="Ruta" disabled></div>
        <div class="col-lg-1 col-xs-1">
        	<a href="#" class="btn btn-primary btn-block"><b><i class="fa fa-external-link-square" aria-hidden="true"></i></b></a>
        </div>        
        
    </div> 
    
    
    <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">Importar Actividades de SOC:</div>
    </div>        
    <div class="row clsPadding2">
        <div class="col-lg-1 col-xs-1">Ruta: </div>
        <div class="col-lg-10 col-xs-10"><input class="form-control input-sm" type="text" placeholder="Ruta"></div>
        <div class="col-lg-1 col-xs-1">
        	<a href="#" class="btn btn-primary btn-block"><b>...</b></a>
        </div>
    </div>
    <div class="row clsPadding2">
	    <div class="col-lg-2 col-xs-2">Versión Actual:</div>
        <div class="col-lg-2 col-xs-2"><input class="form-control input-sm" type="text" placeholder="Ruta" disabled></div>
        <div class="col-lg-2 col-xs-2">Versión Nueva:</div>
        <div class="col-lg-2 col-xs-2"><input class="form-control input-sm" type="text" placeholder="Ruta" disabled></div>
      <div class="col-lg-2 col-xs-2">Generar nueva versión:</div>
      <div class="col-lg-1 col-xs-1"><input type="checkbox" name="chkVersion" id="version" value="op1" checked=""></div> <div class="col-lg-1 col-xs-1">
        	<a href="#" class="btn btn-primary btn-block"><b><i class="fa fa-floppy-o" aria-hidden="true"></i></b></a>
      </div>        
    </div> -->  
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>    
    <div class="row clsPadding2">
      <div class="col-md-4 col-xs-4"></div>
      <div class="col-md-2 col-xs-2">
        <button type="button" class="btn btn-default btn-md" id="add">
          <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> Agregar
        </button>
      </div>
      <div class="col-md-2 col-xs-2">
        <button type="button" class="btn btn-default btn-md" id="del">
          <span class="glyphicon glyphicon-minus-sign" aria-hidden="true"></span> Eliminar
        </button>
      </div>      
      <div class="col-md-4 col-xs-4"></div>
    </div>
    <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">Cronograma:</div>
    </div>   
    <div class="row clsPadding2">	
        <div class="col-lg-12 col-xs-12 table-responsive" id="tableCrono">
          @include('partials.tableCronogramaProyecto',array('listCro'=> (isset($listCro)==1?$listCro:array() )))    
    	</div>
    </div>

    
    
    
    
    
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-4 col-xs-4">
            <button class="btn btn-primary btn-block btn-sm" style="width:200px;"><b>Grabar</b></button> 
        </div>
        <div class="col-lg-4 col-xs-4">
            <!--<a href="#" class="btn btn-primary btn-block" style="width:200px;"><b>Cancelar</b></a> -->
        </div>        
    </div>
    
    <!--
    <div class="row">
    <div class="col-lg-3 col-xs-12 clsPadding"><a href="#" </class="btn btn-primary btn-block"><b>Nuevo</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding"><a href="#" class="btn btn-primary btn-block"><b>Editar</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding"><a href="#" class="btn btn-primary btn-block"><b>Nueva Revisión</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding"><a href="#" class="btn btn-primary btn-block"><b>Transmittals</b></a> </div>
    
        </div>
    	-->
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
    {!! Form::close() !!}
    </section>
    <!-- /.content -->
  @include('partials.searchProyecto')
   @include('partials.modalAprobaciones')

<!-- Modal Upload Archivo-->
     
    <div id="uploadFile" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Subir Archivos</h4>
          </div>
          <div class="modal-body">
          {!! Form::open(
              array(
                  'route' => 'uploadFile', 
                  'class' => 'form', 
                  'novalidate' => 'novalidate', 
                  'files' => true,
                  'id' => 'frmUploadFiles'
                  )) !!}
            {!! Form::hidden('cproyecto_file',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:'')) !!}

              <div class="form-group">
                  {!! Form::label('Especificar el Archivo XML (Project)') !!}
                  {!! Form::file('fileXml', null,array('class'=>'form-control')) !!}
              </div>

              <div class="form-group">
                  {!! Form::submit('Subir Archivos',array('class'=>'btn btn-sucess')) !!}
              </div>
           {!! Form::close() !!} 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" >Cerrar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal Upload Archivo-->

  </div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
        var i_filas = 1;
        var des_act = [''];
        var cod_act = [''];
        var cod_ent = [''];
        var des_ent = [''];
        var id = -10;
        var i = 100;
        <?php if(isset($actividades_s)){ ?>
          <?php foreach($actividades_s as $act) { ?>
              cod_act[cod_act.length] = "<?php echo $act['cactividad']; ?>";
              des_act[des_act.length] = "<?php echo $act['descripcion']; ?>";
          <?php } ?>
        <?php } ?>

        <?php if(isset($entregables_s)){ ?>
          <?php foreach($entregables_s as $ent) { ?>
              cod_ent[cod_ent.length] = "<?php echo $ent['cproyectoentregables']; ?>";
              des_ent[des_ent.length] = "<?php echo $ent['descripcion']; ?>";
          <?php } ?>
        <?php } ?>        

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarCronogramaProyecto/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });           

        $("#add").click(function(e){
          id = id -1;
          i++;
          new_fila="<tr>";
          item = "<td><input type='hidden' name='cro["+i+"][0]' value='"+id+"' /><input type='text' name='cro["+i+"][1]' class='form-control' /></td>";
          act = "<td><select class='form-control' name='cro["+i+"][2]' id='act_"+i_filas+"'></select></td>";
          dur = "<td><input type='text' class='form-control' name='cro["+i+"][3]' id='dur_"+i_filas+"'/></td>";
          fini= "<td><div class='input-group date'><div class='input-group-addon'><i class='fa fa-calendar'></i></div><input type='input' class='form-control pull-right datepicker' id='fini_"+i_filas+"' name='cro["+i+"][4]'/></div></td>";
          ffin= "<td><div class='input-group date'><div class='input-group-addon'><i class='fa fa-calendar'></i></div><input type='input' class='form-control pull-right datepicker' id='ffin_"+i_filas+"' name='cro["+i+"][5]'/></div></td>";
          pre = "<td><input type='text' class='form-control' id='pre_"+i_filas+"' name='cro["+i+"][6]'/></td>";
          suc = "<td><input type='text' class='form-control' id='suc_"+i_filas+"' name='cro["+i+"][7]'/></td>";
          recu = "<td><input type='text' class='form-control' id='rec_"+i_filas+"' name='cro["+i+"][8]'/></td>";
          mon = "<td><input type='text' class='form-control' id='mon_"+i_filas+"' name='cro["+i+"][9]'/></td>";
          ent = "<td><select class='form-control' name='cro["+i+"][10]' id='ent_"+i_filas+"'></select></td>";

          new_fila = new_fila + item + act + dur + fini + ffin + pre + suc + recu + mon + ent + "</tr>";

          $("#cro tr:last").after(new_fila);
          $("#fini_"+i_filas).datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose:true
          });
          $("#ffin_"+i_filas).datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose:true
          });
          fillActividad('act_'+i_filas);
          fillEntregable('ent_'+i_filas);
          i_filas++;
          
        });

        $("#del").click(function(e){
          
        });    

        function fillActividad(id){
            $("#"+id).append(new Option('','',true,true));
            for(i=0;i < cod_act.length ; i++){
              $("#"+id).append(new Option(des_act[i],cod_act[i]));
            }
        }   

        function fillEntregable(id){
            $("#"+id).append(new Option('','',true,true));
            for(i=0;i < cod_ent.length ; i++){
              $("#"+id).append(new Option(des_ent[i],cod_ent[i]));
            }
        } 

         $('#frmproyecto').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            

            $.ajax({

                type:"POST",
                url:'grabarCronogramaProyecto',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    /*
                    $.each(data.responseJSON, function (key, value) {
                            var input = '#frmpropuesta input[name=' + key + ']';
                            $(input + '+span>strong').text(value);
                            $(input).parent().parent().addClass('has-error');

                    });*/
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });  
        $('#frmUploadFiles').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            var fd = new FormData(document.getElementById('frmUploadFiles'));
            
            
            $.ajax({

                type:"POST",
                url:'uploadFileXml',
                data: fd,
                processData : false,
                contentType :  false,
                beforeSend: function () {
                  
                  $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     //$("#resultado").html(data);
                     $("#uploadFile").modal('hide');
                },
                error: function(data){
                    $('#div_carga').hide();

                }
            });
        });
</script>  