@extends('layouts.master')
@section('content')

<div class="content-wrapper">


    <div class="col-sm-12">
      <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ANDDES - Curva S
        <small>Gestión del valor ganado</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
      {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
    </section>
    <hr>

<div class="col-md-3">

  <input type="hidden" value="{{$lider_or_gerente}}" id="lider_or_gerente">

  <select id="proyectos" class="form-control select-box" data-placeholder="Elegir un proyecto..."> 
    <option value="0" >Seleccione un proyecto</option>
    @foreach($tproyectos as $proy)
    <option data-finicio="{{ $proy->finicio }}" data-fcierre="{{ $proy->fcierre }}" value="{{ $proy->cproyecto }}" >{{ $proy->codigo }} {{ $proy->nombre }}</option>
    @endforeach
  </select>
</div>
<div class="col-md-2">
  <select id="cbo_areas" class="form-control area-chosen" name="cbo_areas" data-placeholder="Elegir un area...">
    <option value=""></option>
  </select>
</div>

<div class="col-md-2">
  <div class="col-sm-3 clsPadding">Fecha inicio</div>            
  <div class="input-group date">
    <div class="input-group-addon">
      <i class="fa fa-calendar"></i>
    </div>
    {!! Form::text('fdesde','', array('class'=>'form-control pull-right ','id' => 'fdesde','readonly'=>'true')) !!}
  </div>
</div>


<div class="col-md-2">
  <div class="col-sm-3 clsPadding">Fecha Final</div>            
  <div class="input-group date">
    <div class="input-group-addon">
      <i class="fa fa-calendar"></i>
    </div>
    {!! Form::text('fhasta','', array('class'=>'form-control pull-right ','id' => 'fhasta','readonly'=>'true')) !!}
  </div>        
</div>

<div class="col-md-2">
  <button id="btngenerar" class="btn btn-block btn-primary">Generar</button>
</div>
<div class="col-lg-1" style="display: none;" id="imgLoad" >
  <img src="{{ asset('img/load.gif') }}" width="25px" >
</div>  
<div class="" style="margin-top: 9em"></div> 
  
  <!-- <div id="curva-s" class="body-content"> -->
  <div class="col-sm-6 col-lg-6 col-md-6 col-xs-6">
    @include('proyecto.performance.curva-s')
    @include('proyecto.performance.curvas_costos')
  </div>
  <div class="col-sm-6 col-lg-6 col-md-6 col-xs-6">
    @include('proyecto.performance.curvas_indicadores')
  </div>
  <div class="col-sm-12 col-lg-12 col-md-12 col-xs-12">
    @include('proyecto.performance.curvaSPorcentaje')
  </div>
  <!-- </div> -->
  <!-- <div id="curvarate-s">
    
  </div> -->
  <!--   <div id="avance">
    
  </div>-->
  <!-- {{-- <div class="prueba">HOLA MUNDO</div> --}} -->
  
</div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="{{ url('js/curva.js') }}"></script>
<script src="{{ url('js/curva_tiempo.js') }}"></script>
<script src="{{ url('js/curva_costos.js') }}"></script>
<script src="{{ url('js/curva_indicadores.js') }}"></script>
<script src="{{ url('js/curvas-Porcentaje.js') }}"></script>


@stop 