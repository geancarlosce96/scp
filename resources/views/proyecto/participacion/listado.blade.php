@extends('layouts.master')
@section('content')
<input type="hidden" id="periodo" value="{{ $periodo }}">
<input type="hidden" id="semana_ini" value="{{ $semana_ini }}">
<input type="hidden" id="semana_fin" value="{{ $semana_fin }}">
<div class="content-wrapper" style="min-height: 900px;">

<section class="content-header">
	<h1> Participación en Proyectos</h1>
</section>
    
<section class="content" style="font-size: 14px">
	
	<div class="col-md-2">
		Semana Inicio: 
	</div>

	<div class="col-md-1">
		<input type="text" id="semanaini" class="form-control" value="{{$semana_ini}}" onkeyup="semanaini()">		
	</div>

	<div class="col-md-2">
		Semana Final: 
	</div>

	<div class="col-md-1">
		<input type="text" id="semanafin" class="form-control" value="{{$semana_fin}}" onkeyup="semanafin()">		
	</div>

	<div class="col-md-1">
		Periodo: 
	</div>

	<div class="col-md-2">
		<select id="cbPeriodo" class="form-control" >
			@foreach($periodos as $periodo)
			<option value="{{$periodo}}" >{{$periodo}}</option>
			@endforeach
		</select>		
	</div>

	<div class="col-md-1" style="display: none" id="imgLoad" >
		<img src="{{ asset('img/load.gif') }}" width="25px" >
	</div>
	
	<div class="col-md-12" style="margin-top: 25px;" >
		<table id="tbListado" class="table table-striped table-hover" >
	      <thead>
	      <tr style="background-color: #0069aa; color:white;">
	        <th>Ítem</th>
	        <th>Codigo</th>        
	        <th>Actividad</th>
	        <th>Nombre Proyecto / Actividad</th>
	        <!-- <th>Periodo</th> -->
	        <th>Semana</th>
	        <th>Horas Planificadas</th>
	        <!-- <th>Tipo</th> -->
	        <th>Lider</th>
	        <th>Gerente</th>
	        <!-- <th>F. Registro</th> -->
	      </tr>
	      </thead>
	      <tbody>	      	
	      </tbody>
  		</table>
	</div>

	<div class="col-md-12">
		<p>Se han encontrado <span id="cantidadResultados" >0</span> resultado(s)</p>
	</div>

</section>

</div>
@stop

@section('js')
	<script type="text/javascript" src="{{ url('js/participacion_proyectos.js') }}"></script>
@stop
