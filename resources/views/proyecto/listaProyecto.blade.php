<div class="content-wrapper">
    <section class="content-header">
        <h1>Proyecto <small>Listado de proyecto</small></h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
            <li class="active">Proyecto</li>
        </ol>

        <div class="panel box box-primary" style="margin-top: 25px;">

            <div class="col-md-3 col-xs-3" style="padding-top: 5px;">
                <div class="col-lg-3 col-xs-3 clsPadding"><span>Estado Proyecto</span></div>
                <div class="input-group" style="width:65%">
                    <select  class="form-control chosen" id="estadoproyecto" name="estadoproyecto" >
                        <!-- <option value="">Buscar por estado</option> -->
                        @foreach($estadoproyecto as $estado)
                            <option value="{{ $estado->cestadoproyecto }}"> {{ $estado->descripcion }} </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-4 col-xs-4" style="padding-top: 5px;">
                <div class="col-lg-2 col-xs-2 clsPadding"><span>Tipo Proyecto</span></div>
                <div class="input-group" style="width:80%">
                    <select  class="form-control  mdb-select md-form tipoproyecto"  multiple  id="tipoproyecto" name="tipoproyecto">
                        @foreach($tiproy as $tp)
                            <option value="{{ $tp->ctipoproyecto }}"> {{ $tp->descripcion }} </option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-3 col-xs-3" style="padding-top: 5px;">
                <div class="col-lg-3 col-xs-3 clsPadding"><span>Genera ingresos:</span></div>
                <div class="input-group form-check">
                    @foreach($genera as $gener)
                        <label class="checkbox-inline"><input type="checkbox" class="lingreso" name="lingreso" value="{{ $gener->digide }}" @if($gener->descripcion =='Si')  checked @endif >
                            {{$gener->descripcion}}
                        </label>
                    @endforeach
                </div>
            </div>
        </div>

        <div id="edit">
        <!--@include('proyecto.seniorsproyectos')-->
        </div>
        <input type="hidden" id="proyec">
    </section>

    <section class="content" style="font-size: 12px; height: 100% !important;">
        <div class="row">
            <div class="col-lg-1 hidden-xs"></div>
        </div>
        <div class="row clsPadding table-responsive" style="margin-top:10px;">
            <div class="row clsPadding table-responsive" style="background-color:#FFF;">
                <!--<div class="col-lg-1 hidden-xs"></div>-->
                <div id='listarp' class="col-md-12 col-xs-12 table-responsive">
                    @include('proyecto.listatable')
                </div>
            </div>
        </div>

    </section>
    <div id="modalInProy">
        @include('partials.panelInformacionProyecto')
        @include('partials.panelTransmital')
        @include('partials.panelListaEntregable')
        @include('partials.panelEDT')
        @include('partials.panelHojaResumen')
    </div>
</div>


  
<!-- DataTables -->
<script>
    $('.chosen').chosen({
      allow_single_deselect: true,width:"100%"
    }
  );  

  //Codigo de inputs y cambios registros 

$('#tipoproyecto').chosen(
    {
        allow_single_deselect: true,
        placeholder_text_multiple :"Tipo de proyectos"

    });

$("#estadoproyecto").change(function() {
    var estadoproyecto =  $(this).val();//capturo el valor del combo atravez
    var tipoproyecto = $('#tipoproyecto').val();
    var checkbox = $('input[name=lingreso]');
    var valor =[];
    checkbox.each(function(){
        if ($(this).prop('checked') == true) { //atravez del prop capturo el estado del checkbox
            valor.push($(this).val());
        } else {
          valor.push("ninguna");  
        }   
    });
    var objProy={estadoproyecto: estadoproyecto, valor, tipoproyecto : tipoproyecto};
    //console.log(objProy);
    proyectogenera(objProy,estadoproyecto)
    })
   
  function proyectogenera(objProy,estadoproyecto){
          $.ajax({
            url: 'resultadosTablaProyectos/'+estadoproyecto,
            type:'GET',
            data: objProy,
            beforeSend: function () {
            $('#div_carga').show();  
          },
          success : function (data){
                $('#div_carga').hide();
                $('#listarp').html(data);
                //$("#resultado").html(data);
              },
          }); 
      }

  $('.lingreso').click(function(){
      var estadoproyecto =  $('#estadoproyecto').val();
      var tipoproyecto = $('#tipoproyecto').val();
      var checkbox = $('input[name=lingreso]');
      var valor =[];
      //capturo el valor del combo atravez de un arrays
      checkbox.each(function( ){
        if ($(this).prop('checked') == true) {
            valor.push($(this).val());
        } else {
          valor.push("ninguna");  
        }
        });
        var objProy={estadoproyecto: estadoproyecto, valor , tipoproyecto:tipoproyecto};
        proyectogenera(objProy,estadoproyecto)
       //console.log(objProy);
      })

  $('#tipoproyecto').change(function(){
    var estadoproyecto =  $('#estadoproyecto').val();
    var tipoproyecto = $(this).val();
    var checkbox = $('input[name=lingreso]');
    var valor =[];
      //capturo el valor del combo atravez de un arrays
      checkbox.each(function( ){
        if ($(this).prop('checked') == true) {
            valor.push($(this).val());
        } else {
          valor.push("ninguna");  
        }
      });
    var objProy ={estadoproyecto: estadoproyecto, valor, tipoproyecto: tipoproyecto }
    console.log(objProy);
    proyectogenera(objProy,estadoproyecto)
  })
</script>






