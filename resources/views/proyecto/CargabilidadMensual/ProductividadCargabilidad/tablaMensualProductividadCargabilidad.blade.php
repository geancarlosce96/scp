<div class="col-sm-12 table-responsive " style="height: 650px;width: 98%;">
    <table class="table table-hover table-bordered " id="actividades" style="width: 120%" >
        <thead>
        <tr class="clsCabereraTabla">

            <th>Usuario</th>
            <th>Semana</th>
            <th style="background-color:#CD201A">Proyecto Facturable</th>
            <th style="background-color:#11B15B">General No Facturable</th>
            <th style="background-color:#0069AA">AND Administrativo</th>
            <th style="background-color:#BABDB6FF">Proyectos Internos</th>
            <th style="background-color:#da9694;color: #0c0c0c">Real</th>
            <th style="background-color:#fabf8f;color: #0c0c0c">Planeado</th>
            <th style="background-color:#b1a0c7;color: #0c0c0c">Cargabilidad</th>
            <th style="background-color:#d8e4bc;color: #0c0c0c">No Laborable</th>
            <th style="background-color:#31869b;color: #0c0c0c">No Productivo</th>
            <th style="background-color:#f79646;color: #fff">Productividad</th>
        </tr>
        </thead>
        <tbody>
        @if (isset($array_fac_nofac_adm))
            @foreach ($array_fac_nofac_adm as $gp)
                <tr>
                    <td>{{ $gp['abreviatura'] }}</td>
                    <td>{{ $gp['mes'] }}</td>
                    <td>{{ $gp['sumuser_fac'] }}</td>
                    <td>{{ $gp['sumuser_nofac'] }}</td>
                    <td>{{ $gp['sumuser_admin'] }}</td>
                    <td>{{ $gp['sumuser_interno'] }}</td>
                    <td>{{ $gp['real'] }}</td>
                    <td>{{ $gp['horasplaneado'] }}</td>
                    <td>{{ $gp['cargabilidad_user'] }} %</td>
                    <td>{{ $gp['nolaborables_user'] }}</td>
                    <td>{{ $gp['noproductiva_user'] }}</td>
                    <td>{{ $gp['productividad_user'] }} %</td>
                </tr>
            @endforeach
        @endif

        </tbody>
        <tfoot  class="clsCabereraTabla" style="text-align: left">
        <td id="nombrearea"></td>

        @if(empty($mes))
            <td>Seleccione semana</td>
        @else
            <td>{{ $mes }}</td>
        @endif

        @if(empty($acumulativofac))
            <td>0</td>
        @else
            <td>{{ $acumulativofac }}</td>
        @endif


        @if(empty($acumulativonofac))
            <td>0</td>
        @else
            <td>{{ $acumulativonofac }}</td>
        @endif


        @if(empty($acumulativoadmin))
            <td>0</td>
        @else
            <td>{{ $acumulativoadmin }}</td>
        @endif

        @if(empty($acumulativointernas))
            <td>0</td>
        @else
            <td>{{ $acumulativointernas }}</td>
        @endif


        @if(empty($acumuladoreal))
            <td>0</td>
        @else
            <td>{{ $acumuladoreal }}</td>
        @endif


        @if(empty($acumuladoplaneado))
            <td>0</td>
        @else
            <td>{{ $acumuladoplaneado }}</td>
        @endif

        @if(empty($cargabilidad_total))
            <td>0</td>
        @else
            <td>{{ $cargabilidad_total }}%</td>
        @endif


        @if(empty($acumuladonolaborables))
            <td>0</td>
        @else
            <td>{{ $acumuladonolaborables }}</td>
        @endif


        @if(empty($acumuladonoproductivo))
            <td>0</td>
        @else
            <td>{{ $acumuladonoproductivo }}</td>
        @endif


        @if(empty($productividad_total))
            <td>0</td>
        @else
            <td>{{ $productividad_total }}%</td>
        @endif


        {{--<td>{{ $acumuladoplaneado }}</td>--}}
        {{--
                        <td>{{ $totalempleadoreal }}</td>
                        <td>{{ $sumempleadosplanificados }}</td>
                        <td>{{ round(($fanofacdmin['acumulativofac'] +  $fanofacdmin['acumulativonofac'] - $sumtotaldescansoproyecto) / ( $totalempleadoreal - $totalempleadonolaborable) *100,2) }} %</td>
                        <td>{{ $totalempleadonolaborable }}</td>
                        <td>{{ $totalempleadonoproductivas }}</td>
                        <td>{{ round((( ($fanofacdmin['acumulativofac'] +  $fanofacdmin['acumulativonofac'] +  $fanofacdmin['acumulativoadmin'] ) - ($totalempleadonoproductivas) )/( ($fanofacdmin['acumulativofac'] +  $fanofacdmin['acumulativonofac'] +  $fanofacdmin['acumulativoadmin']) - ($totalempleadonolaborable) )*100),2 ) }} %</td>
        --}}

        </tfoot>

    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{{--<script src="plugins/datatables/jquery.dataTables.min.js"></script>
{{--<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>--}}{{--<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>--}}

<script>
    $('#nombrearea').html($("#careas option:selected").text())


</script>
