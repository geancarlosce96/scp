@extends('layouts.master')
@section ('cargabilidad-mensual-grafico')

    <div class="content-wrapper">
        <!--=====================================
            =            Banner Cargabilidad            =
        ======================================-->
        <div class="col-sm-12">

            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Gráfico Mensual</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
            </section>
            <hr>
        </div>
        <!--====  End of Section comment  ====-->


        <!--=====================================
            =  Menu de areas de los proyectos   =
        ======================================-->
        <div class="col-md-12">
            <div class="box-body col-md-10 col-md-offset-1">
                <div class="form-group col-sm-3 " style="text-align: center" >
                    <label>Áreas</label>
                    <select id="careas" class="form-control chosen" name="careas">
                        <!-- <option value="0" class="indentar">Seleccione un area</option> -->
                        @foreach($careas as $ca)
                            <option value="{{ $ca->carea }}" class="indentar">{{ $ca->codigo_sig }} {{ $ca->descripcion }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group  col-sm-3" style="text-align: center">
                    <label>Mes Periodo - Inicial</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fecha_ini','', array('class'=>'form-control pull-right ','id' => 'fecha_ini','readonly'=>'true')) !!}
                    </div>

                </div>

                <div class="form-group  col-sm-3" style="text-align: center">
                    <label>Mes Periodo - Final</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fecha_final','', array('class'=>'form-control pull-right ','id' => 'fecha_final','readonly'=>'true')) !!}
                    </div>

                </div>



                <div class="col-sm-2" style="    display: flex;align-items: flex-end;height: 52px;">
                    <button id="btngenerar" class="btn btn-block btn-primary" style="height: 34px;padding: 6px 12px">Generar</button>
                </div>
                <div class="col-lg-1" style="display: none;padding-top:10px;" id="imgLoad" >

                    <img src="{{ asset('img/load.gif') }}" width="25px" >
                </div>

            </div>
        </div>
        <!--====  End of  Menu de areas de los proyectos  ====-->
        @include('proyecto.CargabilidadMensual.GraficosCM.07-GraficoMensualProductividadCargabilidad')
        <div class="col-md-6" style="margin-top: 2.5em;">
            @include('proyecto.CargabilidadMensual.GraficosCM.01-GraficoMensual')
        </div>
        <div class="col-md-6" style="margin-top: 2.5em;">
            @include('proyecto.CargabilidadMensual.GraficosCM.02-Grafico3Mensual')
        </div>
         @include('proyecto.CargabilidadMensual.GraficosCM.03-GraficoMensualporUsuario')
        <div class="col-md-4" style="margin-top: 2.5em;">
            @include('proyecto.CargabilidadMensual.GraficosCM.04-GraficoMensualNoFacturable')
        </div>
        <div class="col-md-4" style="margin-top: 2.5em;">
            @include('proyecto.CargabilidadMensual.GraficosCM.05-GraficoMensualAdministratitiva')
        </div>
        <div class="col-md-4" style="margin-top: 2.5em;">
            @include('proyecto.CargabilidadMensual.GraficosCM.06-GraficoMensualProyInternos')
        </div>


    </div>



    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="{{ url('js/cargaMensual/graficopcionescargabilidadMensual.js') }}"></script>
    <script src="{{ url('js/cargaMensual/graficoMensual.js') }}"></script>
    <script src="{{ url('js/cargaMensual/grafica3meses.js') }}"></script>
    <script src="{{ url('js/cargaMensual/graficoMensualporUsuario.js') }}"></script>
    <script src="{{ url('js/cargaMensual/graficoMensualnofacturable.js') }}"></script>
    <script src="{{ url('js/cargaMensual/graficoMensualAdmin.js') }}"></script>
    <script src="{{ url('js/cargaMensual/graficoMensualProyectosInternos.js') }}"></script>
    <script src="{{ url('js/cargaMensual/graficoMensualProductividadCargabilidad.js') }}"></script>
{{--
    <script src="{{ url('js/graficonofacturable.js') }}"></script>
    <script src="{{ url('js/graficaporusuario.js') }}"></script>
    <script src="{{ url('js/graficaadmin.js') }}"></script>
    <script src="{{ url('js/graficointernos.js') }}"></script>--}}


    <script type="text/javascript">

        $(document).ready(function() {


            /*==============================
            =            Chosen            =
            ==============================*/

            $("#careas").chosen(
                { width: '100%' }
            );

            /*=====  End of Chosen  ======*/

            $('#fecha_ini').datepicker({
                format: "yyyy/mm",
                startView: "year",
                minViewMode: "months",
                language: "es",
                autoclose: true,
                endDate: new Date(),


            });
            $('#fecha_final').datepicker({
                format: "yyyy/mm",
                startView: "year",
                minViewMode: "months",
                language: "es",
                autoclose: true,
                endDate: new Date(),

            }).datepicker('setDate', new Date());
            /*========================================================
            =            Listar data dentro de una grilla            =
            ========================================================*/

            $('#btngenerar').click(function(){

                if ($("#careas").val()== 0 || $("#fecha_ini").val()=='' || $("#fecha_final").val()==''){
                    alert('Completar todos los campos requeridos');
                    return;
                }

                var $from=$("#fecha_ini").datepicker('getDate');
                var $to =$("#fecha_final").datepicker('getDate');
                if($from>$to)
                {
                    alert("Recuerda que la fecha final debe ser mayor a la fecha inicial");
                    return;
                }


                var carea = $("#careas").val();
                var fecha_ini = $("#fecha_ini").val();
                var fecha_final = $("#fecha_final").val();

                graficamensualprodCargabilidad(carea,fecha_final);
                graficomensual(carea,fecha_final);
                grafico3meses(carea,fecha_ini,fecha_final);
                graficomensualporusuario(carea,fecha_final);
                graficomensualnofacturable(carea,fecha_final);
                graficomensualadmin(carea,fecha_final);
                graficamensualinternos(carea,fecha_final);

            });

            /*=====  End of Listar data dentro de una grilla  ======*/

        });

    </script>

@stop

