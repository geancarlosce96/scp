@extends('layouts.master')
@section ('indicador-mensual')
    <div class="content-wrapper">
        <!--=====================================
            =            Banner Cargabilidad            =
        ======================================-->
        <div class="col-sm-12">

            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Indicador  Mensual</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
            </section>
            <hr>
        </div>
        <!--====  End of Section comment  ====-->


        <!--=====================================
            =  Menu de areas de los proyectos   =
        ======================================-->
        <div class="col-sm-10 col-sm-offset-1">
            <div class="box-body col-md-12">
                <div class="form-group col-sm-3 " style="text-align: center" >
                    <label>ÁREAS</label>
                    <select id="careas" class="form-control  col-xs-6" style="text-indent: 5px;" name="ccarea">
                        <option value="0" class="indentar">Seleccione un area</option>
                        @foreach($careas as $ca)
                            <option value="{{ $ca->carea }}" class="indentar">{{ $ca->codigo_sig }} {{ $ca->descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-2" style="text-align: center" >
                    <label>TIPO</label>
                    <select name="tipo" id="tipo" class="form-control  col-xs-6">
                        <option value="1" class="indentar">General No Facturable</option>
                        <option value="2" class="indentar">AND Admnistrativo</option>
                    </select>
                </div>

                <div class="form-group  col-sm-2" style="text-align: center">
                    <label>Mes - Periodo</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('datepicker','', array('class'=>'form-control pull-right ','id' => 'datepicker','readonly'=>'true')) !!}
                    </div>

                </div>
                <div class="checkbox form-group  col-sm-2" style="margin-top: 26px">
                    <label><input type="checkbox" value="on" id="showhide"><span style="display: block;padding-top: 4px;"></span>Mostrar / Ocultar Horas Cargadas</label>
                </div>
                <div class="col-sm-2" style="    display: flex;align-items: flex-end;height: 52px;">
                    <button id="btngenerar" class="btn btn-block btn-primary" style="height: 34px;padding: 6px 12px">Generar</button>
                </div>
                <div class="col-sm-1" style="display: none;padding-top:10px;" id="imgLoad" >
                    <img src="{{ asset('img/load.gif') }}" width="25px" >
                </div>

            </div>
        </div>
        <!--====  End of  Menu de areas de los proyectos  ====-->
        <div class="col-md-12" id="lista">
            @include('proyecto.CargabilidadMensual.ActividadesNOFAC_ADM.tablamensual_actividadesNOFAC_ADM')
        </div>

    </div>

    <link rel="stylesheet" href="{{ url('dist/css/chosen/chosencombo.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            /*==============================
            =            Chosen            =
            ==============================*/

            $("#careas").chosen(
                { width: '100%' }
            );
            $("#tipo").chosen(
                { width: '100%' }
            );
            $('#datepicker').datepicker({
                format: "yyyy/mm",
                startView: "year",
                minViewMode: "months",
                language: "es",
                autoclose: true,
                endDate: new Date(),
            });


            /*=====  End of Chosen  ======*/

            /*========================================================
            =            Listar data dentro de una grilla            =
            ========================================================*/

            $('#btngenerar').click(function(){
                verHHFacturable();
            });


            $('#showhide').click(function(){

                if ($('#showhide:checked').val()=='on') {
                    $('.hideshow').hide();
                }
                else{
                    $('.hideshow').show();
                }

            });
            /*=====  End of Listar data dentro de una grilla  ======*/

        });

        function verHHFacturable() {

            if ($("#careas").val()== 0 || $("#datepicker").val()=='' || $("#tipo").val()==''){
                alert('Completar todos los campos requeridos');
                return;
            }

            $("#imgLoad").show();
            // var obj = {careas: $('#careas').val(),tipo: $("#tipo").val()};
            var obj = {tipo: $("#tipo").val(),careas: $('#careas').val(),datepicker: $('#datepicker').val() }
            $.ajax({
                type:'GET',
                url: 'listaActividadesmensualNOFAC',
                data: obj,
                success: function (data) {
                    $("#imgLoad").hide();
                    console.log(data);
                    $('#lista').html(data);
                },
                error: function(data){
                }
            });

        }

    </script>
@stop