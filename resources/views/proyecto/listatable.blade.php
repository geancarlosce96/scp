<div class="row">
  <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
    <div class="table-responsive">
     <table class="table table-hover table-bordered table-responsive" id="cargalistaproyecto" style="font-size:12px;">
              <thead>
              <tr class="clsCabereraTabla">
                <th class="detail">ID</th>  
                <th style="width: 6%;">Código</th>
                <th style="width: 20%;">Nombre</th>
                <th class="detail" >Cliente</th>         
                <th >Unidad Minera</th>
                <th class="detail" >Portafolio</th>
                <th class="detail" >Tipo</th>
                <th >Gerente</th>
                <th class="detail" >CP</th>
                <th class="detail" >CD</th>

                @foreach($buscocargos2  as $senior)
                  <th>{{ $senior->cabecera}}</th>
                @endforeach
                @foreach($lidersis as $dir)
                  <th class="detail">Lider {{ $dir->descripcion }}</th>
                @endforeach
                <th >Fecha Inicio</th>
                <th >Fecha Fin</th>
                <th class="detail" >Fecha Creación</th>
                <th class="detail" >Estado</th>
                <!--th>ingresos</th-->
                <!--th>Tipo proyecto</th-->

           
              </tr>
              </thead>
              <tfoot>
              <tr class="clsCabereraTabla">
                <th class="detail">ID</th>
                <th style="width: 6%;">Código</th>
                <th style="width: 20%;">Nombre</th>
                <th class="detail" >Cliente</th>
                <th >Unidad Minera</th>
                <th class="detail" >Portafolio</th>
                <th class="detail" >Tipo</th>
                <th >Gerente</th>
                <th class="detail" >CP</th>
                <th class="detail" >CD</th>
                @foreach($buscocargos2  as $senior)
                  <th >{{ $senior->descripcion}}</th>
                @endforeach
                @foreach($lidersis as $dir)
                <th class="detail">Lider {{ $dir->descripcion }}</th>
                @endforeach
                <th >Fecha Inicio</th>
                <th >Fecha Fin</th>
                <th class="detail" >Fecha Creación</th>
                <th class="detail" >Estado</th>


              <!--th>ingresos</th-->
                <!--th>Tipo proyecto</th-->

              </tr>
              </tfoot>

              <tbody>
                @foreach($Proyectos as $lista)

                <tr data-idproyecto="{{ $lista->cproyecto }}">
                  <td>{{ $lista->cproyecto }}</td>
                  <td style="width: 6%;">{{ $lista->codigo }}</td>
                  <td style="width: 20%;">{{ $lista->nombrepry }}</td>
                  <td style="width: 14%;">{{ $lista->cliente }}</td>
                  <td >{{ $lista->uminera }}</td>
                  <td >{{ $lista->portafolio }}</td>
                  <td >{{ $lista->tipo_proyecto }}</td>
                  <td >{{ $lista->gerente }}</td>
                  <td >{{ $lista->nomcp }}</td>
                  <td >{{ $lista->nomcd }}</td>
                  
                  <!--inicion de seniors total 5-->
                @foreach($buscocargos2  as $senior)
                    <?php
                    $codigo_descicplina='sen_'.$senior->seniors;
                    $codigo_descicplina=$lista->$codigo_descicplina;
                    //dd($codigo_descicplina);
                    ?>
                    <td><span type="button" class="btn btn-ligh btneditar btn-block" data-toggle="modal" style="font-size:12px" >{{ $codigo_descicplina }}</span></td>
                @endforeach
                @foreach($lidersis as $dir)
                  <?php

                      $codigo_descicplina_lideres='ld_'.$dir->cdisciplina;
                      $codigo_descicplina_lideres=$lista->$codigo_descicplina_lideres;
                      //dd($codigo_descicplina);
                  ?>
                  <td>{{ $codigo_descicplina_lideres }}</td>
               @endforeach
                <!-- Fin de td senior-->
                  <td >{{ $lista->finicio }}</td>
                  <td >{{ $lista->fcierre }}</td>
                  <td >{{ $lista->fproyecto }}</td>
                  <td >{{ $lista->descripcion }}</td>

                  <!--td >{{-- $lista->tipoproy --}}</td-->
                  <!--td>{{-- $lista->ctipoproyecto --}}</td-->

                </tr>
                @endforeach
              </tbody>
      </table>
    </div>
  </div>
</div>
<script>
    
   var table = $('#cargalistaproyecto').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     true,
        "order": [[1, 'asc']],
        lengthChange: false,
        buttons: [ 'excel', 'colvis' ],
        scrollY:        "500px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        // fixedColumns:   {
        //     leftColumns: 8
        // },
        "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "buttons": {
                "colvis": "Columnas visibles"
            },
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    }); 

    table.buttons().container()
        .appendTo( '#cargalistaproyecto_wrapper .col-sm-6:eq(0)' );

    //table.columns( [ 0, 3, 4, 5, 6] ).visible( false, false );
    table.columns( '.detail' ).visible( false );
    table.columns.adjust().draw( false ); // adjust column sizing and redraw

   // Fin Hoja Resumen

  //boton de editar senior por modals lardin

    $(".btneditar").click(function () {
      //alert('modal prueva');
      var  id=$(this).parent().parent().data("idproyecto")
      
      var  url = BASE_URL + '/obtener_senior/'+id;
      $.get(url)
              .done(function ( data ) {
                $('#edit').empty();
                $('#edit').append(data)
              //  $('#myModalSenior').modal('show')
              //console.log(data)
                $('#proyec').val(id);
                $(".seniors-div").draggable();
                $(".seniors-div").resizable();
              })       
    })
</script>