@extends('layouts.master')
@section('resumen-cchh')
    <style>

        /* The contennedor */
        .contennedor {
            display: block;
            position: relative;
            padding-left: 35px;
            margin-bottom: 12px;
            margin-top: 4px;
            cursor: pointer;
            font-size:10px;
            font-weight:400;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        /* Hide the browser's default checkbox */
        .contennedor input {
            position: absolute;
            opacity: 0;
            cursor: pointer;
            height: 0;
            width: 0;
        }

        /* Create a custom checkbox */
        .checkmark {
            position: absolute;
            top: 0;
            left: 0;
            height: 25px;
            width: 25px;
            background-color: #eee;
        }

        /* On mouse-over, add a grey background color */
        .contennedor:hover input ~ .checkmark {
            background-color: #ccc;
        }

        /* When the checkbox is checked, add a blue background */
        .contennedor input:checked ~ .checkmark {
            background-color: #2196F3;
        }

        /* Create the checkmark/indicator (hidden when not checked) */
        .checkmark:after {
            content: "";
            position: absolute;
            display: none;
        }

        /* Show the checkmark when checked */
        .contennedor input:checked ~ .checkmark:after {
            display: block;
        }

        /* Style the checkmark/indicator */
        .contennedor .checkmark:after {
            left: 9px;
            top: 5px;
            width: 5px;
            height: 10px;
            border: solid white;
            border-width: 0 3px 3px 0;
            -webkit-transform: rotate(45deg);
            -ms-transform: rotate(45deg);
            transform: rotate(45deg);
        }

        .highcharts-drilldown-axis-label {
        text-decoration:none!important;
        fill: #666666!important;
        font-weight: 400!important;
    }
        text.highcharts-data-label {
            fill: #666!important;
            text-decoration: none!important;
        }

    </style>
    <div class="content-wrapper">
        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Resumen Control de Horas</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}

            </section>
            <hr>
            <div class="col-sm-12 ">
                <div class="col-md-3">

                    <select id="cbo_areas" class="form-control select-box" data-placeholder="Elegir un area...">
                        @if (in_array("gerente", $explodelideroGerente) || $unique_areas[0] == 56)
                            <option value="Nuevo" >Todos</option>
                            @foreach($careas as $ta)
                                <option value="{{ $ta->cdisciplina }}" >{{ $ta->codigo_sig .' '.$ta->descripcion }}</option>
                            @endforeach
                       {{-- @elseif($careas[0]->cdisciplina == 10)
                            <option value="Nuevo" >Todos</option>
                            @foreach($careas as $ta)
                                <option value="{{ $ta->cdisciplina }}" >{{ $ta->codigo_sig .' '.$ta->descripcion }}</option>
                            @endforeach--}}
                        @else
                            @foreach($careas as $ta)
                                <option value="{{ $ta->cdisciplina }}" >{{  $ta->codigo_sig .' '.$ta->descripcion }}</option>
                            @endforeach
                        @endif

                    </select>
                </div>
                <div class="col-md-3">
                    <div class="col-sm-3 clsPadding">Fecha inicio</div>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fdesde','', array('class'=>'form-control pull-right','id' => 'fdesde','readonly'=>'true')) !!}
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="col-sm-3 clsPadding">Fecha Final</div>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fhasta','', array('class'=>'form-control pull-right','id' => 'fhasta','readonly'=>'true')) !!}
                    </div>
                </div>
                @if ($lideroGerente == 'gerente')
                    <div class="col-md-1">
                        <label class="contennedor">
                            <div style="padding-top: 6px;">Gerente</div>
                            <input type="checkbox" value="on" id="showhide">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-md-1">
                        <button id="btngenerar" class="btn btn-block btn-primary">Generar</button>
                    </div>
                    <div class="col-md-1" style="display: none;" id="imgLoad" >
                        <img src="{{ asset('img/load.gif') }}" width="25px" >
                    </div>
                    <div class="" style="margin-top: 5em"></div>
                @else
                    <div class="col-md-2">
                        <button id="btngenerar" class="btn btn-block btn-primary">Generar</button>
                    </div>
                    <div class="col-md-1" style="display: none;" id="imgLoad" >
                        <img src="{{ asset('img/load.gif') }}" width="25px" >
                    </div>
                    <div class="" style="margin-top: 5em"></div>
                </div>
            @endif
        </div>
        <div id="lista">
            @include('proyecto.ControldeHoras.tablaProyectosCCHH',array('arrayProyectos' => (isset($arrayProyectos)==1?$arrayProyectos:array())))
        </div>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="js/cc-hh/resumen_CCHH.js"></script>
    <script src="js/cc-hh/resumenMonto_CCHH.js"></script>
    <script type="text/javascript">

        $(document).ready(function(){
            /*==============================
            =            Chosen            =
            ==============================*/
            $('#fdesde').datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                autoclose: true,
                calendarWeeks: true,
                todayHighlight: true,
            });

            $('#fhasta').datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                calendarWeeks: true,
                todayHighlight: true,
                autoclose: true,
            });
            $('.select-box').chosen(
                {
                    allow_single_deselect: true,width:"100%"
                });
            $('.cbo_areas').chosen(
                {
                    allow_single_deselect: true,width:"100%"
                });

            $('#btngenerar').click(function(){
                verResumenCCHH();
            });

            $('#showhide').click(function(){

                if ($('#showhide:checked').val()=='on') {
                    $('.hideshow').hide();
                }
                else{
                    $('.hideshow').show();
                }
            });







        });

        function verResumenCCHH() {

            if ($("#cbo_areas").val()== 0 || $("#fdesde").val()=='' || $("#fhasta").val()==''){
                alert('Completar todos los campos requeridos');
                return;
            }

            var $from=$("#fdesde").datepicker('getDate');
            var $to =$("#fhasta").datepicker('getDate');
            if($from>$to)
            {
                alert("Recuerda que la fecha final debe ser mayor a la fecha inicial");
                return;
            }
            //$("#imgLoad").show();
            $("#div_carga").show();

            var obj = {disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()}

            $.ajax({
                type:'GET',
                url: 'getResumenCCHH',
                data: obj,
                success: function (data) {

                    console.log(data);
                    //$("#imgLoad").hide();
                    $("#div_carga").hide();
                    // chart_listadp_areas(data);
                    $('#lista').html(data);
                    $('#actividades').DataTable(
                        {
                            //destroy: true,
                            /*"scrollY": "250px",
                            "scrollCollapse": true,*/
                            scrollY:        '60vh',
                            scrollCollapse: true,
                            "paging":   false,
                            "ordering": false,
                            "info":     true,
                            language: {
                                url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                            },
                            "columnDefs": [
                                { "width": "5%", "targets": [0,1] },
                                { "width": "5%", "targets": [2,3] },
                                { "width": "30%", "targets": $("#lider_or_gerente").val() === 'gerente' ? 4 : 3 },
                                { "width": "5%", "targets": 5 },
                                { "width": "8%", "targets": 6 },
                            ],
                            fixedHeader: {
                                header: true,
                                footer: true
                            },
                            fixedColumns:   {
                                leftColumns: $("#lider_or_gerente").val() == 'gerente' ? 10 : 8

                            },
                            scrollX:        true,

                        }
                    );


                },
                error: function(data){
                }
            });
        }
    </script>
@stop