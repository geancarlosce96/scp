@extends('layouts.master')
@section('control-horas')
<style>
/* The switch - the box around the slider */
.switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

/* Hide default HTML checkbox */
.switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

/* The slider */
.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}

</style>

    <div class="content-wrapper">


        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Control de Horas</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
            </section>
            <hr>

            <div class="col-md-3">

                <input type="hidden" value="{{$esgp}}" id="esgp">
                <input type="hidden" value="{{$cdisciplina}}" id="cdisciplina">

                <select id="proyectos" class="form-control select-box" data-placeholder="Elegir un proyecto...">
                    <option value="" >Seleccione un proyecto</option>
                    @foreach($tproyectos as $proy)
                        <option value="{{ $proy->cproyecto }}" >{{ $proy->codigo }} {{ $proy->nombre }}</option>
                    @endforeach
                </select>
                
            </div>            
            <div  class="col-md-2" style="display: flex;justify-content: space-between;align-items: center;width: 14%;">
                <label style="width:30px;font-size: 14px;">SOC/SubProyectos</label>        
                <label class="switch"  style="width:64px">
                    <input type="checkbox" id="soc" value="1">
                   
                    <span class="slider round"></span>
                  </label>                  
            </div>
            <div class="col-md-2">
                <select id="cbo_areas" multiple class="form-control select-box" name="cbo_areas" data-placeholder="Seleccione area">
                    <option value="" >Elegir un area...</option>
                   
                </select>
            </div>

            <div class="col-md-2">
                <div class="col-sm-3 clsPadding">Fecha inicio</div>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fdesde','', array('class'=>'form-control pull-right','id' => 'fdesde','readonly'=>'true')) !!}
                </div>
            </div>
            <div class="col-md-2">
                <div class="col-sm-3 clsPadding">Fecha Final</div>
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    {!! Form::text('fhasta','', array('class'=>'form-control pull-right','id' => 'fhasta','readonly'=>'true')) !!}
                </div>
            </div>

            <div class="col-md-1">
                <button id="btngenerar" class="btn btn-block btn-primary">Generar</button>
            </div>
            <div class="col-lg-1" style="display: none;" id="imgLoad" >
                <img src="{{ asset('img/load.gif') }}" width="25px" >
            </div>
            <div class="col-md-12" style="margin-top:14px;margin-bottom:14px">
                <select id="cbo_selectSOC" multiple class="form-control select-box" name="cbo_selectSOC" data-placeholder="Seleccione SOC">
                    <option value="" >Elegir una SOC ...</option>
                </select>
            </div>

            <div class="" style="margin-top: 9em"></div>
        </div>
        <div id="lista">
            @include('proyecto.ControldeHoras.actividadesProyectos',array('arrayProyectos' => (isset($arrayProyectos)==1?$arrayProyectos:array())))
        </div>



    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/drilldown.js"></script>
    <script src="js/cc-hh/lista_areas_porActividad.js"></script>
    <script src="js/cc-hh/lista_areasrateCCHH.js"></script>
    <script src="js/cc-hh/resumen_CCHH.js"></script>
    <script src="js/cc-hh/resumenMonto_CCHH.js"></script>
    <script src="js/cc-hh/resumenSOC_CCHH.js"></script>
    <script src="js/cc-hh/resumenMontoSOC_CCHH.js"></script>

    <script type="text/javascript">


        $(document).ready(function(){


         /*==============================
         =            Chosen            =
         ==============================*/
            $('.select-box').chosen(
                {
                    allow_single_deselect: true,width:"100%"
                });

                $('#cbo_areas').change(function () {

                   habilitarcarea()
                }
               );

               


            /*=====  End of Chosen  ======*/


            /*====================================================================
            =            Combo por area enviando el proyecto indicado            =
            ====================================================================*/

            $('#proyectos').change(function(){

                resetear();
                /*getareas*/
                var obj = {cproyecto: $('#proyectos').val()};
                $.ajax({
                    type:'GET',
                    url: 'obtenerAreasCCHH',
                    data : obj,
                    
                    success: function (data) {
                     //   console.log($("#esgp").val(),"es gp");
                       // console.log("aaa",data);
                        
                        tamano = data.length;
                         // console.log("Areas / disciplinas",data);

                        $('#cbo_areas').find('option').remove();
                        //  $('#tablaactiv').html(data)
                        //if($("#esgp").val() == 'gerente' || $("#cdisciplina").val()==10){
                          //  tamano = data.length-1;
                           
                            $('#cbo_areas').append($('<option class="todo" selected></option>').attr('value','Nuevo').text('Todos'));
                            $('#cbo_areas').trigger("chosen:updated");

                        //}

                        for (var i = 0; i < tamano; i++) {
                            $('#cbo_areas').append($('<option class="careas"></option>').attr('value',data[i].disciplina).text(data[i].codigo_sig+' '+data[i].area));
                            $('#cbo_areas').trigger("chosen:updated");

                        }
                        habilitarcarea();
                    },
                    error: function(data){


                    }

                });

                /*getfehca*/
                $.ajax({
                    type:'GET',
                    url: 'getfechaCCHH',
                    data : obj,

                    success: function (data) {
                        var finicio = data[0]['finicio'];
                        var fcierre = data[0]['fcierre'];

                        //var conver_finicio1 =   moment(finicio)
                        var conver_finicio =   moment(finicio).format("DD/MM/YYYY");

                        var conver_fcierre=   moment(fcierre).format("DD/MM/YYYY");
                       // console.log(conver_finicio,conver_fcierre);


                        $('#fdesde').datepicker({
                            format: "dd/mm/yyyy",
                            startDate: conver_finicio,
                            language: "es",
                            autoclose: true,
                            calendarWeeks: true,
                            todayHighlight: true,

                        }).datepicker("setDate", conver_finicio);



                        $('#fhasta').datepicker({
                            format: "dd/mm/yyyy",
                            language: "es",
                            calendarWeeks: true,
                            todayHighlight: true,
                            autoclose: true,
                            endDate : conver_fcierre,
                        }).datepicker("setDate", conver_fcierre);


                    },
                    error: function(data){

                    }

                });

                // Get SOC

                $.ajax({
                    type:'GET',
                    url: 'obtenerSOCProyectos',
                    data : obj,
                    
                    success: function (data) {
                        tamano = data.length;
                        $('#cbo_selectSOC').find('option').remove();
                        for (var i = 0; i < tamano; i++) {
                            $('#cbo_selectSOC').append($('<option class="socProyec"></option>').attr('value',data[i].cproyecto).text(data[i].codigo+' '+data[i].nombre));
                            $('#cbo_selectSOC').trigger("chosen:updated");
                        }
                    },
                    error: function(data){



                    }

                });



            });

                $('input[type=checkbox]').on('change', function() {
                if ($(this).is(':checked') ) {
                    // cuando esta en checked 
                    $('.socProyec').attr( "disabled", true );
                    $('#cbo_selectSOC').val('');
                    $('.select-box').trigger("chosen:updated");
                  
                  
                  
                } else {
                    $('.socProyec').attr( "disabled", false );
                    $('.select-box').trigger("chosen:updated");
                }
                });



            /*========================================================
            =            Listar data dentro de una grilla            =
            ========================================================*/

            $('#btngenerar').click(function(){
                verActividadesCCHH();
                //mostrarAreaporCadaActividad();
            });


        });
      function  habilitarcarea(){

            let       estadocbo = false,
                        estado_todo = false
                   // console.log( $('#cbo_areas :selected').val());
                  
                   if( $('#cbo_areas :selected').val() == 'Nuevo')
                   {
                       estadocbo = true
                   }
                   if ( $('#cbo_areas :selected').val() == undefined ||  $('#cbo_areas :selected').val() != 'Nuevo') {
                          estado_todo = true
                          //console.log("entra ?")
                   }
                   if( $('#cbo_areas :selected').val() == undefined)
                   {
                        estadocbo = false,
                        estado_todo = false
                   }

                   

                   $('.careas').prop( "disabled", estadocbo );
                    $('.todo').prop( "disabled", estado_todo );
                    $('.select-box').trigger("chosen:updated");
        }


        

        function resetear(){

            $("#fdesde,#fhasta").datepicker({
                format: "dd/mm/yyyy",
            }).datepicker("setDate", '');
            $('#cbo_areas').find('option').remove();
            $('#cbo_areas').val('')
            $('#cbo_areas').trigger("chosen:updated");


        }
        function verActividadesCCHH() {

            if (  $('#proyectos').val().trim() === '' ){
                swal("Seleccione un proyecto");
               return;
           }

          if (  $('#cbo_areas').val() == null ){
                swal("Seleccione un área");
               return;
           }

            if ($("#fhasta").val()=='' || $("#fdesde").val()==''){
               // alert('Especifique rango de fechas');
                swal("Especifique rango de fechas");
                return;
            }
            if(  $('#soc').prop('checked') == false &&  $('#cbo_selectSOC').val() == null )
            {
                swal(
                    //"Si desactiva el boton de SOC tendra que seleccionar que proyectos C desea visualizar"
                    'Oops...',
                    'Si desactiva el boton de SOC tendra que seleccionar que proyectos desea visualizar del contractual!',
                    'error'

                    );
                   
                return;
            }

    

            var obj = { cproyecto: $('#proyectos').val(),cbo_selectSOC: $('#cbo_selectSOC').val(),soc: $('#soc').prop('checked'), disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()}



            $.ajax({
                type:'GET',
                url: 'getActividadesCCHH',
                data: obj,
                beforeSend: function(data){
                    $('#div_carga').show();
                },
                success: function (data) {
                    console.log(data);
                    //$("#imgLoad").hide();
                   // chart_listadp_areas(data);
                   $('#div_carga').hide();
                    $('#lista').html(data);
                    $('#actividades').DataTable(
                        {
                            //destroy: true,
                            /*"scrollY": "250px",
                            "scrollCollapse": true,*/
                            searching: false,
                            scrollY:        '60vh',
                            scrollCollapse: true,
                            "paging":   false,
                            "ordering": false,
                            "info":     true,
                            language: {
                                url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                            },
                            "columnDefs": [
                                { "width": "30%", "targets": 0 },
                                { "width": "3%", "targets": 1 },
                                { "width": "3%", "targets": 2 },
                                { "width": "5%", "targets": 3 },
                                { "width": "8%", "targets": 4 },

                            ],
                            fixedHeader: {
                                header: true,
                                footer: true
                            }
                        }
                    );


                },
                error: function(data){



                }
            });



        }









    </script>


@stop