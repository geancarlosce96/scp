<div class="col-sm-12 table-responsive ">
    <table class="table table-hover table-bordered " id="actividades" style="font-size:12px;" >
        <thead>
        <tr  class="clsCabereraTabla">
            <th rowspan="2" style="    text-align: center;vertical-align: middle">Actividades</th>
            <th colspan="2"  style="text-align: center;vertical-align: middle; border-bottom: 1px solid #fff;" >Tarifa $/hr</th>
            <th colspan="5" style="text-align: center;vertical-align: middle; border-bottom: 1px solid #fff;">Control de Horas</th>
            <th colspan="5" style="text-align: center;vertical-align: middle; border-bottom: 1px solid #fff;">Control Horas / Rate </th>
            {{--<th colspan="5">Gestion de Rate / Horas </th>--}}
        </tr>
        <tr class="clsCabereraTabla">
            <!--====  Cabecera actividades  ====-->
            <!--====  Control de Horas  HH ====-->
            <th>Doc</th>
            <th>Planos</th>

            <th>
                <div style="text-align: center" >Horas</div>
                <div style="text-align: center" >presupuestadas</div>

            </th>
            <!--==== Cabecera Horas por rate  ====-->
            <th>
                <div style="text-align: center" >Horas</div>
                <div style="text-align: center" >Cargadas</div>

            </th>
            <th>Saldo</th>
            <th>Avance</th>
            <th>Estado</th>

            <th>
                <div style="text-align: center" >Monto</div>
                <div style="text-align: center" >presupuestadas</div>

            </th>
            <!--==== Cabecera Horas por rate  ====-->
            <th>
                <div style="text-align: center" >Monto</div>
                <div style="text-align: center" >Cargadas</div>

            </th>
            <th>Saldo</th>
            <th>Avance</th>
            <th>Estado</th>

        </tr>

        </thead>
        <tbody>
        @if(isset($arrayProyectos))

            @foreach($arrayProyectos as $lp)
                <tr>
                    <td><b>PROYECTO {{$lp['codigo']}} - {{$lp['nombreProyecto']}} - {{$lp['nombreMinera']}}</b></td>
                   <td></td>
                   <td></td>
                   <td>{{$lp['total_horas_presupuestadas_por_proyecto']}}</td>
                   <td>{{$lp['total_horas_consumidas_por_proyecto']}}</td>
                   <td>{{$lp['total_horas_saldo_por_proyecto']}}</td>
                    @if (($lp['avance_total_por_proyecto']) >= 0  )
                    <td style="font-weight: bold"> {{$lp['avance_total_por_proyecto']}} %</td>
                    @else
                    <td style="color: tomato;font-weight: bold">{{$lp['avance_total_por_proyecto']}} %</td>
                    @endif
                   <td class="resumendehoras_por_proyecto" data-cproyecto=" {{$lp['cproyecto']}}" data-codigoproyecto=" {{$lp['codigo']}}" data-nombreproyecto=" {{$lp['nombreProyecto']}}">
                    @if ($lp['avance_total_por_proyecto'] <= 80)
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                    <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                    </div>
                    @elseif ($lp['avance_total_por_proyecto'] >= 81 && $lp['avance_total_por_proyecto'] <= 100)
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                    <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                    </div>
                    @else
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                    <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                    </div>
                    @endif
                   </td>
                   <td>{{$lp['total_monto_presupuestados_por_proyecto']}}</td>
                   <td>{{$lp['total_monto_consumidos_por_proyecto']}}</td>
                   <td>{{$lp['total_monto_saldo_por_proyecto']}}</td>
                   @if (($lp['avance_total_monto_por_proyecto']) >= 0  )
                   <td style="font-weight: bold"> {{$lp['avance_total_monto_por_proyecto']}} %</td>
                   @else
                   <td style="color: tomato;font-weight: bold">{{$lp['avance_total_monto_por_proyecto']}} %</td>
                   @endif
                   <td class="resumendemonto_por_proyecto" data-cproyecto=" {{$lp['cproyecto']}}" data-codigoproyecto=" {{$lp['codigo']}}" data-nombreproyecto="{{$lp['nombreProyecto']}}">
                    @if ($lp['avance_total_monto_por_proyecto'] <= 80)
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                    <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                    </div>
                    @elseif ($lp['avance_total_monto_por_proyecto'] >= 81 && $lp['avance_total_monto_por_proyecto'] <= 100)
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                    <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                    </div>
                    @else
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                    <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                    </div>
                    @endif
                   </td>
                </tr>
                @if(isset($lp['listaactividades']))
                    @foreach($lp['listaactividades'] as $act)
                        @if ($act['familia'] == 'padre')
                            <tr>
                                
                               

                            @if ($act['nivel0'] != null)
                            <td style="background: #ddd;display: flex;justify-content: flex-start" class="name_actividad">
                                <i  style="display:block;color: black;font-size: 1.5em;cursor:pointer;display: block;margin-left: 5px;" class="glyphicon glyphicon-chevron-down  pull-right  padrePrincipal" data-cactividadproyectoclick="{{$act['cproyectoactividades']}}" ></i>
                                <span  style="display: block;margin-left: 10px">{{$act['codigoactividad']}} - {{$act['descripcionactividad']}}</span>
                            @else
                            <td  data-cproyecto="{{$act['cproyecto']}}" data-idactividad="{{$act['cproyectoactividades']}}" data-descripcion="{{$act['descripcionactividad']}}" data-iddescripcion="{{$act['codigoactividad']}}"   style="display: flex;justify-content: flex-start" class="name_actividad">
                            <span style="display: block;margin-left: 40px;">{{$act['codigoactividad']}} - {{$act['descripcionactividad']}}</span>
                            @endif
                            </td>

                                <td><b>{{$act['acumuladohoras_doc']}}</b></td>
                                <td><b>{{$act['acumuladohoras_plano']}}</b></td>
                                <td><b>{{$act['suma_total_horas_presupuestadas']}}</b></td>
                                <td><b>{{$act['suma_total_horas_cargadas']}}</b></td>
                                @if (($act['suma_total_horas_saldo']) >= 0  )
                                    <td style="font-weight: bold"> {{$act['suma_total_horas_saldo']}}  </td>
                                @else
                                    <td style="color: tomato;font-weight: bold">{{$act['suma_total_horas_saldo']}} </td>
                                @endif
                                <td>
                                    <div class="progress" >
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $act['avance']}}%;background-color: #5cb85c;">
                                            <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $act['avance']}}%</b></span>
                                        </div>
                                    </div>
                                </td>
                                @if ($act['avance'] <= 80)
                                    <td>
                                        <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;;align-items: center;border-radius: 50%" aria-label="Left Align">
                                            <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                        </div>
                                    </td>
                                @elseif ($act['avance'] >= 81 && $act['avance'] <= 100)
                                    <td>
                                        <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;;align-items: center;border-radius: 50%" aria-label="Left Align">
                                            <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                        </div>
                                    </td>

                                @else
                                    <td>
                                        <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;;align-items: center;border-radius: 50%" aria-label="Left Align">
                                            <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                        </div>
                                    </td>

                                @endif

                                <td><b>{{$act['suma_total_horas_rate_presupuestadas']}}</b></td>
                                <td><b>{{$act['suma_total_horas_rate_cargadas']}}</b></td>

                                @if (($act['suma_total_horas_cargadas_saldo']) >= 0  )
                                    <td style="font-weight: bold"> {{$act['suma_total_horas_cargadas_saldo']}}  </td>
                                @else
                                    <td style="color: tomato;font-weight: bold">{{$act['suma_total_horas_cargadas_saldo']}} </td>
                                @endif

                                <td>
                                    <div class="progress" >
                                        <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $act['avance_total_padre']}}%;background-color: #5cb85c;">
                                            <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $act['avance_total_padre']}}%</b></span>
                                        </div>
                                    </div>
                                </td>
                                <td>
                                    @if ($act['avance_total_padre'] <= 80)

                                        <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                            <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                        </div>
                                    @elseif ($act['avance_total_padre'] >= 81 && $act['avance_total_padre'] <= 100)
                                        <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                            <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                        </div>
                                    @else
                                        <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                            <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                        </div>

                                    @endif
                                </td>

                            </tr>
                            @foreach ($act['nivel0'] as $nivel0)
                                @if ($nivel0['familia'] == 'hijo')
                                    <tr  data-cproyecto="{{$nivel0['cproyecto']}}" data-idactividad="{{$nivel0['cproyectoactividades']}}" data-descripcion="{{$nivel0['descripcionactividad']}}" data-iddescripcion="{{$nivel0['codigoactividad']}}"  data-cproyectoactividades_parent="{{$nivel0['cproyectoactividades_parent']}}" class="ocultarActividad">

                                        @if ($nivel0['nivel2'] != null)
                                            <td style="background: #ddd;display: flex;justify-content: flex-start" class="name_actividad">
                                                <i  style="display:block;color: black;font-size: 1.5em;cursor:pointer;display: block;padding-left: 12px;" class="glyphicon glyphicon-chevron-down  pull-right btn_ocultar hijoPrincipal" data-cactividadproyectohijoclick="{{$nivel0['cproyectoactividades']}}"></i>
                                                <span style="display: block;margin-left: 19px;">{{$nivel0['codigoactividad']}} - {{$nivel0['descripcionactividad']}}</span>
                                        @else
                                            <td style="display: flex;justify-content: flex-start" class="name_actividad">
                                                <span style="display: block;margin-left: 40px;">{{$nivel0['codigoactividad']}} - {{$nivel0['descripcionactividad']}}</span>
                                         @endif
                                            </td>

                                            <td>{{$nivel0['numdoc']}}</td>
                                            <td>{{$nivel0['numplanos']}}</td>
                                            <td>{{$nivel0['horaspresuestadashijo']}}</td>
                                            <td>{{$nivel0['horascargadashijo']}}</td>
                                            @if (($nivel0['horaspresuestadashijo'] - $nivel0['horascargadashijo']) >= 0  )
                                                <td > {{$nivel0['horaspresuestadashijo'] - $nivel0['horascargadashijo']}}  </td>
                                            @else
                                                <td style="color: tomato"> {{$nivel0['horaspresuestadashijo'] - $nivel0['horascargadashijo']}}  </td>
                                            @endif
                                            <td>
                                                <div class="progress" >
                                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel0['avance']}}%;background-color: #5cb85c;">
                                                        <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel0['avance']}}%</b></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="verAreas">
                                                @if ($nivel0['avance'] <= 80)

                                                    <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                                        <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                    </div>
                                                @elseif ($nivel0['avance'] >= 81 && $nivel0['avance'] <= 100)
                                                    <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                        <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                    </div>
                                                @else
                                                    <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                        <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                    </div>

                                                @endif
                                            </td>

                                            <td>{{$nivel0['montopresupuestadosshijo']}}</td>
                                            <td>{{$nivel0['montoCargadoshijo']}}</td>
                                            @if (($nivel0['montoCargadoshijo_saldo']) >= 0  )
                                                <td> {{$nivel0['montoCargadoshijo_saldo']}}  </td>
                                            @else
                                                <td style="color: tomato;">{{$nivel0['montoCargadoshijo_saldo']}} </td>
                                            @endif

                                            <td>
                                                <div class="progress" >
                                                    <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel0['avancerate_hijo']}}%;background-color: #5cb85c;">
                                                        <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel0['avancerate_hijo']}}%</b></span>
                                                    </div>
                                                </div>
                                            </td>
                                            <td class="verAreasrate">
                                                @if ($nivel0['avancerate_hijo'] <= 80)

                                                    <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                                        <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                    </div>
                                                @elseif ($nivel0['avancerate_hijo'] >= 81 && $nivel0['avancerate_hijo'] <= 100)
                                                    <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                        <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                    </div>
                                                @else
                                                    <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                        <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                    </div>

                                                @endif
                                            </td>
                                    </tr>
                                    @foreach ($nivel0['nivel2'] as $nivel2)
                                        @if ($nivel2['familia'] == 'nieto')
                                            <tr data-cproyecto="{{$nivel2['cproyecto']}}"  data-idactividad="{{$nivel2['cproyectoactividades']}}" data-descripcion="{{$nivel2['descripcionactividad']}}" data-iddescripcion="{{$nivel2['codigoactividad']}}" data-cproyectoactividades_parent="{{$nivel2['cproyectoactividades_parent']}}" data-cproyectoactividadeshijo_parent="{{$nivel2['cproyectoactividadeshijo_parent']}}" class="ocultarActividad">

                                                @if ($nivel2['nivel3'] != null)
                                                    <td style="background: #ddd;display: flex;justify-content: flex-start" class="name_actividad">
                                                        <i  style="display:block;color: black;font-size: 1.5em;cursor:pointer;display: block;margin-left: 35px;" class="glyphicon glyphicon-chevron-down  pull-right btn_ocultar nietoPrincipal"  data-cactividadproyectonietoclick="{{$nivel2['cproyectoactividades']}}"></i>
                                                @else
                                                    <td style="display: flex;justify-content: flex-start;margin-left: 54px" class="name_actividad">
                                                        @endif
                                                        <span style="display: block;margin-left: 10px;">{{$nivel2['codigoactividad']}} - {{$nivel2['descripcionactividad']}}</span>
                                                    </td>
                                                    <td>{{$nivel2['numdoc']}}</td>
                                                    <td>{{$nivel2['numplanos']}}</td>
                                                    <td>{{$nivel2['horaspresuestadasnieto']}}</td>
                                                    <td>{{$nivel2['horascargadasnieto']}}</td>
                                                    @if (($nivel2['horaspresuestadasnieto'] - $nivel2['horascargadasnieto']) >= 0  )
                                                        <td> {{$nivel2['horaspresuestadasnieto'] - $nivel2['horascargadasnieto']}}  </td>
                                                    @else
                                                        <td style="color: tomato"> {{$nivel2['horaspresuestadasnieto'] - $nivel2['horascargadasnieto']}}  </td>
                                                    @endif
                                                    <td>
                                                        <div class="progress" >
                                                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel2['avance']}}%;background-color: #5cb85c;">
                                                                <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel2['avance']}}%</b></span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td  class="verAreas">
                                                        @if ($nivel2['avance'] <= 80)
                                                            <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                                                <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                            </div>
                                                        @elseif ($nivel2['avance'] >= 81 && $nivel2['avance'] <= 100)
                                                            <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                                <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                            </div>
                                                        @else
                                                            <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                                <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                            </div>
                                                        @endif
                                                    </td>

                                                    <td>{{ $nivel2['montopresupuestadossnieto'] }}</td>
                                                    <td>{{  $nivel2['montoCargadosnieto'] }}</td>
                                                    @if (($nivel2['montoCargadosnieto_saldo']) >= 0  )
                                                        <td> {{$nivel2['montoCargadosnieto_saldo']}}  </td>
                                                    @else
                                                        <td style="color: tomato;">{{$nivel2['montoCargadosnieto_saldo']}} </td>
                                                    @endif
                                                    <td>
                                                        <div class="progress" >
                                                            <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel2['avance_nieto']}}%;background-color: #5cb85c;">
                                                                <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel2['avance_nieto']}}%</b></span>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="verAreasrate">
                                                        @if ($nivel2['avance_nieto'] <= 80)

                                                            <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                                                <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                            </div>
                                                        @elseif ($nivel2['avance_nieto'] >= 81 && $nivel2['avance_nieto'] <= 100)
                                                            <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                                <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                            </div>
                                                        @else
                                                            <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                                <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                            </div>

                                                        @endif
                                                    </td>
                                            </tr>
                                            @foreach ($nivel2['nivel3'] as $nivel3)
                                                @if ($nivel3['familia'] == 'bisnieto')
                                                    <tr  data-cproyecto="{{$nivel3['cproyecto']}}" data-idactividad="{{$nivel3['cproyectoactividades']}}" data-descripcion="{{$nivel3['descripcionactividad']}}" data-iddescripcion="{{$nivel3['codigoactividad']}}" data-cproyectoactividades_parent="{{$nivel3['cproyectoactividades_parent']}}" data-cproyectoactividadeshijo_parent="{{$nivel3['cproyectoactividadeshijo_parent']}}" data-cproyectoactividadeshijodirecto_parent="{{$nivel3['cproyectoactividadeshijodirecto_parent']}}" class="ocultarActividad">
                                                        @if ($nivel3['nivel4'] != null)
                                                            <td style="background: #ddd;display: flex;justify-content: flex-start" class="name_actividad">
                                                                <i  style="display:block;color: black;font-size: 1.5em;cursor:pointer;display: block;margin-left: 52px;" class="glyphicon glyphicon-chevron-down  pull-right btn_ocultar tataranietoPrincipal"  data-cactividadproyectonietoclick="{{$nivel3['cproyectoactividades']}}"></i>
                                                        @else
                                                            <td style="display: flex;justify-content: flex-start;margin-left: 70px" class="name_actividad">
                                                                @endif
                                                                <span style="display: block;margin-left: 10px;">{{$nivel3['codigoactividad']}} - {{$nivel3['descripcionactividad']}}</span>
                                                            </td>
                                                        <td>{{$nivel3['numdoc']}}</td>
                                                        <td>{{$nivel3['numplanos']}}</td>
                                                        <td>{{$nivel3['horaspresuestadasbisnieto']}}</td>
                                                        <td>{{$nivel3['horascargadasbisnieto']}}</td>
                                                        @if (($nivel3['horaspresuestadasbisnieto'] - $nivel3['horascargadasbisnieto']) >= 0  )
                                                            <td> {{$nivel3['horaspresuestadasbisnieto'] - $nivel3['horascargadasbisnieto']}}  </td>
                                                        @else
                                                            <td style="color: tomato;"> {{$nivel3['horaspresuestadasbisnieto'] - $nivel3['horascargadasbisnieto']}}  </td>
                                                        @endif
                                                        <td>
                                                            <div class="progress" >
                                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel3['avance']}}%;background-color: #5cb85c;">
                                                                    <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel3['avance']}}%</b></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td  class="verAreas">
                                                            @if ($nivel3['avance'] <= 80)
                                                                <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align"  data-color='#5cb85c'>
                                                                    <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                                </div>
                                                            @elseif ($nivel3['avance'] >= 81 && $nivel3['avance'] <= 100)
                                                                <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                                    <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                                </div>
                                                            @else
                                                                <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                                    <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                                </div>
                                                            @endif
                                                        </td>

                                                        <td>{{ $nivel3['montopresupuestadossbisnieto'] }}</td>
                                                        <td>{{  $nivel3['montoCargadosbisnieto'] }}</td>
                                                        @if (($nivel3['montoCargadosbisnieto_saldo']) >= 0  )
                                                            <td> {{$nivel3['montoCargadosbisnieto_saldo']}}  </td>
                                                        @else
                                                            <td style="color: tomato;">{{$nivel3['montoCargadosbisnieto_saldo']}} </td>
                                                        @endif
                                                        <td>
                                                            <div class="progress" >
                                                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel3['avance_bisnieto']}}%;background-color: #5cb85c;">
                                                                    <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel3['avance_bisnieto']}}%</b></span>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td class="verAreasrate">
                                                            @if ($nivel3['avance_bisnieto'] <= 80)

                                                                <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                                                    <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                                </div>
                                                            @elseif ($nivel3['avance_bisnieto'] >= 81 && $nivel3['avance_bisnieto'] <= 100)
                                                                <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                                    <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                                </div>
                                                            @else
                                                                <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                                    <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                                </div>

                                                            @endif
                                                        </td>
                                                    </tr>
                                                       @foreach($nivel3['nivel4'] as $nivel4)
                                                           @if ($nivel4['familia'] == 'tataranieto')
                                                               <tr   data-cproyecto="{{$nivel4['cproyecto']}}" data-idactividad="{{$nivel4['cproyectoactividades']}}" data-descripcion="{{$nivel4['descripcionactividad']}}" data-iddescripcion="{{$nivel4['codigoactividad']}}" data-cproyectoactividades_parent="{{$nivel4['cproyectoactividades_parent']}}" data-cproyectoactividadeshijo_parent="{{$nivel4['cproyectoactividadeshijo_parent']}}" data-cproyectoactividadeshijodirecto_parent="{{$nivel4['cproyectoactividadeshijodirecto_parent']}}" data-cproyectoactividadestataranieto_parent="{{ $nivel4['cproyectoactividadestataranieto_parent'] }}" class="ocultarActividad">
                                                                   <td  style="padding-left: 130px">{{$nivel4['codigoactividad']}} - {{$nivel4['descripcionactividad']}} </td>
                                                                   <td>{{$nivel4['numdoc']}}</td>
                                                                   <td>{{$nivel4['numplanos']}}</td>
                                                                   <td>{{$nivel4['horaspresuestadasbisnieto']}}</td>
                                                                   <td>{{$nivel4['horascargadasbisnieto']}}</td>
                                                                   @if (($nivel4['horaspresuestadasbisnieto'] - $nivel4['horascargadasbisnieto']) >= 0  )
                                                                       <td> {{$nivel4['horaspresuestadasbisnieto'] - $nivel4['horascargadasbisnieto']}}  </td>
                                                                   @else
                                                                       <td style="color: tomato;"> {{$nivel4['horaspresuestadasbisnieto'] - $nivel4['horascargadasbisnieto']}}  </td>
                                                                   @endif
                                                                   <td>
                                                                       <div class="progress" >
                                                                           <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel4['avance']}}%;background-color: #5cb85c;">
                                                                               <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel4['avance']}}%</b></span>
                                                                           </div>
                                                                       </div>
                                                                   </td>
                                                                   <td  class="verAreas">
                                                                       @if ($nivel4['avance'] <= 80)
                                                                           <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align"  data-color='#5cb85c'>
                                                                               <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                                           </div>
                                                                       @elseif ($nivel4['avance'] >= 81 && $nivel4['avance'] <= 100)
                                                                           <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                                               <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                                           </div>
                                                                       @else
                                                                           <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                                               <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                                           </div>
                                                                       @endif
                                                                   </td>

                                                                   <td>{{ $nivel4['montopresupuestadossbisnieto'] }}</td>
                                                                   <td>{{  $nivel4['montoCargadosbisnieto'] }}</td>
                                                                   @if (($nivel4['montoCargadosbisnieto_saldo']) >= 0  )
                                                                       <td> {{$nivel4['montoCargadosbisnieto_saldo']}}  </td>
                                                                   @else
                                                                       <td style="color: tomato;">{{$nivel4['montoCargadosbisnieto_saldo']}} </td>
                                                                   @endif
                                                                   <td>
                                                                       <div class="progress" >
                                                                           <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $nivel4['avance_bisnieto']}}%;background-color: #5cb85c;">
                                                                               <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $nivel4['avance_bisnieto']}}%</b></span>
                                                                           </div>
                                                                       </div>
                                                                   </td>
                                                                   <td class="verAreasrate">
                                                                       @if ($nivel4['avance_bisnieto'] <= 80)

                                                                           <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                                                               <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                                                           </div>
                                                                       @elseif ($nivel4['avance_bisnieto'] >= 81 && $nivel4['avance_bisnieto'] <= 100)
                                                                           <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                                                               <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                                                           </div>
                                                                       @else
                                                                           <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                                                               <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                                                           </div>

                                                                       @endif
                                                                   </td>
                                                               </tr>
                                                           @else
                                                           @endif
                                                       @endforeach
                                                @else
                                                @endif
                                            @endforeach
                                        @else
                                        @endif
                                    @endforeach
                                @else
                                @endif
                            @endforeach
                        @endif
                    @endforeach
                @endif
            @endforeach
        @endif
        </tbody>
        <tfoot>
        <tr>

            <td>Total Entregables - Total Horas</td>
            @if(empty($acumulado_total_horas_doc))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $acumulado_total_horas_doc }}</b></td>
            @endif
            @if(empty($acumulado_total_horas_plano))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $acumulado_total_horas_plano }}</b></td>
            @endif
            @if(empty($totalpresuestadasporProyecto_final))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalpresuestadasporProyecto_final }}</b></td>
            @endif

            @if(empty($totalCargadasporProyecto_final))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalCargadasporProyecto_final }}</b></td>
            @endif


            @if(empty($totalSaldoporProyecto_final))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalSaldoporProyecto_final }}</b></td>
            @endif

            @if(empty($totalavanceporProyecto_final) )
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalavanceporProyecto_final  }}%</b></td>
            @endif
        <td rowspan="2" class="resumenProyectoSOC" title="Ver gráfico">
                @if (empty($totalavanceporProyecto_final))
                    {{--  <td rowspan="2">--}}
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                        <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true" ></div>
                    </div>
                    {{-- </td>--}}
                @else

                    @if ($totalavanceporProyecto_final <= 80)
                        <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align"  data-color='#5cb85c'>
                            <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                        </div>
                    @elseif ($totalavanceporProyecto_final >= 81 && $totalavanceporProyecto_final <= 100)
                        <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                            <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                        </div>
                    @else
                        <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                            <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                        </div>
                    @endif
            </td>

            @endif



            @if(empty($totalMontopresuestadasporProyecto_final))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalMontopresuestadasporProyecto_final }}</b></td>
            @endif

            @if(empty($totalMontoCargadasporProyecto_final))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalMontoCargadasporProyecto_final }}</b></td>
            @endif

            @if(empty($totalMontoSaldoporProyecto_final))
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalMontoSaldoporProyecto_final }}</b></td>
            @endif

            @if(empty($totalavancemontoporProyecto_final) )
                <td rowspan="2"><b>0</b></td>
            @else
                <td rowspan="2"><b>{{ $totalavancemontoporProyecto_final  }}%</b></td>
            @endif
            <td rowspan="2" class="resumenMontoProyectoSOC">
                @if (empty($totalavancemontoporProyecto_final))
                    {{-- <td  rowspan="2">--}}
                    <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                        <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true" ></div
                    </div>
                    {{-- </td>--}}
                @else

                    @if ($totalavancemontoporProyecto_final <= 80)
                        <div style="width: 30px;height: 30px;margin:auto;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                            <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                        </div>
                    @elseif ($totalavancemontoporProyecto_final >= 81 && $totalavancemontoporProyecto_final <= 100)
                        <div style="width: 30px;height: 30px;margin:auto;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                            <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                        </div>
                    @else
                        <div style="width: 30px;height: 30px;margin:auto;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                            <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                        </div>
                    @endif
            </td>

            @endif

        </tr>
        <tr>
            <td>Costo subtotal horas USS</td>
        </tr>
        </tfoot>
    </table>

</div>
<style>
    .ocultarActividad
    {
        display:none;
    }
    .highcharts-drilldown-axis-label {
        text-decoration:none!important;
        fill: #666666!important;
        font-weight: 400!important;
    }
    text.highcharts-data-label {
        fill: #666!important;
        text-decoration: none!important;
    }
</style>


<script>
    // HH

    $('.verAreas').click(function(e){

        cactividad=$(this).parent().data("idactividad");
        color=$(this).children().data("color");
        descripcionactividad = $(this).parent().data("descripcion");
        idactividad = $(this).parent().data("iddescripcion");
        cproyecto = $(this).parent().data("cproyecto");
        verActividadesporAreaCCHH(e,cactividad,color,descripcionactividad,idactividad,cproyecto);
        //mostrarAreaporCadaActividad();
    });

    function verActividadesporAreaCCHH(e,cactividad,color,descripcionactividad,idactividad,cproyecto)
    {
        $('.areas').remove();
        detalle = '<div class="box box-widget widget-user areas mover" style="width:862px;min-height:auto;border-radius:5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo() style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';
        detalle += '<div id="listadehorasporsemana" class="mover">';
        detalle += '</div>';
        // detalle +='<div id="sliders" style="min-width: 310px; max-width: 800px;margin: 0 auto;">';
        // detalle +='        <table style="width:100%;">';
        // detalle +='        <tr>';
        // detalle +='        <td>Àngulo Y</td>';
        // detalle +='    <td><input id="alpha" type="range" min="0" max="50" value="35"/></td><td><span id="alpha-value" class="value">35</span></td>';
        // detalle +='    </tr>';
        // detalle +='    <tr>';
        // detalle +='    <td>Àngulo X</td>';
        // detalle +='    <td><input id="beta" type="range" min="-50" max="50" value="0"/></td><td><span id="beta-value" class="value">0</span></td>';
        // detalle +='    </tr>';
        // detalle +='    <tr>';
        // detalle +='    <td>Profundidad</td>';
        // detalle +='    <td><input id="depth" type="range" min="0" max="100" value="100"/></td><td><span id="depth-value" class="value">100</span></td>';
        // detalle +='    </tr>';
        // detalle +='    </table>';
        // detalle +='    </div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.areas').draggable();
        $(".mover").resizable();

        $(this).css('z-index', 10);
        $('.areas').fadeIn('500');
        $('.areas').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.areas').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.areas').css({'border': '1px solid black'})

        var obj = {cproyecto: cproyecto, disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val(),cproyectoactividades:  cactividad};
        console.log(obj,"123");

        $.ajax({
            type:'GET',
            url: 'mostrarAreaporCadaActividad',
            data: obj,
            success: function (data) {
                // console.log("data",data);
                // console.log("colorPu",color);

                chart_listadp_areas(data,color,descripcionactividad,idactividad);

            },
            error: function(data){

            }
        });

    }

    // MONTO

    $('.verAreasrate').click(function(e){

        cactividad=$(this).parent().data("idactividad");
        color=$(this).children().data("color");
        descripcionactividad = $(this).parent().data("descripcion");
        idactividad = $(this).parent().data("iddescripcion");
        cproyecto = $(this).parent().data("cproyecto");
        verActividadesporAreaCCHHMonto(e,cactividad,color,descripcionactividad,idactividad,cproyecto);
        //mostrarAreaporCadaActividad();
    });

    function verActividadesporAreaCCHHMonto(e,cactividad,color,descripcionactividad,idactividad,cproyecto)
    {
        $('.areasCCHH').remove();
        detalle = '<div class="box box-widget widget-user areasCCHH moverCCHH" style="width:862px;min-height:auto;border-radius: 5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo()  style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';
        // detalle += '<ul class="nav nav-stacked">';
        // detalle += '<table class="table table-striped table-hover table-bordered table-responsive display" style="font-size: 12px; width:100%; text-align: center;" id="tablequipo">';
        // <caption>table title and/or explanatory text</caption>

        //detalle += '<tbody>';
        detalle += '<div id="listadehorasporsemanaCCHH" class="moverCCHH">';
        detalle += '</div>';
        // detalle +='<div id="sliders" style="min-width: 310px; max-width: 800px;margin: 0 auto;">';
        // detalle +='        <table style="width:100%;">';
        // detalle +='        <tr>';
        // detalle +='        <td>Àngulo Y</td>';
        // detalle +='    <td><input id="alpha" type="range" min="0" max="50" value="35"/></td><td><span id="alpha-value" class="value">35</span></td>';
        // detalle +='    </tr>';
        // detalle +='    <tr>';
        // detalle +='    <td>Àngulo X</td>';
        // detalle +='    <td><input id="beta" type="range" min="-50" max="50" value="0"/></td><td><span id="beta-value" class="value">0</span></td>';
        // detalle +='    </tr>';
        // detalle +='    <tr>';
        // detalle +='    <td>Profundidad</td>';
        // detalle +='    <td><input id="depth" type="range" min="0" max="100" value="100"/></td><td><span id="depth-value" class="value">100</span></td>';
        // detalle +='    </tr>';
        // detalle +='    </table>';
        // detalle +='    </div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.areasCCHH').draggable();
        $(".moverCCHH").resizable();

        $(this).css('z-index', 10);
        $('.areasCCHH').fadeIn('500');
        $('.areasCCHH').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.areasCCHH').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.areasCCHH').css({'border': '1px solid black'})


        var obj = {cproyecto: cproyecto, disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val(),cproyectoactividades:  cactividad};
        //console.log(obj);

        $.ajax({
            type:'GET',
            url: 'mostrarAreaporCadaActividadCCHH',
            data: obj,
            success: function (data) {

                chart_listadp_areasCCHH(data,color,descripcionactividad,idactividad);

            },
            error: function(data){

            }
        });



    }

    // Resumen Proyecto HORAS
    $('.resumendehoras_por_proyecto').click(function (e) {
        // alert("123");

        color= $(this).children().data("color");
        cproyecto = $(this).data("cproyecto");
        codigoproy = $(this).data("codigoproyecto");
        nombreproy = $(this).data("nombreproyecto");
        verResumenNivelProyecto(e,color,cproyecto,codigoproy,nombreproy)
       // console.log("resumenProyecto",this);

    });

    function verResumenNivelProyecto(e,color,cproyecto) {

        $('.resumenP').remove();
        detalle = '<div class="box box-widget widget-user resumenP moverP" style="width:852px;min-height:auto;border-radius: 5px 5px 5px 5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo() style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';
        // detalle += '<ul class="nav nav-stacked">';
        // detalle += '<table class="table table-striped table-hover table-bordered table-responsive display" style="font-size: 12px; width:100%; text-align: center;" id="tablequipo">';
        // <caption>table title and/or explanatory text</caption>

        //detalle += '<tbody>';
        detalle += '<div id="resumenhorasConsumidosPropuestas" class="moverP">';
        detalle += '</div>';
         // detalle +='<div id="sliders_horas" style="min-width: 310px; max-width: 800px;margin: 0 auto;">';
         // detalle +='        <table style="width:100%;">';
         // detalle +='        <tr>';
         // detalle +='        <td>Àngulo Y</td>';
         // detalle +='    <td><input id="alpha" type="range" min="0" max="50" value="35"/></td><td><span id="alpha-value_horas" class="value">35</span></td>';
         // detalle +='    </tr>';
         // detalle +='    <tr>';
         // detalle +='    <td>Àngulo X</td>';
         // detalle +='    <td><input id="beta" type="range" min="-50" max="50" value="0"/></td><td><span id="beta-value_horas" class="value">0</span></td>';
         // detalle +='    </tr>';
         // detalle +='    <tr>';
         // detalle +='    <td>Profundidad</td>';
         // detalle +='    <td><input id="depth" type="range" min="0" max="100" value="100"/></td><td><span id="depth-value_horas" class="value">100</span></td>';
         // detalle +='    </tr>';
         // detalle +='    </table>';
         // detalle +='    </div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.resumenP').draggable();
        $(".moverP").resizable();

        $(this).css('z-index', 10);
        $('.resumenP').fadeIn('500');
        $('.resumenP').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.resumenP').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.resumenP').css({'border': '1px solid black'});


        var obj = {cproyecto: cproyecto, disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()};
        console.log(obj);

        $.ajax({
            type:'GET',
            url: 'mostrarResumenProyecto',
            data: obj,
            success: function (data) {
                // console.log("data",data);
                // console.log("colorPu",color);

                chart_listado_resumen(data,color,cproyecto,codigoproy,nombreproy);

            },
            error: function(data){

            }
        });

    }

    // Resumen Proyecto Monto

    $('.resumendemonto_por_proyecto').click(function (e) {

        color= $(this).children().data("color");  
        cproyecto = $(this).data("cproyecto"); 
        codigoproy = $(this).data("codigoproyecto");
        nombreproy = $(this).data("nombreproyecto");     
        verResumenMontoNivelProyecto(e,color,cproyecto,codigoproy,nombreproy)

    });
    function verResumenMontoNivelProyecto(e,color,cproyecto,codigoproy,nombreproy) {

        $('.resumenMontoFinal').remove();
        detalle = '<div class="box box-widget widget-user resumenMontoFinal moverResumenP" style="width:862px;min-height:auto;border-radius: 5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo() style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';

        detalle += '<div id="resumenMontoConsumidosPropuestas" class="moverP">';
        detalle += '</div>';
        // detalle +='<div id="sliders_monto" style="min-width: 310px; max-width: 800px;margin: 0 auto;">';
        // detalle +='        <table style="width:100%;">';
        // detalle +='        <tr>';
        // detalle +='        <td>Àngulo Y</td>';
        // detalle +='    <td><input id="alpha" type="range" min="0" max="50" value="35"/></td><td><span id="alpha-value_monto" class="value">35</span></td>';
        // detalle +='    </tr>';
        // detalle +='    <tr>';
        // detalle +='    <td>Àngulo X</td>';
        // detalle +='    <td><input id="beta" type="range" min="-50" max="50" value="0"/></td><td><span id="beta-value_monto" class="value">0</span></td>';
        // detalle +='    </tr>';
        // detalle +='    <tr>';
        // detalle +='    <td>Profundidad</td>';
        // detalle +='    <td><input id="depth" type="range" min="0" max="100" value="100"/></td><td><span id="depth-value_monto" class="value">100</span></td>';
        // detalle +='    </tr>';
        // detalle +='    </table>';
        // detalle +='    </div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.resumenMontoFinal').draggable();
        $(".moverResumenP").resizable();

        $(this).css('z-index', 10);
        $('.resumenMontoFinal').fadeIn('500');
        $('.resumenMontoFinal').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.resumenMontoFinal').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.resumenMontoFinal').css({'border': '1px solid black'});


        var obj = {cproyecto: cproyecto, disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()};
        console.log(obj);

        $.ajax({
            type:'GET',
            url: 'mostrarResumenMontoProyecto',
            data: obj,
            success: function (data) {
                // console.log("data",data);
                // console.log("colorPu",color);

                chart_listado_resumenMonto(data,color,codigoproy,nombreproy);

            },
            error: function(data){

            }
        });

    }

    // Resumen Consolidado de las horas

    $('.resumenProyectoSOC').click(function (e) {
        // alert("123");

        color= $(this).children().data("color");
        cproyecto =$("#proyectos").val();
       
        verResumenNivelProyectoSOC(e,color,cproyecto)
    });

    function verResumenNivelProyectoSOC(e,color,cproyecto)
    {
        $('.resumeHorasFinalSOC').remove();
        detalle = '<div class="box box-widget widget-user resumeHorasFinalSOC moverResumenSOC" style="width:862px;min-height:auto;border-radius: 5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo() style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';

        detalle += '<div id="resumenhorasConsumidosPropuestasProyectoSOC" class="moverP">';
        detalle += '</div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.resumeHorasFinalSOC').draggable();
        $(".moverResumenSOC").resizable();

        $(this).css('z-index', 10);
        $('.resumeHorasFinalSOC').fadeIn('500');
        $('.resumeHorasFinalSOC').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.resumeHorasFinalSOC').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.resumeHorasFinalSOC').css({'border': '1px solid black'});


        var obj = {cproyecto: $("#proyectos").val() ,soc: $('#soc').prop('checked'),cbo_selectSOC: $('#cbo_selectSOC').val(), disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()};
        //console.log(obj);

        $.ajax({
            type:'GET',
            url: 'mostrarResumenHorasConsumidas-Presupuestadas-ProyectoSOC',
            data: obj,
            success: function (data) {
                console.log("data",data);
                // console.log("colorPu",color);
                chart_listadoSOC_resumen(data,color);
                //chart_listado_resumenHorasPresuestadasConsumidas(data,color);

            },
            error: function(data){

            }
        });

    }



     $('.resumenMontoProyectoSOC').click(function (e) {

        color= $(this).children().data("color");  
        cproyecto = $(this).data("cproyecto");   
        verResumenMontoNivelProyectoSOC(e,color,cproyecto)

    });
    function verResumenMontoNivelProyectoSOC(e,color,cproyecto) {

        $('.resumenMontoFinalSoc').remove();
        detalle = '<div class="box box-widget widget-user resumenMontoFinalSoc moverResumenPSOC" style="width:862px;min-height:auto;border-radius: 5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo() style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';

        detalle += '<div id="resumenmontoConsumidosPropuestasProyectoSOC" class="moverP">';
        detalle += '</div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.resumenMontoFinalSoc').draggable();
        $(".moverResumenPSOC").resizable();

        $(this).css('z-index', 10);
        $('.resumenMontoFinalSoc').fadeIn('500');
        $('.resumenMontoFinalSoc').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.resumenMontoFinalSoc').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.resumenMontoFinalSoc').css({'border': '1px solid black'});


        var obj = {cproyecto: $("#proyectos").val(),cbo_selectSOC: $('#cbo_selectSOC').val(),soc: $('#soc').prop('checked'), disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()};
        //console.log(obj);

        $.ajax({
            type:'GET',
            url: 'mostrarResumenMontoConsumidas-Presupuestadas-ProyectoSOC',
            data: obj,
            success: function (data) {
                // console.log("data",data);
                // console.log("colorPu",color);

                chart_listado_resumenMontoSOC(data,color);

            },
            error: function(data){

            }
        });

    }





    // Ocultar Actividades solo Padres

    $('.padrePrincipal').click(function(){
        var codigoPadre = $(this).data("cactividadproyectoclick");
        down = $(this).hasClass('glyphicon-chevron-down');
        if(down)
        {
            mostrarfila(codigoPadre);
        }
        else{
            ocultarfila(codigoPadre);
        }
        $(this).toggleClass("glyphicon-chevron-down glyphicon-chevron-up");

    });

    function ocultarfila(value)
    {
        $('tr[data-cproyectoactividades_parent="' + value + '"]').hide();
    }

    function mostrarfila(value)
    {
        $('tr[data-cproyectoactividades_parent="' + value + '"]').show();
    }

    // Ocultar Actividades solo HIJOS

    $('.hijoPrincipal').click(function(){
        var codigoPadreHIjo = $(this).data("cactividadproyectohijoclick");
        downhijo = $(this).hasClass('glyphicon-chevron-down');
        downhijo == true ? mostrarfilaHijo(codigoPadreHIjo) : ocultarfilaHijo(codigoPadreHIjo);
        $(this).toggleClass("glyphicon-chevron-down glyphicon-chevron-up");
    });

    function mostrarfilaHijo(value)
    {
        $('tr[data-cproyectoactividadeshijo_parent="' + value + '"]').hide();
    }
    function ocultarfilaHijo(value)
    {
        $('tr[data-cproyectoactividadeshijo_parent="' + value + '"]').show();
    }

    $('.nietoPrincipal').click(function(){
        var nieto = $(this).data("cactividadproyectonietoclick");
        downnieto = $(this).hasClass('glyphicon-chevron-down');
        downnieto == true ? mostrarfilatataranieto(nieto) : ocultarfilatataranieto(nieto);
        $(this).toggleClass("glyphicon-chevron-down glyphicon-chevron-up");

    });

    function mostrarfilatataranieto(valueT)
    {
        $('tr[data-cproyectoactividadeshijodirecto_parent="' + valueT + '"]').hide();
    }
    function ocultarfilatataranieto(valueT)
    {
        $('tr[data-cproyectoactividadeshijodirecto_parent="' + valueT + '"]').show();
    }

    $('.tataranietoPrincipal').click(function(){
        var tataranieto = $(this).data("cactividadproyectonietoclick");
        $('tr[data-cproyectoactividadestataranieto_parent="' + tataranieto + '"]').toggle();
        $(this).toggleClass("glyphicon-chevron-down glyphicon-chevron-up");
        //});
    });


</script>


