<div class="col-sm-12 table-responsive ">
    <table class="table table-hover table-bordered " id="actividades" style="font-size:12px;" >
        <thead>
        <tr  class="clsCabereraTabla">
            <th rowspan="2" style="    text-align: center;vertical-align: middle">Gerente del Proyecto</th>
            @if($lideroGerente == 'gerente'  && empty($disciplina))
                <th rowspan="2" style="    text-align: center;vertical-align: middle">Senior</th>
                <th rowspan="2" style="    text-align: center;vertical-align: middle">Lider del Proyecto</th>
            @elseif ($lideroGerente == 'gerente'  && $disciplina != 'Nuevo' )
                <th rowspan="2" style="    text-align: center;vertical-align: middle">Senior</th>
                <th rowspan="2" style="    text-align: center;vertical-align: middle">Lider del Proyecto</th>
            @elseif($lideroGerente == 'gerente' && $disciplina == 'Nuevo')
            @elseif ($lideroGerente == 'lider'  &&  empty($disciplina) )
                <th rowspan="2" style="    text-align: center;vertical-align: middle">Senior</th>
            @elseif ($lideroGerente == 'lider'  && $disciplina != 'Nuevo' )
                <th rowspan="2" style="    text-align: center;vertical-align: middle">Senior</th>
            @else


            @endif
            <th rowspan="2" style="    text-align: center;vertical-align: middle">Unidad Minera</th>
            <th rowspan="2" style="    text-align: center;vertical-align: middle">Codigo</th>
            <th rowspan="2" style="    text-align: center;vertical-align: middle;border-right: none">Nombre</th>
            <th rowspan="2" style="    text-align: center;vertical-align: middle;border: none"><span style="color: #0069aa">Nombre</span></th>
            <th rowspan="2" style="    text-align: center;vertical-align: middle;border: none"><span style="color: #0069aa">Nombre</span></th>
            <th rowspan="2" style="    text-align: center;vertical-align: middle;border-left: 1px solid">Tipo Gestion de Proy.</th>
            <th rowspan="2" style="    text-align: center;vertical-align: middle">Estado</th>
           {{-- <th colspan="2" style="    text-align: center;vertical-align: middle;border-bottom: 1px solid #fff;">Presupuestados</th>
            <th colspan="2" style="text-align: center;vertical-align: middle;border-bottom: 1px solid #fff;">Cargadas</th>--}}
            <th colspan="5" style="text-align: center;vertical-align: middle; border-bottom: 1px solid #fff;">HH</th>
            <th colspan="5" style="text-align: center;vertical-align: middle; border-bottom: 1px solid #fff;">HH ($) </th>
            {{--<th colspan="5" style="text-align: center;vertical-align: middle; border-bottom: 1px solid #fff;">Indicadores</th>--}}
            {{--<th colspan="5">Gestion de Rate / Horas </th>--}}
        </tr>
        <tr class="clsCabereraTabla">
            <!--====  Cabecera actividades  ====-->

            <!--====  Control de Horas  HH ====-->

            <!--==== Cabecera Horas por rate  ====-->
            {{--<th style="text-align: center;vertical-align: middle;">Doc</th>
            <th style="text-align: center;vertical-align: middle;">Planos</th>

            <th style="text-align: center;vertical-align: middle;">Doc</th>
            <th style="text-align: center;vertical-align: middle;">Planos</th>--}}

            <th>
                <div style="text-align: center" >HH</div>
                <div style="text-align: center" >presupuestadas</div>

            </th>
            <!--==== Cabecera Horas por rate  ====-->
            <th>
                <div style="text-align: center" >HH</div>
                <div style="text-align: center" >Cargadas</div>
            </th>
            <th>Saldo</th>
            <th>Avance</th>
            <th>Estado</th>

            <th>
                <div style="text-align: center" >Monto</div>
                <div style="text-align: center" >presupuestadas</div>

            </th>
            <!--==== Cabecera Horas por rate  ====-->
            <th>
                <div style="text-align: center" >Monto</div>
                <div style="text-align: center" >Cargadas</div>
            </th>
            <th>Saldo</th>
            <th>Avance</th>
            <th>Estado</th>

            <!--==== Cabecera INDICADORES  ====-->
            {{--<th>% EV HH</th>
            <th>CPI HH</th>
            <th>% EV Total</th>
            <th>CPI Total</th>
            <th>CV Total</th>--}}

        </tr>
        </thead>
        <tbody>
        @if(isset($arrayProyectos))

            @foreach($arrayProyectos as $lp)
                @if ($lp['abreviatura'] == $nombrelider->abreviatura)
                    <tr>
                @else
                    <tr class="hideshow">
                @endif
                        <td>{{ $lp['abreviatura'] }}</td>
                        @if ($lideroGerente == 'gerente' && $disciplina != 'Nuevo')
                            <td>{{ $lp['senior'] }}</td>
                            <td>{{ $lp['lider'] }}</td>
                        @elseif($lideroGerente == 'gerente' && $disciplina == 'Nuevo')
                        @elseif($lideroGerente == 'lider'  && $disciplina != 'Nuevo')
                            <td>{{ $lp['senior'] }}</td>
                        @else

                        @endif
                        <td>{{ $lp['nombreunidadminera'] }}</td>
                        <td>{{ $lp['codigo'] }}</td>
                        <td colspan="3">{{ $lp['nombre'] }} </td>
                        <td style="display: none">12 </td>
                        <td style="display: none">12 </td>
                        <td> {{ $lp['nombretipogestion'] }} </td>
                        <td>{{ $lp['descripcion'] }} </td>
                        {{--<td>{{ $lp['numdocPresupuestados'] }}</td>
                        <td>{{ $lp['numplaPresupuestados'] }}</td>
                        <td>{{ $lp['numdoc'] }}</td>
                        <td>{{ $lp['numpla'] }}</td>--}}

                        <td>{{ $lp['HHPresupuestadasTotales'] }}</td>
                        <td>{{ $lp['HHConsumidasTotales'] }}</td>
                        <td
                        @if ($lp['HHSaldoTotales']  >= 0)
                        @else
                        style="color: tomato"
                        @endif
                        >
                           {{ $lp['HHSaldoTotales'] }}
                        </td>

                        <td>
                            <div class="progress" >
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $lp['avanceTotalHH'] }}%;background-color: #5cb85c;">
                                    <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $lp['avanceTotalHH'] }}%</b></span>
                                </div>
                            </div>
                        </td>

                        <td class="ProyectohorasResumen" data-cproyecto = "{{  $lp['cproyecto'] }}" data-codnombrepry ="{{ $lp['codigo'] .' '. $lp['nombre'] }}">

                            @if ($lp['avanceTotalHH'] <= 80)

                                <div style="width: 30px;height: 30px;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                    <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                </div>
                            @elseif ($lp['avanceTotalHH'] >= 81 && $lp['avanceTotalHH'] <= 100)
                                <div style="width: 30px;height: 30px;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                    <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                </div>
                            @else
                                <div style="width: 30px;height: 30px;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                    <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                </div>

                            @endif
                        </td>
                        <td>{{ $lp['suma_total_horas_rate_presupuestadas__proyecto'] }} </td>
                        <td>{{ $lp['suma_total_horas_rate_cargadas__proyecto'] }} </td>
                        <td>{{ $lp['suma_total_horas_cargadas_saldo__proyecto'] }} </td>
                        <td>
                            <div class="progress" >
                                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="45" aria-valuemin="0" aria-valuemax="100" style="width: {{ $lp['avance_total_padre_proyecto'] }}%;background-color: #5cb85c;">
                                    <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance"><b>{{ $lp['avance_total_padre_proyecto'] }}%</b></span>
                                </div>
                            </div>
                        </td>
                        <td class="ProyectoMontoResumen" data-cproyecto ="{{$lp['cproyecto']}}" data-codnombrepry ="{{ $lp['codigo'] .' '. $lp['nombre'] }}">
                            @if ($lp['avance_total_padre_proyecto'] <= 80)

                                <div style="width: 30px;height: 30px;background-color: #5cb85c;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#5cb85c'>
                                    <div class="glyphicon glyphicon-ok" style="color: #fff" aria-hidden="true"></div>
                                </div>
                            @elseif ($lp['avance_total_padre_proyecto'] >= 81 && $lp['avance_total_padre_proyecto'] <= 100)
                                <div style="width: 30px;height: 30px;background-color: #ffc107;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#ffc107'>
                                    <div class="glyphicon glyphicon-info-sign" style="color: #fff" aria-hidden="true"></div>
                                </div>
                            @else
                                <div style="width: 30px;height: 30px;background-color: #dc3545;display: flex;justify-content: center;align-items: center;border-radius: 50%" aria-label="Left Align" data-color='#dc3545'>
                                    <div class="glyphicon glyphicon-remove" style="color: #fff" aria-hidden="true"></div>
                                </div>

                            @endif
                        </td>
                       {{-- <td>{{ $lp['resumenPorcentajeFinal'] }}</td>
                        <td>{{ $lp['cpiHH'] }}</td>--}}
                        {{--<td></td>
                        <td></td>
                        <td></td>--}}

                    </tr>

            @endforeach
        @endif
        </tbody>
        <tfoot>

        </tfoot>
    </table>

</div>
<script>
    // Resumen Proyecto HORAS
    $('.ProyectohorasResumen').click(function (e) {
        //alert("123");

        color= $(this).children().data("color");
        cproyecto=$(this).data("cproyecto");
        nombreproy = $(this).data("codnombrepry");
        verResumenNivelProyecto(e,cproyecto,color,nombreproy)
    });

    function verResumenNivelProyecto(e,cproyecto,color,nombreproy) {

        $('.resumenP').remove();
        detalle = '<div class="box box-widget widget-user resumenP moverP" style="width:862px;min-height:auto;border-radius: 5px 5px 5px 5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo() style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';
        // detalle += '<ul class="nav nav-stacked">';
        // detalle += '<table class="table table-striped table-hover table-bordered table-responsive display" style="font-size: 12px; width:100%; text-align: center;" id="tablequipo">';
        // <caption>table title and/or explanatory text</caption>

        //detalle += '<tbody>';
        detalle += '<div id="resumenhorasConsumidosPropuestas" class="moverP">';
        detalle += '</div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.resumenP').draggable();
        $(".moverP").resizable();

        $(this).css('z-index', 10);
        $('.resumenP').fadeIn('500');
        $('.resumenP').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.resumenP').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.resumenP').css({'border': '1px solid black'});



        var obj = {cproyecto: cproyecto, disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()};
        console.log(obj);

        $.ajax({
            type:'GET',
            url: 'mostrarResumenProyecto',
            data: obj,
            success: function (data) {
                // console.log("data",data);
                // console.log("colorPu",color);

                chart_listado_resumen(data,color,nombreproy);

            },
            error: function(data){

            }
        });

    }

    // Resumen Proyecto Monto

    $('.ProyectoMontoResumen').click(function (e) {
        //alert("123456");
        color= $(this).children().data("color");
        cproyecto=$(this).data("cproyecto");
        nombreproy = $(this).data("codnombrepry");
        verResumenMontoNivelProyecto(e,cproyecto,color,nombreproy)

    });
    function verResumenMontoNivelProyecto(e,cproyecto,color,nombreproy) {

        $('.resumenMontoFinal').remove();
        detalle = '<div class="box box-widget widget-user resumenMontoFinal moverResumenP" style="width:862px;min-height:auto;border-radius: 5px 5px 5px 5px;position:absolute;z-index:10001;line-height: 200%;">';
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrarequipo() style="position: relative;top: -1.5px;left: 1px;height: 30px;width: 30px;color: white;background: red;"><i class="fa fa-times"></i></button>';
        detalle += '<div class="box-footer">';
        // detalle += '<ul class="nav nav-stacked">';
        // detalle += '<table class="table table-striped table-hover table-bordered table-responsive display" style="font-size: 12px; width:100%; text-align: center;" id="tablequipo">';
        // <caption>table title and/or explanatory text</caption>

        //detalle += '<tbody>';
        detalle += '<div id="resumenMontoConsumidosPropuestas" class="moverP">';
        detalle += '</div>';
        detalle += '</div>';
        detalle += '</div>';


        $('#lista').append(detalle);
        $('.resumenMontoFinal').draggable();
        $(".moverResumenP").resizable();

        $(this).css('z-index', 10);
        $('.resumenMontoFinal').fadeIn('500');
        $('.resumenMontoFinal').fadeTo('10', 1.9);
        // }).mouseover(function (e) {
        $('.resumenMontoFinal').css('top', e.pageY - 250);
        // $('.areas').css('right', e.pageX + 250);
        $('.resumenMontoFinal').css({'border': '1px solid black'});


        var obj = {cproyecto: cproyecto, disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()};
        console.log(obj);

        $.ajax({
            type:'GET',
            url: 'mostrarResumenMontoProyecto',
            data: obj,
            success: function (data) {
                // console.log("data",data);
                // console.log("colorPu",color);

                chart_listado_resumenMonto(data,color,nombreproy);

            },
            error: function(data){

            }
        });

    }


</script>
