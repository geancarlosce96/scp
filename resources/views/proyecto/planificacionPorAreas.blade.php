<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto -
        <small>Planificación de tareas del proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => 'grabarPlanificacionArea','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('semana_act',$semana_act,array('id'=>'semana_act')) !!}
    <div class="row">
        <!--<div class="col-lg-1 hidden-xs"></div>-->
        <div class="col-md-12 col-xs-12">
            <div class="row"> 
                    <div class="col-md-3 col-xs-3">
                        <div class="col-lg-2 col-xs-2 clsPadding">Desde</div>            
                        <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                            {!! Form::text('fdesde','', array('class'=>'form-control pull-right ','id' => 'fdesde','readonly'=>'true')) !!}
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <div class="col-lg-2 col-xs-2 clsPadding">Hasta</div>           
                        <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                            {!! Form::text('fhasta','', array('class'=>'form-control pull-right ','id' => 'fhasta','readonly'=>'true')) !!}
                        </div>        
                    </div>
                    <div class="col-md-2 col-xs-2">
                        Año del Cliente.
                    </div>                                  
                    <div class="col-md-2 col-xs-2">
                        <select name="anio" id="anio" class="form-control">
                        <?php
                            $anio=date("Y");
                            for($t=$anio+1 ; $t>=2010;$t--){
                            $selected="";
                            $selected = ($t==$anio?"selected":"");
                        ?>
                        <option value="{{ $t }}" {{ $selected }}>{{ $t }}</option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>                     
                    <div class="col-md-1 col-xs-1">
                        <div>
                        <button type="button" class="btn btn-primary btn-block " id="btnView" > <b>Visualizar</b></button></div>         
                    </div> 
                    <div class="col-md-1 col-xs-1">
                        <div>
                        <button type="submit" class="btn btn-primary btn-block "> <b>Grabar</b></button></div>         
                    </div> 
            </div> 
            <div class="row">	
                <br />
            </div>
            <div class="row">	
                <div class="col-lg-12 col-xs-12 table-responsive" id="tablePlani">
                    @include('partials.tablePlanificacionPorArea',array('proyectos'=>(isset($proyectos)==1?$proyectos:array()) ))    
                </div>
            </div>  
            <div class="col-md-12">
              <div class="col-lg-12 col-xs-12 table-responsive" id="participantes">
              @include('partials.planificacionPorAreasParticipantes',array('planiAcu'=>(isset($planiAcu)==1?$planiAcu:array()) ))
              </div>
            <div class="row">
                <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
            </div>
            <br>
            <div class="row clsPadding2">
                <div class="col-md-3 col-xs-3">
                    <div>
                        <button type="submit" class="btn btn-primary btn-block"> <b>Grabar</b></button>
                    </div>         
                </div>
            </div>
    </div>
</div>
   {!! Form::close() !!}
    </section>
    @include('partials.reporteHorasParticipante')
    <!-- Agregar Tareas -->
    <div id="agregarTareas" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color: white">Agregar profesional</h4>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => 'grabarPlanificacionArea','method' => 'POST','id' =>'frmTareas')) !!}
            {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_h')) !!}
            {!! Form::hidden('fdesde','',array('id'=>'fdesde_h')) !!}
            {!! Form::hidden('fhasta','',array('id'=>'fhasta_h')) !!}
            {!! Form::hidden('nroFila','0',array('id'=>'nroFila_h')) !!}
            {!! Form::hidden('cproyecto','',array('id'=>'cproyecto_h')) !!}
		    <div id="deta">
            </div>
            {!! Form::close() !!}
          </div>    
          <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="addTareas();" >Agregar</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
          </div>            
        </div>
      </div>
    </div>    
    <!-- fin Agregar Tareas -->
    <!-- *********************************** -->
    <!-- *************************************************** -->
      <div class="modal fade" id="modalObs" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="ObsModalLabel" style="color: white">Comentario</h4>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="message-text" class="control-label">Ingrese comentario:</label>
                  <textarea class="form-control" id="message-text"></textarea>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="saveObs()">Aceptar</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    <!-- *************************************************** -->      
    <!-- /.content -->
  </div>
<script>
        var selected_actividad =[];
        var selected_actividad_adm=[];
        var cproy=0;
        var idtr =0;
        var nroFila = <?php echo (isset($nroFila)==1?$nroFila:0); ?>;
        var i_col=0;
        var i_fila=0;        
        

        $('input[type=text]').on("paste",function(e) {
            alert(e);
        e.preventDefault();

        });
	        
       $("#btnView").click(function (e){

            e.preventDefault();
            if ( $("#fhasta").val()=='' || $("#fdesde").val()==''){
                alert('Especifique Fecha de la Planificación');
                return;
            }
            $.ajax({
                url: 'viewPlanificacionArea',
                type: 'GET',
                data:  {
                    "fdesde" : $("#fdesde").val(),
                    "fhasta" : $("#fhasta").val(),
                    "anio" : $("#anio").val()
                },
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#tablePlani").html(data);
                    $('#div_carga').hide(); 
                    verAcumuladas();
                },
            });
        });

       function verAcumuladas(){
           
            $.ajax({
                url: 'verHorasAcumuladasParticipante',
                type: 'GET',
                data:  {
                    "fdesde" : $("#fdesde").val(),
                    "fhasta" : $("#fhasta").val() 
                },
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#participantes").html(data);
                    $('#div_carga').hide(); 
                },
            });
        }



        $('#fdesde').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });      
        $('#fhasta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });              
      
        $("#frmproyecto").on('submit',function(e){
            /*if(validarSemana()){
                alert('Solo se puede actualizar semanas posteriores');
                return false;
            } */           
           /* if(!validarNF()){
                alert('Existen filas NF sin asignar Tipo de NF');
                return false;
            }*/
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarPlanificacionArea',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     //$("#resultado").html(data);
                     $("#div_msg").show();
                     $("#btnView").click();
                     
                },
                error: function(data){
                    
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });    
           


        function addTareas(){
            //alert($("input[name='pry']:checked").val());
            $("#fdesde_h").val($("#fdesde").val());
            $("#fhasta_h").val($("#fhasta").val());
           $.ajax({

                type:"POST",
                url:'insertarTareaProyecto',
                data:$("#frmTareas").serialize(),
                beforeSend: function () {
                    $('#div_carga').show(); 
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     $("#agregarTareas").modal('hide');
                     //$("#tablepry_"+ cproy +" tbody").append(data);
                     $("#row_"+idtr).after(data);
                },
                error:function(){
                    $('#div_carga').hide(); 
                }
            });              
        }


        function agregarUsuario(act,pry,idrow){
            /*if(validarSemana()){
                alert('Solo se puede actualizar semanas posteriores');
                return false;
            }     */       
            if($("#fdesde").val()=='' || $("#fhasta").val()==''){
                alert('Ingrese las fechas de Planificación y de click en visualizar');
                return ;
            }
            cproy = pry;
            $('#cproyecto_h').val(pry);
            idtr = idrow;
            var selec = recorrer(cproy);
            
            $.ajax({
                type:"POST",
                url:'asignaPlaPersonal',
                data:{
                     "_token": "{{ csrf_token() }}",
                    act_proy : act,
                    fdesde: $("#fdesde").val(),
                    fhasta: $("#fhasta").val(),
                    cproyecto: pry,
                    nroFila: nroFila,
                    selec : selec
                },
                beforeSend: function () {

                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     $("#deta").html(data);
                     $("#agregarTareas").modal('show');

                     
                },
                error: function(data){
                    $('#div_carga').hide();
 
                }         
            });
            
        }
        function eliminarFila(idFila) {
            /*if(validarSemana()){
                alert('Solo se puede actualizar semanas posteriores');
                return false;
            }*/
          $("#row_"+idFila).css("display","none");
          $("input[name='plani["+idFila+"][9]']").val("S");
        }  
        function clonarfila(idFila){
            /*if(validarSemana()){
                alert('Solo se puede actualizar semanas posteriores');
                return false;
            }*/
            act_fila_proy=$("input[name='plani["+idFila+"][0]']").val();
            act_fila=$("input[name='plani["+idFila+"][6]']").val();
            tipo_fila=$("input[name='plani["+idFila+"][7]']").val();
            cpersona_s=$("input[name='plani["+idFila+"][2]']").val();
            idtr = idFila;
           $.ajax({

                type:"POST",
                url:'insertarTareaClo',
                data:{
                     "_token": "{{ csrf_token() }}",
                    act_proy : act_fila_proy,
                    act_act : act_fila,
                    fdesde: $("#fdesde").val(),
                    fhasta: $("#fhasta").val(),
                    tipo: tipo_fila ,
                    nroFila: nroFila,
                    cpersona: cpersona_s,
                },
                beforeSend: function () {

                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 

                     $("#addTareasAdm").modal('hide');
                     $("#row_"+idtr).after(data);

                     
                }
            });                
        }  

        function viewObs(fila,key){
          
          obj = $("#obs_"+fila+"_"+key);
          document.getElementById('message-text').value=obj.val();
          i_col=key;
          i_fila=fila;
          $('#modalObs').modal('show');
          
        }
        function saveObs(){
            obj =  $("#obs_"+i_fila+"_"+i_col);
            obj.val(document.getElementById('message-text').value);
            $('#modalObs').modal('hide');
        }  
        function viewSobre(id,obj){
            if($(obj).val()!=''){
            if($(obj).val() > 16){
              $(obj).val(16);
            }
          }
          cad = id;
          if($(obj).val()==''){
            $("#"+cad).css("display","none");
          }else{
            $("#"+cad).css("display","block");
          }

        }                           

        $('.select-box').chosen(
        {
            allow_single_deselect: true
        });
        function recorrer(cproy){
            var aSelec = [];
            $("#tablepry_"+cproy+" tbody tr").each(function (index) 
            {
                var id= $(this).attr("id").substring(4);
                obj_cpry = $( "input[name='plani["+id+"][0]']");
                obj_cper = $( "input[name='plani["+id+"][2]']");
                obj_tipo = $( "input[name='plani["+id+"][7]']");
                if(obj_cper.val().length > 0){
                    aSelec.push([obj_cpry.val() , obj_cper.val(),obj_tipo.val() ]);
                }
            });   
            return aSelec;         
        }
        function soloNumeros(e,obj) {

            // Backspace = 8, Enter = 13, ’0′ = 48, ’9′ = 57, ‘.’ = 46
            
            var field = obj;

            key = e.keyCode ? e.keyCode : e.which;
            
            if (key == 8 || key ==9 || key ==11) return true;
            if (key > 47 && key < 58) {
              if (field.value === "") return true;
              var existePto = (/[.]/).test(field.value);
              
              if (existePto === false){
                  regexp = /.[0-9]{2}$/; //PARTE ENTERA 10
              }
              else {
                regexp = /.[0-9]{2}$/; //PARTE DECIMAL2
              }
              return !(regexp.test(field.value));
            }
            if (key == 46) {
              if (field.value === "") return false;
              regexp = /^[0-9]+$/;
              return regexp.test(field.value);
            }

            return false;

        }      
        function validarNF(){
            res= true;
            $("select").each(function () {
                if($.trim($(this).val()) ='' ){
                    id=$.trim($(this).attr("id"));
                    idx = id.substring(5);
                    obj = $("[name='plani["+ idx + "][7]']");
                    if(obj.val()=='2'){
                        return false;
                    }
                    alert(idx + " "+ $.trim($(this).attr("value")) + " " + obj.val() );
                }

            });
            return res;
        }


        function validarSemana(){
            res = false;
            sem_ini = $("#semana_ini").val();
            sem_fin = $("#semana_fin").val();
            sem_act = $("#semana_act").val();
            
            if(sem_ini!='' && sem_fin!='' && sem_act!=''){
                if( sem_act >= sem_ini){
                    res = true;
                }
            }
            return res;
        }          

        function goEditar(cpersona,fecha){              
            $.ajax({

                url:'verReporteHorasParticipante',
                type: 'POST',
                data : {
                    '_token':'{{ csrf_token() }}',
                    'cpersona' : cpersona,
                    'fecha': fecha,
                },
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){                  
                    $('#div_carga').hide(); 
                    $("#horas").html(data);
                    $("#verHT").modal("show");
                    
                },            
            });
        }
</script>       
