<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Proyecto -
      <small>Creacion y Edicion de Actividades </small>
    </h1>
    <ol class="breadcrumb">
      <li>@include('accesos.accesoProyecto')</li>
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Proyecto</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-2 col-xs-12">
      </div>
      <div class="col-lg-10 col-xs-12">
      </div>            
    </div>


    <div class="row">
      <!--<div class="col-lg-1 hidden-xs"></div>-->
      <div class="col-md-12 col-xs-12">

        <div class="row">
          <div class="col-lg-2 col-xs-2 clsPadding">Cliente</div>
          <div class="col-lg-9 col-xs-9 clsPadding">
            {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
            {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
          </div>
          <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>


          <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera</div>
          <div class="col-lg-4 col-xs-4 clsPadding">
            {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad minera','disabled' => 'true','id'=>'umineranombre') ) !!}
          </div> 
          <div class="col-lg-1 col-xs-1 clsPadding">Código proyecto</div>
          <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
          @if(isset($proyecto))
          <div class="col-lg-1 col-xs-1 clsPadding">
            <button class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModalnewactividad"><b>Nuevo</b>
            </button>
          </div>
          @endif
        </div>    
        <div class="row clsPadding">
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;">

          </div>
        </div>


        <div class="row clsPadding2">
          <div class="col-lg-2 col-xs-2 clsPadding">Nombre del proyecto</div>
          <div class="col-lg-9 col-xs-9 clsPadding">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
            ,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de proyecto')) !!}   
          </div>   
        </div> 
        <div class="row clsPadding2">
          <div class="col-lg-2 col-xs-2 clsPadding">
            Fecha inicio
          </div>
          <div class="col-lg-4 col-xs-4 clsPadding">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right', 'disabled'=>'true')) !!}
            </div> 
          </div> 
          <div class="col-lg-1 col-xs-1 clsPadding">Fecha cierre</div>
          <div class="col-lg-4 col-xs-4 clsPadding">
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>

              </div>
              {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}
            </div>

          </div>
        </div>
        <div class="row clsPadding2">

          <div class="col-lg-2 col-xs-2 clsPadding">Estado</div>
          <div class="col-lg-4 col-xs-4 clsPadding">
            {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!}  
          </div>
          <div class="col-lg-1 col-xs-1 clsPadding">Subproyecto</div>
          <div class="col-lg-4 col-xs-4 clsPadding">
            {!! Form::select('subproyecto',(isset($subproyecto)==1?$subproyecto:array()),null,array('class' => 'form-control','id'=>'subproyecto','disabled'=>'true')) !!} 
          </div>    
        </div>

        <div class="row clsPadding">
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>
          <div class="col-lg-12 col-xs-12 table-responsive" id="divActi">
            
            <table class="table-striped table-bordered table-hover" id="tableActividad" style="font-size:12px; width:100%;">
                <thead>
                  <tr class="clsCabereraTabla" style="height: 31px">
                  @if(empty($listaAct))
                    <th class="text-left" style="width: 25%;">Item</th>
                    <th class="text-left">Descripción de actividad</th>
                    <th class="text-center">Estado</th>
                    <th class="clsCabereraTabla">Acciones</th>
                  @endif
                  @if(!empty($listaAct))
                    <th class="text-left" style="width: 25%;">Item</th>
                    <th class="text-left">Descripción de actividad</th>
                    <th class="text-center">Estado</th>        
                    <th class="clsCabereraTabla">Acciones</th>
                  @endif 
                  </tr>
                </thead>

                @if(isset($listaAct))
                <tbody style="overflow:scroll">
                <?php $i=0; ?>
                @foreach($listaAct as $act)
                  <?php $i++; ?>
                  <tr
                  @if(!empty($act['cactividad_parent']))
                    class=""
                  @endif
                  > 
                    <td >{{ $act['item'] }}</td>
                    <td  @if(!empty($act['cactividad_parent']))@endif>{{ $act['descripcionactividad']}}</td>
                    <td class="text-center">
                      <center>
                       @if($act['existoenhabilitados'] >=1)
                        @if($act['estadohabilitado'] >=1)
                        <div class="text-center">
                          <span style="margin: 0 auto;" class="text-center badge bg-green">Con profesionales habilitados</span>    
                        </div>
                        @else
                          <span class="badge bg-red">Sin profesionales habilitados</span>
                        @endif
                      @else
                      @endif 
                      </center>
                      
                    </td>
                    <td class="text-center">
                      @if($act['existoenhabilitados'] >=1)
                        @if($act['estadohabilitado'] >=1)
                          <a onclick="estadotproyectoactividad('{{ $act['cproyectoactividades'] }}')" title="Inactivar"><span class="glyphicon glyphicon-remove-circle" style="font-size: 20px; color:red"></span></a>
                        @else
                          <a onclick="estadotproyectoactividad('{{ $act['cproyectoactividades'] }}')" title="Activar"><span class="glyphicon glyphicon-ok-circle" style="font-size: 20px; color: green"></span></a>
                        @endif
                      @else
                      @endif
                      <a type="" title="Editar" data-toggle="modal" data-target="#editaractividad_{{ $act['cproyectoactividades'] }}"><span class="glyphicon glyphicon-edit" style="font-size: 20px; color: blue"></span></a>

                      <!-- Modal -->
                      <div class="modal fade" id="editaractividad_{{ $act['cproyectoactividades'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                          <div class="modal-content">
                            <div class="modal-header" style="background-color: #0069AA;color: white;">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h4 class="modal-title" id="myModalLabel">Editar Actividad</h4>
                            </div>
                            <div class="modal-body ">
                              {!! Form::open(array('url' => 'ItemActividadesProyecto','method' => 'POST','id' =>'editoactividad_'.$act['cproyectoactividades'] , 'class' => 'form-group')) !!}
                                <input type="hidden" name="cproyectoactividades" value="{{ $act['cproyectoactividades'] }}">
                                <input type="hidden" name="cproyecto" value="{{ $act['cproyecto'] }}">
                                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">  
                                    <label for="item">Item</label>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                                    <input style="width:100%;" type="text" class="form-control" name="item" id="item" value="{{ $act['codigoactividad'] }}">
                                  </div>  
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                                    <label for="decripcionactividad">Descripcion Actividad</label>
                                  </div>
                                  <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12"> 
                                    <input style="width:100%;" type="text" class="form-control" name="decripcionactividad" id="decripcionactividad" value="{{ $act['descripcionactividad'] }}">
                                  </div>  
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center" style="margin-top: 10px;">
                                  <button id="envioedicion" type="submit" class="btn btn-primary" onClick="divFunction({{ $act['cproyectoactividades'] }});" > 
                                    Editar
                                  </button>
                                </div>
                              {!! Form::close() !!}
                            </div>
                            <div class="modal-footer">
                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      @if($act['existoenhabilitados'] >=1)
                        <a type="" title="Ver Equipo" data-toggle="modal" data-target="#equipoactividad_{{ $act['cproyectoactividades'] }}"><span class="glyphicon glyphicon-eye-open" style="font-size: 20px; color: black"></span></a>
                        <div class="modal fade" id="equipoactividad_{{ $act['cproyectoactividades'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                          <div class="modal-dialog" role="document">
                            <div class="modal-content">
                              <div class="modal-header" style="background-color: #0069AA;color: white;">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title" id="myModalLabel">Ver Equipo de trabajo</h4>
                              </div>
                              <div class="modal-body ">
                                
                                  <table class="table table-striped table-hover table-bordered table-responsive" id="listadopersona" style="font-size: 12px;">
                                    <caption>Equipo de Trabajo asignado</caption>
                                    <thead>
                                      <tr>
                                        <th class="clsCabereraTabla">Nombre</th>
                                        <th class="clsCabereraTabla">Area</th>
                                        <th class="clsCabereraTabla">Habilitado</th>
                                      </tr>
                                    </thead>
                                    <tbody style="font-size: 12px;">
                                      
                                        @foreach($act['equipotrabajo'] as $equipohabilitado)
                                        <tr>
                                          <td>{{ $equipohabilitado->nombreemple  }}</td>
                                          <td>{{ $equipohabilitado->areaemple }}</td>
                                          @if($equipohabilitado->activo)
                                            <td><span class="badge bg-green">Habilitado</span></td>
                                          @else
                                            <td><span style="margin: 0 auto;" class="text-center badge bg-red">No habilitado</span></td>
                                          @endif
                                        </tr>
                                        @endforeach
                                      
                                    </tbody>
                                  </table>
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>
                          </div>
                        </div>
                      @endif  
                    </td>
                  </tr>
                @endforeach
                </tbody>
                @endif
                <tfoot>
                  <tr class="clsCabereraTabla" style="height: 31px">

                  
                  @if(empty($listaAct))
                    <th class="col-sm-2 text-left" >Item</th>
                    <th class="col-sm-6 text-left" >Descripción de actividad</th>
                    <th class="text-center">Estado</th>
                    <th class="clsCabereraTabla">Acciones</th>
                  @endif
                  @if(!empty($listaAct))
                    <th class="col-sm-2 text-left">Item</th>
                    <th class="col-sm-6 text-left">Descripción de actividad</th>
                    <th class="text-center">Estado</th>        
                    <th class="clsCabereraTabla">Acciones</th>
                  @endif 

                  </tr>
                </tfoot>
            </table>
          </div>
        



        <div class="row clsPadding">
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>
    {{-- <div class="row clsPadding2">
          <div class="col-lg-3 col-xs-3">
              <button class="btn btn-primary btn-block" style="width:200px;" id='btnSave'><b>Grabar</b></button> 
          </div>
        </div> --}}
        <!--<div class="col-lg-1 hidden-xs"></div>-->

      </div>

    </section>

    <!-- Modal Buscar Proyecto-->
    @include('partials.searchProyecto')
    @if(isset($proyecto))
    <!-- Modal para crear nueva actividad -->
    <div class="modal fade" id="myModalnewactividad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA;color: white;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Creacion de nueva actividad</h4>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => 'grabarActividadesProyecto','method' => 'POST','id' =>'frmActividadProyecto')) !!}
            {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
            {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="checkbox">
                <label>
                  <input id="idhijoparasacarpadre" type="checkbox" onclick="toggle('.asoactividad', this)"> Asociar Actividad
                </label>
              </div>
            </div>
            <div class="form-group asoactividad" id="asociaactividad" style="display:none;">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                  <label for="padreactividad">Asociar actividad</label>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">  
                  <select class="select-box" id="padreactividad" name="padreactividad">
                    <option data-hijo="" value="">Seleccione una opcion</option>

                    @foreach($actividadPadreTotal as $key => $padreact)
                      @if($padreact->habilitadoestado == 0)
                        <option data-hijo="{{ $padreact->nombrehijonuevo }}" value="{{ $padreact->cproyectoactividades }}">{{ $padreact->codigoactvidad }}&nbsp;{{ $padreact->descripcionactividad }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>   
                <p class="help-block">Seleccione una actividad a la cual asociar una nueva actividad</p>
              </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="itemactividadcreate">Item</label>
                <input type="text" class="form-control" name="itemactividadcreate" id="itemactividadcreate" placeholder="1.00" minlength="4" required pattern="^[0-9,.]*$" title="Por favor coloque un formato de titulo correcto">
                <p class="help-block">Los nombres de las actividades deben ser por ej. 1.00 </p>
              </div>
              <div class="form-group">
                <label for="nombreactividad">Nombre de la actividad</label>
                <input type="text" class="form-control" id="nombreactividad" name="nombreactividad" placeholder="Nombre Activiadd" required>
              </div>
              
              
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 text-center">
                <button type="submit" class="btn btn-primary">Crear</button> 
            </div>
            {!! Form::close() !!}
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
          </div>
          
        </div>
      </div>
    </div>
    @endif
    <script>
      $('.select-box').chosen({
        allow_single_deselect: true,
        width: '100%',
      });

      var selected =[];
      var selected_actividad =[];
      var personal_rate =<?php echo (isset($personal_rate)==1?json_encode($personal_rate):'[]'); ?>

      $.fn.dataTable.ext.errMode = 'throw';
      var table=$("#tProyecto").DataTable({
        "processing":true,
        "serverSide": true,

        "ajax": {
          "url": "listarProyLider",
          "type": "GET",
        },
        "columns":[
        {data : 'cproyecto', name: 'tproyecto.cproyecto'},
        {data : 'codigo', name: 'tproyecto.codigo'},
        {data : 'GteProyecto' , name : 'p.abreviatura'},
        {data : 'cliente' , name : 'tper.nombre'},
        {data : 'uminera' , name : 'tu.nombre'}, 
        {data : 'nombre' , name : 'tproyecto.nombre'},
        {data : 'descripcion' , name : 'te.descripcion'}

        ],
        "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Sin Resultados",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No existe registros disponibles",
          "infoFiltered": "(filtrado de un _MAX_ total de registros)",
          "search":         "Buscar:",
          "processing":     "Procesando...",
          "paginate": {
            "first":      "Inicio",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
          },
          "loadingRecords": "Cargando..."
        }
      });

      $('#tProyecto tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
          selected.push( id );

        } 

        $(this).toggleClass('selected');
      });     
      $('#searchProyecto').on('hidden.bs.modal', function () {
       goEditar();
     });

      function goEditar(){
        var id =0;
        if (selected.length > 0 ){
          id = selected[0];
          getUrl('editarActividadesProyecto/'+id.substring(4),'');
        }else{
          $('.alert').show();
        }
      }


      /* Grabar frmDisciplina*/
      $('#frmActividadProyecto').on('submit',function(e){

            /*$.ajaxSetup({
                header: document.getElementById('_token').value
              });*/
              e.preventDefault(e);
              $('input+span>strong').text('');
              $('input').parent().parent().removeClass('has-error');            
              $.ajax({

                type:"POST",
                url:'grabarActividadesProyecto',  
                data:$(this).serialize(),
                /*dataType: 'json',*/

                beforeSend: function () {
                  $('#div_carga').show(); 
                },

                success: function(data){
                 //$(".alert-success").prop("hidden", false);
                 $('#div_carga').hide();  
                 $('.modal-backdrop').hide();
                 $("#resultado").html(data);
                 //$("#div_msg").show();
               },
               error: function(data){
                $('#div_carga').hide();
                $('#detalle_error').html(data);
                $("#div_msg_error").show();

              }
            });
      });        
      /* Fin Grabar FrmDisciplina*/
      $('#envioedicion').click(function(){
          var url_actual = location.href;
        alert(url_actual);
      });

      function divFunction($idcactividad){
        //Some code
          var url_actual = location.href;
          //alert(url_actual);
          var codigoform = $("#cproyectoactividades").val();
          //alert($idcactividad);
        $('#editoactividad_'+$idcactividad).on('submit',function(e){

            /*$.ajaxSetup({
                header: document.getElementById('_token').value
              });*/
              e.preventDefault(e);
              $('input+span>strong').text('');
              $('input').parent().parent().removeClass('has-error');            
              $.ajax({

                type:"POST",
                url:'ItemActividadesProyecto',  
                data:$(this).serialize(),
                /*dataType: 'json',*/

                beforeSend: function () {
                  $('#div_carga').show(); 
                },

                success: function(data){
                 //$(".alert-success").prop("hidden", false);
                 $('#div_carga').hide();  
                 $('.modal-backdrop').hide();
                 $("#resultado").html(data);
                 //$("#div_msg").show();
               },
               error: function(data){
                $('#div_carga').hide();
                $('#detalle_error').html(data);
                $("#div_msg_error").show();

              }
            });
        });     
      };
      /* Editar frmActividad*/
      $('.envioeditoactiidad').on('submit',function(e){
        
            //alert('paso por la funcion');
            /*$.ajaxSetup({
                header: document.getElementById('_token').value
              });*/
              
      });        
      /* Fin Grabar FrmDisciplina*/
      function verparticipantes(id){
        $.ajaxSetup({
          header: document.getElementById('_token').value
        });          

        $.ajax({
          type: 'GET',
          url : 'viewActividadesParticipante',
          data: "cproyecto="+$("#cproyecto").val()+"&cproyectopersona="+id,
          beforeSend : function(){
            $('#div_carga').show();
          },
          success : function (data){
            $('#div_carga').hide(); 
            $("#divConteHoras").html(data);
            $("#divEditarHoras").modal();
          },
          error: function(data){
            $('#div_carga').hide();
          }
        });

      } 

      $('#finicio').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        firstDay: 1,
        calendarWeeks:true,
        /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
      });   

      $('#fcierre').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        firstDay: 1,
        calendarWeeks:true,
        /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
      }); 

      var table = $('#tableActividad').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        lengthChange: false,
        buttons: [ 'excel', 'pdf', 'colvis' ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   {
          leftColumns: 2
        }
      } ); 

      table.buttons().container()
      .appendTo( '#tableActividad_wrapper .col-sm-6:eq(0)' );

  function toggle(className, obj) {
      var $input = $(obj);
      if ($input.prop('checked')){
        $(className).css('display','block');
        $('#itemactividadcreate').prop('readonly','true');

      }else{
        $(className).css('display','none');
        //$('#padreactividad').empty();
        $('#itemactividadcreate').prop('readonly',null);
      }     
  }    

  $("#padreactividad").change(function(){
    //console.log($(this).data("hijo"));
    //alert("The text has been changed.");
    var hijo = $('#padreactividad').find('option[value="'+$(this).val()+'"]').data('hijo');
    $('#itemactividadcreate').val(hijo);

      console.log(hijo);
  });

  //chosen actualizan y limpia chosen
  $('#idhijoparasacarpadre').click(function(){
    $('#itemactividadcreate').prop('readonly',null);
    $('#itemactividadcreate').val('');
    $('#padreactividad').val(''); 
    $(".select-box").val('').trigger("chosen:updated");   
       if ($('#idhijoparasacarpadre:checked').val()=='on') {
           $('#itemactividadcreate').prop('readonly','true');
           
       }
   })

  //activar desactivar 
  function estadotproyectoactividad(id){

    $.ajax({
      url: 'estadotproyectoactividad/'+id,
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      // data: {param1: 'value1'},
    })
    .done(function(data) {
      console.log("estadotproyectoactividad",data);
      $("#resultado").html(data);
      swal("Se realizo la acción", "correctamente","success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }

</script>
   

