<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style>
		/** Establezca los márgenes de la página en 0, de modo que el pie de página y el encabezado Puede ser de la altura y anchura total. !**/
		@page {
			margin: 0.3cm 1cm;
		}
		/** Define ahora los márgenes reales de cada página en el PDF. **/
		body {
			margin-top: 3.05cm;
			margin-left: 0cm;
			margin-right: 0cm;
			margin-bottom: 0.5cm;
		}
		/** Definir las reglas de encabezado. **/
		header {
			position: fixed;
			top: 1.3cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
		}

	</style>
</head>
	<body>
		<header>
			<div class="row">
				<table style="border-collapse: collapse; size: 0.5px;font-size: 10px;width: 100%;border-bottom: none;font-family: Arial,Helvetica,sans-serif;">
					<tr>
						<td  rowspan="1" style="border:1px solid #000000FF; width: 20%;text-align: center;"><img style="width: 150px;" src="images/logorpt.jpg"></td>
						<td rowspan="2" style="border:1px solid #000000FF; width: 60%;text-align: center;font-size: 11px"><strong>GERENCIA DE PROYECTOS <br> Lecciones Aprendidas </strong></td>
						<td rowspan="1"  style="border:1px solid #000000FF; width: 20%;font-size: 9px">
							<table style="width: 100%;">
								<tr>
									<td style="text-align: center;"><strong><?php echo (isset($proyecto)==1?$proyecto->codigo:'');?>-AND-25-LA-001</strong></td>
								</tr>
								<tr>
									<td>Revisión
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										:
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										0</td>
								</tr>
								<tr>
									<td>Fecha
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										:
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										dd-mm-aa</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> SIG AND </td>
						<td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> 10-AND-25-FOR-0503 / R0 / <?php echo date('d-m-y');?> </td>
					</tr>
				</table>
			</div>
		</header>
		<section class="content" style="font-family: Arial,Helvetica,sans-serif;">
			<div class="row">

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 13%;"">Nombre del Proyecto</td>
						<td style="border:1px solid #000000FF;width: 67%;"><?php echo (isset($proyecto)==1?$proyecto->nombre:'');?></td>
						<td style="border:1px solid #000000FF; width: 10%;">Proyecto N°</td>
						<td style="border:1px solid #000000FF; width: 10%;"><?php echo (isset($proyecto)==1?$proyecto->codigo:'');?></td>
					</tr>
					<tr>
						<td style="border:1px solid #000000FF; width: 13%;">Cliente</td>
						<td style="border:1px solid #000000FF; width: 67%;"><?php echo (isset($persona)==1?$persona->nombre:'');?></td>
						<td style="border:1px solid #000000FF; width: 10%;">Unidad Minera</td>
						<td style="border:1px solid #000000FF; width: 10%;"><?php echo (isset($uminera)==1?$uminera->nombre:'');?></td>
					</tr>
				</table>
			</div>
			<div class="row">
				<table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF; width: 13%;">Ubicación</td>
						<td style="border:1px solid #000000FF; width: 87%;">{{ $ubicacion }}</td>
					</tr>
				</table>
			</div>
			<div class="row">
				<table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 13%;">Gerente del Proyecto</td>

						<td style="border:1px solid #000000FF;width: 87%;"><?php echo (isset($proyecto)==1?$proyecto->gerente:'');?></td>


					</tr>
				</table>
			</div>
			<div class="row">
				<table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 100%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Etapas del Proyecto</strong>
						</td>
					</tr>
				</table>
			</div>
			<div class="row">
				<table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 14%;">
							<b>Inicio: </b>
							<br>
							Se define o autoriza el proyecto, incluye la etapa de prouesta y la puesta en marcha del mismo
						</td>
						<td style="border:1px solid #000000FF;width: 14%;">
							<b>Ejecución: </b>
							<br>
							Integra a personas y otros recursos para llevar a cabo el plan de Gestión del proyecto.
						</td>
						<td style="border:1px solid #000000FF;width: 14%;">
							<b>Cierre: </b>
							<br>
							Formaliza la aceptación del producto, servicio o resultado.
						</td>
					</tr>
					<tr>
						<td style="border:1px solid #000000FF;width: 14%;">
							<b>Planificación: </b>
							<br>
							Define los objetivos, y planifica el curso de acción requerido para lograr los obetivos y el alcance del proyecto.
						</td>
						<td style="border:1px solid #000000FF;width: 14%;">
							<b>Monitoreo y Control: </b>
							<br>
							Mide y supervisa regularmente el avance, a  fin de identificar las vairiaciones respecto del plan de gestión de proyectos."
						</td>
						<td style="border:1px solid #000000FF;width: 14%;">
							<b>					</b>
							<br>

						</td>
					</tr>
				</table>
			</div>
			<div class="row">
				<table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
					<thead>
					<tr style="color: #fff;background-color:rgba(0, 105, 170, 255);text-align: center;">
						<td style="border:1px solid #000000FF;height:20px; width: 3%;">Item</td>
						<td style="border:1px solid #000000FF;height:20px; width: 35%;">Descripción</td>
						<td style="border:1px solid #000000FF;height:20px; width: 8%;">Fecha</td>
						<td style="border:1px solid #000000FF;height:20px; width: 14%;">Area</td>
						<td style="border:1px solid #000000FF;height:20px; width: 10%;">Proyecto N°</td>
						<td style="border:1px solid #000000FF;height:20px; width: 14%;">Etapa de proyecto</td>
						<td style="border:1px solid #000000FF;height:20px; width: 25%;">¿Qué sucedió?</td>
						<td style="border:1px solid #000000FF;height:20px; width: 25%;">¿Qué se hizo?</td>
						<td style="border:1px solid #000000FF;height:20px; width: 25%;">¿Qué se debió hacer?</td>
						<td style="border:1px solid #000000FF;height:20px; width: 14%;">Elaborado</td>
					</tr>
					</thead>
					<tbody>
					@if(isset($lecaprend)==1)
						@foreach($lecaprend as $la)
							<tr>
								<td style="border:1px solid #000000FF;text-align: center;width: 3%;">{{ $la['item'] }}</td>
								<td style="border:1px solid #000000FF;width: 35%;">{{ $la['descripcion'] }}</td>
								<td style="border:1px solid #000000FF;text-align: center;width: 8%;">{{ $la['fecha'] }}</td>
								<td style="border:1px solid #000000FF;width: 14%;">{{ $la['area'] }}</td>
								<td style="border:1px solid #000000FF;text-align: center;width: 10%;">{{ $proyecto->codigo }}</td>
								<td style="border:1px solid #000000FF;width: 14%;">{{ $la['fase'] }}</td>
								<td style="border:1px solid #000000FF;width: 25%;">{{ $la['suceso'] }}</td>
								<td style="border:1px solid #000000FF;width: 25%;">{{ $la['hizo'] }}</td>
								<td style="border:1px solid #000000FF;width: 25%;">{{ $la['debiohacer'] }}</td>
								<td style="border:1px solid #000000FF;width: 14%;">{{ $la['elaborado'] }}</td>
							</tr>
						@endforeach
					@endif
					</tbody>
				</table>
			</div>
		</section>
	</body>
</html>
