
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style>
		/** Establezca los márgenes de la página en 0, de modo que el pie de página y el encabezado Puede ser de la altura y anchura total. !**/
		@page {
			margin: 1cm 2cm;
		}
		/** Define ahora los márgenes reales de cada página en el PDF. **/
		body {
			margin-top: 3.05cm;
			margin-left: 0cm;
			margin-right: 0cm;
			margin-bottom: 0.5cm;
		}
		/** Definir las reglas de encabezado. **/
		header {
			position: fixed;
			top:  1.2cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
		}
		/** Definir las reglas del pie de página. **/
		footer {
			position: fixed;
			bottom: 0cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
		}
	</style>
</head>
	<body>
		<header>
			<div class="row">
				<table style="border-collapse: collapse; size: 0.5px;font-size: 11px;width: 100%">
					<tr>
						<td rowspan="1" style="border:1px solid #000000FF; width: 30%;text-align: center;height:5%"><img style="width: 150px;" src="images/logorpt.jpg"></td>
						<td rowspan="2" style="border:1px solid #000000FF; width: 40%;text-align: center;font-size: 11px;"><strong><strong>GERENCIA DE PROYECTOS<br>Estructura de Desglose de Trabajo - EDT</strong></td>
						<td rowspan="1"  style="border:1px solid #000000FF; width: 30%;font-size: 9px">
							<table width="100%">
								<tr  style="text-align: center;">
									<td colspan="3"><?php echo (isset($proyecto[0])==1?$proyecto[0]['codproyecto']:'');?>-AND-25-EDT-001</td>

								</tr>

								<tr>
									<td width="20%">Revisión</td>
									<td width="5%">:</td>
									<td width="75%"><?php echo (isset($proyecto[0])==1?$proyecto[0]['documento_rev']->revision:'');?></td>

								</tr>
								<tr>
									<td width="20%">Fecha</td>
									<td width="5%">:</td>
									<td width="75%"><?php echo (isset($proyecto[0])==1?$proyecto[0]['documento_rev']->fecharev:'');?></td>

								</tr>
							</table>

						</td>
					</tr>
					<tr>

						<td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> SIG AND </td>
						<td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> 10-AND-25-FOR-0206 / R0 / 26-03-18 </td>
					</tr>
				</table>
			</div>
		</header>
		<section class="content"  style="font-family: Arial,Helvetica,sans-serif;">
			<div class="row">

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 10%;""> <strong>Proyecto</strong></td>
						<td colspan="3" style="border:1px solid #000000FF;width: 60%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['nombreproyecto']:'');?></td>
						<td style="border:1px solid #000000FF; width: 13%;"><strong>Proyecto N°</strong></td>
						<td style="border:1px solid #000000FF; width: 17%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['codproyecto']:'');?></td>
					</tr>

					<tr>
						<td style="border:1px solid #000000FF;width: 10%;""><strong>Cliente</strong></td>
						<td colspan="3" style="border:1px solid #000000FF;width: 60%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['cliente']:'');?></td>
						<td style="border:1px solid #000000FF; width: 13%;"><strong>Unidad Minera</strong></td>
						<td  style="border:1px solid #000000FF; width: 17%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['uminera']:'');?></td>
					</tr>

				</table>
			</div>
			<div class="row">

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 10%;"><strong>GP</strong></td>
						<td style="border:1px solid #000000FF;width: 60%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['gerente']:'');?></td>

						<td style="border:1px solid #000000FF; width: 13%;"><strong>N° de Propuesta</strong></td>
						<td style="border:1px solid #000000FF; width: 17%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['propuesta']:'');?></td>
					</tr>


				</table>
			</div>
			<div class="row">

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">

					<tr>
						<td rowspan="5" style="border:1px solid #000000FF;width: 10%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Nivel 1</strong></p></td>

						<td rowspan="5" style="border:1px solid #000000FF;width: 10%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Nivel 2</strong></td>

						<td rowspan="5" style="border:1px solid #000000FF;width: 10%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Nivel 3</strong></td>

						<td rowspan="5" style="border:1px solid #000000FF; width: 70%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Nombre</strong></td>
					</tr>

				</table>
			</div>
			<div class="row">


				<?php if (isset($edt)==1) {  ?>

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">


					<?php $contador_gestion=0; ?>



					<?php foreach ($edt as $e) { ?>
					<?php

					$colorfondo = '';
					$colorletra = 'black';

					if ($e['nivel'] == 0) {$colorfondo='rgb(0,176,80)';$colorletra = 'white';}
					if ($e['nivel'] == 1) {$colorfondo='rgb(0,105,170)';$colorletra = 'white';}
					if ($e['nivel'] == 2) {$colorfondo='rgb(128,128,128)';$colorletra = 'black';}
					if ($e['nivel'] == 3) {$colorfondo='rgb(166,166,166)';$colorletra = 'black';}
					?>

					<?php   //dd($e['ctipoedt'],$e['nivel'],$contador_gestion) ?>


					<tr>
						<td style="border:1px solid #000000FF;width: 10%;text-align: center;color: {{$colorletra}};background-color:{{$colorfondo}}">

							@if($e['nivel'] == 1)
								<strong>{{$e['codigo']}}</strong>
							@endif

						</td>

						<td style="border:1px solid #000000FF;width: 10%;text-align: center;color: {{$colorletra}};background-color:{{$colorfondo}}">

							@if($e['nivel'] == 2)
								<strong>{{$e['codigo']}}</strong>
						@endif

						<td style="border:1px solid #000000FF;width: 10%;text-align: center;color: {{$colorletra}};background-color:{{$colorfondo}}">

							@if($e['nivel'] == 3)
								<strong>{{$e['codigo']}}</strong>
						@endif

						<td  style="border:1px solid #000000FF;width: 70%;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>{{$e['descripcion']}}</strong></td>
					</tr>


					@if($e['ctipoedt'] == '1' and $e['nivel'] == 0 and $contador_gestion == 0)

						<?php  $contador_gestion++?>


						<tr>
							<td style="border:1px solid #000000FF;width: 10%;text-align: center;background-color:#BFBFBF">

							</td>

							<td style="border:1px solid #000000FF;width: 10%;text-align: center;background-color:#BFBFBF">



							<td style="border:1px solid #000000FF;width: 10%;text-align: center;background-color:#BFBFBF">



							<td  style="border:1px solid #000000FF;width: 70%;background-color:#BFBFBF">Documentos de gestión</td>
						</tr>

					@endif

					<?php } ?>
				</table>

				<?php } ?>


			</div>
		</section>
	</body>
</html>
















</section>

</body>
</html>



