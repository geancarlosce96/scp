<div class="content-wrapper" style="font-size: 12px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Lecciones Aprendidas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    {!! Form::open(array('url' => 'grabarLeccionesAprendidas','method' => 'POST','id' =>'frmproyecto')) !!}
    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="box box-primary">
        <div class="row">
            <div class="row"><br></div>
            <div class="col-lg-1 col-xs-1">Cliente:</div>
            <div class="col-lg-3 col-xs-3">
                {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
                {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Unidad minera:</div>
            <div class="col-lg-3 col-xs-3">
                {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Estado:</div>
            <div class="col-lg-1 col-xs-1">
                {!! Form::select('estadoproyecto',(isset($testadoproyecto)==1?$testadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Buscar Proyecto:</div>
            <div class="col-lg-1 col-xs-1">
                <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'>
                    <b>...</b>
                </button>
            </div> 
        </div>
        <div class="row">
            <div class="row"><br></div>
            <div class="col-lg-1 col-xs-1">Código Proyecto:</div>
            <div class="col-lg-2 col-xs-2">
                {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}
            </div>
            <div class="col-lg-1 col-xs-1">Nombre del Proyecto:</div>
            <div class="col-lg-6 col-xs-6">
                {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}   
            </div>
            <div class="col-lg-1 col-xs-1">Agregar Lección:</div>
            <div class="col-lg-1 col-xs-1">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" onclick="verModal()" 
            @if(isset($bsave))
                @if($bsave==true)
                @else
                    disabled   
                @endif
            @else 
                disabled
            @endif
            onclick="clear()"
            >
                    <b>+</b>
                </button>
            </div>
        </div>

        <div class="row"><br></div>

        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>

        <div class="row"><br></div>

        <div class="row clsPadding2">   
            <div class="col-lg-12 col-xs-12 table-responsive" id="idLecApren">
                    @include('partials.modalLeccionesAprendidas',array('lecaprend'=> (isset($lecaprend)==1?$lecaprend:array() )))
            </div>
        </div>

        <div class="box-footer">
            <!--<button type="submit" class="btn btn-primary"
            @if(isset($bsave))
                @if($bsave==true)
                @else
                    disabled   
                @endif
            @else 
                disabled
            @endif
            >Grabar</button>-->
            <button type="button" class="btn btn-primary" id="btnPrint"
            @if(isset($bsave))
                @if($bsave==true)
                @else
                    disabled   
                @endif
            @else 
                disabled
            @endif
            >Imprimir</button>
        </div>
    </div>

{!! Form::close() !!}
</section>
<!-- /.content -->
<div id="modalLA">
@include('partials.addLeccionesAprendidas')
</div>

</div>

@include('partials.searchProyecto')


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

    var selected =[];

    $.fn.dataTable.ext.errMode = 'throw';
      var table=$("#tProyecto").DataTable({
        "processing":true,
        "serverSide": true,

        "ajax": {
          "url": "listaPryTodos",
          "type": "GET",
        },
        "columns":[
        {data : 'cproyecto', name: 'tproyecto.cproyecto'},
        {data : 'codigo', name: 'tproyecto.codigo'},
        {data : 'GteProyecto' , name : 'p.abreviatura'},
        {data : 'cliente' , name : 'tper.nombre'},
        {data : 'uminera' , name : 'tu.nombre'}, 
        {data : 'nombre' , name : 'tproyecto.nombre'},
        {data : 'descripcion' , name : 'te.descripcion'}

        ],
        "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Sin Resultados",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No existe registros disponibles",
          "infoFiltered": "(filtrado de un _MAX_ total de registros)",
          "search":         "Buscar",
          "processing":     "Procesando...",
          "paginate": {
            "first":      "Inicio",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
          },
          "loadingRecords": "Cargando..."
        }
      });

      $('#tProyecto tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
          selected.push( id );

        } 

        $(this).toggleClass('selected');
      });     
      $('#searchProyecto').on('hidden.bs.modal', function () {
       goEditar();
     });   
   

    function goEditar(){
      var id =0;
      if (selected.length > 0 ){
          id = selected[0];

          getUrl('editarLeccionesAprendidas/'+id.substring(4),'');
      }else{
          $('.alert').show();
      }
  }

  

  $('#finicio').datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true,
    firstDay: 1,
    calendarWeeks:true,
    /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
});   

  $('#fcierre').datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true,
    firstDay: 1,
    calendarWeeks:true,
    /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
});   

  $('#fechala').datepicker({
    format: "dd-mm-yy",
    language: "es",
    autoclose: true,
    firstDay: 1,
    calendarWeeks:true,
    /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
    endDate: "<?php echo date('d/m/Y'); ?>",
    todayHighlight: true,
}).datepicker("setDate", new Date());



  $('#frmproyecto').on('submit',function(e){
    $.ajaxSetup({
        header: document.getElementById('_token').value
    });
    e.preventDefault(e);

    $('input+span>strong').text('');
    $('input').parent().parent().removeClass('has-error');            

    $.ajax({

        type:"POST",
        url:'grabarLeccionesAprendidas',
        data:$(this).serialize(),
        /*dataType: 'json',*/
        beforeSend: function () {

            $('#div_carga').show(); 
            
        },
        success: function(data){

         $('#div_carga').hide(); 

         $("#resultado").html(data);
         $("#div_msg").show();
     },
     error: function(data){


        $('#div_carga').hide();
        $('#detalle_error').html(data);
        $("#div_msg_error").show();
    }
});
});  

  function verModal(){
    limpiar();

    $("#addLecApren").modal({
        backdrop:"static"
    });


  }

 

  //Boton Modificar
      function goEditarLA(id){
    // alert(id);
    $.ajax({

                url:'editLecAprend/'+id,
                type: 'GET',
              
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#modalLA").html(data);
                    $('#div_carga').hide(); 
                    $("#addLecApren").modal("show");
                    
                },            
            });
  }
function add(){

    if ($("#descripcion").val()=='') { alert('Por favor ingrese una descripcion'); return ; };
    if ($("#fechala").val()=='') { alert('Por favor seleccione una fecha'); return ; };
    if ($("#cfaseproyecto").val()=='') { alert('Por favor seleccione una fase del proyecto'); return ; };
    if ($("#suceso").val()=='') { alert('Por favor ingrese ¿Qué sucedió?'); return ; };
    if ($("#hizo").val()=='') { alert('Por favor ingrese ¿Qué se hizo?'); return ; };
    if ($("#debiohacer").val()=='') { alert('Por favor ingrese ¿Qué se debió hacer?'); return ; };

  // $("#idAddLeAp").click(function(e){ 
    // alert("Este");
     $.ajax({

        url:'addLeccAprenPy',
        type: 'POST',
        data : $("#frmlecApre").serialize(),
        beforeSend: function(){
            $('#div_carga').show(); 
        },
        success: function(data){
            $('.modal-backdrop').remove();
            $('#div_carga').hide(); 
            $('#resultado').html(data);
            $("#addLecApren").modal("hidden");
           

        },            
    });

 // });  
}

  function fdelLeAp(id){
      $.ajax({
        url:'delLecAprendPy/'+id,
        type: 'GET',
        beforeSend: function(){
            $('#div_carga').show(); 
        },                
        success: function (data){
          $('#div_carga').hide(); 
          $('#resultado').html(data);
      },
  });

  };    

  $('.select-box').chosen(
  {
    allow_single_deselect: true,
    width: "100%",
});

  var table = $('#tableLA').DataTable( {
    "paging":   true,
    "ordering": true,
    "info":     true,
    // lengthChange: false,
        buttons: [ 'excel' ],
        // scrollY:        "600px",
        // scrollX:        true,
        // scrollCollapse: false,
        // paging:         true,
        // fixedColumns:   {
        //     leftColumns: 1
        // }
    } ); 

    table.buttons().container()
        .appendTo( '#tableLA_wrapper .col-sm-6:eq(0)' );

  $("#btnPrint").on('click',function(){    

        goPrintLA();
      //goPrintHR();
    });   

    function goPrintLA(){

        var id =$('#cproyecto').val();
        if (id.length > 0 ){
             win = window.open('printLAProy/'+id,'_blank');
        }else{
            $('.alert').show();
        }
  }

  function limpiar(){
   
    $("#descripcion").val('');
    $("#fechala").val('');
    $("#cfaseproyecto").val('').trigger("chosen:updated");
    // $('#cfaseproyecto').trigger("chosen:updated");
    $("#suceso").val('');
    $("#hizo").val('');
    $("#debiohacer").val('');
    $("#cleccionaprendida").val('');
    
   
  }

</script>  