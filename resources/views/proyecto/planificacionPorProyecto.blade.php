<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Planificación de Actividades</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoProyecto')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>

        {!! Form::open(array('url' => 'grabarPlanificacionProyecto','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!}
    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
       <div class="row">
        <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
        <div class="col-lg-9 col-xs-9 clsPadding">{!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
          {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}</div>
        <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
        <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
        <div class="col-lg-4 col-xs-4 clsPadding"> {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}</div> 
        <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
        <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
       </div>
        <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
        <div class="col-lg-9 col-xs-9">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}   
        </div>   
    </div> 
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
        <div class="col-lg-4 col-xs-4"><div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right datepicker','disabled'=>'true','id'=>'finicio')) !!}</div> 
        </div> 
        <div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>
        <div class="col-lg-4 col-xs-4"><div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right datepicker','disabled'=>'true','id'=>'fcierre')) !!}</div> 
        </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
            Estado:
              {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!} 
        </div>
        <div class="col-lg-6 col-xs-6">
        &nbsp;
        </div> 
    </div>
    <div class="row clsPadding">
    	<div class="col-lg-12 col-xs-12 clsTitulo"> Planificación de Actividades</div>
    </div> 
          
    <div class="row clsPadding2"> 
         <div class="col-lg-3 col-xs-6">
            Área:
              {!! Form::select('disciplina',(isset($disciplinas)==1?$disciplinas:array()),'',array('class' => 'form-control select-box','id'=>'disciplina')) !!}
        </div> 
        <div class="col-lg-3 col-xs-6">
            <label class="clsTxtNormal">Fecha desde:</label>            
            <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fdesde','', array('class'=>'form-control pull-right datepicker','id' => 'fdesde')) !!}
            </div> 
        </div>
        <div class="col-lg-3 col-xs-6">
            <label class="clsTxtNormal">Fecha hasta:</label>            
            <div class="input-group date">
                <div class="input-group-addon">
    	            <i class="fa fa-calendar"></i>
                </div>
    	        {!! Form::text('fhasta','', array('class'=>'form-control pull-right datepicker','id' => 'fhasta')) !!}
            </div>        
        
        </div>  
        <div class="col-lg-3 col-xs-6" style="margin-top:25px;">
        	<button type="button" class="btn btn-primary btn-block " id="btnView"> <b>Visualizar</b></a>
        </div>   
    </div> 
  
    <div class="row clsPadding2">	
        <div class="col-lg-12 col-xs-12 table-responsive" id="tablePlani">
            
            @include('partials.tablePlanificacionProyecto',array('tablePlanificacionProyecto'=> (isset($tablePlanificacionProyecto)==1?$tablePlanificacionProyecto:array() )))
    	</div>
    </div>  
   
    <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">
            <button type="submit" class="btn btn-primary btn-block"><b>Grabar</b></a> 
        </div>
        <div class="col-lg-3 col-xs-3">
            <button type="button" class="btn btn-primary btn-block" disabled="true"><b>Cancelar</b></button> 
        </div>
        <div class="col-lg-3 col-xs-3">
            <button type="button" class="btn btn-primary btn-block" disabled="true"><b>Imprimir</b></button> 
        </div>                       
    </div>
        
    
    
        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 
</div>
    {!! Form::close() !!}
    </section>
    <!-- /.content -->
    @include('partials.searchProyecto')
</div>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        }); 

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarPlanificacionProyecto/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }
        $("#btnView").click(function (e){

            e.preventDefault();
            if (/*$("#disciplina").val()=='' ||*/ $("#fhasta").val()=='' || $("#fdesde").val()==''){
                alert('Especifique el Area, Fecha Desde, Fecha Hasta de la Planificación');
                return;
            }
            
            $.ajax({
                url: 'viewPlanificacionProyecto',
                type: 'GET',
                data: "cproyecto="+ $('#cproyecto').val() +"&cdisciplina="+$("#disciplina").val()+"&fhasta="+ $("#fhasta").val() + "&fdesde=" + $("#fdesde").val(),
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#tablePlani").html(data);
                    $('#div_carga').hide(); 
                },
            });
        });
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });   

        $("#frmproyecto").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarPlanificacionProyecto',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });                
</script>    
<script>
    $('.select-box').chosen(
        {
            allow_single_deselect: true
        });
</script>