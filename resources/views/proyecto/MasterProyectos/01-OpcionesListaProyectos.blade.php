@extends('layouts.master')
@section('master-proyecto')

    <div class="content-wrapper"  style="background: #fff !important;min-height: auto !important;">


        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Lista General de Proyectos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
                </ol>
            </section>
            <hr>

            <div class="col-md-12">
                <select id="proyectos" class="form-control select-box" data-placeholder="Elegir un proyecto...">
                    <option value="0" >Seleccione un proyecto</option>

                    @foreach($tproyectos as $proy)
                        <option value="{{ $proy->cproyecto }}" >{{ $proy->codigo }} - {{ $proy->nombre }}</option>
                    @endforeach
                </select>
            </div>
            <br><br><br><br>

            <div id="lista">
                @include('proyecto.MasterProyectos.02-ListaProyectos',array('arrayProyecto' => (isset($arrayProyecto)==1?$arrayProyecto:array())))
            </div>

            </div>

    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.select-box').chosen(
                {
                    allow_single_deselect: true,width:"100%"
                });

            $('#proyectos').change(function(){

                var obj = {cproyecto: $('#proyectos').val()};
                $.ajax({
                    type:'GET',
                    url: 'capturarProyectoMaster',
                    data : obj,

                    success: function (data) {
                        console.log(data);
                        $('#lista').html(data);


                        $('#cfinicio,#cfvigencia,#srginicio,#srgvigencia,#srpinicio,#srpvigencia,#fechaencuesta,#fechaencuesta1,#fechaencuesta2,#acsfechaenvio,#acsfecharecepcion,#fechafirma,#fechainicio,#fechatermino').datepicker({
                            format: "dd-mm-yyyy",
                            language: "es",
                            autoclose: true,
                            calendarWeeks: true,
                            todayHighlight: true,
                        });



                    },
                    error: function(data){

                    }

                });

            });

        });



    </script>

@stop