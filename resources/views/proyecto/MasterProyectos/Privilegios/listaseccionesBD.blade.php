<div class="col-md-6">
    <div class="box box-primary">
        <div class="box-header ui-sortable-handle" style="cursor: move;">
            <i class="fa fa-edit"></i>

            <h3 class="box-title">Lista de Modulos de la BD</h3>
            <input type="hidden" value="{{isset($cpersona) == 1 ? $cpersona : '' }}" id="cusuario">
            <input type="hidden" value="{{isset($nombre) == 1 ? $nombre : '' }}" id="nombreasignado">

        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
            <ul class="todo-list ui-sortable">
                @if (isset($ola2))
                    @foreach ($ola2 as $element)

                        <li class="">
                            <!-- drag handle -->
                            <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                            <!-- checkbox -->
                            <input type="checkbox" {{$element['estado']}}  value="{{$element['ctseccionmaster']}}" class="guardar_seccion">
                            <!-- todos text -->
                            <span class="text">{{$element['descripcion']  }}</span>
                            <div class="tools" style="display: block">
                                <span style="margin-right: 15px;color: #444" >Escritura</span>
                                <input type="radio"  value="Escritura" class="radiolecescr" style="margin-right: 15px" name="{{$element['ctseccionmaster']}}"  @if($element['lecescr'] == 'Escritura') checked @endif>
                                <span style="margin-right: 15px;color: #444">Lectura</span>
                                <input type="radio"  value="Lectura" class="radiolecescr" name="{{$element['ctseccionmaster']}}"  @if($element['lecescr'] == 'Lectura') checked @endif>
                            </div>
                        </li>
                    @endforeach

                @endif

            </ul>
        </div>
        <!-- /.box-body -->
    </div>
</div>

<script>
    $('.guardar_seccion').click(function(){

        guardarseccionMaster($(this));

    });
    $('.radiolecescr').click(function(){

        guardarseccionMasterRadio($(this));

    });



</script>