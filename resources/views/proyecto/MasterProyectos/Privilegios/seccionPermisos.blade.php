@extends('layouts.master')
@section('master-proyecto-privilegio')
    <div class="content-wrapper">


        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Lista de Asignaciones de la BD Proyectos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
            </section>
            <hr>

            <div class="col-md-12">
                <div class="box box-info">

                    <div class="box-body">
                            @include('proyecto.MasterProyectos.Privilegios.nombreuser',array('usuario'=>(isset($usuario)==1?$usuario:null) ))
                        <!-- /input-group -->
                    </div>
                    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
                    <!-- /.box-body -->
                </div>
            </div>
            <div id="lista">
                @include('proyecto.MasterProyectos.Privilegios.listaseccionesBD',array('ola2'=>(isset($ola2)==1?$ola2:null) ))

            </div>


        </div>

        @include('proyecto.MasterProyectos.Privilegios.searchUsuarioMaster')
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>

        var selected =[];
        $(document).ready(function(){
            $.fn.dataTable.ext.errMode = 'throw';
            var table=$("#tUsuario").DataTable({
                "processing" : true,
                "serverSide" : true,
                "ajax" : 'listarUsuariomaster',
                "columns" : [
                    {data : 'cpersona' , name : 'tp.cpersona'},
                    {data : 'usuario', name: 'tu.nombre'},
                    {data : 'descr', name: 'tar.descripcion'},
                    {data : 'abreviatura', name: 'tp.abreviatura'},
                ],

                "rowCallback" : function( row, data ) {

                    if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

                        $(row).addClass('selected');
                    }
                },
                "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "Sin Resultados",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No existe registros disponibles",
                    "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                    "search":         "Buscar:",
                    "processing":     "Procesando...",
                    "paginate": {
                        "first":      "Inicio",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "loadingRecords": "Cargando..."
                }
            });


            $('#tUsuario tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selected);
                var table = $('#tUsuario').DataTable();
                table.$('tr.selected').removeClass('selected');
                selected.splice(0,selected.length);
                if ( index === -1 ) {
                    selected.push( id );

                }/* else {
            selected.splice( index, 1 );
        }*/


            $(this).toggleClass('selected');
            });

            $('#searchUsuario').on('hidden.bs.modal', function () {
                goVerUsuario2();
            });



            $('#selecTodosEntr').click(function(){
                if ($('#selecTodosEntr').prop('checked')) {
                    $('.entregablesagregados_chk').each(
                        function(){
                            if ($(this).is(':visible')==true) {
                                $(this).prop('checked',true)
                            }
                        }
                    );
                }
                else{
                    $('.entregablesagregados_chk').prop('checked',false)
                }
            })



        });


        function goVerUsuario2(){

            var id =0;
            if (selected.length > 0 ){
                id = selected[0];
                //alert(id.substring(4));

                $.ajax({

                    url:'viewUsuarioMaster/'+id.substring(4),
                    type: 'GET',

                    beforeSend: function(){
                        //$('#div_carga').show();
                    },
                    success: function(data){
                        console.log("mostrar",data);

                        $('#lista').html(data);
                        $("#usuario").val($("#nombreasignado").val());


                    /*    $("#usuario").val(data[0][0].nombre);
                        $("#cusuario").val(data[0][0].cpersona);*/

                      /*  var idseccionesarray =  [];
                        $(".guardar_seccion").each(function() {
                            idseccionesarray.push( parseInt($(this).val()))

                        });

                        console.log("pushh",idseccionesarray);

                        validar(idseccionesarray,data);*/



                    },
                });


            }else{
                $('.alert').show();
            }
        }

/*        function validar(array,data) {
            for (var i=0; i < data.length ; i++) {

                var arrin =  array.indexOf(data[0][i].ctseccionmaster)
                console.log("arrayin",arrin)
                if(arrin >= 0)
                {
                    if(data[0][i].estado == 1)
                    {
                        $("#menu_"+data[0][i].ctseccionmaster).prop('checked')
                    }
                }

            }
        }*/
        function guardarseccionMaster(obj) {
            var estado = ''
            if ($(obj).prop('checked')) {

                estado = 1
            }
            else{
                estado = 0
            }


          // console.log(obj);
            var objSeccionMaster ={ctseccionmaster: "", cpersonaasignada: "",estado: "",lecescr: ""};

            objSeccionMaster.ctseccionmaster = $(obj).val();
            objSeccionMaster.cpersonaasignada =$("#cusuario").val();
            objSeccionMaster.estado = estado;
            objSeccionMaster.lecescr = $("input[name='nombre']:checked").val();
            objSeccionMaster._token = $('input[name="_token"]').first().val();
            //console.log(objSeccionMaster.ctseccionmaster, objSeccionMaster.cpersonaasignada,objSeccionMaster.estado);

            $.ajax({
                url: 'guardar_seccion',
                type:'POST',
                data: objSeccionMaster,

                success : function (data){

                    //verPerformance();

                   // console.log(data);

                },
            });
        }

        function guardarseccionMasterRadio(obj) {
            var estado = ''
            if ($(obj).parent().siblings(".guardar_seccion").prop('checked')) {

                estado = 1
            }
            else{
                estado = 0
            }

            console.log(estado);



            var radio = $(obj).val();
            // console.log(obj);
            var objSeccionMaster ={ctseccionmaster: "", cpersonaasignada: "",estado: "",lecescr: ""};

            objSeccionMaster.ctseccionmaster =  $(obj).parent().siblings(".guardar_seccion").val();
            objSeccionMaster.cpersonaasignada =$("#cusuario").val();
            objSeccionMaster.estado = estado;
            objSeccionMaster.lecescr = radio;
            objSeccionMaster._token = $('input[name="_token"]').first().val();
            //console.log(objSeccionMaster.ctseccionmaster, objSeccionMaster.cpersonaasignada,objSeccionMaster.estado);

            $.ajax({
                url: 'guardar_seccion_radio',
                type:'POST',
                data: objSeccionMaster,

                success : function (data){

                    //verPerformance();
                    // console.log(data);

                },
            });
        }




    </script>
@stop