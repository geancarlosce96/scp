@extends('layouts.master')
@section('masterBD-proyecto')

<style>
    .clsCabereraTabla2
    {
        background-color: #00a65a ;
        color: #ffffff;
        text-align: center;
    }
    .green{
        background-color: #00a65a ;
        color: #ffffff;
        text-align: center;
    }
    .red{
        background-color: #CD201A ;
        color: #ffffff;
        text-align: center;
    }
    .steelblue{
        background-color: steelblue ;
        color: #ffffff;
        text-align: center;
    }
</style>

<div class="content-wrapper"  style="background: #fff !important;min-height: auto !important;">


        <div class="col-sm-12">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Lista General de la BD Proyectos</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
            </section>
            <hr>


            <div class="col-md-12">
                <select id="proyectos"  multiple class="form-control columnas_nombre">
                    <option value="0" >Seleccione campos a ocultarse</option>
                    @foreach ($columnas_nombre as $key => $columna)
                     <option value="{{$columnas_id[$key]}}"  {{ $selecion_opcion[$key] }}>{{$key+1}}-{{$columna}}</option>
                    @endforeach
                </select>
            </div>
            <br><br><br><br>

            <div class="col-md-12">
                <div class="table-responsive" style="overflow-x: hidden !important;">
                    <table class="table" id="grid">
                        <thead>
                        <tr>
                            <th colspan="11" class="clsCabereraTabla">01 - Informacion Preliminar</th>
                            <th colspan="18" class="clsCabereraTabla2">02 - Etapa del Proyecto - Inicio</th>
                            <th colspan="9" class="red">03 - Etapa del Proyecto: Inicio - Carta Fianza y Seguros</th>
                            <th colspan="5" class="steelblue">04 - Etapa del Proyecto: Inicio - Contratos</th>
                            <th colspan="9" class="clsCabereraTabla2">05 - Etapa del Proyecto: Planificación</th>
                            <th colspan="8" class="red">06 - Etapa del Proyecto: Monitoreo y Control</th>
                            <th colspan="7" class="steelblue">07 - Etapa del Proyecto: Cierre</th>

                        </tr>
                        <tr class="clsCabereraTabla">
                            @if(isset($columnas_nombre))
                                @foreach ($columnas_nombre as $key => $colum)
                                <th scope="col" class="{{$columnas_id[$key]}}">{{$colum}}</th>
                                @endforeach
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($tproyectos as $tpy)
                            <tr>
                                @if (isset($tpy->listaCompletaBD))
                                    @foreach ($tpy->listaCompletaBD as $bdPry)

                                     {{-- 01 Lista Preliminar --}}
                                            <td>{{$bdPry->codigo}}</td>
                                            <td>{{$bdPry->nombrepry}}</td>
                                          {{--  <td>{{ $bdPry->descpry}}</td>--}}
                                            <td>{{$bdPry->auditado}}</td>
                                            <td>{{$bdPry->codigoCliente}}</td>
                                            <td>{{$bdPry->cliente}}</td>
                                            <td>{{$bdPry->uminera}}</td>
                                            <td>{{$bdPry->nombreclientefinal}}</td>
                                            <td>{{$bdPry->tipoproynombre}}</td>
                                            <td>{{$bdPry->nombreservicio}}</td>
                                            <td>{{$bdPry->nombretipogestion}}</td>
                                            <td>{{$bdPry ->fase }}</td>

                                     {{-- 02 Etapa del Proyecto - Inicio --}}
                                            <td>{{$bdPry->correoinicio }}</td>
                                            <td>{{$bdPry->montopresupuestado }}</td>
                                            <td>{{$bdPry->cpropuesta }}</td>
                                            <td>{{$bdPry->fadjucicacion }}</td>
                                            <td>{{$bdPry->finicio }}</td>
                                            <td>{{$bdPry->fcierre }}</td>
                                            <td>{{$bdPry->descripcion_contrato }}</td>
                                            <td>{{$bdPry->descripcion_lugar }}</td>
                                            <td>{{$bdPry->descripcion_moneda }}</td>
                                            <td>{{$bdPry->horaspresupuestadas }}</td>
                                            <td>{{$bdPry->numplan }}</td>
                                            <td>{{$bdPry->numdoc }}</td>
                                            <td>{{$bdPry->nummap }}</td>
                                            <td>{{$bdPry->numfig }}</td>
                                            <td>{{$bdPry->plazopagodias }}</td>
                                            <td>{{$bdPry->pplazo }}</td>
                                            <td>{{$bdPry->diascorte }}</td>
                                            <td>{{$bdPry->hoja_resumen }}</td>

                                      {{--03 - Etapa del Proyecto: Inicio - Carta Fianza y Seguros --}}

                                            <td>{{$bdPry->cf }}</td>
                                            <td>{{$bdPry->cf_inicio }}</td>
                                            <td>{{$bdPry->cf_vigencia }}</td>
                                            <td>{{$bdPry->srg }}</td>
                                            <td>{{$bdPry->srg_inicio }}</td>
                                            <td>{{$bdPry->srg_vigencia }}</td>
                                            <td>{{$bdPry->srp }}</td>
                                            <td>{{$bdPry->srp_inicio }}</td>
                                            <td>{{$bdPry->srp_vigencia }}</td>

                                        {{--04 - Etapa del Proyecto: Inicio - Contratos--}}

                                            <td>{{$bdPry->conos }}</td>
                                            <td>{{$bdPry->fechafirma }}</td>
                                            <td>{{$bdPry->nombrepry }}</td>
                                            <td>{{$bdPry->fechainicio }}</td>
                                            <td>{{$bdPry->fechatermino }}</td>


                                        {{--05 - Etapa del Proyecto: Planificación--}}
                                        <td>{{$bdPry->komcliente }}</td>
                                        <td>{{$bdPry->kominterno }}</td>
                                        <td>{{$bdPry->lc }}</td>
                                        <td>{{$bdPry->alc }}</td>
                                        <td>{{$bdPry->croproyecto }}</td>
                                        <td>{{$bdPry->edt }}</td>
                                        <td>{{$bdPry->le }}</td>
                                        <td>{{$bdPry->ram }}</td>
                                        <td>{{$bdPry->listariesgos }}</td>

                                        {{--06 - Etapa del Proyecto: Monitoreo y Control--}}

                                        <td>{{$bdPry->performance }}</td>
                                        <td>{{$bdPry->curvas }}</td>
                                        <td>{{$bdPry->encuesta }}</td>
                                        <td>{{$bdPry->encuesta1 }}</td>
                                        <td>{{$bdPry->encuesta2 }}</td>
                                        <td>{{$bdPry->encuestafecha }}</td>
                                        <td>{{$bdPry->encuestafecha1 }}</td>
                                        <td>{{$bdPry->encuestafecha2 }}</td>


                                        {{--07 - Etapa del Proyecto: Cierre--}}

                                        <td>{{$bdPry->acs }}</td>
                                        <td>{{$bdPry->acscodigo }}</td>
                                        <td>{{$bdPry->acsfechaenvio }}</td>
                                        <td>{{$bdPry->acsfecharecepcion }}</td>
                                        <td>{{$bdPry->la }}</td>
                                        <td>{{$bdPry->reunioncierre }}</td>
                                        <td>{{$bdPry->correofin }}</td>

                                    @endforeach
                                @endif

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <br><br><br><br>
        </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">

    $(document).ready(function(){

        $('.columnas_nombre').chosen(
            {
                allow_single_deselect: true,
                width:"100%",
                placeholder_text_multiple :"Columnas a ocultar"

            });

        $('.columnas_nombre > option').each(function(index, el) {
            if ($(this).prop("selected") == true) {
                $('.'+$(this).val()).hide();
            }else{
                $('.'+$(this).val()).show();
            }
            console.log("soe",$(this).prop("selected"),$(this).val());
        });

        $('.columnas_nombre').on('change',function(){
            var sele = $(this).val();
            seleccionarcolumnas(sele,4);
            $('.columnas_nombre > option').each(function(index, el) {
                if ($(this).prop("selected") == true) {
                    $('.'+$(this).val()).hide();
                }else{
                    $('.'+$(this).val()).show();
                }
                // console.log("soe",$(this).prop("selected"),$(this).val());
            });
        });

        function seleccionarcolumnas(option,tipo){
            $.ajax({
                url: 'seleccionarcolumnas',
                type: 'GET',
                // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
                data: {'option': option,'tipo': tipo},
            })
                .done(function(data) {
                    console.log("seleccionarcolumnas",data);
                })
                .fail(function() {
                    console.log("error");
                })
                .always(function() {
                    console.log("complete");
                });

        }


        var table =  $('#grid').DataTable(
            {
                /*scrollY:        '60vh',
                scrollCollapse: true,
                fixedHeader: {
                    header: true,
                    footer: true
                },

                pageLength: 20,
                // intervalo de paginacion
                lengthMenu: [ 5, 10, 25, 50, 75, 100 ],
                // mensaje de informacion mientras carga
                processing: true,
                language: {
                    url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                fixedColumns:   {
                    leftColumns: 2//Le indico que deje fijas solo las 2 primeras columnas
                }*/
                "paging":   false,
                "ordering": false,
                "info":     false,
                lengthChange: false,
                buttons: [ 'excel' ],
                scrollY:        "600px",
                scrollX:        true,
                scrollCollapse: true,
                paging:         false,
                fixedColumns:   {
                    leftColumns: 2
                },
                "columnDefs": [
                    { "width": "400px", "targets": 1 }
                ]
               /* language: {
                    url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },*/


            }
        );
        table.buttons().container()
            .appendTo( '#grid_wrapper .col-sm-6:eq(0)' );
    });
</script>

@stop