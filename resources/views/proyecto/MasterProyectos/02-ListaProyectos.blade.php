<style>
   .imagentamano{
       height: 128px !important;
       width: 128px !important;
       border-radius: 0% !important; ;
   }
    .plomo
    {
        background: rgb(236, 240, 245);
    }
    .back_icon{
        display: flex !important;
        justify-content: center !important;
        align-items: center !important;
        height: 45px !important;
        width: 45px  !important;
    }
    .tamano_icon{
        font-size: 28px !important;
    }
    .info-box-content{
        padding: 0px !important;
        margin-left: 58px;
    }
   .info-box-content-large{
       /*padding: 0px !important;*/
       margin-left: 15px;
   }
    .tamano_caja
    {
        min-height: 45px !important;
    }
    .tamano_caja_input{
        display: flex;
        align-items: center;
        justify-content: flex-start;
    }
    .input__info
    {
        margin-left: 18px !important;
        display: flex;
        justify-content: flex-start;
    }
   .input__info__left
   {
       margin-left: 0px !important;
   }
    .width__input
    {
        width: 50% !important;
       /* margin-left: 13px;*/
    }
    .rightleft{
        padding-right: 15px;
        padding-left: 15px;
    }
    .rightleft__radio{
        padding-right: 15px;
        padding-left: 15px;
        display: flex;
        justify-content: space-evenly;
    }
   .contennedor {
       display: block;
       position: relative;
       padding-left: 35px;
       margin-bottom: 12px;
       margin-top: 4px;
       cursor: pointer;
       font-size:10px;
       font-weight:400;
       -webkit-user-select: none;
       -moz-user-select: none;
       -ms-user-select: none;
       user-select: none;
   }

   /* Hide the browser's default checkbox */
   .contennedor input {
       position: absolute;
       opacity: 0;
       cursor: pointer;
       height: 0;
       width: 0;
   }

   /* Create a custom checkbox */
   .checkmark {
       position: absolute;
       top: 0;
       left: 0;
       height: 25px;
       width: 25px;
       background-color: #eee;
   }

   /* On mouse-over, add a grey background color */
   .contennedor:hover input ~ .checkmark {
       background-color: #ccc;
   }

   /* When the checkbox is checked, add a blue background */
   .contennedor input:checked ~ .checkmark {
       background-color: #2196F3;
   }

   /* Create the checkmark/indicator (hidden when not checked) */
   .checkmark:after {
       content: "";
       position: absolute;
       display: none;
   }

   /* Show the checkmark when checked */
   .contennedor input:checked ~ .checkmark:after {
       display: block;
   }

   /* Style the checkmark/indicator */
   .contennedor .checkmark:after {
       left: 9px;
       top: 5px;
       width: 5px;
       height: 10px;
       border: solid white;
       border-width: 0 3px 3px 0;
       -webkit-transform: rotate(45deg);
       -ms-transform: rotate(45deg);
       transform: rotate(45deg);
   }
</style>
@if(($arrayProyecto))
    <div class="col-md-6">
        @foreach($permiso as $per)
            @if ($per->ctseccionmaster == 1 )
                @include('proyecto.MasterProyectos.partials.01-InformacioPreliminar')
            @elseif ($per->ctseccionmaster == 3 )
                @include('proyecto.MasterProyectos.partials.03-FinanzasSeguro')
            @elseif ($per->ctseccionmaster == 4 )
                @include('proyecto.MasterProyectos.partials.04-Contratos')
            @elseif ($per->ctseccionmaster == 6 )
                    @include('proyecto.MasterProyectos.partials.06-MonitoreoControl')
            @endif
        @endforeach

    </div>
         <div class="col-md-6">
             <div class="row">
                 @include('proyecto.MasterProyectos.partials.gp')
                 @include('proyecto.MasterProyectos.partials.cp')
                 @include('proyecto.MasterProyectos.partials.cd')
             </div>
             @foreach($permiso as $per )
                 @if ($per->ctseccionmaster == 2 )
                    @include('proyecto.MasterProyectos.partials.02-Inicio')
                 @elseif ($per->ctseccionmaster == 5 )
                     @include('proyecto.MasterProyectos.partials.05-Planificacion')
                 @elseif ($per->ctseccionmaster == 7 )
                     @include('proyecto.MasterProyectos.partials.07-Cierre')
                 @endif
             @endforeach


         </div>
@else
@endif
