<div class="col-sm-12">
    <div class="row">
        <div class="box box-primary">

            <div class="box-header with-border">
                <h3 class="box-title">03 - Etapa del Proyecto: Inicio - Carta Fianza y Seguros</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>

                <div class="box-body plomo">
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input ">

                         <span class="info-box-icon bg-green back_icon"><i class="fa fa-edit tamano_icon"></i></span>
                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">CF.</span>
                             </div>
                             <div class="input-group date rightleft__radio">
                                 <label class="contennedor">
                                     <div style="padding-top: 6px;">Si</div>
                                     <input type="radio" value="Si" name="q1" class="cf" @if($arrayProyecto[0]['cf'] == 'Si') checked @endif
                                     @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                     <span class="checkmark"></span>
                                 </label>
                                 <label class="contennedor">
                                     <div style="padding-top: 6px;">No</div>
                                     <input type="radio" value="No" name="q1" class="cf" @if($arrayProyecto[0]['cf'] == 'No') checked @endif
                                     @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                     <span class="checkmark"></span>
                                 </label>
                             </div>
                         </div>

                         <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">CF INICIO.</span>
                            </div>
                            <div class="input-group date rightleft">

                                <input class="form-control pull-right" id="cfinicio" name="cfinicio" type="text" value="{{ $arrayProyecto[0]['cf_inicio'] }}" readonly="true"
                                       @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>

                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">CF VIGENCIA.</span>
                             </div>
                             <div class="input-group date rightleft">
                                 <input class="form-control pull-right" id="cfvigencia" name="cfvigencia" type="text" value="{{ $arrayProyecto[0]['cf_vigencia'] }}" readonly="true"
                                        @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                             </div>
                         </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-cogs tamano_icon"></i></span>

                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">SRG.</span>
                             </div>
                             <div class="input-group date rightleft__radio">
                                 <label class="contennedor">
                                     <div style="padding-top: 6px;">Si</div>
                                     <input type="radio" value="Si" name="q2" class="srg" @if($arrayProyecto[0]['srg'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                     <span class="checkmark"></span>
                                 </label>
                                 <label class="contennedor">
                                     <div style="padding-top: 6px;">No</div>
                                     <input type="radio" value="No" name="q2" class="srg" @if($arrayProyecto[0]['srg'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                     <span class="checkmark"></span>
                                 </label>
                             </div>
                         </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>
                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">SRG INICIO.</span>
                             </div>
                             <div class="input-group date rightleft">
                                 <input class="form-control pull-right" id="srginicio" name="srginicio" type="text" value="{{ $arrayProyecto[0]['srg_inicio'] }}" readonly="true"
                                        @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                             </div>
                         </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">SRG VIGENCIA.</span>
                             </div>
                             <div class="input-group date rightleft">
                                 <input class="form-control pull-right" id="srgvigencia" name="srgvigencia" type="text" value="{{ $arrayProyecto[0]['srg_vigencia'] }}" readonly="true"
                                        @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                             </div>
                         </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">SRP.</span>
                             </div>
                             <div class="input-group date rightleft__radio">
                                 <label class="contennedor">
                                     <div style="padding-top: 6px;">Si</div>
                                     <input type="radio" value="Si" name="q3" class="srp" @if($arrayProyecto[0]['srp'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                     <span class="checkmark"></span>
                                 </label>
                                 <label class="contennedor">
                                     <div style="padding-top: 6px;">No</div>
                                     <input type="radio" value="No" name="q3" class="srp" @if($arrayProyecto[0]['srp'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                     <span class="checkmark"></span>
                                 </label>
                             </div>
                         </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">SRP INICIO.</span>
                             </div>
                             <div class="input-group date rightleft">
                                 <input class="form-control pull-right" id="srpinicio" name="srpinicio" type="text" value="{{ $arrayProyecto[0]['srp_inicio'] }}" readonly="true"
                                        @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                             </div>
                         </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12" >
                     <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                         <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                             <div class="col-sm-6">
                                 <span class="info-box-text">SRP VIGENCIA.</span>
                             </div>
                             <div class="input-group date rightleft">
                                 <input class="form-control pull-right" id="srpvigencia" name="srpvigencia" type="text" value="{{ $arrayProyecto[0]['srp_vigencia'] }}" readonly="true"
                                        @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                             </div>
                         </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                    @if ($per->lecescr == 'Escritura' )
                        <div class="col-md-6 col-sm-12 col-xs-12" data-cproyecto="{{ $arrayProyecto[0]['cproyecto']}}">
                            <div class="info-box tamano_caja tamano_caja_input container">
                                <button type="button" class="btn btn-block btn-primary grabar_fianza_seguro" >Grabar</button>
                            </div>
                            <!-- /.info-box -->
                        </div>
                    @else
                    @endif


            </div>

        </div>
    </div>
</div>
<script>
    $(".grabar_fianza_seguro").click(function() {
        grabarFianzaSeguro($(this));
    });

    function grabarFianzaSeguro(obj){

        var objPorcentaje={cproyecto: "",cf:"",cfinicio:"",cfvigencia:"",srg:"",srginicio:"",srgvigencia:"",srp:"",srpinicio:"",srpvigencia:""};

        objPorcentaje.cproyecto=$(obj).parent().parent().data("cproyecto");
        objPorcentaje.cf=$(".cf:checked").val();
        objPorcentaje.cfinicio=$("#cfinicio").val();
        objPorcentaje.cfvigencia=$("#cfvigencia").val();
        objPorcentaje.srg=$(".srg:checked").val();
        objPorcentaje.srginicio=$("#srginicio").val();
        objPorcentaje.srgvigencia=$("#srgvigencia").val();
        objPorcentaje.srp=$(".srp:checked").val();
        objPorcentaje.srpinicio=$("#srpinicio").val();
        objPorcentaje.srpvigencia=$("#srpvigencia").val();
        objPorcentaje._token = $('input[name="_token"]').first().val();

      //  console.log(objPorcentaje.cproyecto, objPorcentaje.cf, objPorcentaje.cfinicio, objPorcentaje.cfvigencia);

        swal({
                title: " Desea guardar la informacion",
                text: "Presione Guardar, para el correcto ingreso de datos al sistema",
                type: "info",
                html: true,
                showCancelButton: true,
                confirmButtonText: "Guardar",
                confirmButtonColor: "green",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: 'guardardataFianzaSeguro',
                        type:'POST',
                        data: objPorcentaje,

                        success : function (data){

                            //verPerformance();

                            console.log(data);
                            swal({
                                title: "Guardado",
                                text: "Correctamente",
                                type: "success",
                                timer: 1000,
                                showConfirmButton: false
                            });

                        },
                    });

                }
                else {
                    swal({
                        title: "CANCELADO",
                        text: "Operación Cancelada",
                        type: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            }
        );


    }



</script>