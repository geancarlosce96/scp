<div class="col-sm-12">
    <div class="row">
        <div class="box box-primary">

            <div class="box-header with-border">
                <h3 class="box-title">07 - Etapa del Proyecto: Cierre</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body plomo">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ACS.</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="acs" class="acs" @if($arrayProyecto[0]['acs'] == 'Si') checked @endif
                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="acs" class="acs" @if($arrayProyecto[0]['acs'] == 'No') checked @endif
                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ACS CODIGO.</span>
                            </div>
                            <div class="input-group date rightleft">
                                <label for=""><span id="proyabrev">{{$arrayProyecto[0]['codigo'].'-'.$arrayProyecto[0]['abrv']}}</span>-<span id="texto2"></span></label>

                                <input class="form-control pull-right" id="acscodigo" name="acscodigo" type="text" value="{{$arrayProyecto[0]['acscodigo']}}"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                {!! Form::hidden('acscodigocompleto','', array('class'=>'form-control pull-right','id' => 'acscodigocompleto')) !!}

                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>


                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ACS FECHA ENVIO.</span>
                            </div>
                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="acsfechaenvio" name="acsfechaenvio" type="text" value="{{ $arrayProyecto[0]['acsfechaenvio'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>

                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ACS FECHA RECEPCION.</span>
                            </div>
                            <div class="input-group date rightleft">
                               {{-- {!! Form::text('acsfecharecepcion',$arrayProyecto[0]['acsfecharecepcion'], array('class'=>'form-control pull-right','id' => 'acsfecharecepcion','readonly'=>'true')) !!}--}}
                                <input class="form-control pull-right" id="acsfecharecepcion" name="acsfecharecepcion" type="text" value="{{ $arrayProyecto[0]['acsfecharecepcion'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">LA.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['la']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">REUNION DE CIERRE DE PROYECTO.</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="reunioncierre" class="reunioncierre" @if($arrayProyecto[0]['reunioncierre'] == 'Si') checked @endif readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="reunioncierre" class="reunioncierre" @if($arrayProyecto[0]['reunioncierre'] == 'No') checked @endif readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">CORREO FIN PY.</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="correofin" class="correofin" @if($arrayProyecto[0]['correofin'] == 'Si') checked @endif readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="correofin" class="correofin" @if($arrayProyecto[0]['correofin'] == 'No') checked @endif readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                @if ($per->lecescr == 'Escritura' )
                <div class="col-md-6 col-sm-12 col-xs-12" data-cproyecto="{{ $arrayProyecto[0]['cproyecto']}}">
                    <div class="info-box tamano_caja tamano_caja_input container">
                           <button type="button" class="btn btn-block btn-primary grabar_cierre" >Grabar</button>
                    </div>
                <!-- /.info-box -->
                </div>
                @else
                @endif

            </div>
        </div>
    </div>
</div>
<script>

    $("#acscodigo").keyup(function () {
        var value = $(this).val();
        $("#texto2").text(value);
        $("#acscodigocompleto").val($("#proyabrev").text()+'-'+$("#texto2").text())

    });



   /* $(document).ready(function () {
    });*/

    $(".grabar_cierre").click(function() {
        grabarCierre($(this));
    });

    function grabarCierre (obj) {

        var objPorcentaje={cproyecto: "",acs:"",acscodigo:"",reunioncierre:"",correofin:"",acsfechaenvio:"",acsfecharecepcion:""};

        objPorcentaje.cproyecto=$(obj).parent().parent().data("cproyecto");
        objPorcentaje.acs=$(".acs:checked").val();
        objPorcentaje.acscodigo=$("#acscodigocompleto").val();
        objPorcentaje.reunioncierre=$(".reunioncierre:checked").val();
        objPorcentaje.correofin=$(".correofin:checked").val();
        objPorcentaje.acsfechaenvio=$("#acsfechaenvio").val();
        objPorcentaje.acsfecharecepcion=$("#acsfecharecepcion").val();
        objPorcentaje._token = $('input[name="_token"]').first().val();


        swal({
                title: " Desea guardar la informacion",
                text: "Presione Guardar, para el correcto ingreso de datos al sistema",
                type: "info",
                html: true,
                showCancelButton: true,
                confirmButtonText: "Guardar",
                confirmButtonColor: "green",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: 'guardardataCierre',
                        type:'POST',
                        data: objPorcentaje,

                        success : function (data){

                            //verPerformance();

                            console.log(data);
                            swal({
                                title: "Guardado",
                                text: "Correctamente",
                                type: "success",
                                timer: 1000,
                                showConfirmButton: false
                            });

                        },
                    });

                }
                else {
                    swal({
                        title: "CANCELADO",
                        text: "Operación Cancelada",
                        type: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            }
        );
    }


</script>