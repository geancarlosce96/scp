<div class="col-sm-4">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <!-- USERS LIST -->
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Control de Proyectos </h3>

                <div class="box-tools pull-right">

                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i>
                    </button>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding plomo">
                <ul class="users-list clearfix">
                    <li style="width: 100%">

                        <?php
                        $imagen='images/'.$arrayProyecto[0]['identificacioncp'].'.jpg';
                        if(file_exists($imagen)){
                            $im=1;
                        }

                        else
                        {
                            $im=0;
                        }

                        ?>

                        @if($im==1)
                            <img src="{{ url('images/'.$arrayProyecto[0]['identificacioncp'].'.jpg') }}" class="user-image imagentamano" alt="User Image">
                        @else
                            <img src="{{ url('images/sinfoto.jpg') }}" class="user-image imagentamano" alt="User Image">
                        @endif


                        <a class="users-list-name" href="#">{{ $arrayProyecto[0]['nomcp']}}</a>
                        <span class="users-list-date">CP</span>
                    </li>


                </ul>
                <!-- /.users-list -->
            </div>
        </div>
        <!--/.box -->
    </div>
</div>