<div class="col-sm-12">
    <div class="row">
        <div class="box box-primary">

            <div class="box-header with-border">
                <h3 class="box-title">05 - Etapa del Proyecto: Planificación</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body plomo">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">KOM CON EL CLIENTE</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="q7" class="komcliente"  @if($arrayProyecto[0]['komcliente'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="q7" class="komcliente"  @if($arrayProyecto[0]['komcliente'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">KOM INTERNO</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="kominterno"  class="kominterno" @if($arrayProyecto[0]['kominterno'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="kominterno"  class="kominterno" @if($arrayProyecto[0]['kominterno'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-light-blue back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">LC</span>
                            </div>

                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="lc" class="lc" @if($arrayProyecto[0]['lc'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="lc" class="lc" @if($arrayProyecto[0]['lc'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-light-blue back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ALC</span>
                            </div>

                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="alc" class="alc" @if($arrayProyecto[0]['alc'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="alc" class="alc" @if($arrayProyecto[0]['alc'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-light-blue back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">

                            <div class="col-sm-6">
                                <span class="info-box-text">CRO DEL PROYECTO.</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="croproyecto" class="croproyecto" @if($arrayProyecto[0]['croproyecto'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="croproyecto" class="croproyecto" @if($arrayProyecto[0]['croproyecto'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">EDT.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['edt']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">LE.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['le']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">RAM.</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="ram" class="ram" @if($arrayProyecto[0]['ram'] == 'Si') checked @endif  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="ram" class="ram" @if($arrayProyecto[0]['ram'] == 'No') checked @endif  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">LISTA DE RIESGOS.</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="listariesgos" class="listariesgos" @if($arrayProyecto[0]['listariesgos'] == 'Si') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="listariesgos" class="listariesgos" @if($arrayProyecto[0]['listariesgos'] == 'No') checked @endif @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                @if ($per->lecescr == 'Escritura' )
                    <div class="col-md-6 col-sm-12 col-xs-12" data-cproyecto="{{ $arrayProyecto[0]['cproyecto']}}">
                        <div class="info-box tamano_caja tamano_caja_input container">
                            <button type="button" class="btn btn-block btn-primary grabar_planificacion" >Grabar</button>
                        </div>
                        <!-- /.info-box -->
                    </div>
                @else
                @endif



            </div>
        </div>
    </div>
</div>
<script>
    $(".grabar_planificacion").click(function() {
        grabarPlanificacion($(this));
    });

    function grabarPlanificacion(obj) {
        var objPorcentaje = {cproyecto: "",komcliente:"",kominterno:"",lc:"",alc:"",croproyecto:"",ram:"",listariesgos:""};

        objPorcentaje.cproyecto=$(obj).parent().parent().data("cproyecto");
        objPorcentaje.komcliente=$(".komcliente:checked").val();
        objPorcentaje.kominterno=$(".kominterno:checked").val();
        objPorcentaje.lc=$(".lc:checked").val();
        objPorcentaje.alc=$(".alc:checked").val();
        objPorcentaje.croproyecto=$(".croproyecto:checked").val();
        objPorcentaje.ram=$(".ram:checked").val();
        objPorcentaje.listariesgos=$(".listariesgos:checked").val();
        objPorcentaje._token = $('input[name="_token"]').first().val();


        swal({
                title: " Desea guardar la informacion",
                text: "Presione Guardar, para el correcto ingreso de datos al sistema",
                type: "info",
                html: true,
                showCancelButton: true,
                confirmButtonText: "Guardar",
                confirmButtonColor: "green",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: 'guardardataPlanificacion',
                        type:'POST',
                        data: objPorcentaje,

                        success : function (data){

                            //verPerformance();

                            console.log(data);
                            swal({
                                title: "Guardado",
                                text: "Correctamente",
                                type: "success",
                                timer: 1000,
                                showConfirmButton: false
                            });

                        },
                    });

                }
                else {
                    swal({
                        title: "CANCELADO",
                        text: "Operación Cancelada",
                        type: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            }
        );






    }


</script>