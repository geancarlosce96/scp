<div class="col-md-6 col-sm-12 col-xs-12">
    <div class="info-box tamano_caja">
        @if ($arrayProyecto[0]['descripcion_estado']== 'Activo')
            <span class="info-box-icon bg-light-blue back_icon"><i class="fa fa-check tamano_icon"></i></span>
        @elseif($arrayProyecto[0]['descripcion_estado']== 'Cierre')
            <span class="info-box-icon bg-green back_icon"><i class="fa fa-close tamano_icon"></i></span>
        @elseif($arrayProyecto[0]['descripcion_estado']== 'Cancelado')
            <span class="info-box-icon back_icon" style="background:rgb(210, 214, 222) "><i class="fa fa-exclamation-triangle tamano_icon"></i></span>
        @elseif($arrayProyecto[0]['descripcion_estado']== 'Cierre-tec')
            <span class="info-box-icon back_icon " style="background:#FFF400 "><i class="fa fa-power-off tamano_icon"></i></span>
        @elseif($arrayProyecto[0]['descripcion_estado']== 'Cierre-Adm')
            <span class="info-box-icon  bg-yellow  back_icon"><i class="fa fa-sign-out tamano_icon"></i></span>
        @elseif($arrayProyecto[0]['descripcion_estado']== 'Paralizado')
            <span class="info-box-icon  bg-red back_icon"><i class="fa fa-hand-stop-o tamano_icon"></i></span>
        @else
            <span class="info-box-icon  bg-aqua back_icon"><i class="fa fa-check tamano_icon"></i></span>
        @endif
        <div class="info-box-content no-padding">
                <span class="info-box-text">Estado</span>
                <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['descripcion_estado']}}</span>
            </div>
    </div>
</div>