<div class="col-sm-12">
    <div class="row">
        <div class="box box-primary">
            {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
            <div class="box-header with-border">
                <h3 class="box-title">04 - Etapa del Proyecto: Inicio - Contratos</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body plomo">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">CON / OS.</span>
                            </div>
                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="conos" name="conos" type="text" value="{{ $arrayProyecto[0]['conos'] }}"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">CONFECHAFIRMA</span>
                            </div>
                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="fechafirma" name="fechafirma" type="text" value="{{ $arrayProyecto[0]['fechafirma'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">CONNOMBRE PY</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['nombrepry']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">CONFECHAINICIO</span>
                            </div>
                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="fechainicio" name="fechainicio" type="text" value="{{ $arrayProyecto[0]['fechainicio'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">CONFECHATERMINO</span>
                            </div>
                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="fechatermino" name="fechatermino" type="text" value="{{ $arrayProyecto[0]['fechatermino'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                @if ($per->lecescr == 'Escritura' )
                    <div class="col-md-offset-6 col-md-6 col-sm-12 col-xs-12" data-cproyecto="{{ $arrayProyecto[0]['cproyecto']}}">
                        <div class="info-box tamano_caja tamano_caja_input container">
                            <button type="button" class="btn btn-block btn-primary grabar_contrato" >Grabar</button>
                        </div>
                        <!-- /.info-box -->
                    </div>
                @else
                @endif

            </div>
        </div>
    </div>
</div>
<script>
    $(".grabar_contrato").click(function() {
        grabarMonitoreoContrato($(this));
    });

    function grabarMonitoreoContrato (obj) {
        var objPorcentaje={cproyecto: "",conos:"",fechafirma:"",fechainicio:"",fechatermino:""};

        objPorcentaje.cproyecto=$(obj).parent().parent().data("cproyecto");
        objPorcentaje.conos=$("#conos").val();
        objPorcentaje.fechafirma=$("#fechafirma").val();
        objPorcentaje.fechainicio=$("#fechainicio").val();
        objPorcentaje.fechatermino=$("#fechatermino").val();
        objPorcentaje._token = $('input[name="_token"]').first().val();

        swal({
                title: " Desea guardar la informacion",
                text: "Presione Guardar, para el correcto ingreso de datos al sistema",
                type: "info",
                html: true,
                showCancelButton: true,
                confirmButtonText: "Guardar",
                confirmButtonColor: "green",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: 'guardardataContrato',
                        type:'POST',
                        data: objPorcentaje,

                        success : function (data){

                            //verPerformance();

                            console.log(data);
                            swal({
                                title: "Guardado",
                                text: "Correctamente",
                                type: "success",
                                timer: 1000,
                                showConfirmButton: false
                            });

                        },
                    });

                }
                else {
                    swal({
                        title: "CANCELADO",
                        text: "Operación Cancelada",
                        type: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            }
        );

    }
</script>