<div class="col-sm-12">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">02 - Etapa del Proyecto - Inicio </h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>
            <div class="box-body plomo">
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Correo Ini Py</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['correoinicio']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-money tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PMonto Presupuestados.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['montopresupuestados']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-cogs tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PPA (Cod. Propuestas)</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['cpropuesta']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Fecha de Adjudicación</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['fadjucicacion']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PFI Planificado.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['finicio']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PFF PLANIFICADO.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['fcierre']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">TIPO DE CONTRATO</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['descripcion_contrato']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-map-o tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">LUGAR DE TRABAJO</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['descripcion_lugar']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-money tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PTIPO MONEDA</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['descripcion_moneda']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PCantidad HH</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['horaspresupuestadas']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa  fa-line-chart tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PCANT PLANOS.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['numeroplanos']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-file-text tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">P-CANT DOCUMENTOS</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['numerodocumentos']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-map tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PCantidad Mapas</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['numeromapas']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-area-chart tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">P-CANT FIGURAS</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['numerofiguras']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PLAZO PAGO DE FACTURAS</span>
                            <span class="info-box-number"  style="font-size: 14px">{{ $arrayProyecto[0]['plazopagodias']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PPLAZO</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['pplazo']}} dias</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">FECHA DE CORTE VAL</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['diascorte']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>





                <div class="col-md-6 col-sm-12 col-xs-12">
                   <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">HR</span>
                            <span class="info-box-number"  style="font-size: 14px">{{ $arrayProyecto[0]['hoja_resumen']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
        </div>
    </div>
</div>