<div class="col-sm-12">
    <div class="row">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">01 - Información Preliminar</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body plomo">
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-key tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">N° Proyecto</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['codigo']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            @include('proyecto.MasterProyectos.partials.estado')
            <!-- /.col -->
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box" style="display: flex; align-items: center;justify-content: flex-start;">
                        <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

                        <div class="info-box-content-large">
                            <span class="info-box-text">Nombre del Proyecto</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['nombrepry']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="info-box" style="display: flex; align-items: center;justify-content: flex-start;">
                        <span class="info-box-icon bg-red "><i class="fa fa-book "></i></span>

                        <div class="info-box-content-large ">
                            <span class="info-box-text">Descripcion del Proyecto (HR)</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['descpry']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-bookmark tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Auditado</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['auditado']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- /.col -->
                <!-- fix for small devices only -->
                <div class="clearfix visible-sm-block"></div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-tag tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Cod. Cliente</span>
                            <span class="info-box-number" style="font-size: 14px">{{ substr($arrayProyecto[0]['codigo'],0,4) }}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-user tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Cliente</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['cliente']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-book tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Unidad</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['uminera']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-user tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Cliente Final</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['nombreclientefinal']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-yellow back_icon"><i class="fa fa-sticky-note tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Tipo de Proyecto</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['tipoproynombre']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-cogs tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Portaf. de Servicios</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['nombreservicio']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-arrows tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Tamaño del Proy.</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['nombretipogestion']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                    <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-bookmark tamano_icon"></i></span>

                        <div class="info-box-content no-padding">
                            <span class="info-box-text">Fase</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['fase']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
            </div>
        </div>
    </div>
</div>