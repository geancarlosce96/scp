<div class="col-sm-12">
    <div class="row">
        <div class="box box-primary">

            <div class="box-header with-border">
                <h3 class="box-title">06 - Etapa del Proyecto: Monitoreo y Control</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                    </button>

                </div>
            </div>
            <div class="box-body plomo">
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-file-excel-o tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">PERFORMANCE</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['performance']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja">
                        <span class="info-box-icon bg-green back_icon"><i class="fa fa-area-chart tamano_icon"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">CURVA S</span>
                            <span class="info-box-number" style="font-size: 14px">{{ $arrayProyecto[0]['curva-s']}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-light-blue back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content input__info__left"  style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ENCUESTA</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="q4" class="radioencuesta" @if($arrayProyecto[0]['encuesta'] == 'Si') checked @endif
                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="q4" class="radioencuesta" @if($arrayProyecto[0]['encuesta'] == 'No') checked @endif
                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>

                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-light-blue back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left"  style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ENCUESTA 1 50%</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="q5" class="radioencuesta1" @if($arrayProyecto[0]['encuesta1'] == 'Si') checked @endif

                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="q5" class="radioencuesta1" @if($arrayProyecto[0]['encuesta1'] == 'No') checked @endif
                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-light-blue back_icon"><i class="fa fa-edit tamano_icon"></i></span>

                        <div class="info-box-content  input__info__left"  style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ENCUESTA 2 100%.</span>
                            </div>
                            <div class="input-group date rightleft__radio">
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">Si</div>
                                    <input type="radio" value="Si" name="q6" class="radioencuesta2" @if($arrayProyecto[0]['encuesta2'] == 'Si') checked @endif
                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                                <label class="contennedor">
                                    <div style="padding-top: 6px;">No</div>
                                    <input type="radio" value="No" name="q6" class="radioencuesta2" @if($arrayProyecto[0]['encuesta2'] == 'No') checked @endif
                                    @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                                    <span class="checkmark"></span>
                                </label>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ENC-FECHA</span>
                            </div>

                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="fechaencuesta" name="fechaencuesta" type="text" value="{{ $arrayProyecto[0]['encuestafecha'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ENCUESTA 1  FECHA</span>
                            </div>
                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="fechaencuesta1" name="encuestafecha1" type="text" value="{{ $arrayProyecto[0]['encuestafecha1'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <div class="col-md-6 col-sm-12 col-xs-12">
                       <div class="info-box tamano_caja tamano_caja_input">
                        <span class="info-box-icon bg-aqua back_icon"><i class="fa fa-calendar-check-o tamano_icon"></i></span>

                        <div class="info-box-content input__info__left" style="width: calc(100% - 45px)">
                            <div class="col-sm-6">
                                <span class="info-box-text">ENCUESTA 2  FECHA.</span>
                            </div>
                            <div class="input-group date rightleft">
                                <input class="form-control pull-right" id="fechaencuesta2" name="encuestafecha2" type="text" value="{{ $arrayProyecto[0]['encuestafecha2'] }}" readonly="true"  @if ($per->lecescr == 'Escritura' ) @else disabled  @endif>
                            </div>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>

                @if ($per->lecescr == 'Escritura' )
                    <div class="col-md-offset-6 col-md-6 col-sm-12 col-xs-12" data-cproyecto="{{ $arrayProyecto[0]['cproyecto']}}">
                        <div class="info-box tamano_caja tamano_caja_input container">
                            <button type="button" class="btn btn-block btn-primary grabar_monitoreo" >Grabar</button>
                        </div>
                    </div>
                @else
                @endif


            </div>
        </div>
    </div>
</div>
<script>
    $(".grabar_monitoreo").click(function() {
        grabarMonitoreoControl($(this));
    });

    function grabarMonitoreoControl (obj) {
        var objPorcentaje={cproyecto: "",radioencuesta:"",radioencuesta1:"",radioencuesta2:"",fechaencuesta:"",fechaencuesta1:"",fechaencuesta2:""};

        objPorcentaje.cproyecto=$(obj).parent().parent().data("cproyecto");
        objPorcentaje.radioencuesta=$(".radioencuesta:checked").val();
        objPorcentaje.radioencuesta1=$(".radioencuesta1:checked").val();
        objPorcentaje.radioencuesta2=$(".radioencuesta2:checked").val();
        objPorcentaje.fechaencuesta=$("#fechaencuesta").val();
        objPorcentaje.fechaencuesta1=$("#fechaencuesta1").val();
        objPorcentaje.fechaencuesta2=$("#fechaencuesta2").val();
        objPorcentaje._token = $('input[name="_token"]').first().val();

        swal({
                title: " Desea guardar la informacion",
                text: "Presione Guardar, para el correcto ingreso de datos al sistema",
                type: "info",
                html: true,
                showCancelButton: true,
                confirmButtonText: "Guardar",
                confirmButtonColor: "green",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false
            },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        url: 'guardardataMonitoreo',
                        type:'POST',
                        data: objPorcentaje,

                        success : function (data){

                            //verPerformance();

                            console.log(data);
                            swal({
                                title: "Guardado",
                                text: "Correctamente",
                                type: "success",
                                timer: 1000,
                                showConfirmButton: false
                            });

                        },
                    });

                }
                else {
                    swal({
                        title: "CANCELADO",
                        text: "Operación Cancelada",
                        type: "error",
                        timer: 1000,
                        showConfirmButton: false
                    });
                }
            }
        );

    }
</script>