<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Registro de Hoja Resumen</small>
      </h1>
            
      <ol class="breadcrumb">
        <li>
            @include('accesos.accesoProyecto')
            
        </li>
        
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
        <div class="col-lg-10 col-xs-1"></div>
        <div class="col-lg-1 col-xs-1">
        @if(isset($aprobacion))
            @if($aprobacion)        
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Aprobaciones</button>
            @endif
        @endif
        </div>
        <div class="col-lg-1 col-xs-1"></div>
        </div>    
    {!! Form::open(array('url' => 'grabarHR','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('valor',(isset($valor)==1?$valor:''),array('id'=>'valor')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}

        {!! Form::hidden('cunidadminera_proy',(isset($uminera->cunidadminera)==1?$uminera->cunidadminera:''),array('id'=>'cunidadminera_proy')) !!}


   

    <div class="row">
        <!--<div class="col-lg-1 hidden-xs"></div>-->
    
    <div class="col-md-12 col-xs-12">

        <div class="row clsPadding">


        <!--    <div class="col-lg-1 col-xs-12">Código de Documento:</div>
            <div class="col-lg-3 col-xs-12">
                {!! Form::text('coddocumento',(isset($documento->codigo)==1?$documento->codigo:''), array('class'=>'form-control input-sm','placeholder' => 'Código de Documento','id'=>'coddocumento') ) !!}   
            </div>        
            
            
            <div class="col-lg-1 col-xs-12">Fecha Documento:</div>
            <div class="col-lg-2 col-xs-12">
                <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                     {!! Form::text('fechadoc',(isset($documento->fechadoc)?$documento->fechadoc:''), array('class'=>'form-control pull-right ','id'=>'fechadoc')) !!}
                </div>
            </div>
        </div>-->
        <br>

            <div class="col-md-1 col-xs-12">Cliente:</div>
            <div class="col-md-2 col-xs-12">
                {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
                {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','readonly' => 'true','id' => 'personanombre')) !!}
            </div>

            <div class="col-lg-1 col-xs-12">Unidad minera:</div>
            <div class="col-lg-2 col-xs-12">
                {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','readonly' => 'true','id'=>'umineranombre') ) !!}   
            </div>

            
           <!--  <div class="col-lg-1 col-xs-2">Nueva Revision:</div>
            <div class="col-lg-1 col-xs-2">
                {!! Form::select('nuevarevision',(isset($revisiones)==1?$revisiones:array()),'',array('class' => 'form-control')) !!}
            </div> -->

            <input type="hidden" name="nuevarevision" id="nuevarevision">

            <div class="col-lg-4 col-xs-12">
                
                <div class="input-group" id="resumen">


                    <span class="input-group-addon">

                    <!-- ########################################################################################################-->           
                       
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="notificaciones()">
                        
                          <button name="htparticipante" type="button" id="btnVerRevisiones" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" 
                        @if(isset($editar))
                            @if($editar==true)
                            @else
                                disabled   
                            @endif
                        @else 
                            disabled
                        @endif 
                          ><span class="glyphicon glyphicon-eye-open"></span></button>                   
                         
                        </a>

                        <ul class="dropdown-menu" style="font-size: 10px;">
                            <li class="header" style="background-color:rgba(0, 105, 170, 1);text-align: center;padding: 5px;"><label style="color: white;"><strong>Revisiones Anteriores</strong></label></li>
                            <li>
                              <!-- inner menu: contains the actual data -->
                              <ul class="menu">
                                <br>

                                @if(isset($revisionesAnteriores))
                                    @foreach($revisionesAnteriores as $ra)
                                        <li class="active">

                                            <?php $ruta ='archivos/proyectos/'.$ra->cproyecto.'/'.$ra->cproyecto.'_R'.$ra->revisionanterior;
                                                    $ruta=trim($ruta).'.pdf';

                                            ?>        
                                            <a href="{{$ruta}}"  target="_blank"> 
                                           
                                                <label>Revisión {{$ra->revisionanterior}}</label>                                  
                                             
                                            </a>
                                        </li>                                    
                                    @endforeach    
                                @endif
                              </ul>
                            </li>                           
                        </ul>
                  
                    <!-- ########################################################################################################-->
                   

                        <label class="col-form-label">Revisión :</label>
                    </span>                   
                        {!! Form::text('revisiondocumento',(isset($documento->revision)==1?$documento->revision:''),array('class'=>'form-control pull-right', 'readonly'=>'true','id'=>'revisiondocumento')) !!}
                    
               
                    
                    <span class="input-group-addon">
                        <label class="col-form-label">Fecha :</label>
                    </span>                   
                        <div class="input-group date">
                            <!--<div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>-->
                             {!! Form::text('fechadoc',(isset($fechadoc)?$fechadoc:''), array('class'=>'form-control pull-right ','id'=>'fechadoc','readonly'=>'true')) !!}
                        </div>
                    
                </div>              
                    
                
            </div>     
   

            <div class="col-md-1 col-xs-12  "><button type='button' title="Buscar Proyecto" class="btn btn-primary" data-toggle='modal' data-target='#searchProyecto' style="width: 100%"><b>...</b></button></div>

        </div>    

        <br>     

       <!-- <div class="row ">
            <div class="col-lg-12 col-xs-12 clsTitulo">
                Información General
            </div>
        </div> --> 
         
        
        
        <div class="row clsPadding">
            <div class="col-lg-1 col-xs-12">Nombre del Proyecto:</div>
            <div class="col-lg-5 col-xs-12 ">
                {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','placeholder'=>'Nombre de Proyecto',$readonly)) !!} 
            </div>
            <div class="col-lg-1 col-xs-12">Portafolio de Servicios:</div>
                <div class="col-lg-2 col-xs-12">
                    {!! Form::select('servicio',(isset($servicio)==1?$servicio:array()),(isset($proyecto->cservicio)==1?$proyecto->cservicio:''),array('class' => 'form-control','id'=>'servicio',$disabled)) !!}    
            </div>
        
             <div class="col-lg-1 col-xs-12">Tipo de proyecto:</div>
                <div class="col-lg-2 col-xs-12">

                                     
                    {!! Form::select('tipoproyecto',(isset($tipoproyecto)==1?$tipoproyecto:array()),(isset($proyecto->ctipoproyecto)==1?$proyecto->ctipoproyecto:''),array('class' => 'form-control','id'=>'tipoproyecto',$disabled)) !!}   
            </div>

        </div>

        <div class="row clsPadding">
            <div class="col-lg-1 col-xs-12">Descripción del Proyecto:</div>
            <div class="col-lg-5 col-xs-12">
              {!! Form::textarea('descripcionproyecto',(isset($proyecto->descripcion)==1?$proyecto->descripcion:''),array('class'=>'form-control input-sm','placeholder'=>'Descripcion de Proyecto','rows'=> '6',$readonly)) !!}  
            </div>

            <div class="col-lg-1 col-xs-12">Código de Propuesta:</div>
            <div class="col-lg-2 col-xs-12">
             {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)?$propuesta->ccodigo:''),array('class'=> 'form-control input-sm','readonly'=>'true')) !!}
            </div>

            <div class="col-lg-1 col-xs-12">Código de Proyecto:</div>
            <div class="col-lg-2 col-xs-12 ">
             {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('class'=> 'form-control input-sm',$readonly)) !!}
            </div>

            <br><br><br>

            <div class="col-lg-1 col-xs-12">Fecha Adjudicación Propuesta:</div>
            <div class="col-lg-2 col-xs-12">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fadjudicacion',(isset($fAdjud )?$fAdjud:''), array('class'=>'form-control pull-right ','id'=>'fadjudicacion',$readonly)) !!}</div>
            </div>

            <div class="col-lg-1 col-xs-2">Estado:</div>
            <div class="col-lg-2 col-xs-12">
                  {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control',$disabled)) !!}    
            </div>
            
            <br><br><br>
            <div class="col-lg-1 col-xs-12">Tipo de gestión de proyectos:</div>
            <div class="col-lg-2 col-xs-12">
                {!! Form::select('tipgestionproy',(isset($tipogestionproyecto)==1?$tipogestionproyecto:array()),(isset($proyecto->ctipogestionproyecto)==1?$proyecto->ctipogestionproyecto:''),array('class' => 'form-control','id'=>'tipgestionproy',$disabled)) !!}    
            </div>

            <div class="col-lg-1 col-xs-12">Cliente final:</div>
            <div class="col-lg-2 col-xs-12">
                {!! Form::select('clientefinal',(isset($tcliente_final)==1?$tcliente_final:array()),(isset($clientefin)==1?$clientefin:''),array('class' => 'form-control','id'=>'clientefinal',$disabled)) !!}    
            </div>


            
        </div>   
    
        <div class="row clsPadding">
            <div class="col-lg-1 col-xs-12">Descripción web:</div>
            <div class="col-lg-5 col-xs-12 ">
                {!! Form::text('descweb',(isset($proy_adicional->descripcionweb)==1?$proy_adicional->descripcionweb:''),array('class'=>'form-control input-sm','placeholder'=>'Descripción web',$readonly)) !!} 
            </div>

            <div class="col-lg-1 col-xs-12">Lugar de trabajo:</div>
            <div class="col-lg-2 col-xs-12">
                {!! Form::select('lugartrabajo',(isset($lugartrabajo)==1?$lugartrabajo:array()),(isset($proy_adicional->clugartrabajo)==1?$proy_adicional->clugartrabajo:''),array('class' => 'form-control','id'=>'lugartrabajo',$disabled)) !!}    
            </div>

            <div class="col-lg-1 col-xs-12">Genera ingresos:</div>
            <div class="col-lg-2 col-xs-12">
                {!! Form::select('tipoproy',(isset($tipoproy)==1?$tipoproy:array() ),(isset($proyecto->tipoproy)==1?$proyecto->tipoproy:''),array('class' => 'form-control','id'=>'tipoproy')) !!} 
            </div>
            

        </div>   


        <div class="row clsPadding">

            <div class="col-lg-6 col-xs-12 list-group active" style="font-size:12px">
                <div class="input-group-addon">
                    <center>
                        <span href="#" class="list-group-item list-group-item-action" style="background-color:rgba(180, 180, 180, 1)"><b>Planificado</b></span>
                        <br>

                        <div class="col-lg-12 col-xs-12">
                            
                            <div class="input-group col-lg-6  pull-left">
                                       
                                <span class="input-group-addon"><label class="col-form-label">Fecha Inicio:</label></span>
                               
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {!! Form::text('finicio',(isset($feiniPla)?$feiniPla:''), array('class'=>'form-control pull-right ','id'=>'finicio',$readonly)) !!}
                                    </div>
                               
                            </div>

                            <div class="input-group col-lg-6 pull-right">

                                <span class="input-group-addon"><label class="col-form-label">Fecha Cierre:</label></span>
                               
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                         {!! Form::text('fcierre',(isset($fcierrePla)?$fcierrePla:''), array('class'=>'form-control pull-right ','id'=>'fcierre',$readonly)) !!}
                                    </div>
                                
                            </div>
                            
                        </div>


                    </center>
                </div>
            </div>


            <div class="col-lg-6 col-xs-12 list-group active" style="font-size:12px">
                <div class="input-group-addon">
                    <center>
                        <span href="#" class="list-group-item list-group-item-action" style="background-color:rgba(180, 180, 180, 1)"><b>Real</b></span>
                        <br>

                        <div class="col-lg-12 col-xs-12">
                            
                            <div class="input-group col-lg-6  pull-left">
                                       
                                <span class="input-group-addon"><label class="col-form-label">Fecha Inicio:</label></span>
                               
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                        {!! Form::text('finicioreal',(isset($feiniReal)?$feiniReal:''), array('class'=>'form-control pull-right','id'=>'finicioreal',$readonly)) !!}
                                    </div>
                                
                            </div>

                            <div class="input-group col-lg-6 pull-right">

                                <span class="input-group-addon"><label class="col-form-label">Fecha Cierre:</label></span>
                               
                                    <div class="input-group date">
                                        <div class="input-group-addon">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                         {!! Form::text('fcierrereal',(isset($fcierreReal)?$fcierreReal:''), array('class'=>'form-control pull-right','id'=>'fcierrereal',$readonly)) !!}
                                    </div>
                                
                            </div>
                            
                        </div>


                    </center>
                </div>
            </div>


        </div>
        

        <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>

        <br>


        <div class="col-md-9" >
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#lideres" data-toggle="tab">Información General</a></li>
                  <li><a href="#comercial" data-toggle="tab">Información Contacto</a></li>
                  <li><a href="#presupuesto" data-toggle="tab">Información Presupuesto</a></li>
                </ul>
            </div>

            <div class="tab-content">
                <div class="active tab-pane" id="lideres">
                    <div class="col-lg-6 col-xs-12">
                        <div class="row">

                            <div class="col-lg-4 col-xs-12"> Gerente de Proyecto:</div>
                            <div class="col-lg-4 col-xs-12 clsPadding">      
                                   
                                {!! Form::select('cper_gerente',(isset($personal_gp)==1?$personal_gp:array()),(isset($proyecto->cpersona_gerente)==1?$proyecto->cpersona_gerente:null ),array('class' => 'form-control select-box',$disabled)) !!}
                                

                                <!--<select class="form-control select-box" name="cper_gerent" id="cper_gerent" 
                                <?php if ($estdproy!='001'):
                                 ?>
                                        disabled='true'
                                    
                                <?php endif ?>
                                >

                                @if(isset($personal_gp)==1)
                                <?php 
                                    foreach ($personal_gp as $id => $descripcion):
                                        $gp='';

                                        if ($id==$proyecto->cpersona_gerente){
                                            $gp=' selected="'.$proyecto->cpersona_gerente.'"';

                                        }
                                        echo '<option'.$gp.' value="'.$id.'">'.$descripcion.'</option>';
                                    endforeach;                                   
                                ?>
                                @endif                 

                                
                                 </select> -->

                            </div>                         

                            
                        </div>
               
                        <div class="row">
                            
                            <div class="col-lg-4 col-xs-12"> Control de Proyectos:</div>
                            <div class="col-lg-4 col-xs-12 clsPadding">
                                   
                                {!! Form::select('cp',(isset($personal_cp)==1?$personal_cp:array()),(isset($per_contproy->cpersona)==1?$per_contproy->cpersona:null )
                                ,array('class' => 'form-control select-box','disabled' => 'true')) !!}
                                <!--<input id="new-event" type="text" class="form-control" placeholder="">
                                <div class="input-group-btn">
                                  <a href="#" class="btn btn-primary btn-block "> <b>...</b></a>
                                </div>-->
                                <!-- /btn-group -->

                            </div>                     

                        </div>

                        <div class="row">
                        
                            <div class="col-lg-4 col-xs-12"> Control Documentario:</div>
                            <div class="col-lg-4 col-xs-12 clsPadding">      
                                   
                                {!! Form::select('cd',(isset($personal_cd)==1?$personal_cd:array()),(isset($per_contdoc->cpersona)==1?$per_contdoc->cpersona:null ),array('class' => 'form-control select-box','disabled' => 'true')) !!}
                                <!--<input id="new-event" type="text" class="form-control" placeholder="">
                                <div class="input-group-btn">
                                  <a href="#" class="btn btn-primary btn-block "> <b>...</b></a>
                                </div>-->
                                <!-- /btn-group -->                                    
                            </div>   

                        </div>                  

                    </div>

                    <div class="col-lg-6 col-xs-12" id="tablalideres">
                        
                        <div class="col-lg-18 col-xs-12">Líderes de proyecto</div>                         


                        <div class="col-lg-18 col-xs-12" >
                               @include('partials.tableRDPHR',array('listRDP'=>(isset($listRDP)==1?$listRDP:array() ) ))
                        </div>
                        
                    </div>



                </div>


                
                <div class="tab-pane" id="comercial">

                    <div class="col-lg-9 col-xs-12">
                        <div class="row clsPadding2">
                        <div class="col-lg-2 col-xs-12">Contacto:</div>
                        <div class="col-lg-6 col-xs-8">
                        {!! Form::hidden('cunidadmineracontacto',(isset($uminerac->cunidadmineracontacto)==1?$uminerac->cunidadmineracontacto:''),array('id'=>'cunidadmineracontacto')) !!}
                        {!! Form::text('contacto',(isset($uminerac->apaterno)==1?$uminerac->apaterno." ".$uminerac->amaterno." ".$uminerac->nombres :''),array('id'=>'contacto', 'disabled'=>'true','class'=>'form-control') ) !!}
                        </div>
                        <!--<div class="col-lg-2 col-xs-2">
                                <button type="button" data-toggle="modal" data-target="#searchContacto" class="btn btn-primary btn-block "> <b>...</b></button>
                        </div>-->

                        <div class="col-lg-2 col-xs-2"  >
                                <button type="button" data-toggle="modal" data-target="#divUmineraContacto" class="btn btn-primary btn-block " style="display: {{$hidden}}"> <b>...</b></button>
                        </div>

                        <div class="col-lg-2 col-xs-2">
                                <!--<a href="#" class="btn btn-primary btn-block "> <b>+</b></a>-->
                        </div>
                        </div>
                        <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">
                        <div class="col-lg-2 col-xs-2">Cargo:</div>
                        <div class="col-lg-10 col-xs-10">
                            {!! Form::select('cargo',(isset($tcargos)==1?$tcargos:array()),(isset($uminerac->ccontactocargo)==1?$uminerac->ccontactocargo:null), array('id'=>'cargo','disabled'=>'true','class'=>'form-control') ) !!}
                        </div>            
                        </div>
                        <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Teléfono:</div>
                            <div class="col-lg-4 col-xs-4">
                                {!! Form::text('telecontac',(isset($uminerac->telefono)==1?$uminerac->telefono:''),array('id'=>'telecontac', 'disabled'=>'true','class'=>'form-control') ) !!}
                             </div>
                            <div class="col-lg-2 col-xs-2">Anexo:</div>
                            <div class="col-lg-4 col-xs-4">
                                {!! Form::text('anexocontac',(isset($uminerac->anexo)==1?$uminerac->anexo:''),array('id'=>'anexocontac', 'disabled'=>'true','class'=>'form-control') ) !!}
                             </div>                 
                        </div>
                        <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Cel. 1:</div>
                            <div class="col-lg-4 col-xs-4">
                                {!! Form::text('cel1',(isset($uminerac->cel1)==1?$uminerac->cel1:''),array('id'=>'cel1_c', 'disabled'=>'true','class'=>'form-control') ) !!}
                             </div>
                            <div class="col-lg-2 col-xs-2">Cel. 2:</div>
                            <div class="col-lg-4 col-xs-4">
                                {!! Form::text('cel2',(isset($uminerac->cel2)==1?$uminerac->cel2:''),array('id'=>'cel2_c', 'disabled'=>'true','class'=>'form-control') ) !!}
                             </div>                 
                        </div>
                        <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Email:</div>
                            <div class="col-lg-10 col-xs-10">
                                    {!! Form::text('email',(isset($uminerac->email)==1?$uminerac->email:''),array('id'=>'emailc', 'disabled'=>'true','class'=>'form-control') ) !!}
                            </div>
                        </div>            
                    <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">


                            <div class="col-lg-2 col-xs-2">Razón Social:</div>
                            <div class="col-lg-5 col-xs-5">
                                    <input class="form-control input-sm" type="text" placeholder="Razón Social" value="{{ isset($persona_cliente->razonsocial)==1?$persona_cliente->razonsocial:'' }}" disabled>
                            </div>                
                            <div class="col-lg-2 col-xs-2">RUC:</div>
                            <div class="col-lg-3 col-xs-3">
                                    <input class="form-control input-sm" type="text" placeholder="RUC" value="{{ isset($persona_cliente->identificacion)==1?$persona_cliente->identificacion:'' }}" disabled>
                            </div>
                        </div>  
                    <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Dirección Fiscal:</div>
                            <div class="col-lg-5 col-xs-5">
                                    {!! Form::text('direccion_cli',(isset($direcli)==1?$direcli:''),array('id'=>'direccion_cli','class'=>'form-control','disabled'=>'true') ) !!}
                            </div>                
                            <div class="col-lg-2 col-xs-2">Teléfono:</div>
                            <div class="col-lg-3 col-xs-3">
                                    {!! Form::text('telefono_cli',(isset($persona_cliente->telefono)==1?$persona_cliente->telefono:''),array('id'=>'telefono_cli', 'disabled'=>'true','class'=>'form-control') ) !!}
                            </div>
                        </div>  
                    <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Documento de Aprobación:</div>
                            <div class="col-lg-4 col-xs-10">
                               {!! Form::select('docaproba',(isset($tdocu)==1?$tdocu:array()),(isset($prop_infoad->cdocaprobacion)==1?$prop_infoad->cdocaprobacion:null), array('id'=>'docaproba','class'=>'form-control','disabled'=>'true') ) !!}
                            </div>
                           
                            <div class="col-lg-2 col-xs-2">Número de Documento</div>
                            <div class="col-lg-4 col-xs-10">
                                    {!! Form::text('nrodocaproba',(isset($prop_infoad->nrodocaprobacion)==1?$prop_infoad->nrodocaprobacion:''),array('id'=>'nrodocaproba','class'=>'form-control','disabled'=>'true') ) !!}
                            </div>                
                        </div>                                    
                    <!--  +++++++++++++++++++++++   -->   
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Comentarios.:</div>
                            <div class="col-lg-10 col-xs-10">
                                <!--<textarea class="form-control" rows="3" placeholder="Comentarios" disabled></textarea> -->
                                {!! Form::textarea('comentarios',(isset($proy_adicional->comentarioshr)==1?$proy_adicional->comentarioshr:''),array('id'=>'comentarios', 'class' => 'form-control','rows'=>'3',$readonly) ) !!}
                            </div>
                        </div>  
                      <!--  <div class="row clsPadding2">
                        <div class="col-lg-2 col-xs-12">Imagen Proyecto:</div>
                            <div class="col-lg-6 col-xs-8">
                                    <input class="form-control input-sm" type="text" placeholder="Ruta imagen" disabled>
                            </div>
                            <div class="col-lg-2 col-xs-2">
                                    <a href="#" class="btn btn-primary btn-block "> <b>Ver</b></a>
                            </div>
                            <div class="col-lg-2 col-xs-2">
                                    <a href="#" class="btn btn-primary btn-block "> <b>...</b></a>
                            </div>
                        </div> -->
                    
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Foto Proyecto (link ruta).:</div>
                            <div class="col-lg-10 col-xs-10">
                                <!--<textarea class="form-control" rows="3" placeholder="Comentarios" disabled></textarea> -->
                                {!! Form::text('fotoproyhr',(isset($proy_adicional->fotoproyhr)==1?$proy_adicional->fotoproyhr:''),array('id'=>'comentarios', 'class' => 'form-control','rows'=>'3',$readonly) ) !!}
                            </div>
                        </div>  

                        <!--  +++++++++++++++++++++++   -->
                        <div class="row clsPadding2">
                            <div class="col-lg-2 col-xs-2">Observaciones:</div>
                            <div class="col-lg-10 col-xs-10">
                                <!--<textarea class="form-control" rows="3" placeholder="Observaciones" disabled></textarea> -->
                                {!! Form::textarea('observaciones',(isset($proy_adicional->observacionhr)==1?$proy_adicional->observacionhr:''),array('id'=>'observaciones', 'class' => 'form-control','rows'=>'3',$readonly) ) !!}
                            </div>
                        </div>        
                                     
                    </div>
                     
                </div>


                <div class="tab-pane" id="presupuesto">

                    <div class="col-lg-8 col-xs-12">
                        <div class="row clsPadding2">
                        <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-striped" >
                        <!--<caption class="clsCabereraTabla"><b> Presupuesto </b></caption>-->
                        <thead>
                        <tr class="clsCabereraTabla">
                          <th style="text-align:center !important;">Presupuesto</th>
                          <th style="text-align:center !important;">Anddes</th>
                          <th style="text-align:center !important;">Sub Cont 01</th>
                          <th style="text-align:center !important;">Sub Cont 02</th>
                          <th style="text-align:center !important;">Total Presupuesto</th>              
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Moneda</td>
                            <td>
                                {!! Form::text('moneda',(isset($monedas->descripcion)?$monedas->descripcion:''),array('class'=> 'form-control input-sm','readonly'=>'true')) !!}
                            </td>

                            <td>
                                {!! Form::text('moneda',(isset($monedas->descripcion)?$monedas->descripcion:''),array('class'=> 'form-control input-sm','readonly'=>'true')) !!}
                            </td>

                            <td>
                                {!! Form::text('moneda',(isset($monedas->descripcion)?$monedas->descripcion:''),array('class'=> 'form-control input-sm','readonly'=>'true')) !!}
                            </td>

                            <td>
                                {!! Form::text('moneda',(isset($monedas->descripcion)?$monedas->descripcion:''),array('class'=> 'form-control input-sm','readonly'=>'true')) !!}
                            </td>

                            
                           
                        </tr>
                        <tr style="text-align:center !important;">
                            <td>Honorarios</td>
                            <td>
                                {!! Form::text('hon_and',(isset($proy_adicional->hon_and)?$proy_adicional->hon_and:'0.00'),array('class'=> 'form-control input-sm','id'=>'hon_and','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('hon_con1',(isset($proy_adicional->hon_con1)?$proy_adicional->hon_con1:'0.00'),array('class'=> 'form-control input-sm','id'=>'hon_con1','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('hon_con2',(isset($proy_adicional->hon_con2)?$proy_adicional->hon_con2:'0.00'),array('class'=> 'form-control input-sm','id'=>'hon_con2','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('tot_hon',(isset($totalHon)?$totalHon:'0.00'),array('class'=> 'form-control input-sm','id'=>'tot_hon','readonly'=>'true')) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>Gastos</td>
                            <td>
                                {!! Form::text('gast_and',(isset($proy_adicional->gast_and)?$proy_adicional->gast_and:'0.00'),array('class'=> 'form-control input-sm','id'=>'gast_and','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('gast_con1',(isset($proy_adicional->gast_con1)?$proy_adicional->gast_con1:'0.00'),array('class'=> 'form-control input-sm','id'=>'gast_con1','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('gast_con2',(isset($proy_adicional->gast_con2)?$proy_adicional->gast_con2:'0.00'),array('class'=> 'form-control input-sm','id'=>'gast_con2','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('tot_gast',(isset($totalGast)?$totalGast:'0.00'),array('class'=> 'form-control input-sm','id'=>'tot_gast','readonly'=>'true')) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>Laboratorios</td>
                            <td>
                                {!! Form::text('lab_and',(isset($proy_adicional->lab_and)?$proy_adicional->lab_and:'0.00'),array('class'=> 'form-control input-sm','id'=>'lab_and','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('lab_con1',(isset($proy_adicional->lab_con1)?$proy_adicional->lab_con1:'0.00'),array('class'=> 'form-control input-sm','id'=>'lab_con1','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('lab_con2',(isset($proy_adicional->lab_con2)?$proy_adicional->lab_con2:'0.00'),array('class'=> 'form-control input-sm','id'=>'lab_con2','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('tot_lab',(isset($totalLab)?$totalLab:'0.00'),array('class'=> 'form-control input-sm','id'=>'tot_lab','readonly'=>'true')) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>Otros</td>
                            <td>
                                {!! Form::text('otr_and',(isset($proy_adicional->otr_and)?$proy_adicional->otr_and:'0.00'),array('class'=> 'form-control input-sm','id'=>'otr_and','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('otr_con1',(isset($proy_adicional->otr_con1)?$proy_adicional->otr_con1:'0.00'),array('class'=> 'form-control input-sm','id'=>'otr_con1','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('otr_con2',(isset($proy_adicional->otr_con2)?$proy_adicional->otr_con2:'0.00'),array('class'=> 'form-control input-sm','id'=>'otr_con2','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('tot_otr',(isset($totalOtr)?$totalOtr:'0.00'),array('class'=> 'form-control input-sm','id'=>'tot_otr','readonly'=>'true')) !!}
                            </td>
                        </tr>
                        <tr>
                            <td>Descuento</td>
                            <td>
                                {!! Form::text('desc_and',(isset($proy_adicional->desc_and)?$proy_adicional->desc_and:'0.00'),array('class'=> 'form-control input-sm','id'=>'desc_and','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('desc_con1',(isset($proy_adicional->desc_con1)?$proy_adicional->desc_con1:'0.00'),array('class'=> 'form-control input-sm','id'=>'desc_con1','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('desc_con2',(isset($proy_adicional->desc_con2)?$proy_adicional->desc_con2:'0.00'),array('class'=> 'form-control input-sm','id'=>'desc_con2','onKeyUp'=>'calcularTotFila()','onkeypress'=>'return soloNumeros(event,this)',$readonly)) !!}
                            </td>
                            <td>
                                {!! Form::text('tot_desc',(isset($totalDesc)?$totalDesc:'0.00'),array('class'=> 'form-control input-sm','id'=>'tot_desc','readonly'=>'true')) !!}
                            </td>
                        </tr>
                         
                        <tr class="clsCabereraSubItem">
                            <td>Total</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr> 
                        <!--<tr>
                            <td>SOC 1</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr> 
                        <tr>
                            <td>SOC 2</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>SOC 3</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>-->
                        <tr class="clsCabereraSubItem">
                            <td>Total</td>
                            <td><span id="totAnddes"><?php echo (isset($totalAnddes)==1?$totalAnddes:'0.00');?></span></td>
                            <td><span id="totSubC1"><?php echo (isset($totalSubCont1)==1?$totalSubCont1:'0.00');?></span></td>
                            <td><span id="totSubC2"><?php echo (isset($totalSubCont2)==1?$totalSubCont2:'0.00');?></span></td>
                            <td><span id="totPresup"><?php echo (isset($totalPresupuesto)==1?$totalPresupuesto:'0.00');?></span></td>
                        </tr>                                
                                                                                  
                        </tbody>
                        </table>            
                        </div>
                        </div>
                        <div class="row clsPadding2">
                        <div class="col-lg-3 col-xs-3">
                            Adelanto(%):
                        </div>
                        <!--<div class="col-lg-1 col-xs-1">
                            <input type="checkbox" name="chkadelanto" value="1">
                        </div>-->
                        <div class="col-lg-9 col-xs-9">
                        {!! Form::text('adelanto',(isset($proy_adicional->porcentajeadelanto)==1?$proy_adicional->porcentajeadelanto:'' ),array('id'=>'adelanto', 'placeholder'=>'Porcentaje de adelanto','class'=>'form-control',$readonly)) !!}
                        </div>
                        </div>
                        <div class="row clsPadding2">
                        <div class="col-lg-3 col-xs-3">
                            Tipo de contrato:
                        </div>
                        <div class="col-lg-9 col-xs-9">
                              {!! Form::select('cformacotizacion',(isset($tforma)==1?$tforma:array()),(isset($proy_adicional->cformacotizacion)==1?$proy_adicional->cformacotizacion:null ),array('class' => 'form-control',$disabled)) !!}
                        </div>
                        </div> 
                        <div class="row clsPadding2">
                        <div class="col-lg-3 col-xs-3">
                            Plazo de Pago de Facturas
                        </div>
                        <div class="col-lg-9 col-xs-9">
                              {!! Form::text('plazopagodias',(isset($proy_adicional->plazopagodias)==1?$proy_adicional->plazopagodias:'' ),array('id'=>'plazopagodias', 'placeholder'=>'Cantidad de días de plazo','class'=>'form-control',$readonly )) !!}
                        </div>
                        </div> 
                        <div class="row clsPadding2">
                            <div class="col-lg-3 col-xs-3">
                                Fecha de Corte:
                            </div>
                            <div class="col-lg-9 col-xs-9">   
                                {!! Form::text('diascorte',(isset($proy_adicional->diascorte)?$proy_adicional->diascorte:''), array('class'=>'form-control','id'=> 'diascorte', 'placeholder'=>'Fecha de corte: ej. 15 de cada mes', $readonly)) !!} 
                            </div>

                        </div>                        
                        
                    </div>

                </div>
                
            </div>

        </div>

                    
    <br>
    <br>

    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">
            <button type="button" class="btn btn-primary btn-block" style="display: {{$hidden}} " 

            @if(isset($documento))
                @if($documento->revision=='')
                     
                        onclick="saveHR()" 

                @else      
                        onclick="vermodalAlertRev()"  
                @endif

            @else

            onclick="saveHR()"

            @endif
             
                @if(isset($editar))
                    @if($editar==true)
                    @else
                        disabled   
                            @endif
                    @else 
                        disabled
                @endif 
                      ><b>Grabar</b></button> 
        </div>
        <div class="col-lg-3 col-xs-3">
            <!--<a href="#" class="btn btn-primary btn-block"><b>Cancelar</b></a> -->
        </div>
        <div class="col-lg-3 col-xs-3">
              <button type="button"  id="btnPrint" class="btn btn-primary btn-block" style="display: {{$hidden}} " 
              @if(isset($editar))
                    @if($editar==true)
                    @else
                        disabled   
                            @endif
                    @else 
                        disabled
                @endif 
                ><b>Imprimir</b></button> 
        </div>
        <!-- <div class="col-lg-3 col-xs-3">
            @if((isset($correo_inicio)?$correo_inicio:'')=='') 
              <button type="button"  onclick="enviar_correo()" class="btn btn-primary btn-block" style="display: {{$hidden}} " 
              @if(isset($editar))
                    @if(($editar==true && (isset($proyecto->codigo)?$proyecto->codigo:'')>0) )
                    @else
                        disabled   
                            @endif
                    @else 
                        disabled
                @endif 
                ><b>Correo de Inicio</b>
            </button> 
            @endif
        </div> -->
        <!--
        <div class="col-lg-3 col-xs-3">
                  <select class="form-control" >
                    <option value="xls">XLS</option>
                    <option value="pdf">PDF</option>
                  </select> 
        </div>   -->      
    </div>








    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->

    {!! Form::close() !!}
    </section>      
    <!-- /.content -->
    


    @include('partials.modalRegistroUnidadMinera')
    @include('partials.modalRegistroContactoUnidadMinera')
    @include('partials.searchProyecto')

    @include('partials.modalAprobaciones')

    @include('partials.searchContacto')
  </div>

   <!-- ******************** REVISIONES ******************************* -->
    @include('partials.modalAlertAgregarRevisiones')

    @include('partials.modalAgregarRevisiones')
    <!-- *************************************************** -->


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script src="js/clientes.js"></script>
<script>

    function enviar_correo(){
        swal({
              title: "Enviar correo de inicio",
              // text: "Se enviara un correo al grupo <a href='mailto:inicio.proyectos@anddes.com' ><b>inicio.proyectos@anddes.com</b> </a>",
              text: "Se enviara un correo al grupo <a><b>inicio.proyectos@anddes.com</b> </a>",
              type: "info",
              html: true,
              showCancelButton: true,
              closeOnConfirm: false,
              showLoaderOnConfirm: true,
              confirmButtonText: "Enviar",
            }, function () {
              setTimeout(function () {
                // inicio de ajax de cooreo
                    
                    if ($('#codigoproyecto').val()=='') {
                        // swal("Good job!", "You clicked the button!", "success")
                    }else{
                        $.ajax({
                            url: 'enviarCorreoIni',
                            type: 'POST',
                            data: $('#frmproyecto').serialize(),
                        })
                        .done(function(data) {
                            console.log("enviarCorreoIni");
                            $('#div_carga').hide(); 
                            $("#resultado").html(data);
                        })
                        .fail(function() {
                            console.log("error");
                        })
                        .always(function() {
                            console.log("complete");
                        });
                    }
                    swal("Envio finalizado!");
                  }, 5000);
                // fin de ajax de cooreo
            });
    }

        var selected =[];
        var selected_ccontacto =[];
        $.fn.dataTable.ext.errMode = 'throw';        
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listaPryTodos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'GteProyecto' , name : 'p.abreviatura'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'},
                {data : 'descripcion' , name : 'te.descripcion'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_ (_MAX_ proyectos)",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     

        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });


        
        function goEditar(){
          var id =0;
          var valor='lec';
          if (selected.length > 0 ){
              id = selected[0];
              valor = $('#valor').val()
              getUrl('editarHR/'+valor+'/'+id.substring(4));
          }else{
              $('.alert').show();
          }
        }


        /* Buscar Contacto*/
        var table2=$("#tContacto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "buscarContactos/"+ $("#cproyecto").val(),
                "type": "GET",
                "data" : "cproyecto="+ $("#cproyecto").val(),
            },
            "columns":[
                {data : 'apaterno', name: 'umc.apaterno'},
                {data : 'amaterno', name: 'umc.amaterno'},
                {data : 'nombres' , name : 'umc.nombres'},
                {data : 'descripcion', name : 'con.descripcion'}


            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tContacto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected_ccontacto);
            table2.$('tr.selected').removeClass('selected');
            selected_ccontacto.splice(0,selected_ccontacto.length);
            if ( index === -1 ) {
                selected_ccontacto.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchContacto').on('hidden.bs.modal', function () {
          // goObtenerContacto();
        });



        function goObtenerContacto(id){

            alert(id);
          //var id =0;
          if (selected_ccontacto.length > 0 ){
              id = selected_ccontacto[0];
              //alert(id.substring(7));
              //getUrl('editarHR/'+id.substring(4),'');
              $.ajax({
                type : "GET",
                url : "getContacto/"+id.substring(7),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){ 
                     $('#divUmineraContacto').hide(); 
                     
                     var contacto = data;
                     $("#cunidadmineracontacto").val(contacto[0].unidadmineracontacto);
                     $("#contacto").val(contacto[0].apaterno + ' '+ contacto[0].amaterno + ' ' + contacto[0].nombres);
                    
                     //alert(contacto[0].apaterno);

                },
                error: function(data){
                    $('#div_carga').hide();
                }
              });
          }else{
              //$('.alert').show();
          }
        }

        $('#divUmineraContacto').on('show.bs.modal',function(e){

            contactoUminera($('#cunidadminera_proy').val());
         
          }); 


         function contactoUminera(id){
           
          //limpiarUmineraContacto();
         
          $.ajax({
            url: 'verUmineraContacto/'+id,
            type:'GET',
              beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){  
            //'#divUmineraContacto').modal({backdrop: "static"});
              $('#divMineraCon').html(data);  
              $('#cuminera_con').val(id);     

             //          

              },
          });

          $.ajax({
            url: 'editUmineraCliente/'+id,
            type:'GET',
              beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){

              var str = JSON.stringify(data);

              var pushedData = jQuery.parseJSON(str);
              //$('#div_carga').hide(); 
              console.log(pushedData['cunidadminera']);

              var nombre = pushedData['nombre'];  

              $('#nombre_umc').val(nombre);
              $("#nombre_umc").attr("readonly","readonly"); 
             
              
            },
          });
        
           
        }

        /* Agregar Unidad Minera de Contacto*/

        
         $("#btnAddContactos").click(function(e){

          $.ajax({
            url: 'AddContactosCliente',
            type:'POST',
            data: $('#frmcuminera').serialize() ,
              beforeSend: function () {
                  
                  //$('#div_carga').show(); 

          
              },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divMineraCon').html(data);

              limpiarUmineraContacto();
            },
          });
        })

        //Fin de Agregar Unidad Minera Contactos

         function eliminarUmineraCon(id){


          $.ajax({
            url: 'eliminarUmineraContacto/' + id,
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show(); 

        
            },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divMineraCon').html(data);
              limpiarUmineraContacto();
            },
          });          

        }

        function editarUmineraCon(id){

          $.ajax({
            url: 'editUmineraContacto/'+id,
            type:'GET',
              beforeSend: function () {
                       
              },              
            success : function (data){

              var str = JSON.stringify(data);

              var pushedData = jQuery.parseJSON(str);

              console.log(pushedData['cunidadmineracontacto']);     
              
              $("#cunidadminera_c").val(pushedData['cunidadmineracontacto']);
              $("#cuminera_con").val(pushedData['cunidadminera']);
              $("#apaterno").val(pushedData['apaterno']);
              $("#amaterno").val(pushedData['amaterno']);
              $("#nombres").val(pushedData['nombres']);
              $("#email").val(pushedData['email']);
              $("#telefono").val(pushedData['telefono']);
              $("#cel1").val(pushedData['cel1']);
              $("#cel2").val(pushedData['cel2']);              
              $("#anexo").val(pushedData['anexo']);
             
              if (pushedData['ctipocontacto']==null) {                 
              
                $("#ctipocontacto").val(0);               
              }
            
              else{              

               $("#ctipocontacto").val(pushedData['ctipocontacto']);            

              }      

               if (pushedData['ccontactocargo']==null) {                 
              
                $("#ccontactocargo").val(0);               
              }
            
              else{              

               $("#ccontactocargo").val(pushedData['ccontactocargo']);            

              }                       
             
              
            },
          });
        }


        function limpiarUmineraContacto(){

          $("#cunidadminera_c").val('');
          $("#apaterno").val('');
          $("#amaterno").val('');
          $("#nombres").val('');
          $("#email").val('');
          $("#telefono").val('');
          $("#cel1").val('');
          $("#cel2").val('');              
          $("#anexo").val('');
          $("#ctipocontacto").val(0);   
          $("#ccontactocargo").val(0);    
  
        }

        function agregarContHR(id){

            $.ajax({
            url: 'editUmineraContacto/'+id,
            type:'GET',
              beforeSend: function () {
                       
              },              
            success : function (data){

              $('#divUmineraContacto').hide(); 
              $('.modal-backdrop').hide()

              var str = JSON.stringify(data);

              var pushedData = jQuery.parseJSON(str);

              console.log(pushedData['cunidadmineracontacto']);     
              
              $("#cunidadmineracontacto").val(pushedData['cunidadmineracontacto']);
             
              var nombrecont=pushedData['nombres']+' '+pushedData['apaterno']+' '+pushedData['amaterno']

              $("#contacto").val(nombrecont);
              //$("#amaterno").val(pushedData['amaterno']);
              //$("#nombres").val(pushedData['nombres']);
              $("#emailc").val(pushedData['email']);
              $("#telecontac").val(pushedData['telefono']);
              $("#cel1_c").val(pushedData['cel1']);
              $("#cel2_c").val(pushedData['cel2']);              
              $("#anexocontac").val(pushedData['anexo']);
             
              if (pushedData['ctipocontacto']==null) {                 
              
                $("#ctipocontacto").val(0);               
              }
            
              else{              

               $("#ctipocontacto").val(pushedData['ctipocontacto']);            

              }


               if (pushedData['ccontactocargo']==null) {                 
              
                $("#cargo").val(0);               
              }
            
              else{              

               $("#cargo").val(pushedData['ccontactocargo']);            

              }                       
             
              
            },
          });

        }

        function calcularTotFila(){

            var totalHon=0;
            var totalGast=0;
            var totalLab=0;
            var totalOtr=0;
            var totalDesc=0;

            totalHon=parseFloat($("#hon_and").val())+parseFloat($("#hon_con1").val())+parseFloat($("#hon_con2").val());
            $("#tot_hon").val(parseFloat(totalHon).toFixed(2));

            totalGast=parseFloat($("#gast_and").val())+parseFloat($("#gast_con1").val())+parseFloat($("#gast_con2").val());
            $("#tot_gast").val(parseFloat(totalGast).toFixed(2));

            totalLab=parseFloat($("#lab_and").val())+parseFloat($("#lab_con1").val())+parseFloat($("#lab_con2").val());
            $("#tot_lab").val(parseFloat(totalLab).toFixed(2));

            totalOtr=parseFloat($("#otr_and").val())+parseFloat($("#otr_con1").val())+parseFloat($("#otr_con2").val());
            $("#tot_otr").val(parseFloat(totalOtr).toFixed(2));

            totalDesc=parseFloat($("#desc_and").val())+parseFloat($("#desc_con1").val())+parseFloat($("#desc_con2").val());
            $("#tot_desc").val(parseFloat(totalDesc).toFixed(2));


            var totalAnddes=0;
            var totalSubCont1=0;
            var totalSubCont2=0;
            var totalPresupuesto=0;

            var totalAnddes=parseFloat($("#hon_and").val())+parseFloat($("#gast_and").val())+parseFloat($("#lab_and").val())+parseFloat($("#otr_and").val())-parseFloat($("#desc_and").val());
            var totalSubCont1=parseFloat($("#hon_con1").val())+parseFloat($("#gast_con1").val())+parseFloat($("#lab_con1").val())+parseFloat($("#otr_con1").val())-parseFloat($("#desc_con1").val());
            var totalSubCont2=parseFloat($("#hon_con2").val())+parseFloat($("#gast_con2").val())+parseFloat($("#lab_con2").val())+parseFloat($("#otr_con2").val())-parseFloat($("#desc_con2").val());
            var totalPresupuesto=totalAnddes+totalSubCont1+totalSubCont2;
            

            $("#totAnddes").html(parseFloat(totalAnddes).toFixed(2));
            $("#totSubC1").html(parseFloat(totalSubCont1).toFixed(2));
            $("#totSubC2").html(parseFloat(totalSubCont2).toFixed(2));
            $("#totPresup").html(parseFloat(totalPresupuesto).toFixed(2));


        }





        /* Fin Buscar Contacto*/
        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });  
        $('#fadjudicacion').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        }); 
        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        $('#finicioreal').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        $('#fcierrereal').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });                        
        /*$('#fechadoc').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });*/

        $('#fechacorte').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });


        $("#btnSI").on("click",function() {
            verModalRevisiones();
         
        });

        $("#btnNO").on("click",function() {
            saveHR('Ninguna');
         
        });

        $("#saveRevision").on("click",function() {
            saveAndPrintHR();
         
        });

        $("#cancelRevision").on("click",function() {
            cerrarModalRevisiones();
         
        });
        
        function vermodalAlertRev(){

            $('#mensajeRev').html('¿Guardar los cambios con una nueva revisión?');

            $('#modalAlertRev').modal({
                backdrop:"static"
            });

        }


        function cerrarModalAlerRev(){
            
            $('#modalAlertRev').modal('hide');
        }

        function verModalRevisiones(){

            $('#modalAlertRev').modal('hide');
            $('#modalRevisiones').modal({
                backdrop:"static"
            });

        }

        function cerrarModalRevisiones(){          

            $('#modalRevisiones').modal('hide');
        }

        function enviarRev(){
            var nuevaRev=$('#nuevarevisionModal').val();

            $('#nuevarevision').val(nuevaRev);

            validarNuevaRevision();
 
        }

        /*function validarNuevaRevision(){
            if ($('#revisiondocumento').val()!='') {

                if ($('#nuevarevision').val()!=''){
                    goPrintHR('1');

                    //saveHR();

                }
            }
        }*/


        function saveHR(){



            var l1 =$('input[name="lider1"]:checked').val();
            var l2 =$('input[name="lider2"]:checked').val();

         

            if( typeof l1 != 'undefined' && typeof l2 != 'undefined'  )

                if (l1==l2 ) {

                  $("#tablalideres").css({"border-color": "#FF0040", 
                                         "border-width":"1px", 
                                         "border-style":"solid"});

                  alert('Los líderes seleccionados no pueden ser iguales.');
                  return;

                  }

            $.ajax({

                type:"POST",
                url:'grabarHR',
                data:$('#frmproyecto').serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                    $('#div_carga').hide(); 
                    $("#resultado").html(data);
                    $("#div_msg").show();
                    $('.modal-backdrop').remove();
                },
                error: function(data){
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show(); 
                }                

            });          
            
            
        }

       
    $('.select-box').chosen(
        {
            allow_single_deselect: true,width:"100%"

    }); 

    $("#btnPrint").on('click',function(){    

        goPrintHR('0');

   
      //goPrintHR();
    });   

    function goPrintHR(almacenar){

        var id =$('#cproyecto').val();

     
        if (id.length > 0 ){

             win = window.open('printHRProy/'+almacenar+'/'+id,'_blank');
           
                 
            
        }else{
            $('.alert').show();
        }

      /*  if(almacenar=='1'){
            saveHR();
        }*/

  }

  function saveAndPrintHR(){
            var id =$('#cproyecto').val();
            var l1 =$('input[name="lider1"]:checked').val();
            var l2 =$('input[name="lider2"]:checked').val();
   

            if( typeof l1 != 'undefined' && typeof l2 != 'undefined'  )

                if (l1==l2 ) {

                  $("#tablalideres").css({"border-color": "#FF0040", 
                                         "border-width":"1px", 
                                         "border-style":"solid"});

                  alert('Los líderes seleccionados no pueden ser iguales.');
                  return;

                  }

            $.ajax({



                type:"GET",
                url:'printHRandSaveProy/'+'1'+'/'+id,
               
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){      
                var nuevaRev=$('#nuevarevisionModal').val();

                     $('#nuevarevision').val(nuevaRev);             
                     
                    $('#div_carga').hide();                                  
                    $('.modal-backdrop').remove();
                    saveHR();


                },
                error: function(data){
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show(); 
                }                

            });          
    } 

            
            

    /*$('div#resumen').hover(function(e) {
      $('div#verRevisionesAnteriores').show().css('top', e.pageY)
      .css('left', e.pageX)
      .appendTo('body');
        },
         function() {
      $('div#verRevisionesAnteriores').hide();
    }); */


  $("#servicio").change(function(){

        if(this.value==''){
                return;
        }

        $.ajax({
            type:'GET',
            url: 'listTipoPry/'+this.value,
            success: function (data) {
                var str = JSON.stringify(data);
                var pushedData = jQuery.parseJSON(str);              
                
                $('#tipoproyecto').find('option').remove();
                $('#tipoproyecto').append($('<option></option>').attr('value','').text(''));
                
                $.each(pushedData, function(i, serverData)
                {
                    $('#tipoproyecto').append($('<option></option>').attr('value',i).text(serverData));
                });
            },
            error: function(data){

            }

        });


    });

     /* Inicio Somnbrear los input al hacer click*/
    $("input[type=text]").focus(function(){    
      this.select();
    });


    function soloNumeros(e,obj) {

            // Backspace = 8, Enter = 13, ’0′ = 48, ’9′ = 57, ‘.’ = 46
            
            var field = obj;

            key = e.keyCode ? e.keyCode : e.which;
            
            if (key == 8 || key ==9 || key ==11 || (key > 47 && key < 58)) return true;
            if (key > 47 && key < 58) {
              if (field.value === "") return true;
              var existePto = (/[.]/).test(field.value);
              
              if (existePto === false){
                  regexp = /.[0-9]{2}$/; //PARTE ENTERA 10
              }
              else {
                regexp = /.[0-9]{2}$/; //PARTE DECIMAL2
              }
              return !(regexp.test(field.value));
            }
            if (key == 46) {
              if (field.value === "") return false;
              regexp = /^[0-9]+$/;
              return regexp.test(field.value);
            }

            return false;

        }  







</script>  

<style>
    
.modal-open {
    overflow: auto;
    padding-right: 0px !important;
   
}



</style>

