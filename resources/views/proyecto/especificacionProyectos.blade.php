<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Especificación de Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoProyecto')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>    
        {!! Form::open(array('url' => 'grabarEspecificacionProyecto','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    
    <div class="col-md-12 col-xs-12">
    	<div class="row">
      
    <div class="col-lg-11 col-xs-11 clsPadding">
      {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
      {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
    </div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    </div>
    
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Unidad minera:</div>
        <div class="col-lg-10 col-xs-10">
            {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
        </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Nombre del Proyecto:</div>
        <div class="col-lg-10 col-xs-10">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!} 
        </div>
    </div>    
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Código de Proyecto:</div>
        <div class="col-lg-4 col-xs-3">
         {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}
        </div>
        <div class="col-lg-2 col-xs-2">Estado:</div>
        <div class="col-lg-4 col-xs-3">
              {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control')) !!}  

   
        </div>
    </div>
    <div class="row clsPadding2">
    	<div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
    	<div class="col-lg-4 col-xs-4">
        	                         <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('finicio',(isset($proyecto->finicio)?$proyecto->finicio:''), array('class'=>'form-control pull-right')) !!}</div>
        </div>
    	<div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>        
    	<div class="col-lg-4 col-xs-4">
        	                       <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('fcierre',(isset($proyecto->fcierre)?$proyecto->fcierre:''), array('class'=>'form-control pull-right')) !!}</div>
        </div>               
    </div>
 <!-- **************************************************** -->   
   <!-- <div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
    Importar:
    </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Tipo de Hoja:</div>
        <div class="col-lg-4 col-xs-4">
          <select class="form-control">
            <option value="hh">Honorarios</option>
            <option value="gas">Gastos</option>
            <option value="lab">Laboratorio</option>
            <option value="rooster">Rooster Construcción</option>        
          </select>
        </div>
        <div class="col-lg-1 col-xs-1">Ruta:</div>
        <div class="col-lg-4 col-xs-4">
          <input class="form-control input-sm" type="text" placeholder="Examinar" >
        </div>
        <div class="col-lg-1 col-xs-1"><a href="#" class="btn btn-primary btn-block "> <b>...</b></a></div>   
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
              <label>Generar nueva revisión: 
                <input type="checkbox">
              </label>
        </div>
         <div class="col-lg-3 col-xs-3">
             <input class="form-control input-sm" type="text" placeholder="">
         </div>
         <div class="col-lg-3 col-xs-3">
             <a href="#" class="btn btn-primary btn-block "> <b>Aceptar</b></a>     
         </div>
    </div>  -->  
<!--  ************************************************** -->     


    <div class="row clsPadding2">
      <div class="col-lg-12 col-xs-12 clsTitulo">
      Actividades:
      </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-12 col-xs-12 table-responsive" id="divActi" >
      @include('partials.tableDetalleActividadPry',array('listaAct' => (isset($listaAct)==1?$listaAct:array()), 'listEstr'=> (isset($listEstr)==1?$listEstr:array() ),'listEstr_apo' => (isset($listEstr_apo)==1?$listEstr_apo:array()), 'listEstr_dis' => (isset($listEstr_dis)==1?$listEstr_dis:array() ),'listDis' => (isset($listDis)==1?$listDis:array() ) ) )
      
        </div>
    </div>
<!--  ***************************************   -->
    <!--<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
    Rooster (Solo para Construcción)
    </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-4">Leyenda:</div>
        <div class="col-lg-2 col-xs-4 clsCampo">Campo</div>
        <div class="col-lg-2 col-xs-4 clsOficina">Oficina</div>
        <div class="col-lg-6  hidden-xs"></div>
    </div>-->
<!--  ***************************************   -->
    <!--<div class="row clsPadding2">
        <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
                <thead>
                <tr class="clsCabereraTabla">
                  <th>Item</th>
                  <th style="min-width:200px!important;">Apellidos Nombre</th>
                  <th style="min-width:150px!important;">Cargo</th>
                  <th style="min-width:85px!important;">01/05/2016</th>
                  <th style="min-width:85px!important;">02/05/2016</th>
                  <th style="min-width:85px!important;">03/05/2016</th>
                  <th style="min-width:85px!important;">A</th>
                  <th style="min-width:85px!important;">A</th>
                  <th style="min-width:85px!important;">A</th>
                  <th style="min-width:85px!important;">A</th>
                  <th style="min-width:85px!important;">A</th>
                  <th style="min-width:85px!important;">A</th>
                  <th style="min-width:85px!important;">Ing.</th>
                  <th style="min-width:85px!important;">B</th>
                </tr> 
                <tr class="clsCabereraSubItem">
                  <th></th>
                  <th></th>
                  <th></th>
                  <th>M</th>
                  <th>M</th>
                  <th>J</th>
                  <th>V</th>
                  <th>S</th>
                  <th>D</th>
                  <th>L</th>
                  <th>M</th>
                  <th>M</th>
                  <th>J</th>
                  <th>V</th>                             
                </tr>                       
                </thead>
                <tbody>
                                                                                      	
                </tbody>
                
                <tfoot>
                <tr class="clsCabereraSubItem">
                    <td></td>
                    <td>Total Entregables / Total Horas</td>                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>                                                                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>                
                </tr>
                <tr class="clsCabereraSubItem">
                    <td></td>
                    <td>Subtotal Costo horas</td>                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>                                                                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>                
                    <td></td>
                    <td></td>
                </tr>            
                </tfoot>
              </table>
      
        </div>
    </div>-->
<!--  ***************************************   -->
    <div class="row clsPadding2">
        <div class="col-lg-9 col-xs-9 clsTitulo">Gastos:</div>
        <div class="col-lg-1 col-xs-1" style="text-align:right;">Acciones:</div>
        <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-target="#divGastos" id="btnAddGasto"> <b>+</b></button></div>
        <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " id="btnDelGasto"> <i class="fa fa-trash-o" aria-hidden="true"></i></button></div>        
    </div>
<!--  *****************************************  -->
    <div class="row clsPadding2">
        <div class="col-lg-12 col-xs-12 table-responsive" id="tableGastos">
          @include('partials.tableGastosProyecto',array('gastos'=> (isset($gastos)==1?$gastos:array() ) ))    
        
        
        </div>
    </div>    
<!-- ****************************************** -->
    <div class="row clsPadding2">
        <div class="col-lg-9 col-xs-9 clsTitulo">Laboratorio:</div>
        <div class="col-lg-1 col-xs-1" style="text-align:right;">Acciones:</div>
        <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-target="#divGastosLab" id="btnAddGastoLab"> <b>+</b></button></div>
        <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block " id="btnDelGastoLab"> <i class="fa fa-trash-o" aria-hidden="true"></i> </button></div>        
    </div>
<!--  *****************************************  -->
    <div class="row clsPadding2">
        <div class="col-lg-12 col-xs-12 table-responsive" id="tableGastosLab">
     @include('partials.tableGastosLabProyecto',array('gastosLab'=> (isset($gastosLab)==1?$gastosLab:array() ) ))   
        
        
        </div>
    </div>    
<!-- ****************************************** -->
    <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding">
        <div class="col-lg-4 col-xs-4">
            <button class="btn btn-primary btn-block" style="width:200px;" ><b>Grabar</b></button> 
        </div>
        <div class="col-lg-4 col-xs-4">
            <!--<a href="#" class="btn btn-primary btn-block" style="width:200px;"><b>Cancelar</b></a> -->
        </div>            
    </div>
   </div>
   </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
   






    {!! Form::close() !!}
    </section>
    <!-- /.content -->

   @include('partials.searchProyecto')

<!-- Modal Editar Datos de Horas-->
      <div id="divEditarHoras" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: yellow">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:black">Registro de Horas de Proyecto</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmhoras')) !!}
              {!! Form::hidden('_token_horas',csrf_token(),array('id'=>'_token_horas')) !!}
              
              {!! Form::hidden('cproyecto_h',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto_h')) !!}            
                <div id="divConteHoras">
                @include('partials.tableInputHorasActividadPry',array('listaAct'=> (isset($listaAct)==1?$listaAct:array()) ,'cestructuraproyecto'=> (isset($cestructuraproyecto)==1?$cestructuraproyecto:''), 'proyecto' => (isset($proyecto)==1?$proyecto:null )   ) )
                </div>
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="save_profesional_senior()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Editar Datos de Horas -->

    <!-- Modal Editar Datos de Horas x Disciplina-->
      <div id="divEditarHorasDis" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: yellow">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:black">Registro de Horas de Propuesta - Disciplina</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmhorasDis')) !!}
              {!! Form::hidden('_token_horas_dis',csrf_token(),array('id'=>'_token_horas_dis')) !!}
              
              {!! Form::hidden('cproyecto_hdis',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto_hdis')) !!}            
                <div id="divConteHorasDis">
                @include('partials.tableInputHorasActividadDisPry',array('listaAct'=> (isset($listaAct)==1?$listaAct:array()) ,'cestructuraproyecto'=> (isset($cestructuraproyecto)==1?$cestructuraproyecto:''), 'proyecto' => (isset($proyecto)==1?$proyecto:null ) ,'cdisciplina'=> (isset($cdisciplina)==1?$cdisciplina:'')  ) )
                </div>
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="save_profesional_disciplina()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Editar Datos de Horas x Disciplina-->

    <!-- Modal Editar Datos de Horas Apoyo-->
      <div id="divEditarHorasApo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: blue">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:black">Registro de Horas de Propuesta</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmhorasApo')) !!}
              {!! Form::hidden('_token_horas_apo',csrf_token(),array('id'=>'_token_horas_apo')) !!}
              
              {!! Form::hidden('cproyecto_hapo',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto_hapo')) !!}            
                <div id="divConteHorasApo">
                @include('partials.tableInputHorasActividadPry',array('listaAct'=> (isset($listaAct)==1?$listaAct:array()) ,'cestructuraproyecto'=> (isset($cestructuraproyecto)==1?$cestructuraproyecto:''), 'proyecto' => (isset($proyecto)==1?$proyecto:null )   ) )
                </div>
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" onclick="save_profesional_apoyo()">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Editar Datos de Horas Apoyo-->

<!-- Modal Seleccionar Gastos-->
      <div id="divGastos" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: blue">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:white">Seleccionar Gastos</h4>
            </div>
            <div class="modal-body">
            <div id="treeGastos">
            </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Seleccionar Gastos-->


  <!-- Modal Seleccionar Gastos Laboratorio-->
      <div id="divGastosLab" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: blue">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:white">Seleccionar Gastos de Laboratorio</h4>
            </div>
            <div class="modal-body">
            <div id="treeGastosLab">
            </div>

            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
            </div>
          </div>
        </div>
      </div>
  <!-- Fin Modal Seleccionar Gastos Laboratorio-->

  </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
        $.fn.dataTable.ext.errMode = 'throw';  
        var selected =[];
        var selected_gastos = [];
        var selected_gastos_lab = [];        
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     

        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });
        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('editarEspecificacionProyecto/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        /* Agregar Eliminar Gastos */
        $("#divGastos").on('shown.bs.modal', function(){
          selected_gastos.splice(0,selected_gastos.length);
          $('#treeGastos').highCheckTree({
            data: getTree(),
            onCheck: function (node){
              selected_gastos.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr('rel'));
              if(idx != -1){
                selected_gastos.splice(idx,1);
              }
            }
          });
        });
        $("#divGastos").on('hidden.bs.modal',function(){
          /*$.each(selected_gastos,function(index,value){
            alert(value);
          });*/
          if(selected_gastos.length <=0){
            return;
          }
          $.ajax({
            type:'GET',
            url: 'addGastosProyecto',
            data: {selected_gastos:selected_gastos},
            success: function(data){
              $("#tableGastos").html(data);
              $('#div_carga').hide();
            },
            error: function(data){
              $('#div_carga').hide();
            },
            beforeSend: function(){
              $('#div_carga').show(); 
            }
          });
        });

        $('#btnDelGasto').click(function(){
            var searchIDs = $("#tableListGastos input:checkbox:checked").map(function(){
              return $(this).val();
            }).toArray();
            console.log(searchIDs);
            $.ajax({
              type: 'GET',
              url :'delGastosProyecto',
              data: {chkselec:searchIDs},
              success :function(data){
                $("#tableGastos").html(data);
                $('#div_carga').hide();
              },
              error : function(data){
                 $('#div_carga').hide();
              },
              beforeSend : function(){
                $('#div_carga').show(); 
              }
            });
        });
        /* Fin Agregar Eliminar Gastos */

        /* Agregar Eliminar Gastos Lab*/
        $("#divGastosLab").on('shown.bs.modal', function(){
          selected_gastos_lab.splice(0,selected_gastos_lab.length);
          $('#treeGastosLab').highCheckTree({
            data: getTreeLab(),
            onCheck: function (node){
              selected_gastos_lab.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr('rel'));
              if(idx != -1){
                selected_gastos_lab.splice(idx,1);
              }
            }
          });
        });
        $("#divGastosLab").on('hidden.bs.modal',function(){
          /*$.each(selected_gastos,function(index,value){
            alert(value);
          });*/
          if(selected_gastos_lab.length <=0){
            return;
          }
          $.ajax({
            type:'GET',
            url: 'addGastosLabProyecto',
            data: {selected_gastos_lab:selected_gastos_lab},
            success: function(data){
              $("#tableGastosLab").html(data);
              $('#div_carga').hide();
            },
            error: function(data){
              $('#div_carga').hide();
            },
            beforeSend: function(){
              $('#div_carga').show(); 
            }
          });
        });

        $('#btnDelGastoLab').click(function(){
            var searchIDs = $("#tableListGastosLab input:checkbox:checked").map(function(){
              return $(this).val();
            }).toArray();
            console.log(searchIDs);
            $.ajax({
              type: 'GET',
              url :'delGastosLabProyecto',
              data: {chkselecLab:searchIDs},
              success :function(data){
                $("#tableGastosLab").html(data);
                $('#div_carga').hide();
              },
              error : function(data){
                 $('#div_carga').hide();
              },
              beforeSend : function(){
                $('#div_carga').show(); 
              }
            });
        });
        /* Fin Agregar Eliminar Gastos Lab*/  
        function getTree() {
          // Some logic to retrieve, or generate tree structure
          var tree = [
          <?php echo (isset($estructura)==1?$estructura:'') ?>
          
          ];          
          return tree;
        }

        function getTreeLab() {
          // Some logic to retrieve, or generate tree structure
          var tree = [
          <?php echo (isset($estructuraLab)==1?$estructuraLab:'') ?>
          
          ];          
          return tree;
        }           

        /* Grabar */
        $("#frmproyecto").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarEspecificacionProyecto',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });     

        function profesional_senior(id){
          $.ajaxSetup({
              header: document.getElementById('_token').value
          });          
          $.ajax({
            type: 'GET',
            url : 'editHorasSeniorProyecto',
            data: "cproyecto="+$("#cproyecto").val()+"&cestructuraproyecto="+id,
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divConteHoras").html(data);
              $("#divEditarHoras").modal();
            },
            error: function(data){
              $('#div_carga').hide();
            }
          });
          
        }
        function save_profesional_senior(){
          $.ajaxSetup({
              header: document.getElementById('_token_horas').value
          });         
          //alert($("#frmhoras").serialize());
          $.ajax({
            type: 'POST',
            url : 'saveHorasSeniorProyecto',
            data: $("#frmhoras").serialize(),
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divEditarHoras").modal('hide');
              verHorasDetalle();
            },
            error: function(data){
               $('#div_carga').hide();
               $("#divEditarHoras").modal('hide');
            }
          });
          
        }

        function profesional_disciplina(id,id_dis){
          $.ajaxSetup({
              header: document.getElementById('_token').value
          });  
          //alert(id_dis);
          $.ajax({
            type: 'GET',
            url : 'editHorasDisciplinaProyecto',
            data: "cproyecto="+$("#cproyecto").val()+"&cestructuraproyecto="+id+"&cdisciplina="+id_dis,
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divConteHorasDis").html(data);
              $("#divEditarHorasDis").modal();
            },
            error: function(data){
              $('#div_carga').hide();
            }
          });
        }
        function save_profesional_disciplina(){
          $.ajaxSetup({
              header: document.getElementById('_token_horas_dis').value
          });         
          
          $.ajax({
            type: 'POST',
            url : 'saveHorasDisciplinaProyecto',
            data: $("#frmhorasDis").serialize(),
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divEditarHorasDis").modal('hide');
              verHorasDetalle();
            },
            error: function(data){
               $('#div_carga').hide();
               $("#divEditarHorasDis").modal('hide');
            }
          });
          
        }        
        function profesional_apoyo(id){
          $.ajaxSetup({
              header: document.getElementById('_token').value
          });          
          $.ajax({
            type: 'GET',
            url : 'editHorasApoyoProyecto',
            data: "cproyecto="+$("#cproyecto").val()+"&cestructuraproyecto="+id,
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divConteHorasApo").html(data);
              $("#divEditarHorasApo").modal();
            },
            error: function(data){
              $('#div_carga').hide();
            }
          });
        }

        function save_profesional_apoyo(){
          $.ajaxSetup({
              header: document.getElementById('_token_horas_apo').value
          });         
          
          $.ajax({
            type: 'POST',
            url : 'saveHorasApoyoProyecto',
            data: $("#frmhorasApo").serialize(),
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divEditarHorasApo").modal('hide');
              verHorasDetalle();
            },
            error: function(data){
               $('#div_carga').hide();
               $("#divEditarHorasApo").modal('hide');
            }
          });
          
        }   

        function verHorasDetalle(){
          $.ajax({
            type: 'GET',
            url : 'verHorasDetalleActividadProyecto/'+$("#cproyecto").val(),
            data: "",
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divActi").html(data);
            },
            error: function(data){
               $('#div_carga').hide();
               
            }
          });          
        }                              
</script>