
<html>
    <head>
        <style>
            /*@page { margin: 60px 60px; }*/
            #footer { position: fixed; right: : 0px; bottom: -120px; right: 0px; height: 150px;}
            #footer .page:after { content: counter(page, upper-decimal); }
        </style>
        
    </head>
    


<body>
<div style=" font-family: Arial, Helvetica, sans-serif;font-size: 9.5px;width:100%; border-collapse: collapse; size: 0.5px;">           
    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 10px;width: 100%">

            <tr>
                <td  rowspan="1" style="border:1px solid #000000FF; width: 20%;text-align: center;"><img style="width: 150px;" src="images/logorpt.jpg"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 60%;text-align: center;font-size: 11px"><strong>GERENCIA DE PROYECTOS <br> Hoja Resumen de Proyecto </strong></td>
                <td rowspan="1"  style="border:1px solid #000000FF; width: 20%;font-size: 9px">

                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: center;"><strong><?php echo (isset($proyecto)==1?$proyecto->codigo:'');?>-AND-25-HR-001</strong></td>
                        </tr>

                        <tr>
                            <td>Revisión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo (isset($documento)==1?$documento->revision:'');?> </td>
                        </tr>

                        <tr>
                            <td>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <?php echo (isset($fechadoc)==1?substr($fechadoc, 0,6).substr($fechadoc, -2):'');?> </td>
                        </tr>
                        
                    </table>

                </td>  
                
            </tr>
           
            <tr>
                
                <td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> SIG AND </td>
                <td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> 10-AND-25-FOR-0101 / R2 / 06-06-18 </td>
            </tr>

         
            

        </table>



    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            <tr>
                <td style="border:1px solid #000000FF;width: 100%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Información general</strong>
                    
                </td>
            </tr>
        </table>

    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="3"  style="border:1px solid #000000FF;width: 15%;">Nombre del proyecto</td>

                <td rowspan="3" style="border:1px solid #000000FF;width: 50%;"><?php echo (isset($proyecto)==1?$proyecto->nombre:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">Proyecto N°</td>
                <td style="border:1px solid #000000FF; width: 20%;"><?php echo (isset($proyecto)==1?$proyecto->codigo:'');?></td>
            </tr>
            <tr>
                
                
                <td style="border:1px solid #000000FF; width: 15%;">Propuesta N°</td>
                <td style="border:1px solid #000000FF; width: 20%;"><?php echo (isset($propuesta)==1?$propuesta->ccodigo:'');?></td>
            </tr>
            <tr>
                
            
                <td style="border:1px solid #000000FF; width: 15%;">Fecha de adjudicación propuesta</td>
                <td style="border:1px solid #000000FF; width: 20%;"><?php echo (isset($propuesta)==1?$propuesta->fadjucicacion:'');?></td>
            </tr>   

                    
        </table>
    </div>


    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td rowspan="3" style="border:1px solid #000000FF; width: 15%;">Descripción del proyecto</td>
                <td rowspan="3" style="border:1px solid #000000FF; width: 85%;"><?php echo (isset($proyecto)==1?$proyecto->descripcion:'');?></td>
            </tr>
        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Descripción web</td>
                <td style="border:1px solid #000000FF; width: 85%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->descripcionweb:'');?> </td>
            </tr>          
        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Cliente</td>
                <td style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($persona_cliente)==1?$persona_cliente->nombre:'');?></td>                
                <td style="border:1px solid #000000FF; width: 15%;">Cliente final</td>
                <td style="border:1px solid #000000FF; width: 30%;"><?php echo (isset($tcliente_final)==1?$tcliente_final->razonsocial:'');?></td>
            </tr>      

        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Unidad Minera</td>
                <td style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($uminera)==1?$uminera->nombre:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">Lugar de trabajo</td>
                <td style="border:1px solid #000000FF; width: 30%;"><?php echo (isset($lugartrabajo)==1?$lugartrabajo->descripcion:'');?></td>
            </tr>      

        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Portafolio de servicios</td>
                <td style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($proyecto)==1?$proyecto->servicio:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">Tipo de proyecto</td>
                <td style="border:1px solid #000000FF; width: 30%;"><?php echo (isset($proyecto)==1?$proyecto->tipoproyecto:'');?></td>
            </tr>      

        </table>
    </div>


    <div class="row" >

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Tipo de gestión de proyectos</td>
                <td style="border:1px solid #000000FF;width: 25%;"><?php echo (isset($proyecto)==1?$proyecto->tipgestion:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 30%;background-color:rgba(0, 105, 170, 1);"><div style="text-align: center;color: white"><h4>Planificado</h4></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 30%;background-color:rgba(0, 105, 170, 1);"><div style="text-align: center;color: white"><h4>Real</h4></div></td>
            </tr>
            <tr > 
                <td style="border:1px solid #000000FF;width: 15%;"">Gerente de proyecto</td>
                <td style="border:1px solid #000000FF;width: 15%;"><?php echo (isset($proyecto)==1?$proyecto->gerente:'');?></td>
            </tr>           
                    
        </table>                

    </div>

    <div class="row">

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Líder de Disciplina 1</td>
                <td style="border:1px solid #000000FF;width: 25%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->li1:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha inicio</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"><?php echo (isset($feiniPla)==1?substr($feiniPla, 0,6).substr($feiniPla, -2):'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha inicio</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"><?php echo (isset($feiniReal)==1?substr($feiniReal, 0,6).substr($feiniReal, -2):'');?></td>
            </tr>

            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Líder de disciplina 2</td>
                <td style="border:1px solid #000000FF;width: 25%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->li2:'');?></td>
            </tr>

            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Control de Proyecto</td>
                <td style="border:1px solid #000000FF;width: 25%;"><?php echo (isset($per_contproy)==1?$per_contproy->abreviatura:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha cierre</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"><?php echo (isset($fcierrePla)==1?substr($fcierrePla, 0,6).substr($fcierrePla, -2):'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha cierre</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"><?php echo (isset($fcierreReal)==1?substr($fcierreReal, 0,6).substr($fcierreReal, -2):'');?></td>
            </tr>

            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Control documentario</td>
                <td style="border:1px solid #000000FF;width: 25%;"><?php echo (isset($per_contdoc)==1?$per_contdoc->abreviatura:'');?></td>
            </tr>
        
                    
        </table>                

    </div>

    <!--<div class="row">
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            <tr style="text-align: center">
                <td style="border:1px solid #000000FF;width: 15%;"">Control Documentario</td>
                <td style="border:1px solid #000000FF;width: 85%;"></td>           
                     
            </tr>
            
        </table>

    </div>-->
    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            <tr>
                <td style="border:1px solid #000000FF;width: 100%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Información comercial</strong>
                    
                </td>
            </tr>
        </table>

    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            

            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">Nombre contacto</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;"><?php echo (isset($uminerac)==1?ucwords(strtolower($uminerac->nombres)).' '.ucwords(strtolower($uminerac->apaterno)).' '.ucwords(strtolower($uminerac->amaterno)):'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 15%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Presupuesto</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Anddes</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Sub Cont 01</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Sub Cont 02</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 7%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Total <br>Presupuesto</strong></div></td>
            </tr>
            
            

                    
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">Cargo</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;"><?php echo (isset($uminerac)==1?$uminerac->contcargo:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">Moneda</td>
                <td style="border:1px solid #000000FF; width: 10%;"><?php echo (isset($monedas)==1?ucwords(strtolower($monedas->descripcion)):'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"><?php echo (isset($monedas)==1?ucwords(strtolower($monedas->descripcion)):'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"><?php echo (isset($monedas)==1?ucwords(strtolower($monedas->descripcion)):'');?></td>
                <td style="border:1px solid #000000FF; width: 7%;"><?php echo (isset($monedas)==1?ucwords(strtolower($monedas->descripcion)):'');?></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Honorarios</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->hon_and:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->hon_con1:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->hon_con2:'');?></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"><?php echo (isset($totalHon)==1?$totalHon:'');?></td>
            </tr>                       
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%"><?php //dd($proy_adicional); ?>
            
            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">Teléfono</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"><?php echo (isset($uminerac)==1?$uminerac->telefono:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Anexo</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"><?php echo (isset($uminerac)==1?$uminerac->anexo:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">Gastos</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->gast_and:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->gast_con1:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->gast_con2:'');?></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"><?php echo (isset($totalGast)==1?$totalGast:'');?></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Laboratorio</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->lab_and:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->lab_con1:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->lab_con2:'');?></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"><?php echo (isset($totalLab)==1?$totalLab:'');?></td>
            </tr>   

            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">Celular 1</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"><?php echo (isset($uminerac)==1?$uminerac->cel1:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Celular 2</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"><?php echo (isset($uminerac)==1?$uminerac->cel2:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">Otros</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->otr_and:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->otr_con1:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->otr_con2:'');?></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"><?php echo (isset($totalOtr)==1?$totalOtr:'');?></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Descuento</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->desc_and:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->desc_con1:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"><?php echo (isset($proy_adicional)==1?$proy_adicional->desc_con2:'');?></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"><?php echo (isset($totalDesc)==1?$totalDesc:'');?></td>
            </tr>                   
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">E-mail</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;"><?php echo (isset($uminerac)==1?$uminerac->email:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;;color:black;background-color:rgba(180, 180, 180, 1);font-weight: bold">Total</td>
                <td style="border:1px solid #000000FF; width: 10%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"><?php echo (isset($totalAnddes)==1?$totalAnddes:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"><?php echo (isset($totalSubCont1)==1?$totalSubCont1:'');?></td>
                <td style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"><?php echo (isset($totalSubCont2)==1?$totalSubCont2:'');?></td>
                <td style="border:1px solid #000000FF; width: 7%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"><?php echo (isset($totalPresupuesto)==1?$totalPresupuesto:'');?></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">SOC 01</td>
                <td style="border:1px solid #000000FF; width: 10%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 7%;"></td>
            </tr>       
            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">Razón social</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;"><?php echo (isset($persona_cliente)==1?$persona_cliente->razonsocial:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">SOC 02</td>
                <td style="border:1px solid #000000FF; width: 10%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 7%;"></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">SOC 03</td>
                <td style="border:1px solid #000000FF; width: 10%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 7%;"></td>
            </tr>                   
        </table>
    </div>


    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">RUC</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"><?php echo (isset($persona_cliente)==1?$persona_cliente->identificacion:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Télefono</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"><?php echo (isset($persona_cliente)==1?$persona_cliente->telefono:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 15%;;color:black;background-color:rgba(180, 180, 180, 1);font-weight: bold">Total + SOC</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 7%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
            </tr>   

        </table>                    

    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Dirección fiscal</td>
                <td style="border:1px solid #000000FF;width: 40%;"><?php echo (isset($direcli)==1?$direcli:'');?></td>
                <td style="border:1px solid #000000FF; width: 15%;">Adelanto %</td>
                <td style="border:1px solid #000000FF; width: 0%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->porcentajeadelanto:'');?></td>
            
            </tr>   

                                
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">Documento de aprobación</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->nrodocaprobacion:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Número de <br> documento</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->nrodocaprobacion:'');?></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 15%">Tipo de contrato</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 30%;"><?php echo (isset($proy_adicional)==1?ucfirst(strtolower($proy_adicional->formacotiza)):'');?></td>
                
            </tr>   
        </table>                

    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Comentarios</td>
                <td style="border:1px solid #000000FF;width: 17%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->comentarioshr:'');?><</td>
                <td style="border:1px solid #000000FF;width: 23%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Plazo de pago de facturas</td>
                <td style="border:1px solid #000000FF; width: 30%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->plazopagodias:'');?></td>
                
            </tr>   
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Foto proyecto (link ruta)</td>
                <td style="border:1px solid #000000FF;width: 17%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->fotoproyhr:'');?></td>
                <td style="border:1px solid #000000FF;width: 23%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Fecha de corte</td>
                <td style="border:1px solid #000000FF; width: 30%;"><?php echo (isset($proy_adicional->diascorte)==1?$proy_adicional->diascorte:'');?></td>
                
            </tr>   

                                
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Observaciones</td>
                <td  style="border:1px solid #000000FF;width: 85%;"><?php echo (isset($proy_adicional)==1?$proy_adicional->observacionhr:'');?></td>
                
            </tr>   
            

                                
        </table>
    </div>

 

</div>

  <div id="footer" style="text-align: right;color:rgba(0, 105, 170, 1);font-size: 11px">
    <p class="page">Página 1 de </p>
  </div>
</body>


    
</html>





