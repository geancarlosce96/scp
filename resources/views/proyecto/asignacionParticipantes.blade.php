<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Proyecto -
      <small>Asignación de profesionales</small>
    </h1>
    <ol class="breadcrumb">
      <li>@include('accesos.accesoProyecto')</li>
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Proyecto</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-2 col-xs-12">
      
      </div>
      <div class="col-lg-10 col-xs-12">
      </div>            
    </div>

    {!! Form::open(array('url' => 'grabarParticipante','method' => 'POST','id' =>'frmDisciplina')) !!}
    {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
    <div class="row">
      <!--<div class="col-lg-1 hidden-xs"></div>-->
      <div class="col-md-12 col-xs-12">


        <div class="row clsPadding2">
          <div class="col-lg-12">
              <div class="input-group">
                <span class="input-group-addon">
                  <label class="col-form-label"><b>Cliente</b></label>
                </span>
                {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
                {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
                <span class="input-group-btn">
                  <button type='button' class="btn btn-primary" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button>
                </span>
              </div>
            </div>
        </div> 


        <div class="row clsPadding2">
          <div class="col-lg-6">
              <div class="input-group">
                <span class="input-group-addon">
                  <label class="col-form-label"><b>Unidad Minera</b></label>
                </span>
                {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
              </div>
            </div>

          <div class="col-lg-6">
              <div class="input-group">
                <span class="input-group-addon">
                  <label class="col-form-label"><b>Código proyecto</b></label>
                </span>
                {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}
              </div>
            </div>
        </div>


        <div class="row clsPadding">
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>


        <div class="row clsPadding2">
          <div class="col-lg-12">
              <div class="input-group">
                <span class="input-group-addon">
                  <label class="col-form-label"><b>Nombre del proyecto</b></label>
                </span>
                {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}
              </div>
            </div>
        </div> 

 
          <div class="row clsPadding2">
            <div class="col-lg-3">
              <div class="input-group">
                <span class="input-group-addon">
                  <label class="col-form-label"><b>Fecha inicio</b></label>
                </span>
                <span class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </span>
                {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right', 'disabled'=>'true','placeholder'=>'Fecha inicio')) !!}
              </div>
            </div>
            <div class="col-lg-3">
              <div class="input-group">
                <span class="input-group-addon"">
                  <label class="col-form-label"><b>Fecha cierre</b></label>
                </span>
                <span class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </span>
                {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true','placeholder'=>'Fecha cierre')) !!}
              </div>
            </div>
            <div class="col-lg-2">
              <div class="input-group">
                <span class="input-group-addon">
                  <label class="col-form-label"><b>Estado</b></label>
                </span>
                {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!}
              </div>
            </div>
            <div class="col-lg-4">
              <div class="input-group">
                <span class="input-group-addon">
                  <label class="col-form-label"><b>Subproyecto</b></label>
                </span>
                {!! Form::select('subproyecto',(isset($subproyecto)==1?$subproyecto:array()),null,array('class' => 'form-control','id'=>'subproyecto','disabled'=>'true')) !!} 
              </div>
            </div>
          </div>








    








          <div class="row clsPadding">
            <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
          </div>


          <div class="row ">
            <div class="col-md-12 col-xs-12" ><button type="button" class="btn btn-primary btn-sm" id="btnAddPar"
             @if(isset($bsave))
             @if($bsave)
             @else
             disabled   
             @endif
             @else 
             disabled
             @endif        
             >Agregar profesionales</button></div>
           </div>

           <div class="row"> 
            <div class="col-lg-12 col-xs-12 table-responsive" id="divTable">
              @include('proyecto.tableParticipantes',array('proyectodisciplina'=>(isset($proyectodisciplina)==1?$proyectodisciplina:null) ) )
            </div>
          </div>



          <div class="row clsPadding">
            <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
          </div>
          <div class="row clsPadding2">
            <div class="col-lg-4 col-xs-4">
              <button class="btn btn-primary btn-block" style="width:200px;" 
              @if(isset($bsave))
              @if($bsave)
              @else
              disabled   
              @endif
              @else 
              disabled
              @endif
              id='btnSave'><b>Grabar</b></button> 

            </div>
            <!--<div class="col-lg-1 hidden-xs"></div>-->

          </div>
          {!! Form::close() !!}
        </section>

        <!-- Modal Buscar Proyecto-->
        @include('partials.searchProyecto')

        <!-- Inicio Modal agregar Participantes -->

        <div id="agregarDisciplina" class="modal fade" role="dialog">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">
              <div class="modal-header" style="background-color: #0069AA">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" style="color: white">Asignación de equipos</h4>
              </div>
              <div class="modal-body">
                {!! Form::open(array('url' => 'agregarParticipantes','method' => 'POST','id' =>'frmDisciplinaPersona')) !!}
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_h')) !!}
                {!! Form::hidden('cproyecto',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto_h' )) !!}
                <div class="row">
                  <div class="col-md-5">
                    <table class="table table-bordered">
                      <thead>
                        <th>Profesional</th>
                        <th>Categoría</th>
                        <!-- <th>LÃ­der</th> -->
                      </thead>
                      <tbody>
                        <tr>
                          <td>{!! Form::select('personal',(isset($personal)==1?$personal:array() ),'',array('class' => 'form-control select-box','id'=>'personal')) !!}</td>

                          <td>{!! Form::select('categoria',(isset($categoria)==1?$categoria:array() ),'',array('class' => 'form-control ','id'=>'categoria')) !!}</td>
                          <!-- <td><input type='radio' name="chklider" value="1" onclick="uncheckRadio(this)"></td> -->
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div class="col-md-2">
                    <br /><br /><br />
                    <button type="button" id="btnAgregar" class="btn btn-success btn-sm" onclick="agregarDisciplinas()"><b>Añadir</b></button><br /> <br />
                    <button type="button" id="btnBorrar" class="btn btn-danger btn-sm" onclick="borrarDisciplinas()"><b>Quitar</b></button>
                  </div>
                  <div class="col-md-5">
                    <div class="rounded" style="width:100%;height:100% ;border: solid 1px #0069AA" id="divDisSel">
                      @include('proyecto.tableParticipanteSeleccionado',array('proyectodisciplina'=>(isset($proyectodisciplina)==1?$proyectodisciplina:null) ) )                
                    </div>
                  </div>
                </div>
              </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="agregarLider()">Aceptar</button>
            </div>
            </div>
          </div>      
        </div>


        <!-- Fin Modal agregar Participantes -->


    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>

      var selected =[];
      var selected_actividad =[];
      var personal_rate =<?php echo (isset($personal_rate)==1?json_encode($personal_rate):'[]'); ?>

      $.fn.dataTable.ext.errMode = 'throw';
      var table=$("#tProyecto").DataTable({
        "processing":true,
        "serverSide": true,

        "ajax": {
          "url": "listarProyectos",
          "type": "GET",
        },
        "columns":[
        {data : 'cproyecto', name: 'tproyecto.cproyecto'},
        {data : 'codigo', name: 'tproyecto.codigo'},
        {data : 'GteProyecto' , name : 'p.abreviatura'},
        {data : 'cliente' , name : 'tper.nombre'},
        {data : 'uminera' , name : 'tu.nombre'}, 
        {data : 'nombre' , name : 'tproyecto.nombre'},
        {data : 'descripcion' , name : 'te.descripcion'}

        ],
        "language": {
          "lengthMenu": "Mostrar _MENU_ registros por página",
          "zeroRecords": "Sin Resultados",
          "info": "Página _PAGE_ de _PAGES_",
          "infoEmpty": "No existe registros disponibles",
          "infoFiltered": "(filtrado de un _MAX_ total de registros)",
          "search":         "Buscar",
          "processing":     "Procesando...",
          "paginate": {
            "first":      "Inicio",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
          },
          "loadingRecords": "Cargando..."
        }
      });

      $('#tProyecto tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
          selected.push( id );

        } 

        $(this).toggleClass('selected');
      });     
      $('#searchProyecto').on('hidden.bs.modal', function () {
       goEditar();
     });

      function goEditar(){
        var id =0;
        if (selected.length > 0 ){
          id = selected[0];
          getUrl('editarAsignacionParticipantes/'+id.substring(4),'');
        }else{
          $('.alert').show();
        }
      }

      $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });      

      $("#btnAddPar").on('click',function (){
        $('#agregarDisciplina').modal('show');
        $('.select-box').chosen(
        {
            allow_single_deselect: true
        });

      });

     $("#agregarDisciplina").on('show.bs.modal',function(){
        $('#frmDisciplinaPersona :input[type=checkbox]').each(function() 
        { 
          this.checked = false; 
        });   
        $('#frmDisciplinaPersona :input[type=radio]').each(function() 
        { 
          //this.checked = false; 
        });                         
      });


     $("#agregarDisciplina").on('hidden.bs.modal',function(){

      
       $.ajax({

        type:"GET",
        url:'verParticipantesProyecto',

        beforeSend: function () {

          $('#div_carga').show(); 

        },
        success: function(data){
         $('#div_carga').hide(); 

         $("#divTable").html(data);

       }
     });                        
     });     


      function agregarDisciplinas(){

        var categ = document.getElementById('categoria').value;
        
        if(categ==null || categ ==""){
          alert('Este participante no tiene una categoría asignada');
        }

        else{

          $.ajax({

          type:"POST",
          url:'agregarParticipantes',
          data:$("#frmDisciplinaPersona").serialize(),
          beforeSend: function () {

            $('#div_carga').show(); 
            
          },
          success: function(data){
           $('#div_carga').hide(); 

           $("#divDisSel").html(data);
           $(".select-box").val('').trigger("chosen:updated");
           $("#categoria").val('');

          }
          });

        }

        
      }


/*      $("#v").on('show.bs.modal',function(){
        $('#frmDisciplinaPersona :input[type=checkbox]').each(function() 
        { 
          this.checked = false; 
        });   
        $('#frmDisciplinaPersona :input[type=radio]').each(function() 
        { 
          this.checked = false; 
        });                         
      });


     $("#agregarLider").on('hidden.bs.modal',function(){
       $.ajax({

        type:"GET",
        url:'verEquipo',

        beforeSend: function () {

          $('#div_carga').show(); 

        },
        success: function(data){
         $('#div_carga').hide(); 

         $("#divTable").html(data);

       }
     });                        
     });  */

      

      function borrarDisciplinas(){
        $.ajax({

          type:"POST",
          url:'borrarParticipante',
          data:$("#frmDisciplinaPersona").serialize(),
          beforeSend: function () {

            $('#div_carga').show(); 
            
          },
          success: function(data){
           $('#div_carga').hide(); 

           $("#divDisSel").html(data);

         }
       });
      }


      /* Grabar frmDisciplina*/
      $('#frmDisciplina').on('submit',function(e){

            /*$.ajaxSetup({
                header: document.getElementById('_token').value
              });*/
              e.preventDefault(e);

              $('input+span>strong').text('');
              $('input').parent().parent().removeClass('has-error');            

              $.ajax({

                type:"POST",
                url:'grabarParticipante',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {

                  $('#div_carga').show(); 

                },
                success: function(data){
                // $(".alert-success").prop("hidden", false);
                 $('#div_carga').hide(); 

                 $("#resultado").html(data);
                 $("#div_msg").show();
               },
               error: function(data){


                $.each(data.responseJSON, function (key, value) {
                  var input = '#frmDisciplina input[name=' + key + ']';
                  $(input + '+span>strong').text(value);
                  $(input).parent().parent().addClass('has-error');

                });
                $('#div_carga').hide();
                $('#detalle_error').html(data);
                $("#div_msg_error").show();
              }
            });
            });        
      /* Fin Grabar FrmDisciplina*/

      $("#addActividad").on('show.bs.modal',function(e){

        if ($("#subproyecto").val()=='' || $("#subproyecto").val()==null){
          alert('Seleccione el Subproyecto...');
          e.preventDefault();

        }
        selected_actividad.splice(0,selected_actividad.length);
        $('#treeActividades').highCheckTree({
          data: getTreeActividad(),
          onCheck: function (node){
            selected_actividad.push(node.attr('rel'));
          },
          onUnCheck: function(node){
            var idx = $.inArray(node.attr('rel'));
            if(idx != -1){
              selected_actividad.splice(idx,1);
            }
          }
        });               

      }); 

      $("#idAddAct").click(function(e){
        $.ajax({
          url:'addActividadPy',
          type: 'POST',
          data : {
            "_token": "{{ csrf_token() }}",
            selected_actividad:selected_actividad,
            subproyecto:$('#subproyecto').val()
          },
                //$('input[name="acti[]"]').serialize() + "&subproyecto="+ $('#subproyecto').val(),
                beforeSend: function(){
                  //$("input[name='acti[]']").attr('checked',false);
                  $('#div_carga').show(); 
                },
                success: function(data){
                  $("#idActividades").html(data);
                  $('#div_carga').hide(); 

                },            
              });

      });  
      function fdelActi(id){
        $.ajax({
          url:'delActividadPy/'+id,
          type: 'GET',
          beforeSend: function(){
            $('#div_carga').show(); 
          },                
          success: function (data){
            $("#idActividades").html(data);
            $('#div_carga').hide(); 
          },
        });

      };     
      $("#btnAddEst").click(function(e){
        if($("#categorias").val()==''){
          alert('Especifique la categoria');
          return;
        }

        $.ajax({
          url: 'addEstructuraPy',
          type:'GET',
          data: "categorias=" + $("#categorias").val()+"&profesional=" + $("#profesional").val() + "&rate=" + $("#rate").val()+"&cproyecto=" + $("#cproyecto").val() ,
          beforeSend: function () {

            $('#div_carga').show(); 

          },              
          success : function (data){
            $("#idPersonal").html(data);
            $('#div_carga').hide(); 
          },
        });
      })
      function fdelEstru(id){
        $.ajax({
          url: 'delEstructuraPy/'+id,
          type: 'GET',
          beforeSend: function () {

            $('#div_carga').show(); 

          },              
          success: function(data){
            $("#idPersonal").html(data);
            $('#div_carga').hide(); 
          },

        });
      }

      $("#frmproyecto").on('submit',function(e){
        $.ajaxSetup({
          header: document.getElementById('_token').value
        });
        e.preventDefault(e);
        $.ajax({

          type:"POST",
          url:'grabarEstructuraProyecto',
          data:$(this).serialize(),
          beforeSend: function () {

            $('#div_carga').show(); 
            
          },
          success: function(data){
           $(".alert-success").prop("hidden", false);
           $('#div_carga').hide(); 

           $("#resultado").html(data);
           $("#div_msg").show();
         },
         error: function(data){


          $.each(data.responseJSON, function (key, value) {
            var input = '#frmproyecto input[name=' + key + ']';
          
            if(!($(input).length > 0)){
              input ='#frmproyecto select[name=' + key + ']';
            }
            $(input).parent().parent().addClass('has-error');

          });
          $('#div_carga').hide();
          $('#detalle_error').html(data);
          $("#div_msg_error").show();
        }
      });


      });

      $('#profesional').on('change', function() {
          sel= this.value; // or $(this).val()
          $.each(personal_rate, function( index, value ) {
            //alert( index + ": " + value['cpersona'] + "--" + value['rate1'] );
            if(value['cpersona']==sel){
              $("#rate").val(value['rate1']);
            }
          });          
        });     

        $('#personal').on('change', function() {

          $.ajax({

                type:"POST",
                url:'verCategoria',
                data : {
                  "_token": "{{ csrf_token() }}",
                  cpersona: this.value                 
                },
                beforeSend: function () {
                    $('#div_carga').show(); 
                },
                success: function(data){
                     $('#div_carga').hide();                    
                     $('#categoria').val(data);
                  
                },
                error: function(data){
                    $('#div_carga').hide();
                }
            });  
             
        });    

      function getTreeActividad() {

        var tree = [
        <?php echo (isset($estructura)==1?$estructura:'') ?>

        ];          
        return tree;
      }

      function eliminarPartProyecto(id){
        $.ajax({
          url: 'eliminarParticipanteProyecto/' + id,
          type:'GET',
          beforeSend: function () {                
                //$('#div_carga').show();         
              },              
              success : function (data){

//                $('#div_carga').hide();
                $('#divTable').html(data);
               

              },
            });          

      }


    function obtenerRadioSeleccionado(formulario, nombre){
     var elementos = document.getElementById(formulario).elements;
     var longitud = document.getElementById(formulario).length;
     for (var i = 0; i < longitud; i++){
         if(elementos[i].name == nombre && elementos[i].type == "radio" && elementos[i].checked == true){
             return elementos[i];
         }
     }
     return false;

    }

    </script>


<!--Inicion del Script para funcionamiento de Chosen -->
<script>
    $('.select-box').chosen(
        {
            allow_single_deselect: true,
            width: "100%"
        });
</script>
<!-- Fin del Script para funcionamiento de Chosen -->
