<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Performance - Curva S</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
   <div class="row">
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
    <div class="col-lg-9 col-xs-9 clsPadding">
    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
    {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
    </div>

    <div class="col-lg-1 col-xs-1 clsPadding">
    <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button>
    </div>

    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">
        {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
    </div> 

    <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">
        {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}
    </div>
   </div>    
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
        <div class="col-lg-9 col-xs-9">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
            ,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}  
        </div>	 
    </div> 

    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
        <div class="col-lg-4 col-xs-4">
            {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right','disabled' =>'true')) !!}
        </div> 

        <div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>
        <div class="col-lg-4 col-xs-4">
            {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}
        </div>        
    </div>

    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
            <label for="cestadoproyecto" class="col-sm-2 control-label">Estado</label> 
              {!! Form::select( 'cestadoproyecto',(isset($testado)==1?$testado:array() ),'',array('class' => 'form-control','id'=>'cestadoproyecto')) !!}
        </div>
        <div class="col-lg-6 col-xs-6">
            <label for="catalogoid" class="col-sm-2 control-label">Periodo</label>
              {!! Form::select( 'catalogoid',(isset($tperiodo)==1?$tperiodo:array() ),'',array('class' => 'form-control','id'=>'catalogoid')) !!}        
        </div>    
    </div>
    

    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-6">
        <label class="clsTxtNormal">Código de Documento:</label>
        	<input class="form-control input-sm" type="text" placeholder="Cod. documento">
        
        </div>
        <div class="col-lg-3 col-xs-6">
        <label class="clsTxtNormal">Revisión:</label>
        	<input class="form-control input-sm" type="text" placeholder="A">
                
        </div>
        <div class="col-lg-3 col-xs-6 clsPadding2">
            <div class="form-group">
            <label class="clsTxtNormal">Fecha:</label>
            
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              {!! Form::text('fecha','', array('class'=>'form-control pull-right')) !!}
            </div>
            <!-- /.input group -->
            </div>       
        </div>  
        <div class="col-lg-3 col-xs-6 clsPadding2">
            <div class="form-group">
            <label class="clsTxtNormal">Fecha de Corte:</label>
            
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              {!! Form::text('fcorte','', array('class'=>'form-control pull-right')) !!}
            </div>
            <!-- /.input group -->
            </div>       
        </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-6">
	        <a href="#" class="btn btn-primary btn-block "> <b>Visualizar</b></a>
        </div>
        <div class="col-lg-9 col-xs-6"></div>
    </div>
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    
    
    


<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
    @include('partials.modalcurvaSIndicadores',array('listcurvaS'=> (isset($listcurvaS)==1?$listcurvaS:array() )))
          
    </div>
</div>


  
    
    
    

    
 
    <div class="row">
    <div class="col-lg-3 col-xs-12 clsPadding">
    	<a href="#" </class="btn btn-primary btn-block"><b>Generar Curva S</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding">
    	<a href="#" class="btn btn-primary btn-block"><b>Indicadores</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding">
    	<a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding">
              <select class="form-control" >
                <option>XLS</option>
                <option>PDF</option>
              </select> 
        </div>        
    </div>
   
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
        {!! Form::close() !!}
    </section>
        @include('partials.searchProyecto')

    <!-- /.content -->
  </div>


    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
        
        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';        

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu"    : "Mostrar _MENU_ registros por página",
                "zeroRecords"   : "Sin Resultados",
                "info"          : "Página_PAGE_ de _PAGES",
                "infoEmpty"     : "No existe registros disponibles",
                "infoFiltered"  : "(filtrado de un_MAX_ total de registros)",
                "search"        : "Buscar:",
                "processing"    :  "Procesando...",
                "paginate"      : {
                    "first"     :    "Inicio",
                    "last"      :    "Ultimo",
                    "next"      :     "Siguiente",
                    "previous"  :   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarcurvaS/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

       $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        $('#fcorte').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fecha').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 
         
        
    </script>