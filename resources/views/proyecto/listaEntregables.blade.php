<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Lista de Entregables</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
               
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>    
        <!--<div class="row">
        <div class="col-lg-10 col-xs-1"></div>
        <div class="col-lg-1 col-xs-1">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Aprobaciones</button>
        </div>
        <div class="col-lg-1 col-xs-1"></div>
        </div>  -->
      {!! Form::open(array('url' => 'grabarEntregablesProyecto','method' => 'POST','id' =>'frmproyectoentregables')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
      <div class="row">
          <div class="row"><br></div>
          <div class="col-lg-1 col-xs-1">Cliente:</div>
          <div class="col-lg-3 col-xs-3">
              {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
          </div>
          <div class="col-lg-1 col-xs-1">Unidad minera:</div>
          <div class="col-lg-3 col-xs-3">
              {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
          </div>
          <div class="col-lg-1 col-xs-1">Estado:</div>
          <div class="col-lg-1 col-xs-1">
              {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!}
          </div>
          <div class="col-lg-1 col-xs-1">Buscar Proyecto:</div>
          <div class="col-lg-1 col-xs-1">
              <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'>
                  <b>...</b>
              </button>
          </div> 
      </div>
      <div class="row">
          <div class="row"><br></div>
          <div class="col-lg-1 col-xs-1">Código Proyecto:</div>
          <div class="col-lg-2 col-xs-2">
              {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}
          </div>
          <div class="col-lg-1 col-xs-1">Nombre del Proyecto:</div>
          <div class="col-lg-6 col-xs-6">
              {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}   
          </div>

          <div class="col-lg-1 col-xs-1">
                <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='' onclick="getUrl('editarEdtProyecto/{{ (isset($proyecto)==1?$proyecto->cproyecto:'') }}','')">
                    <b>Ir a EDT</b>
                </button>
            </div> 
      </div>

      <div class="row"><br></div>


      <div class="col-lg-12 col-xs-12" style="background-color:rgba(0, 105, 170, 1); max-height:1px;"></div>

      <div class="row">
          <div class="box-body">
              <div class="box-group" id="accordion">
                  <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                  <div class="panel panel-primary">
                      <div class="box-header with-border panel-heading">
                          <h4 class="box-title panel-title">
                              <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed"> <span class="glyphicon glyphicon-hand-up"></span> &nbsp; Agregar informacion adicional <span id="nombreEDTSeleccionado"></span>  </a>
                          </h4>
                          <!--<button type="submit" class="btn btn-success bg-green pull-right">Registrar</button>-->
                      </div>
                      <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false">
                          <div class="box-body">
                             @include('partials.formEntregableInformacionAdicional')
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>


      <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        <br>

      <div class="row">


        <div class="col-sm-9 pull-left">

          <button type="button" class=" col-lg-2 btn btn-danger" id="btnVerAnulados">
              <b>Ver Anulados</b>
          </button>

          <button type="button" class=" col-lg-2 btn btn-info" id="btnVerTodos" style="display: none">
              <b>Ver Activos</b>
          </button>

          <button type="button" class=" col-lg-2 btn btn-success printLE" id="btnPrintEDT" data-tipoprint="edt">
              <i class="glyphicon glyphicon-print" style="font-size: 11px;"><b> EDT</b></i>
          </button>
          <button type="button" class=" col-lg-2 btn btn-success printLE" id="btnPrintLEAND" data-tipoprint="leand">
              <i class="glyphicon glyphicon-print" style="font-size: 11px;"><b> LE(Anddes)</b></i>
          </button>
          <button type="button" class=" col-lg-2 btn btn-success printLE" id="btnPrintLECLI" data-tipoprint="lecli">
              <i class="glyphicon glyphicon-print" style="font-size: 11px;"><b> LE(Clientes)</b></i>
          </button>

        </div>


        <div class="col-sm-2 pull-right">
            <button type="button" class="btn btn-success btn-block" id="btnCodificarTodos" style="font-size: 11px;"><b>Codificar Todos</b></button>
        </div>
        <!-- <div class="col-sm-2 pull-right">
            <button type="button" class="btn btn-success btn-block" id="btnCodificar"><b>Codificar Seleccionados</b></button>
        </div> -->
        <!-- <div class="col-sm-1 pull-right">
            <input type="text" class="form-control" size="1" name="">
        </div> -->
      </div>

      <br>

       
    <div id="resulados"></div>
      
      <div class="row clsPadding2">	
          <div class="col-lg-12 col-xs-12 table-responsive" id='tableEntdivLEntregable'>
            @include('partials.tableListaEntregables',array('listEnt'=> (isset($listEnt)==1?$listEnt:array() )))
      	  </div>
      </div>

    <!--<div class="row clsPadding2">
    	 <div class="col-lg-3 col-xs-3" style="font-weight:normal !important;">Nueva revisión: <input type="checkbox" name="chkrevision" value="1"></div>
         <div class="col-lg-4 col-xs-4" style="font-weight:normal !important;">Revisión: <input id="txtrevision" type="text" class="form-control" name="txtrevision" placeholder="Revsión"></div>
    </div>-->
      
     
    <div class="row clsPadding">
      <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <!-- <div class="row clsPadding2">
        <div class="col-lg-4 col-xs-4">
            <button class="btn btn-primary btn-block" id="btnSave"><b>Grabar</b></button> 
        </div>
        <div class="col-lg-4 col-xs-4">
            &nbsp;
        </div>
        <div class="col-lg-4 col-xs-4">
            <button type="button" class="btn btn-primary btn-block" disabled="disabled"><b>Imprimir</b></button>
        </div>               
    </div> -->
      

  </div>
      <!--<div class="col-lg-1 hidden-xs"></div>-->
    
        <input type="hidden" name="fechaform" id="fechaform">
        <input type="hidden" name="tipofechaform" id="tipofechaform">
        <input type="hidden" name="formulario" id="formulario">


    </div>
    {!! Form::close() !!}




<div class="modal fade" id="modalPaquetes" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #0069AA">
          -<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" style="color: white"><STRONG>PAQUETE</STRONG></h4>
        </div>
        <div class="modal-body">

          {!! Form::open(array('url' => 'grabarHR','method' => 'POST','id' =>'frmrevisionhr')) !!}

          <div class="row clsPadding2 col-lg-12 col-xs-12" style="text-align: center;">

              <div class="col-lg-1 col-xs-6"></div> 

              <div class="col-lg-10 col-xs-12 list-group active">

                <div class="col-lg-6 col-xs-12 pull-left">

                    <span><label class="col-form-label">Revisión Actual del Documento :</label></span>

                    {!! Form::text('revisiondocumento1',(isset($documento->revision)==1?$documento->revision:''),array('class'=>'form-control pull-right', 'readonly'=>'true','id'=>'revisiondocumento1')) !!}

                </div>   

                <div class="col-lg-6 col-xs-12 pull-right ">

                    <span><label class="col-form-label">Nueva Revision:</label></span>

                    {!! Form::select('nuevarevisionModal',(isset($revisiones)==1?$revisiones:array()),'',array('class' => 'form-control','id'=>'nuevarevisionModal')) !!}

                </div>  
              </div>

              <div class="col-lg-1 col-xs-6"></div>                          

          </div> 

          <div class="row clsPadding2 col-lg-12 col-xs-12" style="text-align: center;"></div> 

          {!! Form::close() !!}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="saveRevision">Aceptar</button>
        <button type="button" class="btn btn-primary" id="cancelRevision">Cancelar</button>
      </div>
    </div>
  </div>
</div>



    </section>
    <!-- /.content -->
    @include('partials.searchProyecto')

     <div id="divEntregable">
      @include('partials.addEntregable')

     </div>

      @include('partials.modalAprobaciones')

        <?php //dd ($estructura); ?>

     
  </div>
        @include('partials.modalAgregarFechas')

<!-- ******************** ASIGNAR ACTIVIDADES ******************************* -->
     @include('partials.modalAsignarActividades')



<!-- *************************************************** -->
      

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="js/lista_entregables.js"></script>

<script src="js/tabla_lista_entregables_proyecto.js"></script>
<script>



        var selected_actividad_todos = [];   

        var selected =[];
        var cproyectoactividades='';
        var des_act='';
        $.fn.dataTable.ext.errMode = 'throw';
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyLider",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'GteProyecto' , name : 'p.abreviatura'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'},
                {data : 'descripcion' , name : 'te.descripcion'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            }
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });        

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('editarEntregablesProyecto/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }
        
        $('#divAct').on('hidden.bs.modal', function () {
          alert(cproyectoactividades);
           document.getElementById('cproyectoactividades').value =cproyectoactividades;
           document.getElementById('des_act').value = des_act;
        });

        $('#editEntregable').on('hidden.bs.modal', function () {
          saveEnt();
        });

       

        $("#frmproyecto").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarEntregablesProyecto',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();

                },
                error: function(data){
                    
                    
                    /*$.each(data.responseJSON, function (key, value) {
                            var input = '#frmproyecto input[name=' + key + ']';
                            if(!($(input).length > 0)){
                                input ='#frmproyecto select[name=' + key + ']';
                            }
                            $(input).parent().parent().addClass('has-error');

                    });*/
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });  


        $("#btnAddEnt").click(function(e){
          $('#tree').treeview({data: getTree()});
          $('#tree').on('nodeSelected', function(event, data) {
            cproyectoactividades= data.tags;
            des_act = data.text;
            document.getElementById('cproyectoactividades').value =cproyectoactividades;
            document.getElementById('des_act').value = des_act;                
            //alert(data.text);
            // Your logic goes here
            //{backdrop:"static"}
          });          
          $("#editEntregable").modal();
          /*$.ajax({
            url: 'addEntregable',
            type:'GET',
            data: "add=1&id=0&cproyecto="+ $('#cproyecto').val(),
            beforeSend: function () {
              $('#div_carga').show(); 
            },              
            success : function (data){


              $("#divEntregable").html(data);
              
              
              
              $('#tree').treeview({data: getTree()});
              $('#tree').on('nodeSelected', function(event, data) {
                cproyectoactividades= data.tags;
                des_act = data.text;
                document.getElementById('cproyectoactividades').value =cproyectoactividades;
                document.getElementById('des_act').value = des_act;                
                //alert(data.text);
                // Your logic goes here
                //{backdrop:"static"}
              });
              $("#editEntregable").modal();
              $('#div_carga').hide(); 
            },
          });*/
        
        });

       
        //Guardar
        //$("#btnSaveAdd").click(function(e){
        function saveEnt(){
          //alert('1');
          $.ajax({
            url: 'saveEntregable',
            type:'POST',
            data: $("#frmentregable").serialize(),
            beforeSend: function () {
              $('#div_carga').show(); 
            },              
            success : function (data){

              $("#divLEntregable").html(data);
              $('#div_carga').hide(); 
            },
          });
        //});
        }



var table = $('#tableEntregablesListaPry').DataTable( {
        "bFilter": true,
  "bSort": true,
  "bPaginate": false,
  "bLengthChange": false,
  "bInfo": false,
   scrollY:"500px",
   scrollX:true,
   scrollCollapse: true,
   columnDefs: [{ targets: [-1], orderable: false }],
  buttons: [ 'excel', 'pdf', 'csv' ,'colvis' ],
 /* fixedColumns:   {
           leftColumns: 6
     }*/
  }); 

    table.buttons().container()
        .appendTo( '#ttableEntregablesListaPry_wrapper .col-sm-6:eq(0)' );        








/*initDataTableLista();
var options={
  "bFilter": true,
  "bSort": true,
  "bPaginate": false,
  "bLengthChange": false,
  "bInfo": false,
   scrollY:"500px",
   scrollX:true,
   scrollCollapse: true,
   columnDefs: [{ targets: [-1], orderable: false }],
  buttons: [ 'excel', 'pdf', 'csv' ,'colvis' ],
  fixedColumns:   {
           leftColumns: 9
     }

};
var tableEnt;

function initDataTableLista() {
  tableEnt =  $('#tableEntregablesListaPry').DataTable(options);
  tableEnt.buttons().container()
        .appendTo( '#tableEntregablesListaPry_wrapper .col-sm-6:eq(0)' );
}
*/
$('#btnCodificar').on('click',function(){

  /*$('#modalPaquetes').modal('show');
  return;*/

  /*$('#modalPaquetes').modal({
    backdrop:"static"
  })
  return;*/

    $.ajax({
          url: 'codificarEntregablesSeleccionados',
          type:'GET',
          data: $("#frmproyectoentregables").serialize(),
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){

            $('#div_carga').hide();

            $("#resultado").html(data);
            //$("#resulados").html(data);

          },
    });   
})

$('#btnCodificarTodos').on('click',function(){

    $.ajax({
          url: 'codificarEntregablesTodos/'+$("#cproyecto").val(),
          type:'GET',
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){

            $('#div_carga').hide();

            $("#resultado").html(data);
            //$("#resulados").html(data);

          },
    });   
})
       

function prueba(){
  $("input[name=checkbox_ent]").each(function (index) {  
       if($(this).is(':checked')){
         alert($(this).val());
       }
    });
} 


$('.printLE').click(function(){

    var id =$('#cproyecto').val();
    var tipo =$(this).data('tipoprint');
    var url = BASE_URL + '/imprimirEntregables/'+tipo+'/'+id;
      if (id.length > 0 ){
             win = window.open(url,'_blank');
      }
      else{
            $('.alert').show();
      }
})


$('#btnVerAnulados').on('click',function(){

    $.ajax({
          url: 'verTablaEntregablesTodos/'+$("#cproyecto").val()+'/anulados',
          type:'GET',
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){

            $('#btnVerTodos').show();
            $('#btnVerAnulados').hide();
            $('#div_carga').hide();

           // $("#resultado").html(data);
            $("#tableEntdivLEntregable").html(data);

          },
    });   
})

$('#btnVerTodos').on('click',function(){

    $.ajax({
          url: 'verTablaEntregablesTodos/'+$("#cproyecto").val()+'/todos',
          type:'GET',
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){

            $('#btnVerAnulados').show();
            $('#btnVerTodos').hide();
            $('#div_carga').hide();

           // $("#resultado").html(data);
            $("#tableEntdivLEntregable").html(data);

          },
    });   
})

</script>    