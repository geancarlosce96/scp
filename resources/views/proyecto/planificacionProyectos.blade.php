<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto -
        <small>Planificación del proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <section class="content">
     {!! Form::open(array('url' => 'grabarPlanificacionArea','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}

      <div class="row"> 
                    <div class="col-md-3 col-xs-3">
                        <div class="col-lg-2 col-xs-2 clsPadding">Desde</div>            
                        <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                            {!! Form::text('fdesde','', array('class'=>'form-control pull-right ','id' => 'fdesde','readonly'=>'true')) !!}
                           
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-3">
                        <div class="col-lg-2 col-xs-2 clsPadding">Hasta</div>           
                        <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                            {!! Form::text('fhasta','', array('class'=>'form-control pull-right ','id' => 'fhasta','readonly'=>'true')) !!}
                        </div>        
                    </div>
                                   
                    <div class="col-md-1 col-xs-1">
                        <div>
                        <button type="button" class="btn btn-primary btn-block " id="btnView" ><b>Visualizar</b></button></div>         
                    </div> 
                    <div class="col-md-1 col-xs-1">
                        <div>
                        <button type="submit" class="btn btn-primary btn-block " onclick="savePlani()"> <b>Grabar</b></button></div>         
                    </div> 
                    
      </div>
      <br><br>

      <div class="row" id="ver">
        

      </div>
      

      <div class="row">
        <div class="col-lg-12 col-xs-12" >

        <div class="col-lg-6 col-xs-12">
          <div class="col-lg-12 col-xs-12"><h6>Tabla de disponibilidad de HH</h6></div>
            <table class="table ">
              <thead>
                <tr class="clsCabereraTabla">

                  <th style="width: 20%">Colaborador <br> </th>

                  @if(isset($semanasPlani))
                    @foreach($semanasPlani as $d) 
                  <th >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Semana {{ $d }}<br>
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Disp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Plani&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ejecu
                  </th>  
                    @endforeach
                             
                </tr>
              </thead>

              <body>
                  @if(isset($listTodosColaboradores))
                    @foreach($listTodosColaboradores as $ltc)     

                    <tr>

                      <td>{{$ltc->abreviatura}}</td>
                      @foreach($semanasPlani as $d) 

                      <td onmouseover="verDashCol('dash_cargabilidad_{{$ltc->cpersona}}_{{$d}}');" onmouseout="ocultarDashCol('dash_cargabilidad_{{$ltc->cpersona}}_{{$d}}');">  
                        <div  id="{{$ltc->cpersona}}_{{$d}}" style="width: 150px;">
                          @if(isset($acolCargSem))
                            <?php //dd($acolCargSem); ?>                         
                            <input disabled="true" type="text" name="" value="<?php echo($acolCargSem[$ltc->cpersona.'_'.$d]['totdis']); ?>" size="2" title="Horas disponibles">
                            <input disabled="true" type="text" name="" value="<?php echo($acolCargSem[$ltc->cpersona.'_'.$d]['totpla']); ?>" size="2" title="Horas Planificadas"> 
                            <input disabled="true" type="text" name="" value="<?php echo($acolCargSem[$ltc->cpersona.'_'.$d]['toteje']); ?>" size="2" title="Horas Ejecutadas">
                        </div>

                      <div id="dash_cargabilidad_{{$ltc->cpersona}}_{{$d}}" style="display:none;position:absolute;width:240px" class="panel panel-default">
                        <div class="panel-heading" >Cargabilidad de {{$ltc->abreviatura}} - semana {{$d}} </div>
                        <div class="panel-body" id="barrasCargabilidad_{{$ltc->cpersona}}_{{$d}}">
                         
                         

                          <?php 
                                $horaspla=$acolCargSem[$ltc->cpersona.'_'.$d]['pla'];
                                $horaseje=$acolCargSem[$ltc->cpersona.'_'.$d]['eje'];

                          ?>                 
                           
                        
                            <div> Planificada</div>
                            <div class="progress" style="height:16px" >
                              <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active"  role="progressbar" aria-valuenow="{{$horaspla}}" aria-valuemin="0" aria-valuemax="100" style="width:{{$horaspla}}%;font-size:10px;padding:1px;background-color: @if($horaspla<60) green @elseif($horaspla<80) orange @else red @endif">
                                <span style="display: block; position: absolute; width: 130px; color: black;" data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en horas">{{$horaspla}}%</span>
                              </div>
                            </div> 
                            <br>
                            <div> Ejecutada</div>      
                            <div class="progress " style="height:16px" >
                              <div class="progress-bar progress-bar-warning progress-bar-striped active"  role="progressbar" aria-valuenow="{{ $horaseje }}" aria-valuemin="0" aria-valuemax="100" style="width:{{$horaseje}}%;font-size:10px;padding:1px;background-color: @if($horaseje<60) green @elseif($horaseje<80) orange @else red @endif">
                                <span style="display: block; position: absolute; width: 130px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en monto">{{ $horaseje }}%</span>
                              </div>
                            </div>  

                           
                          @endif




                      
                      </div>
                    </div>

            
                      </td>
                       @endforeach            
                    </tr>
                
                    @endforeach
                  @endif

                  @endif  
              </body>
                
            </table>
          </div>



          <div class="col-lg-6 col-xs-12">

              <div class="nav-tabs-custom">
                  <ul class="nav nav-tabs">
                    <li class="active"><a href="#resumen" data-toggle="tab" style="height: 10px;">Resúmen</a></li>

                    @if(isset($tproyectos))
                    <?php $i=0; ?>

                      @foreach($tproyectos as $pry)
                      <?php $i++; ?>
                      <li><a href="#pry_{{$i}}" title="{{$pry->nombre}}" data-toggle="tab" style="height: 10px;">{{$pry->codigo}}</a></li>
                     

                      @endforeach
                    @endif


                  </ul>
              </div>

              <div class="tab-content">

                  <div class="active tab-pane" id="resumen">

                  </div>

                  @if(isset($listaProy))
                  <?php $j=0; ?>

                    @foreach($listaProy as $pry)
                    <?php $j++; ?>

                    <div class="tab-pane" id="pry_{{$j}}" value="{{$pry['cproyecto']}}" >

                      <div class="col-lg-12 col-xs-12"><h6>{{$pry['codproy']}}/{{$pry['uminera']}}/{{$pry['nombreproy']}}</h6></div>

                      <table class="table" style="width: 50%;">
                        <thead>
                          <tr class="clsCabereraTabla">
                            <th style="width: 40%">Colaborador</th>
                                @if(isset($semanasPlani))

                                  @foreach($semanasPlani as $d)                             
                                  
                                  <th>
                                  <select id="s_{{ $d }}_{{$anio}}"  style="color:blue" onchange="asignarHoras(this,'{{ $d }}_{{$anio}}');">
                                  <?php $w=1; ?>
                                      <option value="" style="color:blue"> -- </option>
                                      <?php for($w=1;$w<=12;$w++){ ?>
                                          <option value="{{ $w }}" style="color:blue">{{ $w }}</option>
                                      <?php } ?>
                                  </select>
                                  <br />Semana {{ $d }}</th>
                                  @endforeach
                          </tr>
                        </thead>
                        <tbody>
                                  @foreach($pry['colaboradores'] as $col)
                          <tr>
                              <td> {{$col->abreviatura}}</td>

                                  @foreach($semanasPlani as $d) 


                              <td>
                                  <input class="col-1" type="text" value="" size="1" onmousemove ="resaltar({{$col->cpersona}},{{$d}})" onmouseout="resaltarout();ocultarDashCol('dash_cargabilidad_{{$col->cpersona}}_{{$d}}');" onmouseover="verDashCol('dash_cargabilidad_{{$col->cpersona}}_{{$d}}');" name="{{$col->cpersona}}_{{$pry['cproyecto']}}_{{ $d }}_{{$anio}}" id="{{$col->cpersona}}_{{$pry['cproyecto']}}_{{ $d }}_{{$anio}}"> 

                              </td>
                                  @endforeach 
                              
                            
                          </tr>
                                  @endforeach

                              @endif


                        </tbody>
                    </table>






                    
                      
                  </div>

                    @endforeach
                  @endif
                  
              </div>
            
          </div>

          <br><br><br>


          
          

        </div>


      </div>

      {!! Form::close() !!}
      

    </section>


</div>

<script>

var fec_inicial='';
var fec_final='';
  
$('#fdesde').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
           
        });      
        $('#fhasta').datepicker({
            format: "dd-mm-yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
           
        }); 

$("#btnView").click(function (e){

            var fec_ini=$("#fdesde").val();
            var fec_fin=$("#fhasta").val();

          
            $.ajax({
                url: 'visualizarPlanificacion',
                type: 'GET',
                data:  {
                    "fdesde" : $("#fdesde").val(),
                    "fhasta" : $("#fhasta").val(),
                    "anio" : $("#anio").val()
                },
                beforeSend: function () {
                    //$('#div_carga').show(); 
                },              
                success : function (data){
                   // $("#tablePlani").html(data);
                    $('#resultado').html(data); 
                    $('#div_carga').hide(); 
                    fec_inicial=fec_ini;
                    fec_final=fec_fin;

                    $("#fdesde").val(fec_ini);
                    $("#fhasta").val(fec_fin);
                    //verAcumuladas();
                },
            });
}); 

function checkAll(fec,obj){
            $("input[type=checkbox]").each(function (e){
                nombre = this.name;
                if(nombre.indexOf(fec)!=-1 && !(nombre==("chk_"+fec)) ){
                    if (obj.checked){
                        this.checked=true;
                    }
                    else{
                        this.checked=false;
                    }
                }
            });                        
        }

var cper='';
var sem='';

function resaltar(cpersona,semana){
  resaltarout();
 $('#'+cpersona+'_'+semana).css({"border-color": "#FF0040", 
                                         "border-width":"1px", 
                                         "border-style":"solid"});
 cper=cpersona;
 sem=semana;
}

function resaltarout(){
  $('#'+cper+'_'+sem).css({"border-width":"0px","border-style":"solid"});
}


function verDashCol(id){
      $("#"+id).show();

      var idSplit=id.split('_');

      var cpersona=idSplit[2];
      var semana=idSplit[3];




}
function ocultarDashCol(id){
      var idSplit=id.split('_');

      var cpersona=idSplit[2];
      var semana=idSplit[3];
      $("#"+id).hide();
      $("#div_Eje"+cpersona+'_'+semana).remove();
      $("#div_Plani"+cpersona+'_'+semana).remove();
}  

function savePlani(){
 
  $.ajax({
          url: 'savePlanificacion',
          type: 'POST',    
          data : $("#frmgasto").serialize(),
          beforeSend: function () {
                    //$('#div_carga').show(); 
          },              
        success : function (data){                   
        $('#div_carga').hide();  
        $('#barraEje').val(data[1]);      
        $('#barraPlani').val(data[0]); 


        },
  });

}

$('#frmproyecto').on('submit',function(e){
  $.ajaxSetup({
     header: document.getElementById('_token').value
  });
  e.preventDefault(e);

  $.ajax({
          url: 'savePlanificacion',
          type: 'POST',    
          data : $(this).serialize(),
          beforeSend: function () {
                    //$('#div_carga').show(); 
          },              
          success : function (data){                   
          $('#div_carga').hide();  
          $('#ver').html(data); 

       


        },
  });

}); 
</script>

