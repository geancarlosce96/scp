<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Validación de Horas Ejecutadas</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        {!! Form::open(array('url' => 'grabarHR','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    
    <div class="col-md-12 col-xs-12">
    	<div class="row">
      
    <div class="col-lg-11 col-xs-11 clsPadding">
        {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
        {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
    </div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    </div>
    
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Unidad minera:</div>
        <div class="col-lg-10 col-xs-10">
            {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}   
        </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Nombre Proyecto:</div>
        <div class="col-lg-10 col-xs-10">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
    ,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}    
        </div>
    </div>    
    
    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
        <label class="clsTxtNormal">Codigo de Proyecto:</label>
         {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}
        </div>             

        <div class="col-lg-6 col-xs-6">
        	<label class="clsTxtNormal">Estado:</label>
              {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control')) !!}     
        </div>
    </div>
    <div class="row clsPadding2">
    	<div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
    	<div class="col-lg-4 col-xs-4">
        	{!! Form::text('finicio',(isset($proyecto->finicio)?$proyecto->finicio:''), array('class'=>'form-control pull-right','disabled'=>'disabled')) !!}
        </div>
    	<div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>        
    	<div class="col-lg-4 col-xs-4">
        	{!! Form::text('fcierre',(isset($proyecto->fcierre)?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}
        </div>               
    </div>
  
   

    <div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
    Rooster:
    </div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
                <div class="form-group">
                <label class="clsTxtNormal">Desde:</label>
                
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fdesde','', array('class'=>'form-control pull-right')) !!}
                </div>
                <!-- /.input group -->
                </div>        
        </div> 
        <div class="col-lg-6 col-xs-6">
                <div class="form-group">
                <label class="clsTxtNormal">Hasta:</label>
                
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fhasta','', array('class'=>'form-control pull-right')) !!}
                </div>
                <!-- /.input group -->
                </div> 
        </div>               
    </div>      
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-3">Leyenda:<br />
        <button type="button" name="ver" data-target="#viewLeyenda" data-toggle="modal" >Ver</button></div>
        <div class="col-lg-2 col-xs-3"></div>
        <div class="col-lg-2 col-xs-3"></div>
        <div class="col-lg-4  hidden-xs"></div>
        <div class="col-lg-2 col-xs-3">
        	<button type="button" class="btn btn-primary btn-block " id="btnView"> <b>Visualizar</b></button>
        </div>
    </div>
<!--  ***************************************   -->
    <div class="row clsPadding2">
        <div class="col-lg-12 col-xs-12 table-responsive" id="viewHoras">
              @include('partials.tableValidacionHorasRooster', array('crono' => (isset($crono)==1?$crono:array() ),'dias' => (isset($dias)==1?$dias:array() )  ) )
      
        </div>
    </div>
 
<!-- ****************************************** -->
    <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding">
        <div class="col-lg-4 col-xs-4">
            <button class="btn btn-primary btn-block" style="min-width:200px;"><b>Grabar horas Aceptadas</b></button> 
        </div>
        <div class="col-lg-4 col-xs-4">
            <!--<a href="#" class="btn btn-primary btn-block" style="min-width:200px;"><b>Cancelar</b></a> -->
        </div> 
        <div class="col-lg-4 col-xs-4">
            <button type="button" class="btn btn-primary btn-block" style="min-width:200px;" disabled="true"><b>Imprimir Reporte de Confirmación</b></button> 
        </div>                    
    </div>
   </div>
   </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
   





    {!! Form::close() !!}

    </section>
    <!-- /.content -->
    @include('partials.modalLeyenda',array('leyenda'=> (isset($leyenda)==1?$leyenda:array() ) ) )
    @include('partials.searchProyecto')        
  </div>

  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';        
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('verValidahorasRooster/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }


        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 
        $('#fdesde').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fhasta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 
        $('#frmproyecto').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);  
            $.ajax({

                type:"POST",
                url:'grabarValidahorasRooster',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide(); 
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }                

            });          
        });

        $("#btnView").on('click',function(e){
            $.ajax({

                type:"POST",
                url:'verValidarHorasConstruccion',
                data:$("#frmproyecto").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#viewHoras").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide(); 
                    //$('#detalle_error').html(data);
//                    $("#div_msg_error").show();

                }                

            });              
        });                         
</script>  