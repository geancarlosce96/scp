<div class="content-wrapper" style="font-size: 12px;">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Lecciones Aprendidas</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!--  tabla -->
      <!-- Table row -->
      <div class="row clsPadding" style="margin-top:10px;">
      <div class="row clsPadding" style="background-color:#FFF;">
        <!--<div class="col-lg-1 hidden-xs"></div>-->
        <div class="col-md-12 col-xs-12 table-responsive">
          <table class="table table-striped" id="listadoLA" width="100%">
            <thead>
            <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Código</td>
                <td class="clsAnchoTabla">Unidad minera</td>
                <td class="clsAnchoTabla">Nombre del proyecto</td>
                <td class="clsAnchoTabla">Descripción</td>
                <td class="clsAnchoTabla">Etapa de Proyecto</td>
                <td class="clsAnchoTabla">¿Qué sucedió?</td>
                <td class="clsAnchoTabla">¿Qué se hizo?</td>
                <td class="clsAnchoTabla">¿Qué se debió hacer?</td>
                <td class="clsAnchoTabla">Área</td>
                <td class="clsAnchoTabla">GP</td>
                <td class="clsAnchoTabla">Elaborado</td>
                <td class="clsAnchoTabla">Fecha</td>
            </tr>
            </thead>
            <tbody>
                @foreach($listaLa as $la)
                    <tr>
                        <td class="clsAnchoTabla">{{ $la->codigo }}</td>
                        <td class="clsAnchoTabla">{{ $la->uindadminera }}</td>
                        <td class="clsAnchoTabla">{{ $la->nombre }}</td>
                        <td class="clsAnchoTabla">{{ $la->descripcion }}</td>
                        <td class="clsAnchoTabla">{{ $la->fase }}</td>
                        <td class="clsAnchoTabla">{{ $la->suceso }}</td>
                        <td class="clsAnchoTabla">{{ $la->hizo }}</td>
                        <td class="clsAnchoTabla">{{ $la->debiohacer }}</td>
                        <td class="clsAnchoTabla">{{ $la->area }}</td>
                        <td class="clsAnchoTabla">{{ $la->gerente }}</td>
                        <td class="clsAnchoTabla">{{ $la->abreviatura }}</td>
                        <td class="clsAnchoTabla">{{ $la->fec }}</td>
                    </tr>
                @endforeach
            </tbody>
            <tfoot>
            <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Código</td>
                <td class="clsAnchoTabla">Unidad minera</td>
                <td class="clsAnchoTabla">Nombre del proyecto</td>
                <td class="clsAnchoTabla">Descripción</td>
                <td class="clsAnchoTabla">Etapa de Proyecto</td>
                <td class="clsAnchoTabla">¿Qué Sucedió?</td>
                <td class="clsAnchoTabla">¿Qué se hizo?</td>
                <td class="clsAnchoTabla">¿Qué se debió hacer?</td>
                <td class="clsAnchoTabla">Área</td>
                <td class="clsAnchoTabla">GP</td>
                <td class="clsAnchoTabla">Elaborado</td>
                <td class="clsAnchoTabla">Fecha</td>
            </tr>
            </tfoot>
          </table>

          <!-- <table class="table table-striped" style="border-collapse:collapse;">
    <thead>
        <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Codigo</td>
                <td class="clsAnchoTabla">Nombre del proyecto</td>
                <td class="clsAnchoTabla">Fase de Proyecto</td>
                <td class="clsAnchoTabla">Área de conocimiento</td>
                <td class="clsAnchoTabla">Descripción</td>
                <td class="clsAnchoTabla">¿Qué Sucedió?</td>
                <td class="clsAnchoTabla">¿Qué se hizo?</td>
                <td class="clsAnchoTabla">¿Qué se debió hacer?</td>
                <td class="clsAnchoTabla">Área</td>
                <td class="clsAnchoTabla">Elaborado</td>
                <td class="clsAnchoTabla">Fecha</td>
        </tr>
    </thead>
    <tbody>
        <?php $r=0; ?>
        @foreach($listaLaCab as $lapry)
        <?php $r++; ?>
        <tr >
            <td colspan="11" class="hiddenRow" style="background-color:#CACACAFF">
                <div class="accordion-toggle"  data-toggle="collapse" data-target="#demo{{ $r }}"> 
                {{ $lapry['codigo'] }} / {{ $lapry['nombre'] }}
                </div> 
            </td>
        </tr>

            
            @foreach($lapry['detalle'] as $la)
        <tr  id="demo{{ $r }}" class="accordian-body collapse">
                        <td class="clsAnchoTabla">{{ $lapry['codigo'] }}</td>
                        <td class="clsAnchoTabla">{{ $lapry['nombre'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['fase'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['areaconocimiento'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['descripcion'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['suceso'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['hizo'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['debiohacer'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['area'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['abreviatura'] }}</td>
                        <td class="clsAnchoTabla">{{ $la['fecha'] }}</td>
        </tr>
                @endforeach
            
        @endforeach
    </tbody>
    <tfoot>
            <tr class="clsCabereraTabla">
                
                <td class="clsAnchoTabla">Codigo</td>
                <td class="clsAnchoTabla">Nombre del proyecto</td>
                <td class="clsAnchoTabla">Fase de Proyecto</td>
                <td class="clsAnchoTabla">Área de conocimiento</td>
                <td class="clsAnchoTabla">Descripción</td>
                <td class="clsAnchoTabla">¿Qué Sucedió?</td>
                <td class="clsAnchoTabla">¿Qué se hizo?</td>
                <td class="clsAnchoTabla">¿Qué se debió hacer?</td>
                <td class="clsAnchoTabla">Área</td>
                <td class="clsAnchoTabla">Elaborado</td>
                <td class="clsAnchoTabla">Fecha</td>
            </tr>
            </tfoot>
</table> -->
        </div>
        <!--<div class="col-lg-1 hidden-xs"></div>-->
        <!-- /.col -->
      </div>
      </div>
      <!-- /.row -->    
    
    <!-- Fin de tabla -->
</section>

</div>



<!-- <script src="https://code.jquery.com/jquery-1.12.4.js"></script> -->
<!-- <script src="plugins/datatables/dataTables.bootstrap.min.js"></script> -->
<!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script> -->
<!-- <script>
 // Write on keyup event of keyword input element
 $(document).ready(function(){
 $("#search").keyup(function(){
 _this = this;
 // Show only matching TR, hide rest of them
 $.each($("#listadoLA tbody tr"), function() {
 if($(this).text().toLowerCase().indexOf($(_this).val().toLowerCase()) === -1)
 $(this).hide();
 else
 $(this).show();
 });
 });
});
</script> -->
<script>
// $(document).ready(function() {
    // $("#listadoLA").DataTable({
    //   "destroy": true,
    //   "processing" : true,
    //   "serverSide" : true,
    //   "ajax" : "listarLA",
    //   "columns" : [
    //     {data : 'descripcion' , name : 'tle.descripcion'},
    //     {data : 'fecha', name: 'tle.fecha'},
    //     {data : 'area' , name : 'ta.area'},
    //     {data : 'fase', name: 'tfp.fase'},
    //     {data : 'areaconocimiento' , name : 'tac.areaconocimiento'},
    //     {data : 'suceso' , name : 'tle.suceso'},
    //     {data : 'hizo' , name : 'tle.hizo'},
    //     {data : 'debiohacer' , name : 'tle.debiohacer'}, 
    //     {data : 'elaborado' , name : 'tp.abreviatura'}
    //   ]
      // ,
      // "language": {
      //       "lengthMenu": "Mostrar _MENU_ registros por página",
      //       "zeroRecords": "Sin Resultados",
      //       "info": "Página _PAGE_ de _PAGES_ ( _MAX_ )",
      //       "infoEmpty": "No existe registros disponibles",
      //       "infoFiltered": "(filtrado de un _MAX_ total de registros)",
      //       "search":         "Buscar:",
      //       "processing":     "Procesando...",
      //       "paginate": {
      //           "first":      "Inicio",
      //           "last":       "Ultimo",
      //           "next":       "Siguiente",
      //           "previous":   "Anterior"
      //       },
      //       "loadingRecords": "Cargando..."
      //   }
    // });
// } );

var table = $('#listadoLA').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     true,
        // lengthChange: false,
        buttons: [ 'excel' ],
        // scrollY:        "600px",
        // scrollX:        true,
        // scrollCollapse: false,
        // paging:         true,
        // fixedColumns:   {
        //     leftColumns: 1
        // }
    } ); 

    table.buttons().container()
        .appendTo( '#listadoLA_wrapper .col-sm-6:eq(0)' );




// $(document).ready(function() {
//     $('#listadoLA').DataTable({
//     "columns": [
//             { "data": "etat" },
//             { "data": "dest" },
//             { "data": "message" },
//             { "data": "exp" },
//             { "data": "date" },
//             { "data": "extra" }
//             ],
//             "order": [[ 4, "desc" ]],
//             "responsive": true,
//     drawCallback: function (settings) {
//                 var api = this.api();
//                 var rows = api.rows({ page: 'current' }).nodes();
//                 var last = null;

//                 api.column(4, { page: 'current' }).data().each(function (group, i) {

//                     if (last !== group) {

//                         $(rows).eq(i).before(
//                             '<tr class="group"><td colspan="8" style="BACKGROUND-COLOR:rgb(237, 208, 0);font-weight:700;color:#006232;">' + 'GRUPO ....' + group  + '</td></tr>'
//                         );

//                         last = group;
//                     }
//                 });
//             }

//     });

// } );

// var $table = $('#listadoLA');

// $(function () {
//   buildTable($table, 8, 1);
// });

// function expandTable($detail, cells) {
//   buildTable($detail.html('<table></table>').find('table'), cells, 1);
// }

// function buildTable($el, cells, rows) {
//   var i, j, row,
//       columns = [],
//       data = [];

//   for (i = 0; i < cells; i++) {
//     columns.push({
//       field: 'field' + i,
//       title: 'Cell' + i,
//       sortable: true
//     });
//   }
//   for (i = 0; i < rows; i++) {
//     row = {};
//     for (j = 0; j < cells; j++) {
//       row['field' + j] = 'Row-' + i + '-' + j;
//     }
//     data.push(row);
//   }
//   $el.bootstrapTable({
//     columns: columns,
//     data: data,
//     detailView: cells > 1,
//     onExpandRow: function (index, row, $detail) {
//       expandTable($detail, cells - 1);
//     }
//   });
// }

</script>  