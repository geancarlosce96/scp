
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style>
		/** Establezca los márgenes de la página en 0, de modo que el pie de página y el encabezado Puede ser de la altura y anchura total. !**/
		@page {
			margin: 1cm 2cm;
		}
		/** Define ahora los márgenes reales de cada página en el PDF. **/
		body {
			margin-top: 3.05cm;
			margin-left: 0cm;
			margin-right: 0cm;
			margin-bottom: 0.5cm;
		}
		/** Definir las reglas de encabezado. **/
		header {
			position: fixed;
			top: 1.15cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
		}
		/** Definir las reglas del pie de página. **/
		footer {
			position: fixed;
			bottom: 0cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
		}
	</style>
</head>
	<body>
		<header>
				<div class="row">
					<table style="border-collapse: collapse; size: 0.5px;font-size: 11px;width: 100%">
						<tr>
							<td rowspan="1" style="border:1px solid #000000FF; width: 30%;text-align: center;height:5%"><img style="width: 150px;" src="images/logorpt.jpg"></td>
							<td rowspan="2" style="border:1px solid #000000FF; width: 40%;text-align: center;font-size: 11px;"><strong><strong>GERENCIA DE PROYECTOS<br>Lista de Entregables </strong></td>
							<td rowspan="1"  style="border:1px solid #000000FF; width: 30%;font-size: 9px">
								<table width="100%">
									<tr  style="text-align: center;">
										<td colspan="3"><?php echo (isset($proyecto[0])==1?$proyecto[0]['codproyecto']:'');?>-AND-25-LE-001</td>

									</tr>

									<tr>
										<td width="20%">Revisión</td>
										<td width="5%">:</td>
										<td width="75%"><?php echo (isset($proyecto[0])==1?$proyecto[0]['documento_rev']->revision:'');?></td>

									</tr>
									<tr>
										<td width="20%">Fecha</td>
										<td width="5%">:</td>
										<td width="75%"><?php echo (isset($proyecto[0])==1?$proyecto[0]['documento_rev']->fecharev:'');?></td>

									</tr>
								</table>

							</td>
						</tr>
						<tr>

							<td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> SIG AND </td>
							<td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> 10-AND-25-FOR-0207 / R0 / 26-03-18 </td>
						</tr>
					</table>
				</div>

		</header>
		<section class="content" style="font-family: Arial,Helvetica,sans-serif;">
			<div class="row">

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 8%;"> <strong>Proyecto</strong></td>
						<td colspan="3" style="border:1px solid #000000FF;width: 62%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['nombreproyecto']:'');?></td>
						<td style="border:1px solid #000000FF; width: 13%;"><strong>Proyecto N°</strong></td>
						<td style="border:1px solid #000000FF; width: 17%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['codproyecto']:'');?></td>
					</tr>

					<tr>
						<td style="border:1px solid #000000FF;width: 8%"><strong>Cliente</strong></td>
						<td colspan="3" style="border:1px solid #000000FF;width: 62%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['cliente']:'');?></td>
						<td style="border:1px solid #000000FF; width: 13%;"><strong>Unidad Minera</strong></td>
						<td  style="border:1px solid #000000FF; width: 17%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['uminera']:'');?></td>
					</tr>



				</table>
			</div>
			<div class="row">

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">
					<tr>
						<td style="border:1px solid #000000FF;width: 8%;"><strong>GP</strong></td>
						<td style="border:1px solid #000000FF;width: 22%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['gerente']:'');?></td>
						<td style="border:1px solid #000000FF;width: 12%;"><strong>N° de CON / OS</strong></td>
						<td style="border:1px solid #000000FF;width: 28%;">DL-APU-029-17</td>
						<td style="border:1px solid #000000FF; width: 13%;"><strong>N° de Propuesta</strong></td>
						<td style="border:1px solid #000000FF; width: 17%;"><?php echo (isset($proyecto[0])==1?$proyecto[0]['propuesta']:'');?></td>
					</tr>


				</table>
			</div>
			<div class="row">

				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">

					<tr>
						<td style="border:1px solid #000000FF;width: 8%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Item</strong></td>
						<td style="border:1px solid #000000FF;width: 22%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Codigo AND</strong></td>
						<td style="border:1px solid #000000FF;width: 12%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Disciplina</strong></td>
						<td style="border:1px solid #000000FF;width: 28%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Descripción de entregables</strong></td>
						<td style="border:1px solid #000000FF; width: 13%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Fecha A</strong></td>
						<td style="border:1px solid #000000FF; width: 8.5%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Fecha B</strong></td>
						<td style="border:1px solid #000000FF; width: 8.5%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1)"><strong>Fecha 0</strong></td>
					</tr>
				</table>
			</div>
			<div class="row">


				<?php if (isset($edt)==1) {  ?>

				<?php foreach ($edt as $e) { ?>
				<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">

					<?php

					$colorfondo = '';
					$colorletra = 'black';

					if ($e['nivel'] == 0) {$colorfondo='rgb(0,176,80)';$colorletra = 'white';}
					if ($e['nivel'] == 1) {$colorfondo='rgb(0,105,170)';$colorletra = 'white';}
					if ($e['nivel'] == 2) {$colorfondo='rgb(128,128,128)';$colorletra = 'black';}
					if ($e['nivel'] == 3) {$colorfondo='rgb(166,166,166)';$colorletra = 'black';}

					?>

					<?php if ($e['nivel'] != 0) { ?>



					<tr>
						<td style="border:1px solid #000000FF;width: 8%;text-align: center;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>{{$e['codigo']}}</strong></td>
						<td colspan="6" style="border:1px solid #000000FF;width: 92%;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>{{$e['descripcion']}}</strong></td>
					</tr>
					<!--  Documentos -->

					@if(count($e['ent_documentos'])>0)

						<?php //dd($e['ent_documentos']) ?>

						@if($e['ctipoedt']!='1')

							<tr>
								<td colspan="7" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Documentos</strong>
								</td>
							</tr>

						@endif


						<?php
						$contador_d=0;
						$contador_d=count($e['ent_documentos'])+1;
						?>

						@foreach($e['ent_documentos'] as $key1 => $edc)

							<?php $contador_d --  ?>

							<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">
								<tr>
									<td style="border:1px solid #000000FF;width: 8%;text-align: center">{{$contador_d}}</td>
									<td style="border:1px solid #000000FF;width: 22%;">{{$edc->codigo}}</td>
									<td style="border:1px solid #000000FF;width: 12%;"><center>{{$edc->disciplina}}</center></td>
									<td style="border:1px solid #000000FF;width: 28%;">{{$edc->descripcion_entregable}}</td>
									<td style="border:1px solid #000000FF; width: 13%;"><center>{{$edc->fechaA}}</center></td>
									<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$edc->fechaB}}</center></td>
									<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$edc->fecha0}}</center></td>
								</tr>

								@if(count($edc->anexos_mapas)>0)
									<tr>
										<td style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><
										</td>
										<td colspan="6" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Mapas</strong>
										</td>
									</tr>

									@foreach($edc->anexos_mapas as $am)

									<!-- <table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%"> -->
										<tr>
											<td style="border:1px solid #000000FF;width: 8%;text-align: center"></td>
											<td style="border:1px solid #000000FF;width: 22%;">{{$am->codigo}}</td>
											<td style="border:1px solid #000000FF;width: 12%;"><center>{{$am->disciplina}}</center></td>
											<td style="border:1px solid #000000FF;width: 28%;">{{$am->descripcion_entregable}}</td>
											<td style="border:1px solid #000000FF; width: 13%;"><center>{{$am->fechaA}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$am->fechaB}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$am->fecha0}}</center></td>
										</tr>
										<!-- </table> -->

									@endforeach

								@endif


								@if(count($edc->anexos_figuras)>0)

									<tr>
										<td style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><
										</td>
										<td colspan="6" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Figuras</strong>
										</td>
									</tr>

									@foreach($edc->anexos_figuras as $af)

									<!-- <table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%"> -->
										<tr>
											<td style="border:1px solid #000000FF;width: 8%;text-align: center"></td>
											<td style="border:1px solid #000000FF;width: 22%;">{{$af->codigo}}</td>
											<td style="border:1px solid #000000FF;width: 12%;"><center>{{$af->disciplina}}</center></td>
											<td style="border:1px solid #000000FF;width: 28%;">{{$af->descripcion_entregable}}</td>
											<td style="border:1px solid #000000FF; width: 13%;"><center>{{$af->fechaA}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$af->fechaB}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$af->fecha0}}</center></td>

										</tr>
										<!-- </table> -->

									@endforeach

								@endif

							</table>

						@endforeach
					@endif

				<!--  Planos -->

					@if(count($e['ent_planos'])>0)

						<tr>
							<td colspan="7" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Planos</strong>
							</td>
						</tr>

						<?php
						$contador_p=0;
						$contador_p=count($e['ent_planos'])+1;
						?>

						@foreach($e['ent_planos'] as $edc)

							<?php $contador_p --  ?>

							<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">
								<tr>
									<td style="border:1px solid #000000FF;width: 8%;text-align: center">{{$contador_p}}</td>
									<td style="border:1px solid #000000FF;width: 22%;">{{$edc->codigo}}</td>
									<td style="border:1px solid #000000FF;width: 12%;"><center>{{$edc->disciplina}}</center></td>
									<td style="border:1px solid #000000FF;width: 28%;">{{$edc->descripcion_entregable}}</td>
									<td style="border:1px solid #000000FF; width: 13%;"><center>{{$edc->fechaA}}</center></td>
									<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$edc->fechaB}}</center></td>
									<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$edc->fecha0}}</center></td>
								</tr>


								@if(count($edc->anexos_mapas)>0)
									<tr>
										<td style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><
										</td>
										<td colspan="6" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Mapas</strong>
										</td>
									</tr>

									@foreach($edc->anexos_mapas as $am)

									<!-- <table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%"> -->
										<tr>
											<td style="border:1px solid #000000FF;width: 8%;text-align: center"></td>
											<td style="border:1px solid #000000FF;width: 22%;">{{$am->codigo}}</td>
											<td style="border:1px solid #000000FF;width: 12%;"><center>{{$am->disciplina}}</center></td>
											<td style="border:1px solid #000000FF;width: 28%;">{{$am->descripcion_entregable}}</td>
											<td style="border:1px solid #000000FF; width: 13%;"><center>{{$am->fechaA}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$am->fechaB}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$am->fecha0}}</center></td>
										</tr>
										<!-- </table> -->

									@endforeach

								@endif

								@if(count($edc->anexos_figuras)>0)

									<tr>
										<td style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><
										</td>
										<td colspan="6" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Figuras</strong>
										</td>
									</tr>

									@foreach($edc->anexos_figuras as $af)

									<!-- <table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%"> -->
										<tr>
											<td style="border:1px solid #000000FF;width: 8%;text-align: center"></td>
											<td style="border:1px solid #000000FF;width: 22%;">{{$af->codigo}}</td>
											<td style="border:1px solid #000000FF;width: 12%;"><center>{{$af->disciplina}}</center></td>
											<td style="border:1px solid #000000FF;width: 28%;">{{$af->descripcion_entregable}}</td>
											<td style="border:1px solid #000000FF; width: 13%;"><center>{{$af->fechaA}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$af->fechaB}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$af->fecha0}}</center></td>
										</tr>
										<!-- </table> -->

									@endforeach

								@endif

							</table>


						@endforeach
					@endif

				<!--  Mapas -->

					@if(count($e['ent_mapas'])>0)

						<tr>
							<td colspan="7" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Mapas</strong>
							</td>
						</tr>

						<?php
						$contador_m=0;
						$contador_m=count($e['ent_mapas'])+1;
						?>

						@foreach($e['ent_mapas'] as $edc)

							<?php $contador_m --  ?>

							<table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%">
								<tr>
									<td style="border:1px solid #000000FF;width: 8%;text-align: center">{{$contador_m}}</td>
									<td style="border:1px solid #000000FF;width: 22%;">{{$edc->codigo}}</td>
									<td style="border:1px solid #000000FF;width: 12%;"><center>{{$edc->disciplina}}</center></td>
									<td style="border:1px solid #000000FF;width: 28%;">{{$edc->descripcion_entregable}}</td>
									<td style="border:1px solid #000000FF; width: 13%;"><center>{{$edc->fechaA}}</center></td>
									<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$edc->fechaB}}</center></td>
									<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$edc->fecha0}}</center></td>
								</tr>

								@if(count($edc->anexos_mapas)>0)
									<tr>
										<td style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><
										</td>
										<td colspan="6" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Mapas</strong>
										</td>
									</tr>

									@foreach($edc->anexos_mapas as $am)

									<!-- <table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%"> -->
										<tr>
											<td style="border:1px solid #000000FF;width: 8%;text-align: center"></td>
											<td style="border:1px solid #000000FF;width: 22%;">{{$am->codigo}}</td>
											<td style="border:1px solid #000000FF;width: 12%;"><center>{{$am->disciplina}}</center></td>
											<td style="border:1px solid #000000FF;width: 28%;">{{$am->descripcion_entregable}}</td>
											<td style="border:1px solid #000000FF; width: 13%;"><center>{{$am->fechaA}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$am->fechaB}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$am->fecha0}}</center></td>
										</tr>
										<!-- </table> -->

									@endforeach

								@endif


								@if(count($edc->anexos_figuras)>0)

									<tr>
										<td style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><
										</td>
										<td colspan="6" style="border:1px solid #000000FF;color: {{$colorletra}};background-color:{{$colorfondo}}"><strong>Figuras</strong>
										</td>
									</tr>

									@foreach($edc->anexos_figuras as $af)

									<!-- <table style="border-collapse: collapse; size: 0.5px;font-size: 9px;width: 100%"> -->
										<tr>
											<td style="border:1px solid #000000FF;width: 8%;text-align: center"></td>
											<td style="border:1px solid #000000FF;width: 22%;">{{$af->codigo}}</td>
											<td style="border:1px solid #000000FF;width: 12%;"><center>{{$af->disciplina}}</center></td>
											<td style="border:1px solid #000000FF;width: 28%;">{{$af->descripcion_entregable}}</td>
											<td style="border:1px solid #000000FF; width: 13%;"><center>{{$af->fechaA}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$af->fechaB}}</center></td>
											<td style="border:1px solid #000000FF; width: 8.5%;"><center>{{$af->fecha0}}</center></td>
										</tr>
										<!-- </table> -->

									@endforeach

								@endif


							</table>

						@endforeach
					@endif
					<?php } ?>

				</table>

				<?php } ?>

				<?php } ?>


			</div>

		</section>
	</body>
</html>

