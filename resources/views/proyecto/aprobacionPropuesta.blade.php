<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Propuestas Ganadas - Validación de Gerente de Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

    {!! Form::open(array('url' => 'grabarAprobacionPropuesta','method' => 'POST','id' =>'frmpropuesta')) !!}
    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
    <div class="row">
        <!--<div class="col-lg-1 hidden-xs"></div>-->
        <div class="col-md-12 col-xs-12">
        
            <div class="row">
            {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:'')) !!}
            <div class="col-lg-2 col-xs-2 clsPadding">Propuesta:</div>
            <div class="col-lg-3 col-xs-3 clsPadding">{!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)?$propuesta->ccodigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
            <div class="col-lg-6 col-xs-6 clsPadding">{!! Form::text('nombrepropuesta',(isset($propuesta->nombre)==1?$propuesta->nombre:'')
        ,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de la Propuesta')) !!}</div>
            <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle="modal" data-target="#searchPropuesta"><b>...</b></button></div>
            
            <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
            <div class="col-lg-3 col-xs-3 clsPadding">{!! Form::text('nombrecliente',(isset($persona->nombre)?$persona->nombre:''),array('class'=>'form-control input-sm','disabled' => 'true')) !!}</div>
            <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
            <div class="col-lg-5 col-xs-5 clsPadding">{!! Form::text('nombreuminera',(isset($uminera->nombre)==1?$uminera->nombre:''),array('disabled'=>'','class'=> 'form-control input-sm','placeholder'=>'Nombre Unidad Minera')) !!}  </div>    
                
                
                  
              
             </div>    
            <div class="row clsPadding">
            <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
            </div>


            <div class="row clsPadding2">
                <div class="col-lg-3 col-xs-3">Nombre de la Propuesta:</div>
                <div class="col-lg-9 col-xs-9">
            		{!! Form::text('nombrepropuesta',(isset($propuesta->nombre)==1?$propuesta->nombre:'')
            ,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de la Propuesta')) !!}   
                </div>
                   
            </div>
            <div class="row clsPadding2">
                <div class="col-lg-3 col-xs-3">Descripción de la propuesta:</div>
                <div class="col-lg-9 col-xs-9">
            		{!! Form::textarea('propuestadescripcion',(isset($propuesta->descripcion)==1?$propuesta->descripcion:''), array('class' => 'form-control', 'rows'=> '3','placeholder' => 'Descripción de la Propuesta')) !!}  
                </div> 
            </div> 
            <div class="row clsPadding2">

                <div class="col-lg-6 col-xs-6">
                    <label>Estado:</label>
                    {!! Form::select('estadopropuesta',(isset($estadopropuesta)==1?$estadopropuesta:array()),(isset($propuesta->cestadopropuesta)==1?$propuesta->cestadopropuesta:''),array('class' => 'form-control','disabled'=>'')) !!}
            	</div>
                <div class="col-lg-6 col-xs-6">
                <label>GP:</label>
                    {!! Form::select('gtepropuesta',(isset($personal)==1?$personal:array()),(isset($propuesta->cpersona_gteproyecto)==1?$propuesta->cpersona_gteproyecto:''),array('class' => 'form-control')) !!}
            	</div>    
            </div>





            <div class="row clsPadding">
            <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
            </div>


            <div class="row clsPadding2">
                <div class="col-lg-4 col-xs-4">
                    <button class="btn btn-primary btn-block" style="width:200px;"
                    @if(isset($editar))
                        @if(!$editar)
                            disabled="true" 
                        @endif
                    @endif
                    ><b>Grabar</b></button> 
                </div>
                <div class="col-lg-4 col-xs-4">
                    <button class="btn btn-primary btn-block" style="width:200px;" disabled="true"><b>Cancelar</b></button> 
                </div>        
            </div>
        
        </div>
        <!--<div class="col-lg-1 hidden-xs"></div>-->
 





    </div>
    {!! Form::close() !!}
    </section>
    <!-- /.content -->


    <!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuestas</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuestas" class="table" style="font-size: 12px" width="100%">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Cliente</th>
                  <th>Unidad Minera</th>
                  <th>Propuesta</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal -->


    

  </div>



<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
//$(document).ready(function(){
    //alert('1');
        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
        var table=$("#tPropuestas").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listaPropuestaEstados",
                "data": function (data){
                    data.estado = '006';
                    data._token ='{{ csrf_token() }}';
                },
                "type": "POST",
            },
            "columns":[
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tPropuestas tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#frmpropuesta').on('submit',function(e){
                    //alert('save');
                    $.ajaxSetup({
                        header: document.getElementById('_token').value
                    });
                    e.preventDefault(e);

                    $('input+span>strong').text('');
                    $('input').parent().parent().removeClass('has-error');            

                    $.ajax({

                        type:"POST",
                        url:'grabarAprobacionPropuesta',
                        data:$(this).serialize(),
                        beforeSend: function () {
                            $('#div_carga').show(); 
                        },
                        success: function(data){
                             $(".alert-success").prop("hidden", false);
                             $('#div_carga').hide(); 
                             $("#resultado").html(data);
                             $("#div_msg").show();
                        },
                        error: function(data){
                            $.each(data.responseJSON, function (key, value) {
                                    var input = '#frmpropuesta input[name=' + key + ']';
                                    $(input + '+span>strong').text(value);
                                    $(input).parent().parent().addClass('has-error');

                            });
                            //alert(data);
                            $('#div_carga').hide();
                            $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                        }
                    });
        });
        $('#searchPropuesta').on('hidden.bs.modal', function () {
           goEditar();
        });
        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              //alert(id.substring(4));
              getUrl('editarAprobacionPropuesta/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }  
//});         
</script>