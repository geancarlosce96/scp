<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Control de Ejecución</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">        
    


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
    {!! Form::open(array('url' => 'grabarcontrolEjecucion','method' => 'POST','id' =>'frmproyecto','class'=>'form-horizontal')) !!}
    {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!}    

   <div class="row">
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
    <div class="col-lg-9 col-xs-9 clsPadding">    
    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
    {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
    </div>

    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>

    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">
    {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
    </div>
    <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">
    {!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}
    </div>
   </div>    
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
        <div class="col-lg-9 col-xs-9">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}       
        </div>	 
    </div> 
    <div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
        <div class="col-lg-4 col-xs-4">
            {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right','disabled' =>'true')) !!}
        </div> 
        <div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>
        <div class="col-lg-4 col-xs-4">
            {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-6 col-xs-6">
            <label for="cestadoproyecto" class="col-sm-2 control-label">Estado</label> 
              
            {!! Form::select( 'cestadoproyecto',(isset($testado)==1?$testado:array() ),'',array('class' => 'form-control','id'=>'cestadoproyecto')) !!}
              
        </div>
        <div class="col-lg-6 col-xs-6">
            <label for="cdisciplina" class="col-sm-2 control-label">Area</label> 
              
                {!! Form::select( 'cdisciplina',(isset($tarea)==1?$tarea:array() ),'',array('class' => 'form-control','id'=>'cdisciplina')) !!}
                    
        </div>    
    </div>
    

    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-6 clsPadding2">
            <div class="form-group">
            <label class="clsTxtNormal">Fecha Desde:</label>
            
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              {!! Form::text('fdesde','', array('class'=>'form-control pull-right','id' => 'fdesde')) !!}
            </div>
            <!-- /.input group -->
            </div>       
        </div>  
        <div class="col-lg-3 col-xs-6 clsPadding2">
            <div class="form-group">
            <label class="clsTxtNormal">Fecha Hasta:</label>
            
            <div class="input-group date">
              <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
              </div>
              {!! Form::text('fhasta','', array('class'=>'form-control pull-right','id' => 'fhasta')) !!}
            </div>
            <!-- /.input group -->
            </div>       
        </div>
        <div class="col-lg-3 col-xs-6 clsPadding2"></div>
        <div class="col-lg-3 col-xs-6 clsPadding2">
	        <button type="button" class="btn btn-primary btn-block " id="btnVista"><b>Visualizar</b></button>
        </div>
        
    </div>

    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    
    
    


<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
         @include('partials.modalcontrolEjecucion',array('listcont'=> (isset($listcont)==1?$listcont:array() )))

          
    </div>
</div>


   
    
    
    

    
 
    <div class="row">
    <div class="col-lg-3 col-xs-12 clsPadding">
    	<a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> </div>       
    </div>
   
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
    {!! Form::close() !!}
    </section>
     @include('partials.searchProyecto')
    <!-- /.content -->

  </div>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>
     

        

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';        

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu"    : "Mostrar _MENU_ registros por página",
                "zeroRecords"   : "Sin Resultados",
                "info"          : "Página_PAGE_ de _PAGES",
                "infoEmpty"     : "No existe registros disponibles",
                "infoFiltered"  : "(filtrado de un_MAX_ total de registros)",
                "search"        : "Buscar:",
                "processing"    :  "Procesando...",
                "paginate"      : {
                    "first"     :    "Inicio",
                    "last"      :    "Ultimo",
                    "next"      :     "Siguiente",
                    "previous"  :   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarControlEjecucion/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $("#btnVista").click(function (e){

            e.preventDefault();
            if ($("#disciplina").val()=='' || $("#fhasta").val()=='' || $("#fdesde").val()==''){
                alert('Especifique el Area, Fecha Desde, Fecha Hasta de la Planificación');
                return;
            }
            
            $.ajax({
                url: 'viewPlanificacionProyecto',
                type: 'GET',
                data: "cproyecto="+ $('#cproyecto').val() +"&cdisciplina="+$("#disciplina").val()+"&fhasta="+ $("#fhasta").val() + "&fdesde=" + $("#fdesde").val(),
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#tablePlani").html(data);
                    $('#div_carga').hide(); 
                },
            });
        });

        $('#fdesde').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fhasta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        //botn Grabar

        $('#frmproyecto').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            

            $.ajax({

                type:"POST",
                url:'grabarcontrolEjecucion',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
                
                },
                success: function(data){

                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });

    </script>