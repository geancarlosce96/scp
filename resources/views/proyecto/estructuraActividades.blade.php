<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Estructura de Actividades</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoProyecto')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>

  {!! Form::open(array('url' => 'grabarEstructuraProyecto','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
    <div class="row">
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
    <div class="col-lg-9 col-xs-9 clsPadding">
      {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
      {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
    </div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    
    
    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">
      {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
    </div> 
    <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>        
        
        
          
      
     </div>    
<div class="row clsPadding">
<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>


<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
    <div class="col-lg-9 col-xs-9">
		{!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}   
    </div>	 
</div> 
<div class="row clsPadding2">
    <div class="col-lg-2 col-xs-2">Fecha Inicio:</div>
    <div class="col-lg-4 col-xs-4"><div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right')) !!}</div> </div> 
    <div class="col-lg-2 col-xs-2">Fecha de Cierre:</div>
    <div class="col-lg-4 col-xs-4">
      <div class="input-group date">
              <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>

              </div>
              {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right')) !!}
      </div>
      
    </div>
</div>
<div class="row clsPadding2">

    <div class="col-lg-6 col-xs-6">
        <label>Estado:</label>
          {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control')) !!}  
	</div>
    <div class="col-lg-6 col-xs-6">
    <label>Subproyecto:</label>
      {!! Form::select('subproyecto',(isset($subproyecto)==1?$subproyecto:array()),null,array('class' => 'form-control','id'=>'subproyecto')) !!} 
	</div>    
</div>

<!--  ***************************************   -->
    <div class="row clsPadding2">
        <div class="col-lg-9 col-xs-9 clsTitulo">Actividades:</div>
        <div class="col-lg-1 col-xs-1" style="text-align:right;">Acciones:</div>
        <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addActividad"> <b>Agregar</b></button></div>
        <div class="col-lg-1 col-xs-1"><!--<a href="#" class="btn btn-primary btn-block "> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>--></div>        
    </div>
<!--  *****************************************  -->
    <div class="row clsPadding2">
      <div class="col-lg-12 col-xs-12 table-responsive" id="idActividades">
        @include('partials.tableActividadProyecto',array('actividadesp' => (isset($actividadesp)==1?$actividadesp:array())))    
      </div>
    </div>
          
          
<!--  ***************************************   -->
    <div class="row clsPadding2">
        <div class="col-lg-9 col-xs-9 clsTitulo">Estructura:</div>
        <div class="col-lg-1 col-xs-1" style="text-align:right;">Acciones:</div>
        <div class="col-lg-1 col-xs-1"><button type='button' class="btn btn-primary btn-block" id="btnAddEst"> <b>Agregar</b></button></div>
        <div class="col-lg-1 col-xs-1"><!--<a href="#" class="btn btn-primary btn-block "> <i class="fa fa-trash-o" aria-hidden="true"></i> </a>--></div>        
    </div>
<!--  *****************************************  -->   

<div class="row clsPadding2">
    <div class="col-lg-1 col-xs-6">Categoria:</div>
    <div class="col-lg-3 col-xs-6">
            {!! Form::select('categorias',(isset($categorias)==1?$categorias:array()),null,array('class'=>'form-control','id'=>'categorias')) !!}    
    </div>
    <div class="col-lg-1 col-xs-6">Profesional:</div>
    <div class="col-lg-3 col-xs-6">
            {!! Form::select('profesional',(isset($personal)==1?$personal:array()),null,array('class' => 'form-control','id'=>'profesional')) !!}   
    </div>
    <div class="col-lg-1 col-xs-6">Rate:</div>
    <div class="col-lg-3 col-xs-6">
      {!! Form::text('rate','',array('id'=>'rate','class'=>'form-control')) !!}   
    </div>
</div>


<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 table-responsive" id="idPersonal">
      @include('partials.tableEstructuraProyecto',array('participantes' => (isset($participantes)==1?$participantes:array())))
  
    </div>
</div>

       
  
    </div>
</div>



        <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>


        <div class="row clsPadding2">
            <div class="col-lg-4 col-xs-4">
                <button type="submit" class="btn btn-primary btn-block" style="width:200px;"><b>Grabar</b></button> 
            </div>
            <div class="col-lg-4 col-xs-4">
                <button type="button" class="btn btn-primary btn-block" style="width:200px;" onclick="goEditar();"><b>Cancelar</b></button> 
            </div>        
        </div>
    

    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 
</div>
    {!! Form::close() !!}
    </section>
    <!-- /.content -->

    <!-- Modal Buscar Proyecto-->
    @include('partials.searchProyecto')

    <!-- Modal buscar actividades -->
    <div id="addActividad" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Actividades</h4>
          </div>
          <div class="modal-body">
            <div class="table-responsive" id="divAct">
                <?php /*@include('partials.panelActividad',array('actividades' => (isset($actividades)==1?$actividades:array())))*/ ?>
                <div id="treeActividades">
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddAct">Aceptar</button>
          </div>
        </div>

      </div>
    </div>
    <!-- Fin Modal buscar actividades-->

  </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        var selected_actividad =[];
        var personal_rate =<?php echo (isset($personal_rate)==1?json_encode($personal_rate):'[]'); ?>

        $.fn.dataTable.ext.errMode = 'throw';
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });

        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        });     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('editarEstructuraProyecto/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });       

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });  


        $("#addActividad").on('show.bs.modal',function(e){
          
          if ($("#subproyecto").val()=='' || $("#subproyecto").val()==null){
            alert('Seleccione el Subproyecto...');
            e.preventDefault();
            
          }
          selected_actividad.splice(0,selected_actividad.length);
          $('#treeActividades').highCheckTree({
            data: getTreeActividad(),
            onCheck: function (node){
              selected_actividad.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr('rel'));
              if(idx != -1){
                selected_actividad.splice(idx,1);
              }
            }
          });               
          
        }); 
        $("#idAddAct").click(function(e){
          $.ajax({
                url:'addActividadPy',
                type: 'POST',
                data : {
                  "_token": "{{ csrf_token() }}",
                  selected_actividad:selected_actividad,
                  subproyecto:$('#subproyecto').val()
                  },
                //$('input[name="acti[]"]').serialize() + "&subproyecto="+ $('#subproyecto').val(),
                beforeSend: function(){
                  //$("input[name='acti[]']").attr('checked',false);
                  $('#div_carga').show(); 
                },
                success: function(data){
                    $("#idActividades").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        });  
        function fdelActi(id){
          $.ajax({
                url:'delActividadPy/'+id,
                type: 'GET',
                beforeSend: function(){
                    $('#div_carga').show(); 
                },                
                success: function (data){
                  $("#idActividades").html(data);
                  $('#div_carga').hide(); 
                },
          });

        };     
        $("#btnAddEst").click(function(e){
          if($("#categorias").val()==''){
            alert('Especifique la categoria');
            return;
          }

          $.ajax({
            url: 'addEstructuraPy',
            type:'GET',
            data: "categorias=" + $("#categorias").val()+"&profesional=" + $("#profesional").val() + "&rate=" + $("#rate").val()+"&cproyecto=" + $("#cproyecto").val() ,
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
            success : function (data){
              $("#idPersonal").html(data);
              $('#div_carga').hide(); 
            },
          });
        })
        function fdelEstru(id){
          $.ajax({
              url: 'delEstructuraPy/'+id,
              type: 'GET',
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){
                $("#idPersonal").html(data);
                 $('#div_carga').hide(); 
              },

          });
        }

        $("#frmproyecto").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarEstructuraProyecto',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $.each(data.responseJSON, function (key, value) {
                            var input = '#frmproyecto input[name=' + key + ']';
                            if(!($(input).length > 0)){
                                input ='#frmproyecto select[name=' + key + ']';
                            }
                            $(input).parent().parent().addClass('has-error');

                    });
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });

        $('#profesional').on('change', function() {
          sel= this.value; // or $(this).val()
          $.each(personal_rate, function( index, value ) {
            //alert( index + ": " + value['cpersona'] + "--" + value['rate1'] );
            if(value['cpersona']==sel){
              $("#rate").val(value['rate1']);
            }
          });          
        });        
        function getTreeActividad() {
          
          var tree = [
          <?php echo (isset($estructura)==1?$estructura:'') ?>
          
          ];          
          return tree;
        }
</script>