<style>
    .estilocabecera{
        padding-right: 2px !important ;
        padding-left: 2px !important ;
        width: 10px !important;
    }
    .estilocabecerasemana{
        padding-right: 4px !important ;
        padding-left: 4px !important ;

    }
    .pvertical{
        writing-mode: vertical-rl;
        transform: rotate(180deg);
        text-align: center;
    }
</style>
   <div class="col-sm-12 table-responsive" style="height: 700px;width: 98%;">
                <table class="table table-hover table-bordered " id="actividades" style="width: 120%"  >
                    <thead>

                    <tr class="clsCabereraTabla">
                        <th>Id</th>
                        <th>Actividades del Proyecto</th>
                        <th class="estilocabecera" style="background: #11B15B;border-bottom: none;"><p class="pvertical">Horas Presupuestadas</p></th>
                        <th id="hideshowMP" class="estilocabecera" style="background: #f79646;border-bottom: none;"><i class="fa fa-plus"  style="color: white;display: block;font-size: 1.5em;cursor:pointer;padding-bottom: 15px" ></i><p class="pvertical">Monto Presupuestados</p></th>
                        @if(isset($cabecera_participante))
                            @foreach($cabecera_participante as $cp)
                                <th  class="cabezerapresepuestadas estilocabecera">
                                    <p class="pvertical" >{{ $cp->descripcionrol }}</p>
                                </th>
                            @endforeach
                        @endif

                        <th class="estilocabecera" style="background: #11B15B;border-bottom: none;"><p class="pvertical">Horas Consumidas</p></th>
                        <th  id="hideshowMC" class="estilocabecera" style="background: #f79646;border-bottom: none;"><i class="fa fa-plus"  style="color: white;display: block;font-size: 1.5em;cursor:pointer;padding-bottom: 15px" ></i><p class="pvertical">Monto Consumidos</p></th>
                        @if(isset($rateshorasejecutadascabezera))
                            @foreach($rateshorasejecutadascabezera as $crh)
                                <th width="5px !important" style="padding-right: 2px; padding-left: 2px;" class="cabezeraconsumidas">
                                    <p style=" writing-mode: vertical-rl;
                                    transform: rotate(180deg)" >{{ $crh->categoria }}</p>
                                </th>
                            @endforeach
                        @endif
                        @if(isset($array_FinalCabecera))
                        @foreach($array_FinalCabecera as $nm)
                            <th  class="estilocabecerasemana" style="width: 20px">
                                <p class="pvertical" style="height: 50px">{{ $nm['fecha_inicio_final'] }}</p><br>
                                <p style="text-align: center">S-{{ $nm['semana'] }}</p>
                            </th>
                        @endforeach
                        @endif

                    </tr>
                    </thead>
                    <tbody>
                    @if($arrayProyectos)

                        @foreach( $arrayProyectos[0]['actPadre'] as $actPadre)
                            <tr data-actividadPadre = "{{$actPadre['cproyectoactividadesPadre']}}" class="padreGeneral">
                                <td style="background: rgb(221,221,221);display: flex;justify-content: flex-start;">
                                    <b style="display:block;">{{ $actPadre['codigoactividad'] }}</b>
                                    <i  style="display:block;color: black;font-size: 1.5em;cursor:pointer;display: block;margin-left: 5px;" class="glyphicon glyphicon-chevron-down  pull-right btn_ocultar" data-cactividadproyectoclick="{{$actPadre['cproyectoactividadesPadre']}}" ></i>
                                </td>
                                <td style="background: rgb(221,221,221)"><b>{{ $actPadre['nombreAct'] }}</b></td>
                                <td style="text-align: center"><b>{{ $actPadre['Suma__totalHorasPresupuestadas'] }}</b></td>
                                <td class="subtotalmontoPresupuestados" style="text-align: center"><b>{{ $actPadre['Suma__totalMontoPresupuestadas'] }}</b></td>
                                {{--@if(isset($cabecera_participante))
                                    @if (count($cabecera_participante) != 0)
                                        <td class="cabezerapresepuestadas" colspan="{{ count($cabecera_participante) }}"></td>
                                    @endif
                                @endif--}}
                                @foreach($cabecera_participante as $rrr)
                                    <td  class="cabezerapresepuestadas">

                                    </td>
                                @endforeach

                                <td  style="text-align: center"><b>{{ $actPadre['Suma__totalHorasCargadas'] }}</b></td>
                                <td style="text-align: center"><b>{{ $actPadre['Suma__totalHoraConsumidasCargadas'] }}</b></td>
                               {{-- @if(isset($rateshorasejecutadascabezera))
                                    @if (count($rateshorasejecutadascabezera) != 0)
                                        <td class="cabezeraconsumidas" colspan="{{ count($rateshorasejecutadascabezera) }}"></td>
                                    @endif
                                @endif--}}
                                @foreach($rateshorasejecutadascabezera as $abd)
                                    <td  class="cabezeraconsumidas">

                                    </td>
                                @endforeach
                                @if(isset( $actPadre['porcentaje_por_montoPre__semana']))
                                    @foreach( $actPadre['porcentaje_por_montoPre__semana'] as $perfoPadre)
                                        <td class="porcentajeSemana{{ $perfoPadre['semana'] }}_{{ $perfoPadre['anio'] }}" style="text-align: center"> <span id="pp_{{ $actPadre['cproyectoactividadesPadre'] }}_{{ $perfoPadre['semana'] }}_{{ $perfoPadre['anio'] }}">{{ $perfoPadre['porcentaje_por_montoPre']}}</span>%</td>
                                    @endforeach
                                @endif


                            </tr>
                            @foreach($actPadre['actHijo'] as $actHijo)
                                <tr data-cproyectoactividades="{{$actHijo['cproyectoactividades']}}" data-cproyecto="{{$actHijo['cproyecto']}}" data-cproyectoactividades_parent="{{$actHijo['cproyectoactividades_parent']}}" class="hideactividadeshijas">
                                    <td style="padding-left: 15px">{{ $actHijo['codigoactividad'] }}</td>
                                    <td >{{ $actHijo['nombreAct'] }}</td>
                                    <td style="text-align: center">{{ $actHijo['horapresupuestadasperformance'] }}</td>
                                    <td style="text-align: center">{{ $actHijo['monto_Presupuestados'] }}</td>
                                    @if(isset($cabecera_participante))
                                        @foreach($actHijo['array_Presupuestados'] as $ap)
                                            <td class="cabezerapresepuestadas">
                                                {{ $ap['suma'] }}
                                            </td>
                                        @endforeach
                                    @endif
                                    <td style="text-align: center">{{ $actHijo['horaejecutadosperformance'] }}</td>
                                    <td style="text-align: center">{{ $actHijo['monto_Consumidos'] }}</td>
                                    @if(isset($rateshorasejecutadascabezera))
                                        @foreach($actHijo['array_Consumidos'] as $ac)
                                            <td class="cabezeraconsumidas">
                                                {{ $ac['rateporCategoria'] }}
                                            </td>
                                        @endforeach
                                    @endif
                                    @if(isset($actHijo['array_performance']))
                                        @foreach($actHijo['array_performance'] as $per)
                                            @if($actHijo['actNieto'] != null)
                                                <td><b>-</b></td>
                                               <?php continue ?>
                                            @endif
                                            <td style="text-align: center" data-semana="{{$per['semana']}}" data-anio="{{$per['anio']}}" data-cactividadpadre="{{$per['pk_actividadPadre']}}"  data-ratehijo="{{ $actHijo['monto_Presupuestados'] }}">
                                                <input type="text"  value="{{ $per['porcentaje']}}" class="porcentaje calcularPerformance porcentaje_{{$per['pk_actividadPadre']}}_{{$per['semana']}}_{{$per['anio']}} resumenProyecto resu_{{$per['semana']}}_{{$per['anio']}}" data-semanaanio="{{$per['semana']}}_{{$per['anio']}}" style="width: 30px;text-align: center;"
                                                       @if ($lider_or_gerente == 'gerente')
                                                            disabled
                                                       @elseif($lider_or_gerente == 'lider')
                                                            enabled
                                                        @endif
                                                />
                                            </td>
                                        @endforeach

                                    @endif

                                </tr>
                                @foreach($actHijo['actNieto'] as $actNieto)
                                    <tr data-cproyectoactividades="{{$actNieto['cproyectoactividades']}}" data-cproyecto="{{$actNieto['cproyecto']}}" data-cproyectoactividades_parent="{{$actNieto['cproyectoactividades_parent']}}" class="hideactividadeshijas">
                                        <td style="padding-left: 20px">{{ $actNieto['codigoactividad'] }}</td>
                                        <td >{{ $actNieto['nombreAct'] }}</td>
                                        <td style="text-align: center">{{ $actNieto['horapresupuestadasperformance'] }}</td>
                                        <td style="text-align: center">{{ $actNieto['monto_Presupuestados'] }}</td>
                                        @if(isset($cabecera_participante))
                                            @foreach($actNieto['array_Presupuestados'] as $apn)
                                                <td class="cabezerapresepuestadas">
                                                    {{ $apn['suma'] }}
                                                </td>
                                            @endforeach
                                        @endif
                                        <td style="text-align: center">{{ $actNieto['horaejecutadosperformance'] }}</td>
                                        <td style="text-align: center">{{ $actNieto['monto_Consumidos'] }}</td>
                                        @if(isset($rateshorasejecutadascabezera))
                                            @foreach($actNieto['array_Consumidos'] as $ac)
                                                <td class="cabezeraconsumidas">
                                                    {{ $ac['rateporCategoria'] }}
                                                </td>
                                            @endforeach
                                        @endif
                                        @if(isset($actNieto['array_performance']))
                                            @foreach($actNieto['array_performance'] as $pernieto)
                                                @if($actNieto['actBisnieto'] != null)
                                                    <td><b>-</b></td>
                                                    <?php continue ?>
                                                @endif
                                                <td style="text-align: center" data-semana="{{$pernieto['semana']}}" data-anio="{{$pernieto['anio']}}" data-cactividadpadre="{{$pernieto['pk_actividadPadre']}}" data-ratehijo="{{ $actNieto['monto_Presupuestados'] }}">
                                                    <input type="text"  value="{{ $pernieto['porcentaje']}}" class="porcentaje calcularPerformance porcentaje_{{$pernieto['pk_actividadPadre']}}_{{$pernieto['semana']}}_{{$pernieto['anio']}} resumenProyecto resu_{{$pernieto['semana']}}_{{$pernieto['anio']}}" data-semanaanio="{{$pernieto['semana']}}_{{$pernieto['anio']}}"  style="width: 30px;text-align: center;"
                                                           @if ($lider_or_gerente == 'gerente')
                                                           disabled
                                                           @elseif($lider_or_gerente == 'lider')
                                                           enabled
                                                            @endif
                                                    />
                                                </td>
                                            @endforeach

                                        @endif

                                    </tr>
                                    @foreach($actNieto['actBisnieto'] as $actBisnieto)
                                        <tr data-cproyectoactividades="{{$actBisnieto['cproyectoactividades']}}" data-cproyecto="{{$actBisnieto['cproyecto']}}" data-cproyectoactividades_parent="{{$actBisnieto['cproyectoactividades_parent']}}" class="hideactividadeshijas">
                                            <td style="padding-left: 30px">{{ $actBisnieto['codigoactividad'] }}</td>
                                            <td>{{ $actBisnieto['nombreAct'] }}</td>
                                            <td style="text-align: center">{{ $actBisnieto['horapresupuestadasperformance'] }}</td>
                                            <td style="text-align: center">{{ $actBisnieto['monto_Presupuestados'] }}</td>
                                            @if(isset($cabecera_participante))
                                                @foreach($actBisnieto['array_Presupuestados'] as $apb)
                                                    <td class="cabezerapresepuestadas">
                                                        {{ $apb['suma'] }}
                                                    </td>
                                                @endforeach
                                            @endif
                                            <td style="text-align: center">{{ $actBisnieto['horaejecutadosperformance'] }}</td>
                                            <td style="text-align: center">{{ $actBisnieto['monto_Consumidos'] }}</td>
                                            @if(isset($rateshorasejecutadascabezera))
                                                @foreach($actBisnieto['array_Consumidos'] as $ac)
                                                    <td class="cabezeraconsumidas">
                                                        {{ $ac['rateporCategoria'] }}
                                                    </td>
                                                @endforeach
                                            @endif
                                            @if(isset($actBisnieto['array_performance']))
                                                @foreach($actBisnieto['array_performance'] as $perbis)
                                                    <td style="text-align: center" data-semana="{{$perbis['semana']}}" data-anio="{{$perbis['anio']}}" data-cactividadpadre="{{$perbis['pk_actividadPadre']}}" data-ratehijo="{{ $actBisnieto['monto_Presupuestados'] }}">
                                                        <input type="text"  value="{{ $perbis['porcentaje']}}" class="porcentaje calcularPerformance porcentaje_{{$perbis['pk_actividadPadre']}}_{{$perbis['semana']}}_{{$perbis['anio']}} resumenProyecto resu_{{$perbis['semana']}}_{{$perbis['anio']}}" data-semanaanio="{{$perbis['semana']}}_{{$perbis['anio']}}" style="width: 30px;text-align: center;"
                                                               @if ($lider_or_gerente == 'gerente')
                                                               disabled
                                                               @elseif($lider_or_gerente == 'lider')
                                                               enabled
                                                                @endif
                                                        />
                                                    </td>
                                                @endforeach

                                            @endif

                                        </tr>

                                    @endforeach
                                @endforeach
                            @endforeach
                        @endforeach

                    @else
                    @endif

                    </tbody>
                    <tfoot>
                        <tr>
                            @if($arrayProyectos)
                            <td><b>Código : {{ $arrayProyectos[0]['codigo']  }}</b></td>
                            <td><b>Proyecto : {{ $arrayProyectos[0]['nombre']  }}</b></td>
                                <td style="text-align: center"><b>{{ $arrayProyectos[0]['sumahorasPresupuestadasNivelProyecto']  }}</b></td>
                                <td style="text-align: center"><b id="montoPresProyGeneral">{{ $arrayProyectos[0]['sumaMontoPresupuestadasNivelProyecto'] }}</b></td>
                                @if(isset($cabecera_participante))
                                    @foreach($cabecera_participante as $cp)
                                        <td class="cabezerapresepuestadas"></td>
                                    @endforeach
                                @endif
                                <td><b>{{ $arrayProyectos[0]['sumahorasConsumidosNivelProyecto'] }}</b></td>
                                <td><b>{{ $arrayProyectos[0]['sumaMontoConsumidosNivelProyecto'] }} </b></td>
                                @if(isset($rateshorasejecutadascabezera))
                                    @foreach($rateshorasejecutadascabezera as $ac)
                                        <td class="cabezeraconsumidas">
                                        </td>
                                    @endforeach
                                @endif
                                @foreach($arrayProyectos[0]['resumenTotal'] as $art)
                                    <td style="text-align: center"><span id="proyFinal_{{$art['semana']}}_{{$art['anio']}}">{{ $art['resumenFinal'] }}</span>%</td>
                                @endforeach
                            @endif
                        </tr>

                    </tfoot>
                </table>
                <table id="header-fixed"></table>
            </div>

<style>
    .cabezerapresepuestadas,.cabezeraconsumidas,.hideactividadeshijas
    {
        display: none;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>
<script>
    $( "#hideshowMP" ).click(function() {
        $(".cabezerapresepuestadas").toggle('500');
        $("i", this).toggleClass("fa-plus fa-minus");
    });

    $( "#hideshowMC" ).click(function() {
        $(".cabezeraconsumidas").toggle('500');
        $("i", this).toggleClass("fa-plus fa-minus");
    });
    $('.btn_ocultar').click(function(){
        var codigoPadre = $(this).data("cactividadproyectoclick");
            $('tr[data-cproyectoactividades_parent="' + codigoPadre + '"]').toggle();
            console.log( $("i", this));
            $(this).toggleClass("glyphicon-chevron-down glyphicon-chevron-up");
        //});
    });
    //Grabar Porcentaje
    $(".porcentaje").focusout(function() {
        grabarPorcentaje($(this));
    });
    function grabarPorcentaje(obj){

        var objPorcentaje={cproyectoactividades: "", semana: "", periodo: "", porcentaje: "",cproyecto: ""};

        objPorcentaje.cproyectoactividades=$(obj).parent().parent().data("cproyectoactividades");
        objPorcentaje.semana=$(obj).parent().data("semana");
        objPorcentaje.periodo=$(obj).parent().data("anio");
        objPorcentaje.porcentaje=$(obj).val();
        objPorcentaje.cproyecto=$(obj).parent().parent().data("cproyecto");
        objPorcentaje._token = $('input[name="_token"]').first().val();

        //console.log(objPorcentaje.cproyectoactividades,objPorcentaje.semana, objPorcentaje.periodo,objPorcentaje.porcentaje, objPorcentaje.cproyecto);

        $.ajax({
            url: 'guardar_porcentaje',
            type:'POST',
            data: objPorcentaje,

            success : function (data){

                //verPerformance();

                //console.log(data);

            },
        });
    }

    $(".calcularPerformance").keyup(function() {

        if($(this).val()==''){
            $(this).val()==0
        }

        padre=$(this).parent().data("cactividadpadre");
        semana=$(this).parent().data("semana");
        anio=$(this).parent().data("anio");

        padreGeneral=$(this).parent().parent().data("cproyectoactividades_parent");

        // console.log(padre+'_'+semana+'_'+''+anio);
        caluclarValuePerformance(padre,semana,anio,padreGeneral);
    });

    function caluclarValuePerformance(padre,semana,anio,padreGeneral){

        var ratepadre = parseFloat($('#pp_'+padre+'_'+semana+'_'+anio).parent().siblings('.subtotalmontoPresupuestados').text());

        //var parent = $(this).parent().parent().data("cproyectoactividades_parent");
        //var montoPadrePresupuestados = $('tr[data-actividadpadre="'+padreGeneral+'"]').find('.subtotalmontoPresupuestados').find('b').html();

        //console.log(parent,montoPadrePresupuestados);
        //console.log(montoPadrePresupuestados,'parent');


        var acumulativo_porcentaje_ratehijo = 0;

        $('.porcentaje_'+padre+'_'+semana+'_'+anio).each(
            function(){

                var valorhijo= parseFloat($(this).val());
                //ratehijo= $(this).parent().prev().prev().data("ratehijo");
                var ratehijo = parseFloat($(this).parent().data("ratehijo"));

                //console.log(valorhijo,ratehijo);
                acumulativo_porcentaje_ratehijo= acumulativo_porcentaje_ratehijo+(valorhijo*ratehijo);
            });

        var nuevoPadre= acumulativo_porcentaje_ratehijo/ratepadre;
        nuevoPadre_redondeado =  nuevoPadre.toFixed(2);
        if(nuevoPadre_redondeado == 'NaN')
        {
            nuevoPadre_redondeado = 0
        }

        $('#pp_'+padre+'_'+semana+'_'+anio).html(nuevoPadre_redondeado);

        //console.log(nuevoPadre_redondeado,montoPadrePresupuestados);
        var acumulativo_subtotalPadrePerformanceSemana = 0;
        $('.padreGeneral').each(function () {

            porcentaje=$(this).find('.porcentajeSemana'+semana+'_'+anio).find('span').html();
            montoPadrePresupuestadosParents=$(this).find('.subtotalmontoPresupuestados').find('b').html();

             subtotalPadrePerformanceSemana = porcentaje * montoPadrePresupuestadosParents
            acumulativo_subtotalPadrePerformanceSemana += subtotalPadrePerformanceSemana

           // console.log('span',montoPadrePresupuestadosParents,porcentaje)
        })

        montoPresProyGeneral =  parseFloat(acumulativo_subtotalPadrePerformanceSemana) / parseFloat($('#montoPresProyGeneral').html())
        montoPresProyGeneral = montoPresProyGeneral.toFixed(2)

        $('#proyFinal_'+semana+'_'+anio).html(montoPresProyGeneral);


    }
    $("#actividades").tableHeadFixer(
        {
            //'foot' : true,
            'left' : 2
        },

    );
    /*
    $(".resumenProyecto").keyup(function() {

        semana_anio=$(this).data("semanaanio");

        caluclarPerformanceFinalProyecto(semana_anio);
    });
    function caluclarPerformanceFinalProyecto(semana_anio){

        //var ratepadre = parseFloat($('#pp_'+padre+'_'+semana+'_'+anio).parent().siblings('.subtotalmontoPresupuestados').text());


       // var acumulativo_porcentaje_ratehijo = 0;

        $('.resu_'+semana_anio).each(
            function(){

                var valorhijo= parseFloat($(this).val());
                //ratehijo= $(this).parent().prev().prev().data("ratehijo");
                var parent = parseFloat($(this).parent().parent().data("cproyectoactividades_parent"));
                var montoPadrePresupuestados = $('tr[data-actividadpadre="'+parent+'"]').find('.subtotalmontoPresupuestados').find('b').html();

                //console.log(valorhijo,montoPadrePresupuestados);

                //console.log(valorhijo,ratehijo);
                //acumulativo_porcentaje_ratehijo= acumulativo_porcentaje_ratehijo+(valorhijo*ratehijo);
            });




    }*/


</script>

