@extends('layouts.master')
@section('content')

<div class="content-wrapper">


    <div class="col-sm-12">
      <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ANDDES -
        <small>Performance de los proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>
    <hr>

  <div class="col-md-3">

    <select id="proyectos" class="form-control select-box" data-placeholder="Elegir un proyecto..."> 
        @foreach($tproyectos as $proy)
          <option value="{{ $proy->cproyecto }}" >{{ $proy->codigo }} {{ $proy->nombre }}</option>
        @endforeach
    </select>
  </div>

  <div class="col-md-3">
       <select id="cbo_areas" class="form-control" name="cbo_areas" data-placeholder="Elegir un area..."> 
            <option></option>
      </select>
  </div>

  <div class="col-md-2">
       <div class="col-lg-2 col-xs-2 clsPadding">Fecha inicio</div>            
          <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                    {!! Form::text('fdesde','', array('class'=>'form-control pull-right ','id' => 'fdesde')) !!}
        </div>
  </div>


  <div class="col-md-2">
    <div class="col-lg-2 col-xs-2 clsPadding">Fecha Final</div>            
          <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                    {!! Form::text('fdesde','', array('class'=>'form-control pull-right ','id' => 'fdesde')) !!}
        </div>
  </div>

  <div class="col-md-2">
    <button id="buscarActividades" class="btn btn-block btn-primary">Generar</button>
  </div> 
  <div class="" style="margin-top: 9em"></div>    
  </div>
  
   @include('proyecto.performance.listaactivity')

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

    /*==============================
    =            Chosen            =
    ==============================*/
        $('.select-box').chosen(
        {
            allow_single_deselect: true,width:"100%"
        });
    
    /*=====  End of Chosen  ======*/
    

/*====================================================================
=            Combo por area enviando el proyecto indicado            =
====================================================================*/

  $('#proyectos').change(function(){
   //alert(this.value);
   
  $.ajax({
        type:'GET',
        url: 'getAreas/'+this.value,

        success: function (data) {

        //  $('#tablaactiv').html(data)
         
          $('#cbo_areas').find('option').remove();

          for (var i = 0; i < data.length; i++) {
            $('#cbo_areas').append($('<option></option>').attr('value',data[i].carea).text(data[i].area));

          }
    },
    error: function(data){

    

    }
  });

});

/*=====  End of Combo por area enviando el proyecto indicado  ======*/



    });


</script>




@stop 