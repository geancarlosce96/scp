<div class="col-sm-12 table-responsive " style="height: 700px;width: 98%;">
    <table class="table table-hover table-bordered " id="actividades" style="width: 120%" >
        <thead>
            
            <tr class="clsCabereraTabla">
                  <!--====  Cabecera actividades  ====-->
                <th width="20%">
                    Actividades del Proyecto
                </th>
                  <!--====  Control de Horas  HH ====-->

                <th style="text-align: center;width: 2.5%">
                    <div>Horas</div> 
                    presupuestadas
                </th>
                <!--==== Cabecera Horas por rate  ====-->
                <th style="text-align:center; "  width="2.5%" id="button">

                    <i class="fa fa-plus"   style="color: white;display: block;font-size: 1.5em;cursor:pointer;">
                    </i>
                       Monto presupuestados
                </th>

                 <!--==== Cabecera Lista de Roles  ====-->
                @if(isset($cabecera_participante))
                    @foreach($cabecera_participante as $cp)
                    <th class="cabezera" style="width: 4%;text-align: center;">
                        {{ $cp->descripcionrol }}
                    </th>
                    @endforeach
                  
                   <!--==== Cabecerera Lista de semanas  ====-->

                  
                 <!--==== Categoria de proyectos en ejecucion    ====-->

                 
                 <th width="2%" style="text-align: center;" id="cabecera_actividades_button">

                <i class="fa fa-plus"  style="color: white;display: block;font-size: 1.5em;cursor:pointer;">
                    </i>
                 Horas Consumidas
               </th>

                  <!--==== Cabecera Numero de horas  ====-->
                @if(isset($rateshorasejecutadascabezera))
                 @foreach($rateshorasejecutadascabezera as $rhe)
                    <th class="cabecera_actividades" style="width: .5%;text-align: center;">
                        {{ $rhe->categoria }}
                    </th>
                    @endforeach
                @endif 

                  <!--====  Control de Horas  HH ====-->
                  <th style="text-align:center; " width="2%">
                     <div >Monto</div> 
                      Consumidos
                </th>
               
                  
                    @foreach($num_sema as $nm)
                    
                      <th style="width: .5%;text-align: center;">
                          S - {{ $nm['semana'] }}
                      </th>
                  @endforeach
                @endif
            </tr>
        </thead>
        <tbody>
            @if(isset($arrayProyectos))

              @foreach($arrayProyectos as $lp)
                <tr data-cproyecto="{{$lp['cproyecto']}}">
                    <td width="20%">
                        <b>
                            Proyecto : {{$lp['nombre']}}
                        </b>
                    </td>
                    <td></td>
                    <td></td>
                     @if (count($rateshorasejecutadascabezera) == 0) 

                              @else
                                  <td colspan="{{ count($rateshorasejecutadascabezera) }}" class="cabecera_actividades"></td>
                              @endif
                    <td>
                       {{--  <b>
                            {{$lp['sum_act_project']}}
                        </b> --}}
                    </td>
                    <td>
                       {{--  <b>
                            {{$lp['sum_act_total']}}
                        </b> --}}
                    </td>
                </tr>
                @if(isset($lp['act']))
                  @foreach($lp['act'] as $act)
                  
                      <tr cactividadproyecto="{{$act['cproyectoactividades']}}"  data-padre="{{$act['cproyectoactividades_parent']}}" data-cactividadproyecto="{{$act['cproyectoactividades']}}" data-cproyecto="{{$lp['cproyecto']}}" >
                          @if($act['father_or_son'] == 'Padre')
                           <td style="background: #ddd" width="20%"><i class="fa fa-caret-square-o-down btn_ocultar " style="padding-right: 3px" data-cactividadproyectoclick="{{$act['cproyectoactividades']}}" ></i>{{$act['codigoactividad']}} - {{$act['nombreAct']}}</td>
                          
                            <td  style="text-align: center;">{{$act['horas_presuestadas_padre'] }}</td>  
                            <td class="ratepadre" style="text-align: center;">{{$act['sum_hijos_por_padre'] }}</td> 
                         
                             @if (count($rateshorasejecutadascabezera) == 0) 

                              @else
                                  <td colspan="{{ count($rateshorasejecutadascabezera) }} " class="cabecera_actividades" ></td>
                            @endif

                              @if (count($cabecera_participante) == 0) 

                              @else
                                  <td colspan="{{ count($cabecera_participante) }} " class="cabezera"></td>
                              @endif
                             <td style="text-align: center;">{{$act['sumaacumulativa_activity_parent']}}</td>
                             <td style="text-align: center; width: 2%">{{$act['suma_rate_total_acumulativa_activity_parent']}}</td>

                              @foreach($act['performance_final'] as $performance_final)
                                <td >
                                  <span id="pp_{{ $performance_final['pk_actividadpadre'] }}_{{ $performance_final['semana'] }}"> {{ $performance_final['performance'] }} %</span>
                                </td>
                              @endforeach
                              {{-- <td>12</td>    --}}

                          @else

                          <td width="20%">{{$act['codigoactividad']}} - {{$act['nombreAct']}}</td>

                          @endif 
           
                          @if($act['father_or_son'] == 'Hijo')

                            <td style="text-align: center;width: 2.5%">{{$act['sum_act']}}</td>
                            <td style="text-align: center;width: 2.5%">{{$act['sum_act_total']}}</td>
                            @if(isset($act['detalle_suma_activity']))
                                @foreach($act['detalle_suma_activity'] as $act_detail)
                                  <td class="cabezera" style="text-align: center;width: 4%">{{$act_detail['suma']}}</td>
                              @endforeach
                            @endif
                           

                             <td style="text-align: center;">{{$act['horas_ejecutadas']}}</td>

                             {{-- @if (count($rateshorasejecutadascabezera) == 0) 

                              @else
                                  <td colspan="{{ count($rateshorasejecutadascabezera) }} " class="cabecera_actividades"></td>
                              @endif --}}

                               @if(isset($rateshorasejecutadascabezera))
                                 @foreach($act['array_suma_rate_detalle_activity_acumulativo_hijo_x_parent'] as $key=>$ch)
                                  <td  style="text-align: center; width: .8%" class="cabecera_actividades" >{{ $ch['subtotal_rate_hora_categoria']}}</td>
                                  {{-- <td  class="cabecera_actividades"></td> --}}
                                @endforeach 
                               @endif

                           {{--  <td style="text-align: center;width: 2.5%">{{$act['sum_act']}}</td>
                            <td style="text-align: center;width: 2.5%">{{$act['sum_act_total']}}</td> --}}


                            <td style="text-align: center;">{{$act['suma_rate_detalle_activity_acumulativo']}}</td>

                            @if(isset($act['detalle_suma_activity']))
                              
                              @foreach($act['detalle_porcentaje_activity'] as $act_detail_porcent)

                                <td data-actividadparent="{{ $act_detail_porcent['pk_parent'] }}" data-actividad="{{ $act_detail_porcent['pk_actividad'] }}" data-semana="{{$act_detail_porcent['semana']}}" data-anio="{{$act_detail_porcent['anio']}}" data-porcentaje="{{$act_detail_porcent['porcentaje']}}"  data-ratehijo="{{ $act['sum_act_total'] }}" {{--data-ratepadre="{{ $act['sum_hijos_por_padre'] }}" --}}>
                                 {{--  {{($$array_porcentaje[''])}} --}}
                                    <input  name="porcentaje"  size="3" type="text" data-toggle="tooltip" class="porcentaje porcentaje_{{ $act_detail_porcent['pk_parent'] }}_{{$act_detail_porcent['semana']}} ac{{ $act_detail_porcent['pk_actividad'] }} contar" id="porcen_{{$act_detail_porcent['semana']}}_{{$act_detail_porcent['anio']}}_{{$act['cproyectoactividades']}}" value="{{ round($act_detail_porcent['porcentaje'],2)}}"

                                    @if ($act_detail_porcent['habilitado']  ==  false && $lider_or_gerente == 'gerente')   
                                            disabled
                                    @elseif($lider_or_gerente == 'gerente')
                                            enabled
                                    @elseif($act_detail_porcent['habilitado'] ==  false && $lider_or_gerente == 'lider')
                                            enabled
                                    @else
                                             
                                    @endif 

                                    />
                                 
                                </td>
                              @endforeach                                    
                            @endif
                            @else
                            {{-- <td>12</td>  --}}            
                          @endif
                      </tr>
                  @endforeach
                @endif
              @endforeach
            @endif
        </tbody>
        <tfoot>
            <tr>
            </tr>
        </tfoot>
    </table>
    <table id="header-fixed"></table>
</div>
<style>
  .cabezera, .cabecera_actividades
  {
    display: none;
  }

    .validar{
    border: 2px solid tomato !important;
  }


</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 <script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>

<script>

    $( "#button" ).click(function() {
    $(".cabezera").toggle('1000');
      
      $("i", this).toggleClass("fa-plus fa-minus");

    });

    $( "#cabecera_actividades_button" ).click(function() {
      $(".cabecera_actividades").toggle('1000');

      $("i", this).toggleClass("fa-plus fa-minus");
    });

    $('.btn_ocultar').click(function(){

       $('[data-cactividadproyectoclick="'+  $(this).data("cactividadproyectoclick") +'"]').click(function() {
          var contenidopk = $(this).parent().parent().data("cactividadproyecto"); 
          $('tr[data-padre="' + contenidopk + '"]').toggle();
      });
    });


        $(".porcentaje").focusout(function() {

          grabarPorcentaje($(this));


          semana_act_val = $(this).parent().find('input').val();
          semana_ant_val = $(this).parent().prev().find('input').val();
          //semana_ant_integer = semana_ant.toFixed(2);

         // console.log(semana_act_val,semana_ant_val);

          if(semana_act_val > semana_ant_val)
          {
             var inputmayor=$(this).parent().find('input');
            //inputmayor.css({'border': '1px inset','padding': '1px 0'});
            inputmayor.removeClass("validar");
            inputmayor.removeAttr('title','Recuerda que debes ingresar un porcentaje mayor a la semana anterior !')
          }

          else
          {
            var inputmayor=$(this).parent().find('input');
            inputmayor.addClass("validar");
            inputmayor.attr('title','Recuerda que debes ingresar un porcentaje mayor a la semana anterior !')
            // inputmayor.css({'border': '2px solid tomato'});
          }

    
        });

/*         $(".porcentaje").keyup(function() {

          pk_actividad = $(this).parent().data("actividad");
          semana = $(this).parent().data("semana");

       
          //ValidarValueInput(pk_actividad, semana);
        });
*/
       



        function grabarPorcentaje(obj){

          var objPorcentaje={cproyectoactividades: "", semana: "", periodo: "", porcentaje: "",cproyecto: ""};

          objPorcentaje.cproyectoactividades=$(obj).parent().parent().data("cactividadproyecto");
          objPorcentaje.semana=$(obj).parent().data("semana");
          objPorcentaje.periodo=$(obj).parent().data("anio");
          //objPorcentaje.porcentaje= $('#porcen_'+$(obj).parent().data("semana")+'_'+$(obj).parent().data("anio")+'_'+$(obj).parent().parent().data("cactividadproyecto")).val();
          objPorcentaje.porcentaje=$(obj).val();
          objPorcentaje.cproyecto=$(obj).parent().parent().data("cproyecto");
          objPorcentaje._token = $('input[name="_token"]').first().val();

          //console.log(objPorcentaje.semana,objPorcentaje.periodo,objPorcentaje.porcentaje,$('#porcen_'+$(obj).parent().data("semana")+'_'+$(obj).parent().data("anio")+'_'+$(obj).parent().parent().data("cactividadproyecto")).val());
         // console.log(objPorcentaje,$(obj).val());

          $.ajax({
            url: 'guardar_porcentaje',
            type:'POST',
            data: objPorcentaje,

            success : function (data){

                //verPerformance();

             console.log(data);

            },
        }); 
        }






        $(".contar").keyup(function() {

          padre=$(this).parent().data("actividadparent");
          semana=$(this).parent().data("semana")

          caluclarSumaPerformance(padre,semana);

         // console.log(padre+'_'+semana);

        });


        function caluclarSumaPerformance(padre,semana){

          var valorpadre = parseFloat($('#pp_'+padre+'_'+semana).text());
          var ratepadre = parseFloat($('#pp_'+padre+'_'+semana).parent().siblings('.ratepadre').text());
        
        // console.log(ratepadre);
          

            var acumulativo_porcentaje_ratehijo = 0;

           $('.porcentaje_'+padre+'_'+semana).each(
           function(){

              var valorhijo= parseFloat($(this).val());
               //ratehijo= $(this).parent().prev().prev().data("ratehijo");
              var ratehijo = parseFloat($(this).parent().data("ratehijo"));

              /*var multip_porcentaje_ratehijo = valorhijo.map(function(x, index) {
              var curMult = x * ratehijo[index];

              acumulativo_porcentaje_ratehijo += curMult;

              return curMult;

              });*/

              //console.log(valorhijo,ratehijo);
              acumulativo_porcentaje_ratehijo= acumulativo_porcentaje_ratehijo+(valorhijo*ratehijo);



           });

            var nuevoPadre= acumulativo_porcentaje_ratehijo/ratepadre;
            nuevoPadre_redondeado =  nuevoPadre.toFixed(2);
            //console.log(nuevoPadre_redondeado,acumulativo_porcentaje_ratehijo,ratepadre,valorpadre);


           $('#pp_'+padre+'_'+semana).html(nuevoPadre_redondeado);

    
          
        }

        $("#actividades").tableHeadFixer(
          {'left' : 1},
          
);

   
</script>
