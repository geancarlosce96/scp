<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto -
        <small>Planificación de tareas administrativas</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => 'grabarPlanificacionAreaAdm','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('semana_act',$semana_act,array('id'=>'semana_act')) !!}
        <div class="row">
            <div class="col-md-12 col-xs-12">
                <div class="row"> 
                    <div class="col-md-3 col-xs-6">
                        <label class="clsTxtNormal">Fecha (Semana):</label>            
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            {!! Form::text('fdesde','', array('class'=>'form-control pull-right','id' => 'fdesde')) !!}
                        </div>
                    </div>

                    <div class="row">   
                    <div class="col-md-4 col-xs-6" style="margin-top:25px;">
                        <button type="button" class="btn btn-primary btn-block " id="btnView"> <b>Visualizar</b></button>  


                    </div>

                     <div class="col-md-4 col-xs-6" style="margin-top:25px;">

                         <button type="submit" class="btn btn-primary btn-block "><b>Grabar</b></button>       
                    
                    </div>  
                     </div>


                    <div class="col-md-2 col-xs-6" style="margin-top:25px;">
                        
                    </div>   
                    <div class="col-md-2 col-xs-6" style="margin-top:25px;">
                            
                    </div>              
                    <div class="col-md-2 col-xs-6" style="margin-top:25px;">
                        
                    </div>
                </div> 

                <div class="row">	
                    <div class="col-lg-5 col-xs-5 table-responsive" id="tablePlani">
                
                        
                    </div>
                    <div class="col-lg-7 col-xs-7 table-responsive" id="divResultado">
                
                        
                    </div>                    
                </div>  
        
                        

                <div class="col-md-12">
                    <div class="col-lg-12 col-xs-12 table-responsive" id="participantes">
                        
                    @include('partials.planificacionPorAreasParticipantes',array('planiAcu'=>(isset($planiAcu)==1?$planiAcu:array()) ))

                    </div>
                
                    <div class="row">
                        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                    </div>
                    <div class="row clsPadding2"> 
                    
                     <div class="col-lg-3 col-xs-3">

                         <button type="submit" class="btn btn-primary btn-block "><b>Grabar</b></button>       
                    
                    </div>  
                    


                    
                </div>
                </div>
            </div>
        </div>
        {!! Form::close() !!}
    </section>

@include('partials.reporteHorasParticipante')
    <!-- *************************************************** -->
      <div class="modal fade" id="modalObs" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="ObsModalLabel" style="color: white">Comentarios</h4>
            </div>
            <div class="modal-body">
              <form>
                <div class="form-group">
                  <label for="message-text" class="control-label">Ingrese comentario:</label>
                  <textarea class="form-control" id="message-text"></textarea>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="saveObs()">Aceptar</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>
    <!-- *************************************************** -->      
    <!-- /.content -->
</div>

<script>

        var selected_actividad ='';
        $("#btnView").click(function (e){

            e.preventDefault();
            if ( $("#fdesde").val()==''){
                alert('Especifique Fecha de la Planificación de tareas Administrativa');
                return;
            }
            
            $.ajax({
                url: 'viewPlanificacionAreaAdm',
                type: 'GET',
                data:  "fdesde=" + $("#fdesde").val(),
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#tablePlani").html(data);
                    $("#divResultado").html('');
                    $('#div_carga').hide(); 
                    verAcumuladas();
                },
            });
        });




        $('#fdesde').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true
        });       
      
        $("#frmproyecto").on('submit',function(e){
           /* if(validarSemana()){
                alert('Solo se puede planificar semanas posteriores.');
                return false;
            }*/
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarPlanificacionAreaAadm',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#div_msg").show();
                     viewSemana(selected_actividad);

                     
                },
                error: function(data){
                    
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });    
        $('.select-box').chosen(
            {
                allow_single_deselect: true
        });                 



        function viewObs(fila,key){
          
          obj = $("#obs_"+fila+"_"+key);
          document.getElementById('message-text').value=obj.val();
          i_col=key;
          i_fila=fila;
          $('#modalObs').modal('show');
          
        }
        function saveObs(){
            obj =  $("#obs_"+i_fila+"_"+i_col);
            obj.val(document.getElementById('message-text').value);
            $('#modalObs').modal('hide');
        }  
        function viewSobre(id,obj){
          cad = id;
          if($(obj).val()==''){
            $("#"+cad).css("display","none");
          }else{
            $("#"+cad).css("display","block");
          }

        }            
        function viewSemana(cactividad){
            selected_actividad = cactividad;
            //alert(cactividad);
            $.ajax({

                type:"POST",
                url:'viewSemanaAdm',
                data : {
                  "_token": "{{ csrf_token() }}",
                  cactividad: cactividad,
                  fdesde: $("#fdesde").val()
                },
                beforeSend: function () {
                    $('#div_carga').show(); 
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     $("#divResultado").html(data);
                     if($("#fdesde").val()!=''){
                        verAcumuladas();
                     }
                },
                error: function(data){
                    $('#div_carga').hide();
                }
            });  

        }  
        function habilitar(i ,cpersona ,key,obj){
            if(obj.checked){
                $('#val_'+cpersona+'_'+key).attr("readOnly",false);
            }else{
                $('#val_'+cpersona+'_'+key).attr("readOnly",true);
            }
        }

        function asignarHoras(obj,fec){
            $("input[type=checkbox]").each(function (e){
                nombre = this.name;
                fec = fec.replace("-","");
//                alert(fec + '_' + nombre);
                if(nombre.indexOf(fec)!=-1){
                    //alert(this.checked);
                    if(this.checked){
                        var id = this.id;
                        //alert(id);
                        //alert(id.subString(4));
                        $("#val_"+ id.substring(4)).val(obj.value) ;
                    }
                    
                }
            });            

        }
        function checkAll(fec,obj){
            $("input[type=checkbox]").each(function (e){
                nombre = this.name;
                if(nombre.indexOf(fec)!=-1 && !(nombre==("chk_"+fec)) ){
                    if (obj.checked){
                        this.checked=true;
                    }
                    else{
                        this.checked=false;
                    }
                }
            });                        
        }
       function verAcumuladas(){
           
            $.ajax({
                url: 'verHorasAcumuladasParticipanteAdm',
                type: 'GET',
                data:  {
                    "fdesde": $("#fdesde").val()
                },
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#participantes").html(data);
                    $('#div_carga').hide(); 
                },
            });
        }    
        if($("#fdesde").val()!=''){
            verAcumuladas();    
        }

        function soloNumeros(e,obj) {

            // Backspace = 8, Enter = 13, ’0′ = 48, ’9′ = 57, ‘.’ = 46
            
            var field = obj;

            key = e.keyCode ? e.keyCode : e.which;
            
            if (key == 8 || key ==9 || key ==11) return true;
            if (key > 47 && key < 58) {
              if (field.value === "") return true;
              var existePto = (/[.]/).test(field.value);
              
              if (existePto === false){
                  regexp = /.[0-9]{2}$/; //PARTE ENTERA 10
              }
              else {
                regexp = /.[0-9]{2}$/; //PARTE DECIMAL2
              }
              return !(regexp.test(field.value));
            }
            if (key == 46) {
              if (field.value === "") return false;
              regexp = /^[0-9]+$/;
              return regexp.test(field.value);
            }

            return false;

        }           
        function validarSemana(){
            res = false;
            sem_ini = $("#semana_ini").val();
            sem_fin = $("#semana_fin").val();
            sem_act = $("#semana_act").val();
            
            if(sem_ini!='' && sem_fin!='' && sem_act!=''){
                if(sem_act >= sem_ini){
                    res = true;
                }
            }
            return res;
        }         
        function goEditar(cpersona,fecha){
              

        
                $.ajax({

                        url:'verReporteHorasParticipante',
                        type: 'POST',
                        data : {
                            '_token':'{{ csrf_token() }}',
                            'cpersona' : cpersona,
                            'fecha': fecha,
                        },
                        beforeSend: function(){
                            $('#div_carga').show(); 
                        },
                        success: function(data){                  
                            $('#div_carga').hide(); 
                            $("#horas").html(data);
                            $("#verHT").modal("show");
                            
                        },            
                    });
            
        }        

</script>       
