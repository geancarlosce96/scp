@extends('layouts.master')
@section('content')

<div class="content-wrapper">


    <div class="col-sm-12">
      <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        ANDDES -
        <small>Performance de los proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
      {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
    </section>
    <hr>

  <div class="col-md-3">

    <input type="hidden" value="{{$lider_or_gerente}}" id="lider_or_gerente">

    <select id="proyectos" class="form-control select-box" data-placeholder="Elegir un proyecto..."> 
        <option value="0" >Seleccione un proyecto</option>
        @foreach($tproyectos as $proy)
          <option value="{{ $proy->cproyecto }}" >{{ $proy->codigo }} {{ $proy->nombre }}</option>
        @endforeach
    </select>
  </div>
  <div class="col-md-2">
       <select id="cbo_areas" class="form-control select-areas" name="cbo_areas" data-placeholder="Elegir un area...">
            <option value=""></option>
      </select>
  </div>

  <div class="col-md-2">
       <div class="col-sm-3 clsPadding">Fecha inicio</div>            
      <div class="input-group date">
      <div class="input-group-addon">
          <i class="fa fa-calendar"></i>
      </div>
          {!! Form::text('fdesde','', array('class'=>'form-control pull-right fdesde ','id' => 'fdesde','readonly'=>'true')) !!}
      </div>
  </div>


  <div class="col-md-2">
    <div class="col-sm-3 clsPadding">Fecha FinaL</div>
          <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                    {!! Form::text('fhasta','', array('class'=>'form-control pull-right fhasta ','id' => 'fhasta','readonly'=>'true')) !!}
                </div>        
  </div>

  <div class="col-md-2">
    <button id="btngenerar" class="btn btn-block btn-primary">Generar</button>
      <input type="hidden" id="fechainicioBD">
  </div>

  <div class="col-lg-1" style="display: none;" id="imgLoad" >
     <img src="{{ asset('img/load.gif') }}" width="25px" >
</div> 
  <div class="" style="margin-top: 9em"></div>    
  </div>
  <div id="lista">
    @include('proyecto.performance.listaactividadesPerformance',array('arrayProyectos' => (isset($arrayProyectos)==1?$arrayProyectos:array())))
  </div>

</div>



<script type="text/javascript">

  $(document).ready(function(){


    /*  $('.fhasta').datepicker({
          format: "dd/mm/yyyy",
          language: "es",
          calendarWeeks: true,
          autoclose:true,

      });

      $('.fdesde').datepicker({
          format: "dd/mm/yyyy",
          language: "es",
          calendarWeeks: true,
          autoclose:true,

      });*/



    /*==============================
    =            Chosen            =
    ==============================*/
        $('.select-box').chosen(
        {
            allow_single_deselect: true,width:"100%"
        });

      $('.select-areas').chosen(
          {
              allow_single_deselect: true,width:"100%"
          });
    
    /*=====  End of Chosen  ======*/
    

/*====================================================================
=            Combo por area enviando el proyecto indicado            =
====================================================================*/

  $('#proyectos').change(function(){
      //resetear();
    /*getareas*/
      obtenerAreasFecha(this.value);
     // console.log("XDXD",$(this.value))


  });

  $('#btngenerar').click(function(){
    verPerformance();
});



/*=====  End of Listar data dentro de una grilla  ======*/

  /*=======================================
  =            Rango de Fechas            =
  =======================================*/




  /*=====  End of Rango de Fechas  ======*/



    });

  function resetear(){

      $("#fdesde").val("");
      $("#fhasta").val("");
      /*$("#fdesde").datepicker("refresh");
      $("#fhasta").datepicker("refresh");*/
     /* $("#fdesde,#fhasta").datepicker({
          format: "dd/mm/yyyy",
      }).datepicker("setDate", '');*/

  }
   function verPerformance(){

         //   console.log("CD",$("#fechainicioBD").val(),$("#fdesde").val());

       var fechai=$("#fdesde").val();
       var fechaf=$("#fhasta").val();
       var fechainiProy=$("#fechainicioBD").val();
       var fi_string=fechai.substr(3,2)+'/'+fechai.substr(0,2)+'/'+fechai.substr(6,4);
       var ff_string=fechaf.substr(3,2)+'/'+fechaf.substr(0,2)+'/'+fechaf.substr(6,4);
       var fiProy_string=fechainiProy.substr(3,2)+'/'+fechainiProy.substr(0,2)+'/'+fechainiProy.substr(6,4);
      // console.log(fiProy_string,"fiProy_string");
       var fec_des1= new Date(fi_string);
       var fec_has2= new Date(ff_string);
       var fec_has= new Date(fi_string);
       var fec_des= new Date(ff_string);

       var fec_inicio_Proyecto= new Date(fiProy_string);


       //console.log(fec_inicio_Proyecto,fec_des1,"final final" );
       if ( fec_inicio_Proyecto > fec_des1 ){
               //alert('La fecha de inicio del proyecto no empieza en la fecha seleccionada, colocar una fecha mayor ');
                swal("La fecha de inicio del proyecto no empieza en la fecha seleccionada, colocar una fecha mayor");
               return;
           }
           if ($("#fechainicioBD").val()==''){
               //swal("Hello world!");
               //alert('Debe de registrar la fecha de inicio en la hoja de resumen');
               swal("Debe de registrar la fecha de inicio en la hoja de resumen");
               return;
           }

            if ($("#fhasta").val()=='' || $("#fdesde").val()==''){
               // alert('Especifique rango de fechas');
                swal("Especifique rango de fechas");
                return;
            }
            else{

                var anio_has=fec_has.getFullYear(); 
                var anio_des=fec_des.getFullYear();

                var difM = fec_has2 - fec_des1; // diferencia en milisegundos
                var difD = ((difM / (1000 * 60 * 60 * 24)) + 1); // diferencia en dias

              console.log(fec_has,fec_des,"olaola");
                if(fec_has>fec_des){
                   // alert('Verifique que la fecha inicio sea menor a la fecha final');
                    swal("Verifique que la fecha inicio sea menor a la fecha final");
                    return;
                }

                /*if(difD>=35){
                    alert('Seleccione un rango de fechas menor a 5 semanas');
                    return;
                }*/

            }


       $("#div_carga").show();
       var obj = {cproyecto: $('#proyectos').val(), disciplina: $("#cbo_areas").val(), fdesde: $("#fdesde").val(), fhasta: $("#fhasta").val()}
             
            $.ajax({
                  type:'GET',
                  url: 'getActividades/'+ $('#proyectos').val() + '/'+  $('#cbo_areas').val() ,
                  data: obj,
                  success: function (data) {
                      console.log(data,"sss");
                      $("#div_carga").hide();
                      $('#lista').html(data);


              },
              error: function(data){

              }
            });

  }
  function  obtenerAreasFecha(valor) {
      $.ajax({
          type:'GET',
          url: 'getAreas/'+valor,

          success: function (data) {

//console.log("per",data)

              $('#cbo_areas').find('option').remove();
              //  $('#tablaactiv').html(data)
              if($("#lider_or_gerente").val() == 'gerente'){

                  $('#cbo_areas').append($('<option></option>').attr('value','Nuevo').text('Todos'));
                  $('#cbo_areas').trigger("chosen:updated");

              }

              for (var i = 0; i < data.length; i++) {
                  $('#cbo_areas').append($('<option></option>').attr('value',data[i].disciplina).text(data[i].area));
                  $('#cbo_areas').trigger("chosen:updated");

              }

              /*Manejo de FECHAS*/

              var finicio = data[0]['finicio'];
              var fcierre = data[0]['fcierre'];

              var conver_finicio = finicio ? moment(finicio).format("DD/MM/YYYY") : null;
              var conver_fcierre = fcierre ? moment(fcierre).format("DD/MM/YYYY") : null;


              $("#fdesde").datepicker({
                  format: "dd/mm/yyyy",
                  language: "es",
                  calendarWeeks: true,
                  autoclose:true

              });
              //  $('.fdesde').datepicker();
              //$('#fdesde').datepicker('setDate', 'today');


              //.datepicker("setDate", conver_fcierre);
              $("#fhasta").datepicker({
                  format: "dd/mm/yyyy",
                  language: "es",
                  calendarWeeks: true,
                  autoclose:true

              });
              //$('.fhasta').datepicker();
              // $('#fhasta').datepicker('setDate','today');

              // console.log(conver_finicio,"finicio",conver_fcierre);

              $('#fechainicioBD').val(conver_finicio);
              $('.fdesde').val(conver_finicio);
              $('.fhasta').val(conver_fcierre);

              //console.log(conver_finicio,conver_fcierre,"AHI VAMOS OTRA VEZ");




          },
          error: function(data){



          }

      });

  }


</script>




@endsection