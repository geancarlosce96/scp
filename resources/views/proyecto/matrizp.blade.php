<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Matriz de Proyectos</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/css/font-awesome.min.css">
  
  
<!-- Ionicons -->
  
  <link rel="stylesheet" href="dist/css/ionicons.min.css">  
  <!-- Theme style -->
  <link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.css">

  <style>
  .caption_table{
  	color: white;
  	background-color: black;
  }
  .nomgp{
  	background-color: rgb(62,107,149);
  	color: white;
  	font-size: 16px;
  }
  .tr_t{
  	background-color: white;
  	border-color: black;
  	border-width: 2px;
	font-size: 16px;
  }
  .caption{
	  font-size: 16px;
  }
  .nopadding {
	padding: 0 !important;
	margin: 0 !important;
	padding-left: 5px !important;
	}
.carousel-indicators{
	
    background: #404549;
    padding: 0px;
    border-radius: 16px;
	}
footer {
  background-color: black;
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 40px;
  color: white;
}

  </style>
        
        


</head>
<body class="hold-transition skin-blue sidebar-mini"  >
	<video id="my-video" controls preload="auto" width="600" height="200" style="display:none"
>
<source src="videos/RendiciondeGastos.mp4" type='video/mp4'>
	<p class="vjs-no-js">
		To view this video please enable JavaScript, and consider upgrading to a web
browser that
<a href="http://videojs.com/html5-video-support/" target="_blank">supports HTML5
	video</a>
</p>
</video>

<div class="wrapper" id="wrapper">
	<div class="page-header" style="padding-top:0px;padding-bottom:0px;margin-top:0px; margin-bottom:0px" >
		<img src="img/headerMatriz.jpg" class="img-responsive" style="width:100%" />
		
		<div class="col-sm-5" style="padding-top: 0px; color:#0069aa; font-size:25px; color:black;" id="conteo">
			<span style="color:white;"> Faltan </span>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="dia" style="font-size:50px; color:white;">  </span> <br> <b> Dias </b>
			</button>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="hora" style="font-size:50px; color:white;"> </span> <br> <b>Horas </b>
			</button>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="min" style="font-size:50px; color:white;"> </span> <br> <b>Minutos </b>
			</button>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="seg" style="font-size:50px; color:white;"> </span> <br> <b> Segundos </b>
			</button>
			<span style="color:white;"> para el Aniversario de Anddes </span>
		</div>
		
	</div>
	<div id="carouselExampleIndicators" class="carousel slide" style="padding:0px" data-ride="carousel">
		
		<div class="carousel-inner" role="listbox">
		<?php $h=1; ?>
		<?php $j=0; ?>
		<?php $pj=0; ?>
		<?php $p=0; ?>
		<?php $hojas=0; ?>
		@foreach($proyectos as $pry)
			<?php if($j==0 || $p==0){ ?>
			<div class="item <?php echo ($h==1?'active':''); ?> " style="margin:0px"  >
			<?php $hojas++; ?>
			<?php } ?>
			<?php $h++; ?>
			<?php $pj=0; ?>
			<?php $p++; ?>
				<div class="row nopadding" >
					<div class="col-md-1 nopadding">
						<center>
							<div class="thumbnail" >
								<img src="images/{{ $pry['identificacion_gp'] }}.jpg" alt="Foto" style="width:80px; height:80px">
								
								<!--{{Html::image(URL::route("images",array('dni'=>$pry['identificacion_gp'])), $alt="Photo", $attributes = array('width'=>'80', 'height'=>'80')) }}-->
								<div class="nomgp">
									{{ $pry['gp'] }}
								</div>
								<div class="caption">
									Gerente de Proyecto
								</div>
							</div>
						</center>
					</div>
					<div class="col-md-11 nopadding">
						<div class="table-responsive" id="divPry">
							<table class="table" id="tableDetPry">     
									<thead class="tr_t">                  
										<th class="caption_table" width="15%" >Cliente
										</th>
										<th class="caption_table" width="10%" >Código Proyecto
										</th>
										<th class="caption_table" width="25%" >Nombre de Proyecto
										</th>
										<!-- <th class="caption_table" width="100%" >Lider
										</th> -->
										<th class="caption_table" width="10%" >L.D. Civil
										</th>
										<th class="caption_table" width="10%" >L.D. Geotecnia
										</th>             
										<th class="caption_table" width="10%" >L.D. Hidraulico
										</th>   
										<th class="caption_table" width="10%" ><center>Fecha Inicio
										<center></th>	
										<th class="caption_table" width="10%" ><center>Fecha Fin
										<center></th>	
									</thead>
									<tbody>
										<?php $deta = $pry['deta']; ?>
										@foreach($deta as $dpry)
										
											<tr class="tr_t"
											@if($dpry['nuevo']==1)
												style="background-color:rgb(98,183,111)"
											@endif
											@if($dpry['medio']==1)
												style="background-color:rgb(248,210,54) "
											@endif											
											@if($dpry['ultimo']==1)
												style="background-color:rgb(232,59,59)"
											@endif											
											>
												<td width="15%"  >{{ $dpry['cliente'] }}</td>
												<td width="10%"  >{{ $dpry['codigopry'] }}</td>
												<td width="25%"  >{{ $dpry['nombrepry'] }}</td>
												<!-- <td width="100%"  >{{ $dpry['lider'] }}</td> -->
												<td width="10%"  >{{ $dpry['civil'] }}</td>
												<td width="10%"  >{{ $dpry['geotec'] }}</td>
												<td width="10%"  >{{ $dpry['hidrau'] }}</td>
												<td width="10%"  ><center>{{ $dpry['finicio'] }}</center></td>
												<td width="10%"  ><center>{{ $dpry['fcierre'] }}</center></td>
											</tr>
											<?php $j++; ?>
											<?php $pj++; ?>
										@endforeach
									</tbody>
							</table>
						</div>

					</div>
				</div>
			<?php if($j==10 || $p==3 || ($p>=3 && $pj>=4) ){ ?>
			<?php $j=0; ?>
			<?php $p=0; ?>
			</div>
			<?php } ?>

		@endforeach
		</div>

		<!-- Left and right controls -->
		<!--<a class="left carousel-control" href="#carouselExampleIndicators" role="button" data-slide="prev">
			<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
			<span class="sr-only">Previous</span>
		</a>
		<a class="right carousel-control" href="#carouselExampleIndicators" role="button" data-slide="next">
			<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
			<span class="sr-only">Next</span>
		</a>-->



	</div>	<!-- Fin de Carrousel -->

	</div>

	
	<footer>
	<center>
		<div>
			<ol class="carousel-indicators">
				@for($i = 0; $i < $hojas; $i++)
				<li data-target="#carouselExampleIndicators" data-slide-to="{{ $i }}" 
				<?php if($i==0){ ?>
				class="active"
				<?php } ?>
				></li>
				@endfor
			</ol>	
		</div>
		<br>
		<div id="carouselButtons" >
			<button id="anterior" type="button" class="btn btn-default btn-xs">
			<span class="glyphicon glyphicon-chevron-left"></span>
			</button>	
			<button id="pauseButton" type="button" class="btn btn-default btn-xs">
			<span class="glyphicon glyphicon-pause"></span>
			</button>
			<button id="posterior" type="button" class="btn btn-default btn-xs">
			<span class="glyphicon glyphicon-chevron-right"></span>
			</button>		
		</div>
	</center>
	</footer>



<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>


<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script> 
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--<script src="dist/js/pages/dashboard.js"></script>-->
<script>
var num_total = $('#carouselExampleIndicators .item').length; 
var IdTimeout = 0;
if(num_total==1){
	IdTimeout = setTimeout(recarga,15000); 
}
console.log(IdTimeout);
$("button").click(function() {
  if ($(this).attr("id") === "pauseButton") {
    $('#carouselExampleIndicators').carousel('pause');
    $(this).attr("id", "playButton");	
    $("span", this).toggleClass("glyphicon-play glyphicon-pause");
  } else if($(this).attr("id") === "playButton"){
    $('#carouselExampleIndicators').carousel('cycle');
    $(this).attr("id", "pauseButton");
    $("span", this).toggleClass("glyphicon-pause glyphicon-play");
  }

  if($(this).attr("id")==="anterior"){
	  $('#carouselExampleIndicators').carousel('prev');
	  $('#carouselExampleIndicators').keypress(function(e) {
	  	if(e.which == 37) {
	  	alert("Has pulsado la fecha izquierda");
            }
	  });
  }

  if($(this).attr("id")==="posterior"){
	  $('#carouselExampleIndicators').carousel('next');
  }  
});

var handled=false;//global variable
$('#carouselExampleIndicators').on('slid.bs.carousel', function () {
	
	if ($("#carouselExampleIndicators .carousel-inner .item:last").hasClass("active")){
		clearTimeout(IdTimeout);
		IdTimeout=setTimeout(recarga,3000);
	}
	

});
$('#carouselExampleIndicators').on('slide.bs.carousel', function (e) {
	var current=$(e.target).find('.item.active');
	
    var indx=$(current).index();
	
    if((indx+2)>$('.carousel-indicators li').length)
        indx=-1;
     if(!handled)
     {
        $('.carousel-indicators li').removeClass('active')
        $('.carousel-indicators li:nth-child('+(indx+2)+')').addClass('active');
     }
     else
     {
        handled=!handled;//if handled=true make it back to false to work normally.
     }
});
// function recarga(){
// 	refresh();
// }

function recarga(){
mostrarVideo();
}


function refresh(){
	//$('body').fadeOut('1000');
	location.reload(true);
	//$('body').fadeIn('3000');
	//$('body').fadeToggle('slow');
}



$(".carousel-indicators li").on('click',function(){
   //Click event for indicators
   $(this).addClass('active').siblings().removeClass('active');
   //remove siblings active class and add it to current clicked item
   handled=true; //set global variable to true to identify whether indicator changing was handled or not.
});

var v = document.getElementById("my-video");
v.addEventListener("timeupdate", function() {
console.log(v.currentTime);
if(v.currentTime >= v.duration)
refresh();
//alert("termino");

}, true);
function mostrarVideo(){
$("#wrapper").hide();
$("#my-video").show();
$("#my-video").css('position', 'absolute').css('width', '100%').css('height',
'90%').css('margin', 0).css('margin-top', '2%').css('top', '0').css('left', '0').css('float',
'left').css('z-index', 600);
v.play();}

</script>

<script>
var end = new Date('04/12/2019 09:00');
console.log(end);
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;

    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {

            clearInterval(timer);
            document.getElementById('#conteo').innerHTML = 'EXPIRED!';

            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        //document.getElementById('countdown').innerHTML = days + ' dias, ';
        //document.getElementById('countdown').innerHTML += hours + ' horas, ';
        //document.getElementById('countdown').innerHTML += minutes + ' minutos y ';
				//document.getElementById('countdown').innerHTML += seconds + ' segundos';

				$("#dia").text(days);
				$("#hora").text(hours);
				$("#min").text(minutes);
				$("#seg").text(seconds);
				
				$("#conteo").css('position', 'absolute').css('width', '100%').css('height',
'90%').css('margin', 0).css('margin-top', '2%').css('top', '0').css('left', '0').css('float',
'left').css('z-index', 600);
    }

    timer = setInterval(showRemaining, 1000);
</script>


</body>
</html>