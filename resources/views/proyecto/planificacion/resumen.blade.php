
<table class="table table table-bordered table-hover" style="font-size: 12px">
	<thead>
		<tr style="background-color: #0069aa; color: white">
			<th>Proyectos\Personas</th>
			<?php $i=0; ?>
			<?php for ($i=0; $i < count($cab) ; $i++) { ?>
				<th>
				{{ $cab[$i] }} <br>
				(horas disponibles: {{ $arrayHorasDisponibles[$i] }})
				</th>
			<?php } ?>
			
			<th>TOTAL</th>
		</tr>

	</thead>
	<tbody>
		<?php $x = 0; ?>
		@foreach($matriz as $i)
		<tr>				
			@foreach($i as $j)
			<td>
				{{ $j }}
			</td>
			@endforeach	
			<td>{{ $total[$x] }}</td>
			<?php $x++; ?>
		</tr>
		@endforeach
		<tr>
			<td><b>TOTAL</b></td>
		@foreach($totalV as $val)		
			<td>{{ $val }}</td>		
		@endforeach
		</tr>
	</tbody>
</table>
