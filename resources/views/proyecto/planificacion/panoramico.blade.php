<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Evaluación</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="{{ url('dist/css/chosen/chosen.min.css') }}"/>
	<style type="text/css">		
		#contenido{			
			font-size: 10px;			
			margin-top: 5px;			
			margin-bottom: 5px;
		}
		.btn-mostrar-lider:hover{
			background-color: yellow;
		}
	</style>
</head>

<body>

	<input type="hidden" id="periodo" value="{{ $periodo }}">
	<input type="hidden" id="semana" value="{{ $semana }}">
	{{ csrf_field() }}

<!-- <div class="container">       -->

	<div class="content-wrapper" style="min-height: 853px;">

		<section class="content">

			<div class="col-md-12" id="contenido">

				<p><a href="{{ url('planificacion/horas') }}">Regresar a Planificación de horas por semana.</a></p>

				<h4><center>Resumen de Programación Semanal</center></h4>

				<p><b>Periodo:</b> {{ $periodo }}. <b>Semana:</b> {{ $semana }}</p>
				<p>Actualizado al {{ $fecha }}. <span id="msjActualizacion" class="label label-warning" style="font-size: 12px"></span>
				</p>


	<p>Lideres que están ocultos: <span id="listaLideresOcultos" style="display: inline-block;" ></span></p>
	
	<p>Cantidad: <span id="spanCantidadLideresOcultos">0</span> lideres ocultos. </p>

<table class="table table table-bordered table-hover fixed">
	<thead>
	<tr style="background-color: #0069aa; color:white; " >
		<th rowspan="3"><center>Lider de Disciplina</center></th>
		<th rowspan="3"><center>Codigo</center></th>
		<th rowspan="3"><center>Proyecto</center></th>
		<th colspan="{{ count($personas) }}">
			<center>Profesionales
			<button class="btn-info btn-agregar-persona" id="btn-agregar-persona"
						title="Agregar una persona a un proyecto para planificar">
						<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
			</button>
			</center>
		</th>
		<th rowspan="3">Total</th>
	</tr>

	<tr style="background-color: #0069aa; color:white" >
		<?php for ($i=0; $i < count($arrayCategoriasContador) ; $i++) { ?>
		<th colspan="{{ $arrayCategoriasContador[$i]['cantidadColumnas'] }}" >
			<center>{{ $arrayCategoriasContador[$i]['categoriaNombre'] }}</center>
		</th>
		<?php } ?>	
	</tr>
	
	<tr style="background-color: #0069aa; color:white" >

	<?php for ($i=0; $i < count($personas) ; $i++) { ?>
		<th>
			{{ $personas[$i] }}<br/>
		</th>		
	<?php } ?>

	</tr>

	<tr style="background-color: white;">		
		<td colspan="3" style="text-align: right;">Total de horas:</td>
		<?php for ($i=0; $i < count($totalProfesional) ; $i++) { ?>
			<td style="font-size: 13px;" ><center>{{ $totalProfesional[$i] }}</center></td>
		<?php } ?>
		<td></td>
	</tr>

	<tr style="background-color: white;">		
		<td colspan="3" style="text-align: right;">Total de horas disponibles:</td>
		<?php for ($i=0; $i < count($horasDiponibles) ; $i++) { ?>
			<td style="font-size: 13px;" ><center>{{ $horasDiponibles[$i] }}</center></td>
		<?php } ?>
		<td></td>
	</tr>
	</thead>
	
	<tbody>
	<?php $lider_nombre_temp = ""; //para rowspan ?>
	<?php $posTotalProyecto=0; ?>

	@foreach($matriz as $fila)	
	<?php $i = 0; ?>
	<tr data-codigoproyecto="{{ $fila[1] }}" data-lider='{{ $fila[0] }}'>		
		
		@foreach($fila as $valor)
			@if($i==0)				
				@if($lider_nombre_temp != $valor)
					<?php $lider_nombre_temp = $valor; ?>
					<td rowspan="{{ $arrayRowspanLideres[$lider_nombre_temp] }}" >
						{{ $valor }}
						<button class="btn-info btn-ocultar" data-liderocultar="{{ $fila[0] }}" 
							title="Ocultar la lista de proyectos del Lider">
						<span  class="glyphicon glyphicon-minus" aria-hidden="true"></span>
						</button>
					</td>
				@endif

			@elseif(1<=$i and $i<=2)			
			<td>
				{{ $valor }}
			</td>

			@else
			<td style="padding:0px; min-width: 77px;" >
				
				<input class="planificacion_horas" style="text-align: center; font-size: 12px; width: 100%; min-height: 30px;" type="text" data-cpersona="{{ $arrayCodPersonas[$i-3] }}" value="{{ $valor }}" title="{{ $fila[1] }} - {{ $personas[$i-3] }}">
				
			</td>
			@endif
			<?php $i++; ?>
		@endforeach

		<td>{{ $totalProyecto[$posTotalProyecto] }}</td>
		<?php $posTotalProyecto++; ?>
	</tr>
	@endforeach

	<tr>
		<td colspan="3" >TOTAL</td>
		@foreach($totalProfesional as $total)
		<td style="font-size: 13px;" ><center>{{ $total }}</center></td>
		@endforeach

	</tr>
	</tbody>
	
</table>

</div>

</section>

</div>

<!-- </div> -->

<div class="modal fade" tabindex="-1" role="dialog" id="modalAgregarPersona">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Agregar Profesional para planificar</h4>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-3" >
      			Proyecto: 		
      		</div>
      		<div class="col-md-9" >
      			<select id="cproyecto" class="form-control chosen-select">
		      		<option value="0">elegir</option>
		      		<?php for ($i=0; $i < count($arrayProyectos); $i++) { ?>
		      			<option value="{{ $arrayCodProyectos[$i] }}">{{ $arrayCodigoProyectos[$i] }} - {{ $arrayProyectos[$i] }}</option>
		      		<?php } ?>
		      	</select>
      		</div>
      	</div>

      	<div class="row" style="margin-top: 10px;">
      		<div class="col-md-3" >
      			Profesional: 	
      		</div>
      		<div class="col-md-9" >
      			<select id="cpersona" class="form-control chosen-select">
		      		<option value="0">elegir</option>
		      		<?php for ($i=0; $i < count($personas); $i++) { ?>
		      			<option value="{{ $arrayCodPersonas[$i] }}">{{ $personas[$i] }}</option>
		      		<?php } ?>
		      	</select>
      		</div>
      	</div>

      	<div class="row"  style="margin-top: 10px;">
      		<div class="col-md-3" >
      			Categoria: 		
      		</div>
      		<div class="col-md-9" >
      			<select id="ccategoriaprofesional" class="form-control chosen-select">
		      		<option value="0">elegir</option>		      		
		      		@foreach($listaCategorias as $item)
		      			<option value="{{$item->ccategoriaprofesional }}">{{ $item->descripcion }}</option>
		      		@endforeach
		      	</select>
      		</div>
      	</div>
      	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" id="btn-registrar-persona" >Registrar</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script type="text/javascript" src="{{ url('dist/js/chosen/chosen.jquery.min.js') }}"></script>
<script>
	var BASE_URL = "<?php echo url('/'); ?>";
</script>

<script type="text/javascript" src="{{ asset('js/goheadfixed.js') }}" ></script>
<script type="text/javascript" src="{{ asset('js/planificacion_resumen.js') }}" ></script>

</body>

</html>
