@extends('layouts.master')

@section('content')

<style type="text/css">
	#resultado{
		height: auto;
	}
</style>

<input type="hidden" id="periodo">
<input type="hidden" id="semana">
<input type="hidden" id="usuario" value="{{ $usuario }}">
<input type="hidden" id="urlResumenPanoramico" value="{{ url('planificacion/resumen/panoramico') }}">
{{ csrf_field() }}

<style type="text/css">
	.horas-disponible{
		width: 30%;
	}

	.proyecto-seleccionado{
		background-color: #c9e3f4;		
		cursor: pointer; 
	}
	.proyecto-nopermitido{
		background-color: #DCDCDCFF;	
	}
	.proyecto{
		background-color: white;		
		cursor: pointer; 
	}
	#resultado{
		height: auto;
	}
</style>

<div class="content-wrapper" >

<section class="content-header">
  <h1>
    Planificación Diaria
  </h1>
</section>

<section class="content" style="font-size: 12px" style="text-align: center;">          

<div class="row">
	<div class="col-md-1">
		Proyecto
	</div>
	<div class="col-md-4">
		<select id="listaProyectos" class="form-control chosen-select" data-placeholder="Elegir un proyecto...">			
			<option value="0" >elegir</option>
		</select>
	</div>	

	<div class="col-md-1">
		Fecha :
	</div>
	<div class="col-md-2">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right " id="fecha" readonly="true" name="fecha" type="text" value="{{ $fecha }}">
		</div>
	</div>	

	<div class="col-md-3">
		<!--<button class="btn btn-primary btn-ver-resumen " data-tipo="ResumenProVSPer">Ver resumen ProVSPer</button>	-->	
		<button class="btn btn-primary btn-ver-resumen " data-tipo="ResumenActVsPer">Ver resumen ActVsPer</button>		
	</div>

	<div class="col-md-1" style="display: none" id="imgLoad" >
		<img src="{{ asset('img/load.gif') }}" width="25px" >
	</div>

</div>

<div class="row" style="margin-top: 15px;">
	<div class="col-md-12">
		<input id="chkUsuarios" type="checkbox" name="" checked> Mostrar solo usuarios relacionados al proyecto.
	</div>
</div>

<div class="row" style="margin-top: 5px">
	<div class="col-md-12">
		<input id="chkUsuariosHorasRegistradas" type="checkbox"> Mostrar solo usuarios que tienen horas registradas.
	</div>
</div>

<div class="row" style="margin-top: 5px">
	<div class="col-md-12">
		<input id="chkActividadesHorasRegistradas" type="checkbox"> Mostrar solo actividades que tienen horas registradas.
	</div>
</div>

<div class="row" id="mensajes_tiempo_real" style="margin-top: 15px; display:none">
	<div class="col-md-12">		
		<p>
			<span class="label label-warning"><i class="fa fa-bell-o"></i> Alerta </span><span id="tr_mensaje"></span>
		</p> 
	</div>
</div>

<div class="row" style="margin-top: 15px">
	<div class="col-md-5" >
		<h5>Actividades</h5>		
		<select id="comboListaActividades" class="form-control chosen-select">			
			<option value="0" >especificar código o descripción de actividad</option>
		</select>	
		<table id="listaProyectos2" class="table table-hover table-bordered">
			<thead>
			<tr style="background-color: #0069aa; color: white" >
				<!--<th>#</th>-->
				<th>Código</th>
				<th>Descripción</th>
			</tr>
			</thead>

			<tbody>
			</tbody>
			
		</table>
	</div>

	<div class="col-md-7" id="divTabla">
		<h5>Tabla de disponibilidad de HH de la semana <span id="num_semana"></span> </h5>

		<table id="tablaUsuarios" class="table table-hover table-bordered">
			<thead>
			<tr style="background-color: #0069aa; color:white;" >
			<th style='border:1px solid #DCDCDCFF;'>#</th>
			<th style='border:1px solid #DCDCDCFF;'>Nombres</th>
			<th style='border:1px solid #DCDCDCFF;'>Categoria</th>
			<th style='border:1px solid #DCDCDCFF;'>Lun</th>
			<th style='border:1px solid #DCDCDCFF;'>Mar</th>
			<th style='border:1px solid #DCDCDCFF;'>Mie</th>
			<th style='border:1px solid #DCDCDCFF;'>Jue</th>
			<th style='border:1px solid #DCDCDCFF;'>Vie</th>
			<th style='border:1px solid #DCDCDCFF;'>Sab</th>
			<th style='border:1px solid #DCDCDCFF;'>Dom</th>			
			<!--<th style='border:1px solid #DCDCDCFF;'>Comentario</th>-->
			</tr>
			</thead>

			<tbody>
			</tbody>
			
		</table>
		<div style="display:none" id="divMensajeSinRegistros">
			<span class="label label-warning">Alerta</span> No se encontraron registros.		
		</div>	
	</div>
	
</div>

<div id="container" style="min-width: auto; height: 400px;"></div>

<div class="row" style="margin-top: 15px; display: none" >
	<div class="col-md-12" id="mensajes_actualizaciones">
		<p>Últimas actualizaciones:</p>
		<div id="listaActualizaciones">			
		</div>
	</div>
</div>


</section>

</div>

<div class="modal fade bs-example-modal-lg" id="modalResumen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Resumen de Planificación</h4>
      </div>
      <div class="modal-body" id="divTablaResumen">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="SERVIDOR_NODE" value="{{ env('SERVIDOR_NODE') }}">

<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
	crossorigin="anonymous"></script>
<!-- <script src="http://192.168.50.65:3000/socket.io/socket.io.js"></script> -->
<script src="{{ env('SERVIDOR_NODE') }}/socket.io/socket.io.js"></script>
<script type="text/javascript" src="{{ asset('js/fecha.js') }}" ></script>
<script type="text/javascript" src="{{ asset('js/planificacion_diaria.js') }}" ></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

@stop
