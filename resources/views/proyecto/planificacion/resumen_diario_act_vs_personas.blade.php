<?php $cantidadFilas = count($matriz[0]); ?>

<table class="table table table-bordered table-hover" style="font-size: 12px">
	<thead>
		<tr style="background-color: #0069aa; color: white">
			<?php for ($j=0; $j < count($matriz); $j++) { ?>
					<td>{{ $matriz[$j][0] }}</td>
			<?php } ?>
		</tr>

	</thead>
	<tbody>
		
		<?php for ($i=1; $i <$cantidadFilas; $i++) { ?>
			<tr>
				<?php for ($j=0; $j < count($matriz); $j++) { ?>
					<td>{{ $matriz[$j][$i] }}</td>
				<?php } ?>
			</tr>
		<?php } ?>
	</tbody>
</table>

