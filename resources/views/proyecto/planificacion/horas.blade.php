@extends('layouts.master')

@section('content')

<style type="text/css">
	#resultado{
		height: auto;
	}
</style>

<input type="hidden" id="periodo">
<input type="hidden" id="semana">
<input type="hidden" id="usuario" value="{{ $usuario }}">
<input type="hidden" id="urlResumenPanoramico" value="{{ url('planificacion/resumen/panoramico') }}">
{{ csrf_field() }}

<style type="text/css">
	.horas-disponible{
		width: 30%;
	}

	.proyecto-seleccionado{
		background-color: #c9e3f4;		
		cursor: pointer; 
	}
	.proyecto{
		background-color: white;		
		cursor: pointer; 
	}
	#resultado{
		height: auto;
	}
</style>

<div class="content-wrapper" >

<section class="content-header">
  <h1>
    Planificación 
  </h1>
</section>

<section class="content" style="font-size: 12px" style="text-align: center;">          

<div class="row">
	<div class="col-md-1">
		Semana :
	</div>
	<div class="col-md-2">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right " id="fecha" readonly="true" name="fecha" type="text" value="{{ $fecha }}">
		</div>
	</div>	

	<div class="col-md-2">
		<button class="btn btn-primary" id="btn-verResumen">Ver resumen</button>		
	</div>
	<!-- (jefe == 1) -->
	<div class="col-md-3">
		<a href="{{ url('planificacion/resumen/panoramico') }}" class="btn btn-primary" target="_blank" id="linkPanoramico" >Ver programación de proyectos</a>
	</div>
	<!-- endif -->

	<div class="col-md-1" style="display: none" id="imgLoad" >
		<img src="{{ asset('img/load.gif') }}" width="25px" >
	</div>

</div>

<div class="row" style="margin-top: 15px; display: none">
	<div class="col-md-1">
		Proyecto
	</div>
	<div class="col-md-4">
		<select id="listaProyectos" class="form-control">			
		</select>
	</div>	
</div>

<div class="row" style="margin-top: 15px">
	<div class="col-md-12">
		<input id="chkUsuarios" type="checkbox" name="" checked> Mostrar solo usuarios relacionados al proyecto.
	</div>
</div>

<!-- <div class="row" style="margin-top: 5px">
	<div class="col-md-12">
		<input id="chkUsuariosxGerencia" type="checkbox" > Mostrar solo usuarios relacionados a la Gerencia del Área.
	</div>
</div> -->

<div class="row" style="margin-top: 5px">
	<div class="col-md-12">
		<input id="chkUsuariosHorasRegistradas" type="checkbox"> Mostrar solo usuarios que tienen horas registradas.
	</div>
</div>

<div class="row" style="margin-top: 5px">
	<div class="col-md-12">
		<input id="chkProyectosHorasRegistradas" type="checkbox"> Mostrar solo proyectos que tienen horas registradas.
	</div>
</div>

<div class="row" id="mensajes_tiempo_real" style="margin-top: 15px; display:none">
	<div class="col-md-12">		
		<p>
			<span class="label label-warning"><i class="fa fa-bell-o"></i> Alerta </span> <span id="tr_mensaje"> </span>
		</p> 
	</div>
</div>

<div class="row" style="margin-top: 15px">
	<div class="col-md-5" id="divTabla">
		<h5>Tabla de disponibilidad de HH de la semana <span id="num_semana"></span> </h5>

		<table id="tablaUsuarios" class="table table-hover table-bordered">
			<thead>
			<tr style="background-color: #0069aa; color:white;" >
			<th style='border:1px solid #DCDCDCFF;'>#</th>
			<th style='border:1px solid #DCDCDCFF;'>Nombres</th>
			<th style='border:1px solid #DCDCDCFF;'>Categoria</th>
			<th style='border:1px solid #DCDCDCFF;'>Planificadas</th>
			<th style='border:1px solid #DCDCDCFF;'>Disponible</th>
			<th style='border:1px solid #DCDCDCFF;'>Ejecutadas HT</th>
			<!-- <th style='border:1px solid #DCDCDCFF;'>Ejecutadas Adm</th> -->
			</tr>
			</thead>

			<tbody>
			</tbody>
			
		</table>
		<div style="display:none" id="divMensajeSinRegistros">
			<span class="label label-warning">Alerta</span> No se encontraron registros.		
		</div>	
	</div>
	
	<div class="col-md-7" >
		<h5>Proyectos</h5>
		<table id="listaProyectos2" class="table table-hover table-bordered">
			<thead>
			<tr style="background-color: #0069aa; color: white" >
			<th>#</th>
			<th>Codigo - Nombres</th>
			<!-- <th>Avance HH</th> -->
			<!-- <th>Avance GG</th> -->
			</tr>
			</thead>

			<tbody>
			</tbody>
			
		</table>
	</div>
</div>

<div id="container" style="min-width: auto; height: 400px;"></div>

<div class="row" style="margin-top: 15px">
	<div class="col-md-12" id="mensajes_actualizaciones">
		<p>Últimas actualizaciones:</p>
		<div id="listaActualizaciones">			
		</div>
	</div>
</div>


</section>

</div>

<div class="modal fade bs-example-modal-lg" id="modalResumen" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Resumen de Planificación</h4>
      </div>
      <div class="modal-body" id="divTablaResumen">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

<input type="hidden" id="SERVIDOR_NODE" value="{{ env('SERVIDOR_NODE') }}">


<script src="https://code.jquery.com/jquery-3.3.1.slim.js" integrity="sha256-fNXJFIlca05BIO2Y5zh1xrShK3ME+/lYZ0j+ChxX2DA="
	crossorigin="anonymous"></script>
<!-- <script src="http://192.168.50.65:3000/socket.io/socket.io.js"></script> -->
<script src="{{ env('SERVIDOR_NODE') }}/socket.io/socket.io.js"></script>
<script type="text/javascript" src="{{ asset('js/fecha.js') }}" ></script>
<script type="text/javascript" src="{{ asset('js/planificacion_horas.js') }}" ></script>

<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>

@stop
