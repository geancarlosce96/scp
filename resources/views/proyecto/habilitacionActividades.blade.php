<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto -
        <small>Habilitación de tareas del proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li>@include('accesos.accesoProyecto')</li>
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
               

               
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>

        {!! Form::open(array('url' => 'grabarHabilitacion','method' => 'POST','id' =>'frmHabilitacion')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
    <div class="row">
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente</div>
    <div class="col-lg-9 col-xs-9 clsPadding">
      {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
      {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
    </div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    
    
    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera</div>
    <div class="col-lg-4 col-xs-4 clsPadding">
      {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad minera','disabled' => 'true','id'=>'umineranombre') ) !!}
    </div> 
    <div class="col-lg-1 col-xs-1 clsPadding">Código proyecto</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button class="btn btn-primary btn-block" id='btnSave'><b>Grabar</b></button></div>
        
          
      
     </div>    
<div class="row clsPadding">
<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>


<div class="row clsPadding2">
    <div class="col-lg-2 col-xs-2 clsPadding">Nombre del proyecto</div>
    <div class="col-lg-9 col-xs-9 clsPadding">
		{!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de proyecto')) !!}   
    </div>	 
</div> 
<div class="row clsPadding2">
    <div class="col-lg-2 col-xs-2 clsPadding">Fecha inicio</div>
    <div class="col-lg-4 col-xs-4 clsPadding"><div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control pull-right', 'disabled'=>'true')) !!}</div> </div> 
    <div class="col-lg-1 col-xs-1 clsPadding">Fecha cierre</div>
    <div class="col-lg-4 col-xs-4 clsPadding">
      <div class="input-group date">
              <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>

              </div>
        {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}
      </div>
      
    </div>
</div>
<div class="row clsPadding2">

    <div class="col-lg-2 col-xs-2 clsPadding">Estado</div>
      <div class="col-lg-4 col-xs-4 clsPadding">
          {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!}  
      </div>
    <div class="col-lg-1 col-xs-1 clsPadding">Subproyecto</div>
      <div class="col-lg-4 col-xs-4 clsPadding">
      {!! Form::select('subproyecto',(isset($subproyecto)==1?$subproyecto:array()),null,array('class' => 'form-control','id'=>'subproyecto','disabled'=>'true')) !!} 
      </div>    
</div>

        <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div>


     <div class="row">
          <div class="col-lg-12 col-xs-12 table-responsive" id="divActi">
              @include('partials.tableHabilitacionActividadProyecto',array('listaAct' => (isset($listaAct)==1?$listaAct:array()), 'participantes'=> (isset($participantes)==1?$participantes:array() ) ) )

          </div>
      </div>



      <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
      </div>
      <div class="row clsPadding2">
          <div class="col-lg-3 col-xs-3">
              <button class="btn btn-primary btn-block" style="width:200px;" id='btnSave'><b>Grabar</b></button> 
          </div>
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 
</div>
    {!! Form::close() !!}
    </section>

    <!-- Modal Buscar Proyecto-->
    @include('partials.searchProyecto')
   
   


<script>

        var selected =[];
        var selected_actividad =[];
        var personal_rate =<?php echo (isset($personal_rate)==1?json_encode($personal_rate):'[]'); ?>

        $.fn.dataTable.ext.errMode = 'throw';
        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyLider",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'GteProyecto' , name : 'p.abreviatura'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'},
                {data : 'descripcion' , name : 'te.descripcion'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });

        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        });     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              getUrl('editarHabilitacionActividades/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }
       

        /* Grabar frmDisciplina*/
        $('#frmHabilitacion').on('submit',function(e){
          
            /*$.ajaxSetup({
                header: document.getElementById('_token').value
            });*/
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            
            
            $.ajax({

                type:"POST",
                url:'grabarHabilitacion',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                      
                },
                error: function(data){
                    
                    
                   
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();

                }
            });
        });        
        /* Fin Grabar FrmDisciplina*/
        function verparticipantes(id){
          $.ajaxSetup({
              header: document.getElementById('_token').value
          });          

          $.ajax({
            type: 'GET',
            url : 'viewActividadesParticipante',
            data: "cproyecto="+$("#cproyecto").val()+"&cproyectopersona="+id,
            beforeSend : function(){
              $('#div_carga').show();
            },
            success : function (data){
              $('#div_carga').hide(); 
              $("#divConteHoras").html(data);
              $("#divEditarHoras").modal();
            },
            error: function(data){
              $('#div_carga').hide();
            }
          });
          
        } 

        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 

        var table = $('#tableActividad').DataTable( {
        "paging":   false,
        "ordering": false,
        "info":     false,
        lengthChange: false,
        buttons: [ 'excel', 'pdf', 'colvis' ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,
        fixedColumns:   {
            leftColumns: 2
        }
    } ); 

    table.buttons().container()
        .appendTo( '#tableActividad_wrapper .col-sm-6:eq(0)' );        
     
</script>

