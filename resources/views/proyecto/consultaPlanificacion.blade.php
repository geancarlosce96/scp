<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Consulta de Planificacion (GL/GP)</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => 'viewConsultaProyecto','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!}


        <div class="row">
            <!--<div class="col-lg-1 hidden-xs"></div>-->
            <div class="col-md-12 col-xs-12">
                <div class="row clsPadding2"> 
                        <div class="col-lg-3 col-xs-6">
                        Área:
                            {!! Form::select('disciplina',(isset($disciplinas)==1?$disciplinas:array()),'',array('class' => 'form-control','id'=>'disciplina')) !!}
                        </div> 
                        <div class="col-lg-3 col-xs-6">
                            <label class="clsTxtNormal">Fecha desde:</label>            
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('fdesde','', array('class'=>'form-control pull-right datepicker','id' => 'fdesde')) !!}
                            </div>
                        </div>
                        <div class="col-lg-3 col-xs-6">
                            <label class="clsTxtNormal">Fecha hasta:</label>            
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                {!! Form::text('fhasta','', array('class'=>'form-control pull-right datepicker','id' => 'fhasta')) !!}
                            </div>        
                        
                        </div>  
                        <div class="col-lg-3 col-xs-6" style="margin-top:25px;">
                            <button type="button" class="btn btn-primary btn-block " id="btnView"> <b>Visualizar</b></a>
                        </div>   
                </div> 
            
                <div class="row clsPadding2">	
                    <div class="col-lg-12 col-xs-12 table-responsive" id="tablePlani">
                    @include('partials.tableConsultaProyecto',array('pla'=>(isset($pla)==1?$pla:array()) ))   
                    </div>
                </div>  
            
                <div class="row clsPadding">
                    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                </div>
                <div class="row clsPadding2">
                    <div class="col-lg-3 col-xs-3">
                        <button type="button" class="btn btn-primary btn-block"><b>Imprimir</b></button> 
                    </div>                       
                </div>
            </div>
        </div>
    {!! Form::close() !!}
    </section>
    <!-- /.content -->
  </div>

  <script>

    $("#btnView").click(function (e){

            e.preventDefault();
            if ( $("#fhasta").val()=='' || $("#fdesde").val()==''){
                alert('Especifique el Area, Fecha Desde, Fecha Hasta de la Planificación');
                return;
            }
            
            $.ajax({
                url: 'viewConsultaProyecto',
                type: 'GET',
                data: "cdisciplina="+$("#disciplina").val()+"&fhasta="+ $("#fhasta").val() + "&fdesde=" + $("#fdesde").val(),
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#tablePlani").html(data);
                    $('#div_carga').hide(); 
                },
            });
        });

        

        

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });           
</script> 