          <table class="table table-striped">
            <thead>
              <tr class="clsCabereraTabla">
                <th>Disciplina</th>
                <th>Profesional</th>
                <th>Categoría</th>
                <th>Líder</th>
                <th>Eliminar</th>
              </tr>
            </thead>
              <tbody>
              <?php $i=0; ?>
              @if(isset($proyectodisciplina)==1)
              @foreach($proyectodisciplina as $d)
                <?php $activado=false; ?>
                <?php $cpersona_rdp=''; ?>
                <?php $disciplina=''; ?>
                <?php $participante=''; ?>
                <?php $categoria=''; ?>     
                <?php $lider=''; ?>           

                <?php $i++; ?>
                <tr>
                  <td>
                        <?php $cpersona_rdp = $d['cpersona_rdp']; ?>
                        <?php $participante = $d['participante']; ?> 
                        <?php $categoria=$d['categoria'];  ?>
                        <?php $lider=$d['eslider'];  ?>    

                        {{ $d['disciplina'] }}
                  </td>              
                  <td>
                        {{ $participante }}
                  </td>

                  <td>     

                        {{ $categoria }}
                  </td>

                  <td>     

                        {{ $lider }}
                  </td>

                  <td >
                    <a href="#" onclick="eliminarPartProyecto('{{ $d['cproyectopersona'] }}')" data-toggle="tooltip" title="Eliminar solo si ya grabó" class="fa fa-trash" aria-hidden="true" disabled="true"></i></a>
                    </td>

                </tr>
              @endforeach        
              @endif                  
              </tbody>
            </table> 