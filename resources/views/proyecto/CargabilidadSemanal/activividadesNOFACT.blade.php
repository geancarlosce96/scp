<div class="col-sm-12 table-responsive " style="height: 650px;width: 98%;">
    <table class="table table-hover table-bordered " id="actividades" style="width: 120%" >
        <thead>

        <tr  class="clsCabereraTabla">

            @if(empty($tipo))
                <th>Actividades</th>
            @elseif ( $tipo == '1' )
                <th style="background-color: #11B15B;color: #ffffff; text-align: center;">Actividades</th>
            @elseif ( $tipo == '2' )
                <th style="background-color: #0069aa;color: #ffffff; text-align: center;">Actividades</th>
            @endif
            <th>Semana</th>
            <th id="nombrearea"></th>
            @if(isset($empleados_lista))
            @foreach ($empleados_lista as $ep)
                <th>{{  $ep['abreviatura'] }}</th>
            @endforeach
            @endif
        </tr>
        </thead>
        <tbody>

        @if (isset($arrayhorasnofact_nuevo))
                    @foreach ($arrayhorasnofact_nuevo as $nf)
                        @if ($nf['sumatodos'] > 0)
                            <tr>
                        @else
                            <tr class="hideshow">
                        @endif
                            <td>{{ $nf['descripcion'] }}</td>
                            <td>{{ $semana }}</td>
                            <td>{{ $nf['sumatodos'] }}</td>
                            @foreach($nf['empleados']  as $he )
                                <td >{{ $he['sumempleadoactivity'] }}</td>
                            @endforeach
                            </tr>


                    @endforeach
        @endif
        </tbody>
        <tfoot  class="clsCabereraTabla" style="text-align: left">
        <tr>
            @if(empty($tipo))
                <td>Total</td>
            @elseif ( $tipo == '1' )
                <td style="background-color: #11B15B;color: #ffffff; text-align: left;">Total</td>
            @elseif ( $tipo == '2' )
                <td style="background-color: #0069aa;color: #ffffff; text-align: left;">Total</td>
            @endif
            @if(empty($semana))
                <td>Seleccione semana</td>
            @else
                <td>{{ $semana }}</td>
            @endif

            @if(empty($acumuladoporarea))
                <td>0</td>
            @else
                <td>{{ $acumuladoporarea }}</td>
            @endif


            @if(empty($arrayempleadosactivity))
                <td>Seleccione un area para listar area</td>
            @else
                @foreach($arrayempleadosactivity as $arremplact )
                    <td>{{  $arremplact['sumempleadoactivity'] }}</td>
                @endforeach
            @endif

        </tr>
        </tfoot>
    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>
{{--<script src="plugins/datatables/jquery.dataTables.min.js"></script>
{{--<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>--}}{{--<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>--}}

<script>

    $('#nombrearea').html($("#careas option:selected").text())

    /*    $('#actividades').dataTable( {
            searching: true,
            scrollY: 600,
            scrollX: 400,
            scrollCollapse: true,
            scrollXInner: '100%',
            fixedColumns:   {
                leftColumns: 6,
                //rightColumns: 1
            },
            language: {
                url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
            },

        } );*/

    $("#actividades").tableHeadFixer(
        {  left :0,

        }


    );

</script>
