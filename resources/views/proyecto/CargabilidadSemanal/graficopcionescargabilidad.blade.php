@extends('layouts.master')
@section ('cargabilidad-mensual')

    <div class="content-wrapper">
        <!--=====================================
            =            Banner Cargabilidad            =
        ======================================-->
        <div class="col-sm-12">

            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Gráfico Semanal</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
            </section>
            <hr>
        </div>
        <!--====  End of Section comment  ====-->


        <!--=====================================
            =  Menu de areas de los proyectos   =
        ======================================-->
        <div class="col-md-12">
            <div class="box-body col-md-10 col-md-offset-1">
                <div class="form-group col-sm-3 " style="text-align: center" >
                    <label>Áreas</label>
                    <select id="careas" class="form-control chosen" name="careas">
                        <!-- <option value="0" class="indentar">Seleccione un area</option> -->
                        @foreach($careas as $ca)
                            <option value="{{ $ca->carea }}" class="indentar">{{ $ca->codigo_sig }} {{ $ca->descripcion }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group  col-sm-3" style="text-align: center">
                    <label>Fecha inicial</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fecha_ini','', array('class'=>'form-control pull-right ','id' => 'fecha_ini','readonly'=>'true')) !!}
                    </div>

                </div>

                <div class="form-group  col-sm-3" style="text-align: center">
                    <label>Fecha final</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fecha_final','', array('class'=>'form-control pull-right ','id' => 'fecha_final','readonly'=>'true')) !!}
                    </div>

                </div>

                <div class="col-sm-2" style="    display: flex;align-items: flex-end;height: 52px;">
                    <button id="btngenerar" class="btn btn-block btn-primary" style="height: 34px;padding: 6px 12px">Generar</button>
                </div>
                <div class="col-lg-1" style="display: none;padding-top:10px;" id="imgLoad" >

                    <img src="{{ asset('img/load.gif') }}" width="25px" >
                </div>

            </div>
        </div>
        <!--====  End of  Menu de areas de los proyectos  ====-->
        @include('proyecto.CargabilidadSemanal.graficoproductividadsemanal')
        <div class="col-md-6" style="margin-top: 2.5em;">
         @include('proyecto.CargabilidadSemanal.graficocargabilidadsemanal')
        </div>
        <div class="col-md-6" style="margin-top: 2.5em;">
         @include('proyecto.CargabilidadSemanal.graficocargabilidad3semanas')
        </div>
         @include('proyecto.CargabilidadSemanal.graficocargabilidadpersonal')
         <div class="col-md-4" style="margin-top: 2.5em;">
         @include('proyecto.CargabilidadSemanal.graficocargabilidadnofact')
         </div>
         <div class="col-md-4" style="margin-top: 2.5em;">
         @include('proyecto.CargabilidadSemanal.graficocargabilidadadmin')
         </div>
         <div class="col-md-4" style="margin-top: 2.5em;">
         @include('proyecto.CargabilidadSemanal.graficocargabilidadinternos')
         </div>

    </div>


    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/series-label.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>

    <script src="{{ url('js/graficopcionescargabilidad.js') }}"></script>
    <script src="{{ url('js/graficasemana.js') }}"></script>
    <script src="{{ url('js/grafica3semanas.js') }}"></script>
    <script src="{{ url('js/graficonofacturable.js') }}"></script>
    <script src="{{ url('js/graficaporusuario.js') }}"></script>
    <script src="{{ url('js/graficaadmin.js') }}"></script>
    <script src="{{ url('js/graficointernos.js') }}"></script>
    <script src="{{ url('js/graficaproductividadcargabilidad.js') }}"></script>


    <script type="text/javascript">
        $("#tabla-show").hide();
        $("#tabla-show2").hide();
        $(document).ready(function() {

        var carea = '';
        var fecha_ini = '';
        var fecha_final = '';
            /*==============================
            =            Chosen            =
            ==============================*/

            $("#careas").chosen(
                { width: '100%' }
            );

            /*=====  End of Chosen  ======*/

            $('#fecha_ini').datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                calendarWeeks: true,
                autoclose: true,
                endDate: new Date(),
            }).datepicker("setDate", new Date());
            
            $('#fecha_final').datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                calendarWeeks: true,
                autoclose: true,
                endDate: new Date(),
            }).datepicker("setDate", new Date());
            /*========================================================
            =            Listar data dentro de una grilla            =
            ========================================================*/

            $('#btngenerar').click(function(){
                
                var carea = $("#careas").val();
                var fecha_ini = $("#fecha_ini").val();
                var fecha_final = $("#fecha_final").val();

                graficosemana(carea,fecha_final);
                grafico3semanas(carea,fecha_ini,fecha_final);
                graficonofacturable(carea,fecha_final);
                graficoporusuario(carea,fecha_final);
                graficoadmin(carea,fecha_final);
                graficainternos(carea,fecha_final);
                graficaCargaProductividad(carea,fecha_final);
                //verHHFacturable();


            });

            /*=====  End of Listar data dentro de una grilla  ======*/

        });

    </script>

@stop

