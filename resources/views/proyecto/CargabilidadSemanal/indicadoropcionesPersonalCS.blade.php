@extends('layouts.master')
@section ('indicador-semanal')
    <div class="content-wrapper">
        <!--=====================================
            =            Banner Cargabilidad            =
        ======================================-->
        <div class="col-sm-12">

            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Indicador  Semanal</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
            </section>
            <hr>
        </div>
        <!--====  End of Section comment  ====-->


        <!--=====================================
            =  Menu de areas de los proyectos   =
        ======================================-->
        <div class="col-md-8 col-md-offset-2">
            <div class="box-body col-md-12">
                <div class="form-group col-sm-4 " style="text-align: center" >
                    <label>ÁREAS</label>
                    <select id="careas" class="form-control chosen" name="careas">
                        <!-- <option value="0" class="indentar">Seleccione un area</option> -->
                        @foreach($careas as $ca)
                            <option value="{{ $ca->carea }}" class="indentar">{{ $ca->codigo_sig }} {{ $ca->descripcion }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group  col-sm-4" style="text-align: center">
                    <label>SEMANAS</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fecha_ini','', array('class'=>'form-control pull-right ','id' => 'fecha_ini','readonly'=>'true')) !!}
                    </div>

                </div>

                <div class="col-sm-3" style="    display: flex;align-items: flex-end;height: 52px;">
                    <button id="btngenerar" class="btn btn-block btn-primary" style="height: 34px;padding: 6px 12px">Generar</button>
                </div>
                <div class="col-sm-1" style="display: none;padding-top:10px;" id="imgLoad" >
                    <img src="{{ asset('img/load.gif') }}" width="25px" >
                </div>

            </div>
        </div>
        <!--====  End of  Menu de areas de los proyectos  ====-->
        <div class="col-md-12" id="lista">
            @include('proyecto.CargabilidadSemanal.FACNOFACADMINporpersonal',array(
            'array_fac_nofac_adm' => (isset($array_fac_nofac_adm)==1?$array_fac_nofac_adm:array()),
             'semana' => (isset($semana)==1?$semana:array())
            /* 'arraytotalfacnofacadmin' => (isset($arraytotalfacnofacadmin)==1?$arraytotalfacnofacadmin:array()),
             'sumempleadosplanificados' => (isset($sumempleadosplanificados)==1?$sumempleadosplanificados:array()),
             'totalempleadonolaborable' => (isset($totalempleadonolaborable)==1?$totalempleadonolaborable:array()),
             'totalempleadonoproductivas' => (isset($totalempleadonoproductivas)==1?$totalempleadonoproductivas:array()),
             'sumtotaldescansoproyecto' => (isset($sumtotaldescansoproyecto)==1?$sumtotaldescansoproyecto:array())*/
              ))
        </div>

    </div>

    <link rel="stylesheet" href="{{ url('dist/css/chosen/chosencombo.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            /*==============================
            =            Chosen            =
            ==============================*/

            $("#careas").chosen(
                { width: '100%' }
            );

            $('#fecha_ini').datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                calendarWeeks: true,
                autoclose: true,
                endDate: new Date(),
            }).datepicker("setDate", new Date());


            /*=====  End of Chosen  ======*/

            /*========================================================
            =            Listar data dentro de una grilla            =
            ========================================================*/

            $('#btngenerar').click(function(){
                verHHFacturable();
            });

            /*=====  End of Listar data dentro de una grilla  ======*/

        });

        function verHHFacturable() {

            if ($("#careas").val()=='' || $("#fecha_ini").val()==''){
                alert('Completar todos los campos requeridos');
                return;
            }

            $("#imgLoad").show();
           // var obj = {careas: $('#careas').val(),tipo: $("#tipo").val()};
            var obj = {careas: $('#careas').val(),fecha_ini: $('#fecha_ini').val() };
            $.ajax({
                type:'GET',
                url: 'listaActividadespersonalFACNOFACAND',
                data: obj,
                success: function (data) {
                    $("#imgLoad").hide();
                     console.log(data);
                    $('#lista').html(data);
                },
                error: function(data){
                }
            });

        }

    </script>
@stop