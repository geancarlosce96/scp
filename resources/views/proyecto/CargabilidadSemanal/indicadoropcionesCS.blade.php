@extends('layouts.master')
@section ('indicador-semanal')
    <div class="content-wrapper">
        <!--=====================================
            =            Banner Cargabilidad            =
        ======================================-->
        <div class="col-sm-12">

            <section class="content-header">
                <h1>
                    ANDDES -
                    <small>Indicador  Semanal</small>
                </h1>
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
                    <li class="active">Proyecto</li>
                </ol>
            </section>
            <hr>
        </div>
        <!--====  End of Section comment  ====-->


        <!--=====================================
            =  Menu de areas de los proyectos   =
        ======================================-->
        <div class="col-sm-10 col-sm-offset-1">
            <div class="box-body col-md-12">
                <div class="form-group col-sm-3 " style="text-align: center" >
                    <label>ÁREAS</label>
                    <select id="careas" class="form-control  col-xs-6" style="text-indent: 5px;" name="ccarea">
                        <!-- <option value="0" class="indentar">Seleccione un area</option> -->
                        @foreach($careas as $ca)
                            <option value="{{ $ca->carea }}" class="indentar">{{ $ca->codigo_sig }} {{ $ca->descripcion }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-sm-2" style="text-align: center" >
                    <label>TIPO</label>
                    <select name="tipo" id="tipo" class="form-control  col-xs-6">
                        <option value="1" class="indentar">General No Facturable</option>
                        <option value="2" class="indentar">AND Admnistrativo</option>
                    </select>
                </div>

                <div class="form-group  col-sm-2" style="text-align: center">
                    <label>SEMANAS</label>
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div>
                        {!! Form::text('fecha_ini','', array('class'=>'form-control pull-right ','id' => 'fecha_ini','readonly'=>'true')) !!}
                    </div>

                </div>
                <div class="checkbox form-group  col-sm-2" style="margin-top: 26px">
                    <label><input type="checkbox" value="on" id="showhide"><span style="display: block;padding-top: 4px;"></span>Mostrar / Ocultar Horas Cargadas</label>
                </div>
                <div class="col-sm-2" style="    display: flex;align-items: flex-end;height: 52px;">
                    <button id="btngenerar" class="btn btn-block btn-primary" style="height: 34px;padding: 6px 12px">Generar</button>
                </div>
                <div class="col-sm-1" style="display: none;padding-top:10px;" id="imgLoad" >
                    <img src="{{ asset('img/load.gif') }}" width="25px" >
                </div>

            </div>
        </div>
        <!--====  End of  Menu de areas de los proyectos  ====-->
        <div class="col-md-12" id="lista">
            @include('proyecto.CargabilidadSemanal.activividadesNOFACT',array(
            'actividades' => (isset($actividades)==1?$actividades:array()),
            'empleados' => (isset($empleados)==1?$empleados:array()) ,
            'semana' => (isset($semana)==1?$semana:array()),
            'acumuladoporarea' => (isset($acumuladoporarea)==1?$acumuladoporarea:array()),
            'acumuladoporareaemplado' => (isset($acumuladoporareaemplado)==1?$$acumuladoporareaemplado:array()),
            'arrayempleadosactivity' => (isset($arrayempleadosactivity)==1?$arrayempleadosactivity:array())

              ))
        </div>

    </div>

    <link rel="stylesheet" href="{{ url('dist/css/chosen/chosencombo.css') }}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            /*==============================
            =            Chosen            =
            ==============================*/

            $("#careas").chosen(
                { width: '100%' }
            );
            $("#tipo").chosen(
                { width: '100%' }
            );
            $('#fecha_ini').datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                calendarWeeks: true,
                autoclose: true,
                endDate: new Date(),
            }).datepicker("setDate", new Date());


            /*=====  End of Chosen  ======*/

            /*========================================================
            =            Listar data dentro de una grilla            =
            ========================================================*/

            $('#btngenerar').click(function(){
                verHHFacturable();
            });


            $('#showhide').click(function(){

                if ($('#showhide:checked').val()=='on') {
                    $('.hideshow').hide();
                }
                else{
                    $('.hideshow').show();
                }

            });
            /*=====  End of Listar data dentro de una grilla  ======*/

        });

        function verHHFacturable() {

            if ($("#careas").val()== 0 || $("#fecha_ini").val()=='' || $("#tipo").val()==''){
                alert('Completar todos los campos requeridos');
                return;
            }

            $("#imgLoad").show();
           // var obj = {careas: $('#careas').val(),tipo: $("#tipo").val()};
            var obj = {tipo: $("#tipo").val(),careas: $('#careas').val(),fecha_ini: $('#fecha_ini').val() }
            $.ajax({
                type:'GET',
                url: 'listaActividadesNOFAC',
                data: obj,
                success: function (data) {
                    $("#imgLoad").hide();
                     console.log(data);
                    $('#lista').html(data);
                },
                error: function(data){
                }
            });

        }

    </script>
@stop