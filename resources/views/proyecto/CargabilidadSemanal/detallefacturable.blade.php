<div class="col-sm-12 table-responsive " style="height: 650px;width: 98%;">
    <table class="table table-hover table-bordered " id="actividades" style="width: 120%" >
        <thead>
        <tr class="clsCabereraTabla">

            <th>Codigo</th>
            <th>Cliente</th>
            <th>Unidad</th>
            <th>Codigo</th>
            <th width="50%">Proyecto</th>
            <th>Estado</th>
            <th>Semana</th>
            <th>Total</th>
            @if(isset($empleados_lista))
            @foreach ($empleados_lista as $ep)
                <th>{{  $ep['abreviatura'] }}</th>
            @endforeach
            @endif
        </tr>
        </thead>
        <tbody>
        @if (isset($arrayproyectos))
            @foreach ($arrayproyectos as $py)
                    <tr>
                    <td>{{ $py['cunidadminera'] }}</td>
                        <td>{{ $py['nombrecliente'] }}</td>
                        <td>{{ $py['nombreunidadminera'] }}</td>
                        <td>{{ $py['codigo'] }}</td>
                        <td width="50%">{{ $py['descripcion'] }}</td>
                        <td>{{ $py['estado'] }}</td>
                        <td>{{ $py['semana'] }}</td>
                        <td>{{ $py['sum'] }}</td>
                    @foreach($py['sumatotal_empleado'] as $he )
                            <td>{{ $he['sumempleado'] }}</td>
                        @endforeach
                    </tr>
                {{--@endif--}}
            @endforeach

        @endif
        </tbody>
    </table>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
{{--<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>--}}
<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>

<script>

/*    $('#actividades').dataTable( {
        searching: true,
        scrollY: 600,
        scrollX: 400,
        scrollCollapse: true,
        scrollXInner: '100%',
        fixedColumns:   {
            leftColumns: 6,
            //rightColumns: 1
        },
        language: {
            url: "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
        },

    } );*/

$("#actividades").tableHeadFixer(
    {'left' : 6},

);
</script>
