<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Proyecto
        <small>Pre-Proyectos</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Proyecto</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


        {!! Form::open(array('url' => 'grabarPreproyecto','method' => 'POST','id' =>'frmpropuesta','style'=>'font-size:12px')) !!}
        {!! Form::hidden('cpropuesta',(isset($propuesta)==1?$propuesta->cpropuesta:'')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('name_cliente','',array('id'=>'name_cliente')) !!}
        {!! Form::hidden('name_uminera','',array('id'=>'name_uminera')) !!}

    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
        	<div class="row ">
                <div class="col-lg-1 col-xs-12 ">Cliente:</div>
                <div class="col-lg-2 col-xs-12 clsPadding">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
                    {!! Form::hidden('cpersona',(isset($persona)==1?$persona->cpersona:''),array('id'=>'idcpersona')) !!}
                    {!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}
                </div>

                <div class="col-lg-1 col-xs-12">Unidad minera:</div>
                <div class="col-lg-2 col-xs-12 clsPadding">                
                  
            		{!! Form::hidden('cunidadminera',(isset($uminera->cunidadminera)==1?$uminera->cunidadminera:''),array('id'=>'iduminera')) !!}

                    {!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}
                </div>


                <div class="col-lg-1 col-xs-12">Código:</div>
                <div class="col-lg-2 col-xs-12 clsPadding">                
                   {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)==1?$propuesta->ccodigo:(isset($codigoProp)==1?$codigoProp:null ) ),array('class'=>'form-control input-sm','disabled'=>'true')) !!}   
                </div>
               
                <div class="col-lg-1 col-xs-12 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target="#searchCliente"><b>...</b></button></div>

            </div>

            <div class="row clsPadding2">
            <div class="col-lg-2 col-xs-12 clsPadding">&nbsp;</div>
            <div class="col-lg-2 col-xs-12 clsPadding">&nbsp;</div>          
            
            </div>
              
            <div class="row clsPadding">
            <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
            </div>

            <div class="row clsPadding2">
                <div class="col-lg-1 col-xs-12">Código  de la propuesta:</div>

                <div class="col-lg-1 col-xs-12">
            		<!--{!! Form::text('aniocod',(isset($anio)==1?$anio:''),array('class' => 'form-control input-sm')) !!}   -->

                    <select name="aniocod" id="aniocod" class="form-control select-box" onchange="generarcodigoProp()">
                        <?php
                            $anio=date("Y");
                            for($t=$anio ; $t>=2010;$t--){
                            $selected="";
                            $selected = ($t==$aniocod?"selected":"");

                        ?>
                        <option value="{{ $t }}" {{ $selected }}>{{ $t }}</option>
                        <?php
                            }
                        ?>
                        </select>

                </div>


                <div class="col-lg-1 col-xs-12">
                    {!! Form::select('sedecod',(isset($tsedes)==1?$tsedes:array()),(isset($codsede)==1?$codsede:''),array('class' => 'form-control select-box','id' => 'sedecod','onchange' => 'generarcodigoProp()')) !!}     
                </div>

                <div class="col-lg-1 col-xs-12">
                    <!--{!! Form::text('corcod',(isset($correlativo)==1?$correlativo:''),array('class' => 'form-control input-sm','id' => 'corcod','onblur' => 'generarcodigoProp()','placeholder' => 'Número','onkeypress'=>'return soloNumeros(event,this)','maxlength'=>'4')) !!}   -->

                    {!! Form::text('corcod',(isset($correlativo)==1?$correlativo:''),array('class' => 'form-control input-sm','id' => 'corcod','onKeyUp' => 'generarcodigoProp()','placeholder' => 'Número')) !!} 
                </div>

                <div class="col-lg-2 col-xs-12">
                    {!! Form::text('codigoprop',(isset($propuesta->ccodigo)==1?$propuesta->ccodigo:''),array('class' => 'form-control input-sm','readonly'=>'true','id' => 'codigoprop','placeholder' => 'Código de propuesta')) !!}   
                </div>
                   
            </div>
  

            <div class="row clsPadding2">
                <div class="col-lg-1 col-xs-12">Nombre de la propuesta:</div>
                <div class="col-lg-9 col-xs-12">
                    {!! Form::text('propuestanombre',(isset($propuesta->nombre)==1?$propuesta->nombre:''),array('class' => 'form-control input-sm', 'placeholder' => 'Nombre de la Propuesta','id' => 'propuestanombre')) !!}  

                    
                </div>
                   
            </div>

            <div class="row clsPadding2">
                <div class="row ">
                    <div class="col-lg-6 col-xs-12">
                        
                        <div class="col-lg-2 col-xs-12">Descripción de la propuesta:</div>
                        <div class="col-lg-10 col-xs-12">
                            {!! Form::textarea('propuestadescripcion',(isset($propuesta->descripcion)==1?$propuesta->descripcion:''), array('class' => 'form-control', 'rows'=> '10','placeholder' => 'Descripción de la Propuesta')) !!}  
                        </div> 
                        
                    </div>

                    <div class="col-lg-6 col-xs-12 ">
                        <div class="row clsPadding">
                            
                            <div class="col-lg-2 col-xs-12">Moneda:</div>
                            <div class="col-lg-4 col-xs-12">
                              {!! Form::select('moneda',(isset($monedas)==1?$monedas:null),(isset($propuesta->cmoneda)==1?$propuesta->cmoneda:''),array('class' => 'form-control','id'=>'moneda')) !!} 
                            </div>

                            <div class="col-lg-2 col-xs-12">Tipo de gestión de proyectos:</div>
                            <div class="col-lg-4 col-xs-12">
                                {!! Form::select('tipgestionproy',(isset($tipogestionproyecto)==1?$tipogestionproyecto:array()),(isset($propuesta->ctipogestionproyecto)==1?$propuesta->ctipogestionproyecto:''),array('class' => 'form-control','id'=>'tipgestionproy')) !!}    
                            </div>

                          
                            
                        </div>
                

                        <div class="row clsPadding">
                            
                          

                            <div class="col-lg-2 col-xs-12">Portafolio de servicios:</div>
                            <div class="col-lg-4 col-xs-12">
                                {!! Form::select('servicio',(isset($servicio)==1?$servicio:array()),(isset($propuesta->cservicio)==1?$propuesta->cservicio:''),array('class' => 'form-control','id'=>'servicio')) !!}    
                            </div>

                            <div class="col-lg-2 col-xs-12">Tipo de Proyecto:</div>
                            <div class="col-lg-4 col-xs-12">
                                {!! Form::select('tipoproyecto',(isset($tipoproyecto)==1?$tipoproyecto:array()),(isset($propuesta->ctipoproyecto)==1?$propuesta->ctipoproyecto:''),array('class' => 'form-control','id'=>'tipoproyecto')) !!}    
                            </div>
                            
                        </div>
                       
                        <div class="row clsPadding">
                            <div class="col-lg-2 col-xs-2">Documento de Aprobación:</div>
                            <div class="col-lg-4 col-xs-10">
                               {!! Form::select('docaproba',(isset($tdocu)==1?$tdocu:array()),(isset($proy_adicional->cdocaprobacion)==1?$proy_adicional->cdocaprobacion:null), array('id'=>'docaproba','class'=>'form-control') ) !!}
                            </div>

                            <div class="col-lg-2 col-xs-12">Número de Documento:</div>
                            <div class="col-lg-4 col-xs-10">
                                {!! Form::text('nrodocaproba',(isset($proy_adicional->nrodocaprobacion)==1?$proy_adicional->nrodocaprobacion:''),array('id'=>'nrodocaproba','class'=>'form-control') ) !!}
                            </div>                             
                            
                        </div>

                        <div class="row clsPadding">
                            <div class="col-lg-2 col-xs-12">Coordinador de Propuesta:</div>
                            <div class="col-lg-4 col-xs-12">
                              {!! Form::select('coorpropuesta',(isset($personal_cp)==1?$personal_cp:array()),(isset($propuesta->cpersona_coordinadorpropuesta)==1?$propuesta->cpersona_coordinadorpropuesta:''),array('class' => 'form-control select-box','id'=>'coorpropuesta')) !!}        
                            </div>

                            <div class="col-lg-2 col-xs-12">Gerente de proyecto:</div>
                            <div class="col-lg-4 col-xs-12">
                              {!! Form::select('gtepropuesta',(isset($personal_gp)==1?$personal_gp:array()),(isset($propuesta->cpersona_gteproyecto)==1?$propuesta->cpersona_gteproyecto:''),array('class' => 'form-control select-box','id'=>'gtepropuesta')) !!}       
                            
                            </div>                            
                        </div>
                    </div>   
                </div>    
            </div>

            <div class="row clsPadding2">
            	
                    
                               
                    <!--

            		<div class="col-lg-2 col-xs-12">
                        Medio de entrega:    
                        <div class="radio">
                        @if (isset($medioentrega))
                            @foreach($medioentrega as $medio)
                                {!! Form::radio('mentrega',$medio->cmedioentrega,(isset($propuesta->cmedioentrega)==1?($medio->cmedioentrega == $propuesta->cmedioentrega):false)) !!}
                                {{ $medio->descripcion }}
                                <br>
                            @endforeach     
                        @endif
                        </div>        
                    </div>  
                    
            		<div class="col-lg-4 col-xs-12">
                     Detalle de Propuesta:
                    @if (isset($tipopropresentacion))
                        @foreach($tipopropresentacion as $tipo)
                            <?php $activado=false; ?> 
                            <div class="checkbox">
                                @if (isset($propresentacion))
                                    @foreach($propresentacion as $p)
                                        @if($tipo->cpresentacion == $p->cpresentacion)
                                            <?php $activado=true; ?>
                                        @endif
                                    @endforeach
                                @endif
                                {!! Form::checkbox('presentacion[]',$tipo->cpresentacion,$activado) !!}
                                
                                
                                {{ $tipo->descripcion }}
                            </div>
                        @endforeach        
                    @endif
                    </div>      

                    -->                  

            </div> 
            <div class="row clsPadding">
                <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
            </div>



            <div class="row clsPadding2">
                <div class="col-lg-4 col-xs-4">
                    <button class="btn btn-primary btn-block" style="width:200px;" @if(isset($bsave))
                        @if($editar)
                        @else
                            disabled   
                        @endif
                        @else 
                            disabled
                        @endif 
                        id='btnSave'><b>Grabar</b></button>                    
                </div>
                <div class="col-lg-4 col-xs-4">
                    &nbsp;
                </div>    
            </div>
    
        </div>
            <!--<div class="col-lg-1 hidden-xs"></div>-->
 




        {!! Form::close() !!}


    </section>
    <!-- /.content -->

    <!-- Modal Buscar Cliente-->
    <div id="searchCliente" class="modal fade" role="dialog">
      <div class="modal-dialog" >

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Cliente</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
                 <table id="table_id" class="table table-bordered table-hover" style="width: 100%">
                        <thead>
                        <tr>
                            <th>Código</th>
                            <th>Cliente</th>
                            <th>Unidad Minera</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Código</th>
                            <th>Cliente</th>
                            <th>Unidad Minera</th>
                        </tr>
                        </tfoot>
                    </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" id="btn_aceptar">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal -->

  </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
    
<script>
        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';  
        var table=$("#table_id").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "buscarClientes",
                "type": "GET",
            },
            "columns":[
                {data: "codigosig" , name : "tuni.codigo"},
                {data: "nombre" , name : "tpersona.nombre"},
                {data: "uminera", name : "tuni.nombre"}

            ],
            "rowCallback" : function(row, data ){
              if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

                  $(row).addClass('selected');
              }                    
            },
          "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#table_id tbody').on( 'click', 'tr', function () {

            var id = this.id;
            var index = $.inArray(id, selected);
            $('#' + this.id).each(function(indice, elemento) {
                //console.log('El elemento con el índice '+indice+' contiene '+$(elemento).html());
                $.each(elemento.cells, function (name , valor) { 
                        //console.log(name + '_' + valor.innerHTML);
                        if(name==1){
                            document.getElementById('name_cliente').value=valor.innerHTML ;
                        }else if(name==2){
                            document.getElementById('name_uminera').value=valor.innerHTML ;
                        }
                });
            });
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        });
        var cli=0;
        var umin=0;
        $('#btn_aceptar').on('click',function (){
            if(selected.length>0){
                temp= selected[0].substring(4);
                cli = temp.substring(0,temp.lastIndexOf('_'));
                umin = temp.substring(temp.lastIndexOf('_')+1);
                document.getElementById('idcpersona').value=cli;
                document.getElementById('iduminera').value=umin;
                document.getElementById('personanombre').value = document.getElementById('name_cliente').value;
                document.getElementById('umineranombre').value = document.getElementById('name_uminera').value;
                $('#btnSave').prop('disabled',false);
                $('#searchCliente').modal('hide');
            }else{
                alert('Seleccione un Cliente');
            }
        });



        $('#frmpropuesta').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');   


            if($("#propuestanombre").val()==''){
                alert('Ingrese el nombre de la propuesta');
                return;
            }  



            if($("#moneda").val()==''){
                
                alert('Seleccione un tipo de moneda');
                return;
            }  

            if($("#servicio").val()==''){
                alert('Seleccione un servicio');
                return;
            }              

            if($("#coorpropuesta").val()==''){
                alert('Seleccione un coordinador');
                return;
            }          

            if($("#gtepropuesta").val()==''){
                alert('Seleccione un gerente de proyecto');
                return;
            }    


            $.ajax({

                type:"POST",
                url:'grabarPreproyecto',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                     getUrl('preproyecto','')
                },
                error: function(data){
                    
                    
                    $.each(data.responseJSON, function (key, value) {
                            var input = '#frmpropuesta input[name=' + key + ']';
                            $(input + '+span>strong').text(value);
                            $(input).parent().parent().addClass('has-error');

                    });
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });

        function generarcodigoProp(){
            //alert(substring($('#aniocod').val(),2,2));

            var codigo=$('#aniocod').val().substring(2)+'.'+$('#sedecod').val()+'.'+$('#corcod').val();


            $('#codigoprop').val(codigo);
        }

        function soloNumeros(e,obj) {

            // Backspace = 8, Enter = 13, ’0′ = 48, ’9′ = 57, ‘.’ = 46
            
            var field = obj;

            key = e.keyCode ? e.keyCode : e.which;
            
            if (key == 8 || key ==9 || key ==11 || (key > 47 && key < 58)) return true;
            if (key > 47 && key < 58) {
              if (field.value === "") return true;
              var existePto = (/[.]/).test(field.value);
              
              if (existePto === false){
                  regexp = /.[0-9]{2}$/; //PARTE ENTERA 10
              }
              else {
                regexp = /.[0-9]{2}$/; //PARTE DECIMAL2
              }
              return !(regexp.test(field.value));
            }
            if (key == 46) {
              if (field.value === "") return false;
              regexp = /^[0-9]+$/;
              return regexp.test(field.value);
            }

            return false;
    }


    $("#servicio").change(function(){

        if(this.value==''){
                return;
        }

        $.ajax({
            type:'GET',
            url: 'listTipoPry/'+this.value,
            success: function (data) {
                var str = JSON.stringify(data);
                var pushedData = jQuery.parseJSON(str);              
                
                $('#tipoproyecto').find('option').remove();
                $('#tipoproyecto').append($('<option></option>').attr('value','').text(''));
                
                $.each(pushedData, function(i, serverData)
                {
                    $('#tipoproyecto').append($('<option></option>').attr('value',i).text(serverData));
                });
            },
            error: function(data){

            }

        });


    });

        
</script>

<!--Inicion del Script para funcionamiento de Chosen -->
<script>
    $('.select-box').chosen(
        {
            allow_single_deselect: true,width:"100%"
        });
</script>
<!-- Fin del Script para funcionamiento de Chosen -->