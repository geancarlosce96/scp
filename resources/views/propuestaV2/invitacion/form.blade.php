@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
  <h1>
    Registro de Invitación
  </h1>
</section>

<section class="content" style="font-size: 12px" style="text-align: center;">

	<div class="col-md-6">            
  <form class="form-horizontal" action="{{ url('invitacion') }}" method="post" >

              {{ csrf_field() }}
              
            <div class="box-body">

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Cliente</label>

                  <div class="col-sm-7">
                    <select class="form-control" name="cliente_id" >
                      <option value="0">Elegir</option>                      
                      @foreach($clientes as $cliente)
                    	<option value="{{ $cliente->cpersona }}" >{{ $cliente->nombre }}</option>
                    	@endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Propuesta</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="propuesta" required >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Nombres</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="nombres" required >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Apellidos</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="apellidos" required >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Cargo</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="cargo" required >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Numero</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="numero" required >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Correo</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="correo" required >
                  </div>
                </div>

                <div class="form-group">
                	<div class="col-sm-9 col-sm-offset-3">
                		<button class="btn btn-primary" type="submit" >registrar</button>
                	</div>
                </div>

			</div>

  </form>
</div>

</section>

</div>

@stop
