@extends('layouts.master')
@section('content')

<link rel="stylesheet" type="text/css" href="https://docs.handsontable.com/pro/bower_components/handsontable-pro/dist/handsontable.full.min.css">
<script src="https://docs.handsontable.com/pro/bower_components/handsontable-pro/dist/handsontable.full.min.js"></script>

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
      <h1>
        Listado de Actividades
      </h1>
</section>

    <!-- Main content -->
<section class="content">          

  <div class="col-md-6">  

<div id="hot"></div>
<script>var dataObject = [
   {
    id: '1,00',
    flag: 'Actividad 1: Gerencia y control del proyecto',
	},
  {
    id: '1,01',
    flag: 'Gerencia de Proyecto',
	},
  {
    id: '1,02',
    flag: 'Coordinaciones y reuniones',
	},
  {
    id: '1,03',
    flag: 'Reportes diarios e Informes semanales y mensuales',
	},
{
    id: '2,00',
    flag: 'Actividad 2: Etapa de implementación',
	},
	{
    id: '2,01',
    flag: 'Aprobación del check list de pre-inicio del proyecto',
	},
  {},
  {},
  {},
  {},
  {},
  {}
];
var currencyCodes = ['EUR', 'JPY', 'GBP', 'CHF', 'CAD', 'AUD', 'NZD', 'SEK', 'NOK', 'BRL', 'CNY', 'RUB', 'INR', 'TRY', 'THB', 'IDR', 'MYR', 'MXN', 'ARS', 'DKK', 'ILS', 'PHP'];
var flagRenderer = function (instance, td, row, col, prop, value, cellProperties) {
  var currencyCode = value;
  while (td.firstChild) {
    td.removeChild(td.firstChild);
  }
  if (currencyCodes.indexOf(currencyCode) > -1) {
    var flagElement = document.createElement('DIV');
    flagElement.className = 'flag ' + currencyCode.toLowerCase();
    td.appendChild(flagElement);
  } else {
    var textNode = document.createTextNode(value === null ? '' : value);

    td.appendChild(textNode);
  }
};
var hotElement = document.querySelector('#hot');
var hotElementContainer = hotElement.parentNode;
var hotSettings = {
  data: dataObject,
  columns: [
    {
      data: 'id',
      type: 'text'
    },
    {
      data: 'flag',
      type: 'text'
    },
    {
      data: 'currencyCode',
      type: 'text'
    },
    {
      data: 'currency',
      type: 'text'
    },
    {
      data: 'level',
      type: 'text'
    },
    {
      data: 'units',
      type: 'text'
    },
    {
      data: 'asOf',
      type: 'text'      
    },
    {
      data: 'onedChng',
      type: 'text'
    }
  ],
  stretchH: 'all',
  width: 806,
  autoWrapRow: true,
  height: 487,
  maxRows: 22,
  manualRowResize: true,
  manualColumnResize: true,
  rowHeaders: true,
  colHeaders: [
    'Item',
    'Descripcion de Actividad',
    'Categoria',
    'Revisor',
    'Gerente',
    'Civil',
    'Geotecnio',
    'Geologo'
  ],
  manualRowMove: true,
  manualColumnMove: true,
  contextMenu: true,
  filters: true,
  dropdownMenu: true
};
var hot = new Handsontable(hotElement, hotSettings);
</script>


  </div>

</section>

</div>

<script type="text/javascript">
	
$(document).ready(function(){
	$("#hot-display-license-info").hide();
});

</script>

@stop
