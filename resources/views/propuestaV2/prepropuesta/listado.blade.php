@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
      <h1>
        Listado de Prepropuestas
      </h1>
</section>

    <!-- Main content -->
<section class="content">          

	<div class="col-md-6">  
  
    <table class="table table-striped table-hover" >
      <tr>
        <th>#</th>
        <th>Propuesta</th>        
        <th>Cliente</th>
        <th>F. Registro</th>
        <th>Estado</th>
        <th>Acciones</th>
      </tr>
      
      @foreach ($lista as $index => $element)
      <tr>      
        <td>{{ $element->n }} </td>        
        <td>{{ $element->propuesta }} </td>
        <td>@if($element->cliente!=NULL) {{ $element->cliente->nombre }} @endif</td>        
        <td>{{ $element->created_at }} </td>
        <td>{{ $element->estado_toString() }} </td>
        <td>
           <a href="{{ url('/') }}" class="btn btn-primary" title="" > <i class="fa fa-search"></i></a>
           <a href="{{ url('/') }}" class="btn btn-primary"> <i class="fa fa-edit" title="" ></i></a>           
           <a href="#" class="btn btn-danger"> <i class="fa fa-close" title="Cancelar la capacitación" ></i></a>
        </td>
      </tr>      
      @endforeach

    </table>

  </div>
  </div>

</section>

</div>

@stop
