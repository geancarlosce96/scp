@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
  <h1>
    Registro de Prepropuesta
  </h1>
</section>

<section class="content" style="font-size: 12px" style="text-align: center;">

	<div class="col-md-6">            
  <form class="form-horizontal" action="{{ url('prepropuesta') }}" method="post" >

              {{ csrf_field() }}
              
            <div class="box-body">

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Cliente</label>

                  <div class="col-sm-7">
                    <select class="form-control" name="cliente_id" >
                    	<option value="0">Elegir</option>                      
                      @foreach($clientes as $cliente)
                    	<option value="{{ $cliente->cpersona }}" >{{ $cliente->nombre }}</option>
                    	@endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Codigo</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="codigo"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Propuesta</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="propuesta"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">CEP</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="cep"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Nombres</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="nombres"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Apellidos</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="apellidos"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Cargo</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="cargo"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Numero</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="numero"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Correo</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" name="correo"  >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Invitacion</label>

                  <div class="col-sm-7">
                    <select class="form-control" name="invitacion_id" id="invitacion_id" >
                      <option value="0">Elegir (Opcional)</option>
                      @foreach($invitaciones as $obj)
                      <option value="{{ $obj->id }}" >{{ $obj->propuesta }}</option>
                      @endforeach
                    </select>
                  </div>
                  
                  <div class="col-sm-2">
                    <button type="button" class="btn btn-default" id="btn-obtenerInvitacion" >obtener</button>
                  </div>
                </div>

                <div class="form-group">
                	<div class="col-sm-9 col-sm-offset-3">
                		<button class="btn btn-primary" type="submit" >registrar</button>
                	</div>
                </div>

			</div>

  </form>
</div>

</section>

</div>

<script>
  var BASE_URL = "<?php echo url('/'); ?>";
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('js/prepropuesta.js') }}"></script>

@stop
