@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Página en Construcción
        <small>Inicio</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">En construcción...</li>
      </ol>
    </section>

        <!-- Main content -->
    <section class="content clsCover">
    <!--
     style=" width:100%; height:100%; background-color:#F00;  background-image:url(img/bkgAndes.jpg); background-repeat:no-repeat;">
     
     -->
      <div class="error-page">
        

        <div class="error-content">
        <h2 class="text-yellow"><i class="fa fa-cogs text-yellow"></i> En Construcción</h2><br>
          <p>Esta página se encuentra en construcción. Visítenos en breve.</p>




        </div>
        <!-- /.error-content -->
      </div>
      <!-- /.error-page -->
    </section>
    <!-- /.content -->
  </div>
@stop