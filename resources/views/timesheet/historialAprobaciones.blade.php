<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hoja de Tiempo
        <small>Historial de Aprobaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Hoja de Tiempo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">
    
   <div class="row clsPadding2">
    <div class="col-lg-1 col-xs-1 clsPadding">Proyecto:</div>
    <div class="col-lg-9 col-xs-9 clsPadding"><input class="form-control input-sm" type="text" disabled></div>
    <div class="col-lg-2 col-xs-2 clsPadding"><a href="#" class="btn btn-primary btn-block ">
	    <i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Buscar
    </a></div>
            
   </div>
   
   
   
   
   
   
 
 
        <div class="row clsPadding2">	
        <div class="col-lg-12 col-xs-12 table-responsive">
        <table class="table table-striped clsAnchoTabla">
                <thead>
                <tr class="clsCabereraTabla">
                  <th>Semana</th>
                  <th>Proyecto</th>
                  <th>Tarea</th>
                  <th>Estado<br>Solicitud</th>
                  <th>Aprobador</th>                  
                  <th>Cargo</th>                                    
                  <th>Fecha de Envío</th>
                  <th>Fecha de Aprobación</th>
                  <th>Tipo de Hora</th>
                  <th>Sustento</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                  <td class="clsAnchoTabla">27</td>
                  <td class="clsAnchoTabla">Anddes</td>
                  <td class="clsAnchoTabla">x</td>
                  <td class="clsAnchoTabla">Aprobado</td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                </tr>
                <tr>
                  <td class="clsAnchoTabla">27</td>
                  <td class="clsAnchoTabla">Anddes</td>
                  <td class="clsAnchoTabla">w</td>
                  <td class="clsAnchoTabla">Pendiente</td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                </tr>
                <tr>
                  <td class="clsAnchoTabla">27</td>
                  <td class="clsAnchoTabla">Las Bambas</td>
                  <td class="clsAnchoTabla">y</td>
                  <td class="clsAnchoTabla">Observado</td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                  <td class="clsAnchoTabla"></td>
                </tr>                
                                                
                </tbody>
              </table>    
        </div>
        </div>   
    </div>
    
    <div class="col-lg-1 hidden-xs"></div>

</div>

<!--   *******************************************  -->
</div>