    <div class="list-group" style="width: 105%; font-size: 12px;">
        <a href="#" style="width: 100%;" class="list-group-item active" >
            Proyectos
        </a> 
        @if(isset($proyectos))
        <input type="hidden" name="pryTodos" value="" id="cproyecto">
        <div style="width: 100%; height: 500px; overflow-y: auto;">
            @foreach($proyectos as $pry)
            <a href="#" class="list-group-item" onclick="tareasTodos({{ $pry['cproyecto']  }})" id="{{ $pry['cproyecto'] }}">{{ $pry['codigo'] }} {{ $pry['nombre'] }}
                <!-- <input type="radio" name="pryTodos" value="{{ $pry['cproyecto'] }}" onclick="tareasTodos({{ $pry['cproyecto']  }})"/> -->
            </a>            
            @endforeach
        </div>
        @endif

    </div>         
