@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Hoja de Tiempo
            <small>Reporte Cliente</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
            <li class="active">Hoja de Tiempo</li>
        </ol>
    </section>
    <section class="content">
   
        <form class="form-sample" id="frmReporteHT" action="/obtnerdatosht" method="GET" target="_blank">
            <div class="row clsPadding2">
                <div class="col-md-12 col-xs-12">
                    <label> Proyecto: </label>
                    {!! Form::select('proyecto[]',(isset($proyectos)==1?$proyectos:array()),'',array('multiple'=>'multiple','class' => 'form-control select-box','id'=>'proyecto')) !!}  
                </div>
            </div>

            <div class="row clsPadding2">
                
                <div class="col-md-4 col-xs-4">
                    <label> * Profesional: </label>
                    {!! Form::select('persona',(isset($persona)==1?$persona:array()),'',array('class' => 'form-control select-box','id'=>'persona')) !!}  
                </div>
                <div class="col-md-4 col-xs-4">
                    <label> Tipo: </label>
                        <select multiple class="form-control select-box" name="tipoact[]" id="tipoact" >
                            <option selected value="FF">FF</option>
                            <option value="NF">NF</option>
                            <option value="AND">AND</option>
                        </select>
                </div>
                <div class="col-md-4 col-xs-4">
                    <label> Estados: </label>
                    {!! Form::select('condicionoperativa[]',(isset($condicionoperativa)==1?$condicionoperativa:array()),'',array('multiple'=>'multiple','class' => 'form-control select-box','id'=>'condicionoperativa')) !!}  
                </div>
            </div>
            
            <div class="row clsPadding2">

                <div class="col-md-4 col-xs-4">
                        <label> * Fecha inicio: </label>
                    <div class="input-group date">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                        {!! Form::text('fdesde',(isset($fdesde)==1?$fdesde:''), array('class'=>'form-control calendario','id' => 'fdesde','readonly'=>'true')) !!}
                    </div>
                </div>
                <div class="col-md-4 col-xs-4">
                    <label> * Fecha final: </label>         
                    <div class="input-group date">
                        <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </div> 
                        {!! Form::text('fhasta',(isset($fhasta)==1?$fhasta:''), array('class'=>'form-control calendario','id' => 'fhasta','readonly'=>'true')) !!}
                    </div>        
                </div>
                <!--<div class="col-md-2 col-xs-2">
                    <label>Rango de semanas: </label>
                    {!! Form::text('',(isset($rango_semanas)==1?$rango_semanas:''), array('class'=>'form-control','id' => 'rango_semanas','readonly'=>'true')) !!}
                </div> -->
                <div class="col-md-2 col-xs-2">
                    <label> Agrupar Semanas: </label>
                    {!! Form::text('dividir_semanas','3', array('class'=>'form-control','id' => 'dividir_semanas')) !!}
                </div>

                <div class="col-md-2 col-xs-2">
                    <label> Colocar firma: </label>
                    <select class="form-control select-box" name="firma_posicion" id="firma_posicion">
                        <option value="1">Pie de página</option>
                        <option value="2">Final de tabla</option>
                    </select>
                
                </div>
                        

            </div>

            <div class="col-md-12 col-xs-12 ">

                <label id="mensaje_fechas" style="color:red; font-size:12px; display: none;">Seleccione un rango de fechas menor a 6 meses</label>
            </div>
            <br>
            <br>

            <div class="col-md-12 col-xs-12 ">
                <label id="mensaje_fechas" style="color:black;">( * ) Campos obligatorios</label>
            </div>
            
            <div class="row clsPadding2">
                
                <div class="col-md-2 col-xs-2">
                    <label> &nbsp;</label>
                    <button type="submit" class="btn btn-primary btn-block" form="frmReporteHT" id="btnGenerarGenerar" disabled>
                        <b> <i class="glyphicon glyphicon-search"></i> Generar</b>
                    </button>
                    
                </div>
        
                <div class="col-md-1" style="display: none;padding-top:10px;" id="imgLoad" >
                    <label> &nbsp;</label>
                        <img src="{{ asset('img/load.gif') }}" width="25px" >
                </div>

            </div>

            <div class="row clsPadding2" id="prueba">
            </div>

            <div class="row clsPadding2">
                {{-- @include('timesheet.tablareporteht') --}}
            </div>
        </form>
    </section>
</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/javascripts/application.js" type="text/javascript" charset="utf-8" async defer></script>

<script type="text/javascript">

$(document).ready(function(){

    $('.select-box').chosen(
        {
            allow_single_deselect: true,width:"100%"
        });

    // $('#btnGenerar').click(function(){
    //     generarreporte();
    // })

    $('.calendario').datepicker({
        format: "dd-mm-yyyy",
        language: "es",
        autoclose: true,
        firstDay: 1,
        calendarWeeks:true,
        todayHighlight: true,
   
    }).on("input changeDate", function (e) {

        obtenerrangosemanas()
  });

    $('#frmReporteHT').click(function(){
        //alert(234);
    })

    $('#frmReporteHT').on('submit', function(e) {
        // e.preventDefault();
        // generarreporte();
    });

    $('#persona').change(function(){

        $('#btnGenerarGenerar').prop('disabled','');

        if($(this).val()== ""){

            $('#btnGenerarGenerar').prop('disabled',true)
        }
    })

    $('#dividir_semanas').focusout(function(){

        var valor = $('#dividir_semanas').val();
       
        if ($('#dividir_semanas').val() > 3){
            valor = 3
        }
        else if($('#dividir_semanas').val() < 1 || $('#dividir_semanas').val() == ''){
            valor = 1
        }
        $('#dividir_semanas').val(valor)
    })


});

/*function generarreporte(){

        //obj={
        //         var cproyecto = $("#proyecto").val();
        //         var persona  = $("#persona").val();
        //         var tipoact  = $("#tipoact").val();
        //         var fdesde  = $("#fdesde").val();
        //         var fhasta  = $("#fhasta").val();
        //         var condicionoperativa  = $("#condicionoperativa").val();
        //         var dividir_semanas  = $("#dividir_semanas").val();
        //}

        obj={
                cproyecto : $("#proyecto").val(),
                persona  : $("#persona").val(),
                tipoact  : $("#tipoact").val(),
                fdesde  : $("#fdesde").val(),
                fhasta  : $("#fhasta").val(),
                condicionoperativa  : $("#condicionoperativa").val(),
                dividir_semanas  : $("#dividir_semanas").val(),
        }

     


        // window.open('obtnerdatosht/'+cproyecto+'/'+persona+'/'+tipoact+'/'+fdesde+'/'+fhasta+'/'+condicionoperativa+'/'+dividir_semanas)

        var url = BASE_URL + '/obtnerdatosht';

        $("#imgLoad").show();

        $.get(url,obj)
            .done(function( data ) {  
                $("#imgLoad").hide();  
                console.log(data);

                //printht(data);

           // window.open('printReporteHT/'+data[0]+'/'+data[1],'_blank'); 
               
        });
}
*/
function printht(vista){

   // window.open('printReporteHT/'+vista,'_blank');


    var url = BASE_URL + '/printReporteHT';

        $("#imgLoad").show();

        $.get(url,vista)
            .done(function( data ) {  
                
                //location.reload();
                $("#imgLoad").hide();  
        });
}

function obtenerrangosemanas(){

    $.ajax({
        type: "GET",
        url: "/obtenerrangosemanas",
        data: {
            fdesde : $("#fdesde").val(),
            fhasta : $("#fhasta").val(),
        },
        success: function (data) {

            $('#mensaje_fechas').hide()
            $('#btnGenerarGenerar').prop('disabled','');

            if (data >= 6){
              
                $('#mensaje_fechas').show()
                $('#btnGenerarGenerar').prop('disabled',true)
            }
            
        }
    });
}

</script>

@stop