
@extends('layouts.master')

@section('content')



<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Control de Proyectos | Anddes</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/css/font-awesome.min.css">
  
  
<!-- Ionicons -->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->
  <link rel="stylesheet" href="dist/css/ionicons.min.css">  
  <!-- Theme style -->
  <!--<link rel="stylesheet" href="dist/css/AdminLTE.min.css">-->
  <link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- DataTable-->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="plugins/datatables/jquery.dataTables.min.css">

  <link rel="stylesheet" href="dist/css/bootstrap-treeview.min.css" >

  <link rel="stylesheet" type="text/css" href="dist/css/highCheckTree.css"/>
  <link rel="stylesheet" type="text/css" href="dist/css/jquery-ui.css"/>

  <!-- plugin chosen CSS-->
  <link rel="stylesheet" type="text/css" href="dist/css/chosen/chosen.min.css"/>


  



        
        

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    #div_carga{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: url(img/gris.png) repeat;
      opacity: 0.5;
      display:none;
      z-index:810;
    }
    #div_msg{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      
      background-color:rgba(255,255,255,0.6);
      
      display:none;
      z-index:1500;
    }
    #div_contenido{
      opacity: 1;
    }
    #div_msg_error{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background-color:rgba(255,255,255,0.6);
      

      display:none;
      z-index:1500;
    }    
    #cargador{
        position:absolute;
        top:50%;
        left: 50%;
        margin-top: -25px;
        margin-left: -25px;
    }
    .success_editar, .success_eliminar, .success{background: #0000ff;}
    .error{background: #a41b22;}
    .success, .success_editar, .success_eliminar, .error{ width: 100%; padding: 20px 20px; color: #ffffff; font-size: 16px; font-weight: bold; position: fixed; top: 0; left: 0;}
    .success i, .success_editar i,.success_eliminar i, .error i{font-size: 20px;}

    .custom-combobox {
      position: relative;
      display: inline-block;
    }
    .custom-combobox-toggle {
      position: absolute;
      top: 0;
      bottom: 0;
      margin-left: -1px;
      padding: 0;
    }
    .custom-combobox-input {
      margin: 0;
      padding: 5px 10px;
    }


    #resultado {
      height : 1024px;
      overflow-y: auto;
      
    }

  </style>
</head>

<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
<script >


	function welcome(tok){
 
     var f = new Date();
         
         dia1=f.getDate();
         mes1=(f.getMonth() + 1);
         ano=f.getFullYear();
         
         if (dia1<10) {
           dia="0" +dia1
         }
         else{
          dia=dia1
         }

         if (mes1<10) {
           mes="0" +mes1
         }
         else{
          mes=mes1
         }    

     var fecha=dia + "/" + mes + "/" + ano;
		//document.write(fecha);

		//alert(data);
        $.ajax({

              type:"post",
              url:'editarTodosRegistrodeHoras',
              data:{
		            fecha: fecha,
		            "_token": tok,
            	   },
              beforeSend: function () {
                  $('#div_carga').show(); 
          
              },
              success: function(data){
                    $('#div_carga').hide(); 
                    $("#resultado").html(data);


                    //$("#div_msg").show();
              },
              error: function(data){
                  $('#div_carga').hide();
                  //$('#detalle_error').html(data);
                  //$("#div_msg_error").show();
              }
          });    
       

        }

    welcome('{{ csrf_token() }}');





</script>

</html>