
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hoja de Tiempo -
        <small>Aprobaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Hoja de tiempo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => 'grabarRegistroHoras','method' => 'POST','id' =>'frmHoras')) !!}
        
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('obs_txt',csrf_token(),array('id'=>'obs_txt')) !!}
        <input type="hidden" name="valor" value="{{ isset($valor)==1?$valor:'' }}" id="valor">


        <section class="panel box box-primary" >


            <div class="col-md-3 col-xs-3" style="padding-top: 5px;">
                <div class="col-lg-2 col-xs-2 clsPadding">Fecha inicio</div>            
                <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                    {!! Form::text('fdesde','', array('class'=>'form-control pull-right ','id' => 'fdesde','readonly'=>'true')) !!}
                </div>
            </div>
            <div class="col-md-3 col-xs-3" style="padding-top: 5px;">
                <div class="col-lg-2 col-xs-2 clsPadding">Fecha final</div>           
                <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                    {!! Form::text('fhasta','', array('class'=>'form-control pull-right ','id' => 'fhasta','readonly'=>'true')) !!}
                </div>        
            </div>

            <div class="col-lg-1"  style="padding-top: 5px;">
                <button type="button" class="btn btn-primary" style="display: block;" onclick="verHorasAprobar();" >
                    <i class="fa fa-search" aria-hidden="true" ></i> Visualizar
                </button>
            </div>

            @if(isset($valor))
                @if($valor=='aprobar')
                    <div class="col-lg-1"  style="padding-top: 5px;">
                        <button type="button" class="btn btn-primary" style="display: block;" onclick="verTodasHorasAprobar();" >
                            <i class="fa fa-search" aria-hidden="true" ></i> Todas las semanas
                        </button>
                    </div>
                @else
                <div class="col-lg-1"  style="padding-top: 5px;"></div>

                @endif
            @endif
            
            <div class="col-md-3 col-xs-3"  style="padding-top: 5px;">
                <div class="list-group active pull-right" style="font-size:12px">
                    <button type="button" class="btn btn-primary pull-right" id="btnObs"> <b>Observar seleccionadas</b></button>
                    &nbsp;
                </div>

                @if(isset($valor))
                    @if($valor=='aprobar')
                    <div class="list-group active pull-right" style="font-size:12px">
                        &nbsp;
                        <button type="button" class="btn btn-primary pull-left" id="btnAprobar"> <b>Aprobar seleccionadas</b></button>
                    </div>
                
                    @endif
                @endif


            </div>

            <div class="row ">    
                <div class="col-lg-12 col-xs-12 table-responsive" id="divAprobar">
                    @include('partials.tableAprobacionHoras',array('listAprobar'=>(isset($listAprobar)==1?$listAprobar:array()) ) )      
                </div>
            </div>

            <div class="col-lg-2"  style="padding-top: 5px;">
                <!-- <button type="button" class="btn btn-primary" style="display: block;" onclick="verHorasAprobar();" >
                    <i class="fa fa-search" aria-hidden="true"></i> Visualizar
                </button> -->
            </div>
            <div class="col-lg-5" style="padding-top: 0px;">
            </div>
            <div class="col-lg-1 pull-left" style="padding-top: 0px;">
            </div>
            <div class="col-md-3 col-xs-3"  style="padding-top: 5px;">
                <div class="list-group active pull-right" style="font-size:12px">
                    <button type="button" class="btn btn-primary pull-right" id="btnObs2"> <b>Observar seleccionadas</b></button>
                    &nbsp;
                </div>

                @if(isset($valor))
                    @if($valor=='aprobar')
                    <div class="list-group active pull-right" style="font-size:12px">
                        &nbsp;
                        <button type="button" class="btn btn-primary pull-left" id="btnAprobar2"> <b>Aprobar seleccionadas</b></button>
                    </div>
                
                    @endif
                @endif


            </div>
        </section>

        <!-- <div class="row"> -->
            <!--<div class="col-lg-1 hidden-xs"></div>-->
            <!-- <div class="col-md-12 col-xs-12">
        
                <div class="row">
                    <div class="col-md-1 col-xs-1"></div>
                    <div class="col-md-10 col-xs-10 ">
                    </div>
                    <div class="col-md-2 col-xs-1 ">
                            <button type="button" class="btn btn-primary" style="display: block;" onclick="verHorasAprobar();" >
                            <i class="fa fa-search" aria-hidden="true"></i> Visualizar
                            </button>
                    </div> 
                     

                </div>
                    <div class="row"> <br> </div>

                <div class="row ">	
                    <div class="col-lg-12 col-xs-12 table-responsive" id="divAprobar">
                        Aqui va el codigo de la tabla
                    </div>
                </div>   

                <div class="row ">
                    <div class="col-lg-3 col-xs-3">
                        <button type="button" class="btn btn-primary btn-block " id="btnAprobar"> <b>Aprobar seleccionadas</b></button>
                    </div>
                    <div class="col-lg-3 col-xs-3">
                        <button type="button" class="btn btn-primary btn-block " id="btnObs"> <b>Observar seleccionadas</b></button>    
                    </div>
                
                </div>
            </div> -->
        
            <!--<div class="col-lg-1 hidden-xs"></div>-->

        <!-- </div> -->
        {!! Form::close() !!}
    </section>
    <div id="modalHoras">
        @include('partials.modalVerHoras')
    </div>
<!--   *******************************************  -->

    <!-- ********************Observaciones ******************************* -->
      <div class="modal fade" id="modalObs" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
              <h4 class="modal-title" id="ObsModalLabel" style="color: white">Observaciones</h4>
            </div>
            <div class="modal-body">
              <form>
                           
                <div class="form-group">
                  <label for="message-text" class="control-label">Ingrese comentario aprobador:</label>
                  <textarea class="form-control" id="message-text"></textarea>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="saveObs()">Aceptar</button>
              <button type="button" class="btn btn-primary" onclick="cerrar_modal()">Cancelar</button>
            </div>
          </div>
        </div>
      </div>


    <!-- *************************************************** -->



    <!-- ********************Observaciones2 ******************************* -->
      <div class="modal fade" id="modalObsDeta" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabelDeta" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
              <h4 class="modal-title" id="ObsModalLabelDeta" style="color: white">Comentarios</h4>
            </div>
            <div class="modal-body">
              <form>
                <label  class="control-label">
                Observación de planificación:
                </label>
                <div id="obs_planificada">

                </div>
                <label  class="control-label">
                Observación de ejecución:
                </label>
                <div id="obs_ejecucion">

                </div>   
                <label  class="control-label">
                Comentarios de aprobación u observación anteriores:
                </label>
                <div id="obs_aprobacion">

                </div>                              
                <div class="form-group">
                  <label for="message-textDeta" class="control-label">Ingrese comentario aprobador:</label>
                  <textarea class="form-control" id="message-textDeta"></textarea>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary warning" onclick="saveObsDeta()">Aceptar</button>
              <button type="button" class="btn btn-primary" onclick="cerrar_modal()">Cancelar</button>
            </div>
          </div>
        </div>
      </div>


    <!-- *************************************************** -->    
</div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>


        var id_ejecu='';
        var fec_inicial='';
        var fec_final='';



        function verHorasAprobar(){

            if ( $("#fhasta").val()=='' || $("#fdesde").val()==''){
                alert('Especifique rango de fechas');
                return;
            }
            else{
                var fechai=$("#fdesde").val();
                var fechaf=$("#fhasta").val();
                var fi_string=fechai.substr(3,2)+'/'+fechai.substr(0,2)+'/'+fechai.substr(6,4);
                var ff_string=fechaf.substr(3,2)+'/'+fechaf.substr(0,2)+'/'+fechaf.substr(6,4);
                var fec_has= new Date(fi_string);
                var fec_des= new Date(ff_string);
                var anio_has=fec_has.getFullYear(); 
                var anio_des=fec_des.getFullYear(); 

                if(fec_has>fec_des){
                    alert('Verifique que la fecha inicio sea menor a la fecha final');
                    return;
                }

                if(anio_has!=anio_des){
                    //alert('Ingrese fechas que correspondan al del mismo año');
                    //return;
                }              

            }

           
            var fec_ini=$("#fdesde").val();
            var fec_fin=$("#fhasta").val(); 
            var valor= $("#valor").val(); 

            if (valor=='historial') {

                url_view ='viewRechazarHoras';
            }

            if (valor=='aprobar') {

                url_view ='verHorasAprobaciones';
            }


          
            $.ajax({
                url: url_view,
                type: 'GET',
                data:  {

                    "fdesde" : $("#fdesde").val(),
                    "fhasta" : $("#fhasta").val(),    
                    "valor"  : $("#valor").val(),            
                },
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){

                    if(data=='TotSemanaInvalida'){
                        $('#div_carga').hide(); 
                        alert('Seleccione un rango de fechas menor a 2 semanas')
                        return;
                    }

                    else{

                        $("#resultado").html(data)  
                                        
                        $('#div_carga').hide(); 

                        fec_inicial=fec_ini;
                        fec_final=fec_fin;

                        $("#fdesde").val(fec_ini);
                        $("#fhasta").val(fec_fin); 
                        $("#valor").val(valor); 

                    }               

                   
                   ; 
                },
            });
        }


        function verTodasHorasAprobar(){            
            var valor= $("#valor").val(); 
        
          
            $.ajax({
                url: 'verTodasAprobacionHoras',
                type: 'GET',
                data:{  "valor" : $("#valor").val(),

                },
              
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){

                    $('#div_carga').hide();

                    if(data=='TotSemanaInvalida'){
                        $('#div_carga').hide(); 
                        alert('Seleccione un rango de fechas menor a 2 semanas')
                        return;
                    }

                    else{

                        $("#resultado").html(data)                   
                        $('#div_carga').hide(); 

                        fec_inicial=fec_ini;
                        fec_final=fec_fin;

                        $("#fdesde").val(fec_ini);
                        $("#fhasta").val(fec_fin); 
                        $("#valor").val(valor); 

                    }               

                   
                   ; 
                },
            });
        }

        $('#frmHoras').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);



            $.ajax({

                type:"POST",
                url:'grabarRegistroHoras',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        }); 
        function verHoras(semana_ini,semana_fin,anio,cpersona){


            var fecha_inicio=$("#fdesde").val();
            var fecha_fin=$("#fhasta").val(); 
            var valor= $("#valor").val(); 

            if (valor=='historial') {
                url_view ='verDetalleHorasRechazar';
            }

            if (valor=='aprobar') {
                url_view ='verDetalleHoras';
            }

            
            $.ajax({

                type:"POST",
                url:url_view,
                data:{
                    "_token": "{{ csrf_token() }}",
                   // semana: sem ,
                    anio: anio,
                    cpersona: cpersona,
                    fecha_fin: fecha_fin,
                    fecha_inicio: fecha_inicio,
                    semana_ini: semana_ini,
                    semana_fin: semana_fin,

                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#modalHoras").html(data);
                     //$("#div_msg").show();
                     $("#viewHoras").modal('show');
                     $("#valor").val(valor); 
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });            
            
        }

        function getHoras(semana,anio,cpersona){  

            var sem=semana;
            var an=anio;
            var cper_eje=cpersona;

            $.ajax({

                type:"get",
                url:'getFechaParaDetalle',
                data:{
                  
                    anio: anio,
                    cpersona: cpersona,
                    semana: semana
                  

                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){                   
                     
                     $('#div_carga').hide();  

                     $("#fdesde").val(data[0]);
                     $("#fhasta").val(data[1]); 

                     verHoras(sem,sem,an,cper_eje);
                 

                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });            
            
        }

        function aprobarDeta(){
            checks = getSeleccionados();
            nochecks = getNoSeleccionados();
            txts = getObsInput();

            valor = $("#valor").val();

            if (valor=='historial') {
                url_view ='aprobarHorasIn_rechazar';
            }

            if (valor=='aprobar') {
                url_view ='aprobarHorasDeta';
            }

            /*if(checks.length<=0){
                alert('Seleccione los Items a Aprobar');
                return;
            }           */ 
            $("#viewHoras").modal('hide');
            $.ajax({
                type:"POST",
                url:url_view,
                data:{
                    "_token": "{{ csrf_token() }}",
                    cproyectoejecucion: checks ,
                    n_cproyectoejecucion:nochecks,
                    txt_obs:txts,
                     /*textObs: $("#textObs").val(),*/


                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     verHorasAprobar();
                     //$("#div_msg").show();
                     //("#resultado").html(data);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });
        }
/*        function observarDeta(){
            checks = getSeleccionados();
            if(checks.length<=0){
                alert('Seleccione los Items a Observar');
                return;
            }
            $("#viewHoras").modal('hide');
            $.ajax({

                type:"POST",
                url:'observarHorasDeta',
                data:{
                    "_token": "{{ csrf_token() }}",
                    cproyectoejecucion: checks ,
                    textObs: $("#textObs").val(),


                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     verHorasAprobar();
                     //$("#div_msg").show();
                     //("#resultado").html(data);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });
        }*/
        function getSeleccionados(){
            var selected = '';    
            $("input[name='cproyectoejecucion[]']:checked").each(function(){
                if (this.checked) {
                    selected += $(this).val()+',';
                }
            }); 
            var checks = selected.split(',');
            checks.pop();
            return checks;
        }
        function getNoSeleccionados(){
            var not_selected = '';    
            $("input[name='cproyectoejecucion[]']:not(:checked").each(function(){
                if (!this.checked) {
                    not_selected += $(this).val()+',';
                }
            }); 
            var checks = not_selected.split(',');
            checks.pop();
            return checks;
        }              
        function setCheck(chk){
            //alert($(":checkbox").length);
            $("input[name='checkbox_sel[]']").each(function(){
                /*if (this.checked) {
                    
                }*/
                //alert(this.checked);
                this.checked = chk.checked;
            }); 
            
            return;
        }           
        $("#btnAprobar").on('click',function(e){
            var nro = $("input[name='checkbox_sel[]']:checked").length;
            if(nro<=0){
                alert('Debe seleccionar por lo menos un Item');
                return ;
            }            
             
            $.ajax({

                type:"POST",
                url:'aprobarHoras',
                data:$("#frmHoras").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide();                      
                     //$("#resultado").html(data);
                     //$("#div_msg").show();

                    if ( $("#fhasta").val()=='' || $("#fdesde").val()==''){
                       
                        verTodasHorasAprobar();
                    }
                    else{

                        verHorasAprobar();
                    }
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });
        
        $("#btnAprobar2").on('click',function(e){
            var nro = $("input[name='checkbox_sel[]']:checked").length;
            if(nro<=0){
                alert('Debe seleccionar por lo menos un Item');
                return ;
            }   

                 
            $.ajax({

                type:"POST",
                url:'aprobarHoras',
                data:$("#frmHoras").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide();                      
                     //$("#resultado").html(data);
                     //$("#div_msg").show();
                    if ( $("#fhasta").val()=='' || $("#fdesde").val()==''){
                       
                        verTodasHorasAprobar();
                    }
                    else{

                        verHorasAprobar();
                    }
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });

        $("#btnObs").on('click',function(e){
            var nro = $("input[name='checkbox_sel[]']:checked").length;
            if(nro<=0){
                alert('Debe seleccionar por lo menos un Item');
                return ;
            }
            document.getElementById('message-text').value='';
            $('#modalObs').modal({
                backdrop:"static"
            });

        });

        $("#btnObs2").on('click',function(e){
            var nro = $("input[name='checkbox_sel[]']:checked").length;
            if(nro<=0){
                alert('Debe seleccionar por lo menos un Item');
                return ;
            }
            document.getElementById('message-text').value='';
            $('#modalObs').modal({
                backdrop:"static"
            });

        });

        function saveSele(){

            valor = $("#valor").val();

            if (valor=='historial') {
                url_view ='observarHoras_rechazar';
            }

            if (valor=='aprobar') {
                url_view ='observarHoras';
            }

            $.ajax({

                type:"POST",
                url:url_view,
                data:$("#frmHoras").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide();                      
                     //$("#resultado").html(data);
                     //$("#div_msg").show();
                     if ( $("#fhasta").val()=='' || $("#fdesde").val()==''){
                       
                        verTodasHorasAprobar();
                    }
                    else{

                        verHorasAprobar();
                    }
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });            
        }
         function saveObs(){
            
            txt=document.getElementById('message-text').value;
            if(txt!=''){
                $('#modalObs').modal('hide');
                
                $("#obs_txt").val(txt);
                saveSele();
            }
            
        }

        function viewObs(eje){
          //obj = document.getElementsByName('ejecu['+col+']['+fila+']');
            document.getElementById('message-textDeta').value= $("#obs_"+eje).val();
            $('#obs_planificada').html("");
            $('#obs_aprobacion').html("");
            $('#obs_ejecucion').html("");
            id_ejecu=eje;
            if($("#obs_plani"+eje).val()!=''){
                $('#obs_planificada').html($("#obs_plani"+eje).val());  
            }
            if($("#obs_apro"+eje).val()!=''){
                $('#obs_aprobacion').html($("#obs_apro"+eje).val());  
            }          
            if($("#obs_eje"+eje).val()!=''){
                $('#obs_ejecucion').html($("#obs_eje"+eje).val());  
            }              
            $('#modalObsDeta').modal('show');
        }
         function saveObsDeta(){
            
            txt=document.getElementById('message-textDeta').value;
            if(txt!=''){
                $("#obs_"+id_ejecu).val(txt);
                $('#modalObsDeta').modal('hide');
                
                
                
            }

            
        }  

        function getObsInput(){
            var x = $( "input[name^='txt_obs']" ).serializeArray();
            var inputs =[];
            $.each(x, function(i, field){
                var o = field.name;
                o = o.split('[');
                //alert(o[1]);
                var ejecucion = o[1].substring(0,o[1].length-1);
                inputs.push([ejecucion , field.value]);
            });       
            return inputs;
        }     

        function cerrar_modal(){

   $('#modalObsDeta').modal('hide');
   document.getElementById("viewHoras").style.overflow = "scroll"; 
}    

$('.datatable-order-search').dataTable( {
       "paging":   true,
       "ordering": true,
       "info":     true,
} );

var fechai=$("#fdesde").val();
var fechaf=$("#fhasta").val();
var fi_string=fechai.substr(0,2)+'/'+fechai.substr(3,2)+'/'+fechai.substr(6,4);
var ff_string=fechaf.substr(0,2)+'/'+fechaf.substr(3,2)+'/'+fechaf.substr(6,4);


$('#fdesde').datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true,
    firstDay: 1,
    calendarWeeks:true,
    todayHighlight: true,
   
}); 

$('#fhasta').datepicker({
    format: "dd/mm/yyyy",
    language: "es",
    autoclose: true,
    firstDay: 1,
    calendarWeeks:true,
    todayHighlight: true,
});   

</script>  
