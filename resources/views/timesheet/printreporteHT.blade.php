
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
    <style>
        /** Establezca los márgenes de la página en 0, de modo que el pie de página y el encabezado Puede ser de la altura y anchura total. !**/
        @page {
            margin: 1cm 2cm;
        }

        @font-face {
            font-family: 'Arial';
            font-weight: normal;
            font-style: normal;
            font-variant: normal;
        }
        /** Definir las reglas de encabezado. **/
        header {
            position: fixed;
            top: 1.0 cm;
            left: 0cm;
            right: 0cm;
            height: 0cm;
        }
        /** Define ahora los márgenes reales de cada página en el PDF. **/
        body {
			margin-top: 0.5 cm;
			margin-left: 0cm;
			margin-right: 0cm;
			margin-bottom: 3.2cm;
		}
        footer {
            position: fixed;
			bottom: 3.0cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
        }
        
    </style>
</head>
    <!-- <header>
        <div class="row">
            <img style="width: 150px;" src="images/logorpt.jpg"></th>
        </div>
        
    </header> -->

    @if ($firma_posicion == 1)
        @if (count($horas_array) > 0)
            <footer>
                <div>
                    @if (file_exists('images/firmas/'.$persona_nombre->firmaelectronica))
                        <div class="col-md-4 col-xs-4">
                            <img style="width: 150px; height: 100px" src="{{ url('images/firmas/'.$persona_nombre->firmaelectronica) }}">
                        </div>
                    @endif
                </div>
            </footer> 
        @endif
   @endif

    <body>
        <section class="content" style="font-family: Arial,Helvetica,sans-serif;">
            @include('timesheet.tablareporteht')
        </section>
    </body>

</html>