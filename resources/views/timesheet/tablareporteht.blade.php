@if(isset($horas_array))
    @foreach($horas_array as $key => $horas)
        <br>
        
        <div style="@if(($key +1) != count($horas_array))page-break-after:always;@endif size:5px;width: 100%" >
            <div class="row">
                <img style="width: 150px;" src="images/logorpt.jpg"></th>
            </div>

            <span>{{$persona_nombre->abreviatura}}</span>
            <div class="col-sm-2"  style="padding-top: 5px;">

                <span class="card-tittle" style="font-size:11px">Fechas: <b> {{ $horas['dia_inicio'][6] }}  al  {{ $horas['dia_fin'][6] }} </b></span> <br>
            
                <span class="card-tittle" style="font-size:11px">Semanas: <b> {{ $horas['dia_inicio'][4] }} - {{ $horas['dia_inicio'][3] }}  al  {{ $horas['dia_fin'][4] }} - {{ $horas['dia_fin'][3] }} </b></span>
            </div>
        
            <br>

            <table style="border-collapse: collapse;border:1px;width: 100%" class="table table-bordered" id="tableListaTR">
                <thead class="">
                    <tr style="border:1px;solid #000000;">
                        <th style="border:1px;font-size: 10px; solid #000000FF; width: 30%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)">Codigo / Cliente / Unidad Minera / Actividad : Tarea</th>
                        <th style="border:1px;font-size: 10px; solid #000000FF; width: 3%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)">Tipo</th>
                        <th style="border:1px;font-size: 10px; solid #000000FF; width: 10%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)">Categoría</th>
                        <th style="border:1px;font-size: 10px; solid #000000FF; width: 8%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)">GP</th>
                        @foreach($horas['dias'] as $key => $d)
                            <th style="border:1px;font-size: 10px; solid #000000FF; width: 3%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)">{{ $d[2] }}<br>{{ $d[1] }}</th>
                        @endforeach 
                        <th style="border:1px;font-size: 10px; solid #000000FF; width: 3%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)">Total</th>

                    </tr>
                </thead>
                <tbody>
                    @foreach($horas['ht_seccion'] as $key => $act)
                        <tr>
                            <td style="border:1px solid #000000FF;width: auto ;font-size: 10px;">{{$act['actividad']}}</td>
                            <td style="border:1px solid #000000FF;width: auto ;font-size: 10px;">{{$act['tipo']}}</td>
                            <td style="border:1px solid #000000FF;width: auto ;font-size: 10px;">{{$act['tipo_nf']}}</td>
                            <td style="border:1px solid #000000FF;width: auto ;font-size: 10px;">{{$act['gerente']}}</td>  
                            @foreach($horas['dias'] as $key => $d)

                            {{-- {{ dd($act['horas'],$d[0])}} --}}

                            <td style="border:1px solid #000000FF;width: auto ;font-size: 10px; text-align:center">
                                @if($act['horas'] != "sin_datos")
                                
                                    {{$act['horas'][$d[0]][1]}}
                                @else
                                
                                @endif
                                
                            </td>
                            @endforeach
                            <td style="border:1px solid #000000FF;width: auto ;font-size: 10px; text-align:center">{{$act['sumaXactividad']}}</td>
                            
                        </tr>
                    @endforeach 


                </tbody>
                <tfoot class="clsCabereraTabla">

                    <tr>
                        <th style="border:0px solid #000000FF;width: auto ;font-size: 10px;text-align:right" colspan="4"> Total :</th>
                        @foreach($horas['ht_sumatoria'] as $key => $s)
                        <th style="border:0px solid #000000FF;width: auto ;font-size: 10px;text-align: center">{{ $s }}</th>
                        @endforeach 
                        <th style="border:0px solid #000000FF;width: auto ;font-size: 10px;">{{ $horas['ht_sumatoria_total'] }}</th>
                    </tr>

                </tfoot>

            </table>

            <br>
        
            @if ($firma_posicion == 2)
                @if (file_exists('images/firmas/'.$persona_nombre->firmaelectronica))
                    <div class="col-md-4 col-xs-4">
                        <img style="width: 150px; height: 100px" src="{{ url('images/firmas/'.$persona_nombre->firmaelectronica) }}">
                    </div>
                @endif
            @endif
        
        </div>   
    @endforeach 
@endif 