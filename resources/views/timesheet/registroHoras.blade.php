<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hoja de tiempo -
        <small>Registro de horas</small>
      </h1>
      <ol class="breadcrumb">
        <!-- <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li> -->
        <!-- <li> -->
          <a type="button" class="btn btn-primary pull-right btn-xs" href="{{ url('participacion') }}" title="Ver Participación en Proyectos" target="_blank" style="size: 12px;"><b>Participación en Proyectos</b></a>
        <!-- </li> -->
        <!-- <li class="active"> Hoja de tiempo</li> -->
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        {!! Form::open(array('url' => 'grabarRegistroHoras','method' => 'POST','id' =>'frmHoras')) !!}
        <?php /*{!! Form::hidden('cproyecto',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}*/ ?>
        {!! Form::hidden('tipocontrato',(isset($tipocontrato->horas)==1?$tipocontrato->horas:''),array('id'=>'tipocontrato')) !!}
        {!! Form::hidden('semana_anio_ingreso',(isset($semana_anio_ingreso)==1?$semana_anio_ingreso:''),array('id'=>'semana_anio_ingreso')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}

         

    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">
      <div class="row ">
          <div class="col-md-4 col-xs-1"></div>
          <div class="col-md-4 col-xs-3"></div>         
         <div class="col-md-2 col-xs-5 ">
                <?php /*<select class="form-control" onchange="tipoBusquedad(this)" name="consulta" id="tipoConsulta"
                @if($selec!='')
                  readonly="true"
                @endif
                >
                  <option value="P" {{ ($selec=='P'?'selected':'') }} >Proyecto</option>
                  <option value="X" {{ ($selec=='X'?'selected':'') }}>Todos</option>
                </select> */ ?>  
            <div class="input-group date">
                  <!--<div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>-->
                  {!! Form::hidden('fecha',( isset($fecha)==1?$fecha:'' ), array('id' => 'fecha')) !!}
            </div>                       
          </div>
          <div class="col-md-2 col-xs-3">
            <!--<button type="button" class="btn btn-primary " id="btnTodos">
              <i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Buscar
            </button>                     -->
           </div>

      </div>
      
      <?php /*  
      <div class="row" id="Proy"
      @if($selec=='' || $selec=='P')
        style="display:block"
      @else
        style="display:none"
      @endif
      >
        <div class="col-lg-1 col-xs-1">Proyecto:</div>
        <div class="col-lg-9 col-xs-9">{!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:'')
,array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!} </div>
        <div class="col-lg-2 col-xs-2">
          <button type="button" class="btn btn-primary " data-toggle="modal" data-target="#searchProyecto">
            <i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Buscar
          </button>
        </div>
      </div>
      
      
      <div class="row" id="Todos" 
      @if($selec=='X')
        style="display:block"
      @else
        style="display:none"
      @endif
      >
        <div class="col-lg-1 col-xs-1"></div>
        <div class="col-lg-9 col-xs-9"></div>
        <div class="col-lg-2 col-xs-2">
          <button type="button" class="btn btn-primary " id="btnTodos">
            <i class="fa fa-search" aria-hidden="true"></i>&nbsp;&nbsp;Buscar
          </button>
        </div>
      </div> */ ?>
      
  
      <!-- <div class="row box box-primary" >	 -->
        <!-- <div class="col-lg-2 col-xs-6" style="border: 1px solid #0069AA" id="resumen"> -->
              <!-- <span class="card-tittle" style="font-size:11px">Horas de la semana: <b> {{ (isset($semana)==1?$semana:'-') }} -{{ (isset($anio)==1?$anio:'-') }} </b></span> <br /> -->
              <!--<span class="info-box-number" style="font-size:10px"> </span><br />-->

              <!-- <span class="card-tittle" style="font-size:11px">Horas facturable: <b> {{ (isset($sumFF)==1?$sumFF:0) }} </b> </span><br /> -->
              <!--<span class="info-box-number" style="font-size:10px"></span>              -->

              <!-- <span class="card-tittle" style="font-size:11px">Horas no facturable: <b>{{ (isset($sumNF)==1?$sumNF:0) }} </b> </span> <br /> -->
              <!--<span class="info-box-number" style="font-size:10px"></span>-->

              <!-- <span class="card-tittle" style="font-size:11px">Horas Anddes: <b>{{ (isset($sumAN)==1?$sumAN:0) }} </b> </span> <br /> -->
              <!--<span class="info-box-number" style="font-size:10px"></span>-->
              <!-- <span class="card-tittle" style="font-size:11px">Horas total: <b> {{ (isset($sumFF)==1?$sumFF:0) + (isset($sumNF)==1?$sumNF:0) +(isset($sumAN)==1?$sumAN:0) }} </b> </span> -->
              <!--<span class="info-box-number" style="font-size:10px"></span>              -->

        <!-- </div> -->
        <!-- <div class="col-lg-10 col-xs-6" >
            <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" id="btnReabrir" style="margin-left:1px;margin-right: 1px; margin-top: 5px;"
            @if(isset($reabrir)==1)
              @if($reabrir)
  
              @else
                disabled
              @endif
            @else
              disabled
            @endif
            >Reabrir</button>     
            <button type="button" class="btn btn-primary btn-sm pull-right" id="btnenviar" style="margin-left:1px;margin-right: 1px; margin-top: 5px;">Enviar horas</button>
            <button  class="btn btn-primary btn-sm pull-right" id="btnsave" style="margin-left:1px;margin-right: 1px; margin-top: 5px;">Guardar</button>
            <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#agregarTareasTodosAdm" id="btnAgregarAdmTodos" style="margin-left:1px;margin-right: 1px; margin-top: 5px;">Agregar tareas administrativas</button>
            <button type="button" class="btn btn-primary btn-sm pull-right" data-toggle="modal" data-target="#agregarTareasTodos" id="btnAgregarTodos" style="margin-left:1px;margin-right: 1px; margin-top: 5px;">Agregar tareas del proyecto</button>
        </div> -->

        <!-- <div class="col-lg-2 col-xs-6" >
            <div id="dash_semana" style="display:none;position:absolute;width:240px" class="panel panel-default">
                <div class="panel-heading" >Dashboard- Progresos</div>
                <div class="panel-body">
                      <span class="progress-description" style="font-size:11px">
                        {{ (isset($fecha1)==1?$fecha1->toDateString():'')  }} al {{ (isset($fecha2)==1?$fecha2->toDateString():'')  }}
                      </span>                   
                      <div class="progress" >
                        <div class="progress-bar" style="width: {{ (isset($semana)==1?(($semana/52)*100).'%':'0%') }}"></div>
                      </div>
  
                      <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="{{ (isset($nroHoras)==1?$nroHoras:0 ) }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ (isset($nroHoras)==1?$nroHoras:0 ) }}% ;font-size:11px;padding:1px; background-color: @if($nroHoras<60) green @elseif($nroHoras<80) orange @else red @endif"></div>
                          <span class="info-box-number" style="font-size:11px; display: block; position: absolute; width: 200px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje cargabilidad planificada">Cargabilidad planificada: {{ (isset($nroHoras)==1?$nroHoras:0 ) }} %</span>
                      </div>
                      <div class="progress">
                          <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="{{ (isset($nroHorasCar)==1?$nroHorasCar:0 ) }}" aria-valuemin="0" aria-valuemax="100" style="width: {{ (isset($nroHorasCar)==1?$nroHorasCar:0 ) }}% ;font-size:11px;padding:1px; background-color: @if($nroHorasCar<60) green @elseif($nroHorasCar<80) orange @else red @endif"></div>
                          <span class="info-box-number" style="font-size:11px; display: block; position: absolute; width: 200px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje cargabilidad ejecutada">Cargabilidad ejecutada: {{ (isset($nroHorasCar)==1?$nroHorasCar:0 ) }} %</span>
                      </div>                    
                </div>
            </div>


        </div> -->

      <!-- </div>  -->


      <section class="panel box box-primary" >
      <div class="col-sm-2"  style="padding-top: 5px;">
        <span class="card-tittle" style="font-size:11px">Horas de la semana: <b> {{ (isset($semana)==1?$semana:'-') }} -{{ (isset($anio)==1?$anio:'-') }} </b></span>
        <span class="progress-description" style="font-size:12px">
          {{ (isset($fecha1)==1?$fecha1->toDateString():'')  }} al {{ (isset($fecha2)==1?$fecha2->toDateString():'')  }}
        </span>                   
        <div class="progress" >
          <div class="progress-bar" style="width: {{ (isset($semana)==1?(($semana/52)*100).'%':'0%') }}"></div>
        </div>
      </div>
      <div class="col-sm-5" style="padding-top: 0px;">
        <button data-toggle="button" class="btn btn-white info">
          <span style="font-size:12px">HH facturable <br> <b> {{ (isset($sumFF)==1?$sumFF:0) }} </b> </span>
        </button>
        <button data-toggle="button" class="btn btn-white info">
          <span style="font-size:12px">HH no facturable <br> <b>{{ (isset($sumNF)==1?$sumNF:0) }} </b> </span>
        </button>
        <button data-toggle="button" class="btn btn-white info">
          <span style="font-size:12px">HH Anddes <br> <b>{{ (isset($sumAN)==1?$sumAN:0) }} </b> </span>
        </button>
        <button data-toggle="button" class="btn btn-white info">
          <span style="font-size:12px">HH total <br> <b> {{ (isset($sumFF)==1?$sumFF:0) + (isset($sumNF)==1?$sumNF:0) +(isset($sumAN)==1?$sumAN:0) }} </b> </span>
        </button>
      </div>
      <div class="col-sm-1 pull-left" style="padding-top: 0px;">
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="{{ (isset($nroHoras)==1?$nroHoras:0 ) }}" aria-valuemin="0" aria-valuemax="50" style="width: {{ (isset($nroHoras)==1?$nroHoras:0 ) }}% ;font-size:11px;padding:1px; background-color: @if($nroHoras<60) green @elseif($nroHoras<80) orange @else red @endif">
          </div>
          <span class="info-box-number" style="font-size:11px; display: block; position: absolute; width: 200px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje cargabilidad planificada">Cargabilidad Plani. : {{ (isset($nroHoras)==1?$nroHoras:0 ) }} %</span>
        </div>
        <div class="progress">
          <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="{{ (isset($nroHorasCar)==1?$nroHorasCar:0 ) }}" aria-valuemin="0" aria-valuemax="50" style="width: {{ (isset($nroHorasCar)==1?$nroHorasCar:0 ) }}% ;font-size:11px;padding:1px; background-color: @if($nroHorasCar<60) green @elseif($nroHorasCar<80) orange @else red @endif">
          </div>
          <span class="info-box-number" style="font-size:11px; display: block; position: absolute; width: 200px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje cargabilidad ejecutada">Cargabilidad Ejec. : {{ (isset($nroHorasCar)==1?$nroHorasCar:0 ) }} %</span>
        </div>
      </div>
      <div class="col-sm-4" style="padding: 0px;">
        <div class="list-group active pull-right" style="font-size:12px">
          <center>
          <span href="#" class="list-group-item list-group-item-action"><b>Acciones HT</b></span>
          <button type="button" class="btn btn-primary pull-right btn-xs" data-toggle="modal" id="btnReabrir" style="margin-left:1px;margin-right: 1px; margin-top: 5px; margin-bottom: 5px"
          @if(isset($reabrir)==1)
          @if($reabrir)

          @else
          disabled
          @endif
          @else
          disabled
          @endif
          >Reabrir</button>     
          <button type="button" class="btn btn-primary pull-right btn-xs" id="btnenviar" style="margin-left:1px;margin-right: 1px; margin-top: 5px; margin-bottom: 5px">Enviar</button>
          <button  class="btn btn-primary pull-right btn-xs" id="btnsave" style="margin-left:1px;margin-right: 1px; margin-top: 5px; margin-bottom: 5px">Guardar</button>
          </center>
        </div>
        <div class="list-group active pull-right" style="font-size:12px">
          <center>
          <span href="#" class="list-group-item list-group-item-action"><b>Agregar tareas</b></span>
          <button type="button" class="btn btn-primary pull-right btn-xs" data-toggle="modal" data-target="#agregarTareasTodosAdm" id="btnAgregarAdmTodos" style="margin-left:1px;margin-right: 5px; margin-top: 5px; margin-bottom: 5px">Administrativas</button>
          <button type="button" class="btn btn-primary pull-right btn-xs" data-toggle="modal" data-target="#agregarTareasTodos" id="btnAgregarTodos" style="margin-left:1px;margin-right: 1px; margin-top: 5px; margin-bottom: 5px">Proyecto</button>
          </center>
        </div>
      </div>
    </section>
    
    


      <!-- <div class="row "> -->
      	<!-- <div class="col-md-1 hidden-xs"></div> -->
        <!-- <div class="col-md-2 col-xs-6">
          
        </div> -->
        <!-- <div class="col-md-2 col-xs-6">
          
        </div> -->
        <!-- <div class="col-md-2 col-xs-6"> -->
          <?php /*<button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-target="#addTareas" id="btnAgregar"
            @if($selec=='' || $selec=='P')
              style="display:block"
            @else
              style="display:none"
            @endif          
          >Agregar</button>*/ ?>

       
          
        <!-- </div> -->
        <!-- <div class="col-md-2 col-xs-6"> -->
          <?php /*<button type="button" class="btn btn-primary btn-block " data-toggle="modal" data-target="#addTareasAdm" id="btnAgregarAdm" 
            @if($selec=='' || $selec=='P')
              style="display:block"
            @else
              style="display:none"
            @endif            
          >Agregar Adm</button> */ ?>

        <!-- </div> -->
        <!-- <div class="col-md-2 col-xs-6">
     
        </div> -->
        <!-- <div class="col-md-1 col-xs-6"></div> -->
      <!-- </div> -->
      <!-- <br /> -->


     

        
      <!-- <div class="row">	 -->


       
        <div class="col-lg-12 col-xs-12 table-responsive">
            @include('partials.tableHoras',array('alinea'=>(isset($alinea)==1?$alinea:array()),'nofacturables'=> (isset($nofacturables)==1?$nofacturables:array()), 'fecha1'=> isset($fecha1)==1?$fecha1:null,'fecha2'=> isset($fecha2)==1?$fecha2:null,'fecp'=> isset($fecp)==1?$fecp:null ))    
        </div>
      <!-- </div>         -->
      <div class="row">
        <br>
      </div>
      <div class="row">
        <br>
      </div>


      <div class="col-md-12">
        <a class="btn btn-outline-sucess btn-sm" style="background-color: #11B15B; color: black" role="button" data-toggle="collapse" data-target="#haprobaciones" aria-expanded="false" aria-controls="haprobaciones">
        <b>Historial de aprobaciones</b>
        </a>      
        <div class="collapse" id="haprobaciones">
          <div class="well">
            <table class="table table-hover" id="taprobaciones">
              <thead class="clsCabereraTabla">
                <th>
                  Código / Cliente / Unidad Minera / Actividad : tarea
                </th>   
                <th>Horas
                </th>           
                <th>
                  Aprobador
                </th>
                <th>
                  Fecha
                </th>                
                <th>
                  Paso anterior
                </th> 
                <th>
                  Paso actual
                </th>                                
              </thead>
              <tbody>
              @if(isset($historialApro)==1 )
              @foreach($historialApro as $a)
                <tr>
                  <td>
                    {{ $a['actividad'] }}
                  </td>
                  <td>
                    {{ $a['horas'] }}
                  </td>
                  <td>
                    {{ $a['aprobado'] }}
                  </td>
                  <td>
                    {{ $a['fecha'] }}
                  </td>                
                  <td>
                    {{ $a['ccondicionoperativa'] }}
                  </td> 
                  <td>
                    {{ $a['ccondicionoperativa_sig'] }}
                  </td>                                                  
                </tr>
              @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <div class="col-md-12">
        <a class="btn btn-outline-primary " style="background-color: #FFBF00; color: black" role="button" data-toggle="collapse" data-target="#hcomentarios" aria-expanded="false" aria-controls="hcomentarios">
          <b>Historial de comentarios</b>
        </a>      
        <div class="collapse" id="hcomentarios">
          <div class="well">
            <table class="table table-hover" id="tcomentarios">
              <thead class="clsCabereraTabla">
                <th>
                  Código / Cliente / Unidad Minera / Actividad : tarea
                </th>
                <th> Horas
                </th>
                <th>
                  Fecha
                </th>                
                <th>
                  Cometario <br> planificación
                </th> 
                <th>
                  Cometario <br> ejecutado
                </th>      
                <th>
                  Cometario <br>  observado
                </th>                                           
              </thead>
              <tbody>
              @if(isset($historialCom)==1 )
              @foreach($historialCom as $a)              
                <tr>
                  <td>
                    {{ $a['actividad'] }}
                  </td>
                  <td> {{ $a['horas'] }}
                  </td>
                  <td>
                    {{ $a['fecha'] }}
                  </td>                
                  <td>
                    {{ $a['comentariosPla'] }}
                  </td> 
                  <td>
                    {{ $a['comentariosEje'] }}
                  </td> 
                  <td>
                    {{ $a['comentariosObs'] }}
                  </td>                                                                    
                </tr>
                @endforeach
                @endif
              </tbody>
              </table>
          </div>
        </div>
      </div>
        
        
        
        
        
        
        
        
        </div><!-- fin parte central -->
    
    <!--<div class="col-lg-1 hidden-xs"></div>-->

      </div>
      {!! Form::close() !!}
</section>
    <?php /*@include('partials.searchProyecto')*/ ?>
      <!--   *******************************************  -->
  
   <?php /* <!-- *********************************** -->
        <!-- Modal Tareas -->
      <div class="modal fade" id="addTareas" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTask">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabelTask">Agregar Actividades / Tareas</h4>
            </div>
            <div class="modal-body">
                <div id="treeActividades">
                </div>
                 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary" onclick="agregarFilaAct()">Agregar</button>
            </div>
          </div>
        </div>
      </div>
    <!--************************************* -->
    <!-- *********************************** -->
        <!-- Modal Tareas Adm -->
      <div class="modal fade" id="addTareasAdm" tabindex="-1" role="dialog" aria-labelledby="myModalLabelTask">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabelTask">Agregar Actividades / Tareas Administrativas</h4>
            </div>
            <div class="modal-body">
                <div id="treeActividadesAdm">
                </div>
                 
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary" onclick="agregarFilaActAdm()">Agregar</button>
            </div>
          </div>
        </div>
      </div>
    <!--************************************* --> */ ?>
    <!-- Agregar Tareas Todos-->
    <div id="agregarTareasTodos" class="modal fade" role="dialog" >
      <div class="modal-dialog modal-lg">

        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" >Cerrar</button>
            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
            <button type="button" class="btn btn-primary pull-right" onclick="agregarFilaActTodos();" >Agregar</button>
            <h4 class="modal-title" style="color: white">Agregar tareas del proyecto</h4>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => 'agregarDisciplina','method' => 'POST','id' =>'frmTareasTodos')) !!}
            {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_h')) !!}
		      
                <div class="row">
                    <div class="col-md-2" >
                       
                          <div class="list-group" style="width: 111%; font-size: 12px">
                            <a href="#" class="list-group-item active" >
                              Clientes
                            </a>
                            @if(isset($clientes)==1)
                            <div style="width: auto; height: 450px; overflow-y: auto;">
                            @foreach($clientes as $k => $v)
                            <a href="#" onclick="loadumineraT({{ $k }});" class="list-group-item">{{ $v }}</a>
                            @endforeach
                            </div>
                            @endif
                            <input type="hidden" name="clientesT" id="clientesT" value="" />
                          </div>
                    </div><!-- col-md-2  de proyectos-->
                    <div class="col-md-1">
                          <div id="divUmineraT">
                            @include('timesheet.divUmineraT')
                          </div>
                    </div><!-- col-md-2  de proyectos-->
                    <div class="col-md-4">

                          <div id="divProyT">
                            @include('timesheet.divProyTodosT')
                          </div>
                      
                    </div> <!-- col-md-2  de proyectos-->
                    <div class="col-md-5">
                    <div class="rounded" style="width:100%;height:100% ;border: solid 1px #0069AA" id="divActTodos">
                        <div id="treeActividadesTodos" style="width: auto; height: 500px; overflow-y: auto;">

                        </div>                        
                    </div>
                </div>
                </div> <!-- div row -->
            {!! Form::close() !!}
          </div>    
          <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="agregarFilaActTodos();" >Agregar</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
          </div>            
        </div>
      </div>
    </div>    
    <!-- fin Agregar Tareas Todos -->
    <!-- Agregar Tareas Todos ADM-->
    <div id="agregarTareasTodosAdm" class="modal fade" role="dialog">
      <div class="modal-dialog modal-md">

        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="btn btn-primary pull-right" data-dismiss="modal" >Cerrar</button>
            <button type="button" class="btn btn-primary pull-right" onclick="agregarFilaActTodosAdm();" >Agregar</button>
            <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
            <h4 class="modal-title" style="color: white">Agregar tareas administrativas</h4>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => 'agregarDisciplina','method' => 'POST','id' =>'frmTareasTodosAdm')) !!}
            {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_h')) !!}
		      
                <div class="row">
                    <div class="col-md-12">
                      <div class="rounded"  id="divActTodosAdm">
                          <div id="treeAdmin" style="width: auto; height: 500px; overflow-y: auto; font-size:14px;"></div>
                        {{-- <div id="treeActividadesTodosAdm" style="width: auto; height: 500px; overflow-y: auto;">

                        </div>--}}
                    </div>
                </div>
                </div> <!-- div row -->
            {!! Form::close() !!}
          </div>    
          <div class="modal-footer">
          <button type="button" class="btn btn-primary" onclick="agregarFilaActTodosAdm();" >Agregar</button>
          <button type="button" class="btn btn-primary" data-dismiss="modal" >Cerrar</button>
          </div>            
        </div>
      </div>
    </div>    
    <!-- fin Agregar Tareas Todos ADM -->
    <!-- *************************************************** -->
      <div class="modal fade" id="modalObs" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="ObsModalLabel" style="color: white">Comentarios</h4>
            </div>
            <div class="modal-body">
              <form>
                <label  class="control-label">
                Observación de planificación:
                </label>
                <div id="obs_planificada">

                </div>
                <label  class="control-label">
                Observación de aprobación:
                </label>
                <div id="obs_aprobacion">

                </div>                
                <div class="form-group">
                  <label for="message-text" class="control-label">Ingrese comentario:</label>
                  <textarea class="form-control" id="message-text"></textarea>
                </div>
              </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-primary" onclick="saveObs()">Aceptar</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>
          </div>
        </div>
      </div>

    <!-- *************************************************** -->

</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        var selected_actividad =[];
        var selected_actividad_adm =[];        
        var selected_actividad_todos_adm =[];
        var selected_actividad_todos =[];
        //$.fn.dataTable.ext.errMode = 'throw';
        var i_col=0;
        var i_fila=0;
        var i_dia=0;
        var nroFila = <?php echo (isset($nroFila)==1?$nroFila:0); ?>;
        var dia_act = <?php echo (isset($dia_act)==1?$dia_act:''); ?>;
        //alert(nroFila);
        var tiposNoFacturables = [];
        <?php if(isset($nofacturables)){ ?>
          <?php foreach($nofacturables as $nf => $k) { ?>
                tiposNoFacturables[tiposNoFacturables.length]=['<?php echo $k; ?>','<?php echo $nf; ?>'];

          <?php } ?>
        <?php } ?>          
        $("#fecha").datepicker({
          autoclose:true,
          calendarWeeks:true,
          format: "dd/mm/yyyy"
        });
        /*if(!$("body").hasClass("sidebar-collapse")){
          $(".sidebar-toggle").click();
        }*/
        
        /*var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if($("#fecha").val()==''){
            alert('Ingrese fecha de consulta');
            return;
          }
          if (selected.length > 0 ){
              id = selected[0];
              fec = $("#fecha").val();
              fec = fec.replace('/','-');
              getUrl('editarRegistroHoras/'+id.substring(4)+'/'+ fec.replace('/','-'),'');
          }else{
              $('.alert').show();
          }
        }*/
/*
        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });  */

        /* Tareas por Proyecto*/ 
        /*$("#addTareas").on('show.bs.modal',function(e){
          selected_actividad.splice(0,selected_actividad.length);
          $('#treeActividades').highCheckTree({
            data: getTreeActividad(),
            onCheck: function (node){
              selected_actividad.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr(rel));
              if(idx != -1){
                selected_actividad.splice(idx,1);
              }
            }
          });            
        });      
        $("#addTareasAdm").on('show.bs.modal',function(e){
          selected_actividad_adm.splice(0,selected_actividad_adm.length);
          $('#treeActividadesAdm').highCheckTree({
            data: getTreeActividadAdm(),
            onCheck: function (node){
              selected_actividad_adm.push(node.attr('rel'));
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr(rel));
              if(idx != -1){
                selected_actividad_adm.splice(idx,1);
              }
            }
          });            
        });       */  
        /*  Fin de Tareas por proyecto*/

           
      /* Tareas Todos */
        $("#agregarTareasTodos").on('show.bs.modal',function(e){
          selected_actividad_todos.splice(0,selected_actividad_todos.length);
          $("#divProyT").html("");
          $("#treeActividadesTodos").html("");
           //limpiar combos
        });    

        $("#agregarTareasTodosAdm").on('show.bs.modal',function(e){
          
          selected_actividad_todos_adm.splice(0,selected_actividad_todos_adm.length);
          //$('#treeActividadesTodosAdm').highCheckTree({
            //data: getTreeActividadTodosAdm(),
            //onCheck: function (node){
              //selected_actividad_todos_adm.push(node.attr('rel'));
            //},
            //onUnCheck: function(node){
              //alert(node);
              //var idx = $.inArray(node.attr('rel'),selected_actividad_todos_adm);
              //if(idx != -1){
                //selected_actividad_todos_adm.splice(idx,1);
              //}
            //}
          //});            
          $("#treeAdmin").jstree({
            'core' : {
            'data' : getTreeActividadTodosAdm2(),
                },
            'checkbox': {       
              'three_state' : true, // para evitar el hecho de que verificar un nodo también verifica otros
              'whole_node' : true,  // para evitar marcar la casilla simplemente haciendo clic en el nodo 
              'tie_selection' : false // para verificar sin seleccionar y seleccionar sin verificar
            },
            "plugins" : ["checkbox"]
          });
          // iconhiden();
        });  

        function iconhiden(){
          $("#treeAdmin").jstree("toggle_icons");
        }
        function uncheckall(){
          $("#treeAdmin").jstree(true).uncheck_all();
          $(".jstree-node li[role=treeitem]").each(function (e) {
            $(this).attr("aria-selected",'false')
          });
        }

        function recorrerAdm(){
          $(".jstree-node li[role=treeitem]").each(function (e) {
            if($(this).attr("aria-selected") === 'true' ){
              if($(this).attr("id") != 751){
                selected_actividad_todos_adm.push($(this).attr("id"))
              }
            }
          });
          return selected_actividad_todos_adm;
        }

      /* Fin Tareas todos */
        function clonarfila (id){
            agregarFila(id);
       
        }   
        /*function agregarFilaAct(){
          $("#addTareas").modal("hide");
          $.ajax({

                type:"POST",
                url:'agregarRegistroTime',
                data : {
                  "_token": "{{ csrf_token() }}",
                  selected_actividad:selected_actividad,
                  cproyecto : $("#cproyecto").val(),
                  fecha: $("#fecha").val(),
                  nroFila: nroFila
                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#tableHorasAprob tbody").append(data);
                     //alert(nroFila);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                }
            });           
        }*/
        function agregarFilaActTodos(){
          $("#agregarTareasTodos").modal("hide");
          // var cproy = $("input[name='pryTodos']:checked").val();
          var cproy = $("input[name='pryTodos']").val();
          $.ajax({

                type:"POST",
                url:'agregarRegistroTime',
                data : {
                  "_token": "{{ csrf_token() }}",
                  selected_actividad:selected_actividad_todos,
                  cproyecto : cproy,
                  fecha: $("#fecha").val(),
                  nroFila: nroFila
                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#tableHorasAprob tbody").append(data);
                     //alert(nroFila);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                }
            });           
        }        
        /*function agregarFilaActAdm(){
          $("#addTareasAdm").modal("hide");
          $.ajax({

                type:"POST",
                url:'agregarRegistroTimeAdm',
                data : {
                  "_token": "{{ csrf_token() }}",
                  selected_actividad_adm:selected_actividad_adm,
                  cproyecto : $("#cproyecto").val(),
                  fecha: $("#fecha").val(),
                  nroFila: nroFila
                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#tableHorasAprob tbody").append(data);
                     //alert(nroFila);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                }
            });           
        }      */ 
        function agregarFilaActTodosAdm(){
          $("#agregarTareasTodosAdm").modal("hide");
          var cproy = $("input[name='pryTodosAdm']:checked").val();
          selected_actividad_todos_adm = recorrerAdm();
          uncheckall();
          // iconhiden();
          //console.log("actividades",selected_actividad_todos_adm)
          $.ajax({

                type:"POST",
                url:'agregarRegistroTimeAdm',
                data : {
                  "_token": "{{ csrf_token() }}",
                  selected_actividad_adm:selected_actividad_todos_adm,
                  fecha: $("#fecha").val(),
                  nroFila: nroFila
                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#tableHorasAprob tbody").append(data);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                }
            });          
        }              
        function fillNoFacturables(id){
          
            $("#nfac_"+id).append(new Option('','',true,true));
            
            for(i=0;i < tiposNoFacturables.length ; i++){
                $("#nfac_"+id).append(new Option(tiposNoFacturables[i][0],tiposNoFacturables[i][1]));
            }
        }
        function agregarFila(idFila){
            var pry = $('#pry_'+idFila).val();
            var act = $('#act_'+idFila).val();
            var tipo = $('#tipo_'+idFila).val();
            var cproy = $('#cproy_'+idFila).val();
            var desAct = $('#desAct_'+idFila).val();
            var gp = $('#gp_'+idFila).text();
            var desc = $('#desc_'+idFila).val();
            //alert(nroFila);
            var index = nroFila;
            nroFila = nroFila + 1;
            var ini_registro = "<tr class='gradeX' id='row_"+nroFila+"' >";
            var fin_registro = "</tr>";
            var col_actividad = "<th  style='width: 1050px; font-size:12px;  border:1px solid #DCDCDCFF;'>";
                col_actividad =  col_actividad + "<input name='ejecu["+index+"][0]' id='pry_"+nroFila+"' value='"+pry+"' type='hidden'>";
                col_actividad = col_actividad + "<input name='ejecu["+index+"][1]' id='act_"+nroFila+"' value='"+act+"' type='hidden'>";
                col_actividad = col_actividad + "<input name='ejecu["+index+"][2]' id='tipo_"+nroFila+"' value='2' type='hidden'>";
                col_actividad = col_actividad + "<input name='ejecu["+index+"][51]' id='cproy_"+nroFila+"' value='"+cproy+"' type='hidden'>";
                col_actividad = col_actividad + "<input name='ejecu["+index+"][52]' id='desAct_"+nroFila+"' value='"+desAct+"' type='hidden'>";
                col_actividad = col_actividad + "<input name='ejecu["+index+"][53]' id='removeAct_"+nroFila+"' value='N' type='hidden'>";
                col_actividad = col_actividad + "<input name='ejecu["+index+"][54]' id='ejecab"+nroFila+"' value='' type='hidden'>";
                col_actividad = col_actividad + "<input name='proys_"+index+"' id='desc_"+nroFila+"' value='"+desc+"' type='hidden'>";
                col_actividad = col_actividad + ((tipo!=3) ? desc : '') + desAct ;
                col_actividad = col_actividad + "</th>";

            var col_categoria = "<th>"+$('#tableHorasAprob tr').eq(idFila).find('.categoria').text()+"</th>"

            var col_facturable = "<th style='width: 40px; border:1px solid #DCDCDCFF; text-align:center;'>";
                col_facturable = col_facturable+ "<span class='label' style='background-color:#CD201A;'>NF</span>";
                col_facturable = col_facturable+ "</th>";

            var col_recu = "<th  style='width: 60px; border:1px solid #DCDCDCFF;'>";
                col_recu = col_recu + "<select name='ejecu["+index+"][3]' id='nfac_"+nroFila+"'></select> ";
                
                col_recu = col_recu + "</th>";
            var col_gp ="<th style='width: 60px; border:1px solid #DCDCDCFF; text-align:center;' ><div id='gp_"+nroFila+"'>"+gp+"</th>";
            //var col_ent = col_ent +"<td class='clsAnchoTabla'>&nbsp;</td>";

            var key = getKey(idFila,7);
            var col_input1="<th style='width: 60px; padding:0;  border:1px solid #DCDCDCFF; text-align:center;' >";
                      col_input1= col_input1 + "<input name='ejecu["+index+"][4]' value='' type='hidden'>       ";
                      col_input1= col_input1 + "<input name='ejecu["+index+"][5]' value='2' type='hidden'>        ";
                      col_input1= col_input1 + "<input name='ejecu["+index+"][6]' value='' type='hidden'>         ";
                      col_input1= col_input1 + "<input name='ejecu["+index+"][7]' value='"+key+"' type='hidden'> ";
                      col_input1= col_input1 + "<input name='ejecu["+index+"][39]' value='' type='hidden'>        ";
                      col_input1= col_input1 + "<input  size='2' maxlength='5' name='ejecu["+index+"][8]' value='' onchange='disabledEnviar();viewSobre(\""+nroFila+"_1\",this);sumHoras("+index+")' type='text' onkeypress='javascript:return soloNumeros(event,this);'";
                      if(dia_act < key){
                        col_input1= col_input1 + " ";
                      }
                      col_input1= col_input1 + ">";
                      col_input1= col_input1 + "<a href='#' onclick='viewObs("+nroFila+",6,1,\"\",\"\")' id='obs_"+nroFila+"_1' style='display:none'><i class='fa fa-envelope-o' aria-hidden='true'></i></a>";                 
					            col_input1= col_input1 + "</th>";                                                                                                                                             
            key = getKey(idFila,12);                                                                                                                                   
            var col_input2="<th style='width: 60px; padding:0;  border:1px solid #DCDCDCFF; text-align:center;'>                                                                                                                         ";
                        col_input2= col_input2 + "<input name='ejecu["+index+"][9]' value='' type='hidden'>                                                                                                ";
                        col_input2= col_input2 + "<input name='ejecu["+index+"][10]' value='2' type='hidden'>                                                                                               ";
                        col_input2= col_input2 + "<input name='ejecu["+index+"][11]' value='' type='hidden'>                                                                                               ";
                        col_input2= col_input2 +  "<input name='ejecu["+index+"][12]' value='"+key+"' type='hidden'>                                                                                       ";
                        col_input2= col_input2 + "<input name='ejecu["+index+"][40]' value='' type='hidden'>                                                                                               ";
                        col_input2= col_input2 + "<input  size='2' maxlength='5' name='ejecu["+index+"][13]' value=''  onchange='disabledEnviar();viewSobre(\""+nroFila+"_2\",this);sumHoras("+index+")' type='text' onkeypress='return soloNumeros(event,this);'";
                        if(dia_act < key){
                          col_input2= col_input2 + " ";
                        }                        
                        col_input2= col_input2 + ">";
                        col_input2= col_input2 + "<a href='#' onclick='viewObs("+nroFila+",11,2,\"\",\"\")' id='obs_"+nroFila+"_2' style='display:none'><i class='fa fa-envelope-o' aria-hidden='true'></i></a>";
                      col_input2= col_input2 + "</th>";
            key = getKey(idFila,17);          
            var col_input3="<th style='width: 60px; padding:0;  border:1px solid #DCDCDCFF; text-align:center;'>                                                                         ";
                        col_input3= col_input3 + "<input name='ejecu["+index+"][14]' value='' type='hidden'>                                                                              ";
                        col_input3= col_input3 + "<input name='ejecu["+index+"][15]' value='2' type='hidden'>                                                                               ";
                        col_input3= col_input3 + "<input name='ejecu["+index+"][16]' value='' type='hidden'>                                                                                ";
                        col_input3= col_input3 + "<input name='ejecu["+index+"][17]' value='"+key+"' type='hidden'>                                                                        ";
                        col_input3= col_input3 + "<input name='ejecu["+index+"][41]' value='' type='hidden'>                                                                                ";
                        col_input3= col_input3 + "<input  size='2' maxlength='5' name='ejecu["+index+"][18]' value='' onchange='disabledEnviar();viewSobre(\""+nroFila+"_3\",this);sumHoras("+index+")' type='text' onkeypress='return soloNumeros(event,this);'";
                        if(dia_act < key){
                          col_input3= col_input3 + " ";
                        }                               
                        col_input3= col_input3 + ">";                        
                        col_input3= col_input3 + "<a href='#' onclick='viewObs("+nroFila+",16,3,\"\",\"\")' id='obs_"+nroFila+"_3' style='display:none'><i class='fa fa-envelope-o' aria-hidden='true'></i></a>  "; 
                      col_input3= col_input3 + "</th>";
           key = getKey(idFila,22);         
            var col_input4="<th style='width: 60px; padding:0;  border:1px solid #DCDCDCFF; text-align:center;' >";
                      col_input4= col_input4 + "    <input name='ejecu["+index+"][19]' value='' type='hidden'>                                                                                                  ";
                      col_input4= col_input4 + "    <input name='ejecu["+index+"][20]' value='2' type='hidden'>                                                                                                  ";
                      col_input4= col_input4 + "    <input name='ejecu["+index+"][21]' value='' type='hidden'>                                                                                                  ";
                      col_input4= col_input4 + "    <input name='ejecu["+index+"][22]' value='"+key+"' type='hidden'>                                                                                          ";
                      col_input4= col_input4 + "    <input name='ejecu["+index+"][42]' value='' type='hidden'>                                                                                                  ";
                      col_input4= col_input4 + "    <input  size='2' maxlength='5' name='ejecu["+index+"][23]' value='' onchange='disabledEnviar();viewSobre(\""+nroFila+"_4\",this);sumHoras("+index+")' type='text' onkeypress='return soloNumeros(event,this);'";
                        if(dia_act < key){
                          col_input4= col_input4 + " ";
                        }                             
                      col_input4= col_input4 + ">";
                      col_input4= col_input4 + "    <a href='#' onclick='viewObs("+nroFila+",21,4,\"\",\"\")' id='obs_"+nroFila+"_4' style='display:none'><i class='fa fa-envelope-o' aria-hidden='true'></i></a>                                                         ";
                      col_input4= col_input4 + "</th>";
            key = getKey(idFila,27);                                
            var col_input5="<th style='width: 60px; padding:0;  border:1px solid #DCDCDCFF; text-align:center;'>                                                                                                                          ";
                      col_input5= col_input5 + "  <input name='ejecu["+index+"][24]' value='' type='hidden'>                                                                                                ";
                      col_input5= col_input5 + "  <input name='ejecu["+index+"][25]' value='2' type='hidden'>                                                                                                ";
                      col_input5= col_input5 + "  <input name='ejecu["+index+"][26]' value='' type='hidden'>                                                                                                ";
                      col_input5= col_input5 + "  <input name='ejecu["+index+"][27]' value='"+key+"' type='hidden'>                                                                                        ";
                      col_input5= col_input5 + "  <input name='ejecu["+index+"][43]' value='' type='hidden'>                                                                                                ";
                      col_input5= col_input5 + "  <input size='2' maxlength='5' name='ejecu["+index+"][28]' value='' onchange='disabledEnviar();viewSobre(\""+nroFila+"_5\",this);sumHoras("+index+")' type='text' onkeypress='return soloNumeros(event,this);'";
                        if(dia_act < key){
                          col_input5= col_input5 + " ";
                        }                             
                      col_input5= col_input5 + ">";
                      col_input5= col_input5 + "  <a href='#' onclick='viewObs("+nroFila+",26,5,\"\",\"\")' id='obs_"+nroFila+"_5' style='display:none'><i class='fa fa-envelope-o' aria-hidden='true'></i></a>                                                       ";
                      col_input5= col_input5 + "</th>";
            key = getKey(idFila,32);                                  
            var col_input6= "<th style='width: 60px; padding:0;  border:1px solid #DCDCDCFF; text-align:center;' >";
                      col_input6= col_input6 + "  <input name='ejecu["+index+"][29]' value='' type='hidden'>                                                                                               ";
                      col_input6= col_input6 + "  <input name='ejecu["+index+"][30]' value='2' type='hidden'>                                                                                                ";
                      col_input6= col_input6 + "  <input name='ejecu["+index+"][31]' value='' type='hidden'>                                                                                                 ";
                      col_input6= col_input6 + "  <input name='ejecu["+index+"][32]' value='"+key+"' type='hidden'>                                                                                         ";
                      col_input6= col_input6 + "  <input name='ejecu["+index+"][44]' value='' type='hidden'>                                                                                                 ";
                      col_input6= col_input6 + "  <input  size='2' maxlength='5' name='ejecu["+index+"][33]' value='' onchange='disabledEnviar();viewSobre(\""+nroFila+"_6\",this);sumHoras("+index+")' type='text' onkeypress='return soloNumeros(event,this);'";
                        if(dia_act < key){
                          col_input6= col_input6 + " ";
                        }                             
                      col_input6= col_input6 + ">";
                      col_input6= col_input6 + "  <a href='#' onclick='viewObs("+nroFila+",31,6,\"\",\"\")' id='obs_"+nroFila+"_6' style='display:none'><i class='fa fa-envelope-o' aria-hidden='true'></i></a>                                                        ";
                     col_input6= col_input6 +  "</th>";
            key = getKey(idFila,37);                                  
            var col_input7="<th style='width: 60px; padding:0;  border:1px solid #DCDCDCFF; text-align:center;' >                                                                                                                           ";
                     col_input7= col_input7 +  "  <input name='ejecu["+index+"][34]' value='' type='hidden'>                                                                                                 ";
                      col_input7= col_input7 + "  <input name='ejecu["+index+"][35]' value='2' type='hidden'>                                                                                                 ";
                      col_input7= col_input7 + "  <input name='ejecu["+index+"][36]' value='' type='hidden'>                                                                                                 ";
                      col_input7= col_input7 + "  <input name='ejecu["+index+"][37]' value='"+key+"' type='hidden'>                                                                                         ";
                      col_input7= col_input7 + "  <input name='ejecu["+index+"][45]' value='' type='hidden'>                                                                                                 ";
                      col_input7= col_input7 + "  <input size='2' maxlength='5' name='ejecu["+index+"][38]' value='' onchange='disabledEnviar();viewSobre(\""+nroFila+"_7\",this);sumHoras("+index+")' type='text' onkeypress='return soloNumeros(event,this);'";
                        if(dia_act < key){
                          col_input7= col_input7 + " ";
                        }                             
                      col_input7= col_input7 + ">";
                      col_input7= col_input7 + "  <a href='#' onclick='viewObs("+nroFila+",36,7,\"\",\"\")' id='obs_"+nroFila+"_7' style='display:none'><i class='fa fa-envelope-o' aria-hidden='true'></i></a>";                                                          
                      col_input7= col_input7 + "</th>";
                      
            var col_total=  "<th style='width: 60px; border:1px solid #DCDCDCFF; font-size:11px;'><div id='totf_"+index+"'><center>0</center></div>";                                                                                                             
                      col_total= col_total + "</th>";

            /*var col_avance=  "<td  style='border:1px solid grey'>";
                      col_avance= col_avance + "</td>";*/
                      
            var col_acciones= "<th style='width: 22px; border:1px solid #DCDCDCFF;'>";
                      /*col_acciones = col_acciones + "<a href='#' onclick='clonarfila('10')' class='fa fa-plus'></a>&nbsp;&nbsp;&nbsp;&nbsp;       ";*/
                      col_acciones= col_acciones + "<a href='#' onclick='eliminarFila("+nroFila+","+index+")' class='fa fa-trash'></a>";
                      col_acciones = col_acciones + "</th>"; 



            var data  = ini_registro +  col_acciones + col_actividad + col_categoria + col_facturable + col_recu  + col_gp+  /*col_ent +*/ col_input1+  col_input2+  col_input3+ col_input4+ col_input5 + col_input6+ col_input7 + col_total/*+ col_avance*/+ fin_registro;
            //$("#tableHorasAprob tbody").append(data);
            //alert(idFila);
            $("#row_"+idFila).after(data);
            fillNoFacturables(nroFila);

        }
        function disabledEnviar(){
          
          $("#btnenviar").attr('disabled','true');
        }
        function viewObs(fila,col,dia,obs_pla,obs_apro){
          //obj = document.getElementsByName('ejecu['+col+']['+fila+']');
          obj = document.querySelector("[name='ejecu["+(fila-1)+"]["+(col)+"]']");
          document.getElementById('message-text').value=obj.value;
          i_col=col;
          i_fila=fila-1;
          i_dia=dia;
          if(obs_pla!=''){
            $('#obs_planificada').html(obs_pla);  
          }
          else{
             $('#obs_planificada').html('');  
          }
          if(obs_apro!=''){
            $('#obs_aprobacion').html(obs_apro);  
          }  
          else{
             $('#obs_aprobacion').html('');  
          }        
          $('#modalObs').modal('show');
          
        }
        function saveObs(){
            obj = document.querySelector("[name='ejecu["+i_fila+"]["+i_col+"]']");
            obj.value=document.getElementById('message-text').value;
            
            if(obj.value==''){
              $("#iobs_"+(i_fila+1)+"_"+i_dia).removeClass('glyphicon-cloud');
              $("#iobs_"+(i_fila+1)+"_"+i_dia).addClass('fa-envelope-o');
            }else{
              $("#iobs_"+(i_fila+1)+"_"+i_dia).removeClass('fa-envelope-o');
              $("#iobs_"+(i_fila+1)+"_"+i_dia).addClass('glyphicon-cloud');
            }
            $('#modalObs').modal('hide');
        }
        function getKey(fila,col){
          obj = document.querySelector("[name='ejecu["+(fila-1)+"]["+(col)+"]']");
          return obj.value;
        }
        $("#btnenviar").click(function (e){
            e.preventDefault();
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });

            var totalsem=parseFloat($("#totalsemana").val());
            var tipocont=parseFloat($("#tipocontrato").val());
            var semana_anio_ingreso  = $("#semana_anio_ingreso").val();

            if (semana_anio_ingreso == 'false') {

            if(totalsem < tipocont){
             
              // alert('No puede enviar su HT porque no completó las '+ tipocont +' horas necesarias');
              swal({
                  title: "No puede enviar su HT porque no completó las <b style='color:red'>"+ tipocont +"</b> horas necesarias",
                  text: "Hoja de tiempo",
                  type: "info",
                  timer: 5000,
                  html: true,
                  showConfirmButton: false
              });
              return;
            };
            };

            $.ajax({

                type:"POST",
                url:'enviarHoras',
                data: $("#frmHoras").serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     if(data=='OK'){
                      // alert("El HH se envio Correctamente");

                      swal({
                        title: "Enviando",
                        text: "Hoja de tiempo",
                        type: "success",
                        timer: 5000,
                        showConfirmButton: false
                      });
                      
                      /*if($("#tipoConsulta").val()=='P'){
                        var id = "row_" + $("#cproyecto").val();
                        selected.splice(0,selected.length);  
                        selected.push( id);
                        goEditar();
                        
                      }else{*/
                          //$("#btnTodos").click();
                          verTodos();
                     // }
                     }                     
                     
                     //$("#resultado").html(data);
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                }
            });          

        });
        $("#btnReabrir").click(function (e){
            e.preventDefault();
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            $.ajax({

                type:"POST",
                url:'reAbrirHoras',
                data: $("#frmHoras").serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     if(data=='OK'){
                      // alert("Se realizo la Reapertura, Correctamente");

                      swal({
                        title: "Se realizo la Apertura, Correctamente",
                        text: "Hoja de tiempo",
                        type: "success",
                        timer: 5000,
                        showConfirmButton: false
                      });
                      
                      /*if($("#tipoConsulta").val()=='P'){
                        var id = "row_" + $("#cproyecto").val();
                        selected.splice(0,selected.length);  
                        selected.push( id);
                        goEditar();
                        
                      }else{*/
                          //$("#btnTodos").click();
                          verTodos();
                     // }
                     }                     
                     
                     //$("#resultado").html(data);
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                }
            });          

        });        

        $('#frmHoras').on('submit',function(e){
            if(!validarNF()){
              // alert('Existe Filas NF, que no se especifico Actividad');
              swal({
                title: "Existe Filas NF, que no se especifico Actividad",
                text: "Hoja de tiempo",
                type: "info",
                timer: 5000,
                showConfirmButton: false
              });
              e.preventDefault(e);
              return false;
            }
            if(!validarTotales()){
              // alert('Se exceden las 24 horas por día');
              swal({
                title: "Se han excedido las <b style=color:red> 24 </b> horas por día",
                text: "Hoja de tiempo",
                type: "info",
                timer: 5000,
                html:true,
                showConfirmButton: false
              });
              return false;
            }
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            //$('input+span>strong').text('');
            /*$('input').parent().parent().removeClass('has-error');            
            $('select').parent().parent().removeClass('has-error');*/

            

            $.ajax({

                type:"POST",
                url:'grabarRegistroHoras',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     if(data=='OK'){
                      // alert("Información Guardada Correctamente");

                      swal({
                        title: "GUARDANDO",
                        text: "Hojas de tiempo",
                        type: "success",
                        timer: 5000,
                        showConfirmButton: false
                      });
                      
                      /*if($("#tipoConsulta").val()=='P'){
                        var id = "row_" + $("#cproyecto").val();
                        selected.splice(0,selected.length);  
                        selected.push( id);
                        goEditar();
                        
                      }else{*/
                          verTodos();
                      //}
                     }
                     //$("#resultado").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });      

        function getTreeActividad() {
          
          var tree = [
          <?php echo (isset($estructura)==1?$estructura:''); ?>
          
          ];          
          return tree;
        }  
        function getTreeActividadAdm() {
          
          var tree = [
          <?php echo (isset($estructuraAdm)==1?$estructuraAdm:''); ?>
          
          ];          
          return tree;
        }   
        function getTreeActividadTodosAdm() {
          
          var tree = [
          <?php echo (isset($estructuraTodosAdm)==1?$estructuraTodosAdm:''); ?>
          
          ];          
          //console.log('getree',tree)
          return tree;
        }
        
        function getTreeActividadTodosAdm2() {
          
          var gettree = [
          <?php echo (isset($estructuraTodosAdm2)==1?$estructuraTodosAdm2:''); ?>
          ];          
          //console.log('tree',gettree);
          return gettree;
        }

        function viewSobre(id,obj,id_obs){
          if($(obj).val()!=''){
            if($(obj).val() > 16){
              $(obj).val(16);
            }
          }
          cad = 'obs_'+id;
          if($(obj).val()==''){
            $("#"+cad).css("display","none");
            if($("#i"+cad).hasClass("glyphicon-cloud")){
              $("#i"+cad).removeClass("glyphicon");
              $("#i"+cad).removeClass("glyphicon-cloud");
              
            }
            if(!$("#i"+cad).hasClass("fa-envelope-o")){
              $("#i"+cad).addClass("fa");
              $("#i"+cad).addClass("fa-envelope-o");
            }
            obj_obs = $("#"+id_obs);
            obj_obs.val("");
          }else{
            $("#"+cad).css("display","block");
            if(!$("#i"+cad).hasClass("fa-envelope-o")){
              $("#i"+cad).removeClass("fa");
              $("#i"+cad).removeClass("fa-envelope-o");
              
            }            
            if(!$("#i"+cad).hasClass("glyphicon-cloud")){
              $("#i"+cad).addClass("glyphicon");
              $("#i"+cad).addClass("glyphicon-cloud");
              
            }


          }

        }
        /*function tipoBusquedad(obj){
          if ( $(obj).val()=='P' ){
            $("#Proy").show();
            $("#Todos").hide();
            $("#btnAgregarAdm").css('display','block');
            $("#btnAgregar").css('display','block');
            $("#btnAgregarAdmTodos").css('display','none');
            $("#btnAgregarTodos").css('display','none');
          }else{
            $("#Proy").hide();
            $("#Todos").show();
            $("#btnAgregarAdm").css('display','none');
            $("#btnAgregar").css('display','none');
            $("#btnAgregarAdmTodos").css('display','block');
            $("#btnAgregarTodos").css('display','block');            
          }
        }*/

        //$("#btnTodos").on('click',function(e){
        function verTodos(){
          //alert($('#divCalendario').val());        

          if($("#fecha").val()==''){
            alert('Ingrese fecha de consulta');
            return;
          }
          

          $.ajax({

              type:"POST",
              url:'editarTodosRegistrodeHoras',
              data:$("#frmHoras").serialize(),
              beforeSend: function () {
                  $('#div_carga').show(); 
          
              },
              success: function(data){
                    $('#div_carga').hide(); 
                    $("#resultado").html(data);
                    //$("#div_msg").show();
              },
              error: function(data){
                  $('#div_carga').hide();
                  //$('#detalle_error').html(data);
                  //$("#div_msg_error").show();
              }
          });          

        }
        function sumHoras(idFila){
          
          /* Sum Fila*/
            var dia1 =0;
            var dia2 =0;
            var dia3 =0;
            var dia4 =0;
            var dia5 =0;
            var dia6 =0;
            var dia7 =0;
            dia1 = $("input[name='ejecu["+idFila+"][8]']").val();
            dia2 = $("input[name='ejecu["+idFila+"][13]']").val();
            dia3 = $("input[name='ejecu["+idFila+"][18]']").val();
            dia4 = $("input[name='ejecu["+idFila+"][23]']").val();
            dia5 = $("input[name='ejecu["+idFila+"][28]']").val();
            dia6 = $("input[name='ejecu["+idFila+"][33]']").val();
            dia7 = $("input[name='ejecu["+idFila+"][38]']").val();

            dia1 = (dia1!='')?dia1:0;
            dia2 = (dia2!='')?dia2:0;
            dia3 = (dia3!='')?dia3:0;
            dia4 = (dia4!='')?dia4:0;
            dia5 = (dia5!='')?dia5:0;
            dia6 = (dia6!='')?dia6:0;
            dia7 = (dia7!='')?dia7:0;
            total_fila = parseFloat(dia1) + parseFloat(dia2) + parseFloat(dia3) + parseFloat(dia4) + parseFloat(dia5) + parseFloat(dia6) + parseFloat(dia7);

            $("#totf_"+idFila).text(total_fila);
            $("#totf_"+idFila).css('text-align','center');

          /* */
          var i=0;
          dia1 =0;
          dia2 =0;
          dia3 =0;
          dia4 =0;
          dia5 =0;
          dia6 =0;
          dia7 =0;      
          tot = 0;

          var eseliminado='';
          for(i=0;i< nroFila;i++){

            eseliminado = $("input[name='ejecu["+i+"][53]']").val();

           

            if(eseliminado=='N'){
               vdia1 = $("input[name='ejecu["+i+"][8]']").val();
                vdia2 = $("input[name='ejecu["+i+"][13]']").val();
                vdia3 = $("input[name='ejecu["+i+"][18]']").val();
                vdia4 = $("input[name='ejecu["+i+"][23]']").val();
                vdia5 = $("input[name='ejecu["+i+"][28]']").val();
                vdia6 = $("input[name='ejecu["+i+"][33]']").val();
                vdia7 = $("input[name='ejecu["+i+"][38]']").val();

                dia1 = parseFloat(dia1) + parseFloat((vdia1!='')?vdia1:0);
                dia2 = parseFloat(dia2) + parseFloat((vdia2!='')?vdia2:0);
                dia3 = parseFloat(dia3) + parseFloat((vdia3!='')?vdia3:0);
                dia4 = parseFloat(dia4) + parseFloat((vdia4!='')?vdia4:0);
                dia5 = parseFloat(dia5) + parseFloat((vdia5!='')?vdia5:0);
                dia6 = parseFloat(dia6) + parseFloat((vdia6!='')?vdia6:0);
                dia7 = parseFloat(dia7) + parseFloat((vdia7!='')?vdia7:0);
                tot = tot + parseFloat((vdia1!='')?vdia1:0) +parseFloat((vdia2!='')?vdia2:0) + parseFloat((vdia3!='')?vdia3:0);
                tot= tot + parseFloat((vdia4!='')?vdia4:0) + parseFloat((vdia5!='')?vdia5:0) + parseFloat((vdia6!='')?vdia6:0);
                tot = tot + parseFloat((vdia7!='')?vdia7:0);
            }

            else{
            }


           
          }
          $("#tot_1").text(dia1);
          $("#tot_1p").text(dia1);
          if(dia1 > 16){
            $("#tot_1").css('border','1px solid red');
            $("#tot_1p").css('border','1px solid red');
            $("#tot_1").css('text-align','center');
            $("#tot_1p").css('text-align','center');
          }else{
            $("#tot_1").css('border','0px solid white');
            $("#tot_1p").css('border','0px solid white');
            $("#tot_1").css('text-align','center');
            $("#tot_1p").css('text-align','center');
          }
          $("#tot_2").text(dia2);
          $("#tot_2p").text(dia2);
          if(dia2 > 16){
            $("#tot_2").css('border','1px solid red');
            $("#tot_2p").css('border','1px solid red');
            $("#tot_2").css('text-align','center');
            $("#tot_2p").css('text-align','center');
          }else{
            $("#tot_2").css('border','0px solid white');
            $("#tot_2p").css('border','0px solid white');
            $("#tot_2").css('text-align','center');
            $("#tot_2p").css('text-align','center');
          }          
          $("#tot_3").text(dia3);
          $("#tot_3p").text(dia3);
          if(dia3 > 16){
            $("#tot_3").css('border','1px solid red');
            $("#tot_3p").css('border','1px solid red');
            $("#tot_3").css('text-align','center');
            $("#tot_3p").css('text-align','center');
          }else{
            $("#tot_3").css('border','0px solid white');
            $("#tot_3p").css('border','0px solid white');
            $("#tot_3").css('text-align','center');
            $("#tot_3p").css('text-align','center');
          }          
          $("#tot_4").text(dia4);
          $("#tot_4p").text(dia4);
          if(dia4 > 16){
            $("#tot_4").css('border','1px solid red');
            $("#tot_4p").css('border','1px solid red');
            $("#tot_4").css('text-align','center');
            $("#tot_4p").css('text-align','center');
          }else{
            $("#tot_4").css('border','0px solid white');
            $("#tot_4p").css('border','0px solid white');
            $("#tot_4").css('text-align','center');
            $("#tot_4p").css('text-align','center');
          }          
          $("#tot_5").text(dia5);
          $("#tot_5p").text(dia5);
          if(dia5 > 16){
            $("#tot_5").css('border','1px solid red');
            $("#tot_5p").css('border','1px solid red');
            $("#tot_5").css('text-align','center');
            $("#tot_5p").css('text-align','center');
          }else{
            $("#tot_5").css('border','0px solid white');
            $("#tot_5p").css('border','0px solid white');
            $("#tot_5").css('text-align','center');
            $("#tot_5p").css('text-align','center');
          }          
          $("#tot_6").text(dia6);
          $("#tot_6p").text(dia6);
          if(dia6 > 16){
            $("#tot_6").css('border','1px solid red');
            $("#tot_6p").css('border','1px solid red');
            $("#tot_6").css('text-align','center');
            $("#tot_6p").css('text-align','center');
          }else{
            $("#tot_6").css('border','0px solid white');
            $("#tot_6p").css('border','0px solid white');
            $("#tot_6").css('text-align','center');
            $("#tot_6p").css('text-align','center');
          }          
          $("#tot_7").text(dia7);
          $("#tot_7p").text(dia7);
          if(dia7 > 16){
            $("#tot_7").css('border','1px solid red');
            $("#tot_7p").css('border','1px solid red');
            $("#tot_7").css('text-align','center');
            $("#tot_7p").css('text-align','center');
          }else{
            $("#tot_7").css('border','0px solid white');
            $("#tot_7p").css('border','0px solid white');
            $("#tot_7").css('text-align','center');
            $("#tot_7p").css('text-align','center');
          }          
          $("#tot").text(tot);
          $("#tot").css('text-align','center');
        }
        $('.select-box').chosen(
            {
                allow_single_deselect: true
        });                 
      
        function loadumineraT(cpersona){
          $('#clientesT').val(cpersona);
          
            $.ajax({

                type:"POST",
                url:'loadUmineraxCliente',
                data:$("#frmTareasTodos").serialize(),
                beforeSend: function () {
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide();
                     $("#treeActividadesTodos").html(""); 
                     $("#divProyT").html("");
                     $("#divUmineraT").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });  
        }
        /*function loadumineraTAdm(){
          var sel = $('#clientesTAdm').val();
          
            $.ajax({

                type:"POST",
                url:'loadUmineraxClienteAdm',
                data:$("#frmTareasTodosAdm").serialize(),
                beforeSend: function () {
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     $("#divUmineraTAdm").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });  
        }        */
        function loadProyT(um){
          $('#umineraT').val(um);
          
            $.ajax({

                type:"POST",
                url:'loadProyxCliente',
                data:$("#frmTareasTodos").serialize(),
                beforeSend: function () {
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide();
                     $("#treeActividadesTodos").html("");
//                     $("#divProyT").html(""); 
                     $("#divProyT").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });           
        }
        /*function loadProyTAdm(){
          var sel = $('#umineraTAdm').val();
          
            $.ajax({

                type:"POST",
                url:'loadProyxClienteAdm',
                data:$("#frmTareasTodosAdm").serialize(),
                beforeSend: function () {
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     $("#divProyTAdm").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });           
        }      */  
        function tareasTodos(cproy){
          $("#cproyecto").val(cproy)
            $.ajax({

                type:"POST",
                url:'verTareaProyectoTimeSheet',
                data:$("#frmTareasTodos").serialize(),
                beforeSend: function () {
                    selected_actividad_todos.splice(0,selected_actividad_todos.length);
                    $('#div_carga').show(); 
                },
                success: function(data){
                  
                     $('#div_carga').hide(); 
                     var tree = [];
                     res = "tree = [" + data + "]";
                     eval(res);
                    $('#treeActividadesTodos').highCheckTree({
                        data: tree,
                        onCheck: function (node){
                        selected_actividad_todos.push(node.attr('rel'));
                        },
                        onUnCheck: function(node){
                        var idx = $.inArray(node.attr('rel'),selected_actividad_todos);
                        if(idx != -1){
                            selected_actividad_todos.splice(idx,1);
                        }
                        }
                    });                      
                     //$("#divPrySel").html(data);

                     
                }
            });
        }  
        function eliminarFila(idFila, index) {



            swal({
                title: "Deseas eliminar la tarea",
                text: "Presione Si para eliminar",
                type: "info",
                html: true,
                showCancelButton: true,
                confirmButtonText: "Si",
                confirmButtonColor: "green",
                cancelButtonText: "Cancelar",
                closeOnConfirm: false,
                closeOnCancel: false 
              },
              function(isConfirm){
                if (isConfirm) {
                  swal({
                    title: "ELIMINADO",
                    text: "Hojas de tiempo",
                    type: "success",
                    timer: 1000,
                    showConfirmButton: false
                  });
                    $("#row_"+idFila).css("display","none");
                    $("input[name='ejecu["+index+"][53]']").val("S");
                    sumHoras(idFila);
                }
                else {
                  swal({
                    title: "CANCELADO",
                    text: "Hoja de tiempo",
                    type: "error",
                    timer: 1000,
                    showConfirmButton: false
                  });
                } 
              }
              );
          
        }
        /*
        function soloNumeros(obj){
            obj.value = (obj.value + '').replace(/[^0-9\.]/g, '');
          
        }*/

        function soloNumeros(e,obj) {

            // Backspace = 8, Enter = 13, ’0′ = 48, ’9′ = 57, ‘.’ = 46
            
            var field = obj;

            key = e.keyCode ? e.keyCode : e.which;
            
            if (key == 8 || key ==9 || key ==11 || (key > 47 && key < 58)) return true;
            if (key > 47 && key < 58) {
              if (field.value === "") return true;
              var existePto = (/[.]/).test(field.value);
              
              if (existePto === false){
                  regexp = /.[0-9]{2}$/; //PARTE ENTERA 10
              }
              else {
                regexp = /.[0-9]{2}$/; //PARTE DECIMAL2
              }
              return !(regexp.test(field.value));
            }
            if (key == 46) {
              if (field.value === "") return false;
              regexp = /^[0-9]+$/;
              return regexp.test(field.value);
            }

            return false;

        }        

        function validarNF(){
          res=true;
          nrof = nroFila;
          for(var i=1;i<=nrof;i++){
            if ($("#removeAct_"+i).val()=='N'){
              if ($('#nfac_'+i).length) {
                // si existe
                if($("#tipo_"+i).val()=='2'){
                  //NF
                  if($("#nfac_"+i).val()==''){
                    return false;
                  }
                }
                
              }           
            }
          }
          return res;
        }

        function validarTotales(){
          divObj = document.getElementById("tot_1");
          if(parseFloat(divObj.innerText) > 24){
            return false;
          }
          divObj = document.getElementById("tot_2");
          if(parseFloat(divObj.innerText) > 24){
            return false;
          }
          divObj = document.getElementById("tot_3");
          if(parseFloat(divObj.innerText) > 24){
            return false;
          }
          divObj = document.getElementById("tot_4");
          if(parseFloat(divObj.innerText) > 24){
            return false;
          }
          divObj = document.getElementById("tot_5");
          if(parseFloat(divObj.innerText) > 24){
            return false;
          }
          divObj = document.getElementById("tot_6");
          if(parseFloat(divObj.innerText) > 24){
            return false;
          }           
          divObj = document.getElementById("tot_7");
          if(parseFloat(divObj.innerText) > 24){
            return false;
          }                                                 
          return true;
        }


    /* Inicio Somnbrear los input al hacer click*/
    $("input[type=text]").focus(function(){    
      this.select();
    });
    /* Fin Somnbrear los input al hacer click*/

    $('div#resumen').hover(function(e) {
      $('div#dash_semana').show().css('top', e.pageY)
      .css('left', e.pageX)
      .appendTo('body');
    }, function() {
      $('div#dash_semana').hide();
    });   
    
    function mouseOver(id){
      $("#"+id).show();
    }
    function mouseOut(id){
      $("#"+id).hide();
    }    
 //alert($("#totalsemana").val());




        


</script>  
