@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
      <h1>
        Solicitud de Capacitación y/o Evaluación        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">          

	<div class="col-md-6" style="margin-top: 25px;" >
    <p>Se ha registrado correctamente y se notificará a los usuarios que van a aprobar la capacitación. Luego, que la aprobación de todos sea positiva, los usuarios seleccionados visualizarán la capacitación y de ser el caso la evaluación a rendir.</p>
  </div>

</section>

</div>

@stop
