@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
      <h1>
        Listado de Usuarios
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
</section>

    <!-- Main content -->
<section class="content">          

	<div class="col-md-11">  
    <p><b>Capacitación:</b> {{ $capacitacion->titulo }}</p>
    <p><b>Registrada por:</b> {{ $capacitacion->nombre }} </p>
    <p><b>Fecha de Registro:</b> {{ $capacitacion->created_at }} </p>
  </div>

  <div class="col-md-8">  
  
    <table class="table table-striped table-hover" >
      <tr>
        <th>#</th>
        <th>Usuario</th>
        <th>Acciones</th>
      </tr>
      @foreach ($usuarios as $index => $usuario)      
      
      <tr>      
        <td>{{ $usuario->n }} </td>
        <td>{{ $usuario->nombre }} </td>
        <td>
          <a href="{{ url('capacitacion/'.$capacitacion->id.'/retirar/usuario/'.$usuario->cusuario) }}" title="Retirar de Capacitación"> <i class="fa fa-times"></i></a>
        </td>
      </tr>
      
      @endforeach
    </table>

    <a href="{{ url('capacitaciones/propuestas') }}">Regresar</a>
  
  </div>

</section>

</div>

@stop
