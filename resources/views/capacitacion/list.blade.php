@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
      <h1>
        Listado de Capacitaciones Propuestas      
      </h1>
</section>

    <!-- Main content -->
<section class="content">          

	<div class="col-md-12">  
  
    <table class="table table-striped table-hover" >
      <tr>
        <th>#</th>
        <th>Titulo</th>        
        <th>Código</th>
        <th>Evaluación?</th>
        <th>F. Registro</th>
        <th>Estado</th>
        <th>Acciones</th>
      </tr>
      @foreach ($capacitaciones as $index => $element)
      
      @if($element->aprobar==true)
      <tr>
      @else
      <tr class="">
      @endif
        <td>{{ $element->n }} </td>        
        <td>{{ $element->titulo }} </td>
        <td>{{ $element->codigo }} </td>
        <td>
          @if($element->cantidad_evaluacion > 0)          
            <a href="{{ url('capacitacion/'.$element->id.'/editarEvaluacion/') }}" class="btn btn-primary"> <i class="fa fa-edit" title="Aprobar la capacitación" ></i> Si, ver</a>
          @else
            No 
          @endif          
        </td>
        <td>{{ $element->created_at }} </td>
        <td>{{ $element->estado_descripcion }} </td>                
        <td>          
          <a href="{{ url('capacitaciones/'.$element->id.'/ver') }}" class="btn btn-primary" title="Ver la información de la capacitación."><i class="fa fa-search"></i> ver</a> | 
          <a href="{{ url('capacitaciones/'.$element->id.'/usuarios') }}" class="btn btn-primary" title="Ver a los usuarios registrados" > <i class="fa fa-user"></i> usuarios</a>
          @if($element->estado==1 and $element->cantidad_evaluacion > 0)
          | 
          <a href="{{ url('capacitaciones/'.$element->id.'/usuarios/evaluados') }}" class="btn btn-primary" title="Ver a los usuarios con relación a la evaluación." > <i class="fa fa-user "></i> 
          evaluados</a>
          @endif
           | 
          @if($element->aprobar==true)
          <a href="{{ url('capacitacion/'.$element->id.'/aprobar') }}" class="btn btn-primary"> <i class="fa fa-check" title="Aprobar la capacitación" ></i> aprobar</a>
          @endif

          <a href="#" class="btn btn-danger"> <i class="fa fa-close" title="Cancelar la capacitación" ></i> cancelar</a>

        </td>
      </tr>
      
      @endforeach
    </table>

  </div>
  </div>

</section>

</div>

@stop
