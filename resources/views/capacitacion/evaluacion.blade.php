<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Evaluación</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<style type="text/css">
		body{
			background-color: #d3dbed;
		}
		#contenido{
			font-size: 13px;
			background-color: floralwhite;        
			margin-top: 20px;
			border: 2px solid #3e3e4680;
			border-radius: 5px;
			margin-bottom: 30px;
			padding-bottom: 20px;
		}
	</style>

</head>

<body>

<input type="hidden" id="_redirect" value="{{ (isset($_GET['_redirect'])) ? $_GET['_redirect'] : 'capacitaciones' }}">
<input type="hidden" id="cantidad_preguntas" value="{{ $evaluacion->cantidad_preguntas }}">
<input type="hidden" id="evaluacion_id" value="{{ $evaluacion->id }}">
<input type="hidden" id="opcion" value="{{ ($capacitacion->usuario_id == $usuario_id) ? 'guardar' : 'ver' }}">

<div class="container">      

	<div class="content-wrapper" style="min-height: 853px;">

		<section class="content">

			<div class="col-md-12" id="contenido">
							<!-- Horizontal Form -->
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title" style="text-align: center" >Evaluación</h3>
								</div>

								<p><b>Capacitación:</b> {{ $capacitacion->titulo }}</p>
								<p><b>Motivo:</b> {{ $capacitacion->motivo }}</p>								
								
								@if($capacitacion->usuario_id == $usuario_id)
								<p><b>Indicaciones:</b> Escribir al costado del número la pregunta y luego debajo las alternativas. Puede tener 1 o más alternativas correctas en el momento que le hace click al costado izquierdo de cada alternativa.</p>
								<p>Puede también registrar la evaluación en otro momento.</p>
								@endif

				@if($capacitacion->usuario_id == $usuario_id)
				<form id="form" class="form-horizontal" action='{{ url("evaluacion/$id/registrar") }}' method="post"  >
				@else
				<form id="" class="form-horizontal" action="#">
				@endif

									{{ csrf_field() }}
									
								<div class="grupo example" style="display:none">
									
									<div class="row pregunta">
										<div class="col-sm-1 numero"  style="text-align: right;" ></div>
										<div class="col-sm-9 descripcion">
											<input type="text" class="form-control" name="txtp[]" >
										</div>                            
										@if($capacitacion->usuario_id == $usuario_id)
										<div class="col-sm-2 opciones">
											<button class="btn btn-default btn-agregar" title="Agregar">+</button>
											<button class="btn btn-default btn-retirar" title="Retirar">X</button>
										</div>
										@endif
									</div>

									<div class="row alternativa" style="margin-top: 15px" >
										<div class="col-sm-1 col-sm-offset-1 chk" style="text-align: right;" >
											<input type="checkbox">
										</div>
										<div class="col-sm-8 descripcion">
											<input type="text" class="form-control ">
										</div>
										@if($capacitacion->usuario_id == $usuario_id)
										<div class="col-sm-2 opciones">
											<button class="btn btn-default btn-agregar" title="Agregar">+</button>
											<button class="btn btn-default btn-retirar" title="Retirar">X</button>
										</div>
										@endif
									</div>

									<hr>
								</div>								
																
									<div class="box-footer"  style="text-align: center" >										
										@if($capacitacion->usuario_id == $usuario_id)
										<button class="btn btn-primary" id="btn-guardar" type="submit">Guardar</button>
										@else
										<button class="btn btn-primary" disabled>Guardar</button>
										@endif
										<a class="btn btn-danger" href="{{ url('capacitaciones/propuestas') }}"> Cancelar </a>
									</div>
									
				</form>
							</div>
							
			</div>
		</section>

	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalConfimacion">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Registro de Evaluación</h4>
      </div>
      <div class="modal-body">
        <p><span id="msjRespuesta"></span></p>
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-primary" id="btn-aceptar">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('js/capacitacion_evaluacion.js') }}"></script>
<script>
	var BASE_URL = "<?php echo url('/'); ?>";
</script>

</body>
</html>
