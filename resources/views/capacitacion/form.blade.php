@extends('layouts.master')
@section('content')

<input type="hidden" id="capacitacion_id" value="{{ (isset($id)) ? $id : '' }}">

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
  <h1>
    Solicitud de Capacitación 
  </h1>
</section>

<section class="content" style="font-size: 12px" style="text-align: center;">          

<div class="col-md-8">            
  <form class="form-horizontal" action="{{ url('capacitacion') }}" method="post" >

              {{ csrf_field() }}
              
              <div class="box-body">

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Titulo <span class="span-obligatorio" title="Campo Obligatorio" >*</span></label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="titulo" name="txttitulo" required 
                    placeholder="¿Cuál es el Título de la Capacitación?" >
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Motivo <span class="span-obligatorio" title="Campo Obligatorio" >*</span> </label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="motivo" name="txtmotivo" required="" 
                    placeholder="¿Cuál es el Motivo de la capacitación?. Sirve para fundamentar tu respuesta a los aprobadores.">
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Aprobadores</label>
                  <div class="col-sm-9">                    
                    <input size="50" class="form-control" name="aprobadores" id="aprobadores" 
                    placeholder="¿Quiénes son las personas que van a aprobar tu capacitación?" >
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Personas</label>
                  <div class="col-sm-9">                    
                    <input size="50" class="form-control" name="personas" id="personas"
                    placeholder="¿Quiénes son las personas o áreas que visualizarán la capacitación?">
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Videos <span class="span-obligatorio" title="Campo Obligatorio" >*</span></label>

                  <div class="col-sm-9">
                    <textarea class="form-control" name="txtvideos" required rows="5" id="videos" ></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">F. Disponible </label>
                  <div class="col-sm-4">
                    <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control pull-right" id="fecha_disponible" type="text" name="txtfechadisponible" placeholder="dd/mm/aaaa">
                  </div>
                  </div>

                  <div class="col-sm-1" >
                    <i class="fa fa-question-circle" style="font-size: 17px" 
                    title="Fecha Opcional desde que estará disponible para visualizar la capacitación." ></i>
                  </div>
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Evaluación</label>

                  <div class="col-sm-9">
                    <input type="checkbox" id="chkevaluacion" name="chkevaluacion"> Incluir Evaluación
                  </div>
                </div>               

              <div id="divevaluacion" style="display:none" >               

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Preguntas <span class="span-obligatorio" title="Campo Obligatorio" >*</span></label>
                  <div class="col-sm-2">
                    <input type="text" class="form-control" placeholder="10" name="txtcantidadpreguntas" id="cantidadpreguntas" 
                    title="Si la cantidad es mayor de 20, solo se mostrarán 20 preguntas aleatorias.">
                  </div>

                  <label for="" class="col-sm-2 control-label">Duracion</label>

                  <div class="col-sm-2">
                    <input type="text" class="form-control" placeholder="30" name="txtduracion" id="duracion" 
                    title="Duración en minutos de la evaluación." >
                  </div>
                  <div class="col-sm-2">
                    <label for="" class="col-sm-1 control-label"><i> (minutos)</i></label>                     
                  </div>
                  
                </div>

                <div class="form-group">
                  <label for="" class="col-sm-3 control-label">Objetivo</label>

                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="" placeholder="¿Cuál es el objetivo de la Capacitación?. Esto lo visualizarán los usuarios." name="txtobjetivo" id="objetivo">
                  </div>
                </div>

                <div class="form-group">                
                  <label for="" class="col-sm-3 control-label">F. Inicio</label>

                  <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control pull-right" id="fecha_inicio" type="text" name="txtfechainicio" placeholder="dd/mm/aaaa"
                    title="Fecha desde que estará disponible la evaluación para los usuarios.">
                  </div>
                  </div>

                  <label for="" class="col-sm-2 control-label">F. Fin</label>

                  <div class="col-sm-3">
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input class="form-control pull-right" id="fecha_fin" type="text" name="txtfechafin" placeholder="dd/mm/aaaa"
                    title="Fecha máxima para que puedan rendir la evaluación.">
                  </div>
                  </div>                
                </div>

                

              </div>

               <div class="form-group">                  
                  @if($opcion!="ver")
                  <div class="col-sm-3 col-sm-offset-2">
                    <button type="submit" class="btn btn-primary">Solicitar</button>
                  </div>
                  @endif
                  <div class="col-sm-3 col-sm-offset-2">
                    <a href="{{ url('capacitaciones/propuestas') }} " >Regresar</a>
                  </div>
                </div>

              </div>
  </form>
</div>

@if ($opcion !="ver")
<div class="col-md-3" >  

   <div class="form-group row">
    <label for="" class="col-md-6 control-label">Plantilla Código: 
      <i class="fa fa-question-circle" style="font-size: 17px" 
      title="Código de una evaluación existente para completar información." ></i></label>

    <div class="col-sm-6"> 
      <input type="text" class="form form-control" id="codigoPlantilla" >
    </div>
  </div>
  <div class="row" >
    <div class="col-md-6 col-md-offset-6">
      <button class="btn btn-primary" id="btn-obtenerPlantilla" >Obtener</button>    
    </div>
  </div>

</div>
@endif

</section>

</div>

<div class="modal fade" id="modalMensajePlantilla" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Plantilla</h4>
      </div>
      <div class="modal-body">
        <span id="mensajePlantilla" ></span>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script type="text/javascript">

  $( document ).ready(function() {
    
    $("#chkevaluacion").click(function(){

      if( $("#chkevaluacion").prop("checked") == true )
        $("#divevaluacion").show();
      else
        $("#divevaluacion").hide();

    });

    $("#btn-obtenerPlantilla").click(function(){
      obtenerPlantilla();
    });


  $.get( BASE_URL + '/obtenerPersonas', { })
    .done(function( data ) {    
    Personas(data);
  });

  $.get( BASE_URL + '/obtenerAprobadores', { })
    .done(function( data ) {    
    Aprobadores(data);
  });

  });
  
</script>

<script>

function Personas(data){

    var availableTags = data;

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#personas" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
}

function Aprobadores(data){

    var availableTags = data;

    function split( val ) {
      return val.split( /,\s*/ );
    }
    function extractLast( term ) {
      return split( term ).pop();
    }
 
    $( "#aprobadores" )
      // don't navigate away from the field on tab when selecting an item
      .on( "keydown", function( event ) {
        if ( event.keyCode === $.ui.keyCode.TAB &&
            $( this ).autocomplete( "instance" ).menu.active ) {
          event.preventDefault();
        }
      })
      .autocomplete({
        minLength: 0,
        source: function( request, response ) {
          // delegate back to autocomplete, but extract the last term
          response( $.ui.autocomplete.filter(
            availableTags, extractLast( request.term ) ) );
        },
        focus: function() {
          // prevent value inserted on focus
          return false;
        },
        select: function( event, ui ) {
          var terms = split( this.value );
          // remove the current input
          terms.pop();
          // add the selected item
          terms.push( ui.item.value );
          // add placeholder to get the comma-and-space at the end
          terms.push( "" );
          this.value = terms.join( ", " );
          return false;
        }
      });
}

function obtenerPlantilla()
{
  var codigo = $("#codigoPlantilla").val();
  $.get( BASE_URL + '/obtenerPlantilla/' + codigo, {})
    .done(function( data ) {      
      mostrarPlantilla(data.capacitacion, data.msj);
  });
}

function mostrarPlantilla(capacitacion, msj)
{
  $("#mensajePlantilla").text(msj);
  $("#modalMensajePlantilla").modal("show");

  if(capacitacion==null)
    return;

  $("#codigoPlantilla").val("");

  $("#motivo").val(capacitacion.motivo);
  $("#personas").val(capacitacion.personas);
  $("#videos").text(capacitacion.videos);
  $("#aprobadores").val(capacitacion.aprobadores);
  $("#titulo").val(capacitacion.titulo);
}

  </script>

@if ($opcion =="ver")
<script>

$( document ).ready(function() {
  
  obtenerCapacitacion();

});

function obtenerCapacitacion()
{
  var id = $("#capacitacion_id").val();

  $.get( BASE_URL + '/obtenerInfoCapacitacion/' + id, {})
    .done(function( data ) {    
      mostrar(data);
  });
}

function mostrar(c)
{
    $("textarea").prop("readonly", true);
    $("input").prop("readonly", true);

    $("#titulo").val(c.titulo);
    $("#motivo").val(c.motivo);
    $("#aprobadores").val(c.aprobadores);
    $("#personas").val(c.personas);
    $("#videos").text(c.videos);
    $("#fecha_disponible").val(c.fecha_inicio);
    
    if(c.evaluacion!=null)
    {
      var e = c.evaluacion;
      $("#chkevaluacion").prop("checked", true);
      $("#divevaluacion").show();

      $("#objetivo").val(e.objetivo);
      $("#fecha_inicio").val(e.fecha_inicio);
      $("#fecha_fin").val(e.fecha_fin);
      $("#cantidadpreguntas").val(e.cantidad_preguntas);      
    }
 
}

</script>
@endif

@stop
