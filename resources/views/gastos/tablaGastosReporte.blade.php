<table class="table table-striped table-hover table-bordered" id="tablaGastos" width="100%" style="font-size: 12px;">

    <thead>
        <tr class="clsCabereraTabla">
            <th>Item</th>
            <th>Proyecto</th>
            <th>Área</th>
            <th>Nombre gasto</th>
            <th>Asignado</th>
            <th>Venta</th>
            <th>Monto Total</th>
            <th>Monto IGV</th>
            <th>usd_$$</th>
            <th>Moneda</th>
            <th>Fecha rendición</th>
            <th>Fecha factura</th>
            <th>N° factura</th>
            <th>Proveedor</th>
            <th>Descripción</th>
            <th>Nombre rendición</th>
        </tr>
    </thead>
   
    <tbody>
         @if(isset($arrayGastos))
            @foreach($arrayGastos as $key => $g)

            <tr>
                <td>{{$key +1}}</td>
                <td>{{ $g['proyecto'] }}</td>
                <td>{{ $g['area'] }}</td>
                <td>{{ $g['concepto'] }}</td>
                <td>{{ $g['asignado'] }}</td>
                <td>{{ $g['venta'] }}</td>
                <td>{{ $g['mon_total'] }}</td>
                <td>{{ $g['subtotal'] }}</td>
                <td>{{ $g['usd'] }}</td>
                <td>{{ $g['moneda'] }}</td>
                <td>{{ $g['fechaRendicion'] }}</td>
                <td>{{ $g['fechaFactura'] }}</td>
                <td>{{ $g['numeroFactura'] }}</td>
                <td>{{ $g['proveedor'] }}</td>
                <td>{{ $g['descripcion'] }}</td>
                <td>{{ $g['gastocab'] }}</td>

            </tr>

            @endforeach 
        @endif 
       
    </tbody>
    <tfoot>

        <tr>
                <th colspan="6" style="text-align:right">Total:</th>
                <th colspan="2"></th>
            </tr>
        
    </tfoot>

</table>

<script>
   
    var tablaGastos = $('#tablaGastos').DataTable( {
        "paging":   false,
        "ordering": true,
        "oLanguage": {
            "sInfo": 'Se encontraron _END_ registros.',
            "sInfoEmpty": 'No se encontraron registros',
            "sEmptyTable": 'No se encontraron registros',
        },
        lengthChange: false,
        buttons: [ 
                    { text: 'Exportar a Excel',extend: 'excel', footer: true },
                    { text: 'Exportar a PDF',extend: 'pdf',orientation: 'landscape',pageSize: 'A3', footer: true },
                    { text: 'Ocultar columnas',extend: 'colvis', footer: true }

                 ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false, 


        // initComplete: function () {
        //     this.api().columns().every( function () {
        //         var column = this;
        //         var select = $('<select><option value=""></option></select>')
        //             .appendTo( $(column.footer()).empty() )
        //             .on( 'change', function () {
        //                 var val = $.fn.dataTable.util.escapeRegex(
        //                     $(this).val()
        //                 );
 
        //                 column
        //                     .search( val ? '^'+val+'$' : '', true, false )
        //                     .draw();
        //             } );
 
        //         column.data().unique().sort().each( function ( d, j ) {
        //             select.append( '<option value="'+d+'">'+d+'</option>' )
        //         } );
        //     } );
        // },

        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            total = api
                .column( 6 )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Total over this page
            pageTotal = api
                .column( 6, { page: 'current'} )
                .data()
                .reduce( function (a, b) {
                    return intVal(a) + intVal(b);
                }, 0 );
 
            // Update footer
            $( api.column( 6 ).footer() ).html(
                'S/.'+pageTotal +' ( S/.'+ total +' total)'
            );
        }

     } ); 
 
    tablaGastos.buttons().container()
        .appendTo( '#tablaGastos_wrapper .col-sm-6:eq(0)' );







</script>