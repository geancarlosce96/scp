<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gastos
        <small>Aprobación de Rendiciones Usuario</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Gastos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">
    {!! Form::open(array('url' => 'viewAprobacionRendicion','method' => 'POST','id' =>'frmgasto')) !!}


   <!--  <div class="row ">	
        <div class="col-lg-2 col-xs-6"></div>
        <div class="col-lg-4 col-xs-6">
                                         
        </div>
        <div class="col-lg-2 col-xs-6">Colaborador:</div>
        <div class="col-lg-4 col-xs-6">{!! Form::select('cpersona_empleado',(isset($personal)==1?$personal:array() ),'',array('class' => 'form-control select-box','id'=>'ipersonal')) !!}   </div>
    </div> -->
    <?php /*
    <div class="row" >	
        @include('partials.verProyAprobacionGasto',array('tproyecto'=>(isset($tproyecto)==1?$tproyecto:null) ))

        <div class="col-lg-2 col-xs-2">Área:</div>
        <div class="col-lg-4 col-xs-9">{!! Form::select('carea',(isset($areas)==1?$areas:null),'',array('class' => 'form-control select-box','id'=>'carea')) !!}</div>
            
    </div>*/ ?>

    <div class="row ">	
        <div class="col-lg-2 col-xs-2">Fechas:</div>
        <div class="col-lg-2 col-xs-2">
            <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right input-sm" autocomplete="off" id="fdesde">
            </div>    
        </div>
        <div class="col-lg-2 col-xs-2">
            <div class="input-group date">
            <div class="input-group-addon">
                <i class="fa fa-calendar"></i>
            </div>
            <input type="text" class="form-control pull-right input-sm" autocomplete="off" id="fhasta">
            </div>    
        </div>
        <div class="col-lg-2 col-xs-2">Colaborador:</div>
        <div class="col-lg-2 col-xs-2">{!! Form::select('cpersona_empleado',(isset($personal)==1?$personal:array() ),'',array('class' => 'form-control select-box','id'=>'ipersonal')) !!}   </div>    
        <div class="col-lg-2 col-xs-2"></div>
        <div class="col-md-2  col-xs-2">
            <button type="button" class="btn btn-primary btn-sm" id="btnView"> <b>Visualizar</b></button>
        </div>
    </div>

    <br>
        

    <!-- <div class="row">	
        <div class="col-md-11 col-xs-2"></div>
        <div class="col-md-1  col-xs-2">
            <button type="button" class="btn btn-primary btn-sm" id="btnView"> <b>Visualizar</b></button>
        </div>
    </div> -->  


    <div class="row ">	
        <div class="col-lg-12 col-xs-12 table-responsive"  id="divApRendicion">
        @include('partials.tableAprobacionRendicionGastos',array('listaAprobacion'=> (isset($listaAprobacion)==1?$listaAprobacion:array() )))
        </div>
    </div>  
    <div class="row ">
        <div class="col-lg-3 hidden-xs"></div>
        <div class="col-lg-3 col-xs-6">
            <button type="button"  class="btn btn-primary" id="btnAprobar">Aprobar Rendiciones</button>
        </div>
        <div class="col-lg-3 col-xs-6">
            <button type="button" class="btn btn-primary " id="btnObservar">Observar Rendiciones</button>
        </div>
        <div class="col-lg-3 hidden-xs"></div>    
    </div>        
  
    {!! form::close() !!}

    </div><!-- fin parte central -->
    
    <!--<div class="col-lg-1 hidden-xs"></div>-->

</div>
</section>
@include('partials.searchProyecto')
    <div id="modalGasto">
        @include('partials.modalVerGasto')
    </div>
</div>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
    /*var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';        

    var table=$("#tProyecto").DataTable({
        "processing":true,
        "serverSide": true,

        "ajax": {
            "url": "listarProyectos",
            "type": "GET",
        },
        "columns":[
            {data : 'cproyecto', name: 'tproyecto.cproyecto'},
            {data : 'codigo', name: 'tproyecto.codigo'},
            {data : 'cliente' , name : 'tper.nombre'},
            {data : 'uminera' , name : 'tu.nombre'}, 
            {data : 'nombre' , name : 'tproyecto.nombre'}

        ],
        "language": {
            "lengthMenu"    : "Mostrar _MENU_ registros por página",
            "zeroRecords"   : "Sin Resultados",
            "info"          : "Página_PAGE_ de _PAGES",
            "infoEmpty"     : "No existe registros disponibles",
            "infoFiltered"  : "(filtrado de un_MAX_ total de registros)",
            "search"        : "Buscar:",
            "processing"    :  "Procesando...",
            "paginate"      : {
                "first"     :    "Inicio",
                "last"      :    "Ultimo",
                "next"      :     "Siguiente",
                "previous"  :   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });
    $('#tProyecto tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);             
        if ( index === -1 ) {
            selected.push( id );

        } 
    
        $(this).toggleClass('selected');
    } );     
    $('#searchProyecto').on('hidden.bs.modal', function () {
        goVerProy();
    });   

       
    function goVerProy(){
        var id =0;
        if (selected.length > 0 ){
            id = selected[0];


            $.ajax({

            url:'viewProyRendicion/'+id.substring(4),
            type: 'GET',
            beforeSend: function(){
                $('#div_carga').show(); 
            },
            success: function(data){
                $("#bproye").html(data);
                $('#div_carga').hide(); 
                
            },            
        });              

        }else{
            $('.alert').show();
        }
    }*/
        function verGastoDet(cgastoejecucion){
            $.ajax({

                type:"POST",
                url:'verDetalleGasto',
                data:{
                    "_token": "{{ csrf_token() }}",
                    cgastosejecucion: cgastoejecucion,

                },
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#modalGasto").html(data);
                     //$("#div_msg").show();
                     $("#viewGasto").modal('show');
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });            
            
        }    
     $('#fdesde').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fhasta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });          

    $('.select-box').chosen(
    {
        allow_single_deselect: true
    });


    $("#btnView").click(function (e){

        $.ajax({
            url: 'viewAprobacionRendicion',
            type: 'GET',
            
            data: /*"cproyecto="+$("#tproyecto").val() +*/"cpersona_empleado="+$("#ipersonal").val() /*+"&carea="+$("#carea").val()*/+"&fhasta="+ $("#fhasta").val() + "&fdesde=" + $("#fdesde").val(),
            beforeSend: function () {
                $('#div_carga').show(); 
            },              
            success : function (data){
                $("#divApRendicion").html(data);
                $('#div_carga').hide(); 
            },
        });
    });

    /*function cleanProyecto(){
        $("#tproyecto").val("");
        $("#cproyecto").val("");
     
    }*/
    function aprobarGasto(){
        var checks = getSeleccionadosDetalle();
        $.ajax({
            url: 'aprobarGastoDetalle',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                cgastosejecuciondet: checks
            },
            beforeSend: function () {
                $('#div_carga').show(); 
            },              
            success : function (data){
                //$("#divApRendicion").html(data);
                $('#div_carga').hide(); 
                $('#viewGasto').modal('hide');
                $("#btnView").trigger('click');
            },
        });        
    }
    function observarGasto(){
        var checks = getSeleccionadosDetalle();
        $.ajax({
            url: 'observarGastoDetalle',
            type: 'POST',
            data: {
                "_token": "{{ csrf_token() }}",
                cgastosejecuciondet: checks
            },
            beforeSend: function () {
                $('#div_carga').show(); 
            },              
            success : function (data){
                //$("#divApRendicion").html(data);
                $('#div_carga').hide(); 
                $('#viewGasto').modal('hide');
                $("#btnView").trigger('click');
            },
        }); 
    }
    $("#btnAprobar").on('click',function(){
        var checks = getSeleccionados();
        $.ajax({
            url: 'aprobarGastoDetalleSel',
            type: 'POST',
            data : {
                "_token": "{{ csrf_token() }}",
                checks: checks,
            },            
            beforeSend: function () {
                $('#div_carga').show(); 
            },              
            success : function (data){
                //$("#divApRendicion").html(data);
                $('#div_carga').hide(); 
                $("#btnView").trigger('click');
            },
        });
        
    });
    $("#btnObservar").on('click',function(){
        var checks = getSeleccionados();
        $.ajax({
            url: 'observarGastoDetalleSel',
            type: 'POST',
            data : {
                "_token": "{{ csrf_token() }}",
                checks: checks,
            },
            beforeSend: function () {
                $('#div_carga').show(); 
            },              
            success : function (data){
                //$("#divApRendicion").html(data);
                $('#div_carga').hide(); 
                $("#btnView").trigger('click');
            },
        });
    });    
    function getSeleccionados(){
        var selected = '';    
        $('#frmgasto input[type=checkbox]').each(function(){
            if (this.checked) {
                selected += $(this).val()+', ';
            }
        }); 
        var checks = selected.split(',');
        checks.pop();
        return checks;
    }
    function getSeleccionadosDetalle(){
        var selected = '';    
        $('input:checkbox[name=cgastosejecuciondet]').each(function(){
            if (this.checked) {
                selected += $(this).val()+', ';
            }
        }); 
        var checks = selected.split(',');
        checks.pop();
        return checks;
    }    
</script>