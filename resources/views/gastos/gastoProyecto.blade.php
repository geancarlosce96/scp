<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gastos
        <small>Rendición de Gastos por Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Gastos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    
    <div class="col-lg-12 col-xs-12">

        {!! Form::open(array('url' => 'saveGastoProy','method' => 'POST','id' =>'frmgasto','class'=>'form-horizontal')) !!}
        {!! Form::hidden('cgastosejecucion',(isset($gasto->cgastosejecucion)==1?$gasto->cgastosejecucion:''),array('id'=>'cgastosejecucion')) !!}  

        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}   
        {!! Form::hidden('numtabla','',array('id'=>'numtabla')) !!}

        <div class="row clsPadding2">
            
            <div class="col-lg-2 col-xs-2 clsPadding">
                <button type='button' class="btn btn-primary btn-block" id="btnNew"> <b>Nuevo</b></button>
            </div>
            <div class="col-lg-2 col-xs-2 clsPadding">
                <button type="button" class="btn btn-primary btn-block " id="btnSave" 
                 @if(isset($estado))
                     @if( $estado ==0)
                     disabled 
                     @endif 
                 @endif ><b>Guardar</b></button>
            </div>
            <!-- <div class="col-lg-2 col-xs-2 clsPadding">
                <button type="button" class="btn btn-primary btn-block " id="btnCancel"><b>Cancelar</b></button>
            </div> -->
            <div class="col-lg-2 col-xs-2 clsPadding">
                <button type="button" class="btn btn-primary btn-block " id="btnEnviar"
                @if(isset($estado))
                    @if( $estado ==0)
                    disabled 
                    @endif 
                @endif 
                ><b>Enviar</b></button>
            </div>

            <!-- <div class="col-lg-2 col-xs-2 clsPadding">
                <button type="button" class="btn btn-primary btn-block " id="btnReabrir"><b>Reabrir</b></button>
            </div> -->

            <div class="col-lg-2 col-xs-2 clsPadding">
                <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target='#searchGastosProy'><b>Buscar</b></button>
            </div>

            <div class="col-lg-2 col-xs-2 clsPadding">
            <button type='button' class="btn btn-primary btn-block" target="_blank" id="btnPrint">Imprimir</button>
            </div>

            <!-- <div class="col-lg-2 col-xs-2 clsPadding">
                <button type="button" class="btn btn-primary btn-block " id="btnDelete"><b>Eliminar</b></button>
            </div> -->
            

        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                        <div class="col-lg-2 col-xs-12 clsPadding">Nro: Rendición:</div>
                        <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="numerorendicion" name="numerorendicion" value="{{ isset($numrendicion)==1?$numrendicion:'' }}" readonly ></div>
                        <div class="col-lg-2 col-xs-12 clsPadding">Fecha de Rendición:</div>
                        <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="frendicion" name="frendicion" value="{{ isset($frendicion)==1?$frendicion:'' }}" readonly ></div>
                        <div class="col-lg-2 col-xs-12 clsPadding">T Cambio:</div>
                        <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="valorcambio" name="valorcambio" readonly="true" value="{{ isset($valorcambio)==1?$valorcambio:'' }}"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                        <div class="col-lg-2 col-xs-12 clsPadding">Título de la Rendición:</div>
                        <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="descripcion" maxlength="300" name="descripcions" value="{{ isset($descripcions)==1?$descripcions:'' }}" ></div>
                        <div class="col-lg-2 col-xs-12 clsPadding">Tipo Rendición:</div>
                        <div class="col-lg-2 col-xs-12 clsPadding">{!! Form::select('ctipo_rendicion',(isset($tiporendicion)==1?$tiporendicion:null),(isset($gasto->tipo_rendicion)==1?$gasto->tipo_rendicion:''),array('class' => 'form-control select-box input-sm','id'=>'ctipo_rendicion')) !!} </div>
                        <div class="col-lg-2 col-xs-12 clsPadding">Moneda de Reembolso:</div>
                        <div class="col-lg-2 col-xs-12 clsPadding">{!! Form::select('cmoneda_devolucion',(isset($tmoneda)==1?$tmoneda:null),(isset($mon_dev)==1?$mon_dev:''),array('class' => 'form-control select-box input-sm','id'=>'cmoneda_devolucion')) !!} </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-xs-12">
                        <div class="col-lg-2 col-xs-12 clsPadding">Viáticos</div>
                        <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="viatico" name="viatico" value="{{ isset($viatico)==1?$viatico:0.00 }}"></div>
                        <div class="col-lg-1 col-xs-12 clsPadding"></div>
                        <div class="col-lg-1 col-xs-12 clsPadding">Total en Dolares:</div>
                        <div class="col-lg-1 col-xs-12 clsPadding"><span style="font-size: 11px;" id="total_dol">{{ (isset($totaldolar)==1?$totaldolar:0.00)}} </span></div>
                        <div class="col-lg-1 col-xs-12 clsPadding">Total en Soles:</div>
                        <div class="col-lg-1 col-xs-12 clsPadding"><span style="font-size: 11px;" id="total">{{ (isset($total)==1?$total:0.00)}} </span></div>
                        <div class="col-lg-1 col-xs-12 clsPadding">Saldo:</div>
                        <div class="col-lg-1 col-xs-12 clsPadding"><span style="font-size: 11px;" id="total">{{ (isset($saldo)==1?$saldo:0.00)}} </span></div>
            </div>
        </div>
        <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">Destino de Gasto:</div>
        </div>
    
        <div class="row" id="bproyecto">
        
        @include('partials.verProyecto',array('tproyecto'=>(isset($tproyecto)==1?$tproyecto:null) ))
                        
        </div>   

        <div class="row" id="bpropuesta">
            @include('partials.verPropuesta',array('tpropuesta'=>(isset($tpropuesta)==1?$tpropuesta:null) ))
                        
        </div>   
        <div class="row">
                <div class="col-lg-1 col-xs-1">Área:</div>
                <div class="col-lg-2 col-xs-3">
                    {!! Form::select('carea',(isset($area)==1?$area:array() ),'',array('class' => 'form-control select-box','id'=>'carea')) !!}   
                </div>
                <!--<div class="col-lg-2 col-xs-2">Centro de costo:</div>
                <div class="col-lg-2 col-xs-3 "> 
                    {!! Form::select('ccentrocosto',(isset($ccosto)==1?$ccosto:array() ),'',array('class' => 'form-control select-box','id'=>'ccentrocosto')) !!} 
                </div>-->

                <div class="col-lg-1 col-xs-1" >Personal Beneficiario:</div>
                <div class="col-lg-2 col-xs-3" >
                    {!! Form::select('cpersona_empleado',(isset($personal)==1?$personal:array() ),(isset($user->cpersona)==1?$user->cpersona:''),array('class' => 'form-control select-box','id'=>'cpersona_empleado')) !!}   
                </div>
        </div>     
        <div class="row">
            <div class="col-md-11 col-xs-12">&nbsp;</div>
            <div class="col-md-1 col-xs-12"><button class="btn btn-primary btn-sm" type="button"  id="idAddGastoProy">Agregar</button></div>
        </div>      
     
       
        <div class="row">	
            <div class="col-lg-12 col-xs-12 table-responsive" id="idGastoProy">
            @if(isset($gastosKey)==1)
            
                @foreach($gastosKey as $key)

                    @include('partials.modalGastosProyectoDetalle',array('key'=> (isset($key)==1?$key:null) ,'gastosProyDet'=> (isset($key['tabla'])==1?$key['tabla']:array() ),'numReg'=> (isset($key['numregistro'])==1?$key['numregistro']:0) ,'idTabla'=>(isset($key['idTabla'])==1?$key['idTabla']:0),'titulo'=>(isset($key['titulo'])==1?$key['titulo']:'')  ) )    
                @endforeach
            @endif
            </div>
        </div>	
            
    </div><!-- fin parte central -->
    
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    {!! Form::close() !!}
</div>

<!--   *******************************************  -->
</div>
  
    
</section>
@include('partials.searchProyecto')
@include('partials.searchPropuesta')
@include('partials.searchProveedor')
@include('partials.searchGastosProyecto') 
 

</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        var selectedPro =[];
        var selectedProv =[];        
        var selectedGas =[];        
        var numTabla = <?php echo  (isset($numTabla)==1?$numTabla:0); ?>;
        var aTipoDocumento = [''];
        var aMoneda = [''];
        var aTActividad = [''];
        var aTGasto = [''];
        var cat_tipogastos = [];
        var impuesto=0.18;
        var totDolarRend= 0;/*<?php echo  (isset($totaldolar)==1?$totaldolar:0); ?>*/;

        <?php if(isset($tipodocumento)){ ?>
          <?php foreach($tipodocumento as $tdoc => $k) { ?>
                aTipoDocumento[aTipoDocumento.length]=['<?php echo $k; ?>','<?php echo $tdoc; ?>'];

          <?php } ?>
        <?php } ?>        
        
        <?php if(isset($tmoneda)){ ?>
          <?php foreach($tmoneda as $mon => $k) { ?>
                aMoneda[aMoneda.length]=['<?php echo $k; ?>','<?php echo $mon; ?>'];

          <?php } ?>
        <?php } ?> 

        <?php if(isset($tipoactividad)){ ?>
          <?php foreach($tipoactividad as $act) { ?>
                aTActividad[aTActividad.length]=['<?php echo addslashes($act->descripcion); ?>','<?php echo $act->cconcepto; ?>','<?php echo $act->cconcepto_parent; ?>','<?php echo $act->tipogasto; ?>'];
          <?php } ?>
        <?php } ?>

        <?php if(isset($tipogasto)){ ?>
          <?php foreach($tipogasto as $gas ) { ?>
                aTGasto[aTGasto.length]=['<?php echo addslashes($gas->descripcion); ?>','<?php echo $gas->cconcepto; ?>','<?php echo $gas->cconcepto_parent; ?>','<?php echo $gas->tipogasto; ?>'];
          <?php } ?>
        <?php } ?>    

        <?php if(isset($cat_tipogastos)){ ?>
          <?php foreach($cat_tipogastos as $cgas ) { ?>
                cat_tipogastos[cat_tipogastos.length]=['<?php echo addslashes($cgas->descripcion); ?>','<?php echo $cgas->digide; ?>','<?php echo $cgas->valor; ?>'];
          <?php } ?>
        <?php } ?>         

        if(numTabla > 0){
            recalculaNumReg(numTabla);
            colocarPicker();
        }
        $.fn.dataTable.ext.errMode = 'throw';        

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'GteProyecto' , name : 'p.abreviatura'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'},
                {data : 'descripcion' , name : 'te.descripcion'}

            ],
            "language": {
                "lengthMenu"    : "Mostrar _MENU_ registros por página",
                "zeroRecords"   : "Sin Resultados",
                "info"          : "Página_PAGE_ de _PAGES",
                "infoEmpty"     : "No existe registros disponibles",
                "infoFiltered"  : "(filtrado de un_MAX_ total de registros)",
                "search"        : "Buscar:",
                "processing"    :  "Procesando...",
                "paginate"      : {
                    "first"     :    "Inicio",
                    "last"      :    "Ultimo",
                    "next"      :     "Siguiente",
                    "previous"  :   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        });     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditarProy();
        });   

        function goEditarProy(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

               $.ajax({

                url:'viewProy/'+id.substring(4),
                type: 'GET',
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#bproyecto").html(data);
                    $('#div_carga').hide(); 
                    cleanPropuesta();
                    
                },            
            });
                        

          }else{
              //$('.alert').show();
          }
        }

        $("#btnNew").on('click',function(e){         

            getUrl('gastosProyecto');         

        });

        //Listado Propuesta
         var tablePro=$("#tPropuesta").DataTable({
            "processing" : true,
            "serverSide" : true,
            "ajax" : 'listarSoloPropuestas',
            "columns" : [
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'},
                {data : 'disciplina' , name : 'tpropuesta.revision'},
                {data : 'descripcion', name : 'ep.descripcion'},
                {data : 'revision' , name : 'tpropuesta.revision'},
                {data : 'gteProyecto' , name : 'p.nombre'}
            ],

            "rowCallback" : function( row, data ) {
                
                if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

                    $(row).addClass('selected');
                }
            }, 
            "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "Sin Resultados",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No existe registros disponibles",
                    "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                    "search":         "Buscar:",
                    "processing":     "Procesando...",
                    "paginate": {
                        "first":      "Inicio",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "loadingRecords": "Cargando..."
                }
            });

    
            $('#tPropuesta tbody').on('click', 'tr', function () {
                var id = this.id;
                var index = $.inArray(id, selectedPro);
                tablePro.$('tr.selected').removeClass('selected');
                selectedPro.splice(0,selectedPro.length);
                if ( index === -1 ) {
                    selectedPro.push( id );

                }/* else {
                    selected.splice( index, 1 );
                }*/
                
                $(this).toggleClass('selected');
            });

            $('#searchPropuesta').on('hidden.bs.modal', function () {
                goEditarProp();
            });  

            /*Fin lista Propuesta*/

            function goEditarProp(){
                var id =0;
                if (selectedPro.length > 0 ){
                    id = selectedPro[0];


                    $.ajax({

                        url:'viewPropuesta/'+id.substring(4),
                        type: 'GET',
                        beforeSend: function(){
                            $('#div_carga').show(); 
                        },
                        success: function(data){
                            $("#bpropuesta").html(data);
                            $('#div_carga').hide(); 
                            cleanProyecto();
                            
                        },            
                    });              

                }else{
                    // $('.alert').show();
                }
            }

            //Listado Proveedor
            var tableProv=$("#tProveedor").DataTable({
            "processing" : true,
            "serverSide" : true,
            "ajax" : 'buscarProveedor',
            "columns" : [
                {data : 'cpersona', name: 'tpersona.cpersona'},
                {data : 'identificacion', name: 'tpersona.identificacion'},
                {data : 'razonsocial' , name : 'tjur.razonsocial'}
            ],
            "rowCallback" : function( row, data ) {
                
                if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

                    $(row).addClass('selected');
                }
            }, 
            "language": {
                    "lengthMenu": "Mostrar _MENU_ registros por página",
                    "zeroRecords": "Sin Resultados",
                    "info": "Página _PAGE_ de _PAGES_",
                    "infoEmpty": "No existe registros disponibles",
                    "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                    "search":         "Buscar:",
                    "processing":     "Procesando...",
                    "paginate": {
                        "first":      "Inicio",
                        "last":       "Ultimo",
                        "next":       "Siguiente",
                        "previous":   "Anterior"
                    },
                    "loadingRecords": "Cargando..."
                }
            });

    
        $('#tProveedor tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selectedProv);
            tableProv.$('tr.selected').removeClass('selected');
            selectedProv.splice(0,selectedProv.length);
            if ( index === -1 ) {
                selectedProv.push( id );

            }/* else {
                selected.splice( index, 1 );
            }*/
            
            $(this).toggleClass('selected');
        });

        $('#searchProveedor').on('hidden.bs.modal', function () {
            goEditarProv();
        });   

        function goEditarProv(){
            var id =0;
            if (selected.length > 0 ){
                id = selectedProv[0];


                $.ajax({

                    url:'viewProveedor/'+id.substring(8),
                    type: 'GET',
                    beforeSend: function(){
                        $('#div_carga').show(); 
                    },
                    success: function(data){
                        $("#bproveedor").html(data);
                        $('#div_carga').hide(); 
                        
                    },            
                });              

            }else{
                $('.alert').show();
            }
        }

        //Listar Gastos Proyecto
       
      var tableGas=$("#tGastosProy").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listaGastos',
      "columns" : [
        {data : 'cgastosejecucion', name: 'cgastosejecucion'},
        {data : 'numerorendicion', name: 'numerorendicion'},
        {data : 'descripcion' , name : 'descripcion'},
        {data : 'valorcambio', name: 'valorcambio'},
        {data : 'total', name: 'total'}
      ],

      "rowCallback" : function( row, data ) {
          
          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });

    
    $('#tGastosProy tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selectedGas);
        tableGas.$('tr.selected').removeClass('selected');
        selectedGas.splice(0,selectedGas.length);
        if ( index === -1 ) {
            selectedGas.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/
        
        $(this).toggleClass('selected');
    });

    $('#searchGastosProy').on('hidden.bs.modal', function () {
        goEditarGastos();
    });   
 

    function goEditarGastos(){
        var id =0;
        if (selectedGas.length > 0 ){
            id = selectedGas[0];
            getUrl('editargastoProyecto/'+id.substring(4),'');         
            
        }else{
            $('.alert').show();
        }
    }
    //guardar
    $("#btnSave").click(function(e){
        $("#numtabla").val(numTabla);
        $.ajax({
            url:'saveGastoProy',
            type: 'POST',
            data : $("#frmgasto").serialize(),
            beforeSend: function(){
                $('#div_carga').show(); 
            },
            success: function(data){
                if (data == 'descripcion') {
                    alert('falta registrar campo');
                    $('#div_carga').hide();
                    return;
                };
                $("#resultado").html(data);
                $('#div_carga').hide(); 
                $("#div_msg").show();
                
            },            
            error: function(data){
                    $('#div_carga').hide();
                    
                    
                    $("#div_msg_error").show();
            },
        });
    }); 

    //Cancelar
    $("btnCancel").click(function(e){
        $.ajax({
            url:'cancelGastoProy',
            type: 'POST',
            data : $("#frmproyecto").serialize(),
            beforeSend: function(){
                $('#div_carga').show(); 
            },
            success: function(data){
                $("#resultado").html(data);
                $('#div_carga').hide(); 
                
            },            
        });
        
    });

    //Enviar
    $("#btnEnviar").click(function(e){
        e.preventDefault();
        $.ajaxSetup({
            header: document.getElementById('_token').value
        });
        
        $.ajax({
            url:'enviarGastoProy',
            type: 'POST',
            data : $("#frmgasto").serialize(),

            beforeSend: function(){
                $('#div_carga').show(); 
            },
            success: function(data){
                //$("#divGaspro").html(data);
                $('#div_carga').hide(); 
                alert('Rendición enviada');
                getUrl('listaGastosProyecto','')
                
            },            
        });
        
    });

    // $("#btnReabrir").click(function(e){
    //     e.preventDefault();
    //     $.ajaxSetup({
    //         header: document.getElementById('_token').value
    //     });
        
    //     $.ajax({
    //         url:'reabrirRendicionGasto',
    //         type: 'POST',
    //         data : $("#frmgasto").serialize(),

    //         beforeSend: function(){
    //             $('#div_carga').show(); 
    //         },
    //         success: function(data){
    //             $("#divGaspro").html(data);
    //             $('#div_carga').hide(); 
                
    //         },            
    //     });
        
    // });

    // $("#btnDelete").click(function(e){
    //     e.preventDefault();
    //     $.ajaxSetup({
    //         header: document.getElementById('_token').value
    //     });
        
    //     $.ajax({
    //         url:'deleteRendicionGasto',
    //         type: 'POST',
    //         data : $("#frmgasto").serialize(),

    //         beforeSend: function(){
    //             $('#div_carga').show(); 
    //         },
    //         success: function(data){
    //             $("#divGaspro").html(data);
    //             $('#div_carga').hide(); 
                
    //         },            
    //     });
        
    // });
/*
    $('.datepicker').datepicker({
        format: "dd/mm/yyyy",
        language: "es",
        autoclose: true,
        endDate: "<?php echo date('d/m/Y'); ?>",
    });*/

    /*
    *  Genera los Hidden que identifican cada tabla
    * param @numId , numerador de Tabla
    */
    function addHidden(numId){

        var hidden="";
        var s_cproyecto = "<input type='hidden' id='cproyecto_"+numId+"' name='cproyecto_"+numId+"' value='' />";
        var s_codproyecto = "<input type='hidden' id='codproyecto_"+numId+"' name='codproyecto_"+numId+"' value='' />";
        var s_cservicioproy = "<input type='hidden' id='cservicioproy_"+numId+"' name='cservicioproy_"+numId+"' value='' />";
        var s_cpropuesta = "<input type='hidden' id='cpropuesta_"+numId+"' name='cpropuesta_"+numId+"' value='' />";
        var s_codpropuesta = "<input type='hidden' id='codpropuesta_"+numId+"' name='codpropuesta_"+numId+"' value='' />";
        var s_cservicioprop = "<input type='hidden' id='cservicioprop_"+numId+"' name='cservicioprop_"+numId+"' value='' />";
        var s_carea = "<input type='hidden' id='carea_"+numId+"' name='carea_"+numId+"' value='' />";
        var s_ccentrocosto = "<input type='hidden' id='ccentrocosto_"+numId+"' name='ccentrocosto_"+numId+"' value='' />";
        var s_cpersona_empleado = "<input type='hidden' id='cpersona_empleado_"+numId+"' name='cpersona_empleado_"+numId+"' value='' />";
        var s_numregistro = "<input type='hidden' id='numregistro_"+numId+"' name='numregistro_"+numId+"' value='' />";
        hidden = s_cproyecto+s_codproyecto +s_cservicioproy+ s_cpropuesta+s_codpropuesta+ s_cservicioprop + s_carea + s_ccentrocosto + s_numregistro+ s_cpersona_empleado;
        return hidden;
    }
    
    $("#idAddGastoProy").click(function(e){
       
        if(!validarAgregar()){
            alert('Solo se puede escojer un proyecto o propuesta o área');
            return ;
        }
        numTabla = numTabla +1;
        var cabecera_table="<table class='table table-striped' id='table_"+numTabla+"'>";
        cabecera_table = cabecera_table + "<thead >\n\
        <tr class='clsCabereraTabla'>\n\
            <td class='clsAnchoTabla'>Tipo Comprobante</td>\n\
            <td class='clsAnchoTabla'>Moneda</td>\n\
            <td class='clsAnchoTabla'>Fec Documento</td> \n\
            <td class='clsAnchoTabla'>Tipo Actividad</td>\n\
            <td class='clsAnchoTabla'>Tipo Gasto</td>\n\
            <td class='clsAnchoTabla'>Descripcion</td>\n\
            <td class='clsAnchoTabla'>Proveedor</td>\n\
            <td class='clsAnchoTabla'>Nro. Comprobante</td>\n\
            <td class='clsAnchoTabla'>Cantidad</td>\n\
            <td class='clsAnchoTabla'>SubTotal</td>\n\
            <td class='clsAnchoTabla'>IGV</td>\n\
            <td class='clsAnchoTabla'>Otro Imp.</td>\n\
            <td class='clsAnchoTabla'>Total</td>\n\
            <td class='clsAnchoTabla'>Acciones</td>\n\
        </tr>\n\
        </thead>\n\
        <tbody style='height:200px;overflow:auto'>";

       /* var cabecera_table="<table class='table table-striped' id='table_"+numTabla+"'>";
        cabecera_table = cabecera_table + "<thead >\n\
        <tr class='clsCabereraTabla'>\n\
            <td class='clsAnchoTabla'>Tipo Comprobante</td>\n\
            <td class='clsAnchoTabla'>Moneda</td>\n\
            <td class='clsAnchoTabla'>Fec Documento</td> \n\
            <td class='clsAnchoTabla'>Tipo Actividad</td>\n\
            <td class='clsAnchoTabla'>Tipo Gasto</td>\n\
            <td class='clsAnchoTabla'>Descripcion</td>\n\
            <td class='clsAnchoTabla'>Proveedor</td>\n\
            <td class='clsAnchoTabla'>Reembolsable</td>\n\
            <td class='clsAnchoTabla'>Nro. Comprobante</td>\n\
            <td class='clsAnchoTabla'>Cantidad</td>\n\
            <td class='clsAnchoTabla'>SubTotal</td>\n\
            <td class='clsAnchoTabla'>Impuesto</td>\n\
            <td class='clsAnchoTabla'>Otro Imp.</td>\n\
            <td class='clsAnchoTabla'>Total</td>\n\
            <td class='clsAnchoTabla'>Acciones</td>\n\
        </tr>\n\
        </thead>\n\
        <tbody style='height:200px;overflow:auto'>"; */
        
        
        var registro = '';
        var nroreg=0;
        var i=1;
        for (i=1;i<=4;i++){
            nroreg++;
            registro = registro + formarFila(numTabla,i);
        }
        var footer_table = "</tbody></table>";
        var titulo ="<p id='tit_"+numTabla+"'>_TITULO_</p>";
        var hidden = addHidden(numTabla);
        var s_datos = "<div id='div_"+numTabla+"'>"+titulo+hidden+cabecera_table+registro+footer_table+"</div>";
        
        $("#idGastoProy").append(s_datos);
        for (i=1;i<=4;i++){
            obtenerDatos(numTabla+"_"+i,numTabla);
            fillDocumento(numTabla+"_"+i);
            fillMoneda(numTabla+"_"+i);
            fillTActividad(numTabla,i);
            //fillTGasto(numTabla+"_"+i);
            $("#tipodocumento_"+numTabla+"_"+i).addClass('form-control');
            $("#tipodocumento_"+numTabla+"_"+i).chosen({allow_single_dselect:true,width:"100%"});
            $("#cmoneda_"+numTabla+"_"+i).addClass('form-control');
            $("#cmoneda_"+numTabla+"_"+i).chosen({allow_single_dselect:true,width:"100%"});
            $("#fdocumento_"+numTabla+"_"+i).datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                autoclose:true,
                endDate: "<?php echo date('d/m/Y'); ?>",
            });
            $("#tipoactividad_"+numTabla+"_"+i).chosen({allow_single_dselect:true,width:"100%"});
            $("#tipogasto_"+numTabla+"_"+i).chosen({allow_single_dselect:true,width:"100%"});
            $("#table_"+numTabla).data("num",i);            
        }
        
        $("#numregistro_"+numTabla).val(nroreg);
        

        
    }); 

    function agregarFilasTable(numTabla){

        //var num = $("#table_"+numTabla).data("num");
        var num = $("#numregistro_"+numTabla).val();
        //alert(numTabla+'_'+num);

        for(j=1;j<=2;j++){
            num++;
            var fila = formarFila(numTabla,num);
            //alert(numTabla+'_'+num);
            $("#table_"+numTabla +" tr:last").after(fila);
            fillDocumento(numTabla+"_"+num);
            fillMoneda(numTabla+"_"+num);
            fillTActividad(numTabla,num);
            //fillTGasto(numTabla+"_"+num);
            $("#tipodocumento_"+numTabla+"_"+num).addClass('form-control');
            $("#tipodocumento_"+numTabla+"_"+num).chosen({allow_single_dselect:true,width:"100%"});
            $("#cmoneda_"+numTabla+"_"+num).addClass('form-control');
            $("#cmoneda_"+numTabla+"_"+num).chosen({allow_single_dselect:true,width:"100%"});
            $("#fdocumento_"+numTabla+"_"+num).datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                autoclose:true,
                endDate: "<?php echo date('d/m/Y'); ?>",                
            });
            $("#tipoactividad_"+numTabla+"_"+num).chosen({allow_single_dselect:true,width:"100%"});
            $("#tipogasto_"+numTabla+"_"+num).chosen({allow_single_dselect:true,width:"100%"});        
        }
        $("#table_"+numTabla).data("num",num);
        $("#numregistro_"+numTabla).val(num);        
    }

    function formarFila(numTabla,num){
        var registro = "<tr class='clsAnchoTabla'>";
        var tipodocumento = "<td class=''><input type='hidden' name='cpersona_empleado_"+numTabla+"_"+num+"' id='cpersona_empleado_"+numTabla+"_"+num+"' value='' /><input type='hidden' name='cgastoejecuciondet_"+numTabla+"_"+num+"' id='cgastoejecuciondet_"+numTabla+"_"+num+"' value='' /><select name='tipodocumento_"+numTabla+"_"+num+"' id='tipodocumento_"+numTabla+"_"+num+"' onchange='calculoTotal("+numTabla+","+num+");calculoTotalDolar("+numTabla+","+num+");totalRendicion();totalRendicionDolares();'></select></td> ";
              
        var cmoneda = "<td class=''><select name='cmoneda_"+numTabla+"_"+num+"'  id='cmoneda_"+numTabla+"_"+num+"' onchange='calculoTotalDolar("+numTabla+","+num+");totalRendicionDolares();totalRendicion();' ></select></td>";

        var fdocumento = "<td class=''><input size='5' type='text' name='fdocumento_"+numTabla+"_"+num+"' id='fdocumento_"+numTabla+"_"+num+"' onchange='calculoTotalDolar("+numTabla+","+num+");totalRendicion();totalRendicionDolares();' class='datepicker' value='' ></td>";
        var tipoactividad = "<td class=''><select name='tipoactividad_"+numTabla+"_"+num+"' id='tipoactividad_"+numTabla+"_"+num+"' onchange='fillTGasto("+numTabla+","+num+")'></select></td>";
        var tipogasto = "<td class=''><select name='tipogasto_"+numTabla+"_"+num+"' id='tipogasto_"+numTabla+"_"+num+"'></select></td>";
        var descripcion ="<td class=''><textarea type='text' rows='4' placeholder='Descripción' name='descripcion_"+numTabla+"_"+num+"' id='descripcion_"+numTabla+"_"+num+"' class='form-control' value='' ></textarea></td>";
        var razon_cliente="<td class=''><input type='hidden' name='cpersona_prov_"+numTabla+"_"+num+"' id='cpersona_prov_"+numTabla+"_"+num+"' value='' /><input size='12' type='text' name='ruc_"+numTabla+"_"+num+"' id='ruc_"+numTabla+"_"+num+"' placeholder='Ruc' class='pull-left' value='' ><input type='text' name='razon_"+numTabla+"_"+num+"' id='razon_"+numTabla+"_"+num+"' placeholder='Razon' class='input-sm' value='' ></td>";
       /* var reembolsable= "<td class=''>";               
      /*  reembolsable= reembolsable + "<input type='checkbox' name='reembolsable_"+numTabla+"_"+num+"' class='checkbox' id='reembolsable_"+numTabla+"_"+num+"' value='00001'>";
        reembolsable= reembolsable + "</td>";*/
        
        var numerodocumento = "<td class=''><div class='input-group' style='width:100%;'><input style='width:20%' title='Prefijo del comprobante, debe ser letras' placeholder='Prefijo' type='text' name='numerodocumento_p"+numTabla+"_"+num+"' id='numerodocumento_p"+numTabla+"_"+num+"' class='' value='' ><input style='width:30%' title='Serie del comprobante' placeholder='Serie' type='text' name='numerodocumento_s"+numTabla+"_"+num+"' id='numerodocumento_s"+numTabla+"_"+num+"' class='' value='' ><input style='width:50%' title='Número del comprobante' placeholder='Numero' type='text' name='numerodocumento_n"+numTabla+"_"+num+"' id='numerodocumento_n"+numTabla+"_"+num+"' class='' value='' ></div></td>";
        var cantidad = "<td class=''><input size='1' type='text' name='cantidad_"+numTabla+"_"+num+"' id='cantidad_"+numTabla+"_"+num+"' onblur='calculoTotal("+numTabla+","+num+");calculoTotalDolar("+numTabla+","+num+");totalRendicion();totalRendicionDolares();' class='' value='1' ></td>";
        var preciounitario="<td class=''><input size='4' type='text' name='preciou_"+numTabla+"_"+num+"' id='preciou_"+numTabla+"_"+num+"' onblur='calculoTotal("+numTabla+","+num+");calculoTotalDolar("+numTabla+","+num+");totalRendicionDolares();totalRendicion();' class='' value='' ></td>";
        var impuesto="<td class=''><input size='4' type='text' name='impuesto_"+numTabla+"_"+num+"' id='impuesto_"+numTabla+"_"+num+"' onblur='calculoTotal("+numTabla+","+num+");calculoTotalDolar("+numTabla+","+num+");totalRendicion();totalRendicionDolares();'  class='' value='' ></td>";
        var otrimp="<td class=''><input size='4' type='text' name='otrimp_"+numTabla+"_"+num+"' id='otrimp_"+numTabla+"_"+num+"' onblur='calculoTotal("+numTabla+","+num+");calculoTotalDolar("+numTabla+","+num+");totalRendicion();totalRendicionDolares();'  class='' value='' ></td>";
        var total="<td class=''><input size='4' type='text' name='total_"+numTabla+"_"+num+"' id='total_"+numTabla+"_"+num+"'  onblur='/*calculoTotal("+numTabla+","+num+");calculoTotalDolar("+numTabla+","+num+");*/totalRendicion();totalRendicionDolares();' class='' value='' ><input size='4' type='hidden' name='total_dolar_"+numTabla+"_"+num+"' id='total_dolar_"+numTabla+"_"+num+"'  onblur='/*calculoTotalDolar("+numTabla+","+num+");*/totalRendicion();' class='' value=''></td>";
       
        var acciones="<td class=''>";
        
        acciones = acciones + "<a href='#' class='glyphicon glyphicon-plus'  data-toggle='tooltip'  title='Duplicar' onclick='agregarFilasTable("+numTabla+")'></a>";
        acciones = acciones + "<a href='#' class='fa fa-trash' onclick='fdelGastoProy("+numTabla+",this)' data-toggle='tooltip'  title='Eliminar' aria-hidden='true'></a>";
        acciones = acciones + "</td>";
        var fin_registro =  "</tr>";
        registro = registro + tipodocumento + cmoneda + fdocumento +tipoactividad + tipogasto + descripcion + razon_cliente + /*reembolsable + */numerodocumento+ cantidad + preciounitario + impuesto + otrimp + total +acciones + fin_registro;
        return registro;
    }
    function validarAgregar(){
        if(getCproyecto()!='' && getCPropuesta()!=''){
            return false;
        }
        validar=0;
   
        if(getCproyecto()!='' && getCarea()==''){
            //alert(getCproyecto()+'/'+getCarea());
            validar++;           
        }
        if(getCPropuesta()!='' && getCarea()==''){
            validar++;            
        }
        if(getCarea()!='' && getCproyecto()=='' && getCPropuesta()==''){
            validar++;
           
        }
       /* if(getCcosto()!=''){
            validar++;
            alert(getCcosto()+'ccos');
        }*/
        
        if(validar<=0){
            return false;
        }

        return true;
    }
    
    function obtenerDatos(id,numTabla){
        var datos='';
        if(getCproyecto()!=''){
            $("#cproyecto_"+numTabla).val( $("#cproyecto").val() );
            $("#codproyecto_"+numTabla).val( $("#codproyecto").val() );
           
            datos = "PROY: " +$("#codproyecto").val()+"  "+ $("#tproyecto").val();
            $("#cservicioproy_"+numTabla).val( $("#cservicioproy").val()  );
        }
        if(getCPropuesta()!=''){
            $("#cpropuesta_"+numTabla).val( $("#cpropuesta").val() );
            $("#codpropuesta_"+numTabla).val( $("#codpropuesta").val() );
            datos = "PROPUESTA: "+ $("#codpropuesta").val() +"  " + $("#tpropuesta").val();
            $("#cservicioprop_"+numTabla).val( $("#cservicioprop").val()  );
        }        
        if(getCarea()!=''){
            $("#carea_"+numTabla).val( $("#carea").val() );
            datos = datos + "  " + "AREA: " + $("#carea :selected").text();
            
        }
        if(getCcosto()!=''){
            $("#ccentrocosto_"+numTabla).val( $("#ccentrocosto").val() );
            datos = datos + "  " + "C COSTO: " + $("#ccentrocosto :selected").text();
            
        }      

         $("#cpersona_empleado_"+numTabla).val( $("#cpersona_empleado").val() );


        var personalnombre = document.getElementById('cpersona_empleado').options[document.getElementById('cpersona_empleado').selectedIndex].text;

        datos = datos +" / De: "+ personalnombre;

        $("#tit_"+numTabla).html(datos);
        return ;
    }


    function getCproyecto(){
        return $("#cproyecto").val();
    }
    function getCPropuesta(){
        return $("#cpropuesta").val();
    }
    function getCpersona(){
        return $("#cpersona_empleado").val();
    }
    function getCarea(){
        return $("#carea").val();
    }
    function getCcosto(){
        return $("#ccentrocosto").val();
    }
    function fillDocumento(id){
        $("#tipodocumento_"+id).append(new Option('','',true,true));
        
        for(i=0;i < aTipoDocumento.length ; i++){
            $("#tipodocumento_"+id).append(new Option(aTipoDocumento[i][0],aTipoDocumento[i][1]));
        }
    }  
    function fillMoneda(id){
        $("#cmoneda_"+id).append(new Option('','',true,true));
        
        for(i=0;i < aMoneda.length ; i++){
            $("#cmoneda_"+id).append(new Option(aMoneda[i][0],aMoneda[i][1]));
        }
    }      
    function fillTActividad(numTabla,id){
        $("#tipoactividad_"+numTabla+"_"+id).append(new Option(' ',' ',true,true));
        var cservicio='';

        if($("#cproyecto_"+numTabla).val()!=''){
            /*  Cuando la tabla es de Proyectos*/
            var aTipos = [];
            cservicio = $("#cservicioproy_"+numTabla).val();
            //alert(cservicio);
            for(e=0;e < cat_tipogastos.length;e++){

                 // arServicios= servicios segun tipo de actividad -->CONSTRUCCION,00001,2, OPERACIONES,00003,1,3,4,5, LABORATORIO,00002,1,2,3,4,5
                
                var arServicios = cat_tipogastos[e][2].split(','); 

                var res = arServicios.indexOf(cservicio);

                if(res >=-1){                   
                    aTipos.push(cat_tipogastos[e][1]);
                }

            }

            //alert(aTipos);
           
            //alert('SI');
            var aOptions = [];
            //alert(aTipos.length);

            //alert(arServicios+'-'+cservicio+'-'+aTActividad);
            for(r=0;r< aTActividad.length;r++){                 
                for(j=0;j<aTipos.length;j++){
                    if (aTActividad[r][3]==aTipos[j]){

                        aOptions.push(aTActividad[r]); 
                      
                       /* if(cservicio==2){
                           
                            if (aTipos[j]=='00001' || aTipos[j]=='00002' ) {
                                aOptions.push(aTActividad[r]);  
                            }
                        }
                        else{

                            if (aTipos[j]=='00003' || aTipos[j]=='00002' ) {
                                aOptions.push(aTActividad[r]);  
                            }

                        }*/

                                      
                       
                    }
                }
            }
            //alert(aOptions.length);
            for(r=0;r<aOptions.length;r++){
                $("#tipoactividad_"+numTabla+"_"+id).append(new Option(aOptions[r][0],aOptions[r][1]));
            }

        }else if($("#cpropuesta_"+numTabla).val()!=''){
            /* Propuesta */
            var aTipos = [];
            cservicio = $("#cservicioprop_"+numTabla).val();
            for(e=0;e < cat_tipogastos.length;e++){
                
                var arServicios = cat_tipogastos[e][2].split(',');
                //alert(cat_tipogastos[e][2]);
                //alert(cservicio);
                var res = arServicios.indexOf(cservicio);
                //alert(res);
                if(res >=-1){
                    aTipos.push(cat_tipogastos[e][1]);
                }
            }
            //alert('SI');
            var aOptions = [];
            //alert(aTipos.length);
            for(r=0;r< aTActividad.length;r++){
                for(j=0;j<aTipos.length;j++){
                    if (aTActividad[r][3]==aTipos[j]){
                           aOptions.push(aTActividad[r]);

                    }
                }
            }
            //alert(aOptions.length);
            for(r=0;r<aOptions.length;r++){
                $("#tipoactividad_"+numTabla+"_"+id).append(new Option(aOptions[r][0],aOptions[r][1]));
            }            
        }else{
            for(i=0;i < aTActividad.length ; i++){
                $("#tipoactividad_"+numTabla+"_"+id).append(new Option(aTActividad[i][0],aTActividad[i][1]));
            }
        }
        

    }   
    function fillTGasto(numTabla,id){
        var ctipo = $("#tipoactividad_"+numTabla+"_"+id).val();
        
        $("#tipogasto_"+numTabla+"_"+id + " option").remove();
        if(ctipo !=''){
            
            
            $("#tipogasto_"+numTabla+"_"+id).append(new Option('','',true,true));
            
            for(i=0;i < aTGasto.length ; i++){
                //alert(i);
                if(aTGasto[i][2]==ctipo){
                    $("#tipogasto_"+numTabla+"_"+id).append(new Option(aTGasto[i][0],aTGasto[i][1]));
                }
            }
            
        }
        $("#tipogasto_"+numTabla+"_"+id).chosen('destroy');
        $("#tipogasto_"+numTabla+"_"+id).chosen({allow_single_dselect:true,width:"100%"});    
    }           
    function fdelGastoProy(numTabla,objLink){
        var r = confirm("¿Seguro de Eliminar la Fila?");
        if (r == true) {
            var rowCount = $("#table_"+numTabla+" >tbody >tr").length;
            //alert(rowCount);
            if(rowCount>1){
                $(objLink).parent().parent().remove();
            }else{
                $("#div_"+numTabla).remove();
            }
        } 
        

    }; 

    function saveTemporales(){

        $("#numtabla").val(numTabla);
        $.ajax({
            url:'saveGastoProy',
            type: 'POST',
            data : $("#frmgasto").serialize(),
            beforeSend: function(){
                $('#div_carga').show(); 
            },
            success: function(data){
                $("#resultado").html(data);
                $('#div_carga').hide(); 
                //$("#div_msg").show();
                
            },            
            error: function(data){
                    $('#div_carga').hide();
                    
                    
                   // $("#div_msg_error").show();
            },
        });    

    }

    function fdelGastoProyBD(idgastodetalle){        

        var idgastodet=idgastodetalle;     
        $.ajax({
          url: 'deleteDetGasto/' + idgastodet,
          type:'GET',
          beforeSend: function () {                
                //$('#div_carga').show();         
              },              
              success : function (data){

                $("#resultado").html(data);
                $('#div_carga').hide();
                //$('#divTable').html(data);
              },
            });
    }; 


    function recalculaNumReg(nrot){
        var w =1;
        for(w=1;w<=nrot;w++){
            num=  $("#table_"+w+" >tbody >tr").length;
            $("#table_"+numTabla).data("num",num);
        }
    }


    function cimpuesto(){
        var cantidad = document.getElementById('cantidad').value;
        var preciounitario = document.getElementById('preciounitario').value;
        document.getElementById('impuesto').value=  cantidad*preciounitario *0.18;

    };   
    
    function cleanProyecto(){
        $("#tproyecto").val("");
        $("#cproyecto").val("");
        $("#nombreUMineraProy").val("");
        $("#nombreClienteProy").val("");

    }

    function cleanPropuesta(){
        $("#tpropuesta").val("");
        $("#cpropuesta").val("");
        $("#nombreUMineraProp").val("");
        $("#nombreClienteProp").val("");
    }


    $('.select-box').chosen(
    {
        allow_single_deselect: true,width:"100%"
    });

    function calculoTotal(numTabla,num){


        var precio = 0;
        var cantidad = 1;
        if($("#preciou_"+numTabla+"_"+num).val()!=''){
            precio = $("#preciou_"+numTabla+"_"+num).val();
        }
        if($("#cantidad_"+numTabla+"_"+num).val()!=''){
            cantidad = $("#cantidad_"+numTabla+"_"+num).val();
        }
        
        
        var subTotal = precio * cantidad;
        //00001
        if($("#tipodocumento_"+numTabla+"_"+num).val()=='00001' || $("#tipodocumento_"+numTabla+"_"+num).val()=='00007'){
            $("#impuesto_"+numTabla+"_"+num).val(parseFloat(subTotal*impuesto).toFixed(2));
        }else{
            $("#impuesto_"+numTabla+"_"+num).val(0);
        }
        var otrimp = 0;
        if($("#otrimp_"+numTabla+"_"+num).val()!=''){
            otrimp = parseFloat($("#otrimp_"+numTabla+"_"+num).val());
        }
        var total = parseFloat(subTotal) + parseFloat($("#impuesto_"+numTabla+"_"+num).val()) + otrimp;

        $("#total_"+numTabla+"_"+num).val(parseFloat(total).toFixed(2));

       
    }

    function calculoTotalDolar(numTabla,num){

        var moneda = $("#cmoneda_"+numTabla+"_"+num).val();
        var fecha = $("#fdocumento_"+numTabla+"_"+num).val();
        var total = $("#total_"+numTabla+"_"+num).val();
        var cambio=0;
        var totaldol=0;

         
        $.ajax({
                url: 'calcularTotalDolares',
                type: 'GET',
                data:  {
                        "moneda" : $("#cmoneda_"+numTabla+"_"+num).val(),
                        "fecha" : $("#fdocumento_"+numTabla+"_"+num).val(),
                        "total": $("#total_"+numTabla+"_"+num).val()
                },
                           
                success : function (data){
                  
                    totaldol=data;
                    $('#div_carga').hide(); 
                    $("#total_dolar_"+numTabla+"_"+num).val(totaldol);   

                },                
        });       
    }

    function colocarPicker(){
        $("input[name ^=fdocumento]").each(function(){
           // alert($(this).attr('name'));
            $(this).datepicker({
                format: "dd/mm/yyyy",
                language: "es",
                autoclose: true,
                endDate: "<?php echo date('d/m/Y'); ?>",
            });
        });
    }

    function totalRendicionDolares(){
        var totalRendicionDolar = 0;
        var totalRendicionNew=0;

        for(var i=1;i<= numTabla; i++){
            var num = $("#numregistro_"+i).val();
            for(var j=1;j<= num;j++){
                var totaldol=0;    
                var total = $("#total_"+i+"_"+j).val();
               // alert(total);                               
                if($("#total_"+i+"_"+j).val().length >0){
                    $.ajax({
                        url: 'calcularTotalDolaresRendicion',
                        type: 'GET',
                        data:  {
                            "moneda" : $("#cmoneda_"+i+"_"+j).val(),
                            "fecha" : $("#fdocumento_"+i+"_"+j).val(),
                            "total": $("#total_"+i+"_"+j).val()
                        },
                                    
                        success : function (data){
                            $('#div_carga').hide();
                            totaldol=data;                                    
                            totalRendicionDolar = parseFloat(totalRendicionDolar) + parseFloat(totaldol);  
                            totalRendicionNew=parseFloat(totalRendicionDolar) + parseFloat(totDolarRend);  
                           // alert(totalRendicionNew+'/'+data);
                            $("#total_dol").text(totalRendicionNew);
                        },                
                    });                
                }                
            }
        } 
    }   

    function totalRendicion(){
        var totalRendicionSol = 0;
        var totalRendicionNew=0;
        for(var i=1;i<= numTabla; i++){
            var num = $("#numregistro_"+i).val();
         
            for(var j=1;j<= num;j++){
                var totaldol=0;    
                var total = $("#total_"+i+"_"+j).val();
               
                if($("#total_"+i+"_"+j).val().length >0){

                    
                    $.ajax({
                        url: 'calcularTotalSolesRendicion',
                        type: 'GET',
                        data:  {
                            "moneda" : $("#cmoneda_"+i+"_"+j).val(),
                            "fecha" : $("#fdocumento_"+i+"_"+j).val(),
                            "total": $("#total_"+i+"_"+j).val()
                        },
                                    
                        success : function (data){
                            $('#div_carga').hide();
                            totaldol=data;                                    
                            totalRendicionSol = parseFloat(totalRendicionSol) + parseFloat(totaldol);  
                            totalRendicionNew=parseFloat(totalRendicionSol) + parseFloat(totDolarRend);  
                           // alert(totalRendicionNew+'/'+data);
                            $("#total").text(totalRendicionNew);    
                        },                
                    });                
                }                
            }
        }               
    }   
    

/*    function totalRendicion(){
        var totalRendicion = 0;
        
        for(var i=1;i<= numTabla; i++){
            var num = $("#numregistro_"+numTabla).val();
            for(var j=1;j<= num;j++){
                var precio = 0;
                var cantidad = 1;
                var imp = 0;
                var total =0;
                if($("#preciou_"+i+"_"+j).val()!=''){
                    precio = $("#preciou_"+i+"_"+j).val();
                }
                if($("#cantidad_"+i+"_"+j).val()!=''){
                    cantidad = $("#cantidad_"+i+"_"+j).val();
                }
                if($("#impuesto_"+i+"_"+j).val()!=''){
                    imp = $("#impuesto_"+i+"_"+j).val();
                }                
                var subTotal = precio * cantidad;
                var otrimp = 0;
                if($("#otrimp_"+i+"_"+j).val()!=''){
                    otrimp = parseFloat($("#otrimp_"+i+"_"+j).val());
                }
                //alert($("#total_"+i+"_"+j).val());     
                if($("#total_"+i+"_"+j).val()!=''){
                    total = parseFloat($("#total_"+i+"_"+j).val());
                }
                
                //var total = parseFloat(subTotal) + parseFloat(imp) + parseFloat(otrimp);
                totalRendicion = parseFloat(totalRendicion) + parseFloat(total);
            }
        }
        //alert(totalRendicion);
        $("#total").text(totalRendicion);           
    }   */

    /*$("#btnPrint").on('click',function(){      
        var datos = $("#frmgasto").serialize();
        win = window.open('printGastoProy?'+datos,'_blank');
    });*/

    $("#btnPrint").on('click',function(){      
      goPrintGastos();



    });

    function goPrintGastos(){

        var id =$('#cgastosejecucion').val();

     
        if (id.length > 0 ){

             win = window.open('printPDF/'+id,'_blank');
           
                 
            
        }else{
            $('.alert').show();
        }
    }
</script>
    
     






