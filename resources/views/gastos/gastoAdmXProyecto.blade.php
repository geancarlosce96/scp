<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gastos
        <small>Rendición de Gastos Administrativos por Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Gastos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">


      {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
      {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

<div class="row clsPadding2">
	<div class="col-lg-1 col-xs-1 clsPadding"></div>
    <div class="col-lg-2 col-xs-2 clsPadding">
    	     <button type='button' class="btn btn-primary btn-block" id="btnNew"> <b>Nuevo</b></button>
    </div>
    <div class="col-lg-2 col-xs-2 clsPadding">
          <button type="button" class="btn btn-primary btn-block " id="btnSave"><b>Guardar</b></button>
    </div>
    <div class="col-lg-2 col-xs-2 clsPadding">
          <button type="button" class="btn btn-primary btn-block " id="btnCancel"><b>Cancelar</b></button>
    </div>
    <div class="col-lg-2 col-xs-2 clsPadding">
          <button type="button" class="btn btn-primary btn-block " id="btnEnviar"><b>Enviar</b></button>
    </div>
    
    <div class="col-lg-2 col-xs-2 clsPadding">
          <button type='button' class="btn btn-primary btn-block" disabled="">Imprimir</button>
    </div>
    <div class="col-lg-1 col-xs-1 clsPadding"></div>

</div>
  <div class="row clsPadding2">
		<div class="col-lg-6 col-xs-12">
                <div class="col-lg-6 col-xs-12 clsPadding">Nro: Rendición:</div>
                <div class="col-lg-6 col-xs-12 clsPadding"><input class="form-control input-sm" type="text"></div>
                <div class="col-lg-6 col-xs-12 clsPadding">Título de la Rendición:</div>
                <div class="col-lg-6 col-xs-12 clsPadding"><input class="form-control input-sm" type="text"></div>
        </div>
		<div class="col-lg-6 col-xs-12">
            <div class="col-lg-12 col-xs-12 table-responsive">
            <table class="table table-striped">

                    <tbody> 
                    <tr>
                      <td class="clsAnchoTabla"
                      	style="background-color:#999; text-align:center !important">Total Rendición</td>
                      <td class="clsAnchoTabla"></td>
                    </tr> 
                    <tr>
                      <td class="clsAnchoTabla"
                      	style="background-color:#999; text-align:center !important">Total Reembolso</td>
                      <td class="clsAnchoTabla"></td>
                    </tr>                     
                    <tr>
                      <td class="clsAnchoTabla"
                      	style="background-color:#999; text-align:center !important">Total Cliente</td>
                      <td class="clsAnchoTabla"></td>
                    </tr>
                    <tr style="background-color:#ffffff;">
                      <td></td>
                      <td></td>
                    </tr>                     
                    <tr style="margin-top:10px !important;">
                      <td class="clsAnchoTabla"
                      	style="background-color:#999; text-align:center !important">Tipo de Cambio</td>
                      <td class="clsAnchoTabla">3.23</td>
                    </tr>                                                                          
                                                    
                    </tbody>
                  </table>    
            </div>       
        </div>        
        
  </div>
   <div class="row clsPadding2">
   		<div class="col-lg-12 col-xs-12 clsTitulo">Proyecto:</div>
   </div>
   <div class="row clsPadding2">
   		<div class="col-lg-3 col-xs-6">Tipo actividad:</div>
        <div class="col-lg-3 col-xs-6">
              <select class="form-control">
                <option>Cliente</option>
                <option>Anddes</option>
              </select>        
        </div>
        <div class="col-lg-3 col-xs-6">Servicio:</div>
        <div class="col-lg-3 col-xs-6">
              <select class="form-control">
                <option>Construcción</option>
                <option>Ambiental</option>
                <option>Ingeniería</option>                
              </select>         
        </div>
   </div>    
    <div class="row clsPadding2">        
        <div class="col-lg-8 col-xs-8">
            <label for="cpersona" class="col-sm-2 control-label">Cliente</label>         
            {!! Form::select( 'cpersona',(isset($tcliente)==1?$tcliente:array() ),'',array('class' => 'form-control','id'=>'cpersona')) !!}
        </div>
           
    </div>   
    <div class="row clsPadding2">        
        <div class="col-lg-8 col-xs-8">
            <label for="cunidadminera" class="col-sm-2 control-label">Unidad Minera</label>    
            {!! Form::select( 'cunidadminera',(isset($tuminera)==1?$tuminera:array() ),'',array('class' => 'form-control','id'=>'cunidadminera')) !!}
        </div>
         
    </div> 
    <div class="row clsPadding2">       
        <div class="col-lg-8 col-xs-8">
            <label for="cproyecto" class="col-sm-2 control-label">Proyecto</label> 
        
            {!! Form::select( 'cproyecto',(isset($tproyecto)==1?$tproyecto:array() ),'',array('class' => 'form-control','id'=>'cproyecto')) !!} 
        </div>
           
    </div>   
	<div class="row clsPadding2">        
        <div class="col-lg-8 col-xs-8">
            <label for="cproyectosub" class="col-sm-2 control-label">Sub Proyecto</label> 
        
            {!! Form::select( 'cproyectosub',(isset($tsubproyecto)==1?$tsubproyecto:array() ),'',array('class' => 'form-control','id'=>'cproyectosub')) !!}
        </div>
          
    </div>  
	<div class="row clsPadding2">
        <div class="col-lg-2 col-xs-2">Asignar personal:</div>
        <div class="col-lg-10 col-xs-10">
			<input class="form-control input-sm" type="text" placeholder="Personal Asignado" disabled> 
        </div>   
    </div>     



   <div class="row clsPadding2">
   		<div class="col-lg-12 col-xs-12 clsTitulo">Ingreso de Comprobantes:</div>
   </div>
	<div class="row clsPadding2">
        <div class="col-lg-6 col-xs-12">

            <!-- gastos -->
            <div class="row clsPadding2">
                <div class="col-lg-3 col-xs-3">Tipo de Gastos:</div>
                <div class="col-lg-9 col-xs-9">
                      <select class="form-control">
                        <option>Personal de Apoyo</option>
                        <option>Servicios</option>
                        <option>Servicios Profesionales (No Calificada)</option>                
                      </select>                      
                </div>
                </div>   
             <!-- tipo de gastos -->
             <div class="row clsPadding2">
                <div class="col-lg-3 col-xs-3">Descripción:</div>
                <div class="col-lg-9 col-xs-9">
                    
                    	<textarea class="form-control" rows="3" placeholder="Descripción ..."></textarea>
                                        
                </div>  
                </div> 
             <!-- tipo de gastos -->             
        </div>
        <div class="col-lg-6 col-xs-12"> 
        	<div class="row clsPadding2">
	            <div class="col-lg-6 col-xs-12">
                    <div class="form-group">
                    <label class="clsTxtNormal">Reembolsable:</label>
                      <div class="radio">
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1" checked="">
                          Si
                        </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <label>
                          <input type="radio" name="optionsRadios" id="optionsRadios1" value="option2">
                          No
                        </label>                        
                      </div>                   
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="form-group">
                    <label class="clsTxtNormal">Fecha:</label>
                    
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right" id="datepicker">
                    </div>
                    <!-- /.input group -->
                    </div>                
                </div>
                <div class="col-lg-6 col-xs-12">
                <label class="clsTxtNormal">Cantidad:</label>
                <input class="form-control input-sm" type="text" placeholder="Cantidad." >
                
                </div>
                <div class="col-lg-6 col-xs-12">
                <label class="clsTxtNormal">P.U:</label>
                <input class="form-control input-sm" type="text" placeholder="P.U." >
                
                </div>                
                
                
                <div class="col-lg-6 hidden-xs clsPadding2"></div>
                <div class="col-lg-6 col-xs-12 clsPadding2">
                <a href="#" class="btn btn-primary btn-block ">Aceptar</a>  
                </div> 
            </div>
        </div>
    </div>     


    <div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
    Proveedor:
    </div>
    </div>  
    
    <div class="row clsPadding2">
    	<div class="col-lg-2 col-xs-2">Tipo de Documento:</div>
    	<div class="col-lg-6 col-xs-6">
              <select class="form-control">
                    <option>Boleta</option>
                    <option>Factura</option>
                    <option>Ticket</option>                
              </select>        
        </div>
    	<div class="col-lg-4 col-xs-4">
	        <input type="text" class="form-control" placeholder="Nro-Documento">
        </div>                
    </div>       
    <div class="row clsPadding2">
    	<div class="col-lg-2 col-xs-6">RUC:</div>
    	<div class="col-lg-3 col-xs-6">
                <div class="input-group input-group-normal">
                <input type="text" class="form-control">
                    <span class="input-group-btn">
                      <button type="button" class="btn btn-primary btn-flat">
                      <i class="fa fa-search" aria-hidden="true"></i>
                      </button>
                    </span>
                </div>        
        </div> 
    	<div class="col-lg-2 col-xs-6">Razon Social:</div>
    	<div class="col-lg-5 col-xs-6"> 
        	 <input type="text" class="form-control" placeholder="Razón Social" disabled>
        </div>              
    </div>    
     <div class="row clsPadding2">
    	<div class="col-lg-2 col-xs-6">Moneda:</div>
    	<div class="col-lg-4 col-xs-6">
              <select class="form-control">
                <option>US$</option>
                <option>Euro:</option>
              </select>        
        </div>   
    	<div class="col-lg-2 col-xs-6">Tipo de Cambio</div>
    	<div class="col-lg-4 col-xs-6">
        <input type="text" class="form-control" placeholder="Ingrese valor de referencia">
        </div>             
     </div>   
    <div class="row clsPadding2">  
        <div class="col-lg-6 col-xs-12">
            <div class="col-lg-12 col-xs-12 table-responsive">
            <table class="table table-striped">
                    <tbody>
                    <tr>
                      <td class="clsAnchoTabla"
                        style="background-color:#999; text-align:center !important">Valor Neto</td>
                      <td class="clsAnchoTabla" style="text-align:center !important">S/.</td>
                    </tr>  
                    <tr>
                      <td class="clsAnchoTabla"
                        style="background-color:#999; text-align:center !important">Otros impuestos</td>
                      <td class="clsAnchoTabla"></td>
                    </tr> 
                    <tr>
                      <td class="clsAnchoTabla"
                        style="background-color:#999; text-align:center !important">IGV (18%)</td>
                      <td class="clsAnchoTabla"style="text-align:center !important"></td>
                    </tr>                     
                    <tr>
                      <td class="clsAnchoTabla"
                        style="background-color:#999; text-align:center !important">Total</td>
                      <td class="clsAnchoTabla"style="text-align:center !important"></td>
                    </tr>                            
                    </tbody>
                  </table>    
            </div>       
        </div>      
    </div>    


<div class="row clsPadding2"> 
    <div class="col-lg-12 col-xs-12 table-responsive">
        @include('partials.modalgastoAdmXProyecto',array('listAdmProy'=> (isset($listAdmProy)==1?$listAdmProy:array() )))
  </div>
</div>
   
    
    

<!--   <div class="row clsPadding2">	
<div class="col-lg-12 col-xs-12 table-responsive">
<table class="table table-striped">
    <thead>
    <tr class="clsAnchoTabla">
      <th class="clsAnchoTabla" style="background-color:#999; text-align:center !important">Semana</th>
      <th class="clsAnchoTabla">HH FACT</th>
      <th class="clsAnchoTabla">HH NO FACT</th>
      <th class="clsAnchoTabla">HH ANDDES</th>
      <th class="clsAnchoTabla">HH TOTAL</th>                  
      <th class="clsAnchoTabla">Cargabilidad</th>                                    
      <th class="clsAnchoTabla">Año</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td class="clsAnchoTabla" style="background-color:#999; text-align:center !important">27</td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
    </tr>              
                                    
    </tbody>
  </table>    
</div>
</div>-->   
<!--  <div class="row clsPadding2">
    <div class="col-lg-4 hidden-xs"></div>
    <div class="col-lg-2 col-xs-6">
    <a href="#" class="btn btn-primary btn-block ">Guardar</a>
    </div>
    <div class="col-lg-2 col-xs-6">
    <a href="#" class="btn btn-primary btn-block ">Enviar HH</a>
    </div>
    <div class="col-lg-4 hidden-xs"></div>
</div>-->
<!--<div class="row clsPadding2">	
<div class="col-lg-12 col-xs-12 table-responsive">
<table class="table table-striped">
<caption class="clsCabereraTabla">Semana Actual</caption>
        <thead>
        <tr class="clsCabereraTabla">
          <th>Tarea</th>
          <th>FF/NF/AN</th>
          <th>¿Recuperable?</th>
          <th>GP</th>
          <th>Sustento</th>                  
          <th>Entregable</th>                                    
          <th>Lun</th>
          <th>Mar</th>
          <th>Mie</th>
          <th>Jue</th>
          <th>Vie</th>
          <th>Sáb</th>
          <th>Dom</th>
          <th>Total</th>
          <th>Barra Avance<br>(Área / Tarea)</th>
          <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td class="clsAnchoTabla" style="min-width:120px !important">
            <div class="input-group">
            <input id="new-event" type="text" class="form-control" placeholder="Tarea:">
                <div class="input-group-btn">
                <button id="add-new-event" type="button" class="btn btn-primary btn-flat">...</button>
                </div>
            </div>                  
          
          </td>
          <td class="clsAnchoTabla">FF</td>
          <td class="clsAnchoTabla" style="min-width:120px !important">
          <select class="form-control">
            <option>Compensable</option>
            <option>Salud/Enfermedad</option>
          </select>                  </td>
          <td class="clsAnchoTabla">JA</td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla">Ent1 / Ent2</td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla" style="min-width:150px !important">
          <div class="clsBarra">
            <div class="clsAvance" style="width:45px;"></div>
          </div>                
          <div class="clsBarra">
            <div class="clsAvance" style="width:75px;"></div>
          </div>                  
          </td>
          <td class="clsAnchoTabla">
                <a href="#" class="fa fa-plus"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-trash"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-save"></a>&nbsp;&nbsp;&nbsp;&nbsp;
          </td>                                    
        </tr>  
        <tr>
          <td class="clsAnchoTabla" style="min-width:120px !important">
            <div class="input-group">
            <input id="new-event" type="text" class="form-control" placeholder="Tarea:">
                <div class="input-group-btn">
                <button id="add-new-event" type="button" class="btn btn-primary btn-flat">...</button>
                </div>
            </div>                  
          
          </td>
          <td class="clsAnchoTabla">FF</td>
          <td class="clsAnchoTabla" style="min-width:120px !important">
          <select class="form-control">
            <option>Compensable</option>
            <option>Salud/Enfermedad</option>
          </select>                  </td>
          <td class="clsAnchoTabla">JA</td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla">Ent1 / Ent2</td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla" style="min-width:150px !important">
          <div class="clsBarra">
            <div class="clsAvance" style="width:45px;"></div>
          </div>                
          <div class="clsBarra">
            <div class="clsAvance" style="width:75px;"></div>
          </div>                  
          </td>
          <td class="clsAnchoTabla">
                <a href="#" class="fa fa-plus"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-trash"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-save"></a>&nbsp;&nbsp;&nbsp;&nbsp;
          </td>                                    
        </tr>
          
        <tfoot>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>Total</td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td></td>
          <td></td>
          <td></td>                                    
        </tfoot>                                              
                                        
        </tbody>
      </table>    
</div>
</div> -->       
        
        
        
        
        
        
        
        
        
        
        
    </div><!-- fin parte central -->
    
    <div class="col-lg-1 hidden-xs"></div>

</div>

<!--   *******************************************  -->
</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>

      var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';        

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu"    : "Mostrar _MENU_ registros por página",
                "zeroRecords"   : "Sin Resultados",
                "info"          : "Página_PAGE_ de _PAGES",
                "infoEmpty"     : "No existe registros disponibles",
                "infoFiltered"  : "(filtrado de un_MAX_ total de registros)",
                "search"        : "Buscar:",
                "processing"    :  "Procesando...",
                "paginate"      : {
                    "first"     :    "Inicio",
                    "last"      :    "Ultimo",
                    "next"      :     "Siguiente",
                    "previous"  :   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editargastoAdmXProyecto/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        //Nuevo
        $("btnNew").on('show.bs.modal',function(e){
         
          });

        //guardar
        $("btnSave").click(function(e){
             $.ajax({

                url:'saveGastoAdmProy',
                type: 'POST',
                data : $("#frmproyecto").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#divGaspro").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        }); 

        //Cancelar
        $("btnCancel").click(function(e){
             $.ajax({

                url:'cancelGastoAdmProyecto',
                type: 'POST',
                data : $("#frmproyecto").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#divGaspro").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        });

        //Enviar
        $("btnEnviar").click(function(e){
             $.ajax({

                url:'enviarGastoAdmProyecto',
                type: 'POST',
                data : $("#frmproyecto").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#divGaspro").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        });

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });


      

    </script>