<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gastos
        <small>Rendiciones Usuario</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Gastos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
      <!--<div class="col-lg-1 hidden-xs"></div>-->
      <div class="col-lg-12 col-xs-12">
        {!! Form::open(array('url' => 'viewRendicionUsuario','method' => 'GET','id' =>'frmgasto')) !!}

          
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}

          <div class="row clsPadding2">	
              <div class="col-lg-2 col-xs-6">Tipo de Actividad:</div>
              <div class="col-lg-3 col-xs-6">
                {!! Form::select('tipoactividad',(isset($tipoactividad)==1?$tipoactividad:null),'',array('class' => 'form-control select-box','id'=>'tipoactividad')) !!}                            
              </div>
              <div class="col-lg-2 col-xs-6">Area:</div>
              <div class="col-lg-3 col-xs-6">
                {!! Form::select('area',(isset($areas)==1?$areas:null),'',array('class' => 'form-control select-box','id'=>'area')) !!}                            
              </div>              
          </div>
          <div class="row clsPadding2">	
              <div class="col-lg-1 col-xs-2">Fechas:</div>
              <div class="col-lg-5 col-xs-4">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right datepicker" id="fdesde">
                    </div>    
                </div>
              <div class="col-lg-1 col-xs-2"></div>
              <div class="col-lg-5 col-xs-4">
                    <div class="input-group date">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input type="text" class="form-control pull-right datepicker" id="fhasta">
                    </div>    
              </div>    
          </div>
          <div class="row clsPadding2">	
              <div class="col-md-10 col-xs-2"></div>
              <div class="col-md-2  col-xs-2">

                  <button type="button" class="btn btn-primary  " id="btnView"> <b>Visualizar</b></button>

              </div>
          </div>          

     
   
{!! Form::close() !!}  


<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive" id="divRendicion">
        
          @include('partials.tableRendicionUsuario',array('listaUsuario'=>(isset($listaUsuario)==1?$listaUsuario:array() ) ))

	</div>
</div>  
<div class="row clsPadding2">
	<div class="col-lg-3 hidden-xs"></div>
    <div class="col-lg-3 col-xs-6">
    	<a href="#" class="btn btn-primary btn-block ">Enviar Rendicion</a>
    </div>
    <div class="col-lg-3 col-xs-6">
    	<a href="#" class="btn btn-primary btn-block ">Imprimir Rendiciones</a>
    </div>
	<div class="col-lg-3 hidden-xs"></div>    
</div>        
  
    
    
        
        
        
        
        
        
        
        
        
        
    </div><!-- fin parte central -->
    
    <!--<div class="col-lg-1 hidden-xs"></div>-->

</div>

<!--   *******************************************  -->
</div>

<script>


        $("#btnView").click(function (e){

            //e.preventDefault();
            /*if (/*$("#disciplina").val()=='' || $("#fhasta").val()=='' || $("#fdesde").val()==''){
                alert('Especifique el Area, Fecha Desde, Fecha Hasta de la Planificación');
                return;
            }*/
            
            $.ajax({
                url: 'viewRendicionUsuario',
                type: 'GET',
                data: "tipoactividad="+ $('#tipoactividad').val() +"&carea="+$("#area").val()+"&fhasta="+ $("#fhasta").val() + "&fdesde=" + $("#fdesde").val(),
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#divRendicion").html(data);
                    $('#div_carga').hide(); 
                },
            });
        });


        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });

        $('.select-box').chosen(
        {
            allow_single_deselect: true
        });

</script>