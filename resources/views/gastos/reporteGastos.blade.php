@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gastos
        <small>Reporte de Gastos</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gastos</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

    <div class="row">
        <div class="col-lg-9 col-xs-9">
            <label> Proyecto: </label>
            {!! Form::select('proyecto',(isset($proyectos)==1?$proyectos:array()),'',array('class' => 'form-control select-box','id'=>'proyecto')) !!}  
        </div>

        <div class="col-lg-2 col-xs-2">
            <label> &nbsp;</label>
                <button type='button' class="btn btn-primary btn-block" id="btnGenerar">
                            <b> <i class="glyphicon glyphicon-search"></i> Generar</b>
                </button>
        </div>

        <div class="col-lg-1" style="display: none;padding-top:10px;" id="imgLoad" >
            <label> &nbsp;</label>
              <img src="{{ asset('img/load.gif') }}" width="25px" >
        </div>
    </div>

    <br>

    <div class="row">
        <div class="col-lg-3 col-xs-3">
            <label> Área: </label>

            {!! Form::select('area',(isset($areas)==1?$areas:array()),'',array('class' => 'form-control select-box','id'=>'area')) !!}  
        </div>

        <div class="col-lg-3 col-xs-3">
            <label> Profesional: </label>

            {!! Form::select('persona',(isset($persona)==1?$persona:array()),'',array('class' => 'form-control select-box','id'=>'persona')) !!}  
        </div>



        
        <div class="col-lg-3 col-xs-3">
            <label> Concepto Padre: </label>

            {!! Form::select('concepto_parent',(isset($conceptogasto_parent)==1?$conceptogasto_parent:array()),'',array('class' => 'form-control select-box conceptos','id'=>'concepto_parent')) !!}  
        </div>

        <div class="col-lg-3 col-xs-3">
            <label> Concepto Gasto: </label>

            {!! Form::select('concepto',(isset($conceptogasto)==1?$conceptogasto:array()),'',array('class' => 'form-control select-box conceptos','id'=>'concepto')) !!}  
        </div>

    </div>

    <br>


    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>

    <br>

    <div class="row"> 

        <div class="col-lg-12 col-xs-12 table-responsive" id='divTablaGastos'>
         @include('gastos.tablaGastosReporte',array('arrayGastos'=> (isset($arrayGastos)==1?$arrayGastos:array() )))
        </div> 

    </div>

 


    </section>
    <!-- /.content -->


</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
<script type="text/javascript">


$(document).ready(function(){

	$('.select-box').chosen(
        {
            allow_single_deselect: true,width:"100%"
        });

    $('#btnGenerar').click(function(){
        obtenerGastos();
    })

    $('.conceptos').change(function(){

        var concepto=$(this).val();

        $(".conceptos").val('N').trigger("chosen:updated");   
        $(this).val(concepto).trigger("chosen:updated");
        
    })

  
});


function obtenerGastos()
{
        var cproyecto = $("#proyecto").val();     
        var carea = $("#area").val(); 
        var cpersona = $("#persona").val(); 
        var concepto = $("#concepto").val(); 
        var concepto_parent = $("#concepto_parent").val(); 

        obj={
                cproyecto : $("#proyecto").val(),     
                carea     : $("#area").val(), 
                cpersona  : $("#persona").val(), 
                conceptogasto  : $("#concepto").val(), 
                conceptogasto_parent  : $("#concepto_parent").val(), 
        }

        var url = BASE_URL + '/verGastosPorProyecto';


        $("#imgLoad").show();

        $.get(url,obj)
            .done(function( data ) {    

            $("#imgLoad").hide();  

             $("#divTablaGastos").html(data);
                  
               
        });
}




</script>


@stop