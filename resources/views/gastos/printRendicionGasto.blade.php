
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<link rel="stylesheet" href="">
	<style>
		/** Establezca los márgenes de la página en 0, de modo que el pie de página y el encabezado Puede ser de la altura y anchura total. !**/
		@page {
			margin: 1cm 1cm;
		}
		/** Define ahora los márgenes reales de cada página en el PDF. **/
		body {
			margin-top: 3.05cm;
			margin-left: 0cm;
			margin-right: 0cm;
			margin-bottom: 0.5cm;
		}
		/** Definir las reglas de encabezado. **/
		header {
			position: fixed;
			top: 0.4cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
		}
		/** Definir las reglas del pie de página. **/
		footer {
			position: fixed;
			bottom: 0cm;
			left: 0cm;
			right: 0cm;
			height: 0cm;
		}
	</style>
</head>
<body>
	<header>
		<div class="row">
			<table  style="width:100%; border-collapse: collapse; size: 0.5px;font-family: Arial, Helvetica, sans-serif;" class="table table-bordered">
				<tr>
					<th style="border:1px solid #000000FF; width: 15%;"><img style="width: 150px;" src="images/logorpt.jpg"></th>
					<th rowspan="2" style="border:1px solid #000000FF; width: 50%;text-align: center;"> <p>Administración y Finanzas <br> Rendición de Gastos </p></th>
					<th style="border:1px solid #000000FF; width: 30%;">  </th>
				</tr>
				<tr>
					<th style="border:1px solid #000000FF; width: 15%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)"> SIG AND </th>
					<th style="border:1px solid #000000FF; width: 30%;text-align: center; font-size: 14px;"> 10-AND-42-FOR-0100 / R0/ <br> <?php echo date('d-m-y');?> </th>
				</tr>
			</table>
		</div>
	</header>
	<section class="content" style="font-family: Arial,Helvetica,sans-serif;">
		<div class="row">
			<div>
				<table style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;text-align: left;">
					<tr>
						<th style="text-align: left; width: 80px">N° Rendición:</th>
						<th style="text-align: left; width: 120px"> <?php echo $numrendicion; ?></th>
						<th style="text-align: left; width: 100px">Título Rendición:</th>
						<th style="text-align: left; width: 300px"><?php echo $descripcions; ?></th>
					</tr>
					<tr>
						<th style="text-align: left; width: 80px">Fecha:</th>
						<th style="text-align: left; width: 70px"><?php echo date_format(date_create($frendicion),'d-m-y'); ?></th>
						<th style="text-align: left; width: 80px">Registrado Por:</th>
						<th style="text-align: left; width: 120px"><?php echo $registrador; ?></th>
					</tr>
					<tr>
						<th style="text-align: left; width: 100px">DNI Usuario:</th>
						<th style="text-align: left; width: 30px"><?php echo $identificacion; ?></th>

					</tr>

				</table>
				<br>

				<table style="width: 100%; font-family: Arial, Helvetica, sans-serif;font-size: 12px;text-align: left;border:0.5px solid #E0E0E0FF">

					<tr>
						<th style="text-align: left; width: 100px">Monto recibido</th>
						<th style="text-align: left; width: 100px"> S/ <?php echo floatval($viatico); ?></th>
						<th style="text-align: center; width: 100px">Monto rendido</th>
						<th style="text-align: center; width: 100px"> S/ <?php echo floatval($total); ?></th>
						<th style="text-align: right; width: 100px">Saldo</th>
						<th style="text-align: right; width: 100px"> S/ <?php echo floatval($saldo); ?></th>
					</tr>

				</table>
			</div>

		</div>

		<div class="row">

			<div class="col-sm-12">

				<?php if (isset($gastosKey)==1) { ?>
				<?php foreach ($gastosKey as $key) { ?>

				<section class="panel" style="border:1px solid #000000FF;border-radius: 5px;">
					<div class="panel-heading modal-header" style=" font-family: Arial, Helvetica, sans-serif;font-size: 12px; padding: 7px;">

						<?php echo $key['titulo'];
						echo "<br>";
						echo "<strong>";
						echo "Gerente Proyecto: ";
						echo "</strong>";
						echo $key['gerente'];

						?>

						<hr style="color: #E0E0E0FF;size: 1px;" />
					</div>

					<div class="panel-body">
						<div class="adv-table" style="padding: 7px;" >
							<table class="display table table-bordered table-striped table-condensed" style="font-family: Arial, Helvetica, sans-serif; font-size:10px; width: 100%;border-collapse: collapse;">
								<thead>
								<tr >
									<th style="border:1px solid #000000FF; width: auto;">Descripción</th>
									<th style="border:1px solid #000000FF; width: auto"><center>Gasto</center></th>
									<th style="border:1px solid #000000FF; width: auto">Cant.</th>
									<!-- <th style="border:1px solid #000000FF; width: auto;">PU</th> -->
									<!--<th style="border:1px solid #000000FF; width: auto;">Clien.</th>-->
									<!--<th style="border:1px solid #000000FF; width: auto;">Reemb.</th>-->
									<th style="border:1px solid #000000FF; width: auto">Fecha</th>
									<th style="border:1px solid #000000FF; width: auto">Moneda</th>
									<th style="border:1px solid #000000FF; width: auto">Número de <br> comprobante</th>
									<th style="border:1px solid #000000FF; width: auto">Proveedor</th>
									<th style="border:1px solid #000000FF; width: auto">Neto</th>
									<th style="border:1px solid #000000FF; width: auto">Otro <br> Imp.</th>
									<th style="border:1px solid #000000FF; width: auto;">IGV <br> (18%)</th>
									<th style="border:1px solid #000000FF; width: auto">Monto total</th>
								</tr>

								</thead>

								<tbody>
								<?php foreach ($key['tabla'] as $gap) { ?>

								<tr class="gradeX" >

									<td style="border:1px solid #000000FF;width: auto;"><span><?php echo $gap['descripcion'] ; ?></span></td>
									<td style="border:1px solid #000000FF;width: auto "><span><?php echo $gap['descctividad'] ; ?></span></td>
									<td style="text-align: center; border:1px solid #000000FF;width: auto "><span><?php echo $gap['cantidad'] ; ?></span></td>
								<!-- <td style="border:1px solid #000000FF; "><span><?php echo $gap['preciounitario'] ; ?></span></td> -->
								<!-- <td style="border:1px solid #000000FF; width: auto"><span><?php echo $gap['reembolsable'] ; ?></span></td> -->
									<td style="text-align: center; border:1px solid #000000FF;width: auto"><span><?php echo date_format(date_create($gap['fdocumento']),'d-m-y') ; ?></span></td>
									<td style="text-align: center; border:1px solid #000000FF;width: auto "><span><?php echo $gap['moneda'] ; ?></span></td>
									<td style="text-align: center; border:1px solid #000000FF;width: auto "><span><?php echo $gap['numerodocumento'] ; ?></span></td>
									<td style="border:1px solid #000000FF; "><span><?php echo $gap['razon_cliente'] ; ?></span></td>
									<td style="text-align: center; border:1px solid #000000FF;width: auto "><span><?php echo $gap['subtotal'] ; ?></span></td>
									<td style="text-align: center; border:1px solid #000000FF;width: auto "><span><?php echo $gap['otrimp'] ; ?></span></td>
									<td style="text-align: center; border:1px solid #000000FF;width: auto; "><span><?php echo $gap['impuesto'] ; ?></span></td>
									<td style="text-align: center; border:1px solid #000000FF;width: auto "><span><?php echo $gap['totaldeta'] ; ?></span></td>

								</tr>
								<?php } ?>

								</tbody>

							</table>

						</div>

					</div>
					<br>

				</section>
				<br>




				<?php } ?>
				<?php } ?>
			</div>
		</div>
		<table class="table table-bordered" style=" font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
			<tr>
				<th style="width: 150px">Total Importe</th>
				@if(isset($total)==1)        
				<th style="width: 150px">S/ <?php echo floatval($total); ?></th>
						@endif
				<th style="width: 150px">Total Importe</th>

				@if(isset($totaldolar)==1)
				<th style="width: 150px">USD <?php echo floatval($totaldolar); ?></th>
				@endif
			</tr>    
		</table>
	</section>
</body>
</html>



