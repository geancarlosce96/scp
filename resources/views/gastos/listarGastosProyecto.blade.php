<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gastos
        <small>Listado de Gastos de Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gastos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">
    <div class="row clsPadding2">
        	 
        	<div class="col-lg-2 col-xs-12" style="margin-top:25px;">
				    <button type='button' class="btn btn-primary btn-block" id="btnNew"><b>Nuevo</b></button> 
          </div> 
        	<div class="col-lg-2 col-xs-12" style="margin-top:25px;">
				    <button type="button"  onclick="goEditar();" class="btn btn-primary btn-block"><b>Editar</b></button> 
          </div> 
        	<div class="col-lg-2 col-xs-12" style="margin-top:25px;">
				    <button type="button" class="btn btn-primary btn-block" onclick="goReabir();"><b>Reabrir</b></button> 
          </div>
          <div class="col-lg-2 col-xs-12" style="margin-top:25px;">
            <button type="button" class="btn btn-primary btn-block" onclick="goDelete();"><b>Eliminar</b></button> 
          </div>
        	<!-- <div class="col-lg-2 col-xs-12" style="margin-top:25px;">
				    <button type="button" class="btn btn-primary btn-block" onclick="goPrint()"><b>Imprimir</b></button> 
          </div>                                                   -->
          <div class="col-lg-2 col-xs-12">

          </div>              
   </div>   
 
   
  <div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">

          <table class="table table-striped" id="listado">
            <thead>
            <tr class="clsCabereraTabla">
              <th>Número Rendición</th> 
              <th>Nombre</th>
              <th>Fecha</th>
              <th>Monto Total</th>
              <th>Estado</th>


            </tr>
            </thead>


          </table>

    </div>
  </div>   
<!--:-->
 

        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->


</div>
    </section>
    <!-- /.content -->
  </div> 

  <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';     
    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarGastosGrid',
      "columns" : [
        {data : 'numerorendicion', name: 'tge.numerorendicion'},
        {data : 'descripcion', name: 'tge.descripcion'},
        {data : 'frendicion' , name : 'tge.frendicion'},
        {data : 'total' , name : 'tge.total'},
        {data : 'condicion', name: 'est.descripcion'}

      ],

      "rowCallback" : function( row, data ) {
          
          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });

    
    $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/
        
        $(this).toggleClass('selected');
    });

  function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        // alert(id.substring(4));
        getUrl('editargastoProyecto/'+id.substring(4),'');
    }else{
        $('.alert').show();
    }
  }

  function goDelete(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        // alert(id.substring(4));
        getUrl('deleteRendicionGasto/'+id.substring(4),'');
    }else{
        $('.alert').show();
    }
  }
  function goReabir(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        // alert(id.substring(4));
        getUrl('reabrirRendicionGasto/'+id.substring(4),'');
    }else{
        $('.alert').show();
    }
  }

  // function goPrint(){      
  //       // var datos = $("#frmgasto").serialize();

  //       var id =0;
  //   if (selected.length > 0 ){
  //       id = selected[0];
  //       // alert(id.substring(4));
  //       getUrl('printGastoProy/'+id.substring(4),'');
  //       win = window.open('printGastoProy?'+id.substring(4),'_blank');
  //   }else{
  //       $('.alert').show();
  //   }



  //   }

  $("#btnNew").on('click',function(e){        

            getUrl('gastosProyecto');         

        });

</script>