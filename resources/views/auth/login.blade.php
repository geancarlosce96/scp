<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Control de Proyectos | Anddes</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->

  <link rel="stylesheet" href="pages/css/font-awesome.min.css">
  <!-- Ionicons -->

  <link rel="stylesheet" href="pages/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">


</head>
<body class="hold-transition login-page">
<img alt="" src="img/bkgAndes.jpg" id="bkgSistema" /> 
<div class="login-box">
  <div class="login-logo">

    <img src="img/logo.jpg">
  </div>

  <div class="login-box-body">
    <p class="login-box-msg">BIENVENIDO</p>
    {!! Form::open(['route' => 'login','method' => 'POST' ])  !!}
      @include('partials/errors')
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" style="height:30px" placeholder="Usuario" value="{{ old('username')}}">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" class="form-control" style="height:30px" placeholder="Clave">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <!-- <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Recordar clave 	
            </label>
          </div>
        </div> -->

        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat">INGRESAR</button>
        </div>

      </div>
    {!! Form::close() !!}
    <div class="social-auth-links text-center">
      <!-- <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google	"></i> Ingrese usando su cuenta de GMAIL</a> -->
    </div>


    <!-- <a href="#">Olvide mi clave</a><br> -->
    
  </div>

</div>


<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
