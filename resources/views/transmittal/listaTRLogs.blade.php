@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transmittal
        <small>Reporte de Transmittals</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Transmittal</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      


       {!! Form::open(array('url' => 'grabarEntregablesProyecto','method' => 'POST','id' =>'frmproyectoentregables')) !!}
    
    <!-- <div class="row">

      <div class="col-lg-1 col-xs-1">Logo de Cliente:</div>

      <div class="col-lg-3 col-xs-3">
        <img src="{{ url('images/sinfoto.jpg') }}" class="user-image" alt="User Image" width="120px" height="60px">
      </div>
      <div class="col-lg-6 col-xs-6"></div>
      <div class="col-lg-2 col-xs-2">
            <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'>
               <b> <i class="glyphicon glyphicon-search"></i> Proyectos</b>
            </button>
      </div> 
    </div> -->

    <div class="row">
        <div hidden>

          {!! Form::text('cproyecto',(isset($proyecto)==1?$proyecto['cproyecto']:''),array('id'=>'cproyecto')) !!}
          {!! Form::text('cproyectonuevo',(isset($proyecto)==1?$proyecto['cproyecto']:''),array('id'=>'cproyectonuevo')) !!}
          {!! Form::text('_token',csrf_token(),array('id'=>'_token')) !!}
          
        </div>
        <div class="row"><br></div>
        <div class="col-lg-1 col-xs-1">Cliente:</div>
        <div class="col-lg-3 col-xs-3">

            {!! Form::text('cliente',(isset($proyecto)==1?$proyecto['cliente']:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','readonly' => 'true','id' => 'cliente')) !!}
        </div>
        <div class="col-lg-1 col-xs-1">Unidad minera:</div>
        <div class="col-lg-2 col-xs-2">
            {!! Form::text('uminera',(isset($proyecto)==1?$proyecto['uminera']:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','readonly' => 'true','id'=>'uminera') ) !!}
        </div>
        <div class="col-lg-1 col-xs-1">Estado:</div>
        <div class="col-lg-2 col-xs-2">

          {!! Form::text('estadoproyecto',(isset($proyecto)==1?$proyecto['estadoproyecto']:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','readonly' => 'true','id'=>'estadoproyecto') ) !!}
            
        </div>


        <div class="col-lg-2 col-xs-2">
            <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'>
               <b> <i class="glyphicon glyphicon-search"></i> Proyectos</b>
            </button>
        </div> 

    </div>
    <div class="row">
        <div class="row"><br></div>
        <div class="col-lg-1 col-xs-1">Código Proyecto:</div>
        <div class="col-lg-3 col-xs-3">
            {!! Form::text('codproyecto',(isset($proyecto)==1?$proyecto['codproyecto']:''),array('readonly'=>'true','class'=> 'form-control input-sm' ,'id'=>'codproyecto')) !!}
        </div>
        <div class="col-lg-1 col-xs-1">Nombre del Proyecto:</div>
        <div class="col-lg-7 col-xs-7">
            {!! Form::text('nombreproyecto',(isset($proyecto)==1?$proyecto['nombreproyecto']:''),array('class'=>'form-control input-sm','readonly' =>'true','placeholder'=>'Nombre de Proyecto','id'=>'nombreproyecto')) !!}   
        </div>
   
    </div>

    <br>

    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>

    <div class="row btn btn-block" >
        <div class="col-lg-2 col-xs-6" style="padding: 0px">
            <div class="input-group col-lg-12 col-xs-12 dropdown">
              <button type="button" class="btn btn-info dropdown-toggle btn-block" data-toggle="dropdown">
                Enviados / Recibidos </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background-color: #0069AA"><a id="andcli" style="color: white;">ANDCLI</a></li>
                  <li style="background-color: #00B050"><a id="cliand" style="color: white;">CLIAND</a></li>
                </ul>
            </div>
        </div>


        <div class="col-lg-2 col-xs-6" style="padding: 0px">
            <div class="input-group col-lg-12 col-xs-12 dropdown">
              <button type="button" class="btn btn-info dropdown-toggle btn-block" data-toggle="dropdown">
                Lista de Entregables </button>
                <ul class="dropdown-menu" role="menu">
                  <li style="background-color: #FFFF00"><a id="letec" >LETEC</a></li>
                  <li style="background-color: #E26B0A"><a id="leges" style="color: white;">LEGES</a></li>
                  <li style="background-color: #8064A2"><a id="lecom" style="color: white;">LECOM</a></li>
                </ul>
            </div>
        </div>



        <div class="col-lg-1" style="display: none;" id="imgLoad" >
            <label> &nbsp;</label>
          <img src="{{ asset('img/load.gif') }}" width="25px" >
        </div>
    </div>

    <div class="col-lg-12 col-xs-12 table-responsive" id='tableEntdivLEntregable'>

        <h5 id="tituloReporte">
          
        </h5>

        <div id="rep_andcli" class="reportes"></div>
        <div id="rep_cliand" class="reportes"></div>
        <div id="rep_letec" class="reportes"></div>
        <div id="rep_leges" class="reportes"></div>
        <div id="rep_lecom" class="reportes"></div>

        <!--<div id="rep_andcli" class="reportes">
               @include('transmittal.tablaLogANDCLI',array('entregablesTR'=> (isset($entregablesTR)==1?$entregablesTR:array() ))) 
        </div>-->

    </div> 
   

   {!! Form::close() !!}

    </section>
    <!-- /.content -->
    @include('transmittal.modalListaProyectos')
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
<script type="text/javascript">


$(document).ready(function(){

  obtenerProyectos();

  $('#btnSeleccionarPry').click(function () {
      if ($('#cproyectonuevo').val()!="") {
        cargarDatosProyecto($('#cproyectonuevo').val());
      }
  });

  function cargarDatosProyecto(id){
      var url = BASE_URL + '/listaentregablestlogs/'+id;
      $(location).attr('href',url);
  }

  $('#andcli').click(function (){
    obtenerLog('andcli');
  })

  $('#cliand').click(function (){
    obtenerLog('cliand');
  })

  $('#letec').click(function (){
    obtenerLog('letec');
  })

  $('#leges').click(function (){
    obtenerLog('leges');
  })

  $('#lecom').click(function (){
    obtenerLog('lecom');
  })

  $('#searchProyecto').on('show.bs.modal', function() {
        setTimeout(iniDatatablePry,2000);
  })


  // $('.dropdown').hover(function() {
  //     $(this).find('.dropdown-menu').first().stop(true, true).slideDown(150);
  //   }, function() {
  //     $(this).find('.dropdown-menu').first().stop(true, true).slideUp(105)
  //   });
 
});

function obtenerProyectos(){
       var url = BASE_URL + '/listarProyectosTr';
       $.get(url)
          .done(function( data ) { 
            $('#tablaProyectos').html(data);
          });
 } 

function obtenerLog(tipo){

  $("#imgLoad").show();


  var obj = {cproyecto: $('#cproyecto').val()}
  var url = BASE_URL + '/verReportesLogs/'+tipo;
      $.get(url,obj)
        .done(function( data ) { 

          $("#imgLoad").hide();  
          $('.reportes').empty();

          if (tipo=='andcli') {

            $('#tituloReporte').html('ANDCLI');
            $('#rep_andcli').html(data);

          }

          if (tipo=='cliand') {
            $('#tituloReporte').html('CLIAND');
            $('#rep_cliand').html(data);
            
          }

          if (tipo=='letec') {
            $('#tituloReporte').html('LETEC');
            $('#rep_letec').html(data);
            
          }

          if (tipo=='leges') {
            $('#tituloReporte').html('LEGES');
            $('#rep_leges').html(data);
            
          }

          if (tipo=='lecom') {
            $('#tituloReporte').html('LECOM');
            $('#rep_lecom').html(data);
            
          }
         
        });

}


</script>


@stop