<table class="table table-responsive table-hover" id="tableListaTR" width="100%" style="font-size: 12px;">
    <thead>
        <tr class="thead-light">
            <th>Item</th>
            <th>Transmittal</th>
            <th>Fecha</th>
            <th>Tipo de envío</th>
        </tr>
    </thead>
   
    <tbody>
        @if(isset($transmittals))
            @foreach($transmittals as $key => $tr)

            <tr class="seleccionarTR" data-codigotr="{{$tr->ctransmittal}}" data-tipotr="{{$tr->tipotransmittal}}" id="TR_{{$tr->ctransmittal}}">
                <td>{{$key +1}}</td>
                <td>{{$tr->numero}}</td>
                <td>{{$tr->fechaenvio}}</td>
                <td>{{$tr->envio}}</td>
              
            </tr>

            @endforeach 
        @endif 
    </tbody>
</table>

<script>
   
    var tableListaTR = $('#tableListaTR').DataTable( {
    "paging":   true,
    "ordering": true,
    "info":     false,
   // "order":     [[0,"desc"]],
    lengthChange: true,
    //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
    } );
 
    tableListaTR.buttons().container()
        .appendTo( '#tableListaTR_wrapper .col-sm-6:eq(0)' );


    $('.seleccionarTR').click(function() {

        idTR=$(this).data('codigotr');
          
        $('#ctransmittal').val(idTR);

        $('tr.seleccionarTR').css("background-color", "white");
        $('#TR_'+idTR).css("background-color", "#b3b3cc");
      
    });

    $('#modalBuscarTR').on('hidden.bs.modal', function () {

        if ($('#ctransmittal').val()!='nuevo') {
            c_TR = $('#TR_'+ $('#ctransmittal').val()).data('codigotr');
            tipo_TR = $('#TR_'+ $('#ctransmittal').val()).data('tipotr');
            var url = BASE_URL + '/listaentregablestr/'+$('#cproyecto').val()+'/'+'vertransmittal/'+tipo_TR+'/'+c_TR;
            $(location).attr('href',url);
        }
        
    });

</script>




