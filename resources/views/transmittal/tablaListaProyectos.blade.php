
<table id="tProyecto" class="table table-responsive table-hover" style="font-size: 12px;width: 100%">
    <thead>
      <tr class="clsCabereraTabla">
          <th>Ítem</th>
          <th>Código</th>
          <th>Cliente</th>
          <th>Unidad Minera</th>
          <th>Proyecto</th>
          <th>GP</th>
          <th>Estado</th>
      </tr>
    </thead>
    <tbody>

      @if(isset($proyectos))

       @foreach($proyectos as $pry)

        <tr class="seleccionarPry" data-proyecto="{{ $pry->cproyecto }}" id="proyecto_{{$pry->cproyecto}}">
          <td>{{$pry->cproyecto}}</td>
          <td>{{$pry->codigo}}</td>
          <td>{{$pry->cliente}}</td>
          <td>{{$pry->uminera}}</td>
          <td>{{$pry->nombre}}</td>
          <td>{{$pry->GteProyecto}}</td>
          <td>{{$pry->descripcion}}</td>
        
        </tr>

       @endforeach

      @endif

    </tbody>

</table>

<script src="js/jquery.js" type="text/javascript"></script>
<script src="js/jquery.dataTables.js" type="text/javascript"></script>

<script>

function iniDatatablePry(){

  $('#tProyecto').dataTable().fnDestroy();

  var tablePry = $('#tProyecto').DataTable( {
          "paging":   false,
          "ordering": true,
          "info":     false,
          "autoWidth": true,
          lengthChange: false,
          scrollY:        "450px",
          scrollX:        true,
          scrollCollapse: true,
          paging:         false,
    } );
}



$('.seleccionarPry').click(function() {

   idProyecto=$(this).data('proyecto');
  
    //console.log('#proyecto_'+idProyecto);

    $('#cproyectonuevo').val(idProyecto);

    $('tr.seleccionarPry').css("background-color", "white");
    $('#proyecto_'+idProyecto).css("background-color", "#b3b3cc");
  
});



</script>


