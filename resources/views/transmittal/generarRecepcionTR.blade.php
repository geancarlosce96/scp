@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transmittals
        <small>Recepción Transmittal Cliente - Anddes</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Transmittals</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">

       <!--<div>-->
       <div hidden>
          <input type="text" name="NombreProy" value="{{(isset($proyecto)==1?$proyecto['nombreproyecto']:'')}}">
          
          {!! Form::text('cproyecto',(isset($proyecto)==1?$proyecto['cproyecto']:''),array('id'=>'cproyecto')) !!}
          {!! Form::text('cunidadminera',(isset($proyecto)==1?$proyecto['cunidadminera']:''),array('id'=>'cunidadminera')) !!}
          {!! Form::text('ctransmittal',(isset($transmittal)==1?$transmittal->ctransmittal:'nuevo'),array('id'=>'ctransmittal')) !!}
          {!! Form::text('_token',csrf_token(),array('id'=>'_token')) !!}
        
      </div>

      <div class="row">

          <div class="col-lg-1 col-xs-1">Transmittal N°:</div>

          <div class="col-lg-4 col-xs-4">

              <div class="input-group input-group-normal">
                  <input type="text" class="form-control input-sm" id="numTransmittal" placeholder="" name="numTransmittal" value="{{ isset($numeroTR)==1?$numeroTR:'' }}" readonly> 
                 
                  <span class="input-group-btn" style="size: 14px;">

                     <input type="checkbox" name="" id="activarCodigoTR" >

                  </span>
                                           
              </div> 
              <span style="color: red;display: none" id="validarTR" >El Transmittal ya fue registrado</span>

          </div>
          <div class="col-lg-1 col-xs-1">CD:</div>

          <div class="col-lg-4 col-xs-4">


              <input type="text" class="form-control input-sm" readonly="true" placeholder="" value="{{ isset($personaCD)==1?$personaCD->abreviatura:'' }}"> 
           
          </div>
          <div class="col-lg-2 col-xs-2">
              <button type='button' class="btn btn-primary btn-block" id="btnBuscarTR">
                  <b> <i class="glyphicon glyphicon-search"></i> Transmittal</b>
              </button>
          </div> 

      </div>

      <br>

      <br>

      <div class="row">
        
        <div class="col-lg-1 col-xs-1">Tipo de envío:</div>
        <div class="col-lg-4 col-xs-4">
            {!! Form::select('tipoenvio',(isset($tipoenvio)==1?$tipoenvio:array()),(isset($transmittal)==1?$transmittal->tipoenvio:''),array('class' => 'form-control','id'=>'tipoenvio')) !!} 
        </div> 

        <div class="col-lg-1 col-xs-3">Fecha envío:</div>
            <div class="col-lg-4 col-xs-4">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  {!! Form::text('fechaenvio',(isset($fechaemision)?$fechaemision:''), array('class'=>'form-control pull-right ','id'=>'fechaenvio','readonly'=>'true')) !!}
                </div>
            </div>

      </div>

      <br>

      <div class="row">

          <div class="col-lg-10 col-xs-1"></div>
          <div class="col-lg-2 col-xs-2">
              <button type="button" class="btn btn-success btn-block" id="agregarentregables"><b>Agregar Entregables</b></button>
          </div>
      </div>

      <br>

      <div class="row">

          <div class="col-lg-12 col-xs-12 table-responsive" id='tableEntdivLEntregable' style="height: 400px;">
           
            @include('transmittal.tablaRecepcionEntregablesTR',array('listaEntregablesTR'=> (isset($listaEntregablesTR)==1?$listEntTransmittal:array() )))
          </div>
      </div>
      <br><br>

      <div class="row">

        <div class="col-lg-1 col-xs-1">Comentarios:</div>
        <div class="col-lg-11 col-xs-11">
            <textarea  type="textarea" rows="2" name="comentario" id="comentario"  class="form-control">{{(isset($transmittal)?$transmittal->comentario:'')}}</textarea>
        </div> 

          
      </div>
      <br><br>

    <div class="row">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        <br>
        <div class="col-lg-2 col-xs-2"></div>
        <div class="col-lg-2 col-xs-2">
              <a type="button"  id="" href="{{ url('listaentregablestr',[(isset($proyecto)==1?$proyecto['cproyecto']:'')]) }}" class="btn btn-primary btn-block"><b>Ir a Lista Transmittals</b></a> 
        </div>

        <div class="col-lg-2 col-xs-2">
              @if ($estado_enviado != 'ENV')
                  <a type="button" class="btn btn-primary btn-block" id="btnGuardar"><b>Guardar</b></a> 
                  <a type="button" class="btn btn-primary btn-block" id="btnGuardarTemp" disabled style="display: none;margin-top: 0px"><b>Guardar</b></a> 
              @else
                  <a type="button" class="btn btn-primary btn-block" id="btnGuardarTemp" disabled style="margin-top: 0px"><b>Guardar</b></a> 
              @endif 
        </div>

        <div class="col-lg-2 col-xs-2">
              @if ($estado_enviado != 'ENV')
                  <a type="button" class="btn btn-primary btn-block" id="btnEnviar"><b>Enviar</b></a> 
              @else
                  <a type="button" class="btn btn-primary btn-block" id="btnEnviarTemp" disabled style="margin-top: 0px"><b>Enviar</b></a> 
              @endif
        </div>

        <div class="col-lg-2 col-xs-2">
              <a type="button"  id="btnPrint" class="btn btn-primary btn-block"><b>Imprimir</b></a> 
        </div>
    </div>

    <!--   pk de etregables añadidos  -->
    <div class="hidden"> 

            <input type="text" id="entregablesagregados_input">
            <input type="text" name="tipoTransmittal" value="C-A" id="tipoTransmittal">
            <input type="text" id="tipocontacto" value="todos">
    </div>

    </section>
    <!-- /.content -->


<!-- Modal Agregar Contactos-->

@include('transmittal.modalBuscarTR')
@include('transmittal.modalAgregarEntregablesTR')

<!-- Fin Agregar Contactos-->

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/javascripts/application.js" type="text/javascript" charset="utf-8" async defer></script>

@stop


@section('js')
  <script type="text/javascript" src="{{ asset('js/transmittal/registrarCA_TR.js') }}" ></script>
@stop