<table class="table table-striped table-hover table-bordered" id="tablalogs" width="100%" style="font-size: 12px;">
    <thead>

      <tr></tr>
      <tr>
        <th class="colorAnddes">Item</th>
        <th class="colorAnddes">EDT</th>
        <th class="colorAnddes">Área</th>
        <th class="colorAnddes">T. Doc</th>
        <th class="colorAnddes">Código Anddes</th>
        <th class="colorAnddes">Descripción</th>
        <th class="colorAnddes">Revisión</th>
        <th class="colorAnddes">TR</th>
        <th class="colorAnddes">Motivo de Envío</th>
        <th class="colorAnddes">Fecha de Emisión</th>
        <th class="colorAnddes">Envío</th>
        <th class="colorVerde">TR CLI</th>
        <th class="colorVerde">Estado</th>
        <th class="colorVerde">Fecha</th>
        <th class="colorVerde">Días</th>
      </tr>
    </thead>
   
    <tbody>
        @if(isset($entTodosTR))
            @foreach($entTodosTR as $key => $ent)
            <tr>
                <td>{{$key +1}}</td>
                <td>{{ $ent['edt_cod'] }}</td>
                <td>{{ $ent['area'] }}</td>
                <td>{{ $ent['tipo_ent'] }}</td>
                <td>{{ $ent['codigo'] }}</td>
                <td>{{ $ent['descripcion_entregable'] }}</td>
                <td>{{ $ent['revision'] }}</td>
                <td>{{ $ent['numero_tr'] }}</td>
                <td>{{ $ent['motivo'] }}</td>
                <td>{{ $ent['fechaemision'] }}</td>
                <td>{{ $ent['envio'] }}</td>
                <td>{{ $ent['TR'] }}</td>
                <td></td>
                <td>{{ $ent['TR_fecha'] }}</td>
                <td>{{ $ent['dias'] }}</td>
            </tr>
            @endforeach 
        @endif 
     
    </tbody>
</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>



    var tablalogs = $('#tablalogs').DataTable( {
        "paging":   false,
        "ordering": true,
        "oLanguage": {
            "sInfo": 'Se encontraron _END_ registros.',
            "sInfoEmpty": 'No se encontraron registros',
            "sEmptyTable": 'No se encontraron registros',
        },
        lengthChange: false,
        buttons: [ 
                    { text: 'Exportar a Excel',extend: 'excel'},
                    { text: 'Exportar a PDF',extend: 'pdf',orientation: 'landscape',pageSize: 'A3'},
                    { text: 'Ocultar columnas',extend: 'colvis'}

                 ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,  
     } ); 
 
    tablalogs.buttons().container()
        .appendTo( '#tablalogs_wrapper .col-sm-6:eq(0)' );



</script>

<style>

        .colorAnddes{
            background-color: rgb(0,105,170);
            color: white;
        }

        .colorVerde{
            background-color: rgb(0,176,80);
            color: white;
        }
    
</style>
   


