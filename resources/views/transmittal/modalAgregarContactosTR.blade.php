 <div id="modalContacto" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <h4 class="modal-title" style="color: white">Seleccionar Contactos</h4>
      <div class="modal-body">

        <div class="box-group" id="accordion">
          <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
          <div class="panel panel-primary">
            <div class="box-header with-border panel-heading">
              <h4 class="box-title panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed"> <span class="glyphicon glyphicon-hand-up"></span> Registrar Nuevo Contacto</a>
              </h4>
              <!--<button type="submit" class="btn btn-success bg-green pull-right">Registrar</button>-->
            </div>
            <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
              <div class="box-body">


                <div class="row clsPadding2">
                  <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Apellido Paterno</label>
                    <input type="text" class="form-control" placeholder="Apellido Paterno" name="apaterno" id="apaterno">
                  </div>
                  <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Apellido Materno</label>
                    <input type="text" class="form-control" placeholder="Apellidos Materno" name="amaterno" id="amaterno">
                  </div>    
                  <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Nombres</label>
                    <input type="text" class="form-control" placeholder="Nombres" id="nombres" name="nombres">                                        
                  </div>    
                </div>

                <div class="row clsPadding2">
                  <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Teléfono</label>
                    <input type="text" class="form-control" placeholder="Teléfono" name="telefono" id="telefono">
                  </div>
                  <div class="col-lg-8 col-xs-8">
                    <label class="clsTxtNormal">Email</label>
                    <div class="input-group">
                      <span class="input-group-addon">@</span>
                      <input type="text" class="form-control" placeholder="Email" id="email" name="email">
                    </div>                                        
                  </div>    
                </div>

                <div class="row clsPadding2">
                  <div class="col-lg-2 col-xs-2 pull-right">
                    <a type="button" class="btn btn-primary btn-block" id="btnGuardarContacto"><b>Agregar</b></a> 
                  </div>

                </div>

              </div>
            </div>
          </div>
        </div>

        <div class="row clsPadding2">

          <div class="col-lg-12 col-xs-12 table-responsive" id="divContactosTR">
            @include('transmittal.tablaContactosTR',array('listaContactos'=> (isset($listaContactos)==1?$listaContactos:array() )))

          </div>
        </div> 



      </div>
      <div class="modal-footer">

        <div class="col-lg-4 col-xs-4"></div>
        <div class="col-lg-4 col-xs-4">
          
            <button type="button" class="btn btn-primary btn-block" id="btnaceptarContacto" data-dismiss="modal">Agregar Contactos</button>
          
        </div>
       


      </div>
    </div>
  </div>
</div>