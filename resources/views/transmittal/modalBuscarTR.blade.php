
<!-- Modal Agregar Entregables-->
    <div id="modalBuscarTR" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color: white">Seleccionar Transmittal del proyecto {{(isset($proyecto)==1?$proyecto['nombreproyecto']:'')}}</h4>
          </div>
          <div class="modal-body">
          
              <div class="col-lg-12 col-xs-12 table-responsive" id="divListaTR">
                
                @include('transmittal.tablaListaTR',array('listaContactos'=> (isset($listaContactos)==1?$listaContactos:array() )))

              </div>
         
          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-primary" id="" data-dismiss="modal">Aceptar</button>

          </div>
        </div>
      </div>
    </div>
<!-- Fin Agregar Entregables-->