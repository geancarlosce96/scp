<!-- Tabla que muestra los entregables agrgados y por agregar al TRansmittal (Generar A-C) -->
<table class="table table-striped table-hover table-bordered" id="tableEntregablesTR" width="100%" style="font-size: 12px;">
    <thead>
      <tr class="clsCabereraTabla">
        <th width="3%">Item</th>
        <th width="20%">Código Anddes</th>
        <th width="10%">Revisión</th>
        <th width="30%">Descripción</th>
        <th width="20%">Motivo de Envío</th>
        <th width="10%">Fecha de Emisión</th>
       
        <th width="3%">Acciones</th>
        
      </tr>
    </thead>
   
    <tbody>

        @if(isset($entregablesTR))
       
            @foreach($entregablesTR as $key => $ent)
            <tr class='agregados_TR entregablesAgregadosTR' data-cproyectoentregables="{{ $ent->cproyectoentregables }}" data-ctransmittaldetalle="{{ $ent->ctransmittaldetalle }}">
                <td class="item_ent">{{$key +1}}</td>
                <td class='codigo_ent_TR'>{{ $ent->codigo_entregable }}</td>
                <td class='revision_ent_TR'>
                    <div>
                        
                        {!! Form::select('revisiones',(isset($revisiones)==1?$revisiones:array()),(isset($ent->revision_entregable)==1?$ent->revision_entregable:''),array('class' => 'form-control','id'=>'revisiones', 'disabled')) !!}

                    </div> <!-- {{ $ent->revision }} -->


                </td>
                <td class='descripcion_ent_TR'>{{ $ent->descripcion_entregable }}</td>
                <td class='motivo_ent_TR'><!-- {{ $ent->motivo }} -->

                    <div>   
                        {!! Form::select('motivo',(isset($motivo)==1?$motivo:array()),(isset($ent->cmotivoenvio)==1?$ent->cmotivoenvio:''),array('class' => 'form-control','id'=>'motivo')) !!}    
                    </div>
                </td>
                <td> 
                    @if(isset($transmittal->created_at))
                        {{$transmittal->fechaemision}}
                    @endif 
                </td>
                <td>
                    <!-- <a title="Eliminar" class="eliminarEnt_BD"><i class="fa fa-trash-o" aria-hidden="true"></i></a> -->

                </td>
            </tr>
            @endforeach 
        @endif 
     
    </tbody>
</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>


    $('.eliminarEnt_BD').click(function(){

        cdetalleTR=$(this).parent().parent().data('ctransmittaldetalle');

        eliminarEntregable(cdetalleTR);
    });


    function eliminarEntregable(cdetalleTR)
    {

        var objRequest = {};
        objRequest.ctrdetalle = cdetalleTR;
        objRequest._token = $('input[name="_token"]').first().val();
        
        var url = BASE_URL + '/eliminarDetalleTR';
        $.post(url, objRequest)
            .done(function( data ) {
                actualizar_tabla_entregables();
        }); 

    }

</script>
   


