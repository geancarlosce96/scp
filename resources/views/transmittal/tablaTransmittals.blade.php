<table class="table table-striped table-hover table-bordered" id="tablaTR" width="100%" style="font-size: 12px;">

    <thead>
        <tr class="clsCabereraTabla">
            <th>Item</th>
            <th>Transmittal</th>
            <th>Fecha de Emisión</th>
            <th>Tipo de Envío</th>
            <th>Acciones</th>
        </tr>
    </thead>
   
    <tbody>
        @if(isset($transmittals))
            @foreach($transmittals as $key => $tr)

            <tr data-codigotr="{{$tr->ctransmittal}}" data-tipotr="{{$tr->tipotransmittal}}">
                <td>{{$key +1}}</td>
                <td>{{$tr->numero}}</td>
                <td>{{$tr->fechaenvio}}</td>
                <td>{{$tr->envio}}</td>
                <td><a class="btn btn-primary fa fa-eye verEntTR"></td>
            </tr>

            @endforeach 
        @endif 
    </tbody>

</table>

<script>
   
    var tableTR = $('#tablaTR').DataTable( {
    "paging":   true,
    "ordering": true,
    "info":     false,
   // "order":     [[0,"desc"]],
    lengthChange: true,
    //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
    } );
 
    tableTR.buttons().container()
        .appendTo( '#tablaTR_wrapper .col-sm-6:eq(0)' );

     $('.verEntTR').click( function () {

      var c_TR=$(this).parent().parent().data('codigotr');
      var tipo_TR=$(this).parent().parent().data('tipotr');

      var url = BASE_URL + '/listaentregablestr/'+$('#cproyecto').val()+'/'+'vertransmittal/'+tipo_TR+'/'+c_TR;

       $(location).attr('href',url);

    
    });


</script>