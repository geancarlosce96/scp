<table class="table table-striped table-hover table-bordered" id="tableEntregablesTodosTR" width="100%" style="font-size: 12px;">
    <thead>
      <tr class="clsCabereraTabla">
        <th>Item</th>
        <th>Código Anddes</th>
        <th  width="5%">Revisión</th>
        <th>Descripción</th>
        <th>Fecha de Emisión</th>
       
        <th>Acciones</th>
        
      </tr>
    </thead>
   
    <tbody>

        @if(isset($entTodosTR))
            @foreach($entTodosTR as $key => $ent)
            <tr  data-cproyectoentregables="{{ $ent['cproyectoentregables'] }}">
                <td>{{$key +1}}</td>
                <td class="codigo_ent">{{ $ent['codigo'] }}</td>
                <td class="revision_ent" data-crevision="{{ $ent['crevision'] }}">{{ $ent['des_revision'] }}</td>
                <td class="decripcion_ent">{{ $ent['descripcion'] }}</td>
                <td class="fechaEmision_ent">{{ $ent['fechaemisionTR'] }}</td>
                   
                <td>
                    <a class="btn btn-primary fa fa-eye verTR" title="Ver TR"> </a>
                </td>
            </tr>
            @endforeach 
        @endif 
     
    </tbody>
</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

$(document).ready(function(){

  $('.select-box').chosen({
        allow_single_deselect: true,width:"100%"
    }); 

  $('.verTR').click( function () {

        var objPryEnt=$(this).parent().parent().data('cproyectoentregables');
        var codigo = $(this).parent().siblings('.codigo_ent').text();
        var descripcion = $(this).parent().siblings('.decripcion_ent').text();
        console.log(descripcion);

        $('#nombreEntSeleccionado').html(codigo+' | '+descripcion);

         var url = BASE_URL + '/listarTRporEntregables/'+objPryEnt;

            $.get(url)
                .done(function( data ) { 

                $('#divtableTR_Ent').html(data);
                $('#collapseTR').addClass('in');
             
            });


    
  });

   var tableEntTodosTR = $('#tableEntregablesTodosTR').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     false,
        lengthChange: false,
        scrollY:        "500px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false 
        
    } ); 

  tableEntTodosTR.buttons().container()
        .appendTo( '#tableEntregablesTodosTR_wrapper .col-sm-6:eq(0)' );


});
</script>
   


