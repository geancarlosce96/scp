<table class="table table-striped table-hover table-bordered" id="tabla_modal_AgregarEntregablesTR" style="font-size: 12px">
    <thead>
      <tr class="clsCabereraTabla">
        <th>Item</th>
        <th>Código Anddes</th>
        <th>Revisión</th>
        <th>Descripción</th>
       <th>Fecha de Emisión</th>
       
        <th>Acciones
            <input id="selecTodosEntr" type="checkbox">
        </th>
        
      </tr>
    </thead>
   
    <tbody>

        @if(isset($listEnt))
            @foreach($listEnt as $key => $ent)
            <tr id="agregar_ent_{{ $ent['cproyectoentregables'] }}" data-centregable="{{ $ent['cproyectoentregables'] }}">
                <td>{{$key +1}}</td>
                <td class="codigo_ent">{{ $ent['codigo'] }}</td>
                <td class="revision_ent" data-crevision="{{ $ent['crevision'] }}">{{ $ent['des_revision'] }}</td>
                <td class="decripcion_ent">{{ $ent['descripcion'] }}</td>
                <td class="fechaEmision_ent">
                        {{ $ent['fmodificacion_ent'] }}
                </td>
                <td>
                    <center>
                        
                        <input class="entregablesagregados_chk" id="check_{{$ent['cproyectoentregables'] }}" type="checkbox" name="centregables[]" value="{{$ent['cproyectoentregables'] }}">
                    </center>
                </td>
            </tr>
            @endforeach 
        @endif 
     
    </tbody>
</table>

<script>

$(document).ready(function(){

    $('#selecTodosEntr').click(function(){

        if ($('#selecTodosEntr').prop('checked')) {

            $('.entregablesagregados_chk').each(

                function(){

                    if ($(this).is(':visible')==true) {

                        $(this).prop('checked',true)
                    }

                }
            );

        }
        else{
            $('.entregablesagregados_chk').prop('checked',false)   
        }

    })


    //function agregarDatatableBuscarEntregable(){

        //$('#tabla_modal_AgregarEntregablesTR').DataTable().destroy();


    //}
 
  
    

});

function iniDatatableAddEnt(){

  $('#tabla_modal_AgregarEntregablesTR').dataTable().fnDestroy();

      var tabla_modal_AgregarEntregablesTR = $('#tabla_modal_AgregarEntregablesTR').DataTable( {
            "paging":   false,
            "ordering": true,
            "info":     false,
            "autoWidth": false,
            scrollY:        "500px",
            scrollX:        true,
            scrollCollapse: true,
            paging:         false,
       // "order":     [[0,"desc"]],
        //lengthChange: true,
        //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
        } );
}



</script>
   


