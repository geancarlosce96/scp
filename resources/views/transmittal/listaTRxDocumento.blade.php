@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Transmittal
        <small>Listado de Transmittals por Documento</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Transmittal</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
      


       {!! Form::open(array('url' => 'grabarEntregablesProyecto','method' => 'POST','id' =>'frmproyectoentregables')) !!}
    
    <div class="row">

      <div class="col-lg-1 col-xs-1">Logo de Cliente:</div>

      <div class="col-lg-3 col-xs-3">

        @if(isset($proyecto['logouminera']))
            <img src="{{  url('images/logounidadminera/'.$proyecto['logouminera']) }}" style="max-width: 150px;min-width: 10px;max-height: 50px;min-width: 10px" class="user-image" alt="User Image">
        @endif
      </div>
      <div class="col-lg-6 col-xs-6"></div>
      <div class="col-lg-2 col-xs-2">
            <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'>
               <b> <i class="glyphicon glyphicon-search"></i> Proyectos</b>
            </button>
      </div> 
    </div>

    <div class="row">
        <div hidden>

          {!! Form::text('cproyecto',(isset($proyecto)==1?$proyecto['cproyecto']:''),array('id'=>'cproyecto')) !!}
          {!! Form::text('cproyectonuevo',(isset($proyecto)==1?$proyecto['cproyecto']:''),array('id'=>'cproyectonuevo')) !!}
          {!! Form::text('_token',csrf_token(),array('id'=>'_token')) !!}
          
        </div>
        <div class="row"><br></div>
        <div class="col-lg-1 col-xs-1">Cliente:</div>
        <div class="col-lg-3 col-xs-3">

            {!! Form::text('cliente',(isset($proyecto)==1?$proyecto['cliente']:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','readonly' => 'true','id' => 'cliente')) !!}
        </div>
        <div class="col-lg-2 col-xs-2">Unidad minera:</div>
        <div class="col-lg-3 col-xs-3">
        <?php //dd($proyecto['uminera']) ?>
            {!! Form::text('uminera',(isset($proyecto)==1?$proyecto['uminera']:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','readonly' => 'true','id'=>'uminera') ) !!}
        </div>
        <div class="col-lg-1 col-xs-1">Estado:</div>
        <div class="col-lg-2 col-xs-2">

          {!! Form::text('estadoproyecto',(isset($proyecto)==1?$proyecto['estadoproyecto']:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','readonly' => 'true','id'=>'estadoproyecto') ) !!}
            
        </div>
        <!-- <div class="col-lg-1 col-xs-1">Buscar Proyecto:</div>
        <div class="col-lg-1 col-xs-1">
            <button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'>
               <b> <i class="glyphicon glyphicon-search"></i> Proyectos</b>
            </button>
        </div> --> 
    </div>
    <div class="row">
        <div class="row"><br></div>
        <div class="col-lg-1 col-xs-1">Código Proyecto:</div>
        <div class="col-lg-3 col-xs-3">
            {!! Form::text('codproyecto',(isset($proyecto)==1?$proyecto['codproyecto']:''),array('readonly'=>'true','class'=> 'form-control input-sm' ,'id'=>'codproyecto')) !!}
        </div>
        <div class="col-lg-2 col-xs-2">Nombre del Proyecto:</div>
        <div class="col-lg-6 col-xs-6">
            {!! Form::text('nombreproyecto',(isset($proyecto)==1?$proyecto['nombreproyecto']:''),array('class'=>'form-control input-sm','readonly' =>'true','placeholder'=>'Nombre de Proyecto','id'=>'nombreproyecto')) !!}   
        </div>
   
    </div>

    <br>

    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>

    <br>

    <div class="row"> 

        <div class="col-lg-12 col-xs-12 table-responsive" id='tableEntdivLEntregable'>
         @include('transmittal.tablaEntregablesTodosTR',array('entTodosTR'=> (isset($entTodosTR)==1?$entTodosTR:array() )))
        </div> 

    </div>

    <br>

    <div class="row">
        <div class="box-body">
            <div class="box-group" id="accordion2">
                <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                <div class="panel panel-primary">
                    <div class="box-header with-border panel-heading">
                        <h4 class="box-title panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTR" aria-expanded="false" class="collapsed"> <span class="glyphicon glyphicon-hand-up"></span> Transmittals de entregable <span id="codigoEntSeleccionado"></span>   <span id="nombreEntSeleccionado"></span>  </a>
                        </h4>
                        <!--<button type="submit" class="btn btn-success bg-green pull-right">Registrar</button>-->
                    </div>
                    <div id="collapseTR" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="box-body">

                          <div class="col-lg-12 col-xs-12 table-responsive" id='divtableTR_Ent'>
                            @include('transmittal.tablaTransmittals',array('transmittals'=> (isset($transmittals)==1?$transmittals:array() )))
                          </div>
                                                             
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <br>


    <div class="row clsPadding">
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        <br>
        <div class="col-lg-4 col-xs-4"></div>
        <div class="col-lg-2 col-xs-2">
              <a type="button"  id="" href="{{ url('generar_transmittal_nuevo',[(isset($proyecto)==1?$proyecto['cproyecto']:'')]) }}?ent=sin_grupo" class="btn btn-primary btn-block"><b>Nuevo Transmittal</b></a> 
        </div>
        <div class="col-lg-2 col-xs-2">
              <a type="button"  id="" href="{{ url('generar_recepcion_transmittal',[(isset($proyecto)==1?$proyecto['cproyecto']:'')]) }}?ent=sin_grupo" class="btn btn-primary btn-block"><b>Recepción de Transmittal</b></a> 
        </div>

    </div>

    <br>

   {!! Form::close() !!}

    </section>
    <!-- /.content -->

    @include('transmittal.modalListaProyectos')
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  
<script type="text/javascript">


$(document).ready(function(){

  obtenerProyectos();

  $('#btnSeleccionarPry').click(function () {
      if ($('#cproyectonuevo').val()!="") {
        cargarDatosProyecto($('#cproyectonuevo').val());
      }
  });


  function cargarDatosProyecto(id){
      var url = BASE_URL + '/listaentregablestr/'+id;
      $(location).attr('href',url);
     
  }

  $('#searchProyecto').on('show.bs.modal', function() {
        setTimeout(iniDatatablePry,2000);
  })
 
});

function obtenerProyectos(){
       var url = BASE_URL + '/listarProyectosTr';
       $.get(url)
          .done(function( data ) { 
            $('#tablaProyectos').html(data);
          });
} 




</script>


@stop