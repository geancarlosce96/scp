<!-- Modal Agregar Entregables-->
    <div id="searchEntregable" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
              <h4 class="modal-title" style="color: white">Agregar Entregables a Transmittal
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
              </h4>
          </div>
          <div class="modal-body">
            <div id="">
              @include('transmittal.tablaAgregarEntregablesTransmittal',array('listEnt'=> (isset($listEnt)==1?$listEnt:array() )))
            </div>
          </div>
          <div class="modal-footer">

            <button type="button" class="btn btn-primary" id="aceptarEnt" data-dismiss="modal">Aceptar</button>

          </div>
        </div>
      </div>
    </div>
<!-- Fin Agregar Entregables-->

