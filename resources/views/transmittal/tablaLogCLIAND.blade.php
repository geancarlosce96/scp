<table class="table table-striped table-hover table-bordered" id="tablalogcliand" width="100%" style="font-size: 12px;">
    <thead>

      <tr></tr>
      <tr>
        <th class="colorVerde">Item</th>
        <th class="colorVerde">TR</th>
        <th class="colorVerde">Documento N°</th>
        <th class="colorVerde">Revisión</th>
        <th class="colorVerde">Descripción</th>
        <th class="colorVerde">Emitido Para</th>
        <th class="colorVerde">Fecha</th>
        <th class="colorVerde">Envío</th>
        <th class="colorVerde">Estado 1</th>
        <th class="colorVerde">Estado 2</th>
        <th class="colorAnddes">TR AND</th>
        <th class="colorAnddes">Estado 3</th>
        <th class="colorAnddes">Fecha</th>
      </tr>
    </thead>
   
    <tbody>

        @if(isset($entTodosTR))
            @foreach($entTodosTR as $key => $ent)
            <tr>
                <td>{{$key +1}}</td>
                <td>{{ $ent['numero_tr'] }}</td>
                <td>{{ $ent['codigo'] }}</td>
                <td>{{ $ent['revision'] }}</td>
                <td>{{ $ent['descripcion_entregable'] }}</td>
                <td>{{ $ent['motivo'] }}</td>
                <td>{{ $ent['fechaemision'] }}</td>
                <td>{{ $ent['envio'] }}</td>
                <td></td>
                <td></td>
                <td>{{ $ent['TR'] }}</td>
                <td></td>
                <td>{{ $ent['TR_fecha'] }}</td>
            </tr>
            @endforeach 
        @endif 
     
    </tbody>
</table>

<script src="https://ajax.googleapis.com/ajax/libsjquery/3.3.1/jquery.min.js"></script>
<script>

    var tablalogcliand = $('#tablalogcliand').DataTable( {
        "paging":   false,
        "ordering": true,
        "oLanguage": {
            "sInfo": 'Se encontraron _END_ registros.',
            "sInfoEmpty": 'No se encontraron registros',
            "sEmptyTable": 'No se encontraron registros',
        },
        lengthChange: false,
        buttons: [ 
                    { text: 'Exportar a Excel',extend: 'excel'},
                    { text: 'Exportar a PDF',extend: 'pdf',orientation: 'landscape',pageSize: 'A3'},
                    { text: 'Ocultar columnas',extend: 'colvis'}

                 ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,  
     } ); 
 
    tablalogcliand.buttons().container()
        .appendTo( '#tablalogcliand_wrapper .col-sm-6:eq(0)' );

</script>

<style>

        .colorAnddes{
            background-color: rgb(0,105,170);
            color: white;
        }

        .colorVerde{
            background-color: rgb(0,176,80);
            color: white;
        }
    
</style>
   


