<table class="table table-striped table-hover table-bordered" id="tableRecepcionEntregablesTR" width="100%" style="font-size: 12px;">
    <thead>
      <tr class="clsCabereraTabla">
        <th width="3%">Item</th>
        <th width="20%">Código Anddes</th>
        <th width="10%">Revisión</th>
        <th width="40%">Descripción</th>
        <th width="20%">Emitido para</th>
        <th width="3%">Acciones</th>
        
      </tr>
    </thead>
   
    <tbody>

        @if(isset($entregablesTR))
       
            @foreach($entregablesTR as $key => $ent)
                    <?php  //dd($entregablesTR) ?>
            <tr class='agregados_TR entregablesAgregadosTR' data-cproyectoentregables="{{ $ent->cproyectoentregables }}" data-ctransmittaldetalle="{{ $ent->ctransmittaldetalle }}" data-agregado="si">

                <td class="item_ent">{{$key +1}}</td>
                <td class='codigo_ent_TR'>
                        <input class="form-control inputCodEntreg" value="{{ $ent->codigo_entregable }}" data-id="{{ $ent->cproyectoentregables }}" readonly="">
                       <!--  <input list="entregables_{{ $ent->cproyectoentregables }}" class="form-control inputCodEntreg" value="{{ $ent->codigo_entregable }}" >
                        <datalist id="entregables_{{ $ent->cproyectoentregables }}">  
                                <option data-id="{{ $ent->cproyectoentregables }}" value="{{ $ent->codigo_entregable }}" data-revision="{{ $ent->revision_entregable }}"></option>
                        </datalist> -->
                </td>
                <td class='revision_ent_TR'>
                    <div>
                        
                        {!! Form::select('revisiones',(isset($revisiones)==1?$revisiones:array()),(isset($ent->revision_entregable)==1?$ent->revision_entregable:''),array('class' => 'form-control','id'=>'revisiones')) !!}

                    </div> <!-- {{ $ent->revision }} -->


                </td>
                <td class='descripcion_ent_TR'><input class="form-control" type="text" value="{{ $ent->descripcion_entregable }}" readonly ></td>
                <td class='motivo_ent_TR'><!-- {{ $ent->motivo }} -->

                    <div>   
                        {!! Form::select('motivo',(isset($motivo)==1?$motivo:array()),(isset($ent->cmotivoenvio)==1?$ent->cmotivoenvio:''),array('class' => 'form-control','id'=>'motivo')) !!}    
                    </div>
                </td>
                <td>
                    <center>
                        <!--<a title="Eliminar" class="eliminarEnt"><i class="fa fa-trash-o" aria-hidden="true"></i></a>-->
                        <!-- <a title="Editar" class="editarEnt"><i class="fa fa-edit" aria-hidden="true"></i></a> -->
                    </center>
                </td>
            </tr>
            @endforeach 
        @endif 
     
        <tr id='addEntTR' style="text-align: center;" title="Agregar Nueva Fila">
            <td>
               <label class="btn btn-primary">Agregar</label>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>
    </tbody>
   
</table>




 