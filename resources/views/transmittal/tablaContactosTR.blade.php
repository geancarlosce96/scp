<table class="table table-striped table-hover table-bordered" id="tableContactosTR" width="100%" style="font-size: 12px;">
  <thead>
    <tr class="clsCabereraTabla">
      <th>Apellido Paterno</th>
      <th>Apellido Materno</th>
      <th>Nombres</th>
      <th>Tipo Contactos</th>
      <th>Cargo</th>
      <th>Correo</th>
      <th>Agregar</th>
    </tr>
  </thead>
  <tbody>
    @if(isset($listaContactos))
      @foreach($listaContactos as $key => $lc)

      <tr data-cumcontacto="{{$lc['cunidadmineracontacto']}}">
          <td class="contactoApaterno">{{$lc['apaterno']}}</td>
          <td>{{$lc['amaterno']}}</td>
          <td class="contactoNombre">{{$lc['nombres']}}</td>
          <td>{{$lc['tipo']}}</td>
          <td>{{$lc['cargo']}}</td>
          <td>{{$lc['email']}}</td>
          <td>  

            <?php /*

               <input type="checkbox" name="contactos"
                  @if($lc['transmittalActual']==$lc['ctransmittal'])

                      @if($lc['seleccionado']==$lc['tipocontacto'])
                        checked="checked"
                       
                      @endif

                  @endif 

                >

            */ ?>


              <input type="checkbox" name="contactos" value="{{$lc['cunidadmineracontacto']}}" id="cont_{{$lc['cunidadmineracontacto']}}">

          </td>
        
      </tr>

      @endforeach 
    @endif 


  </tbody>
</table> 