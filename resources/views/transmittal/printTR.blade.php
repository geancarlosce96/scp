
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title></title>
    <link rel="stylesheet" href="">
    <style>
        /** Establezca los márgenes de la página en 0, de modo que el pie de página y el encabezado Puede ser de la altura y anchura total. !**/
        @page {
            margin: 1cm 2cm;
        }
        /** Define ahora los márgenes reales de cada página en el PDF. **/
        body {
            margin-top: 3.05cm;
            margin-left: 0cm;
            margin-right: 0cm;
            margin-bottom: 0.5cm;
        }
        /** Definir las reglas de encabezado. **/
        header {
            position: fixed;
            top: 1.15cm;
            left: 0cm;
            right: 0cm;
            height: 0cm;
        }
        td{
            height: 14px;
        }
    </style>
</head>
    <body>
        <header  style="font-family: Arial,Helvetica,sans-serif;">
            <div class="row">
                <table style="border-collapse: collapse; size: 0.5px;font-size: 11px;width: 100%">
                    <tr>
                        <td rowspan="1" style="border:1px solid #000000FF; width: 30%;text-align: center;height:5%"><img style="width: 150px;" src="images/logorpt.jpg"></td>
                        <td rowspan="2" style="border:1px solid #000000FF; width: 40%;text-align: center;font-size: 11px;"><strong>CONTROL DOCUMENTARIO<br>
                                Transmittal  </strong></td>
                        <td rowspan="1"  style="border:1px solid #000000FF; width: 30%;font-size: 9px">

                            <center>
                            <!-- @if(isset($proyecto['logouminera'])) -->
                                <img src="{{ 'images/logounidadminera/'.$proyecto['logouminera'] }}" class="user-image" alt="User Image" style="max-width: 150px;min-width: 10px;max-height: 50px;min-width: 10px">
                            <!-- @endif -->
                            </center>

                        </td>
                    </tr>
                    <tr>

                        <td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> SIG AND </td>
                        <td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> 10-AND-27-FOR-0101 / R1 / 04-10-18 </td>
                    </tr>
                </table>
            </div>

        </header>
        <section class="content" style="font-family: Arial,Helvetica,sans-serif;">
            <div class="row">

                <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">

                    <tr>
                        <td style="border:1px solid #000000FF;width: 8%;"">Para</td>
                        <td style="border:1px solid #000000FF;width: 40%;"><?php echo (isset($proyecto)==1?$proyecto['cliente']:'');?></td>
                        <td style="border:1px solid #000000FF; width: 12%;">Transmittal N°</td>
                        <td colspan="3" style="border:1px solid #000000FF; width: 40%;"><strong><?php echo (isset($transmittal)==1?$transmittal->numero:'');?></strong></td>
                    </tr>
                    <tr>
                        <td style="border:1px solid #000000FF; width: 8%;">Atención</td>
                        <td style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($arrayCA_nom)==1?ucwords(mb_strtolower(str_replace(',',', ',$arrayCA_nom))):'');?></td>
                        <td style="border:1px solid #000000FF; width: 12%;">Proyecto</td>
                        <td colspan="3" style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($proyecto)==1?$proyecto['nombreproyecto']:'');?></td>
                    </tr>

                    <tr>
                        <td style="border:1px solid #000000FF; width: 8%;">C.C</td>
                        <td style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($arrayCC_nom)==1?ucwords(mb_strtolower(str_replace(',',', ',$arrayCC_nom))):'');?></td>
                        <td style="border:1px solid #000000FF; width: 12%;">Referencia</td>
                        <td colspan="3" style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($proyecto)==1?$proyecto['uminera']:'');?></td>
                    </tr>

                    <tr>
                        <td style="border:1px solid #000000FF; width: 8%;">Envío</td>
                        <td style="border:1px solid #000000FF; width: 40%;"><?php echo (isset($tipoenvio)==1?$tipoenvio->descripcion:'');?></td>
                        <td style="border:1px solid #000000FF; width: 12%;">Código</td>
                        <td style="border:1px solid #000000FF; width: 14%;"><?php echo (isset($proyecto)==1?$proyecto['codproyecto']:'');?></td>
                        <td style="border:1px solid #000000FF; width: 8%;">Fecha</td>
                        <td style="border:1px solid #000000FF; width: 18%;"><?php echo (isset($fechaemision)==1?$fechaemision:'');?></td>
                    </tr>

                </table>
            </div>
            <div class="row">

                <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
                    <thead>
                    <tr>
                        <td style="border:1px solid #000000FF;width: 7.55%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>N°</strong>
                        <td style="border:1px solid #000000FF;width: 22.45%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Documento N°</strong>
                        <td style="border:1px solid #000000FF;width: 4%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Rev.</strong>
                        <td style="border:1px solid #000000FF;width: 48.4%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Título o descripción</strong>
                        <td style="border:1px solid #000000FF;width: 5%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Tipo</strong>
                        <td style="border:1px solid #000000FF;width: 12%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Cantidad</strong>
                    </tr>
                    </thead>

                    <tbody >

                    <?php   $inicio=0; ?>

                    @if(isset($entregablesTR))
                        <?php   $inicio=count($entregablesTR) ?>
                        @foreach($entregablesTR as $key=>$le)

                            <tr>
                                <td style="padding-top: 5px; vertical-align: text-top; border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center>{{$key+1}}</center></td>
                                <td style="padding-top: 5px; vertical-align: text-top; border-left:1px solid #000000FF;border-right: 1px solid #000000FF;">{{ $le->codigo_entregable }}</td>
                                <td style="padding-top: 5px; vertical-align: text-top; border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center>{{ $le->revision }}</center></td>
                                <td style="padding-top: 5px; vertical-align: text-top; border-left:1px solid #000000FF;border-right: 1px solid #000000FF;">{{ $le->descripcion_entregable }}</td>
                                <td style="padding-top: 5px; vertical-align: text-top; border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center>{{ $le->abreviatura }}</center></td>
                                <td style="padding-top: 5px; vertical-align: text-top; border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center>1</center></td>
                            </tr>

                        @endforeach

                    @endif

                    <?php for ($i=$inicio; $i <= 33; $i++) { ?>

                    <tr style="page-break-before: always;">
                        <td style="border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center></center></td>
                        <td style="border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center></center></td>
                        <td style="border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center></center></td>
                        <td style="border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center></center></td>
                        <td style="border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center></center></td>
                        <td style="border-left:1px solid #000000FF;border-right: 1px solid #000000FF;"><center></center></td>
                    </tr>

                    <?php  } ?>

                    </tbody>

                    <tfoot>
                    <tr>
                        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%;">
                            <tbody style="border: 1px solid #000000FF;">
                            <tr>
                                <th style="text-align: left;padding-left: 7px">Tipo de Envío:</th>
                                <th colspan="2"></th>
                            </tr>
                            <tr>
                                <td style="padding-left: 16px;text-align: left; width: 31%;">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #000000FF;width: 25px;height: 15px;padding-top: 3px"><center>A</center></div>
                                            </td>
                                            <td>
                                                Para Revisión y Aprobación
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td style="padding-left: 16px;text-align: left; width: 40%">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #000000FF;width: 25px;height: 15px;padding-top: 3px"><center>AP</center></div>
                                            </td>
                                            <td>
                                                Aprobado
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td style="padding-left: 16px;text-align: left; width: 30%">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #000000FF;width: 25px;height: 15px;padding-top: 3px"><center>D</center></div>
                                            </td>
                                            <td>
                                                RFI - Para Respuesta
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            </tr>

                            <tr>
                                <td style="padding-left: 16px;text-align: left; width: 30%">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #000000FF;width: 25px;height: 15px;padding-top: 3px"><center>B</center></div>
                                            </td>
                                            <td>
                                                Para Información
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td style="padding-left: 16px;text-align: left; width: 40%">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #000000FF;width: 25px;height: 15px;padding-top: 3px"><center>AC</center></div>
                                            </td>
                                            <td>
                                                Aprobado con Comentarios
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td style="padding-left: 16px;text-align: left; width: 30%">
                                    &nbsp;
                                </td>

                            </tr>

                            <tr>
                                <td style="padding-left: 16px;text-align: left; width: 30%">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #000000FF;width: 25px;height: 15px;padding-top: 3px"><center>C</center></div>
                                            </td>
                                            <td>
                                                Documento Final
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                                <td style="padding-left: 16px;text-align: left; width: 70%" colspan="2">
                                    <table>
                                        <tr>
                                            <td>
                                                <div style="border: 1px solid #000000FF;width: 25px;height: 15px;padding-top: 3px"><center>NA</center></div>
                                            </td>
                                            <td>
                                                No Aprobado, Corregir y Emitir Nuevamente
                                            </td>
                                        </tr>
                                    </table>
                                </td>

                            </tr>

                            </tbody>

                            <tfoot>
                            <tr>

                                <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
                                    <tbody>
                                    <tr>
                                        <td style="border:1px solid #000000FF;width: 50%">

                                            <table>
                                                <tr>
                                                    <td><strong>Recibido por:</strong></td>
                                                    <td></td>

                                                </tr>
                                                <tr style="padding-top: 10px">
                                                    <td><strong>Fecha:</strong></td>
                                                    <td></td>

                                                </tr>
                                                <tr style="padding-top: 10px">
                                                    <td><strong>Firma:</strong></td>
                                                    <td></td>

                                                </tr>
                                            </table>

                                        </td>

                                        <td style="border:1px solid #000000FF;width: 50%">

                                            <table>
                                                <tr>
                                                    <td><strong>De:</strong></td>
                                                    <td style="padding-top: 10px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo (isset($personaCD)==1?$personaCD->abreviatura:'');?> <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Control Documentario </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                </tr>

                                                <tr>
                                                    <td><strong>Firma:</strong></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <br>
                                    </tbody>
                                </table>
                            </tr>
                            </tfoot>
                        </table>

                    </tr>


                    </tfoot>

                </table>
            </div>
        </section>
    </body>
</html>