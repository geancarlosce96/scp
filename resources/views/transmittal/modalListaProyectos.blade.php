<!-- Modal Buscar Proyecto-->
    <div id="searchProyecto" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color: white">Buscar proyecto</h4>
          </div>
          <div class="modal-body">

            <div id="tablaProyectos"  class="col-lg-12">
              @include('transmittal.tablaListaProyectos')
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSeleccionarPry">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Proyecto-->