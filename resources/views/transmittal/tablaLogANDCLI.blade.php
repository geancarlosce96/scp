<table class="table table-striped table-hover table-bordered" id="tablalogandcli" width="100%" style="font-size: 12px;">
    <thead>

      <tr></tr>
      <tr>
        <th class="colorAnddes">Item</th>
        <th class="colorAnddes">TR</th>
        <th class="colorAnddes">Documento N°</th>
        <th class="colorAnddes">Revisión</th>
        <th class="colorAnddes">Descripción</th>
        <th class="colorAnddes">Emitido Para</th>
        <th class="colorAnddes">Fecha</th>
        <th class="colorAnddes">Envío</th>
        <th class="colorVerde">TR CLI</th>
        <th class="colorVerde">Estado</th>
        <th class="colorVerde">Fecha</th>
        <th class="colorAnddes">Estado 1</th>
        <th class="colorAnddes">Estado 2</th>
        <th class="colorAnddes">Opinion</th>
      </tr>
    </thead>
   
    <tbody>

        @if(isset($entTodosTR))
            @foreach($entTodosTR as $key => $ent)
            <tr>
                <td>{{$key +1}}</td>
                <td>{{ $ent['numero_tr'] }}</td>
                <td>{{ $ent['codigo'] }}</td>
                <td>{{ $ent['revision'] }}</td>
                <td>{{ $ent['descripcion_entregable'] }}</td>
                <td>{{ $ent['motivo'] }}</td>
                <td>{{ $ent['fechaemision'] }}</td>
                <td>{{ $ent['envio'] }}</td>
                <td>{{ $ent['TR'] }}</td>
                <td></td>
                <td>{{ $ent['TR_fecha'] }}</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
            @endforeach 
        @endif 
     
    </tbody>
</table>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>


 

   var tablalogandcli = $('#tablalogandcli').DataTable( {
        "paging":   false,
        "ordering": true,
        "oLanguage": {
            "sInfo": 'Se encontraron _END_ registros.',
            "sInfoEmpty": 'No se encontraron registros',
            "sEmptyTable": 'No se encontraron registros',
        },
        lengthChange: false,
        buttons: [ 
                    { text: 'Exportar a Excel',extend: 'excel'},
                    { text: 'Exportar a PDF',extend: 'pdf',orientation: 'landscape',pageSize: 'A3'},
                    { text: 'Ocultar columnas',extend: 'colvis'}

                 ],
        scrollY:        "600px",
        scrollX:        true,
        scrollCollapse: true,
        paging:         false,  
     } ); 
 
    tablalogandcli.buttons().container()
        .appendTo( '#tablalogandcli_wrapper .col-sm-6:eq(0)' );


</script>

<style>

        .colorAnddes{
            background-color: rgb(0,105,170);
            color: white;
        }

        .colorVerde{
            background-color: rgb(0,176,80);
            color: white;
        }
    
</style>
   


