
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Logistica
      <small>Listado de Requerimiento para aprobar</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Logistica</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">

    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">
        <div class="row clsPadding2"> 
          <div class="col-lg-12 col-xs-12">
            <!-- inicio accordeon -->       

            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">

                <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Listado de Solicitudes</a></li>
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Requerimientos</a></li>        
              <!--<li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                  Dropdown <span class="caret"></span>
                </a>
         <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li>-->
              <li class="pull-left header"><i class="fa fa-newspaper-o"></i> Listado de Requerimientos</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">               
                <!-- Pestaña Listado Vacaciones -->
                
                <div class="row clsPadding2" id="tablaarticulosajax">
                  @include('logistica.tabladesolicitudesaprueba')
                </div> 
                <div class="col-12 row clsPadding2 text-center">

                </div>

                <!-- FIN Datos Personales -->                
              </div>
              <div class="tab-pane" id="tab_2-2">               
                <div class="col-lg-12 col-xs-12 table-responsive">
                  <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
                  <table class="table table-striped table-hover table-bordered text-center" id="listaprobaciones" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
                    <thead>
                      <tr class="clsCabereraTabla">
                        <th>Empresa</th>
                        <th>Proyecto</th>
                        <th>Solicitante</th>
                        <th>Entregado A</th>
                        <th>Total de Articulos</th>
                        <th>Estatus</th>
                        <th>Fecha Solicitud</th>
                        <th>Opciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($listadostatus as $arti)
                      <tr>
                        <td  class="">{{ $arti->empresa }}</td>
                        <td  class="">{{ $arti->proyecto }}</td>
                        <td  class="">{{ $arti->solicitante }}</td>
                        <td  class="">{{ $arti->entregadoa }}</td>
                        <td  class="">{{ $arti->totalarticulos }}</td>
                        <td  class="">{{ $arti->created_at }}</td>
                        @if($arti->estado == '1')
                        <td  class=""><span class="label label-info">Enviado</span></td>
                        @elseif($arti->estado == '2')
                        <td  class=""><span class="label label-success">Aprobada</span></td>
                        @elseif($arti->estado == '3')
                        <td  class=""><span class="label label-warning">Observada</span></td>
                        @endif
                        <td  class="">
                          <button onclick="" id="botonancicipadas" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#mylista_{{ $arti->id }}" >Acciones</button>
                          <div class="modal fade" id="mylista_{{ $arti->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog modal-lg" role="document">
                              <div class="modal-content">
                                <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                  <h4 class="modal-title text-center" id="myModalLabel" >Ver Detalle </h4>
                                </div>
                                <div class="modal-body">
                                  <p>Solicitado por: {{ $arti->solicitante }}</p> <p>Para entregar a: {{ $arti->entregadoa }}</p>

                                  <div class="col-lg-12 col-xs-12 table-responsive">
                                    <table class="table table-striped table-hover table-bordered text-center" id="listadoarticulos" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
                                      <thead>
                                        <tr class="clsCabereraTabla">
                                          <th>Tipo</th>
                                          <th>Codigo</th>
                                          <th>Descripcion</th>
                                          <th>Cantidad Cond</th>
                                          <th>U.M.</th>
                                          <th>C. Costo</th>
                                          <th>Sub Costo</th>
                                          <th>Proyecto</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                        @foreach($arti->detarticulo as $detalle)
                                        <tr>
                                          @if($detalle->es_producto ==1)
                                          <td class=""><span class="label label-info">Suministro</span></td>
                                          @else
                                          <td class=""><span class="label label-success">Herramienta</span></td>
                                          @endif
                                          <td  class="">{{ $detalle->es_producto }}</td>
                                          <td  class="">{{ $detalle->descripcion }}</td>
                                          <td  class="">{{ $detalle->cantidad }}</td>
                                          <td  class="">{{ $detalle->unidad_metrica_producto }}</td>
                                          <td  class="">{{ $detalle->nombreccosto }}</td>
                                          <td  class="">{{ $detalle->nombresubcosto }}</td>
                                          <td  class="">{{ $detalle->codigo_proyecto }}</td>
                                        </tr>                                        
                                        @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                {{-- <div class="col-lg-12 col-xs-12 table-responsive" style="margin-bottom: 25px;">
                  <label for="">Observaciones:</label>
                  <textarea class="form-control" id="observacionjefe" name="observacionjefe"></textarea>
                </div> --}}
              </div>
              <div class="modal-footer text-center" style="margin-top: 25px;">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
              </div>
            </div>
          </div>
        </div>
      </td>
    </tr>                                        
    @endforeach
  </tbody>
</table>
</div>

</div>
</div>
<!--  fin accordeon -->        
</div>

</div>
</div>
<div class="col-lg-1 hidden-xs"></div>
</div>

</section>
</div>
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
//$(document).ready(function(){


  
  //cambio pantalla de suministro y de herramientas
  $("#suministros0").on('click',function() {

    $('#tituloherramienta').show();
    $('#cambiodiv1').show();
    $('#cambiodiv2').hide();
    $('#titulosuministro').hide();
    $("#producto option[value='']").prop('selected', true);
    $("#ccostroproducto option[value='']").prop('selected', true);
    $("#cantidadproducto").val('1');
    $("#descripcionedit").val('');
    $("#observaproducto").val('');
  });

  $("#suministros1").on('click',function() {
    $('#tituloherramienta').hide();
    $('#cambiodiv1').hide();
    $('#cambiodiv2').show();
    $('#titulosuministro').show();
    $("#herramienta option[value='']").prop('selected', true);
    $("#ccostroherramienta option[value='']").prop('selected', true);
    $("#observaherramienta").val('');
  });   
  //fin del cambio
  //detecto cambio del ind_edito
  $("#producto").change(function(){
    //alert($('#producto').find('option[value="'+$(this).val()+'"]').data('indedito'));
    var ind = $('#producto').find('option[value="'+$(this).val()+'"]').data('indedito');
    var inddecimal = $('#producto').find('option[value="'+$(this).val()+'"]').data('inddecimal');
    var descripcionproducto = $('#producto').find('option[value="'+$(this).val()+'"]').data('descrippro');
    $("#guardoind").val(ind);
    if(ind == 0){
      $('#productodescedit').val(descripcionproducto);
      $('#descripcionedit').show();
      
    }else{
      $('#descripcionedit').hide();
      $('#productodescedit').val('');
    }
    if(inddecimal != 0){
      $('#inputenteros').show();
      $('#inputdecimales').val('');
    }else{
      $('#inputdecimales').hide();
      $('#inputenteros').val('');
    }
  });
  //Actualizar items de tabla 

//});


$('.chosen').chosen({
  allow_single_deselect: true,
  width: '100%'
});

  //chosen actualizan y limpia chosen
  $('#idhijoparasacarpadre').click(function(){
    $('#itemactividadcreate').prop('readonly',null);
    $('#itemactividadcreate').val('');
    $('#padreactividad').val(''); 
    $(".select-box").val('').trigger("chosen:updated");   
    
    if ($('#idhijoparasacarpadre:checked').val()=='on') {
     $('#itemactividadcreate').prop('readonly','true');

    }
 });



  

</script>

