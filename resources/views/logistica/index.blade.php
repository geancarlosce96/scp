
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Logistica
      <small>Solicitud de Requerimiento</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Logistica</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">
        <div class="row clsPadding2"> 
          <div class="col-lg-12 col-xs-12">
            <!-- inicio accordeon -->       

            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs pull-right">

                <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Listado de Solicitudes</a></li>
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Requerimiento</a></li>        
              <!--<li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                  Dropdown <span class="caret"></span>
                </a>
         <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li>-->
              <li class="pull-left header"><i class="fa fa-newspaper-o"></i> Solicitud Requerimiento</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">               
                <!-- Pestaña Listado Vacaciones -->
                <input type="hidden" id="idsolicitud" name="idsolicitud" value="{{ $idsolicitud }}">
                <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

                <div class="row clsPadding2">
                  <div class="col-lg-4 col-xs-12">
                    <label class="clsTxtNormal">Empresa:</label>
                    <select class="form-control" id="id_empresa" name="id_empresa">
                      <option value="1" {{ ($itemempresa)=='Anddes'?'selected':'' }}>Anddes</option>
                      <option value="2" {{ ($itemempresa)=='Geolab'?'selected':'' }}>Geolab</option>
                    </select>
                  </div>                    
                  <div class="col-lg-4 col-xs-12">
                    <label class="clsTxtNormal">Proyecto:</label>
                    <select class="form-control chosen" id="proyecto" name="proyecto" required="true">
                      <option value="" selected="true">Buscar Proyectos</option>
                      @foreach($proyectos as $pro)
                      <option value="{{ $pro->cproyecto }}" {{ ($itemproyecto)== $pro->codigo ?'selected':'' }}>{{ $pro->codigo }} {{ $pro->nombre }}</option>
                      @endforeach
                    </select>
                  </div>
                  <div class="col-lg-4 col-xs-12">
                    <label class="clsTxtNormal">Solicitante:</label>
                    <select class="form-control chosen" id="id_solicitante" name="id_solicitante" required="true">
                      <option value="" selected="true">Buscar Colaborador</option>
                      @foreach($personas as $per)
                      <option value="{{ $per->cpersona }}" {{ ($itemsolicitante)== $per->cpersona ?'selected':'' }}>{{ $per->abreviatura }}</option>
                      @endforeach
                    </select> 
                  </div>
                  <div class="col-lg-6 col-xs-12">
                    <label class="clsTxtNormal">Entregar a:</label>
                    <select class="form-control chosen" id="id_entregado" name="id_entregado" required="true">
                      <option value="" selected="true">Buscar Colaborador</option>
                      @foreach($personas as $pers)
                      <option value="{{ $pers->cpersona }}" {{ ($itementregado)== $pers->cpersona ?'selected':'' }}>{{ $pers->abreviatura }}</option>
                      @endforeach
                    </select> 
                  </div>
                  <div class="col-lg-6 col-xs-12">
                    <label class="clsTxtNormal">Aprobado por:</label>
                    <select class="form-control chosen" id="id_aprobador" name="id_aprobador" required="true">
                      <option value="" selected="true">Buscar Aprobador</option>
                      @foreach($personas as $pers)
                      <option value="{{ $pers->cpersona }}" {{ ($itemaprobado)== $pers->cpersona ?'selected':'' }}>{{ $pers->abreviatura }}</option>
                      @endforeach
                    </select> 
                  </div>

                  <div class="col-lg-12 col-xs-12">
                    <label class="clsTxtNormal">Referencia:</label>
                    <textarea class="form-control " id="referencia" name="referencia" maxlength="29">{{ !is_null($itemreferencia)?$itemreferencia:'' }}</textarea>
                  </div>

                  <div class="clsPadding2 text-center">
                    <div class="col-lg-12 col-xs-12" style="margin-top: 15px !important;">
                      <button onclick="" id="btnarticulos" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myherramienta" style="font-size: 22px;" disabled="true">Agregar Articulo</button>
                    </div>

                  </div> 
                </div>
                <div class="row clsPadding2" id="tablaarticulosajax">
                  @include('logistica.tablaarticulos')
                </div> 
                <div class="col-12 row clsPadding2 text-center">
                  <div class="col-lg-12 col-xs-12">
                    <button onclick="cambiastatus({{ $idsolicitud }});" id="botonenviarsolicitud" type="button" class="btn btn-primary" style="font-size: 22px;">Enviar Solicitud </button> 
                  </div> 
                </div>

                <!-- FIN Datos Personales -->                
              </div>
              <div class="tab-pane" id="tab_2-2">
                @include('logistica.listadoenviadas')  
              </div>
            </div>
            <!--  fin accordeon -->        
          </div>
        </div>
<!--<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-12 clsPadding">
        <a href="#" </class="btn btn-primary btn-block"><b>Grabar</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding">
        <a href="#" </class="btn btn-primary btn-block"><b>Cancelar</b></a> </div>
               
      </div>-->

    </div>
    <div class="col-lg-1 hidden-xs"></div>

  </div>
</section>
<!-- /.content -->
<input type="hidden" name="totalvaciones" id="totalvaciones" value="">
<input type="hidden" name="totalanticipadas" id="totalanticipadas" value="">
<!-- Modal Vacaciones-->
<div class="modal fade" id="myherramienta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069aa;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel" >Solicitud </h4>
      </div>
      <div class="modal-body">        
        <p style="font-size: 22px;font-style:font-weight: 1200;text-align: center;">Seleccionar de Articulos
        </p>
        <div class="col-lg-12 col-xs-12 text-center" id="tituloherramienta">
          <h4><span class="label label-success">Herramientas</span></h4>  
        </div>
        <div class="col-lg-12 col-xs-12 text-center" id="titulosuministro" style="display: none;">
          <h4><span class="label label-info">Suministros</span></h4>  
        </div>   
        <div class="col-lg-12 col-xs-12" style="font-size: 14px;">
          <div class="col-lg-6 col-xs-12 pull-left" style="margin-top: 10px;margin-bottom: 10px;">
            {{-- <input id="activoarticulos" type="checkbox"> Seleccionar Suministro --}}
            <label for="sumins0">
              <input class="radiocuento pull-left" type="radio" name="suministros" id="suministros0" value="0" checked="true">Herramientas 
            </label>
          </div>
          <div class="col-lg-6 col-xs-12" style="float: right!important;margin-top: 10px;margin-bottom: 10px;"> 
            <label for="sumins1" class="pull-right">
              <input class="radiocuento" type="radio" name="suministros" id="suministros1" value="1">Suministros 
            </label>
            
          </div>
        </div>
        <div id="cambiodiv1">      
          <label class="clsTxtNormal">Herramientas:</label> 
          <div class="row">
            <div class="col-lg-12 col-xs-12">
              <select class="form-control chosen select-box" id="herramienta" name="herramienta" required="true">
                <option value="n" selected="true">Buscar Herramienta</option>
                @foreach($herramientas as $herra)
                <option value="{{ $herra->id }}">{{ $herra->codigo }} {{ $herra->descripcion }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <label class="clsTxtNormal">Centro de costo:</label> 
          <div class="row">
            <div class="col-lg-12 col-xs-12">
              <select class="form-control chosen select-box" id="ccostroherramienta" name="ccostroherramienta" required="true">
                <option value="n" selected="true">Buscar Centro de costo</option>
                @foreach($ccostosub as $ccostro)
                <option value="{{ $ccostro->id }}">{{ $ccostro->ccostonombre }}  {{ $ccostro->descripcion }}</option>
                @endforeach
              </select>
            </div>
          </div>
          <label class="clsTxtNormal">Observacion Herramienta:</label> 
          <div class="row">
            <div class="col-lg-12 col-xs-12">
              <textarea class="form-control" id="observaherramienta" name="observaherramienta" value="" placeholder="Colocar la observacion" maxlength="249"></textarea>
            </div>
          </div>
          {{-- <label class="clsTxtNormal">Cantidad:</label> 
          <div class="row">
              <div class="col-lg-12 col-xs-12">
                <input class="form-control" type="number" id="cantidadherramienta" name="cantidadherramienta" value="1" placeholder="Solo valores enteros" min="1" pattern="^[0-9]+">
              </div>
            </div> --}}
          </div>
          <div id="cambiodiv2" style="display: none;">      
            <label class="clsTxtNormal">Suministros:</label> 
            <div class="row">
              <div class="col-lg-12 col-xs-12">
                <select class="form-control chosen select-box" id="producto" name="producto" required="true">
                  <option data-indedito="" value="" selected="true">Buscar Suministros</option>
                  @foreach($productos as $produc)
                  <option data-inddecimal="{{ $produc->permitedecimales }}" data-indedito="{{ $produc->ind_texto_variable }}" data-descrippro="{{ $produc->desc_producto }}" value="{{ $produc->id }}">{{ $produc->desc_producto }}</option>
                  @endforeach
                </select>
              </div>
              <input type="hidden" id="guardoind" name="guardoind" value="">
            </div>
            <div id="descripcionedit" style="display: none;">
              <label class="clsTxtNormal">Editar Descripcion:</label> 
              <div class="row">
                <div class="col-lg-12 col-xs-12">
                  <input class="form-control" type="text" id="productodescedit" name="productodescedit" value="">
                </div>
              </div>  
            </div>

            <label class="clsTxtNormal">Centro de costo:</label> 
            <div class="row">
              <div class="col-lg-12 col-xs-12">
                <select class="form-control chosen select-box" id="ccostroproducto" name="ccostroproducto" required="true">
                  <option value="" selected="true">Buscar Centro de costo</option>
                  @foreach($ccostosub as $ccostro)
                  <option value="{{ $ccostro->id }}">{{ $ccostro->ccostonombre }}  {{ $ccostro->descripcion }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <label class="clsTxtNormal">Cantidad:</label> 
            <div class="row">
              <div class="col-lg-12 col-xs-12">
                <input class="form-control" type="number" id="cantidadtotal" name="cantidadtotal" value="" placeholder="Colocar la cantidad">
              </div>
              
            </div>
            <label class="clsTxtNormal">Observacion suministro:</label> 
            <div class="row">
              <div class="col-lg-12 col-xs-12">
                <textarea class="form-control" id="observaproducto" name="observaproducto" value="" placeholder="Colocar la observacion" maxlength="249"></textarea>
              </div>
            </div>
          </div>    
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          <button id="agregaritem" type="button" class="btn btn-primary">Cargar</button>
        </div>
      </div>
    </div>
  </div>

</div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  //compruebo si esta lleno para activar o desactivar el boton
  var validatedcarga = false;
  if($('#proyecto').val().length !== 0 && $('#id_solicitante').val().length !== 0 && $('#id_entregado').val().length !==  0 && $('#id_aprobador').val().length !== 0) 
      validatedcarga = true;
    if(validatedcarga){
      $("#btnarticulos").removeAttr("disabled"); 
    }else{
      $("#btnarticulos").attr("disabled", true); 
  };
  //Valido el boton 
  $('#proyecto').change(function(){
    var validated = false;
    if($('#proyecto').val().length !== 0 && $('#id_solicitante').val().length !== 0 && $('#id_entregado').val().length !==  0 && $('#id_aprobador').val().length !== 0) 
      validated = true;
    if(validated){
      $("#btnarticulos").removeAttr("disabled"); 
    }else{
      $("#btnarticulos").attr("disabled", true); 
    }                             
  });
  $('#id_solicitante').change(function(){
    var validated = false;
    if($('#id_solicitante').val().length !== 0 && $('#proyecto').val().length !== 0 && $('#id_entregado').val().length !==  0 && $('#id_aprobador').val().length !== 0) 
      validated = true;
    if(validated){
      $("#btnarticulos").removeAttr("disabled"); 
    }else{
      $("#btnarticulos").attr("disabled", true); 
    } 
                                  
  });
  $('#id_entregado').change(function(){
    var validated = false;
    if($('#id_entregado').val().length !== 0 && $('#id_solicitante').val().length !== 0 && $('#proyecto').val().length !==  0 && $('#id_aprobador').val().length !== 0) 
      validated = true;
    if(validated){
      $("#btnarticulos").removeAttr("disabled"); 
    }else{
      $("#btnarticulos").attr("disabled", true); 
    }                            
  });
  $('#id_aprobador').change(function(){
    var validated = false;
    if($('#id_aprobador').val().length !== 0 && $('#id_solicitante').val().length !== 0 && $('#id_entregado').val().length !==  0 && $('#proyecto').val().length !== 0) 
      validated = true;
    if(validated){
      $("#btnarticulos").removeAttr("disabled"); 
    }else{
      $("#btnarticulos").attr("disabled", true); 
    }                            
  });

  $('.chosen').chosen({
    allow_single_deselect: true,
    width: '100%',
  });

  //cambio pantalla de suministro y de herramientas
  $("#suministros0").on('click',function() {
    $('#tituloherramienta').show();
    $('#cambiodiv1').show();
    $('#cambiodiv2').hide();
    $('#titulosuministro').hide();
    $("#producto option[value='']").prop('selected', true);
    $("#ccostroproducto option[value='']").prop('selected', true);
    $("#cantidadproducto").val('1');
    $("#descripcionedit").val('');
    $("#observaproducto").val('');
  });

  $("#suministros1").on('click',function() {
    $('#tituloherramienta').hide();
    $('#cambiodiv1').hide();
    $('#cambiodiv2').show();
    $('#titulosuministro').show();
    $("#herramienta option[value='']").prop('selected', true);
    $("#ccostroherramienta option[value='']").prop('selected', true);
    $("#observaherramienta").val('');
  });   
  //fin del cambio
  //detecto cambio del ind_edito
  $("#producto").change(function(){
    //alert($('#producto').find('option[value="'+$(this).val()+'"]').data('indedito'));
    var ind = $('#producto').find('option[value="'+$(this).val()+'"]').data('indedito');
    var inddecimal = $('#producto').find('option[value="'+$(this).val()+'"]').data('inddecimal');
    var descripcionproducto = $('#producto').find('option[value="'+$(this).val()+'"]').data('descrippro');
    $("#guardoind").val(ind);
    if(ind == 0){
      $('#productodescedit').val(descripcionproducto);
      $('#descripcionedit').show();
      
    }else{
      $('#descripcionedit').hide();
      $('#productodescedit').val('');
    }
    if(inddecimal != 0){
      $('#inputenteros').show();
      $('#inputdecimales').val('');
    }else{
      $('#inputdecimales').hide();
      $('#inputenteros').val('');
    }
  })
  //Actualizar items de tabla 
  $("#agregaritem").on('click',function() {
    //alert('hola mundo');
    var estadoproyecto =  $(this).val();
    var idsolicitud = $('#idsolicitud').val();
    var _token = $('input[name="_token"]').first().val();
    var id_empresa =  $('#id_empresa option:selected').val();
    var id_solicitante =  $('#id_solicitante option:selected').val();
    var id_entregado =  $('#id_entregado option:selected').val();
    var id_aprobador =  $('#id_aprobador option:selected').val();
    var proyecto =  $('#proyecto option:selected').val();
    var ccostro =  $('#ccostro option:selected').val();
    var referencia =  $('#referencia').val();
    var herramienta =  $('#herramienta option:selected').val();
    var ccostroherramienta =  $('#ccostroherramienta option:selected').val();
    var producto =  $('#producto option:selected').val();
    var ccostroproducto =  $('#ccostroproducto option:selected').val();
    var cantidadproducto = $('#cantidadproducto').val();
    var indedito = $('#guardoind').val();
    var descripcionproducto = $('#productodescedit').val();
    var cantidadtotal = $('#cantidadtotal').val();
    var observaherramienta = $('#observaherramienta').val();
    var observaproducto = $('#observaproducto').val();

      //alert('id: '+proyecto);
      var objEnt={ _token:_token, idsolicitud: idsolicitud, id_empresa:id_empresa, id_solicitante:id_solicitante, id_entregado:id_entregado, id_aprobador:id_aprobador, proyecto:proyecto, ccostro:ccostro, referencia:referencia, herramienta: herramienta, ccostroherramienta:ccostroherramienta, producto:producto, cantidadproducto:cantidadproducto, indedito:indedito, descripcionproducto:descripcionproducto, cantidadtotal:cantidadtotal, observaherramienta:observaherramienta, observaproducto:observaproducto, ccostroproducto:ccostroproducto };

      if(id_solicitante !="" && proyecto !="" && id_entregado !=""  && id_aprobador !=""){
        $.ajax({
          url: 'logisticacreate',
          type:'POST',
          data: objEnt,
          beforeSend: function () {
          },
          success : function (data){
            $("#tablaarticulosajax").html(data);
            $('#observaherramienta').val('');
            $('#observaproducto').val('');  
          },
        });
      }else{
        alert('Por favor Ingrese los datos Solicitante, Proyecto o C. Costo');
      }
    });


  //chosen actualizan y limpia chosen
  $('#idhijoparasacarpadre').click(function(){
    $('#itemactividadcreate').prop('readonly',null);
    $('#itemactividadcreate').val('');
    $('#padreactividad').val(''); 
    $(".select-box").val('').trigger("chosen:updated");   
    if ($('#idhijoparasacarpadre:checked').val()=='on') {
     $('#itemactividadcreate').prop('readonly','true');
    }
  })



  function cambiastatus(id){
    //alert(id);
    swal({
        title: "¿Estas seguro que deseas enviar la solicitud?",
        text: "",
        type: "info",
        className: "red-bg",
        showCancelButton: true,
        dangerMode: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Confirmar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: false
      },
      function(isConfirm) {
        if (isConfirm) {
          status(id);
        } else {
          swal("Solicitud no enviada", "puede serguir editando", "error");
        }
      });
  };

  function status(id)
  {
    $.ajax({
      url: 'logisticambiostatus/'+id,
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      // data: {param1: 'value1'},
    })
    .done(function(data) {
      
      //$(".select-box").val('').trigger("chosen:updated");
      swal("Se realizo la acción", "correctamente","success");
      //$("#resultado").html(data);
      // $('.chosen').chosen({
      //       allow_single_deselect: true,
      //       width: '100%',
      // });

      //window.location.reload();
      window.location.assign("int?url=logistica/");
      
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }
  

</script>

