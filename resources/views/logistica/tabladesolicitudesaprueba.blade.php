<div class="col-lg-12 col-xs-12 table-responsive">
  <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
  <table class="table table-striped table-hover table-bordered text-center" id="listadoarticulos" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
    <thead>
      <tr class="clsCabereraTabla">
        <th>Empresa</th>
        <th>Proyecto</th>
        <th>Solicitante</th>
        <th>Entregado A</th>
        <th>Total de Articulos</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($tablaarticulos as $arti)
      <tr>
        <td  class="">{{ $arti->empresa }}</td>
        <td  class="">{{ $arti->proyecto }}</td>
        <td  class="">{{ $arti->solicitante }}</td>
        <td  class="">{{ $arti->entregadoa }}</td>
        <td  class="">{{ $arti->totalarticulos }}</td>
        <td  class="">
          <button onclick="" id="botonancicipadas" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myherramienta_{{ $arti->id }}" >Acciones</button>
          <div class="modal fade" id="myherramienta_{{ $arti->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title text-center" id="myModalLabel" >Aprobar Solicitud </h4>
                </div>
                <div class="modal-body">
                <p>Solicitado por: {{ $arti->solicitante }}</p> <p>Para entregar a: {{ $arti->entregadoa }}</p>

                  <div class="col-lg-12 col-xs-12 table-responsive">
                  <table class="table table-striped table-hover table-bordered text-center" id="listadoarticulos" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
                    <thead>
                      <tr class="clsCabereraTabla">
                        <th>Tipo</th>
                        <th>Codigo</th>
                        <th>Descripcion</th>
                        <th>Cantidad Cond</th>
                        <th>U.M.</th>
                        <th>C. Costo</th>
                        <th>Sub Costo</th>
                        <th>Proyecto</th>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($arti->detarticulo as $detalle)
                      <tr>
                        @if($detalle->es_producto ==1)
                        <td class=""><span class="label label-info">Suministro</span></td>
                        @else
                        <td class=""><span class="label label-success">Herramienta</span></td>
                        @endif
                        <td  class="">{{ $detalle->es_producto }}</td>
                        <td  class="">{{ $detalle->descripcion }}</td>
                        <td  class="">{{ $detalle->cantidad }}</td>
                        <td  class="">{{ $detalle->unidad_metrica_producto }}</td>
                        <td  class="">{{ $detalle->nombreccosto }}</td>
                        <td  class="">{{ $detalle->nombresubcosto }}</td>
                        <td  class="">{{ $detalle->codigo_proyecto }}</td>
                      </tr>                                        
                      @endforeach
                    </tbody>
                  </table>
                </div>
                {{-- <div class="col-lg-12 col-xs-12 table-responsive" style="margin-bottom: 25px;">
                  <label for="">Observaciones:</label>
                  <textarea class="form-control" id="observacionjefe" name="observacionjefe"></textarea>
                </div> --}}
                </div>
                <div class="modal-footer text-center" style="margin-top: 25px;">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                  <button id="observar" type="button" class="btn btn-warning" onclick="enviarrespuestajefe('observar',{{ $arti->id }});">Observar</button>
                  <button id="aprobar" type="button" class="btn btn-success" onclick="enviarrespuestajefe('aprobar',{{ $arti->id }});">Aprobar</button>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>                                        
      @endforeach
    </tbody>
  </table>
</div>

<script>
  //Actualizar items de tabla 


  function enviarrespuestajefe(estado,id){
    //alert(estado);
    var observacion = $('observacionjefe').val();
    //alert(observacion);
    var _token = $('input[name="_token"]').first().val();
   
      //alert('id: '+proyecto);
      var objEnt={ _token:_token, estado: estado, id_solicitud:id, observacion:observacion };

      if(estado !=""){
        $.ajax({
          url: 'AprobacionesSolicitud',
          type:'POST',
          data: objEnt,
          beforeSend: function () {
          },
          success : function (data){
            $('.modal-backdrop').remove();
            $("#resultado").html(data);
          },
        });
      }else{
        alert('Si es una Observacion debe llenar el contenido');
      }
  }

</script>