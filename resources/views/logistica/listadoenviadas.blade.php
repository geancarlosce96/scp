<div class="col-lg-12 col-xs-12 table-responsive">
  <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">
  <table class="table table-striped table-hover table-bordered text-center" id="listadoenviadas" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
    <thead>
      <tr class="clsCabereraTabla">
        <th>Empresa</th>
        <th>Proyecto</th>
        <th>Solicitante</th>
        <th>Entregado A</th>
        <th>Total de Articulos</th>
        <th>Estatus</th>
        <th>Opciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($tablasolicitudes as $arti)
      <tr>
        <td  class="">{{ $arti->empresa }}</td>
        <td  class="">{{ $arti->proyecto }}</td>
        <td  class="">{{ $arti->solicitante }}</td>
        <td  class="">{{ $arti->entregadoa }}</td>
        <td  class="">{{ $arti->totalarticulos }}</td>
        @if($arti->estado == '1')
          <td  class=""><span class="label label-info">Enviado</span></td>
        @elseif($arti->estado == '2')
          <td  class=""><span class="label label-success">Aprobada</span></td>
        @elseif($arti->estado == '3')
          <td  class=""><span class="label label-warning">Observada</span></td>
        @elseif($arti->estado == '0')
          <td  class=""><span class="label label-primary">Pendiente</span></td>
        @endif
        <td  class="">
          @if($arti->estado == '0')
            <a href="{{ URL('int?url=logistica/'.$arti->id) }}"> 
              <button class="btn btn-block btn-info btn-sm">Editar</button>
            </a>
          @else
            <button onclick="" id="botonancicipadas" type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#myherramienta_{{ $arti->id }}" >Ver Detalle</button>
          @endif
          
          <div class="modal fade" id="myherramienta_{{ $arti->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title text-center" id="myModalLabel" >Ver Detalle </h4>
                </div>
                <div class="modal-body">

                  <p>Solicitado por: {{ $arti->solicitante }}</p> <p>Para entregar a: {{ $arti->entregadoa }}</p>



                  <div class="col-lg-12 col-xs-12 table-responsive">
                    <table class="table table-striped table-hover table-bordered text-center" id="listadoarticulos" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
                      <thead>
                        <tr class="text-center" style="color: #fff;background-color: #0069aa;">
                          <th>Proyecto</th>
                          <th>Tipo</th>
                          <th>Codigo</th>
                          <th>Descripcion</th>
                          <th>Cantidad Cond</th>
                          <th>U.M.</th>
                          <th>C. Costo</th>
                          <th>Sub Costo</th>
                          <th>Fecha Creacion</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($arti->detarticulo as $detalle)
                        <tr>
                          <td  class="text-center">{{ $detalle->codigo_proyecto }} - {{ $detalle->nombreproyecto }}</td>
                          @if($detalle->es_producto ==1)
                            <td class="text-center">
                              <span class="label label-info">Suministro</span>
                            </td>
                          @else
                            <td class="text-center">
                              <span class="label label-success">Herramienta</span>
                            </td>
                          @endif
                          <td  class="text-center">{{ $detalle->es_producto }}</td>
                          <td  class="text-center">{{ $detalle->descripcion }}</td>
                          <td  class="text-center">{{ $detalle->cantidad }}</td>
                          <td  class="text-center">{{ $detalle->unidad_metrica_producto }}</td>
                          <td  class="text-center">{{ $detalle->nombreccosto }}</td>
                          <td  class="text-center">{{ $detalle->nombresubcosto }}</td>
                          <td  class="text-center">{{ $detalle->created_at }}</td>
                        </tr>                                        
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                  {{-- <div class="col-lg-12 col-xs-12 table-responsive" style="margin-bottom: 25px;">
                    <label for="">Observaciones:</label>
                    <textarea class="form-control" id="observacionjefe" name="observacionjefe"></textarea>
                  </div> --}}
                </div>
                <div class="modal-footer text-center" style="margin-top: 25px;">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                </div>
              </div>
            </div>
          </div>
        </td>
      </tr>                                        
      @endforeach
    </tbody>
  </table>
</div>
<script>
  $('.chosen').chosen({
    allow_single_deselect: true,
    width: '100%',
  });

  var table = $('#listadoenviadas').DataTable( {
    "paging":   true,
    "ordering": true,
    "info":     false,
    paging:     true,
    } ); 

 table.buttons().container()
    .appendTo( '#tablaEntregablesAgregados_wrapper .col-sm-6:eq(0)' ); 

  
  function VerSolicitud(id){
    //alert(id);
    $.ajax({
      url: 'logistica/'+id,
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      // data: {param1: 'value1'},
    })
    .done(function(data) {
      console.log(data);
      $("#btnarticulos").attr("disabled", false);  
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }  
</script>
