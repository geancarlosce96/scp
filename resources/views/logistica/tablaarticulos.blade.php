<div class="col-lg-12 col-xs-12 table-responsive">
  <table class="table table-striped table-hover table-bordered text-center" id="listadoarticulos" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
    <thead>
      <tr class="clsCabereraTabla">
        <th>Tipo</th>
        <th>Codigo</th>
        <th>Descripcion</th>
        <th>Cantidad Cond</th>
        <th>U.M.</th>
        <th>C. Costo</th>
        <th>Sub Costo</th>
        <th>Proyecto</th>
        <th>Acciones</th>
      </tr>
    </thead>
    <tbody>
      @foreach($tablaarticulos as $arti)
      <tr>
        @if($arti->es_producto ==1)
        <td class=""><span class="label label-info">Suministro</span></td>
        @else
        <td class=""><span class="label label-success">Herramienta</span></td>
        @endif
        <td  class="">{{ $arti->proherrcodigo }}</td>
        <td  class="">{{ $arti->descripcion }}</td>
        <td  class="">{{ $arti->cantidad }}</td>
        <td  class="">{{ $arti->unidad_metrica_producto }}</td>
        <td  class="">{{ $arti->nombreccosto }}</td>
        <td  class="">{{ $arti->nombresubcosto }}</td>
        <td  class="">{{ $arti->codigo_proyecto }}</td>
        <td  class="">
          <button type"button" class="btn btn-warning" onclick="eliminar({{ $arti->id }});">X</button>
        </td>
      </tr>                                        
      @endforeach
    </tbody>
  </table>
</div>

<script>
  function eliminar(id){
    //alert(id);
    $.ajax({
      url: 'logisdeleterow/'+id,
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      // data: {param1: 'value1'},
    })
    .done(function(data) {
      console.log("estadotproyectoactividad",data);
      $(".select-box").val('').trigger("chosen:updated");
      $("#tablaarticulosajax").html(data);
      swal("Se realizo la acción", "correctamente","success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  }

</script>