<p>Estimados {{ $para_nombre }},</p>
<p>Hemos sido invitados para el proyecto "{{ $invitacion }}" del cliente {{ $de_cliente }} en la unidad minera {{ $de_minera }}.</p>
<p>Rutas de Acceso al SCP:</p>
<p><a href="http://192.168.1.17:8180">Acceso de oficina</a></p>
<p><a href="http://190.117.152.51:8180">Acceso de campo</a></p>
<p><b>Este mensaje es informativo, por favor no responder.</b></p>
<div dir="ltr"><b><span style="font-size:10.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black">Sistema de Control de Proyectos<br>
</span></b><span style="font-size:10.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black">
</span><b><span style="font-size:11.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0069aa"><br>
Anddes Asociados SAC<br>
</span></b><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black">Av. Javier Prado Este Cdra. 48, Edificio Capital Golf,
Piso 13, Surco, Lima 33, Perú<br>
</span><b><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0069aa">T</span></b><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black">: +51 1 317 4900 / </span><b><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0069aa"></span></b><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black"><br>
</span><b><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0069aa">e</span></b><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black">: </span><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0069aa"><a href="mailto:scp@anddes.com" target="_blank">scp@anddes.com</a> / </span><span style="font-size:11.0pt;line-height:107%;font-family:&quot;Calibri&quot;,&quot;sans-serif&quot;"><a href="http://www.anddes.com/" target="_blank"><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0069aa;text-decoration:none">www.anddes.com</span></a></span><span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:#0069aa"><br>
<i><br>
"Comprometidos con la <b>C</b>alidad, <b>R</b>espuesta rápida e<b> I</b>nnovación”</i></span></div>


