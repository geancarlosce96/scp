<span style="font-size:10.0pt;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;;color:black">
	
<p style="font-family: Arial, sans-serif;">Estimados</p>
<p style="font-family: Arial, sans-serif;">El cliente aprobó la propuesta de <b> "{{ $codigopropuesta }}" </b> del cliente {{ $de_cliente }} en la unidad minera {{ $de_minera }} y agradecería saber quiénes nos apoyarán como líderes de disciplina en el desarrollo del proyecto</p>

<li style="font-family: Arial, sans-serif;">Nombre del proyecto: {{ $nombreproyecto }}</li>
<li style="font-family: Arial, sans-serif;">Número de proyecto: {{ $codigoproyecto }}</li>
<li style="font-family: Arial, sans-serif;">Gerente de proyecto: {{ $gerente }}</li>

<p style="font-family: Arial, sans-serif;">Rutas de Acceso al SCP:</p>
<p style="font-family: Arial, sans-serif;"><a href="http://192.168.1.17:8180">Acceso de oficina</a></p>
<p style="font-family: Arial, sans-serif;"><a href="http://190.117.152.51:8180">Acceso de campo</a></p>
<p style="font-family: Arial, sans-serif;">Agradezco de antemano a todos la pronta respuesta.</p>
<p style="font-family: Arial, sans-serif;"><b>Este mensaje es informativo, por favor no responder.</b></p>

</span>

<div dir="ltr">
	<div>
		<span style="font-size: 10pt;font-family:Arial,sans-serif;color: black;">Saludos.</span>
	</div>
	<div>
		<u style="color: rgb(0, 105, 170); font-family: Arial, sans-serif; font-size: 9pt;">
			<span style="font-size: 8pt; line-height: 110%;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</span>
		</u>
		<u style="color: rgb(0, 105, 170); font-family: Arial, sans-serif; font-size: 9pt;">
			<span style="font-size: 9pt; line-height: 110%;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</span>
		</u>
	</div>
	<div dir="ltr">
		<span style="font-size:9.0pt;line-height:107%;font-family:&quot;Arial&quot;,&quot;sans-serif&quot;color:#0069aa">
			<p class="MsoNormal" style="line-height:110%">
				<b>
					<span style="font-size:9pt;line-height:110%;font-family:Arial,sans-serif;color:black">SCP</span>
				</b>
				<span style="font-size:9pt;line-height:110%;font-family:Arial,sans-serif;color:black">&nbsp;|&nbsp;
					<b>Sistema de Control de Proyectos&nbsp;
					</b>|&nbsp;
				</span>
					<b>
						<span style="font-size: 9pt; line-height: 110%; font-family: Arial, sans-serif;">Anddes Perú</span>
					</b>
					<span style="font-size:9pt;line-height:110%;color:rgb(51,51,153)"><br></span>
					<span style="font-size:8pt;line-height:110%;font-family:Arial,sans-serif;color:black">Av. Javier Prado Este Cdra. 48, Edif. Capital Golf, Piso 13&nbsp;|&nbsp;Surco, Lima 15023, Perú&nbsp;|&nbsp;T: +51 1 317 4900 </span>
					<span style="font-size:8pt;line-height:110%;font-family:Arial,sans-serif;color:rgb(51,51,153)"><br></span>
					<!-- <span style="font-size:8pt;line-height:110%;font-family:Arial,sans-serif;color:black">e: </span> -->
					<!-- <a href="mailto:fabio.colan@anddes.com"><span style="font-size:8pt;line-height:110%;font-family:Arial,sans-serif;color:rgb(0,105,170);text-decoration-line:none">fabio.colan@anddes.com</span></a> -->
					<!-- <span style="font-size:8pt;line-height:110%;font-family:Arial,sans-serif;color:black">&nbsp;|&nbsp;</span> -->
					<a href="http://www.anddes.com/" target="_blank">
						<span style="font-size:8pt;line-height:110%;font-family:Arial,sans-serif;color:rgb(0,105,170);text-decoration-line:none">www.anddes.com</span>
					</a>
			</p>
		</span>
	</div>
</div>



