<h4>{{ $titulo }}</h4>
<p>Buen día {{ $para_nombre }},</p>
<p>Se ha registrado a usted como Aprobador para la Capacitación "{{ $proyecto }}" registrada por el usuario {{ $de_usuario }}.</p>
<p>Favor de dirigirse a la opción del Menú "Capacitaciones e Inducciones" y luego "Capacitaciones Propuestas", o en todo caso hacer click aquí <a href="{{ url('capacitaciones/propuestas') }}">link</a>.
 </p>
<p>Sistema SCP</p>
<p>Muchas Gracias</p>

<p>PD: Este email se ha enviado como prueba de envío de correos del sistema SCP.</p>
