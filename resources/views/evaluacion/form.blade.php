<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Evaluación</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<style type="text/css">
		body{
			background-color: #d3dbed;
		}
		#contenido{
			font-size: 13px;
			background-color: floralwhite;        
			margin-top: 20px;
			border: 2px solid #3e3e4680;
			border-radius: 5px;
			margin-bottom: 30px;
			padding-bottom: 20px;
		}
	</style>

</head>

<body>

<input type="hidden" id="_redirect" value="{{ (isset($_GET['_redirect'])) ? $_GET['_redirect'] : 'capacitaciones' }}">
<input type="hidden" id="cantidad_preguntas" value="{{ $evaluacion->cantidad_preguntas }}">
<input type="hidden" id="evaluacion_id" value="{{ $evaluacion->id }}">

<div class="container">      

	<div class="content-wrapper" style="min-height: 853px;">

		<section class="content">

			<div class="col-md-12" id="contenido">
							<!-- Horizontal Form -->
							<div class="box box-info">
								<div class="box-header with-border">
									<h3 class="box-title" style="text-align: center" >Evaluación</h3>
								</div>
								
								<p><b>Objetivo:</b> {{ $evaluacion->objetivo }}</p>
								<p class="contador" >Tiempo restante <span class="badge badge-warning">
                				<span id="contador_minutos">{{ $evaluacion->duracion }}</span> : 
                				<span id="contador_segundos"></span> minutos </span></p>   
				
				<form id="form" class="form-horizontal" action='{{ url("evaluacion/rendir/$evaluacion->id") }}' method="post">

				{{ csrf_field() }}
									
								@foreach ($evaluacion->preguntas as $index => $pregunta)
								<div class="grupo">
									<input type="hidden" name="preguntas[]" value="{{ $pregunta->id }}">
									
									<div class="row pregunta">
										<div class="col-sm-1 numero"  style="text-align: right;" >{{ $pregunta->n }}.</div>
										<div class="col-sm-9 descripcion">											
											<label>{{ $pregunta->descripcion }}</label>									
										</div>                           
										
									</div>

									@foreach ($pregunta->alternativas as $index => $alternativa)
									<div class="row alternativa" style="margin-top: 15px" >
										<div class="col-sm-12 col-sm-offset-1 chk" style="text-align: left;" >
											@if($pregunta->opcion_unica == 1)
						                    <input type="radio" value="{{ $alternativa->id }}" name="opc_{{ $pregunta->id}}[]">
						                    @else
						                    <input type="checkbox" value="{{ $alternativa->id }}" name="opc_{{ $pregunta->id}}[]" id="">
						                    @endif
						                    {{ $alternativa->descripcion }}
										</div>										
									</div>
									@endforeach

									<hr>
								</div>
								@endforeach
																
									<div class="box-footer"  style="text-align: center" >
										<button class="btn btn-primary" id="btn-guardar" type="submit">Guardar</button>
										<a class="btn btn-danger" href="{{ url('/') }}"> Cancelar </a>
									</div>
									
				</form>
							</div>
							
			</div>
		</section>

	</div>

</div>

<div class="modal fade" tabindex="-1" role="dialog" id="modalAdvertencia">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Registro de Evaluación</h4>
      </div>
      <div class="modal-body">
        <p><span id="msjRespuesta"></span></p>
      </div>
      <div class="modal-footer">        
        <button type="button" class="btn btn-primary" id="btn-aceptar">Aceptar</button>
      </div>
    </div>
  </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>
	var BASE_URL = "<?php echo url('/'); ?>";
</script>

<script>
  var minutos = 0;
  var segundos = 0;
  empezar_contador();

  function empezar_contador()
  {
    minutos = document.getElementById("contador_minutos").innerHTML; 
    setInterval(function(){ contador_regresivo(); }, 1000);
  }  

  function contador_regresivo()
  {
    if(segundos == 0){
      minutos--;
      segundos=60;
    }

    segundos--;
    document.getElementById("contador_minutos").innerHTML = minutos;
    document.getElementById("contador_segundos").innerHTML = segundos;

    console.log(minutos, segundos);

    if(minutos <= 0 && segundos <= 0)
      $('#form').trigger('submit');
  }
</script>

</body>
</html>
