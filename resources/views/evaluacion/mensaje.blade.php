<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Evaluación</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

  <style type="text/css">
    body{
      background-color: #d3dbed;
    }
    #contenido{
      font-size: 13px;
      background-color: floralwhite;        
      margin-top: 20px;
      border: 2px solid #3e3e4680;
      border-radius: 5px;
      margin-bottom: 30px;
      padding-bottom: 20px;
      padding-top: 20px;
    }
  </style>

</head>

<body>

<div class="container">      

  <div class="content-wrapper" style="min-height: 853px;">

    <section class="content">
      <div class="col-md-12" id="contenido">
        <h4>Evaluación</h4>
        <p><b>Capacitación:</b> {{ $capacitacion }}</p>
        <p><b>Objetivo:</b> {{ $evaluacion->objetivo }}</p>
        <p>{{ $msg }}</p>
        <a href="{{ url('panel') }}">Regresar</a>
      </div>
    </section>

  </div>

</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

</body>
</html>
