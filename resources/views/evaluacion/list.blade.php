@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
  <h1>
	Capacitaciones        
  </h1>
<section class="content">          

  <div class="col-md-12">
  
	<table class="table table-striped table-hover" >
	  <tr>
		<th>#</th>        
		<th>Titulo</th>                
		<th>F. Disponible</th>
		<th>Evaluación?</th>        
		<th>Estado</th>
		<th>Acciones</th>
	  </tr>
	  @foreach ($capacitaciones as $index => $element)
	  
	  @if($element->estado==1)
	  <tr>
	  @else
	  <tr class="">
	  @endif
		<td>{{ $element->n }} </td>        
		<td>{{ $element->titulo }} </td>
			   
		<td>{{ $element->fecha_inicio }} </td>
		<td>
		  @if ($element->evaluacion->cantidad_preguntas > 0)                  
		  Si <i class="fa fa-check" style="color:green;" ></i>
		  @else
		  No
		  @endif
		</td>
		<td> {{ $element->evaluacionPersona->estado_descripcion() }} </td> 
		<td>
		  
		  @if($element->evaluacion->estadoEvaluacion == 0)
		  <a href='{{ url("evaluacion/ver/$element->id") }}' class="btn btn-primary" >
		  	<i class="fa fa-search"></i> ver</a> 
		  
		  @if($element->evaluacionPersona->estado == 0)
		  @if($element->evaluacion->cantidad_preguntas > 0)
		  <a href='{{ url("evaluacion/rendir/".$element->evaluacion->id) }}' class="btn btn-primary">
		  	<i class="fa fa-angle-double-right"></i> rendir</a>
		  @endif
		  @endif

		  @endif

		</td>
	  </tr>
	  
	  @endforeach
	</table>

	</div>
  </div>

</section>

</div>

@stop
