@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
      <h1>
        Capacitaciones        
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">             

   <div class="col-md-12">    

    <div class="box box-info">
      <div class="box-header with-border">
        <h3 class="box-title">Video</h3>
    </div>

  <div class="box-body">

    <div class="row">
      <div class="col-md-12" >
        @if($embeber == true)
        <iframe width="560" height="315" src="{{ $link_video }}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
        @else
        <p>{{ $link_video }}</p>
        @endif

      </div>
    </div>

    <div class="row">

      @if ( ($video_numero_actual-1) > 0)
      <div class="col-md-2">
        <a href='{{ url("evaluacion/ver/$capacitacion->id/video/".($video_numero_actual-1) ) }}'>
        
        <button class="btn btn-primary" > <i class="fa fa-angle-double-left"></i> anterior </button>
        </a>
      </div>
      @endif
      
      @if ( $video_siguiente_numero > 0)
      <div class="col-md-2">
        <a href='{{ url("evaluacion/ver/$capacitacion->id/video/$video_siguiente_numero") }}'>
        
        <button class="btn btn-primary" > <i class="fa fa-angle-double-right"></i> siguiente </button>
        </a>
      </div>
      @endif

    </div>

    <div class="row" >
      <a href='{{ url("capacitaciones") }}'>        
        <button class="btn btn-default" > <i class="fa fa-angle-double-left"></i> Regresar </button>
      </a>
    </div>   

  </div>

</div>

</div>

</section>

</div>

@stop
