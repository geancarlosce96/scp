<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Control de Proyectos | Anddes</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="dist/css/font-awesome.min.css">
  
  
<!-- Ionicons -->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">-->
  <link rel="stylesheet" href="dist/css/ionicons.min.css">  
  <!-- Theme style -->
  <!--<link rel="stylesheet" href="dist/css/AdminLTE.min.css">-->
  <link rel="stylesheet" type="text/css" href="dist/css/AdminLTE.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/flat/blue.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="plugins/morris/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="plugins/jvectormap/jquery-jvectormap-1.2.2.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="plugins/datepicker/datepicker3.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- DataTable-->
  <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css">
  <link rel="stylesheet" href="plugins/datatables/jquery.dataTables.min.css">

  <link rel="stylesheet" href="dist/css/bootstrap-treeview.min.css" >

  <link rel="stylesheet" type="text/css" href="dist/css/highCheckTree.css"/>
  <link rel="stylesheet" type="text/css" href="dist/css/jquery-ui.css"/>

  <!-- plugin chosen CSS-->
  <link rel="stylesheet" type="text/css" href="dist/css/chosen/chosen.min.css"/>
            
        
        

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
  <style>
    #div_carga{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background: url(img/gris.png) repeat;
      opacity: 0.5;
      display:none;
      z-index:810;
    }
    #div_msg{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      
      background-color:rgba(255,255,255,0.6);
      
      display:none;
      z-index:1500;
    }
    #div_contenido{
      opacity: 1;
    }
    #div_msg_error{
      position:absolute;
      top:0;
      left:0;
      width:100%;
      height:100%;
      background-color:rgba(255,255,255,0.6);
      

      display:none;
      z-index:1500;
    }    
    #cargador{
        position:absolute;
        top:50%;
        left: 50%;
        margin-top: -25px;
        margin-left: -25px;
    }
    .success_editar, .success_eliminar, .success{background: #0000ff;}
    .error{background: #a41b22;}
    .success, .success_editar, .success_eliminar, .error{ width: 100%; padding: 20px 20px; color: #ffffff; font-size: 16px; font-weight: bold; position: fixed; top: 0; left: 0;}
    .success i, .success_editar i,.success_eliminar i, .error i{font-size: 20px;}

    .custom-combobox {
      position: relative;
      display: inline-block;
    }
    .custom-combobox-toggle {
      position: absolute;
      top: 0;
      bottom: 0;
      margin-left: -1px;
      padding: 0;
    }
    .custom-combobox-input {
      margin: 0;
      padding: 5px 10px;
    }


  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">


<!-- jQuery 2.2.3 -->
<script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<!--<script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>-->
<script src="plugins/jQueryUI/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>

  $.widget.bridge('uibutton', $.ui.button);
</script>

<!-- Bootstrap 3.3.6 -->
<script src="bootstrap/js/bootstrap.min.js"></script>
<!-- Morris.js charts -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>-->
<script src="js/raphael-min.js"></script>
<script src="plugins/morris/morris.min.js"></script>
<!-- Sparkline -->
<script src="plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jvectormap -->
<script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- jQuery Knob Chart -->
<script src="plugins/knob/jquery.knob.js"></script>
<!-- daterangepicker -->
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js"></script>-->
<script src="plugins/daterangepicker/moment.min.js"></script>
<script src="plugins/daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="plugins/datepicker/bootstrap-datepicker.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="plugins/fastclick/fastclick.js"></script>

<!-- AdminLTE App -->
<script src="dist/js/app.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="dist/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="js/func.js"></script>
<script src="dist/js/bootstrap-treeview.min.js"></script>
<script type="text/javascript" src="dist/js/highchecktree.js"></script>


  <!-- plugin chosen JS-->
<script type="text/javascript" src="dist/js/chosen/chosen.jquery.min.js"></script>



</body>



</html>
