<div class = "content-wrapper">
	<!-- Content Header (Page header) -->
      <section class = "content-header">
      	<h1>
          Mantenimiento
          <small>Moneda Cambio</small>
        </h1>

        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
          <li class="active">Moneda Cambio</li>
        </ol>

      </section>

      <!-- Main Content -->
  <section class="Content">
    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">

          <div class="row clsPadding2">
            <div class="col-lg-2 hidden-xs"></div>

            <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addMonCambio"><b>Nuevo</b></button>
              </div> 

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" onclick="goEditar()"; class="btn btn-primary btn-block" data-toggle="modal" data-target=""
              ><b>Modificar</b></a></button>
              </div>

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" onclick="eliminarMonedaCamb()" class="btn btn-primary btn-block" data-toggle="modal" data-target=""><b>Eliminar</b></button>
              </div>

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target=""><b>Imprimir</b></button>
              </div>

          </div>

<!--  Modal Entregable -->

          <div class="row clsPadding2"> 
                <div class="col-lg-12 col-xs-12 table-responsive" >

                    <table class="table table-striped" id="listado">
                        <thead>
                            <tr class="clsCabereraTabla">
                            <th>ID</th>
                              <th>Moneda</th>
                              <th>Moneda Valor</th>
                              <th>Fecha</th>
                              <th>Valor Compra</th>
                              <th>Valor Venta</th>                         
                            </tr>
                        </thead>
                      
                  
                    </table>  
              
                </div>
          </div> 

<!-- Fin Modal Entregable -->


      </div>

      <div class="col-lg-1 hidden-xs"></div>

      

    </div>
  </section>

      <!-- /.content -->
    <div id="modalUser">
          @include('partials.addMonedaCambio')
    </div>

</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';  

    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarMonedaCambio',
      "columns" : [
        {data : 'cmonedacambio', name: 't.cmonedacambio'},
        {data : 'moneda', name: 'm.descripcion'},
        {data : 'monedavalor', name: 'm2.descripcion'},
        {data : 'fecha', name: 't.fecha'},
        {data : 'valorcompra', name: 't.valorcompra'},
        {data : 'valorventa', name: 't.valorventa'}
        
      ],

      "rowCallback" : function( row, data ) {

          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    }); 

     $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/         

        $(this).toggleClass('selected');
      }); 

     //Boton Modificar
      function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
          $.ajax({

                url:'editarMonedaCambio/'+id.substring(7),
                type: 'GET',
              
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#modalUser").html(data);
                    $('#div_carga').hide(); 
                    $("#addMonCambio").modal("show");
                    
                },            
            });
    }else{
        $('.alert').show();
    }
  }

  $("#addMonCambio").on('show.bs.modal',function(e){
         
          });

        function grabar(){
           
             $.ajax({

                url:'agregarMonedaCambio',
                type: 'POST',
                data : $("#frmMonCamb").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){

                  if(data=='1'){
                    alert('Este registro ya fue ingresado anteriormente')
                    $("#addMonCambio").modal("show");
                    $('#div_carga').hide(); 

                  }
                  else{
                    $("#resultado").html(data);
                    $('#div_carga').hide(); 
                  }
                    
                    
                },            
            });
          
        }

    function eliminarMonedaCamb(id){

     var id =0;
    if (selected.length > 0 ){
        id = selected[0];

     
          $.ajax({
            url: 'eliminarMonedaCambio/' + id.substring(7),
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show(); 
        
            },              
            success : function (data){
               //$('#div_carga').hide(); 
              $('#resultado').html(data);
            },
          }); 

           }else{
        $('.alert').show();
       }         

        }


</script>
