<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Mantenimientos
        <small>Listado de Actividades</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Mantenimientos</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">



  <div class="row clsPadding2">
	<div class="col-lg-2 hidden-xs"></div>
    
    <div class="col-lg-2 col-xs-6 clsPadding">
    	<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addActi"
        > <b>Nuevo</b></a></button></div>    

    <div class="col-lg-2 col-xs-6 clsPadding">
    	<button type="button" onclick="goEditar()"; class="btn btn-primary btn-block" data-toggle="modal" data-target=""
        > <b>Modificar</b></a></button></div> 

    <div class="col-lg-2 col-xs-6 clsPadding">

    	<button type="button" onclick="eliminarActividades()"; class="btn btn-primary btn-block" data-toggle="modal" data-target=""
        ><b>Eliminar</b></a></div>
    <div class="col-lg-2 col-xs-6 clsPadding">
    	<a href="#" class="btn btn-primary btn-block "><b>Imprimir</b></a></div>
    <div class="col-lg-2 hidden-xs"></div>

  </div>

    
<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive" >

      <table class="table table-striped" id="listado">
            <thead>
            <tr class="clsCabereraTabla">
              <th>ID</th>
              <th>Código</th>
              <th>Descripción</th>
              <th>Area Padre</th>
            </tr>
            </thead>            
        
      </table>  
    
	</div>
</div>    
    
 
</div><!-- fin parte central -->
    
    <div class="col-lg-1 hidden-xs"></div>
 
</div>


    </section>

    <!-- /.content -->
    <div id="modalUser">
    @include('partials.addActividades')
    </div>

</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>


    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';  

    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarActividad',
      "columns" : [
        {data : 'cactividad', name: 't.cactividad'},
        {data : 'codigo', name: 't.codigo'},
        {data : 'nombre', name: 't.descripcion'},
        {data : 'padre', name: 'ta.descripcion'} 
      ],

      "rowCallback" : function( row, data ) {

          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });  

     $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/         

        $(this).toggleClass('selected');
      }); 


  function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
       
        $.ajax({

                url:'editarActividad/'+id.substring(7),
                type: 'GET',
                /*data : $("#frmusuarios").serialize(),*/
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#modalUser").html(data);
                    $('#div_carga').hide(); 
                    $("#addActi").modal("show");
                    
                },            
            });
    }else{
        $('.alert').show();
    }
  }

    $("#addActi").on('show.bs.modal',function(e){
         
          }); 

/*$("#idAddAler").on("click", */function grabar(e){
           
             $.ajax({

                url:'agregarActividad',
                type: 'POST',
                data : $("#frmactividad").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#resultado").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        }/*);*/ 

    function eliminarActividades(id){

     var id =0;
    if (selected.length > 0 ){
        id = selected[0];

     
          $.ajax({
            url: 'eliminarActividad/' + id.substring(7),
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show(); 
        
            },              
            success : function (data){
               //$('#div_carga').hide(); 
              $('#resultado').html(data);
            },
          }); 

           }else{
        $('.alert').show();
       }         

        } 


</script>  