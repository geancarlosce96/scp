<div class = "content-wrapper">
	<!-- Content Header (Page header) -->
      <section class = "content-header">
      	<h1>
          Mantenimiento
          <small>Areas</small>
        </h1>

        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
          <li class="active">Area</li>
        </ol>

      </section>

      <!-- Main Content -->
  <section class="Content">
    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">

          <div class="row clsPadding2">
            <div class="col-lg-2 hidden-xs"></div>

            <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addAreas"><b>Nuevo</b></button>
              </div> 

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" onclick="goEditar()"; class="btn btn-primary btn-block" data-toggle="modal" data-target=""
              ><b>Modificar</b></a></button>
              </div>

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" onclick="eliminarArea()" class="btn btn-primary btn-block" data-toggle="modal" data-target=""><b>Eliminar</b></button>
              </div>

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target=""><b>Imprimir</b></button>
              </div>

          </div>

<!--  Modal Entregable -->

          <div class="row clsPadding2"> 
                <div class="col-lg-12 col-xs-12 table-responsive" >

                    <table class="table table-striped" id="listado">
                        <thead>
                            <tr class="clsCabereraTabla">
                              <th>Area</th>
                              <th>Codigo</th>
                              <th>Descripcion</th>
                              <th>Siglas</th>
                              <th>Area Superior</th>
                              <th>esGeneral</th>
                              <th>estecnica</th>
                              <th>esarea</th>
                            </tr>
                        </thead>
                      
                  
                    </table>  
              
                </div>
          </div> 

<!-- Fin Modal Entregable -->


      </div>

      <div class="col-lg-1 hidden-xs"></div>

      

    </div>
  </section>

      <!-- /.content -->
    <div id="modalUser">
        @include('partials.addArea')
    </div>

</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';  

    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarArea',
      "columns" : [
        {data : 'carea', name: 'ta.carea'},
        {data : 'codigo', name: 'ta.codigo'},
        {data : 'descripcion', name: 'ta.descripcion'},
        {data : 'siglas', name: 'ta.siglas'},
        {data : 'padre', name: 't.descripcion'},
        {data : 'esgeneral', name: 'ta.esgeneral'},
        {data : 'estecnica', name: 'ta.estecnica'},
        {data : 'esarea', name: 'ta.esarea'},
  
      ],

      "rowCallback" : function( row, data ) {

          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    }); 

     $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/         

        $(this).toggleClass('selected');
      }); 

     //Boton Modificar
      function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(7));
        //getUrl('editarUsuarios/'+id.substring(7),'');
        //$("#addReg").modal("show");
        $.ajax({

                url:'editarArea/'+id.substring(7),
                type: 'GET',
                /*data : $("#frmusuarios").serialize(),*/
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#modalUser").html(data);
                    $('#div_carga').hide(); 
                    $("#addAreas").modal("show");
                    
                },            
            });
    }else{
        $('.alert').show();
    }
  }

  $("#addAreas").on('show.bs.modal',function(e){
         
          });

  //Boton Nuevo - Agregar
  /*$("#idAddArea").on("click", */function grabar(){
           
             $.ajax({

                url:'agregarArea',
                type: 'POST',
                data : $("#frmarea").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#resultado").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        }

    function eliminarArea(id){
          var id =0;
    if (selected.length > 0 ){
        id = selected[0];

     
          $.ajax({
            url: 'eliminarAreaMant/' + id.substring(7),
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show(); 
        
            },              
            success : function (data){
               //$('#div_carga').hide(); 
              $('#resultado').html(data);
            },
          }); 

           }else{
        $('.alert').show();
       }         

        }


</script>
