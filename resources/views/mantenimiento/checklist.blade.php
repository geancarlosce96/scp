<div class = "content-wrapper">
	<!-- Content Header (Page header) -->
      <section class = "content-header">
      	<h1>
          Mantenimiento
          <small>checklist</small>
        </h1>

        <ol class="breadcrumb">
          <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
          <li class="active">checklist</li>
        </ol>

      </section>

      <!-- Main Content -->
  <section class="Content">
    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">

          <div class="row clsPadding2">
            <div class="col-lg-2 hidden-xs"></div>

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addCheLi"><b>Nuevo</b></button>
              </div> 

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" onclick="goEditar()"; class="btn btn-primary btn-block" data-toggle="modal" data-target=""><b>Modificar</b></a></button>
              </div>

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" onclick="eliminarClist()" class="btn btn-primary btn-block" data-toggle="modal" data-target=""><b>Eliminar</b></button>
              </div>

              <div class="col-lg-2 col-xs-6 clsPadding">
                  <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target=""><b>Imprimir</b></button>
              </div>


          </div>

          <!--  Modal Entregable -->

          <div class="row clsPadding2"> 
                <div class="col-lg-12 col-xs-12 table-responsive" >

                    <table class="table table-striped" id="listado">
                        <thead>
                            <tr class="clsCabereraTabla">
                              <th>ID</th>
                              <th>Rol</th>
                              <th>Descripcion</th>
                              <th>Fase</th>
                            </tr>
                        </thead>
                      
                  
                    </table>  
              
                </div>
          </div> 



      </div>

      <div class="col-lg-1 hidden-xs"></div>

    </div>
  </section>

      <!-- /.content -->
    <div id="modalUser">
        @include('partials.addCheckListMantenimiento')
    </div>


</div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';  

    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarCheckList',
      "columns" : [
        {data : 'cchecklist', name: 'tch.cchecklist'},
        {data : 'rol', name: 'trd.descripcionrol'},
        {data : 'descripcionactvividad', name: 'tch.descripcionactvividad'},
        {data : 'fase', name: 'tfa.descripcion'},
  
      ],

      "rowCallback" : function( row, data ) {

          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    }); 

     $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/         

        $(this).toggleClass('selected');
      }); 


     //Boton Modificar
      function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(7));
        //getUrl('editarUsuarios/'+id.substring(7),'');
        //$("#addReg").modal("show");
        $.ajax({

                url:'editarCheckList/'+id.substring(7),
                type: 'GET',
                /*data : $("#frmusuarios").serialize(),*/
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#modalUser").html(data);
                    $('#div_carga').hide(); 
                    $("#addCheLi").modal("show");
                    
                },            
            });
    }else{
        $('.alert').show();
    }
  }

  $("#addCheLi").on('show.bs.modal',function(e){
         
          });


  //Boton Nuevo - Agregar
  /*$("#idAddCheLi").on("click", */function grabar(){
           
             $.ajax({

                url:'agregarCheckListMant',
                type: 'POST',
                data : $("#frmcheck").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#resultado").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        }

        function eliminarClist(id){
         var id =0;
    if (selected.length > 0 ){
        id = selected[0];

     
          $.ajax({
            url: 'eliminarCheckList/' + id.substring(7),
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show(); 
        
            },              
            success : function (data){
               //$('#div_carga').hide(); 
              $('#resultado').html(data);
            },
          }); 

           }else{
        $('.alert').show();
       }         

        }




</script>
