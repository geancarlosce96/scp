  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Versión 2.5.21</b> actualizado al día 14-05-19 hasta las 20:00
    </div>
    <strong>Copyright &copy; 2017- <?php echo Date("Y") ?> <a href="https://anddes.com">Anddes Asociados SAC</a></strong> Todos los derechos reservados.
    <!-- <a href="https:\\wa.me/+51936101081"><i class="fa fa-whatsapp"></i></a> -->
  </footer>