<?php 

use App\Erp\PanelSupport;

session_start();

if(Session::has('menu')){
  $menu=Session('menu');

  $cpersona=Auth::user()->cpersona;

  $tpersona=DB::table('tpersona as per')        
  ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
  ->where('per.cpersona','=',$cpersona)
  ->first();

  $fingreso=$tpersona->fingreso;

  $ccargo=null;
  if ($tpersona) {
    $ccargo=$tpersona->ccargo;
  }

  $tcargos=DB::table('tcargos as tc') 
  ->where('tc.cargoparentdespliegue','=',$ccargo)
  ->get();

  $tproyecto=DB::table('tproyecto as tp') 
  ->where('tp.cpersona_gerente','=',$cpersona)
  ->where('tp.cestadoproyecto','=','001') 
  ->get();

  $esaprobadorji=0;
  $esaprobadorgp=0;

  if($tcargos){
    $esaprobadorji=1;
  }

  if($tproyecto){
    $esaprobadorgp=1;
  }

  $objNotif  = new PanelSupport();

  $JIpersona=$objNotif->JIpersonalogin($cpersona);
//dd($JIpersona);

/*if($esaprobadorgp==1){
  dd($tpersona,$esaprobadorji,$esaprobadorgp,$tproyecto,$tcargos,'Es gerente de proyecto');
}
if($esaprobadorji==1){
  dd($tpersona,$esaprobadorji,$esaprobadorgp,$tcargos,$tproyecto,'Es jefe inmediato');
}*/
  $alertaevaluacion = $objNotif->alertevaluacion();

}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Control de Proyectos | Anddes</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="{{ url('bootstrap/css/bootstrap.min.css') }}">
  <!-- Font Awesome -->
  
  <link rel="stylesheet" href="{{ url('dist/css/font-awesome.min.css') }}">
  
  <!-- Ionicons -->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css') }}">-->
  <link rel="stylesheet" href="{{ url('dist/css/ionicons.min.css') }}">  
  <!-- Theme style -->
  <!--<link rel="stylesheet" href="{{ url('dist/css/AdminLTE.min.css') }}">-->
  <link rel="stylesheet" type="text/css" href="{{ url('dist/css/AdminLTE.css') }}">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="{{ url('dist/css/skins/_all-skins.css') }}">
   <!-- iCheck -->
   <link rel="stylesheet" href="{{ url('plugins/iCheck/flat/blue.css') }}">
   <!-- Morris chart -->
   <link rel="stylesheet" href="{{ url('plugins/morris/morris.css') }}">
   <!-- jvectormap -->
   <link rel="stylesheet" href="{{ url('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
   <!-- Date Picker -->
   <link rel="stylesheet" href="{{ url('plugins/datepicker/datepicker3.css') }}">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="{{ url('plugins/daterangepicker/daterangepicker.css') }}">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="{{ url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
   <!-- DataTable-->
   <link rel="stylesheet" href="{{ url('plugins/datatables/dataTables.bootstrap.css') }}">
   <link rel="stylesheet" href="{{ url('plugins/datatables/jquery.dataTables.min.css') }}">

   <link rel="stylesheet" href="{{ url('dist/css/bootstrap-treeview.min.css') }}" >
    <link rel="stylesheet" href="{{ url('plugins/jstree/themes/default/style.min.css') }}" >
   <link rel="stylesheet" type="text/css" href="{{ url('dist/css/highCheckTree.css') }}"/>
   <link rel="stylesheet" type="text/css" href="{{ url('dist/css/jquery-ui.css') }}"/>

   <!-- plugin chosen CSS-->
   <link rel="stylesheet" type="text/css" href="{{ url('dist/css/chosen/chosen.min.css') }}"/>
   <link rel="stylesheet" href="{{ url('dist/css/chosen/chosencombo.css') }}">
    <!-- plugin sweetalert CSS-->
    <link rel="stylesheet" type="text/css" href="{{ url('plugins/sweetalert/sweetalert.css') }}"/>

<link rel="stylesheet" href=" {{ url('plugins/fullcalendar/fullcalendar.min.css') }}">
  <link rel="stylesheet" href=" {{ url('plugins/fullcalendar/fullcalendar.print.css') }}" media="print">

  <link rel="stylesheet" href=" {{ url('plugins/dropzone/dropzone.css') }}">

   <style>

   body {
    padding-right: 0px !important;
    }


   #div_carga{
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background: url(img/blanco.png) repeat;
    opacity: 1;
    display:none;
    z-index:810;
  }
  #div_msg{
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;

    background-color:rgba(255,255,255,0.6);

    display:none;
    z-index:1500;
  }
  #div_contenido{
    opacity: 1;
  }
  #div_msg_error{
    position:absolute;
    top:0;
    left:0;
    width:100%;
    height:100%;
    background-color:rgba(255,255,255,0.6);


    display:none;
    z-index:1500;
  }    
  #cargador{
    position:absolute;
    top:50%;
    left: 50%;
    margin-top: -25px;
    margin-left: -25px;
  }
  .success_editar, .success_eliminar, .success{background: #11B15B;}
  .error{background: #CD201A;}
  .success, .success_editar, .success_eliminar, .error{ width: 100%; padding: 20px 20px; color: #ffffff; font-size: 16px; font-weight: bold; position: fixed; top: 0; left: 0;}
  .success i, .success_editar i,.success_eliminar i, .error i{font-size: 20px;}

  .custom-combobox {
    position: relative;
    display: inline-block;
  }
  .custom-combobox-toggle {
    position: absolute;
    top: 0;
    bottom: 0;
    margin-left: -1px;
    padding: 0;
  }
  .custom-combobox-input {
    margin: 0;
    padding: 5px 10px;
  }


  #resultado {
    height : 1024px;    
    width: 100% !important;
    overflow: auto;
    background-color: white;
  }
  
  #buscarlistadoproyecto_chosen .chosen-drop{
    width: 100px !important;
    max-width: 100px !important;
    min-width: 100px !important;

    

  }


</style>

@yield('extra-css')
</head>
<body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="{{ url('/panel') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <div class="logo-mini"><!--<b>A</b>LT--></div>
        <!-- logo for regular state and mobile devices -->
        <div class="logo-lg"><!--<b>Admin</b>LTE--></div>
      </a>

      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <ul class="nav navbar-nav">  
          <!--inicio alertas JI horas por aprobar-->
          <?php $totalPendAprJI=0; ?>

          <ul class="nav navbar-nav" id="alertJI">  
           @if($esaprobadorji==1)
           <!--inicio alertas JI horas por aprobar-->
           <?php           
           $nothtJIpend=$objNotif->alertasJIPend($cpersona);
            //dd($nothtpend);
           ?>
           <li class="dropdown notifications-menu" id="notiji"  >
            @if(isset($nothtJIpend))
            <?php 
                  //dd($nothtJIpend);
            $i=1;
            $totnoti=count($nothtJIpend);
            ?>
            <a href="#" @if($totnoti >=1) class="dropdown-toggle" data-toggle="dropdown" @endif id="notificaciones()" title="Tienes {{$totnoti}} semanas por aprobar como JI">
              <i class="fa fa-calendar-check-o"></i>
              @if($totnoti >=1)
              <span class="label" style="font-size: 12px;background-color: red">{{$totnoti}}</span>
              <input type="hidden" value="{{$totnoti}}" id="notifji">
              <?php $totalPendAprJI=$totnoti;?>
              @endif
            </a>
            <ul class="dropdown-menu" >
             <li class="header">Tienes {{$totnoti}} semanas por aprobar como JI</li>
             <li>
              <!-- inner menu: contains the actual data -->
              <ul class="menu">
                @foreach($nothtJIpend as $n)
                <?php  
                $anio=substr($n['fecha'],0,4);
                ?>
                <li>
                  <a href="#" onclick="irSemanaAprobar('{{ csrf_token() }}','{{ $n['fecha'] }}')" title="Tienes {{ $n['cantidad'] }} horas pendientes de aprobación: semana {{ $n['semana'] }} - {{$anio}}">                    
                    <i class="fa fa-warning text-red">
                      <input type="hidden" id="nothtpend_<?php echo $i?>" name="notplan" value="{{ $n['fecha'] }}">
                    </i>Tienes {{ $n['cantidad'] }} horas por aprobar: semana {{ $n['semana'] }} - {{$anio}} 
                  </a>
                </li>
                <?php $i++; ?>                
                @endforeach
                @endif
              </ul>
            </li>
            <!-- <li class="footer"><a href="#">Ver todos</a></li> -->
          </ul>
        </li>

        <!--Fin alertas JI horas por aprobar-->
        @endif

        @if($esaprobadorgp==1)

        <!--inicio alertas GP horas por aprobar-->
        <?php           

        $nothtGPpend=$objNotif->alertasGPPend($cpersona);

            //dd($nothtpend);

        ?>
        <li class="dropdown notifications-menu" id="notigp"  >
          @if(isset($nothtGPpend))

          <?php 

                  //dd($nothtGPpend);
          $i=1;
          $totnoti=count($nothtGPpend);

          ?>
          <a href="#" @if($totnoti >=1) class="dropdown-toggle" data-toggle="dropdown" @endif id="notificaciones()" title="Tienes {{$totnoti}} semanas por aprobar como GP">
            <i class="fa fa-calendar-minus-o"></i>
            @if($totnoti >=1)
            <span class="label" style="font-size: 12px;background-color: red">{{$totnoti}}</span>
            <input type="hidden" value="{{$totnoti}}" id="notifgp">
            @endif
          </a>
          <ul class="dropdown-menu" >
           <li class="header">Tienes {{$totnoti}} semanas por aprobar como GP</li>
           <li>
            <!-- inner menu: contains the actual data -->
            <ul class="menu">
              @foreach($nothtGPpend as $n)
              <?php  
              $anio=substr($n['fecha'],0,4);
              ?>
              <li>
                <a href="#" onclick="irSemanaAprobar('{{ csrf_token() }}','{{ $n['fecha'] }}')" title="Tienes {{ $n['cantidad'] }} horas pendientes de aprobación: semana {{ $n['semana'] }} - {{$anio}}">                    
                  <i class="fa fa-warning text-red">
                    <input type="hidden" id="nothtpend_<?php echo $i?>" name="notplan" value="{{ $n['fecha'] }}">
                  </i>Tienes {{ $n['cantidad'] }} horas por aprobar: semana {{ $n['semana'] }} - {{$anio}} 
                </a>
              </li>
              <?php $i++; ?>                
              @endforeach
              @endif
            </ul>
          </li>
          <!-- <li class="footer"><a href="#">Ver todos</a></li> -->
        </ul>
      </li>

      <!--Fin alertas GP horas por aprobar-->
      @endif
    </ul>
  </ul>

  <div class="col-sm-8" style="color:#0069aa; font-size:20px; color:black; text-align: -webkit-center;" id="conteo" hidden>
			<span style="color:white;"> Faltan </span>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="dia" style="font-size:16px; color:white;">  </span> <br> <b> Dias </b>
			</button>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="hora" style="font-size:16px; color:white;"> </span> <br> <b>Horas </b>
			</button>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="min" style="font-size:16px; color:white;"> </span> <br> <b>Minutos </b>
			</button>
			<button data-toggle="button" class="btn btn bg-navy">
				<span id="seg" style="font-size:16px; color:white;"> </span> <br> <b> Segundos </b>
			</button>
			<span style="color:white;"> para el Aniversario de Anddes </span>
		</div>

  <div class="navbar-custom-menu"> 

    <ul class="nav navbar-nav"> 

  @if($JIpersona!='')

      @if(Session('capacitaciones_cantidad')>0)
      <li style="margin-left: 0px" >
        <div style="padding-top: 18px;">
          <a href="{{ url('capacitaciones') }}" 
          title="Listado de capacitaciones que tengo que revisar. Opcionalmente algunas tendrán una evaluación con una serie de preguntas que tengo que realizar en un tiempo determinado.">
          <span class="label label-white" style="font-size: 12px;"> 
            <i class="fa fa-pencil"></i>             
          </span>
          <span class="label label-warning" style="font-size: 12px">{{ Session('capacitaciones_cantidad') }}</span>
          </a>
        </div>
      </li>
      @endif

      <!--inicio alertas HT no enviados-->
      
      <li><div style="padding-top: 18px;"><span class="label label-white" style="font-size: 12px; background-color: ">JI: <?php echo $JIpersona; ?></span></div></li>
      
      <!--inicio alertas HT no enviados-->
      <?php           


      $nothtpend=$objNotif->alertasHTPend($cpersona);

          //dd($nothtpend);

      ?>
      <li class="dropdown notifications-menu" id="notiv"  >
        @if(isset($nothtpend))

        <?php 

                //dd($nothtpend);
        $i=1;
        $totnoti=count($nothtpend);

        ?>
        <a href="#" @if($totnoti >=1) class="dropdown-toggle" data-toggle="dropdown" @endif id="notificaciones()" title="Tienes {{$totnoti}} HT pendiente(s)">
          <i class="fa fa-bullhorn"></i>
          @if($totnoti >=1)
          <span class="label label-warning" style="font-size: 12px">{{$totnoti}}</span>
          <input type="hidden" value="{{$totnoti}}" id="notifv">
          @endif
        </a>
        <ul class="dropdown-menu" >
         <li class="header">Tienes {{$totnoti}} HT pendiente(s)</li>
         <li>
          <!-- inner menu: contains the actual data -->
          <ul class="menu">
            @foreach($nothtpend as $n)
            <?php  
            $anio=substr($n['fecha'],0,4);
            ?>
            <li>
              <a href="#" onclick="irSemanaObservada('{{ csrf_token() }}','{{ $n['fecha'] }}')" title="Tienes {{ $n['cantidad'] }} HT pendiente de la semana {{ $n['semana'] }} - {{$anio}}">                    
                <i class="fa fa-warning text-yellow">
                  <input type="hidden" id="nothtpend_<?php echo $i?>" name="notplan" value="{{ $n['fecha'] }}">
                </i>Tienes {{ $n['cantidad'] }} HT pendiente de la semana {{ $n['semana'] }} - {{$anio}} 
              </a>
            </li>
            <?php $i++; ?>                
            @endforeach
            @endif
          </ul>
        </li>
        <!-- <li class="footer"><a href="#">Ver todos</a></li> -->
      </ul>
    </li>

    <!--Fin alertas HT no enviados-->
    <!-- Inicio notificaciones Ejecutadas -->
    <?php           

    $noteje=$objNotif->alertasEje($cpersona);

    ?>
    <li class="dropdown notifications-menu" id="notie">
      @if(isset($noteje))
      <?php 
                //dd($noteje);
      $i=1;
      $totnoti=count($noteje);
      ?>
      <a href="#" @if($totnoti >=1) class="dropdown-toggle" data-toggle="dropdown" @endif id="notificaciones()" title="Tienes {{$totnoti}} HT con horas no enviadas">
        <i class="fa fa-pencil-square-o"></i>
        @if($totnoti >=1)
        <span class="label label-info" style="font-size: 12px">{{$totnoti}}</span>
        <input type="hidden" value="{{$totnoti}}" id="notife">
        @endif
      </a>
      <ul class="dropdown-menu" >
       <li class="header">Tienes {{$totnoti}} HT con horas no enviadas</li>
       <li>
        <!-- inner menu: contains the actual data -->
        <ul class="menu">
          @foreach($noteje as $n)
          <?php  
          $anio=substr($n['fecha'],0,4);
          ?>
          <li>
            <a href="#" onclick="irSemanaObservada('{{ csrf_token() }}','{{ $n['fecha'] }}')" title="Tienes {{ $n['cantidad'] }} horas no enviadas en la semana {{ $n['semana'] }} - {{$anio}}">                    
              <i class="fa fa-warning text-aqua">
                <input type="hidden" id="noteje_<?php echo $i?>" name="noteje" value="{{ $n['fecha'] }}">
              </i>Tienes {{ $n['cantidad'] }} horas no enviadas en la semana {{ $n['semana'] }} - {{$anio}} 
            </a>
          </li>
          <?php $i++; ?>                
          @endforeach
          @endif
        </ul>
      </li>
      <!-- <li class="footer"><a href="#">Ver todos</a></li> -->
    </ul>
  </li>
  <!-- Fin notificaciones Ejecutadas -->

  <!-- Inicio notificaciones Planificadas -->
  <?php           

  $notpl=$objNotif->alertasPlani($cpersona);

  ?>
  <li class="dropdown notifications-menu" id="notip">
    @if(isset($notpl))
    <?php 
                //dd($notpl);
    $i=1;
    $totnoti=count($notpl);
    ?>
    <a href="#" @if($totnoti >=1) class="dropdown-toggle" data-toggle="dropdown" @endif id="notificaciones()" title="Tienes {{$totnoti}} HT con horas planificadas">
      <i class="fa fa-calendar"></i>
      @if($totnoti >=1)
      <span class="label label-default" style="font-size: 12px">{{$totnoti}}</span>
      <input type="hidden" value="{{$totnoti}}" id="notifp">
      @endif
    </a>
    <ul class="dropdown-menu" >
     <li class="header">Tienes {{$totnoti}} HT con horas planificadas</li>
     <li>
      <!-- inner menu: contains the actual data -->
      <ul class="menu">
        @foreach($notpl as $n)
        <?php  
        $anio=substr($n['fecha'],0,4);
        ?>
        <li>
          <a href="#" onclick="irSemanaObservada('{{ csrf_token() }}','{{ $n['fecha'] }}')" title="Tienes {{ $n['cantidad'] }} horas planificadas en la semana {{ $n['semana'] }} - {{$anio}}">                    
            <i class="fa fa-warning text-default">
              <input type="hidden" id="notpla_<?php echo $i?>" name="notplan" value="{{ $n['fecha'] }}">
            </i>Tienes {{ $n['cantidad'] }} horas planificadas en la semana {{ $n['semana'] }} - {{$anio}} 
          </a>
        </li>
        <?php $i++; ?>                
        @endforeach
        @endif
      </ul>
    </li>
    <!-- <li class="footer"><a href="#">Ver todos</a></li> -->
  </ul>
</li>
<!-- Fin notificaciones Planificadas -->

<!-- Inicio notificaciones Observadas -->
<?php           

$notob=$objNotif->alertasObs($cpersona);

?>
<li class="dropdown notifications-menu" id="notio"> 
  @if(isset($notob))
  <?php 
  $i=1;
  $totnoti=count($notob);
  ?>
  <a href="#" @if($totnoti >=1) class="dropdown-toggle" data-toggle="dropdown" @endif id="notificaciones()" title="Tienes {{$totnoti}} HT con horas observadas">
    <i class="fa fa-bell-o"></i>
    @if($totnoti >=1)
    <span class="label label-danger" style="font-size: 12px">{{$totnoti}}</span>
    <input type="hidden" value="{{$totnoti}}" id="notifo">
    @endif
  </a>
  <ul class="dropdown-menu" >
   <li class="header">Tienes {{$totnoti}} HT con horas observadas</li>
   <li>
    <!-- inner menu: contains the actual data -->
    <ul class="menu">
      @foreach($notob as $n)
      <?php  
      $anio=substr($n['fecha'],0,4);
      ?>
      <li>
        <a href="#" onclick="irSemanaObservada('{{ csrf_token() }}','{{ $n['fecha'] }}')" title="Tienes {{ $n['cantidad'] }} horas observadas en la semana {{ $n['semana'] }} - {{$anio}}">                    
          <i class="fa fa-warning text-red">
            <input type="hidden" id="notobs_<?php echo $i?>" name="notobs" value="{{ $n['fecha'] }}">
          </i>Tienes {{ $n['cantidad'] }} horas observadas en la semana {{ $n['semana'] }} - {{$anio}} 
        </a>
      </li>
      <?php $i++; ?>
      @endforeach
      @endif
    </ul>
  </li>
  <!-- <li class="footer"><a href="#">Ver todos</a></li> -->
</ul>
</li>



@endif



<!-- Fin notificaciones Observadas -->

<!-- Tasks: style can be found in dropdown.less -->
          <!-- <li class="dropdown tasks-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-flag-o"></i>
              <span class="label label-danger">9</span>
            </a>
            <ul class="dropdown-menu">
              <li class="header">Usted tiene 9 Tareas</li>
              <li> -->
                <!-- inner menu: contains the actual data -->
                <!-- <ul class="menu"> -->
                  <!-- <li> --><!-- Task item -->
                    <!-- a href="#">
                      <h3>
                        Proyecto 1
                        <small class="pull-right">20%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">20% Completo</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
                  <!-- <li> --><!-- Task item -->
                    <!-- <a href="#">
                      <h3>
                        Proyecto 2
                        <small class="pull-right">40%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-green" style="width: 40%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">40% Completo</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
                  <!-- <li> --><!-- Task item -->
                    <!-- <a href="#">
                      <h3>
                        Proyecto 3
                        <small class="pull-right">60%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-red" style="width: 60%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">60% Completo</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
                  <!-- <li> --><!-- Task item -->
                    <!-- <a href="#">
                      <h3>
                        Proyecto 4
                        <small class="pull-right">80%</small>
                      </h3>
                      <div class="progress xs">
                        <div class="progress-bar progress-bar-yellow" style="width: 80%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">
                          <span class="sr-only">80% Completo</span>
                        </div>
                      </div>
                    </a>
                  </li> -->
                  <!-- end task item -->
                <!-- </ul>
              </li>
              <li class="footer">
                <a href="#">Ver todas las tareas</a>
              </li>
            </ul>
          </li> -->
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">

              <?php 


              $persona=DB::table('tpersona')
              ->where('cpersona','=',$cpersona)->first(); 

              $dni=$persona->identificacion;

              $im='';


              $imagen='images/'.$dni.'.jpg';

              if(file_exists($imagen)){
                $im=1;

              }

              else
              {
                $im=0;
              }

              ?>
              @if($im==1)
              <img src="{{ url('images/'.$dni.'.jpg') }}" class="user-image" alt="User Image">
              @else
              <img src="{{ url('images/sinfoto.jpg') }}" class="user-image" alt="User Image">
              @endif

              @if(Auth::user())             

              <span class="hidden-xs">{{$persona->abreviatura}}</span>

              @endif
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                @if($im==1)
                <img src="{{ url('images/'.$dni.'.jpg') }}" class="user-image" alt="User Image">
                @else
                <img src="{{ url('images/sinfoto.jpg') }}" class="user-image" alt="User Image">
                @endif
                @if(Auth::user())
                <p>
                  {{ $persona->abreviatura }}
                  <small></small>
                </p>
                @endif
              </li>
              <!-- Menu Body -->
              <!-- <li class="user-body"> -->
                <!-- <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">&nbsp;</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">&nbsp;</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">&nbsp;</a>
                  </div>
                </div> -->
                <!-- /.row -->
              <!-- </li> -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" onclick="getUrl('passwordUsuario','')" class="btn btn-primary btn-flat">Cambiar contraseña</a>
                </div>
                <div class="pull-right">
                  <a href="{{ URL::to('logout') }}" class="btn btn-danger btn-flat">Cerrar Sesión</a>
                </div>
              </li>
            </ul>
          </li>
           <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>

          <!-- Inicio Ayuda -->
          <!-- <li class="dropdown notifications-menu" > 
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" title="Ayuda">
              <i class="fa fa-question-circle" style="font-size: 12px"></i>
            </a>
            <ul class="dropdown-menu" >
             <li class="header"></li>
             <li>
              <ul class="menu">
                <li>
                  <a href="https://drive.google.com/file/d/1QZbDP2qmTfW0cEJ1XCD5xhp0bqqWuF6v/view" target="_blank" title="Ver video"> 
                    <h6>
                      <i class="fa fa-youtube-play text-blue">
                       Asignación de equipo
                     </i>
                   </h6>
                   <iframe width="250" height="140" src="https://drive.google.com/file/d/1QZbDP2qmTfW0cEJ1XCD5xhp0bqqWuF6v/preview" frameborder="0" allowfullscreen></iframe>
                 </a>
               </li>
             </ul>
             <ul class="menu">
               <li>
                  <a href="https://drive.google.com/file/d/1X8ggxACfYodHvylQC_fJi7pFetl_kGMY/view" target="_blank" title="Ver video"> 
                    <h6>
                      <i class="fa fa-youtube-play text-blue">
                       Habilitación de tareas
                     </i>
                   </h6>
                   <iframe width="250" height="140" src="https://drive.google.com/file/d/1X8ggxACfYodHvylQC_fJi7pFetl_kGMY/preview" frameborder="0" allowfullscreen></iframe>
                 </a>
               </li>
               </ul>
             <ul class="menu">
               <li>
                  <a href="https://drive.google.com/file/d/1IY5neqcQSikNmbSzbuxEMuJiamVBVpy0/view" target="_blank" title="Ver video"> 
                    <h6>
                      <i class="fa fa-youtube-play text-blue">
                       Llenado de hoja de tiempo
                     </i>
                   </h6>
                   <iframe width="250" height="140" src="https://drive.google.com/file/d/1IY5neqcQSikNmbSzbuxEMuJiamVBVpy0/preview" frameborder="0" allowfullscreen></iframe>
                 </a>
               </li>
               </ul>
             <ul class="menu">
               <li>
                  <a href="https://drive.google.com/file/d/1rbjiII9lNXwdHAGls6OAb0TTKYIfKdyD/view" target="_blank" title="Ver video"> 
                    <h6>
                      <i class="fa fa-youtube-play text-blue">
                       Aprobaciones hoja de tiempo
                     </i>
                   </h6>
                   <iframe width="250" height="140" src="https://drive.google.com/file/d/1rbjiII9lNXwdHAGls6OAb0TTKYIfKdyD/preview" frameborder="0" allowfullscreen></iframe>
                 </a>
               </li>
               </ul>
             <ul class="menu">
               <li>
                  <a href="https://drive.google.com/file/d/1BpHus9ak1OO1LXNxFxpWyMCSvmK3V1Kx/view" target="_blank" title="Ver video"> 
                    <h6>
                      <i class="fa fa-youtube-play text-blue">
                       Rendición de gastos 
                     </i>
                   </h6>
                   <iframe width="250" height="140" src="https://drive.google.com/file/d/1BpHus9ak1OO1LXNxFxpWyMCSvmK3V1Kx/preview" frameborder="0" allowfullscreen></iframe>
                 </a>
               </li>
             </ul>
           </li>
         </ul>
       </li> -->
       <!-- Fin Ayuda -->


     </ul>
   </div>
 </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->

          @include('layouts.menus')
          @include('layouts.menusconf')


    <!-- Content Wrapper. Contains page content -->
    <div id="resultado" style="width:100% !important;">
      @yield('content')
      @yield('control-horas')
      @yield('resumen-cchh')
      @yield('cargabilidad-mensual')
      @yield('cargabilidad-semanal-personal')
      @yield('cargabilidad-semanal-nofacturable')
      @yield('cargabilidad-semanal-admin')
      @yield('indicador-semanal')


      @yield('cargabilidad-mensual-proyectos')
      @yield('cargabilidad-mensual-grafico')
      @yield('indicador-mensual')
      @yield('indicador-mensual-general')

       @yield('master-proyecto')
       @yield('master-proyecto-privilegio')
       @yield('masterBD-proyecto')
    </div>

@include('layouts.footer')
  </div>

  <!-- ./wrapper -->

  <div id="div_carga">
    <img id="cargador" style="width: 250px; height: 150px;" src="{{ url('img/anddes2.gif') }}"/>
  </div>

  <!-- Modal Comformidad-->

  <div id="div_msg">
    <div id="div_contenido">
      <div class="success"><i class="fa fa-check-square-o" aria-hidden="true"></i> Registro guardado correctamente
        <button type="button" class="btn btn-primary pull-right" onclick="$('#div_msg').hide();">Cerrar</button>
      </div>

    </div>
  </div>

  <div id="div_msg_error">
    <div id="contenido">
      <div class="error"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Registro no guardado, vuelve a intentar
        <button type="button" class="btn btn-primary pull-right" onclick="$('#div_msg_error').hide();" style="text-align: right">Cerrar</button></div>
        
      </div>
      <div id="detalle_error">
      </div>
    </div>




    <!-- jQuery 2.2.3 -->
    <script src="{{ url('plugins/jQuery/jquery-2.2.3.min.js') }}"></script>

    <!-- jQuery UI 1.11.4 -->
    <!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js') }}"></script> -->
    <script src="{{ url('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>

      $.widget.bridge('uibutton', $.ui.button);
      
    </script>

    <!-- Bootstrap 3.3.6 -->
    <script src="{{ url('bootstrap/js/bootstrap.min.js') }}"></script>
    <!-- Morris.js charts -->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js') }}"></script>-->
    <script src="{{ url('js/raphael-min.js') }}"></script>
    <script src="{{ url('plugins/morris/morris.min.js') }}"></script>
    <!-- Sparkline -->
    <script src="{{ url('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
    <!-- jvectormap -->
    <script src="{{ url('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ url('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <!-- jQuery Knob Chart -->
    <script src="{{ url('plugins/knob/jquery.knob.js') }}"></script>
    <!-- daterangepicker -->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.11.2/moment.min.js') }}"></script>-->
    <script src="{{ url('plugins/daterangepicker/moment.min.js') }}"></script>
    <script src="{{ url('plugins/daterangepicker/daterangepicker.js') }}"></script>
    <!-- datepicker -->
    <script src="{{ url('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ url('plugins/datepicker/locales/bootstrap-datepicker.es.js') }}"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="{{ url('plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <!-- Slimscroll -->
    <script src="{{ url('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
    <!-- FastClick -->
    <script src="{{ url('plugins/fastclick/fastclick.js') }}"></script>
    
    <!-- inicio para exportar -->
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"> </script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"> </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"> </script>
    <script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.colVis.min.js"> </script>
    <!-- fin para exportar -->

    <script src="https://cdn.datatables.net/fixedcolumns/3.2.6/js/dataTables.fixedColumns.min.js"> </script>
    @yield('js')  <!-- js particular -->

    <!-- AdminLTE App -->
    <script src="{{ url('dist/js/app.min.js') }}"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="{{ url('dist/js/pages/dashboard.js') }}"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="{{ url('dist/js/demo.js') }}"></script>
    <script src="{{ url('js/func.js') }}"></script>
    <script src="{{ url('dist/js/bootstrap-treeview.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('dist/js/highchecktree.js') }}"></script>
    <script type="text/javascript" src="{{ url('plugins/jstree/jstree.min.js') }}"></script>

    <!-- plugin chosen JS-->
    <script type="text/javascript" src="{{ url('dist/js/chosen/chosen.jquery.min.js') }}"></script>
    
    <!-- plugin sweetalert JS-->
    <script type="text/javascript" src="{{ url('plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('plugins/sweetalert/sweetalert-dev.js') }}"></script>



    <!-- Script para modo grafico -->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <!--  por aqui debe estar la mis script -->
    <script type="text/javascript" src="{{ url('js/listaproyecto.js') }}"></script>
    <!-- dropzone -->
    <script type="text/javascript" src="{{ url('plugins/dropzone/dropzone.js') }}"></script>
    <script type="text/javascript" src="{{ url('dist/js/tableHeadFixer.js') }}"></script>

    @yield('page-script')
    <script>

      var hoy = new Date();
      dia_ac = hoy.getDate(); 
      mes_ant = hoy.getMonth()+1;
      anio_ac= hoy.getFullYear();
      fecha_ac = String(anio_ac+"/"+mes_ant+"/"+dia_ac);



      var fecingreso=$('#fechaingreso').val();

      var fechainitemp = moment('2017/12/04');
      if (moment(fecingreso) < fechainitemp) {
        var fecinicio=fechainitemp;
      }
      else{
       var fecinicio=fecingreso;
     }

     var fecha_bloq1 = moment(fecinicio);
     var fecha_bloq2 = moment(fecha_ac);
     var dife = fecha_bloq1.diff(fecha_bloq2, 'days');
     var fec_conca = dife+"d";

     console.log(fecha_bloq1.diff(fecha_bloq2, 'days'), ' dias de diferencia');
     //alert(fec_conca);


     $('#divCalendario').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      calendarWeeks: true,
      todayHighlight: true,
      container: '#asdMenu',
      startDate: fec_conca,
    });


     $("#divCalendario").datepicker("setDate", new Date());

     $('#divCalendario').on('changeDate', function() {
      //alert($('#divCalendario').datepicker('getFormattedDate'));
      postUrlTimeSheet('{{ csrf_token() }}');
    }); 



     function irSemanaObservada(tok,fec){


      var fecha= fec;


      $.ajax({

        type:"post",
        url:'editarTodosRegistrodeHoras',
        data:{
          fecha: fecha,
          "_token": tok,
        },
        beforeSend: function () {
          $('#div_carga').show(); 
          
        },
        success: function(data){
          $('#div_carga').hide(); 
          $("#resultado").html(data);


          //$("#div_msg").show();
        },
        error: function(data){
          $('#div_carga').hide();
          //$('#detalle_error').html(data);
          //$("#div_msg_error").show();
        }
      });    


    }

    //irSemanaObservada('{{ csrf_token() }}');

    function irSemanaAprobar(tok,fec){


      var fecha= fec;

      var fec_inicial=fec.substr(8,2)+'/'+fec.substr(5,2)+'/'+fec.substr(0,4);
      var fec_final=fec.substr(8,2)+'/'+fec.substr(5,2)+'/'+fec.substr(0,4);

      //alert(fec+' '+fec_inicial+' '+fec_final);

      $.ajax({
        url: 'verHorasAprobaciones',
        type: 'GET',
        data:  {

          "fdesde" : fecha,
          "fhasta" : fecha, 
          "valor" : 'aprobar', 


        },
        beforeSend: function () {
          $('#div_carga').show(); 
        },              
        success : function (data){
          $("#resultado").html(data)                   
          $('#div_carga').hide();                   

          $("#fdesde").val(fec_inicial);
          $("#fhasta").val(fec_final); 
          $("#valor").val('aprobar'); 
          ; 
        },
      });    


    }

  </script>

  <script type="text/javascript">
    var BASE_URL = "<?php echo url('/'); ?>";
  </script>

  <script type="text/javascript">
    // window.onbeforeunload = confirmExit;
    // function confirmExit()
    // {
      //   return "Ha intentado salir de esta pagina. Si ha realizado algun cambio en los campos sin hacer clic en el boton Guardar, los cambios se perderan. Seguro que desea salir de esta pagina? ";
      // }
      // var refreshId =  setInterval( function(){
        //   $('.noti').reload();//actualiza
        //  }, 1000 );

        function Colorvacio(){
          var notifv =  $("#notifv").val();
          $("#notiv").css('background-color','yellow');
          // $("#notip").css('background-color','b2ffff');
          // $("#notio").css('background-color','red');
        }
        function Colorplan(){
          var notifp =  $("#notifp").val();
          // $("#notiv").css('background-color','yellow');
          $("#notip").css('background-color','silver');
          // $("#notio").css('background-color','red');
        }
        function Coloreje(){
          var notife =  $("#notife").val();
          // $("#notiv").css('background-color','yellow');
          $("#notie").css('background-color','#b2ffff');
          // $("#notio").css('background-color','red');
        }
        function Colorobs(){
          var notifo =  $("#notifo").val();
          // $("#notiv").css('background-color','yellow');
          // $("#notip").css('background-color','b2ffff');
          $("#notio").css('background-color','red');
        }

        function Colorji(){
          var notifji =  $("#notifji").val();
          // $("#notiv").css('background-color','yellow');
          // $("#notip").css('background-color','b2ffff');
          $("#notiji").css('background-color','green');
        }

        function Colorgp(){
          var notifgp =  $("#notifgp").val();
          // $("#notiv").css('background-color','yellow');
          // $("#notip").css('background-color','b2ffff');
          $("#notigp").css('background-color','green');
        }

        function Colorand(){
          // var color =  "#0069AA"; 
          var color = "rgba(0, 105, 170, 0)";
          $("#notiv").css('background-color',color)
          $("#notip").css('background-color',color)
          $("#notio").css('background-color',color)
          $("#notie").css('background-color',color)
          $("#notiji").css('background-color',color)
          $("#notigp").css('background-color',color)
        }
        // console.log('noti',typeof notifp); 
        if (typeof  notifv != 'undefined') {
          setInterval(Colorvacio, 500);
          setInterval(Colorand, 1000);
        }
        if (typeof notifp != 'undefined') {
          setInterval(Colorplan, 500);
          setInterval(Colorand, 1000);
        }
        if (typeof notifo != 'undefined') {
          setInterval(Colorobs, 500);
          setInterval(Colorand, 1000);
        }
        if (typeof notife != 'undefined') {
          setInterval(Coloreje, 500);
          setInterval(Colorand, 1000);
        }

        if (typeof notifji != 'undefined') {
          setInterval(Colorji, 500);
          setInterval(Colorand, 1000);
        }
        if (typeof notifgp != 'undefined') {
          setInterval(Colorgp, 500);
          setInterval(Colorand, 1000);
        }



        // if(document.getElementById("notifji").value>=1){
        //   alert('Usted tiene '+<?php echo $totalPendAprJI ?>+' HT pendiente(s) de aprobación.');

        // }

        
        var cantidad =<?php echo $totalPendAprJI ?>;
        var url = BASE_URL + '/int?url=aprobacionHoras/aprobar' ; 
        var url_actual = location.href;

        if (cantidad > 0 && url != url_actual) {

          swal({
            title: " USTED TIENE : <span style='color:red'>" + cantidad +"</span> <br/> <small>HOJA(S) DE TIEMPO POR APROBAR ....!</small>",
            text: "Presione IR, para aprobar las hojas de tiempo",
            type: "info",
            html: true,
            showCancelButton: true,
            confirmButtonText: "Ir",
            confirmButtonColor: "green",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false 
          },
          function(isConfirm){
            if (isConfirm) {
              $(window).attr('location', url);
              swal({
                title: "APROBAR",
                text: "Hojas de tiempo",
                type: "success",
                timer: 1000,
                showConfirmButton: false
              });
            }
            else {
              swal({
                title: "CANCELADO",
                text: "Hoja de tiempo",
                type: "error",
                timer: 1000,
                showConfirmButton: false
              });
            } 
          }
          );

        };



      </script>
      


     

      <script>
var end = new Date('04/12/2019 09:00');
console.log(end);
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;

    function showRemaining() {
        var now = new Date();
        var distance = end - now;
        if (distance < 0) {

            clearInterval(timer);
            document.getElementById('#conteo').innerHTML = 'EXPIRED!';

            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        //document.getElementById('countdown').innerHTML = days + ' dias, ';
        //document.getElementById('countdown').innerHTML += hours + ' horas, ';
        //document.getElementById('countdown').innerHTML += minutes + ' minutos y ';
				//document.getElementById('countdown').innerHTML += seconds + ' segundos';
        if(BASE_URL == 'http://192.168.1.17:8180' || BASE_URL == 'http://localhost/scp/public'){
          $("#conteo").show()
          $("#dia").text(days);
          $("#hora").text(hours);
          $("#min").text(minutes);
          $("#seg").text(seconds);
        }
				console.log(BASE_URL)
//				$("#conteo").css('position', 'absolute').css('width', '100%').css('height','90%').css('margin', 0).css('margin-top', '2%').css('top', '0').css('left', '0').css('float','left').css('z-index', 600);
    }

    timer = setInterval(showRemaining, 1000);
</script>


    </body>
    @stack('evaluacion360periodo')
    @stack('listado')
    @stack('generalscript')
    @stack('horasextras')
    
    </html>

