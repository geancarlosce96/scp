
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar" id="asdMenu">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          @if($im==1)
          <img src="{{ url('images/'.$dni.'.jpg') }}" class="img-circle" alt="User Image">
          @else
          <img src="{{ url('images/sinfoto.jpg') }}" class="img-circle" alt="User Image">
          @endif
        </div>
        @if(Auth::user())
        <div class="pull-left info">
          <p>{{ Auth::user()->nombre }}</p>
          <a href="#"><i class="fa fa-circle text-success"></i> En linea</a>
          <input type="hidden" value="{{ $fingreso }}" id="fechaingreso">
        </div>
        @endif
      </div>
      <!-- search form -->
      <!-- <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Buscar...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                  <i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form> -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <div style="overflow: auto; max-height: 950px" >
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MENÚ GENERAL</li>
		@if(isset($menu['01'])==1 && $menu['01']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Cliente</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
			@if(isset($menu['0101'])==1 && $menu['0101']=='1')
            <li><a href="{{ url('int?url=listadoClientes') }}"><i class="fa fa-circle-o"></i> Registro Clientes</a></li>
			@endif
			@if(isset($menu['0104'])==1 && $menu['0104']=='1')
            <li><a href="#"><i class="fa fa-circle-o"></i> Registro de Contactos</a></li>
			@endif
          </ul>
        </li>
		@endif
		@if(isset($menu['04'])==1 && $menu['04']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Hoja de Tiempo</span>
            <span class="pull-right-container">
              	<i class="fa fa-angle-left pull-right"></i>
              	@if( isset($nothtJIpend) )
            	<span class="label pull-right bg-red" title="Tienes semanas por aprobar como JI">@if(count($nothtJIpend) >=1){{ count($nothtJIpend) }} @endif</span>
            	@endif
              	@if( isset($nothtGPpend) )
              	<span class="label pull-right bg-red" title="Tienes semanas por aprobar como GP">@if(count($nothtGPpend)>=1) {{ count($nothtGPpend) }} @endif</span>
            	@endif
            	@if( isset($nothtpend) )
              	<span class="label pull-right bg-yellow" title="Tienes HT pendiente">@if(count($nothtpend)>=1) {{ count($nothtpend) }} @endif</span>
            	@endif
            	@if( isset($noteje) )
              	<span class="label pull-right label-info" title="Tienes horas no enviadas">@if(count($noteje)>=1) {{ count($noteje) }} @endif</span>
            	@endif
            	@if( isset($notpl) )
              	<span class="label pull-right label-default" title="Tienes horas planificadas">@if(count($notpl)>=1) {{ count($notpl) }} @endif</span>
            	@endif
            	@if( isset($notob) )
              	<span class="label pull-right label-danger" title="Tienes horas observadas">@if(count($notob)>=1) {{ count($notob) }} @endif</span>
            	@endif
            </span>
          </a>
          <ul class="treeview-menu">
          	@if(isset($menu['0401'])==1 && $menu['0401']=='1')
            <li>
            	<a onclick="postUrlTimeSheet('{{ csrf_token() }}');/*getUrl('registroHoras','')*/" href="#">
            		<i class="fa fa-circle-o"></i> Registro de Horas
            		<span class="pull-right-container">
            			@if( isset($nothtpend) )
		            	<span class="label pull-right bg-yellow" title="Tienes HT pendiente">@if(count($nothtpend)>=1) {{ count($nothtpend) }} @endif</span>
		            	@endif
		              	@if( isset($noteje) )
		              	<span class="label pull-right label-info" title="Tienes horas no enviadas">@if(count($noteje)>=1) {{ count($noteje) }} @endif</span>
		            	@endif
		            	@if( isset($notpl) )
		            	<span class="label pull-right label-default" title="Tienes horas planificadas">@if(count($notpl)>=1) {{ count($notpl) }} @endif</span>
		            	@endif
		              	@if( isset($notob) )
		              	<span class="label pull-right label-danger" title="Tienes horas observadas">@if(count($notob)>=1) {{ count($notob) }} @endif</span>
		            	@endif
            		</span>
            	</a>
            </li>
            @endif            
	        @if(isset($menu['0402'])==1 && $menu['0402']=='1')
            <li>
            	<a href="{{ url('int?url=aprobacionHoras') }}/aprobar">

            		<i class="fa fa-circle-o"></i> Aprobaciones
            		<span class="pull-right-container">
            			@if( isset($nothtJIpend) )
		            	<span class="label pull-right bg-red" title="Tienes semanas por aprobar como JI">{{ count($nothtJIpend) }}</span>
		            	@endif
		              	@if( isset($nothtGPpend) )
		              	<span class="label pull-right bg-red" title="Tienes semanas por aprobar como GP">{{ count($nothtGPpend) }}</span>
		            	@endif
            		</span>
            	</a>
            </li>

            <li>
              <a href="{{ url('int?url=aprobacionHoras') }}/historial">
                <i class="fa fa-circle-o"></i> Historial de Aprobaciones
              </a>
            </li>

            @endif
            @if(isset($menu['0347'])==1 && $menu['0347']=='1')
            <li><a href="{{ url('int?url=validahorasRooster') }}"><i class="fa fa-circle-o"></i> Validación de Rooster</a></li>
            @endif
      			@if(isset($menu['0405'])==1 && $menu['0405']=='1')
            <li><a href="{{ url('int?url=listadodataScp') }}"><i class="fa fa-circle-o"></i> Estados HT</a></li>
            @endif
      			@if(isset($menu['0406'])==1 && $menu['0406']=='1')
            <li><a href="{{ url('int?url=listadodataScpJI') }}"><i class="fa fa-circle-o"></i> Estados</a></li>
            @endif

            @if(isset($menu['0407'])==1 && $menu['0407']=='1')
            <li><a href="{{ url('/reporteht') }}"><i class="fa fa-circle-o"></i> Reporte HT </a></li>
            @endif

            @if($esaprobadorgp==1)
          	<li><a href="{{ url('int?url=listadodataScpGP') }}"><i class="fa fa-circle-o"></i>Estados GP</a></li>
	        @endif
          </ul>
        </li>
        @endif
        @if(isset($menu['05'])==1 && $menu['05']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Rendición de Gastos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	@if(isset($menu['0501'])==1 && $menu['0501']=='1')
            <li><a href="{{ url('int?url=listaGastosProyecto') }}" ><i class="fa fa-circle-o"></i> Registro de Gastos</a></li>
            @endif
            @if(isset($menu['0505'])==1 && $menu['0505']=='1')
            <li><a href="{{ url('int?url=aprobacionRendicion') }}"><i class="fa fa-circle-o"></i> Aprobaciones de Gastos</a></li>
            @endif
             @if(isset($menu['0506'])==1 && $menu['0506']=='1')
            <li><a href="{{ url('reportedegastos') }}"><i class="fa fa-circle-o"></i> Reporte gastos</a></li>
            @endif

          </ul>
        </li>
        @endif
        @if(isset($menu['03'])==1 && $menu['03']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Reportes Generales</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Control Documentario
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              	@if(isset($menu['0701'])==1 && $menu['0701']=='1')
                <li><a href="{{ url('int?url=panelJefe') }}"><i class="fa fa-circle-o"></i> Panel</a></li>
                @endif
                @if(isset($menu['0702'])==1 && $menu['0702']=='1')
                <li><a href="{{ url('int?url=listaJefe') }}"><i class="fa fa-circle-o"></i> Lista</a></li>
                @endif
                @if(isset($menu['0703'])==1 && $menu['0703']=='1')
                <li><a href="{{ url('int?url=programacionJefe') }}"><i class="fa fa-circle-o"></i> Programación</a></li>
                @endif
                @if(isset($menu['0704'])==1 && $menu['0704']=='1')
                <li><a href="{{ url('int?url=ubicacionJefe') }}"><i class="fa fa-circle-o"></i> Ubicación</a></li>
                @endif
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> <span>Proyectos</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              	@if(isset($menu['0340'])==1 && $menu['0340']=='1')
                <li><a href="matrizp" target="_blank"><i class="fa fa-circle-o"></i> Matriz de Proyectos</a></li>
                @endif
                @if(isset($menu['0375'])==1 && $menu['0375']=='1')
                  <li><a href="{{ url('master-proyecto') }}" target="_blank"><i class="fa fa-circle-o"></i>BD del Proyecto</a></li>
                @endif
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Áreas
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
             
              @if(isset($menu['0371'])==1 && $menu['0371']=='1')
                <!-- <li><a href="{{ url('carga-semanal') }}"><i class="fa fa-circle-o"></i> Carga Semanal</a></li> -->
                <!-- <li><a href="{{ url('indicador-semanal') }}"><i class="fa fa-circle-o"></i> Indicador Semanal</a></li> -->
                <li><a href="{{ url('indicador-semanal-general') }}"><i class="fa fa-circle-o"></i> Data Cargabilidad Semanal</a></li>
                <li><a href="{{ url('grafico-semanal') }}"><i class="fa fa-circle-o"></i> Gráfico Cargabilidad Semanal</a></li>
              @endif
              @if(isset($menu['0372'])==1 && $menu['0372']=='1')
                <!-- <li><a href="{{ url('carga-mensual') }}"><i class="fa fa-circle-o"></i> Carga Mensual</a></li> -->
                <!-- <li><a href="{{ url('indicador-mensual') }}"><i class="fa fa-circle-o"></i> Indicador Mensual</a></li> -->
                <li><a href="{{ url('indicador-mensual-general') }}"><i class="fa fa-circle-o"></i> Data Cargabilidad Mensual</a></li>
                <li><a href="{{ url('grafico-mensual') }}"><i class="fa fa-circle-o"></i> Gráfico Cargabilidad Mensual</a></li>
              @endif
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Ventas por Disciplina</a></li> -->
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Valorización Mes</a></li> -->
              </ul>
            </li>
          </ul>
        </li>
        @endif
        @if(isset($menu['02'])==1 && $menu['02']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Propuesta</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<!-- <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Inicio
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('invitacion') }}"><i class="fa fa-circle-o"></i> Listado de Invitación</a></li>
                <li><a href="{{ url('preliminar') }}"><i class="fa fa-circle-o"></i> Listado de Preliminar</a></li>
                <li><a href="{{ url('propuesta') }}"><i class="fa fa-circle-o"></i> Listado de Propuesta</a></li>
              </ul>
            </li -->
            <!-- <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Planificación
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Cronograma</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Ejecución
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="{{ url('equipo') }}"><i class="fa fa-circle-o"></i> Propuestas - PPA</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Monitoreo y Control
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Seguimiento</a></li>
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Cierre
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#"><i class="fa fa-circle-o"></i> Resultado</a></li>
              </ul>
            </li> -->
            <!-- Inicio Propuesta v2 -->
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-clone" style="font-size: 12px"></i>
                <span><b> Propuestas v2</b></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="{{ url('invitacion/registrar') }}" title="">
                    <i class="fa fa-clone"></i> Registrar Invitación</a>
                </li>
                <li>
                  <a href="{{ url('invitaciones') }}" title="">
                    <i class="fa fa-clone"></i> Listado de Invitaciones</a>
                </li>
                <li>
                  <a href="{{ url('prepropuesta/registrar') }}" title="">
                    <i class="fa fa-clone"></i> Registrar PrePropuestas</a>
                </li>
                <li>
                  <a href="{{ url('prepropuestas') }}" title="">
                    <i class="fa fa-clone"></i> Listado de PrePropuestas</a>
                </li>
                <li>
                  <a href="{{ url('propuesta/registrar') }}" title="">
                    <i class="fa fa-clone"></i> Registrar Propuesta</a>
                </li>
                <li>
                  <a href="{{ url('propuestas') }}" title="">
                    <i class="fa fa-clone"></i> Listado de Propuestas</a>
                </li>
              </ul>
            </li> -->
            <!-- Fin Propuesta v2 -->

            <!-- Inicio Propuesta v3 -->
            <!-- <li class="treeview">
              <a href="#">
                <i class="fa fa-clone" style="font-size: 12px"></i>
                <span><b> Propuestas v3</b></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="{{ url('propuestas/inicio') }}" title="">
                    <i class="fa fa-clone"></i> Inicio</a>
                </li>
                <li>
                  <a href="{{ url('propuestas/planificacion') }}" title="">
                    <i class="fa fa-clone"></i> Planificacion</a>
                </li>
                <li>
                  <a href="{{ url('propuestas/ejecucion') }}" title="">
                    <i class="fa fa-clone"></i> Ejecución</a>
                </li>
                <li>
                  <a href="{{ url('propuestas/listas') }}" title="">
                    <i class="fa fa-clone"></i> Listas</a>
                </li>                
              </ul>
            </li> -->
            <!-- Fin Propuesta v3 -->
            @if(isset($menu['0201'])==1 && $menu['0201']=='1')
            <li><a href="{{ url('int?url=listadoPropuesta') }}"><i class="fa fa-circle-o"></i> Registro</a></li>
            @endif
            <!-- @if(isset($menu['0205'])==1 && $menu['0205']=='1')
            <li><a href="{{ url('int?url=disciplinaPropuesta') }}"><i class="fa fa-circle-o"></i> Selección de disciplinas</a></li>
            @endif -->
            @if(isset($menu['0208'])==1 && $menu['0208']=='1')
            <!-- <li><a href="{{ url('int?url=estructuraPropuesta') }}"><i class="fa fa-circle-o"></i> Estructura de propuestas</a></li> -->
            <li><a href="{{url('int?url=importarppavista')}}" ><i class="fa fa-circle-o"></i>Cargar Datos PPA </a></li>
            @endif
            <!-- @if(isset($menu['0211'])==1 && $menu['0211']=='1')
            <li><a href="{{ url('int?url=detallePropuesta') }}"><i class="fa fa-circle-o"></i> Detalle de propuestas</a></li>
            @endif -->
          </ul>
        </li>
        @endif
        @if(isset($menu['03'])==1 && $menu['03']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span style="font-size: 12px"><b>Proyecto</b></span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          	<!-- <li><a href="{{ url('edt/grafico') }}"><i class="fa fa-circle-o"></i> EDT gráfico</a></li> -->
            @if(isset($menu['0368'])==1 && $menu['0368']=='1')
          	<li><a href="{{ url('int?url=listaProyectos') }}"><i class="fa fa-circle-o"></i> Listado de proyectos</a></li>
          	@endif
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Inicio
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(isset($menu['0304'])==1 && $menu['0304']=='1') 
                <li><a href="{{ url('int?url=preproyecto') }}"><i class="fa fa-circle-o"></i> Registro de Preproyecto / SOC</a></li>
                @endif
                @if(isset($menu['0301'])==1 && $menu['0301']=='1')
                <li><a href="{{ url('int?url=aprobacionPropuesta') }}"><i class="fa fa-circle-o"></i> Validación GP</a></li>
                @endif
                @if(isset($menu['0316'])==1 && $menu['0316']=='1')
                <li><a href="{{ url('int?url=registroHR') }}/esc"><i class="fa fa-circle-o"></i>Hoja Resumen</a></li>
                @endif
                @if(isset($menu['0370'])==1 && $menu['0370']=='1')
                <li><a href="{{ url('int?url=registroHR') }}/lec"><i class="fa fa-circle-o"></i>Ver Hoja Resumen</a></li>
                @endif
                @if(isset($menu['0307'])==1 && $menu['0307']=='1')
                  <li><a href="{{ url('int?url=ActividadesProyecto') }}"><i class="fa fa-circle-o"></i>Estructura de actividades</a></li>
                @endif
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Planificación
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(isset($menu['0356'])==1 && $menu['0356']=='1')
                <li><a href="{{ url('int?url=asignacionParticipantesProyecto') }}"><i class="fa fa-circle-o"></i> Asignación de Profesionales</a></li>
                @endif
                @if(isset($menu['0359'])==1 && $menu['0359']=='1')
                <li><a href="{{ url('int?url=habilitarTareasProyecto') }}"><i class="fa fa-circle-o"></i> Habilitación de tareas del proyecto</a></li>
                @endif
                <li><a href="{{ url('participacion') }}" title="Participación en Proyectos"><i class="fa fa-circle-o"></i> Participación en Proyectos</a></li>
                @if(isset($menu['0362'])==1 && $menu['0362']=='1')
                <li><a href="{{ url('int?url=planificacionareaProyecto') }}"><i class="fa fa-circle-o"></i> Planificación de tareas del proyecto</a></li>
                <li><a href="{{ url('planificacion/horas') }}"><i class="fa fa-circle-o"></i> Planificación de proyectos</a></li>
                <li><a href="{{ url('planificacion/horas/diarias') }}"><i class="fa fa-circle-o"></i>Planificación de proyectos diaria</a></li>
                @endif
                @if(isset($menu['0365'])==1 && $menu['0365']=='1')
                <li><a href="{{ url('int?url=planificacionTareaAdm') }}"><i class="fa fa-circle-o"></i> Planificación de tareas administrativas</a></li>
                @endif
                @if(isset($menu['0325'])==1 && $menu['0325']=='1')
                <li><a href="{{ url('int?url=edtProyecto') }}"><i class="fa fa-circle-o"></i> EDT</a></li>
                @endif
                @if(isset($menu['0322'])==1 && $menu['0322']=='1')
                <li><a href="{{ url('int?url=entregablesProyecto') }}"><i class="fa fa-circle-o"></i> Lista de entregables</a></li>
                @endif
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Ejecución
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Minuta</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> RFI - Estado RFI</a></li>-->
                @if(isset($menu['0374'])==1 && $menu['0374']=='1')
                <li><a href="{{ url('int?url=ruteoedtproyecto') }}"><i class="fa fa-circle-o"></i>Bandeja de entregables</a></li>
                @endif
                @if(isset($menu['0341'])==1 && $menu['0341']=='1')
                <li><a href="{{ url('int?url=cambiosCronogramaRooster') }}"><i class="fa fa-circle-o"></i> Cambios en Rooster</a></li>
                @endif
                @if(isset($menu['0344'])==1 && $menu['0344']=='1')
                <li><a href="{{ url('int?url=horasEjecutadasRooster') }}"><i class="fa fa-circle-o"></i> Control de Rooster Eejecutado</a></li>
                @endif
                @if(isset($menu['0714'])==1 && $menu['0714']=='1')
                <li><a href="{{ url('listaentregablestr') }}"><i class="fa fa-circle-o"></i> Listado de TR por Documento </a></li>
                @endif
              <!--   @if(isset($menu['0706'])==1 && $menu['0706']=='1')
                <li><a href="{{ url('int?url=configuracionTRUser') }}"><i class="fa fa-circle-o"></i> Configuración Transmittal</a></li>
                @endif -->
                <!-- @if(isset($menu['0710'])==1 && $menu['0710']=='1') -->
                <!-- <li><a href="{{ url('generartransmittal') }}"><i class="fa fa-circle-o"></i> Generación de Transmittal</a></li> -->
                <!-- @endif -->
                <!-- @if(isset($menu['0711'])==1 && $menu['0711']=='1') -->
                <!-- <li><a href="{{ url('int?url=recepcionTR') }}"><i class="fa fa-circle-o"></i> Recepción de Transmittal</a></li> -->
                <!-- @endif -->
                 @if(isset($menu['0711'])==1 && $menu['0711']=='1')
                <li><a href="{{ url('listaentregablestlogs') }}""><i class="fa fa-circle-o"></i> Logs de Entregables </a></li>
                @endif
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Respuesta de Transmittal</a></li> -->
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Monitoreo y control
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Cumplimiento de Procesos</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Panel de Control</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Ordenes de Cambio</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Control de Cronograma</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Control Programación semanal HH</a></li> -->
                @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                <li><a href="{{ url('int?url=registroDeviaciones') }}"><i class="fa fa-circle-o"></i> Control de cambios</a></li>
                @endif
                @if(isset($menu['0373'])==1 && $menu['0373']=='1')
                <li><a href="{{ url('performance') }}"><i class="fa fa-circle-o"></i> Performance</a></li>
                @endif                
                @if(isset($menu['0336'])==1 && $menu['0336']=='1')
                <li><a href="{{ url('curva-s') }}"><i class="fa fa-circle-o"></i> Curva S / Indicadores</a></li>
                @endif
                @if(isset($menu['0337'])==1 && $menu['0337']=='1')
                <li><a href="{{ url('control-horas') }}"><i class="fa fa-circle-o"></i>Control de Horas</a></li>
                {{-- <li><a href="{{ url('resumen-cchh') }}"><i class="fa fa-circle-o"></i>Resumen CCHH</a></li> --}}
                @endif                
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Control HH - GP</a></li> -->
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Control HH - Áreas</a></li> -->
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Control Progamación <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Semanal Entregables</a></li> -->
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Cronograma valorizado</a></li> -->
                <!-- <li><a href="{{ url('curvas') }}"><i class="fa fa-circle-o"></i> Curva S / Indicadores</a></li> -->
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> Valorizaciones</a></li> -->
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Cierre
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
              	@if(isset($menu['0339'])==1 && $menu['0339']=='1')
                <li><a href="{{ url('int?url=leccionesAprendidas') }}"><i class="fa fa-circle-o"></i> Lecciones Aprendidas</a></li>
                @endif
                @if(isset($menu['0369'])==1 && $menu['0369']=='1')
                <li><a href="{{ url('int?url=listarleccionesAprendidas') }}"><i class="fa fa-circle-o"></i> Listar Lecciones Aprendidas</a></li>
                @endif
                <!-- <li><a href="#"><i class="fa fa-circle-o"></i> ACS</a></li>
                <li><a href="#"><i class="fa fa-circle-o"></i> Lista de ACS</a></li> -->
              </ul>
            </li>
          </ul>
        </li>
        @endif
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        	<div class="box-body no-padding">
              <div id="divCalendario"></div>
            </div>
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        <li class="header">SOPORTE</li>
        
        {{-- aqui coloco mi nuevo menu depurado  --}}
        @if(isset($menu['06'])==1 && $menu['06']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Gestión Humana</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
              <small class="label pull-right bg-yellow">{{ $alertaevaluacion }}</small>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Evaluaciones de Desempeño
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(isset($menu['0606'])==1 && $menu['0606']=='1') 
                <li><a href="{{ url('periodoevaluacion') }}"><i class="fa fa-circle-o"></i> Periodo Evaluaciones</a></li>
                @endif
                @if(isset($menu['0607'])==1 && $menu['0607']=='1')
                <li><a href="{{ url('auditoriaevaluaciones') }}"><i class="fa fa-circle-o"></i> Auditoria Evaluaciones</a></li>
                @endif
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Vacaciones
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(isset($menu['0608'])==1 && $menu['0608']=='1') 
                <li><a href="{{ url('vacaciones') }}"><i class="fa fa-circle-o"></i> Vacaciones</a></li>
                @endif
                @if(isset($menu['0609'])==1 && $menu['0609']=='1')
                <li><a href="{{ url('vacacionesuccess') }}"><i class="fa fa-circle-o"></i> Apruebo Vacaciones</a></li>
                @endif
              </ul>
            </li>
            <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Horas Extras
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(isset($menu['0610'])==1 && $menu['0610']=='1') 
                <li><a href="{{ url('horasextras') }}"><i class="fa fa-circle-o"></i> Horas Extras</a></li>
                @endif
                @if(isset($menu['0611'])==1 && $menu['0611']=='1')
                <li><a href="{{ url('horasextrassuccess') }}"><i class="fa fa-circle-o"></i> Apruebo Horas Extras</a></li>
                @endif
              </ul>
            </li>
          </ul>
        </li>
        @endif
        {{-- aqui coloco mi nuevo menu depurado  --}}
        @if(isset($menu['10'])==1 && $menu['10']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Logística</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li class="treeview">
              <a href="#"><i class="fa fa-circle-o"></i> Solicitud de Herramientas y Suministros
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(isset($menu['1001'])==1 && $menu['1001']=='1')
                <li><a href="{{ url('int?url=logistica') }}"><i class="fa fa-circle-o"></i>Solicitar</a></li>
                @endif
                @if(isset($menu['1002'])==1 && $menu['1002']=='1')
                <li><a href="{{ url('int?url=listalogistica') }}"><i class="fa fa-circle-o"></i>Aprobar Solicitud</a></li>
                @endif
              </ul>
            </li>
          </ul> 
        </li>
        @endif

        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Capacitaciones</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <a href="{{ url('capacitacion') }}" title="Registrar una capacitación para un área o un grupo de usuarios del sistema, que opcionalmente tendrá una evaluación con preguntas.">
                <i class="fa fa-info-circle"></i> Solicitud de Capacitación</a>
            </li>
            <li>
              <a href="{{ url('capacitaciones/propuestas') }}" title="Listado de las capacitaciones que he realizado y/o aprobaciones que tengo que realizar para que las capacitaciones puedan ser vistas por los usuarios.">
                <i class="fa fa-info-circle"></i> Capacitaciones Propuestas 
              @if(Session('aprobaciones_cantidad')>0)
                <span class="label label-danger" title="Capacitaciones por Aprobar">{{ Session('aprobaciones_cantidad') }}</span>
              @endif
              </a>
            </li>
            <li>
              <a href="{{ url('capacitaciones') }}" title="Listado de capacitaciones que tengo que revisar. Opcionalmente algunas tendrán una evaluación con una serie de preguntas que tengo que realizar en un tiempo determinado.">
                <i class="fa fa-info-circle"></i> Capacitaciones
              @if(Session('capacitaciones_cantidad')>0)
                <span class="label label-danger" title="Capacitaciones por Aprobar">{{ Session('capacitaciones_cantidad') }}</span>
              @endif
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Tutoriales</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li>
              <!-- <a href="https://drive.google.com/file/d/1QZbDP2qmTfW0cEJ1XCD5xhp0bqqWuF6v/view" target="_blank" title="Ver video"> -->
              <a href="https://anddesperu.sharepoint.com/:v:/r/SIG/10/26_Control%20de%20Proyectos/6_DOC%20AREA/10-AND-26-VD-18-002_R0%20Asignacion%20de%20equipo%20SCPv2.mp4?csf=1&e=8pGqDS" target="_blank" title="Ver video">
                <i class="fa fa fa-youtube-play">
                </i>
                Asignación de equipo
              </a>
            </li>
            <li>
              <!-- <a href="https://drive.google.com/file/d/1X8ggxACfYodHvylQC_fJi7pFetl_kGMY/view" target="_blank" title="Ver video"> -->
              <a href="https://anddesperu.sharepoint.com/:v:/r/SIG/10/26_Control%20de%20Proyectos/6_DOC%20AREA/10-AND-26-VD-18-003_R0%20Habilitacion%20de%20tareas%20SCPv2.mp4?csf=1&e=H0t0g0" target="_blank" title="Ver video">
                <i class="fa fa fa-youtube-play">
                </i>
                Habilitación de tareas
              </a>
            </li>
            <li>
              <!-- <a href="https://drive.google.com/file/d/1IY5neqcQSikNmbSzbuxEMuJiamVBVpy0/view" target="_blank" title="Ver video"> -->
              <a href="https://anddesperu.sharepoint.com/:v:/r/SIG/10/26_Control%20de%20Proyectos/6_DOC%20AREA/10-AND-26-VD-18-004_R0%20Llenado%20de%20hoja%20de%20tiempo%20SCPv2.mp4?csf=1&e=rBPRRI" target="_blank" title="Ver video">
                <i class="fa fa fa-youtube-play">
                </i>
                 Llenado de hoja de tiempo
              </a>
            </li>
            <li>
              <!-- <a href="https://drive.google.com/file/d/1rbjiII9lNXwdHAGls6OAb0TTKYIfKdyD/view" target="_blank" title="Ver video"> -->
              <a href="https://anddesperu.sharepoint.com/:v:/r/SIG/10/26_Control%20de%20Proyectos/6_DOC%20AREA/10-AND-26-VD-18-005_R0%20Aprobacion%20de%20hoja%20de%20tiempo%20SCPv2.mp4?csf=1&e=K3UG2K" target="_blank" title="Ver video">
                <i class="fa fa fa-youtube-play">
                </i>
                Aprobaciones hoja de tiempo
              </a>
            </li>
            <li>
              <!-- <a href="https://drive.google.com/file/d/1BpHus9ak1OO1LXNxFxpWyMCSvmK3V1Kx/view" target="_blank" title="Ver video"> -->
              <a href="https://anddesperu.sharepoint.com/:v:/r/SIG/10/26_Control%20de%20Proyectos/6_DOC%20AREA/10-AND-26-VD-18-001_R0%20Inducccion%20de%20Rendicion%20de%20Gastos%20SCPv2.mp4?csf=1&e=tojzZI" target="_blank" title="Ver video">
                <i class="fa fa fa-youtube-play">
                </i>
                Rendición de gastos
              </a>
            </li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Encuestas</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active">
            	<!-- <a href="https://docs.google.com/forms/d/e/1FAIpQLSeZMNl7c74G9bOuPizhpN3ny52uMsBZDhgO13-oZvhIqQWj3A/viewform" target="_blank"> -->
              <a href="https://forms.office.com/Pages/ResponsePage.aspx?id=hyx8LtEO-UihenYmEhm_VIY5jvxH4BBIunXreFs0istUQlZUSlQ3SjFGOTgxUzEzWU00VEZEQzdKRC4u" target="_blank">
	            	<i class="fa fa-circle-o"></i>
	            	Conformidad de Servicio de <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camioneta y Conductor
	            </a>
	        </li>
          </ul>
        </li>
        @if(isset($menu['09'])==1 && $menu['09']=='1')
        <li class="treeview">
	        <a href="#">
	          <i class="fa fa-clone"></i> <span>Mantenimiento del SCP</span>
	          <span class="pull-right-container">
	            <i class="fa fa-angle-left pull-right"></i>
	          </span>
	        </a>
        <ul class="treeview-menu">
			@if(isset($menu['0601'])==1 && $menu['0601']=='1')
			<!-- <li><a href="{{ url('int?url=listadoEmpleado') }}"><i class="fa fa-circle-o"></i>Registro de colaboradores</a></li> -->
      <li><a href="{{ url('int?url=listarpersonal') }}"><i class="fa fa-circle-o"></i>Listado de profesionales</a></li>
			@endif
			@if(isset($menu['0803'])==1 && $menu['0803']=='1')
			<li><a href="{{ url('int?url=registroUsuario') }}"><i class="fa fa-circle-o"></i>Registro de usuarios</a></li>
			@endif
			@if(isset($menu['0813'])==1 && $menu['0813']=='1')
			<li><a href="{{ url('int?url=asigPermisos') }}"><i class="fa fa-circle-o"></i>Asignación de permisos</a></li>
      <li class="active"><a href="{{ url('int?url=usuarioConectado') }}" ><i class="fa fa-pencil"></i>Usuarios conectados</a></li>
			@endif
          @if(isset($menu['0925'])==1 && $menu['0925']=='1')
          <li><a href="{{ url('int?url=alergias') }}"><i class="fa fa-circle-o"></i>Alergias</a></li>

          
          <li><a href="{{ url('int?url=servicios') }}"><i class="fa fa-circle-o"></i>Servicios</a></li>
          
          
          <li><a href="{{ url('int?url=listaConceptoGastos') }}"><i class="fa fa-circle-o"></i>Concepto gastos</a></li>
          
          
          <li><a href="{{ url('int?url=listaActividades') }}"><i class="fa fa-circle-o"></i> Actividades</a></li>
          
          
          <li><a href="{{ url('int?url=listaEntregables') }}"><i class="fa fa-circle-o"></i>Entregables</a></li>
          
          
          <li><a href="{{ url('int?url=listaAreas') }}"><i class="fa fa-circle-o"></i>Areas</a></li>
          
          
          <li><a href="{{ url('int?url=listaCargos') }}"><i class="fa fa-circle-o"></i>Cargos</a></li>
          
          
          <li><a href="{{ url('int?url=listaSedes') }}"><i class="fa fa-circle-o"></i>Sedes</a></li>
          
          
          <li><a href="{{ url('int?url=listaFasesProyecto') }}"><i class="fa fa-circle-o"></i>Fases proyecto</a></li>
          
          
          <li><a href="{{ url('int?url=listaCheckList') }}"><i class="fa fa-circle-o"></i> CheckList</a></li>
          
          
          <li><a href="{{ url('int?url=listaAreasConocimiento') }}"><i class="fa fa-circle-o"></i>Areas conocimiento</a></li>
          
          
          <li><a href="{{ url('int?url=listaPais') }}"><i class="fa fa-circle-o"></i>Pais</a></li>
          
          
          <li><a href="{{ url('int?url=listaDisciplina') }}"><i class="fa fa-circle-o"></i>Disciplina</a></li>
          
          
          <li><a href="{{ url('int?url=listaContactoCargo') }}"><i class="fa fa-circle-o"></i>Contacto cargo</a></li>
          
          
          <li><a href="{{ url('int?url=listaInstitucion') }}"><i class="fa fa-circle-o"></i>Institución</a></li>
          
          
          <li><a href="{{ url('int?url=listaTiposContacto') }}"><i class="fa fa-circle-o"></i>Tipos contacto</a></li>
          
          
          <li><a href="{{ url('int?url=listaTiposInformacionContacto') }}"><i class="fa fa-circle-o"></i>Tipos información contacto</a></li>
          
          
          <li><a href="{{ url('int?url=listaOficina') }}"><i class="fa fa-circle-o"></i> Oficina</a></li>
          
          
          <li><a href="{{ url('int?url=listaTiposNoFacturables') }}"><i class="fa fa-circle-o"></i>Tipos no facturables</a></li>
          
          
          <li><a href="{{ url('int?url=listaTiposEntregables') }}"><i class="fa fa-circle-o"></i>Tipos entregables</a></li>
          
          
          <li><a href="{{ url('int?url=catalogoTablas') }}"><i class="fa fa-circle-o"></i>Catálogo tablas</a></li>
          
          
          <li><a href="{{ url('int?url=listaEnfermedad') }}"><i class="fa fa-circle-o"></i>Enfermedad</a></li>
          
          
          <li><a href="{{ url('int?url=listaEspecialidad') }}"><i class="fa fa-circle-o"></i>Especialidad</a></li>
          
          
          <li><a href="{{ url('int?url=catalogoGrupoTablas') }}"><i class="fa fa-circle-o"></i>Catálogo grupo tablas</a></li>
          
          
          <li><a href="{{ url('int?url=listaMonedaCambio') }}"><i class="fa fa-circle-o"></i>Moneda cambio</a></li>
          @endif
          @if(isset($menu['0926'])==1 && $menu['0926']=='1')
                <li><a href="{{ url('master-proyecto-privilegios') }}"><i class="fa fa-circle-o"></i>Asig Permisos - BD Proyecto</a></li>
          @endif
        </ul>
      </li>
      @endif
        <li class="header">LEYENDA</li>
        <li class="treeview"><a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Mostrar</a>
            <!-- <div style="overflow-y: auto; max-height: 200px" > -->
              <!-- <h5 style="text-align: center;"><a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Leyenda</a></h5> -->
              <div class="collapse" id="collapseExample">
                <b><input type="text" style="background-color: #FFBF00; width: 100%; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="HH planificadas" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="HH planificadas" disabled></b><br>
                <b><input type="text" style="background-color: #CD201A; width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="HH no enviadas u observadas" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="HH no enviadas u observadas" disabled></b><br>
                <b><input type="text" style="background-color: #0069AA; width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="HH enviadas u aprobadas JI" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="HH enviadas u aprobadas JI" disabled></b><br>
                <b><input type="text" style="background-color: #11B15B; width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="HH aprobadas" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="HH aprobadas" disabled></b><br>
                <b><input type="text" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;; background-color:green; " value="% de avance menor a 60" data-toggle="tooltip" data-placement="right" title="Porcentaje de avance menor a 60" disabled></b><br>
                <b><input type="text" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;; background-color:orange; " value="% de avance menor a 80" data-toggle="tooltip" data-placement="right" title="Porcentaje de avance menor a 80" disabled></b><br>
                <b><input type="text" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;; background-color:red; " value="% de avance mayor a 80" data-toggle="tooltip" data-placement="right" title="Porcentaje de avance mayor a 80" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="AND = Anddes" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="AND = Anddes" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="NF = No Facturable" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="NF = No Facturable" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="FF = Facturable" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="FF = Facturable" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="EJE = Ejecutado" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="EJE = Ejecutado" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="VJI = Validación Jefe Inmediato" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="VJI = Validación Jefe Inmediato" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="RGP = Revisor GP" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="RGP = Revisor GP" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="APR = Aprobado" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="APR = Aprobado" disabled></b><br>
                <br><br><br><br>
              </div>
            <!-- </div> -->
          </li>
      </ul>
        </div>
    </section>
    <!-- /.sidebar -->
  <input type="hidden" id="my_hidden_input">
  </aside>


