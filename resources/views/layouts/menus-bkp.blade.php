<aside class="main-sidebar" id="asdMenu">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        @if($im==1)
        <img src="{{ url('images/'.$dni.'.jpg') }}" class="img-circle" alt="User Image">
        @else
        <img src="{{ url('images/sinfoto.jpg') }}" class="img-circle" alt="User Image">
        @endif
      </div>
      @if(Auth::user())
      <div class="pull-left info">
        <p>{{ Auth::user()->Nombre }}</p>
        <a href="#"><i class="fa fa-circle text-success"></i>En Linea</a>
        <input type="hidden" value="{{ $fingreso }}" id="fechaingreso">
      </div>
      @endif
    </div>
    <!-- search form -->
    <form action="#" method="get" class="sidebar-form">
      <div class="input-group">
          <!-- <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span> -->
            </div>
          </form>
          <!-- /.search form -->
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">

            <!-- Inicio Capacitaciones -->
            <li class="treeview">
              <a href="#">
                <i class="fa fa-question-circle" style="font-size: 12px"></i>
                <span><b> Capacitaciones e Inducciones</b></span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li>
                  <a href="{{ url('capacitacion') }}" title="Registrar una capacitación para un área o un grupo de usuarios del sistema, que opcionalmente tendrá una evaluación con preguntas.">
                    <i class="fa fa-info-circle"></i> Solicitud de Capacitación</a>
                </li>
                <li>
                  <a href="{{ url('capacitaciones/propuestas') }}" title="Listado de las capacitaciones que he realizado y/o aprobaciones que tengo que realizar para que las capacitaciones puedan ser vistas por los usuarios.">
                    <i class="fa fa-info-circle"></i> Capacitaciones Propuestas 
                  
                  @if(Session('aprobaciones_cantidad')>0)
                    <span class="label label-danger" title="Capacitaciones por Aprobar">{{ Session('aprobaciones_cantidad') }}</span>
                  @endif

                  </a>
                </li>
                <li>
                  <a href="{{ url('capacitaciones') }}" title="Listado de capacitaciones que tengo que revisar. Opcionalmente algunas tendrán una evaluación con una serie de preguntas que tengo que realizar en un tiempo determinado.">
                    <i class="fa fa-info-circle"></i> Capacitaciones
                  @if(Session('capacitaciones_cantidad')>0)
                    <span class="label label-danger" title="Capacitaciones por Aprobar">{{ Session('capacitaciones_cantidad') }}</span>
                  @endif
                  </a>
                </li>

                <li>
                  <a href="https://drive.google.com/file/d/1QZbDP2qmTfW0cEJ1XCD5xhp0bqqWuF6v/view" target="_blank" title="Ver video">
                    <i class="fa fa fa-youtube-play">
                    </i>
                    Asignación de equipo
                  </a>
                </li>
                <li>
                  <a href="https://drive.google.com/file/d/1X8ggxACfYodHvylQC_fJi7pFetl_kGMY/view" target="_blank" title="Ver video">
                    <i class="fa fa fa-youtube-play">
                    </i>
                    Habilitación de tareas
                  </a>
                </li>
                <li>
                  <a href="https://drive.google.com/file/d/1IY5neqcQSikNmbSzbuxEMuJiamVBVpy0/view" target="_blank" title="Ver video">
                    <i class="fa fa fa-youtube-play">
                    </i>
                     Llenado de hoja de tiempo
                  </a>
                </li>
                <li>
                  <a href="https://drive.google.com/file/d/1rbjiII9lNXwdHAGls6OAb0TTKYIfKdyD/view" target="_blank" title="Ver video">
                    <i class="fa fa fa-youtube-play">
                    </i>
                    Aprobaciones hoja de tiempo
                  </a>
                </li>
                <li>
                  <a href="https://drive.google.com/file/d/1BpHus9ak1OO1LXNxFxpWyMCSvmK3V1Kx/view" target="_blank" title="Ver video">
                    <i class="fa fa fa-youtube-play">
                    </i>
                    Rendición de gastos
                  </a>
                </li>
              </ul>
            </li>
            <!-- Fin Capacitaciones -->

            <!--<li class="header">MAIN NAVIGATION</li>-->
            <!-- <li class="active treeview">  activar el menu por defecto  -->

              <!-- Menu fuera fuera pra todos -->
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-clone"></i> <span><b>Encuesta</b></span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li class="active"><a href="https://docs.google.com/forms/d/e/1FAIpQLSeZMNl7c74G9bOuPizhpN3ny52uMsBZDhgO13-oZvhIqQWj3A/viewform" target="_blank"><i class="fa fa-circle-o"></i><b>Conformidad de Servicio de <br> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Camioneta y Conductor</b></a></li>
                </ul>
              </li>
              <!-- Menu fuera fuera pra todos -->

              @if(isset($menu['01'])==1 && $menu['01']=='1')
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-clone"></i> <span>Clientes</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @if(isset($menu['0101'])==1 && $menu['0101']=='1')
                  <li class="active"><a href="#" onclick="getUrl('listadoClientes','')"><i class="fa fa-circle-o"></i>Registro</a></li>
                  @endif
                </ul>
              </li>
              @endif
              @if(isset($menu['02'])==1 && $menu['02']=='1')
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-clone"></i> <span>Propuesta</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                  <li class="active"><a href="#" onclick="getUrl('listadoPropuesta','')"><i class="fa fa-circle-o"></i>Registro</a></li>
                  @endif
                  @if(isset($menu['0205'])==1 && $menu['0205']=='1')
                  <li class="active"><a href="#" onclick="getUrl('disciplinaPropuesta','')"><i class="fa fa-circle-o"></i>Selección de disciplinas</a></li>
                  @endif
                  @if(isset($menu['0208'])==1 && $menu['0208']=='1')
                  <li class="active"><a href="#" onclick="getUrl('estructuraPropuesta','')"><i class="fa fa-circle-o"></i>Estructura de propuestas</a></li>
                  @endif
                  @if(isset($menu['0211'])==1 && $menu['0211']=='1')
                  <li class="active"><a href="#" onclick="getUrl('detallePropuesta','')"><i class="fa fa-circle-o"></i>Detalle de propuestas</a></li>
                  @endif
                  @if(isset($menu['0214'])==1 && $menu['0214']=='1')
                  <li class="active"><a href="#" onclick="getUrl('revisionPropuesta','')"><i class="fa fa-circle-o"></i>Generación de revisión</a></li>
                  @endif
                  @if(isset($menu['0217'])==1 && $menu['0217']=='1')
                  <li class="active"><a href="#" onclick="getUrl('confTransmittal','')"><i class="fa fa-circle-o"></i>Configuración de transmittal</a></li>
                  @endif
                  @if(isset($menu['0220'])==1 && $menu['0220']=='1')
                  <li class="active"><a href="#" onclick="getUrl('genTransmittal','')"><i class="fa fa-circle-o"></i>Generación de transmittal</a></li>
                  @endif
                  @if(isset($menu['0221'])==1 && $menu['0221']=='1')
                  <li><a href="#" onclick="getUrl('reportePropuestas','')"><i class="fa fa-circle-o"></i>Reportes</a></li>
                  @endif
                </ul>



              </li> 
              @endif

              <!--    **********************  -->
              @if(isset($menu['03'])==1 && $menu['03']=='1')
              <li class="treeview">
                <a href="#">
                  <i class="fa fa-clone"></i> <span>Proyecto</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">

                 <li>
                  <a href="#"><i class="fa fa-circle-o"></i>Inicio
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    @if(isset($menu['0301'])==1 && $menu['0301']=='1')
                    <li><a href="#" onclick="getUrl('aprobacionPropuesta','')"><i class="fa fa-circle-o"></i>Propuesta aprobada</a></li>
                    @endif
                    @if(isset($menu['0304'])==1 && $menu['0304']=='1')
                    <li><a href="#" onclick="getUrl('preproyecto','')"><i class="fa fa-circle-o"></i>Registro de preproyecto</a></li>
                    @endif
                    @if(isset($menu['0307'])==1 && $menu['0307']=='1')
                    <li><a href="#" onclick="getUrl('disciplinaProyecto','')"><i class="fa fa-circle-o"></i>Selección de disciplinas</a></li>
                    @endif
                    @if(isset($menu['0307'])==1 && $menu['0307']=='1')
                    <li><a href="#" onclick="getUrl('estructuraProyecto','')"><i class="fa fa-circle-o"></i>Estructura de actividades</a></li>
                    @endif
                    @if(isset($menu['0310'])==1 && $menu['0310']=='1')
                    <li><a href="#" onclick="getUrl('especificacionProyecto','')"><i class="fa fa-circle-o"></i>Especificación de proyecto</a></li>
                    @endif
                    @if(isset($menu['0311'])==1 && $menu['0311']=='1')
                    <li><a href="#" onclick="getUrl('registroContactosProy','')"><i class="fa fa-circle-o"></i>Lista de contacto</a></li>
                    @endif
                    @if(isset($menu['0316'])==1 && $menu['0316']=='1')
                    <li><a href="#" onclick="getUrl('registroHR','')"><i class="fa fa-circle-o"></i>HR del proyecto</a></li>
                    @endif
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    <li><a href="#" onclick="getUrl('checklistProy','')"><i class="fa fa-circle-o"></i>CheckList</a></li>
                    @endif

                  </ul>
                </li>           

                <li>
                  <a href="#"><i class="fa fa-circle-o"></i>Planificación
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    @if(isset($menu['0325'])==1 && $menu['0325']=='1')
                    <li><a href="#" onclick="getUrl('edtProyecto','')"><i class="fa fa-circle-o"></i>EDT</a></li>
                    @endif
                    @if(isset($menu['0322'])==1 && $menu['0322']=='1')
                    <li><a href="#" onclick="getUrl('entregablesProyecto','')"><i class="fa fa-circle-o"></i>Lista de entregables</a></li>
                    @endif
                    @if(isset($menu['0319'])==1 && $menu['0319']=='1')
                    <li><a href="#" onclick="getUrl('cronogramaProyecto','')"><i class="fa fa-circle-o"></i>Cronograma</a></li>
                    @endif
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    @endif
                    <!--<li><a href="#" onclick="getUrl('planificacionProyecto','')"><i class="fa fa-circle-o"></i>Planificación por Proyecto</a></li>-->
                    @if(isset($menu['0356'])==1 && $menu['0356']=='1')
                    <li><a href="#" onclick="getUrl('asignacionParticipantesProyecto','')"><i class="fa fa-circle-o"></i>Asignación de profesionales</a></li>
                    @endif
                    @if(isset($menu['0359'])==1 && $menu['0359']=='1')
                    <li><a href="#" onclick="getUrl('habilitarTareasProyecto','')"><i class="fa fa-circle-o"></i>Habilitación de tareas del proyecto</a></li>
                    @endif
                   
                    @if(isset($menu['0362'])==1 && $menu['0362']=='1')
                    <li><a href="#" onclick="getUrl('planificacionareaProyecto','')"><i class="fa fa-circle-o"></i>Planificación de tareas del proyecto</a></li>
                    @endif
              

                    @if(isset($menu['0362'])==1 && $menu['0362']=='1')
                    <li>
                      <a href="{{ url('planificacion/horas') }}"><i class="fa fa-circle-o"></i>Planificación de proyectos</a>
                    </li>
                    <!-- <li><a href="#" onclick="getUrl('planificacionProy','')" data-toggle="offcanvas"><i class="fa fa-circle-o"></i>Planificación </a></li> -->
                    @endif

                    @if(isset($menu['0365'])==1 && $menu['0365']=='1')
                    <li><a href="#" onclick="getUrl('planificacionTareaAdm','')"><i class="fa fa-circle-o"></i>Planificación de tareas administrativas</a></li>               
                    @endif

                    <!--<li><a href="#" onclick="getUrl('consultaplanificacionProyecto','')"><i class="fa fa-circle-o"></i>Consulta de Planificación</a></li>-->


                  </ul>
                </li>

                <li>
                  <a href="#"><i class="fa fa-circle-o"></i>Ejecución
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    @if(isset($menu['0368'])==1 && $menu['0368']=='1')
                    <li><a href="#" onclick="getUrl('listaProyectos','')"><i class="fa fa-circle-o"></i>Listado de proyectos</a></li>
                    @endif
                  </ul>              
                  <ul class="treeview-menu">
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    <li><a href="#" onclick="getUrl('checklistProy','')"><i class="fa fa-circle-o"></i>CheckList</a></li>
                    @endif
                  </ul>
                </li>


                <li>
                  <a href="#"><i class="fa fa-circle-o"></i>Monitoreo y control
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    <li><a href="#" onclick="getUrl('controlEjecucion','')"><i class="fa fa-circle-o"></i>Control de ejecución</a></li>
                    @endif
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    <li><a href="#" onclick="getUrl('curvaS','')"><i class="fa fa-circle-o"></i>Curva S / indicadores</a></li>
                    @endif
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    <li><a href="#" onclick="getUrl('registroDeviaciones','')"><i class="fa fa-circle-o"></i>Control de cambios / desviaciones</a></li>
                    @endif
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    <li><a href="matrizp" target="_blank"><i class="fa fa-circle-o"></i>Matriz de proyectos</a>
                      @endif

                    </li>
                  </ul>
                </li>                       


                <li>
                  <a href="#"><i class="fa fa-circle-o"></i>Cierre
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    @if(isset($menu['0201'])==1 && $menu['0201']=='1')
                    <li><a href="#" onclick="getUrl('checklistProy','')"><i class="fa fa-circle-o"></i>CheckList</a></li>
                    @endif
                    @if(isset($menu['0339'])==1 && $menu['0339']=='1')
                    <li><a href="#" onclick="getUrl('leccionesAprendidas','')"><i class="fa fa-circle-o"></i>Lecciones aprendidas</a></li>
                    <li><a href="#" onclick="getUrl('listarleccionesAprendidas','')"><i class="fa fa-circle-o"></i>Listar lecciones aprendidas</a></li> 
                    @endif
                    @if(isset($menu['369'])==1 && $menu['369']=='1')
                    <li><a href="#" onclick="getUrl('listarleccionesAprendidas','')"><i class="fa fa-circle-o"></i>Listar lecciones aprendidas</a></li>
                    @endif

                  </ul>
                </li>           

                <li>
                  <a href="#"><i class="fa fa-circle-o"></i>Construcción
                    <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                  </a>
                  <ul class="treeview-menu">
                    @if(isset($menu['0341'])==1 && $menu['0341']=='1')
                    <li><a href="#" onclick="getUrl('cambiosCronogramaRooster','')"><i class="fa fa-circle-o"></i>Cambios en cronograma</a></li>
                    @endif
                    @if(isset($menu['0344'])==1 && $menu['0344']=='1')
                    <li><a href="#" onclick="getUrl('horasEjecutadasRooster','')"><i class="fa fa-circle-o"></i>Control de horas ejecutadas</a></li>
                    @endif
                    @if(isset($menu['0347'])==1 && $menu['0347']=='1')
                    <li><a href="#" onclick="getUrl('validahorasRooster','')"><i class="fa fa-circle-o"></i>Validación de horas</a></li>
                    @endif
                    @if(isset($menu['0350'])==1 && $menu['0350']=='1')
                    <li><a href="#" onclick="getUrl('reporteProyectos','')"><i class="fa fa-circle-o"></i>Reportes</a></li>                
                    @endif
                  </ul>
                </li>            

              </ul>


            </li>


            <!--    *********************   -->

          </li> 	               
          @endif
          @if(isset($menu['07'])==1 && $menu['07']=='1')
          <li class="treeview">
            <a href="#">
              <i class="fa fa-clone"></i> <span>Control Documentario</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="#"><i class="fa fa-circle-o"></i>Jefe de CD
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                @if(isset($menu['0701'])==1 && $menu['0701']=='1')
                <li><a href="#" onclick="getUrl('panelJefe','')"><i class="fa fa-circle-o"></i>Panel</a></li>
                @endif
                @if(isset($menu['0702'])==1 && $menu['0702']=='1')
                <li><a href="#" onclick="getUrl('listaJefe','')"><i class="fa fa-circle-o"></i>Lista</a></li>
                @endif
                @if(isset($menu['0703'])==1 && $menu['0703']=='1')
                <li><a href="#" onclick="getUrl('programacionJefe','')"><i class="fa fa-circle-o"></i>Programación</a></li>
                @endif
                @if(isset($menu['0704'])==1 && $menu['0704']=='1')
                <li><a href="#" onclick="getUrl('ubicacionJefe','')"><i class="fa fa-circle-o"></i>Ubicación</a></li>
                @endif
              </ul>                
            </li>
            <li><a href="#"><i class="fa fa-circle-o"></i>Profesional de CD
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if(isset($menu['0705'])==1 && $menu['0705']=='1')
              <li><a href="#" onclick="getUrl('panelUsuario','')"><i class="fa fa-circle-o"></i>Panel</a></li>
              @endif
              @if(isset($menu['0706'])==1 && $menu['0706']=='1')
              <li><a href="#" onclick="getUrl('configuracionTRUser','')"><i class="fa fa-circle-o"></i>Configuración transmittal</a></li>
              @endif
              @if(isset($menu['0707'])==1 && $menu['0707']=='1')
              <li><a href="#" onclick="getUrl('consultaEDT','')"><i class="fa fa-circle-o"></i>Consulta de EDT</a></li>
              @endif
              @if(isset($menu['0708'])==1 && $menu['0708']=='1')
              <li><a href="#" onclick="getUrl('listaEntregableCD','')"><i class="fa fa-circle-o"></i>Lista de entregables</a></li>
              @endif
              @if(isset($menu['0709'])==1 && $menu['0709']=='1')
              <li><a href="#" onclick="getUrl('revisionDocumentos','')"><i class="fa fa-circle-o"></i>Revisión de documentos </a></li>
              @endif
              @if(isset($menu['0710'])==1 && $menu['0710']=='1')
              <li><a href="#" onclick="getUrl('generacionTR','')"><i class="fa fa-circle-o"></i>Generación de transmittal</a></li>
              @endif
              @if(isset($menu['0711'])==1 && $menu['0711']=='1')
              <li><a href="#" onclick="getUrl('recepcionTR','')"><i class="fa fa-circle-o"></i>Recepción de transmittal - documentos</a></li>
              @endif
              <!--<li><a href="#" onclick="getUrl('RegistroDOCsnTR','')"><i class="fa fa-circle-o"></i>Registro Doc. Sin Transmittal </a></li>-->                
              <!--<li><a href="#" onclick="getUrl('planificacionCD','')"><i class="fa fa-circle-o"></i>Planificación</a></li>-->
              @if(isset($menu['0712'])==1 && $menu['0701']=='1')
              <li><a href="#" onclick="getUrl('reporteControlDocumentario','')"><i class="fa fa-circle-o"></i>Reportes</a></li>
              @endif                  
            </ul>             
          </li>
        </ul>

      </li>
      @endif
      @if(isset($menu['04'])==1 && $menu['04']=='1')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-clone"></i> <span>Hoja de Tiempo</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @if(isset($menu['0401'])==1 && $menu['0401']=='1')
          <li class="active"><a href="#" onclick="postUrlTimeSheet('{{ csrf_token() }}');/*getUrl('registroHoras','')*/"><i class="fa fa-circle-o"></i>Registro de horas</a></li>
          @endif            
          @if(isset($menu['0402'])==1 && $menu['0402']=='1')
          <li class="active"><a href="#" onclick="getUrl('aprobacionHoras','')"><i class="fa fa-circle-o"></i>Aprobaciones</a></li>
          @endif            
          <!--<li class="active"><a href="#" onclick="getUrl('historialAprobaciones','')"><i class="fa fa-pencil"></i> Historial de Aprobaciones</a></li>-->
          @if(isset($menu['0405'])==1 && $menu['0405']=='1')
          <li class="active"><a href="#" onclick="getUrl('listadodataScp','')"><i class="fa fa-circle-o"></i>Estados HT</a></li>
          @endif

          @if(isset($menu['0406'])==1 && $menu['0406']=='1')
          <li class="active"><a href="#" onclick="getUrl('listadodataScpJI','')"><i class="fa fa-circle-o"></i>Estados</a></li>
          @endif

          @if($esaprobadorgp==1)
          <li class="active"><a href="#" onclick="getUrl('listadodataScpGP','')"><i class="fa fa-circle-o"></i>Estados GP</a></li>
          @endif


        </ul>



      </li>
      @endif
      @if(isset($menu['05'])==1 && $menu['05']=='1')
      <li class="treeview">
        <a href="#">
          <i class="fa fa-clone"></i> <span>Rendición de Gastos</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          @if(isset($menu['0501'])==1 && $menu['0501']=='1')
          <li class="active"><a href="#" onclick="getUrl('listaGastosProyecto','')" data-toggle="offcanvas"><i class="fa fa-circle-o"></i>Registro de gastos</a></li>
          @endif

          <!-- @if(isset($menu['0502'])==1 && $menu['0502']=='1') -->
          <!-- <li class="active"><a href="http://190.117.156.193/" target="_blank" ><i class="fa fa-circle-o"></i>Gastos Externo -->
          <!-- </a></li> -->
          <!-- @endif -->
            

          <!-- onclick="getUrl('listaGastosProyecto','')" este codigo esta fuera de gasto temporal  -->

            <!-- <li class="active"><a href="#" onclick="getUrl('gastosAdmProyecto','')"><i class="fa fa-pencil"></i> Gastos administrativos<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;por proyecto</a></li> -->
            <!-- <li class="active"><a href="#" onclick="getUrl('gastosVariosProyecto','')"><i class="fa fa-pencil"></i> Gastos administrativos<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;varios proyectos</a></li>  --> 
            @if(isset($menu['0505'])==1 && $menu['0505']=='1')
            <li class="active"><a href="#" onclick="getUrl('aprobacionRendicion','')"><i class="fa fa-circle-o"></i>Aprobaciones</a></li>
            @endif            
            <!--<li class="active"><a href="#" onclick="getUrl('rendiciongastosProyecto','')"><i class="fa fa-pencil"></i> Historial de<br>
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;aprobaciones Usuario</a></li>-->


            
            
          </ul>
        </li>  
        @endif
        <!--    **********************  -->
        @if(isset($menu['06'])==1 && $menu['06']=='1')
        <li class="treeview">
          <a href="#">
            <i class="fa fa-clone"></i> <span>Gestión Humana</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            @if(isset($menu['0601'])==1 && $menu['0601']=='1')
            <li><a href="#" onclick="getUrl('listadoEmpleado','')"><i class="fa fa-circle-o"></i>Registro de colaboradores</a></li>
            @endif
            @if(isset($menu['0602'])==1 && $menu['0602']=='1')
            <li>
              <a href="#"><i class="fa fa-circle-o"></i>Vacaciones
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu">
                <li><a href="#" onclick="getUrl('vacaciones','')"><i class="fa fa-circle-o"></i>Solicitud</a></li>
                <!--<li><a href="#" onclick="getUrl('construccion','')"><i class="fa fa-circle-o"></i> Aprobación</a></li>
                  <li><a href="#" onclick="getUrl('construccion','')"><i class="fa fa-circle-o"></i> Historial</a></li>-->
                </ul>
              </li>           
              @endif
              @if(isset($menu['0603'])==1 && $menu['0603']=='1')
              <li>
                <a href="#"><i class="fa fa-circle-o"></i>Compensación de horas
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="#" onclick="getUrl('compensaciones','')"><i class="fa fa-circle-o"></i>Solicitud</a></li>
                <!--<li><a href="#" onclick="getUrl('construccion','')"><i class="fa fa-circle-o"></i> Aprobación</a></li>
                  <li><a href="#" onclick="getUrl('construccion','')"><i class="fa fa-circle-o"></i> Historial</a></li>-->

                </ul>
              </li>
              @endif
              @if(isset($menu['0604'])==1 && $menu['0604']=='1')
              <li><a href="#" onclick="getUrl('reporteGestionHumana','')"><i class="fa fa-circle-o"></i>Reportes</a></li>            
              @endif
            </ul>


          </li>
          @endif
          <!--    *********************   -->     
          @if(isset($menu['08'])==1 && $menu['08']=='1')   
          <li class="treeview">
            <a href="#">
              <i class="fa fa-clone"></i> <span>Configuración</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if(isset($menu['0801'])==1 && $menu['0801']=='1')
              <li class="active"><a href="#" onclick="getUrl('generarBackup','')"><i class="fa fa-book"></i>Generar backup</a></li>
              @endif            
              @if(isset($menu['0803'])==1 && $menu['0803']=='1')
              <li class="active"><a href="#" onclick="getUrl('registroUsuario','')"><i class="fa fa-pencil"></i>Registro de usuarios</a></li>
              @endif            
              @if(isset($menu['0806'])==1 && $menu['0806']=='1')
              <li class="active"><a href="#" onclick="getUrl('usuarioConectado','')"><i class="fa fa-pencil"></i>Usuarios conectados</a></li>
              @endif            
              @if(isset($menu['0808'])==1 && $menu['0808']=='1')
              <li class="active"><a href="#" onclick="getUrl('trazaUsuario','')"><i class="fa fa-book"></i>Trazabilidad usuarios</a></li>
              @endif            
              @if(isset($menu['0809'])==1 && $menu['0809']=='1')
              <li class="active"><a href="#" onclick="getUrl('trazaProy','')"><i class="fa fa-pencil"></i>Trazabilidad proyectos </a></li>
              @endif            
              @if(isset($menu['0810'])==1 && $menu['0810']=='1')
              <li class="active"><a href="#" onclick="getUrl('trazaGral','')"><i class="fa fa-pencil"></i>Trazabilidad general</a></li>
              @endif            
              @if(isset($menu['0811'])==1 && $menu['0811']=='1')
              <li class="active"><a href="#" onclick="getUrl('confSistema','')"><i class="fa fa-pencil"></i>Configuración de sistemas </a></li>
              @endif            
              @if(isset($menu['0813'])==1 && $menu['0813']=='1')
              <li class="active"><a href="#" onclick="getUrl('asigPermisos','')"><i class="fa fa-pencil"></i>Asignación de permisos</a></li>
              @endif
            </ul>
          </li>   
          @endif

          <!--    *********************   --> 
          @if(isset($menu['09'])==1 && $menu['09']=='1')   
          <li class="treeview">
            <a href="#">
              <i class="fa fa-clone"></i> <span>Mantenimiento del SCP</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if(isset($menu['0901'])==1 && $menu['0901']=='1')
              <li class="active"><a href="#" onclick="getUrl('alergias','')"><i class="fa fa-circle-o"></i>Alergias</a></li>
              @endif            
              @if(isset($menu['0902'])==1 && $menu['0902']=='1')
              <li class="active"><a href="#" onclick="getUrl('servicios','')"><i class="fa fa-circle-o"></i>Servicios</a></li>
              @endif            
              @if(isset($menu['0903'])==1 && $menu['0903']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaConceptoGastos','')"><i class="fa fa-circle-o"></i>Concepto gastos</a></li>
              @endif            
              @if(isset($menu['0904'])==1 && $menu['0904']=='1')      
              <li class="active"><a href="#" onclick="getUrl('listaActividades','')"><i class="fa fa-circle-o"></i> Actividades</a></li>
              @endif            
              @if(isset($menu['0905'])==1 && $menu['0905']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaEntregables','')"><i class="fa fa-circle-o"></i>Entregables</a></li>
              @endif            
              @if(isset($menu['0906'])==1 && $menu['0906']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaAreas','')"><i class="fa fa-circle-o"></i>Areas</a></li>
              @endif            
              @if(isset($menu['0907'])==1 && $menu['0907']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaCargos','')"><i class="fa fa-circle-o"></i>Cargos</a></li>
              @endif            
              @if(isset($menu['0908'])==1 && $menu['0908']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaSedes','')"><i class="fa fa-circle-o"></i>Sedes</a></li>
              @endif            
              @if(isset($menu['0909'])==1 && $menu['0909']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaFasesProyecto','')"><i class="fa fa-circle-o"></i>Fases proyecto</a></li>
              @endif            
              @if(isset($menu['0910'])==1 && $menu['0910']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaCheckList','')"><i class="fa fa-circle-o"></i> CheckList</a></li>
              @endif            
              @if(isset($menu['0911'])==1 && $menu['0911']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaAreasConocimiento','')"><i class="fa fa-circle-o"></i>Areas conocimiento</a></li>
              @endif            
              @if(isset($menu['0912'])==1 && $menu['0912']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaPais','')"><i class="fa fa-circle-o"></i>Pais</a></li>
              @endif            
              @if(isset($menu['0913'])==1 && $menu['0913']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaDisciplina','')"><i class="fa fa-circle-o"></i>Disciplina</a></li>
              @endif            
              @if(isset($menu['0914'])==1 && $menu['0914']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaContactoCargo','')"><i class="fa fa-circle-o"></i>Contacto cargo</a></li>
              @endif            
              @if(isset($menu['0915'])==1 && $menu['0915']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaInstitucion','')"><i class="fa fa-circle-o"></i>Institución</a></li>
              @endif            
              @if(isset($menu['0916'])==1 && $menu['0916']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaTiposContacto','')"><i class="fa fa-circle-o"></i>Tipos contacto</a></li>
              @endif            
              @if(isset($menu['0917'])==1 && $menu['0917']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaTiposInformacionContacto','')"><i class="fa fa-circle-o"></i>Tipos información contacto</a></li>
              @endif            
              @if(isset($menu['0918'])==1 && $menu['0918']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaOficina','')"><i class="fa fa-circle-o"></i> Oficina</a></li>
              @endif            
              @if(isset($menu['0919'])==1 && $menu['0919']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaTiposNoFacturables','')"><i class="fa fa-circle-o"></i>Tipos no facturables</a></li>
              @endif            
              @if(isset($menu['0920'])==1 && $menu['0920']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaTiposEntregables','')"><i class="fa fa-circle-o"></i>Tipos entregables</a></li>
              @endif            
            <!--  @if(isset($menu['0921'])==1 && $menu['0921']=='1')-->
              <li class="active"><a href="#" onclick="getUrl('catalogoTablas','')"><i class="fa fa-circle-o"></i>Catálogo tablas</a></li>
             <!-- @endif  -->          
              @if(isset($menu['0922'])==1 && $menu['0922']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaEnfermedad','')"><i class="fa fa-circle-o"></i>Enfermedad</a></li>
              @endif            
              @if(isset($menu['0923'])==1 && $menu['0923']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaEspecialidad','')"><i class="fa fa-circle-o"></i>Especialidad</a></li>
              @endif            
              @if(isset($menu['0924'])==1 && $menu['0924']=='1')
              <li class="active"><a href="#" onclick="getUrl('catalogoGrupoTablas','')"><i class="fa fa-circle-o"></i>Catálogo grupo tablas</a></li>
              @endif            
              @if(isset($menu['0925'])==1 && $menu['0925']=='1')
              <li class="active"><a href="#" onclick="getUrl('listaMonedaCambio','')"><i class="fa fa-circle-o"></i>Moneda cambio</a></li>
              @endif
            </ul>


          </li>             
          @endif
          <br>
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
          <!-- /.sidebar -->
          <li class="treeview active">

            <div id="divCalendario">    </div>

          </li>
          <br>
          <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
          <li class="treeview active">
            <div >
              <h5 style="text-align: center;"><a data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">Leyenda</a></h5>
              <div class="collapse" id="collapseExample">
                <b><input type="text" style="background-color: #FFBF00; width: 100%; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="Horas planificadas" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="Horas planificadas" disabled></b><br>
                <b><input type="text" style="background-color: #CD201A; width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="Horas no enviadas u observadas" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="Horas no enviadas u observadas" disabled></b><br>
                <b><input type="text" style="background-color: #0069AA; width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="Horas enviadas" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="Horas enviadas" disabled></b><br>
                <b><input type="text" style="background-color: #11B15B; width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;" value="Horas aprobadas" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="Horas aprobadas" disabled></b><br>
                <b><input type="text" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;; background-color:green; " value="% de avance menor a 60" data-toggle="tooltip" data-placement="right" title="Porcentaje de avance menor a 60" disabled></b><br>
                <b><input type="text" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;; background-color:orange; " value="% de avance menor a 80" data-toggle="tooltip" data-placement="right" title="Porcentaje de avance menor a 80" disabled></b><br>
                <b><input type="text" class="progress-bar progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100% ; color: white; font-size: 11pt; font-family: arial; text-align: left;; background-color:red; " value="% de avance mayor a 80" data-toggle="tooltip" data-placement="right" title="Porcentaje de avance mayor a 80" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="AND = Anddes" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="AND = Anddes" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="NF = No Facturable" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="NF = No Facturable" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="FF = Facturable" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="FF = Facturable" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="EJE = Ejecutado" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="EJE = Ejecutado" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="VJI = Validación Jefe Inmediato" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="VJI = Validación Jefe Inmediato" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="RGP = Revisor GP" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="RGP = Revisor GP" disabled></b><br>
                <b><input type="text" style="background-color: #E0E0E0; width: 100%; color: black; font-size: 11pt; font-family: arial; text-align: left;" value="APR = Aprobado" class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" data-toggle="tooltip" data-placement="right" title="APR = Aprobado" disabled></b><br>
                <br><br><br><br>
              </div>
            </div>
          </li>
          <div class="col-lg-12 col-xs-12" style="background-color:#0069AA; max-height:1px;"></div>




        </ul>



      </section>



      <input type="hidden" id="my_hidden_input">
    </aside>