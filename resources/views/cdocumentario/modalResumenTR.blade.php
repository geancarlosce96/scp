      <div class="modal fade" id="modalResumenTR" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header modal-header-primary">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="ObsModalLabel">Resumen de TR</h4>
            </div>
            <div class="modal-body">
            {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmResumenTR')) !!}
                {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto_rtr')) !!}
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_rtr')) !!}             
                <div class="form-group">
                @if(isset($documentos)==1)
                    @foreach($documentos as $doc)
                        <div class="alert alert-info" role="alert">{{ $doc['codigo'] }} - {{ $doc['descripcion'] }}</div>
                        <?php $listTr = $doc['trs']; ?>
                        <table class="table table-bordered table-hover"> 
                        <thead> 
                        <tr> 
                            <th>#</th> 
                            <th>Transmittal</th> 
                            <th>Fecha</th>
                            <th>Revision</th>
                            <th ></th>
                        </tr> 
                        </thead> 
                        @if(isset($listTr))
                            <tbody> 
                                @foreach($listTr as $t)
                                    <tr>
                                    <td>{{ $t['item'] }}</td> 
                                    <td>{{ $t['nrotrasmittal'] }}</td> 
                                    <td>{{ $t['fecha'] }}</td>
                                    <td>{{ $t['revision'] }}</td>
                                    <td ></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        @endif
                        </table>
                    @endforeach
                @endif
                </div>
            {!! Form::close() !!}
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            <!--<button type="button" class="btn btn-primary" onclick="saveObs()">Aceptar</button>-->
            </div>
          </div>
        </div>
      </div>