<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario
        <small>Planificación de Control Documentario</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">
    <div class="row">
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
    <div class="col-lg-9 col-xs-9 clsPadding">{!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}</div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!} </div> 
    <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
   </div>    
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div> 
    <div class="row clsPadding2">
        <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
        <div class="col-lg-9 col-xs-9">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}   
        </div>	 
    </div>     
    <div class="row clsPadding2">
        <div class="col-lg-4 col-xs-4">
        <label class="clsTxtNormal">Fecha Inicio:</label>
        {!! Form::text('finicio',(isset($proyecto->finicio)?$proyecto->finicio:''), array('class'=>'form-control pull-right datepicker')) !!}</div> 
        <div class="col-lg-4 col-xs-4">
        <label class="clsTxtNormal">Fecha Cierre:</label>
        {!! Form::text('fcierre',(isset($proyecto->fcierre)?$proyecto->fcierre:''), array('class'=>'form-control pull-right datepicker')) !!}</div>
        <div class="col-lg-4 col-xs-4">
            <label class="clsTxtNormal">Estado:</label>
            {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control')) !!}
        </div>        
        
    </div>
    <div class="row clsPadding2">
        <div class="col-lg-4 col-xs-4 clsPadding6">
            <div class="form-group">
              <label>Documentos:</label>
              <select multiple="" class="form-control">
                <option>Técnico</option>
                <option>Gestión</option>
                <option>Comunicación</option>
              </select>
            </div>        
        
        </div>     
        <div class="col-lg-4 col-xs-4">
       		<label class="clsTxtNormal">Revisión:</label>
              <select class="form-control" >
                <option>A</option>
                <option>B</option>
                <option>C</option>
              </select>
        </div>  

        <div class="col-lg-4 col-xs-4"></div>          
    </div>  
   
  

<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
   Detalle (Resumen TR):
    </div>
   
</div>
<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
      	<thead>
	        <tr>
            	<td colspan="10"></td>
                <td colspan="7" style="background-color:#D6E180; text-align:center;">ANTERIOR</td>
                <td colspan="7" style="background-color:#FDE86F; text-align:center;">ACTUAL</td>
                <td colspan="7" style="background-color:#669BD0; text-align:center;">PRÓXIMA</td>
            </tr>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">EDT</td>
                <td class="clsAnchoTabla">Disciplina</td>
                <td class="clsAnchoTablaMax">Cod. Anddes</td>
                <td class="clsAnchoTablaMax">Cod. Cliente</td>
                <td class="clsAnchoTablaMax">Descripción</td>
                <td class="clsAnchoTabla">FA</td>
                <td class="clsAnchoTabla">FB</td>
                <td class="clsAnchoTabla">F0</td>
                <td class="clsAnchoTabla">Rev.</td>
				<td class="clsAnchoDiaA">01-07</td> 
                <td class="clsAnchoDiaA">02-07</td>  
                <td class="clsAnchoDiaA">03-07</td>  
                <td class="clsAnchoDiaA">04-07</td>  
                <td class="clsAnchoDiaA">05-07</td>  
                <td class="clsAnchoDiaA">06-07</td>  
                <td class="clsAnchoDiaA">07-07</td> 
				<td class="clsAnchoDiaB">08-07</td> 
                <td class="clsAnchoDiaB">09-07</td>  
                <td class="clsAnchoDiaB">10-07</td>  
                <td class="clsAnchoDiaB">11-07</td>  
                <td class="clsAnchoDiaB">12-07</td>  
                <td class="clsAnchoDiaB">13-07</td>  
                <td class="clsAnchoDiaB">14-07</td>
				<td class="clsAnchoDiaC">15-07</td> 
                <td class="clsAnchoDiaC">16-07</td>  
                <td class="clsAnchoDiaC">17-07</td>  
                <td class="clsAnchoDiaC">18-07</td>  
                <td class="clsAnchoDiaC">19-07</td>  
                <td class="clsAnchoDiaC">20-07</td>  
                <td class="clsAnchoDiaC">21-07</td>                                                
                
            </tr>
        </thead>

      	<tbody>
        <tr>
            	<td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>   
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
             </tr>
        <tr>
            	<td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>   
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
             </tr>
        <tr>
            	<td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTablaMax"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>   
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>   
                <td class="clsAnchoDia"></td>  
                <td class="clsAnchoDia"></td>
				<td class="clsAnchoDia"></td> 
             </tr>                                                                              
        </tbody>
      </table>    
    </div>
</div>

<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-6 clsPadding">
        <a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> </div> 
    <div class="col-lg-3 col-xs-6 clsPadding">
              <select class="form-control" >
                <option>PDF</option>
                <option>XLS</option>
              </select>
         </div> 
     <div class="col-lg-6 hidden-xs clsPadding"></div>                       
</div>





<!--:-->

    

        
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 






</div>
    {!! Form::close() !!}
    </section>
    @include('partials.searchProyecto')
    <!-- /.content -->
  </div>
  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
     

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarplanificacionCD/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });           

          



         $('#frmproyecto').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            

            $.ajax({

                type:"POST",
                url:'grabarplanificacionCD',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });  

</script>  