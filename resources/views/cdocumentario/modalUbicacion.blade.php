<div id="modalUbica" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ubicación de Proyecto </h4>
            </div>
            <div class="modal-body">
            {!! Form::open(
                array(
                    'route' => 'saveUbicacion', 
                    'id' => 'frmubica'
                    )) !!}
                    {!! Form::hidden('cproyecto',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
                    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
                    {!! Form::select('ubicacion',(isset($tubicacion)==1?$tubicacion:array()), (isset($adicional->ubicacion)==1?$adicional->ubicacion:'') ,array('class' => 'form-control select-box', 'id'=>'ubicacion')) !!}    
            {!! Form::close() !!} 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" onclick="saveUbica()" >Grabar</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
            </div>
        </div>

    </div>
</div>