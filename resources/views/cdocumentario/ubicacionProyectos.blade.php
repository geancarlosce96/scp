<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario / Ubicación
        <small>Inicio</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<!-- ********** -->
        <div class="row">
            <!--<div class="col-lg-1 hidden-xs"></div>-->
            <!-- ********** -->
            <div class="col-md-12 col-xs-12">


            
            
        <div class="row">
            
        
                            
                            <div class="row">
                                <div class="col-md-12 col-xs-12 clsTitulo">
                                    Carga Laboral CD
                                </div>
                            </div>

                            <div class="row clsPadding2">	
                                <div class="col-lg-12 col-xs-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td class="clsCabereraTabla">Ubicación</td>
                                        <td class="clsCabereraTabla">&nbsp;</td>
                                        <td class="clsCabereraTabla">En Proceso</td>
                                        <td class="clsCabereraTabla">Stand by</td>
                                        <td class="clsCabereraTabla">Cancelado</td>
                                        <td class="clsCabereraTabla">Cerrado</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(isset($listUbi)==1)
                                    @foreach($listUbi as $ubi)
                                    <tr>
                                        <td class="clsAnchoDia">{{ $ubi['ubi'] }}</td>
                                        <td class="clsAnchoDia">{{ $ubi['cubi'] }}</td>
                                        <td class="clsAnchoDia"></td>
                                        <td class="clsAnchoDia"></td>
                                        <td class="clsAnchoDia"></td>
                                        <td class="clsAnchoDia"></td>         
                                    </tr>
                                    @endforeach
                                    @endif                                            
                                    </tbody>
                                    <tfoot>
                                        <tr class="clsCabereraSubItem">
                                            <td>SubTotal</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                            <td>0</td>
                                        </tr>
                                    </tfoot>
                                    </table>
                                </div>
                            </div>
                            
                            <div class="row clsPadding2">
                                <div class="col-lg-2 col-xs-2">
                                    <button type="button" class="btn btn-primary btn-block " id="btnexpor"> <b>Exportar</b></button>
                                </div>
                                <div class="col-lg-2 col-xs-2">
                                    <button type="button" class="btn btn-primary btn-block " id="btnprint"> <b>Imprimir</b></button>
                                </div> 
                                <div class="col-lg-4 col-xs-4">
                                <select class="form-control">
                                    <option value="pdf">PDF</option>
                                    <option value="xls">XLS</option>
                                </select>  
                                </div>
                                <div class="col-lg-4 col-xs-4"></div>

                                
                            </div>  
                            
                            
                            <!-- Cuadro estadistico -->
                            <div class="row clsPadding2">
                                <!--<div id="chartdiv" style="width: 600px; height: 400px;"></div>-->
                                <div id="chartdiv" style="width: 800px; height: 400px;"></div>
                            </div>








            
            </div>
            <!-- ********** -->
            <!--<div class="col-lg-1 hidden-xs"></div>-->
        </div>
<!-- ********** -->
    </section>
    <!-- /.content -->
  </div>
  <script>
        $("#btnexpor").on('click',function(e){
            alert('1');
        });
        $("#btnprint").on('click',function(e){
            alert('2');
        });        
  </script>