<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario
        <small>Lista de Entregables ANDDES</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
        <div class="col-lg-12 col-xs-12">
            <div class="row">
                <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
                <div class="col-lg-9 col-xs-9 clsPadding">{!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}</div>
                <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-sm" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
                <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
                <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!}</div> 
                <div class="col-lg-2 col-xs-2 clsPadding">Código Proyecto:</div>
                <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
            </div>    
            <div class="row ">
                <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
            </div>
            <div class="row ">
                <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
                <div class="col-lg-9 col-xs-9">
                    {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!}
                </div>	 
            </div> 
            <div class="row ">
                <div class="col-lg-3 col-xs-6">
                <label class="clsTxtNormal">Fecha Inicio:</label>
                {!! Form::text('finicio',(isset($proyecto->finicio)?$proyecto->finicio:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}</div> 
                <div class="col-lg-3 col-xs-6">
                <label class="clsTxtNormal">Fecha de Cierre:</label>
                {!! Form::text('fcierre',(isset($proyecto->fcierre)?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}</div>
                <div class="col-lg-3 col-xs-6">
                    <label class="clsTxtNormal">Estado:</label>
                    {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!}
                </div>   
                <div class="col-lg-3 col-xs-6">
                    <!--<label class="clsTxtNormal">Documentos técnicos:</label>
                    <select class="form-control" >
                        <option>Técnicos</option>
                        <option>Gestión</option>
                        <option>Comunicación</option>
                    </select>-->
                </div>                
            </div>  
            
            <div class="row ">
                <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
 
            <div class="row ">	
                <div class="col-lg-12 col-xs-12 table-responsive" id="divEntregable">
                    @include('partials.tableListaEntregablesDocumentario',array('proyectoentregables'=> (isset($proyectoentregables)==1?$proyectoentregables:null) ))
                </div>
            </div>

            
        
            <div class="row ">
                <div class="col-lg-2 col-xs-6 ">
                    <button type="submit"  </class="btn btn-default btn-block"><b>Grabar</b></button> </div>            
                <div class="col-lg-2 col-xs-6 ">
                    <button type='button' class="btn btn-info btn-block" id="btnResumen"><b>Resumen TR</b></button> </div>
                <div class="col-lg-2 col-xs-6 ">
                    <button type='button' class="btn btn-warning btn-block" id="btnAgregarTR"><b>Agregar a TR</b></button> </div> 
                <div class="col-lg-3 col-xs-6 ">
                    <a href="#" class="btn btn-primary btn-block" disabled="true"><b>Imprimir</b></a> </div> 
                <div class="col-lg-3 col-xs-6 ">
                        <select class="form-control" >
                            <option value="pdf">PDF</option>
                            <option value="xls">XLS</option>
                        </select>
                    </div>                         
            </div>



            
        </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 
    </div>
    {!! Form::close() !!}
    </section>
    @include('partials.searchProyecto')
    <div id="divResumenTR">
    <!-- ********************Modal  ******************************* -->
    @include('cdocumentario.modalResumenTR',array('proyecto'=> (isset($proyecto)==1?$proyecto:null) ))


    <!-- *************************************************** -->   
    </div>     
    <!-- /.content -->
  </div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
     

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarListaEntregableCD/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

         $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });           

          



         $('#frmproyecto').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            

            $.ajax({

                type:"POST",
                url:'grabarListaEntregableCD',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });  

        $("#btnResumen").on("click",function(e){
            //divResumenTR
            //modalResumenTR
            


            $.ajax({

                type:"POST",
                url:'viewResumenTR',
                data:$("#frmproyecto").serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#divResumenTR").html(data);
                     $("#modalResumenTR").modal("show");
                     //$("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });            
        });

        $("#btnAgregarTR").on("click",function(e){
            $.ajax({

                type:"POST",
                url:'agregaraTR',
                data:$("#frmproyecto").serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     
                     //$("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show();
                }
            });              
        });

</script>  