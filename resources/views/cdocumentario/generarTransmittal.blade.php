<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario
        <small>Generación de Transmittal de CD</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => 'grabarGenTransmittal','method' => 'POST','id' =>'frmpropuesta')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    <div class="row">
        {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:'')) !!}
        
    <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
    <div class="col-lg-9 col-xs-9 clsPadding">{!! Form::text('nombrecliente',(isset($persona->nombre)?$persona->nombre:''),array('class'=>'form-control input-sm','disabled' => 'true')) !!}</div>
    <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchPropuesta'><b>...</b></button></div>
    <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('nombreuminera',(isset($uminera->nombre)==1?$uminera->nombre:''),array('disabled'=>'','class'=> 'form-control input-sm','placeholder'=>'Nombre Unidad Minera')) !!}</div> 
    <div class="col-lg-2 col-xs-2 clsPadding">Código Propuesta:</div>
    <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)?$propuesta->ccodigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!} </div>
   </div>    
    <div class="row clsPadding">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div> 

    <div class="row clsPadding2">
        <div class="col-lg-4 col-xs-4">
       		<label class="clsTxtNormal">Transmittal ANDDES:</label>
        	<input class="form-control input-sm" type="text">
        </div>  
        <div class="col-lg-3 col-xs-3 clsPadding2" style="margin-top:15px;">
        <a href="#" class="btn btn-primary btn-block "> <b>Buscar TR Anterior</b></a>
        </div> 
        <div class="col-lg-5 col-xs-5"></div>          
    </div>  
    <div class="row clsPadding2">
        <div class="col-lg-4 col-xs-4">
        	<label class="clsTxtNormal">Transmittal CLIENTE:</label>
        	<input class="form-control input-sm" type="text">
        </div>  
        <div class="col-lg-4 col-xs-4">
        	<label class="clsTxtNormal">CDocumentario:</label>
        	<input class="form-control input-sm" type="text">        
        
        </div> 
        <div class="col-lg-4 col-xs-4">
        	<label class="clsTxtNormal">Tipo de Envio::</label>
              {!! Form::select('tipoenvio',(isset($ttipoenvio)==1?$ttipoenvio:array()),(isset($configuracion->tipoenvio )==1?$configuracion->tipoenvio :''),array('class' => 'form-control')) !!}             
        
        </div>          
    </div>     
  
  
  
  
  
  
  
  
  

<div class="row clsPadding2">
    <div class="col-lg-9 col-xs-9 clsTitulo">
   Contactos:
    </div>
    <div class="col-lg-2 col-xs-2">Agregar Contacto:</div>
    <div class="col-lg-1 col-xs-1"><a href="#" class="btn btn-primary btn-block "> <b>+</b></a></div>    
</div>


<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
      	<thead>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoTabla">Nombre</td>
                <td class="clsAnchoTabla">Area</td>
                <td class="clsAnchoTabla">Correo</td>
                <td class="clsAnchoTabla">Cargo</td>
                <td class="clsAnchoTabla">Tipo de Contacto</td>
                <td class="clsAnchoTabla">Principal</td>
                <td class="clsAnchoTabla">CC</td>
                <td class="clsAnchoTabla">Acciones</td>
            </tr>
        </thead>

      	<tbody>
        <tr>
            <td class="clsAnchoTabla">Carlos Cabrera</td>
            <td class="clsAnchoTabla">Control D</td>
        	<td class="clsAnchoTabla">lcar@yahoo.es</td>
            <td class="clsAnchoTabla">LIMA</td>
            <td class="clsAnchoTabla">Control D</td>
        	<td class="clsAnchoTabla">lcar@yahoo.es</td>
            <td class="clsAnchoTabla">LIMA</td>            
            <td class="clsAnchoTabla">
            	<a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Contacto">
                <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
            </td>           
        </tr> 
        <tr>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>            
            <td class="clsAnchoTabla">
            	<a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Contacto">
                <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
            </td>           
        </tr>
        <tr>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>            
            <td class="clsAnchoTabla">
            	<a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Contacto">
                <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
            </td>           
        </tr>                    
        </tbody>
      </table>    
    </div>
</div>

<div class="row clsPadding2">
    <div class="col-lg-9 col-xs-9 clsTitulo">
   Detalle (Resumen TR):
    </div>
    <div class="col-lg-2 col-xs-2">Agregar Detalle:</div>
    <div class="col-lg-1 col-xs-1"><a href="#" class="btn btn-primary btn-block "> <b>+</b></a></div>    
</div>
<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
      	<thead>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">Cod. Cliente</td>
                <td class="clsAnchoTabla">Descripción</td>
                <td class="clsAnchoTabla">Rev.</td>
                <td class="clsAnchoTabla">Tipo Cantidad</td>
                <td class="clsAnchoTabla">Acciones</td>
            </tr>
        </thead>

      	<tbody>
        <tr>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"  style="max-width:100px !important;">
				<div class="input-group">
                <input id="new-event" type="text" class="form-control" placeholder="">
                <div class="input-group-btn">
                  <button id="add-new-event" type="button" class="btn btn-primary btn-flat">R</button>
                </div>
              </div> 
            </td>
            <td class="clsAnchoTabla"></td>         
            <td class="clsAnchoTabla">
            	<a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Detalle">
                <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
            </td>           
        </tr>  
        <tr>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"  style="max-width:100px !important;">
				<div class="input-group">
                <input id="new-event" type="text" class="form-control" placeholder="">
                <div class="input-group-btn">
                  <button id="add-new-event" type="button" class="btn btn-primary btn-flat">R</button>
                </div>
              </div> 
            </td>
            <td class="clsAnchoTabla"></td>         
            <td class="clsAnchoTabla">
            	<a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Detalle">
                <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
            </td>           
        </tr> 
        <tr>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"  style="max-width:100px !important;">
				<div class="input-group">
                <input id="new-event" type="text" class="form-control" placeholder="">
                <div class="input-group-btn">
                  <button id="add-new-event" type="button" class="btn btn-primary btn-flat">R</button>
                </div>
              </div> 
            </td>
            <td class="clsAnchoTabla"></td>         
            <td class="clsAnchoTabla">
            	<a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Detalle">
                <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
            </td>           
        </tr>                         
        </tbody>
      </table>    
    </div>
</div>







<!--:-->

    
 
<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-12 clsPadding">
        
        <button class="btn btn-primary btn-block" style="width:200px;" ><b>Emitir TR</b></button>
    </div>
</div>
        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>

    </section>
    <!-- /.content -->
    <!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuestas</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuestas" class="table">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Cliente</th>
                  <th>Unidad Minera</th>
                  <th>Propuesta</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal de Buscar Propuesta-->

  </div>

  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
        /* buscar Propuestas */
        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';

        var table=$("#tPropuestas").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarPropuestas",
                "type": "GET",
            },
            "columns":[
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });


        $('#tPropuestas tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            var table = $('#tPropuestas').DataTable();
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        /* Fin Buscar Propuestas*/

        /*
        * @Descripcion 
        * @param
        * @return
        */
        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              //alert(id.substring(4));
              //$('#searchPropuesta').modal("hide");

              getUrl('editarGenTransmittal/'+id.substring(4),'');
              
              //alert(id.substring(4));
              
          }else{
              $('.alert').show();
          }
        }   
        $('#searchPropuesta').on('hidden.bs.modal', function () {
            
           goEditar();
        });
        /* Fin de Buscar Propuestas*/   


        
        $("#frmpropuesta").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarGenTransmittal',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });   
</script>