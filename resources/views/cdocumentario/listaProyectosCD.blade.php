<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario
        <small>Lista de Proyectos</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
  
      {!! Form::open(array('url' => 'buscarListaJefe','method' => 'POST','id' =>'frmlista')) !!}
      {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}

      <div class="row ">
          <div class="col-lg-12 col-xs-12 clsTitulo">
            Listar Proyectos
          </div>  
      </div>
      <div class="row ">	
          <div class="col-lg-4 col-xs-4">
            <label class="clsTxtNormal">Buscar:</label>
            <div class="input-group">
            
            <span class="input-group-addon"><i class="fa fa-search"></i></span>
                {!! Form::select('cproyecto',(isset($proyecto)==1?$proyecto:array()),'',array('class' => 'form-control select-box', 'id'=>'cproyecto')) !!}    
            </div>    
          </div>
        <div class="col-lg-4 col-xs-4">
              <div class="form-group">
                  <label class="clsTxtNormal">De:</label>
              
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="finicio" class="form-control pull-right" id="finicio">
                  </div>
                  <!-- /.input group -->
            </div>    	
        </div>    
        <div class="col-lg-4 col-xs-4">
              <div class="form-group">
                  <label class="clsTxtNormal">A:</label>
              
                  <div class="input-group date">
                    <div class="input-group-addon">
                      <i class="fa fa-calendar"></i>
                    </div>
                    <input type="text" name="ffin" class="form-control pull-right" id="ffin">
                  </div>
                  <!-- /.input group -->
            </div> 
        </div>        
      </div>
      <div class="row ">	
          <div class="col-lg-6 col-xs-6">
            <label class="clsTxtNormal">Estado:</label>
            {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),'',array('class' => 'form-control select-box', 'id'=>'estadoproyecto')) !!}    
          </div>
          <div class="col-lg-6 col-xs-6">
            <label class="clsTxtNormal">G.P.:</label>
              {!! Form::select('gteproyecto',(isset($personal_gp)==1?$personal_gp:null),'',array('class' => 'form-control select-box','id'=>'gteproyecto')) !!}     
          </div>
          
      </div>    
      <div class="row ">	
          <div class="col-lg-6 col-xs-6">
            <button type="submit" class="btn btn-primary btn-block" style="width:80px" >Buscar</button>
          </div>
          <div class="col-lg-6 col-xs-6">
          </div>          
     </div>




      <!-- Tabla de Resultados -->
      <div class="row clsPadding2">	
          <div class="col-lg-12 col-xs-12 table-responsive" id="divLista">
            @include('partials.tableListaProyectosCD',array('listaProy'=>(isset($listaProy)==1?$listaProy:array() ) ))   
          </div>
      </div>
      <!--  Fin de tabla de resultados-->

    
      <!-- botones -->
      <div class="row ">
          <div class="col-md-2 col-xs-12 clsPadding">
              <button type="button" class="btn btn-primary btn-block" id="btnSave"> <b>Grabar</b></button> </div>      
          <div class="col-md-2 col-xs-12 clsPadding">
              <button type="button" class="btn btn-primary btn-block"><b>Exportar</b></button> </div>
          <div class="col-md-2 col-xs-12 clsPadding">
              <button type="button" class="btn btn-primary btn-block"><b>Imprimir</b></button> </div>
          <div class="col-md-2 col-xs-12 clsPadding">
                    <select class="form-control" >
                      <option value="pdf">PDF</option>
                      <option value="xls">XLS</option>
                    </select>  
          </div>     
          <div class="col-md-4 col-xs-12 clsPadding">
          </div>

      </div>
            {!! Form::close() !!}
    </div> <!-- FIN DIV col-xs-12 -->
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div><!-- Fin Div row-->
    </section>
    <!-- /.content -->

       <!-- Modal Agregar Ubicacion de Pry-->
     <div id="divUbica">
        @include('cdocumentario.modalUbicacion')
    </div>

    <!-- Fin Modal Agregar  Ubicacion de Pry-->

</div>

<script>


        $('#frmlista').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);  
            $.ajax({

                type:"POST",
                url:'buscarListaJefe',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#divLista").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show(); 
                }                

            });          
        });   
        $('#btnSave').on('click',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);  
            $.ajax({

                type:"POST",
                url:'grabarListaJefe',
                data:$("#frmlista").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#divLista").html(data);
                     //$("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide();
                    //$('#detalle_error').html(data);
                    //$("#div_msg_error").show(); 
                }                

            });          
        }); 
        $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#ffin').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 
      

        $('.select-box').chosen(
        {
            allow_single_deselect: true
        });
        function viewUbica(cproyecto){
            $("#cproyecto").val(cproyecto);
            


            $.ajax({
              url: 'viewUbicacion',
              type: 'POST',
              data: {
                  "_token":"{{ csrf_token() }}",
                  cproyecto: cproyecto
              },
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){

                $("#divUbica").html(data);
                $("#modalUbica").modal("show");                
                $('#div_carga').hide(); 
              },

          });
        }        
        function saveUbica(){

            $("#modalUbica").modal("hide");                


            $.ajax({
              url: 'saveUbicacion',
              type: 'POST',
              data: $("#frmubica").serialize(),
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){

                //$("#divUbica").html(data);
                
                $('#div_carga').hide(); 
              },

          });
        }                
</script>