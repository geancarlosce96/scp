<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario
        <small>Generación de Transmittal de CD</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        
        {!! Form::hidden('ctransmittalejecucion',(isset($transmittalejecucion)==1?$transmittalejecucion->ctransmittalejecucion:''),array('id'=>'ctransmittalejecucion')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
        <!--<div class="col-lg-1 hidden-xs"></div> -->
    <div class="col-lg-12 col-xs-12">
        <div class="row">
            <div class="col-lg-2 col-xs-2 ">Cliente:</div>
            <div class="col-lg-9 col-xs-9 ">{!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}</div>
            <div class="col-lg-1 col-xs-1 "><button type='button' class="btn btn-primary  btn-sm" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
            <div class="col-lg-2 col-xs-2 ">Unidad minera:</div>
            <div class="col-lg-4 col-xs-4 ">{!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!} </div> 
            <div class="col-lg-2 col-xs-2 ">Código Proyecto:</div>
            <div class="col-lg-4 col-xs-4 ">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
        </div>    
        <div class="row">
            <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
        </div> 
        <div class="row ">
            <div class="col-lg-4 col-xs-4">
                <label class="clsTxtNormal">Fecha Inicio:</label>
                {!! Form::text('finicio',(isset($proyecto->finicio)?$proyecto->finicio:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}
            </div> 
            <div class="col-lg-4 col-xs-4">
                <label class="clsTxtNormal">Fecha Cierre:</label>
                {!! Form::text('fcierre',(isset($proyecto->fcierre)?$proyecto->fcierre:''), array('class'=>'form-control pull-right','disabled'=>'true')) !!}
            </div>
            <div class="col-lg-4 col-xs-4">
                <label class="clsTxtNormal">Estado:</label>
                {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control','disabled'=>'true')) !!} 
            </div>        
        </div>
        <div class="row ">
            <div class="col-lg-4 col-xs-4">
                <label class="clsTxtNormal">Transmittal ANDDES:</label>
                <div class="input-group">
                    <span class="input-group-btn">
                        <span class="input-group-addon" id="basic-addon2">{{(isset($configuracion->numero)==1?$configuracion->numero:'') }}</span>
                    </span>            
                    <input type="text" name="secuencia" id="secuencia" value="{{(isset($numero_sec)==1?$numero_sec:'') }}" placeholder="Secuencia"   class="form-control input-sm" style="">
                    <button type="button" class="btn btn-primary btn-block btn-xs" data-toggle='modal' data-target='#searchTR'>Buscar Transmittal</button>
                </div>
            </div>  
            <div class="col-lg-4 col-xs-4">
                <label class="clsTxtNormal">CDocumentario:</label>
                    {!! Form::select('personal_cd',(isset($personal_cd)==1?$personal_cd:array()),(isset($per_cdocu)==1?$per_cdocu:''),array('class' => 'form-control select-box')) !!}                  
            
            </div> 
            <div class="col-lg-4 col-xs-4">
                <label class="clsTxtNormal">Tipo de Envio::</label>
                    {!! Form::select('tipoenvio',(isset($tipoenvio)==1?$tipoenvio:array()),(isset($transmittalejecucion->tipoenvio)==1?$transmittalejecucion->tipoenvio:''),array('class' => 'form-control select-box','id'=>'tipoenvio')) !!}            
            
            </div>          
        </div>  
    
  
  
  

        <div class="row ">
            <div class="col-lg-9 col-xs-9 clsTitulo">
        Contactos:
            </div>
            <div class="col-lg-2 col-xs-2 clsTitulo">&nbsp;</div>
            <div class="col-lg-1 col-xs-1 clsTitulo">&nbsp;</div>    
        </div>


        <div class="row ">	
            <div class="col-lg-12 col-xs-12 table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr class="clsCabereraTabla">
                        <td class="clsAnchoTabla">Nombre</td>
                        <!--<td class="clsAnchoTabla">Area</td>-->
                        <td class="clsAnchoTabla">Correo</td>
                        <td class="clsAnchoTabla">Cargo</td>
                        <td class="clsAnchoTabla">Tipo de Contacto</td>
                        <!--<td class="clsAnchoTabla">Principal</td>
                        <td class="clsAnchoTabla">CC</td>-->
                    <!--<td class="clsAnchoTabla">Acciones</td>-->
                    </tr>
                </thead>

                <tbody>
                @if(isset($transconfigcontac))
                @foreach($transconfigcontac as $con)
                <tr>
                    <td class="clsAnchoTabla">{{ $con['apellidos'] }} {{ $con['nombres'] }}</td>
                    <!--<td class="clsAnchoTabla">Control D</td>-->
                    <td class="clsAnchoTabla">{{ $con['email'] }}</td>
                    <td class="clsAnchoTabla">{{ $con['ccontactocargo'] }}</td>
                    <td class="clsAnchoTabla">{{ $con['contacto']}}</td>
                    <!--<td class="clsAnchoTabla">lcar@yahoo.es</td>
                    <td class="clsAnchoTabla">LIMA</td>  -->          
                    <!--<td class="clsAnchoTabla">
                        <a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Contacto">
                        <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
                    </td>       -->    
                </tr> 
                @endforeach
                @endif          
                </tbody>
            </table>    
            </div>
        </div>

        <div class="row ">
            <div class="col-lg-9 col-xs-9 clsTitulo">
                Detalle (Resumen TR):
            </div>
            <div class="col-lg-2 col-xs-2">Agregar Detalle:</div>
            <div class="col-lg-1 col-xs-1"><button type="button" class="btn btn-primary btn-block btn-xs" id="btnDeta"> <b>+</b></button></div>    
        </div>
        <div class="row ">	
            <div class="col-lg-12 col-xs-12 table-responsive" id="divTable">

            @include('partials.tableDetalleGenTransmittal',array('ejecu_deta'=> (isset($ejecu_deta)==1?$ejecu_deta:array()) ))    
            </div>
        </div>

  
 
        <div class="row ">
            <div class="col-md-3 col-xs-12">
                <button type="submit" class="btn btn-primary btn-block btn-sm" style="width:80px" id="btnSave"><b>Grabar TR</b></button> 
            </div>        
            <div class="col-md-3 col-xs-12">
                <button type="button" class="btn btn-primary btn-block btn-sm" style="width:80px" id="btnEmitir"><b>Emitir TR</b></button> 
            </div>
            <div class="col-md-3 col-xs-12">
                <button type="button" class="btn btn-primary btn-block btn-sm" style="width:80px" id="btnPrevio"><b>Vista Previa</b></button> 
            </div>            
            <div class="col-md-3 col-xs-12">
                
            </div>            
        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
    {!! Form::close() !!}
    </section>
    @include('partials.searchProyecto')
    @include('partials.searchTransmittal')

    <div id="divDetalle">
        {!! Form::open(array('url' => 'saveItemGeneraTR','method' => 'POST','id' =>'frmdeta','class'=>'form-horizontal')) !!}
            @include('partials.modalAddEditGenTR')
        {!! Form::close()!!}
    </div>
    <!-- /.content -->
  </div>

    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <script>

        var selected =[];
        var selected_tr =[];
        $.fn.dataTable.ext.errMode = 'throw';
     

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarGeneracionTR/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

        /* Buscar Transmittal*/
        var table=$("#tTransmittal").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarTR",
                "type": "GET",
            },
            "columns":[
                {data : 'codigo', name: 'pry.codigo'},
                {data : 'proyecto' , name : 'pry.nombre'},
                {data : 'numero', name: 'conf.numero'},
                {data : 'secuencia' , name : 'eje.codigo'}
                

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tTransmittal tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected_tr);
            table.$('tr.selected').removeClass('selected');
            selected_tr.splice(0,selected_tr.length);             
            if ( index === -1 ) {
                selected_tr.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchTR').on('hidden.bs.modal', function () {
           goEditarTR();
        });   

        function goEditarTR(){
          var id =0;
          if (selected_tr.length > 0 ){
              id = selected_tr[0];

              getUrl('obtenerTransmittal/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }        
        /* */
         $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });          

          



         $('#frmproyecto').on('submit',function(e){
            if($('#personal_cd').val()=='' ){
                alert('Especifique el personal de Control Documentario');
                return ;
            }
            if($('#tipoenvio').val()=='' ){
                alert('Especifique el tipo de envio');
                return ;
            }        
            if($('#secuencia').val()=='' ){
                alert('Especifique el Nro de Transmittal a Generar.');
                return ;
            }               
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            

            $.ajax({

                type:"POST",
                url:'grabarGeneracionTR',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });  

        $('.select-box').chosen(
        {
            allow_single_deselect: true
        });

        $("#btnEmitir").on('click',function(e){
            if($('#personal_cd').val()=='' ){
                alert('Especifique el personal de Control Documentario');
                return ;
            }
            if($('#tipoenvio').val()=='' ){
                alert('Especifique el tipo de envio');
                return ;
            }        
            if($('#secuencia').val()=='' ){
                alert('Especifique el Nro de Transmittal a Generar.');
                return ;
            }                  
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'emitirGeneracionTR',
                data:$("#frmproyecto").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });

        $("#btnPrevio").on('click',function(e){
            var datos = $("#frmproyecto").serialize();
            win = window.open('printedGeneracionTR?'+datos,'_blank');            
             

        });        

        $('#btnDeta').on('click',function(e){
            limpiarDatos();
             $("#viewDeta").modal("show");
            /*
            $.ajax({
                type:"GET",
                url:'modalItemGeneraTR/0',
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     
                     $("#divDeta").html(data);
                     $("#viewDeta").modal("show");
                },
                error: function(data){
                    $('#div_carga').hide();

                }                
            });*/
        });
        $("#btnAddItem").on('click',function(e){
            
            $.ajax({
                type:"POST",
                url:'saveItemGeneraTR',
                data:$("#frmdeta").serialize(),
                beforeSend: function () {
                    $("#viewDeta").modal("hide");
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $('#div_carga').hide(); 
                     
                     $("#divTable").html(data);
                     
                },
                error: function(data){
                    $('#div_carga').hide();

                }                
            });
        });
        function editarItem(id){
            limpiarDatos();
            $.ajax({
                type:"GET",
                url:'modalItemGeneraTR/'+id,
                beforeSend: function () {
                    /*$("#viewDeta").modal("hide");
                    $('#div_carga').show(); */
            
                },
                success: function(data){
                     
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    console.log(pushedData);
                    $("#ctransmittalejedetalle").val(pushedData.ctransmittalejedetalle);
                    $("#cproyectoentregables").val(pushedData.cproyectoentregables);
                    $("#codigo").val(pushedData.codigo);
                    $("#descripcion").val(pushedData.descripcion);
                    $("#revision").val(pushedData.revision);
                    $("#cantidad").val(pushedData.cantidad);
                    $("#viewDeta").modal("show");
                     
                     
                },
                error: function(data){
                    /*$('#div_carga').hide();*/

                }                
            });
        }
        function eliminarItem(id){
            $.ajax({
                type:"GET",
                url:'eliminarItemGenerarTR/'+id,
                beforeSend: function () {
                    $("#viewDeta").modal("hide");
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#divTable").html(data);
                     
                     
                },
                error: function(data){
                    $('#div_carga').hide();

                }                
            });
        }
        function limpiarDatos(){
                $("#ctransmittalejedetalle").val('');
                $("#cproyectoentregables").val('');
                $("#codigo").val('');
                $("#descripcion").val('');
                $("#revision").val('');
                $("#cantidad").val('');
                
        }
    </script>  