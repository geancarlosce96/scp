<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Propuestas
        <small>Configuración de Transmittal</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuestas</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-2 col-xs-12">
                @include('accesos.accesoPropuesta')
            </div>
            <div class="col-lg-10 col-xs-12">
            </div>            
        </div>    
        {!! Form::open(array('url' => 'grabarDisciplinaPropuesta','method' => 'POST','id' =>'frmpropuesta')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">
    
      <div class="row">
        {!! Form::hidden('cpropuesta',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:'')) !!}
        {!! Form::hidden('ctransconfiguracion',(isset($configuracion->ctransconfiguracion)==1?$configuracion->ctransconfiguracion:'')) !!}
        <div class="col-lg-2 col-xs-2 clsPadding">Cliente:</div>
        <div class="col-lg-8 col-xs-8 clsPadding">{!! Form::text('nombrecliente',(isset($persona->nombre)?$persona->nombre:''),array('class'=>'form-control input-sm','disabled' => 'true')) !!}</div>
        
        <div class="col-lg-1 col-xs-1 clsPadding"><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchPropuesta'><b>...</b></button></div>
        <div class="col-lg-1 col-xs-1 clsPadding"><button class="btn btn-primary btn-block" disabled><b>+</b></button></div>
        
        <div class="col-lg-2 col-xs-2 clsPadding">Unidad minera:</div>
        <div class="col-lg-4 col-xs-4 clsPadding">{!! Form::text('nombreuminera',(isset($uminera->nombre)==1?$uminera->nombre:''),array('disabled'=>'','class'=> 'form-control input-sm','placeholder'=>'Nombre Unidad Minera')) !!}</div>
         
        <div class="col-lg-2 col-xs-2 clsPadding">Código Propuesta:</div>
        <div class="col-lg-4 col-xs-4 clsPadding">
            {!! Form::text('codigopropuesta',(isset($propuesta->ccodigo)?$propuesta->ccodigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}    
        </div>
        
        <div class="col-lg-2 col-xs-2 clsPadding">Nombre Propuesta:</div>
        <div class="col-lg-10 col-xs-10 clsPadding">
            {!! Form::text('nombrepropuesta',(isset($propuesta->nombre)?$propuesta->nombre:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}    
        </div>        
      </div> 


  <!--<div class="row clsPadding">
  	<div class="col-lg-2 col-xs-12 clsPadding">Nro Transmittal:</div>
      <div class="col-lg-7 col-xs-6 clsPadding">
      	<input class="form-control input-sm" type="text" placeholder="" name="codtransmittal" value="{{ (isset($configuracion->numero)==1?$configuracion->numero:'') }}" ></div>
  	<div class="col-lg-3 col-xs-6 clsPadding"> -->
      	
    </div>              
  </div>   
  <div class="row clsPadding">
  	<div class="col-lg-2 col-xs-12 clsPadding">Logo:</div>
      <div class="col-lg-8 col-xs-10 clsPadding">
      	<img src="{{ URL::route('viewLogoEmpresa',['id'=> (isset($persona->cpersona)==1?$persona->cpersona:0)]) }}"></div>
  	<div class="col-lg-2 col-xs-2 clsPadding">
      <button type="button" class="btn btn-primary btn-block" data-toggle='modal' data-target="#uploadFile"><b>Seleccionar archivo </b></button>
    </div>            
  </div>   

  <div class="row">
            <div class="col-md-4 col-xs-9">
            Lista de Contactos:
            </div>
            <div class="col-md-4 col-xs-2"><button type="button" data-toggle="modal" data-target="#contactUM" class="btn btn-primary btn-sm "> <b>Agregar Contacto de  UM</b></button></div>
            <div class="col-md-4 col-xs-1"><button type="button" data-toggle="modal" data-target="#contact" class="btn btn-primary btn-sm "> <b>Agregar Contacto a Transmittal</b></button></div>    
        </div>


  <div class="row clsPadding2">	
      <div class="col-lg-12 col-xs-12 table-responsive" id="divContact">
      @include('partials.tableConfiguracionTransmittalContactoPropuesta',array('transconfigcontac'=> (isset($transconfigcontac)==1?$transconfigcontac:null) ))  
      </div>
  </div>

  <div class="row clsPadding2">	
      <div class="col-lg-4 col-xs-4">
      	  <label class="clsTxtNormal">Formato:</label>
            {!! Form::select('formato',(isset($tformato)==1?$tformato:array()),(isset($configuracion->formato)==1?$configuracion->formato:''),array('class' => 'form-control')) !!}        
      </div>
      <div class="col-lg-4 col-xs-4">
      	  <label class="clsTxtNormal">Tipo de Envío:</label>
            {!! Form::select('tipoenvio',(isset($ttipoenvio)==1?$ttipoenvio:array()),(isset($configuracion->tipoenvio )==1?$configuracion->tipoenvio :''),array('class' => 'form-control')) !!}     
      </div>
      <div class="col-lg-4 col-xs-4">
  		  
      </div>
  </div>      
 
  <div class="row clsPadding2">
      <div class="col-lg-3 col-xs-12 clsPadding">
          <button class="btn btn-primary btn-block" style="width:200px;" ><b>Grabar</b></button>  
      </div>
      <div class="col-lg-3 col-xs-12 clsPadding">
      </div>  
  </div>
        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
{!! Form::close() !!}
    </section>
    <!-- /.content -->

    <!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuestas</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuestas" class="table">
                <thead>
                  <th>Item</th>
                  <th>Código</th>
                  <th>Cliente</th>
                  <th>Unidad Minera</th>
                  <th>Propuesta</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal de Buscar Propuesta-->

<!-- Modal Upload Archivo Logo-->
     
    <div id="uploadFile" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Subir Archivos</h4>
          </div>
          <div class="modal-body">
          {!! Form::open(
              array(
                  'route' => 'uploadLogo', 
                  'class' => 'form', 
                  'novalidate' => 'novalidate', 
                  'files' => true,
                  'id' => 'frmUploadFiles'
                  )) !!}
                  {!! Form::hidden('cpropuesta_file',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:'')) !!}

              <div class="form-group">
                  {!! Form::label('Especificar el Archivo de Imagen') !!}
                  {!! Form::file('logo', null,array('class'=>'form-control')) !!}
              </div>

              <div class="form-group">
                  {!! Form::submit('Subir Archivos',array('class'=>'btn btn-sucess')) !!}
              </div>
           {!! Form::close() !!} 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Close</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal Upload Archivo-->

    
    <!-- Modal Agregar Contacto-->
     
    <div id="contact" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Contactos de Transmittal </h4>
          </div>
          <div class="modal-body">
          {!! Form::open(
              array(
                  'route' => 'addContactoTransmittalPropuesta', 
                  'id' => 'frmcontacto'
                  )) !!}
                  {!! Form::hidden('cunidadmineracontacto','',array('id'=>'cunidadmineracontacto')) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Apellido Paterno:') !!}
                        {!! Form::text('apaterno', '',array('class'=>'form-control','id'=>'apaterno')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Apellido Materno:') !!}
                        {!! Form::text('amaterno', '',array('class'=>'form-control','id'=>'amaterno')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Nombres:') !!}
                        {!! Form::text('nombres', '',array('class'=>'form-control','id'=>'nombres')) !!}
                    </div>              
                </div>                
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Tipo contacto:') !!}
                        {!! Form::select('tipocontacto',(isset($ttiposcontacto)==1?$ttiposcontacto:array()),'',array('class' => 'form-control','id'=>'tipocontacto')) !!}        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Cargo:') !!}
                        {!! Form::select('contactocargo',(isset($tcontactocargo)==1?$tcontactocargo:array()),'',array('class' => 'form-control','id'=>'contactocargo')) !!}        
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Email:') !!}
                        {!! Form::text('email', '',array('class'=>'form-control','id'=>'email')) !!}
                    </div>              
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        {!! Form::label('Tipo InfoRmación Contacto:') !!}
                        {!! Form::text('tipoinformacioncontacto','',array('class' => 'form-control','id'=>'tipoinformacioncontacto')) !!}        
                    </div>
                </div>
            </div>            
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnSaveCon">Guardar</button>
                    </div>
                </div>
            </div>
           {!! Form::close() !!} 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal Agregar Contacto-->

     <!-- Modal -->
    <div id="contactUM" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Contactos de Unidad Minera </h4>
          </div>
          <div class="modal-body">
            {!! Form::open(
              array(
                  'route' => 'addContactoTransmittalProyectoUM', 
                  'id' => 'frmcontactoUM'
                  )) !!}
        <table class="table table-striped">
          <thead>
            <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Apellidos</td>
                <td class="clsAnchoTabla">Nombre</td>
                  <td class="clsAnchoTabla">Tipo Contacto</td>
                  <td class="clsAnchoTabla">Cargo</td>
                  <td class="clsAnchoTabla">Correo</td>
                  <td class="clsAnchoTabla">Sel</td>
              </tr>
          </thead>
        <tbody>
        @if(isset($contactUM))
        @foreach($contactUM as $cn)
          <tr>
                <td class="clsAnchoTabla">{{ $cn['apaterno'] }} {{ $cn['amaterno'] }}</td>
                <td class="clsAnchoTabla">{{ $cn['nombres'] }}</td>
                <td class="clsAnchoTabla">{{ $cn['ctipocontacto'] }}</td>
                <td class="clsAnchoTabla">{{ $cn['ccontactocargo'] }} </td>
                <td class="clsAnchoTabla">{{ $cn['email'] }}</td>
                <td class="clsAnchoTabla">
                    <input type='checkbox' name="conum[]" value="{{ $cn['cunidadmineracontacto'] }}" />          
                </td>           
          </tr>
        @endforeach
        @endif          
        </tbody>
        </table>  
            <div class="row">
                <div class="col-md-10"></div>
                <div class="col-md-2">
                    <div class="form-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnAddConUM">Agregar</button>
                    </div>
                </div>
            </div>
            {!! Form::close()!!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
          </div>
        </div>

      </div>
    </div>        

    <!-- Fin Modal agregar contacto  de Unidad Minera contacto-->

  </div>

  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
        /* buscar Propuestas */
        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';

        var table=$("#tPropuestas").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarPropuestas",
                "type": "GET",
            },
            "columns":[
                {data : 'cpropuesta', name: 'tpropuesta.cpropuesta'},
                {data : 'ccodigo', name: 'tpropuesta.ccodigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tpropuesta.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });


        $('#tPropuestas tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            var table = $('#tPropuestas').DataTable();
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);            
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        /* Fin Buscar Propuestas*/

        /*
        * @Descripcion 
        * @param
        * @return
        */
        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];
              //alert(id.substring(4));
              //$('#searchPropuesta').modal("hide");

              getUrl('editarConfTransmittal/'+id.substring(4),'');
              
              //alert(id.substring(4));
              
          }else{
              $('.alert').show();
          }
        }   
        $('#searchPropuesta').on('hidden.bs.modal', function () {
            
           goEditar();
        });
        /* Fin de Buscar Propuestas*/   


        $('#frmUploadFiles').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            var fd = new FormData(document.getElementById('frmUploadFiles'));
            
            
            $.ajax({

                type:"POST",
                url:'uploadLogo',
                data: fd,
                processData : false,
                contentType :  false,
                beforeSend: function () {
                  
                  $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     //$("#resultado").html(data);
                     $("#uploadFile").modal('hide');
                },
                error: function(data){
                    $('#div_carga').hide();
                }
            });
        });
        $("#frmpropuesta").on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);
            $.ajax({

                type:"POST",
                url:'grabarConfTransmittal',
                data:$(this).serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });

          
        });   
        $('#btnSaveCon').on('click', function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            
            e.preventDefault(e);
            $.ajax({
                type:"POST",
                url:'addContactoTransmittalPropuesta',
                data: $("#frmcontacto").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
                    
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#divContact").html(data);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    
                }                
            });            
        });
        function eliminarContacto(id){
            $.ajax({
              url: 'delContactoTransmittalPropuesta/'+id,
              type: 'GET',
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){
                //$("#idPersonal").html(data);
                $("#divContact").html(data);
                 $('#div_carga').hide(); 
              },

          });
        }

        $("#btnAddConUM").on('click',function(e){
            $("contactUM").modal('hide');
            $.ajax({
              url: 'addContactoTransmittalPropuestaUM',
              type: 'POST',
              data: $("#frmcontactoUM").serialize(),
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){
                //$("#idPersonal").html(data);
                $("#divContact").html(data);
                 $('#div_carga').hide(); 
              },

          });
        });
</script>