<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario / Panel de Control
        <small>Inicio</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
<!-- ********** -->
<div class="row">
	<!--<div class="col-lg-1 hidden-xs"></div>-->
    <!-- ********** -->
    <div class="col-md-12 col-xs-12">


<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
        Resumen
    </div>
</div>
 <div class="row clsPadding2">
      	<div class="col-lg-3 col-xs-3">
        	Número de Proyectos:
        </div>
      	<div class="col-lg-4 col-xs-4">
        	<input class="form-control input-sm" type="text" placeholder="# proyectos" 
          value="{{ (isset($datos['nroproy'])==1?$datos['nroproy']:0) }}"
          readonly="true">

        </div> 
        <div class="col-lg-5 col-xs-5"></div>      
 </div>
      
      
 <div class="row clsPadding2">
      
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
              {{ (isset($datos['proy_estado']['001'])==1?$datos['proy_estado']['001']:0 ) }}
              </h3>

              <p>En proceso</p>
            </div>
            <div class="icon">
              <!-- <i class="ion ion-bag"></i>  Reemplazar por Icono de En Proceso-->
              <i class="fa fa-cog"></i> 
            </div>
            <a href="#" class="small-box-footer">Más  Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <!--<h3>53<sup style="font-size: 20px">%</sup></h3>-->
              <h3>{{ (isset($datos['proy_estado']['003'])==1?$datos['proy_estado']['003']:0 ) }}	</h3>
              <p>En espera</p>
            </div>
            <div class="icon">
              <i class="fa fa-hand-paper-o"></i>
            </div>
            <a href="#" class="small-box-footer">Más  Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{ (isset($datos['proy_estado']['002'])==1?$datos['proy_estado']['002']:0 ) }}</h3>

              <p>Cancelados</p>
            </div>
            <div class="icon">
              <i class="fa fa-times"></i>
            </div>
            <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-gray">
            <div class="inner">
              <h3>{{ (isset($datos['proy_estado']['004'])==1?$datos['proy_estado']['004']:0 ) }}</h3>

              <p>Cerrados</p>
            </div>
            <div class="icon">
              <i class="fa fa-lock"></i>
            </div>
            <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
 </div>
 

 <!--<div class="row clsPadding2">
      	<div class="col-lg-3 col-xs-3">
        	Número de Entregables:
        </div>
      	<div class="col-lg-4 col-xs-4">
        	<input class="form-control input-sm" type="text" placeholder="# entregables" readOnly="true" 
          value="{{ (isset($datos['nroent'])==1?$datos['nroent']:0) }}"
          >
          
        </div> 
        <div class="col-lg-5 col-xs-5"></div>      
 </div>
 
  <div class="row clsPadding2">
      
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{ (isset($datos['ent_estado']['001'])==1?$datos['ent_estado']['001']:0 ) }}</h3>

              <p>Asignados</p>
            </div>
            <div class="icon">
              <i class="fa fa-hand-o-right" aria-hidden="true"></i> 
            </div>
            <a href="#" class="small-box-footer">Más  Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>

        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-gray bg-gray">
            <div class="inner">
              <h3>{{ (isset($datos['ent_estado']['004'])==1?$datos['ent_estado']['004']:0 ) }}</h3>

              <p>Cerrados</p>
            </div>
            <div class="icon">
              <i class="fa fa-lock"></i>
            </div>
            <a href="#" class="small-box-footer">Más Información <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
 </div>-->
<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
        Carga Laboral CD
    </div>
</div>

<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
      	<thead>
        <tr>
        	<td></td>
            <td colspan="2"class="clsCabereraTabla">En Proceso</td>
            <td class="clsCabereraTabla">En Espera</td>
            <td class="clsCabereraTabla">Cancelado</td>
            <td colspan="2" class="clsCabereraTabla">Cerrado</td>                                   
        </tr>
        <tr>
        	<td class="clsCabereraTabla">Coordinador CD</td>
            <td class="clsCabereraTabla"># Proy</td>
            <td class="clsCabereraTabla"># Ent</td>
            <td class="clsCabereraTabla"># Proy</td>
            <td class="clsCabereraTabla"># Proy</td>
            <td class="clsCabereraTabla"># Proy</td>
            <td class="clsCabereraTabla"># Ent</td>
        </tr>
        </thead>
        <tbody>
        @if(count($datos['personal']) >0)
        @foreach($datos['personal'] as $per)
        <tr>
            <td class="clsAnchoDia">{{ $per['nombre'] }}</td>
            <td class="clsAnchoDia">
            {{ (isset($per['proy_001'])==1?$per['proy_001']:0 ) }}
            </td>
            <td class="clsAnchoDia">
            {{ (isset($per['ent_001'])==1?$per['ent_001']:0 ) }}
            </td>
            <td class="clsAnchoDia">
            {{ (isset($per['proy_003'])==1?$per['proy_003']:0 ) }}
            </td>
            <td class="clsAnchoDia">
            {{ (isset($per['proy_002'])==1?$per['proy_002']:0 ) }}
            </td>
            <td class="clsAnchoDia">
            {{ (isset($per['proy_004'])==1?$per['proy_004']:0 ) }}
            </td>
            <td class="clsAnchoDia">
            {{ (isset($per['ent_004'])==1?$per['ent_004']:0 ) }}
            </td>            
        </tr>
        @endforeach
        @endif
        </tbody>
        </table>
     </div>
 </div>
<div class="row clsPadding2">
	<!--<div id="chartdiv" style="width: 600px; height: 400px;"></div>-->
    <div id="chartdiv" style="width: 800px; height: 400px;"></div>
</div>





    
    </div>
    <!-- ********** -->
    <!--<div class="col-lg-1 hidden-xs"></div>-->
</div>
<!-- ********** -->
    </section>
    <!-- /.content -->
  </div>