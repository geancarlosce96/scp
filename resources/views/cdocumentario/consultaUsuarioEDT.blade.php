<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Control Documentario
        <small>Consulta del EDT por Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Control Documentario</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmproyecto')) !!}
        {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}


    <div class="row">
    <!---<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-md-12 col-xs-12">
    
    <div class="row">
    <div class="col-lg-2 col-xs-2 ">Cliente:</div>
    <div class="col-lg-9 col-xs-9 ">{!! Form::text('personanombre',(isset($persona->nombre)==1?$persona->nombre:''), array('class' => 'form-control input-sm','placeholder' => 'Cliente','disabled' => 'true','id' => 'personanombre')) !!}</div>
    <div class="col-lg-1 col-xs-1 "><button type='button' class="btn btn-primary btn-block" data-toggle='modal' data-target='#searchProyecto'><b>...</b></button></div>
    <div class="col-lg-2 col-xs-2 ">Unidad minera:</div>
    <div class="col-lg-4 col-xs-4 ">{!! Form::text('umineranombre',(isset($uminera->nombre)==1?$uminera->nombre:''), array('class'=>'form-control input-sm','placeholder' => 'Unidad Minera','disabled' => 'true','id'=>'umineranombre') ) !!} </div> 
    <div class="col-lg-2 col-xs-2 ">Código Proyecto:</div>
    <div class="col-lg-4 col-xs-4 ">{!! Form::text('codigoproyecto',(isset($proyecto->codigo)?$proyecto->codigo:''),array('disabled'=>'','class'=> 'form-control input-sm')) !!}</div>
    </div>    
    <div class="row ">
    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
    </div>
    <div class="row ">
        <div class="col-lg-3 col-xs-3">Nombre del Proyecto:</div>
        <div class="col-lg-9 col-xs-9">
            {!! Form::text('nombreproyecto',(isset($proyecto->nombre)==1?$proyecto->nombre:''),array('class'=>'form-control input-sm','disabled' =>'','placeholder'=>'Nombre de Proyecto')) !!} 
        </div>	 
    </div> 
    <div class="row ">
        <div class="col-lg-4 col-xs-4">
        <label class="clsTxtNormal">Fecha Inicio:</label>
        {!! Form::text('finicio',(isset($proyecto->finicio)?$proyecto->finicio:''), array('class'=>'form-control pull-right')) !!}</div> 
        <div class="col-lg-4 col-xs-4">
        <label class="clsTxtNormal">Fecha de Cierre:</label>
        {!! Form::text('fcierre',(isset($proyecto->fcierre)?$proyecto->fcierre:''), array('class'=>'form-control pull-right')) !!}</div>
        <div class="col-lg-4 col-xs-4">
            <label class="clsTxtNormal">Estado:</label>
                {!! Form::select('estadoproyecto',(isset($estadoproyecto)==1?$estadoproyecto:array()),(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''),array('class' => 'form-control')) !!} 
        </div>        
    </div>
    <div class="row ">
        <div class="col-lg-4 col-xs-4">
        </div>
        <div class="col-lg-4 col-xs-4"></div>
        <div class="col-lg-4 col-xs-4"></div>                
   </div>    


<div class="row ">	
    <div class="col-lg-12 col-xs-12 table-responsive" id="divEdt">
    @include('partials.tableEDTDocumentario', array('tproyectoedt' => (isset($tproyectoedt)==1?$tproyectoedt:null) ))
    </div>
</div>

      
 
<div class="row ">
    <div class="col-lg-2 col-xs-6 clsPadding">
        <button type="button" class="btn btn-primary btn-block" id="btnprint" disabled><b>Imprimir</b></button> </div>
    <div class="col-lg-2 col-xs-6 clsPadding">
         </div> 
    <div class="col-lg-2 col-xs-6 clsPadding">
        <button type="button" class="btn btn-primary btn-block" id="btnagregar"><b>Agregar</b></button> </div> 
    <div class="col-lg-2 col-xs-6 clsPadding">
        <button type="submit" class="btn btn-primary btn-block" id="btnsave"><b>Guardar</b></button> </div>  
    <div class="col-lg-2 col-xs-6 clsPadding">
        <button type="button" class="btn btn-primary btn-block" id="viewGrafico" disabled><b>Vista Gráfica</b></button> </div>                          
</div>
        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
    {!! Form::close() !!}
    </section>
    @include('partials.searchProyecto')
    <!-- /.content -->


       <!-- Modal Agregar EDT-->
     
    <div id="modalEdt" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Items de EDT </h4>
          </div>
          <div class="modal-body">
          {!! Form::open(
              array(
                  'route' => 'addItemEDT', 
                  'id' => 'frmitemedt'
                  )) !!}
                  {!! Form::hidden('cproyectoedt','',array('id'=>'cproyectoedt')) !!}
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Codigo:') !!}
                        {!! Form::text('codigo', '',array('class'=>'form-control','id'=>'codigo','size'=>'5')) !!}
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        {!! Form::label('Descripcion:') !!}
                        {!! Form::text('descripcion', '',array('class'=>'form-control','id'=>'descripcion')) !!}
                    </div>
                </div>
                <div class="col-md-4">
             
                </div>                
            </div>
            
           
            <div class="row">
                <div class="col-md-8"></div>
                <div class="col-md-4">
                    <div class="form-group">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnsaveItem">Agregar</button>
                    </div>
                </div>
            </div>
           {!! Form::close() !!} 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Cerrar</button>
          </div>
        </div>

      </div>
    </div>

    <!-- Fin Modal Agregar EDT-->
  </div>

  <script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

        var selected =[];
        $.fn.dataTable.ext.errMode = 'throw';
     

        var table=$("#tProyecto").DataTable({
            "processing":true,
            "serverSide": true,

            "ajax": {
                "url": "listarProyectos",
                "type": "GET",
            },
            "columns":[
                {data : 'cproyecto', name: 'tproyecto.cproyecto'},
                {data : 'codigo', name: 'tproyecto.codigo'},
                {data : 'cliente' , name : 'tper.nombre'},
                {data : 'uminera' , name : 'tu.nombre'}, 
                {data : 'nombre' , name : 'tproyecto.nombre'}

            ],
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tProyecto tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchProyecto').on('hidden.bs.modal', function () {
           goEditar();
        });   

        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];

              getUrl('editarConsultaEDT/'+id.substring(4),'');
          }else{
              $('.alert').show();
          }
        }

         $('#finicio').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

        $('#fcierre').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });           



         $('#frmproyecto').on('submit',function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            

            $.ajax({

                type:"POST",
                url:'grabarConsultaEDT',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     //$(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();
                },
                error: function(data){
                    

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });  

        $('#btnagregar').on('click',function(e){
            $("#modalEdt").modal("show");

        });
        $('#btnsaveItem').on('click', function(e){
            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            
            e.preventDefault(e);
            $.ajax({
                type:"POST",
                url:'addItemEDT',
                data: $("#frmitemedt").serialize(),
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
                    
            
                },
                success: function(data){
                     
                     $('#div_carga').hide(); 
                     
                     $("#divEdt").html(data);
                     
                },
                error: function(data){
                    
                    
                    $('#div_carga').hide();
                    
                }                
            }); 
        });

        function eliminarItem(id){
            $.ajax({
              url: 'delItemEDT/'+id,
              type: 'GET',
              beforeSend: function () {
                  
                  $('#div_carga').show(); 
          
              },              
              success: function(data){
                //$("#idPersonal").html(data);
                $("#divEdt").html(data);
                 $('#div_carga').hide(); 
              },

          });
        }

</script>  