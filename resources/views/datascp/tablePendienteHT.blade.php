<table class="table table-striped table-bordered" id="tableAprobar" style="font-size: 12px;">
    <thead>
    <tr class="clsCabereraTabla">
      <th><center>Ítem</center></th>
      <th><center>Semana</center></th>
      <th><center>Periodo</center></th>
      <th><center>Profesional</center></th>
      <th><center>Área</center></th>
      <th><center>Horas</center></th>
      <!-- <th><center>cpersona</center></th> -->
      <!-- <th><center>fecha</center></th> -->
      @if($columna_aprob == true)
      <th><center>Aprobador</center></th>
      <th><center>Área Aprobación</center></th>
      @endif
      <th>Condición</th>
 
      @if($esGP==0)

      <th><center>HT</center></th>
      @endif

    </tr> 
    </thead>
    @if(isset($listAprobar))
    <tbody>
       <?php //dd($esGP); ?>
    @foreach($listAprobar as $ap)

    <tr class="gradeX">
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['item'] }}</center></td>
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['semana'] }}</center></td>
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['anio'] }}</center></td>
      <td style="border:1px solid #DCDCDCFF;">{{ $ap['nomper'] }}</td>
      <td style="border:1px solid #DCDCDCFF;">{{ $ap['descripcion'] }}</td>
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['total'] }}</center></td>
      <!-- <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['cpersona'] }}</center></td> -->
      <!-- <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['fecha'] }}</center></td> -->
      @if($columna_aprob == true)
        <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['aprobador'] }}</center></td>
        <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['areaapr'] }}</center></td>
      @endif
      <td style="border:1px solid #DCDCDCFF;">{{ $ap['estado'] }}</td>
      @if($esGP==0)
      <td style="border:1px solid #DCDCDCFF;"><center>
        <button name="htparticipante" type="button" onclick="goEditar({{$ap['cpersona']}},'{{ $ap['fecha'] }}')" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="left" title="" data-original-title="Ver Hoja de tiempo de {{ $ap['nomper'] }} "><span class="glyphicon glyphicon-eye-open"></span></button></center>
      </td>
      @endif
    </tr>
    @endforeach                                                          
    </tbody>
    @endif
</table>