
<div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Hoja de Tiempo -
        <small>Estados</small>
      </h1>
      
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Hoja de tiempo</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        {!! Form::open(array('url' => 'grabarRegistroHoras','method' => 'POST','id' =>'frmHoras')) !!}
        
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
        {!! Form::hidden('obs_txt',csrf_token(),array('id'=>'obs_txt')) !!}


        <section class="panel box box-primary" >

            <div class="col-lg-2"  style="padding-top: 5px;">                
            </div>

            <div class="col-md-2 col-xs-2"  style="padding-top: 5px;">
               {!! Form::select('estadohoras',(isset($filtroestadoji)==1?$filtroestadoji:array() ),'',array('class' => 'form-control select-box','id'=>'estadohoras')) !!}

            </div>

            <div class="col-md-2 col-xs-2" style="padding-top: 5px;">
                        <select name="anio" id="anio" class="form-control select-box">
                        <?php
                            $anio=date("Y");
                            for($t=$anio ; $t>=2010;$t--){
                            $selected="";
                            $selected = ($t==$anio?"selected":"");
                        ?>
                        <option value="{{ $t }}" {{ $selected }}>{{ $t }}</option>
                        <?php
                            }
                        ?>
                        </select>
                    </div>         

            <div class="col-lg-2"  style="padding-top: 5px;">
                <button type="button" class="btn btn-primary" style="display: block;" id="btnView">
                    <i class="fa fa-search" aria-hidden="true"></i> Visualizar
                </button>
            </div>

            <div class="col-lg-5" style="padding-top: 0px;">
            </div>
            <div class="col-lg-1 pull-left" stimetyle="padding-top: 0px;">
            </div>

            <div class="row ">    
                <div class="col-lg-12 col-xs-12 table-responsive" id="divAprobar">
                    @include('datascp.tablePendienteHT',array('listAprobar'=>(isset($listAprobar)==1?$listAprobar:array()) ) )      
                </div>
            </div>
            
            <div class="col-lg-5" style="padding-top: 0px;">
            </div>
            <div class="col-lg-1 pull-left" style="padding-top: 0px;">
            </div>
        </section>


        {!! Form::close() !!}
    </div>

    @include('partials.reporteHorasParticipante')

<script>
        var id_ejecu='';
function verHorasAprobar(){
    getUrl('listarHTGP');
}


var table = $('#tableAprobar').DataTable( {
        "paging":   false,
        "ordering": true,
        "info":     true,
        lengthChange: false,
        buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
    } );
 
    table.buttons().container()
        .appendTo( '#tableAprobar_wrapper .col-sm-6:eq(0)' );


        $("#btnView").click(function (e){
           
            $.ajax({
                url: 'listarHTGP',
                type: 'GET',
                data:  {
                    "estadohoras" : $("#estadohoras").val(),
                    "anio" : $("#anio").val()
                },
                beforeSend: function () {
                    $('#div_carga').show(); 
                },              
                success : function (data){
                    $("#resultado").html(data);
                    $('#div_carga').hide(); 
                   
                },
            });
        });

$('.select-box').chosen(
        {
            allow_single_deselect: true,
            width: "100%",
        });

function goEditar(cpersona,fecha){              
    $.ajax({

            url:'verReporteHorasParticipante',
            type: 'POST',
            data : {
                '_token':'{{ csrf_token() }}',
                'cpersona' : cpersona,
                'fecha': fecha,
            },
            beforeSend: function(){
                $('#div_carga').show(); 
            },
            success: function(data){                  
                $('#div_carga').hide(); 
                $("#horas").html(data);
                $("#verHT").modal("show");
                
            },            
    });
}

</script>  
