@extends('layouts.master')
@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
  <h1>
    Propuestas / Inicio
  </h1>
</section>

<section class="content" style="font-size: 12px" style="text-align: center;">
	<div class="col-md-12">
  		<form class="form-horizontal" action="{{ url('propuestas/inicio') }}" method="post" >
  			{{ csrf_field() }}

  			<div class="panel panel-info">

			  	<div class="panel-heading">Datos de Cliente</div>
			  	<div class="panel-body">
				     <div class="form-group">
		                  <label for="" class="col-sm-1 control-label">Nombre</label>

		                  	<div class="col-sm-2">
			                    <select class="form-control" name="cliente_id" >
			                      <option value="0">Elegir</option>
			                      @foreach($clientes as $cliente)
			                      <option value="{{ $cliente->cpersona }}" >{{ $cliente->nombre }}</option>
			                      @endforeach
			                    </select>
		                  	</div>

		                  	<label for="" class="col-sm-1 control-label">Unidad Minera</label>
		                  	<div class="col-sm-2" >
		                  		<select class="form-control" name="unidad_minera_id" >
			                      <option value="0">Elegir</option>
			                      @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
			                      @endforeach
			                    </select>
		                  	</div>
		                  	<label for="" class="col-sm-1 control-label">Fecha de Registro</label>
		                  	<div class="col-sm-2" >
		                  	<input type="text" class="form-control" placeholder="" readonly >
		              		</div>

		              		<div class="col-sm-2">
		              			<button class="btn btn-primary" id="btn-guardar" type="submit">guardar</button>
		              		</div>
                	</div>
                	<div class="form-group">
                		<label for="" class="col-sm-1 control-label">Propuesta</label>
                		<div class="col-sm-11" >
                			<textarea class="form-control" name="propuesta" ></textarea>
                		</div>
                	</div>
			  	</div>
			</div>

			<div class="panel panel-info">
			  	<div class="panel-heading">Datos de Contacto</div>
			  	<div class="panel-body">
			  	<label for="" class="col-sm-1 control-label">Nombre</label>
				<div class="col-sm-2">
					<input type="text" name="nombre" class="form-control">
				</div>
				<label for="" class="col-sm-1 control-label">Cargo</label>
				<div class="col-sm-2">
					<input type="text" name="cargo" class="form-control">
				</div>
				<label for="" class="col-sm-1 control-label">Telefono</label>
				<div class="col-sm-2">
					<input type="text" name="telefono" class="form-control">
				</div>
				<label for="" class="col-sm-1 control-label">Email</label>
				<div class="col-sm-2">
					<input type="text" name="email" class="form-control">
				</div>
			  	</div>
			</div>

			<div class="panel panel-info">
			  	<div class="panel-heading">Datos Adicionales</div>
			  	<div class="panel-body">
			  		<div class="row">            	
		            	<div class="col-md-3" ><b>Tipo de Servicio</b></div>
		            	<div class="col-md-3" ><b>Tipo de Registro</b></div>
		            </div>
		            <div class="row">
		            	<div class="col-md-3" >
		            		@foreach($tipo_servicio as $servicio)
		            		<input type="radio" name="tipo_servicio_id" value="{{ $servicio->id }}"> {{ $servicio->descripcion }}<br>
		            		@endforeach		            		
						</div>
						<div class="col-md-3" >
							<input type="radio" name="tipo_registro" value="3"> Invitación<br>
		            		<input type="radio" name="tipo_registro" value="2"> Preliminar<br>
		            		<input type="radio" name="tipo_registro" value="1"> Propuesta<br>
						</div>
						<div class="col-md-2" >
							<input type="text" name="codigo" id="codigo" class="form-control" placeholder="codigo" readonly>
						</div>
						<div class="col-md-1" > 
							<button class="form-control btn btn-info" id="btn-info" >obtener</button>
						</div>
		            </div>
			  	</div>
			</div>

			<div class="panel panel-info">
			  	<div class="panel-heading">Tag</div>
			  	<div class="panel-body">
			  		<div class="row">
						<div class="col-md-1 col-md-offset-1" >
							Estructuras
						</div>
						<div class="col-md-3" >
							 <div class="input-group">
						      <input type="text" class="form-control" readonly placeholder="elegir">
						      <span class="input-group-btn">
						        <button class="btn btn-default" type="button">+</button>
						      </span>
						    </div><!-- /input-group -->
						</div>				
						<div class="col-md-1 col-md-offset-1" >
							Servicios
						</div>
						<div class="col-md-3" >
							 <div class="input-group">
						      <input type="text" class="form-control" readonly placeholder="elegir">
						      <span class="input-group-btn">
						        <button class="btn btn-default" type="button">+</button>
						      </span>
						    </div><!-- /input-group -->
						</div>
					</div>
					<div class="row">
						<div class="col-md-3 col-md-offset-2" id="divTagEstructuras" style="margin-top: 20px">
							<div class="panel panel-primary">							    
							    
							    <div class="panel-group" id="accordionT" role="tablist" aria-multiselectable="true">
									
									
									<div class="panel panel-default">
									    <div class="panel-heading" role="tab" id="divT">
									      <h4 class="panel-title">
									        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#divTag" aria-expanded="true" aria-controls="divTag">
									          Tags
									        </a>
									      </h4>
									    </div>
									    <div id="divTag" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="divT">
									      <div class="panel-body">
									        @foreach($tag_estructuras as $tag)
												<input type="checkbox" name="chk_tag_estructuras[]" value="{{ $tag->id }}">
												{{ $tag->descripcion }} ({{ $tag->tag }}) <br>
											@endforeach
									      </div>
									    </div>
									</div>
									
								</div>

							</div>
						</div>
						<div class="col-md-3 col-md-offset-2" id="divTagServicios" style="margin-top: 20px" >
							<div class="panel panel-primary">

								<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
									@foreach($tag_servicios as $grupo)
									@if( count($grupo->items)>0 )
									<div class="panel panel-default">
									    <div class="panel-heading" role="tab" id="{{ $grupo->tag }}">
									      <h4 class="panel-title">
									        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#{{$grupo->id}}" aria-expanded="true" aria-controls="{{$grupo->id}}">
									          {{ $grupo->descripcion }}
									        </a>
									      </h4>
									    </div>
									    <div id="{{$grupo->id}}" class="panel-collapse collapse" role="tabpanel" aria-labelledby="{{ $grupo->tag }}">
									      <div class="panel-body">
									        @foreach($grupo->items as $tag)
												<input type="checkbox" name="chk_tag_servicios[]" value="{{ $tag->id }}">
												{{ $tag->descripcion }} ({{ $tag->tag }}) <br>
											@endforeach
									      </div>
									    </div>
									</div>
									@endif
									@endforeach
								</div>

							</div>

						</div>
					</div>
			  	</div>
			 </div>

  		</form>
  	</div>
</section>

@stop
