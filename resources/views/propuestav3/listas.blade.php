@extends('layouts.master')
@section('content')

<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
      <h1>
        Listado de Propuestas
      </h1>
</section>

    <!-- Main content -->
<section class="content">          

  <div class="row">
    <div class="col-md-12">      
      <button class="btn btn-sucess" id="btn-filtros" >filtros</button>
      <button class="btn btn-sucess" id="btn-buscar" >aplicar filtros</button>
    </div>
  </div>

  <div class="row" id="divFiltros" >
    <div class="col-md-1">
      Tags 
    </div>
    <div class="col-md-2">
      <input type="text" name="tags" id="tags">
    </div>
    <div class="col-md-2">
      Tipo de Servicio 
    </div>
    <div class="col-md-2">
      <select id="tipo_servicio_id" >
      <option value="0" >(ninguno)</option>
      @foreach($tipo_servicio as $servicio)
        <option value="{{ $servicio->id }}" >{{ $servicio->descripcion }}</option>
      @endforeach 
      </select>
    </div>

    </div>

  <div class="row">
  <div class="col-md-12">  
  
    <table class="table table-striped table-hover" id="tbLista" >
      <thead>
      <tr>
        <th>#</th>
        <th>Propuesta</th>        
        <th>Codigo</th> 
        <th>Cliente</th>
        <th>F. Registro</th>
        <th>Estado</th>
        <th>Acciones</th>
      </tr>
      </thead>

      <tbody>      
      @foreach ($lista as $index => $element)
      <tr>      
        <td>{{ $element->n }} </td>        
        <td>{{ $element->propuesta }} </td>
        <td>{{ $element->codigo }} </td>
        <td>@if($element->cliente!=NULL) {{ $element->cliente->nombre }} @endif</td>        
        <td>{{ $element->created_at }} </td>
        <td>{{ $element->estado_toString() }} </td>
        <td>
           <a href="{{ url('#') }}" class="btn btn-primary" title="" > <i class="fa fa-search"></i></a>
           <a href="{{ url('#') }}" class="btn btn-primary"> <i class="fa fa-edit" title="" ></i></a>           
           <a href="{{ url('propuesta/actividades') }}" class="btn btn-primary" title="act" > <i class="fa fa-clone" title="act" ></i></a>           
           <a href="{{ url('#') }}" class="btn btn-danger"> <i class="fa fa-close" title="Cancelar la capacitación" ></i></a>
        </td>
      </tr>      
      @endforeach
      </tbody>

    </table>
  </div>
  </div>

</section>

</div>

<script>
  var BASE_URL = "<?php echo url('/'); ?>";
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" 
integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="{{ asset('js/propuestas_listas.js') }}"></script>

@stop
