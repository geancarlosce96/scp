@extends('layouts.master')
@section('content')

<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

	<div class="content-wrapper" style="min-height: 853px;">

<section class="content-header">
  <h1>
    Propuestas / Planificación
  </h1>
</section>

<section class="content" style="font-size: 12px" style="text-align: center;">
	<form class="form-horizontal" action="{{ url('propuestas/planificacion') }}" method="post" >
	<div class="col-md-12">
  		
  			{{ csrf_field() }}

  			<div class="panel panel-info">

			  	<div class="panel-heading">Datos de Cliente</div>
			  	<div class="panel-body">
				     <div class="form-group">
		                  <label for="" class="col-sm-1 control-label">Nombre</label>

		                  	<div class="col-sm-2">
			                    <select class="form-control" name="cliente_id" >
			                      <option value="0">Elegir</option>
			                      @foreach($clientes as $cliente)
			                      <option value="{{ $cliente->cpersona }}" >{{ $cliente->nombre }}</option>
			                      @endforeach
			                    </select>
		                  	</div>

		                  	<label for="" class="col-sm-1 control-label">Unidad Minera</label>
		                  	<div class="col-sm-2" >
		                  		<select class="form-control" name="unidad_minera_id" >
			                      <option value="0">Elegir</option>
			                      @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
			                      @endforeach
			                    </select>
		                  	</div>
		                  	<label for="" class="col-sm-1 control-label">Codigo</label>
		                  	<div class="col-sm-2" >
		                  		<input type="text" class="form-control" placeholder="codigo" id="codigo" name="id">
		              		</div>

		              		<div class="col-sm-1">
		              			<button class="btn btn-warning" id="btn-obtener" type="button">obtener</button>
		              		</div>

		              		<div class="col-sm-1">
		              			<button class="btn btn-primary" id="btn-guardar" type="submit">guardar</button>
		              		</div>
                	</div>
                	<div class="form-group">
                		<label for="" class="col-sm-1 control-label">Propuesta</label>
                		<div class="col-sm-11" >
                			<textarea class="form-control" name="propuesta" ></textarea>
                		</div>
                	</div>
			  	</div>
			</div>
  		
  	</div>

  	<div class="col-md-4">
  		<div class="panel panel-info">
				<div class="panel-heading">Calendario</div>
			  	<div class="panel-body">		
		
		<label for="" class="col-sm-6 control-label">Inicio Propuesta</label>
		<div class="col-md-6">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right fecha" id="fecha" readonly="true" name="inicio_propuesta" value="" type="text">
		</div>
		</div>

		<label for="" class="col-sm-6 control-label">Visita Técnica</label>
		<div class="col-md-6">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right fecha" id="fecha" readonly="true" name="visita_tecnica" value="" type="text">
		</div>
		</div>

		<label for="" class="col-sm-6 control-label">Presentación Consultas</label>
		<div class="col-md-6">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right fecha" id="fecha" readonly="true" name="presentacion_consultas" value="" type="text">
		</div>
		</div>

		<label for="" class="col-sm-6 control-label">Absolución Consultas</label>
		<div class="col-md-6">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right fecha" id="fecha" readonly="true" name="absolucion_consultas" value="" type="text">
		</div>
		</div>

		<label for="" class="col-sm-6 control-label">Sustentación</label>
		<div class="col-md-6">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right fecha" id="fecha" readonly="true" name="sustentacion" value="" type="text">
		</div>
		</div>

		<label for="" class="col-sm-6 control-label">Presentación</label>
		<div class="col-md-6">
		<div class="input-group date">
			<div class="input-group-addon">
				<i class="fa fa-calendar"></i>
			</div>
			<input class="form-control pull-right fecha" id="fecha" readonly="true" name="presentacion" value="" type="text">
		</div>
		</div>

			  	</div>
			</div>
  	</div>

  	<div class="col-md-4">
  		<div class="panel panel-info">
				<div class="panel-heading">Equipo de Trabajo</div>
			  	<div class="panel-body">
					
					<label for="" class="col-sm-6 control-label">Responsable Propuesta</label>
					<div class="col-sm-6" >
						<select class="form-control" name="responsable_propuesta_id" >
			                      <option value="0">Elegir</option>
			                    @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
								@endforeach
			            </select>
					</div>
					<label for="" class="col-sm-6 control-label">Reviso Final</label>
					<div class="col-sm-6" >
						<select class="form-control" name="revisor_final_id" >
			                      <option value="0">Elegir</option>
			                    @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
								@endforeach
			            </select>
					</div>
					<label for="" class="col-sm-6 control-label">Gerente Proyecto</label>
					<div class="col-sm-6" >
						<select class="form-control" name="gerente_proyecto_id" >
			                      <option value="0">Elegir</option>
			                    @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
								@endforeach
			            </select>
					</div>
					<label for="" class="col-sm-6 control-label">S.S.A</label>
					<div class="col-sm-6" >
						<select class="form-control" name="ssa_id" >
			                      <option value="0">Elegir</option>
			                    @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
								@endforeach
			            </select>
					</div>
					<label for="" class="col-sm-6 control-label">Responsable Técnico</label>
					<div class="col-sm-6" >
						<select class="form-control" name="responsable_tecnico_id" >
			                      <option value="0">Elegir</option>
			                    @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
								@endforeach
			            </select>
					</div>
					<label for="" class="col-sm-6 control-label">Propuestas y Licitaciones</label>
					<div class="col-sm-6" >
						<select class="form-control" name="propuestas_id" >
			                      <option value="0">Elegir</option>
			                    @foreach($umineras as $uminera)
			                      <option value="{{ $uminera->cunidadminera }}" >{{ $uminera->nombre }}</option>
								@endforeach
			            </select>
					</div>

			  	</div>
			</div>
  	</div>

  	<div class="col-md-4">
  		<div class="panel panel-info">
				<div class="panel-heading">Presentacion</div>
			  	<div class="panel-body">
			  		<input type="radio" name="presentacion_tipo" value="1"> Físico <br>
			  		<input type="radio" name="presentacion_tipo" value="2"> Digital
			  	</div>
			</div>
  	</div>

  	<div class="col-md-12">
  		<div class="panel panel-info">
				<div class="panel-heading">Seguimiento</div>
			  	<div class="panel-body">
			  		<div class="form-group">
                		<label for="" class="col-sm-1 control-label">Comentario</label>
                		<div class="col-sm-11" >
                			<textarea class="form-control" name="comentario" ></textarea>
                		</div>
                	</div>
			  	</div>
			</div>
  	</div>

  	</form>
</section>

<script type="text/javascript">
	
	$(document).ready(function(){
		$("#btn-obtener").click(function(){
      		obtenerInfo();
		});
	});

	function obtenerInfo()
	{
		console.log("obtenerInfo");
	}

</script>

@stop

@section('js')
	<script type="text/javascript" src="{{ asset('js/propuestas_planificacion.js') }}"></script>
@stop
