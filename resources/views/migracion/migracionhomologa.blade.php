@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Carga de Excel para importar
        <small>Homologacion data SCP</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Migracion</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">
        <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">
          Seleccionar un archivo
            </div>  
        </div>
        <div class="row clsPadding2">	
            <div class="col-lg-3 col-xs-12">
            </div>
            <div class="col-lg-3 col-xs-12">
            @if(!is_null($error))
              <div>{{ $error }}</div>
            @endif
            </div> 
            <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="getUrl('empleado');"  class="btn btn-primary btn-block"><b>Nuevo</b></button> --> 
            </div> 
            <div class="col-lg-3 col-xs-12">
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <p>Seleccion de la Ruta donde se encuetra el archivo:</p>
          {!! Form::open(array('url'=>'migracion/datos','method'=>'POST', 'enctype' => 'multipart/form-data', 'autocomplete'=>'off','role'=>'search', 'id'=>'uploadForm', 'class'=>'formarchivo1',)) !!}
              
                <div class="form-group">
                  <label for="Ruta Archivo">Nombre</label>
              <input id="fileok" name="fileok" type="file" class="file" multiple="true" enctype="multipart/form-data">
            </div>
            <div class="form-group">
              <button class="btn btn-primary">Cargar Archivo</button>
              <button class="btn btn-danger" type="reset">Reset</button>
            </div>
              {!!Form::close()!!} 

          <div class="progress">
            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" >
              <span class="sr-only"></span>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-1 hidden-xs"></div>
    </div>
    </section>
    <!-- /.content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@push('evaluacion360periodo')
<script>
 
    var table = $('#listadoperiodoevaluacion').DataTable( {
         "paging":   true,
         "ordering": false,
         "info":     true,
        // "order":     [[0,"desc"]],
         lengthChange: true,
         //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
     } );  


  </script>
@endpush
@stop