@extends('layouts.master')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="matrizp" target="_blank"><i class="fa fa-id-card-o">Matriz de Proyectos</i></a></li>
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Panel de Control</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <input type="hidden" name="carea" value="{{$datosempleado->carea}}" id="carea">
      <input type="hidden" name="disciplina" value="{{isset($disciplina)==1?$disciplina->cdisciplina:'' }}" id="disciplina">
      <input type="hidden" name="cargo" value="{{$cargo->esjefaturaarea}}" id="cargo">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6" id="atajo_1" hidden>
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="fechahoy"></h3>

              <p>Hoja de Tiempo</p>
            </div>
            <div class="icon">
            <div id="reloj" style="min-width: 300px; max-width: 500px;" class="pull-right"></div>
              <!-- <i class="fa fa-calendar"></i> -->
            </div>
            <a href="" class="small-box-footer" id="htiempo">Ir &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" id="atajo_2" hidden>
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>0</h3>

              <p>Rendición de Gastos</p>
            </div>
            <div class="icon">
              <i class="fa fa-dollar"></i>
            </div>
            <a href="{{ url('int?url=listaGastosProyecto') }}" class="small-box-footer">Ir &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" id="atajo_3" hidden>
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$lecaprendidas}}</h3>

              <p>Lecciones Aprendidas</p>
            </div>
            <div class="icon">
              <i class="fa fa-list"></i>
            </div>
            <a href="{{ url('int?url=leccionesAprendidas') }}" class="small-box-footer">Ir &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6" id="atajo_4" hidden>
          
          <div class="small-box bg-red">
            <div class="inner">
              <h3>0</h3>

              <p>Performance</p>
            </div>
            <div class="icon">
              <i class="fa fa-lock"></i>
            </div>
            <a href="{{ url('performance') }}" class="small-box-footer">Ir &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <!-- /.row -->
      <div class="col-lg-12" id="widgets_5" hidden>
        <select id="listaproyec" class="form-control">
          <option value="todos">Todos los proyecto</option>
          @foreach ($tproyectos as $key => $value)
          <option data-finicio="{{ $value->finicio }}" data-fcierre="{{ $value->fcierre }}" value="{{ $value->cproyecto }}" >{{ $value->codigo }} {{ $value->nombre }}</option>
          @endforeach
        </select>
      </div>

      <br><br><br><br>
      <!-- Main row -->

      <div class="row">
        <!-- Left col -->
        <section class="col-lg-6 connectedSortable" id="widgets_1" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-calendar"></i>
              <h3 class="box-title">Calendario</h3>
                
                <select id="fechastodas" class="form-control">
                  <option value="all">Fechas de control</option>
                  @foreach ($fechastodas as $key => $value)
                  <option value="{{ $value->descripcion}}">{{ $value->descripcion}}</option>
                  @endforeach
                </select>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body" id="calendarioentre">
              <div class="box-body no-padding">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycal">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo calendario...</h3>
                    </center>
                  </span>
                </div>
                <div id="calendarfull" style="font-size: 12px;"></div>
              </div>
            </div>
            
            <div class="box-footer clearfix">
              <ul class="fc-color-picker">
                <li><a style="font-size:0.7em;color:red"><i class="fa fa-square"></i></a><span style="font-size:0.7em;"> Fechas vencidas </span></li>
                <li><a style="font-size:0.7em;color:yellow"><i class="fa fa-square"></i></a><span style="font-size:0.7em;"> Fechas por vencer (a 1 sem.) </span></li>
                <li><a style="font-size:0.7em;color:green"><i class="fa fa-square"></i></a><span style="font-size:0.7em;"> Fechas a tiempo (mas de 1 sem.) </span></li>
                </ul>
            </div>
          </div>

        </section>

        <section class="col-lg-6 connectedSortable" id="widgets_2" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-list-ol"></i>
              <h3 class="box-title">Bandeja de Entregables</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>

            <div class="box-body">
              <div class="table-responsive">
                @include('partials.ruteotable')
              </div>
            </div>
            <div class="box-footer clearfix">
            </div>
          </div>
        </section>
      
        <section class="col-lg-6 connectedSortable" id="widgets_4" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-list-ol"></i>
              <h3 class="box-title">Lista de equipo de proyecto</h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table table-responsive" id="tablaproyectoslistaequipo">
                <div class="overlay" style="display:none; color:#0069aa" id="overlayeq">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Listando equipo...</h3>
                    </center>
                  </span>
                </div>
                @include('partials.equipoproyecto',array('equipo' => (isset($equipo)==1?$equipo:array())) ) 
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>

        <section class="col-lg-6 connectedSortable" id="widgets_3" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('curva-s') }}">Curva S - Tiempo</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycarper">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('proyecto.performance.curva-s')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>
  
        <section class="col-lg-6 connectedSortable" id="widgets_8" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('curva-s') }}">Curva S - Costos</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycarper">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('proyecto.performance.curvas_costos')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>

        <section class="col-lg-6 connectedSortable" id="widgets_9" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('curva-s') }}">Indicadores de gestión</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycarper">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('proyecto.performance.curvas_indicadores')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>

        <section class="col-lg-6 connectedSortable" id="" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('grafico-semanal') }}">Proyección CD</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycarper">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('cdocumentario.entregablesxcd')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>

        <section class="col-lg-6 connectedSortable" id="widgets_7" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('grafico-semanal') }}">Productividad y Cargabilidad Semanal del área por profesional</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycartipo">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('proyecto.CargabilidadSemanal.graficoproductividadsemanal')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>

        

        <section class="col-lg-6 connectedSortable" id="widgets_6" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('indicador-semanal-general') }}">Control de horas semanal del área por profesional</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycarper">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('proyecto.CargabilidadSemanal.graficocargabilidadpersonal')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>

        <section class="col-lg-6 connectedSortable" id="widgets_11" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('indicador-mensual-general') }}">Control de horas mensual del área por profesional</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycartipo">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('proyecto.CargabilidadMensual.GraficosCM.03-GraficoMensualporUsuario')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>

        <section class="col-lg-6 connectedSortable" id="widgets_12" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="{{ url('grafico-mensual') }}">Productividad y Cargabilidad mensual del área por profesional</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaycarper">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gráfica...</h3>
                    </center>
                  </span>
                </div>
                @include('proyecto.CargabilidadMensual.GraficosCM.07-GraficoMensualProductividadCargabilidad')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>




        <section class="col-lg-12 connectedSortable" id="widgets_10" hidden>
          <div class="box box-primary">
            <div class="box-header with-border">
              <i class="fa fa-bar-chart-o"></i>
              <h3 class="box-title"><a href="#">Roadmap entregables</a></h3>

              <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="table-responsive">
                <div class="overlay" style="display:none; color:#0069aa" id="overlaygantt">
                  <i class="fa fa-refresh fa-spin" style="color:#0069aa"></i>
                  <br><br><br>
                  <span>
                    <center>
                      <h3>Construyendo gantt...</h3>
                    </center>
                  </span>
                </div class="table-responsive">
                @include('proyecto.gantt_entregable')
              </div>
            </div>
            <div class="box-footer clearfix">
              
            </div>
          </div>
        </section>
        
      </div>

    </section>
  </div>

@include('partials.modalruteoLE')
@include('partials.modalcomentariosLE')

<div class="control-sidebar-bg"></div>

@stop
@section('js')


<script src="{{ url('plugins/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ url('plugins/fullcalendar/locale/es.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/gcal.js"></script>
<script src="https://code.highcharts.com/gantt/highcharts-gantt.js"></script>
<script src="https://code.highcharts.com/gantt/modules/exporting.js"></script>
<script src="https://code.highcharts.com/stock/modules/stock.js"></script>
<script src="https://code.highcharts.com/stock/modules/exporting.js"></script>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/series-label.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/highcharts-more.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>

<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>

<script src="{{ url('js/ruteoedtproyecto.js') }}"></script>
<script src="{{ url('js/reloj.js') }}"></script>
<script src="{{ url('js/curva.js') }}"></script>
<script src="{{ url('js/curva_tiempo.js') }}"></script>
<script src="{{ url('js/curva_costos.js') }}"></script>
<script src="{{ url('js/curva_indicadores.js') }}"></script>
<script src="{{ url('js/graficopcionescargabilidad.js') }}"></script>
<script src="{{ url('js/graficasemana.js') }}"></script>
<script src="{{ url('js/graficonofacturable.js') }}"></script>
<script src="{{ url('js/graficaporusuario.js') }}"></script>
<script src="{{ url('js/graficaadmin.js') }}"></script>
<script src="{{ url('js/graficointernos.js') }}"></script>
<script src="{{ url('js/grafica3semanas.js') }}"></script>
<script src="{{ url('js/graficaproductividadcargabilidad.js') }}"></script>
<script src="{{ url('js/graficaentregablexcd.js') }}"></script>
<script src="{{ url('js/ganttentregable.js') }}"></script>
<script src="{{ url('js/cargaMensual/graficopcionescargabilidadMensual.js') }}"></script>
<script src="{{ url('js/cargaMensual/graficoMensualporUsuario.js') }}"></script>
<script src="{{ url('js/cargaMensual/graficoMensualProductividadCargabilidad.js') }}"></script>


<script>
// carga inicial
$(function (){

  $('.atajo').each(function() {
  var id = $(this).val();
    if ($(this).prop('checked')){
      $('#atajo_'+id).show()
    }else{
      $('#atajo_'+id).hide();
    }
  });

  $('.widgets').each(function() {
  var id = $(this).val();
    if ($(this).prop('checked')){
      $('#widgets_'+id).show()
    }else{
      $('#widgets_'+id).hide();
    }
  });

  $('.widgets').click(function(){
    var id = $(this).val();
    var check = $(this).prop('checked');
    // console.log("atajo",id,check);
    var arrayDashboard=validar_check_widgets();
    seleccionar(arrayDashboard,3);
    if (check == true) {$('#widgets_'+id).show();} else {$('#widgets_'+id).hide();}
  });

  $('.atajo').click(function(){
    var id = $(this).val();
    var check = $(this).prop('checked');
    // console.log("atajo",id,check);
    var arrayAtajo=validar_check_atajo();
    seleccionar(arrayAtajo,2);
    if (check == true) {$('#atajo_'+id).show();} else {$('#atajo_'+id).hide();}

  });

  

  var hoy = moment().format('YYYY-MM-DD');
  var carea = $('#carea').val();
  var disciplina = $('#disciplina').val();
  var ruteo = $('#verdetallerruteo').val();
  var fecha_anterior = moment().subtract(7, 'days');
  var fecha_7dias_anterior = moment(fecha_anterior).format("DD/MM/YYYY");
  var fecha_21anterior = moment().subtract(21, 'days');
  var fecha_21dias_anterior = moment(fecha_21anterior).format("DD/MM/YYYY");
  var fecha_anterior_mes = moment().subtract(1, 'months');
  var fecha_1mes_anterior = moment(fecha_anterior_mes).format("YYYY/MM");
  var proy = '';

  $("#fechahoy").text(moment().format('DD-MM-YY'));
  $('.highcharts-background').css('opacity', '0');
  $('.highcharts-root').css('width', '500');



  $('#fechastodas').chosen({
    width: "100%",

  });

  $('#fechastodas').on('change',function(){
    $('#calendarfull').fullCalendar('rerenderEvents');
  });

  $('#listaproyec').chosen({
    width: "100%", 
    placeholder_text_single : "Seleccione proyecto",
  });

  var dt = $('#listatablepry').DataTable( {
    "paging":   false,
    "ordering": true,
    "info":     true,
    lengthChange: false,
    scrollY:        "700px",
    scrollX:        false,
    scrollCollapse: true,
    paging:         false,
    // fixedColumns:   {
    //     leftColumns: 2
    // }
    "order": [[1, 'asc']],
    "language": {
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "Sin Resultados",
        "info": "_MAX_ proyectos",
        "infoEmpty": "No existe registros disponibles",
        "infoFiltered": "(filtrado de un _MAX_ total de registros)",
        "search":         "Buscar:",
        "processing":     "Procesando...",
        "paginate": {
            "first":      "Inicio",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "loadingRecords": "Cargando...",
      },
    } ); 

  $('#listaproyec').on('change',function () {

    var cproyecto = $("#listaproyec option:selected").val();
    var finicio = $("#listaproyec option:selected").data('finicio');
    var fcierre = $("#listaproyec option:selected").data('fcierre');

    if (finicio == '') {
      swal("El proyecto no cuenta con fecha de inicio");
    }
    else if (fcierre == '') {
      fcierre = hoy; swal("El proyecto no cuenta con fecha de cierre, se considerará la fecha de hoy para graficar la Cuerva S");
    }
    if (ruteo=='GP') {disciplina = 'Nuevo';} else {disciplina = disciplina;}

    vercurva(cproyecto,finicio,fcierre,disciplina);
    verequipoproyecto(cproyecto,'','');
    // console.log(cproyecto,finicio,fcierre,disciplina);
    dataentregble(cproyecto);
    dataentregblegantt(cproyecto);

  });

    $('.verequipoproyecto').click(function(e){
      // body...
      var cproyecto = $(this).parent().parent().parent().data('proyecto');
      // console.log(cproyecto);
      verequipoproyecto(cproyecto,e.pageX,e.pageY);
  });

    $('.vercurva').click(function(e) {
      var cproyecto = $(this).parent().parent().parent().data('proyecto');
      var finicio = $(this).parent().parent().parent().data('finicio');
      var fcierre = $(this).parent().parent().parent().data('fcierre');
      // console.log(cproyecto);
      if (finicio == '') {
        swal("El proyecto no cuenta con fecha de inicio");
      }
      else if (fcierre == '') {
        fcierre = hoy; swal("El proyecto no cuenta con fecha de cierre, se considerará el día de hoy");
      }
      if (ruteo=='GP') {disciplina = 'Nuevo';} else {disciplina = disciplina;}
      vercurva(cproyecto,finicio,fcierre,disciplina);
      $('tr[data-proyecto="' + proy + '"]').css("background-color", "");
      $('tr[data-proyecto="' + cproyecto + '"]').css('background-color', 'gray');

       proy = cproyecto;
      // console.log(cproyecto,finicio,fcierre,'Nuevo',moment().format('YYYY-MM-DD'));
    });

// $('.verequipoproyecto').mouseout(function(){
//     $(this).css('z-index', 8);
//             $('.equipos').remove();
//       })

// $("#tablequipo").tableHeadFixer();

  $("#htiempo").click(function() {
    postUrlTimeSheet('{{ csrf_token() }}');
  });

  // console.log(carea,"semanacargabilidad",moment().subtract(7, 'days'));
  dataentregble('todos');
  dataentregblegantt('todos');

// graficos de cargabilidad semanal
  graficoporusuario(carea,fecha_7dias_anterior);
  graficaCargaProductividad(carea,fecha_7dias_anterior);
// graficos de cargabilidad mensual  
  graficomensualporusuario(carea,fecha_1mes_anterior);
  graficamensualprodCargabilidad(carea,fecha_1mes_anterior);

});
// fin de la carga inicial

function vercurva(cproyecto,finicio,fcierre,disciplina){
  var finicio = moment(finicio).format("DD/MM/YYYY");
  var fcierre = moment(fcierre).format("DD/MM/YYYY");
  $('#curva-s').empty();
  $('#curvarate-s').empty();
  $('#overlaycur').show();
  $.ajax({
    url: 'curva_s_array/'+cproyecto,
    type: 'GET',
    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    data: {
      'cproyecto': cproyecto,
      'fdesde': finicio,
      'fhasta': fcierre,
      'disciplina': disciplina,
    },
  })
  .done(function(data) {
    $('#overlaycur').hide();
    console.log(data);
    chart_curva(data);
    chart_curvas_costos(data);
    chart_indicadores(data);
  })
  .fail(function() {
    console.log("error");
    $('#overlaycur').hide();
  })
  .always(function() {
    console.log("complete");
    $('#overlaycur').hide();
  });
  
}


function verequipoproyecto(cproyecto,x,y){
  $('#tablaproyectoslistaequipo').empty();
  $('#overlayeq').show();
  $.ajax({
    url: 'verequipoproyecto',
    type: 'GET',
    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    data: {'cproyecto': cproyecto},
  })
  .done(function(data) {
    // console.log("verequipoproyecto",data); 
    $('#tablaproyectoslistaequipo').html(data);
    $('#overlayeq').hide();
  })
  .fail(function() {
    console.log("error");
    $('#overlayeq').hide();
  })
  .always(function() {
    console.log("complete");
    $('#overlayeq').hide();
  });
  
};

function dataentregble(cproyecto) {
  $('#calendarfull').fullCalendar('destroy');
  $('#overlaycal').show();
  $.ajax({
    url: 'calendario',
    type: 'GET',
    // dataType: 'json',
    data: {'cproyecto': cproyecto},
  })
  .done(function(data) {
    // console.log("calendario",data);
    $('#overlaycal').hide();
    calendario(data);
    // ganttentregable(data);
  })
  .fail(function() {
    $('#overlaycal').hide();
    console.log("error");
  })
  .always(function() {
    $('#overlaycal').hide();
    console.log("complete");
  });
};

function dataentregblegantt(cproyecto) {
  $('#gantt-entregable').empty();
  $('#overlaygantt').show();
  $.ajax({
    url: 'entregblegantt',
    type: 'GET',
    // dataType: 'json',
    data: {'cproyecto': cproyecto},
  })
  .done(function(data) {
    console.log("entregblegantt",data);
    $('#overlaygantt').hide();
    // calendario(data);
    ganttentregable(data);
  })
  .fail(function() {
    $('#overlaygantt').hide();
    console.log("error");
  })
  .always(function() {
    $('#overlaygantt').hide();
    console.log("complete");
  });
};


var fecha_origen = '';
var fecha_destino = '';
var editar = false;

  function calendario(data) {

    if (data[1]==="true") {
      editar = true;
    }else{
      editar = false;
    }

    console.log("casisale",data);
    $('#calendarfull').fullCalendar({

      locale: 'es',
      // themeSystem: 'jquery-ui',
      // themeSystem: 'bootstrap3',
      // themeSystem: 'bootstrap4',
      weekNumbers: true,
      weekNumbersWithinDays: true,
      eventLimit: true,
      droppable : editar, // this allows things to be dropped onto the calendar !!!
      editable: editar,
      // defaultView: 'listMonth',
      header    : {
        left  : 'prev,next today',
        center: 'title',
        right : 'month,agendaWeek,agendaDay,listMonth'
      },
      businessHours: [ // specify an array instead
        {
          dow: [ 1, 2, 3,4 ], // Monday, Tuesday, Wednesday, Thursday
          start: '09:00', // 9am
          end: '19:00' // 7pm
        },
        {
          dow: [ 5 ], // Friday
          start: '09:00', // 9am
          end: '13:00' // 1pm
        },
        // {
        //   data.dow,
        //   data.start,
        //   data.end,
        // }
      ],
      // businessHours: data,
      timeFormat: 'H(:mm)',
      // googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
      // eventSources: [{googleCalendarId: 'en.usa#holiday@group.v.calendar.google.com'}],
      events    : data,

      eventDragStart: function(event) {
        fecha_origen = event.start.format();
        // console.log("fecha_origen",event.start.format());
      },
      eventDrop: function(event,delta,revertFunc) { 
        fecha_destino = event.start.format();
        id = event.id;
        entregable = event.entregable;
        orden = event.orden;
        // console.log("event",event); 
        // grabarfechaentregable(id,entregable,orden);
            $.ajax({
            url: 'grabarfechaentregablecalendario',
            type: 'GET',
            data: {'id': id, 'fecha_origen':fecha_origen, 'fecha_destino':fecha_destino, 'entregable':entregable, 'orden':orden},
            })
            .done(function(data) {
              // console.log("grabarfechaentregablecalendario",data);

              if (data == "false") {
                swal("Fecha no actualizada");
                revertFunc();
                return false;
              } else {
                dataentregblegantt(event.cproyecto);
                swal("Fecha actualizada");
                $('#calendarfull').fullCalendar('updateEvent', event);
                return false;
              }
            })
            .fail(function() {
              console.log("error");
            })
            .always(function() {
              console.log("complete");
            });
      
      },
      eventRender: function(event, element, view){
          return ['all', event.nombre].indexOf($('#fechastodas').val()) >= 0;
      },
      eventClick: function (data, event, view) {
        // console.log("hice click");
        $('.tooltiptopicevent').remove();
        if (data.entregable) {
        detalle = '<div class="box box-widget widget-user hidden-md hidden-lg tooltiptopicevent" style="width:auto;height:auto;border-radius: 5px 5px 5px 5px;position:absolute;z-index:10001;padding:10px 10px 10px 10px;line-height: 200%;">'
        detalle += '<button type="button" class="btn btn-box-tool pull-right" data-widget="remove" onclick=cerrar() style="color:black"><i class="fa fa-times"></i></button>'
              detalle += '<div class="widget-user-header" style="height:auto; padding:10px 10px 10px 10px; background:'+data.color+';color:'+data.textColor+'">';
                detalle += '<h3 class="widget-user-username">'+data.codpy+'-'+data.nombre+'</h3>';
                detalle += '<h4 class="widget-user-desc">'+data.descripcion+'</h4>';
                detalle += '<h5 class="widget-user-desc">Cod. AND.: '+data.codigo+'</h5>';
                detalle += '<h5 class="widget-user-desc">Cod. CLI.: '+data.codigocliente+'</h5>';
                //detalle += '<h6 class="widget-user-desc">'+data.nombre+'</h6>';
              detalle += '</div>';
              detalle += '<div class="box-footer no-padding">';
                detalle += '<ul class="nav nav-stacked">';
                  detalle += '<li><a href="#">Responsable<br>interno<span class="pull-right badge bg-blue">'+data.resp+'</span></a></li>';
                  detalle += '<li><a href="#">Elaborador<span class="pull-right badge bg-aqua">'+data.elab+'</span></a></li>';
                  detalle += '<li><a href="#">Revisor<span class="pull-right badge bg-green">'+data.revi+'</span></a></li>';
                  detalle += '<li><a href="#">Aprobador<span class="pull-right badge bg-red">'+data.apr+'</span></a></li>';
                detalle += '</ul>';
              detalle += '</div>';
            detalle += '</div>';

            $("body").append(detalle);
            // $(this).tooltip(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            // }).tooltip(function (e) {
                $('.tooltiptopicevent').css('top', '5%');
            // });
          }else{
            return false;
          }
      },
      eventMouseover: function (data, event, view) {

            if (data.entregable) {

            detalle = '<div class="box box-widget widget-user tooltiptopicevent" style="width:auto;height:auto;border-radius: 5px 5px 5px 5px;position:absolute;z-index:10001;padding:10px 10px 10px 10px;line-height: 200%;">'
              detalle += '<div class="widget-user-header" style="background:'+data.color+';color:'+data.textColor+'">';
                detalle += '<h3 class="widget-user-username">'+data.codpy+'-'+data.nombre+'</h3>';
                detalle += '<h4 class="widget-user-desc">'+data.descripcion+'</h4>';
                detalle += '<h5 class="widget-user-desc">Cod. AND.: '+data.codigo+'</h5>';
                detalle += '<h5 class="widget-user-desc">Cod. CLI.: '+data.codigocliente+'</h5>';
                //detalle += '<h6 class="widget-user-desc">'+data.nombre+'</h6>';
              detalle += '</div>';
              detalle += '<div class="box-footer no-padding">';
                detalle += '<ul class="nav nav-stacked">';
                  detalle += '<li><a href="#">Responsable<br>interno<span class="pull-right badge bg-blue">'+data.resp+'</span></a></li>';
                  detalle += '<li><a href="#">Elaborador<span class="pull-right badge bg-aqua">'+data.elab+'</span></a></li>';
                  detalle += '<li><a href="#">Revisor<span class="pull-right badge bg-green">'+data.revi+'</span></a></li>';
                  detalle += '<li><a href="#">Aprobador<span class="pull-right badge bg-red">'+data.apr+'</span></a></li>';
                detalle += '</ul>';
              detalle += '</div>';
            detalle += '</div>';

            $("body").append(detalle);
            $(this).mouseover(function (e) {
                $(this).css('z-index', 10000);
                $('.tooltiptopicevent').fadeIn('500');
                $('.tooltiptopicevent').fadeTo('10', 1.9);
            }).mousemove(function (e) {
                $('.tooltiptopicevent').css('top', e.pageY - 250);
                $('.tooltiptopicevent').css('left', e.pageX + 20);
            });

            } else {
              return false;
            }

        },
        eventMouseout: function (data, event, view) {
            $(this).css('z-index', 8);
            $('.tooltiptopicevent').remove();
        },

    });
  };

  function cerrar(){
    $('.tooltiptopicevent').remove();
  };

  function cerrarequipo(){
    $('.equipos').remove();
  };

  function validar_check_atajo(){
  array = [];
  $('.atajo').each(function() {
  var id = $(this).val();
    if ($(this).prop('checked')){
      array.push($(this).val());
    }
  });
  console.log(array);
  return array;
 };

 function validar_check_widgets(){
  array = [];
  $('.widgets').each(function() {
  var id = $(this).val();
    if ($(this).prop('checked')){
      array.push($(this).val());
    }
  });
  console.log(array);
  return array;
 };

  function seleccionar(option,tipo){
  $.ajax({
    url: 'seleccionarcolumnas',
    type: 'GET',
    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    data: {'option': option,'tipo': tipo},
  })
  .done(function(data) {
    console.log("seleccionar",data);
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
};

function entregablexcd(fecha){
  $.ajax({
    url: 'entregablecd',
    type: 'GET',
    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    data: {'fecha': fecha},
  })
  .done(function(data) {
    console.log("success");
    graficaentregablexcd(data);
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
};


$(".generarTR").click(function(){

  var entregables= $(this).data('entregables');
  var cproyecto=  $(this).parent().parent().parent().data('proyecto');

  var obj = { entregables: entregables}

  var url = BASE_URL + '/generar_transmittal_nuevo/'+cproyecto;


      $.get(url,obj)
      .done(function( data ) { 

        // console.log(data);

        // $('#resultado').html(data);

         //$(location).attr('href',url)


         
      });


})

</script>
@endsection

