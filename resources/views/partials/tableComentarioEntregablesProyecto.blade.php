<div class="box box-primary direct-chat direct-chat-primary">
  <div class="box-header with-border" style="background-color: #0069AA; color: white;">
    <h3 class="box-title" >Comentarios: {{ $codigo_ent }}</h3>
    <input type="hidden" name="cproyectoentregable" value="{{ $cproyectoentregable }}" id="cproyectoentregable">
    <div class="box-tools pull-right">
      <span data-toggle="tooltip" data-placement="left" class="badge bg-green" data-original-title="{{ $contador }} Comentarios">{{ $contador }}</span>
      <!-- <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button> -->
      <!-- <button type="button" class="btn btn-box-tool" data-toggle="tooltip" title="" data-widget="chat-pane-toggle" data-original-title="Contacts"><i class="fa fa-comments"></i></button> -->
      <button type="button" class="btn btn-box-tool" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button>
      <!-- <button type="button" class="close btn btn-box-tool" data-dismiss="modal" aria-label="Close"><i class="fa fa-times"></i></button> -->
    </div>
  </div>

  <div class="box-body">
    <div style="overflow-y: auto; max-height: 600px;">
      @foreach($trazabilidad as $key => $value)

        @if($value['cpersona'] == false)
        <div class="direct-chat-msg left">
          <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-left">{{ $value['abreviatura'] }}</span>
            <span class="direct-chat-timestamp pull-right"><i class="fa fa-clock-o"></i>&nbsp;{{ $value['fecha'] }}</span>
          </div>
          <img src="{{ url('images/'.$value['imagen'].'.jpg') }}"  class="direct-chat-img" alt="message user image">
          <div class="direct-chat-text">
            {{ $value['comentario'] }}
          </div>
        </div>

        @endif
        @if($value['cpersona'] == true)

        <div class="direct-chat-msg right">
          <div class="direct-chat-info clearfix">
            <span class="direct-chat-name pull-right">{{ $value['abreviatura'] }}</span>
            <span class="direct-chat-timestamp pull-left">{{ $value['fecha'] }}&nbsp;<i class="fa fa-clock-o"></i></span>
          </div>
          <img src="{{ url('images/'.$value['imagen'].'.jpg') }}"  class="direct-chat-img" alt="message user image">
          <div class="direct-chat-text">
            {{ $value['comentario'] }}
          </div>
        </div>
        @endif

      @endforeach
    </div>

    <div class="box-footer" style="margin-top: 15px;">
        <div class="input-group">
          <input type="text" name="comentarioentregable" placeholder="Ingrese un comentario  ..." class="input-sm" maxlength="150" value="{{ $comenta }}" id="comentarioentregable" onKeyDown="contadorcaracteres()" onKeyUp="contadorcaracteres()" style="width: 100%; height: 35px;">
          <span class="input-group-btn">
            <button type="button" class="btn btn-primary" id="guardarcomentrio" style="height: 34px;">Guardar</button>
          </span>
        </div>
          <label id="result">Máximo 150 caracteres </label>
          <br>
          <label><input type="checkbox" name="checkbox_todos_comentario[]" title=" Agregar a todos los entregables" id="checkbox_todos_comentario">&nbsp;&nbsp;&nbsp;Agregar a todos los entregables</label>
    </div>
  </div>
</div>

<script type="text/javascript">

  
  function contadorcaracteres() {
    $("#result").text($("#comentarioentregable").val().length + "/ Máximo 150 caracteres"); 
  };

  $("#guardarcomentrio").click(function() {

    var comentario = $("#comentarioentregable").val();
    var cproyectoentregable = $("#cproyectoentregable").val();
    var check = $('#checkbox_todos_comentario').prop('checked');
    // alert(check);
    var entregables=[];
    var contador_entregable = 0;

    if (comentario != '' && check == false ) { 
      comentariosave(cproyectoentregable,comentario,entregables);
    }else if (comentario != '' && check == true ) { 
      $('.entregable_ruteo').each(
      function() {
        entregables[contador_entregable] =$(this).data('entregable');
        contador_entregable++;
      });
      comentariosave(cproyectoentregable,comentario,entregables);
    }else {
     swal("Ingrese un comentario");
    }


    
  });

  function comentariosave(cproyectoentregable,comentario,entregables){
     // alert(entregables)
     // console.log("comentario",entregables);

    $.ajax({
      url: 'actualizarruteo',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'cproyectoentregables':cproyectoentregable, 'comentario': comentario, 'entregables':entregables},
    })
    .done(function(data) {
      console.log("success");
      $('#modalcometarioLE').modal('toggle');
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
  };

  

</script>