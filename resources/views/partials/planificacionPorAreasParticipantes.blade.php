<div class="modal-content">
  <div class="well">
    <table class="table table-hover" id="tacumuladas">
      <thead>
        <th>
          Profesional
        </th>  
        <?php 
        $i=0;
        $j=0;
        ?>
        @if(isset($dias))
        @foreach($dias as $d)
        <th>{{ $d[2] }} <br />{{ $d[1] }}</th>   

        <?php 
        $i++;
        ?>

        @if($i%7==0)



        <th>Hoja de <br /> tiempo
                          <!--<span class="label label-default">Hoja de<br />
                            <span class="label label-default">HT</span> -->

                          </th>
                          @endif

                          @endforeach                      
                          @endif

                          <th style="text-align:right">
                            Total
                          </th>                                                            
                        </thead>
                        <tbody>

                          @if(isset($planiAcu)==1 )
                          @foreach($planiAcu as $pla)
                          <?php $total=0; ?>
                          <?php $partic=0; ?>
                          <tr>
                            <td>
                             {{ $pla['nombre'] }}

                             <?php $partic = $pla['cpersona_ejecuta'] ; ?>
                           </td>
                           <?php $semana=''; ?>
                           @if(isset($dias))
                           @foreach($dias as $d)                        
                           <td>

                            <?php 
                            $key = str_replace("-","",$d[0]);  
                            $valor = (isset($pla[$key]['horas'])==1?$pla[$key]['horas']:'');
                            echo $valor;
                            //$total +=$valor;


                            if(strlen($valor)>0){
                             $total +=$valor;  
                            //dd($valor,$total);
                           }

                           $j++;             



                           ?>
                         </td>

                         @if($j%7==0)

                         <td >  
                          <button name='htparticipante' type="button" onclick="goEditar({{$pla['cpersona_ejecuta']}},'{{ $d[0] }}')" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="right" title="Ver Hoja de tiempo de {{ $pla['nombre'] }} ">
                           <span class="glyphicon glyphicon-eye-open"></span></button>
                         </td>
                         @endif

                         @endforeach
                         @endif


                         <td style="text-align:right"><?php echo number_format($total,2);?>

                         </td> 


                       </tr>
                       @endforeach
                       @endif
                     </tbody>
                   </table>
                 </div>
               </div>
             </div>

             <script> 


              function goEditar(cpersona,fecha){
                $.ajax({
                  url:'verReporteHorasParticipante',
                  type: 'POST',
                  data : {
                    '_token':'{{ csrf_token() }}',
                    'cpersona' : cpersona,
                    'fecha': fecha,
                  },
                  beforeSend: function(){
                    $('#div_carga').show(); 
                  },
                  success: function(data){                  
                    $('#div_carga').hide(); 
                    $("#horas").html(data);
                    $("#verHT").modal("show");

                  },            
                });            
              }        

            </script>       






