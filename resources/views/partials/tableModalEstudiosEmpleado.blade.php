<table class="table table-striped">
                      	<thead>
                        <tr>
                        	<td class="clsCabereraTabla">Nivel</td>
                            <td class="clsCabereraTabla">Profesión</td>
                            <td class="clsCabereraTabla">Institución</td>
                            <td class="clsCabereraTabla">Estado</td>
                            <td class="clsCabereraTabla">Colegiatura</td>
                            <td class="clsCabereraTabla">F Ingreso</td>
                            <td class="clsCabereraTabla">F Término</td>                                    
                            <td class="clsCabereraTabla">Acción</td>
                          </tr>  
                         </thead>
                         <tbody>
                            @if(isset($tpersonadatosestudio)==1)
                            @foreach($tpersonadatosestudio as $te)
                         	<tr>
                            <td class="clsAnchoTabla">{{ $te->nivel }}</td>
                            <td class="clsAnchoTabla">{{ $te->cespecialidad }}</td>
                            <td class="clsAnchoTabla">{{ $te->cinstitucion }}</td>
                            <td class="clsAnchoTabla">{{ $te->estado }}</td>
                            <td class="clsAnchoTabla">{{ $te->colegiatura }}</td>
                            <td class="clsAnchoTabla">{{ $te->fingreso }}</td>
                            <td class="clsAnchoTabla">{{ $te->ftermino }}</td>
                            <td class="clsAnchoTablaMax">
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="Inicio">
                                </div>             
                            </td>
                            <td class="clsAnchoTablaMax">
                                  <div class="input-group date">
                                      <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                      </div>
                                      <input type="text" class="form-control pull-right" id="datepicker" placeholder="Fin">
                                    </div>             
                            </td>            
                            <td class="clsAnchoDia">
                				    <a href="#" onclick="eliminarEmpleadoEstudio('{{ $te->cpersonaestudios }}')" data-toggle="tooltip" data-container="body" title="Eliminar Estudio">
                                <i class="fa fa-trash" aria-hidden="true"></i></a>            
                            </td>            
                            </tr>  
                            @endforeach    
                            @endif         
                         </tbody>
</table>  