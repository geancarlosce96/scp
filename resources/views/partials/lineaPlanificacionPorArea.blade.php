            <?php 
            
            //$i=$nroFilaI - 1;
            
            ?>
              @if(isset($planificaciones))
                @foreach($planificaciones as $pla)
                    <?php $i =$pla['indice'];  ?>
                    <tr id="row_{{ $i}}">
                        <td>
                        @if(strlen($pla['cpersona_ejecuta'])>0)
                            @if($pla['tipo']=='1' || $pla['tipo']=='2')
                            <a href="#" onclick="clonarfila('{{ ($i) }}')" class="fa fa-plus"></a><br />
                            @endif 
                            <a href="#" onclick="eliminarFila('{{ ($i) }}')" class="fa fa-trash"></a>
                        @else 
                            <a href="#" onclick="agregarUsuario('{{ $pla['cproyectoactividades'] }}','{{ $pla['cproyecto'] }}','{{ $i }}')" class="fa fa-child"></a>                        
                        @endif
                                                
                        

                        <input type="hidden" name="plani[{{ $i }}][0]" value="{{ $pla['cproyectoactividades'] }}" />
                        <input type="hidden" name="plani[{{ $i }}][1]" value="{{ $pla['cproyecto'] }}" />
                        <input type="hidden" name="plani[{{ $i }}][2]" value="{{ $pla['cpersona_ejecuta'] }}" />
                        <input type="hidden" name="plani[{{ $i }}][3]" value="{{ $pla['cpersona_ejecuta'] }}" />
                        <input type="hidden" name="plani[{{ $i }}][5]" value="{{ $pla['cproyectoejecucioncab'] }}" />
                        <input type="hidden" name="plani[{{ $i }}][6]" value="{{ $pla['cactividad'] }}" />
                        <input type="hidden" name="plani[{{ $i }}][7]" value="{{ $pla['tipo'] }}" />   
                        <input type="hidden" name="plani[{{ $i }}][9]" value="N" />                                             
                        </td>

                        @if($pla['tipo']=='1' || $pla['tipo']=='2')
                        <td ><input type="hidden" value="{{ $pla['des_act'] }}"></td>
                        @else
                        <td><b>{{ $pla['des_act'] }}</b></td>
                        @endif
                        
                        <td >

                            @if($pla['tipo']=='1')
                            <span class="label" style="background-color: #11B15B">FF</span>
                            @endif
                            @if($pla['tipo']=='2')
                            <span class="label" style="background-color: #CD201A">NF</span>
                            @endif
                            @if($pla['tipo']=='3')
                            <span class="label" style="background-color: #0069AA">AND</span>
                            @endif

                        </td>
                        <td  style="min-width:120px !important">
                            @if(strlen($pla['cpersona_ejecuta'])>0)
                            {!! Form::select('plani['.$i.'][8]',(isset($nofacturables)==1?$nofacturables:array()),(isset($pla['ctiponofacturable'])==1?$pla['ctiponofacturable']:null),array('id'=>'nfac_'.$i))!!}
                            @endif
                        </td>                        
                        <td>{{ $pla['horasPla'] }}</td>
                        <td>{{ $pla['horaseje'] }}</td>
                        <td>{{ $pla['porav'] }} </td>
                        <td  style="width:200px;padding:0px">
                            @if(strlen($pla['cpersona_ejecuta'])>0)
                            {{ $pla['cpersona_ejecuta_nom'] }}                
                            @endif
                        </td>
                        @if(isset($dias))
                            @foreach($dias as $d)
                            <?php 
                                $key = str_replace("-","",$d[0]);  
                                $valor = (isset($pla[$key]['horas'])==1?$pla[$key]['horas']:'');
                            ?>
                                <td>
                                <input type='text' name="plani[{{ $i }}][4][{{ $key }}][0]" id='h_{{ $i }}_{{ $key }}'  size="4" 
                                value="{{ (isset($pla[$key]['horas'])==1?$pla[$key]['horas']:'') }}" onchange="viewSobre('observa_{{ $i }}_{{ $key }}',this);" 
                                onkeypress="return soloNumeros(event,this);"
                                @if($dia_act>=$key)
                                    readonly
                                @endif                                  
                                />
                                <a href="#" onclick="viewObs('{{ $i }}','{{ $key }}')" id="observa_{{ $i }}_{{ $key }}" 
                                @if(strlen($valor)<=0)
                                    style="display:none"
                                @else
                                    @if($valor <=0)
                                    style="display:none"                        
                                    @endif
                                @endif             
                      
                                ><i class="fa fa-envelope-o" aria-hidden="true"></i></a>                               
                                <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][1]" id='e_{{ $i }}_{{ $key }}' value="{{ (isset($pla[$key]['cproyectoejecucion'])==1?$pla[$key]['cproyectoejecucion']:'') }}" />
                                <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][2]" id='f_{{ $i }}_{{ $key }}' value="{{ (isset($key)==1?$key:'') }}" />
                                <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][3]" id='t_{{ $i }}_{{ $key }}' value="{{ (isset($pla[$key]['estado'])==1?$pla[$key]['estado']:'') }}" />
                                <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][4]" id='obs_{{ $i }}_{{ $key }}' value="{{ (isset($pla[$key]['observacion'])==1?$pla[$key]['observacion']:'') }}" />                                
                                </td>
                            @endforeach
                        @endif
                        <td><!--<a href="#" data-toggle="tooltip" title="Duplicar Tarea">
                        <i class="fa fa-files-o" aria-hidden="true"></i></a>--></td>
                    </tr>
                @endforeach
            @endif
            <script>
                /*nroFila = <?php echo (isset($nroFila)==1?$nroFila:0) ;?>*/
            </script>
  
