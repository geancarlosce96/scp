<!-- Modal Buscar Propuesta-->
    <div id="searchPropuesta" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Propuesta</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tPropuesta" class="table" style="font-size: 12px" width="100%">
            <thead>
              <th>ID</th> 
              <th>Código</th>
              <th>Cliente</th>
              <th>Unidad Minera</th>
              <th>Nombre</th>
              <th>Disciplina</th>
              <th>Estado</th>
              <th>Revisión</th>
              <th>GP</th>
            
            </thead>
           
          </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Propuesta-->