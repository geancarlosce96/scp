<!-- Modal agregar Alergias-->
    <div id="addAler" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Alergias</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarAlergia','method' => 'POST','id' =>'frmalergia','class'=>'form-horizontal')) !!}

          {!! Form::hidden('calergias',(isset($alergias->calergias)==1?$alergias->calergias:''),array('id'=>'calergias')) !!}

                   
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        
          <div class="form-group">
            <label for="nombre" class="col-sm-2 control-label">ID</label>
              <div class="col-sm-8">
              {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('calergias',(isset($alergias->calergias)==1?$alergias->calergias:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'calergias') ) !!}     
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div> 
      

            <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
              <div class="col-sm-8">
              {!! Form::text('descripcion',(isset($descripcion->descripcion)==1?$descripcion->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'descripcion') ) !!}     
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Alergias-->
