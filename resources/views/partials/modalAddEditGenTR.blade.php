<!-- Modal -->
    <div id="viewDeta" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Item de Transmittal</h4>
          </div>
          <div class="modal-body">
            <div class="panel panel-default">
          <!-- Default panel contents -->
              
                    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
                    {!! Form::hidden('ctransmittalejedetalle','',array('id'=>'ctransmittalejedetalle')) !!}
                    <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Entregable de Proyecto</label>
                        <div class="col-sm-8">
                            
                            {!! Form::select('cproyectoentregables',(isset($tentregables)==1?$tentregables:array()),'',array('class' => 'form-control ','id'=>'cproyectoentregables')) !!}                  
                        </div>
                        <div class="col-sm-2">&nbsp;
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                        <div class="col-sm-8">
                            {!! Form::text('codigo','',array('class'=>'form-control input-sm','id'=>'codigo','placeholder'=>'Código')) !!}
                        </div>
                        <div class="col-sm-2">&nbsp;
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Descripcion</label>
                        <div class="col-sm-8">
                            {!! Form::text('descripcion','',array('class'=>'form-control input-sm','id'=>'descripcion','placeholder'=>'Descripcion')) !!}
                        </div>
                        <div class="col-sm-2">&nbsp;
                        </div>
                    </div>    

                    <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Revision</label>
                        <div class="col-sm-8">
                            {!! Form::text('revision','',array('class'=>'form-control input-sm','id'=>'revision','placeholder'=>'Revisión')) !!}
                        </div>
                        <div class="col-sm-2">&nbsp;
                        </div>
                    </div>                                    

                    <div class="form-group">
                        <label for="codigo" class="col-sm-2 control-label">Cantidad</label>
                        <div class="col-sm-8">
                            {!! Form::text('cantidad','',array('class'=>'form-control input-sm','id'=>'cantidad','placeholder'=>'Cantidad')) !!}
                        </div>
                        <div class="col-sm-2">&nbsp;
                        </div>
                    </div>  
                    <button type="button" class="btn btn-default" id="btnAddItem" >Grabar</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>

            </div>
          </div>
          <div class="modal-footer">
            
          </div>
        </div>

      </div>
    </div>  
<!-- fin modal -->