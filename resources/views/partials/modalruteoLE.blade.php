<div class="modal fade" id="modalruteoLE" tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog modal-sm" role="document" style="width: 80%">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: white;" aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="color: white">Entregables para ruteo</h4>
      </div>
      <div class="modal-body table-responsive" id="tablaEntregableruteo" style="max-height: 600px; background-color: white; padding: 0px;">
        @include('partials.tableRuteoEntregablesProyecto',array('entregables' => (isset($entregables)==1?$entregables:array())))
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnruteo">Aprobar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


