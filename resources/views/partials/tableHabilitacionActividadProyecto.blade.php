
  <table class="table-striped table-bordered table-hover" id="tableActividad" style="font-size:12px; width:100%;">
      <thead>
        <tr class="clsCabereraTabla" style="height: 31px">

        
        @if(empty($listaAct))
          <th class="text-center clsPadding" >Item</th>
          <th class="col-sm-6 text-center clsPadding" >Descripción de actividad</th>
          <th class="col-sm-6 text-center clsPadding" >Participantes</th>
        @endif
        @if(!empty($listaAct))
          <th class="col-sm-1 text-center">Item</th>
          <th class="col-sm-2 text-center">Descripción de actividad</th>        
          @foreach($participantes as $par)
          <th class="col-sm-1 text-center">{{ $par['usuario'] }}                      
          @endforeach
        @endif 

        </tr>
      </thead>
      @if(isset($listaAct))
      <tbody style="overflow:scroll">
      <?php $i=0; ?>
      @foreach($listaAct as $act)
        <?php $i++; ?>
        <tr
        @if(!empty($act['cactividad_parent']))
          class=""
        @endif
        > 
          <td>{{ $act['item'] }}</td>
          <td 
          @if(!empty($act['cactividad_parent']))
            
          @endif                      
          >{{ $act['descripcionactividad']}}</td>
          
          @foreach($participantes as $par)             
          <td class="text-center" >

          <input type="checkbox" name="chkacthabi[]" value="1_{{ $act['cproyectoactividades']}}_{{$par['cpersona']}}" id="chkacthabi"
            @foreach($actPadre as $p)
              @if($act['cproyectoactividades']==$p['cproyectoactividades'])      

              disabled
                  
                            
              @endif 
            @endforeach 
            @foreach($activhabil as $ah)
            <?php $var='1_'.$act['cproyectoactividades'].'_'.$par['cpersona'];?>
            @if($ah['activo']==$var)
              checked="checked"
            @endif
            @endforeach
             data-toggle="tooltip" data-placement="top" title="{{ $act['item'] }}-{{ $act['descripcionactividad'] }}:{{ $par['usuario'] }}"
          > 
          @foreach($activhabil as $ah)
            <?php $var='1_'.$act['cproyectoactividades'].'_'.$par['cpersona'];?>
            @if($ah['activo']==$var)
              <label style="display: none;">Habilitado</label>
            @endif
            @endforeach  
          @if(!empty($act['cactividad_parent']))                      
          @endif
          </td>
          @endforeach
        </tr>
      @endforeach
      </tbody>
      @endif
      <tfoot>
        <tr class="clsCabereraTabla" style="height: 31px">

        
        @if(empty($listaAct))
          <th class="text-center clsPadding" >Item</th>
          <th class="col-sm-6 text-center clsPadding" >Descripción de actividad</th>
          <th class="col-sm-6 text-center clsPadding" >Participantes</th>
        @endif
        @if(!empty($listaAct))
          <th class="col-sm-1 text-center">Item</th>
          <th class="col-sm-2 text-center">Descripción de actividad</th>        
          @foreach($participantes as $par)
          <th class="col-sm-1 text-center">{{ $par['usuario'] }}                      
          @endforeach
        @endif 

        </tr>
      </tfoot>
  </table>
