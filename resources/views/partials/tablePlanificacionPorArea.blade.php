<input type="hidden" name="semana_ini" value="{{ (isset($semana_ini)==1?$semana_ini:'') }}" id="semana_ini" />
<input type="hidden" name="semana_fin" value="{{ (isset($semana_fin)==1?$semana_fin:'') }}" id="semana_fin"/>
<input type="hidden" name="dia_act" value="{{ (isset($dia_act)==1?$dia_act:'') }}" id="dia_act"/>
<div class="panel-group" id="accordion_disciplinas"> 
@if(isset($proyectos)==1)
<?php $r=0; ?>
<?php $i=-1; ?>
@foreach($proyectos as $pry)
<?php $r++; ?>
            <br />
            <div class="panel panel-default">
                <div class="panel-heading">
                  <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion_disciplinas" href="#collapse{{ $r }}" >
                    {{ $pry['codigo'] }}/{{ $pry['uminera'] }}/
                    {{ $pry['nombre'] }} 
                    </a>
                  </h4>
                </div>            
                <div id="collapse{{ $r }}" class="panel-collapse collapse" style="overflow:scroll">
                    <table class="table table-striped" id="tablepry_{{ $pry['cproyecto'] }}">
                        <thead>
                        <tr class="clsCabereraTabla">
                        <th>#</th>
                        <th>Actividad</th>
                        <th>Tipo</th>
                        <th>Categoría</th>              
                        <th>Monto <br> presupuestado</th>
                        <th>Monto <br> ejecucado</th>
                        <th>Saldo</th>
                        <th>Profesional</th>
                    @if(isset($dias))
                    @foreach($dias as $d)
                    <th>{{ $d[2] }} <br />{{ $d[1] }}</th>
                    @endforeach
                    @endif
                        </tr>
                        </thead>
                        <tbody>
                        <?php $planificaciones = $pry['planificaciones']; ?>
                        @if(isset($planificaciones))
                            @foreach($planificaciones as $pla)
                                <?php $i=$pla['indice']; ?>
                                <tr id="row_{{ $i}}">
                                    <td>
                                    @if(strlen($pla['cpersona_ejecuta'])>0)
                                        @if($pla['tipo']=='1' || $pla['tipo']=='2')
                                            <a href="#" onclick="clonarfila('{{ ($i) }}')" class="fa fa-plus"></a><br />
                                        @endif
                                        <a href="#" onclick="eliminarFila('{{ ($i) }}')" class="fa fa-trash"></a>
                                    @else

                                      <a href="#" onclick="agregarUsuario('{{ $pla['cproyectoactividades'] }}','{{ $pla['cproyecto'] }}','{{ $i }}')" class="fa fa-child"

                                        @if(isset($actPadre))
                                            @foreach($actPadre as $p)
                                                @if($pla['cproyectoactividades']==$p['cproyectoactividades'])
                                                style="display: none;" 
                    
                                                @endif

                                            @endforeach

                                        @endif></a>
                                        
                                        
                                    @endif
                                    <input type="hidden" name="plani[{{ $i }}][0]" value="{{ $pla['cproyectoactividades'] }}" />
                                    <input type="hidden" name="plani[{{ $i }}][1]" value="{{ $pla['cproyecto'] }}" />
                                    <input type="hidden" name="plani[{{ $i }}][2]" value="{{ $pla['cpersona_ejecuta'] }}" />
                                    <input type="hidden" name="plani[{{ $i }}][3]" value="{{ $pla['cpersona_ejecuta'] }}" />
                                    <input type="hidden" name="plani[{{ $i }}][5]" value="{{ $pla['cproyectoejecucioncab'] }}" />
                                    <input type="hidden" name="plani[{{ $i }}][6]" value="{{ $pla['cactividad'] }}" />
                                    <input type="hidden" name="plani[{{ $i }}][7]" value="{{ $pla['tipo'] }}" />
                                    <input type="hidden" name="plani[{{ $i }}][9]" value="N" />
                                    </td>
                                    @if($pla['tipo']=='1' || $pla['tipo']=='2')
                                    <td ><input type="hidden" value="{{ $pla['des_act'] }}"></td>
                                    @else
                                    <td><b>{{ $pla['des_act'] }}</b></td>
                                    @endif
                                    <td >
                                        @if($pla['tipo']=='1')
                                        <span class="label" style="background-color: #11B15B">FF</span>
                                        @endif
                                        @if($pla['tipo']=='2')
                                        <span class="label" style="background-color: #CD201A">NF</span>
                                        @endif
                                        @if($pla['tipo']=='3')
                                        <span class="label" style="background-color: #0069AA">AND</span>
                                        @endif
                                    </td>
                                    <td  style="min-width:120px !important">
                                        @if(strlen($pla['cpersona_ejecuta'])>0)
                                        {!! Form::select('plani['.$i.'][8]',(isset($nofacturables)==1?$nofacturables:array()),(isset($pla['ctiponofacturable'])==1?$pla['ctiponofacturable']:null),array('id'=>'nfac_'.$i))!!}
                                        @endif
                                    </td>                        
                                    <td title="Horas presupuestadas: {{ $pla['horaspresup'] }}">{{ $pla['montpresup'] }}</td>
                                    <td  title="Horas ejecutadas: {{ $pla['horasejecu'] }}">{{ $pla['montejecu'] }}</td>
                                    <td  title="Horas saldo: {{ $pla['saldohoras'] }}">{{ $pla['saldo'] }}</td>  
                                    <td style="width:200px;padding:0px">
                                        @if(strlen($pla['cpersona_ejecuta'])>0)
                                        {{ $pla['cpersona_ejecuta_nom'] }}                
                                        @endif
                                    </td>
                                    @if(isset($dias))
                                        @foreach($dias as $d)
                                        <?php 
                                            $key = str_replace("-","",$d[0]);  
                                            $valor = (isset($pla[$key]['horas'])==1?$pla[$key]['horas']:'');
                                            $valor_eje = (isset($pla[$key]['horas_eje'])==1?$pla[$key]['horas_eje']:'');
                                        ?>
                                            <td>
                                            <input type='text' name="plani[{{ $i }}][4][{{ $key }}][0]" id='h_{{ $i }}_{{ $key }}' class="" size="4" 
                                            value="{{ (isset($pla[$key]['horas'])==1?$pla[$key]['horas']:'') }}" onchange="viewSobre('observa_{{ $i }}_{{ $key }}',this);"
                                            onkeypress="return soloNumeros(event,this);"
                                            @if(strlen($pla['cpersona_ejecuta'])<=0)
                                                style="display:none";
                                            @endif 

                                            @if(isset($pla[$key]['cproyectoejecucion'])==1)
                                                @if($pla[$key]['estado']!='1')
                                                    readonly
                                                    style="background-color: #CD201A"

                                                    data-toggle="tooltip" data-placement="top" title="Ejecutado: {{ $valor_eje }}"
                                                @else
                                                    @if($dia_act>$key)
                                                        readonly
                                                    @endif
                                                @endif
                                            @else 
                                                @if($dia_act>$key)
                                                    readonly
                                                @endif                                                
                                            @endif
                                            maxlength="5"/>
                                            <a href="#" onclick="viewObs('{{ $i }}','{{ $key }}')" id="observa_{{ $i }}_{{ $key }}" 
                                            @if(strlen($valor)<=0)
                                                style="display:none"
                                            @else
                                                @if($valor <=0)
                                                style="display:none"                        
                                                @endif
                                            @endif  
                                            <?php 
                                            $obs=(isset($pla[$key]['observacion'])==1?$pla[$key]['observacion']:'');
                                            ?> 
                                            @if(strlen($obs)<=0)
                                            ><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                                            @else  
                                             <i class="glyphicon glyphicon-comment" aria-hidden="true" id="observa_{{ $i }}_{{ $key }}"></i>
                                            @endif                              
                                            <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][1]" id='e_{{ $i }}_{{ $key }}' value="{{ (isset($pla[$key]['cproyectoejecucion'])==1?$pla[$key]['cproyectoejecucion']:'') }}" />
                                            <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][2]" id='f_{{ $i }}_{{ $key }}' value="{{ (isset($key)==1?$key:'') }}" />

                                            <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][3]" id='t_{{ $i }}_{{ $key }}' value="{{ (isset($pla[$key]['estado'])==1?$pla[$key]['estado']:'') }}" />
                                            <input type='hidden' name="plani[{{ $i }}][4][{{ $key }}][4]" id='obs_{{ $i }}_{{ $key }}' value="{{ (isset($pla[$key]['observacion'])==1?$pla[$key]['observacion']:'') }}" />
                                            </td>
                                        @endforeach
                                    @endif
                                    <td><!--<a href="#" data-toggle="tooltip" title="Duplicar Tarea">
                                    <i class="fa fa-files-o" aria-hidden="true"></i></a>--></td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table> 
                </div>
            </div>                
@endforeach
@endif     
</div>        
 

     
