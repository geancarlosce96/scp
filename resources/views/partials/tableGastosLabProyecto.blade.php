<table class="table" id="tableListGastosLab">
                  <thead>
                  <tr class="clsCabereraTabla">
                    <th>Item</th>
                    <th style="min-width:200px!important;">Descripción</th>
                    <th style="min-width:50px!important;">Precio Unitario</th>
                    <th>Cimentación</th>
                    <th>Botadero</th>
                    <th>Cantera</th>
                    <th>Total (US$)</th>  
                    <th>Seleccionar</th>                                         
                  </tr>                       
                  </thead>
                  @if(isset($gastosLab))
                  <tbody>
                  @foreach($gastosLab as $gasl)
                    <tr>
                        <td>{{ $gasl['item'] }} 
                      {!! Form::hidden('cproyectogastoslab',$gasl['cproyectogastoslab'],array('id' => 'cproyectogastoslab' )) !!}</td>
                        <td>{{ $gasl['descripcion'] }}</td>
                        <td>{{ $gasl['preciou'] }}</td>                
                        <td>
                        
                        <input type="text" name="gaslcimen[{{ $gasl['cproyectogastoslab'] }}]" class="form-control" size="4" value="{{ $gasl['cantcimentacion'] }}" /> 
                        </td>
                        <td>
                        <input type="text" name="gaslbota[{{ $gasl['cproyectogastoslab'] }}]" class="form-control" size="4" value="{{ $gasl['cantbotadero'] }}" /> 
                        </td>
                        <td>
                        <input type="text" name="gaslcant[{{ $gasl['cproyectogastoslab'] }}]" class="form-control" size="4" value="{{ $gasl['cantcanteras'] }}" /> 
                        </td>
                        <td>{{ $gasl['subtotal'] }}</td>                
                        <td>
                        
                        <input name='delLab[]' type="checkbox" value="{{ $gasl['cproyectogastoslab'] }}">
                     
                        </td>                                                                              
                    </tr>
                  @endforeach                                                            
                  </tbody>
                  @endif
                  <tfoot>
                  <tr class="clsSubTotal">
                     <td></td>
                     <td></td>
                     <td>Total:</td>
                     <td><input class="form-control input-sm" type="text" placeholder="" disabled></td>
                     <td><input class="form-control input-sm" type="text" placeholder="" disabled></td>
                     <td><input class="form-control input-sm" type="text" placeholder="" disabled></td>
                     <td><input class="form-control input-sm" type="text" placeholder="" disabled></td>
                  </tr>
                  
                  </tfoot>
                </table>  