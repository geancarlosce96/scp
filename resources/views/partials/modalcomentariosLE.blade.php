<div class="modal fade" id="modalcometarioLE" tabindex="-1" role="dialog" aria-hidden="true" data-backdrop="false">
  <div class="modal-dialog modal-sm" role="document" style="width: 30%">
    <div class="modal-content">
      <!-- <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: white;" aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="color: white">Trazabilidad de comentarios</h4>
      </div> -->
      <div class="modal-body" id="tablaEntregableRuteoComentario">
        @include('partials.tableComentarioEntregablesProyecto',array('trazabilidad' => (isset($trazabilidad)==1?$trazabilidad:array()),'contador'=>(isset($contador)==1?$contador:''),'comenta'=>(isset($comenta)==1?$comenta:''),'cproyectoentregable'=>(isset($cproyectoentregable)==1?$cproyectoentregable:''),'codigo_ent'=>(isset($codigo_ent)==1?$codigo_ent:''),'nombre_ent'=>(isset($nombre_ent)==1?$nombre_ent:'') ))
     </div>
      <!-- <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnruteo">Enviar para validarción</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div> -->
    </div>
  </div>
</div>


