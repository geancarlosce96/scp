<div id="divLE" class="modal fade" role="dialog" style="overflow-y: scroll;">
  <div class="modal-dialog modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title" style="color:white">Registro de entregables para el EDT  <strong><span id="descripcionedt"></span></strong></h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmentregable')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_Cuminera')) !!}
        {!! Form::hidden('cproyecto_ent','',array('id'=>'cproyecto_ent')) !!}
        {!! Form::hidden('cproyectoedt_m','',array('id'=>'cproyectoedt_m')) !!}
        {!! Form::hidden('cproyectoentregables','',array('id'=>'cproyectoentregables')) !!}
        {!! Form::hidden('crevisionDoc','',array('id'=>'crevisionDoc')) !!}

        <!-- <div class="col-lg-4 col-xs-4">
          <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12">
              <label class="clsTxtNormal">Nombre del EDT:</label>

              {!! Form::text('descripcionedt','', array('class'=>'form-control input-sm','placeholder' => '','id'=>'descripcionedt','readonly' => 'true')) !!}  
            </div>
          </div>


        </div> -->


      <div class="col-lg-12 col-xs-12">
        <div class="row">
            <div class="box-body">
                <div class="box-group" id="accordion">
                    <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
                    <div class="panel panel-primary">
                        <div class="box-header with-border panel-heading">
                            <h4 class="box-title panel-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed"><span class="glyphicon glyphicon-hand-up"></span>&nbsp;&nbsp;&nbsp;Añadir Entregable</a>
                            </h4>
                            <!--<button type="submit" class="btn btn-success bg-green pull-right">Registrar</button>-->
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                            <div class="box-body">

                              <div class="col-lg-12 col-xs-12 table-responsive">
                                @include('partials.formEntregable')
                              </div>
                                                                 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
          </div>
      </div>

        <div class="row clsPadding2"> 
          <div class="col-lg-12 col-xs-12 table-responsive" id="divLEntregable">

           @include('partials.tableEntregablesProyecto',array('listEnt'=>(isset($listEnt)==1?$listEnt:null )  , 'listEnt' => (isset($listEnt)==1?$listEnt:null ) ))
         </div>
       </div>
       {!! Form::close() !!}  
       <div class="modal-footer">
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div> 
    </div>
  </div>
</div>
</div>
