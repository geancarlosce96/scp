<table class="table table-striped">
            <thead>
            <tr class="clsCabereraTabla">
                    <td class="clsAnchoDia">Item</td>
                    <td class="clsAnchoTabla">Rendición</td>
                    <td class="clsAnchoTabla">Fecha</td>
                    <td class="clsAnchoTabla">Proyecto</td>
                    <td class="clsAnchoTablaMax">Moneda</td>
                    <td class="clsAnchoTablaMax">Monto</td>
                    <td class="clsAnchoTablaMax">Estado</td>
                    <td class="clsAnchoTabla">Descripción</td> 
                    <!--<td class="clsAnchoTabla">Acciones</td>
                    <td class="clsAnchoTabla">Sel</td> -->

                                  
            </tr>
            </thead>
            <tbody>
              @if(isset($listaUsuario)==1)
              @foreach($listaUsuario as $usu)

            <tr class="clsAnchoTabla">
              <td class="clsAnchoTabla">{{ $usu['item'] }}</td>
              <td class="clsAnchoTabla">{{ $usu['numerorendicion'] }}</td>
              <td class="clsAnchoTabla">{{ $usu['fdocumento'] }}</td>
              <td class="clsAnchoTabla">{{ $usu['proyecto'] }}</td>
              <td class="clsAnchoTabla">{{ $usu['moneda'] }}</td>
              <td class="clsAnchoTabla">{{ $usu['total'] }}</td>
              <td class="clsAnchoTabla">{{ $usu['ccondicionoperativa'] }}</td>
              <td class="clsAnchoTabla">{{ $usu['comentario'] }}</td>              
              <!--<td class="clsAnchoTabla">
              <a href="#" class="fa fa-pencil"  data-toggle="tooltip" data-container="body" title="Editar"></a> 
              		&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="#" class="fa fa-search"  data-toggle="tooltip" data-container="body" title="Buscar"></a> 
              		&nbsp;&nbsp;&nbsp;&nbsp;  
              <a href="#" class="fa fa-print"  data-toggle="tooltip" data-container="body" title="Imprimir"></a> 
              		&nbsp;&nbsp;&nbsp;&nbsp;                                      
              <a href="#" class="fa fa-trash"  data-toggle="tooltip" data-container="body" title="Eliminar"></a>
              </td>
              <td class="clsAnchoTabla">
                    <input type="checkbox">
              </td>-->

            </tr>            
            @endforeach
            @endif                                 	
            </tbody>
          </table>