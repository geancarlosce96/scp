    
      <table class="table table-striped">
      	<thead>
            <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Proceso</td>
                <td class="clsAnchoTabla">Actividad</td>
                <td class="clsAnchoTabla">Resultado</td>
                <td class="clsAnchoTabla">CHECK</td>
                <td class="clsAnchoTabla">% Avance</td>
                <td class="clsAnchoTabla">observacion</td>

            </tr>
        </thead>      

     	<tbody>
       @if(isset($checkEnt)==1)
        @foreach($checkEnt as $cen)
        <tr>
        	  <td class="clsAnchoTabla"></td>  
            <td class="clsAnchoTabla">{{ $cen['des_check'] }}</td>      
            <td class="clsAnchoTabla">{{ $cen['resultado'] }}</td>
            <td class="clsAnchoTabla">{{ $cen['CHECK'] }}</td>
            <td class="clsAnchoTabla">{{ $cen['porcentajeavance'] }}</td>
            <td class="clsAnchoTabla">{{ $cen['observacion'] }}</td>            
        </tr>   
         
         @endforeach
         @endif 
        </tbody>      
      </table> 
