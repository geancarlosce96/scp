      <table class="table table-striped">
      	<thead>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">Cod. Cliente</td>
                <td class="clsAnchoTabla">Cod. Anddes</td>
                <td class="clsAnchoTabla">Descripción</td>
                <td class="clsAnchoTabla">Revision</td>
                <td class="clsAnchoTabla">Cantidad</td>
                <td class="clsAnchoTabla">Comentarios</td>
                <td class="clsAnchoTablaMax">Resultado de Documento</td>
                <td class="clsAnchoTablaMax"> Acciones
                </td>
            </tr>
        </thead>

      	<tbody>
        @if(isset($ejecu_deta))
        @foreach($ejecu_deta as $deta)        
        <tr>
            <td class="clsAnchoTabla">{{ $deta['item'] }}</td>
            <td class="clsAnchoTabla">{{ $deta['codigocliente'] }}</td>
            <td class="clsAnchoTabla">{{ $deta['codigoanddes'] }}</td>
        	  <td class="clsAnchoTabla">{{ $deta['descripcion'] }}</td>
            <td class="clsAnchoTabla"  style="max-width:100px !important;">
            {{ $deta['revision_des'] }}
            </td>
            <td class="clsAnchoTabla">{{ $deta['cantidad'] }}</td>         
            <td class="clsAnchoTabla">{{ $deta['comentarios'] }}</td>
            <td class="clsAnchoTabla">
              {{ $deta['resultado_des'] }}            
            </td>       
            <td class="clsAnchoTabla">
                            <a href="#" onclick="eliminarItem({{ $deta['ctransmittalejedetalle'] }})" data-toggle="tooltip" data-container="body" title="Eliminar">
                            <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <a href="#" onclick="editarItem({{ $deta['ctransmittalejedetalle'] }})" data-toggle="tooltip" data-container="body" title="Editar">
                            <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>  
            </td>    
        </tr> 
        @endforeach
        @endif
         

                        
        </tbody>
      </table> 