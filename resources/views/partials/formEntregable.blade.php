<div class="row">

  <div  class="col-sm-12">

   <div class="row">

      <div class="form-group">

        <label for="edt" class="col-sm-1 control-label">Tipo</label>
        <div class="col-sm-2">
          {!! Form::select('tipoproyent',(isset($tipoProyEntregable)==1?$tipoProyEntregable:array()),'',array('class' => 'form-control','id'=>'tipoproyent','title'=>'Se refiere a si el documento es categorizado como entregable final al cliente o es parte de otro')) !!}             
        </div>

        <label for="edt" class="col-sm-1 control-label">Entregable</label>
        <div class="col-sm-2">
          {!! Form::select('tipoentregable',(isset($tipoentregables)==1?$tipoentregables:array()),'',array('class' => 'form-control','id'=>'tipoentregable','title'=>'Se refiere al tipo de entregable')) !!}
        </div>
        
        <div class="col-sm-4"></div>

        <div class="col-sm-2">
          <!-- <button type="button" class="btn btn-primary btn-block" id="btnSaveEnt"><b>Agregar</b></button> -->
        </div>
       <!--  <div class="col-sm-2">
          <button type="button" class="btn btn-danger btn-block" id="btnEliminar"><b>Quitar</b></button>
        </div> -->
        
      </div>

    </div>
    
  </div>


</div>

<div class="row">

  <div  class="col-sm-6" style="padding: 20px;">

   <!--  <div class="row">

      <div class="form-group">

        <label for="edt" class="col-sm-2 control-label">Tipo</label>
        <div class="col-sm-2">

          {!! Form::select('tipoproyent',(isset($tipoProyEntregable)==1?$tipoProyEntregable:array()),'',array('class' => 'form-control','id'=>'tipoproyent')) !!}             
        </div>
      <div class="col-sm-4"></div>
      <div class="col-sm-3"><button type="button" class="btn btn-danger" id="btnEliminar"><b>Quitar</b></button><button type="button" class="btn btn-primary" id="btnSaveEnt"><b>Agregar</b></button></div>
        
      </div>

    </div> 

    <br>-->

    <div class="row">
      
      <div class="form-group">
        <label for="cproyectoactividades" class="col-sm-2 control-label">Fases</label>
        <div class="col-sm-4">
          {!! Form::select('fasesEdt',(isset($faseProy)==1?$faseProy:array()),'',array('class' => 'form-control','id' => 'fasesEdt','title'=>'Se refiere a la fase del diseño según SIG')) !!}
        </div>

        <label for="cproyectoactividades" class="col-sm-2 control-label">Disciplina</label>
        <div class="col-sm-4">
          {!! Form::select('disciplina',(isset($disciplina)==1?$disciplina:array()),'',array('class' => 'form-control','id' => 'disciplina','onchange'=>'listado()','title'=>'Área que pertenece el entregable')) !!}
        </div>
        
      </div>
      
        <!--<label for="codigo" class="col-sm-1 control-label">Código</label>
        <div class="col-sm-2">
          {!! Form::text('codigo','',array('class'=>'form-control','id'=>'codigoent','placeholder'=>'Código')) !!}
        </div>-->

         <div class="col-sm-2">
         
        </div> 
    </div>
   
    <br>
    <div class="row">

      <label for="observacionent" class="col-sm-2 control-label">Observación</label>
      <div class="col-sm-10">
        {!! Form::textarea('observacionent','',array('class'=>'form-control','id'=>'observacionent','placeholder'=>'Observación','rows'=>'1')) !!}
      </div>


        <!--<label for="cproyectoactividades" class="col-sm-2 control-label">Responsable</label>
        <div class="col-sm-4">
          {!! Form::select('responsable',(isset($responsable)==1?$responsable:array()),'',array('class' => 'form-control','id' => 'responsable')) !!}
        </div>-->

    </div>

    <br>

    <div style="padding: 20px;border:0.5px solid rgba(0, 105, 173, 1);border-radius: 5px;display: none;" id="divEntAgregados">
<!-- 
    <div class="row" style="height: 55px">
      <div class="pull-right">
                      <div class="inner-addon right-addon">

                          <i class="glyphicon glyphicon-search"></i>
                          <input class=type="text" name="searchEntAgregado" id="searchEntAgregado" placeholder='Buscar entregable contractual'/>
                      </div>
        
      </div>
        <table class="table table-hover" style="font-size: 12px;">

          <tbody>
              <tr style="text-align: left;">
                <th></th>
                <th>Descripción </th>
                <th>Disciplina</th>
                <th>Entregable</th>
                <td style="text-align: right;" colspan="2">

                </td>
              </tr>
          </tbody>
        </table>

    </div> -->

    <div class="row">
      <div  id="divEntreg" style="height: 300px;">

        <table class="table table-hover" width="100%" style="font-size: 12px;" id="tablaEntregablesAgregados">

          <thead>
            <tr>
                <th width="55%">Descripción </th>
                <th width="20%">Disciplina</th>
                <th width="20%">Entregable</th>
                <th width="5%">Seleccionar</th>
              
            </tr>
            
          </thead>

          <tbody>
            
          </tbody>
        </table>
        
      </div>

    </div>

    </div>
      
  </div>

  <div  class="col-sm-6" style="padding: 20px;">


   <div class="row">
  <!--     <label for="centregable" class="col-sm-1 control-label">Nivel 0</label>
      <div class="col-sm-5">
         {!! Form::select('entregableN0',(isset($entregable)==1?$entregable:array()),'',array('class' => 'form-control','id' => 'entregableN0')) !!} -->

       <!-- <input size="50" class="form-control" name="entregable" id="entregable"> -->
     <!--  </div> -->

      <div class="col-lg-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <button type="button" id="nuevoEntN0" title="Agregar componente del entregable" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top"><span class="fa fa-plus-circle"></span></button>
                </a>
                <label class="col-form-label">Componente</label>
            </span>                   
                {!! Form::select('entregableN0',(isset($entregable)==1?$entregable:array()),'',array('class' => 'form-control','id' => 'entregableN0')) !!}
          </div>              
      </div>

      <div class="col-lg-6 col-xs-12">
        <div class="input-group">
            <span class="input-group-addon">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <button type="button" id="nuevoEntN1" title="Agregar instalación del entregable" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top"><span class="fa fa-plus-circle"></span></button>
                </a>
                <label class="col-form-label">Instalación</label>
            </span>                   
                {!! Form::select('entregableN1',(isset($entregable)==1?$entregable:array()),'',array('class' => 'form-control','id' => 'entregableN1')) !!}
          </div>              
      </div>

    </div>

    <br> 
    <!--<div class="row">

      <div class="row">
        <div  class="col-sm-4"> <h5>Entregables</h5></div>
        
      </div>

      <div class="row">


        <div  class="col-sm-4">  <input type='checkbox' name='marcarTodo' id='marcarTodo'> Seleccionar Todos</div>
        <div  class="col-sm-4"></div>
        <div  class="col-sm-4">

          <div class="inner-addon left-addon">
              <i class="glyphicon glyphicon-search"></i>
              <input class=type="text" name="searchEnt" id="searchEnt" placeholder='Buscar Entregable'/>
          </div>




        </div>
        
      </div>
 
    




    </div> -->

    <div class="row" style="height: 50px">
        <table class="table table-hover" style="font-size: 12px;">
          <tbody>
              <!-- <tr style="background-color:rgb(166,166,166);">
                <th>Entregables</th>
                <td style="text-align: left;" >
                    <div class="inner-addon left-addon">
                        <i class="glyphicon glyphicon-search"></i>
                        <input class=type="text" name="searchEnt" id="searchEnt" placeholder='Buscar entregable'/>
                        <button name="htparticipante" type="button" id="nuevoEntN2" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="left"><span class="fa fa-plus-circle"></span></button>
                    </div>
                </td>
                <th>Abreviartura <br> Doc</th>
                <th>Cantidad</th>
                <th style="text-align: center;"><input type='checkbox' name='marcarTodo' id='marcarTodo' title="Seleccionar todos"></th>
              </tr> -->


              <tr style="background-color:rgb(166,166,166);">
                <!-- <th>Entregables</th> -->
                <td style="text-align: left;" rowspan="2" width="60%">
                    <div class="inner-addon left-addon">
                        <label>Entregables</label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <i class="glyphicon glyphicon-search"></i>
                        <input type="text" name="searchEnt" id="searchEnt" placeholder='Buscar entregable'/>
                        <button name="htparticipante" type="button" id="nuevoEntN2" title="Agregar entegables nuevos en los niveles seleccionados" class="btn btn-warning btn-xs" data-toggle="tooltip" data-placement="top"><span class="fa fa-plus-circle"></span></button>
                    </div>
                </td>
                <th width="10%"><center>Abreviatura</center></th>
                <th width="10%"><center>Cantidad</center></th>
                <th width="10%" style="text-align: center;"><a class="btn btn-success btn-xs" id="btnSaveEnt" title="Agregar entregables seleccionados"><i class="fa fa-download"></i></a></th>
                <th width="5%" style="text-align: left;"><input type='checkbox' name='marcarTodo' id='marcarTodo' title="Seleccionar todos los entregables"></th>
              </tr>
          </tbody>
        
        
        </table>


    </div>

    <div class="row">
      <div  id="divEntreg" style="height: 300px; overflow-y: auto;padding:0px;" class="table-responsive-sm">
        <table class="table table-sm" style="font-size: 12px;" id="tablaEntregables">
          <tbody>
          </tbody>
        </table>
      </div>
    </div>


<!-- 
    <div class="row">

      <label for="cproyectoactividades" class="col-sm-2 control-label">Nuevo Entregable</label>
      <div class="col-sm-4">
         {!! Form::text('nuevoEntregable','', array('class'=>'form-control input-sm','placeholder' => 'Descripción del entregable','id'=>'nuevoEntregable') ) !!}
      </div>

      <div class="col-sm-2"> 
        <button type="button" class="btn btn-success btn-block" id="btnEliminar"><b>Grabar</b></button>

      </div>

    </div> -->


    




    
  </div>
    
</div>
<br>





<div class="row">

	
	<!-- <div class="form-group">
		<label for="fechaA" class="col-sm-2 control-label">Fecha A</label>
		<div class="col-sm-2">
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				{!! Form::text('fechaA',(isset($objEnt['fechaA'])==1?$objEnt['fechaA']:''), array('class'=>'form-control pull-right datepicker','id'=>'fechaA','style'=>'z-index:1151 !important')) !!}
			</div>                 
		</div>
		<label for="fechaB" class="col-sm-2 control-label">Fecha B</label>
		<div class="col-sm-2">
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				{!! Form::text('fechaB',(isset($objEnt['fechaB'])==1?$objEnt['fechaB']:''), array('class'=>'form-control pull-right datepicker','id' => 'fechaB')) !!}
			</div>                
		</div>
		<label for="fecha0" class="col-sm-2 control-label">Fecha 0</label>
		<div class="col-sm-2">
			<div class="input-group date">
				<div class="input-group-addon">
					<i class="fa fa-calendar"></i>
				</div>
				{!! Form::text('fecha0',(isset($objEnt['fecha0'])==1?$objEnt['fecha0']:''), array('class'=>'form-control pull-right datepicker','id'=>'fecha0')) !!}
			</div>                 
		</div>
	</div> -->
</div>

<script type="text/javascript">

$("#marcarTodo").change(function () {
    if ($(this).is(':checked')) {
        //$("input[type=checkbox]").prop('checked', true); //todos los check
        $("#tablaEntregables > tbody tr input[type=checkbox]").prop('checked', true); //solo los del objeto #diasHabilitados
    } else {
        //$("input[type=checkbox]").prop('checked', false);//todos los check
        $("#tablaEntregables > tbody tr input[type=checkbox]").prop('checked', false);//solo los del objeto #diasHabilitados
    }
});

function agregardatatable(){
  

 var table = $('#tablaEntregablesAgregados').DataTable( {
    "paging":   true,
    "ordering": true,
    "info":     false,
    lengthChange: true,
    scrollY:        "250px",
    scrollX:        true,
    scrollCollapse: true,
    paging:         true,
    } ); 

 table.buttons().container()
    .appendTo( '#tablaEntregablesAgregados_wrapper .col-sm-6:eq(0)' );   

}





  
</script>


