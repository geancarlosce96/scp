<div id="divUmineraContacto" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:white">Registro de Contactos de Unidad Minera</h4>
            </div>
            <div class="modal-body">
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmcuminera')) !!}
              {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_Cuminera')) !!}
              
              {!! Form::hidden('cpersona_u',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_u')) !!}            
              
              {!! Form::hidden('cunidadminera_c','',array('id'=>'cunidadminera_c')) !!}
              {!! Form::hidden('cuminera_con','',array('id'=>'cuminera_con')) !!}
              
            <div class="col-lg-4 col-xs-4">
              <div class="row clsPadding2">
                  <div class="col-lg-12 col-xs-12">
                  <label class="clsTxtNormal">Unidad Minera:</label>
                   
                   {!! Form::text('nombre_umc','', array('class'=>'form-control input-sm','placeholder' => '','id'=>'nombre_umc','maxlength'=>'100','readonly'=>'true')) !!}  
                  </div>
              </div>
              
              
            </div>


<div class="col-lg-12 col-xs-12">
    <div class="box box-default box-solid">
        <div class="panel-heading" > 
          <h3 class="panel-title">Agregar Contacto
              <span class="box-tools pull-left">
                <button type="button" data-target="#bodyAgregarContacto" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i>
                </button>
              </span> 
          </h3>


          </div>  

              <div class="box-body collapse" style="" id="bodyAgregarContacto">   

              <div class="row clsPadding2">
                  <div class="col-lg-4 col-xs-4">
                  <label class="clsTxtNormal">Apellidos:</label>
                  <input type="text" class="form-control" placeholder="Apellido Paterno" name="apaterno" id="apaterno">
                  <input type="text" class="form-control" placeholder="Apellidos Materno" name="amaterno" id="amaterno">
                  </div>
                  <div class="col-lg-4 col-xs-4">
                  <label class="clsTxtNormal">Nombres:</label>
                  <input type="text" class="form-control" placeholder="Nombres" id="nombres" name="nombres">  
                  </div>    
                  <div class="col-lg-4 col-xs-4">
                  <label class="clsTxtNormal">Email:</label>
                    <div class="input-group">
                      <span class="input-group-addon">@</span>
                      <input type="text" class="form-control" placeholder="Email" id="email" name="email">
                    </div> 
                  </div>    
              </div>
              <div class="row clsPadding2">
                  <div class="col-lg-2 col-xs-4">
                  <label class="clsTxtNormal">Teléfono:</label>
                   <input type="text" class="form-control" placeholder="telefono" id="telefono" name="telefono">
                  <input type="text" class="form-control" placeholder="cel1" id="cel1" name="cel1">
                  <input type="text" class="form-control" placeholder="cel2" id="cel2" name="cel2"> 
                   <input type="text" class="form-control" placeholder="anexo" id="anexo" name="anexo">                   
                  </div>

                  <div class="col-lg-4 col-xs-3">
                  <label class="clsTxtNormal">Dirección:</label>
                    <input type="text" class="form-control" placeholder="Dirección" id="direccion" name="direccion">
                  </div> 

                  <div class="col-lg-2 col-xs-3">
                  <label class="clsTxtNormal">Cargo:</label>
                    {!! Form::select( 'ccontactocargo',(isset($tcargo)==1?$tcargo:array() ),'',array('class' => 'form-control','id'=>'ccontactocargo')) !!}  
                  </div> 
                   
                  <div class="col-lg-2 col-xs-3">
                  <label class="clsTxtNormal">Tipo:</label>
                  {!! Form::select( 'ctipocontacto',(isset($ttipo)==1?$ttipo:array() ),'',array('class' => 'form-control','id'=>'ctipocontacto')) !!}   
                  </div>
                  
                      <div class="col-lg-2 col-xs-2" style="padding-top: 20px;">
                           <button type="button" class="btn btn-primary btn-block" id="btnAddContactos">
                                <i class="fa fa-plus" aria-hidden="true"></i><b>Guardar</b>
                           </button>
                      </div>
                 
                  </div> 
              </div>              
           
      </div>   

  </div>

              <div class="row clsPadding2"> 
                  <div class="col-lg-12 col-xs-12 table-responsive" id="divMineraCon">

                 @include('partials.modalContactoUnidadMinera',array('tunidadmineracontactos'=>(isset($tunidadmineracontactos)==1?$tunidadmineracontactos:null )  , 'tunidadmineracontactos' => (isset($tunidadmineracontactos)==1?$tunidadmineracontactos:null ) ))
                 </div>
                </div>
                
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" >Aceptar</button>
            </div>
          </div>
        </div>
      </div>