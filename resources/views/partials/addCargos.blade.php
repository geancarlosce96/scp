<!-- Modal agregar Usuario-->
    <div id="addCarg" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Cargos</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarCargos','method' => 'POST','id' =>'frmcargo','class'=>'form-horizontal')) !!}

          {!! Form::hidden('ccargo',(isset($cargo->ccargo)==1?$cargo->ccargo:''),array('id'=>'ccargo')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="ccargo" class="col-sm-2 control-label">cargo</label>
                <div class="col-sm-8">
                     {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('ccargo',(isset($cargo->ccargo)==1?$cargo->ccargo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'ccargo') ) !!}          
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="codigo" placeholder="Codigo" name="codigo" value="{{(isset($codigo->codigo)==1?$codigo->codigo:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">       
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="siglas" class="col-sm-2 control-label">Siglas</label>
                  <div class="col-sm-8">                             
                    <input type="text" class="form-control" id="siglas" placeholder="Siglas" name="siglas" value="{{(isset($siglas->siglas)==1?$siglas->siglas:'')}}">     
                 
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="ccargo_parent" class="col-sm-2 control-label">Cargo Superior</label>
                  <div class="col-sm-8">
                  {!! Form::select('ccargo_parent',(isset($cargo_parent)==1?$cargo_parent:null),(isset($cargo->ccargo_parent)==1?$cargo->ccargo_parent:''),array('class' => 'form-control select-box','id'=>'ccargo_parent')) !!} 

  
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>
    
    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

