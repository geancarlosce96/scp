<!-- Modal agregar Usuario-->
    <div id="addSed" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Sede</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarSede','method' => 'POST','id' =>'frmsede','class'=>'form-horizontal')) !!}

          {!! Form::hidden('csede',(isset($sede->csede)==1?$sede->csede:''),array('id'=>'csede')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="csede" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('csede',(isset($sede->csede)==1?$sede->csede:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'csede') ) !!}     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="codigosede" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="codigosede" placeholder="Codigo" name="codigosede" value="{{(isset($codigosede->codigosede)==1?$codigosede->codigosede:'')}}">         
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddSede" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

