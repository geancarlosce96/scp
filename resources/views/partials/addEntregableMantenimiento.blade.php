<!-- Modal agregar Usuario-->
    <div id="addEntMant" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Entregable</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarEntregableMantenimiento','method' => 'POST','id' =>'frmentregable','class'=>'form-horizontal')) !!}

          {!! Form::hidden('centregables',(isset($entregable->centregables)==1?$entregable->centregables:''),array('id'=>'centregables')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="centregables" class="col-sm-2 control-label">Entregable</label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('centregables',(isset($entregable->centregables)==1?$entregable->centregables:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'centregables') ) !!}      
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="codigo" placeholder="Codigo" name="codigo" value="{{(isset($codigo->codigo)==1?$codigo->codigo:'')}}">      
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">  
             
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="ctipoentregable" class="col-sm-2 control-label">Tipo</label>
                  <div class="col-sm-8">                             
                     {!! Form::select('ctipoentregable',(isset($tipo)==1?$tipo:null),(isset($entregable->ctipoentregable)==1?$entregable->ctipoentregable:''),array('class' => 'form-control select-box','id'=>'ctipoentregable')) !!}     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="cdisciplina" class="col-sm-2 control-label">Disciplina</label>
                  <div class="col-sm-8">                     
                   
                     {!! Form::select('cdisciplina',(isset($disciplina)==1?$disciplina:null),(isset($entregable->cdisciplina)==1?$entregable->cdisciplina:''),array('class' => 'form-control select-box','id'=>'cdisciplina')) !!}   
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddEntMant" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->
