<table class="table table-striped datatable-order-search" id="divApRendicion">
            <thead>
            <tr class="clsCabereraTabla">
              <th>Item</th>
              <th>Rendición</th>
              <th>Nombre Rendición</th>              
              <th>Colaborador</th>
              <th>Fecha</th>
              <th>Moneda Dev</th>
              <th>Monto Dev</th>
              <th>Sel</th>

            </tr>
            </thead>
            <tbody>

              @if(isset($listaAprobacion)==1)
              @foreach($listaAprobacion as $la)

            <tr >
              <td >{{ $la['item'] }}</td>
              <td>
              <button name="viewH" type="button" class="btn btn-primary"  onclick="verGastoDet({{ $la['cgastosejecucion'] }})">              
              {{ $la['numerorendicion'] }}
              </button>
              </td>
              <td >{{ $la['gasto'] }}</td>                
              <td >{{ $la['empleado'] }}</td>
              <td >{{ $la['fdocumento'] }}</td>
              <td >{{ $la['moneda'] }}</td>
              <td >{{ $la['total'] }}</td>

              <?php /*<td >
              <a href="#" class="fa fa-thumbs-o-up"  data-toggle="tooltip" data-container="body" title="Aprobar" onclick="aprobarGasto({{ $la['cgastosejecuciondet'] }});"></a> 
              		&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="#" class="fa fa-thumbs-o-down"  data-toggle="tooltip" data-container="body" title="Observar" onclick="observarGasto({{ $la['cgastosejecuciondet'] }});"></a>
              </td>*/?>
              <td >
                    <input type="checkbox" name="chkSel" value="{{ $la['cgastosejecucion'] }}">
              </td>
                    
            </tr>          
            @endforeach
            @endif                              	
            </tbody>
          </table>  

          <script>
            $('.datatable-order-search').dataTable( {
                 "paging":   true,
                 "ordering": true,
                 "info":     true,
             } );
          </script>