              <table id="tActividad" class="table">
                <thead>
                  <th>Código</th>
                  <th>Nombre</th>
                  <th>Seleccionar</th>
                </thead>
                @if(isset($actividades))
                	<tbody>
					@foreach($actividades as $act)
						<tr>
						<td>
							{{ (isset($act->codigo)?$act->codigo:'') }}
						</td>
						<td>
							{{ (isset($act->descripcion)?$act->descripcion:'') }}
						</td>
						<td>
							{!! Form::checkbox('acti[]',(isset($act->cactividad)?$act->cactividad:''),false) !!}
						</td>
						</tr>
					@endforeach
					</tbody>
				@endif
              </table>