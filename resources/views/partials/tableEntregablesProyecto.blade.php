<table class="table table-striped" id="tableEntregablesProy">
    <thead>
      <tr class="clsCabereraTabla">
        <th>Item</th>
        <th>Estado</th>
        <th>Fase</th>
        <th>Entregable</th>
        <th>Tipo</th>
        <th>Componente</th>
        <th>Disciplina</th>
        <th>Descripción</th>
         
        <th>Observación</th>
        <!--<th>Responsable</th>-->
        <th>Elaborado por</th>
        <th>Acciones</th>
        <th>
            <input type="checkbox" name="chkTodos_ent" id="chkTodos_ent" onclick="setCheck_ent(this);" />
            <button type='button' class="fa fa-trash btn-danger" id="btnEliminar"></button>
        </th>
      </tr>
    </thead>
    @if(isset($listEnt))   
    <tbody>
      @foreach($listEnt as $ent)

      <?php $color=''; ?>
        @if($ent['activo']=='0')

        <?php $color='red'; ?>

        @endif 

        @if($ent['estado']=='Modificado') 

        <?php $color='yellow'; ?>

        @endif   
 
    <!--   <tr  style="@if($ent['activo']=='0')text-decoration:line-through; color: red; @endif @if($ent['estado']=='Modificado') background-color: yellow @endif  "  >  -->

    <tr id="fila_{{ $ent['cproyectoentregables'] }}" data-edt="{{ $ent['cproyectoedt'] }}" data-ident="{{ $ent['cproyectoentregables'] }}"> 
     
      
        <td>{{ $ent['item'] }}</td>
        <td><span class='label' style="color: black;font-size: 12px;background-color: <?php echo($color) ?>;">{{ $ent['estado'] }}</span></td>
        <td >{{ $ent['fase'] }}</td>
        <td >{{ $ent['tipoentregable'] }}</td>
        <td >{{ $ent['tipoproyentregable'] }}</td>
        <td>{{ $ent['componente'] }}</td>

        <td >{{ $ent['des_dis'] }}</td>
        <td >{{ $ent['descripcion'] }}</td> 

        <td >{{ $ent['observacion'] }}</td>
        <!-- <td >{{ $ent['responsable'] }}</td> -->
        <td >{{ $ent['elaborador'] }}</td>
        <td style="text-decoration:line-through; color: white">   

          <center>
          

            <a href="#" title="Anular" style="color: red"><i class="fa fa-times" aria-hidden="true" onclick="eliminarEntregable({{ $ent['cproyectoentregables'] }})"></i></a>

            
          </center>    
        </td>
        <td><input type="checkbox" name="checkbox_ent_tabla[]" id="checkbox_ent_tabla" value="{{ $ent['centregable'] }}_{{ $ent['cproyectoentregables'] }}" /></td>
      </tr>
      </tr>

      @endforeach
    </tbody>
    @endif
</table>

<script type="text/javascript">
  

    var table = $('#tableEntregablesProy').DataTable( {
        "paging":   true,
        "ordering": true,
        "info":     true,
       // "order":     [[0,"desc"]],
        lengthChange: true,
        //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
    } );
 
    table.buttons().container()
        .appendTo( '#tableEntregablesProy_wrapper .col-sm-6:eq(0)' );


function setCheck_ent(chk){
  //alert($(":checkbox").length);
  $("input[name='checkbox_ent_tabla[]']").each(function(){
      /*if (this.checked) {
          
      }*/
      //alert(this.checked);
      this.checked = chk.checked;
    }); 
  return;
}

$("#btnEliminar").click(function(){

    var eliminar=confirm('¿Eliminar estos registros de forma permanente?');

    if(eliminar==false){
        $('#marcarTodo').prop('checked',false);
        return;
    }
    if(eliminar==true){

        $.ajax({
          url: 'eliminarEntregablePrySeleccionados',
          type:'GET',
          data: $("#frmentregable").serialize(),
          beforeSend: function () {
            $('#div_carga').show(); 
        },              
        success : function (data){

            $('#marcarTodo').prop('checked',false);

            limpiarlistaEntregables();
            buscarEntregableHijo($('#entregableN1').val())
            
                //limpiarEntregables();  
                $("#divLEntregable").html(data);
                $('#div_carga').hide(); 
            },
        });
    }
});

</script>
