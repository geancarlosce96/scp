<table class="table table-striped table-hover table-bordered table-responsive" style="font-size: 12px;margin-left: 0px;width: 1480px;" id="tableaprobarLE">
  <thead>
    <tr class="clsCabereraTabla">
      <th>Ítem</th>
      <th>Código</th>
      <th>Nombre</th>
      <th>Rev.</th>
      <th>Ruta</th>
      <th class="check_act" title="Pasar a siguiente revisión los entregables seleccionados / Observar los entregables no seleccionados" >Validar &nbsp;<input type="checkbox" name="checkbox_rev_todos[]" title="Seleccionar todos los entregables" id="checkbox_rev_todos"></th>
      <th class="check_act" title="Enviar los entregables seleccionados para su emisión">Emitir &nbsp;<input type="checkbox" name="checkbox_emitir_todos[]" title="Seleccionar todos los entregables" id="checkbox_emitir_todos"></th>
      <th class="check_act_otro">Cambiar Rev &nbsp;<input type="checkbox" name="checkbox_todos_otro[]" title="Seleccionar todos los entregables" id="checkbox_todos_otro"></th>
    </tr>
  </thead>
  <tbody>

    @foreach ($entregables as $key => $value)
    <tr class="entregable_ruteo" data-entregable="{{ $value['cproyectoentregables'] }}" data-revision="{{ $value['valor']}}">
      <td style="text-align: center;">{{ $key + 1 }}</td>
      <td>{{ $value['codigo'] }}</td>
      <td>{{ $value['descripcion_entregable'] }}</td>
      <td style="text-align: center;">{{ $value['valor'] }}</td>
      <td>
        <input id="ruta_{{ $value['cproyectoentregables'] }}" value="{{ $value['ruta'] }}" class="input-sm" style="width: 400px;" readonly>&nbsp;&nbsp;&nbsp;
        <a data-toggle="tooltip" data-placement="top" title="Copiar ruta" class="btn copiarruta" id="copiarruta_{{ $value['cproyectoentregables'] }}"><i class="fa fa-copy"></i></a>
        @if($value['comentario'] != "")
        <a data-toggle="tooltip" data-placement="top" title="Cmentarios" class="btn comentario" id="comentario_{{ $value['cproyectoentregables'] }}"><i class="fa fa-commenting-o"></i></a>
        @else
        <a data-toggle="tooltip" data-placement="top" title="Comentario" class="btn comentario" id="vercomentario_{{ $value['cproyectoentregables'] }}"><i class="fa fa-commenting"></i></a>
        @endif
        <br>
        <span id="copiado_{{ $value['cproyectoentregables'] }}" style="display: none;" class="copiado"></span>
      </td>
      <td class="check_act" >
        <center>
      @if($value['valor'] == "A")
          <input type="checkbox" name="checkbox_rev[]" value="{{ $value['cproyectoentregables'] }}" id="checkbox_rev_{{ $value['cproyectoentregables'] }}" class="checkbox_rev">
      @else
          <input type="checkbox" disabled>
      @endif
        </center>
      </td>
      <td class="check_act">
        <center>
          <input type="checkbox" name="checkbox_emitir[]" value="{{ $value['cproyectoentregables'] }}" id="checkbox_emitir_{{ $value['cproyectoentregables'] }}" class="checkbox_emitir">
        </center>
      </td>
      <td class="check_act_otro">
        <center>
          <input type="checkbox" name="checkbox_otro[]" value="{{ $value['cproyectoentregables'] }}" id="checkbox_otro_{{ $value['cproyectoentregables'] }}" class="checkbox_otro">
        </center>
      </td>
    </tr>
    @endforeach 

  </tbody>
</table>

<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>

<script type="text/javascript">

  // $('#tableaprobarLE').DataTable( {
  //   "bFilter": false,
  //   "bSort": false,
  //   "bPaginate": false,
  //   "bLengthChange": false,
  //   "bInfo": false,
  //       lengthChange: false,
  //       scrollY:        "600px",
  //       scrollCollapse: true,
  //     } );
  
  $(".copiarruta").click(function() {
    var ver = $(this).attr('id');
    var id = ver.split('copiarruta_');
    // console.log("copiar ruta",id[1]);
    copiarruta(id[1]);
    $('#copiado_'+id[1]).show();
    $('#copiado_'+id[1]).text('Ruta copiada en portapapeles');
    $('#copiado_'+id[1]).css('color', 'gray');
    setTimeout(function(){
      $('#copiado_'+id[1]).hide();
    }, 5000);
  });

  $("#btnruteo").off('click');
  $('#btnruteo').click(function() {
    
    var arrayEntregables = validar_check_todos();
    var encontre = $.inArray("validar",arrayEntregables);
    // console.log("encontre",encontre);
    // console.log('checkssssss',arrayEntregables);
    arrayEntregables.shift();
    // console.log('check222222',arrayEntregables);
    // return;

    // var rev =validarcheck_rev();
    // var emitir = validarcheck_emitir();
    var otros = validarcheck_otros();
    // console.log(otros,"otros");
    

    if ($("#verdetallerruteo").val()== "LD") {
      if (otros.length > 0) {
        swal({
            title: "¿Estas seguro que deseas cambiar la revisión de los entregables seleccionados?",
            // text: "El LD cambiara la revisión de los entregables",
            type: "info",
            html: true,
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
          },
          function(isConfirm) {
            if (isConfirm) {
              swal("Se cambio de revisión", "Gracias.", "success");
              grabaruteo(otros,'ruteo'); 
            } else {
              swal("Cancelado", "Gracias", "error");
            }
          });
      }
        else {
          swal("No se seleccionaron entregables");
      }
    }else if($("#verdetallerruteo").val()== "GP" && encontre == 0){
      if (arrayEntregables.length > 0) {
        swal({
            title: "¿Estas seguro que deseas enviar estos entregables?",
            text: "El LD cambiará la revisión de los entregables",
            type: "info",
            html: true,
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Confirmar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
          },
          function(isConfirm) {
            if (isConfirm) {
              swal("Entregables enviados", "Gracias.", "success");
              grabaruteo(arrayEntregables,'todos'); 
            } else {
              swal("Cancelado", "Gracias", "error");
            }
          });
      }
        else {
          swal("No se seleccionaron entregables");
      }
    }else {
      colaentregable(arrayEntregables);
    }

  });

  function colaentregable(le) {

    $.ajax({
      url: 'colaentregable',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'le': le},
    })
    .done(function(data) {

      // console.log("colaentregable",data);

        
          // if (emitir.length > 0) {
          //   grabaruteo(emitir,'emitir'); 
          //   console.log(emitir,"emitir");
          // }
          // if (rev.length > 0) {
          //   grabaruteo(rev,'rev'); 
          //   console.log(rev,"rev");
          // }
          swal({
            title: "¿Estas seguro que deseas emitir estos entregables?",
            text: "El control documentario "+data[0]+" tiene <b>"+ data[1] +"</b> entregables por emitir",
            type: "info",
            html: true,
            showCancelButton: true,
            confirmButtonClass: "btn-success",
            confirmButtonText: "Emitir",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: false
          },
          function(isConfirm) {
            if (isConfirm) {
              swal("Emitidos!", "Gracias.", "success");
              grabaruteo(le,'todos');
            } else {
              swal("Cancelado", "Gracias", "error");
            }
          });
        
    })
    .fail(function(data) {
      console.log("error");
    })
    .always(function(data) {
      console.log("complete-colaentregable",data);
      // return data;
    });
    
  }

  $('#checkbox_rev_todos').click(function() { //alert("hola");
      if ($(this).prop('checked')) {
    $('.checkbox_rev').each(
      function() {
          $('.checkbox_rev').prop('checked', true);
          $('.checkbox_emitir').prop('checked', false);
          $('#checkbox_emitir_todos').prop('checked', false);
      });
      } else {
        $('.checkbox_rev').each(
      function() {
          $('.checkbox_rev').prop('checked', false);
          });
      }
  });

  $('#checkbox_emitir_todos').click(function() { //alert("hola");
      if ($(this).prop('checked')) {
    $('.checkbox_emitir').each(
      function() {
          $('.checkbox_emitir').prop('checked', true);
          $('.checkbox_rev').prop('checked', false);
          $('#checkbox_rev_todos').prop('checked', false);
      });
      } else {
        $('.checkbox_emitir').each(
      function() {
          $('.checkbox_emitir').prop('checked', false);
          });
      }
  });

  $('#checkbox_todos_otro').click(function() { //alert("hola");
      if ($(this).prop('checked')) {
    $('.checkbox_otro').each(
      function() {
          $('.checkbox_otro').prop('checked', true);
      });
      } else {
        $('.checkbox_otro').each(
      function() {
          $('.checkbox_otro').prop('checked', false);
          });
      }
  });

  $(".checkbox_rev").click(function() {
    var id = $(this).parent().parent().parent().data('entregable');
    console.log(id);

      if ($(this).prop('checked')) {
        $('#checkbox_emitir_'+id).prop('checked', false);
      }
  });

  $(".checkbox_emitir").click(function() {
    var id = $(this).parent().parent().parent().data('entregable');
    console.log(id);

      if ($(this).prop('checked')) {
        $('#checkbox_rev_'+id).prop('checked', false);
      }
  });

  $(".comentario").click(function() {

      var id = $(this).parent().parent().data('entregable');
      var rev = $(this).parent().parent().data('revision');
      var ruteo = $('#verdetallerruteo').val();
      console.log(id,ruteo,rev);
      // vercomentarioentregable(id,ruteo,rev);

      $.ajax({
      url: 'verentregableruteocomentario',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'cproyectoentregable': id, 'ruteo': ruteo,'vista':'tableComentarioEntregablesProyecto', 'estado':['APR','REV','OBS'],'rev':rev},
    })
    .done(function(data) {
      // console.log("tablaEntregableRuteoComentario",data);
      $('#tablaEntregableRuteoComentario').html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

      $('#modalcometarioLE').modal({
        backdrop:"static"
      });
  });

  function copiarruta(id) {
    $('#ruta_'+id).select();
    document.execCommand("copy");
  };

  $("#tableaprobarLE").tableHeadFixer({'left' : 1},);

</script>