<!-- Modal agregar checkList-->
    <div id="btnAddcheckEnt" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Formulario</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarcheckList','method' => 'POST','id' =>'frmentregable','class'=>'form-horizontal')) !!}
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!}
            <div class="form-group">
              <label for="Proceso" class="col-sm-2 control-label">Proceso</label>
              <div class="col-sm-8">
                {!! Form::select( 'cfaseproyecto',(isset($tproceso)==1?$tproceso:array() ),'',array('class' => 'form-control','id'=>'cfaseproyecto')) !!}                
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>
            
            <div class="form-group">
              <label for="cchecklist" class="col-sm-2 control-label">Actividad</label> 
              <div class="col-sm-8">
                {!! Form::select( 'cchecklist',(isset($tactividad)==1?$tactividad:array() ),'',array('class' => 'form-control','id'=>'cchecklist')) !!} 
              </div>   
            </div>    
            <div class="form-group">
              <label for="resultado" class="col-sm-2 control-label">Resultado</label>
              <div class="col-sm-8">
                 <input class="form-control" id="" placeholder="Resultado" name="resultado" type="text"></input>        
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>                 
            <div class="form-group">
              <label for="CHECK" class="col-sm-2 control-label">Check</label>
              <div class="col-sm-8">
                  <td>
                        {!! Form::checkbox('acti[]',(isset($act->CHECK)==1?$act->CHECK:''),false) !!}
                  </td>              
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 
            <div class="form-group">
              <label for="porcentajeavance" class="col-sm-2 control-label">% de Avance</label> &nbsp;&nbsp;&nbsp;&nbsp;
                  <div class="col-sm-8">
                <input class="form-control" id="" placeholder="Porcentaje de Avance" name="porcentajeavance" type="numeric"></input>
              </div>            
             
            </div>          

            <div class="form-group">
              <label for="observacion" class="col-sm-2 control-label">Observaciones</label>
              <div class="col-sm-8">
                  {!! Form::textarea('observacion',(isset($objEnt['observacion'])==1?$objEnt['observacion']:''), array('class' => 'form-control', 'rows'=> '3','placeholder' => 'Observación','id'=>'observacion')) !!}                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>            
          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" id="idcheckAct">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar checkList-->