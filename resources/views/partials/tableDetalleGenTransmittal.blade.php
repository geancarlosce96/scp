<table class="table table-striped">
                <thead>
                    <tr class="clsCabereraTabla">
                        <td class="clsAnchoTabla">Item</td>
                        <td class="clsAnchoTabla">Cod. Cliente</td>
                        <td class="clsAnchoTabla">Descripción</td>
                        <td class="clsAnchoTabla">Rev.</td>
                        <td class="clsAnchoTabla">Tipo</td>
                        <td class="clsAnchoTabla">Cantidad</td>
                        <td class="clsAnchoTabla">Acciones</td>
                    </tr>
                </thead>
                @if(isset($ejecu_deta))
                @foreach($ejecu_deta as $deta)
                <tbody>
                    <tr>
                        <td class="clsAnchoTabla">{{ $deta['item'] }}</td>
                        <td class="clsAnchoTabla">{{ $deta['codigo']}}</td>
                        <td class="clsAnchoTabla">{{ $deta['descripcion'] }}</td>
                        <td class="clsAnchoTabla"  style="max-width:100px !important;">
                            {{ $deta['revision'] }}
                        </td>
                        <td class="clsAnchoTabla"></td>
                        <td class="clsAnchoTabla">{{ $deta['cantidad'] }}</td>         
                        <td class="clsAnchoTabla">
                            <a href="#" onclick="eliminarItem({{ $deta['ctransmittalejedetalle'] }})" data-toggle="tooltip" data-container="body" title="Eliminar">
                            <i class="fa fa-trash-o" aria-hidden="true"></i></a>
                            <a href="#" onclick="editarItem({{ $deta['ctransmittalejedetalle'] }})" data-toggle="tooltip" data-container="body" title="Editar">
                            <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i></a>            
                        </td>           
                    </tr>  
                @endforeach
                @endif
                </tbody>
            </table>