
<input type="hidden" name="" value="{{ $flujo }}" id="verdetallerruteo">
<table class="table table-striped table-hover table-bordered" style="font-size: 12px;" id="ruteotableapr">
  
  <thead>
    <tr class="clsCabereraTabla">
      <th>Ítem</th>
      <th>Código</th>
      <th>Nombre</th>
      <th>Cliente</th>
      <th>Unidad Minera</th>
      <th>Fecha de recepción</th>
      <th>Cantidad de Entregables</th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($proyectos as $key => $value)

    <?php //dd($proyectos) ?>
    <tr class="entregable_ruteo" data-proyecto="{{ $value->cproyecto }}">
      <td>{{ $key + 1 }}</td>
      <td>{{ $value->codigo }}</td>
      <td>{{ $value->nombre }}</td>
      <td>{{ $value->cliente }}</td>
      <td>{{ $value->uminera }}</td>
      <td>{{ $value->fecha_modificacion }}</td>
      <td><center>{{ $value->count }}</center></td>
      <td>
        <center>
          <a data-toggle="tooltip" data-placement="top" title="Ver detalle" class="btn verdetalle" id="verdetalle_{{ $value->cproyecto }}_{{ $value->fecha_modificacion }}"><i class="fa fa-eye"></i></a>

            <!-- <form action="{{ url('generar_transmittal_nuevo',$value->cproyecto)}}?ent={{ $value->centregables_todos }}" method="post" target="_blank"> -->

              <a href="{{ url('generar_transmittal_nuevo',$value->cproyecto)}}?ent={{ $value->centregables_todos }} " data-toggle="tooltip" data-placement="top" title="Ir a TR" class="btn " data-entregables="{{ $value->centregables_todos }}" target="_blank"><i class="fa fa-list-alt"></i></a>

              <!-- <input type="submit" data-toggle="tooltip" data-placement="top" title="TR" class="btn" data-entregables="{{ $value->centregables_todos }}" target="_blank"><i class="fa fa-hand-peace-o"></i> -->
              
            </form>

        </center>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>

<script>

 

</script>

