<!-- Modal -->
    <div id="viewLeyenda" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Leyenda Rooster</h4>
          </div>
          <div class="modal-body">
            <div class="panel panel-default">
          <!-- Default panel contents -->
              
              <div class="panel-body">
                  <table id="tblLeyenda" class="table">
                      <thead>
                          <th>Código</th>
                          <th>Descripción</th>
                      </thead>
                      <tbody>
                          @if(isset($leyenda))
                              @foreach($leyenda as $ley)
                                  <tr>
                                      <td style="background-color: rgb({{ $ley['color'] }})">
                                          {{ $ley['cod'] }}
                                      </td>
                                      <td style="background-color: rgb({{ $ley['color'] }})">
                                          {{ $ley['des'] }}
                                      </td>
                                  </tr>
                              @endforeach
                          @endif
                      </tbody>
                  </table>
              </div>

          
            
            </div>
          </div>
          <div class="modal-footer">
            
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>  
<!-- fin modal -->