<table class="table " id="tableActividad">


                  <thead>
                    <tr class="clsCabereraTabla">
                      <th>Item</th>
                      <th style="min-width:200px!important;">Descripción de Actividad</th>
                      <th>Docs</th>
                      <th>Planos</th>
                      @foreach($listEstr as $est)
                      <th>{{ $est['descripcionrol'] }}<br />
                      {{ $est['rate'] }} <br />
                      <a href="#" class="btn btn-default btn-xs" onclick="profesional_senior('{{ $est['cestructuraproyecto'] }}');">
                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                      </a>
                      </th>
                      @endforeach
                      @foreach($listEstr_dis as $est)
                        <th>{{ $est['descripcionrol'] }}<br />
                        {{ $est['rate'] }} </th>
                        @foreach($listDis as $dis)
                        <th>
                        {{ $dis['abrevia'] }}
                        <br />
                        <a href="#" class="btn btn-default btn-xs" onclick="profesional_disciplina('{{ $est['cestructuraproyecto'] }}','{{ $dis['cdisciplina'] }}');">
                          <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                        </a>
                      
                        </th>     
                        @endforeach
                      @endforeach                      
                      @foreach($listEstr_apo as $est)
                      <th>{{ $est['descripcionrol'] }}<br />
                      {{ $est['rate'] }} <br />
                      <a href="#" class="btn btn-default btn-xs" onclick="profesional_apoyo('{{ $est['cestructuraproyecto'] }}');">
                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                      </a>
               
                      </th>     
                      @endforeach

                    </tr>
                  </thead>
                  @if(isset($listaAct))
                  <tbody>
                  <?php $i=0; ?>
                  @foreach($listaAct as $act)
                    <?php $i++; ?>
                    <tr
                    @if(!empty($act['cactividad_parent']))
                      class="active"
                    @endif
                    >
                      <td>{{ $act['item'] }} </td>
                      <td
                      @if(!empty($act['cactividad_parent']))
                        style="padding-left: 20px"
                      @endif                      
                      >{{ $act['descripcionactividad']}}</td>
                      <td>{{ $act['numerodocumentos'] }}</td>
                      <td>{{ $act['numeroplanos'] }}</td>   
                      @foreach($listEstr as $est)             
                      <td>
                      @if(!empty($act['cactividad_parent']))
                        @if(isset($est['hora_'.$est['cestructuraproyecto'].'_'.$act['cproyectoactividades'] ]))
                          {{ $est['hora_'.$est['cestructuraproyecto'].'_'.$act['cproyectoactividades']] }}
                        @else
                          0
                        @endif
                      @endif
                      </td>
                      @endforeach
                      @foreach($listEstr_dis as $est)
                        <td>
                        @if(!empty($act['cactividad_parent']))
                        0
                        @endif
                        </td>
                        @foreach($listDis as $dis)
                          <td class="warning">
                          @if(!empty($act['cactividad_parent']))
                            @if(isset($est['hora_'.$est['cestructuraproyecto'].'_'.$act['cproyectoactividades'].'_'.$dis['cdisciplina']  ]))
                              {{ $est['hora_'.$est['cestructuraproyecto'].'_'.$act['cproyectoactividades'].'_'.$dis['cdisciplina'] ] }}
                            @else
                              0
                            @endif                        
                          @endif
                          </td>
                        @endforeach
                      @endforeach
                      @foreach($listEstr_apo as $est)
                      <td>
                      @if(!empty($act['cactividad_parent']))
                        @if(isset($est['hora_'.$est['cestructuraproyecto'].'_'.$act['cproyectoactividades'] ]))
                          {{ $est['hora_'.$est['cestructuraproyecto'].'_'.$act['cproyectoactividades']] }}
                        @else
                          0
                        @endif
                      @endif
                      </td>                
                      @endforeach
                                                               
                    </tr>
                  @endforeach
                  </tbody>
                  @endif
                  <!--<tfoot>
                  <tr class="clsCabereraSubItem">
                      <td></td>
                      <td>Total Entregables / Total Horas</td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                  </tr>
                  <tr class="clsCabereraSubItem">
                      <td></td>
                      <td>Subtotal Costo horas</td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                  </tr>            
                  </tfoot>-->
               
              </table>