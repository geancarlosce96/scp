<table class="table " id="tableInputActividad">
{!! Form::hidden('cestructurapropuesta_h',( isset($cestructurapropuesta)==1?$cestructurapropuesta:'' ),array('id'=> 'cestructurapropuesta_h') ) !!}

    <thead>
      <tr class="clsCabereraTabla">
        <th>Item</th>
        <th style="min-width:200px!important;">Descripción de Actividad</th>

        <th>Horas
        </th>

      </tr>
    </thead>
    @if(isset($listaAct))
    <tbody>
    <?php $i=0; ?>
    @foreach($listaAct as $act)
      <?php $i++; ?>
      <tr
      @if(!empty($act['cactividad_parent']))
        class="active"
      @endif
      >
        <td>{{ $act['item'] }} </td>
        <td
        @if(!empty($act['cactividad_parent']))
          style="padding-left: 20px"
        @endif                      
        >{{ $act['descripcionactividad'] }}</td>
        <td>
        @if(!empty($act['cactividad_parent']))
        <input type="text" class="form-control" size="3" style="10px"  name="horas[{{ $act['cpropuestaactividades']  }}]" value="
        @if(isset($cestructurapropuesta))
          @if(isset($act['hora_'.$act['cpropuestaactividades']."_".$cestructurapropuesta]))
          {{ $act['hora_'.$act['cpropuestaactividades']."_".$cestructurapropuesta] }}
          @else
          0
          @endif
        @else
        0
        @endif
        " />
        @endif
        </td>

        
                                                 
      </tr>
    @endforeach
    </tbody>
    @endif

</table>