            <!-- <div class="form-group"> -->

      

            @if(isset($nuevo))

            <input type="hidden" id="nuevo" name="nuevo" value="{{$nuevo}}">

            <div class="row">
             
              <label class="col-sm-2 control-label">Usuario:</label>
              <div class="col-sm-8">
              		<input type="text" class="form-control" id="usuario" placeholder="Contraseña nueva" name="usuario" value="{{$nomusuario}}" readonly="true">        
              </div>
            </div>
            <br>
			
          
            <div class="row">
            	
            <label  class="col-sm-2 control-label">Contraseña Anterior:</label>
             <div class="col-sm-8">
             		<!-- Campo ingresado por el usuario -->
            		<input type="password" class="form-control" id="passwordAct" placeholder="Contraseña Actual" name="passwordAct" onkeyup="getpasw()">  

					<!-- Valor de contraseña encriptada ingresada por el usuario -->
            		<input type="hidden" class="form-control" id="passwordGETEncriptado" placeholder="Contraseña Ingresada" name="passwordGETEncriptado" value="">    
            		<!-- Contraseña encriptada Actual -->

            		<input type="hidden" class="form-control" id="passwordActEncriptado" placeholder="Contraseña actual" name="passwordActEncriptado" value="{{$passwordActual}}">   
              </div>
              <div class="col-sm-2">&nbsp;</div>
            	
            </div>

            <br>          
		

            <div class="row">
             
              <label class="col-sm-2 control-label">Contraseña Nueva:</label>
              <div class="col-sm-8">
              		<input type="password" class="form-control" id="password" placeholder="Contraseña nueva" name="password">        
              </div>
            </div>
            <br>

            <div class="row">
             
              <label class="col-sm-2 control-label">Confirmar Contraseña:</label>
              <div class="col-sm-8">
              		<input type="password" class="form-control" id="passwordconfirm" placeholder="Confirmar contraseña nueva" name="passwordconfirm">        
              </div>
            </div>

            @endif
            <!-- </div> -->
