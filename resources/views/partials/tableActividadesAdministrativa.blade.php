
<table class="table">
    <thead>
	    <tr class="clsCabereraTabla">
	      <th>Código</th>
	      <th>Tareas Administrativas</th>
	      <th>Selección</th>
	    </tr>
    </thead>
    <tbody>
    	@if(isset($actividadesAdm))
    		@foreach($actividadesAdm as $act)
		    <tr>
		      <td>{{ $act['codigo'] }}</td>
		      <td>{{ $act['descripcion'] }}</td>
		      <td style="padding:1px;">
		      	<input type="radio" name="sel"
						value="{{ $act['cactividad'] }}"
						onclick="viewSemana({{ $act['cactividad'] }});"
						>
		      </td>

		    </tr>
		    @endforeach
	    @endif
    </tbody>
</table>
