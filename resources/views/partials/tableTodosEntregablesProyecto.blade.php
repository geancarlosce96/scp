



<table class="table table-striped table-hover" id="tableEntregablesTodosPry">
@if(isset($columnas_nombre))
  <caption>
    <select name="columna" multiple class="form-control columnas_nombre" >
      @foreach ($columnas_nombre as $key => $columna)
        <option value="{{$columnas_id[$key]}}"  {{ $selecion_opcion[$key] }}>{{$key+1}}-{{$columna}}</option>
      @endforeach
      option
    </select>
  </caption>
@endif
    <thead>
      <tr class="clsCabereraTabla">
        <th>#</th>
        @if(isset($columnas_nombre))
        @foreach ($columnas_nombre as $key => $colum)
          @if($colum != 'Fechas')
            <th class="{{$columnas_id[$key]}}">{{$colum}}</th>
          @else
            @if(isset($fechasCab)) 
              @foreach($fechasCab as $f)
                <th class="{{$columnas_id[$key]}}">{{$f->tipofec}}</th>
              @endforeach
            @endif
          @endif
        @endforeach
        @endif
       <!--  <th>Estado</th>
        <th>Fase</th>
        <th>Entregable</th>
        <th>Tipo</th>
        <th>Código</th>
        <th>Rev.</th>
        <th>Disciplina</th>
        <th>Descripción</th>
        @if(isset($fechasCab)) 
          @foreach($fechasCab as $f)
            <th>{{$f->tipofec}}</th>
          @endforeach
        @endif -->
<!--       
          <th id="fech_head">Fecha0</th>   
        
-->           
        <!-- <th>Observación</th>
        <th>Responsable</th>
        <th>Elaborador</th> -->
        <th>Acciones</th>
        <th>
          <center>
            <div class="btn-group">
              <button type="button" class="btn btn-primary">
                <input type="checkbox" class="minimal" name="chkTodos" id="chkTodos" onclick="setCheck(this);" title="Seleccionar todos los entregables">
              </button>
              <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Seleccionar la acción a relizar a los entregables seleccionados">
                <span class="caret"></span>
                <span class="sr-only"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a onclick="verModalfechas('general')" class=""><i class="fa fa-calendar"></i>Agregar fechas</a></li>
                <li role="separator" class="divider"></li>
                <li><a class="entremasivo"><i class="fa fa-cart-plus"></i>Para validar</a></li>
              </ul>
            </div>
          </center>
        </th>
      </tr>
    </thead>
    @if(isset($listEnt))   
    <tbody>

      @foreach($listEnt as $key => $ent)
      <?php $color=''; ?>
        @if($ent['activo']=='0')

        <?php $color='red'; ?>

        @endif 

        @if($ent['estado']=='Modificado') 

        <?php $color='yellow'; ?>

        @endif   

      <!--<tr id="fila_{{ $ent['cproyectoentregables'] }}" style="@if($ent['activo']=='0')text-decoration:line-through; color: red; @endif @if($ent['estado']=='Modificado') background-color: yellow @endif"  data-edt="{{ $ent['cproyectoedt'] }}" data-ident="{{ $ent['cproyectoentregables'] }}" data-anexos="{{ $ent['anexos'] }}"> -->

      <tr id="fila_{{ $ent['cproyectoentregables'] }}" data-edt="{{ $ent['cproyectoedt'] }}" data-ident="{{ $ent['cproyectoentregables'] }}" data-anexos="{{ $ent['anexos'] }}"> 
      
        <td style="text-decoration:line-through; color: white">
        
            @if($ent['anexos']>0 )

            <a id="btnmostrar_{{ $ent['cproyectoentregables'] }}" class="mostrarAnexos" href="#" title="Ver anexos"  style="color: green;font-size: 13px;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
            <a id="btnocultar_{{ $ent['cproyectoentregables'] }}" class="ocultarAnexos" href="#" title="Ocultar anexos"  style="color: red;font-size: 13px;display: none"><i class="fa fa-minus-circle" aria-hidden="true" ></i></a>;
            
            @else 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            @endif

            <div class="input-group">
              <div class="pull-left">

              @if($ent['cantidadFechas']<=4 )
             
                <a href="#" title="¡Las fechas no están completas!" style="color: red"><i class="fa fa-exclamation" aria-hidden="true"></i>&nbsp;<i class="fa fa-calendar-times-o" aria-hidden="true"></i></a>&nbsp;
              @else
                
                <a href="#" title="¡Fechas completas!" style="color: gray">&nbsp;&nbsp;&nbsp;<i class="fa  fa-calendar-check-o" aria-hidden="true"></i></a>&nbsp;
              @endif
              
              </div>            
              
              <div class="pull-right">
                
                <label style="color: black;font-size: 12px;"><strong><!-- {{ $ent['item'] }} --></strong></label> 

              </div>
            </div>

        </td>

        <td class="1"><span class='label' style="color: black;font-size: 12px;background-color: <?php echo($color) ?>;">{{ $ent['estado'] }}</span></td>

        <td class="2 faseEnt">

          <span id="sp_fase_{{ $ent['cproyectoentregables'] }}">{{ $ent['fase'] }}</span>

            <div id="divfase_{{ $ent['cproyectoentregables'] }}">

                <select class="form-control faseEntregable" name="faseEntr" id="fase_{{ $ent['cproyectoentregables'] }}" style="display: none">

                  @if(isset($faseProy)==1)
                  <?php //dd($tipoProyEntregable);
                      foreach ($faseProy as $id => $descripcion):
                          $fase='';

                          if ($id==$ent['cfaseproyecto']){
                              $fase=' selected="'.$ent['cfaseproyecto'].'"';

                          }
                          echo '<option'.$fase.' value="'.$id.'">'.$descripcion.'</option>';
                      endforeach;                                   
                  ?>
                  @endif      

                </select> 
             
              
            </div>
        </td>

        <td class="3" >{{ $ent['tipoentregable'] }}</td>
        <td class="4" >{{ $ent['tipoproyentregable'] }}</td>
        <td class="5">

        <?php $cod=strlen($ent['codigo']); ?>


              @if($cod<=0)
                <!-- <a href="#" class="label label-info" title="Asignar actividades" style="color: black;font-size: 12px;">Sin Código</a> -->
                <span style="color: black;font-size: 12px; text-align: center;" class="label label-info">Sin Código</span>

                
              @else
                {{ $ent['codigo'] }}

              @endif

                         

        </td>
        <td class="6"><center>{{ $ent['des_revision'] }}</center></td>
        <td class="7" >{{ $ent['des_dis'] }}</td>

        
        <td class="8 descripcion_ent">
            <span id="sp_tdesc_{{ $ent['cproyectoentregables'] }}">{{ $ent['descripcion'] }}</span>
            <input id="tdesc_{{ $ent['cproyectoentregables'] }}" class="form-control descripcionEnt" type="text" name="" style='display:none' value="{{ $ent['descripcion'] }}">  

        </td> 

        @if(isset($fechasCab)) 
          @if($ent['fechas']!='SinFechas')
          <?php $i=0; ?>
          
            @foreach($ent['fechas'] as $f) 
           
            <?php 

            $claseMostrarInput='fechasTodEnt'; 

              if (strlen($f['fecha'])<1){
                $i++;
                if ($i==1) {

                  $claseMostrarInput='fechasTodEnt'; 
                 
                }

                if($i>1){
                  $claseMostrarInput='';

                }

              } ?>
             
      
            <?php  ?>
            <td class="9 {{$claseMostrarInput}}" id="td_fecha_{{ $ent['cproyectoentregables'] }}_{{$f['ctipo']}}"  data-ctipo="{{$f['ctipo']}}" disabled="true">
                <span id="sp_tfec_{{ $ent['cproyectoentregables'] }}_{{$f['ctipo']}}">{{$f['fecha']}}</span>
                <input id="tfec_{{ $ent['cproyectoentregables'] }}_{{$f['ctipo']}}" class="input-sm fechaEnt" type="text"  name="" style='display:none' value="{{$f['fecha']}}">
            </td>
            
            @endforeach 
          @else  
            @foreach($fechasCab as $f) 

            <?php 

                $claseMostrarInput=''; 
                 if ($fechasCab[0]==$f): 
                   $claseMostrarInput='fechasTodEnt'; 
                  
                endif 
             ?>

            <td class="9 {{$claseMostrarInput}}" id="td_fecha_{{ $ent['cproyectoentregables'] }}_{{$f->ctipofechasentregable}}" data-ctipo="{{$f->ctipofechasentregable}}">
                <span id="sp_tfec_{{ $ent['cproyectoentregables'] }}_{{$f->ctipofechasentregable}}"></span>
                <input id="tfec_{{ $ent['cproyectoentregables'] }}_{{$f->ctipofechasentregable}}" class="input-sm fechaEnt" type="text"  name="" style='display:none' value="">
            </td>

            @endforeach 
          @endif
        @endif
       
        <td class="10 observacion">
            <span id="sp_tobs_{{ $ent['cproyectoentregables'] }}">{{ $ent['observacion'] }}</span>
            <input id="tobs_{{ $ent['cproyectoentregables'] }}" class="input-sm observacionEnt" type="text"  name="" style='display:none' value="{{ $ent['observacion'] }}">
              
        </td>
        <td class="11 responsable">
            <span id="sp_tresp_{{ $ent['cproyectoentregables'] }}">{{ $ent['responsable'] }}</span>

            <?php  //dd($responsable);?>

            <select class="input-sm responsableEnt" name="responsable" id="tresp_{{ $ent['cproyectoentregables'] }}" style="display: none">

              @if(isset($responsable)==1)
              <?php //dd($responsable);
                  foreach ($responsable as $id => $descripcion):
                      $resp='';

                      if ($id==$ent['cpersona_responsable']){
                          $resp=' selected="'.$ent['cpersona_responsable'].'"';

                      }
                      echo '<option'.$resp.' value="'.$id.'">'.$descripcion.'</option>';
                  endforeach;                                   
              ?>
              @endif                 

            </select> 
        </td>

        <td class="12" >{{ $ent['elaborador'] }}</td>
        <td style="text-decoration:line-through; color: white; font-size: 12px">   

          <center>           
            <!-- <a class="verActivProy" title="Asociar actividades del proyecto al entregable" ><i class="fa fa-list-ul" aria-hidden="true"></i></a> &nbsp; -->
            @if($ent['activo']=='1')
            <a class="anularEnt" title="Anular entregable" style="color: red"><i class="fa fa-minus-square" aria-hidden="true"></i></a> &nbsp;
            @else
            <a class="activarEnt" title="Activar entregable" style="color: green"><i class="fa fa-check-square" aria-hidden="true" ></i></a> &nbsp;
            @endif
            <a class="fechasEnt"  title="Agregar fechas al entregable" ><i class="fa fa-calendar" aria-hidden="true" ></i></a>&nbsp;
            <a class="addruteo" title="Agregar entregable para validar" ><i class="fa fa-cart-plus"></i></a> 
          </center>
        </td>
        <td>
          <center>
            <input type="checkbox" name="checkbox_ent[]" id="checkbox_ent" value="{{ $ent['cproyectoentregables'] }}" title="Seleccionar entregable">
          </center></td>
      </tr>
      @endforeach
    </tbody>
    @endif

</table>



<script type="text/javascript">

$('.columnas_nombre').chosen(
    {
        allow_single_deselect: true,
        width:"100%",
        placeholder_text_multiple :"Columnas a ocultar"

    });

// $('.buttons-columnVisibility').click(function(event) {
//   alert(event);
// });
// 
$('.columnas_nombre > option').each(function(index, el) {
    if ($(this).prop("selected") == true) {
      $('.'+$(this).val()).hide();
    }else{
      $('.'+$(this).val()).show();
    }
  console.log("soe",$(this).prop("selected"),$(this).val());
  });

$('.columnas_nombre').on('change',function(){
  var sele = $(this).val();
  seleccionarcolumnas(sele,1);
  $('.columnas_nombre > option').each(function(index, el) {
    if ($(this).prop("selected") == true) {
      $('.'+$(this).val()).hide();
    }else{
      $('.'+$(this).val()).show();
    }
  // console.log("soe",$(this).prop("selected"),$(this).val());
  });
});

function seleccionarcolumnas(option,tipo){
  $.ajax({
    url: 'seleccionarcolumnas',
    type: 'GET',
    // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
    data: {'option': option,'tipo': tipo},
  })
  .done(function(data) {
    console.log("seleccionarcolumnas",data);
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
  
}

$("a.mostrarAnexos").on("click",function(){

  var id=$(this).parent().parent().data("ident");
  obtenerAnexos(id);

});


$("a.ocultarAnexos").on("click",function(){
  var id=$(this).parent().parent().data("ident");
  var anexos=$(this).parent().parent().data("anexos");
  ocultarAnexos(id,anexos);

});

tabla_eventos_EntregablesTodos();

initDataTable();
var options={
   "paging":   false,
  "ordering": true,
  "info":     true,
  "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            },
  lengthChange: true,
  columnDefs: [
  { targets: [-1], orderable: false }
  ],
  // buttons: [ /*'excel', 'pdf', 'csv' ,*/'colvis' ]
  // dom: 'Bfrtip',
  // buttons: [
  //       {
  //           extend: 'colvis',
  //           columns: ':gt(0)',
  //           text: 'Ocultar columnas',
  //           // columns: [ 1,2,3,4,5,6,7,8,9],
            
  //       }]

    //     buttons: [ {
    //     extend: 'colvis',
    //     text: 'Ocultar columnas',
    //     columnText: function ( dt, idx, title ) {
    //         return (idx+1)+': '+title;
    //     },
    //     // columns: ':gt('+idx+')'
    // } ]

};
var tableEnt;

function initDataTable() {
  tableEnt =  $('#tableEntregablesTodosPry').DataTable(options);
  // tableEnt.buttons().container().appendTo( '#tableEntregablesTodosPry_wrapper .col-sm-6:eq(0)' );
}


// $('#tableEntregablesTodosPry').on( 'column-visibility.dt', function ( e, settings, column, state ) {
//     console.log(
//         'Column '+ column +' has changed to '+ (state ? 'visible' : 'hidden')
//     );
// } );



function tabla_eventos_EntregablesTodos(){

  $(".addruteo").off('click');
  $('.addruteo').click(function() {
    var id=$(this).parent().parent().parent().data("ident");
    grabaruteo(id,'sele');
  });

 $(".entremasivo").off('click');
 $('.entremasivo').click(function() {
  validarcheck();
  });


  $(".descripcion_ent").dblclick(function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();

    mostrar_input_ent(id,desc);

    //tabla_eventos();
  });

  $(".observacion").on("click",function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();

    mostrar_input_ent(id,desc);

  });

  $(".responsable").on("click",function() {

    var id=$(this).find("select").attr("id");
    var desc=$(this).find("span").text();  

    mostrar_input_ent(id,desc);
  });

  $(".tipo").on("click",function() {

    var id=$(this).find("select").attr("id");
    var desc=$(this).find("span").text();  

    mostrar_input_ent(id,desc);

    //actualizar_entregable($(this),'tipo');

  });

  $(".fechasTodEnt").dblclick(function() {

    var id=$(this).find("input").attr("id");
    var desc=$(this).find("span").text();

    mostrar_input_ent(id,desc);

  });


  $(".descripcionEnt").focusout(function() {
    //console.log($(this));

    actualizar_entregable($(this),'descripcion');

  });

  $(".observacionEnt").focusout(function() {
    //console.log($(this));

    actualizar_entregable($(this),'observacion');

  });

  $(".responsableEnt").focusout(function() {

    //.on("change",function() {
    //console.log($(this).val());

    actualizar_entregable($(this),'responsable');

  });


  $(".tipoEnt").focusout(function() {

    //.on("change",function() {
    //console.log($(this).val());

    actualizar_entregable($(this),'tipo');

  });


  $('.fechaEnt').datepicker({
      format: "dd-mm-yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "<?php echo date('d/m/Y'); ?>",
      todayHighlight: true,

  }).on("input changeDate", function (e) {
      actualizar_entregable($(this),'fecha')
  });

  $("a.verActivProy").off('click');
  $("a.verActivProy").on("click",function() {

    var id=$(this).parent().parent().parent().data("ident");

    verActividadesProyecto(id);

  });

  $("a.anularEnt").off('click');
  $("a.anularEnt").on("click",function() {
    var id=$(this).parent().parent().parent().data("ident");

    activarEnt(id,'1');

  });

  $("a.activarEnt").off('click');
  $("a.activarEnt").on("click",function() {
    var id=$(this).parent().parent().parent().data("ident");

    activarEnt(id,'0');

  });

  $("a.fechasEnt").off('click');
  $("a.fechasEnt").on("click",function() {
    var id=$(this).parent().parent().parent().data("ident");
    var descripcion=$(this).parent().parent().parent().find(".descripcion_ent").text();    

    verfechasEntregable(id,descripcion,'entregable');

  });

  $(".faseEnt").off('click')
  $(".faseEnt").on("click",function() {


    var idSpanFase=$(this).find("select").attr("id"); 
    var valSpanFase=$(this).find("select").text(); 
   
    mostrar_input_ent(idSpanFase);

  });
  $(".faseEntregable").focusout(function() {

      actualizar_entregable($(this),'fase');

    }); 

 
}



</script>




