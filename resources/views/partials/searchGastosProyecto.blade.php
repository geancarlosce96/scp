<!-- Modal Buscar Gastos Proyecto-->
    <div id="searchGastosProy" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Gastos Proyecto</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tGastosProy" class="table" width="100%" >
            <thead>
              <th>ID</th> 
              <th>Nro Rendición</th>
              <th>Decripción</th>
              <th>Valor Cambio</th>
              <th>Total</th>
              </thead>
           
          </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Gastos Proyecto-->