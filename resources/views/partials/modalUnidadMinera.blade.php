<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
      	<thead>
        <tr>
        	<td class="clsCabereraTabla">Código</td>
            <td class="clsCabereraTabla">Nombre</td>
            <td class="clsCabereraTabla">Estado</td>
            <td class="clsCabereraTabla">Dirección</td>
            <td class="clsCabereraTabla">País- Dpto- Prov.- Dist</td>
            <td class="clsCabereraTabla">Acción</td>
          </tr>  
         </thead>
         <tbody>
         @if(isset($tunidadminera)==1)
         @foreach($tunidadminera as $tum)
         <tr>
         	<td class="clsAnchoTabla">{{ $tum['codigo'] }}</td>
            <td class="clsAnchoTabla">{{ $tum['nombre'] }}</td>
            <td class="clsAnchoTabla">{{ $tum['destado'] }}</td>
            <td class="clsAnchoTabla">{{ $tum['direccion'] }}</td>
            <td class="clsAnchoTabla">{{ $tum['ubigeo'] }}</td>
            <td class="clsAnchoTablaMax">
                    <a href="#" onclick="editarUminera('{{ $tum['cunidadminera'] }}')" data-toggle="tooltip" data-container="body" title="Editar">
                    <i class="fa fa-pencil" aria-hidden="true"></i></a>   &nbsp;&nbsp;
                    <a href="#" onclick="eliminarUminera('{{ $tum['cunidadminera'] }}')" data-toggle="tooltip" data-container="body" title="Eliminar">
                    <i class="fa fa-trash" aria-hidden="true"></i></a>   &nbsp;&nbsp;  
                    <a href="#" onclick="contactoUminera('{{ $tum['cunidadminera'] }}')" data-toggle="tooltip" data-container="body" title="Contactos">
                    <i class="fa fa-user" aria-hidden="true"></i></a> &nbsp;&nbsp; 
                    <a href="#" onclick="editarUminera('{{ $tum['cunidadminera'] }}')" data-toggle="modal" data-container="body" title="Contactos" data-target="#divUmineralogo">
                    <i class="fa fa-photo" aria-hidden="true"></i></a>                                                  
            </td>
         </tr>
         @endforeach
        @endif
                                            
         </tbody>
      </table>
     </div>
</div>