    <div class="col-lg-1 col-xs-4">Proyecto:</div>
    <div class="col-lg-4 col-xs-4">
        <div class="input-group input-group-normal">
            <input type="text" class="form-control input-sm" id="tproyecto" placeholder="" name="tproyecto" value="{{ isset($tproyecto->nombre)==1?$tproyecto->nombre:'' }}" disabled> 
            {!! Form::hidden('cproyecto',(isset($tproyecto->cproyecto)==1?$tproyecto->cproyecto:''),array('id'=>'cproyecto'))  !!}
            {!! Form::hidden('cservicioproy',(isset($tproyecto->cservicio)==1?$tproyecto->cservicio:''),array('id'=>'cservicioproy'))  !!}
            {!! Form::hidden('codproyecto',(isset($tproyecto->codigo)==1?$tproyecto->codigo:''),array('id'=>'codproyecto'))  !!}
            <span class="input-group-btn">
            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target='#searchProyecto'><b>...</b></button>
            </span>
            <span class="input-group-btn">
            <button type="button" class="btn btn-primary btn-sm" onclick="cleanProyecto()"><b>X</b></button>
            </span>                            
        </div>  
    </div>  
    <div class="col-lg-1 col-xs-1">Unidad Minera:</div>
    <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="nombreUMineraProy" name="nombreUMineraProy" value="{{ (isset($tumineraproy->nombre)==1?$tumineraproy->nombre:'' ) }}" disabled></div>
    <div class="col-lg-1 col-xs-12 clsPadding">Cliente:</div>
    <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="nombreClienteProy" name="nombreClienteProy" value="{{ (isset($persona->nombre)==1?$persona->nombre:'' ) }}" disabled></div>