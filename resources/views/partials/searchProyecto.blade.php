<!-- Modal Buscar Proyecto-->
    <div id="searchProyecto" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color: white">Buscar proyecto</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tProyecto" class="table table-responsive table-hover" style="font-size: 12px" width="100%">
                <thead>
                <tr>
                  <th scope="col" >Ítem</th>
                  <th scope="col" >Código</th>
                  <th scope="col" >Gerente de Proyecto</th>
                  <th scope="col" >Cliente</th>
                  <th scope="col" >Unidad Minera</th>
                  <th scope="col" >Proyecto</th>
                  <th scope="col" >Estado</th>
                </tr>
                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Proyecto-->