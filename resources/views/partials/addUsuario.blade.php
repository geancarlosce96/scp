<!-- Modal agregar Usuario-->
    <div id="addUs" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:#0069AA; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Usuario</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarUsuario','method' => 'POST','id' =>'frmusuarios','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cusuario',(isset($usuario->cusuario)==1?$usuario->cusuario:''),array('id'=>'cusuario')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        
              <div class="form-group">
                <label for="nombre" class="col-sm-2 control-label">Usuario</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="nombre" placeholder="Usuario" name="nombre" value="{{(isset($usuario->nombre)==1?$usuario->nombre:'')}}">      
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
            <label for="cpersona" class="col-sm-2 control-label">Nombre</label>
              <div class="col-sm-8">


               {!! Form::select('cpersona',(isset($tnombrepersona)==1?$tnombrepersona:null),(isset($usuario->cpersona)==1?$usuario->cpersona:''),array('class' => 'form-control select-box','id'=>'cpersona')) !!}   


              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>
            


            <div class="form-group">
              <label for="estado" class="col-sm-2 control-label">Estado</label>
              <div class="col-sm-8">
             

               {!! Form::select('estado',(isset($testado)==1?$testado:null),(isset($usuario->estado)==1?$usuario->estado:''),array('class' => 'form-control select-box','id'=>'estado')) !!}   
             

              </div>     
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 
            
            <div class="form-group">
              <label for="fhasta" class="col-sm-2 control-label">Fecha Límite</label>
              <div class="col-sm-8">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                       {!! Form::text('fhasta',(isset($usuariopassword->fhasta)==1?$usuariopassword->fhasta:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'fhasta') ) !!}
                    </div>                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 

            <div class="form-group">
              <label for="fdesde" class="col-sm-2 control-label">Fecha Inicio</label>
              <div class="col-sm-8">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                       {!! Form::text('fdesde',(isset($usuariopassword->fdesde)==1?$usuariopassword->fdesde:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'fdesde') ) !!}
                    </div>                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 

            <div class="form-group">
             <label for="password" class="col-sm-2 control-label">Contraseña</label>
              <div class="col-sm-8">
              <input type="text" class="form-control" id="password" placeholder="Contraseña" name="password">      
              </div>
              <div class="col-sm-2">&nbsp;
              </div> 
           
            </div>


            <div class="form-group">
              <label for="caduca" class="col-sm-2 control-label">Caduca</label>
              <div class="col-sm-8">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                       {!! Form::text('caduca',(isset($usuariopassword->caduca)==1?$usuariopassword->caduca:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'caduca') ) !!}
                    </div>                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 
          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" data-dismiss="modal" id="idAddUsua" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="plugins/datepicker/locales/bootstrap-datepicker.es.js"></script>
<script>


     $('#fhasta').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });   

    $('#fdesde').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        });  


    $('#caduca').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true,
            firstDay: 1,
            calendarWeeks:true,
            /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
        }); 


</script>  

<!--Inicion del Script para funcionamiento de Chosen -->
<script>
    $('.select-box').chosen(
        {
            allow_single_deselect: true,
            width: "100%",
        });
</script>
<!-- Fin del Script para funcionamiento de Chosen -->