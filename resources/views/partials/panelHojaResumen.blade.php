    <!-- Modal Informacion -->
    <div id="btnHRe" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Hoja Resumen</h4>
          </div>
          <div class="modal-body">

          <div class="form-group">
              <label for="cpersonacliente" class="col-sm-2 control-label">Cliente</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersonacliente',(isset($hojaRes->cpersonacliente)==1?$hojaRes->cpersonacliente:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersonacliente') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-8">
                      {!! Form::text('nombre',(isset($hojaRes->nombre)==1?$hojaRes->nombre:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'nombre') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="cunidadminera" class="col-sm-2 control-label">Unidad Minera</label>
                  <div class="col-sm-8">
                      {!! Form::text('cunidadminera',(isset($hojaRes->cunidadminera)==1?$hojaRes->cunidadminera:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cunidadminera') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="cproyectodocumentos" class="col-sm-2 control-label">Cod Documento</label>
                  <div class="col-sm-8">
                      {!! Form::text('cproyectodocumentos',(isset($hojaRes->cproyectodocumentos)==1?$hojaRes->cproyectodocumentos:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyectodocumentos') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Cod Proyecto</label>
                  <div class="col-sm-8">
                      {!! Form::text('codigo',(isset($hojaRes->codigo)==1?$hojaRes->codigo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'codigo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          <div class="form-group">
              <label for="cestadoproyecto" class="col-sm-2 control-label">Estado</label>
                  <div class="col-sm-8">
                      {!! Form::text('cestadoproyecto',(isset($hojaRes->cestadoproyecto)==1?$hojaRes->cestadoproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cestadoproyecto') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="finicio" class="col-sm-2 control-label">Fecha Inicio</label>
                  <div class="col-sm-8">
                      {!! Form::text('finicio',(isset($hojaRes->finicio)==1?$hojaRes->finicio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'finicio') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="fcierre" class="col-sm-2 control-label">Fecha Cierre</label>
                  <div class="col-sm-8">
                      {!! Form::text('fcierre',(isset($hojaRes->fcierre)==1?$hojaRes->fcierre:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fcierre') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="revisionactual" class="col-sm-2 control-label">Rev Documento</label>
                  <div class="col-sm-8">
                      {!! Form::text('revisionactual',(isset($hojaRes->revisionactual)==1?$hojaRes->revisionactual:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'revisionactual') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="fechadoc" class="col-sm-2 control-label">Fecha Documento</label>
                  <div class="col-sm-8">
                      {!! Form::text('fechadoc',(isset($hojaRes->fechadoc)==1?$hojaRes->fechadoc:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fechadoc') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  
       

                   


             {!! Form::close() !!}

           </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>





</script>