                    <?php 

                      $num = $nroFilaI;
                      $i=$num; 
                      $j=$num-1;                      
                    ?>
                    @if(isset($alinea))
                    @foreach($alinea as $eje)
                    <?php 
                      $fec = Carbon\Carbon::createFromDate($fecp->year,$fecp->month,$fecp->day);
                      $i++;
                      $j++;
                      $num++;
                    ?>

                    <tr class="gradeX" id="row_{{ $i }}">
                      <th style="width: 22px; border:1px solid #DCDCDCFF;">
                        <center>
                          @if($eje['tipo']=='1')
                          <a href="#" onclick="clonarfila('{{ ($i) }}')" class="fa fa-plus"></a><br />
                          @endif
                          <a href="#" onclick="eliminarFila({{ ($i) }},{{ $j }})" class="fa fa-trash"></a>
                          <!--<a href="#" class="fa fa-save"></a>&nbsp;&nbsp;&nbsp;&nbsp;-->
                        </center>
                      </th>                     
                      <th style="width: 1050px; font-size:12px;  border:1px solid #DCDCDCFF;">
                        <input type='hidden' name='ejecu[{{ $j }}][0]' id="pry_{{ $i }}" value="{{ $eje['cproyectoactividades'] }}" />
                        <input type='hidden' name='ejecu[{{ $j }}][1]' id="act_{{ $i }}" value="{{ $eje['cactividad'] }}" />
                        <input type='hidden' name='ejecu[{{ $j }}][2]' id="tipo_{{ $i }}" value="{{ $eje['tipo'] }}" />
                        <input type='hidden' name='ejecu[{{ $j }}][51]' id="cproy_{{ $i }}" value="{{ $eje['cproyecto'] }}" />
                        <input type='hidden' name='ejecu[{{ $j }}][52]' id="desAct_{{ $i }}" value="{{ $eje['descripcionactividad'] }}" />
                        <input type='hidden' name='ejecu[{{ $j }}][53]' id="removeAct_{{ $i }}" value="N" />
                        <input type='hidden' name='ejecu[{{ $j }}][54]' id="ejecab_{{ $i }}" value="{{ $eje['cproyectoejecucioncab'] }}" />     
                        <input type='hidden' name='proys_{{ $j }}' id="desc_{{ $i}}" value="{{ $eje['codigopry'] }}/{{ $eje['personanombre'] }} / {{ $eje['unidadminera'] }} / {{ $eje['codigoactividad'] }} / "  />
                        @if($eje['codigopry'])
                         {{ $eje['codigopry'] }}/{{ $eje['personanombre'] }} / {{ $eje['unidadminera'] }} / {{ $eje['codigoactividad'] }} / 
                        @endif
                         {{ $eje['descripcionactividad'] }}    
                      </th>
                      <th style="width: 40px; border:1px solid #DCDCDCFF;" class="categoria">{{$eje['categoriaproy']}}</th>
                      <th style="width: 40px; border:1px solid #DCDCDCFF;" onmouseover="mouseOver('dash_tarea_{{ $i }}');" onmouseout="mouseOut('dash_tarea_{{ $i }}');">
                        <center>
                          @if($eje['tipo']=='1')
                          <span class="label" style="background-color:#11B15B;">FF</span>
                          @endif
                          @if($eje['tipo']=='2')
                          <span class="label" style="background-color:#CD201A;">NF</span>
                          @endif
                          @if($eje['tipo']=='3')
                          <span class="label" style="background-color:#0069AA;"">AND</span>
                          @endif
                        </center>
                      </th>
                      <th style="width: 60px; border:1px solid #DCDCDCFF;">
                        <center>
                        {!! Form::select('ejecu['.$j.'][3]',(isset($nofacturables)==1?$nofacturables:array()),'',array('id'=>'nfac_'.$i) ) !!} 
                        </center>
                      </th>
                      <th style="width: 60px; border:1px solid #DCDCDCFF;">
                        <div id="gp_{{ $i }}">
                          <center>
                            {{ $eje['gp'] }}
                          </center>
                        </div>
                      </th>
                      <!--<td class="clsAnchoTabla"></td>-->
                      <!--<td >&nbsp;</td>-->
                            <?php
                            $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
                            ?>                      
                      <th style="width: 60px; padding:0;  border:1px solid #DCDCDCFF;">
                        <center class="div-celda">
                          <div>
                            <input type='hidden' name='ejecu[{{ $j }}][4]' value="" />
                            <input type='hidden' name='ejecu[{{ $j }}][5]' value="2" />
                            <input type='hidden' name='ejecu[{{ $j }}][6]' value="" id="txt_{{ $j }}_6"/>
                            <input type='hidden' name='ejecu[{{ $j }}][7]' value="{{ $key }}" />
                            <input type='hidden' name='ejecu[{{ $j }}][39]' value="" />
                            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][8]' value='' onchange="disabledEnviar();viewSobre('{{ $i }}_1',this,'txt_{{ $j }}_6');sumHoras({{ $j }})"
                            onkeypress="return soloNumeros(event,this);"
                            
                            >
                            <a href="#" onclick="viewObs({{ $i }},6,1,'','')" id="obs_{{ $i }}_1" style="display:none"><i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i></a>
                          </div>
                        </center>
                      </th>
                      <?php 
                        $fec->addDay();
                        $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
                      ?>
                      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
                        <center class="div-celda">
                          <div>
                            <input type='hidden' name='ejecu[{{ $j }}][9]' value="" />
                            <input type='hidden' name='ejecu[{{ $j }}][10]' value="2" />
                            <input type='hidden' name='ejecu[{{ $j }}][11]' value="" id="txt_{{ $j }}_11"/>
                            <input type='hidden' name='ejecu[{{ $j }}][12]' value="{{ $key }}" />
                            <input type='hidden' name='ejecu[{{ $j }}][40]' value="" />
                            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][13]' value='' onchange="disabledEnviar();viewSobre('{{ $i }}_2',this,'txt_{{ $j }}_11');sumHoras({{ $j }})"
                            onkeypress="return soloNumeros(event,this);"
                                                    
                            >
                            <a href="#" onclick="viewObs({{ $i }},11,2,'','')" id="obs_{{ $i }}_2" style="display:none"><i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_2"></i></a>
                          </div>
                        </center>
                      </th>
                      <?php 
                        $fec->addDay();
                        $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
                      ?>
                      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
                        <center class="div-celda">
                          <div>
                            <input type='hidden' name='ejecu[{{ $j }}][14]' value="" />
                            <input type='hidden' name='ejecu[{{ $j }}][15]' value="2" />
                            <input type='hidden' name='ejecu[{{ $j }}][16]' value="" id="txt_{{ $j }}_16" />
                            <input type='hidden' name='ejecu[{{ $j }}][17]' value="{{ $key }}" />
                            <input type='hidden' name='ejecu[{{ $j }}][41]' value="" />
                            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][18]' value='' onchange="disabledEnviar();viewSobre('{{ $i }}_3',this,'txt_{{ $j }}_16');sumHoras({{ $j }})" 
                            onkeypress="return soloNumeros(event,this);"
                                                    
                            >
                            <a href="#" onclick="viewObs({{ $i}},16,3,'','')" id="obs_{{ $i }}_3" style="display:none"><i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_3"></i></a>
                          </div>
                        </center>
                      </th>
                       <?php 
                        $fec->addDay();
                        $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
                      ?>
                      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
                        <center class="div-celda">
                          <div>
                            <input type='hidden' name='ejecu[{{ $j }}][19]' value="" />
                            <input type='hidden' name='ejecu[{{ $j }}][20]' value="2" />
                            <input type='hidden' name='ejecu[{{ $j }}][21]' value="" id="txt_{{ $j }}_21"/>
                            <input type='hidden' name='ejecu[{{ $j }}][22]' value="{{ $key }}" />
                            <input type='hidden' name='ejecu[{{ $j }}][42]' value="" />
                            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][23]' value='' onchange="disabledEnviar();viewSobre('{{ $i }}_4',this,'txt_{{ $j }}_21');sumHoras({{ $j }})" 
                            onkeypress="return soloNumeros(event,this);"
                                                      
                            >
                            <a href="#" onclick="viewObs({{ $i }},21,4,'','')" id="obs_{{ $i }}_4" style="display:none"><i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_4"></i></a>
                          </div>
                        </center>
                      </th>
                      <?php 
                        $fec->addDay();
                        $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
                      ?>                      
                      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
                        <center class="div-celda">
                          <div>
                            <input type='hidden' name='ejecu[{{ $j }}][24]' value="" />
                            <input type='hidden' name='ejecu[{{ $j }}][25]' value="2" />
                            <input type='hidden' name='ejecu[{{ $j }}][26]' value="" id="txt_{{ $j }}_26"/>
                            <input type='hidden' name='ejecu[{{ $j }}][27]' value="{{ $key }}" />
                            <input type='hidden' name='ejecu[{{ $j }}][43]' value="" />
                            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][28]' value='' onchange="disabledEnviar();viewSobre('{{ $i }}_5',this,'txt_{{ $j }}_26');sumHoras({{ $j }})" 
                            onkeypress="return soloNumeros(event,this);"
                                                    
                            >
                            <a href="#" onclick="viewObs({{ $i }},26,5,'','')" id="obs_{{ $i }}_5" style="display:none"><i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_5"></i></a>
                          </div>
                        </center>
                      </th>
                      <?php 
                        $fec->addDay();
                        $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
                      ?>
                      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
                        <center class="div-celda">
                          <div>
                            <input type='hidden' name='ejecu[{{ $j }}][29]' value="" />
                            <input type='hidden' name='ejecu[{{ $j }}][30]' value="2" />
                            <input type='hidden' name='ejecu[{{ $j }}][31]' value="" id="txt_{{ $j }}_31"/>
                            <input type='hidden' name='ejecu[{{ $j }}][32]' value="{{ $key }}" />
                            <input type='hidden' name='ejecu[{{ $j }}][44]' value="" />
                            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][33]' value='' onchange="disabledEnviar();viewSobre('{{ $i }}_6',this,'txt_{{ $j }}_31');sumHoras({{ $j }})" 
                            onkeypress="return soloNumeros(event,this);"
                                                    
                            >
                            <a href="#" onclick="viewObs({{ $i }},31,6,'','')" id="obs_{{ $i }}_6" style="display:none"><i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_6"></i></a>
                          </div>
                        </center>
                      </th>
                       <?php 
                        $fec->addDay();
                        $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
                      ?>
                      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
                        <center class="div-celda">
                          <div>
                            <input type='hidden' name='ejecu[{{ $j }}][34]' value="" />
                            <input type='hidden' name='ejecu[{{ $j }}][35]' value="2" />
                            <input type='hidden' name='ejecu[{{ $j }}][36]' value="" id="txt_{{ $j }}_36" />
                            <input type='hidden' name='ejecu[{{ $j }}][37]' value="{{ $key }}" />
                            <input type='hidden' name='ejecu[{{ $j }}][45]' value="" />
                            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][38]' value='' onchange="disabledEnviar();viewSobre('{{ $i }}_7',this,'txt_{{ $j }}_36');sumHoras({{ $j }})" 
                            onkeypress="return soloNumeros(event,this);"
                                                    
                            >
                            <a href="#" onclick="viewObs({{ $i }},36,7,'','')" id="obs_{{ $i }}_7" style="display:none"><i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_7"></i></a>
                          </div>
                        </center>
                      </th>
                      <th style="width: 60px; border:1px solid #DCDCDCFF; font-size:11px;">
                        <div id="totf_{{ $j }}" class="div-celda" style="width: 100%;">
                          <center>
                            0
                          </center>
                        </div>
                      </th>
                      <!--<td class="clsAnchoTabla" style="min-width:150px !important">
                        <div class="progress" style="height:16px" >
                          <div class="progress-bar"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;font-size:10px;padding:1px">
                            0%
                          </div>
                        </div>                        
                        <div class="progress" style="height:16px" >
                          <div class="progress-bar progress-bar-danger"  role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%;font-size:10px;padding:1px">
                            0%
                          </div>
                        </div>  
                      </td>-->
                                   
                    </tr> 
                    @endforeach
                    @endif
                    <script>
                      nroFila = <?php echo (isset($nroFila)==1?$nroFila:0) ;?>
                    </script>