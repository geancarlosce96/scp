<table class="table table-striped" style="font-size: 12px;" id="tablaEDT">
    <thead>
      <tr class="clsCabereraTabla" >
      <th width='2%'>LE</th>
        <th width='5%'>Agregar Nivel</th>
        <th width='5%'>Nivel</th>
        <!-- <th width='10%'>Fase</th> -->
        <th width='10%'>Codigo</th>
        <th width='35%'>EDT</th>
        <th style="text-align: center;"  width='5%'>Documentos</th>
        <th style="text-align: center;"  width='5%'>Planos</th>
        <th style="text-align: center;"  width='5%'>Figuras</th>
        <th style="text-align: center;"  width='5%'>Mapas</th>
        <th  style="text-align: center;" width='10%'>Cant. Entregables</th>
        <!-- <th>Detalle Entregables</th> -->
        <th width='5%' style="text-align: center;">Acciones</th>
      </tr>
    </thead>
    <?php $i=0; ?>
    @if(isset($listaEDT))
    <tbody>
      @foreach($listaEDT as $edt)
      <?php $i++; ?>
      <tr style="background-color:
      @if($edt['nivel'] == 0) rgb(0,176,80) @endif
      @if($edt['nivel'] == 1) rgb(0,105,170) @endif
      @if($edt['nivel'] == 2) rgb(128,128,128) @endif
      @if($edt['nivel'] == 3) rgb(166,166,166) @endif 
      ; color:

      @if($edt['nivel'] == 0) white @endif
      @if($edt['nivel'] == 1) white @endif
      @if($edt['nivel'] == 2) black @endif
      @if($edt['nivel'] == 3) black @endif 
      ;     
      "
      data-edt="{{ $edt['cproyectoedt'] }}" data-descEdt="{{ $edt['descripcion'] }}"
      >
        <td>
          <a class="verLE" title="Visualizar entregables en el nivel del EDT: {{ $edt['descripcion'] }}" style="color: red;font-size: 13px;">
            <i class="fa fa-eye"></i>
          </a>
        </td>
        <td>
          <a class="agregarnivel" data-toggle="tooltip" data-placement="top" title="Añadir nivel EDT" style="color: black;font-size: 13px; transform: skewX(45deg) ">
            <i class="fa fa-level-down"></i>
          </a>
        </td>
  
        <td>{{ $edt['nivel'] }}</td>
        <!-- <td @if ($edt['ctipoedt']!=1) class="fase" @endif >

            <span id="sp_fase_{{ $edt['cproyectoedt'] }}">{{ $edt['faseproyecto'] }}</span>

            <div id="divfase_{{ $edt['cproyectoedt'] }}">

              @if ($edt['ctipoedt']!=1)

                <select class="form-control faseEdt" name="faseEdt" id="fase_{{ $edt['cproyectoedt'] }}" style="display: none">

                  @if(isset($faseProy)==1)
                  <?php //dd($tipoProyEntregable);
                      foreach ($faseProy as $id => $descripcion):
                          $fase='';

                          if ($id==$edt['cfaseproyecto']){
                              $fase=' selected="'.$edt['cfaseproyecto'].'"';

                          }
                          echo '<option'.$fase.' value="'.$id.'">'.$descripcion.'</option>';
                      endforeach;                                   
                  ?>
                  @endif      

                </select> 
              @endif
              
            </div>

        </td>
 -->
        <!--<td>{{ $edt['tipoedt'] }}</td>-->
        <td class="codigo">
             <span id="sp_cod_{{ $edt['cproyectoedt'] }}">{{ $edt['codigo'] }}</span>

              @if($edt['nivel'] != 0) 
               <input id="cod_{{ $edt['cproyectoedt'] }}" class="form-control codigoEdt" type="text" size="3" name="" style='display:none' value="{{ $edt['codigo'] }}">
              @endif


        </td>
        <td class="descripcion">
             <span id="sp_des_{{ $edt['cproyectoedt'] }}">{{ $edt['descripcion'] }}</span>

             @if($edt['nivel'] != 0) 
               <input id="des_{{ $edt['cproyectoedt'] }}" class="form-control nombreEdt" type="text" name="" style='display:none' value="{{ $edt['descripcion'] }}">
             @endif

        </td>
        <td  style="text-align: center;">{{ $edt['nroentDoc'] }}</td>
        <td  style="text-align: center;">{{ $edt['nroentPla'] }}</td>
        <td  style="text-align: center;">{{ $edt['nroentFig'] }}</td>
        <td  style="text-align: center;">{{ $edt['nroentMap'] }}</td>
        <td  style="text-align: center;">{{ $edt['nroent'] }}</td>

        <td style="text-align: center;"> 
        <a data-toggle="tooltip" data-placement="top" title="Agregar entregables" class="agregarEntregables" style="color: black;font-size: 13px"><i class="fa fa-plus"></i></a> 
        <!-- <a data-toggle="tooltip" data-placement="top" title="Editar EDT" class="editarEdt" style="color: yellow;font-size: 13px"><i class="fa fa-pencil" aria-hidden="true" ></i></a> &nbsp; -->
        &nbsp;&nbsp;&nbsp;
        <a data-toggle="tooltip" data-placement="top" title="Eliminar EDT" class="deleteEDT" style="color: red;font-size: 13px"><i class="fa fa-trash"></i></a> 
       
        </td>
      </tr> 
    
      @endforeach
    </tbody>
    @endif
</table>




