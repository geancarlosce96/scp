<!-- Modal agregar Usuario-->
    <div id="addCheLi" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">CheckList</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarCheckListMant','method' => 'POST','id' =>'frmcheck','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cchecklist',(isset($check->cchecklist)==1?$check->cchecklist:''),array('id'=>'cchecklist')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="cchecklist" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cchecklist',(isset($check->cchecklist)==1?$check->cchecklist:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cchecklist') ) !!}      
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="crol_despliegue" class="col-sm-2 control-label">Rol</label>
                  <div class="col-sm-8">
                    {!! Form::select('crol_despliegue',(isset($troldes)==1?$troldes:null),(isset($check->crol_despliegue)==1?$check->crol_despliegue:''),array('class' => 'form-control select-box','id'=>'crol_despliegue')) !!}    

                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcionactvividad" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcionactvividad" placeholder="Descripcion" name="descripcionactvividad" value="{{(isset($descripcionactvividad->descripcionactvividad)==1?$descripcionactvividad->descripcionactvividad:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="cfaseproyecto" class="col-sm-2 control-label">Fase</label>
                  <div class="col-sm-8">
                    {!! Form::select('cfaseproyecto',(isset($tfase)==1?$tfase:null),(isset($check->cfaseproyecto)==1?$check->cfaseproyecto:''),array('class' => 'form-control select-box','id'=>'cfaseproyecto')) !!}      
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddCheLi" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>



</script>  