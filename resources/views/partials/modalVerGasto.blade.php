<!-- Modal -->
    <div id="viewGasto" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Detalle de Rendición:  </h4>
          </div>
          <div class="modal-body">
            <div class="panel panel-default">
          <!-- Default panel contents -->
              <div class="panel-heading"> Realizado por: </b></div>
              <div class="panel-body">
                <table class="table table-bordered">
                  <thead>
                    <tr class="clsCabereraTabla">
                      <th>Item</th>
                      <th>Destino de Gasto</th>
                      <th>Tipo de Gasto</th>              
                      <th>Fecha</th>
                      <th>Proveedor</th>
                      <th>Descripción</th>
                      <th>Moneda</th>
                      <th>Total</th>
                      <th>Sel</th>
                    </tr>                  
                  </thead>
                  <tbody>
                  @if(isset($listGastos)==1)
                    @foreach($listGastos as $gas)
                    <tr>
                      <td>{{ $gas['item'] }}</td>
                      <td>{{ $gas['destino'] }}</td>
                      <td>{{ $gas['tipogasto'] }}</td>
                      <td>{{ $gas['fecha'] }}</td>
                      <td>{{ $gas['proveedor'] }}</td>
                      <td>{{ $gas['descripcion'] }}</td>
                      <td>{{ $gas['moneda'] }}</td>
                      <td>{{ $gas['total'] }}</td>                      
                      <td>
                      <input type='checkbox' name='cgastosejecuciondet' value='{{ $gas['cgastosejecuciondet'] }}' />
                      </td>                                            
                    </tr>
                    @endforeach
                  @endif
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" onclick="aprobarGasto()">Aprobar</button>
            <button type="button" class="btn btn-default" onclick="observarGasto()">Observar</button>
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>  
<!-- fin modal -->