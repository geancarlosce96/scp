<!-- Modal agregar Usuario-->
    <div id="addEsp" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Especialidad</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarEnfermedad','method' => 'POST','id' =>'frmespecialidad','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cespecialidad',(isset($espec->cespecialidad)==1?$espec->cespecialidad:''),array('id'=>'cespecialidad')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="cespecialidad" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                      {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cespecialidad',(isset($tenf->cespecialidad)==1?$tenf->cespecialidad:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cespecialidad') ) !!}  
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>  

          <div class="form-group">
              <label for="abrevia" class="col-sm-2 control-label">Abrevia</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="abrevia" placeholder="Abrevia" name="abrevia" value="{{(isset($abrevia->abrevia)==1?$abrevia->abrevia:'')}}">       
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>        

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">       
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddEnf" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->