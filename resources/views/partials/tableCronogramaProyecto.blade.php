<table class="table table-striped" id="cro">
  <thead>
    <tr class="clsCabereraTabla">
      <th>Item</th>
      <th>Actividad</th>
      <th>Duración</th>
      <th>F Inicio</th>
      <th>F Fin</th>
      <th>Predecesoras</th>
      <th>Sucesoras</th>
      <th>Recurso</th>
      <th>Monto</th>
      <th style="max-width:80px;">Tiene Entregable</th>
    </tr>
  </thead>
  <?php $i=0; ?>
  @if(isset($listCro))
  <tbody>
    @foreach($listCro as $cro)
    <?php $i++; ?>
      <tr>
        <td>
        {!! Form::hidden('cro['.$i.'][0]',$cro['cproyectocronogramadetalle']) !!}
        {!! Form::text('cro['.$i.'][1]',$cro['item'],array('class'=>'form-control input-sm','placeholder'=>'Item')) !!} 
        </td>
        <td>
        {!! Form::select('cro['.$i.'][2]',(isset($actividades)==1?$actividades:array()),(isset($cro['cproyectoactividades'])==1?$cro['cproyectoactividades']:''),array('class' => 'form-control')) !!}
        </td>
        <td>
        {!! Form::text('cro['.$i.'][3]',$cro['duracion'],array('class'=>'form-control input-sm','placeholder'=>'Duración')) !!}         
        </td>
        <td>
        {!! Form::text('cro['.$i.'][4]',(isset($cro['finicio'])==1?$cro['finicio']:''), array('class'=>'form-control pull-right datepicker')) !!}
        </td>
        <td>
        {!! Form::text('cro['.$i.'][5]',(isset($cro['ffin'])==1?$cro['ffin']:''), array('class'=>'form-control pull-right datepicker')) !!}
        </td>
        <td>
        {!! Form::text('cro['.$i.'][6]',$cro['idpredecesora'],array('class'=>'form-control input-sm','placeholder'=>'Predecesora')) !!} 
        </td>
        <td>
        {!! Form::text('cro['.$i.'][7]',$cro['idsucesora'],array('class'=>'form-control input-sm','placeholder'=>'Sucesora')) !!} 
        </td>
        <td>
        {!! Form::text('cro['.$i.'][8]',$cro['idrecursoproject'],array('class'=>'form-control input-sm','placeholder'=>'Recurso')) !!}
        </td>
        <td>
        {!! Form::text('cro['.$i.'][9]',$cro['monto'],array('class'=>'form-control input-sm','placeholder'=>'Monto')) !!}
        </td>
        <td>{!! Form::select('cro['.$i.'][10]',(isset($entregables)==1?$entregables:array()),(isset($cro['cproyectoentregable'])==1?$cro['cproyectoentregable']:''),array('class'=>'form-control')) !!} </td>
      </tr>
    @endforeach
  </tbody>
  @endif
</table>