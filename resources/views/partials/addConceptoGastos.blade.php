<!-- Modal agregar Usuario-->
    <div id="addConGasto" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Concepto Gastos</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarGasto','method' => 'POST','id' =>'frmcongastos','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cconcepto',(isset($concepto->cconcepto)==1?$concepto->cconcepto:''),array('id'=>'cconcepto')) !!}
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="cconcepto" class="col-sm-2 control-label">Concepto</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="cconcepto" placeholder="Concepto" name="conceptop" value="{{(isset($concepto->cconcepto)==1?$concepto->cconcepto:'')}}" readonly>
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="unidad" class="col-sm-2 control-label">Unidad</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="unidad" placeholder="Unidad" name="unidad"
                      value="{{(isset($unidad->unidad)==1?$unidad->unidad:'')}}">
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="cmoneda" class="col-sm-2 control-label">Moneda</label>
                  <div class="col-sm-8">
                        {!! Form::select('cmoneda',(isset($moneda)==1?$moneda:null),(isset($concepto->cmoneda)==1?$concepto->cmoneda:''),array('class' => 'form-control','id'=>'cmoneda')) !!}   
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="costounitario" class="col-sm-2 control-label">Costo Unitario</label>
                  <div class="col-sm-8">                             
                    <input type="text" class="form-control" id="costounitario" placeholder="Costo Unitario" name="costounitario"
                    value="{{(isset($costounitario->costounitario)==1?$costounitario->costounitario:'')}}">
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="tipogasto" class="col-sm-2 control-label">Tipo de Gasto</label>
                  <div class="col-sm-8">
                        {!! Form::select('tipogasto',(isset($tipo)==1?$tipo:null),(isset($concepto->tipogasto)==1?$concepto->tipogasto:''),array('class' => 'form-control','id'=>'tipogasto')) !!}   
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="cconcepto_parent" class="col-sm-2 control-label">Gasto Superior</label>
                  <div class="col-sm-8">                     

                        {!! Form::select('cconcepto_parent',(isset($conceptop)==1?$conceptop:null),(isset($concepto->cconcepto_parent)==1?$concepto->cconcepto_parent:''),array('class' => 'form-control','id'=>'cconcepto_parent')) !!}   

                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="categoria_gasto" class="col-sm-2 control-label">Categoria</label>
                  <div class="col-sm-8">
                    {!! Form::select('categoria_gasto',(isset($categoria)==1?$categoria:null),(isset($concepto->categoria_gasto)==1?$concepto->categoria_gasto:''),array('class' => 'form-control','id'=>'categoria_gasto')) !!}   
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddConGasto" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>



</script>

