<table class="table table-striped">
            <thead>
            <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">Tipo Gasto</td>
                <td class="clsAnchoTabla">Nombre de Proveedor</td>
                <td class="clsAnchoTabla">Tipo de Comprobante</td>
                <td class="clsAnchoTabla">Nro. Comprobante</td>
                <td class="clsAnchoTabla">Moneda</td>
                <td class="clsAnchoTabla">Acciones</td>
            </tr>
            </thead>
            <tbody>
           
            <tr class="clsAnchoTabla">
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla">
              <a href="#" class="fa fa-pencil"  data-toggle="tooltip" data-container="body" title="Editar"></a> 
              		&nbsp;&nbsp;&nbsp;&nbsp;
              <a href="#" class="fa fa-trash"  data-toggle="tooltip" data-container="body" title="Eliminar"></a>
              </td>
            </tr>
          
                                                	
            </tbody>
            <tfoot>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td>Total</td>
              <td class="clsAnchoTabla"></td>
              <td></td>
            
            </tfoot>
          </table>