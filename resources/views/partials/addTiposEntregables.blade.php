<!-- Modal agregar Usuario-->
    <div id="addTEnt" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Area</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarArea','method' => 'POST','id' =>'frmtipoent','class'=>'form-horizontal')) !!}

          {!! Form::hidden('ctiposentregable',(isset($ttipoent->ctiposentregable)==1?$ttipoent->ctiposentregable:''),array('id'=>'ctiposentregable')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="ctiposentregable" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                                           
              {!! Form::text('ctiposentregable',(isset($ttipoent->ctiposentregable)==1?$ttipoent->ctiposentregable:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'ctiposentregable') ) !!}  
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>          

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">           
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddTnfac" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->