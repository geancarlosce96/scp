<table class="table table-striped table-responsive table-hover table-bordered" id="tableEntregablesListaPry" width="100%">
    <thead>
      <tr class="clsCabereraTabla">
        <th width="5%">Item</th>
        <th>Estado</th>
        <th>Código EDT</th>
        <th>EDT</th>
        <th>Fase</th>

        <th>Entregable</th>
        <th>Tipo</th>
        <th>Código Cliente</th>
        <th>Código</th>
        <th>Disciplina</th>
        <th>Descripción</th>
        @if(isset($fechasCab)) 
          @foreach($fechasCab as $f)
            <th>{{$f->tipofec}}</th>
          @endforeach
        @endif
          
        <th>Observación</th>
        <th>Responsable Interno</th> 
        <th>Elaborador</th>        

        <th>Revisado por</th>
        <th>Aprobado por</th>
       
        <!-- <th>Acciones</th> -->
        <th>
            <input type="checkbox" name="chkTodos" id="chkTodos" onclick="setCheck(this);" />
            <!-- <button type='button' class="fa fa-calendar btn-warning" onclick="verModalfechas('general')"></button> -->
        </th>
      </tr>
    </thead>
    @if(isset($listEnt))   

    <tbody>
    <?php $i=0; ?>
      

      @foreach($listEnt as $ent)
      <?php $i++; ?>

      <?php $color=''; ?>
        @if($ent['activo']=='0')

        <?php $color='red'; ?>

        @endif 

        @if($ent['estado']=='Modificado') 

        <?php $color='yellow'; ?>

        @endif   

      <!--<tr id="fila_{{ $ent['cproyectoentregables'] }}" style="@if($ent['activo']=='0')text-decoration:line-through; color: red; @endif @if($ent['estado']=='Modificado') background-color: yellow @endif"  data-edt="{{ $ent['cproyectoedt'] }}" data-ident="{{ $ent['cproyectoentregables'] }}" data-anexos="{{ $ent['anexos'] }}"> -->
      <tr id="fila_{{ $ent['cproyectoentregables'] }}" data-edt="{{ $ent['cproyectoedt'] }}" data-ident="{{ $ent['cproyectoentregables'] }}" data-anexos="{{ $ent['anexos'] }}"> 
     
     
      
        <td style="text-decoration:line-through; color: white">
           
            @if($ent['anexos']>0 )
            <?php //dd($ent['anexos']); ?>

             <a id="btnmostrar_{{ $ent['cproyectoentregables'] }}" class="mostrarAnexos" href="#" title="Ver anexos"  style="color: green;font-size: 13px;"><i class="fa fa-plus-circle" aria-hidden="true"></i></a>
            <a id="btnocultar_{{ $ent['cproyectoentregables'] }}" class="ocultarAnexos" href="#" title="Ocultar anexos"  style="color: red;font-size: 13px;display: none"><i class="fa fa-minus-circle" aria-hidden="true" ></i></a>; 
            
            @else 
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            @endif

            <div class="input-group">
              <div class="pull-left">

              @if($ent['cantidadFechas']<=4 )
             
                <a href="#" title="¡Las fechas no están completas!" style="color: red"><i class="fa fa-exclamation" aria-hidden="true"></i>&nbsp;<i class="fa fa-calendar-times-o" aria-hidden="true"></i></a>&nbsp;
              @else
                
                <a href="#" title="¡Fechas completas!" style="color: gray">&nbsp;&nbsp;&nbsp;<i class="fa  fa-calendar-check-o" aria-hidden="true"></i></a>&nbsp;
              @endif
              
              </div>            
              
              <div class="pull-right">
                
                <label style="color: black;font-size: 12px;"><strong><!-- {{ $ent['item'] }} --></strong></label> 

              </div>
            </div>

        </td>


        <td><span class='label' style="color: black;font-size: 12px;background-color: <?php echo($color) ?>;">{{ $ent['estado'] }}</span></td>

        <td >{{ $ent['cedt'] }}</td>
        <td >{{ $ent['des_edt'] }}</td>

        <td class="faseEnt">

          <span id="sp_fase_{{ $ent['cproyectoentregables'] }}">{{ $ent['fase'] }}</span>

            <div id="divfase_{{ $ent['cproyectoentregables'] }}">

                <select class="form-control faseEntregable" name="faseEntr" id="fase_{{ $ent['cproyectoentregables'] }}" style="display: none">

                  @if(isset($faseProy)==1)
                  <?php //dd($tipoProyEntregable);
                      foreach ($faseProy as $id => $descripcion):
                          $fase='';

                          if ($id==$ent['cfaseproyecto']){
                              $fase=' selected="'.$ent['cfaseproyecto'].'"';

                          }
                          echo '<option'.$fase.' value="'.$id.'">'.$descripcion.'</option>';
                      endforeach;                                   
                  ?>
                  @endif      

                </select> 
             
              
            </div>
        </td>

        <td >{{ $ent['tipoentregable'] }}</td>
        <td >{{ $ent['tipoproyentregable'] }}</td>
        <td class="codigoEntCli">

            <span id="sp_codcli_{{ $ent['cproyectoentregables'] }}">{{ $ent['codigoCliente'] }}</span>
            <input id="codcli_{{ $ent['cproyectoentregables'] }}" class="input-sm codigoCli" type="text"  name="" style='display:none' value="{{ $ent['codigoCliente'] }}">
          
        </td>
        <td class="codigoEnt">

            <span id="sp_cod_{{ $ent['cproyectoentregables'] }}">{{ $ent['codigo'] }}</span>
            <input id="cod_{{ $ent['cproyectoentregables'] }}" class="input-sm codigo" type="text"  name="" style='display:none' value="{{ $ent['codigo'] }}">
              

              
        </td>
        <td >{{ $ent['des_dis'] }}</td>

        
        <td class="descripcion_ent">
            <span id="sp_tdesc_{{ $ent['cproyectoentregables'] }}">{{ $ent['descripcion'] }}</span>
            <input id="tdesc_{{ $ent['cproyectoentregables'] }}" class="form-control descripcionEnt" type="text" name="" style='display:none' value="{{ $ent['descripcion'] }}">  

        </td> 

        @if(isset($fechasCab)) 
          @if($ent['fechas']!='SinFechas')
            @foreach($ent['fechas'] as $f) 
            <td class="fechasTodEnt" data-ctipo="{{$f['ctipo']}}">
                <span id="sp_tfec_{{ $ent['cproyectoentregables'] }}_{{$f['ctipo']}}">{{$f['fecha']}}</span>
                <input id="tfec_{{ $ent['cproyectoentregables'] }}_{{$f['ctipo']}}" class="input-sm fechaEnt" type="text"  name="" style='display:none' value="{{$f['fecha']}}">
            
            </td>
            @endforeach 
          @else  
            @foreach($fechasCab as $f) 
            <td class="fechasTodEnt" data-ctipo="{{$f->ctipofechasentregable}}">
                <span id="sp_tfec_{{ $ent['cproyectoentregables'] }}_{{$f->ctipofechasentregable}}"></span>
                <input id="tfec_{{ $ent['cproyectoentregables'] }}_{{$f->ctipofechasentregable}}" class="input-sm fechaEnt" type="text"  name="" style='display:none' value="">
            </td>

            @endforeach 
          @endif
         
        @endif
       
        <td class="observacion">
            <span id="sp_tobs_{{ $ent['cproyectoentregables'] }}">{{ $ent['observacion'] }}</span>
            <input id="tobs_{{ $ent['cproyectoentregables'] }}" class="input-sm observacionEnt" type="text"  name="" style='display:none' value="{{ $ent['observacion'] }}">
              
        </td>

        <td class="responsable">
            <span id="sp_tresp_{{ $ent['cproyectoentregables'] }}">{{ $ent['responsable'] }}</span>

            <?php  //dd($responsable);?>

            <select class="input-sm responsableEnt" name="responsable" id="tresp_{{ $ent['cproyectoentregables'] }}" style="display: none">

              @if(isset($responsable)==1)
              <?php //dd($responsable);
                  foreach ($responsable as $id => $descripcion):
                      $resp='';

                      if ($id==$ent['cpersona_responsable']){
                          $resp=' selected="'.$ent['cpersona_responsable'].'"';

                      }
                      echo '<option'.$resp.' value="'.$id.'">'.$descripcion.'</option>';
                  endforeach;                                   
              ?>
              @endif                 

            </select> 
        </td>

        <td >{{ $ent['elaborador'] }}</td>

        <td>{{ $ent['revisor'] }}</td>
        <td>{{ $ent['aprobador'] }}</td>

        <!-- <td style="text-decoration:line-through; color: white">   

          <center>           
            <a href="#" class="verActivProy" title="Asignar actividades" ><i class="fa fa-list-ul" aria-hidden="true"></i></a> &nbsp;
            @if($ent['activo']=='1')
            <a href="#" class="anularEnt" title="Anular" style="color: red"><i class="fa fa-minus-square" aria-hidden="true"></i></a> &nbsp;
            @else
            <a href="#" class="activarEnt" title="Activar" style="color: green"><i class="fa fa-check-square" aria-hidden="true" ></i></a> &nbsp;
            @endif
             <a href="#" class="fechasEnt"  title="Agregar fechas" ><i class="fa fa-calendar" aria-hidden="true" ></i></a> 
          </center>    
        </td> -->

       <!--  <td><input type="checkbox" name="checkbox_ent[]" id="checkbox_ent" value="{{ $ent['cdisciplina'] }}_{{ $ent['centregable'] }}_{{ $ent['cproyectoentregables'] }}" /></td> -->

        <td><input onclick="prueba()" type="checkbox" name="checkbox_ent[]" id="checkbox_ent" value="{{ $ent['cproyectoentregables'] }}" /></td>

      </tr>
      @endforeach

    </tbody>
    @endif

</table>

<script type="text/javascript">


</script>

