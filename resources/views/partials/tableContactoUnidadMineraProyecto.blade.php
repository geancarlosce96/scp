 <table class="table table-striped">
            <thead>
            <tr class="clsCabereraTabla">
              <th class="clsAnchoTabla">Unidad Minera</th>
              <th class="clsAnchoTabla">Apellidos</th>
              <th class="clsAnchoTabla">Nombre</th>
              <th class="clsAnchoTabla">Correo</th>
              <th class="clsAnchoTabla">Teléfono</th>
              <th class="clsAnchoTabla">Tipo</th>
              <th class="clsAnchoTabla">Cargo</th>
              <th colspan="6"  class="clsAnchoTabla">Tipo de Información</th>
              <th class="clsAnchoTabla">Acción</th>
            </tr>
                <tr class=" clsCabereraSubItem">
                  <td class="clsAnchoTabla" colspan="7"></td>
                  <td class="clsAnchoTabla">Fi</td>
                  <td class="clsAnchoTabla">Co</td>
                  <td class="clsAnchoTabla">Cn</td>
                  <td class="clsAnchoTabla">Pl</td>
                  <td class="clsAnchoTabla">Tc</td>
                  <td class="clsAnchoTabla">Hs</td>
                  <td class="clsAnchoTabla"></td>
                </tr>            
            </thead>
            <tbody>
                 @if(isset($tunidadmineracontactos)==1)
              @foreach($tunidadmineracontactos as $tum)
        <tr>
                  <td class="clsAnchoTabla">{{ $tum->uminera }}</td>
                    <td class="clsAnchoTabla">{{ $tum->apaterno }} {{ $tum->amaterno }}</td>
                    <td class="clsAnchoTabla">{{ $tum->nombres }}</td>
                    <td class="clsAnchoTabla">{{ $tum->email }}</td>
                    <td class="clsAnchoTabla">{{ $tum->telefono }}</td>
                    <td class="clsAnchoTabla">{{ $tum->tipo }}</td>
                    <td class="clsAnchoTabla">{{ $tum->cargo }}</td>
                    <td class="clsAnchoTabla">C</td>
                    <td class="clsAnchoTabla">D</td>
                    <td class="clsAnchoTabla">C</td>
                    <td class="clsAnchoTabla"></td>
                    <td class="clsAnchoTabla"></td>
                    <td class="clsAnchoTabla"></td>
                    <td class="clsAnchoTabla">
                          <a href="#" class="fa fa-pencil"></a>
                          <a href="#" class="fa fa-trash"></a>
                          <a href="#" class="fa fa-save"></a>                    
                    </td>                                       
                </tr>                                                  
            @endforeach
            @endif                  
            </tbody>
          </table> 