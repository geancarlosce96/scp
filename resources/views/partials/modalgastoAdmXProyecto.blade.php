<table class="table table-striped">
            <thead>
            <tr class="clsCabereraTabla">
              <th>Item</th>
              <th>Proyecto</th>
              <th>Tipo Gasto</th>
              <th>Nombre de Proveedor</th>
              <th>Tipo de Comprobante</th>
              <th>Nro. Comprobante</th>
              <th>Moneda</th>
              <th>Total</th>
              <th>Acciones</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($contejec)==1)
            @foreach($contejec as $cont)
            <tr class="clsAnchoTabla">
              <td class="clsAnchoTabla">{{ $cont['item'] }}</td>
              <td class="clsAnchoTabla">{{ $cont['proyecto'] }}</td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla">
              <a href="#" class="fa fa-pencil"  data-toggle="tooltip" data-container="body" title="Editar"></a> 
                  &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="#" class="fa fa-trash"  data-toggle="tooltip" data-container="body" title="Eliminar"></a>
              </td>
            </tr>
            <tr class="clsAnchoTabla">
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla">
              <a href="#" class="fa fa-pencil"  data-toggle="tooltip" data-container="body" title="Editar"></a> 
                  &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="#" class="fa fa-trash"  data-toggle="tooltip" data-container="body" title="Eliminar"></a>
              </td>
            </tr>   
            <tr class="clsAnchoTabla">
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla">
              <a href="#" class="fa fa-pencil"  data-toggle="tooltip" data-container="body" title="Editar"></a> 
                  &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="#" class="fa fa-trash"  data-toggle="tooltip" data-container="body" title="Eliminar"></a>
              </td>
            </tr>  
            <tr class="clsAnchoTabla">
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla"></td>
              <td class="clsAnchoTabla">
              <a href="#" class="fa fa-pencil"  data-toggle="tooltip" data-container="body" title="Editar"></a> 
                  &nbsp;&nbsp;&nbsp;&nbsp;
              <a href="#" class="fa fa-trash"  data-toggle="tooltip" data-container="body" title="Eliminar"></a>
              </td>
            </tr> 
            @endforeach
            @endif                                     
            </tbody>
          </table>  