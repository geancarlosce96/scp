<!-- Modal -->
    <div id="viewHoras" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color: white">Detalle de horas: Semana {{ isset($semana_ini)==1?$semana_ini:'' }} al {{ isset($semana_fin)==1?$semana_fin:'' }} - {{ isset($anio)==1?$anio:'' }}</h4>
          </div>
          <div class="modal-body">
            <div class="panel panel-default">
          <!-- Default panel contents -->
              <div class="panel-heading" style="font-size: 12px;"> Horas ejecutadas por: <b>{{ isset($per_nombre)==1?$per_nombre:'' }} </b></div>
              <div class="panel-body">
                <table class="table table-bordered table-hover" style="font-size: 12px;border-collapse: collapse;"> 
                <thead> 
                  <tr> 
                    <th class="text-center" >#</th> 
                    <th class="text-center" ><!-- Código / Cliente / Unidad Minera / Actividad :  -->Tarea</th> 
                    <th class="text-center" >Tipo</th>
                    <th class="text-center" >Categoría</th>
                    @if(isset($dias))
                    <?php $sem=0; ?>
                    @foreach($dias as $d)
                      <?php $sem++; ?>

                          <th style="background-color:@if($sem%7==0 or $sem%6==0) #ffc1c1 @endif " class="text-center" width="3%">{{ $d[2] }}<br />{{ $d[1] }}</th>

                          <?php if ($sem==7) {
                            $sem=0;
                            
                          } ?>

                    
                    @endforeach
                    @endif 

                    <th class="text-center">Total</th>
                    <th class="text-center" width="10%">Barra avance<br>(tarea)</th>
                    <!--<th>Fecha</th> 
                    <th>Hr Pla</th>
                    <th>Obs Pla</th>
                    <th>Hr Eje</th>
                    <th>Obs Eje</th>
                    <th>Sel</th> -->
                  </tr> 
                </thead> 
                @if(isset($listHoras))
                <tbody> 
                  <?php $i=-1; ?>
                  @foreach($listHoras as $h)
                  <?php 
                    $obs=''; 
                    $obs_aprobadas="";
                    $obs_planificadas="";
                    $obs_observadas="";
                    $i++;
                  ?>
                  <tr>
                  <td style="border:1px solid #DCDCDCFF;">{{  $h['item'] }}
                  </td>
                  <td style="border:1px solid #DCDCDCFF;">
                  <!-- <center>
                    <i class="fa fa-info-circle" ></i>
                    </center> -->
                    @if($h['codigo']) {{ $h['codigo'] }} / @endif {{ $h['descripcionactividad'] }}
                  </td>
                  <td style="border:1px solid #DCDCDCFF;">
                    <center>
                        @if($h['tipo']=='1')
                          <span class="label" style="background-color:#11B15B;">FF</span>
                        @endif
                        @if($h['tipo']=='2')
                          <span class="label" style="background-color:#CD201A;">NF</span>
                        @endif
                        @if($h['tipo']=='3')
                          <span class="label" style="background-color:#0069AA;"">AND</span>
                        @endif
                        </center>
                  </td>          
                  <td style="border:1px solid #DCDCDCFF;">{{ $h['des_ctiponofacturable'] }}
                  </td>                          
                  @if(isset($dias))
                  <?php $sema=0; ?>
                  @foreach($dias as $d)
                    <?php  $key = str_replace('-','',$d[0]); ?>
                  <?php 
                    $obs=''; 
                    $obs_aprobadas="";
                    $obs_planificadas="";
                    $obs_observadas="";
                  ?>  

                  <?php $sema++; ?>

                    <td style="border:1px solid #DCDCDCFF;background-color:@if($sema%7==0 or $sema%6==0) #ffc1c1 @endif" class="text-center" title="@if($h['codigo']) {{ $h['codigo'] }} / @endif {{ $h['descripcionactividad'] }}">
                      <?php 
                            if ($sema==7) {
                              $sema=0;
                            } 
                      ?>
                    @if(isset($h['deta'][$key]['cproyectoejecucion'])==1)
                      
                    <input type="checkbox"  name="cproyectoejecucion[]" value="{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}" checked />
                    <br />
                    <input type="hidden" name="txt_obs[{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}]" value="" id="obs_{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}"/>
                    @endif                    
                    <?php

                      if(isset($h['deta'][$key]['obsplanificada'])==1){
                        $obs_planificadas=$h['deta'][$key]['obsplanificada'];

                      }
                      if(isset($h['deta'][$key]['obsaprobadas'])==1){
                        $obs_aprobadas=$h['deta'][$key]['obsaprobadas'];

                      }
                      if(isset($h['deta'][$key]['obsejecutadas'])==1){
                        $obs=$h['deta'][$key]['obsejecutadas'];

                      }     
                      if(isset($h['deta'][$key]['obsobservadas'])==1){
                        $obs_observadas=$h['deta'][$key]['obsobservadas'];

                      }        
                      $valor=(isset($h['deta'][$key]['nrohoras_eje'])==1?$h['deta'][$key]['nrohoras_eje']:'');


                      //dd($obs_planificadas,$obs_aprobadas,$obs,$obs_observadas,$h);                                  
                    ?>

                    
                    
                    <h5><span class="label" style="background-color:#0069AA; color:white">{{ (isset($h['deta'][$key]['nrohoras_eje'])==1?$h['deta'][$key]['nrohoras_eje']:'') }}</span></h5>   
                    
                    <!-- Inicio Codigo temporal  -->

                    @if($h['ccondicionoperativa']!="RGP")
                    <span class="label" style="background-color:#FFBF00; color:black">{{ (isset($h['deta'][$key]['nrohoras_pla'])==1?$h['deta'][$key]['nrohoras_pla']:'') }}</span>
                    @endif 

                    <!-- Fin Codigo temporal  -->
                    <input type="hidden" name="itxt_obs_plani[{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_planificadas }}" id="obs_plani{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_apro[{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_aprobadas }}" id="obs_apro{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_eje[{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}]" value="{{ $obs }}" id="obs_eje{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}"/>


                    @if(strlen($obs)>0 || strlen($obs_aprobadas)>0 || strlen($obs_planificadas) >0 || strlen($valor)>0 )
                      <a href="#" onclick="viewObs('{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}')" id="hobs_{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}" 
                      >


                      @if(($obs != '') or ($obs_planificadas != '') or ($obs_observadas != ''))
                      <i class= "glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}"></i>
                      @else
                      <i class= "fa fa-envelope-o" aria-hidden="true" id="iobs_{{ (isset($h['deta'][$key]['cproyectoejecucion'])==1?$h['deta'][$key]['cproyectoejecucion']:'') }}"></i>
                      @endif


                    
                    @endif
                    </td>
                  @endforeach
                  @endif         

                  <td style="border:1px solid #DCDCDCFF;" class="text-center">{{ $h['thoraact'] }}
                  </td>   
                  
                           
                    <td style="border:1px solid #DCDCDCFF;" class="clsAnchoTabla" style="min-width:150px !important" width="10%">
                        <div class="progress" style="height:16px" >
                          <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active"  role="progressbar" aria-valuenow="{{ $h['porav'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $h['porav'] }}%;font-size:10px;padding:1px; background-color: @if($h['porav']<60) green @elseif($h['porav']<80) orange @else red @endif">
                         
                            <span style="display: block; position: absolute; width: 50px; color: black;" data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en horas"><b>{{ $h['mensajehoras'] }}</b></span>
                                                 
  
                          </div>
                        </div>       
                               
                        <div class="progress " style="height:16px" >
                          <div class="progress-bar progress-bar-warning progress-bar-striped active"  role="progressbar" aria-valuenow="{{ $h['porav_costo'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $h['porav_costo'] }}%;font-size:10px;padding:1px; background-color: @if($h['porav_costo']<60) green @elseif($h['porav_costo']<80) orange @else red @endif">
                          
                            <span style="display: block; position: absolute; width: 50px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en monto"><b>{{ $h['mensajecosto'] }}</b></span>
                                             

                          </div>
                        </div>                          
                      </td>

                  </tr>

                 
                  @endforeach
                </tbody> 
                @endif

                <tfoot>
                  <td></td>                
                  <td></td>
                  <td></td>   

                 
                  <td class="text-right">Total:</td>
                    @if(isset($totales))
                    
                    @foreach($totales as $t)
                    <?php //dd($totales); ?>                      

                      <td class="text-center">{{ $t['sumdia'] }}</td>
                    @endforeach
                    @endif



                 <!-- <td class="text-right">Total:</td>

                  
                  @if(isset($dia2))
                  <td class="text-center"v>{{ $dia2 }}</td>
                  @endif
                  
                  @if(isset($dia3))
                  <td class="text-center">{{ $dia3 }}</td>
                  @endif

                  @if(isset($dia4))
                  <td class="text-center">{{ $dia4 }}</td>
                  @endif

                  @if(isset($dia5))
                  <td class="text-center">{{ $dia5 }}</td>
                  @endif

                  @if(isset($dia6))
                  <td class="text-center">{{ $dia6 }}</td>
                  @endif

                  @if(isset($dia7))
                  <td class="text-center">{{ $dia7 }}</td>
                  @endif

                  @if(isset($dia1))
                  <td class="text-center">{{ $dia1 }}</td>
                  @endif
                  -->
                  @if(isset($total))
                  <td class="text-center">{{ $total }}</td>
                  @endif

                </tfoot>    

                </table>
              </div>          
            
            </div>
          </div>
          <!--<div>
            Comentarios:
            <textarea class="form-control" name="textObs" id="textObs"></textarea>

          </div>-->

          <!--   *** INICIO HISTORIAL DE COMENTARIOS ***   -->

          <div class="col-md-12">
        <a class="btn btn-outline-primary " style="background-color: #FFBF00; color: black" role="button" data-toggle="collapse" data-target="#hcomentarios" aria-expanded="false" aria-controls="hcomentarios">
          <b>Historial de comentarios</b>
        </a>      
        <div class="collapse" id="hcomentarios">
          <div class="well">
            <table class="table table-hover" id="tcomentarios">
              <thead class="clsCabereraTabla">
                <th>
                  Código / Cliente / Unidad Minera / Actividad : tarea
                </th>
                <th> Horas
                </th>
                <th>
                  Fecha
                </th>                
                <th>
                  Cometario <br> planificación
                </th> 
                <th>
                  Cometario <br> ejecutado
                </th>      
                <th>
                  Cometario <br>  observado
                </th>                                           
              </thead>
              <tbody>
              @if(isset($historialCom)==1 )
              @foreach($historialCom as $a)              
                <tr>
                  <td>
                    {{ $a['actividad'] }}
                  </td>
                  <td> {{ $a['horas'] }}
                  </td>
                  <td>
                    {{ $a['fecha'] }}
                  </td>                
                  <td>
                    {{ $a['comentariosPla'] }}
                  </td> 
                  <td>
                    {{ $a['comentariosEje'] }}
                  </td> 
                  <td>
                    {{ $a['comentariosObs'] }}
                  </td>                                                                    
                </tr>
                @endforeach
                @endif
              </tbody>
              </table>
          </div>
        </div>
      </div>
          
          <!--   *** FIN HISTORIAL DE COMENTARIOS ***   -->

          <!--   *** INICIO HISTORIAL DE APROBACIONES ***   -->

          <div class="col-md-12">
        <a class="btn btn-outline-sucess btn-sm" style="background-color: #11B15B; color: black" role="button" data-toggle="collapse" data-target="#haprobaciones" aria-expanded="false" aria-controls="haprobaciones">
        <b>Historial de aprobaciones</b>
        </a>      
        <div class="collapse" id="haprobaciones">
          <div class="well">
            <table class="table table-hover" id="taprobaciones">
              <thead class="clsCabereraTabla">
                <th>
                  Código / Cliente / Unidad Minera / Actividad : tarea
                </th>   
                <th>Horas
                </th>           
                <th>
                  Aprobador
                </th>
                <th>
                  Fecha
                </th>                
                <th>
                  Paso anterior
                </th> 
                <th>
                  Paso actual
                </th>                                
              </thead>
              <tbody>
              @if(isset($historialApro)==1 )
              @foreach($historialApro as $a)
                <tr>
                  <td>
                    {{ $a['actividad'] }}
                  </td>
                  <td>
                    {{ $a['horas'] }}
                  </td>
                  <td>
                    {{ $a['aprobado'] }}
                  </td>
                  <td>
                    {{ $a['fecha'] }}
                  </td>                
                  <td>
                    {{ $a['ccondicionoperativa'] }}
                  </td> 
                  <td>
                    {{ $a['ccondicionoperativa_sig'] }}
                  </td>                                                  
                </tr>
              @endforeach
              @endif
              </tbody>
            </table>
          </div>
        </div>
      </div>


          <!--     *** FIN HISTORIAL DE APROBACIONES ***    -->

 

          <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="aprobarDeta();">Confirmar</button>
            <!--<button type="button" class="btn btn-default" onclick="observarDeta();">Observar</button>-->
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>  
<!-- fin modal -->