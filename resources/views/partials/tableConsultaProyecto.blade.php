<table class="table table-striped">
            <thead>
            <tr class="clsCabereraTabla">
              <th>Item</th>
              <th>Cliente</th>
              <th>Unidad<br>Minera</th>
              <th>Proyecto</th>
              <th>Actividad</th>
              <th>Hrs Plan</th>
              <th>Hrs Eje</th>
              <th>% AV</th>
              <th style="max-width:120px;">Entregable</th>
              <th style="max-width:120px;">Colaborador</th>
              <th style="max-width:120px;">Participante</th>
          @if(isset($dias))
          @foreach($dias as $d)
          <th>{{ $d }}</th>
          @endforeach
          @endif

            </tr>
            </thead>
            <tbody>
              @if(isset($planificaciones))
                @foreach($planificaciones as $pla)
                    <tr>
                        <td>{{ $pla['item'] }}
                        <input type="hidden" name='plani[][0]' value="{{ $pla['cproyectocronogramadetalle'] }}" />
                        <input type="hidden" name='plani[][0]' value="{{ $pla['cproyectoejecucion'] }}" />
                        </td>
                        <td>{{ $pla['personanombre'] }}</td>
                        <td>{{ $pla['unidadminera'] }}</td>
                        <td>{{ $pla['nombreproy'] }}</td>

                        <td>{{ $pla['des_act'] }}</td>
                        <td>{{ $pla['horasPla'] }}</td>
                        <td>{{ $pla['horaseje'] }}</td>
                        <td>{{ $pla['porav'] }}</td>
                        <td>
                            {{ $pla['entregable']}} 
                        </td>
                        <td>
                            {{ $pla['colaborador'] }}
                        </td>
                        <td>{{ $pla['participante'] }}</td>
                        @if(isset($dias))
                            @foreach($dias as $d)
                                <td>
                                {{ (isset($pla[$d])==1?$pla[$d]:'') }}
                                </td>
                            @endforeach
                        @endif
                        <td><!--<a href="#" data-toggle="tooltip" title="Duplicar Tarea">
                        <i class="fa fa-files-o" aria-hidden="true"></i></a>--></td>
                    </tr>
                @endforeach
            @endif
                                            
            </tbody>
          </table>    