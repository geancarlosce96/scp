<table class="table table-striped">
    <thead>
        <tr class="clsCabereraTabla">
          <th>Ítem</th>
          <th>Actividad</th>
          <th>Hrs Plan</th>
          <th>Hrs Eje</th>
          <th>% AV</th>
          <th style="max-width:120px;">Entregable</th>
          <th style="max-width:120px;">Colaborador</th>
          <th style="max-width:120px;">Participante</th>
          <!--<th>L<br> 01</th>
          <th>M<br> 02</th>
          <th>M<br> 03</th>
          <th>J<br> 04</th>
          <th>V<br> 05</th>
          <th>S<br> 06</th>
          <th>D<br> 07</th>-->
          @if(isset($dias))
          @foreach($dias as $d)
            <th>{{ $d }}</th>
          @endforeach
          @endif
          <th style="max-width:80px;">Acciones</th>	
        </tr>
    </thead>
        <tbody>
            @if(isset($planificaciones))
                @foreach($planificaciones as $pla)
                    <tr>
        				<td>{{ $pla['item'] }}
                        <input type="hidden" name="plani[{{ $pla['cproyectocronogramadetalle'].'_'.$pla['cestructuraproyecto'] }}][0]" value="{{ $pla['cproyectocronogramadetalle'] }}" />
                        <input type="hidden" name="plani[{{ $pla['cproyectocronogramadetalle'].'_'.$pla['cestructuraproyecto'] }}][1]" value="{{ $pla['cproyectoejecucion'] }}" />
                        <input type="hidden" name="plani[{{ $pla['cproyectocronogramadetalle'].'_'.$pla['cestructuraproyecto'] }}][4]" value="{{ $pla['cestructuraproyecto'] }}" />
                        </td>
                        <td>{{ $pla['des_act'] }}</td>
                        <td>{{ $pla['horasPla'] }}</td>
                        <td>{{ $pla['horaseje'] }}</td>
                        <td>{{ $pla['porav'] }}</td>
                        <td>
                            {{ $pla['entregable'] }}

                            <!--<div class="input-group">
                            <input id="new-event" type="text" class="form-control" placeholder="Ent1 / Ent2">
                                <div class="input-group-btn">
                                <button id="add-new-event" type="button" class="btn btn-primary btn-flat">...</button>
                                </div>
                            </div>-->                
                        </td>
                        <td>
                            {!! Form::select('plani['.$pla['cproyectocronogramadetalle'].'_'.$pla['cestructuraproyecto'].'][2]',(isset($personal)==1?$personal:array()),'',array('class' => 'form-control select-box')) !!}                
                        </td>
                        <td>{{ $pla['participante'] }}</td>
                        @if(isset($dias))
                            @foreach($dias as $d)
                                <td><input type='text' name="plani[{{ $pla['cproyectocronogramadetalle'].'_'.$pla['cestructuraproyecto'] }}][3][{{ $d }}]"  class="form-control" size="4" /></td>
                            @endforeach
                        @endif
                        <td><!--<a href="#" data-toggle="tooltip" title="Duplicar Tarea">
                        <i class="fa fa-files-o" aria-hidden="true"></i></a>--></td>
                    </tr>
                @endforeach
            @endif
        </tbody>
</table>