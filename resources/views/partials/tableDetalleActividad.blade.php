<table class="table " id="tableActividad">


                  <thead>
                    <tr class="clsCabereraTabla">
                      <th>
                        <input type="checkbox" name="selAll" onclick="setCheck(this);" />
                        <button type="button" class="btn btn-danger btn-sm" style="width:25px" id="btnDelAct"><i class="fa fa-trash-o" style="padding:1px" aria-hidden="true"></i> </button>
                      </th>                    
                      <th>Item</th>
                      <th style="min-width:200px!important;">Descripción de Actividad</th>
                      <th>Docs</th>
                      <th>Planos</th>
                      @foreach($listEstr as $est)
                      <th>{{ $est['descripcionrol'] }}<br />
                      {{ $est['rate'] }} <br />
                      <!--<a href="#" class="btn btn-default btn-xs" onclick="profesional_senior('{{ $est['cestructurapropuesta'] }}');">
                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                      </a>-->
                      </th>
                      @endforeach
                      @foreach($listEstr_dis as $est)
                        <th>{{ $est['descripcionrol'] }}<br />
                        {{ $est['rate'] }} </th>
                        @foreach($listDis as $dis)
                        <th>
                        {{ $dis['abrevia'] }}
                        <br />
                       <!-- <a href="#" class="btn btn-default btn-xs" onclick="profesional_disciplina('{{ $est['cestructurapropuesta'] }}','{{ $dis['cdisciplina'] }}');">
                          <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                        </a>-->
                      
                        </th>     
                        @endforeach
                      @endforeach                      
                      @foreach($listEstr_apo as $est)
                      <th>{{ $est['descripcionrol'] }}<br />
                      {{ $est['rate'] }} <br />
                      <!--<a href="#" class="btn btn-default btn-xs" onclick="profesional_apoyo('{{ $est['cestructurapropuesta'] }}');">
                        <span class="glyphicon glyphicon-th" aria-hidden="true"></span>
                      </a>-->
               
                      </th>     
                      @endforeach
                      <th> Totales<br />                    
                      </th>
                    </tr>
                  </thead>
                  @if(isset($listaAct))
                  <tbody>
                  <?php $i=0; ?>
                  @foreach($listaAct as $act)
                    <?php $i++; ?>
                    <tr id="{{ $act['cpropuestaactividades']  }}"
                    @if(!empty($act['cpropuestaactividades_parent']))
                      class="active"
                    @else
                      style="padding:1px"
                    @endif
                    >
                      <td>
                        @if(!empty($act['cpropuestaactividades_parent']))
                          <input type="checkbox" name="sel_cpropuestaactividades[]" value="{{ $act['cpropuestaactividades']  }}"/>
                        @endif
                      </td>                    
                      <td style="padding:1px">{{ $act['codigoactividad'] }} 
                      <input type="hidden" name="hora[{{ $i }}][0]" value="{{ $act['cpropuestaactividades'] }}" />
                      <input type="hidden" name="hora[{{ $i }}][1]" value="N" id="remove_{{ $act['cpropuestaactividades'] }}" />
                      </td>
                      <td style="padding:1px
                      @if(!empty($act['cpropuestaactividades_parent']))
                        padding-left: 20px
                      @endif                      
                      "
                      >
                      @if( $act['flag_sugerencia']=='1')
                        {{ $act['sugerencia_descripcion']}}
                      @else
                        {{ $act['descripcionactividad']}}
                      @endif
                      
                      <span class="glyphicon glyphicon-comment" aria-hidden="true" onclick="viewObs({{ $i }},4)" id="obs_{{ $i }}" ></span>
                      <span class="glyphicon glyphicon-tags" aria-hidden="true" onclick="viewSug({{ $i }},2)" id="sug_{{ $i }}"></span>
                      <!-- Sugerencia de Nombre -->
                      <input type="hidden" name="hora[{{ $i }}][2]" value="{{ $act['sugerencia_descripcion'] }}" />
                      <input type="hidden" name="hora[{{ $i }}][3]" value="{{ $act['flag_sugerencia'] }}" />
                      <!-- Comentario -->
                      <input type="hidden" name="hora[{{ $i }}][4]" value="{{ $act['comentario'] }}" />
                      </td>
                      <td style="padding:1px">{{ $act['numerodocumentos'] }}</td>
                      <td style="padding:1px">{{ $act['numeroplanos'] }}</td>   
                      @foreach($listEstr as $est)
                        <td style="padding:1px">
                          @if(!empty($act['cpropuestaactividades_parent']))
                            @if(isset($est['hora_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades'] ]))
                              <input type="text" name="hora_{{ $est['cestructurapropuesta'] }}_{{ $act['cpropuestaactividades'] }}" size="2" maxlength="3" value="{{ $est['hora_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades']] }}" />
                            @else
                              <input type="text" name="hora_{{ $est['cestructurapropuesta'] }}_{{ $act['cpropuestaactividades'] }}" size="2" maxlength="3" value="0"  />
                            @endif
                          @endif
                        </td>
                      @endforeach
                      @foreach($listEstr_dis as $est)
                        <td style="padding:1px"><center>
                        @if(!empty($act['cpropuestaactividades_parent']))
                        @if(isset($est['suma_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades']]))
                        {{ $est['suma_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades']] }}
                        @else 
                          0
                        @endif
                        @endif
                        </center>
                        </td>
                        @foreach($listDis as $dis)
                          <td class="warning" style="padding:1px">
                          @if(!empty($act['cpropuestaactividades_parent']))
                            @if(isset($est['hora_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades'].'_'.$dis['cdisciplina']  ]))
                              <input type="text" name="horad_{{ $est['cestructurapropuesta'] }}_{{ $act['cpropuestaactividades'] }}_{{ $dis['cdisciplina'] }}" size="2" maxlength="3" value="{{ $est['hora_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades'].'_'.$dis['cdisciplina'] ] }}"  />
                            @else
                              <input type="text" name="horad_{{ $est['cestructurapropuesta'] }}_{{ $act['cpropuestaactividades'] }}_{{ $dis['cdisciplina'] }}" size="2" maxlength="3" value="0" />
                            @endif                        
                          @endif
                          </td>
                        @endforeach
                      @endforeach
                      @foreach($listEstr_apo as $est)
                        <td style="padding:1px">
                          @if(!empty($act['cpropuestaactividades_parent']))
                            @if(isset($est['hora_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades'] ]))
                              <input type="text" name="horaa_{{ $est['cestructurapropuesta'] }}_{{ $act['cpropuestaactividades'] }}" size="2" maxlength="3" value="{{ $est['hora_'.$est['cestructurapropuesta'].'_'.$act['cpropuestaactividades']] }}" />
                            @else
                              <input type="text" name="horaa_{{ $est['cestructurapropuesta'] }}_{{ $act['cpropuestaactividades'] }}" size="2" maxlength="3" value="0"  />
                            @endif
                          @endif
                        </td>           
                      @endforeach
                      <td style="padding:1px;">
                          @if(!is_null($lactividades1[$i-1]->cpropuestaactividades_parent))  
                            <p>{{ $lactividades1[$i-1]->total }}</p>
                          @else
                          @endif
                        </td>             
                      </tr>
                  @endforeach
                   
                   
                  </tbody>
                  <tfoot>
                    <tr class="clsCabereraTabla">
                      <th>
                        <!-- <input type="checkbox" name="selAll" onclick="setCheck(this);" /> -->
                        <!-- <button type="button" class="btn btn-danger btn-sm" style="width:25px" id="btnDelAct"><i class="fa fa-trash-o" style="padding:1px" aria-hidden="true"></i> </button> -->
                      </th>                    
                      <th></th>
                      <th style="min-width:200px!important;">Total entregables - Total horas</th>
                      <th><center>{{ isset($cont_doc)?$cont_doc:0  }}</center></th>
                      <th><center>{{ isset($cont_pla)?$cont_pla:0  }}</center></th>
                      @foreach($listEstr as $est)
                      <td>
                        <center>
                          {{ $est['suma_total_'.$est['croldespliegue']] }}
                        </center>
                      </th>
                      @endforeach
                      @foreach($listEstr_dis as $est)
                        <th> 
                          <center>
                            {{ $est['suma_total_'.$est['croldespliegue']] }}
                          </center>
                        </th>
                        @foreach($listDis as $dis)
                        <th>
                          <!-- <input type="text" style="width: 35px;"> -->
                        </th>     
                        @endforeach
                      @endforeach                      
                      @foreach($listEstr_apo as $est)
                      <th>
                        <center>
                          {{ $est['suma_total_'.$est['croldespliegue']] }}
                        </center>
                      </th>     
                      @endforeach
                      <th> Totales<br />                    
                      </th>
                    </tr>
                  </tfoot>
                  @endif
                  <!--<tfoot>
                  <tr class="clsCabereraSubItem">
                      <td></td>
                      <td>Total Entregables / Total Horas</td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                  </tr>
                  <tr class="clsCabereraSubItem">
                      <td></td>
                      <td>Subtotal Costo horas</td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                
                      <td></td>
                      <td></td>                                                                
                      <td></td>
                  </tr>            
                  </tfoot>-->
               
              </table>