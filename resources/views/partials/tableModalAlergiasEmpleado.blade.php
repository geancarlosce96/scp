<table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <td class="clsCabereraTabla">Descripción</td>
                                        <td class="clsCabereraTabla">Observación</td>
                                        <td class="clsCabereraTabla">Acción</td>
                                      </tr>  
                                     </thead>
                                     <tbody>
                                    @if(isset($tpersonadatosmedicosalergia)==1)
                                    @foreach($tpersonadatosmedicosalergia as $tpa)
                                     	<tr>
                                            <td class="clsAnchoTablaMax">{{ $tpa->descripcion }}</td>
                                            <td class="clsAnchoTablaMax">{{ $tpa->observacion }}</td>
                                                <td class="clsAnchoDia">
                                                    <a href="#" onclick="eliminarEmpleadoAlergia('{{ $tpa->cpersonaalergia }}')" data-toggle="tooltip" data-container="body" title="Eliminar Alergia">
                                                    <i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                        </tr>
                                    @endforeach    
                                    @endif 
                                     </tbody>
</table>