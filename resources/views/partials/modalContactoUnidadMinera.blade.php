


      <table class="table table-striped" style="font-size: 12px;">
      	<thead>
        <tr>
        	<td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td colspan="6" class="clsCabereraTabla">Tipo de información</td>
            <td></td>
          </tr>
        <tr>
        	<!--<td class="clsCabereraTabla">Unidad Minera</td>-->
            <td class="clsCabereraTabla">Apellidos</td>
            <td class="clsCabereraTabla">Nombre</td>
            <td class="clsCabereraTabla">Correo</td>
            <td class="clsCabereraTabla">Teléfono 1</td>
            <td class="clsCabereraTabla">Cel 1</td>
            <td class="clsCabereraTabla">Cel 2</td>
            <td class="clsCabereraTabla">Anexo</td>
            <td class="clsCabereraTabla">Dirección</td>
            <td class="clsCabereraTabla">Tipo</td>
            <td class="clsCabereraTabla">Cargo</td>
            <td class="clsAnchoDia" style="background-color:#0069aa; color:#FFF;">Tipo Información Contacto</td>
            <td class="clsCabereraTabla">Acción</td>
          </tr>            
          
         </thead>
         <tbody>
         @if(isset($tunidadmineracontactos)==1)
         @foreach($tunidadmineracontactos as $tmc)
         <tr>
         	<!--<td class="clsAnchoTabla">{{ $tmc->uminera }}</td>-->
            <td class="clsAnchoTabla">{{ $tmc->apaterno }} {{ $tmc->amaterno }}</td>
            <td class="clsAnchoTabla">{{ $tmc->nombres }}</td>
            <td class="clsAnchoTabla">{{ $tmc->email }}</td>
            <td class="clsAnchoTabla">{{ $tmc->telefono }} </td>
             <td class="clsAnchoTabla">{{ $tmc->cel1 }}</td>
              <td class="clsAnchoTabla">{{ $tmc->cel2 }}</td>
               <td class="clsAnchoTabla">{{ $tmc->anexo }}</td>
               <td class="clsAnchoTabla">{{ $tmc->direccion }}</td>
            <td class="clsAnchoTabla">{{ $tmc->des_tipo }}</td>
            <td class="clsAnchoTabla">{{ $tmc->des_cargo }}</td>
            <td class="clsAnchoDia"></td>
            <td class="clsAnchoTablaMax">
                <a href="#" onclick="editarUmineraCon('{{ $tmc->cunidadmineracontacto }}')" data-toggle="tooltip" data-container="body" title="Editar">
                <i class="fa fa-pencil" aria-hidden="true"></i></a>   &nbsp;&nbsp;
                <a href="#" onclick="eliminarUmineraCon('{{ $tmc->cunidadmineracontacto }}')" data-toggle="tooltip" title="Eliminar">
                <i class="fa fa-trash" aria-hidden="true"></i></a>   &nbsp;&nbsp;  
                <a href="#" onclick="agregarContHR('{{ $tmc->cunidadmineracontacto }}')" data-toggle="tooltip" title="Asignar">
                <i class="fa fa-check-circle" aria-hidden="true"></i></a>   &nbsp;&nbsp;  
            </td>            
            
         </tr>
         @endforeach
         @endif                     
         </tbody>
         </table>
