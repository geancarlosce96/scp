<!-- Modal -->
         <!-- Default panel contents -->

              <div class="panel-heading"> 
              <h5>Ejecutadas por:  
              <b>{{ isset($per_nombre)==1?$per_nombre:'' }} </b>&nbsp; &nbsp;|&nbsp; &nbsp;
              Hoja de tiempo de la semana: <b>{{ isset($semana)==1?$semana:'' }} - {{ isset($anio)==1?$anio:'' }}</b>&nbsp; &nbsp;|&nbsp; &nbsp;
              <!--          <br>
                        <b>Hoja de tiempo de la semana: </b>
                        {{ isset($semana)==1?$semana:'' }} - {{ isset($anio)==1?$anio:'' }}
                        <br>-->
                         
                       <!-- <b>Fecha del : </b>{{ isset($fecha1)==1?$fecha1:'' }}
                        <b>al : </b>{{ isset($fecha2)==1?$fecha2:'' }}-->

                        <?php  
                            $fecdesde=explode('-',substr($fecha1,0,10));
                            $fechasta=explode('-',substr($fecha2,0,10));
                           // dd($fecdesde);

                         ?>

                        del:&nbsp;<b>{{ $fecdesde[2]}}-{{ $fecdesde[1]}}-{{ $fecdesde[0]}}</b>&nbsp; &nbsp; &nbsp; &nbsp; 
                        al :&nbsp;<b>{{ $fechasta[2]}}-{{ $fechasta[1]}}-{{ $fechasta[0]}}</b>&nbsp; &nbsp;|&nbsp; &nbsp; 
                        
                
               Cargabilidad planificada: <b>{{ (isset($nroHoras)==1?$nroHoras:0).' %' }}</b> &nbsp; &nbsp;|&nbsp; &nbsp; 
              Cargabilidad ejecutada: <b>{{ (isset($nroHorasCar)==1?$nroHorasCar:0).' %' }}</b>
              </h5>               
              </div>
                <table class="table table-bordered table-hover"> 
                <thead> 
                  <tr> 
                    <th>#</th> 
                    <th class="col-sm-4 text-center">Código / Cliente / Unidad Minera / Actividad : tarea</th> 
                    <th class="text-center">Tipo</th>
                    <th class="col-sm-1 text-center">Categoría</th>
                    @if(isset($dias))
                    @foreach($dias as $d)
                      <th class="col-sm-1 text-center">{{ $d[2] }}<br />{{ $d[1] }}</th>
                    @endforeach
                    @endif 

                    <th>Total</th>  
                    
                    <th class="col-sm-2 text-center">Barra avance<br>(tarea)</th>
                  </tr> 
                </thead> 
                <?php

                      $dia1=0;
                      $dia2=0;
                      $dia3=0;
                      $dia4=0;
                      $dia5=0;
                      $dia6=0;
                      $dia7=0;
                      $total=0;
                ?>
                @if(isset($alinea))
                <tbody> 
                  @foreach($alinea as $h)

                  <?php 
                   $obs=''; 
                   $obs_aprobadas="";
                   $obs_planificadas="";
                   $obs_observadas="";
                  
                  ?>



                  <?php 
                      $sumaacti=0;
                      $sumadia=0;

                  ?>
                  <tr>
                  <td>{{  $h['item'] }}
                  </td>
                  <td>
                  @if($h['tipo']!=3)
                    {{ $h['codigopry'] }} / {{ $h['personanombre'] }} / {{ $h['unidadminera'] }} /{{ $h['nombreproy'] }}/ {{ $h['codigoactividad'] }} :
                  @endif
                  {{ $h['descripcionactividad'] }}

                  </td>
                  <td>{{ ($h['tipo']=='1'?'FF':($h['tipo']=='2'?'NF':'AND')) }}
                  </td>          
                  <td>
                    @if(isset($h['des_ctiponofacturable']))

                    {{ $h['des_ctiponofacturable'] }}

                    @endif

                  </td>                          
                  @if(isset($dias))
                  @foreach($dias as $d)
                    <?php /* Prepara cuadro de texto para el input dia 1 */
                        $key = str_replace('-','',$d[0]);
                       
                        $estado='';
                       
                        if (isset($h[$key])==1){
                          $estado= (isset($h[$key]['estado'])==1?$h[$key]['estado']:'');                          
                        }                                       
                      ?> 

                  <?php 
                    $obs=''; 
                    $obs_aprobadas="";
                    $obs_planificadas="";
                    $obs_observadas="";
                    
                  ?>      

                             
                    
                  <?php

                      if(isset($h[$key]['obsplanificada'])==1){
                        $obs_planificadas=$h[$key]['obsplanificada'];

                      }
                      if(isset($h[$key]['obsaprobadas'])==1){
                        $obs_aprobadas=$h[$key]['obsaprobadas'];

                      }
                      if(isset($h[$key]['obsejecutadas'])==1){
                        $obs=$h[$key]['obsejecutadas'];

                      }     
                      if(isset($h[$key]['obsobservadas'])==1){
                        $obs_observadas=$h[$key]['obsobservadas'];

                      }        
                     
                            
                    ?>
  
                    <!--   Inicio Acumulado de Horas -->

                    <td  class="text-center">                    
                  
                    @if($estado=='1')
                    <h5><span class="label" style="background-color:#FFBF00; color:black" title="Horas Planificadas">{{ (isset($h[$key]['horasplanificadas'])==1?$h[$key]['horasplanificadas']:'') }}</span></h5>


                    <input type="hidden" name="itxt_obs_plani[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_planificadas }}" id="obs_plani{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_apro[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_aprobadas }}" id="obs_apro{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_eje[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs }}" id="obs_eje{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>

                    @if(strlen($obs)>0 || strlen($obs_aprobadas)>0 || strlen($obs_planificadas) >0 )
                      <a href="#" onclick="viewObs('{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}')" id="hobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}" 
                      >


                      @if(($obs != '') or ($obs_planificadas != '') or ($obs_observadas != ''))
                      <i class= "glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"></i>
                      
                      @endif 

                    @endif



                    <?php 
                      $sumaacti=$sumaacti+$h[$key]['horasplanificadas'];

                     
                      if($h[$key]['diasemana']==1){
                          $dia1=$dia1+$h[$key]['horasplanificadas'];
                      }
                      if($h[$key]['diasemana']==2){
                          $dia2=$dia2+$h[$key]['horasplanificadas'];
                      }
                      if($h[$key]['diasemana']==3){
                          $dia3=$dia3+$h[$key]['horasplanificadas'];
                      }
                      if($h[$key]['diasemana']==4){
                          $dia4=$dia4+$h[$key]['horasplanificadas'];
                      }
                      if($h[$key]['diasemana']==5){
                          $dia5=$dia5+$h[$key]['horasplanificadas'];
                      }
                      if($h[$key]['diasemana']==6){
                          $dia6=$dia6+$h[$key]['horasplanificadas'];
                      }
                      if($h[$key]['diasemana']==7){
                          $dia7=$dia7+$h[$key]['horasplanificadas'];
                      }
                    ?>

                    @endif                    

                    @if($estado=='2')
                    <h5><span class="label" style="background-color:#CD201A; color:white" title="Hora Ejecutada">{{ (isset($h[$key]['horasejecutadas'])==1?$h[$key]['horasejecutadas']:'') }}</span></h5>

                    <input type="hidden" name="itxt_obs_plani[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_planificadas }}" id="obs_plani{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_apro[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_aprobadas }}" id="obs_apro{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_eje[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs }}" id="obs_eje{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>


                    @if(strlen($obs)>0 || strlen($obs_aprobadas)>0 || strlen($obs_planificadas) >0 )
                      <a href="#" onclick="viewObs('{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}')" id="hobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}" 
                      >


                      @if(($obs != '') or ($obs_planificadas != '') or ($obs_observadas != ''))
                      <i class= "glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"></i>
                     
                      @endif 

                    @endif
        

                     <?php 
                      $sumaacti=$sumaacti+$h[$key]['horasejecutadas'];

                      if($h[$key]['diasemana']==1){
                          $dia1=$dia1+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==2){
                          $dia2=$dia2+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==3){
                          $dia3=$dia3+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==4){
                          $dia4=$dia4+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==5){
                          $dia5=$dia5+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==6){
                          $dia6=$dia6+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==7){
                          $dia7=$dia7+$h[$key]['horasejecutadas'];
                      }

                    ?>
                    @endif 

                    @if($estado=='3')
                    <h5><span class="label" style="background-color:#0069AA; color:white" title="Horas Enviadas">{{ (isset($h[$key]['horasejecutadas'])==1?$h[$key]['horasejecutadas']:'') }}</span></h5>

                    <input type="hidden" name="itxt_obs_plani[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_planificadas }}" id="obs_plani{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_apro[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_aprobadas }}" id="obs_apro{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_eje[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs }}" id="obs_eje{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>


                    @if(strlen($obs)>0 || strlen($obs_aprobadas)>0 || strlen($obs_planificadas) >0 )
                      <a href="#" onclick="viewObs('{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}')" id="hobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}" 
                      >


                      @if(($obs != '') or ($obs_planificadas != '') or ($obs_observadas != ''))
                      <i class= "glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"></i>
                      
                      @endif 

                    @endif

                    <?php 
                      $sumaacti=$sumaacti+$h[$key]['horasejecutadas'];

                      if($h[$key]['diasemana']==1){
                          $dia1=$dia1+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==2){
                          $dia2=$dia2+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==3){
                          $dia3=$dia3+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==4){
                          $dia4=$dia4+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==5){
                          $dia5=$dia5+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==6){
                          $dia6=$dia6+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==7){
                          $dia7=$dia7+$h[$key]['horasejecutadas'];
                      }
                    ?>
                    @endif 

                    @if($estado=='4')
                    <h5><span class="label" style="background-color:#0069AA; color:white" title="Aprobado por JI">{{ (isset($h[$key]['horasejecutadas'])==1?$h[$key]['horasejecutadas']:'') }}</span></h5>

                    <input type="hidden" name="itxt_obs_plani[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_planificadas }}" id="obs_plani{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_apro[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_aprobadas }}" id="obs_apro{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_eje[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs }}" id="obs_eje{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>



                    @if(strlen($obs)>0 || strlen($obs_aprobadas)>0 || strlen($obs_planificadas) >0 )
                      <a href="#" onclick="viewObs('{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}')" id="hobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}" 
                      >


                      @if(($obs != '') or ($obs_planificadas != '') or ($obs_observadas != ''))
                      <i class= "glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"></i>
                     
                      @endif 

                    @endif

                    <?php 
                      $sumaacti=$sumaacti+$h[$key]['horasejecutadas'];

                      if($h[$key]['diasemana']==1){
                          $dia1=$dia1+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==2){
                          $dia2=$dia2+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==3){
                          $dia3=$dia3+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==4){
                          $dia4=$dia4+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==5){
                          $dia5=$dia5+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==6){
                          $dia6=$dia6+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==7){
                          $dia7=$dia7+$h[$key]['horasejecutadas'];
                      }
                    ?>
                    @endif 

                    @if($estado=='5')
                    <h5><span class="label" style="background-color:#11B15B; color:white" title="Horas Aprobadas">{{ (isset($h[$key]['horasejecutadas'])==1?$h[$key]['horasejecutadas']:'') }}</span></h5>

                    <input type="hidden" name="itxt_obs_plani[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_planificadas }}" id="obs_plani{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_apro[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs_aprobadas }}" id="obs_apro{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>
                    <input type="hidden" name="itxt_obs_eje[{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}]" value="{{ $obs }}" id="obs_eje{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"/>


                    @if(strlen($obs)>0 || strlen($obs_aprobadas)>0 || strlen($obs_planificadas) >0 )
                      <a href="#" onclick="viewObs('{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}')" id="hobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}" 
                      >


                      @if(($obs != '') or ($obs_planificadas != '') or ($obs_observadas != ''))
                      <i class= "glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ (isset($h[$key]['cproyectoejecucion'])==1?$h[$key]['cproyectoejecucion']:'') }}"></i>
                    
                      @endif 

                    @endif


                    <?php 
                      $sumaacti=$sumaacti+$h[$key]['horasejecutadas'];

                      if($h[$key]['diasemana']==1){
                          $dia1=$dia1+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==2){
                          $dia2=$dia2+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==3){
                          $dia3=$dia3+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==4){
                          $dia4=$dia4+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==5){
                          $dia5=$dia5+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==6){
                          $dia6=$dia6+$h[$key]['horasejecutadas'];
                      }
                      if($h[$key]['diasemana']==7){
                          $dia7=$dia7+$h[$key]['horasejecutadas'];
                      }
                    ?>
                    @endif 

                    </td>

                    <!--   Fin Acumulado de Horas -->

                  @endforeach
                  @endif   

                  <?php 
                      $total=$dia1+$dia2+$dia3+$dia4+$dia5+$dia6+$dia7;
                  ?>


                  <td class="text-center"> {{ $sumaacti }} 
                  </td>    

                  <td class="clsAnchoTabla" style="min-width:150px !important">
                        <div class="progress" style="height:16px" >
                          <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active"  role="progressbar" aria-valuenow="{{ $h['porav'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $h['porav'] }}%;font-size:10px;padding:1px; background-color: @if($h['porav']<60) green @elseif($h['porav']<80) orange @else red @endif">
                         
                            <span style="display: block; position: absolute; width: 130px; color: black;" data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en horas"">{{ $h['mensajehoras'] }}</span>
                                                 
  
                          </div>
                        </div>       
                               
                        <div class="progress " style="height:16px" >
                          <div class="progress-bar progress-bar-warning progress-bar-striped active"  role="progressbar" aria-valuenow="{{ $h['porav_costo'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $h['porav_costo'] }}%;font-size:10px;padding:1px; background-color: @if($h['porav_costo']<60) green @elseif($h['porav_costo']<80) orange @else red @endif">
                          
                            <span style="display: block; position: absolute; width: 130px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en monto"">{{ $h['mensajecosto'] }}</span>
                                             

                          </div>
                        </div>                          
                      </td>

                  </tr>
                  @endforeach
                </tbody> 
                @endif

                <tfoot>
                  <td></td>                
                  <td></td>
                  <td></td>                 
                  <td class="text-right">Total:</td>
                  @if(isset($dia2))
                  <td class="text-center">{{ $dia2 }}</td>
                  @endif

                  @if(isset($dia3))
                  <td class="text-center">{{ $dia3 }}</td>
                  @endif

                  @if(isset($dia4))
                  <td class="text-center">{{ $dia4 }}</td>
                  @endif

                  @if(isset($dia5))
                  <td class="text-center">{{ $dia5 }}</td>
                  @endif

                  @if(isset($dia6))
                  <td class="text-center">{{ $dia6 }}</td>
                  @endif

                  @if(isset($dia7))
                  <td class="text-center">{{ $dia7 }}</td>
                  @endif

                  @if(isset($dia1))
                  <td class="text-center">{{ $dia1 }}</td>
                  @endif

                  @if(isset($total))
                  <td class="text-center">{{ $total }}</td>
                  @endif
                                    
                </tfoot>    

                </table>
              </div>

<!-- ********************Observaciones2 ******************************* -->
      <div class="modal fade" id="modalObsDeta" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabelDeta" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
              <h4 class="modal-title" id="ObsModalLabelDeta" style="color: white">Comentarios</h4>
            </div>
            <div class="modal-body">
              <form>
                <label  class="control-label">
                Observación de planificación:
                </label>
                <div id="obs_planificada">

                </div>
                <label  class="control-label">
                Observación de ejecución:
                </label>
                <div id="obs_ejecucion">

                <div id="obs_aprobacion">

                </div>

                </div>   
                <label  class="control-label">
                Comentarios de aprobación u observación anteriores:
                </label>

                <div class="modal-footer">
           
                <button type="button" class="btn btn-primary" onclick="cerrar_modal()">Cerrar</button>
          </div>
                
              </form>
            </div>
            
            <!--<div class="modal-footer">
              <button type="button" class="btn btn-primary warning" onclick="saveObsDeta()">Aceptar</button>
              <button type="button" class="btn btn-primary" data-dismiss="modal">Cancelar</button>
            </div>-->
          </div>
        </div>
      </div>


    <!-- *************************************************** -->    

<script>

function viewObs(eje){

            $('#obs_planificada').html("");
            $('#obs_aprobacion').html("");
            $('#obs_ejecucion').html("");
            id_ejecu=eje;
            if($("#obs_plani"+eje).val()!=''){
                $('#obs_planificada').html($("#obs_plani"+eje).val());  
            }
            if($("#obs_apro"+eje).val()!=''){
                $('#obs_aprobacion').html($("#obs_apro"+eje).val());  
            }          
            if($("#obs_eje"+eje).val()!=''){
                $('#obs_ejecucion').html($("#obs_eje"+eje).val());  
            }              
            $('#modalObsDeta').modal('show');


           // $('#modalObsDeta').CSS("overflow:scroll");
}


function cerrar_modal(){

   $('#modalObsDeta').modal('hide');

   document.getElementById("verHT").style.overflow = "scroll"; 

}




</script>





<!-- fin modal -->