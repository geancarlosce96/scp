<!-- Modal agregar Catalogo Grupo Tablas-->
    <div id="addGrTab" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Catalogo Grupo Tablas</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarCatalogoGrupoTablas','method' => 'POST','id' =>'frmcatgrtab','class'=>'form-horizontal')) !!}

          {!! Form::hidden('catalogoid',(isset($catalogrtab->catalogoid)==1?$catalogrtab->catalogoid:''),array('id'=>'catalogoid')) !!}   
                 
           {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        
              <div class="form-group">
                <label for="catalogoid" class="col-sm-2 control-label">ID</label>
              <div class="col-sm-8">
              {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('catalogoid',(isset($catalogrtab->catalogoid)==1?$catalogrtab->catalogoid:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'catalogoid') ) !!}      
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
            <label for="codtab" class="col-sm-2 control-label">Codigo Tabla</label>
              <div class="col-sm-8">
                {!! Form::text('codtab',(isset($codtabla->codtab)==1?$codtabla->codtab:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'codtab') ) !!}
                 </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>           

             <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
              <div class="col-sm-8">
              
               {!! Form::text('descripcion',(isset($descripcion->descripcion)==1?$descripcion->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'descripcion') ) !!} 
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>  
            

          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Catalogo Grupo Tablas-->

 