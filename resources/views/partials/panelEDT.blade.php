    <!-- Modal Informacion -->
    <div id="btnEdt" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">EDT</h4>
          </div>
          <div class="modal-body">

          <div class="form-group">
            <label for="cproyectoedt" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">                       
                {!! Form::text('cproyectoedt',(isset($edt->cproyectoedt)==1?$edt->cproyectoedt:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyectoedt') ) !!}    
                </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="cproyecto" class="col-sm-2 control-label">Item</label>
                  <div class="col-sm-8">
                      {!! Form::text('cproyecto',(isset($edt->cproyecto)==1?$edt->cproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyecto') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      {!! Form::text('codigo',(isset($edt->codigo)==1?$edt->codigo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'codigo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      {!! Form::text('descripcion',(isset($edt->descripcion)==1?$edt->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'descripcion') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;          

          <div class="form-group">
              <label for="cproyectoedt_parent" class="col-sm-2 control-label">EDT parent</label>
                  <div class="col-sm-8">
                      {!! Form::text('cproyectoedt_parent',(isset($edt->cproyectoedt_parent)==1?$edt->cproyectoedt_parent:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyectoedt_parent') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="tipo" class="col-sm-2 control-label">Tipo</label>
                  <div class="col-sm-8">
                      {!! Form::text('tipo',(isset($edt->tipo)==1?$edt->tipo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'tipo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;   
       

                   


             {!! Form::close() !!}

           </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>




</script>