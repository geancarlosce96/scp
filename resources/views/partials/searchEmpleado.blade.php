<!-- Modal Buscar Empleado-->
    <div id="searchEmpleado" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Empleado</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tEmpleado" class="table">
                <thead>
                  <th>ID</th>
                  <th>A. paterno</th>
                  <th>A. materno</th>
                  <th>Nombres</th>
                  <th>Área</th>

                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Empleado-->