<!-- ******************** FECHAS ******************************* -->
      <div class="modal fade" id="modalfechas" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog modal-sm" role="document" style="width: 50%">
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: white;" aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" style="color: white">Registrar fechas del entregable <strong><span id="nomb_ent"></span></strong></h4>
            </div>
            <div class="modal-body">
                {!! Form::open(array('url' => 'grabarFechasEntregablePry','method' => 'POST','id' =>'frmfechasentproy')) !!}
                {!! Form::hidden('centregableproy_fecha','',array('id'=>'centregableproy_fecha')) !!}

                <div class="row clsPadding">
                  <div class="col-lg-12 col-xs-12">
                    
                    <!--<input class="form-control" width="100%" type="text" name="nomb_ent" id="nomb_ent" disabled="true">-->
                    <input type="hidden" class="form-control" id="fechasAnteriores">

                  </div> 

                </div> 
               
                <div class="row clsPadding2">
                    <div class="col-lg-2 col-xs-12"></div> 
                    <div class="col-lg-8 col-xs-12 list-group active">

                        <div class="col-lg-5 col-xs-12">
                            <span><label class="col-form-label">Tipo de Fecha :</label></span>
                            {!! Form::select('tipofecha',(isset($fechas)==1?$fechas:array()),'',array('class' => 'form-control','id'=>'tipofecha')) !!}
                                    <input type="hidden" id="desc_fecha">
                        </div>   
                        <div class="col-lg-5 col-xs-12 ">
                            <span><label class="col-form-label">Fecha :</label></span>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>

                                    {!! Form::text('fecha',(isset($documento->fecha)?$documento->fecha:''), array('class'=>'form-control pull-right ','id'=>'fecha')) !!}

                            </div>
                        </div> 
                        <br> 
                            <div class="col-lg-1 col-xs-12 ">
                                <button type="button" class="btn btn-primary" id="btnSaveEnt" onclick="validarFechaIngresada()"><b>Agregar</b></button>
                                <!-- <button type="button" class="btn btn-primary" id="btnSaveEnt" onclick="listafechasAgregadasTodos()"><b>Prueba</b></button> -->
                                
                            </div>
                        
                    </div>                    
                </div> 

        

                <div class="row clsPadding">
                    <div class="col-lg-12 col-xs-12 table-responsive" id="divFechaEntregable">



                     @include('partials.tableFechasEntregablesProyecto',array('listFecEnt' => (isset($listFecEnt)==1?$listFecEnt:array())))
                    </div>
                    <br><br><br>

                </div> 

                {!! Form::close() !!}          
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnSaveFechas">Grabar</button>
            </div> 
            
          </div>
        </div>
      </div>


    <!-- *************************************************** -->

    <script type="text/javascript">

    $('#tipofecha').on('change',function(){

        $('#desc_fecha').val($('#tipofecha option:selected').text());

    })
        
    </script>