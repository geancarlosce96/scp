<!-- Modal -->
    <div id="myModal" class="modal fade" role="dialog">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Aprobaciones</h4>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => 'sgteFlujoPropuesta','method' => 'POST','id' =>'frmaprobacion')) !!}
            {!! Form::hidden('cpropuesta_aprob',(isset($propuesta)==1?$propuesta->cpropuesta:'')) !!}
            {!! Form::hidden('_token_aprob',csrf_token(),array('id'=>'_token_aprob')) !!}
            Siguiente Estado: 
            {!! Form::select('flujo',isset($aflujo)==1?$aflujo:array(),null,array('id'=>'flujo') ) !!}
            <button class="btn btn-primary"> Procesar</button>
            <br>
            <div class="panel panel-default">
              <!-- Default panel contents -->
              <div class="panel-heading">Secuencia de Aprobaciones</div>
              <div class="panel-body">
                <table class="table"> 
                  <thead> 
                    <tr> <th>#</th> <th>Estado Anterior</th> <th>Aprobador</th> <th>Siguiente Estado </th> </tr> 
                  </thead> 
                  @if(isset($hflujo))
                  <tbody> 
                    @foreach($hflujo as $hf)
                      <tr>
                        <td>{{ $hf['item'] }}
                        </td>
                        <td>{{ $hf['desante'] }}
                        </td>
                        <td>{{ $hf['aprobador'] }}
                        </td>   
                        <td>{{ $hf['dessgte'] }}
                        </td>                        
                      </tr>
                    @endforeach
                  </tbody> 
                  @endif
                </table>
              </div>

            
            </div>
            {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
        </div>

      </div>
    </div>  
<!-- fin modal -->