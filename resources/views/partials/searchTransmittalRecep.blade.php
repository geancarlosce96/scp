<!-- Modal Buscar Transmittal-->
    <div id="searchRecepTR" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Transmittal de Recepción</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tTransmittalRecep" class="table">
                <thead>
                  <th>Código</th>
                  <th>Nombre de  Proyecto</th>
                  <th>Transmittal</th>
                  <th>Secuencia</th>
                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Transmittal-->