<table class="table table-striped">
              <thead>
                <tr class="clsCabereraTabla">
                    <td class="clsAnchoTabla">Código</td>
                      <td class="clsAnchoTabla">Cliente</td>
                      <td class="clsAnchoTabla">Nombre</td>
                      <td class="clsAnchoTabla">GP</td>
                      <td class="clsAnchoTabla">CP</td>
                      <td class="clsAnchoTabla">CD</td>
                      <td class="clsAnchoTabla">Carpeta</td>
                      <td class="clsAnchoTabla">Estado</td> 
                      <td class="clsAnchoTabla">Acciones</td>                
                  </tr>
              </thead>
              
              <tbody>
                    @if(isset($listaProy))
                    @foreach($listaProy as $proy)
                    <tr>
                        <td class="clsAnchoTabla">{{ $proy['codigo'] }} </td>
                        <td class="clsAnchoTabla">{{ $proy['cliente'] }} </td>            
                        <td class="clsAnchoTabla">{{ $proy['nombre'] }} </td> 
                        <td class="clsAnchoTabla">{{ $proy['gerente'] }} </td>
                        <td class="clsAnchoTabla">{{ $proy['controlproy'] }} </td>
                        <td class="clsAnchoTabla"  style="min-width:160px !important;">
                        {{ $proy['controldoc'] }}           
                        </td>
                        <td class="clsAnchoTabla" style="min-width:160px !important;">
                                         {{ $proy['carpeta'] }} 
                                         <a href="#" data-toggle="tooltip" data-container="body" title="cambiar carpeta">
                                         <i class="glyphicon glyphicon-folder-open" aria-hidden="true"></i>     
                                         </a>
                        </td>
                        <td class="clsAnchoTabla">{{ $proy['estado'] }}</td>            
                        <td class="clsAnchoTabla"  style="min-width:190px !important;">
                            <button type="button" class="btn btn-primary btn-block"
                            style="width:50px !important;float:left; margin-top:0px !important; margin-right:5px;" disabled="true"><b>HR</b></button>
                            
                            <button type="button" class="btn btn-primary btn-block"
                            style="width:50px !important;float:left; margin-top:0px !important; margin-right:5px;" disabled="true"><b>TR</b></button>
                            
                            <button type="button" class="btn btn-primary btn-block"
                            style="width:50px !important;float:left; margin-top:0px !important; margin-right:5px;" disabled="true"><b>RPT</b></button>
                        </td>                       
                    </tr> 
                    @endforeach
                    @endif
              </tbody>
            </table> 