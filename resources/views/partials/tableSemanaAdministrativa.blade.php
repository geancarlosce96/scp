<input type="hidden" name="view_cactividad" value="{{ (isset($cactividad)==1?$cactividad:'') }}" />
<input type="hidden" name="semana_ini" value="{{ (isset($semana_ini)==1?$semana_ini:'') }}" id="semana_ini"/>
<input type="hidden" name="semana_fin" value="{{ (isset($semana_fin)==1?$semana_fin:'') }}" id="semana_fin"/>
<input type="hidden" name="dia_act" value="{{ (isset($dia_act)==1?$dia_act:'') }}" id="dia_act"/>
<table class="table">
    <thead>
	    <tr class="clsCabereraTabla">
	      <th class="col-sm-1">Colaborador</th>
            @if(isset($dias))
            @foreach($dias as $d)
            <?php $ky = str_replace("-","",$d[0]);  ?>
            <th><input type='checkbox' name="fec_{{ $ky }}" onclick="checkAll('{{ $ky }}',this)" 
                    @if($dia_act>=$ky)
                    disabled
                    @endif

                 />
            <select id="s_{{ $d[2] }}_{{ $d[1] }}" name="s_{{ $d[2] }}_{{ $d[1] }}" style="color:blue" onchange="asignarHoras(this,'{{ $d[0] }}');">
            <?php $w=1; ?>
                <option value="" style="color:blue"> -- </option>
                <?php for($w=1;$w<=12;$w++){ ?>
                    <option value="{{ $w }}" style="color:blue">{{ $w }}</option>
                <?php } ?>
            </select>
            <br />{{ $d[2] }} <br />{{ $d[1] }}</th>
            @endforeach
            @endif
	    </tr>
    </thead>
    <tbody>
    <?php $i=-1; ?>
        @if(isset($colaborador)==1)
        @foreach($colaborador as $c)
            <?php $i++; ?>
            <tr>
            <td>
            <input type="hidden" name="plaadm[{{ $i }}][0]" value="{{ $c['cproyectoejecucioncab'] }}" />
            <input type="hidden" name="plaadm[{{ $i }}][1]" value="{{ $c['cpersona'] }}" />
            {{ $c['nombre'] }}</td>
            @if(isset($dias))
            @foreach($dias as $d)   
                <?php 
                    $key = str_replace("-","",$d[0]);  
                    $cproyectoejecucion = (isset($c[$key]['cproyectoejecucion'])==1?$c[$key]['cproyectoejecucion']:'');
                    $valor = (isset($c[$key]['horas'])==1?$c[$key]['horas']:'');
                    $valor_eje = (isset($c[$key]['horas_eje'])==1?$c[$key]['horas_eje']:'');
                    $estado=(isset($c[$key]['estado'])==1?$c[$key]['estado']:'');

                ?>                                 
            <td
                @if(strlen($estado)>0  && $estado!='1' )
                    style="border: 1px solid red"
                    
                @endif                
            >
                

                <input type="checkbox" name="plaadm[{{ $i }}][{{ $key }}][0]" value="1" onclick="habilitar({{ $i }},{{ $c['cpersona'] }},{{ $key }},this)"
                @if(strlen($valor)>0)
                    
                @endif  
                id="chk_{{ $c['cpersona']}}_{{ $key}}"   

                @if($dia_act>=$key)
                    disabled
                    @endif           
                />
                <input type="hidden" name="plaadm[{{ $i }}][{{ $key }}][1]" value="{{ $cproyectoejecucion }}" />
                <input type='text' name="plaadm[{{ $i }}][{{ $key }}][2]" value="{{ $valor }}" size="4" maxlength="4"
                @if(strlen($estado)>0  && $estado!='1' )
                    readonly="true"

                    data-toggle="tooltip" data-placement="top" title="{{ $valor_eje }}"
                 @else
                                                    @if($dia_act>=$key)
                                                        readonly
                                                    @endif
                                                @endif
                
                onkeypress="return soloNumeros(event,this);"                
                id="val_{{ $c['cpersona']}}_{{ $key}}"
                 />
            </td>
            @endforeach
            @endif
            </tr>
        @endforeach
        @endif
    </tbody>
</table>
