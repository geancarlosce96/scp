<!-- Modal agregar Usuario-->
    <div id="addDis" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Disciplina</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarDisciplina','method' => 'POST','id' =>'frmdisciplina','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cdisciplina',(isset($disciplina->cdisciplina)==1?$disciplina->cdisciplina:''),array('id'=>'cdisciplina')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="cdisciplina" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cdisciplina',(isset($disciplina->cdisciplina)==1?$disciplina->cdisciplina:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cdisciplina') ) !!}     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>
          
          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">      
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="abrevia" class="col-sm-2 control-label">Abreviatura</label>
                  <div class="col-sm-8">                             
                    <input type="text" class="form-control" id="abrevia" placeholder="Abreviatura" name="abrevia" value="{{(isset($abrevia->abrevia)==1?$abrevia->abrevia:'')}}">        
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>


    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddDis" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>


</script>  