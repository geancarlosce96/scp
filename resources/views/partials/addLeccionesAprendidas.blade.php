    <div id="addLecApren" class="modal fade" role="dialog" style="font-size: 12px;">
      <div class="modal-dialog modal-md">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:#0069AA; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Lecciones Aprendidas</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarLeccionAprend','method' => 'POST','id' =>'frmlecApre','class'=>'form-horizontal')) !!}
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!}
          {!! Form::hidden('cleccionaprendida',(isset($ediLA->cleccionaprendida)==1?$ediLA->cleccionaprendida:''),array('id'=>'cleccionaprendida')) !!}
          {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!} 


            <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
              <div class="col-sm-8">

              {!! Form::textarea('descripcion',(isset($ediLA->descripcion)==1?$ediLA->descripcion:''),array('class'=>'form-control input-sm','placeholder'=>'Descripcion de Proyecto','rows'=> '3','id'=>'descripcion')) !!}  

              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
              <label for="fecha" class="col-sm-2 control-label">Fecha</label>
              <div class="col-sm-8">
                    <div class="input-group date" id="fecha">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                       {!! Form::text('fecha',(isset($ediLA->fecha)==1?$ediLA->fecha:''), array('class'=>'form-control input-sm datepicker','placeholder' => '','id'=>'fechala') ) !!}
                    </div>                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 

            <div class="form-group">
              <label for="cfaseproyecto" class="col-sm-2 control-label">Etapa de Proyecto</label>
              <div class="col-sm-8">
                {!! Form::select('cfaseproyecto',(isset($tfaseproyecto)==1?$tfaseproyecto:array() ),(isset($ediLA->cfaseproyecto)==1?$ediLA->cfaseproyecto:''),array('class' => 'form-control select-box','id'=>'cfaseproyecto')) !!}                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>   

            <div class="form-group">
              <label for="suceso" class="col-sm-2 control-label">¿Qué sucedió?</label>
              <div class="col-sm-8">
                  {!! Form::textarea('suceso',(isset($ediLA->suceso)==1?$ediLA->suceso:''), array('class' => 'form-control', 'rows'=> '3','placeholder' => '¿Qué sucedió?','id'=>'suceso')) !!}                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>
            <div class="form-group">
              <label for="hizo" class="col-sm-2 control-label">¿Qué se hizo?</label>
              <div class="col-sm-8">
                  {!! Form::textarea('hizo',(isset($ediLA->hizo)==1?$ediLA->hizo:''), array('class' => 'form-control', 'rows'=> '3','placeholder' => '¿Qué se hizo?','id'=>'hizo')) !!}                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>
            <div class="form-group">
              <label for="debiohacer" class="col-sm-2 control-label">¿Qué se debió hacer?</label>
              <div class="col-sm-8">
                  {!! Form::textarea('debiohacer',(isset($ediLA->debiohacer)==1?$ediLA->debiohacer:''), array('class' => 'form-control', 'rows'=> '3','placeholder' => '¿Qué se debió hacer?','id'=>'debiohacer')) !!}                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 
           
          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="idAddLeAp" onclick="add()">Registrar</button>
            <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Lecciones Aprendidas-->

<script>
  $('.select-box').chosen(
  {
    allow_single_deselect: true,
    width: "100%",
});

    $('#fechala').datepicker({
    format: "dd-mm-yy",
    language: "es",
    autoclose: true,
    firstDay: 1,
    calendarWeeks:true,
    /*startDate: '+<?php echo (7-date('w'))+1; ?>d',*/
    endDate: "<?php echo date('d/m/Y'); ?>",
    todayHighlight: true,
});

</script>