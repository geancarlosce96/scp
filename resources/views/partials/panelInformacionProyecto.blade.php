    <!-- Modal Informacion -->
    <div id="btnInf" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Información del Proyecto</h4>
          </div>
          <div class="modal-body">

          <div class="form-group">
            <label for="cproyecto" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">                       
                {!! Form::text('cproyecto',(isset($proyecto->cproyecto)==1?$proyecto->cproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyecto') ) !!}    
                </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      {!! Form::text('codigo',(isset($proyecto->codigo)==1?$proyecto->codigo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'codigo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-8">
                      {!! Form::text('nombre',(isset($proyecto->nombre)==1?$proyecto->nombre:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'nombre') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="cpersonacliente" class="col-sm-2 control-label">Cliente</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersonacliente',(isset($proyecto->cpersonacliente)==1?$proyecto->cpersonacliente:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersonacliente') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;          

          <div class="form-group">
              <label for="cunidadminera" class="col-sm-2 control-label">Unidad Minera</label>
                  <div class="col-sm-8">
                      {!! Form::text('cunidadminera',(isset($proyecto->cunidadminera)==1?$proyecto->cunidadminera:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cunidadminera') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="cpersona_gerente" class="col-sm-2 control-label">Gerente</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersona_gerente',(isset($proyecto->cpersona_gerente)==1?$proyecto->cpersona_gerente:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersona_gerente') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      {!! Form::text('descripcion',(isset($proyecto->descripcion)==1?$proyecto->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'descripcion') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;          

          <div class="form-group">
              <label for="cpersona_coordinador" class="col-sm-2 control-label">Coordinador</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersona_coordinador',(isset($proyecto->cpersona_coordinador)==1?$proyecto->cpersona_coordinador:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersona_coordinador') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="cmoneda" class="col-sm-2 control-label">Moneda</label>
                  <div class="col-sm-8">
                      {!! Form::text('cmoneda',(isset($proyecto->cmoneda)==1?$proyecto->cmoneda:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cmoneda') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cmedioentrega" class="col-sm-2 control-label">Medio Entrega</label>
                  <div class="col-sm-8">
                      {!! Form::text('cmedioentrega',(isset($proyecto->cmedioentrega)==1?$proyecto->cmedioentrega:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cmedioentrega') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          <div class="form-group">
              <label for="cservicio" class="col-sm-2 control-label">Servicio</label>
                  <div class="col-sm-8">
                      {!! Form::text('cservicio',(isset($proyecto->cservicio)==1?$proyecto->cservicio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cservicio') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="csede" class="col-sm-2 control-label">Sede</label>
                  <div class="col-sm-8">
                      {!! Form::text('csede',(isset($proyecto->csede)==1?$proyecto->csede:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'csede') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="fproyecto" class="col-sm-2 control-label">Proyecto</label>
                  <div class="col-sm-8">
                      {!! Form::text('fproyecto',(isset($proyecto->fproyecto)==1?$proyecto->fproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fproyecto') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cestadoproyecto" class="col-sm-2 control-label">Estado</label>
                  <div class="col-sm-8">
                      {!! Form::text('cestadoproyecto',(isset($proyecto->cestadoproyecto)==1?$proyecto->cestadoproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cestadoproyecto') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="finicio" class="col-sm-2 control-label">Fecha Inicio</label>
                  <div class="col-sm-8">
                      {!! Form::text('finicio',(isset($proyecto->finicio)==1?$proyecto->finicio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'finicio') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          <div class="form-group">
              <label for="finicioreal" class="col-sm-2 control-label">Fecha Inicio Real</label>
                  <div class="col-sm-8">
                      {!! Form::text('finicioreal',(isset($proyecto->finicioreal)==1?$proyecto->finicioreal:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'finicioreal') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="fcierrereal" class="col-sm-2 control-label">Fecha Cierre Real</label>
                  <div class="col-sm-8">
                      {!! Form::text('fcierrereal',(isset($proyecto->fcierrereal)==1?$proyecto->fcierrereal:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fcierrereal') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          <div class="form-group">
              <label for="fcierre" class="col-sm-2 control-label">Fecha Cierre</label>
                  <div class="col-sm-8">
                      {!! Form::text('fcierre',(isset($proyecto->fcierre)==1?$proyecto->fcierre:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fcierre') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="fcierreadministrativo" class="col-sm-2 control-label">Fecha Cierre Administrativo</label>
                  <div class="col-sm-8">
                      {!! Form::text('fcierreadministrativo',(isset($proyecto->fcierreadministrativo)==1?$proyecto->fcierreadministrativo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fcierreadministrativo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;   

                   


             {!! Form::close() !!}

           </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>





</script>