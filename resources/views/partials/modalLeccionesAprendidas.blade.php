<table class="table table-striped" id="tableLA">
        <thead>
            <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">Descripción</td>
                <!-- <td class="clsAnchoTabla">Palabra Clave</td> -->
                <td class="clsAnchoTabla">Fecha</td>
                <!-- <td class="clsAnchoTabla">Área</td> -->
                <td class="clsAnchoTabla">Etapa de Proyecto</td>
                <!-- <td class="clsAnchoTabla">Área de conocimiento</td> -->
                <td class="clsAnchoTabla">¿Qué Sucedió?</td>
                <td class="clsAnchoTabla">¿Qué se hizo?</td>
                <td class="clsAnchoTabla">¿Qué se debió hacer?</td>
                <td class="clsAnchoTabla">Elaborado</td>
                <!-- <td class="clsAnchoTabla">Rol Elaborado</td> -->
                <!-- <td class="clsAnchoTabla">Aprobado</td> -->
                <td class="clsAnchoTabla">Acciones</td>
            
            </tr>
        </thead>

        <tbody>

        @if(isset($lecaprend)==1)
            @foreach($lecaprend as $la)
        <tr>

            <td class="clsAnchoTabla">{{ $la['item'] }}</td>
            <td class="clsAnchoTabla">{{ $la['descripcion'] }}</td>
            <!-- <td class="clsAnchoTabla">{{ $la['tag'] }}</td> -->
            <td class="clsAnchoTabla">{{ $la['fecha'] }}</td>
            <!-- <td class="clsAnchoTabla">{{ $la['area'] }}</td> -->
            <td class="clsAnchoTabla">{{ $la['fase'] }}</td>
            
            <td class="clsAnchoTabla">{{ $la['suceso'] }}</td>
            <td class="clsAnchoTabla">{{ $la['hizo'] }}</td>
            <td class="clsAnchoTabla">{{ $la['debiohacer'] }}</td>
            <td class="clsAnchoTabla">{{ $la['elaborado'] }}</td>
            <td class="clsAnchoTabla">
                <a href="#" onclick="fdelLeAp('{{ $la['cleccionaprendida'] }}')" data-toggle="tooltip" data-placement="top" title="" data-original-title="Eliminar Registro">
                <i class="fa fa-trash" aria-hidden="true"></i></a>   &nbsp;&nbsp; 
                <a href="#" onclick="goEditarLA({{ $la['cleccionaprendida'] }})" data-toggle="tooltip" data-placement="top" title="" data-original-title="Editar Registro">
                <i class="fa fa-edit" aria-hidden="true"></i></a>   &nbsp;&nbsp; 
            </td>          
        </tr>
            @endforeach
        @endif
            
        </tbody>
</table>    