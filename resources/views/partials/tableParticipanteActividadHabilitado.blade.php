<table class="table " id="tableInputActividad">
{!! Form::hidden('cestructuraproyecto_h',( isset($cestructuraproyecto)==1?$cestructuraproyecto:'' ),array('id'=> 'cestructuraproyecto_h') ) !!}
{!! Form::hidden('cdisciplina',( isset($cdisciplina)==1?$cdisciplina:'' ),array('id'=> 'cdisciplina') ) !!}
    <thead>
      <tr class="clsCabereraTabla">
        <th>Ítem</th>
        <th style="min-width:200px!important;">Descripción de Actividad</th>

        <th>Horas
        </th>

      </tr>
    </thead>
    @if(isset($listaAct))
    <tbody>
    <?php $i=0; ?>
    @foreach($listaAct as $act)
      <?php $i++; ?>
      <tr
      @if(!empty($act['cactividad_parent']))
        class="active"
      @endif
      >
        <td>{{ $act['item'] }} </td>
        <td
        @if(!empty($act['cactividad_parent']))
          style="padding-left: 20px"
        @endif                      
        >{{ $act['descripcionactividad'] }}</td>
        <td>
        @if(!empty($act['cactividad_parent']))
        <input type="text" class="form-control" size="3" style="10px"  name="horas[{{ $act['cproyectoactividades']  }}]" value="
        @if(isset($cestructuraproyecto) && isset($cdisciplina) )
          @if(isset($act['hora_'.$act['cproyectoactividades']."_".$cestructuraproyecto."_".$cdisciplina]))
          {{ $act['hora_'.$act['cproyectoactividades']."_".$cestructuraproyecto."_".$cdisciplina] }}
          @else
          0
          @endif
        @else
        0
        @endif
        " />
        @endif
        </td>

        
                                                 
      </tr>
    @endforeach
    </tbody>
    @endif

</table>