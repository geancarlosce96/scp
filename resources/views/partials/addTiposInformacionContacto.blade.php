<!-- Modal agregar Alergias-->
    <div id="addTInfCont" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Tipos Informacion Contacto</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarTiposInformacionContacto','method' => 'POST','id' =>'frmtipinfocont','class'=>'form-horizontal')) !!}

          {!! Form::hidden('ctipoinformacioncontacto',(isset($tipoinformacioncontacto->ctipoinformacioncontacto)==1?$tipoinformacioncontacto->ctipoinformacioncontacto:''),array('id'=>'ctipoinformacioncontacto')) !!}

                   
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        
          <div class="form-group">
            <label for="ctipoinformacioncontacto" class="col-sm-2 control-label">ID</label>
              <div class="col-sm-8">
                          
              {!! Form::text('ctipoinformacioncontacto',(isset($tipoinformacioncontacto->ctipoinformacioncontacto)==1?$tipoinformacioncontacto->ctipoinformacioncontacto:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'ctipoinformacioncontacto') ) !!}      

              </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div> 
      

            <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
              <div class="col-sm-8">
              <input type="text" class="form-control" id="descripcion" placeholder="Descripción" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">       
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Alergias-->
