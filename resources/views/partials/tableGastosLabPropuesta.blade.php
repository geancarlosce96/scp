<table class="table" id="tableListGastosLab">
                  <thead>
                  <tr class="clsCabereraTabla">
                    <th>Item</th>
                    <th style="min-width:200px!important;">Descripción</th>
                    <th style="min-width:50px!important;">Precio Unitario</th>
                    <th>Cimentación</th>
                    <th>Botadero</th>
                    <th>Cantera</th>
                    <th>Total (US$)</th>  
                    <th>Seleccionar</th>                                         
                  </tr>                       
                  </thead>
                  @if(isset($gastosLab))
                  <tbody>
                  @foreach($gastosLab as $gasl)
                    <tr
                      @if(empty($gasl['cconcepto_parent']))
                        class="active"
                      @endif                    
                    >
                        <td>{{ $gasl['item'] }} 
                      {!! Form::hidden('cpropuestagastoslab',$gasl['cpropuestagastoslab'],array('id' => 'cpropuestagastoslab' )) !!}</td>
                        <td
                       @if(!empty($gasl['cconcepto_parent']))
                        style="padding-left: 40px"
                        @endif                        
                        >{{ $gasl['descripcion'] }}</td>
                        <td>{{ $gasl['preciou'] }}</td>                
                        <td>
                        @if(!empty($gasl['cconcepto_parent']))
                        <input type="text" name="gaslcimen[{{ $gasl['cpropuestagastoslab'] }}]" class="form-control" size="4" value="{{ $gasl['cantcimentacion'] }}" /> 
                        @else
                          {{ $gasl['cantcimentacion'] }}
                        @endif
                        </td>
                        <td>
                        @if(!empty($gasl['cconcepto_parent']))
                        <input type="text" name="gaslbota[{{ $gasl['cpropuestagastoslab'] }}]" class="form-control" size="4" value="{{ $gasl['cantbotadero'] }}" /> 
                        @else
                          {{ $gasl['cantbotadero'] }}
                        @endif
                        </td>
                        <td>
                        @if(!empty($gasl['cconcepto_parent']))
                        <input type="text" name="gaslcant[{{ $gasl['cpropuestagastoslab'] }}]" class="form-control" size="4" value="{{ $gasl['cantcanteras'] }}" /> 
                        @else
                          {{ $gasl['cantcanteras'] }}
                        @endif
                        </td>
                        <td id="{{ $gasl['cpropuestagastoslab'] }}_subtot">{{ $gasl['subtotal'] }}</td>                
                        <td>
                        
                        <input name='delLab[]' type="checkbox" value="{{ $gasl['cpropuestagastoslab'] }}">
                     
                        </td>                                                                              
                    </tr>
                  @endforeach                                                            
                  </tbody>
                  @endif
                  <tfoot>
                  <tr class="clsSubTotal">
                     <td></td>
                     <td></td>
                     <td>Total:</td>
                     <td><input class="form-control input-sm" type="text" id="txtTotalLabCim" placeholder="" disabled value="{{ isset($totGastosLabCim)==1?$totGastosLabCim:0 }}"></td>
                     <td><input class="form-control input-sm" type="text" id="txtTotalLabBot" placeholder="" disabled value="{{ isset($totGastosLabBot)==1?$totGastosLabBot:0 }}"></td>
                     <td><input class="form-control input-sm" type="text" id="txtTotalLabCan" placeholder="" disabled value="{{ isset($totGastosLabCan)==1?$totGastosLabCan:0 }}"></td>
                     <td><input class="form-control input-sm" type="text" id="txtTotalLab" placeholder="" disabled value="{{ isset($totGastosLab)==1?$totGastosLab:0 }}"></td>
                  </tr>
                  
                  </tfoot>
                </table>  