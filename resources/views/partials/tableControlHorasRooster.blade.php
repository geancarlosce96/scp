<table class="table table-striped">
                <thead>
                <tr class="clsCabereraTabla">
                  <th>Item</th>
                  <th>Apellidos Nombre</th>
                  <th>Cargo</th>
                  @if(isset($dias))
                  @foreach($dias as $d)
                    <th>{{ $d }}</th>
                  @endforeach
                  @endif


                </tr>                       
                </thead>
                <tbody>
                @if(isset($crono))
                <?php $i=-1; ?>
                @foreach($crono as $cro)
                <?php $i++; ?>
                <tr>
                    <td class="clsAnchoDia"><?php echo $i+1; ?>
                    <input type="hidden" name="rooster[{{ $i }}][0]" value="{{ $cro['cpersonaempleado'] }}" />
                    <input type="hidden" name="rooster[{{ $i }}][1]" value="{{ $cro['cproyectocronogramacons'] }}" />
                    <input type="hidden" name="rooster[{{ $i }}][2]" value="{{ $cro['cproyectocronograma'] }}" />                    
                    </td>
                    <td class="clsAnchoTablaMax">{{ $cro['empleado'] }}</td>
                    <td class="clsAnchoTabla"></td>                
                      @if(isset($dias))
                            @foreach($dias as $d)
                                <?php $key=str_replace('-','',$d) ?>
                                <td 
                                @if(isset($cro['deta'][$key]['eje'])==1)
                                  @if($cro['deta'][$key]['eje'] == '1')
                                    style = "background-color:yellow"
                                  @endif
                                @endif
                                >
                                <input type='hidden' name='rooster[{{ $i }}][3][{{ $key }}][0]' value="{{ (isset($cro['deta'][$key]['cconstrucciondetafecha'])==1?$cro['deta'][$key]['cconstrucciondetafecha']:'') }}"  />
                                <input type='hidden' name='rooster[{{ $i }}][3][{{ $key }}][1]' value="{{ (isset($cro['deta'][$key]['fecha'])==1?$cro['deta'][$key]['fecha']:'') }}"  />
                                <input type='text' name='rooster[{{ $i }}][3][{{ $key }}][3]' value="{{ (isset($cro['deta'][$key]['nrohoras'])==1?$cro['deta'][$key]['nrohoras']:'') }}"  size="2" maxlength="2" />
                                <select name="rooster[{{ $i }}][3][{{ $key }}][2]">
                                <option value="">&nbsp;</option>
                                  @foreach($leyenda as $ley)
                                    <option value="{{ $ley['cod'] }}"
                                    @if(isset($cro['deta'][$key]['codactividad']))
                                        @if($cro['deta'][$key]['codactividad']==$ley['cod'] )
                                            selected
                                        @endif
                                    @endif
                                    >{{ $ley['cod'] }}</option>
                                  @endforeach
                                </select>
                                <input type='hidden' name='rooster[{{ $i }}][3][{{ $key }}][4]' value="{{ (isset($cro['deta'][$key]['observacion'])==1?$cro['deta'][$key]['observacion']:'') }}"  />
                                <input type="checkbox" name="rooster[{{ $i }}][3][{{ $key }}][5]" value="1" 
                                @if(isset($cro['deta'][$key]['eje'])==1)
                                  @if($cro['deta'][$key]['eje'] == '1')
                                    checked
                                  @endif
                                @endif
                                 />
                                <input type='hidden' name='rooster[{{ $i }}][3][{{ $key }}][6]' value="{{ (isset($cro['deta'][$key]['cproyectoejecucionconstruccion'])==1?$cro['deta'][$key]['cproyectoejecucionconstruccion']:'') }}"  />
                                
                                </td>
                            @endforeach
                        @endif
                                                                               
                </tr>
                @endforeach
                @endif                                                                     	
                </tbody>
              </table>