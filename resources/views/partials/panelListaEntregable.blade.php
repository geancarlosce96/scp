    <!-- Modal Informacion -->
    <div id="btnEnt" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Lista Entregable</h4>
          </div>
          <div class="modal-body">

          <div class="form-group">
            <label for="cproyecto" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">                       
                {!! Form::text('cproyecto',(isset($listaentregable->cproyecto)==1?$listaentregable->cproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyecto') ) !!}    
                </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      {!! Form::text('codigo',(isset($listaentregable->codigo)==1?$listaentregable->codigo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'codigo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="nombre" class="col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-8">
                      {!! Form::text('nombre',(isset($listaentregable->nombre)==1?$listaentregable->nombre:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'nombre') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="cpersonacliente" class="col-sm-2 control-label">Cliente</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersonacliente',(isset($listaentregable->cpersonacliente)==1?$listaentregable->cpersonacliente:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersonacliente') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;          

          <div class="form-group">
              <label for="cunidadminera" class="col-sm-2 control-label">Unidad Minera</label>
                  <div class="col-sm-8">
                      {!! Form::text('cunidadminera',(isset($listaentregable->cunidadminera)==1?$listaentregable->cunidadminera:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cunidadminera') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="cpersona_gerente" class="col-sm-2 control-label">Gerente</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersona_gerente',(isset($listaentregable->cpersona_gerente)==1?$listaentregable->cpersona_gerente:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersona_gerente') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      {!! Form::text('descripcion',(isset($listaentregable->descripcion)==1?$listaentregable->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'descripcion') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;          

          <div class="form-group">
              <label for="cpersona_coordinador" class="col-sm-2 control-label">Coordinador</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersona_coordinador',(isset($listaentregable->cpersona_coordinador)==1?$listaentregable->cpersona_coordinador:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersona_coordinador') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="cmoneda" class="col-sm-2 control-label">Moneda</label>
                  <div class="col-sm-8">
                      {!! Form::text('cmoneda',(isset($listaentregable->cmoneda)==1?$listaentregable->cmoneda:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cmoneda') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cmedioentrega" class="col-sm-2 control-label">Medio Entrega</label>
                  <div class="col-sm-8">
                      {!! Form::text('cmedioentrega',(isset($listaentregable->cmedioentrega)==1?$listaentregable->cmedioentrega:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cmedioentrega') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          <div class="form-group">
              <label for="cservicio" class="col-sm-2 control-label">Servicio</label>
                  <div class="col-sm-8">
                      {!! Form::text('cservicio',(isset($listaentregable->cservicio)==1?$listaentregable->cservicio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cservicio') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="csede" class="col-sm-2 control-label">Sede</label>
                  <div class="col-sm-8">
                      {!! Form::text('csede',(isset($listaentregable->csede)==1?$listaentregable->csede:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'csede') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="fproyecto" class="col-sm-2 control-label">Proyecto</label>
                  <div class="col-sm-8">
                      {!! Form::text('fproyecto',(isset($listaentregable->fproyecto)==1?$listaentregable->fproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fproyecto') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cestadoproyecto" class="col-sm-2 control-label">Estado</label>
                  <div class="col-sm-8">
                      {!! Form::text('cestadoproyecto',(isset($listaentregable->cestadoproyecto)==1?$listaentregable->cestadoproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cestadoproyecto') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="finicio" class="col-sm-2 control-label">Fecha Inicio</label>
                  <div class="col-sm-8">
                      {!! Form::text('finicio',(isset($listaentregable->finicio)==1?$listaentregable->finicio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'finicio') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          <div class="form-group">
              <label for="finicioreal" class="col-sm-2 control-label">Fecha Inicio Real</label>
                  <div class="col-sm-8">
                      {!! Form::text('finicioreal',(isset($listaentregable->finicioreal)==1?$listaentregable->finicioreal:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'finicioreal') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="fcierrereal" class="col-sm-2 control-label">Fecha Cierre Real</label>
                  <div class="col-sm-8">
                      {!! Form::text('fcierrereal',(isset($listaentregable->fcierrereal)==1?$listaentregable->fcierrereal:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fcierrereal') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          <div class="form-group">
              <label for="fcierre" class="col-sm-2 control-label">Fecha Cierre</label>
                  <div class="col-sm-8">
                      {!! Form::text('fcierre',(isset($listaentregable->fcierre)==1?$listaentregable->fcierre:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fcierre') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="fcierreadministrativo" class="col-sm-2 control-label">Fecha Cie Adm</label>
                  <div class="col-sm-8">
                      {!! Form::text('fcierreadministrativo',(isset($listaentregable->fcierreadministrativo)==1?$listaentregable->fcierreadministrativo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fcierreadministrativo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cproyectoentregables" class="col-sm-2 control-label">ID2</label>
                  <div class="col-sm-8">
                      {!! Form::text('cproyectoentregables',(isset($listaentregable->cproyectoentregables)==1?$listaentregable->cproyectoentregables:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyectoentregables') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="centregable" class="col-sm-2 control-label">Entregable</label>
                  <div class="col-sm-8">
                      {!! Form::text('centregable',(isset($listaentregable->centregable)==1?$listaentregable->centregable:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'centregable') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cproyectoedt" class="col-sm-2 control-label">EDT</label>
                  <div class="col-sm-8">
                      {!! Form::text('cproyectoedt',(isset($listaentregable->cproyectoedt)==1?$listaentregable->cproyectoedt:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyectoedt') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="observacion" class="col-sm-2 control-label">Observacion</label>
                  <div class="col-sm-8">
                      {!! Form::text('observacion',(isset($listaentregable->observacion)==1?$listaentregable->observacion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'observacion') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cproyectoactividades" class="col-sm-2 control-label">Actividad</label>
                  <div class="col-sm-8">
                      {!! Form::text('cproyectoactividades',(isset($listaentregable->cproyectoactividades)==1?$listaentregable->cproyectoactividades:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cproyectoactividades') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cpersona_responsable" class="col-sm-2 control-label">Responsable</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersona_responsable',(isset($listaentregable->cpersona_responsable)==1?$listaentregable->cpersona_responsable:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersona_responsable') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="crol_responsable" class="col-sm-2 control-label">Rol</label>
                  <div class="col-sm-8">
                      {!! Form::text('crol_responsable',(isset($listaentregable->crol_responsable)==1?$listaentregable->crol_responsable:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'crol_responsable') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      {!! Form::text('codigo',(isset($listaentregable->codigo)==1?$listaentregable->codigo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'codigo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cestadoentregable" class="col-sm-2 control-label">Estado</label>
                  <div class="col-sm-8">
                      {!! Form::text('cestadoentregable',(isset($listaentregable->cestadoentregable)==1?$listaentregable->cestadoentregable:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cestadoentregable') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          

                   


             {!! Form::close() !!}

           </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>





</script>