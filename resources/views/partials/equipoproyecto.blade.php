

<table class="table table-striped table-hover table-bordered table-responsive" style="font-size: 12px; width:100%;" id="verequipo">
  <thead>
    <tr class="clsCabereraTabla">
      <th width="1%">Ítem</th>
      <th width="15%">Abreviatura</th>
      <th width="40%">Área</th>
      <th width="30%">Categoría</th>
      <th width="5%">HH</th>
      <th width="4%">Líder</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($equipo as $key => $value)
    <tr>
      <td width="1%">{{ $key + 1 }}</td>
      <td width="15%">{{ $value->abreviatura }}</td>
      <td width="40%">{{ $value->area }}</td>
      <td width="30%">{{ $value->categoria }}</td>
      <td width="5%">{{ $value->suma }}</td>
      <td width="4%">{{ $value->eslider == '1' ? 'Si':'No' }}</td>
    </tr>
    @endforeach
  </tbody>
</table>


<script type="text/javascript">
  
$('#verequipo').DataTable( {
    "paging":   false,
    "ordering": true,
    "info":     true,
    lengthChange: false,
    scrollY:        "600px",
    scrollX:        false,
    scrollCollapse: true,
    paging:         false,
    // fixedColumns:   {
    //     leftColumns: 2
    // }
    "order": [[1, 'asc']],
    "language": {
        "lengthMenu": "Mostrar _MENU_ registros por página",
        "zeroRecords": "Sin Resultados",
        "info": "_MAX_ profesionales",
        "infoEmpty": "No existe registros disponibles",
        "infoFiltered": "(filtrado de un _MAX_ total de registros)",
        "search":         "Buscar:",
        "processing":     "Procesando...",
        "paginate": {
            "first":      "Inicio",
            "last":       "Ultimo",
            "next":       "Siguiente",
            "previous":   "Anterior"
        },
        "loadingRecords": "Cargando...",
      },
    });
</script>