      @if(!$errors->isEmpty())
      <div class="alert alert-danger">
        <p>Ocurrieron los siguientes Errores:</p>
        <ul>
        @foreach($errors->all() as $error)
          <li>{{ $error }}</li>
        @endforeach
        </ul>
      </div>
      @endif