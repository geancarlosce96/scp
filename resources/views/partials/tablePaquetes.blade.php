              <table id="tPaquetes" class="table">
                <thead>
                  <th>Código</th>
                  <th>Nombre</th>
                  <th>Acciones</th>
                </thead>
                @if(isset($paquetesp))
                	<tbody>
					@foreach($paquetesp as $paq)
						<tr>
						<td>
							{{ (isset($paq['codigopaquete'])?$paq['codigopaquete']:'') }}
						</td>
						<td>
							{{ (isset($paq['nombrepaquete'])?$paq['nombrepaquete']:'') }}
						</td>
						<td>
							
							<button type="button" onclick="eliminarpaq({{ (isset($paq['cpropuestapaquete'])?$paq['cpropuestapaquete']:'') }})" class='btn btn-xs btn-warning'>Eliminar</button>
						</td>
						</tr>
					@endforeach
					</tbody>
				@endif
              </table>