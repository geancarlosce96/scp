<table class="table table-striped">
      	<thead>
        	<tr>
            	<td colspan="6"></td>
                <td colspan="3"  class="clsCabereraTabla"  style="background-color:#FC0;">ANDCLI</td>
                <td colspan="3"  class="clsCabereraTabla" style="background-color:#099;">CLIAND</td>
            </tr>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoDia">EDT</td>
                <td class="clsAnchoTabla">Tipo</td>
                <td class="clsAnchoTabla">Disciplina</td>
                <td class="clsAnchoTablaMax">Código Anddes</td>
                <td class="clsAnchoTablaMax">Descripción Entregable</td>
                <td class="clsAnchoTablaMax">Rev. Act.</td> 
                <td class="clsAnchoTabla" style="background-color:#FC0;">TR</td>
                <td class="clsAnchoTabla" style="background-color:#FC0;">Emitido</td>
                <td class="clsAnchoTabla" style="background-color:#FC0;">Fecha</td> 
                <td class="clsAnchoTabla" style="background-color:#099;">TR</td>
                <td class="clsAnchoTabla"style="background-color:#099;">Emitido</td>
                <td class="clsAnchoTabla"style="background-color:#099;">Fecha</td>
                <td class="clsAnchoTabla">Estado</td>                 
                <!--<td class="clsAnchoTablaMax">Sel</td>    -->                                                             
            </tr>
        </thead>
        <tbody>
            @if(isset($listRevDoc))
            @foreach($listRevDoc as $rev)
        	<tr>
            	<td class="clsAnchoDia">{{ $rev['cod_edt'] }}</td>
                <td class="clsAnchoTabla">{{ $rev['tipo_entregable'] }}</td>
                <td class="clsAnchoTabla">{{ $rev['disciplina'] }}</td>
                <td class="clsAnchoTabla">{{ $rev['codigo'] }}</td>
                <td class="clsAnchoTabla">{{ $rev['descripcion'] }}</td>
                <td class="clsAnchoTabla">{{ $rev['revision'] }}</td>
                <td class="clsAnchoTabla"></td>                
                <td class="clsAnchoTabla"></td> 
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td> 
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>                
                <td class="clsAnchoTabla">{{ $rev['estado_ent'] }}</td>                 
                <!--<td class="clsAnchoDia"><input type="checkbox" ></td> -->
        
        	</tr> 
            @endforeach
            @endif
        	                                             
        </tbody>

      </table>  