<!-- Modal agregar Entregable-->
    <div id="editEntregable" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Entregable</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarEntregable','method' => 'POST','id' =>'frmentregable','class'=>'form-horizontal')) !!}
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!}
          @include('partials.formEntregable')
          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" >Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Entregable-->

<!-- Modal Buscar Actividades-->
    <div id="divAct" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style="background-color: blue">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:white">Seleccionar Actividad</h4>
          </div>
          <div class="modal-body">
          <div id="tree">
            
          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Actividades-->