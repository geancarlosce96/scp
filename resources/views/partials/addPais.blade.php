<!-- Modal agregar Usuario-->
    <div id="addPa" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Pais</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarArea','method' => 'POST','id' =>'frmpais','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cpais',(isset($pais->cpais)==1?$pais->cpais:''),array('id'=>'cpais')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="cpais" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="cpais" placeholder="ID" name="cpais" value="{{(isset($pais->cpais)==1?$pais->cpais:'')}}">     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">      
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>
          

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddPa" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>




</script>  