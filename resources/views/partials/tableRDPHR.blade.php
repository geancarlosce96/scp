 <table class="table table-striped" id="tableRDP">
  <thead>
      <tr class="clsCabereraTabla">
        <th>Área</th>
        <th>Nombre</th>
        <th>Líder 1</th>
        <th>Líder 2</th>
        <!--<th>LP</th>-->
        <th style="text-align:center !important;">
        	  <!--<a href="#" class="fa fa-plus clsEnlaceBlanco" data-container="body" data-toggle="tooltip" title="Crear asignación"></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a href="#" class="fa fa-trash clsEnlaceBlanco"  data-container="body"data-toggle="tooltip" title="Eliminar asignación"></a>-->
         </th>
      </tr>            
  </thead>
  @if(isset($listRDP))
  <tbody>
      @foreach($listRDP as $rdp)
      <tr>
          <td>{{ $rdp['descripcion'] }}</td>     
          
          <td>{{ $rdp['nombre'] }}          
          {!! Form::hidden('rdp[][1]',$rdp['cdisciplina'],array('id'=>'cdisciplina_'.$rdp['cdisciplina'] )) !!}
          {!! Form::hidden('rdp[][2]',$rdp['cpersona_rdp'],array('id'=>'cpersona_rdp_'.$rdp['cpersona_rdp'] )) !!}          
          </td>
          <!--<td><input type="radio" name="lp" value="{{ $rdp['cdisciplina'] }}"
          @if(!empty($rdp['eslp']) && $rdp['eslp']=='1')
            checked="true" 
          @endif
          > </td>   -->   

          <td>
        
          <input type="radio" name="lider1" id="lider1" value="{{ $rdp['cpersona_rdp'] }}" {{$disabled}}  

          @if(isset($proy_adicional))
            <?php if ($proy_adicional->lider1==$rdp['cpersona_rdp']) {
              echo ( 'checked="true"' );
           
              } ?>

           
          @endif


          ></td>
          <td><input type="radio" name="lider2" id="lider2" value="{{ $rdp['cpersona_rdp'] }}" {{$disabled}}
          @if(isset($proy_adicional))
            <?php if ($proy_adicional->lider2==$rdp['cpersona_rdp']) {
              echo ( 'checked="true"' );
           
              } ?>

           
          @endif

          ></td>     
          <td style="text-align:center;" ><!--<input name="rdp[]" type="checkbox" value="{{ $rdp['cdisciplina'] }}">--></td>                                                                
      </tr>
      @endforeach
  </tbody>
  @endif
</table>
<br>

<script>

function validarlider(){

  var l1 =$('input[name="lider1"]:checked').val();
  var l2 =$('input[name="lider2"]:checked').val();

  if (l1==l2) {

    $('#lider1').prop('disabled',true);
    $('#lider2').prop('disabled',false);

  }


}
  

</script> 
