<!-- Modal agregar Catalogo Grupo Tablas-->
    <div id="addCTab" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Catalogo Tablas</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarCatalogoGrupoTablas','method' => 'POST','id' =>'frmcatab','class'=>'form-horizontal')) !!}

          {!! Form::hidden('catalogoid',(isset($catalotab->catalogoid)==1?$catalotab->catalogoid:''),array('id'=>'catalogoid')) !!}   
                 
           {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        
              <div class="form-group">
              <label for="catalogoid" class="col-sm-2 control-label">ID</label>
              <div class="col-sm-8">
              {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('catalogoid',(isset($catalotab->catalogoid)==1?$catalotab->catalogoid:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'catalogoid') ) !!}      
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

             <div class="form-group">
              <label for="codtab" class="col-sm-2 control-label">Tabla</label>
              <div class="col-sm-8">
                        

              {!! Form::select('codtab',(isset($tabladesc)==1?$tabladesc:null),(isset($catalotab->codtab)==1?$catalotab->codtab:''),array('class' => 'form-control select-box','id'=>'codtab')) !!}   

              </div>     
              <div class="col-sm-2">&nbsp;
              </div>
            </div>
          
           

            <div class="form-group">
            <label for="digide" class="col-sm-2 control-label">Identificador</label>
              <div class="col-sm-8">
             
                {!! Form::text('digide',(isset($digide->digide)==1?$digide->digide:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'codtab') ) !!}
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>  

            <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
              <div class="col-sm-8">
               
                {!! Form::text('descripcion',(isset($descripcion->descripcion)==1?$descripcion->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'descripcion') ) !!}
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>  

              <div class="form-group">
            <label for="valor" class="col-sm-2 control-label">Valor</label>
              <div class="col-sm-8">
               
                {!! Form::text('valor',(isset($valor->valor)==1?$valor->valor:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'valor') ) !!}
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 


          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Catalogo Grupo Tablas-->

 