
<table class="table">
    <thead>
	    <tr class="clsCabereraTabla">
	      <th>Código</th>
	      <th>Actividad/Tarea</th>
	      <th>Documentos</th>
	      <th>Planos</th>
	      <th>Acción</th>
	    </tr>
    </thead>
    <tbody>
    	@if(isset($actividadesp))
    		@foreach($actividadesp as $act)
		    <tr
		      @if(!empty($act['cpropuestaactividades_parent']))
		      	class="active"
					@else
						style="font-weight:bold"	
		      @endif		    
		    >
		      <td>{{ $act['codigoactividad'] }}</td>
		      <td style="padding:1px;
		      @if(!empty($act['cpropuestaactividades_parent']))
		      	padding-left: 20px;
		      @endif
					"
		      >{{ $act['descripcionactividad'] }}</td>
		      <td style="padding:1px;">
		      	
		      	{!! Form::text('txtnrodoc['.$act['cpropuestaactividades'].']',$act['numerodocumentos'],array('id'=>'txtnrodoc_'.$act['cactividad'],'maxlength'=>'4','size'=>'2'))!!}
		      </td>
		      <td style="padding:1px;">{!!Form::text('txtnropla['.$act['cpropuestaactividades'].']', $act['numeroplanos'],array('id'=>'txtnropla_'.$act['cactividad'],'maxlength'=>'4','size'=>'2'))  !!}</td>
		      <td style="padding:1px;">
			      <!--<a href="#" class="fa fa-pencil"></a>-->
			      
			      <button type="button" class="fa fa-trash" id="btnDelAct" onclick="fdelActi('{{ $act['cpropuestaactividades'] }}')"></button>
			      <!--<a href="#" class="fa fa-save"></a>-->
		      </td>
		    </tr>
		    @endforeach
	    @endif
    </tbody>
</table>
