<table class="table table-striped">
                      	<thead>
                        <tr>
                        	<td class="clsCabereraTabla">Parentesco</td>
                            <td class="clsCabereraTabla">Apellidos y Nombres</td>
                            <td class="clsCabereraTabla">Género</td>
                            <td class="clsCabereraTabla">DNI</td>
                            <td class="clsCabereraTabla">Acción</td>
                        </tr>  
                        </thead>
                        <tbody>
                        @if(isset($tpersonadatosfamiliare)==1)
                        @foreach($tpersonadatosfamiliare as $tfe)
                        <tr>
                        	 <td class="clsAnchoTabla">{{ $tfe->parentesco }}</td>
                           <td class="clsAnchoTabla">{{ $tfe->apaterno }} {{ $tfe->amaterno }} {{ $tfe->nombres }}</td>
                           <td class="clsAnchoTabla">{{ $tfe->genero }}</td>
                           <td class="clsAnchoTabla">{{ $tfe->identificacion }}</td>
                           <td class="clsAnchoDia">
                				          <a href="#" onclick="eliminarEmpleadoFamiliar('{{ $tfe->cpersonafamiliar }}')" data-toggle="tooltip" data-container="body" title="Eliminar Familiar">
                                  <i class="fa fa-trash" aria-hidden="true"></i></a>            
                            </td>            
                            </tr> 
                        @endforeach    
                        @endif      
                         </tbody>
</table>  