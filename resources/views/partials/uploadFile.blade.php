    <div id="uploadFile" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Subir Archivos</h4>
          </div>
          <div class="modal-body">
          {!! Form::open(
              array(
                  'route' => 'uploadFile', 
                  'class' => 'form', 
                  'novalidate' => 'novalidate', 
                  'files' => true)) !!}
            {!! Form::hidden('cpropuesta_file',(isset($propuesta->cpropuesta)==1?$propuesta->cpropuesta:'')) !!}

              <div class="form-group">
                  {!! Form::label('XLS') !!}
                  {!! Form::file('fileXls', null) !!}
              </div>

              <div class="form-group">
                  {!! Form::submit('Subir Archivos') !!}
              </div>
           {!! Form::close() !!} 
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" >Close</button>
          </div>
        </div>

      </div>
    </div>