<div class="modal fade" id="modalRevisiones" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #0069AA">
          -<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" style="color: white"><STRONG>NUEVA REVISIÓN</STRONG></h4>
        </div>
        <div class="modal-body">

          {!! Form::open(array('url' => 'grabarHR','method' => 'POST','id' =>'frmrevisionhr')) !!}

          <div class="row clsPadding2 col-lg-12 col-xs-12" style="text-align: center;">

              <div class="col-lg-1 col-xs-6"></div> 

              <div class="col-lg-10 col-xs-12 list-group active">

                  <div class="col-lg-6 col-xs-12 pull-left">

                      <span><label class="col-form-label">Revisión Actual del Documento :</label></span>

                      {!! Form::text('revisiondocumento1',(isset($documento->revision)==1?$documento->revision:''),array('class'=>'form-control pull-right', 'readonly'=>'true','id'=>'revisiondocumento1')) !!}

                  </div>   

                  <div class="col-lg-6 col-xs-12 pull-right ">

                      <span><label class="col-form-label">Nueva Revision:</label></span>

                      {!! Form::select('nuevarevisionModal',(isset($revisiones)==1?$revisiones:array()),'',array('class' => 'form-control','id'=>'nuevarevisionModal')) !!}

                  </div>  
              </div>

              <div class="col-lg-1 col-xs-6"></div>                          

          </div> 

          <div class="row clsPadding2 col-lg-12 col-xs-12" style="text-align: center;"></div> 

          {!! Form::close() !!}

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="saveRevision">Aceptar</button>
        <button type="button" class="btn btn-primary" id="cancelRevision">Cancelar</button>
      </div>
    </div>
  </div>
</div>