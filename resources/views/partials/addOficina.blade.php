<!-- Modal agregar Usuario-->
    <div id="addOfic" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Oficina</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarOficina','method' => 'POST','id' =>'frmoficina','class'=>'form-horizontal')) !!}

          {!! Form::hidden('coficinas',(isset($oficina->coficinas)==1?$oficina->coficinas:''),array('id'=>'coficinas')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="coficinas" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('coficinas',(isset($oficina->coficinas)==1?$oficina->coficinas:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'coficinas') ) !!}     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="csede" class="col-sm-2 control-label">Sede</label>
                  <div class="col-sm-8">                

                      {!! Form::select('csede',(isset($tsede)==1?$tsede:null),(isset($oficina->csede)==1?$oficina->csede:''),array('class' => 'form-control select-box','id'=>'csede')) !!}    
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="codigoofi" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="codigoofi" placeholder="Siglas" name="codigoofi"  value="{{(isset($codigoofi->codigoofi)==1?$codigoofi->codigoofi:'')}}">        
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">      
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>       

          

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"  onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>



</script>  