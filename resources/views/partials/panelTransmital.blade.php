    <!-- Modal Informacion -->
    <div id="btnTra" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">

        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Transmital</h4>
          </div>
          <div class="modal-body">

          <div class="form-group">
            <label for="ctransmittalejecucion" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">                       
                {!! Form::text('ctransmittalejecucion',(isset($transmital->ctransmittalejecucion)==1?$transmital->ctransmittalejecucion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'ctransmittalejecucion') ) !!}    
                </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="ctipo" class="col-sm-2 control-label">Tipo</label>
                  <div class="col-sm-8">
                      {!! Form::text('ctipo',(isset($transmital->ctipo)==1?$transmital->ctipo:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'ctipo') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="fecha" class="col-sm-2 control-label">Fecha</label>
                  <div class="col-sm-8">
                      {!! Form::text('fecha',(isset($transmital->fecha)==1?$transmital->fecha:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'fecha') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;

          <div class="form-group">
              <label for="tipoenvio" class="col-sm-2 control-label">Tipo Envio</label>
                  <div class="col-sm-8">
                      {!! Form::text('tipoenvio',(isset($transmital->tipoenvio)==1?$transmital->tipoenvio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'tipoenvio') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;          

          <div class="form-group">
              <label for="nrotrasmittal" class="col-sm-2 control-label">Num. Transmital</label>
                  <div class="col-sm-8">
                      {!! Form::text('nrotrasmittal',(isset($transmital->nrotrasmittal)==1?$transmital->nrotrasmittal:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'nrotrasmittal') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="ctransmittal_cliente" class="col-sm-2 control-label">Cliente</label>
                  <div class="col-sm-8">
                      {!! Form::text('ctransmittal_cliente',(isset($transmital->ctransmittal_cliente)==1?$transmital->ctransmittal_cliente:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'ctransmittal_cliente') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cpersona_cdoc" class="col-sm-2 control-label">Persona</label>
                  <div class="col-sm-8">
                      {!! Form::text('cpersona_cdoc',(isset($transmital->cpersona_cdoc)==1?$transmital->cpersona_cdoc:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cpersona_cdoc') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;          

          <div class="form-group">
              <label for="ctransconfiguracion" class="col-sm-2 control-label">Configuracion</label>
                  <div class="col-sm-8">
                      {!! Form::text('ctransconfiguracion',(isset($transmital->ctransconfiguracion)==1?$transmital->ctransconfiguracion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'ctransconfiguracion') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="ctransmittalejedetalle" class="col-sm-2 control-label">Detalle</label>
                  <div class="col-sm-8">
                      {!! Form::text('ctransmittalejedetalle',(isset($transmital->ctransmittalejedetalle)==1?$transmital->ctransmittalejedetalle:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'ctransmittalejedetalle') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="item" class="col-sm-2 control-label">Item</label>
                  <div class="col-sm-8">
                      {!! Form::text('item',(isset($transmital->item)==1?$transmital->item:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'item') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          <div class="form-group">
              <label for="codcliente" class="col-sm-2 control-label">Codigo Cliente</label>
                  <div class="col-sm-8">
                      {!! Form::text('codcliente',(isset($transmital->codcliente)==1?$transmital->codcliente:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'codcliente') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;         

          
          <div class="form-group">
              <label for="codanddes" class="col-sm-2 control-label">Codigo Anddes</label>
                  <div class="col-sm-8">
                      {!! Form::text('codanddes',(isset($transmital->codanddes)==1?$transmital->codanddes:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'codanddes') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      {!! Form::text('descripcion',(isset($transmital->descripcion)==1?$transmital->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'descripcion') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="revisionactual" class="col-sm-2 control-label">Revision</label>
                  <div class="col-sm-8">
                      {!! Form::text('revisionactual',(isset($transmital->revisionactual)==1?$transmital->revisionactual:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'revisionactual') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp; 

          <div class="form-group">
              <label for="cantidad" class="col-sm-2 control-label">Cantidad</label>
                  <div class="col-sm-8">
                      {!! Form::text('cantidad',(isset($transmital->cantidad)==1?$transmital->cantidad:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cantidad') ) !!}   
                  </div>
              <div class="col-sm-2">
              </div>
          </div>
          &nbsp;&nbsp;&nbsp;&nbsp;  

          

                   


             {!! Form::close() !!}

           </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>

      </div>
    </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

    



</script>