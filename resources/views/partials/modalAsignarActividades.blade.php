 <div class="modal fade" id="modalAsignarAct" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
        <div class="modal-dialog modal-lg" role="document" >
        <!-- Modal content-->
        <div class="modal-content">
        {!! Form::open(array('url' => 'asignarActividadesEntregables','method' => 'POST','id' =>'frmActivEmtreg')) !!}
        {!! Form::hidden('centregableproyecto','',array('id'=>'centregableproyecto')) !!}
          <div class="modal-header" style="background-color: #0069AA">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: white;" aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" style="color:white">Seleccionar Actividad</h4>
          </div>
          <div class="modal-body">
          <div class="row">

            <div class="col-lg-6 col-xs-6">
              <div class="col-lg-10 col-xs-6" id="treeActividadesTodos">
                
              </div>
              <br><br><br>
              <div class="col-lg-2 col-xs-6" >
                <button type="button" class="btn btn-success" onclick="asignarActividadesEntregables()">Agregar</button>
              </div>
                
            </div>

            <div class="col-lg-6 col-xs-6" id="tableActivAsig">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>Actividad</th>
                  <th>Porcentaje</th>
                  <th>Acciones</th>                  
                </tr>
                
              </thead>

              <tbody>
              </tbody>
            </table> 


              
            </div>            

          </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
          {!! Form::close() !!}
        </div>
      </div>