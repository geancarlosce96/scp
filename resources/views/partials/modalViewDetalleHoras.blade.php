<div class="row clsPadding2">
	<div class="col-lg-1 hidden-xs"></div>
	<div class="col-lg-10">
	    <div class="row clsPadding2">
    		<div class="col-lg-12 clsTitulo">Trazabilidad</div>
        </div>
	    <div class="row clsPadding2">
       		 <div class="col-lg-2 col-xs-2 ">Solicitante:</div>
             <div class="col-lg-4 col-xs-4 ">
             	<input type="text" class="form-control pull-right" id="datepicker" placeholder="Bruno Rosas">
              </div>
       		 <div class="col-lg-2 col-xs-2 ">Proyecto:</div>
             <div class="col-lg-4 col-xs-4 ">
             	<input type="text" class="form-control pull-right" id="datepicker" placeholder="Las Bambas">
              </div>             
		</div>
	    <div class="row clsPadding2">
       		 <div class="col-lg-2 col-xs-2 ">Actividad:</div>
             <div class="col-lg-4 col-xs-4 ">
             	<input type="text" class="form-control pull-right" id="datepicker" placeholder="1.2">
              </div>
       		 <div class="col-lg-2 col-xs-2 ">Tarea:</div>
            <div class="col-lg-4 col-xs-4 ">
             	<input type="text" class="form-control pull-right" id="datepicker" placeholder="1.2.1">
            </div>             
		</div>
        <div class="row clsPadding2" style="height:250px;">	
        <div class="col-lg-12 col-xs-12 table-responsive">
          <table class="table table-striped">
                    <thead>
                    <tr class="clsCabereraTabla">
                      <th>Semana</th>
                      <th>Secuencia</th>
                      <th>Estado</th>
                      <th>Aprobador</th>
                      <th>Cargo</th>
                      <th>Fecha de Envío</th>
                      <th>Fecha de Aprobación</th>
                      <th>Fecha de Observación</th>
                      <th>Tipo de Observación</th>
                      <th>Comentarios</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                      <td>1</td>
                      <td style="min-width:50px;">27</td>
                      <td style="min-width:50px;">1</td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>          
                    </tr>
                    <tr>
                      <td>1</td>
                      <td style="min-width:50px;">27</td>
                      <td style="min-width:50px;">2</td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>           
                    </tr>
                    <tr>
                      <td></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>
                      <td style="min-width:50px;"></td>           
                    </tr>                    
                               
                                                    
                    </tbody>
                  </table>    
        </div>
        </div>
        <div class="row clsPadding2">	
        <div class="col-lg-10 col-xs-10"></div>
        <div class="col-lg-2 col-xs-2">
        <a href="#" class="btn btn-primary btn-block "> <b>Cerrar</b></a> 
        </div>
        </div>       
    </div>
	<div class="col-lg-1 hidden-xs"></div>    
</div>
