<div id="divUminera" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:white">Registro de Unidad Minera</h4>
            </div>
            <div class="modal-body" >
              {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmuminera')) !!}
              {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_uminera')) !!}
              
              {!! Form::hidden('cpersona_u',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_u')) !!} 
              {!! Form::hidden('cuminera_u','',array('id'=>'cuminera_u')) !!}   

              <div class="row clsPadding2">
                  <div class="col-lg-12 col-xs-12 clsTitulo">
                Unidad Minera:
                  </div>  
              </div>

              <div class="row clsPadding2"> 
                  <div class="col-lg-2 col-xs-6">
                    <label class="clsTxtNormal">Código:</label>
                      {!! Form::text('codigo_um','', array('class'=>'form-control input-sm','placeholder' => '','id'=>'codigo_um','maxlength'=>'15') ) !!}
                  </div>
                  <div class="col-lg-3 col-xs-6">
                    <label class="clsTxtNormal">Nombre:</label>
                      {!! Form::text('nombre_um','', array('class'=>'form-control input-sm','placeholder' => '','id'=>'nombre_um','maxlength'=>'100')) !!}   
                  
                  </div>
                  <div class="col-lg-2 col-xs-6">
                    <label class="clsTxtNormal">Estado:</label>
                    {!! Form::select('umineraestado',(isset($umineraestado)==1?$umineraestado:array() ),'',array('class' => 'form-control','id'=>'umineraestado')) !!}             
                  </div>
                  <div class="col-lg-2 col-xs-6">
                    <label class="clsTxtNormal">Teléfono</label> 
                      {!! Form::text('telefono_um','', array('class'=>'form-control input-sm','placeholder' => '','id'=>'telefono_um','maxlength'=>'20')) !!} 
                  </div>
                  <div class="col-lg-3 col-xs-6">
                  <label class="clsTxtNormal">Dirección</label>
                    {!! Form::text('direccion_um','', array('class'=>'form-control input-sm','placeholder' => '','id'=>'direccion_um','maxlength'=>'150')) !!}   
                  </div>
              </div>      
              <div class="row clsPadding2">
                  <div class="col-lg-3 col-xs-6">
                    <label class="clsTxtNormal">Pais</label>
                    {!! Form::select('pais_um',(isset($tpais_um)==1?$tpais_um:array() ),'',array('class' => 'form-control','id'=>'pais_um')) !!} 
                  </div>
                  <div class="col-lg-3 col-xs-6">
                  <label class="clsTxtNormal">Departamento:</label>
                  {!! Form::select('dpto_um',(isset($tdpto_um)==1?$tdpto_um:array() ),'',array('class' => 'form-control','id'=>'dpto_um')) !!}       
                  </div>
                  <div class="col-lg-3 col-xs-6">
                  <label class="clsTxtNormal">Provincia:</label>
                  {!! Form::select('prov_um',(isset($tprov_um)==1?$tprov_um:array() ),'',array('class' => 'form-control','id'=>'prov_um')) !!}     
                  </div>    
                  <div class="col-lg-3 col-xs-6">
                  <label class="clsTxtNormal">Distrito:</label>
                  {!! Form::select('dis_um',(isset($tdis_um)==1?$tdis_um:array() ),'',array('class' => 'form-control','id'=>'dis_um')) !!}     
                  </div>
              </div>
              {!! Form::close() !!} 
              <div class="row clsPadding2">
                  <div class="col-lg-2 col-xs-2">
                    <button type="button" class="btn btn-primary btn-block" id="btnAddUm">
                      <i class="fa fa-plus" aria-hidden="true"></i><b> Agregar</b>
                    </button>
                  </div>
                  <div class="col-lg-8 col-xs-8">
                  </div>
                  @if(isset($tunidadminera)==1)
                    <div id="btnlogominera" class="col-lg-2 col-xs-2" style="display:none;">
                      <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#divUmineralogo">
                        Agregar logo Unidad Minera
                      </button>  
                    </div>
                  @else
                  @endif
                                        
              </div>    
               
                <div id="divMinera">
                @include('partials.modalUnidadMinera',array('umineraestado'=>(isset($umineraestado)==1?$umineraestado:null )  , 'tunidadminera' => (isset($tunidadminera)==1?$tunidadminera:null ) ))
                </div>
                @include('partials.modalRegistroLogoUnidadMinera',array('umineraestado' => (isset($umineraestado)==1?$umineraestado:null), 'tunidadminera' => (isset($tunidadminera) == 1 ? $tunidadminera:null )))
              </div>
              <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal" >Aceptar</button>
            </div>
               
            </div>
            
          </div>
        </div>
      </div>