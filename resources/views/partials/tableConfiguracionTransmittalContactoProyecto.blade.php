        <table class="table table-striped" id="table_contacto">
        	<thead>
          	<tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Apellidos</td>
              	<td class="clsAnchoTabla">Nombre</td>
                  <td class="clsAnchoTabla">Tipo Contacto</td>
                  <td class="clsAnchoTabla">Tipo Información</td>                  
                  <td class="clsAnchoTabla">Tipo Destino</td>
                  <td class="clsAnchoTabla">Cargo</td>
                  <td class="clsAnchoTabla">Correo</td>
                  
                  <td class="clsAnchoTabla">Acciones</td>
              </tr>
          </thead>

        <tbody>
        <?php $i=-1; ?>        
        @if(isset($transconfigcontac))
        @foreach($transconfigcontac as $con)
        <?php $i++; ?>
          <tr id="row_{{ $i }}">
                <td class="clsAnchoTabla">
                <input type="hidden" name="con[{{ $i }}][2]" value="{{ $con['ctransmittalconfiguracioncontacto'] }}" /> <!-- ctransmittalconfiguracioncontacto -->
                <input type="hidden" name="con[{{ $i }}][3]" value="{{ $con['cpersona_contacroanddes'] }}" /> <!-- cpersona_contacroanddes --> 
                <input type="hidden" name="con[{{ $i }}][4]" value="{{ $con['cunidadmineracontacto'] }}" /> <!-- cunidadmineracontacto --> 
                <input type="hidden" name="con[{{ $i }}][5]" value="{{ $con['apellidos'] }}" /> <!-- apellidos --> 
                <input type="hidden" name="con[{{ $i }}][6]" value="{{ $con['nombres'] }}" /> <!-- nombres --> 
                <input type="hidden" name="con[{{ $i }}][7]" value="{{ $con['ctipocontacto'] }}" /> <!-- ctipocontacto --> 
                <input type="hidden" name="con[{{ $i }}][8]" value="{{ $con['ccontactocargo'] }}" /> <!-- ccontactocargo --> 
                <input type="hidden" name="con[{{ $i }}][9]" value="{{ $con['email'] }}" /> <!-- email --> 
                <input type="hidden" name="con[{{ $i }}][10]" value="N" /> <!-- eliminar --> 
                               
                {{ $con['apellidos'] }}</td>
                <td class="clsAnchoTabla">{{ $con['nombres'] }}</td>
                <td class="clsAnchoTabla">{{ $con['contacto'] }}</td>
                <td class="clsAnchoTabla">
                
                  {!! Form::select('con['.$i.'][0][]',(isset($ttipoinformacion)==1?$ttipoinformacion:array()),(isset($con['ttipoinformacion'])==1?$con['ttipoinformacion']:array()),array('class' => 'chosen-select','id'=>'tipoinformacion_'.$i.'_0','data-placeholder' => 'Seleccione Tipo de Información','multiple'=>'multiple','style'=>'width:250px;')) !!}

                </td>                
                <td class="clsAnchoTabla">

                  {!! Form::select('con['.$i.'][1][]',(isset($ttipodestino)==1?$ttipodestino:array()),(isset($con['ttipodestino'])==1?$con['ttipodestino']: array() ),array('class' => 'chosen-select','id'=>'tipodestino_'.$i.'_1','data-placeholder' => 'Seleccione Tipo de Destino','multiple'=>'multiple','style'=>'width:250px;')) !!}                
                </td>
                <td class="clsAnchoTabla">{{ $con['contacto_cargo'] }} </td>
                <td class="clsAnchoTabla">{{ $con['email'] }}</td>
                
                <td class="clsAnchoTabla">
              	    <a href="#" onclick="eliminarFila({{ $i }});/*eliminarContacto({{$con['ctransmittalconfiguracioncontacto'] }})*/" data-toggle="tooltip"  title="Eliminar Contacto">
                  <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
                </td>           
          </tr>
        @endforeach
        @endif
        </tbody>
        </table>  