<div id="div_{{ (isset($idTabla)==1?$idTabla:0) }}">
    <p id="tit_{{ (isset($idTabla)==1?$idTabla:0) }}">{{ (isset($titulo)==1?$titulo:'') }}</p>
    <input id="cproyecto_{{ (isset($idTabla)==1?$idTabla:0) }}" name="cproyecto_{{ (isset($idTabla)==1?$idTabla:0) }}" value="{{ (isset($key['cproyecto'])==1?$key['cproyecto']:'') }}" type="hidden">
    <input id="cservicioproy_{{ (isset($idTabla)==1?$idTabla:0) }}" name="cservicioproy_{{ (isset($idTabla)==1?$idTabla:0) }}" value="{{ (isset($key['cservicioproy'])==1?$key['cservicioproy']:'') }}" type="hidden">
    <input id="cpropuesta_{{ (isset($idTabla)==1?$idTabla:0) }}" name="cpropuesta_{{ (isset($idTabla)==1?$idTabla:0) }}" value="{{ (isset($key['cpropuesta'])==1?$key['cpropuesta']:'') }}" type="hidden">
    <input id="cservicioprop_{{ (isset($idTabla)==1?$idTabla:0) }}" name="cservicioprop_{{ (isset($idTabla)==1?$idTabla:0) }}" value="{{ (isset($key['cservicioprop'])==1?$key['cservicioprop']:'') }}" type="hidden">
    <input id="carea_{{ (isset($idTabla)==1?$idTabla:0) }}" name="carea_{{ (isset($idTabla)==1?$idTabla:0) }}" value="{{ (isset($key['carea'])==1?$key['carea']:'') }}" type="hidden">
    <input id="ccentrocosto_{{ (isset($idTabla)==1?$idTabla:0) }}" name="ccentrocosto_{{ (isset($idTabla)==1?$idTabla:0) }}" value="{{ (isset($key['ccentrocosto'])==1?$key['ccentrocosto']:'') }}" type="hidden">
    <input id="numregistro_{{ (isset($idTabla)==1?$idTabla:0) }}" name="numregistro_{{ (isset($idTabla)==1?$idTabla:0) }}" value="{{ (isset($numReg)==1?$numReg:0) }}" type="hidden">
    <table class="table table-striped" id="table_{{ (isset($idTabla)==1?$idTabla:0) }}">

            <thead>
            <tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Tipo Comprobante</td>
                <td class="clsAnchoTabla">Moneda</td>
                <td class="clsAnchoTabla">Fec Documento</td> 
                <td class="clsAnchoTabla">Tipo Actividad</td>
                <td class="clsAnchoTabla">Tipo Gasto</td>
                <td class="clsAnchoTabla">Descripcion</td>
                <td class="clsAnchoTabla">Proveedor</td>
               <!-- <td class="clsAnchoTabla">Reembolsable</td>-->
                <td class="clsAnchoTabla" style="width: auto;">Nro. Comprobante</td>
                <td class="clsAnchoTabla" width="1%">Cantidad</td>
                <td class="clsAnchoTabla">SubTotal</td>
                <td class="clsAnchoTabla">IGV</td>
                <td class="clsAnchoTabla">Otro Imp</td>
                <td class="clsAnchoTabla">Total</td>
                <td class="clsAnchoTabla" width="1%" >Acciones</td>
            </tr>
            </thead>
            <tbody>

        @if(isset($gastosProyDet)==1)
        <?php $j=0; ?>
            @foreach($gastosProyDet as $gap)
              <?php $j++; ?>
              <tr class="">
                <td class="">
                  <input name="cpersona_empleado_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="cpersona_empleado_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" value="{{ $gap['cpersona_empleado'] }}" type="hidden">
                  <input name="cgastoejecuciondet_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="cgastoejecuciondet_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" value="{{ $gap['cgastosejecuciondet'] }}" type="hidden">
                  
                  {!! Form::select('tipodocumento_'.(isset($idTabla)==1?$idTabla:0).'_'.$j,(isset($tipodocumento)==1?$tipodocumento:null), $gap['tipodocumento'] ,array('class' => ' select-box','id'=>'tipodocumento_'.(isset($idTabla)==1?$idTabla:0).'_'.$j, 'onchange'=>'calculoTotal('.$idTabla.','.$j.');calculoTotalDolar('.$idTabla.','.$j.');totalRendicion();totalRendicion();totalRendicionDolares();' )) !!} 
                </td> 
                <td class="">
                {!! Form::select('cmoneda_'.(isset($idTabla)==1?$idTabla:0).'_'.$j,(isset($tmoneda)==1?$tmoneda:null),$gap['cmoneda'],array('class' => ' select-box','id'=>'cmoneda_'.(isset($idTabla)==1?$idTabla:0).'_'.$j,'onchange'=>'calculoTotalDolar('.$idTabla.','.$j.');totalRendicion();totalRendicionDolares();')) !!}  
                  
                </td>
                <td class="">
                  <input size="5" name="fdocumento_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="fdocumento_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class=" datepicker" value="{{ $gap['fdocumento'] }}" type="text" onchange='calculoTotalDolar({{ $idTabla }},{{ $j }});totalRendicionDolares();'>
                </td>
                <td class="">
                  <select name="tipoactividad_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="tipoactividad_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" onchange='fillTGasto({{ (isset($idTabla)==1?$idTabla:0) }},{{ $j }})' class='select-box'>
                  <?php 
                  $option_select = $gap['tipoactividad_option'];
                  ?>
                  @if(isset($option_select)==1)
                    <?php echo  $option_select; ?>
                  @endif
                  </select>
                  
                </td>
                <td class="">
                  <select name="tipogasto_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="tipogasto_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}"  class='select-box'>
                  <?php 
                  $option_select_t = $gap['tipogasto_option'];
                  ?>
                  @if(isset($option_select)==1)
                    <?php echo  $option_select_t; ?>
                  @endif
                  </select>                  
              
                </td>
                <td class="">
                  <textarea name="descripcion_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="descripcion_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['descripcion'] }}" type="text" rows='4'>{{ $gap['descripcion'] }}</textarea>
                </td>
                <td class="">
                  <input name="cpersona_prov_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="cpersona_prov_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" value="{{ $gap['cpersona_cliente'] }}" type="hidden">
                  <input size="12" name="ruc_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="ruc_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="pull-left" value="{{ $gap['ruc_cliente'] }}" type="text" placeholder="Ruc">
                  <input name="razon_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="razon_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="input-sm" value="{{ $gap['razon_cliente'] }}" type="text" placeholder="Razon Social">
                </td>
              <!--  <td class="">
                    <input name="reembolsable_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="reembolsable_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" value="00001" type="checkbox" {{ ($gap['reembolsable']=='00001'?'checked':'' ) }}>
                </td>-->
                <td class="">
                  <div class='input-group' style='width:100%;'>
                  <input style='width:20%' title='Prefijo del comprobante, debe ser letras' placeholder='Prefijo' name="numerodocumento_p{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="numerodocumento_p{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['numerodocumentop'] }}" type="text">
                  <input style='width:30%' title='Serie del comprobante' placeholder='Serie' name="numerodocumento_s{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="numerodocumento_s{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['numerodocumentos'] }}" type="text">
                  <input style='width:50%' title='Número del comprobante' placeholder='Numero' name="numerodocumento_n{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="numerodocumento_n{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['numerodocumenton'] }}" type="text">
                </div>
                </td>
                <td class="">
                  <input size="1" name="cantidad_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="cantidad_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['cantidad'] }}" onblur="calculoTotal({{ $idTabla }},{{ $j }});calculoTotalDolar({{ $idTabla }},{{ $j }});totalRendicion();totalRendicionDolares();" type="text">
                </td>
                <td class="">
                  <input size="4" name="preciou_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="preciou_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['preciounitario'] }}" onblur="calculoTotal({{ $idTabla }},{{ $j }});calculoTotalDolar({{ $idTabla }},{{ $j }});totalRendicion();totalRendicionDolares();" type="text"></td>     


                <td class="">
                  <input size="4" name="impuesto_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="impuesto_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['impuesto'] }}"  onblur="calculoTotal({{ $idTabla }},{{ $j }});calculoTotalDolar({{ $idTabla }},{{ $j }});totalRendicion();totalRendicionDolares();" type="text">
                </td>
                <td class="">
                  <input size="4" name="otrimp_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="otrimp_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['otrimp'] }}" onblur="calculoTotal({{ $idTabla }},{{ $j }});calculoTotalDolar({{ $idTabla }},{{ $j }});totalRendicion();totalRendicionDolares();"  type="text">
                </td>
                <td class="">
                  <input size="4" name="total_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="total_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" class="" value="{{ $gap['totaldet'] }}" onblur="totalRendicion();totalRendicionDolares();"  type="text">

                  <input size="4" name="total_dolar_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" id="total_dolar_{{ (isset($idTabla)==1?$idTabla:0) }}_{{ $j }}" value="{{ $gap['totaldolares'] }}" type="hidden">

                </td>                                
                <td class="">
                  <a href="#" class="glyphicon glyphicon-plus" data-toggle="tooltip"  title="Duplicar" onclick="agregarFilasTable({{ (isset($idTabla)==1?$idTabla:0) }})"></a>
                  <!--<a href="#" class="fa fa-trash" onclick="fdelGastoProy({{ (isset($idTabla)==1?$idTabla:0) }},this)" data-toggle="tooltip"  title="Eliminar" aria-hidden="true"></a>-->
                  <a href="#" class="fa fa-trash" onclick="fdelGastoProyBD('{{ $gap['cgastosejecuciondet'] }}');saveTemporales()" data-toggle="tooltip"  title="Eliminar" aria-hidden="true" id="btnDelete"></a>
               
                </td>
              </tr>

          
                                                	
              @endforeach
        @endif
            
        </tbody>
            
          </table>
</div>