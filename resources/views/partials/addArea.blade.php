<!-- Modal agregar Usuario-->
    <div id="addAreas" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Area</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarArea','method' => 'POST','id' =>'frmarea','class'=>'form-horizontal')) !!}

          {!! Form::hidden('carea',(isset($area->carea)==1?$area->carea:''),array('id'=>'carea')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="carea" class="col-sm-2 control-label">Area</label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('carea',(isset($area->carea)==1?$area->carea:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'carea') ) !!}     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="codigo" placeholder="Codigo" name="codigo" value="{{(isset($codigo->codigo)==1?$codigo->codigo:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="siglas" class="col-sm-2 control-label">Siglas</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="siglas" placeholder="Siglas" name="siglas" value="{{(isset($siglas->siglas)==1?$siglas->siglas:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="carea_parent" class="col-sm-2 control-label">Area Superior</label>
                  <div class="col-sm-8">                            
                    
                    {!! Form::select('carea_parent',(isset($area_parent)==1?$area_parent:null),(isset($area->carea_parent)==1?$area->carea_parent:''),array('class' => 'form-control select-box','id'=>'carea_parent')) !!} 

                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="esgeneral" class="col-sm-2 control-label">esGeneral</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="esgeneral" placeholder="esGen" name="esgeneral" value="{{(isset($esgeneral->esgeneral)==1?$esgeneral->esgeneral:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="estecnica" class="col-sm-2 control-label">estecnica</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="estecnica" placeholder="esTecnica" name="estecnica" value="{{(isset($estecnica->estecnica)==1?$estecnica->estecnica:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="esarea" class="col-sm-2 control-label">esArea</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="esarea" placeholder="esArea" name="esarea" value="{{(isset($esarea->esarea)==1?$esarea->esarea:'')}}">     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddArea" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>




</script>  