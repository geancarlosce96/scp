 <div class="modal fade" id="modalAlertRev" tabindex="-1" role="dialog" aria-labelledby="ObsModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: white;" aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="color: white"><STRONG>¡ALERTA!</STRONG></h4>
      </div>
      <div class="modal-body">

          <div class="row clsPadding2" style="text-align: center;">
              <h3 id="mensajeRev"> </h3> 
          </div> 
          
          <div class="row clsPadding2"></div>

          <div class="row clsPadding2">
              <div class="col-lg-2 col-xs-6"></div>
              <div class="col-lg-4 col-xs-6"><button style="width: 100%" type="button" class="btn btn-primary" id="btnSI"><strong style="font-size: 12px;">SI</strong></button></div>
              <div class="col-lg-4 col-xs-6"><button style="width: 100%" type="button" class="btn btn-primary" id="btnNO"><strong style="font-size: 12px;">NO</strong></button></div>
              <div class="col-lg-2 col-xs-6"></div>
          </div>            
      </div>
    </div>
  </div>
</div>