<table class="table">
    <thead>
	    <tr class="clsCabereraTabla">
	      <th>Código</th>
	      <th>Actividad/Tarea</th>
	      <th>Documentos</th>
	      <th>Planos</th>
	      <th>Acción</th>
	    </tr>
    </thead>
    <tbody>
    	@if(isset($actividadesp))
    		@foreach($actividadesp as $act)
		    <tr
		      @if(!empty($act['cactividad_parent']))
		      	class="active"
					@else
						style="font-weight:bold"
		      @endif		    
		    >
		      <td style="padding:1px;">{{ $act['item'] }}</td>
		      <td style="padding:1px;
				@if(!empty($act['cactividad_parent']))
		      	padding-left: 20px
		      @endif	
					"	      
		      >{{ $act['descripcionactividad'] }}</td>
		      <td style="padding:1px;">
		      	
		      	{!! Form::text('txtnrodoc['.$act['cactividad'].']',$act['numerodocumentos'],array('id'=>'txtnrodoc_'.$act['cactividad'],'size'=>'2','maxlength'=>'4'))!!}
		      </td>
		      <td style="padding:1px;">{!!Form::text('txtnropla['.$act['cactividad'].']', $act['numeroplanos'],array('id'=>'txtnropla_'.$act['cactividad'],'size'=>'2','maxlength'=>'4'))  !!}</td>
		      <td style="padding:1px;">
			      <!--<a href="#" class="fa fa-pencil"></a>-->
			      
			      <button type="button" class="fa fa-trash" id="btnDelAct" onclick="fdelActi('{{ $act['cproyectoactividades'] }}')"></button>
			      <!--<a href="#" class="fa fa-save"></a>-->
		      </td>
		    </tr>
		    @endforeach
	    @endif
    </tbody>
</table>
