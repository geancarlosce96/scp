<table id="tableHorasAprob" class="table table-hover table-responsive" style="border-spacing:0px;">
  @if(isset($alinea))
  <tbody>
    <?php 
    $i=0; 
    $j=-1;
    $totalsem=0;
    $tot_1=0;
    $tot_2=0;
    $tot_3=0;
    $tot_4=0;
    $tot_5=0;
    $tot_6=0;
    $tot_7=0;                      
    ?>
    @foreach($alinea as $eje)


    <?php 

    $fec = Carbon\Carbon::createFromDate($fecp->year,$fecp->month,$fecp->day);
    $i++;
    $j++;
    $totl=0;
    ?>
    <tr class="gradeX" id="row_{{ $i}}">
      <th style="width: 22px; border:1px solid #DCDCDCFF;">
        <center>
        @if($eje['tipo']=='1')
        <a href="#" onclick="clonarfila('{{ ($i) }}')" class="fa fa-plus"></a><br />
        @endif
        <a href="#" onclick="eliminarFila({{ ($i) }},{{ $j }})" class="fa fa-trash"></a>
        <!--<a href="#" class="fa fa-save"></a>&nbsp;&nbsp;&nbsp;&nbsp;-->
        </center>
      </th>
      <th style="width: 1050px; font-size:12px;  border:1px solid #DCDCDCFF;">
        <input type='hidden' name='ejecu[{{ $j }}][0]' id="pry_{{ $i }}" value="{{ $eje['cproyectoactividades'] }}" />
        <input type='hidden' name='ejecu[{{ $j }}][1]' id="act_{{ $i }}" value="{{ $eje['cactividad'] }}" />
        <input type='hidden' name='ejecu[{{ $j }}][2]' id="tipo_{{ $i }}" value="{{ $eje['tipo'] }}" />
        <input type='hidden' name='ejecu[{{ $j }}][51]' id="cproy_{{ $i }}" value="{{ $eje['cproyecto'] }}" />
        <input type='hidden' name='ejecu[{{ $j }}][52]' id="desAct_{{ $i }}" value="{{ $eje['descripcionactividad'] }}" />
        <input type='hidden' name='ejecu[{{ $j }}][53]' id="removeAct_{{ $i }}" value="N" />
        <input type='hidden' name='ejecu[{{ $j }}][54]' id="ejecab_{{ $i }}" value="{{ $eje['cproyectoejecucioncab'] }}" />
        <input type='hidden' name='proys_{{ $j }}' id="desc_{{ $i}}" value="{{ $eje['codigopry'] }}/ {{ $eje['personanombre'] }} / {{ $eje['unidadminera'] }} / {{ $eje['codigoactividad'] }} /" />
        @if($eje['codigopry'])
        {{ $eje['codigopry'] }}/{{ $eje['personanombre'] }} / {{ $eje['unidadminera'] }} / {{ $eje['nombreproy'] }}  / {{ $eje['codigoactividad'] }}  : 
        @endif
        {{ $eje['descripcionactividad'] }}
      </th>
      <th style="width: 40px; border:1px solid #DCDCDCFF;" class="categoria">{{$eje['categoriaproy']}}</th>
      <th style="width: 40px; border:1px solid #DCDCDCFF;" onmouseover="mouseOver('dash_tarea_{{ $i }}');" onmouseout="mouseOut('dash_tarea_{{ $i }}');">
          <center>
            @if($eje['tipo']=='1')
            <span class="label" style="background-color:#11B15B;">FF</span>
            @endif
            @if($eje['tipo']=='2')
            <span class="label" style="background-color:#CD201A;">NF</span>
            @endif
            @if($eje['tipo']=='3')
            <span class="label" style="background-color:#0069AA;"">AND</span>
            @endif
          </center>
          <div id="dash_tarea_{{ $i}}" style="display:none;position:absolute;width:240px" class="panel panel-default">
            <div class="panel-heading" >Dashboard - Avance de Tarea</div>
            <div class="panel-body">
              <div class="progress" style="height:16px" >
                <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active"  role="progressbar" aria-valuenow="{{ $eje['porav'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $eje['porav'] }}%;font-size:10px;padding:1px; background-color: @if($eje['porav']<60) green @elseif($eje['porav']<80) orange @else red @endif">
                  <span style="display: block; position: absolute; width: 130px; color: black;" data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en horas">{{ $eje['mensajehoras'] }}</span>
                </div>
              </div>       
              <div class="progress " style="height:16px" >
                <div class="progress-bar progress-bar-warning progress-bar-striped active"  role="progressbar" aria-valuenow="{{ $eje['porav_costo'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $eje['porav_costo'] }}%;font-size:10px;padding:1px; background-color: @if($eje['porav_costo']<60) green @elseif($eje['porav_costo']<80) orange @else red @endif">
                  <span style="display: block; position: absolute; width: 130px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en monto">{{ $eje['mensajecosto'] }}</span>
                </div>
              </div>  
            </div>
          </div>
      </th>
      <th style="width: 60px; border:1px solid #DCDCDCFF;">
          <center>
            <?php /*@if($eje['tipo']=='2' )*/ ?>
            {!! Form::select('ejecu['.$j.'][3]',(isset($nofacturables)==1?$nofacturables:array()),(isset($eje['ctiponofacturable'])==1?$eje['ctiponofacturable']:null),array('id'=>'nfac_'.$i))!!}
            <?php /*@endif*/ ?>
          </center>
      </th>
      <th style="width: 60px; border:1px solid #DCDCDCFF;">
        <div id="gp_{{ $i }}">
          <center>
            {{ $eje['gp'] }}
          </center>
        </div>
      </th>
      <!--<td class="clsAnchoTabla"></td>-->
      <!--<td >&nbsp;</td>-->
      <?php /* Prepara cuadro de texto para el input dia 1 */
        $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
        $valor= '';
        $valor_pla = "";
        $estado='';
        $cproyectoejecucion='';
        $obs='';
        $obs_planificada="";
        $obs_aprobada="";
        $obs_observada="";
        $obs_ejecutada="";
        $color="";
        $bgcolor="";
        $disabled=false;
        $cestructuraproyecto="";
        if (isset($eje[$key])==1){
          $estado= (isset($eje[$key]['estado'])==1?$eje[$key]['estado']:'');
          $cproyectoejecucion = (isset($eje[$key]['cproyectoejecucion'])==1?$eje[$key]['cproyectoejecucion']:'');
          $cestructuraproyecto = (isset($eje[$key]['cestructuraproyecto'])==1?$eje[$key]['cestructuraproyecto']:'');
          $valor_pla = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
          $obs_planificada=(isset($eje[$key]['obsplanificadas'])==1?$eje[$key]['obsplanificadas']:'');
          $obs_aprobada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
          $obs_observada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
          $obs_ejecutada=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          switch ($eje[$key]['estado']){
            case '1':
                $valor = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
                
                $color="black";
                $bgcolor="#FFBF00";
              break;
            case '2':
                $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
                $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
                $color="white";
                $bgcolor="#CD201A";
              break;                            
            case '3':
                $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
                $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
                $color="white";
                $bgcolor="#0069AA";
                break;
            case '4':
              $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
                $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
                $color="white";
                $bgcolor="#CD201A";
              break;
            case '5':
                $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
                
                $color="black";
                $bgcolor="#11B15B";
                $disabled=true;
              break;
          }
          if(strlen($valor)>0){
            $totl+=$valor;
            $tot_1+=$valor;
          }

        }
     
      ?>
      <th style="width: 60px; padding:0;  border:1px solid #DCDCDCFF;">
        <center class="div-celda">
          <div>
            <input type='hidden' name='ejecu[{{ $j }}][4]' value="{{ $cproyectoejecucion }}" />
            <input type='hidden' name='ejecu[{{ $j }}][5]' value="{{ $estado }}" />
            <input type='hidden' name='ejecu[{{ $j }}][6]' value="{{ $obs }}" id="txt_{{ $j }}_6"/>
            <input type='hidden' name='ejecu[{{ $j }}][7]' value="{{ $key }}" />
            <input type='hidden' name='ejecu[{{ $j }}][39]' value="{{ $cestructuraproyecto }}" />
            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][8]' value='{{ $valor }}' 

            onchange="disabledEnviar();viewSobre('{{ $i }}_1',this,'txt_{{ $j }}_6');sumHoras({{ $j }})" 
            onkeypress="return soloNumeros(event,this);"
            @if(!($estado==1 || $estado==2 ||  $estado==4 || strlen($estado)<=0))
            @if(strlen($valor) > 0)
            readonly='true'
            @else
            
            @endif
            @else
            
            @endif     

            @if(strlen($bgcolor)>0)
            @if(strlen($valor) > 0)
            style="background-color:{{ $bgcolor }};color: {{ $color }}; font-weight: bold; text-align: center;"
            @endif
            @endif   
            @if(strlen($valor_pla)>0)
            data-toggle="tooltip" data-placement="top" title="Planificado: {{ $valor_pla }}"
            @endif                                                               
            ><br/>
            <a href="#" onclick="viewObs({{ $i }},6,1,'{{ $obs_planificada }}','{{ $obs_aprobada }}')" id="obs_{{ $i }}_1" 
            @if(strlen($valor)<=0)
            style="display:none"
            @else
            @if($valor <=0)
            style="display:none"                        
            @endif
            @endif
            >
            @if(($obs != '') or ($obs_planificada != '') or ($obs_observada != ''))
            <i class="glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @else
            <i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @endif
          </a>
          </div>
        </center>
      </th>
      <?php /* Prepara cuadro de texto para el input dia 2 */
      $fec->addDay();
      $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
      $valor= '';
      $valor_pla = '';
      $estado='';
      $cproyectoejecucion='';
      $obs='';
      $obs_planificada="";
      $obs_aprobada="";
      $obs_observada="";
      $obs_ejecutada="";
      $color="";
      $bgcolor="";
      $disabled=false;     
      $cestructuraproyecto="";                   
      if (isset($eje[$key])==1){
        $estado= (isset($eje[$key]['estado'])==1?$eje[$key]['estado']:'');
        $cproyectoejecucion = (isset($eje[$key]['cproyectoejecucion'])==1?$eje[$key]['cproyectoejecucion']:'');
        $cestructuraproyecto = (isset($eje[$key]['cestructuraproyecto'])==1?$eje[$key]['cestructuraproyecto']:'');
        $valor_pla = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
        $obs_planificada=(isset($eje[$key]['obsplanificadas'])==1?$eje[$key]['obsplanificadas']:'');
        $obs_aprobada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_observada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_ejecutada=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
        switch ($eje[$key]['estado']){
          case '1':
          $valor = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');

          $color="black";
          $bgcolor="#FFBF00";
          break;
          case '2':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;                            
          case '3':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#0069AA";
          break;
          case '4':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;
          case '5':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');

          $color="black";
          $bgcolor="#11B15B";
          $disabled=true;
          break;
        }
        if(strlen($valor)>0){
          $totl+=$valor;
          $tot_2+=$valor;
        }       

      }

      ?>
      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
        <center class="div-celda">
          <div>
            <input type='hidden' name='ejecu[{{ $j }}][9]' value="{{ $cproyectoejecucion }}" />
            <input type='hidden' name='ejecu[{{ $j }}][10]' value="{{ $estado }}" />
            <input type='hidden' name='ejecu[{{ $j }}][11]' value="{{ $obs }}" id="txt_{{ $j }}_11" />
            <input type='hidden' name='ejecu[{{ $j }}][12]' value="{{ $key }}" />
            <input type='hidden' name='ejecu[{{ $j }}][40]' value="{{ $cestructuraproyecto }}" />
            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][13]' value='{{ $valor }}'

            onchange="disabledEnviar();viewSobre('{{ $i }}_2',this,'txt_{{ $j }}_11');sumHoras({{ $j }})" 
            onkeypress="return soloNumeros(event,this);"
            @if(!($estado==1 || $estado==2 ||  $estado==4 || strlen($estado)<=0))
            @if(strlen($valor) > 0)
            readonly='true'
            @else
                                        
            @endif
            @else
                                    
            @endif   
            @if(strlen($bgcolor)>0)
            @if(strlen($valor) > 0)
            style="background-color:{{ $bgcolor }};color: {{ $color }}; font-weight: bold; text-align: center;"
            @endif
            @endif     
            @if(strlen($valor_pla)>0)
            data-toggle="tooltip" data-placement="top" title="Planificado: {{ $valor_pla }}"
            @endif                                                                      
            ><br/>
            <a href="#" onclick="viewObs({{ $i }},11,2,'{{ $obs_planificada }}','{{ $obs_aprobada }}')" id="obs_{{ $i }}_2"
            @if(strlen($valor)<=0)
            style="display:none"
            @else
            @if($valor <=0)
            style="display:none"                        
            @endif
            @endif                        
            >
            @if(($obs != '') or ($obs_planificada != '') or ($obs_observada != ''))
            <i class="glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @else
            <i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @endif                      
          </a>
          </div>
        </center>
      </th>
      <?php /* Prepara cuadro de texto para el input dia 3 */
      $fec->addDay();
      $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
      $valor= '';
      $valor_pla ='';
      $estado='';
      $cproyectoejecucion='';
      $obs='';
      $obs_planificada="";
      $obs_aprobada="";
      $obs_observada="";
      $obs_ejecutada="";
      $color="";
      $bgcolor="";
      $disabled=false;     
      $cestructuraproyecto="";                  
      if (isset($eje[$key])==1){
        $estado= (isset($eje[$key]['estado'])==1?$eje[$key]['estado']:'');
        $cproyectoejecucion = (isset($eje[$key]['cproyectoejecucion'])==1?$eje[$key]['cproyectoejecucion']:'');
        $cestructuraproyecto = (isset($eje[$key]['cestructuraproyecto'])==1?$eje[$key]['cestructuraproyecto']:'');
        $valor_pla = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
        $obs_planificada=(isset($eje[$key]['obsplanificadas'])==1?$eje[$key]['obsplanificadas']:'');    
        $obs_aprobada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_observada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_ejecutada=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');                      
        switch ($eje[$key]['estado']){
          case '1':
          $valor = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');

          $color="black";
          $bgcolor="#FFBF00";
          break;
          case '2':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;                            
          case '3':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#0069AA";
          break;
          case '4':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;
          case '5':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');

          $color="black";
          $bgcolor="#11B15B";
          $disabled=true;
          break;
        }
        if(strlen($valor)>0){
          $totl+=$valor;
          $tot_3+=$valor;
        }    

      }

      ?> 
      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
        <center class="div-celda">
          <div>
            <input type='hidden' name='ejecu[{{ $j }}][14]' value="{{ $cproyectoejecucion }}" />
            <input type='hidden' name='ejecu[{{ $j }}][15]' value="{{ $estado }}" />
            <input type='hidden' name='ejecu[{{ $j }}][16]' value="{{ $obs }}" id="txt_{{ $j }}_16" />
            <input type='hidden' name='ejecu[{{ $j }}][17]' value="{{ $key }}" />
            <input type='hidden' name='ejecu[{{ $j }}][41]' value="{{ $cestructuraproyecto }}" />
            <input type='text' size='2' maxlength="5" name='ejecu[{{ $j }}][18]' value='{{ $valor }}' 

            onchange="disabledEnviar();viewSobre('{{ $i }}_3',this,'txt_{{ $j }}_16');sumHoras({{ $j }})" 
            onkeypress="return soloNumeros(event,this);"
            @if(!($estado==1 || $estado==2 ||  $estado==4 || strlen($estado)<=0))
            @if(strlen($valor) > 0)
            readonly='true'
            @else
                                      
            @endif
            @else
                                    
            @endif  
            @if(strlen($bgcolor)>0)
            @if(strlen($valor) > 0)
            style="background-color:{{ $bgcolor }};color: {{ $color }}; font-weight: bold; text-align: center;"
            @endif
            @endif   
            @if(strlen($valor_pla)>0)
            data-toggle="tooltip" data-placement="top" title="Planificado: {{ $valor_pla }}"
            @endif                                                                         
            ><br/>
            <a href="#" onclick="viewObs({{ $i }},16,3,'{{ $obs_planificada }}','{{ $obs_aprobada }}')" id="obs_{{ $i }}_3" 
            @if(strlen($valor)<=0)
            style="display:none"
            @else
            @if($valor <=0)
            style="display:none"                        
            @endif
            @endif                        
            >
            @if(($obs != '') or ($obs_planificada != '') or ($obs_observada != ''))
            <i class="glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @else
            <i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @endif                      
          </a>
          </div>
        </center>
      </th>
      <?php /* Prepara cuadro de texto para el input dia 4 */
      $fec->addDay();
      $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
      $valor= '';
      $valor_pla="";
      $estado='';
      $cproyectoejecucion='';
      $obs='';
      $obs_planificada="";
      $obs_aprobada="";
      $obs_observada="";
      $obs_ejecutada="";
      $color="";
      $bgcolor="";
      $disabled=false;    
      $cestructuraproyecto="";                    
      if (isset($eje[$key])==1){
        $estado= (isset($eje[$key]['estado'])==1?$eje[$key]['estado']:'');
        $cproyectoejecucion = (isset($eje[$key]['cproyectoejecucion'])==1?$eje[$key]['cproyectoejecucion']:'');
        $cestructuraproyecto = (isset($eje[$key]['cestructuraproyecto'])==1?$eje[$key]['cestructuraproyecto']:'');
        $valor_pla = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
        $obs_planificada=(isset($eje[$key]['obsplanificadas'])==1?$eje[$key]['obsplanificadas']:'');
        $obs_aprobada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_observada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_ejecutada=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');

        switch ($eje[$key]['estado']){
          case '1':
          $valor = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');

          $color="black";
          $bgcolor="#FFBF00";
          break;
          case '2':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;                            
          case '3':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#0069AA";
          break;
          case '4':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;
          case '5':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');

          $color="black";
          $bgcolor="#11B15B";
          $disabled=true;
          break;
        }
        if(strlen($valor)>0){
          $totl+=$valor;
          $tot_4+=$valor;
        }           

      }

      ?>
      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
        <center class="div-celda">
          <div>
            <input type='hidden' name='ejecu[{{ $j }}][19]' value="{{ $cproyectoejecucion }}" />
            <input type='hidden' name='ejecu[{{ $j }}][20]' value="{{ $estado }}" />
            <input type='hidden' name='ejecu[{{ $j }}][21]' value="{{ $obs }}" id="txt_{{ $j }}_21" />
            <input type='hidden' name='ejecu[{{ $j }}][22]' value="{{ $key }}" />
            <input type='hidden' name='ejecu[{{ $j }}][42]' value="{{ $cestructuraproyecto }}" />
            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][23]' value='{{ $valor }}' 

            onchange="disabledEnviar();viewSobre('{{ $i }}_4',this,'txt_{{ $j }}_21');sumHoras({{ $j }})" 
            onkeypress="return soloNumeros(event,this);"
            @if(!($estado==1 || $estado==2 ||  $estado==4 || strlen($estado)<=0))}
            @if(strlen($valor) > 0)
            readonly='true'
            @else
                                      
            @endif
            @else
                                    
            @endif      
            @if(strlen($bgcolor)>0)
            @if(strlen($valor) > 0)
            style="background-color:{{ $bgcolor }};color: {{ $color }}; font-weight: bold; text-align: center;"
            @endif
            @endif       
            @if(strlen($valor_pla)>0)
            data-toggle="tooltip" data-placement="top" title="Planificado: {{ $valor_pla }}"
            @endif                                                                   
            ><br/>
            <a href="#" onclick="viewObs({{ $i }},21,4,'{{ $obs_planificada }}','{{ $obs_aprobada }}')" id="obs_{{ $i }}_4" 
            @if(strlen($valor)<=0)
            style="display:none"
            @else
            @if($valor <=0)
            style="display:none"                        
            @endif
            @endif                          
            >
            @if(($obs != '') or ($obs_planificada != '') or ($obs_observada != ''))
            <i class="glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @else
            <i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @endif                         
          </a>
          </div>
        </center>
      </th>
      <?php /* Prepara cuadro de texto para el input dia 5 */
      $fec->addDay();
      $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
      $valor= '';
      $valor_pla="";
      $estado='';
      $cproyectoejecucion='';
      $obs='';
      $obs_planificada="";
      $obs_aprobada="";
      $obs_observada="";
      $obs_ejecutada="";
      $color="";
      $bgcolor="";
      $disabled=false;     
      $cestructuraproyecto="";                   
      if (isset($eje[$key])==1){
        $estado= (isset($eje[$key]['estado'])==1?$eje[$key]['estado']:'');
        $cproyectoejecucion = (isset($eje[$key]['cproyectoejecucion'])==1?$eje[$key]['cproyectoejecucion']:'');
        $cestructuraproyecto = (isset($eje[$key]['cestructuraproyecto'])==1?$eje[$key]['cestructuraproyecto']:'');
        $valor_pla = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
        $obs_planificada=(isset($eje[$key]['obsplanificadas'])==1?$eje[$key]['obsplanificadas']:'');   
        $obs_aprobada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_observada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_ejecutada=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');                       
        switch ($eje[$key]['estado']){
          case '1':
          $valor = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');

          $color="black";
          $bgcolor="#FFBF00";
          break;
          case '2':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;                            
          case '3':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#0069AA";
          break;
          case '4':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;
          case '5':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');

          $color="black";
          $bgcolor="#11B15B";
          $disabled=true;
          break;
        }
        if(strlen($valor)>0){
          $totl+=$valor;
          $tot_5+=$valor;
        }          

      }

      ?>
      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
        <center class="div-celda">
          <div>
            <input type='hidden' name='ejecu[{{ $j }}][24]' value="{{ $cproyectoejecucion }}" />
            <input type='hidden' name='ejecu[{{ $j }}][25]' value="{{ $estado }}" />
            <input type='hidden' name='ejecu[{{ $j }}][26]' value="{{ $obs }}" id="txt_{{ $j }}_26"/>
            <input type='hidden' name='ejecu[{{ $j }}][27]' value="{{ $key }}" />
            <input type='hidden' name='ejecu[{{ $j }}][43]' value="{{ $cestructuraproyecto }}" />
            <input type='text' size='2' maxlength="5" name='ejecu[{{ $j }}][28]' value='{{ $valor }}' 

            onchange="disabledEnviar();viewSobre('{{ $i }}_5',this,'txt_{{ $j }}_26');sumHoras({{ $j }})"
            onkeypress="return soloNumeros(event,this);"
            @if(!($estado==1 || $estado==2 ||  $estado==4 || strlen($estado)<=0))
            @if(strlen($valor) > 0) 
            readonly='true'
            @else
                                      
            @endif
            @else
                                    
            @endif    
            @if(strlen($bgcolor)>0)
            @if(strlen($valor) > 0)
            style="background-color:{{ $bgcolor }};color: {{ $color }}; font-weight: bold; text-align: center;"
            @endif
            @endif      
            @if(strlen($valor_pla)>0)
            data-toggle="tooltip" data-placement="top" title="Planificado: {{ $valor_pla }}"
            @endif                                                                    
            > <br/>
            <a href="#" onclick="viewObs({{ $i }},26,5,'{{ $obs_planificada }}','{{ $obs_aprobada }}')" id="obs_{{ $i }}_5" 
            @if(strlen($valor)<=0)
            style="display:none"
            @else
            @if($valor <=0)
            style="display:none"                        
            @endif
            @endif                        
            >
            @if(($obs != '') or ($obs_planificada != '') or ($obs_observada != ''))
            <i class="glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @else
            <i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @endif                       
          </a>
          </div>
        </center>
      </th>
      <?php /* Prepara cuadro de texto para el input dia 6 */
      $fec->addDay();
      $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
      $valor= '';
      $valor_pla="";
      $estado='';
      $cproyectoejecucion='';
      $obs='';
      $obs_planificada="";
      $obs_aprobada="";
      $obs_observada="";
      $obs_ejecutada="";
      $color="";
      $bgcolor="";                        
      $disabled=false;         
      $cestructuraproyecto="";               
      if (isset($eje[$key])==1){
        $estado= (isset($eje[$key]['estado'])==1?$eje[$key]['estado']:'');
        $cproyectoejecucion = (isset($eje[$key]['cproyectoejecucion'])==1?$eje[$key]['cproyectoejecucion']:'');
        $cestructuraproyecto = (isset($eje[$key]['cestructuraproyecto'])==1?$eje[$key]['cestructuraproyecto']:'');
        $valor_pla = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
        $obs_planificada=(isset($eje[$key]['obsplanificadas'])==1?$eje[$key]['obsplanificadas']:'');
        $obs_aprobada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_observada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_ejecutada=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');

        switch ($eje[$key]['estado']){
          case '1':
          $valor = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');

          $color="black";
          $bgcolor="#FFBF00";
          break;
          case '2':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;                            
          case '3':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#0069AA";
          break;
          case '4':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;
          case '5':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');

          $color="black";
          $bgcolor="#11B15B";
          $disabled=true;
          break;
        }
        if(strlen($valor)>0){
          $totl+=$valor;
          $tot_6+=$valor;
        }        
      }

      ?>   
      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
        <center class="div-celda">
          <div>
            <input type='hidden' name='ejecu[{{ $j }}][29]' value="{{ $cproyectoejecucion }}" />
            <input type='hidden' name='ejecu[{{ $j }}][30]' value="{{ $estado }}" />
            <input type='hidden' name='ejecu[{{ $j }}][31]' value="{{ $obs }}" id="txt_{{ $j }}_31" />
            <input type='hidden' name='ejecu[{{ $j }}][32]' value="{{ $key }}" />
            <input type='hidden' name='ejecu[{{ $j }}][44]' value="{{ $cestructuraproyecto }}" />
            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][33]' value='{{ $valor }}' 

            onchange="disabledEnviar();viewSobre('{{ $i }}_6',this,'txt_{{ $j }}_31');sumHoras({{ $j }})"
            onkeypress="return soloNumeros(event,this);"
            @if(!($estado==1 || $estado==2 ||  $estado==4 || strlen($estado)<=0))
            @if(strlen($valor) > 0)
            readonly='true'
            @else
                                      
            @endif
            @else
            
            @endif         
            @if(strlen($bgcolor)>0)
            @if(strlen($valor) > 0)
            style="background-color:{{ $bgcolor }};color: {{ $color }}; font-weight: bold; text-align: center;"
            @endif
            @endif        
            @if(strlen($valor_pla)>0)
            data-toggle="tooltip" data-placement="top" title="Planificado: {{ $valor_pla }}"
            @endif                                                             
            ><br/>
            <a href="#" onclick="viewObs({{ $i }},31,6,'{{ $obs_planificada }}','{{ $obs_aprobada }}')" id="obs_{{ $i }}_6" 
            @if(strlen($valor)<=0)
            style="display:none"
            @else
            @if($valor <=0)
            style="display:none"                        
            @endif
            @endif                        
            >
            @if(($obs != '') or ($obs_planificada != '') or ($obs_observada != ''))
            <i class="glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @else
            <i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @endif                
          </a>
          </div>
        </center>
      </th>
      <?php /* Prepara cuadro de texto para el input dia 7 */
      $fec->addDay();
      $key=str_pad($fec->year,4,'0',STR_PAD_LEFT)."".str_pad($fec->month,2,'0',STR_PAD_LEFT)."".str_pad($fec->day,2,'0',STR_PAD_LEFT);
      $valor= '';
      $valor_pla="";
      $estado='';
      $cproyectoejecucion='';
      $obs='';
      $obs_planificada="";
      $obs_aprobada="";
      $obs_observada="";
      $obs_ejecutada="";
      $color="";
      $bgcolor="";
      $disabled=false;    
      $cestructuraproyecto="";                    
      if (isset($eje[$key])==1){
        $estado= (isset($eje[$key]['estado'])==1?$eje[$key]['estado']:'');
        $cproyectoejecucion = (isset($eje[$key]['cproyectoejecucion'])==1?$eje[$key]['cproyectoejecucion']:'');
        $cestructuraproyecto = (isset($eje[$key]['cestructuraproyecto'])==1?$eje[$key]['cestructuraproyecto']:'');
        $valor_pla = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');
        $obs_planificada=(isset($eje[$key]['obsplanificadas'])==1?$eje[$key]['obsplanificadas']:'');   
        $obs_aprobada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_observada=(isset($eje[$key]['obsaprobadas'])==1?$eje[$key]['obsaprobadas']:'');
        $obs_ejecutada=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');                       
        switch ($eje[$key]['estado']){
          case '1':
          $valor = (isset($eje[$key]['horasplanificadas'])==1?$eje[$key]['horasplanificadas']:'');

          $color="black";
          $bgcolor="#FFBF00";
          break;
          case '2':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;                            
          case '3':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#0069AA";
          break;
          case '4':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');
          $obs=(isset($eje[$key]['obsejecutadas'])==1?$eje[$key]['obsejecutadas']:'');
          $color="white";
          $bgcolor="#CD201A";
          break;
          case '5':
          $valor = (isset($eje[$key]['horasejecutadas'])==1?$eje[$key]['horasejecutadas']:'');

          $color="black";
          $bgcolor="#11B15B";
          $disabled=true;
          break;
        }
        if(strlen($valor)>0){
          $totl+=$valor;
          $tot_7+=$valor;
        }   

      }

      ?>
      <th style="width: 60px; padding:0; border:1px solid #DCDCDCFF;">
        <center class="div-celda">
          <div>
            <input type='hidden' name='ejecu[{{ $j }}][34]' value="{{ $cproyectoejecucion }}" />
            <input type='hidden' name='ejecu[{{ $j }}][35]' value="{{ $estado }}" />
            <input type='hidden' name='ejecu[{{ $j }}][36]' value="{{ $obs }}" id="txt_{{ $j }}_36" />
            <input type='hidden' name='ejecu[{{ $j }}][37]' value="{{ $key }}" />
            <input type='hidden' name='ejecu[{{ $j }}][45]' value="{{ $cestructuraproyecto }}" />
            <input type='text'  size='2' maxlength="5" name='ejecu[{{ $j }}][38]' value='{{ $valor }}' 
            
            onchange="disabledEnviar();viewSobre('{{ $i }}_7',this,'txt_{{ $j }}_36');sumHoras({{ $j }})" 
            onkeypress="return soloNumeros(event,this);"
            @if(!($estado==1 || $estado==2 ||  $estado==4 || strlen($estado)<=0))
            @if(strlen($valor) > 0)
            readonly='true'
            @else
                                      
            @endif
            @else
                                    
            @endif
            @if(strlen($bgcolor)>0)
            @if(strlen($valor) > 0)
            style="background-color:{{ $bgcolor }};color: {{ $color }}; font-weight: bold; text-align: center;"
            @endif
            @endif   
            @if(strlen($valor_pla)>0)
            data-toggle="tooltip" data-placement="top" title="Planificado: {{ $valor_pla }}"
            @endif                       
            ><br/>
            <a href="#" onclick="viewObs({{ $i }},36,7,'{{ $obs_planificada }}','{{ $obs_aprobada }}')" id="obs_{{ $i }}_7" 
            @if(strlen($valor)<=0)
            style="display:none"
            @else
            @if($valor <=0)
            style="display:none"                        
            @endif
            @endif                        
            >
            @if(($obs != '') or ($obs_planificada != '') or ($obs_observada != ''))
            <i class="glyphicon glyphicon-comment" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @else
            <i class="fa fa-envelope-o" aria-hidden="true" id="iobs_{{ $i }}_1"></i>
            @endif                       
          </a>
          </div>
        </center>
      </th>
      <th style="width: 60px; border:1px solid #DCDCDCFF; font-size:11px;">
        <div id="totf_{{ $j }}" class="div-celda" style="width: 100%;">
          <center>
            {{ $totl }}
          </center>
        </div>
      </th>
      <!--<td class="clsAnchoTabla" style="min-width:150px !important">
        <div class="progress" style="height:16px" >
          <div class="progress-bar progress-bar-striped progress-bar-striped progress-bar-animated active"  role="progressbar" aria-valuenow="{{ $eje['porav'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $eje['porav'] }}%;font-size:10px;padding:1px; background-color: @if($eje['porav']<60) green @elseif($eje['porav']<80) orange @else red @endif">
         
            <span style="display: block; position: absolute; width: 130px; color: black;" data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en horas">{{ $eje['mensajehoras'] }}</span>
                                 

          </div>
        </div>       
               
        <div class="progress " style="height:16px" >
          <div class="progress-bar progress-bar-warning progress-bar-striped active"  role="progressbar" aria-valuenow="{{ $eje['porav_costo'] }}" aria-valuemin="0" aria-valuemax="100" style="width:{{ $eje['porav_costo'] }}%;font-size:10px;padding:1px; background-color: @if($eje['porav_costo']<60) green @elseif($eje['porav_costo']<80) orange @else red @endif">
          
            <span style="display: block; position: absolute; width: 130px; color: black; " data-toggle="tooltip" data-placement="left" title="Porcentaje de avance en monto">{{ $eje['mensajecosto'] }}</span>
                             

          </div>
        </div>                          
      </td>-->
    </tr>
    @endforeach
  </tbody>
  @endif
  <thead class="clsCabereraTabla" style="font-size:11px;">
    <tr>
      <th style="width: 5px;"><center>#</center></th>
      <th style="width: 870px;" ><center>Código/ Cliente / Unidad Minera / Actividad : tarea</center></th>
      <th style="width: 150;">Categoría Proy</th>
      <th style="width: 5px;"><center>Tipo</center></th>
      <th style="width: 175px;"><center>Categoría</center></th>
      <th style="width: 60px;"><center>GP</center></th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Lu<br>{{ isset($fecha1)==1?$fecha1->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Ma<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Mi<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Ju<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Vi<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Sa<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Do<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px;">
        <div class="div-celda">
          <center class="div-celda">Total</center>
        </div>
      </th>
    </tr>
    <tr>
      <th colspan="6" style="width: 1112px; border:1px solid #DCDCDCFF;"><center>Total de horas</center></th>
      <th style="width: 57px;">
        <div id="tot_1" class="div-celda" style="width: 100%;" @if($tot_1 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_1 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_2" class="div-celda" style="width: 100%;" @if($tot_2 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_2 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_3" class="div-celda" style="width: 100%;" @if($tot_3 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_3 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_4" class="div-celda" style="width: 100%;" @if($tot_4 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_4 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_5" class="div-celda" style="width: 100%;" @if($tot_5 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_5 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_6" class="div-celda" style="width: 100%;" @if($tot_6 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_6 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_7" class="div-celda" style="width: 100%;" @if($tot_7 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_7 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;"><center>{{ $tot_1 + $tot_2 + $tot_3 + $tot_4 + $tot_5 + $tot_6 + $tot_7 }}</center></th>
    </tr>
  </thead>
  <tfoot class="clsCabereraTabla" style="font-size:11px;">
    <tr>
      <th colspan="6" style="width: 1112px; border:1px solid #DCDCDCFF;"><center>Total de horas</center></th>
      <th style="width: 57px;">
        <div id="tot_1p" class="div-celda" style="width: 100%;" @if($tot_1 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_1 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_2p" class="div-celda" style="width: 100%;" @if($tot_2 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_2 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_3p" class="div-celda" style="width: 100%;" @if($tot_3 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_3 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_4p" class="div-celda" style="width: 100%;" @if($tot_4 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_4 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_5p" class="div-celda" style="width: 100%;" @if($tot_5 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_5 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_6p" class="div-celda" style="width: 100%;" @if($tot_6 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_6 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;">
        <div id="tot_7p" class="div-celda" style="width: 100%;" @if($tot_7 > 16) style="border: 1px solid red" @endif>
          <center>
            {{ $tot_7 }}
          </center>
        </div>
      </th>
      <th style="width: 57px;"><center>{{ $tot_1 + $tot_2 + $tot_3 + $tot_4 + $tot_5 + $tot_6 + $tot_7 }}</center></th>

      <?php $totalsem=$tot_1 + $tot_2 + $tot_3 + $tot_4 + $tot_5 + $tot_6 + $tot_7; 
     // dd($totalsem);?>

      <input type="hidden" name="totalsemana" id="totalsemana" value="{{$totalsem}}" />


    </tr>
    <tr>
      <th style="width: 5px;"><center>#</center></th>
      <th style="width: 870px;" ><center>Código/ Cliente / Unidad Minera / Actividad : tarea</center></th>
      <th style="width: 150px;">Categoría Proy</th>
      <th style="width: 5px;"><center>Tipo</center></th>
      <th style="width: 175px;"><center>Categoría</center></th>
      <th style="width: 60px;"><center>GP</center></th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Lu<br>{{ isset($fecha1)==1?$fecha1->subDay(6)->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Ma<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Mi<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Ju<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Vi<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Sa<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px; padding:0;"><center class="div-celda">Do<br>{{ isset($fecha1)==1?$fecha1->addDay()->day:'' }}</th>
      <th style="width: 60px;">
        <div class="div-celda">
          <center class="div-celda">Total</center>
        </div>
      </th>
    </tr>
  </tfoot>
</table>

<script>
     
</script>


