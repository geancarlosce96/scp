<div id="divUmineralogo" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
          <!-- Modal content-->
          <div class="modal-content">
            <div class="modal-header" style="background-color: #0069AA">
              <button type="button" class="close" data-dismiss="modal">&times;</button>
              <h4 class="modal-title" style="color:white">Registro el Logo de la Unidad Minera</h4>
            </div>
            <div class="modal-body">
              <div class="col-md-12 col-lg-12 text-center">
                
                <img id="imagenlogo" alt="" style="width: 250px;height: 100px;">
              </div>

              {!! Form::open(array('url' => 'egregarlogounidadminera','method' => 'POST','id' =>'frmcuminera', 'enctype' => 'multipart/form-data')) !!}
              {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_Cuminera')) !!}
              
              {!! Form::hidden('cpersona_u_1',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_u_1')) !!}            
              {!! Form::hidden('cuminera_u_1','',array('id'=>'cuminera_u_1')) !!}
              <div class="col-lg-4 col-xs-4">
                <div class="row clsPadding2">
                    
                    <div class="form-group">
                        <label for="Ruta Archivo">Logo de la Cliente</label>
                        <input id="file" name="file" type="file" class="file" multiple="true" enctype="multipart/form-data">
                    </div>
                    <div class="form-group">
                        <button class="btn btn-primary">Cargar Archivo</button>
                        <button class="btn btn-danger" type="reset">Reset</button>
                    </div>
                </div>
              </div>
              {!! Form::close() !!}   
            </div>
            <div class="modal-footer">
              
            </div>
          </div>
        </div>
      </div>