<!-- Modal agregar Actividades-->
    <div id="addActi" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Actividades</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarActividad','method' => 'POST','id' =>'frmactividad','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cactividad',(isset($actividad->cactividad)==1?$actividad->cactividad:''),array('id'=>'cactividad')) !!}

                   
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        
          <div class="form-group">
            <label for="cactividad" class="col-sm-2 control-label">ID</label>
              <div class="col-sm-8">
              {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cactividad',(isset($actividad->cactividad)==1?$actividad->cactividad:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cactividad') ) !!}     
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div> 


           <div class="form-group">
            <label for="codigo" class="col-sm-2 control-label">Código</label>
              <div class="col-sm-8">
              <input type="text" class="form-control" id="codigo" placeholder="Código" name="codigo" value="{{(isset($codigo->codigo)==1?$codigo->codigo:'')}}">      
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
              <div class="col-sm-8">
              <input type="text" class="form-control" id="descripcion" placeholder="Descripción" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">  
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
            <label for="cactividad_parent" class="col-sm-2 control-label">Actividad Padre</label>
              <div class="col-sm-8">
                  {!! Form::select('cactividad_parent',(isset($actividad_parent)==1?$actividad_parent:null),(isset($actividad->cactividad_parent)==1?$actividad->cactividad_parent:''),array('class' => 'form-control select-box','id'=>'cactividad_parent')) !!}   
             
            </div>

          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Actividades-->
