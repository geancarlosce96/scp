<table class="table table-striped">
      	<thead>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">actividad</td>
                <td class="clsAnchoTabla">Hrs/Item</td>
                <td class="clsAnchoTabla">Hrs Plan</td>
                <td class="clsAnchoTabla">Entregable</td>
                <td class="clsAnchoTabla">Hrs Plan</td>
                <td class="clsAnchoTabla">Colaborador</td> 
                <td class="clsAnchoTabla">L 01<br>(P)</td>
                <td class="clsAnchoTabla">L 01<br>(E)</td>
                <td class="clsAnchoTabla">M 02<br>(P)</td>
                <td class="clsAnchoTabla">M 02<br>(E)</td>
                <td class="clsAnchoTabla">MI 03<br>(P)</td>
                <td class="clsAnchoTabla">MI 03<br>(E)</td>
                <td class="clsAnchoTabla">J 04<br>(P)</td>
                <td class="clsAnchoTabla">J 04<br>(E)</td>
                <td class="clsAnchoTabla">V 05<br>(P)</td>
                <td class="clsAnchoTabla">V 05<br>(E)</td>                              
            </tr>
        </thead>

      	<tbody>
            @if(isset($contejec)==1)
            @foreach($contejec as $cont)

        <tr>
            <td class="clsAnchoTabla">{{ $cont['item'] }}</td>
            <td class="clsAnchoTabla">{{ $cont['actividad'] }}</td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla">Ent1/Ent2</td>
            <td class="clsAnchoTabla">Juan Castañon</td>
            <td class="clsAnchoTabla">10</td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla">10</td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla">10</td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla">10</td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>                        
        </tr>                     
                
            @endforeach
            @endif        
        </tbody>
      </table>