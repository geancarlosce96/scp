<table class="table table-striped">
      <thead>
      <tr class="clsCabereraTabla">
        <th>Categoría</th>
        <th>Profesional</th>
        <th>Rate</th>
        <th>Acción</th>
      </tr>
      </thead>
      <tbody>
      @if(isset($participantes))
        @foreach($participantes as $est)
          <tr>
            <td style="padding:1px;">{{ $est['descripcionrol'] }}</td>
            <td style="padding:1px;">{{ $est['nombre'] }}</td>
            <td style="padding:1px;">{{ $est['rate'] }}</td>
            <td style="padding:1px;">
            <!--<a href="#" class="fa fa-pencil"></a>-->
            <button type='button'class="fa fa-trash" onclick="fdelEstru('{{ $est['cestructurapropuesta'] }}')"></button>
            <!--<a href="#" class="fa fa-save"></a>-->
            </td>
          </tr>
        @endforeach
      @endif
      </tbody>
</table>