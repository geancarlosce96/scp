 <div class="col-lg-1 col-xs-1">Propuesta:</div>
                    <div class="col-lg-4 col-xs-4">
                        <div class="input-group input-group-normal">
                        <input type="text" class="form-control input-sm" id="tpropuesta" placeholder="" name="tpropuesta" value="{{ isset($tpropuesta->nombre)==1?$tpropuesta->nombre:'' }}" disabled> 
                          {!! Form::hidden('cpropuesta',(isset($tpropuesta->cpropuesta)==1?$tpropuesta->cpropuesta:''),array('id'=>'cpropuesta') )  !!}
                          {!! Form::hidden('cservicioprop',(isset($tpropuesta->cservicio)==1?$tpropuesta->cservicio:''),array('id'=>'cservicioprop') )  !!}
                          {!! Form::hidden('codpropuesta',(isset($tpropuesta->ccodigo)==1?$tpropuesta->ccodigo:''),array('id'=>'codpropuesta') )  !!}
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target='#searchPropuesta'><b>...</b></button>
                            </span>
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-primary btn-sm" onclick="cleanPropuesta()"><b>X</b></button>
                            </span>                            
                        </div>  
                    </div> 
                    
                    <div class="col-lg-1 col-xs-1">Unidad Minera:</div>
                    <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="nombreUMineraProp" name="nombreUMineraProp" value="{{ (isset($tumineraprop->nombre)==1?$tumineraprop->nombre:'' ) }}" disabled></div>
                    <div class="col-lg-1 col-xs-12 clsPadding">Cliente:</div>
                    <div class="col-lg-2 col-xs-12 clsPadding"><input class="form-control input-sm" type="text" id="nombreClienteProp" name="nombreClienteProp" value="{{ (isset($personaprop->nombre)==1?$personaprop->nombre:'' ) }}" disabled></div>