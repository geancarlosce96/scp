<div class="modal fade" id="modallinkLE" tabindex="-1" role="dialog" aria-hidden="true" >
  <div class="modal-dialog modal-sm" role="document" style="width: 60%">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span style="color: white;" aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" style="color: white">Ruta de los entregables seleccionados</h4>
      </div>
      <div class="modal-body">
        
          <input type="text" name="" value="" placeholder="Ingrese ruta" class="form-control input-lg" id="rutatodos"  style="border-radius: 5px;">
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="btnlink">Guardar</button>
        <button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>


