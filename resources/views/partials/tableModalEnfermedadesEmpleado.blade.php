<table class="table table-striped">
                                   <thead>
                                    <tr>
                                        <td class="clsCabereraTabla">Descripción</td>
                                        <td class="clsCabereraTabla">Observación</td>
                                        <td class="clsCabereraTabla">Acción</td>
                                      </tr>  
                                     </thead>
                                     <tbody>
                                    @if(isset($tpersonadatosmedicosenfermedad)==1)
                        			     @foreach($tpersonadatosmedicosenfermedad as $tpenf)
                                     	<tr>
                                            <td class="clsAnchoTablaMax">{{ $tpenf->descripcion }}</td>
                                            <td class="clsAnchoTablaMax">{{ $tpenf->observacion }}</td>
                                                <td class="clsAnchoDia">
                                                    <a href="#" onclick="eliminarEmpleadoEnfer('{{ $tpenf->cpersonaenfermedad }}')" data-toggle="tooltip" data-container="body" title="Eliminar Enfermedad">
                                                    <i class="fa fa-trash" aria-hidden="true"></i></a>
                                                </td>
                                        </tr>
                                    @endforeach    
                        			      @endif 
                                    </tbody>
</table>