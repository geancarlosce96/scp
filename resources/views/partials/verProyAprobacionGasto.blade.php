<div id="bproye">
    <div class="col-lg-2 col-xs-2">Proyecto:</div>
    <div class="col-lg-4 col-xs-9">
      <div class="input-group input-group-normal">
            <input type="text" class="form-control input-sm" id="tproyecto" placeholder="" name="tproyecto" value="{{ isset($tproyecto->nombre)==1?$tproyecto->nombre:'' }}" disabled> 
            {!! Form::hidden('cproyecto',(isset($tproyecto->cproyecto)==1?$tproyecto->cproyecto:''),array('id'=>'cproyecto'))  !!}
            <span class="input-group-btn">
              <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target='#searchProyecto'><b>...</b></button>
            </span>
            <span class="input-group-btn">
              <button type="button" class="btn btn-primary btn-sm " onclick="cleanProyecto()"><b>X</b></button>
            </span>                            
      </div>  
    </div>
</div>