<table class="table table-striped">
      	<thead>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">Usuario</td>
                <td class="clsAnchoTabla">Descripción</td>
                <td class="clsAnchoTabla">Actividad</td>
                <td class="clsAnchoTabla">¿SOC?</td>
                <td class="clsAnchoTabla">Fecha Planificado</td>
                <td class="clsAnchoTabla">Fecha Real</td> 
                <td class="clsAnchoTabla">Observaciones</td> 
                <td class="clsAnchoTabla">Acciones</td>                             
            </tr>
        </thead>

      	<tbody>
            @if(isset($regdesv)==1)
            @foreach($regdesv as $reg)
        <tr>
            <td class="clsAnchoTabla">{{ $reg['item'] }}</td>
            <td class="clsAnchoTabla">{{ $reg['usuario'] }}</td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla">Actividad 1</td>
            <td class="clsAnchoTabla"><input type="checkbox"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla">
            	<a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Registro">
                <i class="fa fa-trash-o" aria-hidden="true"></i></a></td>                          
        </tr>
                              
        @endforeach
        @endif          
               
        </tbody>
      </table>    