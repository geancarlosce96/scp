

<table class="table table-striped table-hover table-bordered table-responsive" style="font-size: 12px; width:100%;" id="listatablepry">
  
  <thead>
    <tr class="clsCabereraTabla">
      <th width="5%">Ítem</th>
      <th width="10%">Código</th>
      <th width="40%">Nombre</th>
      <th width="30%">Cliente</th>
      <th width="10%">Unidad Minera</th>
      <th width="5%">Acciones</th>
    </tr>
  </thead>
  <tbody>
    @foreach ($tproyectos as $key => $value)
    <tr class="equipoproyecto" data-proyecto="{{ $value->cproyecto }}" data-finicio="{{ $value->finicio }}" data-fcierre="{{ $value->fcierre }}">
      <td width="5%">{{ $key + 1 }}</td>
      <td width="10%">{{ $value->codigo }}</td>
      <td width="40%">{{ $value->nombre }}</td>
      <td width="30%">{{ $value->cliente }}</td>
      <td width="10%">{{ $value->uminera }}</td>
      <td width="5%">
        <center>
          <a data-toggle="tooltip" data-placement="left" title="Ver Equipo" class="btn verequipoproyecto" ><i class="fa fa-eye"></i></a>
          <a data-toggle="tooltip" data-placement="left" title="Ver Curva S desde {{ $value->finicio }} al {{ $value->fcierre }}" class="btn vercurva" ><i class="fa fa-line-chart"></i></a>
        </center>
      </td>
    </tr>
    @endforeach
  </tbody>
</table>



