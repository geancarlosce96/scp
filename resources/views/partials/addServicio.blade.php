<!-- Modal agregar Usuario-->
    <div id="addServ" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Usuario</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarServicio','method' => 'POST','id' =>'frmservicio','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cservicio',(isset($servicio->cservicio)==1?$servicio->cservicio:''),array('id'=>'cservicio')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="cservicio" class="col-sm-2 control-label">Servicio</label>
                <div class="col-sm-8">                       
              {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cservicio',(isset($servicio->cservicio)==1?$servicio->cservicio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cservicio') ) !!}    
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      {!! Form::text('descripcion',(isset($descripcion->descripcion)==1?$descripcion->descripcion:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'descripcion') ) !!}   
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddServ" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>




</script>  