                <input type="hidden" name="cproyectoactividades" value="{{ $cproyectoactividades }}" />
                <p>{{ $descripcion }} </p>
                <div class="row">
                    <div class="col-md-8 label-primary">
                        Profesional
                    </div> <!-- col-md-8  de proyectos-->
                    <div class="col-md-2 label-primary text-center">
                        FF
                    </div>
                    <div class="col-md-2 label-primary text-center">
                        NF
                    </div>                    
                </div> <!-- div row -->                
                @if(isset($personal)==1)
                @foreach($personal as $per)
                <div class="row">
                    <div class="col-md-8">
                        {{ $per['nombre'] }}
                    </div> <!-- col-md-8  de proyectos-->
                    <div class="col-md-2 text-center">
                        <input type="checkbox" name="chk_fact[]" value="{{ $per['cpersona'] }}"
                        @if($per['sele_fact']=='1')
                            disabled
                        @endif
                         />
                    </div>
                    <div class="col-md-2 text-center">
                        <input type="checkbox" name="chk_nfact[]" value="{{ $per['cpersona'] }}" 
                        @if($per['sele_nfact']=='1')
                            disabled
                        @endif                        
                        />
                    </div>              
                </div> <!-- div row -->
                @endforeach
                @endif
