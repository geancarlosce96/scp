<!-- Modal -->
{{-- 			<div class="modal fade" id="editaractividad_{{ $act['cproyectoactividades'] }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
			  <div class="modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header" style="background-color: #0069AA;color: white;">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			        <h4 class="modal-title" id="myModalLabel">Editar Actividad</h4>
			      </div>
			      <div class="modal-body">
			        {!! Form::open(array('url' => 'editarItemActividadesProyecto','method' => 'POST','id' =>'frmActividadProyectoedit', 'class' => 'form-group')) !!}
		            <input type="hidden" name="cproyectoactividades" value="{{ $act['cproyectoactividades'] }}">
		            <input type="hidden" name="cproyecto" value="{{ $proyecto->cproyecto}}">
		            {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
					  <div class="form-group col-xs-12 col-sm-12 col-md-12 col-lg-12">
					  	<div class="form-group">
					    <label for="item">Item</label>
					    
					    	<input type="text" class="form-control" name="item" id="item" value="{{ $act['codigoactividad'] }}">	
					    </div>
					    
					  </div>
					  <div class="form-group">
					    <label for="decripcionactividad">Descripcion Actividad</label>
					    <input type="text" class="form-control" name="decripcionactividad" id="decripcionactividad" value="{{ $act['descripcionactividad'] }}">
					  </div>
					  <button type="submit" class="btn btn-primary" >Editar</button>
					</form>
					{!! Form::close() !!}
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			  </div>
			</div> --}}

			<div class="modal fade" id="myModalnewactividad" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header" style="background-color: #0069AA;color: white;">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Creacion de nueva actividad</h4>
          </div>
          <div class="modal-body">
            {!! Form::open(array('url' => 'grabarActividadesProyecto','method' => 'POST','id' =>'frmActividadProyecto')) !!}
            {!! Form::hidden('cproyecto',(isset($proyecto)==1?$proyecto->cproyecto:''),array('id'=>'cproyecto')) !!}
            {!! Form::hidden('cproyectoactividades',(isset($listaAct)==1?$listaAct['cproyectoactividades']:''),array('id'=>'cproyectoactividades')) !!}
            {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
              <div class="form-group">
                <label for="itemactividadcreate">Item</label>
                <input type="text" class="form-control" name="itemactividadcreate" id="itemactividadcreate" placeholder="1.00">
              </div>
              <div class="form-group">
                <label for="nombreactividad">Nombre de la actividad</label>
                <input type="text" class="form-control" id="nombreactividad" name="nombreactividad" placeholder="Nombre Actividad">
              </div>
              
              <div class="form-group asoactividad" id="asociaactividad" style="display:none;">
                <label for="padreactividad">Asociar actividad</label>
                <select name="padreactividad">
                  <option value="">Seleccione una opcion</option>
                  @foreach($actividadPadreTotal as $key => $padreact)
                    <option value="{{ $padreact->cproyectoactividades }}">{{ $padreact->codigoactvidad }} - {{ $padreact->descripcionactividad }}
                    </option>
                  @endforeach
                </select> 
                <p class="help-block">Seleccione una actividad a la cual asociar dicha nueva actividad</p>
              </div>

              <div class="checkbox">
                <label>
                  <input type="checkbox" onclick="toggle('.asoactividad', this)"> Asociar Actividad
                </label>
              </div>
              
            </div>
            <button type="submit" class="btn btn-primary" onclick="CrearActividadProyecto();">Crear</button>  
            {!! Form::close() !!}
            
            
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            
          </div>
          
        </div>
      </div>
    </div>