<table class="table table-hover table-responsive datatable-order-search" id="tableAprobar" style="font-size: 12px;">
    <thead>
    <tr class="clsCabereraTabla">
      <th><center>Ítem</center></th>
      <th><center>Semana - Periodo</center></th>
      <!-- <th>Condición</th> -->
      <!--<th>Proyecto</th>-->
      <th><center>Profesional</center></th>
      <th><center>Facturable</center></th>
      <th><center>No facturable</center></th>
      <th><center>Anddes</center></th>
      <th><center>Total</center></th>
      <!--<th>Ciclo de Aprobación</th>-->
      <th><center><input type="checkbox" name="chkGen" id="chekGen" onclick="setCheck(this);" /></center></th>
    </tr>
    </thead>
    @if(isset($listAprobar))


   
    <tbody>
    @foreach($listAprobar as $ap)

    <?php //dd(date("d/m/Y", strtotime($fec_fin)));
    
     ?>
    
    <tr class="gradeX">
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['item'] }}</center></td>
      <td style="border:1px solid #DCDCDCFF;"><center>

      @if(isset($semanatodas))

      <?php //dd($listAprobar); ?>

        <button name="viewH" type="button" class="btn btn-primary" onclick="getHoras( {{ $ap['semana'] }},{{ $ap['anio'] }},{{ $ap['cpersona'] }})">
        {{ $ap['semana'] }} al {{ $ap['semana'] }} del {{ $ap['anio'] }}</button>

      
      
      @else

        <button name="viewH" type="button" class="btn btn-primary" onclick="verHoras( {{$semana_ini}},{{ $semana_fin }},{{ $ap['anio'] }},{{ $ap['cpersona'] }} )">
        {{$semana_ini}} al {{ $semana_fin }} del {{ $ap['anio'] }}</button>

      @endif

      </center>
      </td>
      <!-- <td> </td> -->
      <td style="border:1px solid #DCDCDCFF;">{{ $ap['nomper'] }}</td>
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['nrofact'] }}</center></td>
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['nronfact'] }}</center></td>
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['nroand'] }}</center></td>
      <td style="border:1px solid #DCDCDCFF;"><center>{{ $ap['total'] }}</center></td>
      <!--<td style="min-width:50px;">&nbsp;</td>    -->       
      
      <td style="border:1px solid #DCDCDCFF;"><center>

      @if(isset($semanatodas))

      <input type="checkbox" name="checkbox_sel[]" value="{{ $ap['semana'] }}_{{ $ap['semana'] }}_{{ $ap['anio'] }}_{{ $ap['cpersona'] }}"></center>

      
      
      @else
            <input type="checkbox" name="checkbox_sel[]" value="{{ $semana_ini }}_{{ $semana_fin }}_{{ $ap['anio'] }}_{{ $ap['cpersona'] }}"></center>

      @endif


      </td>
    </tr>
    @endforeach                                                          
    </tbody>
    @endif
</table>