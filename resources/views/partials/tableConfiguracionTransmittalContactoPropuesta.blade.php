        <table class="table table-striped">
        	<thead>
          	<tr class="clsCabereraTabla">
                <td class="clsAnchoTabla">Apellidos</td>
              	<td class="clsAnchoTabla">Nombre</td>
                  <td class="clsAnchoTabla">Tipo Contacto</td>
                  <td class="clsAnchoTabla">Cargo</td>
                  <td class="clsAnchoTabla">Correo</td>
                  <td class="clsAnchoTabla">Acciones</td>
              </tr>
          </thead>

        <tbody>
        @if(isset($transconfigcontac))
        @foreach($transconfigcontac as $con)
          <tr>
                <td class="clsAnchoTabla">{{ $con['apellidos'] }}</td>
                <td class="clsAnchoTabla">{{ $con['nombres'] }}</td>
                <td class="clsAnchoTabla">{{ $con['contacto'] }}</td>
                <td class="clsAnchoTabla">{{ $con['contacto_cargo'] }} </td>
                <td class="clsAnchoTabla">{{ $con['email'] }}</td>
                <td class="clsAnchoTabla">
              	    <a href="#" onclick="eliminarContacto({{$con['ctransmittalconfiguracioncontacto'] }})" data-toggle="tooltip"  title="Eliminar Contacto">
                  <i class="fa fa-trash-o" aria-hidden="true"></i></a>            
                </td>           
          </tr>
        @endforeach
        @endif
        </tbody>
        </table>  