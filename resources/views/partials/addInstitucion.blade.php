<!-- Modal agregar Institucion-->
    <div id="addInsti" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Institucion</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarInstitucion','method' => 'POST','id' =>'frminsti','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cinstitucion',(isset($institucion->cinstitucion)==1?$institucion->cinstitucion:''),array('id'=>'cinstitucion')) !!}

                   
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        
          <div class="form-group">
            <label for="cinstitucion" class="col-sm-2 control-label">ID</label>
              <div class="col-sm-8">
              {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cinstitucion',(isset($institucion->cinstitucion)==1?$institucion->cinstitucion:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cinstitucion') ) !!}     
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div> 
      

            <div class="form-group">
            <label for="abrevia" class="col-sm-2 control-label">Abreviatura</label>
              <div class="col-sm-8">
              <input type="text" class="form-control" id="abrevia" placeholder="Abreviatura" name="abrevia" value="{{(isset($abrevia->abrevia)==1?$abrevia->abrevia:'')}}">        
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Descripción</label>
              <div class="col-sm-8">
              <input type="text" class="form-control" id="descripcion" placeholder="Descripción" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">        
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
            <label for="tipoinstitucion" class="col-sm-2 control-label">Tipo Institucion</label>
              <div class="col-sm-8">
               {!! Form::select('tipoinstitucion',(isset($tipoInstitucion)==1?$tipoInstitucion:null),(isset($institucion->tipoinstitucion)==1?$institucion->tipoinstitucion:''),array('class' => 'form-control select-box','id'=>'tipoinstitucion')) !!} 
             
            </div>

          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Institucion-->
