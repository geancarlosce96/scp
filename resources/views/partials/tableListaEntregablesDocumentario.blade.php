      <table class="table table-striped">
      	<thead>
        	<tr class="clsCabereraTabla">
            	<!--<td class="clsAnchoDia">#</td>-->
                <td class="clsAnchoTabla">EDT</td>
                <td class="clsAnchoTabla">Tipo</td>
                <td class="clsAnchoTabla">Disciplina</td>
                <td>Código </td>
                <td class="clsAnchoTablaMax">Descripción Entregable</td>
                <td>Rev. Act.</td>                
                <td class="clsAnchoTabla">Fecha A</td> 
                <td class="clsAnchoTabla">Fecha B</td>
                <td class="clsAnchoTabla">Fecha 0</td> 
                <!--<td class="clsAnchoTablaMax">Acción</td> -->
                <td class="clsAnchoTablaMax">Sel</td>
                                                            
            </tr>
        </thead>
        <tbody>
            @if(isset($proyectoentregables))
            <?php $i=0; ?>
            @foreach($proyectoentregables as $pent)
            <?php $i++; ?>
        	<tr>
            	<!--<td class="clsAnchoDia">2</td>-->
                <td class="clsAnchoTabla">{{ $pent['des_edt'] }}</td>
                <td class="clsAnchoTabla">{{ $pent['tipo_entregable'] }}</td>
                <td class="clsAnchoTabla">{{ $pent['disciplina'] }}</td>
                <td class="clsAnchoTabla">
                <input type='hidden' name="ent[{{ $i }}][0]" value="{{ $pent['cproyectoentregables'] }}" />
                <input type='text' class="form-control" name="ent[{{ $i }}][1]" value="{{ $pent['codigo'] }}" /></td>
                <td class="clsAnchoTabla">{{ $pent['descripcion'] }}</td>
                <td class="clsAnchoTabla">
                <input type='text' class="form-control" size="2" maxlength="1" name="ent[{{ $i }}][2]" value="{{ $pent['revision'] }}" />
                </td>
                <td class="clsAnchoTabla">{{ isset($pent['fechaA'])==1?$pent['fechaA']:'' }} </td> 
                <td class="clsAnchoTabla">{{ isset($pent['fechaB'])==1?$pent['fechaB']:'' }} </td>
                <td class="clsAnchoTabla">{{ isset($pent['fecha0'])==1?$pent['fecha0']:'' }} </td> 
                <!--<td class="clsAnchoTabla">
                		<a href="#" data-toggle="tooltip"  title="HIS">
                        	<i class="clsBtnHIS" style="width:100px;">HIS</i></a>&nbsp;&nbsp;&nbsp;
                      
                 </td> -->

                 <td class="clsAnchoTabla"><input type="checkbox" name="sel[]" value="{{ $pent['cproyectoentregables'] }}" > </td>
        	</tr>
        @endforeach
        @endif                                            
        </tbody>

      </table>  