<div id="modalNuevoEntregable" class="modal fade" role="dialog">
  <div class="modal-dialog modal-xl">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 id="tituloNuevoEntregable" class="modal-title" style="color:white"></h4>
      </div>
      <div class="modal-body">
        {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmnuevoentregable')) !!}
        {!! Form::hidden('nivelNuevo','',array('id'=>'nivelNuevo')) !!}

        <div class="row">

          <label class="col-sm-3 control-label" id="descNivel"></label>
          <div class="col-sm-7">
             {!! Form::text('nuevoEntregable','', array('class'=>'form-control input-sm','placeholder' => 'Descripción del entregable','id'=>'nuevoEntregable') ) !!}
          </div>
          
        </div>

        <br>

        <div class="row">

          <label class="col-sm-3 control-label">Disciplina</label>
          <div class="col-sm-7">
            {!! Form::select('disciplinaNuevoEnt',(isset($disciplinaTodos)==1?$disciplinaTodos:array()),'',array('class' => 'form-control','id' => 'disciplinaNuevoEnt')) !!}
          </div>
        </div>

        <br>

        <div class="row">

          <label class="col-sm-3 control-label">Tipo de Documento</label>
          <div class="col-sm-7">
            {!! Form::select('tiposDocumento',(isset($tiposDocumento)==1?$tiposDocumento:array()),'',array('class' => 'form-control','id' => 'tiposDocumento')) !!}
          </div>
        </div>

        <br>

     <!--    <div class="row">

          <label for="cproyectoactividades" class="col-sm-3 control-label">Paquete</label>
          <div class="col-sm-7">
            {!! Form::text('paquete','', array('class'=>'form-control input-sm','placeholder' => 'Paquete','id'=>'paquete') ) !!}
          </div>
        </div>

        <br>

        <div class="row">

          <label for="cproyectoactividades" class="col-sm-3 control-label">Tipo Paquete</label>
          <div class="col-sm-7">
            <label>No</label><input type="radio" name="tipopaquete" value="0" checked>
            &nbsp;&nbsp;&nbsp;
            <label>Si</label> <input type="radio" name="tipopaquete" value="1" >
          </div>
        </div>
        <br>
 -->





       {!! Form::close() !!}  
       <div class="modal-footer">
            <button type="button" class="btn btn-success" id="btnGrabarNuevoEnt"><b>Grabar</b></button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div> 
    </div>
  </div>
</div>
</div>
