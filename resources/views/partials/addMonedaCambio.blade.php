<!-- Modal agregar Alergias-->
    <div id="addMonCambio" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:#0069AA'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" style="color:white">Moneda Cambio</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarMonedaCambio','method' => 'POST','id' =>'frmMonCamb','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cmonedacambio',(isset($monedaCambio->cmonedacambio)==1?$monedaCambio->cmonedacambio:''),array('id'=>'cmonedacambio')) !!}

                   
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
          
           <div class="form-group">
            <label for="cmonedacambio" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                  {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cmonedacambio',(isset($monedaCambio->cmonedacambio)==1?$monedaCambio->cmonedacambio:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cmonedacambio') ) !!}     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
            <label for="nombre" class="col-sm-2 control-label">Moneda</label>
              <div class="col-sm-8">
             {!! Form::select('cmoneda',(isset($moneda)==1?$moneda:null),(isset($monedaCambio->cmoneda)==1?$monedaCambio->cmoneda:''),array('class' => 'form-control select-box','id'=>'cmoneda')) !!} 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div> 
      

            <div class="form-group">
            <label for="descripcion" class="col-sm-2 control-label">Moneda Cambio</label>
              <div class="col-sm-8">
              {!! Form::select('cmonedavalor',(isset($monedaVal)==1?$monedaVal:null),(isset($monedaCambio->cmonedavalor)==1?$monedaCambio->cmonedavalor:''),array('class' => 'form-control select-box','id'=>'cmonedavalor')) !!} 
      
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
              <label for="fhasta" class="col-sm-2 control-label">Fecha</label>
              <div class="col-sm-8">
                    <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                       {!! Form::text('fecha',(isset($monedaCambio->fecha)==1?$monedaCambio->fecha:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'fecha') ) !!}
                    </div>                 
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div> 

            <div class="form-group">
                <label for="valorcompra" class="col-sm-2 control-label">Valor Compra</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="valorcompra" placeholder="Valor Compra" name="valorcompra" value="{{(isset($valorcompra->valorcompra)==1?$valorcompra->valorcompra:'')}}">      
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

            <div class="form-group">
                <label for="valorventa" class="col-sm-2 control-label">Valor Venta</label>
              <div class="col-sm-8">
                <input type="text" class="form-control" id="valorventa" placeholder="Valor Venta" name="valorventa" value="{{(isset($valorventa->valorventa)==1?$valorventa->valorventa:'')}}">      
              </div>
              <div class="col-sm-2">&nbsp;
              </div>
            </div>

          {!! Form::close() !!}
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="grabar()">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal agregar Alergias-->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>


   $('#fecha').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      endDate: "<?php echo date('d/m/Y'); ?>",
  }); 



</script>  