<!-- Modal agregar Usuario-->
    <div id="addFasProy" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Fases Proyecto</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarFaseProyecto','method' => 'POST','id' =>'frmfase','class'=>'form-horizontal')) !!}

          {!! Form::hidden('cfaseproyecto',(isset($fase->cfaseproyecto)==1?$fase->cfaseproyecto:''),array('id'=>'cfaseproyecto')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="cfaseproyecto" class="col-sm-2 control-label">ID </label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('cfaseproyecto',(isset($fase->cfaseproyecto)==1?$fase->cfaseproyecto:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'cfaseproyecto') ) !!}     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="codigo" class="col-sm-2 control-label">Codigo</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="codigo" placeholder="Codigo" name="codigo" value="{{(isset($codigo->codigo)==1?$codigo->codigo:'')}}">         
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">  
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>
          

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddFasProy" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->
