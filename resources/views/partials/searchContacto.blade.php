<!-- Modal Buscar Contacto-->
    <div id="searchContacto" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Buscar Contacto</h4>
          </div>
          <div class="modal-body">

            <div class="table-responsive">
              <table id="tContacto" class="table" style="width: 100%;">
                <thead>
                  <th>A Paterno</th>
                  <th>A Materno</th>
                  <th>Nombres</th>
                  <th>Cargo</th>
                </thead>

              </table>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Aceptar</button>
          </div>
        </div>
      </div>
    </div>
<!-- Fin Modal Buscar Proyecto-->