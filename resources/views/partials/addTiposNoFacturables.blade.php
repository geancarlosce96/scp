<!-- Modal agregar Usuario-->
    <div id="addTnfac" class="modal fade" role="dialog">
      <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
          <div class="modal-header" style='background-color:red; color:white'>
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Area</h4>
            
          </div>
          <div class="modal-body">
          {!! Form::open(array('url' => 'agregarArea','method' => 'POST','id' =>'frmtnfacturable','class'=>'form-horizontal')) !!}

          {!! Form::hidden('ctiponofacturable',(isset($ttiponofac->ctiponofacturable)==1?$ttiponofac->ctiponofacturable:''),array('id'=>'ctiponofacturable')) !!}   
                 
          {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

          <div class="form-group">
            <label for="ctiponofacturable" class="col-sm-2 control-label">ID</label>
                <div class="col-sm-8">
                    {!! Form::hidden('editar',(isset($editar)==1?$editar:'false')) !!}
              {!! Form::text('ctiponofacturable',(isset($ttiponofac->ctiponofacturable)==1?$ttiponofac->ctiponofacturable:''), array('class'=>'form-control input-sm','placeholder' => '','disabled' => 'true','id'=>'ctiponofacturable') ) !!}     
                </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>          

          <div class="form-group">
              <label for="descripcion" class="col-sm-2 control-label">Descripcion</label>
                  <div class="col-sm-8">
                      <input type="text" class="form-control" id="descripcion" placeholder="Descripcion" name="descripcion" value="{{(isset($descripcion->descripcion)==1?$descripcion->descripcion:'')}}">      
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>

          <div class="form-group">
              <label for="tipo" class="col-sm-2 control-label">Tipo</label>
                  <div class="col-sm-8">
                      <div class="radio">                        
                        {!! Form::radio('tipo','1',(isset($ttiponofac->tipo)==1?$ttiponofac->tipo == '1':false), array('id' => 'tipo-0')) !!}No Facturable
                        <br>
                        {!! Form::radio('tipo','2',(isset($ttiponofac->tipo)==1?$ttiponofac->tipo == '2':false), array('id' => 'tipo-0')) !!}Facturable
                        
                        </div>     
                  </div>
              <div class="col-sm-2">&nbsp;
              </div>
          </div>          

    {!! Form::close() !!}
    </div>
        <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id="idAddTnfac" onclick="grabar()">Aceptar</button>
        </div>
      </div>
    </div>
  </div>

<!-- Fin Modal agregar Usuario-->

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>


</script> 