<table class="table " id="tableListGastos">
                  <thead>
                    <tr class="clsCabereraTabla">
                      <th>Item</th>
                      <th style="min-width:200px!important;">Descripción</th>
                      <th style="min-width:50px!important;">Unidad</th>
                      <th>Cantidad</th>
                      <th>Costo Unitario</th>
                      <th>Sub Total</th>
                      <th style="min-width:150px!important;">Comentarios</th>  
                      <th>Seleccionar</th>                                         
                    </tr>                       
                  </thead>
                  @if(isset($gastos))
                  <tbody>
                  @foreach($gastos as $gas)
                      <tr
                      @if(empty($gas['cconcepto_parent']))
                        class="active"
                      @endif
                      >
                      <td>{{ $gas['item'] }} 
                      {!! Form::hidden('cpropuestaproyectogastos',$gas['cpropuestaproyectogastos'],array('id' => 'cpropuestaproyectogastos' )) !!}
                      </td>
                      <td
                       @if(!empty($gas['cconcepto_parent']))
                        style="padding-left: 40px"
                        @endif
                      >{{ $gas['descripcion'] }}</td>
                      <td>{{ $gas['unidad'] }}</td>                
                      <td>
                      @if(!empty($gas['cconcepto_parent']))
                      <input type="text" name="gas[{{ $gas['cpropuestaproyectogastos'] }}]" class="form-control" size="4" value="{{ $gas['cantidad'] }}" /> 
                      @endif
                      </td>
                      <td>{{ $gas['costou'] }}</td>
                      <td>{{ $gas['subtotal'] }}</td>
                      <td><textarea name="txt[{{ $gas['cpropuestaproyectogastos'] }}]" rows="2">{{ $gas['comentario'] }} </textarea></td>                
                      <td>
                      @if(!empty($gas['cconcepto_parent']))
                      <input name='del[]' type="checkbox" value="{{ $gas['cpropuestaproyectogastos'] }}">
                      @endif
                      </td>                                                                              
                      </tr>

                  @endforeach
                                                                                      
                  </tbody>
                  @endif
                  <tfoot>
                    <tr class="clsSubTotal">
                       <td></td>
                       <td></td>
                       <td></td>
                       <td></td>
                       <td>Total:</td>
                       <td><input class="form-control input-sm" type="text" placeholder="" disabled id="txtTotal" value="{{ isset($totGastos)==1?$totGastos:0 }}"></td>
                       <td></td>
                    </tr>
                  </tfoot>
</table>