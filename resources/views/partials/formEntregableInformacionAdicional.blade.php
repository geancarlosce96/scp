<div class="row">

  <div  class="col-sm-12">

   <div class="row">

      <div class="form-group">


        <!-- <div style="width: 100%">

          <div style="width: 20%">
            <div class="input-group">
              <span class="input-group-addon">
                <label class="col-form-label"><b>Responsable Interno</b></label>
              </span>
              {!! Form::select('responsableInt',(isset($responsable)==1?$responsable:array()),'',array('class' => 'form-control','id' => 'responsableInt')) !!}
            </div>
          </div>

          <div style="width: 20%">
            <div class="input-group">
              <span class="input-group-addon">
                <label class="col-form-label"><b>Elaborador</b></label>
              </span>
              {!! Form::select('aprobador',(isset($responsable)==1?$responsable:array()),'',array('class' => 'form-control','id'=>'aprobador')) !!}             
            </div>
          </div>

          <div style="width: 20%">
            <div class="input-group">
              <span class="input-group-addon">
                <label class="col-form-label"><b>Revisado por</b></label>
              </span>
              {!! Form::select('revisor',(isset($responsable)==1?$responsable:array()),'',array('class' => 'form-control','id'=>'revisor')) !!}          
            </div>
          </div>

          <div style="width: 20%">
            <div class="input-group">
              <span class="input-group-addon">
                <label class="col-form-label"><b>Aprobado por</b></label>
              </span>
              {!! Form::select('aprobador',(isset($responsable)==1?$responsable:array()),'',array('class' => 'form-control','id'=>'aprobador')) !!}             
            </div>
          </div>

        </div> -->




        <label for="cproyectoactividades" class="col-sm-1 control-label">Responsable Interno</label>
        <div class="col-sm-2">
          {!! Form::select('responsableInt',(isset($responsable)==1?$responsable:array()),'',array('class' => 'form-control select-box','id' => 'responsableInt')) !!}
        </div>
        

        <label for="edt" class="col-sm-1 control-label">Elaborador</label>
        <div class="col-sm-2">
          {!! Form::select('elaborador',(isset($elaborador)==1?$elaborador:array()),'',array('class' => 'form-control select-box','id'=>'elaborador')) !!}             
        </div>


        <label for="edt" class="col-sm-1 control-label">Revisado por</label>
        <div class="col-sm-2">
          {!! Form::select('revisor',(isset($revisor)==1?$revisor:array()),'',array('class' => 'form-control select-box','id'=>'revisor')) !!}             
        </div>
       
        <label for="edt" class="col-sm-1 control-label">Aprobado por</label>
        <div class="col-sm-2">
          {!! Form::select('aprobador',(isset($aprobador)==1?$aprobador:array()),'',array('class' => 'form-control select-box','id'=>'aprobador')) !!}             
        </div>


       <!--  <div class="col-sm-1">
          <button type="button" class="btn btn-danger btn-block" id="btnEliminar"><b>Quitar</b></button>
        </div> -->

      </div>

    </div>

    <br>

    <div class="row">
        <div class="col-sm-11">
          
        </div>

        <div class="col-sm-1">
          <button type="button" class="btn btn-primary btn-block" id="btnGrabarInfoAdicional"><b>Grabar</b></button>
        </div>
    </div>


    
  </div>


</div>

<br>


<script type="text/javascript">

$('#btnGrabarInfoAdicional').on('click',function(){


    $.ajax({
          url: 'grabarInfoAdicionalEntregable',
          type:'POST',
          data: $("#frmproyectoentregables").serialize(),
          beforeSend: function () {
            $('#div_carga').hide(); 
          },              
          success : function (data){

            $('#div_carga').hide();

            $("#resultado").html(data);

          },
    });   
})




  
</script>

<!--Inicion del Script para funcionamiento de Chosen -->
<script>
   $('.select-box').chosen({
        allow_single_deselect: true,
        width: '100%'
    });



</script>


</style>

<!-- Fin del Script para funcionamiento de Chosen -->


