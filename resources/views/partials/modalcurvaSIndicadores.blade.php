<table class="table table-striped">
      	<thead>
        	<tr class="clsCabereraTabla">
            	<td class="clsAnchoTabla">Item</td>
                <td class="clsAnchoTabla">Descripción de Tareas</td>
                <td class="clsAnchoTabla">Hrs/Item</td>
                <td class="clsAnchoTabla">USD /item</td>
                <td class="clsAnchoTabla">Inicio Planificado</td>
                <td class="clsAnchoTabla">Fin Planificado</td>
                <td class="clsAnchoTabla">Inicio<br>Real</td>
                <td class="clsAnchoTabla">Fin<br>Real</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(P)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(R)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(VG)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(P)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(R)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(VG)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(P)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(R)</td>
                <td class="clsAnchoTabla">dd/MM/yyyy<br>(VG)</td>                                
                <td class="clsAnchoTabla">Observaciones</td>               
            </tr>
        </thead>

      	<tbody>
            @if(isset($curvaind)==1)
            @foreach($curvaind as $cind)
        <tr>
        	<td class="clsAnchoTabla">{{ $cind['item'] }}</td>
            <td class="clsAnchoTabla">{{ $cind['descripcion'] }}</td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
        	<td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>
            <td class="clsAnchoTabla"></td>                        
        </tr> 
         @endforeach
        @endif                 
               
        </tbody>
      </table>