<table class="table table-striped table-hover table-bordered" style="font-size: 12px;" id="tableresumenLE">
  <thead>
    <tr class="clsCabereraTabla">
      <th>Ítem</th>
      <th>Código</th>
      <th>Nombre</th>
      <th>Rev.</th>
      <th>Ruta &nbsp;&nbsp;&nbsp;
        <input type="checkbox" name="checkbox_link_todos[]" title="Seleccionar todos los entregables" id="checkbox_link_todos" style="display: none;">&nbsp;&nbsp;&nbsp;
        <a data-toggle="tooltip" data-placement="bottom" title="Seleccionar entregable para editar ruta" class="mostareditarruta" id="mostareditarruta" style="color: white;"><i class="fa fa-unlink"></i></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Agregar ruta a todos los entregable seleccionados" class="editarrutatodos" id="editarrutatodos" style="color: white; display: none;"><i class="fa fa-link"></i></a>
        <a data-toggle="tooltip" data-placement="bottom" title="Ocultar check" class="btn cerrarrutatodos" id="cerrarrutatodos" style="color: red; display: none;"><i class="fa fa-close"></i></a>
      </th>
      <th>Acciones</th>
    </tr>
  </thead>
  <tbody>

    @foreach ($entregables as $key => $value)
    <tr class="entregable_ruteo" data-entregable="{{ $value['cproyectoentregables'] }}" data-revision="{{ $value['valor']}}">
      <td style="text-align: center;">{{ $key + 1 }}</td>
      <td>{{ $value['codigo'] }}</td>
      <td>{{ $value['descripcion_entregable'] }}</td>
      <td style="text-align: center;">{{ $value['valor'] }}</td>
      <td>
        <input type="checkbox" name="checkbox_link[]" value="{{ $value['cproyectoentregables'] }}" id="checkbox_link_{{ $value['cproyectoentregables'] }}" class="checkbox_link" style="display: none;">
        <input type="input" id="{{ $value['cproyectoentregables'] }}" class="form-control input-sm rutaentregable" placeholder="Ingrese la ruta del entregable..." name="{{ $value['cproyectoentregables'] }}" value="{{ $value['ruta'] }}" style="display: none;">
        <a data-toggle="tooltip" data-placement="bottom" title="Cerrar edición de ruta" class="btn cerrarruta" id="cerrarruta_{{ $value['cproyectoentregables'] }}" style="color: red; display: none;"><i class="fa fa-close"></i></a>
        <span id="ruta_{{ $value['cproyectoentregables'] }}">{{ $value['ruta'] }}</span>
        &nbsp;&nbsp;&nbsp;
        <br><span id="grabadoruta_{{ $value['cproyectoentregables'] }}" style="display: none;" class="grabadoruta"></span>
      </td>
      <td>
        <center>
        <a data-toggle="tooltip" data-placement="left" title="Editar ruta" class="btn editarruta" id="editarruta_{{ $value['cproyectoentregables'] }}"><i class="fa fa-link"></i></a>
        @if($value['cantidad'] == 0 )
        <a data-toggle="tooltip" data-placement="left" title="Cmentarios" class="btn comentario" id="comentario_{{ $value['cproyectoentregables'] }}"><i class="fa fa-commenting-o"></i></a>
        @else
        <a data-toggle="tooltip" data-placement="left" title="Comentario" class="btn comentario" id="vercomentario_{{ $value['cproyectoentregables'] }}"><i class="fa fa-commenting"></i></a>
        @endif
        <!-- <a data-toggle="tooltip" data-placement="top" title="Eliminar" class="deleteRuteo" style="color: red;"><i class="fa fa-trash"></i></a> -->
        </center>
      </td>
    </tr>
    @endforeach 

  </tbody>
</table>
<script src="{{ url('dist/js/tableHeadFixer.js') }}"></script>
<script type="text/javascript">
  
  $(".rutaentregable").focusout(function() {
      console.log($(this));
      actualizar_ruteo($(this));
    });

  $(".rutaentregable").click(function() {
    var ver = $(this).attr('id');
    $('#grabadoruta_'+ver).hide();
  });

  $(".editarruta").click(function() {
    var ver = $(this).attr('id');
    var id = ver.split('editarruta_');
    console.log("editar ruta", ver.split('editarruta_'),id[1]);
    $('#'+id[1]).show();
    $('#cerrarruta_'+id[1]).show();
    $("#"+ver).hide();
    $("#ruta_"+id[1]).hide();
    $('#grabadoruta_'+id[1]).hide();
  });

  $('.cerrarruta').click(function() {
    var ver = $(this).attr('id');
    var id = ver.split('cerrarruta_');
    // var ruta = $(this).val();
    console.log("cerrar ruta", ver.split('cerrarruta_'),id[1]);
    $("#"+id[1]).hide();
    $("#editarruta_"+id[1]).show();
    $("#cerrarruta_"+id[1]).hide();
    $("#ruta_"+id[1]).show();
    // $("#ruta_"+id[1]).text(ruta);

  });

  $(".comentario").click(function() {

      var id = $(this).parent().parent().parent().data('entregable');
      var rev = $(this).parent().parent().parent().data('revision');
      var ruteo = $('#ruteo').val();
      console.log(id,ruteo,rev);
      // vercomentarioentregable(id,ruteo,rev);

      $.ajax({
      url: 'verentregableruteocomentario',
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      data: {'cproyectoentregable': id, 'ruteo': ruteo,'vista':'tableComentarioEntregablesProyecto', 'estado':['REG'],'rev':rev},
    })
    .done(function(data) {
      // console.log("tablaEntregableRuteoComentario",data);
      $('#tablaEntregableRuteoComentario').html(data);
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });

      $('#modalcometarioLE').modal({
        backdrop:"static",
        keyboard: false,
      });

      $("#modalcometarioLE").draggable({
          handle: ".modal-content",
      });

      $("#modalcometarioLE")


      });

  $("#mostareditarruta").click(function() {
    $('.checkbox_link').show();
    $('#checkbox_link_todos').show();
    $('#mostareditarruta').hide();
    $('#editarrutatodos').show();
    $('#cerrarrutatodos').show();
  });

  $('#checkbox_link_todos').click(function() { //seleccionar todos los check
      if ($(this).prop('checked')) {
    $('.checkbox_link').each(
      function() {
          $('.checkbox_link').prop('checked', true);
      });
      } else {
        $('.checkbox_link').each(
      function() {
          $('.checkbox_link').prop('checked', false);
          });
      }
  });

  $("#tableresumenLE").tableHeadFixer({'left' : 1},);
  // $('#tableresumenLE').DataTable( {
  //   "bFilter": false,
  //   "bSort": false,
  //   "bPaginate": false,
  //   "bLengthChange": false,
  //   "bInfo": false,
  //       lengthChange: false,
  //       scrollY:        "600px",
  //       scrollCollapse: true,
  //     } );
  //     
  $("#editarrutatodos").click(function() { 
    var entregables= validarcheck_link();
    if (entregables == '') {
      swal("Seleccione algún entregable");
    }else{
     $('#modallinkLE').modal({
          backdrop:"static",
          keyboard: false,
        });
    }
  });

  $(".cerrarrutatodos").click(function() {
    $('.checkbox_link').prop('checked', false);
    $('.checkbox_link').hide();
    $('#mostareditarruta').show();
    $('#checkbox_link_todos').hide();
    $("#editarrutatodos").hide();
    $("#cerrarrutatodos").hide();
  });

  function validarcheck_link(){

  var check_activos_link=[];
  var contador_check_link = 0;

  $('.checkbox_link').each(
      function() {
        if ($(this).prop('checked')) {
          check_activos_link[contador_check_link] = $(this).val();
        }
          contador_check_link++;
      });

   // console.log("checkbox_link_",check_activos_link);
   return check_activos_link;
    // grabaruteo(check_activos_link,'rev');
 }



</script>