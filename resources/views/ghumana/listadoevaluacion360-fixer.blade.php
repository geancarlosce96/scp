@extends('layouts.master')
@section('content')
<div class="content-wrapper">
  <section class="content-header">
      <h1>
        Propuesta
        <small>Propuesta</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Propuestas</li>
      </ol>
    </section>
    <section class="content">
      <div class="box-body">
        <div class="box-group" id="accordion">
          <!-- we are adding the .panel class so bootstrap.js collapse plugin detects it -->
          <div class="panel panel-primary">
            <div class="box-header with-border panel-heading">
              <h4 class="box-title panel-title">
                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed"> <span class="glyphicon glyphicon-hand-up"></span> Agregar propuesta</a>
              </h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
              <div class="box-body">
                
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                <table class="table table-striped table-hover table-bordered" id="listado5485" style="font-size: 14px;">
                      <thead>
                        <tr>
                          <th class="clsCabereraTabla">Nro</th>
                          <th class="clsCabereraTabla">Pregunta</th>
                          <th class="clsCabereraTabla">A</th>
                          <th class="clsCabereraTabla">B</th>
                          <th class="clsCabereraTabla">C</th>
                          <th class="clsCabereraTabla">D</th>                    
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($preguntas as $key => $pregunta)
                          @if($pregunta->tipo_respuesta =='A al D')
                        <tr>

                          <td class="text-center">{{ $key + 1}}</td>
                          <td>{{ $pregunta->detalle }}</td>
                          <td class="text-center">
                           <input type="hidden" name="idcab" value="{{ $idcab }}">
                           <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="1" @if($pregunta->id_respuesta == '1') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                         </td>
                         <td class="text-center">
                           <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="2" @if($pregunta->id_respuesta == '2') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                         </td>
                         <td class="text-center">
                          <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="3" @if($pregunta->id_respuesta == '3') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                        </td>
                        <td class="text-center">
                          <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="4" @if($pregunta->id_respuesta == '4') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                        </td>
                      </tr>
                        @endif
                      @endforeach          
                    </tbody>

                  </table>
      </div>
    </section>
            
</div>

@include('partials.modalRegistroUnidadMinera')
@include('partials.modalRegistroContactoUnidadMinera')
                
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

 <script>
  $(document).ready(function(){



    var table = $('#listado5485').DataTable( {
    "bFilter": false,
    "bSort": false,
    "bPaginate": false,
    "bLengthChange": false,
    "bInfo": false,
        lengthChange: false,
        scrollY:        "600px",
        scrollCollapse: true,
      } ); ;


  });
  </script>
@stop

