@extends('layouts.master')
@section('content')

@if( empty($vacaciones[0]['saldo']) || empty($anticipadas[0]['saldo']))
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="alert alert-warning" role="alert"><p style="font-size: 25px;text-align: center;">Usted no posee vacaciones cargadas favor comuníquese con el administrador</p></div>
    </section>
</div>
@else
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Solicitud de Vacaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">
      <div class="row clsPadding2"> 
        <div class="col-lg-12 col-xs-12">
<!-- inicio accordeon -->       

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                
                {{-- <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Detalle de Vacaciones</a></li>  --}}
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Solicitud Vacaciones</a></li>        
              <!--<li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                  Dropdown <span class="caret"></span>
                </a>
         <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li>-->
              <li class="pull-left header"><i class="fa fa-newspaper-o"></i> Listado de Vacaciones por periodo</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">DNI:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['identificacion'] }}" disabled="true"> 
                      </div>                    
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">Apellidos y Nombres:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['nombreusuario'] }}" disabled="true"> 
                      </div>
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">Cargo:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['cargouser'] }}" disabled="true"> 
                      </div>
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">Jefe Inmediato:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['jefeinmediato'] }}" disabled="true"> 
                      </div>
                  </div>
                  <div class="col-12 row clsPadding2 text-center">
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <input type="hidden" id="saldovacionesinicial" name="saldovacionesinicial" value="{{ $saldo = ( $vacaciones[0]['saldo'] - $valoracumulado = (is_null($vacaciones[0]['saldo_temp']))? 0:$vacaciones[0]['saldo_temp'])<0 ? 0:$vacaciones[0]['saldo'] - $valoracumulado = (is_null($vacaciones[0]['saldo_temp']))? 0:$vacaciones[0]['saldo_temp']}}">
                      <button onclick="tipovaca()"  id="botonvaciones" type="button" class="btn btn-success" data-toggle="modal" data-target="#myModalva" style="font-size: 22px;">Vacaciones Disponibles: {{ $saldo = ( $vacaciones[0]['saldo'] - $valoracumulado = (is_null($vacaciones[0]['saldo_temp']))? 0:$vacaciones[0]['saldo_temp'])<0 ? 0:$vacaciones[0]['saldo'] - $valoracumulado = (is_null($vacaciones[0]['saldo_temp']))? 0:$vacaciones[0]['saldo_temp']}}</button>   
                    </div>
                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                      <button onclick="tipoanti()" id="botonancicipadas" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalva" style="font-size: 22px;" disabled="true">Vacaciones Anticipadas: {{ $anticipadas[0]['saldo'] - $valoracumulado = (is_null($anticipadas[0]['saldo_temp']))? 0:$anticipadas[0]['saldo_temp'] }}</button>  
                    </div>
                  </div>

                  <div class="row clsPadding2">
                      <div class="col-lg-12 col-xs-12 table-responsive">
                    <table class="table table-striped table-hover table-bordered text-center" id="listadovaca" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
                        <thead>
                        <tr class="clsCabereraTabla">
                          <th>Fecha Inicio</th>
                          <th>Fecha Fin</th>
                          <th>Dias Tomados</th>
                          <th>Estado</th>
                          <th>Tipo</th>
                          <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($solicitudes as $key => $solicitud)
                            <tr>
                              <td class="">{{ $solicitud->fdesde }}</td>
                              <td  class="">{{ $solicitud->fhasta }}</td>
                              <td  class="">{{ $solicitud->diassolicitados }}</td>
                              <td  class="">
                                @if( $solicitud->estadova =='Aprobado JI' && $vauser['niveldeaprobacion']=='1' || $solicitud->estadova =='Aprobado JS' && $vauser['niveldeaprobacion']=='2')
                                  <span class="label alert-success" style="font-size: 14px;">
                                    Aprobado
                                  </span>
                                @elseif( $solicitud->estadova =='Observaciones' )
                                  <span class="label alert-warning" style="font-size: 14px;">
                                    {{ $solicitud->estadova }}
                                  </span>
                                @elseif( $solicitud->estadova =='Solicitado')
                                  <span class="label alert-info" style="font-size: 14px;">
                                    {{ $solicitud->estadova }}
                                  </span>
                                @endif
                              </td>
                              <td  class="">
                                @if($solicitud->tipovaca == 'Vacaciones_Pendientes')
                                  <b>{{ ucfirst('Vacaciones Solcicitadas') }}</b>
                                @else
                                  <b>{{ ucfirst('Vacaciones Adelantadas') }}</b>
                                @endif
                              </td>
                              <td  class="" tyle="color:#fff;">  
                                @if( $solicitud->estadova =='Aprobado JI' && $vauser['niveldeaprobacion']=='1' || $solicitud->estadova =='Aprobado JS' && $vauser['niveldeaprobacion']=='2')
                                  <a href="{{ url('pdfvacaciones/aprobadasvaca', $solicitud->idsolicitud) }}" style="color:#fff !important;" target="_blank">
                                    <button type="button" class="btn btn-primary" style="color:#fff !important;">
                                      Imprimir Comprobante
                                  </a>
                                @elseif( $solicitud->estadova =='Observaciones' )
                                  
                                  <!-- Button trigger modal -->
                                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal_{{$key}}">
                                    {{ $solicitud->estadova  }}
                                  </button>

                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_{{$key}}">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Observación</h4>
                                        </div>
                                        <div class="modal-body">
                                          {{ $solicitud->observacion  }}
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                

                                @elseif( $solicitud->estadova =='Solicitado')
                                  <button type="submit" class="btn btn-primary btn-block" disabled="true">Pendiente...</button> 
                                @endif  
                              </td>
                            </tr>
                          @endforeach                                              
                        </tbody>
                      </table>
                    </div>
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
          </div>
<!--  fin accordeon -->        
    </div>
</div>
<!--<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-12 clsPadding">
        <a href="#" </class="btn btn-primary btn-block"><b>Grabar</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding">
        <a href="#" </class="btn btn-primary btn-block"><b>Cancelar</b></a> </div>
               
</div>-->
        
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 
</div>
    </section>
    <!-- /.content -->
    <input type="hidden" name="totalvaciones" id="totalvaciones" value="{{ $saldo = ( $vacaciones[0]['saldo'] - $valoracumulado = (is_null($vacaciones[0]['saldo_temp']))? 0:$vacaciones[0]['saldo_temp'])<0 ? 0:$vacaciones[0]['saldo'] - $valoracumulado = (is_null($vacaciones[0]['saldo_temp']))? 0:$vacaciones[0]['saldo_temp']}}">
    <input type="hidden" name="totalanticipadas" id="totalanticipadas" value="{{ $anticipadas[0]['saldo'] - $valoracumulado = (is_null($anticipadas[0]['saldo_temp']))? 0:$anticipadas[0]['saldo_temp']}}">
<!-- Modal Vacaciones-->
<div class="modal fade" id="myModalva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069aa;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel" >Solicitud Vacaciones </h4>
      </div>
      {!! Form::open(['url' => 'vacaciones/solicitud','method' => 'POST', 'id' => 'solicitudvacaciones']) !!}
      {!! Form::token(); !!}
      <div class="modal-body">        
        <input type="hidden" name="saldovacaciones" id="saldovacaciones" value="">
        <input type="hidden" name="diassolicitados" id="diassolicitados" value="">
        <input type="hidden" name="estadova" id="estadova" value="Solicitado">
        <input type="hidden" name="tipovaca" id="tipovaca" value="">
        <input type="hidden" name="idjefeinmediato" id="idjefeinmediato" value="{{ $vauser['idjefeinmediato'] }}">
        <input type="hidden" name="idjefesuperior" id="idjefesuperior" value="{{ $vauser['idsuperjefe'] }}">
        <input type="hidden" name="idempleado" id="idempleado" value="{{ $vauser['idtpersonalogin'] }}">
        <input type="hidden" name="dias" id="idempleado" value="{{ $vauser['idtpersonalogin'] }}">
        <p style="font-size: 22px;font-style:font-weight: 1200;">Dias Disponibles: 
          <span class="label alert-success" style="font-size: 14px;" id="imprimosaldovacaspan"></span>
          a los que se les restara
          <span  class="label alert-danger" style="font-size: 14px;">  <span id="restavacacionesdias"></span></span>
        </p>
        <div id="alertavacaciones" class="alert alert-danger" role="alert" style="display:none;">
          <p style="font-size: 15px;">Cantidad de dias es Incorrecta !!</p>
        </div>
        
        <label class="clsTxtNormal">Fechas:</label> 

         
        <div class="row ">
            <div class="col-lg-1 col-xs-1">Desde:</div>
            <div class="col-lg-5 col-xs-5">
                <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input onchange="calculovacaciones()" type="text" name="fdesdeva" class="form-control pull-right input-sm" autocomplete="off" id="fdesdeva">
                </div>    
            </div>
            <div class="col-lg-1 col-xs-1">Hasta:</div>
            <div class="col-lg-5 col-xs-5">
                <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input onchange="calculovacaciones()" type="text" name="fhastava" class="form-control pull-right input-sm" autocomplete="off" id="fhastava">
                </div>    
            </div>  
          </div>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="validofechasvaca()">Solcitar</button>
      </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>

  </div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

    var saldoinicial = document.getElementById("saldovacionesinicial").value;
    
    if (saldoinicial <= 0 ) {
        document.getElementById("botonancicipadas").disabled = false;
        document.getElementById("botonvaciones").disabled = true;
        
    }

    $('#listadovaca').DataTable({
      "bFilter": true,
       "bSort": true,
       "bPaginate": true,
       "bInfo": true,
       "scrollY":        "600px",
       "scrollCollapse": true,
    });
    $('#listadovaca2').DataTable();

    $('#fdesdeva').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "tomorrow"
    }).datepicker("setDate",'now');  

    $('#fhastava').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "tomorrow"
    }).datepicker("setDate",'now');

    
  });

function calculovacaciones() {
  var fechaI = document.getElementById("fdesdeva").value;
  var fechaF = document.getElementById("fhastava").value;
  var difM = fechaF - fechaI // diferencia en milisegundos
  var difD = difM / (1000 * 60 * 60 * 24) // diferencia en dias
  var f1 = document.getElementById("fdesdeva").value;
  var f2 = document.getElementById("fhastava").value;
  var aFecha1 = f1.split('/'); 
  var aFecha2 = f2.split('/'); 
  var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
  var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
  var dif = fFecha2 - fFecha1;
  var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
  document.getElementById("restavacacionesdias").innerHTML =  dias+1;
  document.getElementById("diassolicitados").value = dias+1;
}

function validofechasvaca(){ 
  // swal($('#tipovaca').val());
  var fechaI = document.getElementById("fdesdeva").value;
  var fechaF = document.getElementById("fhastava").value;
  var difM = fechaF - fechaI // diferencia en milisegundos
  var difD = difM / (1000 * 60 * 60 * 24) // diferencia en dias
  var f1 = document.getElementById("fdesdeva").value;
  var f2 = document.getElementById("fhastava").value;
  var aFecha1 = f1.split('/'); 
  var aFecha2 = f2.split('/'); 
  var fFecha1 = Date.UTC(aFecha1[2],aFecha1[1]-1,aFecha1[0]); 
  var fFecha2 = Date.UTC(aFecha2[2],aFecha2[1]-1,aFecha2[0]); 
  var dif = fFecha2 - fFecha1;
  var dias = Math.floor(dif / (1000 * 60 * 60 * 24)); 
  var saldovaca = document.getElementById("saldovacaciones").value;
  var x = document.getElementById("alertavacaciones");

  if ($('#tipovaca').val()=='Vacaciones_Pendientes') {
    
    if (dias > saldovaca || dias < 0) {
          x.style.display = "block";
          return false;

      }
      else {
          x.style.display = "none";
          document.getElementById("solicitudvacaciones").submit();
          return true;
        }
  }
       else {
          x.style.display = "none";
          document.getElementById("solicitudvacaciones").submit();
          return true;
        }
}

function tipovaca(){
  var saldovacaciones = document.getElementById("totalvaciones").value;
  document.getElementById("imprimosaldovacaspan").innerHTML =  saldovacaciones;
  document.getElementById("tipovaca").value = 'Vacaciones_Pendientes';
  document.getElementById("saldovacaciones").value = saldovacaciones;
  var x = document.getElementById("alertavacaciones");
  var d = new Date();
  // var dia = moment(d).format('L');
  var month = d.getMonth()+1;
  var day = d.getDate();
  var dia =    day +'/'+ ((''+month).length<2 ? '0' : '') + month + '/' + ((''+day).length<2 ? '0' : '')  + d.getFullYear();
  
  $('#fdesdeva').val(dia);
  $('#fhastava').val(dia);
  $('#calculovacaciones').val(0);
  x.style.display = "none";
}

function tipoanti(){
  var saldoanticipadas = document.getElementById("totalanticipadas").value;
  document.getElementById("imprimosaldovacaspan").innerHTML =  saldoanticipadas;
  document.getElementById("tipovaca").value = 'Vacaciones_Adelantar';
  document.getElementById("saldovacaciones").value = saldoanticipadas;
  var x = document.getElementById("alertavacaciones");
  var d = new Date();
  // var dia = moment(d).format('L');
  var month = d.getMonth()+1;
  var day = d.getDate();
  var dia =    day +'/'+ ((''+month).length<2 ? '0' : '') + month + '/' + ((''+day).length<2 ? '0' : '')  + d.getFullYear();
  
  $('#fdesdeva').val(dia);
  $('#fhastava').val(dia);
  $('#calculovacaciones').val(0);
  x.style.display = "none";
}




</script>



@endif

@stop