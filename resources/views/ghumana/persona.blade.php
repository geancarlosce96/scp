<div class="modal fade bd-example-modal-lg" id="persona" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
      {!! Form::open(array('url' => 'grabarPropuesta','method' => 'POST','id' =>'frmpersonal')) !!}
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069AA">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h5 class="modal-title" id="exampleModalLabel" style="color: white">Usuario</h5>
      </div>
      <div class="modal-body">
       {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}
       <input type="hidden" name="cpersona" id="cpersona">
       <div class="row">
         <div class="col-xs-3 col-xs-12">
           <div class="input-group">
            <span class="input-group-addon">
             <label>Nombres</label>
           </span>
           <input type="text" class="form-control" id="nombres" name="nombres" value="{{isset($listarpersonal->nombres)==1?$listarpersonal->nombres:''}}" required>
         </div>
       </div>
       <div class="col-xs-2 col-xs-12">
         <div class="input-group">
          <span class="input-group-addon">
           <label>Apellido Paterno</label>
         </span>
         <input type="text" class="form-control" id="apaterno" name="apaterno" value="{{isset($listarpersonal->apaterno)==1?$listarpersonal->apaterno:''}}" required>
       </div>
     </div>
     <div class="col-xs-2 col-xs-12">
       <div class="input-group">
        <span class="input-group-addon">
         <label>Apellido Materno</label>
       </span>
       <input type="text" class="form-control" id="amaterno" name="amaterno" value="{{isset($listarpersonal->amaterno)==1?$listarpersonal->amaterno:''}}" required>
     </div>
   </div>
   <div class="col-xs-2 col-xs-12">
     <div class="input-group">
      <span class="input-group-addon">
       <label>DNI</label>
     </span>
     <input type="text" class="form-control" id="dni" name="dni" value="{{isset($listarpersonal->identificacion)==1?$listarpersonal->identificacion:''}}" required>
   </div>
 </div>
 <div class="col-xs-2 col-xs-12">
     <div class="input-group">
      <span class="input-group-addon">
       <label>Alias</label>
     </span>
     <input type="text" class="form-control" id="alias" name="alias" value="{{isset($listarpersonal->abreviatura)==1?$listarpersonal->abreviatura:''}}"required>
   </div>
 </div>
</div>
<br>
<div class="row">
 <div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Estado Civil</label>
           </span>
           <select class="form-control" id="estadocivil" name="estadocivil" required>
               @foreach($testadocivil as $civil)
               <option value="{{ $civil->digide }}">{{ $civil->descripcion }}</option>
               @endforeach
           </select>
       </div>
   </div>
<div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Genero</label>
           </span>
           <select class="form-control" id="genero" name="genero" required>
               @foreach($tgenero as $gen)
               <option value="{{ $gen->digide }}">{{ $gen->descripcion }}</option>
               @endforeach
           </select>
       </div>
   </div>
<div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Estado personal</label>
           </span>
           <select class="form-control" id="estadopersona" name="estadopersona" required>
               @foreach($testado as $est)
               <option value="{{ $est->digide }}">{{ $est->descripcion }}</option>
               @endforeach
           </select>
       </div>
   </div>

<div class="col-xs-2 col-xs-12">
  <div class="form-group">
   <div class="input-group date">
    <div class="input-group-addon">
     <label>Fecha Ingreso</label>
     <i class="fa fa-calendar"></i>
   </div>
   <input type="text" class="form-control pull-right"  id="fecha" name="fecha"  required>
 </div>
</div>
</div>
</div>
<br>
<div class="row">
  <div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Área</label>
           </span>
           <select class="form-control" id="area" name="area" required>
               @foreach($tarea as $area)
               <option value="{{ $area->carea }}">{{ $area->descripcion }}</option>
               @endforeach
           </select>
       </div>
   </div>
   <div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Cargo</label>
           </span>
           <select class="form-control" id="cargo" name="cargo" required>
               @foreach($tcargo as $cargo)
               <option value="{{ $cargo->ccargo }}">{{ $cargo->descripcion }}</option>
               @endforeach
           </select>
       </div>
   </div>
   <div class="col-xs-3 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Categoria</label>
           </span>
           <select class="form-control" id="categoria" name="categoria" required>
               @foreach($tcategoria as $categoria)
               <option value="{{ $categoria->ccategoriaprofesional }}">{{ $categoria->descripcion }}</option>
               @endforeach
           </select>
       </div>
   </div>
   <div class="col-xs-2 col-xs-12">
         <div class="input-group">
            <span class="input-group-addon">
               <label>Contrato</label>
           </span>
           <select class="form-control" id="contrato" name="contrato" required>
               @foreach($tcontrato as $contrato)
               <option value="{{ $contrato->ctipocontrato }}">{{ $contrato->descripcion }}</option>
               @endforeach
           </select>
       </div>
   </div>
</div>
<br>
<div class="row" id="nuevo" >

<div class="col-xs-3 col-xs-12">
     <div class="input-group">
      <span class="input-group-addon">
       <label>Usuario</label>
     </span>
     <input type="text" class="form-control" id="usuario" name="usuario">
   </div>
 </div>
 <div class="col-xs-2 col-xs-12">
     <div class="input-group">
      <span class="input-group-addon">
       <label>Clave</label>
     </span>
     <input type="text" class="form-control" id="clave" name="clave">
   </div>
 </div>

<!--  <div class="col-xs-3 col-xs-12">
 </div>
 <div class="col-xs-3 col-xs-12">
   <a type="button" href="{{ url('int?url=listarpersonal') }}" class="btn btn-primary bg-blue" >Volver al Listado</a>
 </div> -->
<!-- <div class="col-xs-1 col-xs-12">
 <div class="input-group">
  <span class="input-group-btn">
    <button type="submit" class="btn btn-success bg-green" >
    @if($valor == "nuevo")
    Registrar
    @else
    Actualizar
    @endif
    </button>
 </span>
</div>
</div> -->
</div>


</div>
      <div class="modal-footer">
        <button type="button" class="btn btn-success bg-green" id="accion" ></button>
        <button type="button" class="btn btn-primary bg-blue" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
{!! Form::close() !!}
  </div>
</div>


