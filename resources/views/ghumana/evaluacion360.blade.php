@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Listado de Evaluaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">
        <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">
          Lista de {{ $nombrevaluacion }}
            </div>  
        </div>
        <div class="row clsPadding2">	
            <div class="col-lg-3 col-xs-12">
            </div>
            <div class="col-lg-3 col-xs-12">
            <!-- <a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> -->
            </div> 
            <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="getUrl('empleado');"  class="btn btn-primary btn-block"><b>Nuevo</b></button> --> 
            </div> 
            <div class="col-lg-3 col-xs-12">
        </div>
        <div class="row clsPadding2" width="100%">	
            <div class="col-lg-12 col-xs-12 table-responsive">
              <table class="table table-striped table-hover table-bordered" id="listadoevaluacion" width="100%" style="font-size: 12px;">
              	<thead>
                <tr>
                  <td class="clsCabereraTabla">Evaluado</td>
                  <td class="clsCabereraTabla">Cargo</td>
                  <td class="clsCabereraTabla">Estado</td>
                  <td class="clsCabereraTabla">Acceso</td>
                  <td class="clsCabereraTabla">Terminar</td>
                </tr>
                </thead>
                <tbody>
                @foreach($evaluados as $evaluado)
                  <tr>
                       <td class="text-center">{{ $evaluado->abreviatura }}</td>
                       <td class="text-center">{{ $evaluado->descripcion }}</td>
                       @if($evaluado->estado_cab =='1')
                        <td class="text-center"><span class="label alert-warning" style="font-size: 12px;">Pendiente</span></td>
                       @elseif($evaluado->estado_cab =='2')
                        <td class="text-center"><span class="label alert-success" style="font-size: 12px;">Finalizada</span></td>
                       @elseif($evaluado->estado_cab =='99')
                        <td class="text-center"><span class="label alert-info" style="font-size: 12px;">Por Terminar</span></td>
                       @endif 
                       {{-- <td class="text-center">{{ $evaluado->dni_evaluado }}</td>  --}}
                       <td class="text-center">
                        @if($evaluado->estado_cab =='1')
                          <a type="button" class="btn btn-primary btn-block" href="{{ url('evaluaciones', [$evaluado->idcab])}}" style="font-size: 12px;">
                            Evaluar
                          </a>
                        @elseif($evaluado->estado_cab =='2')
                          <a type="button" class="btn btn-primary btn-block" href="{{ url('evaluaciones', [$evaluado->idcab])}}" style="font-size: 12px;">
                            Ver Detalle
                          </a>
                        @elseif($evaluado->estado_cab =='99') 
                          <a type="button" class="btn btn-primary btn-block" href="{{ url('evaluaciones', [$evaluado->idcab])}}" style="font-size: 12px;">
                            Ver Detalle
                          </a> 
                        @endif 
                        </td>
                      <td class="text-center">
                        <div class="text-center">
                            @if($evaluado->estado_cab =='1')
                              <button type="button" class="btn btn-primary btn-block" href="{{ url('evaluaciones', [$evaluado->idcab])}}" style="font-size: 12px;" disabled>
                                Enviar Encuesta
                              </button>
                            @elseif($evaluado->estado_cab =='2')
                              <span class="label alert-success" style="font-size: 12px;">Completada</span>
                            @elseif($evaluado->estado_cab =='99')
                            {!! Form::open(['url' => 'evaluacion/cerrarencuesta','method' => 'POST', 'id' => 'terminoform2']) !!}
                            {!! Form::token(); !!}
                              <input  type="hidden" name="idcab_terminada" value="{{ $evaluado->idcab }}"> 
                              <button onclick="totalmenteseguro();" type="submit" class="btn btn-primary btn-block" >
                                Enviar Encuesta
                              </button>
                            {!! Form::close() !!}     
                            @endif
                        </div>
                          
                      </td>
                  </tr>  
                @endforeach            
                </tbody>
                </table>
             </div>
        </div>
      </div>
      <div class="col-lg-1 hidden-xs"></div>
    </div>
    </section>
    <!-- /.content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
  $(document).ready(function(){
    var table = $('#listadoevaluacion').DataTable( {
         "paging":   true,
         "ordering": false,
         "info":     true,
        // "order":     [[0,"desc"]],
         lengthChange: true
         //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
     } );  
   } );   
</script>
<script type="text/javascript">

function totalmenteseguro() {
  event.preventDefault();
  var casilisto = confirm("¿Esta totalmente seguro de enviar esta Encuesta?. Al terminarla no podra modificar los datos");

  //alert(casilisto);
  //return 'epale miguuel carlos';
  if (casilisto) {
    $("#terminoform2").submit();
  } else {
    return false;
  }
}
</script>
@stop