<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Listado de Empleados</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">

        <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">
          Lista de Empleados
            </div>  
        </div>



        <div class="row clsPadding2">	
            <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="estadoempleadousuario();"  class="btn btn-block btn-success"><b>Activar/Inactivar</b></button> -->
             </div>
            
            <div class="col-lg-3 col-xs-12">
            <!-- <a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> -->
            </div> 
            
            <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="getUrl('empleado');"  class="btn btn-primary btn-block"><b>Nuevo</b></button> --> 
            </div> 
          
            <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="getUrl('personal/nuevo');"  class="btn btn-primary btn-block"><b>Nuevo</b></button> -->
            <button type="button" onclick="addnuevo('nuevo');"  class="btn btn-primary btn-block"><b>Nuevo</b></button>
        </div>
            <!-- <button type="button"  onclick="goEditar();"  class="btn btn-primary btn-block"><b>Editar</b></button> </div> -->
         
        </div>
        <div class="row clsPadding2" width="100%">	
            <div class="col-lg-12 col-xs-12 table-responsive">
              <!-- <table class="table table-striped" id="listado" width="100%">
              	<thead>
                <tr>
                    <td class="clsCabereraTabla">Id</td>
                    <td class="clsCabereraTabla">Identificacion</td>
                    <td class="clsCabereraTabla">A. paterno</td>
                    <td class="clsCabereraTabla">A. materno</td>
                    <td class="clsCabereraTabla">Nombres</td>
                    <td class="clsCabereraTabla">Área</td>
                    <td class="clsCabereraTabla">Estado</td>
                    
                </tr>
                </thead>
                <tbody>
                                
                </tbody>
                </table> -->

                @include('ghumana.listadopersona')
             </div>
        </div>
         
      </div>
      <div class="col-lg-1 hidden-xs"></div>
    </div>
    </section>
    <!-- /.content -->
</div>

@include('ghumana.persona')
  <!-- <script src="plugins/datatables/jquery.dataTables.min.js"></script> -->
<!-- <script src="plugins/datatables/dataTables.bootstrap.min.js"></script> -->
<!-- <script src="https://code.jquery.com/jquery-3.3.1.js" type="text/javascript" charset="utf-8" async defer></script> -->
<!-- <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript" charset="utf-8" async defer></script> -->

<script>
    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';     
    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarEmpleado',
      "order": [[ 0, "desc" ]],
      "columns" : [
        {data : 'cpersona', name: 'tper.cpersona'},
        {data : 'identificacion', name: 'tper.identificacion'},
        {data : 'apaterno', name: 'tnat.apaterno'},
        {data : 'amaterno' , name : 'tnat.amaterno'},
        {data : 'nombres' , name : 'tnat.nombres'},
        {data : 'descripcion' , name : 'are.descripcion'},
        {data : 'estado', name: 'tot.estado'},
      ],

      "rowCallback" : function( row, data ) {
          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {
              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });
    
    $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );
        }/* else {
            selected.splice( index, 1 );
        }*/
        
        $(this).toggleClass('selected');
    });

    $('#listadopersona').DataTable();


    function modal(){
       $("#persona").modal({
        backdrop:"static"
      });
    }


  function addnuevo(valor){
    limpiar();
    $('#nuevo').prop('hidden',false);
    $('#accion').text('Registrar');
    $('#accion').prop("type", "submit");
    $('#cpersona').val(valor);
    modal();
  }

  
  function estadoempleadousuario(id){

    $.ajax({
      url: 'estadoempleadousuario/'+id,
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      // data: {param1: 'value1'},
    })
    .done(function(data) {
      console.log("estadoempleadousuario",data);
      $("#resultado").html(data);
      swal("Se realizo la acción", "correctamente","success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
  }

  function editarempleadousuario(id){

    // getUrl('personal/'+id);

    $.ajax({
      url: 'personal/'+id,
      type: 'GET',
      // dataType: 'default: Intelligent Guess (Other values: xml, json, script, or html)',
      // data: {param1: 'value1'},
    })
    .done(function(data) {
      console.log("editarempleadousuario",data);
      limpiar();
      modal();
      $('#accion').text('Actualizar');
      $('#accion').prop("type", "submit");
      $('#nuevo').prop('hidden',true);
      $('#cpersona').val(data[0].cpersona);
      $('#nombres').val(data[0].nombres);
      $('#apaterno').val(data[0].apaterno);
      $('#amaterno').val(data[0].amaterno);
      $('#dni').val(data[0].identificacion);
      $('#alias').val(data[0].abreviatura);
      $('#fecha').val(data[0].fingreso.substring(0,10));
      $('#estadopersona').val(data[0].estadousuario).trigger('chosen:updated');
      $('#cargo').val(data[0].ccargo).trigger('chosen:updated');
      $('#categoria').val(data[0].ccategoriaprofesional).trigger('chosen:updated');
      $('#area').val(data[0].carea).trigger('chosen:updated');
      $('#contrato').val(data[0].ctipocontrato).trigger('chosen:updated');
      $('#estadocivil').val(data[0].estadocivil).trigger('chosen:updated');
      $('#genero').val(data[0].genero).trigger('chosen:updated');
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    
    
  }

  function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(4));
        getUrl('editarEmpleado/'+id.substring(4),'');
    }else{
        $('.alert').show();
    }
  }

  function limpiar(){
    $('#cpersona').val('');
    $('#nombres').val('');
    $('#apaterno').val('');
    $('#amaterno').val('');
    $('#dni').val('');
    $('#alias').val('');
    $('#fecha').val('');
    $('#estadopersona').val('').trigger('chosen:updated');
    $('#cargo').val('').trigger('chosen:updated');
    $('#categoria').val('').trigger('chosen:updated');
    $('#area').val('').trigger('chosen:updated');
    $('#contrato').val('').trigger('chosen:updated');
    $('#estadocivil').val('').trigger('chosen:updated');
    $('#genero').val('').trigger('chosen:updated');
  }

    $('#fecha').datepicker({ format: 'yyyy-mm-dd', autoclose: true, calendarWeeks: true, language: "es"});
    addchosen('estadocivil');
    addchosen('genero');
    addchosen('estadopersona');
    addchosen('area');
    addchosen('cargo');
    addchosen('categoria');
    addchosen('contrato');


$('#frmpersonal').on('submit',function(e){

            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            $.ajax({

                type:"POST",
                url:'grabarpersonal',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $('.modal-backdrop').hide()
                     // $("#div_msg").show();
                     swal("Se realizo la acción", "correctamente","success");
                     
                },
                error: function(data){

                    $('#div_carga').hide();
                    
                    $('#detalle_error').html(data);
                    // $("#div_msg_error").show();
                    swal("No realizo la acción", "correctamente","success");
                }
            });
        });

</script>
