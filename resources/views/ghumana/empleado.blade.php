<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Empleado</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
 



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">
<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12">
<!-- inicio accordeon -->       

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                <li class=""><a href="#tab_6-2" data-toggle="tab" aria-expanded="false">Expericiencia Laboral</a></li>
                <li class=""><a href="#tab_5-2" data-toggle="tab" aria-expanded="false">Laboral</a></li>
                <li class=""><a href="#tab_4-2" data-toggle="tab" aria-expanded="false">Médicos</a></li>
                <li class=""><a href="#tab_3-2" data-toggle="tab" aria-expanded="false">Estudios</a></li>
                <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Familiar</a></li>
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Datos Generales</a></li>
              <li class="pull-left header"><i class="fa fa-user"></i>Empleado</li>
            </ul>
            <div class="tab-content">
             
              <div class="tab-pane active" id="tab_1-1">
                    <!-- Pestaña Datos Personales -->              
                    {!! Form::open(array('url' => 'grabarEmpleado','method' => 'POST','id' =>'frmempleado')) !!}
                    {!! Form::hidden('cpersona',(isset($persona)==1?$persona->cpersona:''),array('id'=>'cpersona')) !!}
                    {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}  
                    <div class="row clsPadding2">
                        <div class="col-lg-6 col-xs-6">
                        <label class="clsTxtNormal">DNI:</label>
                        {!! Form::text('identificacion',(isset($persona->identificacion)==1?$persona->identificacion:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'identificacion','maxlength'=>'15') ) !!}
                        </div>
                        <div class="col-lg-6 col-xs-6">
                        <label class="clsTxtNormal">Código SIG.:</label>
                        {!! Form::text('codsig',(isset($personaemp->codigosig)==1?$personaemp->codigosig:''), array('class'=>'form-control input-sm','placeholder' => 'SIG','id'=>'sig','maxlength'=>'20') ) !!} 
                        </div>
                    </div>
                    <div class="row clsPadding2">
                        <div class="col-lg-6 col-xs-6">
                        <label class="clsTxtNormal">Apellidos paterno:</label>
                        {!! Form::text('apaterno',(isset($personanat->apaterno)==1?$personanat->apaterno:''), array('class'=>'form-control input-sm','placeholder' => 'Paterno','id'=>'apaterno','maxlength'=>'50') ) !!}  
                        </div>
                        <div class="col-lg-6 col-xs-6">
                        <label class="clsTxtNormal">Apellido Materno:</label>
                        {!! Form::text('amaterno',(isset($personanat->amaterno)==1?$personanat->amaterno:''), array('class'=>'form-control input-sm','placeholder' => 'Materno','id'=>'amaterno','maxlength'=>'50') ) !!} 
                        </div>
                    </div>
                    <div class="row clsPadding2">
                        <div class="col-lg-6 col-xs-6">
                         <label class="clsTxtNormal">Nombres:</label>
                        {!! Form::text('nombres',(isset($personanat->nombres)==1?$personanat->nombres:''), array('class'=>'form-control input-sm','placeholder' => 'Nombres','id'=>'nombres','maxlength'=>'50') ) !!} 
                        </div>
                        <div class="col-lg-6 col-xs-6">
                        <label class="clsTxtNormal">Alias:</label>
                        {!! Form::text('abreviatura',(isset($personanat->abreviatura)==1?$personanat->abreviatura:''), array('class'=>'form-control input-sm','placeholder' => 'Alias','id'=>'abreviatura','maxlength'=>'50') ) !!} 
                        </div>
                    </div>                    
                    <div class="row clsPadding2">
                        <div class="col-lg-4 col-xs-4">
                        <label class="clsTxtNormal">Estado Civil:</label>
                        {!! Form::select('estadocivil',(isset($testadocivil)==1?$testadocivil:array() ),(isset($personanat->estadocivil)==1?$personanat->estadocivil:''),array('class' => 'form-control select-box','id'=>'estadocivil')) !!} 
                        </div>
                        <div class="col-lg-4 col-xs-4">
                        <label class="clsTxtNormal">Género:</label>
                        {!! Form::select('genero',(isset($tgenero)==1?$tgenero:array() ),(isset($personanat->genero)==1?$personanat->genero:''),array('class' => 'form-control select-box','id'=>'genero')) !!} 
                        </div>
                        <div class="col-lg-4 col-xs-4">
                        <label class="clsTxtNormal">Fecha de Nacimiento::</label>
                            <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  {!! Form::text('fnacimiento',(isset($personanat->fnacimiento)==1?$personanat->fnacimiento:''), array('class'=>'form-control input-sm datepicker','placeholder' => '','id'=>'fnacimiento') ) !!} 
                                </div>    
                         </div>
                    </div>  
                    <div class="row clsPadding2">
                        <div class="col-lg-12 col-xs-12 clsTitulo">
                    	  Lugar de Nacimiento:
                        </div>  
                    </div>  
                    <div class="row clsPadding2">
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">Nacionalidad:</label>
                        {!! Form::select('pais_nac',(isset($tpais_nac)==1?$tpais_nac:array() ),(isset($personanat->cnacionalidad)==1?$personanat->cnacionalidad:''),array('class' => 'form-control','id'=>'pais_nac')) !!} 
                        </div>
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">Departamento:</label>
                        {!! Form::select('dpto_nac',(isset($tdpto_nac)==1?$tdpto_nac:array() ),(isset($cdpto_nac)==1?$cdpto_nac:''),array('class' => 'form-control','id'=>'dpto_nac')) !!}  
                        </div>
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">Provincia:</label>
                        {!! Form::select('prov_nac',(isset($tprov_nac)==1?$tprov_nac:array() ),(isset($cprov_nac)==1?$cprov_nac:''),array('class' => 'form-control','id'=>'prov_nac')) !!}  
                        </div>
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">Distrito:</label>
                        {!! Form::select('dis_nac',(isset($tdis_nac)==1?$tdis_nac:array() ),(isset($cubigeo_nac)==1?$cubigeo_nac:''),array('class' => 'form-control','id'=>'dis_nac')) !!} 
                        </div>  
                    </div>    
                    <div class="row clsPadding2">
                        <div class="col-lg-12 col-xs-12 clsTitulo">
                    	  Dirección:
                        </div>  
                    </div>  
                    <div class="row clsPadding2">
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">País:</label>
                        {!! Form::select('pais',(isset($tpais)==1?$tpais:array() ),(isset($personadir->cpais)==1?$personadir->cpais:''),array('class' => 'form-control','id'=>'pais')) !!} 
                        </div>
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">Departamento:</label>
                        {!! Form::select('dpto',(isset($tdpto)==1?$tdpto:array() ),(isset($cdpto)==1?$cdpto:''),array('class' => 'form-control','id'=>'dpto')) !!}  
                        </div>
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">Provincia:</label>
                        {!! Form::select('prov',(isset($tprov)==1?$tprov:array() ),(isset($cprov)==1?$cprov:''),array('class' => 'form-control','id'=>'prov')) !!}  
                        </div>
                        <div class="col-lg-3 col-xs-3">
                        <label class="clsTxtNormal">Distrito:</label>
                        {!! Form::select('dis',(isset($tdis)==1?$tdis:array() ),(isset($cubigeo)==1?$cubigeo:''),array('class' => 'form-control','id'=>'dis')) !!} 
                        </div>     
                        <div class="col-lg-6 col-xs-6">
                        <label class="clsTxtNormal">Dirección:</label> 
                        {!! Form::text('direccion',(isset($personadir->direccion)==1?$personadir->direccion:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'direccion','maxlength'=>'150') ) !!}  
                        </div>  
                        <div class="col-lg-6 col-xs-6">
                        <label class="clsTxtNormal">Referencia:</label> 
                        {!! Form::text('referencia',(isset($personadir->referencia)==1?$personadir->referencia:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'referencia','maxlength'=>'150') ) !!}  
                        </div>          
                        

                    </div> 
                    <div class="row clsPadding2">
                        <div class="col-lg-4 col-xs-4">
                        	<label class="clsTxtNormal">Teléfono:</label> 
                            <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            </span>
                            {!! Form::text('personatel_do',(isset($personatel_do->telefono)==1?$personatel_do->telefono:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'personatel_do') ) !!} 
                            </div>
                        </div>  
                        <div class="col-lg-4 col-xs-4">
                            <label class="clsTxtNormal">Celular:</label> 
                            
                            <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fa fa-mobile" aria-hidden="true"></i>
                            </span>
                            {!! Form::text('personatel_cel',(isset($personatel_cel->telefono)==1?$personatel_cel->telefono:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'personatel_cel','maxlength'=>'15') ) !!} 
                            </div>
                        </div>  
                        <div class="col-lg-4 col-xs-4">
                            <label class="clsTxtNormal">Email:</label> 
                            <div class="input-group">
                            <span class="input-group-addon">@</span>
                            {!! Form::text('personadir_ema',(isset($personadir_ema->direccion)==1?$personadir_ema->direccion:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'personadir_ema','maxlength'=>'150') ) !!} 
                            </div>
                        </div>      

                    </div>
                    <div class="row clsPadding2">
                        <div class="col-lg-6 col-xs-6">
                        	<label class="clsTxtNormal">Teléfono de Emergencia:</label> 
                            <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fa fa-phone" aria-hidden="true"></i>
                            </span>
                            {!! Form::text('personatel_eme',(isset($personatel_eme->telefono)==1?$personatel_eme->telefono:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'personatel_eme') ) !!} 
                            </div>    
                        </div>
                    	<div class="col-lg-6 col-xs-6">
                        	<label class="clsTxtNormal">Contacto:</label> 
                            <div class="input-group">
                            <span class="input-group-addon">
                            <i class="fa fa-user" aria-hidden="true"></i>
                            </span>
                            {!! Form::text('personatel_con',(isset($personatel_con->telefono)==1?$personatel_con->telefono:''), array('class'=>'form-control input-sm','placeholder' => '','id'=>'personatel_con') ) !!} 
                            </div>    
                        </div>    
                    </div>
                    <!-- FIN Datos Personales --> 

                    <div class="row clsPadding2">
                        <div class="col-lg-3 col-xs-12 clsPadding">
                            <button type="submit" class="btn btn-primary btn-block"><b>Grabar</b></button></div>
                        <div class="col-lg-3 col-xs-12 clsPadding"></div>
                                   
                    </div>               
                    {!! Form::close() !!}
              </div>
            
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2-2">
                <!-- Datos Familiares -->  
                {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmfamiliaemp')) !!}
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_famemp')) !!}
            
                {!! Form::hidden('cpersona_f',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_f')) !!} 
                {!! Form::hidden('cfamiliaemp','',array('id'=>'cfamiliaemp')) !!}          

              <div class="row clsPadding2">
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">DNI:</label>
                    <p> {{ (isset($persona->identificacion)==1?$personanat->identificacion:'') }}</p>
                    </div>
                    <div class="cofrmfamiliaempl-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Apellidos:</label>
                    <p>{{ (isset($personanat->apaterno)==1?$personanat->apaterno:'') }}
                    {{ (isset($personanat->amaterno)==1?$personanat->amaterno:'') }}
                    </p> 
                    </div>
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Nombres:</label>
                    <p>{{ (isset($personanat->nombres)==1?$personanat->nombres:'') }}</p>
                    </div>
                </div>
                <div class="row clsPadding2">
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">Parentesco:</label>
                    {!! Form::select('parentesco_fam',(isset($tparentesco)==1?$tparentesco:array() ),'',array('class' => 'form-control','id'=>'parentesco_fam')) !!}     

                    </div>
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">Apellidos y Nombres:</label>
                    <input type="text" class="form-control" id="" placeholder="Apellidos y Nombres" name='apaterno' >
                    <input type="text" class="form-control" id="" placeholder="Apellidos y Nombres" name='amaterno' >
                    <input type="text" class="form-control" id="" placeholder="Apellidos y Nombres" name='nombres' >
                    </div>
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">Género:</label>
                    {!! Form::select('genero_fam',(isset($tgenero)==1?$tgenero:array() ),'',array('class' => 'form-control','id'=>'genero_fam')) !!} 
                    </div>
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">DNI:</label>
                    <input type="text" class="form-control" id="identificacion_f" placeholder="DNI" name='identificacion'> 
                    
                    </div>
                </div>
                <div class="row clsPadding2">
                    <div class="col-lg-2 col-xs-2">Agregar Familiar</div>
                    <div class="col-lg-1 col-xs-2">
                    	<button type="button" class="btn btn-primary btn-block" id="saveFam">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        </button>
                    </div>
                    <div class="col-lg-9 col-xs-8"></div>
                </div> 
                <div class="row clsPadding2">	
                    <div class="col-lg-12 col-xs-12 table-responsive" id="divFamiliar">
                     @include('partials.tableModalFamiliaresEmpleado',array('tpersonadatosfamiliare'=> (isset($tpersonadatosfamiliare)==1?$tpersonadatosfamiliare:array() )))
                    </div>
                </div>     

                {!! Form::close() !!}              
                                
                <!-- Fin de Datos Familiares -->
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3-2">
                <!-- Datos de Estudio -->  

                {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmestudioemp')) !!}
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_estemp')) !!}
            
                {!! Form::hidden('cpersona_e',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_e')) !!} 
                {!! Form::hidden('cestudemp','',array('id'=>'cestudemp')) !!}   

                <div class="row clsPadding2">
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">DNI:</label>
                    <p> {{ (isset($persona->identificacion)==1?$personanat->identificacion:'') }}</p>
                    </div>
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Apellidos:</label>
                    <p>{{ (isset($personanat->apaterno)==1?$personanat->apaterno:'') }}
                    {{ (isset($personanat->amaterno)==1?$personanat->amaterno:'') }}
                    </p> 
                    </div>
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Nombres:</label>
                    <p>{{ (isset($personanat->nombres)==1?$personanat->nombres:'') }}</p>
                    </div>
                </div>   
                <div class="row clsPadding2">
                    <div class="col-lg-12 col-xs-12 clsTitulo">
                	  Estudios:
                    </div>  
                </div> 
                <div class="row clsPadding2">
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">Nivel:</label>
                    {!! Form::select('nivel_est',(isset($tnivel)==1?$tnivel:array() ),'',array('class' => 'form-control','id'=>'nivel_est')) !!}  
                    </div>
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">Carrera:</label>
                    {!! Form::select('carrera_est',(isset($tcarrera)==1?$tcarrera:array() ),'',array('class' => 'form-control','id'=>'carrera_est')) !!} 
                    </div>
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">Institución:</label>
                    {!! Form::select('instituto_est',(isset($tinstitucion)==1?$tinstitucion:array() ),'',array('class' => 'form-control','id'=>'instituto_est')) !!} 
                    </div>
                    <div class="col-lg-3 col-xs-3">
                    <label class="clsTxtNormal">Colegiatura:</label>
                    <input type="text" class="form-control" id="colegiatura_e" placeholder="Colegiatura" name="Colegiatura"> 
                    </div>    
                </div>           
                <div class="row clsPadding2">
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Estado:</label>
                    {!! Form::select('estado_est',(isset($testado)==1?$testado:array() ),'',array('class' => 'form-control','id'=>'estado_est')) !!} 
                    </div>
                  <div class="col-lg-4 col-xs-4">
                      <label class="clsTxtNormal">Fecha de Inicio:</label>
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          {!! Form::text('finicio','', array('class'=>'form-control input-sm datepicker','placeholder' => '','id'=>'finicio') ) !!}
                        </div>   
                    </div>
                  <div class="col-lg-4 col-xs-4">
                      <label class="clsTxtNormal">Fecha de Término:</label>
                      <div class="input-group date">
                          <div class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                          </div>
                          {!! Form::text('ffin','', array('class'=>'form-control input-sm datepicker','placeholder' => '','id'=>'ffin') ) !!}
                        </div>   
                    </div>    
                </div>
                <div class="row clsPadding2">
                    <div class="col-lg-2 col-xs-2">Agregar Estudios</div>
                    <div class="col-lg-1 col-xs-2">
                    	<button type="button" class="btn btn-primary btn-block"= id="saveEstud">
                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-lg-9 col-xs-8"></div>
                </div> 
                <div class="row clsPadding2">	
                    <div class="col-lg-12 col-xs-12 table-responsive" id="divEstud">
                    @include('partials.tableModalEstudiosEmpleado',array('tpersonadatosestudio'=> (isset($tpersonadatosestudio)==1?$tpersonadatosestudio:array() )))

                    </div>
                </div>

                {!! Form::close() !!}               
                                
                <!-- Fin de Datos de Estudio -->                
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_4-2">
                <!-- Inicio de Datos Medicos -->  
                {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmmedicemp')) !!}
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_medicemp')) !!}
            
                <div class="row clsPadding2">
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">DNI:</label>
                    <p> {{ (isset($persona->identificacion)==1?$personanat->identificacion:'') }}</p>
                    </div>
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Apellidos:</label>
                    <p>{{ (isset($personanat->apaterno)==1?$personanat->apaterno:'') }}
                    {{ (isset($personanat->amaterno)==1?$personanat->amaterno:'') }}
                    </p> 
                    </div>
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Nombres:</label>
                    <p>{{ (isset($personanat->nombres)==1?$personanat->nombres:'') }}</p>
                    </div>
                </div> 
                      <div class="row clsPadding2">
                        <div class="col-lg-2 col-xs-2">Grupo Sanguíneo</div>
                        <div class="col-lg-4 col-xs-4">
                        {!! Form::select('grupo_san',(isset($tgrupo_san)==1?$tgrupo_san:array() ),'',array('class' => 'form-control','id'=>'grupo_san')) !!}    
                        </div>    
                        <div class="col-lg-6 col-xs-6">
                          
                        </div>  
                        
                       </div> 

                       <div class="row clsPadding2"> 
                            <div class="col-lg-2 col-xs-2">
                            <label class="clsTxtNormal">Descripción:</label> </div>
                            <div class="col-lg-4 col-xs-4">

                             {!! Form::select('descrip_enf',(isset($tenfermedad)==1?$tenfermedad:array() ),'',array('class' => 'form-control','id'=>'descrip_enf')) !!}    

                            </div> 

                        <div class="col-lg-2 col-xs-2">
                            <label class="clsTxtNormal">Descripción:</label> </div>
                            <div class="col-lg-4 col-xs-4">

                            {!! Form::select('descrip_aler',(isset($talergia)==1?$talergia:array() ),'',array('class' => 'form-control','id'=>'descrip_aler')) !!}
                            </div>

                        </div>  

                        <div class="row clsPadding2"> 
                            <div class="col-lg-2 col-xs-2">
                            <label class="clsTxtNormal">Observación:</label> </div>
                            <div class="col-lg-4 col-xs-4">
                            <input type="text" class="form-control" id="observ_enf" placeholder="" name="observacion_enf"> 
                            </div> 

                        <div class="col-lg-2 col-xs-2">
                            <label class="clsTxtNormal">Observación:</label> </div>
                            <div class="col-lg-4 col-xs-4">
                            <input type="text" class="form-control" id="observ_aler" placeholder="" name="observacion_aler"> 
                        </div>
                        </div> 

                       <div class="row clsPadding2">
                            <div class="col-lg-6 col-xs-12">
                            <!-- Enfermadades -->

                            {!! Form::hidden('cpersona_enf',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_enf')) !!} 
                            {!! Form::hidden('cenfemp','',array('id'=>'cenfemp')) !!}  

                                <div class="row clsPadding2">
                                    <div class="col-lg-4 col-xs-4">Agregar Enfermedad</div>
                                    <div class="col-lg-2 col-xs-2">
                                        <a href="#" class="btn btn-primary btn-block " id="saveEnf">
                                        <i class="fa fa-medkit" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="col-lg-6 col-xs-6"></div>
                                </div> 
                                <div class="row clsPadding2">	
                                <div class="col-lg-12 col-xs-12 table-responsive" id = "divEnfermedad">
                                 @include('partials.tableModalEnfermedadesEmpleado',array('tpersonadatosmedicosenfermedad'=> (isset($tpersonadatosmedicosenfermedad)==1?$tpersonadatosmedicosenfermedad:array() )))
                                </div>
                                </div>   

                            
                                       
                            <!-- Fin Enfermadades -->


                            </div> 
                            <div class="col-lg-6 col-xs-12">
                            <!-- Alergias -->

                            {!! Form::hidden('cpersona_alerg',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_alerg')) !!} 
                            {!! Form::hidden('calergemp','',array('id'=>'calergemp')) !!} 
                               <div class="row clsPadding2">
                                    <div class="col-lg-4 col-xs-4">Agregar Alergia</div>
                                    <div class="col-lg-2 col-xs-2">
                                        <a href="#" class="btn btn-primary btn-block " id="saveAlerg">
                                        <i class="fa fa-stethoscope" aria-hidden="true"></i>
                                        </a>
                                    </div>
                                    <div class="col-lg-6 col-xs-6"></div>
                                </div> 
                                <div class="row clsPadding2">	
                                <div class="col-lg-12 col-xs-12 table-responsive" id="divAlergia">

                                @include('partials.tableModalAlergiasEmpleado',array('tpersonadatosmedicosalergia'=> (isset($tpersonadatosmedicosalergia)==1?$tpersonadatosmedicosalergia:array() )))
                                </div>
                                </div>         
                            <!-- Fin Alergias -->
                            </div>  
                      </div>           
                         
                    {!! Form::close() !!}                                  
                                    
                    <!-- Fin de Datos Medicos -->                
              </div>              
              <!-- /.tab-pane -->
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_5-2">
                <!-- Inicio datos laborales -->              
                <div class="row clsPadding2">
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">DNI:</label>
                    <p> {{ (isset($persona->identificacion)==1?$personanat->identificacion:'') }}</p>
                    </div>
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Apellidos:</label>
                    <p>{{ (isset($personanat->apaterno)==1?$personanat->apaterno:'') }}
                    {{ (isset($personanat->amaterno)==1?$personanat->amaterno:'') }}
                    </p> 
                    </div>
                    <div class="col-lg-4 col-xs-4">
                    <label class="clsTxtNormal">Nombres:</label>
                    <p>{{ (isset($personanat->nombres)==1?$personanat->nombres:'') }}</p>
                    </div>
                </div>
                 <div class="row clsPadding2">
                    <div class="col-lg-12 col-xs-12 clsTitulo">
                	  Anddes:
                    </div>  
                </div>     
                <div class="row clsPadding2">
                    <div class="col-lg-2 col-xs-2">Agregar Cargo</div>
                    <div class="col-lg-2 col-xs-2">
                        <a href="#" class="btn btn-primary btn-block ">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        </a>
                    </div>
                    <div class="col-lg-8 col-xs-8"></div>
                </div>          
                <div class="row clsPadding2">	
                    <div class="col-lg-12 col-xs-12 table-responsive">
                      <table class="table table-striped">
                        <thead>
                        <tr>
                            <td class="clsCabereraTabla">Área</td>
                            <td class="clsCabereraTabla">Sub Área</td>
                            <td class="clsCabereraTabla">Cargo</td>
                            <td class="clsCabereraTabla">Cod SIG</td>
                            <td class="clsCabereraTabla">Rate 1</td>
                            <td class="clsCabereraTabla">Rate 2</td>
                            <td class="clsCabereraTabla">Rate 3</td>
                            <td class="clsCabereraTabla">Rate 4</td>
                            <td class="clsCabereraTabla">Rate 5</td>
                            <td class="clsCabereraTabla">F Ingreso</td>
                            <td class="clsCabereraTabla">F. Término</td>
                            <td class="clsCabereraTabla">Estado</td>
                            <td class="clsCabereraTabla">Mótivo</td>
                            <td class="clsCabereraTabla">Acción</td>
                          </tr>  
                         </thead>
                         <tbody>
                         <tr>
                         	
                            <td class="clsAnchoTablaMax">
                            <select class="form-control" >
                                <option>Área 1</option>
                                <option>Área 2</option>
                                <option>Área 3</option>               
                            </select>            
                            </td>
                            <td class="clsAnchoTablaMax">
                            <select class="form-control" >
                                <option>Sub Área 1</option>
                                <option>Sub Área 2</option>
                                <option>Sub Área 3</option>               
                            </select>             
                            
                            </td>
                            <td class="clsAnchoTablaMax">
                            <select class="form-control" >
                                <option>Cargo 1</option>
                                <option>Cargo 2</option>
                                <option>Cargo 3</option>               
                            </select>             
                            </td>  
                            <td class="clsAnchoTablaMax">
                            <input type="text" class="form-control" id="" placeholder="Cod. SIG"> 
                            </td>            
                            <td class="clsAnchoTabla">
                            	<input type="text" class="form-control" id="" placeholder="Rate1"> 
                            </td>
                            <td class="clsAnchoTabla">
                            <input type="text" class="form-control" id="" placeholder="Rate2"> 
                            </td>
                            <td class="clsAnchoTabla">
                            <input type="text" class="form-control" id="" placeholder="Rate3"> 
                            </td>
                            <td class="clsAnchoTabla">
                            <input type="text" class="form-control" id="" placeholder="Rate4"> 
                            </td>
                            <td class="clsAnchoTabla">
                            <input type="text" class="form-control" id="" placeholder="Rate5"> 
                            </td>
                            <td class="clsAnchoTablaMax">
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="Inicio">
                                </div>                       
                            </td>
                            <td class="clsAnchoTablaMax">
                                <div class="input-group date">
                                  <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                  </div>
                                  <input type="text" class="form-control pull-right" id="datepicker" placeholder="Fin">
                                </div>                       
                            </td> 
                            <td class="clsAnchoTablaMax">
                            <select class="form-control" >
                                <option>Cargo 1</option>
                                <option>Cargo 2</option>
                                <option>Cargo 3</option>               
                            </select>             
                            </td> 
                             <td class="clsAnchoTablaMax">
                             <input type="text" class="form-control" id="" placeholder="Motivo"> 
                             </td>            
                            <td class="clsAnchoDia">
                                <a href="#" data-toggle="tooltip" data-container="body" title="Eliminar Cargo">
                                <i class="fa fa-trash" aria-hidden="true"></i></a>
                            </td>          
                         </tr>
                         </tbody>
                         </table>
                     </div>
                </div>
                <!-- Fin de datos laborales -->

              </div>             
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_6-2">
                <!-- Inicio de Experiencia Laboral -->  
                {!! Form::open(array('url' => '','method' => 'POST','id' =>'frmlaboral')) !!}
                {!! Form::hidden('_token',csrf_token(),array('id'=>'_token_laboral')) !!}
            
                    <div class="row clsPadding2">
                        <div class="col-lg-4 col-xs-4">
                        <label class="clsTxtNormal">DNI:</label>
                        <p> {{ (isset($persona->identificacion)==1?$personanat->identificacion:'') }}</p>
                        </div>
                        <div class="col-lg-4 col-xs-4">
                        <label class="clsTxtNormal">Apellidos:</label>
                        <p>{{ (isset($personanat->apaterno)==1?$personanat->apaterno:'') }}
                        {{ (isset($personanat->amaterno)==1?$personanat->amaterno:'') }}
                        </p> 
                        </div>
                        <div class="col-lg-4 col-xs-4">
                        <label class="clsTxtNormal">Nombres:</label>
                        <p>{{ (isset($personanat->nombres)==1?$personanat->nombres:'') }}</p>
                        </div>
                    </div> 
                      <div class="row clsPadding2">
                        <div class="col-lg-2 col-xs-2">Desde</div>
                        <div class="col-lg-4 col-xs-4">
                        <input type="text" class="form-control pull-right datepicker"  placeholder="Inicio">
                        </div>    
                        <div class="col-lg-2 col-xs-2">Hasta</div>
                        
                        <div class="col-lg-4 col-xs-4">
                        <input type="text" class="form-control pull-right datepicker"  placeholder="Fin">  
                        </div>  
                        
                       </div> 

                       <div class="row clsPadding2"> 
                            <div class="col-lg-2 col-xs-2">
                            <label class="clsTxtNormal">Empresa:</label> 
                            </div>
                            <div class="col-lg-4 col-xs-4">

                             <input type="text" name="exp_empresa" class="form-control" />

                            </div> 

                            <div class="col-lg-2 col-xs-2">
                                <label class="clsTxtNormal">Cargo:</label> 
                            </div>
                            <div class="col-lg-4 col-xs-4">
                                <input type="text" name="exp_cargo" class="form-control" />
                            </div>

                        </div>  

                        <div class="row clsPadding2"> 
                            <div class="col-lg-2 col-xs-2">
                            <label class="clsTxtNormal">Funciones:</label> </div>
                            <div class="col-lg-4 col-xs-4">
                            <textarea type="text" class="form-control" id="exp_funciones" placeholder="" name="exp_funciones"></textarea> 
                            </div> 

                            <div class="col-lg-2 col-xs-2">
                            <label class="clsTxtNormal">Observaciones</label> </div>
                            <div class="col-lg-4 col-xs-4">
                            <textarea type="text" class="form-control" id="exp_observaciones" placeholder="" name="exp_observaciones"></textarea>
                            </div>
                        </div> 
                        <div class="row clsPadding2"> 
                            <div class="col-md-2 col-xs-2">
                                <label class="clsTxtNormal">Referencias:</label>
                            </div>
                            <div class="col-md-4 col-xs-2">
                                <input type="text" name="exp_referencia" class="form-control" />
                            </div>           
                            <div class="col-md-6 col-xs-2">
                            </div>                                               
                        </div>                        

                       <div class="row clsPadding2">
                            <div class="col-lg-12 col-xs-12">
                                <!-- Enfermadades -->

                                {!! Form::hidden('cpersona_enf',(isset($persona->cpersona)==1?$persona->cpersona:''),array('id'=>'cpersona_enf')) !!} 
                                {!! Form::hidden('cenfemp','',array('id'=>'cenfemp')) !!}  

                                    <div class="row clsPadding2">
                                        <div class="col-lg-4 col-xs-4"></div>
                                        <div class="col-lg-2 col-xs-2">
                                            <a href="#" class="btn btn-primary btn-block " id="saveEnf">
                                            Agregar
                                            </a>
                                        </div>
                                        <div class="col-lg-6 col-xs-6"></div>
                                    </div> 
                                    <div class="row clsPadding2">	
                                    <div class="col-lg-12 col-xs-12 table-responsive" id = "divEnfermedad">
                                    @include('partials.tableModalExperienciaLaboral')
                                    </div>
                                    </div>
                                <!-- Fin Enfermadades -->
                            </div>
                      </div>
                    {!! Form::close() !!}
                    <!-- Fin de Datos Medicos -->                
              </div>              
              <!-- /.tab-pane -->               
            </div>
            <!-- /.tab-content -->
          </div>
<!--  fin accordeon -->        
    </div>
</div>
        <!-- <div class="row clsPadding2">
            <div class="col-lg-3 col-xs-12 clsPadding">
                <button class="btn btn-primary btn-block"><b>Grabar</b></button> </div>
            <div class="col-lg-3 col-xs-12 clsPadding">
                 </div>
                       
        </div> -->
        
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 
</div>

    </section>
    <!-- /.content -->
  </div>
<script>
        

        function Valida(){
            var result =true;
            
            if($('#dpto').val()=='' || $('#dpto').val()==null){
                alert('Especifique el Departamento de Residencia');
                result=false;
            }         
            if($('#prov').val()=='' || $('#prov').val()==null){
                alert('Especifique el Provincia de Residencia');
                result=false;
            }                 
            if($('#dis').val()=='' || $('#dis').val()==null){
                alert('Especifique el Distrito de Residencia');
                result=false;
            }
           
            return result;
        }

        $('#frmempleado').on('submit',function(e){
            // alert(123);         
            if(!Valida()){
                return false;
            }

            $.ajaxSetup({
                header: document.getElementById('_token').value
            });
            e.preventDefault(e);

            //$('input+span>strong').text('');
            $('input').parent().parent().removeClass('has-error');            
            $('select').parent().parent().removeClass('has-error');

            $.ajax({

                type:"POST",
                url:'grabarEmpleado',
                data:$(this).serialize(),
                /*dataType: 'json',*/
                beforeSend: function () {
                    
                    $('#div_carga').show(); 
            
                },
                success: function(data){
                     $(".alert-success").prop("hidden", false);
                     $('#div_carga').hide(); 
                     
                     $("#resultado").html(data);
                     $("#div_msg").show();

                },
                error: function(data){

                    $('#div_carga').hide();
                    $('#detalle_error').html(data);
                    $("#div_msg_error").show();
                }
            });
        });

        $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });
        //residencia
        $('#pais').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDpto/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    
                    $('#dpto').find('option').remove();
                    $('#prov').find('option').remove();
                    $('#dis').find('option').remove();
                    $('#dpto').append($('<option></option>').attr('value','').text(''));
                    
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dpto').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){
                }
            });
        });
        $('#dpto').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listProv/'+$('#pais').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);

                    $('#prov').find('option').remove();
                    $('#dis').find('option').remove();
                    $('#prov').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#prov').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){
                }
            });
        });
        $('#prov').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDis/'+$('#pais').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    $('#dis').find('option').remove();
                    $('#dis').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dis').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){

                }

            });
            
        })
        //nacimiento
        $('#pais_nac').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDpto/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    
                    $('#dpto_nac').find('option').remove();
                    $('#prov_nac').find('option').remove();
                    $('#dis_nac').find('option').remove();
                    $('#dpto_nac').append($('<option></option>').attr('value','').text(''));
                    
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dpto_nac').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){
                }
            });
        });

        $('#dpto_nac').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listProv/'+$('#pais_nac').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);

                    $('#prov_nac').find('option').remove();
                    $('#dis_nac').find('option').remove();
                    $('#prov_nac').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#prov_nac').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){
                }
            });
        });

        $('#prov_nac').change(function(){
            if(this.value==''){
                return;
            }
            $.ajax({
                type:'GET',
                url: 'listDis/'+$('#pais_nac').val()+'/'+this.value,
                success: function (data) {
                    var str = JSON.stringify(data);
                    var pushedData = jQuery.parseJSON(str);
                    $('#dis_nac').find('option').remove();
                    $('#dis_nac').append($('<option></option>').attr('value','').text(''));
                    $.each(pushedData, function(i, serverData)
                    {
                        //alert(i + '-' + serverData);
                        $('#dis_nac').append($('<option></option>').attr('value',i).text(serverData));
                    });
                },
                error: function(data){
                }
            });
        });
        /* Agregar Familiares*/
        $("#saveFam").click(function(e){
          $.ajax({
            url: 'saveFamEmpleado',
            type:'POST',
            data: $('#frmfamiliaemp').serialize() ,
              beforeSend: function () {
                  //$('#div_carga').show();
              },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divFamiliar').html(data);
            },
          });
        })
        /* Fin de agregar Familiares*/
        /* Agregar Estudios*/
        $("#saveEstud").click(function(e){
          $.ajax({
            url: 'saveEstuEmpleado',
            type:'POST',
            data: $('#frmestudioemp').serialize() ,
              beforeSend: function () {
                  //$('#div_carga').show();
              },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divEstud').html(data);
            },
          });
        })
        /* Fin de agregar Estudios*/
        /* Agregar Enfermedad*/
        $("#saveEnf").click(function(e){
          $.ajax({
            url: 'saveEnfEmpleado',
            type:'POST',
            data: $('#frmmedicemp').serialize() ,
              beforeSend: function () {
                  //$('#div_carga').show();
              },              
            success : function (data){
              //$('#div_carga').hide(); 
              $('#divEnfermedad').html(data);
            },
          });
        })
        /* Fin de agregar Enfermedad*/
         /* Agregar Alergia*/
        $("#saveAlerg").click(function(e){

          $.ajax({
            url: 'saveAlergEmpleado',
            type:'POST',
            data: $('#frmmedicemp').serialize() ,
              beforeSend: function () {
                  
                  //$('#div_carga').show();
              },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divAlergia').html(data);
            },
          });
        })
        /* Fin de agregar Alergia*/
        function eliminarEmpleadoFamiliar(id){
          $.ajax({
            url: 'eliminarFamiliarEmpleado/' + id,
            type:'GET',
            beforeSend: function () {
                //$('#div_carga').show();
            },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divFamiliar').html(data);
            },
          });          

        }
        function eliminarEmpleadoEstudio(id){
          $.ajax({
            url: 'eliminarEstudioEmpleado/' + id,
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show();
            },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divEstud').html(data);
            },
          });
        }
        function eliminarEmpleadoEnfer(id){
          $.ajax({
            url: 'eliminarEnfEmpleado/' + id,
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show();
            },              
            success : function (data){
              
              //$('#div_carga').hide(); 
              $('#divEnfermedad').html(data);
            },
          });          

        }
        function eliminarEmpleadoAlergia(id){
          $.ajax({
            url: 'eliminarAlergEmpleado/' + id,
            type:'GET',
            beforeSend: function () {
                
                //$('#div_carga').show();
            },              
            success : function (data){
              //$('#div_carga').hide(); 
              $('#divAlergia').html(data);
            },
          });
        }
</script>
<!--Inicion del Script para funcionamiento de Chosen -->
<script>
    $('.select-box').chosen(
        {
            allow_single_deselect: true
        });
</script>
<!-- Fin del Script para funcionamiento de Chosen -->