<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Solicitud Vacaciones</title>

</head>
<body>
    <br>
    <div style="font-size: 14px;font-family: Arial, Helvetica, sans-serif;">
        <div class="row" style="width:100%;"> 
            <div style="font-family: Arial, Helvetica, sans-serif;font-size: 12px;width:100%; border-collapse: collapse; size: 0.5px;">
                <table style="border-collapse: collapse; size: 0.5px;font-size: 10px;width: 100%">
                    <tr>
                        <td  rowspan="1" style="border:1px solid #000000FF; width: 20%;text-align: center;">
                            <img style="width: 300px;padding: 10px 0 10px 0;" src="images/logorpt.jpg">
                        </td>
                        <td rowspan="2" style="border:1px solid #000000FF; width: 60%;text-align: center;font-size: 20px">
                            <strong>GESTIÓN HUMANA<br> Solicitud de Vacaciones </strong>
                        </td>
                        <td rowspan="1"  style="border:1px solid #000000FF; width: 20%;font-size: 12px">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="text-align: center;"><strong></strong></td>
                                </tr>
                                <tr>
                                    <td>Revisión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                                </tr>
                                <tr>
                                    <td>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </td>
                                </tr>
                            </table>
                        </td>  
                    </tr>
                    <tr>
                        <td style="border:1px solid #000000FF; width: 20%;height: 25px;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 14px"> SIG AND </td>
                        <td style="border:1px solid #000000FF; width: 20%;height: 25px;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 14px"> 10-AND-43-FOR-0503 / R0 / 01/10/14 </td>
                    </tr>
                </table> 
                <div class="row" style="font-size: 24px">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" >
                        <form class="form-horizontal" style="border:1px solid #000000FF;">
                          <div class="form-group">
                          </div>
                          <div class="form-group">
                            <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9"></div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"><p>Fecha:</p></div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1">2018-05-25</div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>     
                          </div>
                          <div class="form-group">
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>  
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5">Nombre del Empleado Calos ochoa</div>
                            <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5"><p>Área / Departamento Geologia</p></div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1"></div>  
                               
                          </div>
                            
                        </form>
                    </div>
                </div>
                <form class="form-horizontal" style="border:1px solid #000000FF;">
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Nombre del Empleado</label>
                    <div class="col-sm-4">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="Calos ochoa">
                    </div>
                    <label for="inputPassword3" class="col-sm-2 control-label">Área / Departamento</label>
                    <div class="col-sm-4">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="Geologia">
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputEmail3" class="col-sm-1 control-label">Del</label>
                    <div class="col-sm-2">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="25-10-2018">
                    </div>
                    <label for="inputPassword3" class="col-sm-1 control-label">Al</label>
                    <div class="col-sm-2">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="25-11-2018">
                    </div>
                    <label for="inputPassword3" class="col-sm-1 control-label">N° de Días</label>
                    <div class="col-sm-1">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="30">
                    </div>
                    <label for="inputPassword3" class="col-sm-1 control-label">Periodo Vacacional</label>
                    
                    <div class="col-sm-3">
                      <input type="email" class="form-control" id="inputEmail3" placeholder="">
                    </div>    
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <div class="checkbox">
                        <label>
                          <input type="checkbox"> Remember me
                        </label>
                      </div>
                    </div>
                  </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-default">Sign in</button>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label">Email</label>
                    <div class="col-sm-10">
                      <p class="form-control-static">email@example.com</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="inputPassword" class="col-sm-2 control-label">Password</label>
                    <div class="col-sm-10">
                      <input type="password" class="form-control" id="inputPassword" placeholder="Password">
                    </div>
                  </div>
                </form>   
            </div>
        </div>
    </div>
</body>

</html>