@extends('layouts.master')
@section('content')
@push('evaluacion360periodostyle')
 <style type="text/css" media="screen">
    input[type="radio"] {
      -ms-transform: scale(1.8); /* IE 9 */
      -webkit-transform: scale(1.8); /* Chrome, Safari, Opera */
      transform: scale(1.8);
    }

    .listado-fixed {
      position:fixed;
      z-index:1000;
      top:0;
      max-width:1000px;
      width:100%;
      box-shadow:0px 4px 3px rgba(0,0,0,.5);
    }
 </style>
@endpush
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Gestión Humana
      <small>Evaluación de personal</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
      <li class="active">Gestión Humana</li>
    </ol>
  </section>
  <!-- Main content -->
  <section class="content">
    @if($usuariopermitido)
    <div class="row">
      <div class="col-lg-12 col-xs-12 text-center">
        <div class="alert alert-danger" role="alert" style="margin-top: 25px;">
          <strong style="font-size: 18px;">Su Usuario no tiene permitido hacer esta evaluacion o no existe la evaluación</strong>
        </div>
        <div class="text-center"> 
            <a href="{{ url('evaluacion') }}" title="Registrar una capacitación para un área o un grupo de usuarios del sistema, que opcionalmente tendrá una evaluación con preguntas.">
              <button type="submit" class="btn btn-primary btn-lg" >
                Volver a la encuesta
              </button>
            </a>
          </div>
      </div>  
    </div>
    @else
    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">
        <div class="row clsPadding2">
          <div class="col-lg-12 col-xs-12 clsTitulo">
            Evaluación de {{ $evaluados->abreviatura }} 
          </div>
          <div class="col-lg-12 col-xs-12 text-center">
            @if(!empty($errorad))
              <div class="alert alert-warning" role="alert" style="margin-top: 25px;">
                <strong style="font-size: 18px;">{{ $errorad }}</strong>
              </div>
            @endif
            @if(!empty($errortexto))
              <div class="alert alert-warning" role="alert" style="margin-top: 25px;">
                <strong style="font-size: 18px;">{{ $errortexto }}</strong>
              </div>
            @endif
            
          </div>   
        </div>
        <div class="row clsPadding2"> 
          <div class="col-lg-4 col-xs-12">
          </div>
          <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="getUrl('empleado');"  class="btn btn-primary btn-block"><b>Nuevo</b></button> --> 
          </div> 
          <div class="col-lg-3 col-xs-12">
          </div>
          <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
              <div class="panel-heading" role="tab" id="headingTwo" style="background-color: #0069aa;">
                <h4 class="panel-title" style="color:white;">
                  <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                    <span class="glyphicon glyphicon-hand-up"></span> Preguntas de competencias 
                    @if($preguntasad =='0')
                      <span class="label alert-success" style="font-size: 10px;">Preguntas Terminadas</span>
                    @else
                      <span class="label alert-danger" style="font-size: 10px;">Faltan por responder {{ $preguntasad }}</span>
                    @endif
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">
                  <div class="row clsPadding2" width="100%">  
                    <div class="col-lg-12 col-xs-12 table-responsive">
                      {!! Form::open(['url' => 'evaluacion/terminauno','method' => 'POST']) !!}
                      {!! Form::token(); !!}
                      <table class="table table-striped table-hover table-bordered" id="listado" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;">
                        <thead>
                          <tr id="titulotable">
                            <th class="clsCabereraTabla">Nro</th>
                            <th class="clsCabereraTabla">Pregunta</th>
                            <th class="clsCabereraTabla">A</th>
                            <th class="clsCabereraTabla">B</th>
                            <th class="clsCabereraTabla">C</th>
                            <th class="clsCabereraTabla">D</th>                    
                          </tr>
                        </thead>
                        <tbody>
                          @foreach($preguntas as $key => $pregunta)
                          <tr>
                            @if($pregunta->tipo_respuesta =='A al D')

                            <td class="text-center">{{ $key + 1}}</td>
                            <td>{{ $pregunta->detalle }}</td>
                            <td class="text-center">
                             <input type="hidden" name="idcab" value="{{ $idcab }}">
                             <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="1" @if($pregunta->id_respuesta == '1') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                           </td>
                           <td class="text-center">
                             <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="2" @if($pregunta->id_respuesta == '2') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                           </td>
                           <td class="text-center">
                            <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="3" @if($pregunta->id_respuesta == '3') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                          </td>
                          <td class="text-center">
                            <input type="radio" name="radio[{{ $pregunta->id }}]" id="inlineRadio1" value="4" @if($pregunta->id_respuesta == '4') checked @endif @if($pregunta->id_estado == '2') disabled="true" @endif> 
                          </td>
                          @endif
                        </tr>
                        @endforeach          
                      </tbody>

                    </table>
                    <tfooter class="text-center">
                      <div class="text-center">
                        @if($pregunta->id_estado == '2') 
                          <div class="alert alert-success" role="alert" style="margin-top: 25px;">
                            <strong style="font-size: 18px;">Encuesta terminada</strong>
                          </div>
                        @else
                          <button type="submit" class="btn btn-primary btn-lg">
                            Grabar
                          </button> 
                        @endif
                    </div>

                  </tfooter>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="panel panel-default">
          <div class="panel-heading" role="tab" id="headingThree" style="background-color: #0069aa;">
            <h4 class="panel-title" style="color:white;">
              <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                <span class="glyphicon glyphicon-hand-up"></span> Comentarios
                @if($preguntastexto =='0')
                  <span class="label alert-success" style="font-size: 10px;">Preguntas Terminadas</span>
                @else
                  <span class="label alert-danger" style="font-size: 10px;">Faltan por responder {{ $preguntastexto }}</span>
                @endif
              </a>
            </h4>
          </div>
          <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
            <div class="panel-body">
              <div class="row clsPadding2" width="100%">  
                <div class="col-lg-12 col-xs-12 table-responsive">
                  {!! Form::open(['url' => 'evaluacion/terminados','method' => 'POST']) !!}
                  {!! Form::token(); !!}
                  <table class="table table-striped" id="listado" width="100%">
                    <tbody>

                      @foreach($preguntas as $key => $pregunta)
                      <tr>
                        @if($pregunta->tipo_respuesta =='Textos')
                          @if($evaluadores)
                            <div class="text-center" style="margin-top: 15px;">
                              <h5 style="margin-bottom: 10px;font-size: 18px;"> {{ $pregunta->detalle }} </h5>
                              <input type="hidden" name="idcab" value="{{ $idcab }}">
                              <input type="hidden" name="iddet" value="{{ $pregunta->id }}">

                              <textarea style="font-size: 16px;" name="respuestatexto[{{ $pregunta->id }}]" class="form-control" rows="3" minlength="5" placeholder="Las respuestas deben ser mayores de 4 caracteres" @if($evaluadores->tipo_evaluador == 'Jefe Inmediato') required @endif @if($pregunta->id_estado == '2') disabled="true" @endif>{{ $pregunta->texto_respuesta }}</textarea>
                            </div>
                          @endif
                        @endif
                      </tr>
                      @endforeach          
                    </tbody>
                  </table>
                  <tfooter class="text-center">
                    <div class="text-center">
                      @if($evaluadores)
                        @if($pregunta->id_estado == '2') 
                          <div class="alert alert-success" role="alert" style="margin-top: 25px;">
                            <strong style="font-size: 14px;">Encuesta terminada</strong>
                          </div>
                        @else
                          <button type="submit" class="btn btn-primary btn-lg">
                            Grabar
                          </button> 
                        @endif
                      @endif
                    </div>
                  </tfooter>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="col-lg-12 col-xs-12">
        @if($pregunta->id_estado == '2')
          <div class="text-center"> 
            <a href="{{ url('evaluacion', $idcat) }}" title="Registrar una capacitación para un área o un grupo de usuarios del sistema, que opcionalmente tendrá una evaluación con preguntas.">
              <button type="submit" class="btn btn-primary btn-lg">
                Volver a la encuesta
              </button>
            </a>
          </div>
        @else
          {!! Form::open(['url' => 'evaluacion/finalizarencuesta','method' => 'POST']) !!}
          {!! Form::token(); !!}
          <div class="text-center">
            <input type="hidden" name="idcab" value="{{ $idcab }}">
            @if($evaluadores)
              <input type="hidden" name="tipoevaluador" value="{{ $evaluadores->tipo_evaluador }}">
            @endif
            @if($activobotonfinalizar)
              <button type="submit" class="btn btn-primary btn-lg" >
                Terminar
              </button>
            @endif
          </div>
          {!! Form::close() !!}                
        @endif
      </div>
    </div>
    @endif
    <div class="col-lg-1 hidden-xs"></div>
  </div>
</section>
</div>
 <!-- inicio para exportar -->
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script>
  $(document).ready(function(){
    alert("hola mundo");

      var altura = $('#titulotable').offset().top;
  
    $(window).on('scroll', function(){
      if ( $(window).scrollTop() > altura ){
        $('#titulotable').addClass('listado-fixed');
      } else {
        $('#titulotable').removeClass('listado-fixed');
      }
    });



  });
  </script>

@stop