<!--inicio del modal -->
<div class="modal fade" id="Modaledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel" >Observacion de hora extras</h4>
            </div>
            {!! Form::open(['url' => 'editsolicitud','method' => 'POST', 'id' => 'editsolicitud']) !!}
            {!! Form::token(); !!}
            <div class="modal-body">
                {{-- <label class="clsTxtNormal">Fecha y cantidad de horas:</label>  --}}
                @if(isset($solicitud))
                <input type="hidden" name="idjefeinmediato" id="idjefeinmediato" value="{{ $solicitud['idjefeinmediato'] }}">
                <input type="hidden" name="idempleado" id="idempleado" value="{{ $solicitud['idtpersonalogin'] }}">

                <input type="hidden" name="idsolicitud" id="idempleado" value="{{ $solicitud['id'] }}">
                <div class="row ">
                    <div class="col-lg-1 col-xs-1">Fecha:</div>
                    <div class="col-lg-6 col-xs-6">
                        <div class="input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input onchange="calculovacaciones()" type="text" name="fhorasextras" class="form-control pull-right input-sm"
                                   autocomplete="off" id="fhorasextras" readonly="readonly" required="true" value="{{$solicitud['fechahorasextras']}} ">
                        </div>
                        {{ $solicitud->observacion }}
                    </div>
                    <div class="col-lg-2 col-xs-2">Cantidad de Horas:</div>
                    <div class="col-lg-3 col-xs-3">
                        <input type="number" name="numerodehoras" class="form-control pull-right input-sm" min="1" max="24" value="{{$solicitud['horassolicitadas']}}" required="true">
                    </div>

                    <div class="col-lg-3 col-xs-3" style="margin-top: 10px;">Seleccione Proyecto:</div>
                    <div class="col-lg-12 col-xs-12">
                        <!-- single dropdown -->
                        <select id="proyectolistadohorasex" class="form-control select-box" name="proyectoid">
                            @if(isset($proyectos))
                                @foreach($proyectos as $proyecto)
                                    <?php $seleccionado='';
                                    if($solicitud['cproyecto']==$proyecto->cproyecto){
                                        $seleccionado = 'selected';
                                    }
                                    ?>
                                    <option {{$seleccionado}} value="{{ $proyecto->cproyecto}}">{{ $proyecto->codigo }} {{ $proyecto->nombre }}</option>
                                @endforeach
                            @endif
                        </select>
                    </div>
                        <div class="col-lg-12 col-xs-12">
                            <div class="col-lg-3 col-xs-3" style="margin-top: 10px;">Motivo:</div>
                                <div class="col-lg-12 col-xs-12">
                                    <textarea id="motivohorasextras" name="motivohorasextras" class="form-control" rows="3" placeholder="Motivo...."  maxlength="500" required="true">{{$solicitud['motivo']}}</textarea>
                                </div>
                        </div>
                @endif
            </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <button type="button" class="btn btn-primary" onclick="edithoras()">Guardar</button>
                </div>
                {!! Form::close() !!}
            </div>
    </div>
    </div>
</div>
<!--fin de modal de observacion-->