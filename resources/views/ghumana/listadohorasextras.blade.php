@extends('layouts.master')
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Solicitud de Vacaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">
<div class="row clsPadding2"> 
    <div class="col-lg-12 col-xs-12">
<!-- inicio accordeon -->       

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                
                {{-- <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Detalle de Vacaciones</a></li>  --}}
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Solicitud de Horas Extras</a></li>        
              <!--<li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                  Dropdown <span class="caret"></span>
                </a>
         <ul class="dropdown-menu">
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Another action</a></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Something else here</a></li>
                  <li role="presentation" class="divider"></li>
                  <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Separated link</a></li>
                </ul>
              </li>-->
              <li class="pull-left header"><i class="fa fa-newspaper-o"></i> Listado de Horas Extras solicitadas</li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1-1">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">DNI:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['identificacion'] }}" disabled="true"> 
                      </div>                    
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">Apellidos y Nombres:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['nombreusuario'] }}" disabled="true"> 
                      </div>
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">Cargo:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['cargouser'] }}" disabled="true"> 
                      </div>
                      <div class="col-lg-3 col-xs-12">
                        <label class="clsTxtNormal">Jefe Inmediato:</label>
                        <input type="text" class="form-control" id="" value="{{ $vauser['jefeinmediato'] }}" disabled="true"> 
                      </div>
                  </div>
                  <div class="col-12 row clsPadding2 text-center">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                      <button onclick="tipoanti()" id="botonancicipadas" type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModalva" style="font-size: 22px;">Solicitar Horas Extras</button>  
                    </div>
                  </div>

                  <div class="row clsPadding2">
                      <div class="col-lg-12 col-xs-12 table-responsive">
                    <table class="table table-striped table-hover table-bordered text-center" id="listadovaca" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;padding: 0px !important;">
                        <thead>
                        <tr class="clsCabereraTabla">
                          <th>Fecha</th>
                          <th>Dia de la semana</th>
                          <th>Horas solicitadas</th>
                          <th>Estado</th>
                          <th>Accion</th>
                        </tr>
                        </thead>
                        <tbody>
                          @foreach($solicitudes as $key => $solicitud)
                            <tr>
                              <td class="">{{ $solicitud->fechahorasextras }}</td>
                              <td  class="">{{ $solicitud->diasemana }}</td>
                              <td  class="">{{ $solicitud->horassolicitadas }}</td>
                              <td  class="">
                                @if( $solicitud->estadohe =='Aprobado JI' && $vauser['niveldeaprobacion']=='1' || $solicitud->estadohe =='Aprobado JS' && $vauser['niveldeaprobacion']=='2')
                                  <span class="label alert-success" style="font-size: 14px;">
                                    Aprobado
                                  </span>
                                @elseif( $solicitud->estadohe =='Observaciones' )
                                  <span class="label alert-warning" style="font-size: 14px;">
                                    {{ $solicitud->estadohe }}
                                  </span>
                                @elseif( $solicitud->estadohe =='Solicitado')
                                  <span class="label alert-info" style="font-size: 14px;">
                                    {{ $solicitud->estadohe }}
                                  </span>
                                @else
                                  <span class="label alert-info" style="font-size: 14px;">
                                    Solicitado
                                  </span>
                                @endif
                              </td>
                              <td  class="" tyle="color:#fff;">  
                                @if( $solicitud->estadohe =='Aprobado JI' && $vauser['niveldeaprobacion']=='1' || $solicitud->estadohe =='Aprobado JS' && $vauser['niveldeaprobacion']=='2')
                                  <a href="{{ url('pdfvacaciones/aprobadasvaca', $solicitud->idsolicitud) }}" style="color:#fff !important;" target="_blank">
                                    <button type="button" class="btn btn-primary" style="color:#fff !important;">
                                      Imprimir Comprobante
                                  </a>
                                @elseif( $solicitud->estadohe =='Observaciones' )
                                  
                                  <!-- Button trigger modal -->
                                  <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal_{{$key}}">
                                    {{ $solicitud->estadohe  }}
                                  </button>

                                  <!-- Modal -->
                                  <div class="modal fade" id="myModal_{{$key}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_{{$key}}">
                                    <div class="modal-dialog" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                          <h4 class="modal-title" id="myModalLabel">Observación</h4>
                                        </div>
                                        <div class="modal-body">
                                          {{ $solicitud->observacion  }}
                                        </div>
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                

                                @elseif( $solicitud->estadohe =='Solicitado')
                                  <button type="submit" class="btn btn-primary btn-block" disabled="true">Pendiente...</button> 
                                @else
                                  <button type="submit" class="btn btn-primary btn-block" disabled="true">Pendiente...</button>   
                                @endif  
                              </td>
                            </tr>
                          @endforeach                                              
                        </tbody>
                      </table>
                    </div>
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
          </div>
<!--  fin accordeon -->        
    </div>
</div>
<!--<div class="row clsPadding2">
    <div class="col-lg-3 col-xs-12 clsPadding">
        <a href="#" </class="btn btn-primary btn-block"><b>Grabar</b></a> </div>
    <div class="col-lg-3 col-xs-12 clsPadding">
        <a href="#" </class="btn btn-primary btn-block"><b>Cancelar</b></a> </div>
               
</div>-->
        
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 
</div>
</section>
<!-- /.content -->

<!-- Modal Vacaciones-->
<div class="modal fade" id="myModalva" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #0069aa;color: #fff;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title text-center" id="myModalLabel" >Solicitud Horas Extras </h4>
      </div>
      {!! Form::open(['url' => 'horasextras/solicitud','method' => 'POST', 'id' => 'solicitudhorasextras']) !!}
      {!! Form::token(); !!}
      <div class="modal-body">                
        {{-- <label class="clsTxtNormal">Fecha y cantidad de horas:</label>  --}}
        <input type="hidden" name="idjefeinmediato" id="idjefeinmediato" value="{{ $vauser['idjefeinmediato'] }}">
        <input type="hidden" name="idjefesuperior" id="idjefesuperior" value="{{ $vauser['idsuperjefe'] }}">
        <input type="hidden" name="idempleado" id="idempleado" value="{{ $vauser['idtpersonalogin'] }}">
        <div class="row ">
            <div class="col-lg-1 col-xs-1">Fecha:</div>
            <div class="col-lg-6 col-xs-6">
                <div class="input-group date">
                <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                </div>
                <input onchange="calculovacaciones()" type="text" name="fhorasextras" class="form-control pull-right input-sm" autocomplete="off" id="fhorasextras" required="true">
                </div>    
            </div>
            <div class="col-lg-2 col-xs-2">Cantidad de Horas:</div>
            <div class="col-lg-3 col-xs-3">
              <input type="number" name="numerodehoras" class="form-control pull-right input-sm" min="1" max="24" value="1" required="true">
            </div>
            <div class="col-lg-3 col-xs-3" style="margin-top: 10px;">Seleccione Proyecto:</div>
            <div class="col-lg-12 col-xs-12">
              <!-- single dropdown -->
              <select id="proyectolistadohorasex" class="form-control select-box" name="proyectoid">
                 @foreach($proyectos as $proyecto)
                    <option value="{{ $proyecto->cproyecto}}">{{ $proyecto->nombre }}</option>
                 @endforeach    
              </select>
            </div>  
            <div class="col-lg-3 col-xs-3" style="margin-top: 10px;">Motivo:</div>
            <div class="col-lg-12 col-xs-12">
              <textarea id="motivohorasextras" name="motivohorasextras" class="form-control" rows="3" placeholder="Motivo...." maxlength="500" required="true"></textarea>
            </div>    
          </div>   
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="enviohoras()">Solcitar</button>
      </div>
      {!! Form::close() !!} 
    </div>
  </div>
</div>

  </div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){

    $('.select-box').chosen(
    {
        allow_single_deselect: true,width:"100%"
    });

    $('#listadovaca').DataTable({
      "bFilter": true,
       "bSort": true,
       "bPaginate": true,
       "bInfo": true,
       "scrollY":        "600px",
       "scrollCollapse": true,
    });
    $('#listadovaca2').DataTable();

    $('#fhorasextras').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "tomorrow"
    }).datepicker("setDate",'now');  
    
  });



function enviohoras(){ 
  n =  new Date();
  y = n.getFullYear();
  m = n.getMonth() + 1;
  d = n.getDate();
  //alert(document.getElementById("fhorasextras").value);

  if(document.getElementById("fhorasextras").value ==''){
      alert('Deb colocar una fecha para enviar la solicitud');
      return false;
  }
 
  // swal($('#tipovaca').val());
  document.getElementById("solicitudhorasextras").submit();
  return true;
}






</script>





@stop