@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Aprobacion de Horas Extras</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">

<div class="row clsPadding2"> 
    <div class="col-lg-12 col-xs-12">
<!-- inicio accordeon -->       

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Horas extras Pendientes</a></li>
                <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Horas extras Aprobadas</a></li>
                <li class=""><a href="#tab_3-3" data-toggle="tab" aria-expanded="false">Horas extras con Observadas</a></li>           
              <li class="pull-left header"><i class="fa fa-newspaper-o"></i> Listado de horas extras para aprobar</li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane 1-1 -->  
              <div class="tab-pane active" id="tab_1-1">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                   
                  {{-- @if(count($resultado)>0)
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div class="alert alert-success">
                        <ul>
                          <li> Total de registros insertados: {{ $resultado }}</li>
                        </ul>
                      </div>
                    </div>
                  @endif   --}}
                      <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-center" id="listadovacacionesparaabrobar" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;">
                                  <thead>
                                  <tr class="clsCabereraTabla">
                                    <th>Colaborador</th>
                                    <th>Fecha</th>
                                    <th>Dia de la semana</th>
                                    <th>Horas solicitadas</th>
                                    <th>Estado</th>
                                    <th>Accion</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($solicitudes as $aprobar)
                                      <tr>
                                        <td class="clsAnchoTabla">{{ $aprobar->nombre }}</td>
                                        <td class="clsAnchoTabla">{{ $aprobar->fechahorasextras }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->dia }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->horassolicitadas }}</td>
                                        <td  class="clsAnchoDia"><span class="label alert-success" style="font-size: 14px;"> {{ $aprobar->estadohe }}</span></td>
                                        <td  class="clsAnchoDia">
                                          <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal_{{ $aprobar->id }}">
                                        Pendientes
                                      </button>
                                      <!-- Modal -->
                                      <div class="modal fade" id="myModal_{{ $aprobar->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_{{ $aprobar->id }}">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="myModalLabel_{{ $aprobar->id }}">Solicitud de horas extras {{ $aprobar->nombre }} </h4>
                                            </div>
                                            {!! Form::open(['url' => 'horasextras/aprobadasjefes','method' => 'POST', 'id' => 'apruebohorasextras', 'class' => 'form-inline']) !!}
                                              <div class="modal-body">
                                              
                                              <input  type="hidden" id="proyectoid" name="proyectoid" value="{{ $aprobar->cproyecto }}">
                                              <input  type="hidden" id="idhorasextrassolicitud" name="idhorasextrassolicitud" value="{{ $aprobar->id }}">
                                              <input  type="hidden" id="dia" name="dia" value="{{ $aprobar->dia }}">  
                                              <input  type="hidden" id="rechazar" name="rechazar" value="false">  
                                                <div class="row clsPadding2">
                                                  <div class="col-lg-4 col-xs-12">
                                                    <label class="clsTxtNormal">Colaborador:</label>
                                                    <input type="text" class="form-control text-center" id="" value="{{ $aprobar->nombreempleado }}" disabled="true">
                                                  </div>                    
                                                  <div class="col-lg-4 col-xs-12">
                                                    <label class="clsTxtNormal">Fecha</label>
                                                    <input type="text" class="form-control text-center" id="" value="{{ $aprobar->fechahorasextras }}" disabled="true"> 
                                                  </div>

                                                  <div class="col-lg-4 col-xs-12">
                                                    <label class="clsTxtNormal">Horas Solicitadas:</label>
                                                    <input type="text" class="form-control text-center" id="" value="{{ $aprobar->horassolicitadas }}" disabled="true"> 
                                                  </div>
                                                  <div class="col-lg-12" style="margin-top: 10px;">
                                                    <div class="col-lg-12 col-xs-12">
                                                      <label class="col-lg-12 text-left clsTxtNormal">Proyecto:</label>
                                                      <input type="text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-control text-center" id="" value="{{ $aprobar->codigo }} {{ $aprobar->nombre }}" disabled="true" style="width: 100% !important;">
                                                    </div> 
                                                  </div>
                                                  <div class="col-lg-12" style="margin-top: 10px;">
                                                    <div class="col-lg-12 col-xs-12">
                                                      <label class="col-lg-12 text-left clsTxtNormal">Motivo:</label>
                                                      <input type="text" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 form-control text-center" id="" value="{{ $aprobar->motivo }}" disabled="true" style="width: 100% !important;"> 
                                                    </div> 
                                                  </div>
                                                  <div id="divobservacion" class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                                    <span class="pull-left alert-warning" style="font-size: 12px;margin-top: 15px;margin-left: 45px; padding-left: 5px;">Observaciones obligatorias al momento de corregir las vacaciones</span>
                                                      <textarea id="observacionrechazohorasextras_{{ $aprobar->id }}" name="observacionrechazohorasextras" class="form-control" rows="5" style="max-width: 450px;min-width: 450px;margin-top: 10px;margin-bottom: 10px;"></textarea>
                                                  </div>
                                                </div>
                                                <div class="form-group">
                                                  <div class="has-success">
                                                    <div class="checkbox">
                                                      <label>
                                                        <input type="checkbox" id="checkboxSuccess" value="option1" required="true">
                                                        Seguro de aprobar esta solicitud
                                                      </label>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer" style="margin-top: 5px;">
                                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>   
                                              <button onclick="rechazoypregunto({{ $aprobar->id }});" id="botonrechazo" type="button" class="btn btn-warning">Corregir</button> 
                                              <button type="submit" class="btn btn-success">Aprobar</button>
                                            </div>
                                          {!! Form::close() !!}     
                                          </div>
                                        </div>
                                      </div>
                                        </td>
                                      </tr>
                                    @endforeach                                                 
                                  </tbody>
                        </table>
                      </div>                            
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
              <!-- /.tab-pane  1-1-->
              <!-- /.tab-pane  2-2-->
              <div class="tab-pane" id="tab_2-2">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                      <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-center" id="listadovacacionesparaabrobar" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;">
                                  <thead>
                                  <tr class="clsCabereraTabla">
                                    <th>Colaborador</th>
                                    <th>Fecha</th>
                                    <th>Dia de la semana</th>
                                    <th>Horas solicitadas</th>
                                    <th>Estado</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($solicitudesporaprobadas as $aprobar)
                                      <tr>
                                        <td class="clsAnchoTabla">{{ $aprobar->nombre }}</td>
                                        <td class="clsAnchoTabla">{{ $aprobar->fechahorasextras }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->dia }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->horassolicitadas }}</td>
                                        <td  class="clsAnchoDia">
                                          <span class="label alert-success" style="font-size: 14px;"> {{ $aprobar->estadohe }}</span>
                                        </td>
                                      </tr>
                                    @endforeach                                                 
                                  </tbody>
                        </table>
                      </div>                            
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
              <!-- /.tab-pane  2-2-->  
              <!-- /.tab-pane  3-3-->  
              <div class="tab-pane" id="tab_3-3">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                      <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-center" id="listadovacacionesparaabrobar" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;">
                          <thead>
                          <tr class="clsCabereraTabla">
                            <th>Colaborador</th>
                            <th>Fecha</th>
                            <th>Dia de la semana</th>
                            <th>Horas solicitadas</th>
                            <th>Observacion</th>
                          </tr>
                          </thead>
                          <tbody>
                            @foreach($solicitudesobservadas as $aprobar)
                              <tr>
                                <td class="clsAnchoTabla">{{ $aprobar->nombre }}</td>
                                <td class="clsAnchoTabla">{{ $aprobar->fechahorasextras }}</td>
                                <td  class="clsAnchoDia">{{ $aprobar->dia }}</td>
                                <td  class="clsAnchoDia">{{ $aprobar->horassolicitadas }}</td>
                                <td  class="clsAnchoDia" style="min-width:500px;width: 500px;-webkit-box-sizing:border-box;-moz-box-sizing: border-box;box-sizing:border-box;"><textarea style="width:100%;-webkit-box-sizing:border-box;-moz-box-sizing: border-box;box-sizing:border-box;" disabled="true" readonly>{{ $aprobar->observacion }}</textarea></td>
                              </tr>
                            @endforeach                                                 
                          </tbody>
                        </table>
                      </div>                            
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
              <!-- /.tab-pane  3-3-->      
            </div> 
          </div>
<!--  fin accordeon -->        
    </div>
</div>
</div>
    <div class="col-lg-1 hidden-xs"></div>
</div>
</section>
<!-- /.content -->
</div>


<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
     $('#listadovacacionesparaabrobar').DataTable({
        "bFilter": true,
         "bSort": true,
         "bPaginate": true,
         "bInfo": true,
         "scrollY":        "600px",
         "scrollCollapse": true,
      });
     $('#listadovacacionesparaabrobar').DataTable();

     $('#listadovacacionesparaabrobar2').DataTable();

     $('#listadovacacionesparaabrobar3').DataTable();

    $('#fdesde').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "tomorrow"
    }).datepicker("setDate",'now');  

    $('#fhasta').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "+1d",
    }).datepicker("setDate",'+1d');
  });

</script>

<script type="text/javascript">

function rechazoypregunto($id) {

  var id = $("#idhorasextrassolicitud").val();
  divObservacion = $('#divobservacion');
  observaciones = divObservacion.find('textarea[id="observacionrechazohorasextras_'+id+'"]').val();
  //alert(observaciones);

  if (observaciones =='' ) {
    alert('Debe colocar observaciones.');
    return false;
  } else {
    confirm = confirm("Esta totalmente seguro de Observar esta solicitud!");
    
    if (confirm==true) {
      //document.getElementById("rechazar").value = "true";
      $("#rechazar").attr("value","true");
      //alert($('#rechazar').val());
      $('#apruebohorasextras').submit();
      return true;
      //alert('llegue al sumit');
    } else {
      return false;
    }
  }
}
</script>
@stop