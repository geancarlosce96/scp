
<html>
    <head>
        <style>
            /*@page { margin: 60px 60px; }*/
            #footer { position: fixed; right: : 0px; bottom: -120px; right: 0px; height: 150px;}
            #footer .page:after { content: counter(page, upper-decimal); }
        </style>
        
    </head>
    


<body>
<div style=" font-family: Arial, Helvetica, sans-serif;font-size: 9.5px;width:100%; border-collapse: collapse; size: 0.5px;">           
    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 10px;width: 100%">

            <tr>
                <td  rowspan="1" style="border:1px solid #000000FF; width: 20%;text-align: center;"><img style="width: 150px;" src="images/logorpt.jpg"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 60%;text-align: center;font-size: 11px"><strong>GESTIÓN HUMANA <br> Solicitud de Vacaciones </strong></td>
                <td rowspan="1"  style="border:1px solid #000000FF; width: 20%;font-size: 9px">

                    <table style="width: 100%;">
                        <tr>
                            <td style="text-align: center;"><strong></strong></td>
                        </tr>

                        <tr>
                            <td>Revisión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                        </tr>

                        <tr>
                            <td>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </td>
                        </tr>
                        
                    </table>

                </td>  
                
            </tr>
           
            <tr>
                
                <td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> SIG AND </td>
                <td style="border:1px solid #000000FF; width: 20%;text-align: center;color: #fff;background-color:rgba(0, 105, 170, 1);font-size: 8px"> 10-AND-43-FOR-0503 / R0 / 01/10/14 </td>
            </tr>

         
            

        </table>



    </div>


    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="3"  style="border:1px solid #000000FF;width: 15%;">Nombre del Proyecto</td>

                <td rowspan="3" style="border:1px solid #000000FF;width: 50%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Proyecto N°</td>
                <td style="border:1px solid #000000FF; width: 20%;"></td>
            </tr>
            <tr>
                
                
                <td style="border:1px solid #000000FF; width: 15%;">Propuesta N°</td>
                <td style="border:1px solid #000000FF; width: 20%;"></td>
            </tr>
            <tr>
                
            
                <td style="border:1px solid #000000FF; width: 15%;">Fecha de adjudicación propuesta</td>
                <td style="border:1px solid #000000FF; width: 20%;"></td>
            </tr>   

                    
        </table>
    </div>


    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td rowspan="3" style="border:1px solid #000000FF; width: 15%;">Descripción del Proyecto</td>
                <td rowspan="3" style="border:1px solid #000000FF; width: 85%;"></td>
            </tr>
        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Descripción Web</td>
                <td style="border:1px solid #000000FF; width: 85%;"></td>
            </tr>          
        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Cliente</td>
                <td style="border:1px solid #000000FF; width: 40%;"></td>                
                <td style="border:1px solid #000000FF; width: 15%;">Cliente final</td>
                <td style="border:1px solid #000000FF; width: 30%;"></td>
            </tr>      

        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Unidad Minera</td>
                <td style="border:1px solid #000000FF; width: 40%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Lugar de trabajo</td>
                <td style="border:1px solid #000000FF; width: 30%;"></td>
            </tr>      

        </table>
    </div>

    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">                 
            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Portafolio de Servicios</td>
                <td style="border:1px solid #000000FF; width: 40%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Tipo de Proyecto</td>
                <td style="border:1px solid #000000FF; width: 30%;"></td>
            </tr>      

        </table>
    </div>


    <div class="row" >

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Tipo de gestión de proyectos</td>
                <td style="border:1px solid #000000FF;width: 25%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 30%;background-color:rgba(0, 105, 170, 1);"><div style="text-align: center;color: white"><h4>Planificado</h4></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 30%;background-color:rgba(0, 105, 170, 1);"><div style="text-align: center;color: white"><h4>Real</h4></div></td>
            </tr>
            <tr > 
                <td style="border:1px solid #000000FF;width: 15%;"">Gerente de Proyecto</td>
                <td style="border:1px solid #000000FF;width: 15%;"></td>
            </tr>           
                    
        </table>                

    </div>

    <div class="row">

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Líder de Disciplina 1</td>
                <td style="border:1px solid #000000FF;width: 25%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha Inicio</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha Inicio</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"></td>
            </tr>

            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Líder de Disciplina 2</td>
                <td style="border:1px solid #000000FF;width: 25%;"></td>
            </tr>

            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Control de Proyecto</td>
                <td style="border:1px solid #000000FF;width: 25%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha Cierre</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;">Fecha Cierre</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 20%;"></td>
            </tr>
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;"">Control Documentario</td>
                <td style="border:1px solid #000000FF;width: 25%;"></td>
            </tr>
        </table>                
    </div>
    <!--<div class="row">
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            <tr style="text-align: center">
                <td style="border:1px solid #000000FF;width: 15%;"">Control Documentario</td>
                <td style="border:1px solid #000000FF;width: 85%;"></td>           
            </tr>
        </table>
    </div>-->
    <div class="row"> 
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            <tr>
                <td style="border:1px solid #000000FF;width: 100%;color: #fff;background-color:rgba(0, 105, 170, 1);text-align: center;" class="clsCabereraTabla"><strong>Información Comercial</strong>
                </td>
            </tr>
        </table>
    </div>
    <div class="row">           
        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">Nombre contacto</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;">></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 15%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Presupuesto</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Anddes</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Sub Cont 01</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Sub Cont 02</strong></div></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 7%;background-color:rgba(0, 105, 170, 1);color: white"><div style="text-align: center;"><strong>Total <br>Presupuesto</strong></div></td>
            </tr>  
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">Cargo</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Moneda</td>
                <td style="border:1px solid #000000FF; width: 10%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 7%;"></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Honorarios</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"></td>
            </tr>                       
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%"><?php //dd($proy_adicional); ?>
            
            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">Teléfono</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Anexo</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Gastos</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Laboratorio</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"></td>
            </tr>   

            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">Celular 1</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Celular 2</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Otros</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">Descuento</td>
                <td style="border:1px solid #000000FF; width: 10%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 7%;text-align: right;font-weight: bold"></td>
            </tr>                   
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">E-mail</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;;color:black;background-color:rgba(180, 180, 180, 1);font-weight: bold">Total</td>
                <td style="border:1px solid #000000FF; width: 10%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"></td>
                <td style="border:1px solid #000000FF; width: 7%;color:black;background-color:rgba(180, 180, 180, 1);text-align: right;font-weight: bold"></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">SOC 01</td>
                <td style="border:1px solid #000000FF; width: 10%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 7%;"></td>
            </tr>       
            <tr>
                <td rowspan="2"  style="border:1px solid #000000FF;width: 15%;">Razón social</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 40%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">SOC 02</td>
                <td style="border:1px solid #000000FF; width: 10%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 7%;"></td>
            </tr>   

            <tr>                        
                <td style="border:1px solid #000000FF; width: 15%;">SOC 03</td>
                <td style="border:1px solid #000000FF; width: 10%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 6.5%;"></td>
                <td style="border:1px solid #000000FF; width: 7%;"></td>
            </tr>                   
        </table>
    </div>


    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">RUC</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Télefono</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 15%;;color:black;background-color:rgba(180, 180, 180, 1);font-weight: bold">Total + SOC</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 10%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 6.5%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 7%;color:black;background-color:rgba(180, 180, 180, 1);"></td>
            </tr>   

        </table>                    

    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Dirección fiscal</td>
                <td style="border:1px solid #000000FF;width: 40%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Adelanto %</td>
                <td style="border:1px solid #000000FF; width: 0%;"></td>
            
            </tr>   

                                
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;">Documento de aprobación</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 17%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 8%;">Número de <br> documento</td>
                <td rowspan="2" style="border:1px solid #000000FF;width: 15%;"></td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 15%">Tipo de contrato</td>
                <td rowspan="2" style="border:1px solid #000000FF; width: 30%;"></td>
                
            </tr>   
        </table>                

    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Comentarios</td>
                <td style="border:1px solid #000000FF;width: 17%;"><</td>
                <td style="border:1px solid #000000FF;width: 23%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Plazo de pago de facturas</td>
                <td style="border:1px solid #000000FF; width: 30%;"></td>
                
            </tr>   
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Foto proyecto (link ruta)</td>
                <td style="border:1px solid #000000FF;width: 17%;"></td>
                <td style="border:1px solid #000000FF;width: 23%;"></td>
                <td style="border:1px solid #000000FF; width: 15%;">Fecha de corte</td>
                <td style="border:1px solid #000000FF; width: 30%;"></td>
                
            </tr>   

                                
        </table>
    </div>

    <div class="row">           

        <table style="border-collapse: collapse; size: 0.5px;font-size: 9.5px;width: 100%">
            
            <tr>
                <td style="border:1px solid #000000FF;width: 15%;">Observaciones</td>
                <td  style="border:1px solid #000000FF;width: 85%;"></td>
                
            </tr>   
            

                                
        </table>
    </div>

 

</div>

  <div id="footer" style="text-align: right;color:rgba(0, 105, 170, 1);font-size: 11px">
    <p class="page">Página 1 de </p>
  </div>
</body>


    
</html>





