@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Listado de Evaluaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">
        <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">
          Lista de Auditoria
            </div>  
        </div>
        <div class="row clsPadding2">	
            <div class="col-lg-3 col-xs-12">
            </div>
            <div class="col-lg-3 col-xs-12">
            <!-- <a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> -->
            </div> 
            <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="getUrl('empleado');"  class="btn btn-primary btn-block"><b>Nuevo</b></button> --> 
            </div> 
            <div class="col-lg-3 col-xs-12">
        </div>
         <div class="form-group col-lg-12">
      	 <label class="control-label" for="inputSuccess1">Seleccione un Usuario Evaluador</label>		 
            {!! Form::open(['url' => 'auditoriaevaluaciones','method' => 'GET','class' => 'form-inline text-center']) !!}
			    <select name="evaluador" class="form-control col-md-10 col-lg-10" id="selectevaluador">
        		<option value="">Seleccionar una Opcion</option>
		        	@foreach($evaluadores as $evaluador)
		        		<option value="{{ $evaluador->dni_evaluador }}">{{ $evaluador->abreviatura_evaluador}}</option>
		        	@endforeach	
				</select>

			    <button type="submit" class="form-control btn btn-primary"style="margin-top: 15px;">Buscar Evaluaciones</button>
			{!! Form::close() !!}
        </div>
        @if(!is_null($evaluados))
        	<div class="row clsPadding2" width="100%">	
	            <div class="col-lg-12 col-xs-12 table-responsive">
	              <table class="table table-striped table-hover table-bordered" id="listadoevaluacion" width="100%" style="font-size: 12px;">
	              	<thead>
	              		<td class="clsCabereraTabla">Evaluador</td>
	                	<td class="clsCabereraTabla">Evaluado</td>
	                  	<td class="clsCabereraTabla">Cargo</td>
	                  	<td class="clsCabereraTabla">Tipo evaluador</td>
	                  	<td class="clsCabereraTabla">Accion</td>
	                </thead>
	                <tbody>
	                	@foreach($evaluados as $evaluado)
	                		<tr> 
	                			<td class="text-center">{{ $evaluado->abreviatura_evaluador }}</td>
	                		 	<td class="text-center">{{ $evaluado->abreviatura_evaluado }}</td>
	                		 	<td class="text-center">{{ $evaluado->descripcion }}</td>  
	                		 	<td class="text-center">{{ $evaluado->tipo_evaluador }}</td>  
	                		 	<td class="text-center">
	                		 		<!-- Button trigger modal -->
									<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal_{{$evaluado->idcab }}">
									  Ver Preguntas
									</button>

									<!-- Modal -->
									<div class="modal fade bs-example-modal-lg" id="myModal_{{$evaluado->idcab }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
									  <div class="modal-dialog modal-lg" role="document">
									    <div class="modal-content">
									      <div class="modal-header" style="background-color: #0069aa;color: #fff;">
									        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
									        <h4 class="modal-title" id="myModalLabel">Preguntas {{ $evaluado->abreviatura_evaluado }} </h4>
									      </div>
									      <div class="modal-body">
									        <table class="table table-striped table-hover table-bordered" id="listadoevaluacion2" width="100%" style="font-size: 12px;">
									        	<thead>
									        		<td class="clsCabereraTabla">Todos las preguntas</td>
									        		<tbody>
									        			@foreach($evaluado->preguntas as $pregunta)
									        				<tr>
									        					<td>{{ $pregunta->detalle }}</td>
									        				</tr>
									        			@endforeach
									        		</tbody>
												</thead>
									        </table>
									      </div>
									      <div class="modal-footer">
									        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									      </div>
									    </div>
									  </div>
									</div>
	                		 	</td>  

					  		</tr> 
	                	@endforeach         
	                </tbody>
	              </table>
	                <div>

					</div>
	             </div>
	        </div>
        @endif
        
      </div>
      <div class="col-lg-1 hidden-xs"></div>
    </div>
    </section>
    <!-- /.content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>
  $(document).ready(function(){
    var table = $('#listadoevaluacion').DataTable( {
         "paging":   true,
         "ordering": false,
         "info":     true,
        // "order":     [[0,"desc"]],
         lengthChange: true
         //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]

     } );  

    addchosen("selectevaluador");
   } );   
</script>
<script type="text/javascript">

function totalmenteseguro() {
  event.preventDefault();
  var casilisto = confirm("¿Esta totalmente seguro de enviar esta Encuesta?. Al terminarla no podra modificar los datos");
  //alert(casilisto);
  //return 'epale miguuel carlos';
  if (casilisto) {
    $("#terminoform2").submit();
  } else {
    return false;
  }
}


</script>
@stop