<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestion Humana
        <small>Reporte Gestion humana</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestion Humana</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">

    {!! Form::open(array('url' => 'grabarCliente','method' => 'POST','id' =>'frmreporte')) !!}


<div class="row clsPadding2">
    <div class="col-lg-1 col-xs-2">Reporte</div>
        <div class="col-lg-3 col-xs-3">
           <input type="text" class="form-control" id="nombre" placeholder="" name="nombre"> 
        </div>
             
        <div class="col-lg-7 col-xs-7">
            {!! Form::select('ireporte',(isset($reporte)==1?$reporte:array() ),'',array('class' => 'form-control','id'=>'ireporte')) !!}        

        </div>
</div>

<div class="row clsPadding2"> 
    <div class="col-lg-1 col-xs-2">Formato</div>
        <div class="col-lg-2 col-xs-2">
         {!! Form::select('iformato',(isset($formatodoc)==1?$formatodoc:array() ),'',array('class' => 'form-control','id'=>'iformato')) !!}   
        </div>
</div>

<div class="row clsPadding2">
    @include('partials.verEmpleadoRep',array('tpersona'=>(isset($tpersona)==1?$tpersona:null) ))    
</div>

<div class="row clsPadding2"> 
    <div class="col-lg-1 col-xs-2">Fecha</div>
        <div class="col-lg-3 col-xs-4">
          <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                       {!! Form::text('fdesde','', array('class'=>'form-control input-sm datepicker','placeholder' => '','id'=>'fdesde') ) !!}
                    </div>  
        </div>

         <div class="col-lg-3 col-xs-4">
          <div class="input-group date">
                        <div class="input-group-addon">
                          <i class="fa fa-calendar"></i>
                        </div>
                       {!! Form::text('fhasta','', array('class'=>'form-control input-sm datepicker','placeholder' => '','id'=>'fhasta') ) !!}
                    </div>  
        </div>
</div>


 <div class="row clsPadding">
                <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
            </div>

<div class="row clsPadding2">
    <div class="col-lg-4 col-xs-12 clsPadding">
    </div>
    <button type="button" class="btn btn-primary btn-block" id="btngenerar"><b>Generar</b></button>
    </div>
    <div class="col-lg-4 col-xs-12 clsPadding">
        <!--<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#divUminera"><b>Contactos de Unidad Minera</b></button>-->
    </div>               
</div>
{!! Form::close() !!}
 
        
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 
</div>
    </section>

    @include('partials.searchEmpleado')
    <!-- /.content -->
  </div>

  <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
 var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';   

var table=$("#tEmpleado").DataTable({
            "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarEmpleado',
      "columns" : [
        {data : 'identificacion', name: 'tper.identificacion'},
        {data : 'apaterno', name: 'tnat.apaterno'},
        {data : 'amaterno' , name : 'tnat.amaterno'},
        {data : 'nombres' , name : 'tnat.nombres'},
        {data : 'descripcion' , name : 'are.descripcion'}

      ],
     
            "language": {
                "lengthMenu": "Mostrar _MENU_ registros por página",
                "zeroRecords": "Sin Resultados",
                "info": "Página _PAGE_ de _PAGES_",
                "infoEmpty": "No existe registros disponibles",
                "infoFiltered": "(filtrado de un _MAX_ total de registros)",
                "search":         "Buscar:",
                "processing":     "Procesando...",
                "paginate": {
                    "first":      "Inicio",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "loadingRecords": "Cargando..."
            }
        });
        $('#tEmpleado tbody').on('click', 'tr', function () {
            var id = this.id;
            var index = $.inArray(id, selected);
            table.$('tr.selected').removeClass('selected');
            selected.splice(0,selected.length);             
            if ( index === -1 ) {
                selected.push( id );

            } 
     
            $(this).toggleClass('selected');
        } );     
        $('#searchEmpleado').on('hidden.bs.modal', function () {
              goEditar();
        });   


        function goEditar(){
          var id =0;
          if (selected.length > 0 ){
              id = selected[0];


             $.ajax({

                url:'cargarReporteGestionHumana/'+id.substring(4),
                type: 'GET',
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#bempleado").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });              

          }else{
              $('.alert').show();
          }
        }
         
    $('.datepicker').datepicker({
            format: "dd/mm/yyyy",
            language: "es",
            autoclose: true
        });

  function cleanEmpleado(){      
        $("#tpersona").val("");
        $("#cpersona").val("");
    }

$("#btngenerar").on('click',function(){
        var datos = $("#frmreporte").serialize();
        win = window.open('printGestionHumana?'+datos,'_blank');
    });

</script> 