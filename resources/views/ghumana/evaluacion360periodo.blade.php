@extends('layouts.master')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Evaluacion de desempeño</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="col-lg-1 hidden-xs"></div>
      <div class="col-lg-10 col-xs-12">
        <div class="row clsPadding2">
            <div class="col-lg-12 col-xs-12 clsTitulo">
          Lista de Evaluaciones
            </div>   
        </div>
        <div class="row clsPadding2">	
            <div class="col-lg-3 col-xs-12">
            </div>
            <div class="col-lg-3 col-xs-12">
            <!-- <a href="#" class="btn btn-primary btn-block"><b>Imprimir</b></a> -->
            </div> 
            <div class="col-lg-3 col-xs-12">
            <!-- <button type="button" onclick="getUrl('empleado');"  class="btn btn-primary btn-block"><b>Nuevo</b></button> --> 
            </div> 
            <div class="col-lg-3 col-xs-12">
        </div>
        <div class="row clsPadding2" width="100%">	
            <div class="col-lg-12 col-xs-12 table-responsive">
              <table class="table table-striped table-hover table-bordered" id="listadoperiodoevaluacion" width="100%" style="font-size: 12px;">
              	<thead>
                <tr>
                  <th class="clsCabereraTabla">Evaluacion </th>
                  <th class="clsCabereraTabla">Periodo</th>
                  <th class="clsCabereraTabla">Estado</th>
                  <th class="clsCabereraTabla">Acceso</th>
                </tr>
                </thead>
                <tbody>
                @foreach($periodos as $periodo)
                  <tr>
                       <td class="text-center">{{ $periodo->descripcion }}</td>
                       <td class="text-center">{{ substr($periodo->desde, 0, 4) }}-{{ substr($periodo->desde, 4, 2) }} al {{ substr($periodo->hasta, 0, 4) }}-{{ substr($periodo->hasta, 4, 2) }}</td>
                       @if($periodo->respuestascab > $periodo->totalevaluados*2)
                        <td class="text-center"><span class="label alert-success" style="font-size: 12px;">Finalizada</span></td>
                       @elseif($periodo->respuestascab == $periodo->totalevaluados*2)
                        <td class="text-center"><span class="label alert-success" style="font-size: 12px;">Finalizada</span></td>
                       @elseif($periodo->respuestascab != $periodo->totalevaluados*2)
                       <td class="text-center"> <span class="label alert-warning" style="font-size: 12px;">Pendiente</span> </td> 
                       @endif 
                       {{-- <td class="text-center">{{ $evaluado->dni_evaluado }}</td>  --}}
                       <td class="text-center">
                        @if($periodo->respuestascab != $periodo->cuento_cab*2)
                            <a type="button" class="btn btn-primary btn-block" href="{{ url('evaluacion', [$periodo->idcat])}}"  style="font-size: 12px;">
                              Ver Evaluaciones
                            </a>
                          {!! Form::close() !!}
                        @elseif($periodo->respuestascab == $periodo->cuento_cab*2)
                          <a type="button" class="btn btn-primary btn-block" href="{{ url('evaluacion', [$periodo->idcat])}}"  style="font-size: 12px;">
                            Ver Detalle
                          </a>
                        @endif 
                        
                      </td>
                  </tr>  
                @endforeach            
                </tbody>
              </table>

             </div>
        </div>
      </div>
      <div class="col-lg-1 hidden-xs"></div>
    </div>
    </section>
    <!-- /.content -->
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@push('evaluacion360periodo')
<script>
 
    var table = $('#listadoperiodoevaluacion').DataTable( {
         "paging":   true,
         "ordering": false,
         "info":     true,
        // "order":     [[0,"desc"]],
         lengthChange: true,
         //buttons: [ 'excel', 'pdf'/*, 'colvis'*/ ]
     } );  


  </script>
@endpush
@stop