
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Comprobante Vacaciones</title>
<style> 
#idprueba{
	font-size: 12px;
	border-left: 1px solid #000;
	border-right: 1px solid #000;
	border-bottom: 1px solid #000;
}
#fecha{
	margin-top: 15px;
}
.topsuperior{
	padding-top: 15px;
	text-align: left;
}
.firmas{
	padding-top: 35px;
	text-align: center;
}
.copiadministrativa{
	padding-top: 15px;
	text-align: center;
}
.nota{
	text-align: left;
	font-size: 11px;
}
.nota2{
	padding-top: 10px;
	text-align: center;
	padding-bottom: 10px;
	padding-left: 75px;
}

</style>
<div class="content-wrapper"> 
<div class="row">	
<div class="col-lg-12 col-xs-12 table-responsive" id="idGastoProy">
<section class="content">
<br>
<br>
<table  style="width:100%; border-collapse: collapse; size: 0.5px;" class="table table-bordered" style=" font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
    <tr>
        <th style="border:1px solid #000000FF; width: 30%;">
        	<img style="width: 150px;padding: 10px 0px 10px 0px;" src="images/logorpt.jpg">
        </th>
        <th rowspan="2" style="border:1px solid #000000FF; width: 40%;text-align: center;font-size: 14px">
        	<p>GESTIÓN HUMANA <br> Solicitud de Vacaciones </p>
        </th>
        <td style="border:1px solid #000000FF; width: 20%;font-size: 12px">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: center;"><strong></strong></td>
                </tr>
                <tr>
                    <td>Revisión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                </tr>
                <tr>
                    <td>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 10px;"> SIG AND </td>
        <td style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 10px;"> 10-AND-43-FOR-0503 / R0 / 01/10/14 </td>
    </tr>
</table>
<div id="idprueba">
	<table id="fecha">
		<tr>
			<td width="90%"></td>
			<td colspan="" rowspan="" headers="" scope="" width="95%"></td>
			<td >Fecha</td>
			<td colspan="" rowspan="" headers="" scope="" width="5%">{{ $datoscomprovante['fechaactual'] }}</td>
		</tr>
		<tr>
			<td class="topsuperior" colspan="2" rowspan="" headers="" scope="" width="40%">&nbsp;&nbsp;Nombre del Empleado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $datoscomprovante['nombreempleado'] }}&nbsp;&nbsp;</td>
			
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%">Área / Departamento &nbsp;&nbsp;</td> 
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%" style="text-align: center;">{{ $datoscomprovante['departamento'] }}</td>
		</tr>
		<tr>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%" style="text-align: center;">&nbsp;&nbsp;Del&nbsp;&nbsp;&nbsp;&nbsp;{{ $datoscomprovante['fdesde'] }}&nbsp;&nbsp;&nbsp;&nbsp;Al&nbsp;&nbsp;&nbsp;{{ $datoscomprovante['fhasta'] }}</td>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%">&nbsp;&nbsp;N° de Días&nbsp;&nbsp;&nbsp;_________{{ $datoscomprovante['diassolicitados'] }}__________</td>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="40%">Periodo Vacacional</td> 
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%">_______________________________</td>

		</tr>
		<tr>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%">&nbsp;&nbsp;Autotizaciones:</td>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="40%"></th> 
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>

		</tr>
		<tr>
			<td class="firmas" colspan="1" rowspan="2" headers="" scope="" style="line-height:22px;">{{ $datoscomprovante['firmacolaborador'] }}<br>Firma del Colaborador</td>
			<td class="firmas" colspan="2" rowspan="2" headers="" scope="" style="line-height:22px;">{{ $datoscomprovante['firmajefearea'] }}<br>Firma Jefe de Área</td>
			<td class="firmas" colspan="1" rowspan="2" headers="" scope="" style="line-height:22px;">{{ $datoscomprovante['firmagerentearea'] }}<br>Firma Gerente de Área</td> 
		</tr>
	</table>
	<table>
		<tr>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%">&nbsp;&nbsp;Nota:</td>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="40%"></th> 
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>
		</tr>
		<tr>
			<td class="nota" colspan="4" rowspan="0" headers="" scope="" style="font-size: 9px;">&nbsp;&nbsp;&nbsp;Este formato deberá contar con las firmas autorizadas. El área de Gestión Humana informará al Colaborador el número de días de vacaciones disponibles.</td>
		</tr>
		<tr>
			<td class="nota2" colspan="4" rowspan="0" headers="" scope="" ><center>( Copia Administración )</center></td>	
		</tr>
	</table>
</div>
<hr style="border-top: 4px dotted blue;margin-top: 28px;margin-bottom: 25px;">
<br>	
<table  style="width:100%; border-collapse: collapse; size: 0.5px;" class="table table-bordered" style=" font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
    <tr>
        <th style="border:1px solid #000000FF; width: 30%;">
        	<img style="width: 150px;padding: 10px 0px 10px 0px;" src="images/logorpt.jpg">
        </th>
        <th rowspan="2" style="border:1px solid #000000FF; width: 40%;text-align: center;font-size: 14px">
        	<p>GESTIÓN HUMANA <br> Solicitud de Vacaciones </p>
        </th>
        <td rowspan="1"  style="border:1px solid #000000FF; width: 20%;font-size: 12px">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: center;"><strong></strong></td>
                </tr>
                <tr>
                    <td>Revisión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                </tr>
                <tr>
                    <td>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <th height="10px;" style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 173, 1); font-size: 10px;"> SIG AND </th>
        <th height="10px;" style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 10px;"> 10-AND-43-FOR-0503 / R0 / 01/10/14 </th>
    </tr>
</table>
<div id="idprueba">
	<table id="fecha">
		<tr>
			<td width="90%"></td>
			<td colspan="" rowspan="" headers="" scope="" width="95%"></td>
			<td >Fecha</td>
			<td colspan="" rowspan="" headers="" scope="" width="5%">{{ $datoscomprovante['fechaactual'] }}</td>
		</tr>
		<tr>
			<td class="topsuperior" colspan="2" rowspan="" headers="" scope="" width="40%">&nbsp;&nbsp;Nombre del Empleado:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $datoscomprovante['nombreempleado'] }}&nbsp;&nbsp;</td>
			
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%">Área / Departamento &nbsp;&nbsp;</td> 
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%" style="text-align: center;">{{ $datoscomprovante['departamento'] }}</td>
		</tr>
		<tr>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%" style="text-align: center;">&nbsp;&nbsp;Del&nbsp;&nbsp;&nbsp;&nbsp;{{ $datoscomprovante['fdesde'] }}&nbsp;&nbsp;&nbsp;&nbsp;Al&nbsp;&nbsp;&nbsp;{{ $datoscomprovante['fhasta'] }}</td>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%">&nbsp;&nbsp;N° de Días&nbsp;&nbsp;&nbsp;_________{{ $datoscomprovante['diassolicitados'] }}__________</td>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="40%">Periodo Vacacional</td> 
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%">_______________________________</td>

		</tr>
		<tr>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%">Autotizaciones:</td>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="40%"></th> 
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>

		</tr>
		<tr>
			<td class="firmas" colspan="1" rowspan="2" headers="" scope="" style="line-height:22px;">{{ $datoscomprovante['firmacolaborador'] }}<br>Firma del Colaborador</td>
			<td class="firmas" colspan="2" rowspan="2" headers="" scope="" style="line-height:22px;">{{ $datoscomprovante['firmajefearea'] }}<br>Firma Jefe de Área</td>
			<td class="firmas" colspan="1" rowspan="2" headers="" scope="" style="line-height:22px;">{{ $datoscomprovante['firmagerentearea'] }}<br>Firma Gerente de Área</td> 
		</tr>
	</table>
	<table>
		<tr>
			<td class="topsuperior" colspan="" rowspan="" headers="" scope="" width="50%">Nota:</td>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="40%"></th> 
			<th class="topsuperior" colspan="" rowspan="" headers="" scope="" width="10%"></th>
		</tr>
		<tr>
			<td class="nota" colspan="4" rowspan="0" headers="" scope="" style="font-size: 8px;">Este formato deberá contar con las firmas autorizadas. El área de Gestión Humana informará al Colaborador el número de días de vacaciones disponibles.</td>
		</tr>
		<tr>
			<td class="nota2" colspan="4" rowspan="0" headers="" scope="">( Copia Empleado )</td>	
		</tr>
	</table>
</div>
</section>
</div>
</div>
</div>