@extends('layouts.master')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Gestión Humana
        <small>Aprobacion de Vacaciones</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Gestión Humana</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">

<div class="row clsPadding2"> 
    <div class="col-lg-12 col-xs-12">
<!-- inicio accordeon -->       

<div class="nav-tabs-custom">
            <ul class="nav nav-tabs pull-right">
                <li class="active"><a href="#tab_1-1" data-toggle="tab" aria-expanded="true">Vacaciones Pendientes</a></li>
                <li class=""><a href="#tab_2-2" data-toggle="tab" aria-expanded="false">Vacaciones Aprobadas</a></li>
                <li class=""><a href="#tab_3-3" data-toggle="tab" aria-expanded="false">Vacaciones Observadas</a></li>           
              <li class="pull-left header"><i class="fa fa-newspaper-o"></i> Listado de Vacaciones para aprobar</li>
            </ul>
            <div class="tab-content">
              <!-- /.tab-pane 1-1 -->  
              <div class="tab-pane active" id="tab_1-1">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                      <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-center" id="listadovacacionesparaabrobar" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;">
                                  <thead>
                                  <tr class="clsCabereraTabla">
                                    <th>Colaborador</th>
                                    <th>Desde - Hasta</th>
                                    <th>Tipo de Solicitud</th>
                                    <th>Días Solicitados</th>
                                    <th>Total de Días Acumulados</th>
                                    <th>Saldo restante</th>
                                    <th>Estado</th>
                                    <th>Accion</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($solicitudes as $aprobar)
                                      <tr>
                                        <td class="clsAnchoTabla">{{ $aprobar->nombre }}</td>
                                        <td class="clsAnchoTabla">{{ $aprobar->fdesde }} - {{ $aprobar->fhasta }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->tipovaca}}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->diassolicitados }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->totalvacionesacumuladas + $aprobar->diassolicitados }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->totalvacionesacumuladas }}</td>
                                        <td  class="clsAnchoDia"><span class="label alert-success" style="font-size: 14px;"> {{ $aprobar->estadova }}</span></td>
                                        <td  class="clsAnchoDia">
                                          <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myModal_{{ $aprobar->id }}">
                                        Pendientes
                                      </button>
                                      <!-- Modal -->
                                      <div class="modal fade" id="myModal_{{ $aprobar->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel_{{ $aprobar->id }}">
                                        <div class="modal-dialog" role="document">
                                          <div class="modal-content">
                                            <div class="modal-header" style="background-color: #0069aa;color: #fff;">
                                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                              <h4 class="modal-title" id="myModalLabel_{{ $aprobar->id }}">Solicitud Vacaciones de {{ $aprobar->nombre }} </h4>
                                            </div>
                                            {!! Form::open(['url' => 'vaca/aprobadas','method' => 'POST', 'id' => 'apruebovacaciones', 'class' => 'form-inline']) !!}
                                              <div class="modal-body">
                                              
                                              <input  type="hidden" id="idvacacionessolicitud" name="idvacacionessolicitud" value="{{ $aprobar->id }}">
                                              <input  type="hidden" id="totaldias" name="totaldias" value="{{ $aprobar->diassolicitados - $aprobar->totalvacionesacumuladas }}">  
                                              <input  type="hidden" id="rechazar" name="rechazar" value="false">  
                                                <div class="form-group">
                                                  <input style="font-size: 14px;text-align: center" type="text" class="form-control" id="exampleInputName2" value="{{ $aprobar->fdesde }}" placeholder="" disabled>
                                                </div>
                                                <div class="form-group">
                                                  <label for="exampleInputEmail2">AL</label>
                                                  <input style="font-size: 14px;text-align: center" type="text" class="form-control" id="exampleInputEmail2" value="{{ $aprobar->fhasta }}" placeholder="" disabled>
                                                </div>
                                                <div class=" col-xs-12 col-sm-12 col-md-12 col-lg-12 form-group">
                                                  <span id="requeridotextoobservacion" class="pull-left alert-warning" style="font-size: 12px;margin-top: 15px;margin-left: 45px; padding-left: 5px;">Observaciones obligatorias al momento de corregir las vacaciones</span>
                                                    <textarea id="observacionrechazo" name="observacionrechazo" class="form-control" rows="5" style="max-width: 450px;min-width: 450px;margin-top: 10px;margin-bottom: 10px;"></textarea>
                                                </div>
                                                <div class="form-group">
                                                  <div class="has-success">
                                                    <div class="checkbox">
                                                      <label>
                                                        <input type="checkbox" id="checkboxSuccess" value="option1" required="true">
                                                        Seguro de aprobar esta solicitud
                                                      </label>
                                                    </div>
                                                  </div>
                                                   
                                                </div> 
                                               
                                            </div>
                                            <div class="modal-footer" style="margin-top: 5px;">
                                              <button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>   
                                              <button onclick="rechazoypregunto();" id="botonrechazo" type="button" class="btn btn-warning">Corregir</button> 
                                              <button type="submit" class="btn btn-success">Aprobar</button>
                                            </div>
                                          {!! Form::close() !!}     
                                          </div>
                                        </div>
                                      </div>
                                        </td>
                                      </tr>
                                    @endforeach                                                 
                                  </tbody>
                        </table>
                      </div>                            
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
              <!-- /.tab-pane  1-1-->
              <!-- /.tab-pane  2-2-->
              <div class="tab-pane" id="tab_2-2">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                      <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-center" id="listadovacacionesparaabrobar" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;">
                                  <thead>
                                  <tr class="clsCabereraTabla">
                                    <th>Colaborador</th>
                                    <th>Desde - Hasta</th>
                                    <th>Tipo de Solicitud</th>
                                    <th>Días Solicitados</th>
                                    <th>Estado</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($solicitudesporaprobadas as $aprobar)
                                      <tr>
                                        <td class="clsAnchoTabla">{{ $aprobar->nombre }}</td>
                                        <td class="clsAnchoTabla">{{ $aprobar->fdesde }} - {{ $aprobar->fhasta }}</td>
                                        <td  class="clsAnchoDia">{{ ucfirst($aprobar->tipovaca) }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->diassolicitados }}</td>
                                        <td  class="clsAnchoDia"><span class="label alert-success" style="font-size: 14px;"> {{ $aprobar->estadova }}</span></td>
                                        
                                      </tr>
                                    @endforeach                                                 
                                  </tbody>
                        </table>
                      </div>                            
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
              <!-- /.tab-pane  2-2-->  
              <!-- /.tab-pane  3-3-->  
              <div class="tab-pane" id="tab_3-3">               
                  <!-- Pestaña Listado Vacaciones -->   
                  <div class="row clsPadding2">
                      <div class="col-lg-12 col-xs-12 table-responsive">
                        <table class="table table-striped table-hover table-bordered text-center" id="listadovacacionesparaabrobar" width="100%;" style="font-size: 14px;height:100px;max-height:100px !important;">
                                  <thead>
                                  <tr class="clsCabereraTabla">
                                    <th>Colaborador</th>
                                    <th>Desde - Hasta</th>
                                    <th>Tipo de Solicitud</th>
                                    <th>Días Solicitados</th>
                                    <th>Observacion</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                    @foreach($solicitudesobservadas as $aprobar)
                                      <tr>
                                        <td class="clsAnchoTabla">{{ $aprobar->nombre }}</td>
                                        <td class="clsAnchoTabla">{{ $aprobar->fdesde }} - {{ $aprobar->fhasta }}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->tipovaca}}</td>
                                        <td  class="clsAnchoDia">{{ $aprobar->diassolicitados }}</td>
                                        <td  class="clsAnchoDia" style="min-width:500px;width: 500px;-webkit-box-sizing:border-box;-moz-box-sizing: border-box;box-sizing:border-box;"><textarea style="width:100%;-webkit-box-sizing:border-box;-moz-box-sizing: border-box;box-sizing:border-box;" disabled="true">{{ $aprobar->observacion }}</textarea></td>
                                        
                                      </tr>
                                    @endforeach                                                 
                                  </tbody>
                        </table>
                      </div>                            
                  </div>          
                  <!-- FIN Datos Personales -->                
              </div>
              <!-- /.tab-pane  3-3-->      
            </div> 
          </div>
<!--  fin accordeon -->        
    </div>
</div>
</div>
    <div class="col-lg-1 hidden-xs"></div>
</div>
</section>
<!-- /.content -->
</div>



<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  $(document).ready(function(){
     $('#listadovacacionesparaabrobar').DataTable({
        "bFilter": true,
         "bSort": true,
         "bPaginate": true,
         "bInfo": true,
         "scrollY":        "600px",
         "scrollCollapse": true,
      });
     $('#listadovacacionesparaabrobar').DataTable();

     $('#listadovacacionesparaabrobar2').DataTable();

     $('#listadovacacionesparaabrobar3').DataTable();

    $('#fdesde').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "tomorrow"
    }).datepicker("setDate",'now');  

    $('#fhasta').datepicker({
      format: "dd/mm/yyyy",
      language: "es",
      autoclose: true,
      firstDay: 1,
      calendarWeeks:true,
      startDate: "+1d",
    }).datepicker("setDate",'+1d');
  });

</script>

<script type="text/javascript">

function rechazoypregunto() {
  var cuentorespuesta = document.getElementById("observacionrechazo").value;
  var n = cuentorespuesta.length;
  //event.preventDefault();
  if (n > 0) {
    document.getElementById("rechazar").value = "true";
    $("#apruebovacaciones").submit();
  } else {
    alert('Debe colocar observaciones.');
    return false;
  }
}
</script>
@stop