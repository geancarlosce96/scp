
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<title>Comprobante Horas Extras</title>
<style> 
#idprueba{
	font-size: 12px;
	border-left: 1px solid #000;
	border-right: 1px solid #000;
	border-bottom: 1px solid #000;
}
#fecha{
	margin-top: 15px;
}
.topsuperior{
	padding-top: 15px;
	text-align: left;
}
.firmas{
	padding-top: 35px;
	text-align: center;
}
.copiadministrativa{
	padding-top: 15px;
	text-align: center;
}
.nota{
	text-align: left;
	font-size: 11px;
}
.nota2{
	padding-top: 10px;
	text-align: center;
	padding-bottom: 10px;
	padding-left: 75px;
}

</style>
<div class="content-wrapper"> 
<div class="row">	
<div class="col-lg-12 col-xs-12 table-responsive" id="idGastoProy">
<section class="content">
<br>
<br>
<table  style="width:100%; border-collapse: collapse; size: 0.5px;" class="table table-bordered" style=" font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
    <tr>
        <th style="border:1px solid #000000FF; width: 30%;">
        	<img style="width: 150px;padding: 10px 0px 10px 0px;" src="images/logorpt.jpg">
        </th>
        <th rowspan="2" style="border:1px solid #000000FF; width: 40%;text-align: center;font-size: 14px">
        	<p>CONTROL DOCUMENTARIO <br> Registro de Horas </p>
        </th>
        <td style="border:1px solid #000000FF; width: 20%;font-size: 12px">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: center;"><strong></strong></td>
                </tr>
                <tr>
                    <td>Revisión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                </tr>
                <tr>
                    <td>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 10px;">  </td>
        <td style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 10px;">  </td>
    </tr>
    <tr>
		<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Colaborador
		</td>
		<td colspan="2" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			{{ $datoscomprovante['nombreempleado'] }}
		</td>

    </tr>
    <tr>
    	<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Jefe Inmediato
		</td>
		<td colspan="2" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			{{ $datoscomprovante['firmajefearea'] }}
		</td>
    </tr>
    <tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 30%;text-align: left; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 12px;">
			Detalle del registro de horas
		</td>
    </tr>
    <tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Código Proyecto: {{ $datoscomprovante['codigoproyecto'] }}
		</td>
    </tr>
    <tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Nombre del Proyecto: {{ $datoscomprovante['nombreproyecto'] }}
		</td>
    </tr>
    <tr>
    	<td colspan="3" HEIGHT="50" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			<p style="margin:0px;padding:0px;">Motivo:</p>
			{{ $datoscomprovante['motivo'] }}
		</td>

    </tr>
    {{-- <tr>
    	<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Hora de Inicio:
		</td>
		<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: right;">
			Hora de Salida:
		</td>
		<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			s
		</td>	
   	</tr> --}}
   	<tr>
   		<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
   			Fecha: {{ $datoscomprovante['fechahorasextras'] }}
   		</td>
   	</tr>
   	<tr>
   		<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
   			Cantidad de Horas: {{ $datoscomprovante['horassolicitadas'] }}
   		</td>
   	</tr>
   	<tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 30%;text-align: left; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 12px;">
			Aprobación
		</td>
    </tr>
    <tr>
    	<td colspan="1" HEIGHT="25" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center;">
    		Gerente del Proyecto
    	</td>
    	<td colspan="1" HEIGHT="25" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center;">
    		Colaborador
    	</td>
    	<td colspan="1" HEIGHT="25" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center;">
    		Jefe Inmediato
    	</td>
    </tr>
    <tr>
		<td colspan="1" rowspan="0" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Nombre: {{ $datoscomprovante['gerenteproyecto'] }}
    	</td>
    	<td colspan="1" rowspan="0" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Nombre: {{ $datoscomprovante['firmacolaborador'] }}
    	</td>
    	<td colspan="1" rowspan="0" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Nombre: {{ $datoscomprovante['firmajefearea'] }}
    	</td>
    </tr>
    <tr>
		<td colspan="1" HEIGHT="35" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Firma:
    	</td>
    	<td colspan="1" HEIGHT="35" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Firma:
    	</td>
    	<td colspan="1" HEIGHT="35" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Firma:
    	</td>
    </tr>
     <tr>
		<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center">
    		(Copia Área):
    	</td>
    	
    </tr>
    
	


</table>


<hr style="border-top: 4px dotted blue;margin-top: 28px;margin-bottom: 25px;">
<br>

<table  style="width:100%; border-collapse: collapse; size: 0.5px;" class="table table-bordered" style=" font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
    <tr>
        <th style="border:1px solid #000000FF; width: 30%;">
        	<img style="width: 150px;padding: 10px 0px 10px 0px;" src="images/logorpt.jpg">
        </th>
        <th rowspan="2" style="border:1px solid #000000FF; width: 40%;text-align: center;font-size: 14px">
        	<p>CONTROL DOCUMENTARIO <br> Registro de Horas </p>
        </th>
        <td style="border:1px solid #000000FF; width: 20%;font-size: 12px">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: center;"><strong></strong></td>
                </tr>
                <tr>
                    <td>Revisión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
                </tr>
                <tr>
                    <td>Fecha&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 10px;">  </td>
        <td style="border:1px solid #000000FF; width: 30%;text-align: center; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 10px;">  </td>
    </tr>
    <tr>
		<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Colaborador
		</td>
		<td colspan="2" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			{{ $datoscomprovante['nombreempleado'] }}
		</td>

    </tr>
    <tr>
    	<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Jefe Inmediato
		</td>
		<td colspan="2" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			{{ $datoscomprovante['firmajefearea'] }}
		</td>
    </tr>
    <tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 30%;text-align: left; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 12px;">
			Detalle del registro de horas
		</td>
    </tr>
    <tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Código Proyecto: {{ $datoscomprovante['codigoproyecto'] }}
		</td>
    </tr>
    <tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Nombre del Proyecto: {{ $datoscomprovante['nombreproyecto'] }}
		</td>
    </tr>
    <tr>
    	<td colspan="3" HEIGHT="50" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			<p style="margin:0px;padding:0px;">Motivo:</p>
			{{ $datoscomprovante['motivo'] }}
		</td>

    </tr>
    {{-- <tr>
    	<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			Hora de Inicio:
		</td>
		<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: right;">
			Hora de Salida:
		</td>
		<td colspan="" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
			s
		</td>	
   	</tr> --}}
   	<tr>
   		<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
   			Fecha: {{ $datoscomprovante['fechahorasextras'] }}
   		</td>
   	</tr>
   	<tr>
   		<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
   			Cantidad de Horas: {{ $datoscomprovante['horassolicitadas'] }}
   		</td>
   	</tr>
   	<tr>
    	<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 30%;text-align: left; color: #fff; background-color:rgba(0, 105, 170, 1); font-size: 12px;">
			Aprobación
		</td>
    </tr>
    <tr>
    	<td colspan="1" HEIGHT="25" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center;">
    		Gerente del Proyecto
    	</td>
    	<td colspan="1" HEIGHT="25" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center;">
    		Colaborador
    	</td>
    	<td colspan="1" HEIGHT="25" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center;">
    		Jefe Inmediato
    	</td>
    </tr>
    <tr>
		<td colspan="1" rowspan="0" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Nombre: {{ $datoscomprovante['gerenteproyecto'] }}
    	</td>
    	<td colspan="1" rowspan="0" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Nombre: {{ $datoscomprovante['firmacolaborador'] }}
    	</td>
    	<td colspan="1" rowspan="0" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Nombre: {{ $datoscomprovante['firmajefearea'] }}
    	</td>
    </tr>
    <tr>
		<td colspan="1" HEIGHT="35" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Firma:
    	</td>
    	<td colspan="1" HEIGHT="35" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Firma:
    	</td>
    	<td colspan="1" HEIGHT="35" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px">
    		Firma:
    	</td>
    </tr>
     <tr>
		<td colspan="3" rowspan="" headers="" style="border:1px solid #000000FF; width: 20%;font-size: 12px;text-align: center">
    		(Copia Administración):
    	</td>
    	
    </tr>
    
	


</table>


</section>
</div>
</div>
</div>