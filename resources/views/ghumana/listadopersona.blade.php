<table class="table table-striped table-hover table-bordered table-responsive" id="listadopersona" style="font-size: 12px;">
	<!-- <caption>table title and/or explanatory text</caption> -->
	<thead>
		<tr>
			<th class="clsCabereraTabla">Isssd</th>
			<th class="clsCabereraTabla">DNI</th>
			<th class="clsCabereraTabla">Apellidos y Nombres</th>
			<th class="clsCabereraTabla">Usuario</th>
			<th class="clsCabereraTabla">Cargo</th>
			<th class="clsCabereraTabla">Área</th>
			<th class="clsCabereraTabla">Fecha Ingreso</th>
			<th class="clsCabereraTabla">Aprobador</th>
			<th class="clsCabereraTabla">Estado</th>
			<th class="clsCabereraTabla">Acciones</th>
		</tr>
	</thead>
	<tbody style="font-size: 12px;">
		@foreach ($listarpersonal as $key => $lista)
		<tr>
			<td>{{ $lista->cpersona }}</td>
			<td>{{ $lista->identificacion }}</td>
			<td>{{ $lista->nombre }}</td>
			<td>{{ $lista->usuario }}</td>
			<td>{{ $lista->cargo }}</td>
			<td>{{ $lista->area }}</td>
			<td>{{ substr($lista->fingreso,0,10) }}</td>
			<td>{{ $lista->aprobador }}</td>
			<td>
				<center>
				@if($lista->estadousuario=='ACT' || $lista->estadopersona=='ACT')
					<span class="pull-right badge bg-green">Activo</span>
				@endif
				@if($lista->estadousuario=='INA' || $lista->estadopersona=='INA')
					<span class="pull-right badge bg-red">Inactivo</span>
				@endif
				</center>
			</td>
			<td>
				<center>
				@if($lista->estadousuario=='ACT' || $lista->estadopersona=='ACT')
					<a onclick="estadoempleadousuario('{{$lista->cpersona}}')" title="Inactivar"><span class="glyphicon glyphicon-ok-circle" style="font-size: 20px; color:red"></span></a>
				@endif
				@if($lista->estadousuario=='INA' || $lista->estadopersona=='INA')
					<a onclick="estadoempleadousuario('{{$lista->cpersona}}')" title="Activar"><span class="glyphicon glyphicon-remove-circle" style="font-size: 20px; color: green"></span></a>
				@endif
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a onclick="editarempleadousuario('{{$lista->cpersona}}')" title="Editar"><span class="glyphicon glyphicon-edit" style="font-size: 20px; color: blue"></span></a>
				</center>
			</td>
		</tr>
		@endforeach
	</tbody>
</table>

