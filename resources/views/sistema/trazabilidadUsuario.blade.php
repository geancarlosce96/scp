<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración
        <small>Trazabilidad por Usuario</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Configuración</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">

  
  
<div class="row clsPadding2">	
    <div class="col-lg-1 col-xs-1">Usuario:</div>
    <div class="col-lg-9 col-xs-9">
    	<input type="text" class="form-control" id="" placeholder="Usuario...">
    </div> 
    <div class="col-lg-2 col-xs-2">
	    <a href="#"class="btn btn-primary btn-block"><b>...</b></a>
    </div>  
</div>

<div class="row clsPadding2">	
    <div class="col-lg-8 col-xs-8">
        <label class="clsTxtNormal">Fecha:</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker" placeholder="Fecha">
        </div>  
    </div>   
    <div class="col-lg-2 col-xs-2">
    	<a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
        	<b>Visualizar</b></a>
    </div>
    <div class="col-lg-2 col-xs-2">
    	<a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
        	<b>Imprimir</b></a>
    </div>    
</div>
<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
      	<thead>
        <tr>
        	<td class="clsCabereraTabla">Opción de menú</td>
        	<td class="clsCabereraTabla">Acción</td>
            <td class="clsCabereraTabla">Información</td>
            <td class="clsCabereraTabla">Hora</td>                                   
            <td class="clsCabereraTabla"></td>
          </tr>  
         </thead>
         <tbody>
         	<tr>
        	<td class="clsAnchoTabla">J. Carreño</td>
            <td class="clsAnchoTabla">print Arhivos</td>
            <td class="clsAnchoTablaMax">4:37pm</td>            
            <td class="clsAnchoDia">
				<a href="#" data-toggle="tooltip" data-container="body" title="Ver Trazabilidad">
                <i class="fa fa-eye" aria-hidden="true"></i></a>            
            </td>            
            </tr>
         	<tr>
        	<td class="clsAnchoTabla">F. Murga</td>
            <td class="clsAnchoTabla">delete "Factivilidad.xls"</td>
            <td class="clsAnchoTablaMax">11:45am</td>            
            <td class="clsAnchoDia">
				<a href="#" data-toggle="tooltip" data-container="body" title="Ver Trazabilidad">
                <i class="fa fa-eye" aria-hidden="true"></i></a>            
            </td>            
            </tr>                      
         </tbody>
      </table>  
     </div>
</div>
<br><br><br><br>

<div class="col-lg-12 col-xs-12" style="border:1px solid #CCC; border-radius:2px;">
 
<div class="row clsPadding2">
	<div class="col-lg-12 col-xs-12">Detalle de Trazabilidad</div>
</div>
<div class="row clsPadding">
	<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>
<div class="row clsPadding">
	<div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 table-responsive">
      <table class="table table-striped">
      	<thead>
        <tr>
        	<td class="clsCabereraTabla">Proyecto</td>
            <td class="clsCabereraTabla">Tipo Dato</td>
            <td class="clsCabereraTabla">Inf. Anterior</td>
            <td class="clsCabereraTabla">Inf. Posterior</td>                                   
            <td class="clsCabereraTabla">Sel</td>
          </tr>  
         </thead>
         <tbody>
         	<tr> 
            	<td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoDia">
                	<input type="checkbox">
                </td>          
            </tr> 
         	<tr> 
            	<td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoDia">
                	<input type="checkbox">
                </td>          
            </tr>                 
         </tbody>
      </table>     
    </div>
	<div class="col-lg-1 hidden-xs"></div>    
</div>
<div class="row clsPadding">
	<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>
<div class="row clsPadding2">
	<div class="col-lg-2 col-xs-2">
	    <a href="#" class="btn btn-primary btn-block "data-toggle="tooltip" data-container="body" title="Imprimir">
         <b>Imprimir</b></a>
    </div>
	<div class="col-lg-2 col-xs-2">
	    <a href="#" class="btn btn-primary btn-block "data-toggle="tooltip" data-container="body" title="Cerrar">
         <b>Cerrar</b></a>
    </div> 
     <div class="col-lg-8 col-xs-8"></div>  
</div>

</div>




<!-- fin  -->
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 






</div>
    </section>
    <!-- /.content -->
  </div>