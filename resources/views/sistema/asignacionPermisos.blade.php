<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración del Sistema
        <small>Asignación de permisos</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Configuración del Sistema</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
       {!! Form::open(array('url' => 'agregarPermisos','method' => 'POST','id' =>'frmasigperm')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!} 

    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">
          @include('partials.verUsuariosAsignacionPermisos',array('usuario'=>(isset($usuario)==1?$usuario:null) ))
   <div class="row clsPadding2">
   		     
    </div>   

    <div class="row clsPadding2">
      <div class="col-lg-5 col-xs-5">
         <div id="treePermiso"></div> 
      </div> 
      <div class="col-lg-1 col-xs-1">
         <button type='button' class="btn btn-primary btn-block" id="savePermiso"><b> ></b></button>
         <button type='button' class="btn btn-primary btn-block" id="btnDelPermiso"><b> X</b></button>
      </div>  

      <div class="col-lg-5 col-xs-5 table-responsive" id="TablePerm"> 
          @include('partials.tableUsuarioPermisos',array('menupersona'=> (isset($menupersona)==1?$menupersona:array() )))
      </div> 

   </div> 
           
                
    </div><!-- fin parte central -->
    
    <div class="col-lg-1 hidden-xs"></div>

</div>

</section>
    <!-- /.content -->
    @include('partials.searchUsuario')
  </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>

    var selected_permiso = [];   
    var selected =[];
    var usuario=[];
    $.fn.dataTable.ext.errMode = 'throw';     
    var table=$("#tUsuario").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarUsuario',
      "columns" : [
         {data : 'cpersona' , name : 'tp.cpersona'},
        {data : 'abreviatura', name: 'tp.abreviatura'},
        {data : 'usuario', name: 'tu.nombre'},  
      ],

      "rowCallback" : function( row, data ) {
          
          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });

    
    $('#tUsuario tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#tUsuario').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/
        
        $(this).toggleClass('selected');
    });

    $('#searchUsuario').on('hidden.bs.modal', function () {
      goEditar();
    });  


  function goEditar(){
   
    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(4));
       
        getUrl('editarAsignacionPermiso/'+id.substring(4),'');      


    }else{
        $('.alert').show();
    }
  }

function goVerUsuarioPermisos(){
   
    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(4));
       
          $.ajax({

                url:'viewUsuarioPermisos/'+id.substring(4),
                type: 'GET',
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){

                    $("#TablePerm").html(data);
                    //$('#div_carga').hide(); 
                    
                },            
            });              


    }else{
        $('.alert').show();
    }
  }

  function goVerUsuario(){
   
    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(4));
       
          $.ajax({

                url:'viewUsuario/'+id.substring(4),
                type: 'GET',           
                   
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                  
                    $("#bUsuper").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });              


    }else{
        $('.alert').show();
    }
  }

function getTree() {

          // Some logic to retrieve, or generate tree structure
          var tree = [
          <?php echo (isset($estructura)==1?$estructura:'') ?>
          
          ];          
          return tree;
  }
 

        $('#treePermiso').highCheckTree({
            data: getTree(),
            onCheck: function (node){

              selected_permiso.push(node.attr('rel'));
          
            },
            onUnCheck: function(node){
              var idx = $.inArray(node.attr('rel'),selected_permiso);
    
              if(idx != -1){
                selected_permiso.splice(idx,1);
              }
            }
          });

    function cleanUsuario(){
            $("#usuario").val("");

          }

       $('#btnDelPermiso').click(function(id){
            var searchIDs = $("#TablelistPerm input:checkbox:checked").map(function(){
              return $(this).val();
            }).toArray();
            console.log(searchIDs);
            $.ajax({
              type: 'GET',
              url :'delAsignacionPermiso/'+id,
              data: 
              {"_token": "{{ csrf_token() }}",
              chkselec:searchIDs,
              usuario: $("#cpersona").val()
              },
              success :function(data){
                $('#TablePerm').html(data);               
              },
             
              beforeSend : function(){
                //$('#div_carga').show(); 
              }
            });
        });

      
        $("#savePermiso").click(function(e){
          
           if(selected_permiso.length<=0){
            return;
          }

          $.ajax({
            url: 'agregarPermisos',
            type:'POST',
            data: {"_token": "{{ csrf_token() }}",
              selected_permiso: selected_permiso,
              usuario: $("#cpersona").val()
              },
              success : function (data){
                  $('#TablePerm').html(data);
                // $('#div_carga').hide();   

            },

              beforeSend: function () {                  
                 // $('#div_carga').show();           
              }           
            
          });

        })


</script>