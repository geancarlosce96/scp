<div class="content-wrapper" style="font-size: 12px">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración del Sistema
        <small>Registro de Usuarios</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Configuración del Sistema</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">


    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">



<div class="row clsPadding2">
	<div class="col-lg-2 hidden-xs"></div>
    
    <div class="col-lg-2 col-xs-6 clsPadding">
    	<button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#addUs"
        > <b>Nuevo</b></a></button>
    </div>    

    <div class="col-lg-2 col-xs-6 clsPadding">
    	<button type="button" onclick="goEditar()"; class="btn btn-primary btn-block" data-toggle="modal" data-target=""
        ><b>Modificar</b></a></button>
    </div> 

    <div class="col-lg-2 col-xs-6 clsPadding">

    	<!-- <a href="#" class="btn btn-primary btn-block ">Activar</a> -->
    </div>
    <div class="col-lg-2 col-xs-6 clsPadding">
    	<!-- <a href="#" class="btn btn-primary btn-block ">Desactivar</a> -->
    </div>

    <div class="col-lg-2 hidden-xs"></div>

</div>

<!--   <div class="row clsPadding2">
        <div class="col-lg-4 col-xs-12">
        <label class="clsTxtNormal">Área:</label>
			
              {!! Form::select('area',(isset($tarea)==1?$tarea:array() ),'',array('class' => 'form-control select-box','id'=>'area')) !!}
              </div>        
         		
        <div class="col-lg-4 col-xs-12">
        <label class="clsTxtNormal">Buscar Usuario:</label>
			<div class="input-group">
                <input type="text" class="form-control">
                <span class="input-group-addon"><i class="fa fa-search"></i></span>
              </div>        
        </div>
        <div class="col-lg-4 col-xs-12">
			<label class="clsTxtNormal">Estado:</label>
                  {!! Form::select('estado_est',(isset($testado)==1?$testado:array() ),'',array('class' => 'form-control','id'=>'estado_est')) !!}      
        </div>
  </div>  --> 
 
 
<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive" >

      <table class="table table-striped" id="listado">
            <thead>
            <tr class="clsCabereraTabla">
              <th>Id</th>
              <th>Usuario</th>
              <th>Identificación</th>
              <th>Nombres y Apellidos</th>
              <th>Estado</th>
              <th>Area</th>
            </tr>
            </thead>
            
        
      </table>  
    
	 </div>
</div>    
    
    

<!--   <div class="row clsPadding2">	
<div class="col-lg-12 col-xs-12 table-responsive">
<table class="table table-striped">
    <thead>
    <tr class="clsAnchoTabla">
      <th class="clsAnchoTabla" style="background-color:#999; text-align:center !important">Semana</th>
      <th class="clsAnchoTabla">HH FACT</th>
      <th class="clsAnchoTabla">HH NO FACT</th>
      <th class="clsAnchoTabla">HH ANDDES</th>
      <th class="clsAnchoTabla">HH TOTAL</th>                  
      <th class="clsAnchoTabla">Cargabilidad</th>                                    
      <th class="clsAnchoTabla">Año</th>
    </tr>
    </thead>
    <tbody>
    <tr>
      <td class="clsAnchoTabla" style="background-color:#999; text-align:center !important">27</td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
      <td class="clsAnchoTabla"></td>
    </tr>              
                                    
    </tbody>
  </table>    
</div>
</div>-->   
<!--  <div class="row clsPadding2">
    <div class="col-lg-4 hidden-xs"></div>
    <div class="col-lg-2 col-xs-6">
    <a href="#" class="btn btn-primary btn-block ">Guardar</a>
    </div>
    <div class="col-lg-2 col-xs-6">
    <a href="#" class="btn btn-primary btn-block ">Enviar HH</a>
    </div>
    <div class="col-lg-4 hidden-xs"></div>
</div>-->
<!--<div class="row clsPadding2">	
<div class="col-lg-12 col-xs-12 table-responsive">
<table class="table table-striped">
<caption class="clsCabereraTabla">Semana Actual</caption>
        <thead>
        <tr class="clsCabereraTabla">
          <th>Tarea</th>
          <th>FF/NF/AN</th>
          <th>¿Recuperable?</th>
          <th>GP</th>
          <th>Sustento</th>                  
          <th>Entregable</th>                                    
          <th>Lun</th>
          <th>Mar</th>
          <th>Mie</th>
          <th>Jue</th>
          <th>Vie</th>
          <th>Sáb</th>
          <th>Dom</th>
          <th>Total</th>
          <th>Barra Avance<br>(Área / Tarea)</th>
          <th>Acciones</th>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td class="clsAnchoTabla" style="min-width:120px !important">
            <div class="input-group">
            <input id="new-event" type="text" class="form-control" placeholder="Tarea:">
                <div class="input-group-btn">
                <button id="add-new-event" type="button" class="btn btn-primary btn-flat">...</button>
                </div>
            </div>                  
          
          </td>
          <td class="clsAnchoTabla">FF</td>
          <td class="clsAnchoTabla" style="min-width:120px !important">
          <select class="form-control">
            <option>Compensable</option>
            <option>Salud/Enfermedad</option>
          </select>                  </td>
          <td class="clsAnchoTabla">JA</td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla">Ent1 / Ent2</td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla" style="min-width:150px !important">
          <div class="clsBarra">
            <div class="clsAvance" style="width:45px;"></div>
          </div>                
          <div class="clsBarra">
            <div class="clsAvance" style="width:75px;"></div>
          </div>                  
          </td>
          <td class="clsAnchoTabla">
                <a href="#" class="fa fa-plus"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-trash"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-save"></a>&nbsp;&nbsp;&nbsp;&nbsp;
          </td>                                    
        </tr>  
        <tr>
          <td class="clsAnchoTabla" style="min-width:120px !important">
            <div class="input-group">
            <input id="new-event" type="text" class="form-control" placeholder="Tarea:">
                <div class="input-group-btn">
                <button id="add-new-event" type="button" class="btn btn-primary btn-flat">...</button>
                </div>
            </div>                  
          
          </td>
          <td class="clsAnchoTabla">FF</td>
          <td class="clsAnchoTabla" style="min-width:120px !important">
          <select class="form-control">
            <option>Compensable</option>
            <option>Salud/Enfermedad</option>
          </select>                  </td>
          <td class="clsAnchoTabla">JA</td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla">Ent1 / Ent2</td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"><a href="#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla" style="min-width:150px !important">
          <div class="clsBarra">
            <div class="clsAvance" style="width:45px;"></div>
          </div>                
          <div class="clsBarra">
            <div class="clsAvance" style="width:75px;"></div>
          </div>                  
          </td>
          <td class="clsAnchoTabla">
                <a href="#" class="fa fa-plus"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-trash"></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="#" class="fa fa-save"></a>&nbsp;&nbsp;&nbsp;&nbsp;
          </td>                                    
        </tr>
          
        <tfoot>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td>Total</td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td class="clsAnchoTabla"></td>
          <td></td>
          <td></td>
          <td></td>                                    
        </tfoot>                                              
                                        
        </tbody>
      </table>    
</div>
</div> -->       
      
        
    </div><!-- fin parte central -->
    
    <div class="col-lg-1 hidden-xs"></div>
 
</div>


    </section>

    <!-- /.content -->
    <div id="modalUser">
    @include('partials.addUsuario')
    </div>

</div>

<!-- <script src="plugins/datatables/jquery.dataTables.min.js"></script> -->
<!-- <script src="plugins/datatables/dataTables.bootstrap.min.js"></script> -->
<script>



    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';  

    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarUsuarios',
      "columns" : [
        {data : 'cpersona', name: 'tp.cpersona'},
        {data : 'nombre', name: 'tu.nombre'},
        {data : 'identificacion', name: 'tp.identificacion'},
        {data : 'nombrepersona', name: 'tp.nombre'},
        {data : 'estado' , name : 'tc.descripcion'},
        {data : 'area' , name : 'tca.descripcion'}
  
      ],

      "rowCallback" : function( row, data ) {

          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });  

     $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/         

        $(this).toggleClass('selected');
      }); 


  function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(7));
        //getUrl('editarUsuarios/'+id.substring(7),'');
        //$("#addUs").modal("show");
        $.ajax({

                url:'editarUsuarios/'+id.substring(7),
                type: 'GET',
                /*data : $("#frmusuarios").serialize(),*/
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#modalUser").html(data);
                    $('#div_carga').hide(); 
                    $("#addUs").modal("show");
                    
                },            
            });
    }else{
        $('.alert').show();
    }
  }

    $("#addUs").on('show.bs.modal',function(e){
         
          }); 

/*$("#idAddUsua").on("click", */function grabar(e){
           
             $.ajax({

                url:'agregarUsuario',
                type: 'POST',
                data : $("#frmusuarios").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#resultado").html(data);
                    $('#div_carga').hide(); 
                    $('.modal-backdrop').remove();
                    
                },            
            });
          
        }/*);*/  

     $('.select-box').chosen(
        {
            allow_single_deselect: true
        });


</script>  