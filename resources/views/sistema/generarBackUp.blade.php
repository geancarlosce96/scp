<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración del Sistema
        <small>Generar Backup</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
        <li class="active"> Configuración del Sistema</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">





   <div class="row clsPadding2">
   		<div class="col-lg-12 col-xs-12 clsTitulo">Esquema de BD:</div>
   </div>
   <div class="row clsPadding2">
   		
        <div class="col-lg-3 col-xs-3" style="text-align:left !important;">
			<input type="checkbox"><label class="clsTxtNormal">&nbsp;&nbsp;ERP ANDDES:</label>
        </div>
        <div class="col-lg-3 col-xs-3" style="text-align:left !important;">
			<input type="checkbox"><labelclass="clsTxtNormal">&nbsp;&nbsp;HIS ERP ANDDES:</label>
        </div>        
        <div class="col-lg-6 col-xs-6"></div>                
   </div>   
   <div class="row clsPadding2">
   		<div class="col-lg-2 col-xs-2">Ruta:</div>
        <div class="col-lg-7 col-xs-7">
        	<input class="form-control input-sm" type="text" placeholder="Ruta">  
        </div>
        <div class="col-lg-3 col-xs-3">
        	<a href="#" class="btn btn-primary btn-block ">Examinar...</a>
        </div>
   </div>   
   
   <div class="row clsPadding2">
   		<div class="col-lg-2 col-xs-2">Observación:</div>
        <div class="col-lg-9 col-xs-9">
        	<textarea class="form-control" rows="3" placeholder="Observación ..."></textarea> 
        </div>
        <div class="col-lg-1 col-xs-1"></div>
   </div>      


<div class="row clsPadding2">
    <div class="col-lg-2 col-xs-2 clsPadding">
    	<a href="#" class="btn btn-primary btn-block ">Nuevo</a>
    </div>
</div>      
        
        
        
        
        
        
        
        
        
    </div><!-- fin parte central -->
    
    <div class="col-lg-1 hidden-xs"></div>

</div>

<!--   *******************************************  -->
</div>