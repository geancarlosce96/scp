<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración
        <small>Configuración del Sistema</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Configuración</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">

     {!! Form::open(array('url' => 'grabarConfiguracion','method' => 'POST','id' =>'frmconfig')) !!}
        {!! Form::hidden('cconfiguracion',(isset($configuracion)==1?$configuracion->cconfiguracion:''),array('id'=>'cconfiguracion')) !!}
        {!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}  
 

<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
  Servidor de Correo:
    </div>  
</div>



<div class="row clsPadding2">	
    <div class="col-lg-6 col-xs-6">
    <label class="clsTxtNormal">Servidor SMTP:</label>
        <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-server" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name= "sevidorsmtp" id="sevidorsmtp" placeholder="smtp.gmail.com" value="{{(isset($sevidorsmtp->sevidorsmtp)==1?$sevidorsmtp->sevidorsmtp:'')}}">     
          </div>
    </div>
   <!-- <div class="col-lg-2 col-xs-2">
    	<a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
        	<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;<b>Guardar</b></a>
    </div>-->
</div>

<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
  Seguridad:
    </div>  
</div>
<div class="row clsPadding2">	
    <div class="col-lg-5 col-xs-5">
    <label class="clsTxtNormal">Tiempo Sesión:</label>
        <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="tiemposesion" id="" placeholder="tiemposesion" value="{{(isset($tiemposesion->tiemposesion)==1?$tiemposesion->tiemposesion:'')}}">
        </div>     
    </div>
    <div class="col-lg-5 col-xs-5">
    <label class="clsTxtNormal">Tiempo caducidad:</label>
        <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="tiempocaducidad" id="tiempocaducidad" placeholder="" value="{{(isset($tiempocaducidad->tiempocaducidad)==1?$tiempocaducidad->tiempocaducidad:'')}}">  
          </div>
    </div>    
    <!--<div class="col-lg-2 col-xs-2">
    	<a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
        	<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;<b>Guardar</b></a>
    </div>-->
</div>
<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
  Alertas y notificaciones:
    </div>  
</div>
<div class="row clsPadding2">	
    <div class="col-lg-5 col-xs-5">
    <label class="clsTxtNormal">Correo remitente de alertas:</label>
        <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-bell" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="correo_remi_ale" id="correo_remi_ale" placeholder="alertas@anddes.com" value="{{(isset($correo_remi_ale->correo_remi_ale)==1?$correo_remi_ale->correo_remi_ale:'')}}">     
          </div>
    </div>
    <div class="col-lg-5 col-xs-5">
    <label class="clsTxtNormal">Correo destino de notificaciones de Sistema:</label>
        <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-commenting-o" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="correo_dest_not" id="" placeholder="correo_dest_not" value="{{(isset($correo_dest_not->correo_dest_not)==1?$correo_dest_not->correo_dest_not:'')}}">     
          </div>
    </div>    
   <!-- <div class="col-lg-2 col-xs-2">
    	<a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
        	<i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;<b>Guardar</b></a>
    </div>-->
</div>

<div class="row clsPadding2">
    <div class="col-lg-12 col-xs-12 clsTitulo">
  Generales:
    </div>  
</div>

<div class="row clsPadding2"> 
    <div class="col-lg-5 col-xs-5">
    <label class="clsTxtNormal">Tiempo de Permanencia:</label>
        <div class="input-group">
           <span class="input-group-addon"><i class="fa fa-clock-o" aria-hidden="true"></i></span>
            <input type="text" class="form-control" name="tiempoper" id="" placeholder="tiempoper" value="{{(isset($tiempoper->tiempoper)==1?$tiempoper->tiempoper:'')}}">     
          </div>
    </div>
     
   <!-- <div class="col-lg-2 col-xs-2">
      <a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
          <i class="fa fa-floppy-o" aria-hidden="true"></i>&nbsp;<b>Guardar</b></a>
    </div>-->
</div>

<div class="row clsPadding">
                    <div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
                </div>

                <div class="row clsPadding2">
                    <div class="col-lg-4 col-xs-4 cls-centered"></div>
                        
                        <button type="button" class="btn btn-primary btn-block" style="width:200px" onclick="grabar()"><b>Grabar</b></button>                        
                    </div>
                </div>
<!-- fin  -->
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 
</div>
    </section>
    <!-- /.content -->
  </div>

<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
<script>
      function grabar(){
           
             $.ajax({

                url:'grabarConfiguracion',
                type: 'POST',
                data : $("#frmconfig").serialize(),
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){
                    $("#resultado").html(data);
                    $('#div_carga').hide(); 
                    
                },            
            });
          
        }

</script>