<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Configuración
        <small>Trazabilidad por Proyecto</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Configuración</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 col-xs-12">

  
  
<div class="row clsPadding2">	
    <div class="col-lg-1 col-xs-1">Proyecto:</div>
    <div class="col-lg-9 col-xs-9">
    	<input type="text" class="form-control" id="" placeholder="Proyecto...">
    </div> 
    <div class="col-lg-2 col-xs-2">
	    <a href="#"class="btn btn-primary btn-block"><b>...</b></a>
    </div>  
</div>

<div class="row clsPadding2">	
    <div class="col-lg-4 col-xs-4">
        <label class="clsTxtNormal">Fecha Desde:</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker" placeholder="Desde">
        </div>  
    </div>
    <div class="col-lg-4 col-xs-4">
        <label class="clsTxtNormal">Fecha Hasta:</label>
        <div class="input-group date">
          <div class="input-group-addon">
            <i class="fa fa-calendar"></i>
          </div>
          <input type="text" class="form-control pull-right" id="datepicker" placeholder="Hasta">
        </div>  
    </div>    
    <div class="col-lg-2 col-xs-2">
    	<a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
        	<b>Visualizar</b></a>
    </div>
    <div class="col-lg-2 col-xs-2">
    	<a href="#"class="btn btn-primary btn-block" style="margin-top:25px;">
        	<b>Imprimir</b></a>
    </div>    
</div>
<div class="row clsPadding2">	
    <div class="col-lg-12 col-xs-12 table-responsive">
      <table class="table table-striped">
      	<thead>
        <tr>
        	<td class="clsCabereraTabla">Usuario</td>
            <td class="clsCabereraTabla">Información</td>
            <td class="clsCabereraTabla">Fecha / Hora</td>                                   
            <td class="clsCabereraTabla">Acción</td>
          </tr>  
         </thead>
         <tbody>
         	<tr>
        	<td class="clsAnchoTabla">User 1</td>
            <td class="clsAnchoTabla">Info 1</td>
            <td class="clsAnchoTablaMax">28/7/2016 4:37pm</td>            
            <td class="clsAnchoDia">
				<a href="#" data-toggle="tooltip" data-container="body" title="Ver Trazabilidad">
                <i class="fa fa-eye" aria-hidden="true"></i></a>            
            </td>            
            </tr>
         	<tr>
        	<td class="clsAnchoTabla">GP 2</td>
            <td class="clsAnchoTabla">Info 7</td>
            <td class="clsAnchoTablaMax">23/8/2016 11:45am</td>            
            <td class="clsAnchoDia">
				<a href="#" data-toggle="tooltip" data-container="body" title="Ver Trazabilidad">
                <i class="fa fa-eye" aria-hidden="true"></i></a>            
            </td>            
            </tr>                      
         </tbody>
      </table>  
     </div>
</div>
<br><br><br><br>

<div class="col-lg-12 col-xs-12" style="border:1px solid #CCC; border-radius:2px;">
 
<div class="row clsPadding2">
	<div class="col-lg-12 col-xs-12">Detalle de Trazabilidad</div>
</div>
<div class="row clsPadding">
	<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>
<div class="row clsPadding">
	<div class="col-lg-1 hidden-xs"></div>
    <div class="col-lg-10 table-responsive">
      <table class="table table-striped">
      	<thead>
        <tr>
        	<td class="clsCabereraTabla">Proyecto</td>
            <td class="clsCabereraTabla">Tipo Dato</td>
            <td class="clsCabereraTabla">Inf. Anterior</td>
            <td class="clsCabereraTabla">Inf. Posterior</td>                                   
            <td class="clsCabereraTabla">Sel</td>
          </tr>  
         </thead>
         <tbody>
         	<tr> 
            	<td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoDia">
                	<input type="checkbox">
                </td>          
            </tr> 
         	<tr> 
            	<td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoTabla"></td>
                <td class="clsAnchoDia">
                	<input type="checkbox">
                </td>          
            </tr>                 
         </tbody>
      </table>     
    </div>
	<div class="col-lg-1 hidden-xs"></div>    
</div>
<div class="row clsPadding">
	<div class="col-lg-12 col-xs-12" style="background-color:#0069aa; max-height:1px;"></div>
</div>
<div class="row clsPadding2">
	<div class="col-lg-2 col-xs-2">
	    <a href="#" class="btn btn-primary btn-block "data-toggle="tooltip" data-container="body" title="Imprimir">
         <b>Imprimir</b></a>
    </div>
	<div class="col-lg-2 col-xs-2">
	    <a href="#" class="btn btn-primary btn-block "data-toggle="tooltip" data-container="body" title="Cerrar">
         <b>Cerrar</b></a>
    </div> 
     <div class="col-lg-8 col-xs-8"></div>  
</div>

</div>




<!-- fin  -->
    </div>
    <div class="col-lg-1 hidden-xs"></div>
 






</div>
    </section>
    <!-- /.content -->
  </div>