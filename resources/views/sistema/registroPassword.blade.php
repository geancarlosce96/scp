<!DOCTYPE html>
<html>




<head>
   @if(isset($nuevo))
   @if($nuevo=='si')
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Sistema de Control de Proyectos | Anddes</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
  <!-- Font Awesome -->

  <link rel="stylesheet" href="pages/css/font-awesome.min.css">
  <!-- Ionicons -->

  <link rel="stylesheet" href="pages/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/iCheck/square/blue.css">


  <script src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"> </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"> </script>
  <script src="//cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"> </script>
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <!-- <script src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script> -->
  <script src="plugins/jQueryUI/jquery-ui.min.js"></script>
  <script src="plugins/jQuery/jquery-2.2.3.min.js"></script>

  <script src="bootstrap/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
  

        


</head>
<body class="hold-transition login-page">
<img alt="" src="img/bkgAndes.jpg" id="bkgSistema" /> 
<div class="login-box">

  <div class="login-logo">

    <img src="img/logo.jpg">
  </div>

  <div class="login-box-body">
    <p class="login-box-msg"><strong style="font-size: 12px;">Cambio de Contraseña</strong></p>
    {!! Form::open(array('url' => 'agregarPassword','method' => 'POST','id' =>'frmpassword','class'=>'form-horizontal')) !!}
      {!! Form::hidden('cusuario',(isset($usuario)==1?$usuario:''),array('id'=>'cusuario')) !!}   
      {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 

      @if(isset($nuevo))

      <input type="hidden" id="nuevo" name="nuevo" value="{{$nuevo}}">


      <div class="form-group has-feedback">
       
        <input type="text" class="form-control" id="usuario" style="height:30px"  name="usuario" value="{{$nomusuario}}" readonly="true">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>


      <div class="form-group has-feedback">
       

        <input type="password" class="form-control" style="height:30px" id="passwordAct" placeholder="Contraseña Actual" name="passwordAct" onkeyup="getpasw()"" onLoad="limpiarPass()">  

        <!-- Valor de contraseña encriptada ingresada por el usuario -->
        <input type="hidden" class="form-control"  id="passwordGETEncriptado" placeholder="Contraseña Ingresada" name="passwordGETEncriptado" value="">    
        <!-- Contraseña encriptada Actual -->

        <input type="hidden" class="form-control" id="passwordActEncriptado" placeholder="Contraseña actual" name="passwordActEncriptado" value="{{$passwordActual}}"> 

        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="password"  style="height:30px" placeholder="Contraseña nueva" name="password">  
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="passwordconfirm" style="height:30px" placeholder="Confirmar contraseña nueva" name="passwordconfirm"> 
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>

      @endif


      <div class="row">
        
        <div class="col-xs-12">
          <button type="submit" class="btn btn-primary btn-block btn-flat"><strong>CAMBIAR</strong></button>
        </div>

      </div>
    {!! Form::close() !!}
    <div class="social-auth-links text-center">
      <!-- <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google  "></i> Ingrese usando su cuenta de GMAIL</a> -->
    </div>


    <!-- <a href="#">Olvide mi clave</a><br> -->
    
  </div>

</div>

@endif
@endif


@if(isset($nuevo))
@if($nuevo=='no')

<div class="content-wrapper" style="font-size: 12px;">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Configuración del Sistema
      <small>Cambio de contraseña</small>
    </h1>

    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i>Inicio</a></li>
      <li class="active"> Configuración del Sistema</li>
    </ol>
   
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="login-box-body">
      {!! Form::open(array('url' => 'agregarPassword','method' => 'POST','id' =>'frmpassword','class'=>'form-horizontal')) !!}
      {!! Form::hidden('cusuario',(isset($usuario)==1?$usuario:''),array('id'=>'cusuario')) !!}   
      {!! Form::hidden('_token',csrf_token(),array('id'=>'_token'),array('id'=>'_token')) !!} 
        <div class="form-group has-feedback">
          @include('partials.passwordUsuario')
        </div>
        <div class="row col-lg-12 col-xs-12">
          
        </div>
        <div class="row col-lg-12 col-xs-12">

          <div class="col-lg-2 col-xs-12 "></div>

          <div class="col-lg-2 col-xs-12 ">         
            <button style="width: 100%;" type="submit" class="btn btn-primary btn-block btn-flat"><strong>CAMBIAR</strong></button>
        </div>
          </div>
          <!-- /.col -->
        </div>
      {!! Form::close() !!}
    </div>
  </section>
</div>


@endif
@endif



<script>

function getpasw(){
 
  $.ajax({
                url:'getPasswordEncrip',
                type: 'POST',            

                data:  {
                  "_token": "{{ csrf_token() }}",
                        "passwordAct":$("#passwordAct").val()
                        
                },
                beforeSend: function(){
                   
                },
                success: function(data){ 
                  $("#passwordGETEncriptado").val(data);
                  $("#passwordGETEncriptado").html(data);

               
                },            
            });

}


  $("#frmpassword").on("submit",function (e){ 
     e.preventDefault();

    if($("#passwordActEncriptado").val()!=$("#passwordGETEncriptado").val()){

      alert('La contraseña actual es incorrecta'); 

      $("#passwordAct").val('');
      $("#password").val('');
      $("#passwordconfirm").val('');

      return;                
      
    }
    if($("#password").val()==''){
      alert('Ingrese la contraseña nueva')    
      return;                
      
    }
    if($("#passwordconfirm").val()==''){
      alert('Vuelva a ingresar la contraseña nueva')    
      return;                
      
    }
    if($("#password").val()!=$("#passwordconfirm").val()){
      alert('Las contraseñas ingresadas no coinciden'); 
      $("#password").val('');
      $("#passwordconfirm").val('');   
      return;                
      
    }

     $.ajax({
                url:'agregarPassword',
                type: 'POST',
                data : $(this).serialize(),"_token": "{{ csrf_token() }}",
                beforeSend: function(){
                    $('#div_carga').show(); 
                },
                success: function(data){ 

                  if(data=='ok'){
                    $('#div_carga').hide(); 
                    alert('Contraseña actualizada');
                    location.reload();
                  } 

             
                },            
       
            });
   
       
        });

  function limpiarPass(){
    $("#passwordAct").val('');
  }

</script>

</body>

</html>