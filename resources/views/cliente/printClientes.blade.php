<div class="content-wrapper"> 
<div class="row">	
<div class="col-lg-12 col-xs-12 table-responsive" id="">
<section class="content">




<table  style="width:100%; border-collapse: collapse; size: 0.5px;" class="table table-bordered" style=" font-family: Arial, Helvetica, sans-serif;font-size: 10px;">
    <tr>
        <th style="border:1px solid #000000FF; width: 15%;"><img style="width: 150px;" src="images/logorpt.jpg"></th>
        <th rowspan="2" style="border:1px solid #000000FF; width: 50%;text-align: center;"> <p>Propuestas y Licitaciones <br> Listado de Contactos </p></th>
        <th style="border:1px solid #000000FF; width: 30%;">  </th>
    </tr>
    <tr>
        <th style="border:1px solid #000000FF; width: 15%;text-align: center;color: #fff;background-color:rgba(0, 105, 173, 1)"> SIG AND </th>
        <th style="border:1px solid #000000FF; width: 30%;text-align: center; font-size: 14px;"> 10-AND-xx-FOR-0xxx / R0/ <br> <?php echo date('d-m-y');?> </th>
    </tr>
</table>

<br>
<div class="row">
    <table style="width : 700px; font-family: Arial, Helvetica, sans-serif;font-size: 12px;text-align: left;">
        <tr>
            <th style="text-align: left; width: 40px">Cliente:</th>
            <th style="text-align: left; width: 150px"> <?php  echo (isset($clientes->nombre)==1?$clientes->nombre:'');?></th>
            <th style="text-align: right; width: 80px">RUC:</th>
            <th style="text-align: right; width: 60px"> <?php  echo (isset($clientes->identificacion)==1?$clientes->identificacion:'');?></th>
            
        </tr>
        <tr>
            <th style="text-align: left; width: 80px">País:</th>
            <th style="text-align: left; width: 120px"> <?php  echo (isset($pais->descripcion)==1?$pais->descripcion:'')?></th>
           
            
        </tr>

    </table>
   
</div>
<hr color="black" size=3>

<div class="row">
	
	<div class="col-sm-12">

		<?php if (isset($clientes)==1) { ?>
		<?php foreach ($aunidadminera as $aum) { ?>

            	<section class="panel" style="border:1px solid #000000FF;border-radius: 5px;">
            		<header class="panel-heading modal-header" style=" font-family: Arial, Helvetica, sans-serif;font-size: 12px; padding: 7px;">
	    			
			            <?php 
			            	echo "<strong>";
			            	echo "Unidad Minera: ";	
			             	echo "</strong>";
			            	echo $aum['nombre'];
	                        echo "<br>";
	                        echo "<strong>";
	                        echo "Código: ";		                        
	                        echo "</strong>";
	                        echo $aum['codigo'];
	                       	echo "&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;";
	                        echo "<strong>";
	                        echo "Estado: ";		                        
	                        echo "</strong>";
	                        echo $aum['destado'];
	                        echo "</pre>";
	                        
	                    ?>	
	                    
	                    <hr style="color: #E0E0E0FF;size: 1px;" />
		            </header>

		            <div class="panel-body">
                    	<div class="adv-table" style="padding: 7px;" >
		            		<table class="display table table-bordered table-striped table-condensed" style="font-family: Arial, Helvetica, sans-serif; font-size:10px; width: 100%;border-collapse: collapse;">
		            			<thead>
	                                <tr >
	                                    <th style="border:1px solid #000000FF;">Ítem</th>
	                                    <th style="border:1px solid #000000FF;"><center>Nombre</center></th>
	                                    <th style="border:1px solid #000000FF;">Apellido Paterno</th>
	                                    <th style="border:1px solid #000000FF;">Apellido Materno</th>
	                                    <th style="border:1px solid #000000FF;">Cargo</th>
	                                    <th style="border:1px solid #000000FF;">Tipo</th>
	                                    <th style="border:1px solid #000000FF;">Correo</th>
	                                    <th style="border:1px solid #000000FF;">Teléfono</th>
	                                    <th style="border:1px solid #000000FF;">Anexo</th>
	                                    <th style="border:1px solid #000000FF;">Celular 1</th>
	                                    <th style="border:1px solid #000000FF;">Celular 2</th>
	                                    
	                                </tr>

	                            </thead>

	                            <tbody>
	                            	<?php foreach ($aum['contactos'] as $ac) { ?>

		                            	<tr class="gradeX" >	

			                            	<td style="border:1px solid #000000FF;"><span><?php echo $ac['item'] ; ?></span></td>
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['nombres'] ; ?></span></td>
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['apaterno'] ; ?></span></td>	
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['amaterno'] ; ?></span></td>
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['cargo'] ; ?></span></td>
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['tipo'] ; ?></span></td>	
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['email'] ; ?></span></td>
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['telefono'] ; ?></span></td>               	
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['anexo'] ; ?></span></td>	
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['cel1'] ; ?></span></td>
			                            	<td style="border:1px solid #000000FF; "><span><?php echo $ac['cel2'] ; ?></span></td>
			                          
								            
								        </tr> 
							        <?php } ?>
				                            	
	                            </tbody>
		            			
		            		</table>
		            		
		            	</div>
		            	
		            </div>
		            <br>

            	</section>
            	<br>




            <?php } ?>
        <?php } ?>
	</div>

</div>


</section>
</div>
</div>
</div>