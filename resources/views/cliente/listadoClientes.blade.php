<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clientes
        <small>Listado de Clientes</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Clientes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">
    <div class="row clsPadding2">
          <div class="col-lg-2 col-xs-12">
      
           </div>
           
          <div class="col-lg-2 col-xs-12" style="margin-top:25px;">
            <button type="button" onclick="getUrl('registroClientes');" class="btn btn-primary btn-block"><b>Nuevo</b></button> 
          </div> 
          <div class="col-lg-2 col-xs-12" style="margin-top:25px;">
            <button type="button"  onclick="goEditar();" class="btn btn-primary btn-block"><b>Editar</b></button> 
          </div> 
          <!--<div class="col-lg-2 col-xs-12" style="margin-top:25px;">
            <button type="button" class="btn btn-primary btn-block" disabled="true"><b>Eliminar</b></button> 
          </div>--> 
          <div class="col-lg-2 col-xs-12" style="margin-top:25px;">
            <button type="button"  id="btnPrint" class="btn btn-primary btn-block" ><b>Imprimir</b></button> 
          </div>                                                  
          <div class="col-lg-2 col-xs-12">

          </div>              
   </div>   
 
   
  <div class="row clsPadding2"> 
    <div class="col-lg-12 col-xs-12 table-responsive">

          <table class="table table-striped" id="listado">
            <thead>
            <tr class="clsCabereraTabla">
              <th>Id</th> 
              <th>RUC</th> 
              <th>Nombre</th>
              <th>Unidad Minera</th>

            </tr>
            </thead>
          </table>
    </div>
  </div>   





<!--:-->

    

        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
    </section>
    <!-- /.content -->
  </div>

  <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

<script>
    var selected =[];
    $.fn.dataTable.ext.errMode = 'throw';     
    var table=$("#listado").DataTable({
      "processing" : true,
      "serverSide" : true,
      "ajax" : 'listarClientes',
      "columns" : [
        {data : 'cpersona', name: 'tper.cpersona'},
        {data : 'identificacion', name: 'tper.identificacion'},
        {data : 'cliente', name: 'tper.nombre'},
        {data : 'uminera' , name : 'tu.nombre'},


      ],

      "rowCallback" : function( row, data ) {
          
          if ( $.inArray(data.DT_RowId, selected) !== -1 ) {

              $(row).addClass('selected');
          }
      }, 
      "language": {
            "lengthMenu": "Mostrar _MENU_ registros por página",
            "zeroRecords": "Sin Resultados",
            "info": "Página _PAGE_ de _PAGES_",
            "infoEmpty": "No existe registros disponibles",
            "infoFiltered": "(filtrado de un _MAX_ total de registros)",
            "search":         "Buscar:",
            "processing":     "Procesando...",
            "paginate": {
                "first":      "Inicio",
                "last":       "Ultimo",
                "next":       "Siguiente",
                "previous":   "Anterior"
            },
            "loadingRecords": "Cargando..."
        }
    });

    
    $('#listado tbody').on('click', 'tr', function () {
        var id = this.id;
        var index = $.inArray(id, selected);
        var table = $('#listado').DataTable();
        table.$('tr.selected').removeClass('selected');
        selected.splice(0,selected.length);
        if ( index === -1 ) {
            selected.push( id );

        }/* else {
            selected.splice( index, 1 );
        }*/
        
        $(this).toggleClass('selected');
    });

  function goEditar(){

    var id =0;
    if (selected.length > 0 ){
        id = selected[0];
        //alert(id.substring(4));
        getUrl('editarCliente/'+id.substring(4),'');
    }else{
        $('.alert').show();
    }
  }

  $("#btnPrint").on('click',function(){      
      goPrintCliente();
    });

  function goPrintCliente(){
     var id =0;

    if (selected.length > 0 ){
        id = selected[0]; 
        win = window.open('printClientes/'+id.substring(4),'_blank');     
        
    }else{
        $('.alert').show();
    }


  }


</script>