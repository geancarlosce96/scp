<div class="box box-primary" >
{!! Form::open(array('url' => 'grabarCliente','method' => 'POST','id' =>'frmcliente')) !!}
{!! Form::hidden('cpersona',(isset($persona)==1?$persona->cpersona:''),array('id'=>'cpersona')) !!}
{!! Form::hidden('vista',(isset($vista)==1?$vista:''),array('id'=>'vista')) !!}
{!! Form::hidden('_token',csrf_token(),array('id'=>'_token')) !!}  
  
<div class="row">	
    <div class="col-lg-2 col-xs-12">
  		<label class="control-label">RUC:</label>
        {!! Form::text('identificacion',(isset($persona->identificacion)==1?$persona->identificacion:''), array('class'=>'form-control input-sm','placeholder' => 'RUC','id'=>'identificacion','maxlength'=>'11') ) !!}
    </div>
    <div class="col-lg-4 col-xs-12">
   		<label class="control-label">Razón Social:</label>
        {!! Form::text('razonsocial',(isset($personajur->razonsocial)==1?$personajur->razonsocial:''), array('class'=>'form-control input-sm','placeholder' => 'Razon Social','id'=>'razonsocial','maxlength'=>'150') ) !!}
    </div>   
    <div class="col-lg-4 col-xs-12">
    	<label class="control-label">Nombre Comercial:</label>   
        {!! Form::text('nombrecomercial',(isset($personajur->nombrecomercial)==1?$personajur->nombrecomercial:''), array('class'=>'form-control input-sm','placeholder' => 'Nombre Comercial','id'=>'nombrecomercial','maxlength'=>'150') ) !!} 
    </div>
</div>

<div class="row">	
    <div class="col-lg-3 col-xs-12">
  		<label class="control-label">Abreviatura:</label>
        {!! Form::text('abrevia',(isset($personajur->abrevia)==1?$personajur->abrevia:''), array('class'=>'form-control input-sm','placeholder' => 'Abreviatura','id'=>'abrevia') ) !!}
    </div>
    <div class="col-lg-4 col-xs-12">
   		<label class="control-label">Dirección:</label>
        {!! Form::text('direccion',(isset($personadir->direccion)==1?$personadir->direccion:''), array('class'=>'form-control input-sm','placeholder' => 'Dirección','id'=>'direccion') ) !!}
    </div>   
    <div class="col-lg-3 col-xs-12">
   		<label class="control-label">Web:</label>
        {!! Form::text('web',(isset($personajur->web)==1?$personajur->web:''), array('class'=>'form-control input-sm','placeholder' => 'WEB','id'=>'web') ) !!}
    </div>   
</div>
<div class="row">	
    <div class="col-lg-3 col-xs-6">
    <label class="control-label">País:(*)</label>
      {!! Form::select('pais',(isset($tpais)==1?$tpais:array() ),(isset($personadir->cpais)==1?$personadir->cpais:''),array('class' => 'form-control','id'=>'pais')) !!}     
    
    </div>
    <div class="col-lg-3 col-xs-6">
    <label class="control-label">Departamento:(*)</label>
    {!! Form::select('dpto',(isset($tdpto)==1?$tdpto:array() ),(isset($cdpto)==1?$cdpto:''),array('class' => 'form-control','id'=>'dpto')) !!}       
    </div>    
    <div class="col-lg-3 col-xs-6">
    <label class="control-label">Provincia:(*)</label>
    {!! Form::select('prov',(isset($tprov)==1?$tprov:array() ),(isset($cprov)==1?$cprov:''),array('class' => 'form-control','id'=>'prov')) !!}     
    </div>

    <div class="col-lg-3 col-xs-6">
    <label class="control-label">Distrito:(*)</label>
    {!! Form::select('dis',(isset($tdis)==1?$tdis:array() ),(isset($cubigeo)==1?$cubigeo:''),array('class' => 'form-control','id'=>'dis')) !!}    
    </div>
</div>
<div class="row">	
</div>

    <div class="modal-footer">
	  	<button type="button" class="btn btn-primary pull-right"  id='ModalUminera'>Unidad Minera</button>
	    <button type="submit" class="btn btn-primary pull-right" id='grabarcli'>Grabar</button>
	</div>

{!! Form::close() !!}
</div>