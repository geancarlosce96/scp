<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Clientes
        <small>Registro de Clientes</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li class="active">Clientes</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">



    <div class="row">
    <!--<div class="col-lg-1 hidden-xs"></div>-->
    <div class="col-lg-12 col-xs-12">

@include('cliente.fromClientes')
  <!-- Modal Unidad Minera-->
  @include('partials.modalRegistroUnidadMinera')
      
  <!-- Fin Modal Unidad Minera-->

<!-- Modal Contactos de Unidad Minera-->

  @include('partials.modalRegistroContactoUnidadMinera')
      
<!-- Fin Modal Contactos Unidad Minera-->       
 

        
    </div>
    <!--<div class="col-lg-1 hidden-xs"></div>-->
 






</div>
    </section>
    <!-- /.content -->
  </div>

  <!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables/dataTables.bootstrap.min.js"></script>

        
<script src="js/clientes.js"></script>

