<!-- Single button -->
<div class="btn-group">
  <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
  {{ (isset($propuesta)==1?"":"disabled") }}>
    <img src="img/acceso.jpg" > <span class="caret"></span>
  </button>
  <ul class="dropdown-menu">
    <li><a href="#" onclick="getUrl('editarPropuesta/{{ (isset($propuesta)==1?$propuesta->cpropuesta:'') }}','');">Registro de Propuesta</a></li>
    <li><a href="#" onclick="getUrl('editarDisciplinaPropuesta/{{ (isset($propuesta)==1?$propuesta->cpropuesta:'') }}','');">Disciplinas de Propuesta</a></li>
    <li><a href="#" onclick="getUrl('editarEstructuraPropuesta/{{ (isset($propuesta)==1?$propuesta->cpropuesta:'') }}','');">Estructura de Propuesta</a></li>
    <!--<li role="separator" class="divider"></li>-->
    <li><a href="#" onclick="getUrl('editarDetallePropuesta/{{ (isset($propuesta)==1?$propuesta->cpropuesta:'') }}','');">Detalle de Propuesta</a></li>
    <li><a href="#" onclick="getUrl('editarConfTransmittal/{{ (isset($propuesta)==1?$propuesta->cpropuesta:'') }}','');">Configuración de Transmittal - Propuesta</a></li>
  </ul>
</div>