<?php
namespace App\Auth;
use App\Models\Tusuario;
use App\Models\Tusuariopassword;
use App\Models\Tusuariosesione;
use App\Models\Tpersona;
// use App\Models\Tpersonadatosempleado;
// use DB;
use Carbon\Carbon;
use Illuminate\Auth\GenericUser;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class CustomUserProvider implements UserProvider {

	protected $model;

    public function __construct()
    {
        //$this->model = $model;
    }
	/**
 * Retrieve a user by their unique identifier.
 *
 * @param  mixed $identifier
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
public function retrieveById($identifier)
{
    // TODO: Implement retrieveById() method.


    $qry = Tusuario::where('cusuario','=',$identifier);

    // $qry = DB::table('tusuarios as tu')
    // ->join('tpersonadatosempleado as tpe','tu.cpersona','=','tpe.cpersona')
    // ->where('tu.cusuario','=',$identifier);

    if($qry->count() >0)
    {
        $user = $qry->select('cusuario', 'nombre','estado','cpersona')->first();
        // $user = $qry->select('tu.cusuario', 'tu.nombre','tu.estado','tu.cpersona','tpe.carea','tpe.ccargo','tpe.ccategoriaprofesional','tpe.ctipocontrato')->first();

        // foreach ($users as $value) {
        //     $user = $value;
        // }
        
        $user_pass = Tusuariopassword::where('cusuario','=',$identifier)->first();  

        $persona  = Tpersona::where('cpersona','=',$user->cpersona)->first();
        // dd($user);
        $attributes = array(
            'id' => $user->cusuario,
            'username' => $user->nombre,
            'password' => $user_pass->password,
            'estado' => $user->estado,
            'name' => $persona->nombre,
        );
        if ($user->estado=='ACT') {
            return $user;
        }
    }
    return null;
}

/**
 * Retrieve a user by by their unique identifier and "remember me" token.
 *
 * @param  mixed $identifier
 * @param  string $token
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
public function retrieveByToken($identifier, $token)
{
    // TODO: Implement retrieveByToken() method.
    $qry = Tusuario::where('cusuario','=',$identifier)->where('remember_token','=',$token);

    if($qry->count() >0)
    {
        $user = $qry->select('cusuario', 'nombre')->first();
		$user_pass = Tusuariopassword::where('cusuario','=',$identifier)->first();

        $persona  = Tpersona::where('cpersona','=',$user->cpersona)->first();        

        $attributes = array(
            'id' => $user->cusuario,
            'username' => $user->nombre,
            'password' => $user_pass->password,
            'name' => $persona->nombre,
        );

        return $user;
    }
    return null;



}

/**
 * Update the "remember me" token for the given user in storage.
 *
 * @param  \Illuminate\Contracts\Auth\Authenticatable $user
 * @param  string $token
 * @return void
 */
public function updateRememberToken(Authenticatable $user, $token)
{
    // TODO: Implement updateRememberToken() method.
    $user->setRememberToken($token);

    $user->save();

}

/**
 * Retrieve a user by the given credentials.
 *
 * @param  array $credentials
 * @return \Illuminate\Contracts\Auth\Authenticatable|null
 */
public function retrieveByCredentials(array $credentials)
{
    // TODO: Implement retrieveByCredentials() method.
    $qry = Tusuario::where('nombre','=',$credentials['username']);

    if($qry->count() >0)
    {
        $user = $qry->select('cusuario','nombre')->first();




        return $user;
    }
    return null;


}

/**
 * Validate a user against the given credentials.
 *
 * @param  \Illuminate\Contracts\Auth\Authenticatable $user
 * @param  array $credentials
 * @return bool
 */
public function validateCredentials(Authenticatable $user, array $credentials)
{
    // TODO: Implement validateCredentials() method.
    // we'll assume if a user was retrieved, it's good
	$user_pass = Tusuariopassword::where('cusuario','=',$user->cusuario)->first();

    if($user->nombre == $credentials['username'] && $user_pass->password == md5($credentials['password'].\Config::get('constants.SALT')))
    {
    	$user_sess = new Tusuariosesione();
    	$user_sess->cusuario = $user->cusuario;
    	$user_sess->fhasta= Carbon::create('2999', '12', '31', '00', '00', '00');
        $user_sess->fdesde = Carbon::now();
        $user_sess->sesionid =  $_SERVER['REMOTE_ADDR'];
        $user_sess->terminal =  $_SERVER['HTTP_HOST'];
        $user_sess->save();

        return true;
    }
    return false;


}
}

?>