<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth/login');
});

// Rutas para matriz...
Route::get('matrizp',[
        'uses'=>'Proyecto\ProyectoController@matrizp',
        'as' => 'matrizp'
        ]
        );



// Rutas para autenticación...
Route::get('login', ['uses' =>'Auth\AuthController@getLogin','as' => 'login']);
Route::post('login', ['uses' =>'Auth\AuthController@postLogin','as' => 'login']);
Route::get('logout', ['uses' =>'Auth\AuthController@getLogout','as' => 'logout']);

    /*
     * Construcción
     */
Route::get('construccion', [function (){
        return view('errors.enConstruccion');
    },'as'=>'construccion'] );

//Error 500
Route::get('error500', [function (){
        return view('errors.500');
    },'as'=>'error500'] );

/* Pruebas de Plugin */
Route::get('vista',function(){
    return view('prueba.plugins');
});
/* Fin de Plugin*/    

Route::group(['middleware' => ['auth','filtro']], function() {

    // Route::get('panel', [
    //    'uses' => 'Propuesta\PropuestaController@welcome', 
    //     'as' => 'panel'
    //     ]
    // );

Route::get('panel', [
   'uses' => 'WelcomeController@welcome',        
    'as' => 'panel'
    ]
);
// Rutas del dashboard
Route::get('calendario', [
    'uses' =>'WelcomeController@entregables',
    'as' =>'calendario'
]);

Route::get('entregblegantt', [
    'uses' =>'WelcomeController@gantentregables',
    'as' =>'entregblegantt'
]);

Route::get('verequipoproyecto', [
    'uses' =>'WelcomeController@verequipoproyecto',
    'as' =>'verequipoproyecto'
]);


/**************************************************************************
    * Data SCP pendiente de HT
    ***************************************************************************/
Route::get('listadodataScp', [
        'uses' =>'Datascp\DatascpController@listadodataScp',
        'as' =>'listadodataScp'
        ]
        );

Route::get('listarHT',[
        'uses' => 'Datascp\DatascpController@listarHT',
        'as' => 'listarHT' ]
        );

Route::get('listadodataScpJI', [
        'uses' =>'Datascp\DatascpController@listadodataScpJI',
        'as' =>'listadodataScpJI'
        ]
        );

Route::get('listarHTJI',[
        'uses' => 'Datascp\DatascpController@listarHTJI',
        'as' => 'listarHTJI' ]
        ); 

Route::get('listadodataScpGP', [
        'uses' =>'Datascp\DatascpController@listadodataScpGP',
        'as' =>'listadodataScpGP'
        ]
        );

Route::get('listarHTGP',[
        'uses' => 'Datascp\DatascpController@listarHTGP',
        'as' => 'listarHTGP' ]
        ); 

/**************************************************************************
    * Data SCP pendiente de HT
    ***************************************************************************/




    /**************************************************************************
    * Propuesta
    ***************************************************************************/
    Route::get('listadoPropuesta', [
        'uses' => 'Propuesta\PropuestaController@listar',
        'as' => 'listadoPropuesta']);

    Route::get('listarPropuestas',
        [
        'uses' =>'Propuesta\PropuestaController@listarGrid' ,
        'as' => 'listarPropuestas'
        ]);

    Route::get('listarSoloPropuestas',
        [
        'uses' =>'Propuesta\PropuestaController@listarSoloPropuestas' ,
        'as' => 'listarSoloPropuestas'
        ]);
    
    Route::post('listaPropuestaEstados/',
        [
        'uses' =>'Propuesta\PropuestaController@listaPropuestaEstados' ,
        'as' => 'listaPropuestaEstados'
        ]);

    Route::get('registroPropuesta', [
        'uses' => 'Propuesta\PropuestaController@registro',
        'as' => 'registroPropuesta']);

    Route::get('editarPropuesta/{id}',[
        'uses' => 'Propuesta\PropuestaController@editar',
         'as' => 'editarPropuesta'
        ]
    );
    Route::post('buscarPropuesta',[
        'uses' => 'Propuesta\PropuestaController@buscar',
            'as' => 'buscarPropuesta'
        ]

    );
    Route::post('grabarPropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabar',
        'as'   => 'grabarPropuesta'
        ]
    );

    Route::post('sgteFlujoPropuesta',[
        'uses' => 'Propuesta\PropuestaController@sgteFlujo',
        'as' => 'sgteFlujoPropuesta'
        ]
        );

    /* gestión de paquetes*/
    Route::get('listarPaquetes/{idPropuesta}',[
        'uses'=>'Propuesta\PropuestaController@mostrarPaquetes',
        'as'=>'listarPaquetes'
        ]);

    Route::get('addPaquete',[
        'uses'=>'Propuesta\PropuestaController@addPaquete',
        'as'=>'addPaquete'
        ]);

    Route::get('delPaquete',[
        'uses'=>'Propuesta\PropuestaController@delPaquete',
        'as'=>'delPaquete'
        ]);
    /* fin gestión de paquetes */
    
    /* Disciplinas de Propuestas */
    Route::get('disciplinaPropuesta', [
        'uses' => 'Propuesta\PropuestaController@disciplina',
        'as' => 'disciplinaPropuesta']);

    Route::get('editarDisciplinaPropuesta/{idPropuesta}',[
        'uses' => 'Propuesta\PropuestaController@editarDisciplina',
        'as' => 'editarDisciplinaPropuesta'
        ]
        );

    Route::post('grabarDisciplinaPropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabarDisciplina',
        'as' => 'grabarDisciplinaPropuesta'
        ]
        );

    Route::post('agregarDisciplinasPropuesta',[
        'uses' => 'Propuesta\PropuestaController@agregarDisciplinas',
        'as' =>'agregarDisciplinasPropuesta'
    ]);
    Route::post('borrarDisciplinasPropuesta',[
        'uses' => 'Propuesta\PropuestaController@borrarDisciplinas',
        'as' =>'borrarDisciplinasPropuesta'
    ]);    
    Route::get('verDisciplinasPropuesta',[
        'uses' => 'Propuesta\PropuestaController@verDisciplinas',
        'as' => 'verDisciplinasPropuesta'
        ]
        );    
    /* Fin de Disciplinas de Propuestas */


    /* Estructura Propuesta */
    Route::get('estructuraPropuesta', [
        'uses' => 'Propuesta\PropuestaController@estructura',
        'as' => 'estructuraPropuesta']);

    Route::get('editarEstructuraPropuesta/{idPropuesta}',[
        'uses' => 'Propuesta\PropuestaController@editarEstructura',
        'as' => 'editarEstructuraPropuesta'
        ]);

    Route::post('grabarEstructuraPropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabarEstructura',
        'as' => 'grabarEstructuraPropuesta'
        ]);

    /* Fin de Estructura Propuesta */

    /* Agregar Actividades*/
    Route::get('listarActividades/{idPropuesta}',[
        'uses' => 'Propuesta\PropuestaController@listarActividades',
        'as' => 'listarActividades'
        ]);
    Route::post('addActividad',[
        'uses' =>'Propuesta\PropuestaController@addActividad',
        'as' => 'addActividad'
        ]);

    

    Route::get('delActividad/{idActividad}',[
        'uses' => 'Propuesta\PropuestaController@delActividad',
        'as' => 'delActividad'
        ]);

    /* Fin de agregar Actividades*/

    /* Agregar checklist*/

    Route::get('btnAddcheckEnt',[
        'uses' =>'Proyecto\ProyectoController@btnAddcheckEnt',
        'as' => 'btnAddcheckEnt'
        ]);

    Route::post('btnAddcheckEntPy',[
        'uses' =>'Proyecto\ProyectoController@btnAddcheckEnt',
        'as' => 'btnAddcheckEntPy'
        ]);     


    /* Fin de agregar Actividades*/

    /* Agregar Estructura*/
    Route::get('addEstructura',[
        'uses' =>'Propuesta\PropuestaController@addEstructura',
        'as' => 'addEstructura'
        ]);

    Route::get('delEstructura/{idEstructura}',[
        'uses' => 'Propuesta\PropuestaController@delEstructura',
        'as' => 'delEstructura'
        ]);

    /* Fin de agregar Estructura*/



    /* Detalle de Propuesta*/
    Route::get('detallePropuesta', [
        'uses' => 'Propuesta\PropuestaController@detalle',
        'as' => 'detallePropuesta']);

    Route::get('importarppavista', 'Propuesta\PropuestaController@importarppavista');
    Route::post('importarppa', 'Propuesta\PropuestaController@importarppa');
    Route::get('verimportarppavista', 'Propuesta\PropuestaController@verimportarppavista');
    Route::get('actividades_ppa_proy', 'Propuesta\PropuestaController@actividades_ppa_proy');
    Route::get('guardar_ascocidos', 'Proyecto\ProyectoController@guardar_ascocidos');
    Route::get('leyendappa', 'Propuesta\PropuestaController@leyendappa');
    Route::get('leyendasig', 'Propuesta\PropuestaController@leyendasig');

    Route::get('editarDetallePropuesta/{idPropuesta}', [
        'uses' => 'Propuesta\PropuestaController@editarDetalle',
        'as' => 'editarDetallePropuesta'
        ]
        );
    Route::get('verHorasDetalleActividadPropuesta/{id}',[
        'uses' => 'Propuesta\PropuestaController@viewDetalleActividad',
        'as' => 'verHorasDetalleActividadPropuesta'
        ]
        );
    Route::post('verDetalleActividadPropuesta',[
        'uses' => 'Propuesta\PropuestaController@viewDetalleActividad',
        'as' => 'verDetalleActividadPropuesta'
    ]);
    Route::get('editHorasSeniorPropuesta',[
        'uses' => 'Propuesta\PropuestaController@editarHorasSenior',
        'as' => 'editHorasSeniorPropuesta'
        ]
        );
    Route::post('saveHorasSeniorPropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabarHorasSenior',
        'as' => 'saveHorasSeniorPropuesta'
        ]
        );    
    Route::get('editHorasDisciplinaPropuesta',[
        'uses' => 'Propuesta\PropuestaController@editarHorasDisciplina',
        'as' => 'editHorasDisciplinaPropuesta'
        ]
        );
    Route::post('saveHorasDisciplinaPropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabarHorasDisciplina',
        'as' => 'saveHorasDisciplinaPropuesta'
        ]
        );  
    Route::get('editHorasApoyoPropuesta',[
        'uses' => 'Propuesta\PropuestaController@editarHorasApoyo',
        'as' => 'editHorasApoyoPropuesta'
        ]
        );
    Route::post('saveHorasApoyoPropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabarHorasApoyo',
        'as' => 'saveHorasApoyoPropuesta'
        ]
        );              
    Route::post('grabarDetallePropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabarDetalle',
        'as' => 'grabarDetallePropuesta'
        ]
        );
    Route::post('uploadFile',[
            'uses' => 'Propuesta\PropuestaController@upload',
        'as' => 'uploadFile'                
        ]);
    Route::post('uploadFileXml',[
            'uses' => 'Proyecto\ProyectoController@uploadXml',
        'as' => 'uploadFileXml'                
        ]);    
    Route::get('addGastosPropuesta',[
        'uses' => 'Propuesta\PropuestaController@addGastos',
        'as' => 'addGastosPropuesta'
        ]
        );
    Route::get('delGastosPropuesta',[
        'uses' => 'Propuesta\PropuestaController@delGastos',
        'as' => 'delGastosPropuesta'
        ]
        );    
    Route::get('addGastosLabPropuesta',[
        'uses' => 'Propuesta\PropuestaController@addGastosLab',
        'as' => 'addGastosLabPropuesta'
        ]
        );
    Route::get('delGastosLabPropuesta',[
        'uses' => 'Propuesta\PropuestaController@delGastosLab',
        'as' => 'delGastosLabPropuesta'
        ]
        );   
    /* Fin de Detalle de Propuesta */

    /* Revisión de Propuesta */
    Route::get('revisionPropuesta', [
        'uses' => 'Propuesta\PropuestaController@revision',
        'as' => 'revisionPropuesta']);

    Route::get('editarRevisionPropuesta/{idPropuesta}',[
        'uses' => 'Propuesta\PropuestaController@editarRevision',
        'as' => 'editarRevisionPropuesta'
        ]);

    Route::post('grabarRevisionPropuesta',[
        'uses' => 'Propuesta\PropuestaController@grabarRevision',
        'as' => 'grabarRevisionPropuesta'
        ]);
    Route::get('confTransmittal', [
        'uses' =>'Cdocumentario\DocumentarioController@configuracionTransmittalPropuesta',
        'as'=>'confTransmittal'
        ]);

    Route::get('editarConfTransmittal/{id}', [
        'uses' =>'Cdocumentario\DocumentarioController@editarConfiguracionTransmittalPropuesta',
        'as'=>'editarConfTransmittal'
        ]);
        
    Route::post('uploadLogo',[
        'uses' => 'Cdocumentario\DocumentarioController@UploadLogoEmpresa',
        'as' => 'uploadLogo'                
        ]);   

    Route::post('uploadLogoTRProy',[
        'uses' => 'Cdocumentario\DocumentarioController@UploadLogoEmpresaTRProy',
        'as' => 'uploadLogoTRProy'                
        ]);   

    Route::post('grabarConfTransmittal',[
        'uses' => 'Cdocumentario\DocumentarioController@grabarConfTransmittalPropuesta',
        'as' => 'grabarConfTransmittal'
        ]);
    Route::get('genTransmittal', [
        'uses' => 'Cdocumentario\DocumentarioController@genTransmittalPropuesta',
        'as' => 'genTransmittal'
        ]);

    Route::get('editarGenTransmittal/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarGenTransmittalPropuesta',
        'as' => 'editarGenTransmittal'
        ]);    

    Route::post('grabarGenTransmittal',[
        'uses' => 'Cdocumentario\DocumentarioController@grabarGenTransmittalPropuesta',
        'as' => 'grabarGenTransmittal'
        ]);
    Route::get('modalContactoTransmittalPropuesta/{id}',[
        'uses' =>'Cdocumentario\DocumentarioController@modalContactoTransmittalPropuesta',
        'as' => 'modalContactoTransmittalPropuesta'
    ]);        
    Route::post('addContactoTransmittalPropuesta',[
        'uses' =>'Cdocumentario\DocumentarioController@addContactoTransmittalPropuesta',
        'as' => 'addContactoTransmittalPropuesta'
    ]);        
    Route::get('delContactoTransmittalPropuesta/{id}',[
        'uses' =>'Cdocumentario\DocumentarioController@delContactoTransmittalPropuesta',
        'as' => 'delContactoTransmittalPropuesta'
    ]);

    Route::post('addContactoTransmittalPropuestaUM',[
        'uses' =>'Cdocumentario\DocumentarioController@addContactoTransmittalPropuestaUM',
        'as' => 'addContactoTransmittalPropuestaUM'
    ]);   


    

    /* Fin de Revisión de Propuestas */

    /* *************************************************************
    * Fin de Propuestas 
    *****************************************************************/
    /* 
    * Clientes  y proveedores
    */
    Route::get('listadoClientes', [
        'uses' => 'Cliente\ClienteController@listadoClientes',
        'as' =>'listadoClientes']
    );

    Route::get('listarClientes', [
        'uses' => 'Cliente\ClienteController@listarClientes',
        'as' =>'listarClientes']
    );

    Route::get('registroClientes',[
        'uses' =>'Cliente\ClienteController@registroClientes',
        'as' =>'registroClientes'
        ]
    );

    Route::get('editarCliente/{id}', [
        'uses' =>'Cliente\ClienteController@editarCliente',
        'as' =>'editarCliente'
        ]
    );

    Route::post("grabarlogo","Cliente\ClienteController@grabarlogo")->name('grabarlogo');

    Route::post('egregarlogounidadminera', [
        'uses' =>'Cliente\ClienteController@grabarLogoCliente',
        'as' =>'grabarLogoCliente'
        ]
    );


    Route::post('grabarCliente', [
        'uses' =>'Cliente\ClienteController@grabarCliente',
        'as' =>'grabarCliente'
        ]
    );

    Route::get('buscarClientes',[
            'uses' => 'Cliente\ClienteController@buscar',
            'as' => 'buscarClientes'
        ]

    );

    Route::get('listDpto/{idPais}',[
        'uses'=>'Cliente\ClienteController@listDpto',
        'as'=>'listDpto'
        ]
        );

    Route::get('listProv/{idPais}/{id}',[
            'uses' => 'Cliente\ClienteController@listProv',
            'as' => 'listProv'
        ]
        );

    Route::get('listDis/{idPais}/{id}',[
            'uses' => 'Cliente\ClienteController@listDis',
            'as' => 'listDis'
        ]
        );

    Route::get('buscarProveedor',[
            'uses' => 'Cliente\ClienteController@listarProveedor',
            'as' => 'buscarProveedor'
        ]

    );    

    Route::get('listarCliente',[
            'uses' => 'Cliente\ClienteController@listarCliente',
            'as' => 'listarCliente'
        ]

    ); 

    Route::post('addUmineraCliente',[
        'uses' =>'Cliente\ClienteController@addUmineraCliente',
        'as' =>'addUmineraCliente'
        ]
        );
    Route::get('verUmineraCliente/{id}',[
        'uses' =>'Cliente\ClienteController@verUmineraCliente',
        'as' =>'verUmineraCliente'
        ]
        );
    Route::get('editUmineraCliente/{id}',[
        'uses' =>'Cliente\ClienteController@editUmineraCliente',
        'as' =>'editUmineraCliente'
        ]
        );     

    Route::get('eliminarUmineraCliente/{id}',
        [
        'uses' => 'Cliente\ClienteController@eliminarUmineraCliente',
        'as' => 'eliminarUmineraCliente'
        ]
        );   

    Route::get('getRespuestaEliminarUminera/{id}',
        [
        'uses' => 'Cliente\ClienteController@getRespuestaEliminarUminera',
        'as' => 'getRespuestaEliminarUminera'
        ]
        ); 

    Route::get('eliminarUmineraContacto/{id}',
        [
        'uses' => 'Cliente\ClienteController@eliminarUmineraContacto',
        'as' => 'eliminarUmineraContacto'
        ]
        );   

    Route::get('editUmineraContacto/{id}',[
        'uses' =>'Cliente\ClienteController@editUmineraContacto',
        'as' =>'editUmineraContacto'
        ]
        );  

    Route::get('RegistroUmineraCliente/{id}',
        [
        'uses' => 'Cliente\ClienteController@contactoUmineraCliente',
        'as' => 'contactoUmineraCliente'
        ]
        ); 

    Route::get('dataClientes/{id}',[
        'uses' =>'Cliente\ClienteController@dataClientes',
        'as' =>'dataClientes'
        ]
        );        

    Route::get('printClientes/{id}',[
        'uses' =>'Cliente\ClienteController@printClientes',
        'as' =>'printClientes'
        ]
        );   


    /*  Fin de Clientes y proveedores*/

    /*  Empleados*/
    Route::get('buscarEmpleadosRDP',[
            'uses' => 'Empleado\EmpleadoController@listarEmpleadosRDP',
            'as' => 'buscarEmpleados'
        ]

    ); 

    Route::get('personal/{valor}',[
            'uses' => 'Empleado\PersonalController@personal',
            'as' => 'personal'
        ]

    );

    Route::post('grabarpersonal',[
            'uses' => 'Empleado\PersonalController@grabarpersonal',
            'as' => 'grabarpersonal'
        ]

    );


    // Route::get("personal","Empleado\PersonalController@personal");
    // Route::post("grabarpersonal","Empleado\PersonalController@grabarpersonal");


    /*  FIn de empleados*/

    /* Listar Contactos de Unminera*/

    

    Route::post('AddContactosCliente',
        [
        'uses' => 'Cliente\ClienteController@AddContactosCliente',
        'as' => 'AddContactosCliente'
        ]
        );


    Route::get('verUmineraContacto/{id}',
        [
        'uses' => 'Cliente\ClienteController@verUmineraContacto',
        'as' => 'verUmineraContacto'
        ]
        );
        
    Route::get('buscarContactos/{id}',
        [
        'uses' => 'Cliente\ClienteController@buscarContactos',
        'as' => 'buscarContactos'
        ]
        );


    Route::get('getContacto/{id}',
        [
        'uses' =>'Cliente\ClienteController@getContacto',
        'as' => 'getContacto'
        ]
        );

    Route::get('editarContactoUminera/{id}',
        [
        'uses' => 'Cliente\ClienteController@editarContactoUminera',
        'as' => 'editarContactoUminera'
        ]
        );

    Route::get('editUmineraCliente/{id}',[
        'uses' =>'Cliente\ClienteController@editUmineraCliente',
        'as' =>'editUmineraCliente'
        ]
        ); 
    /* */

    /* proyecto */
    Route::get('listarProyectos',[
        'uses' => 'Proyecto\ProyectoController@listarGrid',
        'as' => 'listarProyectos' ]
        );

     Route::get('listaPryTodos',[
        'uses' => 'Proyecto\ProyectoController@listaPryTodos',
        'as' => 'listaPryTodos' ]
        );


     Route::get('listaGrid',[
        'uses' => 'Proyecto\ProyectoController@listaGrid',
        'as' => 'listaGrid' ]
        );

     Route::get('listaProyectos',[
        'uses' => 'Proyecto\ProyectoController@listar',
        'as' => 'listaProyectos' ]
        );
    /** lardin nueva routa table */
    Route::get('resultadosTablaProyectos/{estado}',[
        'uses' => 'Proyecto\ProyectoController@resultadosTablaProyectos',
        'as' => 'resultadosTablaProyectos' ]
        );              


//Informacion Proyecto
     Route::get('listarInformacionProyecto',[
        'uses' =>'Proyecto\ProyectoController@listarInformacionProyecto',
        'as' =>'listarInformacionProyecto'
        ]);

    
    Route::get('listadoInformacionProyecto', [
        'uses' => 'Proyecto\ProyectoController@listadoInformacionProyecto',
        'as' =>'listadoInformacionProyecto']
        );


    Route::get('viewInformacionProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@viewInformacionProyecto',
        'as' => 'viewInformacionProyecto'
        ]);

    Route::get('viewTransmital/{idTransmital}',[
        'uses' => 'Proyecto\ProyectoController@viewTransmital',
        'as' => 'viewTransmital'
        ]);

    Route::get('viewListaEntregable/{idEntregable}',[
        'uses' => 'Proyecto\ProyectoController@viewListaEntregable',
        'as' => 'viewListaEntregable'
        ]);

    Route::get('viewEDT/{idEdt}',[
        'uses' => 'Proyecto\ProyectoController@viewEDT',
        'as' => 'viewEDT'
        ]);

    Route::get('viewHojaResumen/{idHRes}',[
        'uses' => 'Proyecto\ProyectoController@viewHojaResumen',
        'as' => 'viewHojaResumen'
        ]);




//Fin Informacion Proyecto

    /* PrePropuesta Aprobada*/
    Route::get('aprobacionPropuesta', [
        'uses' => 'Proyecto\ProyectoController@aprobacion',
        'as' => 'aprobacionPropuesta']);

    Route::get('editarAprobacionPropuesta/{idPropuesta}',[
        'uses' => 'Proyecto\ProyectoController@editarAprobacion',
        'as' => 'editarAprobacionPropuesta'
        ]);

    Route::post('grabarAprobacionPropuesta',[
        'uses' => 'Proyecto\ProyectoController@grabarAprobacion',
        'as' => 'grabarAprobacionPropuesta'
        ]);

    /* Fin de Prepropuesta Aprobada*/



    /* Inicio Preproyecto */
    Route::get('preproyecto', [
        'uses' => 'Proyecto\ProyectoController@preproyecto',
        'as' => 'preproyecto']);

    Route::get('disciplinaProyecto', [
        'uses' => 'Proyecto\ProyectoController@disciplinaProyecto',
        'as' => 'disciplinaProyecto']);

    Route::post('grabardisciplinaProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabardisciplinaProyecto',
        'as' => 'grabardisciplinaProyecto'
        ]);   

    Route::get('editarDisciplinaProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@editarDisciplinaProyecto',
        'as' => 'editarDisciplinaProyecto'
        ]);

    Route::post('agregarDisciplinasProyecto',[
        'uses' => 'Proyecto\ProyectoController@agregarDisciplinasProyecto',
        'as' => 'agregarDisciplinasProyecto'
        ]);  

    Route::post('borrarDisciplinasProyecto',[
        'uses' => 'Proyecto\ProyectoController@borrarDisciplina',
        'as' =>'borrarDisciplinasProyecto'
    ]);    

     Route::get('verDisciplinasProyecto',[
        'uses' => 'Proyecto\ProyectoController@verDisciplinas',
        'as' => 'verDisciplinasProyecto'
        ]
        ); 

    Route::get('editarPreproyecto/{idPropuesta}',[
        'uses' => 'Proyecto\ProyectoController@editarPreproyecto',
        'as' => 'editarPreproyecto'
        ]);

    Route::post('grabardisciplinaProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabardisciplinaProyecto',
        'as' => 'grabardisciplinaProyecto'
        ]);

    Route::post('grabarPreproyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarPreproyecto',
        'as' => 'grabarAprobacionPreproyecto'
        ]);    

    Route::get('listTipoPry/{id}',[
        'uses'=>'Proyecto\ProyectoController@listTipoPry',
        'as'=>'listTipoPry'
        ]
    );

    /* Fin de Preproyecto*/

    /* Inicio de Estructura de Proyecto*/
    Route::get('ActividadesProyecto', [
        'uses' => 'Proyecto\ProyectoActividadesController@ActividadesProyecto',
        'as' => 'estructuraProyecto']);

    Route::get('editarActividadesProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoActividadesController@editarActividadesProyecto',
        'as' => 'editarActividadesProyecto'
        ]);

    Route::post('grabarActividadesProyecto',[
        'uses' => 'Proyecto\ProyectoActividadesController@CreoActividadProyecto',
        'as' => 'grabarActividadesProyecto'
        ]);
    Route::post('ItemActividadesProyecto',[
        'uses' => 'Proyecto\ProyectoActividadesController@ModificoActividadProyecto',
        'as' => 'ItemActividadesProyecto'
        ]);

    Route::get('estadotproyectoactividad/{id}',  [
        'uses' =>'Proyecto\ProyectoActividadesController@estadotproyectoactividad',
        'as' =>'estadotproyectoactividad'
        ]);

    /* Fin de Estructura de Proyecto */

    /* Agregar Actividades Proyecto*/
    Route::get('listarActividadesPy/{idPropuesta}',[
        'uses' => 'Proyecto\ProyectoController@listarActividades',
        'as' => 'listarActividadesPy'
        ]);
    Route::post('addActividadPy',[
        'uses' =>'Proyecto\ProyectoController@addActividad',
        'as' => 'addActividadPy'
        ]);

    Route::get('delActividadPy/{idActividad}',[
        'uses' => 'Proyecto\ProyectoController@delActividad',
        'as' => 'delActividadPy'
        ]);

    /* Fin de agregar Actividades Proyecto*/

    

    /* Agregar Estructura Proyecto*/
    Route::get('addEstructuraPy',[
        'uses' =>'Proyecto\ProyectoController@addEstructura',
        'as' => 'addEstructuraPy'
        ]);

    Route::get('delEstructuraPy/{idEstructura}',[
        'uses' => 'Proyecto\ProyectoController@delEstructura',
        'as' => 'delEstructuraPy'
        ]);

    /* Fin de agregar Estructura Proyecto*/    

    /* Especificacion de Proyecto */
    Route::get('especificacionProyecto', [
        'uses' => 'Proyecto\ProyectoController@especificacion',
        'as' => 'especificacionProyecto']);
    Route::get('editarEspecificacionProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@editarEspecificacion',
        'as' => 'editarEspecificacionProyecto'
        ]);

    Route::get('editHorasSeniorProyecto',[
        'uses' => 'Proyecto\ProyectoController@editarHorasSenior',
        'as' => 'editHorasSeniorProyecto'
        ]
        );
    Route::post('saveHorasSeniorProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarHorasSenior',
        'as' => 'saveHorasSeniorProyecto'
        ]
        );    
    Route::get('editHorasDisciplinaProyecto',[
        'uses' => 'Proyecto\ProyectoController@editarHorasDisciplina',
        'as' => 'editHorasDisciplinaProyecto'
        ]
        );
    Route::post('saveHorasDisciplinaProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarHorasDisciplina',
        'as' => 'saveHorasDisciplinaProyecto'
        ]
        );  
    Route::get('editHorasApoyoProyecto',[
        'uses' => 'Proyecto\ProyectoController@editarHorasApoyo',
        'as' => 'editHorasApoyoProyecto'
        ]
        );
    Route::post('saveHorasApoyoProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarHorasApoyo',
        'as' => 'saveHorasApoyoProyecto'
        ]
        );    
    Route::get('verHorasDetalleActividadProyecto/{id}',[
        'uses' => 'Proyecto\ProyectoController@viewDetalleActividad',
        'as' => 'verHorasDetalleActividadProyecto'
        ]
        );           

    Route::post('grabarEspecificacionProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarEspecificacion',
        'as' => 'grabarEspecificacionProyecto'
        ]); 


    /* Inicio Registro de Contacto */

    Route::get('editarContactosUmineraProyecto/{id}',[
        'uses' => 'Proyecto\ProyectoController@editarContactosUmineraProyecto',
        'as' => 'editarContactosUmineraProyecto'
        ]);

    /* Fin Registro de Contacto*/

    //Gastos Proyecto

    Route::get('listaGastos',[
        'uses' =>'Gastos\GastosController@listaGastos',
        'as' =>'listaGastos'
    ]);

    Route::get('gastoProyecto',[
        'uses' =>'Gastos\GastosController@gastoProyecto',
        'as' =>'gastoProyecto'
        ]); 

    Route::get('btnNuevoGastosProyecto',[
        'uses' =>'Gastos\GastosController@btnNuevoGastosProyecto',
        'as' =>'btnNuevoGastosProyecto'
        ]
        );  
    Route::get('viewProy/{id}',[
        'uses' =>'Gastos\GastosController@viewProy',
        'as' =>'viewProy'
    ]);

    Route::get('viewPropuesta/{id}',[
        'uses' =>'Gastos\GastosController@viewPropuesta',
        'as' =>'viewPropuesta'
    ]);

     Route::get('viewProveedor/{id}',[
        'uses' =>'Gastos\GastosController@viewProveedor',
        'as' =>'viewProveedor'
    ]);

        
    Route::get('editargastoProyecto/{id}',[
        'uses' => 'Gastos\GastosController@editarGastoProyecto',
        'as' => 'editargastoProyecto'
        ]);   
        
    Route::post('saveGastoProy',[
        'uses' => 'Gastos\GastosController@saveGastoProy',
        'as' => 'saveGastoProy'
        ]);

    Route::post('cancelGastoProy',[
        'uses' => 'Gastos\GastosController@cancelGastoProy',
        'as' => 'cancelGastoProy'
        ]);

    Route::post('enviarGastoProy',[
        'uses' => 'Gastos\GastosController@enviarGastoProy',
        'as' => 'enviarGastoProy'
        ]);

    Route::get('reabrirRendicionGasto/{id}',[
        'uses' => 'Gastos\GastosController@reabrirRendicionGasto',
        'as' => 'reabrirRendicionGasto'
        ]);

    Route::get('deleteRendicionGasto/{id}',[
        'uses' => 'Gastos\GastosController@deleteRendicionGasto',
        'as' => 'deleteRendicionGasto'
        ]);

    Route::post('agregarGastosProy',[
        'uses' => 'Gastos\GastosController@agregarGastosProy',
        'as' => 'agregarGastosProy'
        ]
        );
    Route::get('delGastoProy/{id}',[
        'uses' =>'Gastos\GastosController@delGastoProy',
        'as' => 'delGastoProy'
        ]);  

    Route::get('printGastoProy',[
    'uses' => 'Gastos\GastosController@printGastoProy',
    'as' => 'printGastoProy'
    ]);



    Route::get('printGastoProyecto',function(){
        $pdf=PDF::loadview('gastos/printRendicionGasto');
        return $pdf->download('archivo.pdf');
    });

     Route::get('dataGastoDetProyecto/{id}',[
        'uses' => 'Gastos\GastosController@dataGastoDetProyecto',
        'as' => 'dataGastoDetProyecto'
    ]);

     Route::get('printPDF/{id}','Gastos\GastosController@printPDF');


     Route::get('viewProyRendicion/{id}',[
        'uses' =>'Gastos\GastosController@viewProyRendicion',
        'as' =>'viewProyRendicion'
    ]);

      Route::get('viewAprobacionRendicion',[
        'uses' =>'Gastos\GastosController@viewAprobacionRendicion',
        'as' =>'viewAprobacionRendicion'
    ]);

      Route::get('calcularTotalDolares',[
        'uses' => 'Gastos\GastosController@calcularTotalDolares',
        'as' => 'calcularTotalDolares'
        ]
        );

      Route::get('calcularTotalDolaresRendicion',[
        'uses' => 'Gastos\GastosController@calcularTotalDolaresRendicion',
        'as' => 'calcularTotalDolaresRendicion'
        ]
        );

      Route::get('calcularTotalSolesRendicion',[
        'uses' => 'Gastos\GastosController@calcularTotalSolesRendicion',
        'as' => 'calcularTotalSolesRendicion'
        ]
        );

      Route::get('deleteDetGasto/{id}',[
        'uses' =>'Gastos\GastosController@deleteDetGasto',
        'as' => 'deleteDetGasto'
        ]);  
    //Gastos Adm Proyecto

    Route::get('gastoAdmXProyecto',[
        'uses' =>'Gastos\GastosController@gastoAdmXProyecto',
        'as' =>'gastoAdmXProyecto'
        ]);

    Route::get('btnNuevoGastoAdmProy',[
        'uses' =>'Gastos\GastosController@btnNuevoGastoAdmProy',
        'as' =>'btnNuevoGastoAdmProy'
        ]
        );  

    Route::get('editargastoAdmXProyecto/{id}',[
        'uses' => 'Gastos\GastosController@editargastoAdmXProyecto',
        'as' => 'editargastoAdmXProyecto'
        ]); 
    
    Route::post('saveGastoAdmProy',[
        'uses' => 'Gastos\GastosController@saveGastoAdmProy',
        'as' => 'saveGastoAdmProy'
        ]);

    Route::post('cancelGastoAdmProyecto',[
        'uses' => 'Gastos\GastosController@cancelGastoAdmProyecto',
        'as' => 'cancelGastoAdmProyecto'
        ]);

    Route::post('enviarGastoAdmProyecto',[
        'uses' => 'Gastos\GastosController@enviarGastoAdmProyecto',
        'as' => 'enviarGastoAdmProyecto'
        ]);

    //Fin Gastos Adm Proyecto


    //Varios Proyecto

    Route::get('gastoVariosProyecto',[
        'uses' =>'Gastos\GastosController@gastoVariosProyecto',
        'as' =>'gastoVariosProyecto'
        ]);

    Route::get('btnNuevoGastoVariosProyecto',[
        'uses' =>'Gastos\GastosController@btnNuevoGastoVariosProyecto',
        'as' =>'btnNuevoGastoVariosProyecto'
        ]
        );  

    Route::get('editargastoAdmXProyecto/{id}',[
        'uses' => 'Gastos\GastosController@editargastoAdmXProyecto',
        'as' => 'editargastoAdmXProyecto'
        ]); 
    
    Route::post('saveGastoAdmProy',[
        'uses' => 'Gastos\GastosController@saveGastoAdmProy',
        'as' => 'saveGastoAdmProy'
        ]);

    Route::post('cancelVariosProyecto',[
        'uses' => 'Gastos\GastosController@cancelVariosProyecto',
        'as' => 'cancelVariosProyecto'
        ]);

    Route::post('enviarVariosProyecto',[
        'uses' => 'Gastos\GastosController@enviarVariosProyecto',
        'as' => 'enviarVariosProyecto'
        ]);

    //Fin de Varios Proyecto



    Route::get('addGastosProyecto',[
        'uses' => 'Proyecto\ProyectoController@addGastos',
        'as' => 'addGastosProyecto'
        ]
        );
    Route::get('delGastosProyecto',[
        'uses' => 'Proyecto\ProyectoController@delGastos',
        'as' => 'delGastosProyecto'
        ]
        );    
    Route::get('addGastosLabProyecto',[
        'uses' => 'Proyecto\ProyectoController@addGastosLab',
        'as' => 'addGastosLabProyecto'
        ]
        );
    Route::get('delGastosLabProyecto',[
        'uses' => 'Proyecto\ProyectoController@delGastosLab',
        'as' => 'delGastosLabProyecto'
        ]
        );
    /* Fin de Especificacion de Proyecto */

    
    /* Inicio  Registro de HR*/
    Route::get('registroHR/{valor}', [
        'uses' => 'Proyecto\ProyectoController@registroHR',
        'as' => 'registroHR']);
    
    Route::get('editarHR/{valor}/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@editarHR',
        'as' => 'editarHR'
        ]);

    Route::post('grabarHR',[
        'uses' => 'Proyecto\ProyectoController@grabarHR',
        'as' => 'grabarHR'
        ]);

    Route::get('printHRProy/{almacenar}/{id}',[
        'uses' =>'Proyecto\ProyectoController@printHRProy',
        'as' =>'printHRProy']
        ); 

     Route::post('enviarCorreoIni',[
        'uses' =>'Proyecto\ProyectoController@enviarCorreoIni',
        'as' =>'enviarCorreoIni']
        );

    Route::get('printHRandSaveProy/{almacenar}/{id}',[
        'uses' =>'Proyecto\ProyectoController@printHRandSaveProy',
        'as' =>'printHRandSaveProy'
        ]
    ); 
    /* Fin de Registro de HR*/



    /* Inicio Cronograma de Proyecto*/
    Route::get('cronogramaProyecto', [
        'uses' => 'Proyecto\ProyectoController@cronogramaProyecto',
        'as' => 'cronogramaProyecto']);

    Route::get('editarCronogramaProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@editarCronogramaProyecto',
        'as' => 'editarCronogramaProyecto'
        ]);

    Route::post('grabarCronogramaProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarCronogramaProyecto',
        'as' => 'grabarCronogramaProyecto'
        ]);

    /* Fin  de Cronograma de Proyecto*/

    /* Inicio de Entregables de Proyecto*/
    Route::get('entregablesProyecto', [
        'uses' => 'Proyecto\ProyectoController@entregablesProyecto',
        'as' => 'entregablesProyecto']);

    Route::get('editarEntregablesProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@editarEntregablesProyecto',
        'as' => 'editarEntregablesProyecto'
        ]);

    Route::post('grabarEntregablesProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarEntregablesProyecto',
        'as' => 'grabarEntregablesProyecto'
        ]); 
    
    Route::post('grabarEntregablesSeleccionados',[
        'uses' => 'Proyecto\ProyectoController@grabarEntregablesSeleccionados',
        'as' => 'grabarEntregablesSeleccionados'
        ]); 

    Route::get('verActividadesProyecto',[
        'uses' => 'Proyecto\ProyectoController@verActividadesProyecto',
        'as' => 'verActividadesProyecto'
        ]);  

    Route::get('addEntregable',[
        'uses' => 'Proyecto\ProyectoController@addEntregable',
        'as' => 'addEntregable'
        ]);  

    Route::post('asignarActividadesEntregables',[
        'uses' => 'Proyecto\ProyectoController@asignarActividadesEntregables',
        'as' => 'asignarActividadesEntregables'
        ]);

    Route::get('activarEntregable/{ent}/{act}',[
        'uses' => 'Proyecto\ProyectoController@activarEntregable',
        'as' => 'activarEntregable'
        ]); 

    Route::get('verTodosEntregables/{cproy}',[
        'uses' => 'Proyecto\ProyectoController@verTodosEntregables',
        'as' => 'verTodosEntregables'
        ]);

    Route::get('verTodosEntregablesPorEDT/{edt}',[
        'uses' => 'Proyecto\ProyectoController@verTodosEntregablesPorEDT',
        'as' => 'verTodosEntregablesPorEDT'
        ]);

    Route::get('seleccionarcolumnas',[
        'uses' => 'Proyecto\ProyectoController@seleccionarcolumnas',
        'as' => 'seleccionarcolumnas'
        ]);
    
    Route::get('obtenerCabeceraFechasXEDT/{edt}',[
        'uses' => 'Proyecto\ProyectoController@obtenerCabeceraFechasXEDT',
        'as' => 'obtenerCabeceraFechasXEDT'
        ]);

    Route::get('obtenerCabeceraFechasXProyecto/{idProy}',[
        'uses' => 'Proyecto\ProyectoController@obtenerCabeceraFechasXProyecto',
        'as' => 'obtenerCabeceraFechasXProyecto'
        ]);

    Route::get('getFechaEntAnteriores/{idEnt}',[
        'uses' => 'Proyecto\ProyectoController@getFechaEntAnteriores',
        'as' => 'getFechaEntAnteriores'
        ]);

    Route::get('getFechasNoAgregadas/{idEnt}',[
        'uses' => 'Proyecto\ProyectoController@getFechasNoAgregadas',
        'as' => 'getFechasNoAgregadas'
        ]);

    Route::get('listarTodasLasFechas',[
        'uses' => 'Proyecto\ProyectoController@listarTodasLasFechas',
        'as' => 'listarTodasLasFechas'
        ]);
    Route::get('getFechaEntAnterioresTodos/{idEdt}',[
        'uses' => 'Proyecto\ProyectoController@getFechaEntAnterioresTodos',
        'as' => 'getFechaEntAnterioresTodos'
        ]);

    Route::get('obtenerAnexos/{idEnt}/{edt}/{vista}',[
        'uses' => 'Proyecto\ProyectoController@obtenerAnexos',
        'as' => 'obtenerAnexos'
        ]);

    Route::get('getFasesProy',[
        'uses' => 'Proyecto\ProyectoController@getFasesProy',
        'as' => 'getFasesProy'
        ]);

    Route::get('actualizarEntregable',[
        'uses' => 'Proyecto\ProyectoController@actualizarEntregable',
        'as' => 'actualizarEntregable'
    ]);

    Route::post('saveEntregable',[
        'uses' => 'Proyecto\ProyectoController@saveEntregable',
        'as' => 'saveEntregable'
        ]);  

    Route::post('savecheckListEnt',[
        'uses' => 'Proyecto\ProyectoController@savecheckListEnt',
        'as' => 'savecheckListEnt'
        ]);

     Route::get('viewEntregables/{edt}',[
        'uses' => 'Proyecto\ProyectoController@viewEntregables',
        'as' => 'viewEntregables'
        ]);
    Route::post('grabarFechasEntregablePry',[
        'uses' => 'Proyecto\ProyectoController@grabarFechasEntregablePry',
        'as' => 'grabarFechasEntregablePry'
        ]);

    Route::post('grabarFechasEntregablePrySelec',[
        'uses' => 'Proyecto\ProyectoController@grabarFechasEntregablePrySelec',
        'as' => 'grabarFechasEntregablePrySelec'
        ]);

    Route::get('grabarEntregablePrySelecRuteo',[
        'uses' => 'Edt\EdtController@grabarEntregablePrySelecRuteo',
        'as' => 'grabarEntregablePrySelecRuteo'
        ]);

    Route::get('verentregableruteo',[
        'uses' => 'Edt\EdtController@verentregableruteo',
        'as' => 'verentregableruteo'
        ]);
    
    Route::get('verentregableruteocomentario',[
        'uses' => 'Edt\EdtController@verentregableruteocomentario',
        'as' => 'verentregableruteocomentario'
        ]);

    Route::get('actualizarruteo',[
        'uses' => 'Edt\EdtController@actualizarruteo',
        'as' => 'actualizarruteo'
        ]);

    Route::get('ruteoedtproyecto',[
        'uses' => 'Edt\EdtController@ruteoedtproyecto',
        'as' => 'ruteoedtproyecto'
        ]);
    
    Route::get('colaentregable',[
        'uses' => 'Edt\EdtController@colaentregable',
        'as' => 'colaentregable'
        ]);

    Route::get('grabarfechaentregablecalendario',[
        'uses' => 'Edt\EdtController@grabarfechaentregablecalendario',
        'as' => 'grabarfechaentregablecalendario'
        ]);

    Route::get('viewFechasEntregablesPry/{idEnt}',[
        'uses' => 'Proyecto\ProyectoController@viewFechasEntregablesPry',
        'as' => 'viewFechasEntregablesPry'
        ]);

    Route::get('getEntregableProyecto/{idEnt}',[
        'uses' => 'Proyecto\ProyectoController@getEntregableProyecto',
        'as' => 'getEntregableProyecto'
        ]);


    Route::post('grabarEDTFila',[
        'uses' => 'Proyecto\ProyectoController@grabarEDTFila',
        'as' => 'grabarEDTFila'
        ]); 

    Route::get('actualizarEDT',[
        'uses' => 'Proyecto\ProyectoController@actualizarEDT',
        'as' => 'actualizarEDT'
    ]); 

    Route::get('obtenerDisciplinasEntregables/{tipoEnt}',[
        'uses' => 'Proyecto\ProyectoController@obtenerDisciplinasEntregables',
        'as' => 'obtenerDisciplinasEntregables'
        ]);

    Route::get('eliminarEntregablePry/{idEntPry}',[
        'uses' => 'Proyecto\ProyectoController@eliminarEntregablePry',
        'as' => 'eliminarEntregablePry'
        ]);

    Route::get('eliminarEntregablePrySeleccionados',[
        'uses' => 'Proyecto\ProyectoController@eliminarEntregablePrySeleccionados',
        'as' => 'eliminarEntregablePrySeleccionados'
        ]);

    Route::post('grabarRevisionEntregable',[
        'uses' => 'Proyecto\ProyectoController@grabarRevisionEntregable',
        'as' => 'grabarRevisionEntregable'
        ]);   

    Route::post('grabarInfoAdicionalEntregable',[
        'uses' => 'Proyecto\ProyectoController@grabarInfoAdicionalEntregable',
        'as' => 'grabarInfoAdicionalEntregable'
        ]);

    Route::get('codificarEntregablesSeleccionados',[
        'uses' => 'Proyecto\ProyectoController@codificarEntregablesSeleccionados',
        'as' => 'codificarEntregablesSeleccionados'
        ]);

    Route::post('agregarEntregablesGrupos',[
        'uses' => 'Proyecto\ProyectoController@agregarEntregablesGrupos',
        'as' => 'agregarEntregablesGrupos'
        ]); 
    
    Route::post('grabarEntregablesNuevo',[
        'uses' => 'Proyecto\ProyectoController@grabarEntregablesNuevo',
        'as' => 'grabarEntregablesNuevo'
        ]); 

    Route::get('listarComboEntregablesXTipo/{tipo}', [
        'uses' => 'Proyecto\ProyectoController@listarComboEntregablesXTipo',
        'as' => 'listarComboEntregablesXTipo']);

    Route::get('codificarEntregablesTodos/{cproyecto}', [
        'uses' => 'Proyecto\ProyectoController@codificarEntregablesTodos',
        'as' => 'codificarEntregablesTodos']);

    Route::get("imprimirEntregables/{tipo}/{cproyecto}", "Proyecto\ProyectoController@imprimirEntregables"); 

    Route::get("verTablaEntregablesTodos/{cproyecto}/{estado}", "Proyecto\ProyectoController@verTablaEntregablesTodos"); 


    /* Fin de Entregables de Proyecto*/

    /* Inicio de EDT de Proyecto*/
    Route::get('edtProyecto', [
        'uses' => 'Proyecto\ProyectoController@edtProyecto',
        'as' => 'edtProyecto']);

    Route::get('editarEdtProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@editarEdtProyecto',
        'as' => 'editarEdtProyecto'
        ]);
    Route::get('verDetalleEDT/{idProyecto}/{revision}',[
        'uses' => 'Proyecto\ProyectoController@verDetalleEDT',
        'as' => 'verDetalleEDT'
        ]);

    Route::post('grabarEdtProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarEdtProyecto',
        'as' => 'grabarEdtProyecto'
        ]);    

    Route::get('addEDTPy',[
        'uses' =>'Proyecto\ProyectoController@addEDT',
        'as' => 'addEDTPy'
        ]);
    Route::get('delEDTPy/{idEdt}',[
        'uses' =>'Proyecto\ProyectoController@delEDT',
        'as' => 'delEDTPy'
        ]);

    Route::get('updateestadoedt/{idEdt}',[
        'uses' =>'Proyecto\ProyectoController@updateestadoedt',
        'as' => 'updateestadoedt'
        ]);

    Route::get('delete_edt/{idEdt}',[
        'uses' =>'Proyecto\ProyectoController@delete_edt',
        'as' => 'delete_edt'
        ]);

    Route::get('Selentregable/{disciplina}/{tipoentregable}',[
            'uses' => 'Proyecto\ProyectoController@Selentregable',
            'as' => 'Selentregable'
        ]
        );  

    Route::get('SelentregableHijo/{idEntregable}/{cproy}/{edt}',[
            'uses' => 'Proyecto\ProyectoController@SelentregableHijo',
            'as' => 'SelentregableHijo'
        ]
    ); 

    Route::get('obtenerEntregAgregadosXEDT/{val}/{desc}/{edt}',[
            'uses' => 'Proyecto\ProyectoController@obtenerEntregAgregadosXEDT',
            'as' => 'obtenerEntregAgregadosXEDT'
        ]
    ); 

    Route::get('SearchEntregableHijo/{idEntregable}/{descripcion}/{cproy}/{edt}',[
            'uses' => 'Proyecto\ProyectoController@SearchEntregableHijo',
            'as' => 'SearchEntregableHijo'
        ]
    );  

    Route::get('editEDTProyecto/{edt}',[
            'uses' => 'Proyecto\ProyectoController@editEDTProyecto',
            'as' => 'editEDTProyecto'
        ]
        );    

    Route::get('printEDTProy/{id}',[
        'uses' =>'Proyecto\ProyectoController@printEDTProy',
        'as' =>'printEDTProy'
        ]
        );

    /* Fin de EDT de Proyecto*/ 

    /* Inicio planificacion de Proyecto*/
    Route::get('planificacionProyecto', [
        'uses' => 'Proyecto\ProyectoController@planificacionProyecto',
        'as' => 'planificacionProyecto']);
    Route::get('editarPlanificacionProyecto/{idProyecto}',[
        'uses' => 'Proyecto\ProyectoController@editarPlanificacionProyecto',
        'as' => 'editarPlanificacionProyecto'
        ]);
    Route::get('viewPlanificacionProyecto',[
        'uses' => 'Proyecto\ProyectoController@viewPlanificacionProyecto',
        'as'   => 'viewPlanificacionProyecto'
        ]);

    Route::post('grabarPlanificacionProyecto',[
        'uses' => 'Proyecto\ProyectoController@grabarPlanificacionProyecto',
        'as' => 'grabarPlanificacionProyecto'
        ]);    

    /* Inicio Planificacion de Proyecto Por Areas */
    Route::get('planificacionareaProyecto', [
        'uses' => 'Proyecto\ProyectoController@planificacionareaProyecto',
        'as' => 'planificacionareaProyecto']);



     Route::get('planificacionProy', [
        'uses' => 'Proyecto\ProyectoController@planificacionProy',
        'as' => 'planificacionProy']);

     Route::get('visualizarPlanificacion',[
            'uses' => 'Proyecto\ProyectoController@visualizarPlanificacion',
            'as'   => 'visualizarPlanificacion'
            ]);

    Route::get('getCargabilidadColaborador', [
        'uses' => 'Proyecto\ProyectoController@getCargabilidadColaborador',
        'as' => 'getCargabilidadColaborador']);
    

    Route::post('savePlanificacion',[
        'uses' => 'Proyecto\ProyectoController@savePlanificacion',
        'as' => 'savePlanificacion'
    ]);

    /*Route::get('editarPlanificacionArea/{idProyecto}',[
            'uses' => 'Proyecto\ProyectoController@editarPlanificacionArea',
            'as' => 'editarPlanificacionArea'
            ]);*/

    Route::get('viewPlanificacionArea',[
            'uses' => 'Proyecto\ProyectoController@viewPlanificacionArea',
            'as'   => 'viewPlanificacionArea'
            ]);
    Route::post('grabarPlanificacionArea',[
            'uses' => 'Proyecto\ProyectoController@grabarPlanificacionArea',
            'as' => 'grabarPlanificacionArea'
            ]);  
    
    Route::get('verHorasAcumuladasParticipante',[
            'uses' => 'Proyecto\ProyectoController@verHorasAcumuladasParticipante',
            'as' => 'verHorasAcumuladasParticipante'            
    ]);

    Route::post('verTareaProyecto',[
            'uses' => 'Proyecto\ProyectoController@verTareaProyecto',
            'as' => 'verTareaProyecto'            
    ]);

    Route::post('insertarTareaProyecto',[
            'uses' => 'Proyecto\ProyectoController@insertarTareaProyecto',
            'as' => 'insertarTareaProyecto'            
    ]);    

    Route::post('asignaPlaPersonal',[
            'uses' => 'Proyecto\ProyectoController@asignaPlaPersonal',
            'as' => 'asignaPlaPersonal'            
    ]);    
  
    Route::post('insertarTareaClo',[
            'uses' => 'Proyecto\ProyectoController@insertarTareaClo',
            'as' => 'insertarTareaClo'            
    ]);    

    Route::post('verReporteHorasParticipante',[
            'uses' => 'Proyecto\ProyectoController@verReporteHorasParticipante',
            'as' => 'verReporteHorasParticipante'            
    ]);
    /* Fin de Planificacion de Proyecto Por Areas */


    /* Inicio Consulta Planificacion de Proyecto*/
    /*Route::get('consultaplanificacionProyecto', [
        'uses' => 'Proyecto\ProyectoController@consultaplanificacionProyecto',
        'as' => 'consultaplanificacionProyecto']);

    
    Route::get('viewConsultaProyecto',[
        'uses' => 'Proyecto\ProyectoController@viewConsultaProyecto',
        'as'   => 'viewConsultaProyecto'
        ]);*/

    /* Fin de Consulta de Planificacion*/

    /* Inicio Planificación de Tareas Administrativas */
    
    Route::get('planificacionTareaAdm',[
        'uses'=>'Proyecto\ProyectoController@planificacionTareaAdm',
        'as' =>'planificacionTareaAdm'
    ]);

    Route::get('viewPlanificacionAreaAdm',[
        'uses'=>'Proyecto\ProyectoController@viewPlanificacionAreaAdm',
        'as' =>'viewPlanificacionAreaAdm'
    ]);
    Route::get('verHorasAcumuladasParticipanteAdm',[
        'uses'=>'Proyecto\ProyectoController@verHorasAcumuladasParticipanteAdm',
        'as' =>'verHorasAcumuladasParticipanteAdm'
    ]);
    Route::post('viewSemanaAdm',[
        'uses' =>'Proyecto\ProyectoController@viewSemanaAdm',
        'as' => 'viewSemanaAdm'
    ]);
    Route::post('grabarPlanificacionAreaAadm',[
        'uses' => 'Proyecto\ProyectoController@grabarPlanificacionAreaAadm',
        'as' =>'grabarPlanificacionAreaAadm'
    ]);
    /* Fin de Planificación de Tareas Administrativas */

    /* Inicio Asignacion de Participantes Proyecto */

    Route::get('asignacionParticipantesProyecto', [
        'uses' => 'Proyecto\ProyectoController@asignacionParticipantesProyecto',
        'as' => 'asignacionParticipantesProyecto']);

    Route::get('editarAsignacionParticipantes/{id}', [
        'uses' => 'Proyecto\ProyectoController@editarAsignacionParticipantes',
        'as' => 'editarAsignacionParticipantes']);

    Route::get('verParticipantesProyecto',[
        'uses' => 'Proyecto\ProyectoController@verParticipantes',
        'as' => 'verParticipantesProyecto']); 

     Route::get('verEquipo',[
        'uses' => 'Proyecto\ProyectoController@verEquipo',
        'as' => 'verEquipo']); 

    Route::post('agregarParticipantes', [
        'uses' => 'Proyecto\ProyectoController@agregarParticipantes',
        'as' => 'agregarParticipantes']); 

    Route::post('agregarLiderEquipo', [
        'uses' => 'Proyecto\ProyectoController@agregarLiderEquipo',
        'as' => 'agregarLiderEquipo']); 

    Route::post('borrarParticipante',[
        'uses' => 'Proyecto\ProyectoController@borrarParticipante',
        'as' => 'borrarParticipante']); 

    Route::get('eliminarParticipanteProyecto/{id}',[
        'uses' => 'Proyecto\ProyectoController@eliminarParticipanteProyecto',
        'as' => 'eliminarParticipanteProyecto']); 

     Route::get('verParticipProyec/{id}',[
        'uses' =>'Proyecto\ProyectoController@verParticipProyec',
        'as' =>'verParticipProyec'
        ]
        );

    Route::post('grabarParticipante',[
        'uses' => 'Proyecto\ProyectoController@grabarParticipante',
        'as' => 'grabarParticipante'
        ]
        );

    Route::get('verEquipo',[
        'uses' => 'Proyecto\ProyectoController@verEquipo',
        'as' => 'verEquipo']); 

    Route::post('verCategoria', [
        'uses' => 'Proyecto\ProyectoController@verCategoria',
        'as' => 'verCategoria']);

    Route::get('ordenarParticipanteSeleccionado',[
        'uses' => 'Proyecto\ProyectoController@ordenarParticipanteSeleccionado',
        'as' => 'ordenarParticipanteSeleccionado']); 


   /* Fin Asignacion de Participantes Proyecto */

   /* Inicio  de Habilitacion de Tareas Proyecto */
   Route::get('habilitarTareasProyecto', [
        'uses' => 'Proyecto\ProyectoController@habilitarTareasProyecto',
        'as' => 'habilitarTareasProyecto']);

   Route::get('editarHabilitacionActividades/{id}', [
        'uses' => 'Proyecto\ProyectoController@editarHabilitacionActividades',
        'as' => 'editarHabilitacionActividades']);

   Route::get('viewActividadesParticipante/{id}', [
        'uses' => 'Proyecto\ProyectoController@viewActividadesParticipante',
        'as' => 'viewActividadesParticipante']);

   Route::post('grabarHabilitacion',[
        'uses' => 'Proyecto\ProyectoController@grabarHabilitacion',
        'as' => 'grabarHabilitacion'
        ]
        );

   Route::get('listarProyLider',[
        'uses' => 'Proyecto\ProyectoController@listarProyLider',
        'as' => 'listarProyLider' ]
        );
   /* Fin de Habilitacion de Tareas Proyecto */

    /* Registro de Contactos */
    Route::get('registroContactosProy', function(){
            return view('proyecto/registroContactosProyecto');
        });
    /* fin registro de contactos */

    /* checklist */
    Route::get('checklistProy', [
        'uses' => 'Proyecto\ProyectoController@checklistProy',
        'as' => 'grabarPlanificacionProyecto'
        ]);
    Route::get('editarChecklistProy/{id}',[
        'uses' => 'Proyecto\ProyectoController@editarChecklistProy',
        'as' => 'editarChecklistProy'
        ]);  
    Route::POST('grabarChecklistProy', [
        'uses' => 'Proyecto\ProyectoController@grabarChecklistProy',
        'as' => 'grabarChecklistProy'
        ]);        

              
    /* fin checklist */

    /* Inicio de Planificacion Por area*/
    Route::get('planificacionareaProyecto', [
        'uses' => 'Proyecto\ProyectoController@planificacionareaProyecto',
        'as' => 'planificacionareaProyecto']);

/*
    Route::get('editarPlanificacionArea/{idArea}',[
        'uses' => 'Proyecto\ProyectoController@editarPlanificacionArea',
        'as' => 'editarPlanificacionArea'
        ]);

    Route::post('grabarPlanificacionArea',[
        'uses' => 'Proyecto\ProyectoController@grabarPlanificacionArea',
        'as' => 'grabarPlanificacionArea'
        ]);   */ 

    /* fin de Planificacion por area */

/* Inicio de Consulta Proyecto*/


/* Fin de Consulta Proyecto */

    /*
     * Proyecto - Indicadores
     */

    
    //Seguimiento y control
    Route::get('curvaS', [
        'uses' => 'Proyecto\ProyectoController@curvaS',
        'as' => 'curvaS']);

     Route::get('editarcurvaS/{id}',[
        'uses' => 'Proyecto\ProyectoController@editarcurvaS',
        'as' => 'editarcurvaS'
        ]);  

    Route::get('controlEjecucion', [
        'uses' => 'Proyecto\ProyectoController@controlEjecucion',
        'as' => 'controlEjecucion']);

    Route::get('editarControlEjecucion/{id}',[
        'uses' => 'Proyecto\ProyectoController@editarControlEjecucion',
        'as' => 'editarControlEjecucion'
        ]);  

    Route::POST('grabarControlEjecucion', [
        'uses' => 'Proyecto\ProyectoController@grabarControlEjecucion',
        'as' => 'grabarControlEjecucion'
        ]);

    Route::get('registroDeviaciones', [
        'uses' => 'Proyecto\ProyectoController@registroDeviaciones',
        'as' => 'registroDeviaciones']);

    Route::get('editarregistroDesviacion/{id}',[
        'uses' => 'Proyecto\ProyectoController@editarregistroDesviacion',
        'as' => 'editarregistroDesviacion'
        ]);  

   //Fin Seguimiento y Control


    Route::get('listarleccionesAprendidas', [
        'uses' => 'Proyecto\ProyectoController@listarleccionesAprendidas',
        'as' => 'listarleccionesAprendidas']);

    Route::get('leccionesAprendidas', [
        'uses' => 'Proyecto\ProyectoController@leccionesAprendidas',
        'as' => 'leccionesAprendidas']);


    Route::get('editarLeccionesAprendidas/{id}',[
        'uses' => 'Proyecto\ProyectoController@editarLeccionesAprendidas',
        'as' => 'editarLeccionesAprendidas'
        ]);  
    Route::post('grabarLeccionesAprendidas', [
        'uses' => 'Proyecto\ProyectoController@grabarLeccionesAprendidas',
        'as' => 'grabarLeccionesAprendidas'
        ]);   

    Route::post('addLeccAprenPy',[
        'uses' =>'Proyecto\ProyectoController@addLeccApren',
        'as' => 'addLeccAprenPy'
        ]);

    Route::get('delLecAprendPy/{id}',[
        'uses' =>'Proyecto\ProyectoController@delLecAprend',
        'as' => 'delLecAprendPy'
        ]);

    Route::get('editLecAprend/{id}',
        [
        'uses' => 'Proyecto\ProyectoController@editLecAprend',
        'as' => 'editLecAprend'
        ]);

    Route::get('printLAProy/{id}',[
        'uses' =>'Proyecto\ProyectoController@printLAProy',
        'as' =>'printLAProy'
        ]
        );

    // Route::get('matrizp',[
    //     'uses'=>'Proyecto\ProyectoController@matrizp',
    //     'as' => 'matrizp'
    //     ]
    //     );

        
    Route::get('images/{dni}', [
        'uses' =>'Proyecto\ProyectoController@images',
        'as' => 'images'
    ]);        
            
    Route::get('cambiosCronogramaRooster', [
        'uses' => 'Proyecto\ProyectoController@cambiosCronogramaRooster',
        'as' => 'cambiosCronogramaRooster']);

    Route::get('verCambiosCronogramaRooster/{id}', [
        'uses' => 'Proyecto\ProyectoController@verCambiosCronogramaRooster',
        'as' => 'verCambiosCronogramaRooster']);

    Route::post('visualizarCambiosCronogramaRooster', [
        'uses' => 'Proyecto\ProyectoController@visualizarCambiosCronogramaRooster',
        'as' => 'visualizarCambiosCronogramaRooster']);


    Route::post('grabarCambiosCronogramaRooster', [
        'uses' => 'Proyecto\ProyectoController@grabarCambiosCronogramaRooster',
        'as' => 'grabarCambiosCronogramaRooster']);

    Route::get('horasEjecutadasRooster', [
        'uses' => 'Proyecto\ProyectoController@horasEjecutadasRooster',
        'as' => 'horasEjecutadasRooster']);

    Route::get('verHorasEjecutadasRooster/{id}', [
        'uses' => 'Proyecto\ProyectoController@verHorasEjecutadasRooster',
        'as' => 'verHorasEjecutadasRooster']);

    Route::post('verControlHorasConstruccion',[
        'uses' =>'Proyecto\ProyectoController@verControlHorasConstruccion',
        'as' => 'verControlHorasConstruccion'
    ]);

    
    Route::post('grabarHorasEjecutadasRooster', [
        'uses' => 'Proyecto\ProyectoController@grabarHorasEjecutadasRooster',
        'as' => 'grabarHorasEjecutadasRooster']);

    Route::get('validahorasRooster', [
        'uses' => 'Proyecto\ProyectoController@validahorasRooster',
        'as' => 'validahorasRooster']);

    Route::get('verValidahorasRooster/{id}', [
        'uses' => 'Proyecto\ProyectoController@verValidahorasRooster',
        'as' => 'verValidahorasRooster']);
    Route::post('verValidarHorasConstruccion',[
        'uses' => 'Proyecto\ProyectoController@verValidarHorasConstruccion',
        'as' =>'verValidarHorasConstruccion'
    ]);
    Route::get('grabarValidahorasRooster', [
        'uses' => 'Proyecto\ProyectoController@grabarValidahorasRooster',
        'as' => 'grabarValidahorasRooster']);    

    /*
     * TimeSheet
     */
    Route::get('registroHoras', [
        'uses' => 'TimeSheet\TimeSheetController@registroHoras',
        'as' => 'registroHoras']);

    Route::get('editarRegistroHoras/{id}/{fec}', [
        'uses' => 'TimeSheet\TimeSheetController@editarRegistroHoras',
        'as' => 'editarRegistroHoras']);

    Route::post('grabarRegistroHoras', [
        'uses' => 'TimeSheet\TimeSheetController@grabarRegistroHoras',
        'as' => 'grabarRegistroHoras']);

    Route::get('clonarregistro/{id}',[
        'uses' => 'TimeSheet\TimeSheetController@clonarregistro',
        'as' =>'clonarregistro'
    ]);
    
    Route::post('agregarRegistroTime',[
        'uses' => 'TimeSheet\TimeSheetController@agregarRegistroTime',
        'as' =>'agregarRegistroTime'
    ]);
    Route::post('agregarRegistroTimeAdm',[
        'uses' => 'TimeSheet\TimeSheetController@agregarRegistroTimeAdm',
        'as' =>'agregarRegistroTimeAdm'
    ]);   
      
    Route::post('editarTodosRegistrodeHoras',[
        'uses' => 'TimeSheet\TimeSheetController@editarTodosRegistrodeHoras',
        'as' => 'editarTodosRegistrodeHoras'
    ]);

    Route::post('loadUmineraxCliente',[
        'uses' =>'TimeSheet\TimeSheetController@loadUmineraxCliente',
        'as' => 'loadUmineraxCliente'
    ]);
    Route::post('loadUmineraxClienteAdm',[
        'uses' =>'TimeSheet\TimeSheetController@loadUmineraxClienteAdm',
        'as' => 'loadUmineraxCliente'
    ]);
  
    Route::post('loadProyxCliente',[
        'uses' =>'TimeSheet\TimeSheetController@loadProyxCliente',
        'as' => 'loadProyxCliente'
    ]);
    Route::post('loadProyxClienteAdm',[
        'uses' =>'TimeSheet\TimeSheetController@loadProyxClienteAdm',
        'as' => 'loadProyxClienteAdm'
    ]);    
    
    Route::post('verTareaProyectoTimeSheet',[
        'uses' =>'TimeSheet\TimeSheetController@verTareaProyectoTimeSheet',
        'as' => 'verTareaProyectoTimeSheet'
    ]);    
    
    Route::post('enviarHoras', [
        'uses' => 'TimeSheet\TimeSheetController@enviarHH',
        'as' => 'enviarHoras']);

    Route::post('reAbrirHoras', [
        'uses' => 'TimeSheet\TimeSheetController@reAbrirHoras',
        'as' => 'reAbrirHoras']);


    Route::get('verHorasAprobaciones',[
        'uses' => 'TimeSheet\TimeSheetController@viewAprobacionHoras',
        'as' => 'verHorasAprobaciones']);

    Route::get('verTodasAprobacionHoras',[
        'uses' => 'TimeSheet\TimeSheetController@verTodasAprobacionHoras',
        'as' => 'verTodasAprobacionHoras']);

    Route::post('verDetalleHoras',[
        'uses' =>'TimeSheet\TimeSheetController@verDetalleHoras',
        'as' =>'verDetalleHoras'
    ]
    );

    Route::post('verDetalleHorasRechazar',[
        'uses' =>'TimeSheet\TimeSheetController@verDetalleHorasRechazar',
        'as' =>'verDetalleHorasRechazar'
    ]
    );

    Route::post('aprobarHorasDeta',[
        'uses' => 'TimeSheet\TimeSheetController@aprobarHorasIn',
        'as' =>'aprobarHorasDeta'
    ]);

    Route::post('aprobarHorasIn_rechazar',[
        'uses' => 'TimeSheet\TimeSheetController@aprobarHorasIn_rechazar',
        'as' =>'aprobarHorasIn_rechazar'
    ]);
    /*Route::post('observarHorasDeta',[
        'uses' => 'TimeSheet\TimeSheetController@observarHorasIn',
        'as' =>'observarHorasDeta'
    ]);   */ 
    Route::get('aprobacionHoras/{valor}', [
        'uses' => 'TimeSheet\TimeSheetController@aprobacionHoras',
        'as' => 'aprobacionHoras']);

    Route::get('viewRechazarHoras', [
        'uses' => 'TimeSheet\TimeSheetController@viewRechazarHoras',
        'as' => 'viewRechazarHoras']);
    
    Route::post('aprobarHoras',[
        'uses' => 'TimeSheet\TimeSheetController@aprobarHoras',
        'as' => 'aprobarHoras'
    ]);

    Route::post('observarHoras',[
        'uses' => 'TimeSheet\TimeSheetController@observarHoras',
        'as' => 'observarHoras'
    ]);

    Route::post('observarHoras_rechazar',[
        'uses' => 'TimeSheet\TimeSheetController@observarHoras_rechazar',
        'as' => 'observarHoras_rechazar'
    ]);

    Route::get('historialAprobaciones', [
        'uses' => 'TimeSheet\TimeSheetController@historialAprobaciones',
        'as' => 'historialAprobaciones']);

    Route::get('getFechaParaDetalle', [
        'uses' => 'TimeSheet\TimeSheetController@getFechaParaDetalle',
        'as' => 'getFechaParaDetalle']);


    /************** INICIO DATA SCP *************/

    Route::get('listadodataScp', [
        'uses' => 'Datascp\DatascpController@listadodataScp',
        'as' => 'listadodataScp']);

    Route::get('listarHT', [
        'uses' => 'Datascp\DatascpController@listarHT',
        'as' => 'listarHT']);





   /************** FIN DATA SCP *************/


    /*** Gastos ***/


 
    Route::get('listarGastosGrid', [
        'uses' => 'Gastos\GastosController@listarGrid',
        'as' => 'listarGastosGrid']);

     Route::get('listaGastosProyecto', [
        'uses' => 'Gastos\GastosController@listaGastosProyecto',
        'as' => 'listaGastosProyecto']);

     Route::get('gastosProyecto', [
        'uses' => 'Gastos\GastosController@gastosProyecto',
        'as' => 'gastosProyecto']);


    Route::post('saveGastoProy',[
        'uses' =>'Gastos\GastosController@saveGastoProy',
        'as' =>'saveGastoProy'
    ]);

    Route::get('gastosAdmProyecto', [
        'uses' => 'Gastos\GastosController@gastosAdmProyecto',
        'as' => 'gastosAdmProyecto']);

    Route::get('gastosVariosProyecto', [
        'uses' => 'Gastos\GastosController@gastosVariosProyecto',
        'as' => 'gastosVariosProyecto']);

    Route::get('rendiciongastosProyecto', [
        'uses' => 'Gastos\GastosController@rendiciongastosProyecto',
        'as' => 'rendiciongastosProyecto']);



    Route::get('aprobacionRendicion', [
        'uses' => 'Gastos\GastosController@aprobacionRendicion',
        'as' => 'aprobacionRendicion']);

    Route::post('verAprobacionRendicion', [
        'uses' => 'Gastos\GastosController@verAprobacionRendicion',
        'as' => 'verAprobacionRendicion']);

    Route::post('aprobarGastoDetalle',[
        'uses' => 'Gastos\GastosController@aprobarGastoDetalle',
        'as' => 'aprobarGastoDetalle'
    ]);
    Route::post('observarGastoDetalle',[
        'uses' => 'Gastos\GastosController@observarGastoDetalle',
        'as' => 'observarGastoDetalle'
    ]);    
    Route::post('verDetalleGasto',[
        'uses' =>'Gastos\GastosController@verDetalleGasto',
        'as' =>'verDetalleGasto'
    ]);
    Route::post('aprobarGastoDetalleSel',[
        'uses' => 'Gastos\GastosController@aprobarGastoDetalleSel',
        'as' => 'aprobarGastoDetalleSel'
    ]);
    Route::post('observarGastoDetalleSel',[
        'uses' => 'Gastos\GastosController@observarGastoDetalleSel',
        'as' => 'observarGastoDetalleSel'
    ]);    

    Route::get('printPrueba',[
        'uses' => 'ReporteController@genPDF',
            'as' =>'printPrueba'
        ]);

    Route::get('printPrueba_2',[
        'uses' => 'ReporteController@genPDF_2',
            'as' =>'printPrueba_2'
        ]);

    Route::get('rendiciongastosProyecto', [
        'uses' => 'Gastos\GastosController@rendiciongastosProyecto',
        'as' => 'rendiciongastosProyecto'
    ]);


    Route::get('RendicionUsuario', [
        'uses' => 'Gastos\GastosController@RendicionUsuario',
        'as' => 'RendicionUsuario'
    ]);
    
    Route::get('viewRendicionUsuario', [
        'uses' => 'Gastos\GastosController@viewRendicionUsuario',
        'as' => 'viewRendicionUsuario'
    ]);
    

    /*
    * Control Documentario
    *
    */
    Route::get('panelJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@panelJefe',
        'as' => 'panelJefe'
    ]);

    
    Route::get('listaJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@listaJefe',
        'as' => 'listaJefe'
    ]);   

    Route::post('viewUbicacion',[
        'uses' =>'Cdocumentario\DocumentarioController@viewUbicacion',
        'as' => 'viewUbicacion'
    ]);
    Route::post('saveUbicacion',[
        'uses' =>'Cdocumentario\DocumentarioController@saveUbicacion',
        'as' => 'saveUbicacion'
    ]);    
    Route::post('buscarListaJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@buscarListaJefe',
        'as' => 'buscarListaJefe'
    ]);   
    Route::post('grabarListaJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarListaJefe',
        'as' => 'grabarListaJefe'
    ]); 
    Route::get('programacionJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@programacionJefe',
        'as' => 'programacionJefe'
    ]);

    Route::post('buscarProgramacionJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@buscarProgramacionJefe',
        'as' => 'buscarProgramacionJefe'
    ]);

    Route::get('ubicacionJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@ubicacionJefe',
        'as' => 'ubicacionJefe'
    ]);     
        
    Route::post('buscarUbicacionJefe', [
        'uses' => 'Cdocumentario\DocumentarioController@buscarUbicacionJefe',
        'as' => 'buscarUbicacionJefe'
    ]);

    /* Usuarios*/
    Route::get('panelUsuario', [
        'uses' => 'Cdocumentario\DocumentarioController@panelUsuario',
        'as' => 'panelUsuario'
    ]);

    Route::post('buscarPanelUsuario', [
        'uses' => 'Cdocumentario\DocumentarioController@buscarPanelUsuario',
        'as' => 'buscarPanelUsuario'
    ]);

    Route::get('entregablecd', [
        'uses' => 'Cdocumentario\DocumentarioController@entregablexcd',
        'as' => 'entregablecd'
    ]);


    Route::get('configuracionTRUser', [
        'uses' => 'Cdocumentario\DocumentarioController@configuracionTRUser',
        'as' => 'configuracionTRUser'        
    ]);   

    Route::get('editarConfiguracionTRUser/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarConfiguracionTRUser',
        'as' => 'editarConfiguracionTRUser'        
    ]);     

    Route::get('modalContactoTransmittalProyecto/{id}',[
        'uses' =>'Cdocumentario\DocumentarioController@modalContactoTransmittalProyecto',
        'as' => 'modalContactoTransmittalProyecto'
    ]);        
    Route::post('addContactoTransmittalProyecto',[
        'uses' =>'Cdocumentario\DocumentarioController@addContactoTransmittalProyecto',
        'as' => 'addContactoTransmittalProyecto'
    ]);        
    Route::get('delContactoTransmittalProyecto/{id}',[
        'uses' =>'Cdocumentario\DocumentarioController@delContactoTransmittalProyecto',
        'as' => 'delContactoTransmittalProyecto'
    ]);
    Route::post('addContactoTransmittalProyectoUM',[
        'uses' => 'Cdocumentario\DocumentarioController@addContactoTransmittalProyectoUM',
        'as' => 'addContactoTransmittalProyectoUM'
    ]);

    Route::post('grabarConfiguracionTRUser', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarConfiguracionTRUser',
        'as' => 'grabarConfiguracionTRUser'        
    ]);        

    Route::get('viewLogoEmpresa/{id}',[
        'uses'=>'Cdocumentario\DocumentarioController@viewLogoEmpresa',
        'as' =>'viewLogoEmpresa'
    ]);

    Route::get('consultaEDT', [
        'uses' => 'Cdocumentario\DocumentarioController@consultaEDT',
        'as' => 'consultaEDT'        
    ]);

    Route::get('editarConsultaEDT/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarConsultaEDT',
        'as' => 'editarConsultaEDT'        
    ]);    

    Route::post('addItemEDT',[
        'uses' => 'Cdocumentario\DocumentarioController@addItemEDT',
        'as' => 'addItemEDT'
    ]);

    Route::get('delItemEDT/{id}',[
        'uses' => 'Cdocumentario\DocumentarioController@delItemEDT',
        'as' => 'delItemEDT'
    ]);    
    Route::post('grabarConsultaEDT', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarConsultaEDT',
        'as' => 'grabarConsultaEDT'        
    ]);




    Route::get('listaEntregableCD', [
        'uses' => 'Cdocumentario\DocumentarioController@listaEntregableCD',
        'as' => 'listaEntregableCD'                
    ]);  

    Route::get('editarListaEntregableCD/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarListaEntregableCD',
        'as' => 'editarListaEntregableCD'                
    ]);  
    Route::post('viewResumenTR',[
        'uses' => 'Cdocumentario\DocumentarioController@viewResumenTR',
        'as' => 'viewResumenTR'
    ]);
    Route::post('agregaraTR',[
        'uses' =>'Cdocumentario\DocumentarioController@agregaraTR',
        'as' =>'agregaraTR'
    ]);

    Route::post('grabarListaEntregableCD', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarListaEntregableCD',
        'as' => 'grabarListaEntregableCD'                
    ]);  

        
    Route::get('revisionDocumentos',[
        'uses' => 'Cdocumentario\DocumentarioController@revisionDocumentos',
        'as' => 'revisionDocumentos'                        
    ]);

    Route::get('editarRevisionDocumentos/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarRevisionDocumentos',
        'as' => 'editarRevisionDocumentos'                        
    ]);

    Route::post('verRevisionesDoc',[
        'uses'=>'Cdocumentario\DocumentarioController@verRevisionesDoc',
        'as' =>'verRevisionesDoc'
    ]);

    Route::post('grabarRevisionDocumentos', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarRevisionDocumentos',
        'as' => 'grabarRevisionDocumentos'                
    ]);


    Route::get('generacionTR', [
        'uses' => 'Cdocumentario\DocumentarioController@generacionTR',
        'as' => 'generacionTR'                
    ]);    
        
    Route::get('editarGeneracionTR/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarGeneracionTR',
        'as' => 'editarGeneracionTR'                
    ]);    

    Route::get('obtenerTransmittal/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@obtenerTransmittal',
        'as' => 'obtenerTransmittal'                
    ]);   

    Route::post('grabarGeneracionTR', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarGeneracionTR',
        'as' => 'grabarGeneracionTR'                
    ]);    
    Route::post('emitirGeneracionTR',[
        'uses' => 'Cdocumentario\DocumentarioController@emitirGeneracionTR',
        'as' => 'emitirGeneracionTR'
    ]);

    Route::get('printedGeneracionTR',[
        'uses' => 'Cdocumentario\DocumentarioController@printedGeneracionTR',
        'as' => 'printedGeneracionTR '
    ]);   
    Route::get('listarTR',[
        'uses' =>'Cdocumentario\DocumentarioController@listarTR',
        'as' =>'listarTR'
    ]);
    Route::get('listarRecTR',[
        'uses' =>'Cdocumentario\DocumentarioController@listarRecTR',
        'as' =>'listarRecTR'
    ]);    
    Route::get('modalItemGeneraTR/{id}',[
        'uses' => 'Cdocumentario\DocumentarioController@modalItemGeneraTR',
        'as' =>'modalItemGeneraTR'
    ]);
    Route::post('saveItemGeneraTR',[
        'uses' => 'Cdocumentario\DocumentarioController@saveItemGeneraTR',
        'as' =>'saveItemGeneraTR'
    ]);
    Route::get('eliminarItemGenerarTR/{id}',[
        'uses' => 'Cdocumentario\DocumentarioController@eliminarItemGenerarTR',
        'as' =>'eliminarItemGenerarTR'
    ]);         
    Route::get('recepcionTR', [
        'uses' => 'Cdocumentario\DocumentarioController@recepcionTR',
        'as' => 'recepcionTR'                
    ]);

    Route::get('editarRecepcionTR/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarRecepcionTR',
        'as' => 'editarRecepcionTR'                
    ]);
    Route::get('obtenerTransmittalRecepcion/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@obtenerTransmittalRecepcion',
        'as' => 'obtenerTransmittalRecepcion'                
    ]); 
    Route::get('obtenerTransmittalEnviado/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@obtenerTransmittalEnviado',
        'as' => 'obtenerTransmittalEnviado'                
    ]);     
    Route::post('grabarRecepcionTR', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarRecepcionTR',
        'as' => 'grabarRecepcionTR'                
    ]);                
    Route::get('modalItemRecepcionTR/{id}',[
        'uses' => 'Cdocumentario\DocumentarioController@modalItemRecepcionTR',
        'as' =>'modalItemRecepcionTR'
    ]);
    Route::post('saveItemRecepcionTR',[
        'uses' => 'Cdocumentario\DocumentarioController@saveItemRecepcionTR',
        'as' =>'saveItemRecepcionTR'
    ]);
    Route::get('eliminarItemRecepcionTR/{id}',[
        'uses' => 'Cdocumentario\DocumentarioController@eliminarItemRecepcionTR',
        'as' =>'eliminarItemRecepcionTR'
    ]); 
    Route::get('RegistroDOCsnTR', [
        'uses' => 'Cdocumentario\DocumentarioController@RegistroDOCsnTR',
        'as' => 'RegistroDOCsnTR'                
    ]);                  

    Route::get('editarRegistroDOCsnTR/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarRegistroDOCsnTR',
        'as' => 'editarRegistroDOCsnTR'                
    ]);                  

    Route::post('grabarRegistroDOCsnTR', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarRegistroDOCsnTR',
        'as' => 'grabarRegistroDOCsnTR'                
    ]);                                  
           
    Route::get('planificacionCD', [
        'uses' => 'Cdocumentario\DocumentarioController@planificacionCD',
        'as' => 'planificacionCD'                
    ]);   

    Route::get('editarplanificacionCD/{id}', [
        'uses' => 'Cdocumentario\DocumentarioController@editarplanificacionCD',
        'as' => 'editarplanificacionCD'                
    ]);   

    Route::post('grabarplanificacionCD', [
        'uses' => 'Cdocumentario\DocumentarioController@grabarplanificacionCD',
        'as' => 'grabarplanificacionCD'                
    ]);                   
    /* Fin de Control Documentario*/    

    /*
    *Gestion Humana
    *
    */
    Route::get('listarpersonal', [
        'uses' =>'Empleado\PersonalController@listarpersonal',
        'as' =>'listarpersonal'
        ]
        );

    Route::get('listadoEmpleado', [
        'uses' =>'Empleado\EmpleadoController@listadoEmpleado',
        'as' =>'listadoEmpleado'
        ]
        );
    Route::get('listarEmpleado', [
        'uses' =>'Empleado\EmpleadoController@listarEmpleado',
        'as' =>'listarEmpleado'
        ]
        );    
    Route::get('empleado',  [
        'uses' =>'Empleado\EmpleadoController@registroEmpleado',
        'as' =>'empleado'
        ]); 

    Route::get('editarEmpleado/{id}',  [
        'uses' =>'Empleado\EmpleadoController@editarEmpleado',
        'as' =>'editarEmpleado'
        ]);

    Route::get('estadoempleadousuario/{id}',  [
        'uses' =>'Empleado\PersonalController@estadoempleadousuario',
        'as' =>'estadoempleadousuario'
        ]);


    Route::post('grabarEmpleado',  [
        'uses' =>'Empleado\EmpleadoController@grabarEmpleado',
        'as' =>'grabarEmpleado'
        ]);

    Route::post('saveFamEmpleado',[
        'uses' =>'Empleado\EmpleadoController@saveFamEmpleado',
        'as' =>'saveFamEmpleado'
        ]
        );

     Route::post('addFamEmpleado',[
        'uses' =>'Empleado\EmpleadoController@addFamEmpleado',
        'as' =>'addFamEmpleado'
        ]
        );

    Route::get('verFamEmpleado/{id}',[
        'uses' =>'Empleado\EmpleadoController@verFamEmpleado',
        'as' =>'verFamEmpleado'
        ]
        );

    Route::post('saveEstuEmpleado',[
        'uses' =>'Empleado\EmpleadoController@saveEstudEmpleado',
        'as' =>'saveEstudEmpleado'
        ]
        );

    Route::post('addEstuEmpleado',[
        'uses' =>'Empleado\EmpleadoController@addEstuEmpleado',
        'as' =>'addEstuEmpleado'
        ]
        );

    Route::get('verEstudEmpleado/{id}',[
        'uses' =>'Empleado\EmpleadoController@verEstudEmpleado',
        'as' =>'verEstudEmpleado'
        ]
        );

    Route::post('saveEnfEmpleado',[
        'uses' =>'Empleado\EmpleadoController@saveEnfEmpleado',
        'as' =>'saveEnfEmpleado'
        ]
        );

    Route::post('addEnfEmpleado',[
        'uses' =>'Empleado\EmpleadoController@addEnfEmpleado',
        'as' =>'addEnfEmpleado'
        ]
        );
    
    Route::get('verEnfEmpleado/{id}',[
        'uses' =>'Empleado\EmpleadoController@verEnfEmpleado',
        'as' =>'verEnfEmpleado'
        ]
        );

    Route::post('saveAlergEmpleado',[
        'uses' =>'Empleado\EmpleadoController@saveAlergEmpleado',
        'as' =>'saveAlergEmpleado'
        ]
        );

    Route::post('addAlergEmpleado',[
        'uses' =>'Empleado\EmpleadoController@addAlergEmpleado',
        'as' =>'addAlergEmpleado'
        ]
        );
    
    Route::get('verAlergEmpleado/{id}',[
        'uses' =>'Empleado\EmpleadoController@verAlergEmpleado',
        'as' =>'verAlergEmpleado'
        ]
        );

    Route::get('eliminarFamiliarEmpleado/{id}',
        [
        'uses' => 'Empleado\EmpleadoController@eliminarFamiliarEmpleado',
        'as' => 'eliminarFamiliarEmpleado'
        ]
        );

    Route::get('eliminarEstudioEmpleado/{id}',
        [
        'uses' => 'Empleado\EmpleadoController@eliminarEstudioEmpleado',
        'as' => 'eliminarEstudioEmpleado'
        ]
        );

    Route::get('eliminarEnfEmpleado/{id}',
        [
        'uses' => 'Empleado\EmpleadoController@eliminarEnfEmpleado',
        'as' => 'eliminarEnfEmpleado'
        ]
        );
    
    Route::get('eliminarAlergEmpleado/{id}',
        [
        'uses' => 'Empleado\EmpleadoController@eliminarAlergEmpleado',
        'as' => 'eliminarAlergEmpleado'
        ]
        );

    
    /* Fin de Gestion Huamana*/


    // Panel Principal

    Route::get('panelPrincipal', [
        'uses' => 'MainController@panelPrincipal',
        'as' => 'panelPrincipal'
    ]);

    
    Route::get('listaPrincipal', [
        'uses' => 'MainController@listaPrincipal',
        'as' => 'listaPrincipal'
    ]);

    Route::post('buscarListaPrincipal', [
        'uses' => 'MainController@buscarListaPrincipal',
        'as' => 'buscarListaPrincipal'
    ]); 


    //Fin Panel Principal

    /*
    * Sistema
    *
    */
    Route::get('generarBackup', function (){
            return view('sistema.generarBackUp');
        });
    Route::get('registroUsuario', [
        'uses' => 'Usuario\UsuarioController@registroUsuarios',
        'as' => 'registroUsuario'        
        ]);   

    Route::get('editarUsuarios/{id}',
        [
        'uses' => 'Usuario\UsuarioController@editarUsuarios',
        'as' => 'editarUsuarios'
        ]
        );

    Route::get('listarUsuarios',[
        'uses' =>'Usuario\UsuarioController@listarUsuarios',
        'as' =>'listarUsuarios'
        ]
        );
    
    Route::get('listadoUsuarios', [
        'uses' => 'Usuario\UsuarioController@listadoUsuarios',
        'as' =>'listadoUsuarios']
        );
    
    Route::post('agregarUsuario',[
        'uses' =>'Usuario\UsuarioController@agregarUsuario',
        'as' => 'agregarUsuario'
        ]
        );

    Route::get('passwordUsuario',[
        'uses' =>'Usuario\UsuarioController@passwordUsuario',
        'as' => 'passwordUsuario'
        ]
        );

    Route::post('agregarPassword',[
        'uses' =>'Usuario\UsuarioController@agregarPassword',
        'as' => 'agregarPassword'
        ]
        );
    
    Route::post('getPasswordEncrip',[
        'uses' =>'Usuario\UsuarioController@getPasswordEncrip',
        'as' => 'getPasswordEncrip'
        ]
        );

    /*Route::get('usuarioConectado', function (){
            return view('sistema.usuariosConectados');
        });  */

    Route::get('usuarioConectado',[
        'uses' =>'Configuracion\ConfiguracionController@listadousuarioConectado',
        'as' => 'usuarioConectado'
        ]);

    Route::get('listarUsuarioConectados',[
        'uses' =>'Configuracion\ConfiguracionController@listarUsuarioConectados',
        'as' =>'listarUsuarioConectados'
        ]
        );

    Route::get('editarusuarioConectado/{id}',
        [
        'uses' => 'Usuario\UsuarioController@editarusuarioConectado',
        'as' => 'editarusuarioConectado'
        ]);



    Route::get('trazaUsuario', function (){
            return view('sistema.trazabilidadUsuario');
        });   
    Route::get('trazaProy', function (){
            return view('sistema.trazabilidadPorProyecto');
        });   
    Route::get('trazaGral', function (){
            return view('sistema.trazabilidadGeneral');
        });      
      

    Route::post('grabarConfiguracion',[
        'uses' =>'Configuracion\ConfiguracionController@grabarConfiguracion',
        'as' => 'grabarConfiguracion'
        ]
        );
    Route::get('confSistema',[
        'uses' =>'Configuracion\ConfiguracionController@mostrarDatosConfiguracion',
        'as' => 'confSistema'
        ]
        );

    /*Route::get('asigPermisos', function (){
            return view('sistema.asignacionPermisos');
        });  
    */
    Route::get('asigPermisos',[
        'uses' =>'Configuracion\ConfiguracionController@listaAsignacionPermiso',
        'as' =>'asigPermisos'
        ]
        );   

    Route::get('listarUsuario',[
        'uses' =>'Configuracion\ConfiguracionController@listarUsuario',
        'as' => 'listarUsuario'
        ]
        );        

    Route::get('editarAsignacionPermiso/{id}',[
        'uses' =>'Configuracion\ConfiguracionController@editarAsignacionPermiso',
        'as' => 'editarAsignacionPermiso'
        ]
        );  

    Route::get('delAsignacionPermiso/{id}',[
        'uses' => 'Configuracion\ConfiguracionController@delAsignacionPermiso',
        'as' => 'delAsignacionPermiso'
        ]);   
              
    Route::post('agregarPermisos',        [
        'uses' => 'Configuracion\ConfiguracionController@agregarPermisos',
        'as' => 'agregarPermisos'
        ]);

    Route::get('viewUsuarioPermisos/{id}',[
        'uses' =>'Configuracion\ConfiguracionController@viewUsuarioPermisos',
        'as' => 'viewUsuarioPermisos'
        ]
        );  

    Route::get('viewUsuario/{id}',[
        'uses' =>'Configuracion\ConfiguracionController@viewUsuario',
        'as' => 'viewUsuario'
        ]
        );  
                
    /* Fin de Sistema*/

    /* Inicio Mantenimientos */

    Route::get('listarAlergias',[
        'uses' =>'Mantenimientos\MantenimientosController@listarAlergias',
        'as' => 'listarAlergias'
        ]
        );
    Route::get('alergias',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoAlergias',
        'as' => 'alergias'
        ]
        );   

     Route::get('editarAlergias/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@editarAlergias',
        'as' => 'editarAlergias'
        ]
        );


     Route::post('agregarAlergia',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarAlergia',
        'as' => 'agregarAlergia'
        ]
        );

      Route::get('eliminarAlergias/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@eliminarAlergias',
        'as' => 'eliminarAlergias'
        ]
        );



    Route::get('listarGastos', [
        'uses' =>'Mantenimientos\MantenimientosController@listarGastos',
        'as' => 'listarGastos'        
        ]);

    Route::get('listaConceptoGastos',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoGastos',
        'as' => 'listaConceptoGastos'
        ]
        );

     Route::get('editarGasto/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@editarGasto',
        'as' => 'editarGasto'
        ]
        );


     Route::post('agregarGasto',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarGasto',
        'as' => 'agregarGasto'
        ]
        );

     Route::get('eliminarGasto/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@eliminarGasto',
        'as' => 'eliminarGasto'
        ]
        );

   Route::get('listaActividades', function (){
            return view('mantenimiento.actividades');
        }); 

      Route::get('listarActividad',[
        'uses' =>'Mantenimientos\MantenimientosController@listarActividad',
        'as' => 'listarActividad'
        ]
        );
    Route::get('listaActividades',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoActividad',
        'as' => 'listaActividades'
        ]
        );

     Route::get('editarActividad/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@editarActividad',
        'as' => 'editarActividad'
        ]
        );

     Route::post('agregarActividad',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarActividad',
        'as' => 'agregarActividad'
        ]
        );

     Route::get('eliminarActividad/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@eliminarActividad',
        'as' => 'eliminarActividad'
        ]
        );

     Route::get('listaAreas', function (){
            return view('mantenimiento.areas');
        });  

     Route::get('listaAreas', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoArea',
        'as' => 'listaAreas'        
        ]); 
      

    Route::get('listaCargos', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoCargos',
        'as' => 'listaCargos'        
        ]);

    Route::get('listaCheckList', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoCheckList',
        'as' => 'listaCheckList'        
        ]);

    Route::get('listaPais', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoPais',
        'as' => 'listaPais'        
        ]);

    Route::get('listaAreasConocimiento', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoAreasConocimiento',
        'as' => 'listaAreasConocimiento'        
        ]);



    Route::get('listaFasesProyecto', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoFaseProyecto',
        'as' => 'listaFasesProyecto'        
        ]);



    Route::get('listaSedes', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoSedes',
        'as' => 'listaSedes'        
        ]);


    Route::get('listaDisciplina', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoDisciplina',
        'as' => 'listaDisciplina'        
        ]);

    Route::get('listaContactoCargo', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoContactoCargo',
        'as' => 'listaContactoCargo'        
        ]);

    Route::get('listarServicios',[
        'uses' =>'Mantenimientos\MantenimientosController@listarServicios',
        'as' =>'listarServicios'
        ]);

    Route::get('servicios', [
        'uses' => 'Mantenimientos\MantenimientosController@listadoserv',
        'as' =>'servicios']
        );

 
    Route::post('agregarServicio',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarServicio',
        'as' => 'agregarServicio'
        ]);

    Route::get('editarServicios/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarServicios',
        'as' => 'editarServicios'
        ]);


     Route::get('eliminarServicios/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@eliminarServicios',
        'as' => 'eliminarServicios'
        ]
        );


    Route::get('listaEntregables', [
        'uses' => 'Mantenimientos\MantenimientosController@listadoEntregablesMantenimiento',
        'as' =>'listaEntregables']
        );

    Route::get('listarEntregablesMantenimiento',[
        'uses' =>'Mantenimientos\MantenimientosController@listarEntregablesMantenimiento',
        'as' =>'listarEntregablesMantenimiento'
        ]);

    Route::post('agregarEntregableMantenimiento',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarEntregableMantenimiento',
        'as' => 'agregarEntregableMantenimiento'
        ]);

    
    Route::get('editarEntregablesMantenimiento/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarEntregablesMantenimiento',
        'as' => 'editarEntregablesMantenimiento'
        ]);

     Route::get('eliminarentregableMantenimiento/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarentregableMantenimiento',
        'as' => 'eliminarentregableMantenimiento'
        ]);


     Route::get('listarCargos',[
        'uses' =>'Mantenimientos\MantenimientosController@listarCargos',
        'as' =>'listarCargos'
        ]);

     Route::get('registroCargos', [
        'uses' => 'Mantenimientos\MantenimientosController@registroCargos',
        'as' => 'registroCargos'        
        ]);

     Route::get('editarCargos/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarCargos',
        'as' => 'editarCargos'
        ]);

     Route::post('agregarCargos',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarCargos',
        'as' => 'agregarCargos'
        ]);

      Route::get('eliminarCargo/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarCargo',
        'as' => 'eliminarCargo'
        ]);


     Route::get('listarArea',[
        'uses' =>'Mantenimientos\MantenimientosController@listarArea',
        'as' =>'listarArea'
        ]);
  

     Route::get('editarArea/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarArea',
        'as' => 'editarArea'
        ]);

     Route::post('agregarArea',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarArea',
        'as' => 'agregarArea'
        ]);

      Route::get('eliminarAreaMant/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarAreaMant',
        'as' => 'eliminarAreaMant'
        ]);

   
     Route::get('listarSedes',[
        'uses' =>'Mantenimientos\MantenimientosController@listarSedes',
        'as' =>'listarSedes'
        ]);    

     Route::get('editarSede/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarSede',
        'as' => 'editarSede'
        ]);

     Route::post('agregarSede',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarSede',
        'as' => 'agregarSede'
        ]);

     Route::get('eliminarSedeMant/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarSedeMant',
        'as' => 'eliminarSedeMant'
        ]);


      Route::get('listarFaseProyecto',[
        'uses' =>'Mantenimientos\MantenimientosController@listarFaseProyecto',
        'as' =>'listarFaseProyecto'
        ]);

     Route::get('editarFaseProyecto/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarFaseProyecto',
        'as' => 'editarFaseProyecto'
        ]);

     Route::post('agregarFaseProyecto',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarFaseProyecto',
        'as' => 'agregarFaseProyecto'
        ]);

      Route::get('eliminarFaseProyecto/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarFaseProyecto',
        'as' => 'eliminarFaseProyecto'
        ]);

      Route::get('listarAreasConocimiento',[
        'uses' =>'Mantenimientos\MantenimientosController@listarAreasConocimiento',
        'as' =>'listarAreasConocimiento'
        ]);

     Route::get('editarAreasConocimiento/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarAreasConocimiento',
        'as' => 'editarAreasConocimiento'
        ]);

     Route::post('agregarAreasConocimiento',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarAreasConocimiento',
        'as' => 'agregarAreasConocimiento'
        ]);

      Route::get('eliminarAreasConocimiento/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarAreasConocimiento',
        'as' => 'eliminarAreasConocimiento'
        ]);

      Route::get('listarPais',[
        'uses' =>'Mantenimientos\MantenimientosController@listarPais',
        'as' =>'listarPais'
        ]);

     Route::get('editarPais/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarPais',
        'as' => 'editarPais'
        ]);

     Route::post('agregarPais',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarPais',
        'as' => 'agregarPais'
        ]);

     Route::get('eliminarPais',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarPais',
        'as' => 'eliminarPais'
        ]);

     Route::get('listarCheckList',[
        'uses' =>'Mantenimientos\MantenimientosController@listarCheckList',
        'as' =>'listarCheckList'
        ]);

     Route::get('editarCheckList/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarCheckList',
        'as' => 'editarCheckList'
        ]);

     Route::post('agregarCheckListMant',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarCheckListMant',
        'as' => 'agregarCheckListMant'
        ]);

     Route::get('eliminarCheckList/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarCheckList',
        'as' => 'eliminarCheckList'
        ]);
     
     Route::get('listarDisciplina',[
        'uses' =>'Mantenimientos\MantenimientosController@listarDisciplina',
        'as' =>'listarDisciplina'
        ]);

      Route::get('listarFaseProyecto',[
        'uses' =>'Mantenimientos\MantenimientosController@listarFaseProyecto',
        'as' =>'listarFaseProyecto'
        ]);

     Route::get('editarFaseProyecto/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarFaseProyecto',
        'as' => 'editarFaseProyecto'
        ]);

     Route::post('agregarFaseProyecto',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarFaseProyecto',
        'as' => 'agregarFaseProyecto'
        ]);

      Route::get('eliminarFaseProyecto',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarFaseProyecto',
        'as' => 'eliminarFaseProyecto'
        ]);

      Route::get('listarAreasConocimiento',[
        'uses' =>'Mantenimientos\MantenimientosController@listarAreasConocimiento',
        'as' =>'listarAreasConocimiento'
        ]);
   
     Route::get('editarAreasConocimiento/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarAreasConocimiento',
        'as' => 'editarAreasConocimiento'
        ]);

     Route::post('agregarAreasConocimiento',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarAreasConocimiento',
        'as' => 'agregarAreasConocimiento'
        ]);

      Route::get('eliminarAreasConocimiento/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarAreasConocimiento',
        'as' => 'eliminarAreasConocimiento'
        ]);

      Route::get('listarPais',[
        'uses' =>'Mantenimientos\MantenimientosController@listarPais',
        'as' =>'listarPais'
        ]);

     Route::get('editarPais/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarPais',
        'as' => 'editarPais'
        ]);

     Route::post('agregarPais',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarPais',
        'as' => 'agregarPais'
        ]);

     Route::get('eliminarPais/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarPais',
        'as' => 'eliminarPais'
        ]);


     Route::get('listarCheckList',[
        'uses' =>'Mantenimientos\MantenimientosController@listarCheckList',
        'as' =>'listarCheckList'
        ]);

     Route::get('editarCheckList/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarCheckList',
        'as' => 'editarCheckList'
        ]);

     Route::post('agregarCheckListMant',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarCheckListMant',
        'as' => 'agregarCheckListMant'
        ]);

     Route::get('eliminarCheckList',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarCheckList',
        'as' => 'eliminarCheckList'
        ]);


     Route::get('listarDisciplina',[
        'uses' =>'Mantenimientos\MantenimientosController@listarDisciplina',
        'as' =>'listarDisciplina'
        ]);

     Route::get('editarDisciplina/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarDisciplina',
        'as' => 'editarDisciplina'
        ]);

     Route::post('agregarDisciplina',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarDisciplina',
        'as' => 'agregarDisciplina'
        ]);

     Route::get('eliminarDisciplina/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarDisciplina',
        'as' => 'eliminarDisciplina'
        ]);


     Route::get('listarContactoCargo',[
        'uses' =>'Mantenimientos\MantenimientosController@listarContactoCargo',
        'as' =>'listarContactoCargo'
        ]);
     
     Route::get('editarContactoCargo/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarContactoCargo',
        'as' => 'editarContactoCargo'
        ]);

     Route::post('agregarContactoCargo',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarContactoCargo',
        'as' => 'agregarContactoCargo'
        ]);

     Route::get('eliminarContactoCargo/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarContactoCargo',
        'as' => 'eliminarContactoCargo'
        ]);


     Route::get('listarInstitucion',[
        'uses' =>'Mantenimientos\MantenimientosController@listarInstitucion',
        'as' =>'listarInstitucion'
        ]);

    Route::get('listaInstitucion',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoInstitucion',
        'as' => 'listaInstitucion'
        ]
        );
   
     Route::get('editarInstitucion/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@editarInstitucion',
        'as' => 'editarInstitucion'
        ]
        );

      Route::get('eliminarInstitucion/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarInstitucion',
        'as' => 'eliminarInstitucion'
        ]
        );

     Route::post('agregarInstitucion',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarInstitucion',
        'as' => 'agregarInstitucion'
        ]
        );

     Route::get('eliminarContactoCargo/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarContactoCargo',
        'as' => 'eliminarContactoCargo'
        ]);



      Route::get('listarTiposContacto',[
        'uses' =>'Mantenimientos\MantenimientosController@listarTiposContacto',
        'as' =>'listarTiposContacto'
        ]);

    Route::get('listaTiposContacto',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoTiposContacto',
        'as' => 'listaTiposContacto'
        ]
        );

    Route::get('registroTiposContacto',[
        'uses' =>'Mantenimientos\MantenimientosController@registroTiposContacto',
        'as' => 'registroTiposContacto'
        ]
        );

     Route::get('editarTiposContacto/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@editarTiposContacto',
        'as' => 'editarTiposContacto'
        ]
        );

     Route::get('eliminarTiposContacto/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarTiposContacto',
        'as' => 'eliminarTiposContacto'
        ]
        );

     Route::post('agregarTiposContacto',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarTiposContacto',
        'as' => 'agregarTiposContacto'
        ]
        );

      Route::get('listarTiposInformacionContacto',[
        'uses' =>'Mantenimientos\MantenimientosController@listarTiposInformacionContacto',
        'as' =>'listarTiposInformacionContacto'
        ]);

    Route::get('listaTiposInformacionContacto',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoTiposInformacionContacto',
        'as' => 'listaTiposInformacionContacto'
        ]
        );
   
     Route::get('editarTiposInformacionContacto/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@editarTiposInformacionContacto',
        'as' => 'editarTiposInformacionContacto'
        ]
        );

      Route::get('eliminarTiposInformacionContacto/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarTiposInformacionContacto',
        'as' => 'eliminarTiposInformacionContacto'
        ]
        );

     Route::post('agregarTiposInformacionContacto',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarTiposInformacionContacto',
        'as' => 'agregarTiposInformacionContacto'
        ]
        );


      Route::get('listaOficina', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoOficina',
        'as' => 'listaOficina'        
        ]);

      Route::get('listarOficina',[
        'uses' =>'Mantenimientos\MantenimientosController@listarOficina',
        'as' =>'listarOficina'
        ]);

     Route::get('editarOficina/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarOficina',
        'as' => 'editarOficina'
        ]);

     Route::post('agregarOficina',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarOficina',
        'as' => 'agregarOficina'
        ]);

     Route::get('eliminarOficina/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarOficina',
        'as' => 'eliminarOficina'
        ]);

      Route::get('listaTiposNoFacturables', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoTiposNoFacturables',
        'as' => 'listaTiposNoFacturables'        
        ]);

      Route::get('listarTiposNoFacturables',[
        'uses' =>'Mantenimientos\MantenimientosController@listarTiposNoFacturables',
        'as' =>'listarTiposNoFacturables'
        ]);

     Route::get('editarTiposNoFacturables/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarTiposNoFacturables',
        'as' => 'editarTiposNoFacturables'
        ]);

     Route::post('agregarTiposNoFacturables',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarTiposNoFacturables',
        'as' => 'agregarTiposNoFacturables'
        ]);


     Route::get('eliminarTiposNoFacturables/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarTiposNoFacturables',
        'as' => 'eliminarTiposNoFacturables'
        ]);    


       Route::get('listaTiposEntregables', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoTiposEntregables',
        'as' => 'listaTiposEntregables'        
        ]);

       Route::get('listarTiposEntregables',[
        'uses' =>'Mantenimientos\MantenimientosController@listarTiposEntregables',
        'as' =>'listarTiposEntregables'
        ]);

       Route::get('editarTiposEntregables/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarTiposEntregables',
        'as' => 'editarTiposEntregables'
        ]);

     Route::post('agregarTiposEntregables',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarTiposEntregables',
        'as' => 'agregarTiposEntregables'
        ]);

     Route::get('eliminarTiposEntregables/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarTiposEntregables',
        'as' => 'eliminarTiposEntregables'
        ]);



     Route::get('listaEnfermedad', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoEnfermedad',
        'as' => 'listaEnfermedad'        
        ]);

     Route::get('listarEnfermedad',[
        'uses' =>'Mantenimientos\MantenimientosController@listarEnfermedad',
        'as' =>'listarEnfermedad'
        ]);

     Route::get('editarEnfermedad/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarEnfermedad',
        'as' => 'editarEnfermedad'
        ]);

     Route::post('agregarEnfermedad',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarEnfermedad',
        'as' => 'agregarEnfermedad'
        ]);

     Route::get('eliminarEnfermedad/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarEnfermedad',
        'as' => 'eliminarEnfermedad'
        ]);

     Route::get('listaEspecialidad', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoEspecialidad',
        'as' => 'listaEspecialidad'        
        ]);

     Route::get('listarEspecialidad',[
        'uses' =>'Mantenimientos\MantenimientosController@listarEspecialidad',
        'as' =>'listarEspecialidad'
        ]);
     Route::get('editarEspecialidad/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarEspecialidad',
        'as' => 'editarEspecialidad'
        ]);

     Route::post('agregarEspecialidad',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarEspecialidad',
        'as' => 'agregarEspecialidad'
        ]);

     Route::get('eliminarEspecialidad/{id}',[
        'uses' =>'Mantenimientos\MantenimientosController@eliminarEspecialidad',
        'as' => 'eliminarEspecialidad'
        ]); 
     
     Route::get('listarCatalogoTablas',[
        'uses' =>'Mantenimientos\MantenimientosController@listarCatalogoTablas',
        'as' =>'listarCatalogoTablas'
        ]);
     Route::get('catalogoTablas',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoCatalogoTablas',
        'as' => 'catalogoTablas'
        ]);

     Route::get('editarCatalogoTablas/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarCatalogoTablas',
        'as' => 'editarCatalogoTablas'
        ]);

     Route::get('eliminarCatalogoTablas/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@eliminarCatalogoTablas',
        'as' => 'eliminarCatalogoTablas'
        ]);

     Route::post('agregarCatalogoTablas',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarCatalogoTablas',
        'as' => 'agregarCatalogoTablas'
        ]);

     Route::get('catalogoGrupoTablas',[
        'uses' =>'Mantenimientos\MantenimientosController@listadoCatalogoGrupoTablas',
        'as' => 'catalogoGrupoTablas'
        ]);
     

     Route::get('listarCatalogoGrupoTablas',[
        'uses' =>'Mantenimientos\MantenimientosController@listarCatalogoGrupoTablas',
        'as' =>'listarCatalogoGrupoTablas'
        ]);
   

    Route::get('editarCatalogoGrupoTablas/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarCatalogoGrupoTablas',
        'as' => 'editarCatalogoGrupoTablas'
        ]);

     Route::post('agregarCatalogoGrupoTablas',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarCatalogoGrupoTablas',
        'as' => 'agregarCatalogoGrupoTablas'
        ]);

     Route::get('eliminarCatalogoGrupoTablas/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@eliminarCatalogoGrupoTablas',
        'as' => 'eliminarCatalogoGrupoTablas'
        ]);

     Route::get('listaMonedaCambio', [
        'uses' =>'Mantenimientos\MantenimientosController@listadoMonedaCambio',
        'as' =>'listaMonedaCambio'
        ]);
     
     Route::get('listarMonedaCambio',[
        'uses' =>'Mantenimientos\MantenimientosController@listarMonedaCambio',
        'as' =>'listarMonedaCambio'
        ]);
   

    Route::get('editarMonedaCambio/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@editarMonedaCambio',
        'as' => 'editarMonedaCambio'
        ]);

    Route::get('eliminarMonedaCambio/{id}',
        [
        'uses' => 'Mantenimientos\MantenimientosController@eliminarMonedaCambio',
        'as' => 'eliminarMonedaCambio'
        ]);


     Route::post('agregarMonedaCambio',[
        'uses' =>'Mantenimientos\MantenimientosController@agregarMonedaCambio',
        'as' => 'agregarMonedaCambio'
        ]);

    /* Fin Mantenimientos */




 /*** Inicio Migracion */

Route::get('migracionGastos',[
        'uses' => 'Migracion\MigracionController@migracionGastos',
        'as' => 'migracionGastos'
        ]);

Route::get('migracionActividades',[
        'uses' => 'Migracion\MigracionController@migracionActividades',
        'as' => 'migracionActividades'
        ]);

Route::get('migracionAreas',[
        'uses' => 'Migracion\MigracionController@migracionAreas',
        'as' => 'migracionAreas'
        ]);

Route::get('migracionCargo',[
        'uses' => 'Migracion\MigracionController@migracionCargo',
        'as' => 'migracionCargo'
        ]);

Route::get('migracionMasterEntregables',[
        'uses' => 'Migracion\MigracionController@migracionMasterEntregables',
        'as' => 'migracionMasterEntregables'
        ]);

Route::get('migracionProveedores',[
        'uses' => 'Migracion\MigracionController@migracionProveedores',
        'as' => 'migracionProveedores'
        ]);

Route::get('migracionClientes',[
        'uses' => 'Migracion\MigracionController@migracionClientes',
        'as' => 'migracionClientes'
        ]);

Route::get('migracionClientesOtro',[
        'uses' => 'Migracion\MigracionController@migracionClientesOtro',
        'as' => 'migracionClientesOtro'
        ]);

Route::get('migracionContactosUminera',[
        'uses' => 'Migracion\MigracionController@migracionContactosUminera',
        'as' => 'migracionContactosUminera'
        ]);

Route::get('migracionProyectos',[
        'uses' => 'Migracion\MigracionController@migracionProyectos',
        'as' => 'migracionProyectos'
        ]);

Route::get('migracionRendicionGasto',[
        'uses' => 'Migracion\MigracionController@migracionRendicionGasto',
        'as' => 'migracionRendicionGasto'
        ]);
 
Route::get('migracionHoras',[
        'uses' => 'Migracion\MigracionController@migracionHoras',
        'as' => 'migracionHoras'
        ]);

Route::get('migracionMonedaCambio',[
        'uses' => 'Migracion\MigracionController@migracionMonedaCambio',
        'as' => 'migracionMonedaCambio'
        ]);

Route::get('migracionData',[
        'uses' => 'Migracion\MigracionController@migracionData',
        'as' => 'migracionData'
        ]);

Route::get('migracionProyectosDisciplina',[
        'uses' => 'Migracion\MigracionController@migracionProyectosDisciplina',
        'as' => 'migracionProyectosDisciplina'
        ]);

Route::get('migracionProyectoActividades',[
        'uses' => 'Migracion\MigracionController@migracionProyectoActividades',
        'as' => 'migracionProyectoActividades'
        ]);

Route::get('migracionUsuariosAbreviatura',[
        'uses' => 'Migracion\MigracionController@migracionUsuariosAbreviatura',
        'as' => 'migracionUsuariosAbreviatura'
        ]);

Route::get('migracionProyActivHabilitacion',[
        'uses' => 'Migracion\MigracionController@migracionProyActivHabilitacion',
        'as' => 'migracionProyActivHabilitacion'
        ]);

Route::get('migracionProyEstados',[
        'uses' => 'Migracion\MigracionController@migracionProyEstados',
        'as' => 'migracionProyEstados'
        ]);

Route::get('migracionAreaCargoDespliegue',[
        'uses' => 'Migracion\MigracionController@migracionAreaCargoDespliegue',
        'as' => 'migracionAreaCargoDespliegue'
        ]);

Route::get('asignarpermisoListaProy',[
        'uses' => 'Migracion\MigracionController@asignarpermisoListaProy',
        'as' => 'asignarpermisoListaProy'
        ]);

Route::get('asignarpermisoGastos',[
        'uses' => 'Migracion\MigracionController@asignarpermisoGastos',
        'as' => 'asignarpermisoGastos'
]);

Route::get('asignarArea',[
        'uses' => 'Migracion\MigracionController@asignarArea',
        'as' => 'asignarArea'
]);

Route::get('asignarpermisoLecAprend',[
        'uses' => 'Migracion\MigracionController@asignarpermisoLecAprend',
        'as' => 'asignarpermisoLecAprend'
]);

Route::get('asignarpermisoVerHR',[
        'uses' => 'Migracion\MigracionController@asignarpermisoVerHR',
        'as' => 'asignarpermisoVerHR'
]);

Route::get('conectarPropuestaActividades',[
        'uses' => 'Migracion\MigracionController@conectarPropuestaActividades',
        'as' => 'conectarPropuestaActividades'
]);

Route::get('copiarHonorariosDePropuesta',[
        'uses' => 'Migracion\MigracionController@copiarHonorariosDePropuesta',
        'as' => 'copiarHonorariosDePropuesta'
]);

Route::get('actualizarCategoriaEjecucion',[
        'uses' => 'Migracion\MigracionController@actualizarCategoriaEjecucion',
        'as' => 'actualizarCategoriaEjecucion'
]);


Route::get('permisosLideres',[
        'uses' => 'Migracion\MigracionController@permisosLideres',
        'as' => 'permisosLideres'
]);

Route::get('registrarPropuestaDeProyecto',[
    'uses' => 'Migracion\MigracionController@registrarPropuestaDeProyecto',
    'as' => 'registrarPropuestaDeProyecto'
]);

Route::get('actualizarCodigoPropuesta',[
    'uses' => 'Migracion\MigracionController@actualizarCodigoPropuesta',
    'as' => 'actualizarCodigoPropuesta'
]);


  /* Fin Migracion */


  /* Reportes */

Route::get('reporteProyectos',[
        'uses' =>'ReporteController@listadoReportesProyecto',
        'as' => 'reporteProyectos'
        ]);

Route::get('reportePropuestas',[
        'uses' =>'ReporteController@listadoReportesPropuestas',
        'as' => 'reportePropuestas'
        ]);
Route::get('reporteControlDocumentario',[
        'uses' =>'ReporteController@listadoReportesControlDocumentario',
        'as' => 'reporteControlDocumentario'
        ]);

Route::get('reporteGestionHumana',[
        'uses' =>'ReporteController@listadoReportesGestionHumana',
        'as' => 'reporteGestionHumana'
        ]);

Route::get('cargarReporteGestionHumana/{id}',[
        'uses' =>'ReporteController@cargarReporteGestionHumana',
        'as' => 'cargarReporteGestionHumana'
        ]);


Route::get('verClientePropuesta/{id}',[
        'uses' =>'ReporteController@verClientePropuesta',
        'as' => 'verClientePropuesta'
        ]);

Route::get('verGerenteProyecto/{id}',[
        'uses' =>'ReporteController@verGerenteProyecto',
        'as' => 'verGerenteProyecto'
        ]);

Route::get('verProyecto/{id}',[
        'uses' =>'ReporteController@verProyecto',
        'as' => 'verProyecto'
        ]);

Route::get('verGerenteCDocum/{id}',[
        'uses' =>'ReporteController@verGerenteCDocum',
        'as' => 'verGerenteCDocum'
        ]);

Route::get('verProyectoCDocum/{id}',[
        'uses' =>'ReporteController@verProyectoCDocum',
        'as' => 'verProyectoCDocum'
        ]);

Route::get('asignarCC',[
        'uses' => 'Migracion\MigracionController@asignarCC',
        'as' => 'asignarCC'
]);



  /* Fin Reportes */
Route::get('printRendicion/{id}',[
    'uses' => 'ReporteController@printRendicion',
    'as' => 'printRendicion'
]);

Route::get('printPropuesta',[
    'uses' => 'ReporteController@printPropuesta',
    'as' => 'printPropuesta'
]);

Route::get('printProyecto',[
    'uses' => 'ReporteController@printProyecto',
    'as' => 'printProyecto'
]);

Route::get('printCdocumentario',[
    'uses' => 'ReporteController@printCdocumentario',
    'as' => 'printCdocumentario'
]);

Route::get('printGestionHumana',[
    'uses' => 'ReporteController@printGestionHumana',
    'as' => 'printGestionHumana'
]);


/******  Master ******/

Route::get('verNotificaciones',[
        'uses' => 'TimeSheet\TimeSheetController@verNotificaciones',
        'as' => 'verNotificaciones'
]);

/******* Capacitaciones ****/
Route::get("capacitacion", 'CapacitacionController@index');
Route::get("capacitacion/{id}/aprobar", 'CapacitacionController@capacitacion_aprobar');
Route::get("capacitacion/{id}/editarEvaluacion", "CapacitacionController@editarEvaluacion");
Route::get("capacitaciones", 'CapacitacionController@capacitaciones');
Route::get("capacitaciones/propuestas", 'CapacitacionController@capacitacionesRegistradas');
Route::get("capacitaciones/{id}/usuarios", 'CapacitacionController@capacitaciones_usuarios');
Route::get("capacitaciones/{id}/usuarios/evaluados", 'CapacitacionController@capacitaciones_usuarios_evaluados');
Route::get("capacitaciones/{id}/ver", 'CapacitacionController@verCapacitacion');
Route::get("evaluacion/rendir/{i}", 'EvaluacionController@rendir');
Route::get("evaluacion/ver/{i}", 'CapacitacionController@ver');
Route::get("evaluacion/ver/{i}/video/{numero}", 'CapacitacionController@ver');
Route::get("evaluacion/{id}/cancelar", "CapacitacionController@evaluacion_cancelar");
Route::get("evaluacion/{id}/registrar", 'CapacitacionController@evaluacion_form');
Route::get("obtenerAprobadores", 'CapacitacionController@obtenerAprobadores');
Route::get("obtenerInfoCapacitacion/{id}", "CapacitacionController@obtenerInfoCapacitacion");
Route::get("obtenerPersonas", 'CapacitacionController@obtenerPersonas');
Route::post("capacitacion", 'CapacitacionController@procesar');
Route::post("evaluacion/rendir/{i}", 'EvaluacionController@rendir_verificar');
Route::post("evaluacion/{id}/registrar", "CapacitacionController@evaluacion_registrar");
Route::get("obtenerPreguntasEvaluacion/{id}", "CapacitacionController@obtenerPreguntasEvaluacion");
Route::get("capacitacion/{id}/retirar/usuario/{usuario_id}", "CapacitacionController@retirarUsuario");
Route::get("obtenerPlantilla/{codigo}", "CapacitacionController@obtenerPlantilla");

/***** Planificación ******/
Route::get("planificacion/horas", "Proyecto\PlanificacionController@index");
Route::get("planificacion/obtenerProyectos", "Proyecto\PlanificacionController@obtenerProyectos");
Route::get("planificacion/obtenerUsuariosdeProyecto/{id}", "Proyecto\PlanificacionController@obtenerUsuariosdeProyecto");
Route::get("planificacion/obtenerUsuariosDeArea/{id}", "Proyecto\PlanificacionController@obtenerUsuariosDeArea");
Route::get("planificacion/obtenerUltimasActualizaciones/{id}", "Proyecto\PlanificacionController@obtenerUltimasActualizaciones");
Route::post("planificacion/guardar/horas", "Proyecto\PlanificacionController@guardarHoras");
Route::get("planificacion/obtenerLogsdeProyecto/{id}", "Proyecto\PlanificacionController@obtenerLogsdeProyecto");
Route::get("planificacion/resumen", "Proyecto\PlanificacionController@resumen");
Route::get("planificacion/resumen/panoramico/{periodo?}/{semana?}", "Proyecto\PlanificacionResumenController@resumen_panoramico");
Route::post("planificacion/resumen/panoramico/guardar", "Proyecto\PlanificacionResumenController@guardar");
Route::post("planificacion/resumen/panoramico/agregarPersona", "Proyecto\PlanificacionResumenController@agregarPersona");
Route::get("planificacion/horas_totales", "Proyecto\PlanificacionController@horas_totales");
// Route::get("planificacion/obtenerUsuariosDeGerencia/{id}", "Proyecto\PlanificacionController@obtenerUsuariosDeGerencia");
/***** planificación diaria *****/ 
Route::get("planificacion/horas/diarias", "Proyecto\PlanificacionDiariaController@diarias");
Route::get("planificacion/obtenerActividades/{cproyecto}", "Proyecto\PlanificacionDiariaController@obtenerActividades");
Route::get("planificacion/obtenerUsuariosdeProyecto/diarias/{cproyecto}", "Proyecto\PlanificacionDiariaController@obtenerUsuariosdeProyecto");
Route::post("planificacion/guardar/horas/diarias", "Proyecto\PlanificacionDiariaController@guardarHoras");
Route::get("planificacion/obtenerUsuariosDeArea/diarias/{id}", "Proyecto\PlanificacionDiariaController@obtenerUsuariosDeArea");
Route::get("planificacion/resumen/diaria", "Proyecto\PlanificacionDiariaController@resumen");

Route::get("test", "TestController@index"); //test temp*

/***** Propuesta ******/
Route::get("invitacion/registrar", "Propuestav2\InvitacionController@form");
Route::post("invitacion", "Propuestav2\InvitacionController@formPost");
Route::get("invitaciones", "Propuestav2\InvitacionController@index");
Route::get("invitacion/obtener", "Propuestav2\InvitacionController@obtener");
Route::get("obtenerInvitacion/{id}", "Propuestav2\InvitacionController@obtenerInvitacion");

Route::get("prepropuesta/registrar", "Propuestav2\PrepropuestaController@form");
Route::post("prepropuesta", "Propuestav2\PrepropuestaController@formPost");
Route::get("prepropuestas", "Propuestav2\PrepropuestaController@index");
Route::get("prepropuesta/obtener", "Propuestav2\PrepropuestaController@obtener");
Route::get("obtenerPrepropuesta/{id}", "Propuestav2\PrepropuestaController@obtenerPrepropuesta");

Route::get("propuesta/registrar", "Propuestav2\PropuestaController@form");
Route::post("propuesta", "Propuestav2\PropuestaController@formPost");
Route::get("propuestas", "Propuestav2\PropuestaController@index");
Route::get("propuestas/obtener", "Propuestav2\PropuestaController@obtener");

Route::get("propuesta/actividades", "Propuestav2\ActividadesController@form");
Route::post("propuesta/actividades", "Propuestav2\ActividadesController@formPost");

Route::get("obtenerTagEstructura", "TagController@obtenerTagEstructura");
Route::get("obtenerTagServicioGrupal", "TagController@obtenerTagServicioGrupal");
Route::get("obtenerTagServicio/{id?}", "TagController@obtenerTagServicio");

//v3
Route::get("propuestas/inicio", "Propuestav3\PropuestaController@inicio");
Route::post("propuestas/inicio", "Propuestav3\PropuestaController@inicioPost");
Route::get("propuestas/planificacion", "Propuestav3\PropuestaController@planificacion");
Route::post("propuestas/planificacion", "Propuestav3\PropuestaController@planificacionPost");
Route::get("propuestas/ejecucion", "Propuestav3\PropuestaController@ejecucion");
Route::get("propuestas/listas", "Propuestav3\PropuestaController@listas");
Route::get("propuestas/obtenerLista", "Propuestav3\PropuestaController@obtenerLista");

//participacion 
Route::get("participacion", "ParticipacionController@index");
Route::get("obtenerListadoParticipacion", "ParticipacionController@obtenerListadoParticipacion");

// Listado
Route::get("verCliente","Propuesta\InvitacionController@verCliente");
Route::get("minera/{idcliente}","Propuesta\InvitacionController@minera");
Route::get("contacto/{idminera}","Propuesta\InvitacionController@contacto");

// Invitación 
Route::get("invitacion","Propuesta\InvitacionController@invitacion");
Route::post("grabarinvitacion","Propuesta\InvitacionController@grabarinvitacion");
Route::get("getEdit/{id}","Propuesta\InvitacionController@getEdit");
// Preliminar
Route::get("preliminar","Propuesta\PreliminarController@preliminar");
Route::post("grabarpreliminar","Propuesta\PreliminarController@grabarpreliminar");
Route::get("generarPre/{id}","Propuesta\PreliminarController@generarPre");
// Propuesta
Route::get("propuesta","Propuesta\PropuestasController@propuesta");
Route::post("grabarpropuesta","Propuesta\PropuestasController@grabarpropuesta");
Route::get("generarPro/{id}","Propuesta\PropuestasController@generarPro");

// Route::get("ejecucion", "Propuesta\PropuestasController@ejecucion");
Route::get("equipo/{cpropuesta}", "Propuesta\EjecucionController@equipo");
Route::get("disciplinas/{cpropuesta}", "Propuesta\EjecucionController@disciplinas");
Route::get("tag", "Propuesta\EjecucionController@tag");
Route::get("estructura", "Propuesta\EjecucionController@estructura");
Route::post("grabarpropuestaequipo","Propuesta\EjecucionController@grabarpropuestaequipo");

//Menu Opciones. Por vista intermedia, para refrescar pantalla y luego obtener contenido por ajax
Route::get("int","IntermedioController@index");

//EDT gráfico
Route::get("edt/grafico","Edt\EdtController@grafico");
Route::get("edt/obtener/{cproyecto}","Edt\EdtController@obtener");
Route::post("edt/guardarHijo","Edt\EdtController@guardarHijo");
Route::post("edt/eliminarElemento","Edt\EdtController@eliminarElemento");


/**************************************************************************
    * Evaluacion de Desempeño
***************************************************************************/



Route::get("colaboradorusuario","Migracion\SincronizadorController@list");

//funcionalidad trabajando
Route::get("funcionalidad_trabajando","MainController@funcionalidad_trabajando");

// Propuesta Ejecutar Opciones
Route::get("propuesta/{propuesta_id}/ejecutar","Propuesta\EjecutarController@ejecutar");
Route::get("propuesta/{propuesta_id}/ejecutar/actividades","Propuesta\EjecutarController@actividades");
Route::get("propuesta/{propuesta_id}/ejecutar/disciplinas","Propuesta\EjecutarController@disciplinas");
Route::get("propuesta/{propuesta_id}/ejecutar/equipo","Propuesta\EjecutarController@equipo");
Route::get("propuesta/{propuesta_id}/ejecutar/gastos","Propuesta\EjecutarController@gastos");
Route::get("propuesta/{propuesta_id}/ejecutar/gastosLaboratorios","Propuesta\EjecutarController@gastosLaboratorios");
Route::get("propuesta/{propuesta_id}/ejecutar/horas","Propuesta\EjecutarController@horas");
Route::get("propuesta/{propuesta_id}/ejecutar/resumenGlobal","Propuesta\EjecutarController@resumenGlobal");
Route::get("propuesta/{propuesta_id}/ejecutar/resumenHH","Propuesta\EjecutarController@resumenHH");
Route::get("propuesta/{propuesta_id}/ejecutar/tags","Propuesta\EjecutarController@tags");

Route::post("propuesta/{propuesta_id}/ejecutar/actividadesGuardar","Propuesta\EjecutarController@actividadesGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/disciplinasGuardar","Propuesta\EjecutarController@disciplinasGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/equipoGuardar","Propuesta\EjecutarController@equipoGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/gastosGuardar","Propuesta\EjecutarController@gastosGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/gastosLabGuardar","Propuesta\EjecutarController@gastosLabGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/horasGuardar","Propuesta\EjecutarController@horasGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/resumenGlobalGuardar","Propuesta\EjecutarController@resumenGlobalGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/resumenHHGuardar","Propuesta\EjecutarController@resumenHHGuardar");
Route::post("propuesta/{propuesta_id}/ejecutar/tagsGuardar","Propuesta\EjecutarController@tagsGuardar");

Route::get("propuesta/ejecutar/obtenerActividades","Propuesta\EjecutarController@obtenerActividades");
Route::get("propuesta/{propuesta_id}/ejecutar/obtenerHorasHH","Propuesta\EjecutarController@obtenerHorasHH");
Route::get("propuesta/{propuesta_id}/ejecutar/obtenerActividadesActuales","Propuesta\EjecutarController@obtenerActividadesActuales");
Route::get("propuesta/{propuesta_id}/ejecutar/obtenerGastos","Propuesta\EjecutarController@obtenerGastos");
Route::get("propuesta/{propuesta_id}/ejecutar/obtenerGastosLab","Propuesta\EjecutarController@obtenerGastosLab");
Route::get("propuesta/{propuesta_id}/ejecutar/obtenerInformacionMatrizHoras","Propuesta\EjecutarController@obtenerInformacionMatrizHoras");

// Propuesta - tabla horas para la opcion horas

Route::get("propuesta/{propuesta_id}/tabla/obtenerTablaHoras", "Propuesta\EjecutarController@obtenerTablaHoras");
Route::post("propuesta/{propuesta_id}/tabla/guardarTablaHoras", "Propuesta\EjecutarController@guardarTablaHoras");
Route::post("propuesta/{propuesta_id}/tabla/grabarCelda", "Propuesta\EjecutarController@grabarCelda");

// Propuesta - generar revision

Route::get("propuesta/{propuesta_id}/revision/generar", "Propuesta\RevisionController@generar");

// mantenimiento - item
Route::post("item/gasto/registrar", "Mantenimientos\ItemController@gasto_registrar");

/*********************  Inicio Tranmisttal  *********************/

Route::get("listaentregablestr", "Cdocumentario\TransmittalController@listado_transmittal_documento");
Route::get("listarProyectosTr", "Cdocumentario\TransmittalController@listarProyectosTr");
Route::get("obtenerEntregablesProyecto/{idProyecto}", "Cdocumentario\TransmittalController@obtenerEntregablesProyecto");
Route::get("listaentregablestr/{idProyecto}", "Cdocumentario\TransmittalController@cargar_transmittal_proyecto_seleccionado");
Route::get("listaentregablestr/{idProyecto}/vertransmittal/{tipo}/{transmittal}", "Cdocumentario\TransmittalController@ver_transmittal_proyecto");
Route::get("generar_transmittal_nuevo/{idProyecto}", "Cdocumentario\TransmittalController@generar_transmittal_nuevo");
Route::get("obtenerMotivoCombo", "Cdocumentario\TransmittalController@obtenerMotivoCombo");
Route::get("obtenerRevisionesCombo", "Cdocumentario\TransmittalController@obtenerRevisionesCombo");
Route::post("generar_transmittal_nuevo/{idProyecto}/transmittalGuardar", "Cdocumentario\TransmittalController@transmittal_guardar");
Route::get("obtenerListaClientes/{idProyecto}/{transmittal}/{tipo}", "Cdocumentario\TransmittalController@obtenerListaClientes");
Route::post("guardar_contacto", "Cdocumentario\TransmittalController@guardar_contacto");
Route::get("obtenerListaTRProyecto/{idProyecto}", "Cdocumentario\TransmittalController@obtenerListaTRProyecto");
Route::get("generar_recepcion_transmittal/{idProyecto}", "Cdocumentario\TransmittalController@generar_recepcion_transmittal");
Route::get("listarTodosEntregables/{idProyecto}/{vista}/{ent_grupo}", "Cdocumentario\TransmittalController@listarTodosEntregables");
Route::get("listarTRporEntregables/{cPryEnt}", "Cdocumentario\TransmittalController@listarTRporEntregables");
Route::get("buscaTRporNumero/{numero}", "Cdocumentario\TransmittalController@buscaTRporNumero");
Route::get("generarNumeroTR/{idProyecto}/{tipo}/{estado}", "Cdocumentario\TransmittalController@generarNumeroTR");
Route::get("imprimirTransmittal/{ctransmittal}", "Cdocumentario\TransmittalController@imprimirTransmittal"); 
Route::get("listaentregablestlogs", "Cdocumentario\TransmittalController@listado_transmittal_logs");
Route::get("listaentregablestlogs/{idProyecto}", "Cdocumentario\TransmittalController@cargar_logs_proyecto_seleccionado");
Route::get("verReportesLogs/{tipo}", "Cdocumentario\TransmittalController@verReportesLogs");
Route::post("eliminarDetalleTR","Cdocumentario\TransmittalController@eliminarDetalleTR");
Route::get("actualizarTablaDetalleTR/{ctransmittal}","Cdocumentario\TransmittalController@actualizarTablaDetalleTR");

Route::post("generar_transmittal_nuevo/{idProyecto}/transmittalEnviar", "Cdocumentario\TransmittalController@transmittal_enviar");


/***********************  Fin Tranmisttal  **********************/



Route::get("periodoevaluacion","GestionHumana\Evaluacion360Controller@periodoindex")->name('periodoindex');
Route::get("evaluacion/{idcat}","GestionHumana\Evaluacion360Controller@index")->name('indexevaluacion');
Route::get("evaluaciones/{idcab}","GestionHumana\Evaluacion360Controller@evaluacion");
Route::post("evaluacion/terminauno","GestionHumana\Evaluacion360Controller@responderevaluacion");
Route::post("evaluacion/terminados","GestionHumana\Evaluacion360Controller@responderevaluacion");
Route::post("evaluacion/finalizarencuesta","GestionHumana\Evaluacion360Controller@FinalizarEncuesta");
Route::post("evaluacion/cerrarencuesta","GestionHumana\Evaluacion360Controller@CerrarEncuesta");

Route::get("auditoriaevaluaciones", "GestionHumana\Evaluacion360Controller@AuditaDataEvaluaciones");
Route::get("auditoriaevaluaciones/{evaluador}", "GestionHumana\Evaluacion360Controller@AuditaDataEvaluaciones");


/**************************************************************************
    * Solicitud de Vacaciones
***************************************************************************/

Route::get('vacaciones','GestionHumana\VacacionesController@index')->name('vacacionesindex');
Route::post("vacaciones/solicitud","GestionHumana\VacacionesController@SolicitudVacaciones");
Route::get('vacacionesuccess','GestionHumana\VacacionesController@AprueboSolicitudVacaciones')->name('aprueboindex');  
Route::post("vaca/aprobadas","GestionHumana\VacacionesController@CambioStatusVacaciones");
Route::get('pdfvacaciones/aprobadasvaca/{id}','GestionHumana\VacacionesController@PrintPdfVacaciones')->name('imprimirpdfvaca');    

 

/**************************************************************************
    * MIgracion de Datos SCP
***************************************************************************/

Route::get('migracionGG',"Migracion\MigracionController@MigracionHomologaDataIndex");
Route::post('migracion/datos',"Migracion\MigracionController@MigracionHomologaData");
Route::get('ejecucion',"Migracion\MigracionController@ejecucion");


/**************************************************************************
    * Performance
***************************************************************************/

Route::get('performance',[
    'uses' => 'Proyecto\PerformanceController@listarprojectgeneral',
    'as' => 'performance'
]);

 Route::get('getAreas/{id}',[
        'uses' => 'Proyecto\PerformanceController@getAreas',
        'as' => 'getAreas' ]
        );

 Route::get('getActividades/{id}/{disciplina}',[
        'uses' => 'Proyecto\PerformanceController@getActividades',
        'as' => 'getActividades' ]
        );

  Route::post('guardar_porcentaje',[
        'uses' => 'Proyecto\PerformanceController@guardar_porcentaje',
        'as' => 'guardar_porcentaje' ]
        );


 Route::get('getCabezera/{id}/{disciplina}',[
    
        'uses' => 'Proyecto\PerformanceController@getCabezera',
        'as' => 'getCabezera' ]
        );


/**************************************************************************
    * Solicitud de horas extras
***************************************************************************/
Route::get('horasextras','GestionHumana\HorasExtrasController@index')->name('horasextrasindex');
Route::post("horasextras/solicitud","GestionHumana\HorasExtrasController@SolicitudHorasExtras");

Route::get('horasextrascontrol','GestionHumana\HorasExtrasController@controlindex')->name('horasextrascontrolindex');
Route::post('horasextrascontrol/config','GestionHumana\HorasExtrasController@control')->name('horasextrascontrol');

/**
    ruta editar horas extras
 */
Route::get('updateSoli/{id}','GestionHumana\HorasExtrasController@updateSoli');
Route::post('editsolicitud','GestionHumana\HorasExtrasController@editsolicitud');

/****
 * fin de ruta
*/


Route::get('horasextrassuccess','GestionHumana\HorasExtrasController@AprueboSolicitudHorasExtras')->name('aprueboindexhorasextras');  
Route::post("horasextras/aprobadasjefes","GestionHumana\HorasExtrasController@CambioStatusHorasExtras");
Route::get('pdfhorasextras/aprobadasjefes/{id}','GestionHumana\HorasExtrasController@PrintPdfHorasExtras')->name('imprimirpdfhoras');  

Route::get('controlindex','GestionHumana\HorasExtrasController@controlindex');
Route::get('controlindex','GestionHumana\HorasExtrasController@ListadoHorasExtras');

/**************************************************************************
    * Control de horas
***************************************************************************/

  Route::get('getfecha/{id}',[
        'uses' => 'Proyecto\PerformanceController@getfecha',
        'as' => 'getfecha' ]
        );

  /*Curva S - Horas*/
  Route::get('curva-s',[
    'uses' => 'Proyecto\PerformanceController@listarprojectgeneralcurva',
    'as' => 'curva-s'
]);

    Route::get('curva_s_array/{proy}',[
    'uses' => 'Proyecto\PerformanceController@getArrayPerformanceCurvas',
    'as' => 'curva-s'
]);


/*********************  Inicio Reporte Gasto  *********************/

Route::get("reportedegastos", "Gastos\ReporteGastos@reporteGastos");
Route::get("verGastosPorProyecto", "Gastos\ReporteGastos@verGastosPorProyecto");


/***********************  Fin Reporte Gasto  **********************/



/**************************************************************************
 * Cargabilidad Semanal
 ***************************************************************************/

Route::get("carga-semanal", "Proyecto\cargaSemanalController@verAreas");
Route::get("getproyectos", "Proyecto\cargaSemanalController@getproyectos");

// Grafico Semanal
Route::get("grafico-semanal", "Proyecto\cargaSemanalController@verAreasGrafico");

// Grafico de n semanas
Route::get("graficosemana", "Proyecto\cargaSemanalController@graficosemana");
// Grafico de n semanas
Route::get("grafico3semanas", "Proyecto\cargaSemanalController@grafico3semanas");
// Grafico de FAC NOFAC ADM por usuarios
Route::get("grafico_porusuario", "Proyecto\cargaSemanalController@grafico_porusuarios");
// Grafico de no facturables
Route::get("graficnofacturable", "Proyecto\cargaSemanalController@grafico_nofacturable");
// Grafico admininistrativas
Route::get("graficoadmin", "Proyecto\cargaSemanalController@grafico_admin");
// Grafico proyectointernos
Route::get("graficointernos", "Proyecto\cargaSemanalController@grafico_internos");
// Grafico Productividad Y Cargabilidad
Route::get("graficosemanalCargaProd", "Proyecto\cargaSemanalController@grafico_productividadCargabilidad");



Route::get("indicador-semanal", "Proyecto\cargaSemanalController@verAreasIndicadores");
Route::get("listaActividadesNOFAC", "Proyecto\cargaSemanalController@getActividadesNOFAC");

Route::get("indicador-semanal-general", "Proyecto\cargaSemanalController@verAreasPersonalIndicadores");
Route::get("listaActividadespersonalFACNOFACAND", "Proyecto\cargaSemanalController@getFACNOFACADMIN");


  /**************************************************************************
     * Cargabilidad Mensual
  ***************************************************************************/

Route::get("carga-mensual", "CargaMensual\cargaMensualProyectosController@verAreas");
Route::get("listaProyectosCargaMensual", "CargaMensual\cargaMensualProyectosController@getproyectosCM");

// Grafico Mensual

 Route::get("grafico-mensual", "CargaMensual\cargaMensualGraficosController@verAreasGraficoMensual");

 Route::get("graficomensual", "CargaMensual\cargaMensualGraficosController@graficoMensual");
 Route::get("grafico3meses", "CargaMensual\cargaMensualGraficosController@grafico3Mensual");
 Route::get("graficomensualporusuario", "CargaMensual\cargaMensualGraficosController@graficoMensual_porusuarios");
 Route::get("graficomensualnofacturable", "CargaMensual\cargaMensualGraficosController@graficomensual_nofacturable");
 Route::get("graficomensualadmin", "CargaMensual\cargaMensualGraficosController@graficomensual_admin");
 Route::get("graficomensualinternos", "CargaMensual\cargaMensualGraficosController@graficomensual_internos");
 Route::get("graficomensualproductividadCargabilidad", "CargaMensual\cargaMensualGraficosController@getCM_ProductividadCargabilidad");

Route::get("indicador-mensual", "CargaMensual\cargaMensualANFController@verAreasIndicadores");
Route::get("listaActividadesmensualNOFAC", "CargaMensual\cargaMensualANFController@getActividadesMensualNOFAC_ADM");

Route::get("indicador-mensual-general", "CargaMensual\cargaMensualANFController@verAreasMensualIndicadoresGeneral");
Route::get("cargabilidadproductividadfinal", "CargaMensual\cargaMensualANFController@getCM_ProductividadCargabilidad");


/**************************************************************************
 * Control de Horas - HH by Me
 ***************************************************************************/
//Barra de Opciones
Route::get("control-horas", "ControldeHoras\controldeHorasController@listadeProyectos");
Route::get("obtenerAreasCCHH", "ControldeHoras\controldeHorasController@obtenerAreasCCHH");
Route::get("getfechaCCHH", "ControldeHoras\controldeHorasController@getfecha");
Route::get("obtenerSOCProyectos", "ControldeHoras\controldeHorasController@obtenerSOCProyectos");

Route::get("getActividadesCCHH", "ControldeHoras\controldeHorasController@getProyectos_con_ActividadesCCHH");
Route::get("mostrarAreaporCadaActividad", "ControldeHoras\controldeHorasController@getMostrarTodaslasAreasporActividad");
Route::get("mostrarAreaporCadaActividadCCHH", "ControldeHoras\controldeHorasController@getMostrarTodaslasAreasporActividadCCHH");
// Resumen a nivel Proyecto
Route::get("mostrarResumenProyecto", "ControldeHoras\controldeHorasController@getMostrarResumenProyecto");
Route::get("mostrarResumenMontoProyecto", "ControldeHoras\controldeHorasController@getmostrarResumenMontoProyecto");

// Resumen de Control de horas
Route::get("resumen-cchh", "ControldeHoras\controldeHorasController@listadeAreasresumenCCHH");
Route::get("getResumenCCHH", "ControldeHoras\controldeHorasController@getProyectosResumenCCHH");


// Resumen del consolidado de horas
Route::get("mostrarResumenHorasConsumidas-Presupuestadas-ProyectoSOC", "ControldeHoras\controldeHorasController@getMostrarResumenProyectoSOC");
Route::get("mostrarResumenMontoConsumidas-Presupuestadas-ProyectoSOC", "ControldeHoras\controldeHorasController@getMostrarResumenMontoProyectoSOC");


});

//Creo actualizador get de listado proyecto
Route::get('ActualizaListaProyecto',[
        'uses' => 'Proyecto\ProyectoController@ActualizaListaProyecto',
    'as' => 'ActualizaListaProyecto'
]);

Route::get('obtener_senior/{id}','Proyecto\ProyectoController@obtenerSenior');

//Solicitud Requerimiento

Route::get("logistica/{id?}", "Logistica\LogisticaController@index")->name('logistica.index');
//Route::get("logisticaver/{id}", "Logistica\LogisticaController@PuenteVerId")->name('logisticaver.index');
Route::post("logisticacreate", "Logistica\LogisticaController@CreoSolicitud");
Route::get("listalogistica", "Logistica\LogisticaController@listaindex")->name('listadologistica.index');;
Route::get("logisdeleterow/{id}", "Logistica\LogisticaController@deleteitemtablaarticulo");
Route::get("logisticambiostatus/{id}", "Logistica\LogisticaController@logisticambiostatus");
Route::post("AprobacionesSolicitud", "Logistica\LogisticaController@AprobacionesSolicitud");

/**************************************************************************
 * Master Proyectos GCE
 ***************************************************************************/

Route::get("master-proyecto", "MasterProyectos\ListaProyectosController@listadeProyectos");
Route::get("master-proyecto-privilegios", "MasterProyectos\ListaProyectosController@listadoSecciones");
Route::get("capturarProyectoMaster", "MasterProyectos\ListaProyectosController@CapturarProyectoMaster");
Route::post("guardardataFianzaSeguro", "MasterProyectos\ListaProyectosController@guardardataFianzaSeguro")->name('fianza_Seguro.store');
Route::post("guardardataMonitoreo", "MasterProyectos\ListaProyectosController@guardardataMonitoreo")->name('monitoreo.store');
Route::post("guardardataPlanificacion", "MasterProyectos\ListaProyectosController@guardardataPlanificacion")->name('planificacion.store');
Route::post("guardardataCierre", "MasterProyectos\ListaProyectosController@guardardataCierre")->name('cierre.store');
Route::post("guardardataContrato", "MasterProyectos\ListaProyectosController@guardardataContrato")->name('contrato.store');

// Permisos

Route::get('listarUsuariomaster',[
        'uses' =>'MasterProyectos\ListaProyectosController@listarUsuario',
        'as' => 'listarUsuario'
    ]
);

Route::get('viewUsuarioMaster/{id}',[
        'uses' =>'MasterProyectos\ListaProyectosController@viewUsuario',
        'as' => 'viewUsuario'
    ]
);

Route::post("guardar_seccion", "MasterProyectos\ListaProyectosController@guardar_seccion")->name('guardarseccion.store');
Route::post("guardar_seccion_radio", "MasterProyectos\ListaProyectosController@guardar_seccion_radio")->name('guardar_seccion_radio.store');

// Listado de Proyectos
Route::get("listadobd-proyecto", "MasterProyectos\ListaProyectosController@listadoBDProyectos");

Route::get("reporteht", "TimeSheet\ReporteHTController@index"); 
// Route::get("obtnerdatosht/{cproyecto}/{persona}/{tipoact}/{fdesde}/{fhasta}/{condicionoperativa}/{dividir_semanas}", "TimeSheet\ReporteHTController@obtnerdatosht"); 
Route::get("obtnerdatosht", "TimeSheet\ReporteHTController@obtnerdatosht"); 
Route::get("obtenerrangosemanas", "TimeSheet\ReporteHTController@obtenerrangosemanas"); 

