<?php

namespace App\Http\Middleware;

use DB;
use Closure;
use Route;
use Auth;
use Session;

use App\Models\Tpersona;


class FilterUrlMiddleware
{


    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */    

    public function handle($request, Closure $next)
    {
        //return redirect()->route('panel');
        //dd(substr(Route::getCurrentRoute()->getPath(),0,strpos(Route::getCurrentRoute()->getPath(),"/") ));
        if(strpos(Route::getCurrentRoute()->getPath(),"panel")===FALSE || strpos(Route::getCurrentRoute()->getPath(),"construccion")===FALSE ){

            

            $menu = DB::table('tmenu')
            ->where('ruta','=',substr(Route::getCurrentRoute()->getPath(),0,strpos(Route::getCurrentRoute()->getPath(),"/")))
            ->first();
            if($menu){
                $menupersona = DB::table('tmenupersona')
                ->where('cpersona','=',Auth::user()->cpersona)
                ->where('menuid','=',$menu->menuid)
                ->where('permiso','=','01')
                ->first();
                if(!$menupersona){
                    return redirect()->route('construccion');
                }
            }
            
        }
        $menu = array();
        Session::put("menu",$menu);
        //omitido para utilizar página completa - capacitaciones
        //if(!(strpos(Route::getCurrentRoute()->getPath(),"panel")===FALSE)){
            $menu = $this->createMenu();
            $menupersona = DB::table('tmenupersona')
            ->where('cpersona','=',Auth::user()->cpersona)
            ->where('permiso','=','01')
            ->get();
            foreach($menupersona as $men){
               $menu[$men->menuid]='1';
            }

            Session::put("menu",$menu);
        //}

        //verificar capacitaciones por aprobar
        $aprobaciones = DB::table('capacitacion')
            ->join('capacitacion_aprobacion', 'capacitacion_aprobacion.capacitacion_id', '=', 'capacitacion.id')            
            ->select('capacitacion.*')
            ->where("capacitacion_aprobacion.estado", "=", 1)
            ->where("capacitacion_aprobacion.usuario_id", "=", Auth::user()->cusuario)
            ->get();
        $aprobaciones_cantidad = count($aprobaciones);        
        Session::put("aprobaciones_cantidad",$aprobaciones_cantidad);

        //verificar capacitaciones por revisar
        $fecha = date("Y-m-d H:i:s");        
        $capacitaciones = DB::table('capacitacion_persona')
            ->join('capacitacion', 'capacitacion_persona.capacitacion_id', '=', 'capacitacion.id')            
            ->select('capacitacion.*')
            ->where("capacitacion.estado", "=", 1)
            ->where("capacitacion_persona.estado", "=", 1)
            ->where("capacitacion_persona.usuario_id", "=", Auth::user()->cusuario)
            ->where("capacitacion.fecha_inicio", "<=", $fecha)
            ->get();
        $capacitaciones_cantidad = count($capacitaciones);
        Session::put("capacitaciones_cantidad",$capacitaciones_cantidad);

        //var_dump($capacitaciones_cantidad);
        //var_dump($aprobaciones_cantidad);
        //return;

        return $next($request);
        
    }

    public function createMenu(){
        $menu['01']='0';
        $menu['0101']='0';
        $menu['0102']='0';
        $menu['0103']='0';
        $menu['02']='0';
        $menu['0201']='0';
        $menu['0202']='0';
        $menu['0203']='0';
        $menu['0204']='0';
        $menu['0205']='0';
        $menu['0206']='0';
        $menu['0207']='0';
        $menu['0208']='0';
        $menu['0209']='0';
        $menu['0210']='0';
        $menu['0211']='0';
        $menu['0212']='0';
        $menu['0213']='0';
        $menu['0214']='0';
        $menu['0215']='0';
        $menu['0216']='0';
        $menu['0217']='0';
        $menu['0218']='0';
        $menu['0219']='0';
        $menu['0220']='0';
        $menu['0221']='0';
        $menu['0222']='0';
        $menu['03']='0';
        $menu['0301']='0';
        $menu['0302']='0';
        $menu['0303']='0';
        $menu['0304']='0';
        $menu['0305']='0';
        $menu['0306']='0';
        $menu['0307']='0';
        $menu['0308']='0';
        $menu['0309']='0';
        $menu['0310']='0';
        $menu['0311']='0';
        $menu['0312']='0';
        $menu['0313']='0';
        $menu['0314']='0';
        $menu['0315']='0';
        $menu['0316']='0';
        $menu['0317']='0';
        $menu['0318']='0';
        $menu['0319']='0';
        $menu['0320']='0';
        $menu['0321']='0';
        $menu['0322']='0';
        $menu['0323']='0';
        $menu['0324']='0';
        $menu['0325']='0';
        $menu['0326']='0';
        $menu['0327']='0';
        $menu['0328']='0';
        $menu['0329']='0';
        $menu['0330']='0';
        $menu['0331']='0';
        $menu['0332']='0';
        $menu['0333']='0';
        $menu['0334']='0';
        $menu['0335']='0';
        $menu['0336']='0';
        $menu['0337']='0';
        $menu['0338']='0';
        $menu['0339']='0';
        $menu['0340']='0';
        $menu['0341']='0';
        $menu['0342']='0';
        $menu['0343']='0';
        $menu['0344']='0';
        $menu['0345']='0';
        $menu['0346']='0';
        $menu['0347']='0';
        $menu['0348']='0';
        $menu['0349']='0';
        $menu['0350']='0';
        $menu['0351']='0';
        $menu['0352']='0';
        $menu['0353']='0';
        $menu['0354']='0';
        $menu['0355']='0';
        $menu['0356']='0';
        $menu['0357']='0';
        $menu['0358']='0';
        $menu['0359']='0';
        $menu['0360']='0';
        $menu['0361']='0';
        $menu['0362']='0';
        $menu['0363']='0';
        $menu['0364']='0';
        $menu['0365']='0';
        $menu['0366']='0';
        $menu['0367']='0';
        $menu['0368']='0';
        $menu['0369']='0';
        $menu['0370']='0';
        $menu['0371']='0';
        $menu['0372']='0';
        $menu['0373']='0';
        $menu['0374']='0';
        $menu['04']='0';
        $menu['0401']='0';
        $menu['0402']='0';
        $menu['0403']='0';
        $menu['0404']='0';
        $menu['0405']='0';
        $menu['0406']='0';
        $menu['0407']='0';
        $menu['05']='0';
        $menu['0501']='0';
        $menu['0502']='0';
        $menu['0503']='0';
        $menu['0504']='0';
        $menu['0505']='0';
        $menu['0506']='0';
        $menu['06']='0';
        $menu['0601']='0';
        $menu['0602']='0';
        $menu['0603']='0';
        $menu['0604']='0';
        $menu['0605']='0';
        $menu['0606']='0';
        $menu['0607']='0';
        $menu['0608']='0';
        $menu['0609']='0';
        $menu['0610']='0';
        $menu['0611']='0';
        $menu['07']='0';
        $menu['0701']='0';
        $menu['0702']='0';
        $menu['0703']='0';
        $menu['0704']='0';
        $menu['0705']='0';
        $menu['0706']='0';
        $menu['0707']='0';
        $menu['0708']='0';
        $menu['0709']='0';
        $menu['0710']='0';
        $menu['0711']='0';
        $menu['0712']='0';
        $menu['0713']='0';
        $menu['0714']='0';
        $menu['08']='0';
        $menu['0801']='0';
        $menu['0802']='0';
        $menu['0803']='0';
        $menu['0804']='0';
        $menu['0805']='0';
        $menu['0806']='0';
        $menu['0807']='0';
        $menu['0808']='0';
        $menu['0809']='0';
        $menu['0810']='0';
        $menu['0811']='0';
        $menu['0812']='0';
        $menu['0813']='0';
        $menu['0814']='0';
        $menu['0815']='0';
        $menu['09']='0';
        $menu['0925']='0';
        $menu['10']='0';
        $menu['1001']='0';
        $menu['1002']='0';
        return $menu;     
    }
}
