<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class PropuestaRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'propuestanombre'  => 'required',
            
            'tipopropuesta' => 'required',
            'formacotiza' => 'required',
            'servicio' => 'required',
            'moneda' => 'required',
            'mentrega' => 'required',
            /*'propuestafbases' => 'required',*/
            'propuestaobservaciones' => 'required',
            /*'presentacion' => 'required|array',
            /*'fpresentacion' => 'required',*/
            /*'fvtecnica' => 'required',*/
            /*'fconsulta' => 'required',*/
            /*'frespuesta' => 'required',*/
            /*'fabsolucionconsultas' => 'required',*/
            /*'fcharla' => 'required',*/
            'estadopropuesta' => 'required'
            //
        ];

        return $rules;
    }
}
