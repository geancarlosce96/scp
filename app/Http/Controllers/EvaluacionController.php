<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Alternativa;
use App\Models\Capacitacion;
use App\Models\Capacitacion_aprobacion;
use App\Models\Capacitacion_persona;
use App\Models\Evaluacion;
use App\Models\Evaluacion_persona;
use App\Models\Pregunta;
use App\Models\Tusuario;
use Auth;
use DB;

class EvaluacionController extends Controller
{
	//public route
	
	public function rendir($id)
	{
		$usuario_id = Auth::user()->cusuario;

		$evaluacion = Evaluacion::find($id);        
		$evaluacion->preguntas = $this->obtenerPreguntas($id, true);

		$evaluacion_persona = Evaluacion_persona::where("evaluacion_id", "=", $evaluacion->id)
			->where("usuario_id", "=", $usuario_id)->where("estado", "=", 0)->first();

		if($evaluacion_persona==null)
			return redirect("capacitaciones");

		$evaluacion_persona->estado = 1;
		$evaluacion_persona->save();

		return view("evaluacion.form", ["evaluacion" => $evaluacion]);
	}

	public function rendir_verificar(Request $request, $id)
	{
		$puntos = 0;        
		$usuario_id = Auth::user()->cusuario;
		$preguntas = $request->input("preguntas");

		//obtener evaluacion
		$e = Evaluacion::find($id);

		//obtener titulo de la capacitacion
		$capacitacion = Capacitacion::find($e->capacitacion_id);

		//registrar la puntuacion        
		$evaluacion = Evaluacion_persona::where("evaluacion_id", "=", $id)->where("usuario_id", "=", $usuario_id)->where("estado", "=", 1)->first();

		if($evaluacion == null)
			return redirect("capacitaciones");
		
		//obtener preguntas
		$e->preguntas = $this->obtenerPreguntas($id);

		//recorrer segun la cantidad de preguntas mostradas
		foreach ($preguntas as $key => $pregunta) {
			if($this->verificar($pregunta, $request->input("opc_".$pregunta)))
				$puntos++;
		}

		$msg = "";

		if($evaluacion == null)
			return "no hay evaluacion relacionada";
		   
		$evaluacion->puntaje = $puntos;        
		
		if($puntos >= $e->cantidad_preguntas_min){
			$msg = "Ud ha aprobado.";
			$evaluacion->estado = 2;
		}            
		else{
			$evaluacion->estado = 3;
			$msg = "Ud ha desaprobado.";
		}

		$evaluacion->save();

		//obtener capacitacion, actualizar capacitacion

		$capacitacion = Capacitacion::find($e->capacitacion_id);
		$capacitacion_persona = Capacitacion_persona::where("capacitacion_id", "=", $capacitacion->id)
			->where("usuario_id", "=", $usuario_id)->first();

		$capacitacion_persona->estado = 2;
		$capacitacion_persona->save();

		return view("evaluacion.mensaje", ["msg" => $msg, "evaluacion" => $e, "capacitacion" => $capacitacion->titulo]);
	}

	//functions privates

	private function aleatorioPreguntas(&$preguntas)
	{
		$cantidad_mezclas =  rand(1, 10);
		$cantidad = count($preguntas);

		for ($i=0; $i <$cantidad_mezclas; $i++) { 
			$numOrigen = rand(1, $cantidad)-1;
			$numDestino = rand(1, $cantidad)-1;

			$objTemp = $preguntas[$numOrigen];
			$preguntas[$numOrigen] = $preguntas[$numDestino];
			$preguntas[$numDestino] = $objTemp;
		}
		
	}

	private function obtenerPreguntas($id, $aleatorio = false)
	{
		$preguntas = Pregunta::where("evaluacion_id", "=", $id)->get();        
		$this->aleatorioPreguntas($preguntas);

		$i = 1;

		foreach ($preguntas as $key => $pregunta) {
			$pregunta->n = $i;
			$i++;
			$pregunta->alternativas = $this->obtenerAlternativas($pregunta->id, true);
		}

		return $preguntas;
	}

	private function obtenerAlternativas($id, $aleatorio = false)
	{
		$alternativas = Alternativa::where("pregunta_id", "=", $id)->get();
		$this->aleatorioPreguntas($alternativas);

		$i = 1;

		foreach ($alternativas as $key => $alternativa) {
			$alternativa->n = $i;
			$i++;            
		}

		return $alternativas;
	}

	private function verificar($pregunta_respondida_id, $alternativas_respondidas_id)
	{
		if($alternativas_respondidas_id == null)
			return false;

		//verificar preguntas mostradas con su respuesta, con la lista de preguntas y respuestas
		$alternativas = Alternativa::where("pregunta_id", "=", $pregunta_respondida_id)->where("correcta","=",1)->get();
		$puntos = 0;
		$puntos_total = count($alternativas);        

		if(count($alternativas_respondidas_id) > $puntos_total)
			return false;

		foreach ($alternativas_respondidas_id as $key => $alternativa_respondida) {
			foreach ($alternativas as $key => $alternativa) {                

				if($alternativa_respondida == $alternativa->id){                    
					$puntos++;
				}
			}
		}

		if($puntos_total == $puntos)
			return true;

		return false;
	}
}
