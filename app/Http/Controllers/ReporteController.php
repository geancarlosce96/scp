<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Erp\EmpleadoSupport;
use Carbon\Carbon;
use DB;
use Fpdf;
use App\Erp\RptPdf;
use App\Erp\PropuestaPdf;
use App\Erp\ProyectoPdf;
use App\Erp\CdocumentarioPdf;
use App\Erp\GhumanaPdf;
use App\Erp\TransmittalPropuestaPdf;


class ReporteController extends Controller
{
    //
    public function genPDF(){

        $pdf = \PDF::loadView('propuesta/disciplina');
        return $pdf->stream();
        //return $pdf->download('pruebapdf.pdf');
    }
	public function genPDF_2(){
		Fpdf::AddPage();
		Fpdf::SetFont('Courier','B',18);
		Fpdf::Cell(50,25,'Hola ');
		Fpdf::Output();
	}

    public function listadoReportesProyecto(){
    	$objEmp = new EmpleadoSupport(); 
    	$reporte= DB::table('tcatalogo')
    	->where('codtab','=','00031')
		->whereNotNull('digide')
		->lists('descripcion','digide');  
		$reporte = [''=>''] + $reporte;

		$formatodoc= DB::table('tcatalogo')
    	->where('codtab','=','00032')
		->whereNotNull('digide')
		->lists('descripcion','digide');  
		$formatodoc = [''=>''] + $formatodoc;

		$proyecto= DB::table('tproyecto')
		->orderBy('nombre','ASC')
    	->lists('nombre','cproyecto');      	
		$proyecto = [''=>''] + $proyecto;

        $gerente = $objEmp->getPersonalRol('GP','001','2') ;
        $gerente = [''=>''] + $gerente;     
		

    	return view('proyecto.reporteProyectos',compact('reporte','formatodoc','proyecto','gerente'));
    }

    public function verGerenteProyecto(Request $request,$idPersona){

    	$tpersona = DB::table('tpersona')
    	->where('cpersona','=',$idPersona)
		->first();		
		
    	return view('partials.verGerente',compact('tpersona'));

    }

     public function verProyecto(Request $request,$idProyecto){

    	$tproyecto = DB::table('tproyecto')
    	->where('cproyecto','=',$idProyecto)
		->first();		
		
    	return view('partials.verProyectoReporte',compact('tproyecto'));

    }

     public function verGerenteCDocum(Request $request,$idPersona){

    	$tpersona = DB::table('tpersona')
    	->where('cpersona','=',$idPersona)
		->first();		
		
    	return view('partials.verGerenteCDocum',compact('tpersona'));

    }

     public function verProyectoCDocum(Request $request,$idProyecto){

    	$tproyecto = DB::table('tproyecto')
    	->where('cproyecto','=',$idProyecto)
		->first();		
		
    	return view('partials.verProyectoCDocum',compact('tproyecto'));

    }

    public function listadoReportesPropuestas(){
    	
    	$reporte= DB::table('tcatalogo')
    	->where('codtab','=','00035')
		->whereNotNull('digide')
		->orderBy('descripcion','ASC')
		->lists('descripcion','digide');  

		$reporte = [''=>''] + $reporte;

		$formatodoc= DB::table('tcatalogo')
    	->where('codtab','=','00032')
		->whereNotNull('digide')
		->lists('descripcion','digide');  
		$formatodoc = [''=>''] + $formatodoc;

		$cliente= DB::table('tpersonajuridicainformacionbasica')
		->orderBy('razonsocial','ASC')
    	->lists('razonsocial','cpersona');      	
		$cliente = [''=>''] + $cliente;

		

    	return view('propuesta.reportePropuestas',compact('reporte','formatodoc','cliente'));
    }

    public function verClientePropuesta(Request $request,$idPersona){

    	$tpersona = DB::table('tpersona')
    	->where('cpersona','=',$idPersona)
		->first();		
		
    	return view('partials.verClientePropuesta',compact('tpersona'));

    }
    
     public function listadoReportesControlDocumentario(){

     	$objEmp = new EmpleadoSupport(); 
   		$gerente = $objEmp->getPersonalRol('GP','001','2') ;
        $gerente = [''=>''] + $gerente;

    	$reporte= DB::table('tcatalogo')
    	->where('codtab','=','00033')
		->whereNotNull('digide')
		->lists('descripcion','digide');  
		$reporte = [''=>''] + $reporte;

		$formatodoc= DB::table('tcatalogo')
    	->where('codtab','=','00032')
		->whereNotNull('digide')
		->lists('descripcion','digide');  
		$formatodoc = [''=>''] + $formatodoc;

		$proyecto= DB::table('tproyecto')
		->orderBy('nombre','ASC')
    	->lists('nombre','cproyecto');      	
		$proyecto = [''=>''] + $proyecto;

		

    	return view('cdocumentario.reporteControlDocumentario',compact('reporte','formatodoc','proyecto','gerente'));
    }

    public function listadoReportesGestionHumana(){
    	$reporte= DB::table('tcatalogo')
    	->where('codtab','=','00034')
		->whereNotNull('digide')
		->lists('descripcion','digide');  
		$reporte = [''=>''] + $reporte;

		$formatodoc= DB::table('tcatalogo')
    	->where('codtab','=','00032')
		->whereNotNull('digide')
		->lists('descripcion','digide');  
		$formatodoc = [''=>''] + $formatodoc;

		$proyecto= DB::table('tproyecto')
		->orderBy('nombre','ASC')
    	->lists('nombre','cproyecto');      	
		$proyecto = [''=>''] + $proyecto;		

		

    	return view('ghumana.reporteGestionHumana',compact('reporte','formatodoc','proyecto'));
    }

    public function cargarReporteGestionHumana(Request $request,$idEmpleado){
    	$tpersona = DB::table('tpersona')
		->where('cpersona','=',$idEmpleado)
		->first();

    	return view('partials.verEmpleadoRep',compact('tpersona'));

    }
	public function printRendicion(Request $request,$id){
		$tgastos = DB::table('tgastosejecucion as g')
		->where('cgastosejecucion','=',$id)
		->first();
		$obj= new RptPdf();
		$obj->AddPage();
		$obj->SetFont('Courier','B',18);
		$obj->Cell(50,25,'Hola ');
		$obj->Output('D');
		exit;
		
	}

	
	public function printPropuesta(Request $request){	
	    

		//Hora  y Fecha
		$fec= Carbon::now();
		$fec=$fec->format('d-m-Y');
		$hor=Carbon::now();
		$hor=$hor->format('h:i A');

		if($request->input('ireporte')=='00001'){	

			$cpersona=$request->input('cpersona');
	   
			$listaPropuesta=DB::table('tpropuesta as tp')
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gteproyecto','=','tpe.cpersona')
			->leftJoin('testadopropuesta as te','tp.cestadopropuesta','=','te.cestadopropuesta')
			->leftJoin('tcondicionesoperativas as tc','tp.ccondicionoperativa','=','tc.ccondicionoperativa')
			->leftJoin('tpropuestadisciplina as tpd','tp.cpropuesta','=','tpd.cpropuesta')
			->leftJoin('tdisciplina as td','tpd.cdisciplina','=','td.cdisciplina')
			->select('tp.cpropuesta as cpropuesta','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tc.descripcion as condicion',DB::raw('fn_getdisciplinaspropuesta(tp.cpropuesta) as disciplina'),'tp.ccodigo as codpropuesta','tu.codigo as coduminera')
			->where('tp.cpersona_gteproyecto','=',$cpersona)
			->orderBy('tp.ccodigo','ASC')
			->distinct('tp.cunidadminera')
			->get();		
			//Listado de Propuesta
			$obj= new PropuestaPdf('L');
			$obj->titulo='Listado de Propuestas';
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaPropuesta as $lp) {				

			   $obj->codProp=$lp->codpropuesta;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->disciplina=$lp->disciplina;
			   $obj->estado=$lp->estado;   
			   $obj->coperat=$lp->condicion; 				  

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hdis=(ceil((strlen($obj->disciplina)/14))*$h);
			   $hest=(ceil((strlen($obj->estado)/14))*$h);	
			   $hcop=(ceil((strlen($obj->coperat)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProp)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);
			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hdis && $hnonm>=$hest && $hnonm>=$hcop && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hdis && $humin>=$hest && $humin>=$hcop && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hdis && $hserv>=$hest && $hserv>=$hcop && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hdis && $hGP>=$hest && $hGP>=$hcop && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hdis>=$hnonm && $hdis>=$hserv && $hdis>=$hGP && $hdis>=$humin && $hdis>=$hest && $hdis>=$hcop && $hdis>=$hcprop && $hdis>=$hcuni){
					$h1=$hdis;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hcop && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hcop && $hcprop>=$hdis && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hcop && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hcop;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProp,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(52,$h,$obj->nombre,0,'L');
       		   $obj->Rect(72,38+$y,52,$h1);

       		   $obj->SetXY(124,38+$y);			     		
       		   $obj->MultiCell(22,$h,$obj->servicio,0,'L');
       		   $obj->Rect(124,38+$y,22,$h1);

			   $obj->SetXY(146,38+$y);			       		
       		   $obj->MultiCell(34,$h,$obj->GP,0,'L');
       		   $obj->Rect(146,38+$y,34,$h1);

			   $obj->SetXY(180,38+$y);			      		
       		   $obj->MultiCell(30,$h,$obj->disciplina,0,'L');
       		   $obj->Rect(180,38+$y,30,$h1);

			   $obj->SetXY(210,38+$y);			   		
       		   $obj->MultiCell(37,$h,$obj->estado,0,'L');
       		   $obj->Rect(210,38+$y,37,$h1);

			   $obj->SetXY(247,38+$y);			       		
       		   $obj->MultiCell(40,$h,$obj->coperat,0,'L');
       		   $obj->Rect(247,38+$y,40,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}
		if($request->input('ireporte')=='00002'){


			$cpersona=$request->input('cpersona');
	   
			$listaPropuesta=DB::table('tpropuesta as tp')
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gteproyecto','=','tpe.cpersona')
			->leftJoin('testadopropuesta as te','tp.cestadopropuesta','=','te.cestadopropuesta')
			->leftJoin('tcondicionesoperativas as tc','tp.ccondicionoperativa','=','tc.ccondicionoperativa')
			->leftJoin('tpropuestadisciplina as tpd','tp.cpropuesta','=','tpd.cpropuesta')
			->leftJoin('tdisciplina as td','tpd.cdisciplina','=','td.cdisciplina')
			->select('tp.cpropuesta as cpropuesta','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tc.descripcion as condicion',DB::raw('fn_getdisciplinaspropuesta(tp.cpropuesta) as disciplina'),'tp.ccodigo as codpropuesta','tu.codigo as coduminera')
			->where('tp.cpersona_gteproyecto','=',$cpersona)
			->orderBy('servicio','ASC')
			->distinct('tp.cunidadminera')
			->get();		
			//Listado de Propuesta
			$obj= new PropuestaPdf('L');
			$obj->titulo='Listado de Propuestas por Tipo de Servicio';
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;			

			$obj->AddPage();

			$y=0;
			$ys=0;
			$servicio='';
						
			foreach ($listaPropuesta as $lp) {				

			   $obj->codProp=$lp->codpropuesta;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->disciplina=$lp->disciplina;
			   $obj->estado=$lp->estado;   
			   $obj->coperat=$lp->condicion; 	

			   $h=4;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hdis=(ceil((strlen($obj->disciplina)/14))*$h);
			   $hest=(ceil((strlen($obj->estado)/14))*$h);	
			   $hcop=(ceil((strlen($obj->coperat)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProp)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hdis && $hnonm>=$hest && $hnonm>=$hcop && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hdis && $humin>=$hest && $humin>=$hcop && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hdis && $hserv>=$hest && $hserv>=$hcop && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hdis && $hGP>=$hest && $hGP>=$hcop && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hdis>=$hnonm && $hdis>=$hserv && $hdis>=$hGP && $hdis>=$humin && $hdis>=$hest && $hdis>=$hcop && $hdis>=$hcprop && $hdis>=$hcuni){
					$h1=$hdis;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hcop && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hcop && $hcprop>=$hdis && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hcop && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hcop;
				}
				
			   if($servicio!=$obj->servicio){
			   	$obj->SetFont('Times','B',9);
			   	$obj->SetXY(10,38+$y+$ys);			      		
       		    $obj->MultiCell(250,9,'SERVICIO: '.$obj->servicio,0,'L',false);
       		   	$servicio=$obj->servicio;
       		   	$ys=$ys+6;
			   }
			   
			   if($y>=120){
			   		$y=0;
			   		$ys=0;
       				$obj->AddPage();
			   }
       			
			  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y+$ys);			      		
       		   $obj->MultiCell(18,$h,$obj->codProp,0,'L',false);  
       		   $obj->Rect(10,38+$y+$ys,18,$h1);     		   

			   $obj->SetXY(28,38+$y+$ys);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y+$ys,18,$h1);

			   $obj->SetXY(46,38+$y+$ys);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y+$ys,26,$h1);

			   $obj->SetXY(72,38+$y+$ys);			        		
       		   $obj->MultiCell(52,$h,$obj->nombre,0,'L');
       		   $obj->Rect(72,38+$y+$ys,52,$h1);

       		   $obj->SetXY(124,38+$y+$ys);			     		
       		   $obj->MultiCell(22,$h,$obj->servicio,0,'L');
       		   $obj->Rect(124,38+$y+$ys,22,$h1);

			   $obj->SetXY(146,38+$y+$ys);			       		
       		   $obj->MultiCell(34,$h,$obj->GP,0,'L');
       		   $obj->Rect(146,38+$y+$ys,34,$h1);

			   $obj->SetXY(180,38+$y+$ys);			      		
       		   $obj->MultiCell(30,$h,$obj->disciplina,0,'L');
       		   $obj->Rect(180,38+$y+$ys,30,$h1);

			   $obj->SetXY(210,38+$y+$ys);			   		
       		   $obj->MultiCell(37,$h,$obj->estado,0,'L');
       		   $obj->Rect(210,38+$y+$ys,37,$h1);

			   $obj->SetXY(247,38+$y+$ys);			       		
       		   $obj->MultiCell(40,$h,$obj->coperat,0,'L');
       		   $obj->Rect(247,38+$y+$ys,40,$h1);   

     		   $y=$y+$h1;


			}
			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00003'){


			$cpersona=$request->input('cpersona');
	   
			$listaPropuesta=DB::table('tpropuesta as tp')
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gteproyecto','=','tpe.cpersona')
			->leftJoin('testadopropuesta as te','tp.cestadopropuesta','=','te.cestadopropuesta')
			->leftJoin('tcondicionesoperativas as tc','tp.ccondicionoperativa','=','tc.ccondicionoperativa')
			->leftJoin('tpropuestadisciplina as tpd','tp.cpropuesta','=','tpd.cpropuesta')
			->leftJoin('tdisciplina as td','tpd.cdisciplina','=','td.cdisciplina')
			->select('tp.cpropuesta as cpropuesta','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tc.descripcion as condicion',DB::raw('fn_getdisciplinaspropuesta(tp.cpropuesta) as disciplina'),'tp.ccodigo as codpropuesta','tu.codigo as coduminera')
			->where('tp.cpersona_gteproyecto','=',$cpersona)
			->orderBy('servicio','ASC')
			->distinct('tp.cunidadminera')
			->get();		
			//Listado de Propuesta
			$obj= new PropuestaPdf('L');
			$obj->titulo='Listado de Propuestas por Tipo de Servicio';
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;			

			$obj->AddPage();

			$y=0;
			$ys=0;
			$servicio='';
						
			foreach ($listaPropuesta as $lp) {				

			   $obj->codProp=$lp->codpropuesta;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->disciplina=$lp->disciplina;
			   $obj->estado=$lp->estado;   
			   $obj->coperat=$lp->condicion; 	

			   $h=4;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hdis=(ceil((strlen($obj->disciplina)/14))*$h);
			   $hest=(ceil((strlen($obj->estado)/14))*$h);	
			   $hcop=(ceil((strlen($obj->coperat)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProp)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hdis && $hnonm>=$hest && $hnonm>=$hcop && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hdis && $humin>=$hest && $humin>=$hcop && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hdis && $hserv>=$hest && $hserv>=$hcop && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hdis && $hGP>=$hest && $hGP>=$hcop && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hdis>=$hnonm && $hdis>=$hserv && $hdis>=$hGP && $hdis>=$humin && $hdis>=$hest && $hdis>=$hcop && $hdis>=$hcprop && $hdis>=$hcuni){
					$h1=$hdis;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hcop && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hcop && $hcprop>=$hdis && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hcop && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hcop;
				}
				
			   if($servicio!=$obj->servicio){
			   	$obj->SetFont('Times','B',9);
			   	$obj->SetXY(10,38+$y+$ys);			      		
       		    $obj->MultiCell(250,9,'SERVICIO: '.$obj->servicio,0,'L',false);
       		   	$servicio=$obj->servicio;
       		   	$ys=$ys+6;
			   }
			   
			   if($y>=120){
			   		$y=0;
			   		$ys=0;
       				$obj->AddPage();
			   }
       			
			  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y+$ys);			      		
       		   $obj->MultiCell(18,$h,$obj->codProp,0,'L',false);  
       		   $obj->Rect(10,38+$y+$ys,18,$h1);     		   

			   $obj->SetXY(28,38+$y+$ys);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y+$ys,18,$h1);

			   $obj->SetXY(46,38+$y+$ys);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y+$ys,26,$h1);

			   $obj->SetXY(72,38+$y+$ys);			        		
       		   $obj->MultiCell(52,$h,$obj->nombre,0,'L');
       		   $obj->Rect(72,38+$y+$ys,52,$h1);

       		   $obj->SetXY(124,38+$y+$ys);			     		
       		   $obj->MultiCell(22,$h,$obj->servicio,0,'L');
       		   $obj->Rect(124,38+$y+$ys,22,$h1);

			   $obj->SetXY(146,38+$y+$ys);			       		
       		   $obj->MultiCell(34,$h,$obj->GP,0,'L');
       		   $obj->Rect(146,38+$y+$ys,34,$h1);

			   $obj->SetXY(180,38+$y+$ys);			      		
       		   $obj->MultiCell(30,$h,$obj->disciplina,0,'L');
       		   $obj->Rect(180,38+$y+$ys,30,$h1);

			   $obj->SetXY(210,38+$y+$ys);			   		
       		   $obj->MultiCell(37,$h,$obj->estado,0,'L');
       		   $obj->Rect(210,38+$y+$ys,37,$h1);

			   $obj->SetXY(247,38+$y+$ys);			       		
       		   $obj->MultiCell(40,$h,$obj->coperat,0,'L');
       		   $obj->Rect(247,38+$y+$ys,40,$h1);   

     		   $y=$y+$h1;


			}
			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00004'){

			$cpersona=$request->input('cpersona');
	   
			$listaPropuesta=DB::table('ttransmittalconfiguracion as tco')
			->leftJoin('ttransmittalejecucion as tej','tco.ctransconfiguracion','=','tej.ctransconfiguracion')
			->leftJoin('ttransmittalejecuciondetalle as ted','tej.ctransmittalejecucion','=','ted.ctransmittalejecucion')
			->leftJoin('tpropuesta as tp','tco.cpropuesta','=','tp.cpropuesta')
			->leftJoin('tpersona as tper','tp.cpersona','=','tper.cpersona')
			->leftJoin('tpersona as tper1','tej.cpersona_cdoc','=','tper1.cpersona')
			->leftJoin('tpersona as tper2','tp.cpersona_gteproyecto','=','tper2.cpersona')
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
			->leftJoin('testadopropuesta as te','tp.cestadopropuesta','=','te.cestadopropuesta')
			->leftJoin('tcondicionesoperativas as tc','tp.ccondicionoperativa','=','tc.ccondicionoperativa')
			->leftJoin('tcatalogo as tca','tej.ctipo','=','tca.digide')
			->leftJoin('tcatalogo as tca1','tej.tipoenvio','=','tca1.digide')
			->select('tp.cpropuesta as cpropuesta','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','tp.ccodigo as codpropuesta','tu.codigo as coduminera','tco.numero','tco.archivoformato','tej.fecha','ted.ctransmittalejedetalle','tca.descripcion as tipo','tper.nombre as cliente','tper1.nombre as percdocument','tper2.nombre as gerente','tca1.descripcion as tipoenvio')
			->where('tca.codtab','=','00041')
			->where('tca1.codtab','=','00042')
			->where('tej.ctipo','=','2')
			->orderBy('tp.cpropuesta','ASC')
			->orderBy('ted.ctransmittalejedetalle','ASC');

			if(!($cpersona==null || strlen($cpersona)<1)){
			 $listaPropuesta=$listaPropuesta->where('tp.cpersona_gteproyecto','=',$cpersona);
			}

			$listaPropuesta=$listaPropuesta->get();

			//Listado de Propuesta
			$obj= new TransmittalPropuestaPdf('L');
			$obj->titulo='Propuesta: Transmittals Recibidos';
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		

			$obj->AddPage();

			$y=0;	
			$ys=0;
			$propuesta='';

		
			foreach ($listaPropuesta as $lp) {		
			   $cpropuesta=$lp->cpropuesta;	
			   $codProp=$lp->codpropuesta;   
			   $codUmin=$lp->coduminera; 
			   $umin=$lp->uninombre; 			   
			   $nombre=$lp->prnombre;
			   $numtrans=$lp->numero;	
			   $tipoenv=$lp->tipoenvio;	
			   $archivform=$lp->archivoformato;	
			   $fecha=$lp->fecha;	
			   $tipo=$lp->tipo;	
			   $cliente=$lp->cliente;	
			   $percdoc=$lp->percdocument;	
			   $gerente=$lp->gerente;	

			   //	$nombre='JGSDKFJSOIFJEWOI JWEKHWEKRJL WEKRJ EWKRJQPLWJERIWE DSFKJEWRKTJERL KJWELRKTJ ERLTYJRGERT ERDF DR';
				


			   $h=4;	

			   $hnonm=(ceil((strlen($nombre)/16))*$h);
			   $humin=(ceil((strlen($umin)/15))*$h);
			   $hGP=(ceil((strlen($gerente)/16))*$h);
			   $hcli=(ceil((strlen($cliente)/9))*$h);			   
			   $hperdoc=(ceil((strlen($percdoc)/14))*$h);
			   $htipenv=(ceil((strlen($tipoenv)/14))*$h);				  
				
				if($hnonm>=$humin && $hnonm>=$hGP && $hnonm>=$hcli && $hnonm>=$hperdoc && $hnonm>=$htipenv){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hGP && $humin>=$hcli && $humin>=$hperdoc && $humin>=$htipenv){
					$h1=$humin;
				}

				elseif($hGP>=$humin && $hGP>=$hnonm && $hGP>=$hcli && $hGP>=$hperdoc && $hGP>=$htipenv){
					$h1=$hGP;
				}

				elseif($hcli>=$humin && $hcli>=$hGP && $hcli>=$hnonm && $hcli>=$hperdoc && $hcli>=$htipenv){
					$h1=$hcli;
				}

				elseif($hperdoc>=$humin && $hperdoc>=$hGP && $hperdoc>=$hcli && $hperdoc>=$hnonm && $hperdoc>=$htipenv){
					$h1=$hperdoc;
				}		
				
				else{
					$h1=$htipenv;
				}
				//DD($h1);

			
			   if($propuesta!=$cpropuesta){
			   	$obj->SetFont('Times','B',9);
			   	$obj->SetXY(10,38+$y+$ys);			      		
       		    $obj->MultiCell(18,$h,$codProp,0,'L',false);  
       		    $obj->Rect(10,38+$y+$ys,18,$h1);     		   

			    $obj->SetXY(28,38+$y+$ys);			        		
       		    $obj->MultiCell(18,$h,$codUmin,0,'L');
       		    $obj->Rect(28,38+$y+$ys,18,$h1);

			    $obj->SetXY(46,38+$y+$ys);			        		
       		    $obj->MultiCell(26,$h,$umin,0,'L');
       		    $obj->Rect(46,38+$y+$ys,26,$h1);

			    $obj->SetXY(72,38+$y+$ys);			        		
       		    $obj->MultiCell(52,$h,$nombre,0,'L');
       		    $obj->Rect(72,38+$y+$ys,52,$h1);
       		   	$propuesta=$cpropuesta;
       		   	$ys=$h1+6;
			   }
			   
			   if($y>=120){
			   		$y=0;
			   		$ys=0;
       				$obj->AddPage();
			   }
       			
			 /* 
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y+$ys);			      		
       		   $obj->MultiCell(18,$h,$codProp,'L',false);  
       		   $obj->Rect(10,38+$y+$ys,18,$h1);   		   

			   $obj->SetXY(28,38+$y+$ys);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y+$ys,18,$h1);

			   $obj->SetXY(46,38+$y+$ys);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y+$ys,26,$h1);

			   $obj->SetXY(72,38+$y+$ys);			        		
       		   $obj->MultiCell(52,$h,$obj->nombre,0,'L');
       		   $obj->Rect(72,38+$y+$ys,52,$h1);

       		   $obj->SetXY(124,38+$y+$ys);			     		
       		   $obj->MultiCell(22,$h,$obj->servicio,0,'L');
       		   $obj->Rect(124,38+$y+$ys,22,$h1);

			   $obj->SetXY(146,38+$y+$ys);			       		
       		   $obj->MultiCell(34,$h,$obj->GP,0,'L');
       		   $obj->Rect(146,38+$y+$ys,34,$h1);

			   $obj->SetXY(180,38+$y+$ys);			      		
       		   $obj->MultiCell(30,$h,$obj->disciplina,0,'L');
       		   $obj->Rect(180,38+$y+$ys,30,$h1);

			   $obj->SetXY(210,38+$y+$ys);			   		
       		   $obj->MultiCell(37,$h,$obj->estado,0,'L');
       		   $obj->Rect(210,38+$y+$ys,37,$h1);

			   $obj->SetXY(247,38+$y+$ys);			       		
       		   $obj->MultiCell(40,$h,$obj->coperat,0,'L');
       		   $obj->Rect(247,38+$y+$ys,40,$h1);   */

     		   //$y=$h1;


			}
		
			$obj->Output('D');
			exit;

		}


	}

	public function printProyecto(Request $request){	

		//Hora  y Fecha
		$fec= Carbon::now();
		$fec=$fec->format('d-m-Y');
		$hor=Carbon::now();
		$hor=$hor->format('h:i A');

		if($request->input('ireporte')=='00001'){	

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson='-d13-4h}{df"",-fg48*/';

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera');

			if(!($cpersona= null || strlen($cpersona)<1)){
			$listaProyectos=$listaProyectos->where('tpe.nombre','ilike','%'.$cperson.'%');
			}

			if(!($cproyecto= null || strlen($cproyecto)<1)){
			$listaProyectos=$listaProyectos->where('cproyecto','=',$cproye);
				
			}
			
			$listaProyectos=$listaProyectos->get();
			
		
			
			//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo='Listado de Proyectos';
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
							 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,utf8_decode($obj->umin),0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}

		if($request->input('ireporte')=='00002'){

			
			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson='-d13-4h}{df"",-fg48*/';

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera');

			if(!($cpersona= null || strlen($cpersona)<1)){
			$listaProyectos=$listaProyectos->where('tpe.nombre','ilike','%'.$cperson.'%');
			}

			if(!($cproyecto= null || strlen($cproyecto)<1)){
			$listaProyectos=$listaProyectos->where('cproyecto','=',$cproye);
				
			}
			
			$listaProyectos=$listaProyectos->get();
			
		
			
			//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo='Listado de Proyectos';
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
							 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,utf8_decode($obj->umin),0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00003'){		

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson='-d13-4h}{df"",-fg48*/';

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tpe.nombre','ASC')
			->distinct('tp.cunidadminera');

			if(!($cpersona= null || strlen($cpersona)<1)){
			$listaProyectos=$listaProyectos->where('tpe.nombre','ilike','%'.$cperson.'%');
			}

			if(!($cproyecto= null || strlen($cproyecto)<1)){
			$listaProyectos=$listaProyectos->where('cproyecto','=',$cproye);
				
			}
			
			$listaProyectos=$listaProyectos->get();
		
			
			//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Proyectos por Gte.Proyecto');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
			$ygp=0;
			$gerentepr='';
						
			foreach ($listaProyectos as $lp) {		
				

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=5;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}

				if($gerentepr!=$obj->GP){
					$obj->SetFont('Times','B',9);
				   	$obj->SetXY(10,38+$y+$ygp);			      		
	       		    $obj->MultiCell(250,9,'GERENTE DE PROYECTO: '.utf8_decode($obj->GP),0,'L',false);
	       		   	$gerentepr=$obj->GP;
	       		   	$ygp=$ygp+7;

				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }

			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y+$ygp);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y+$ygp,18,$h1);     		   

			   $obj->SetXY(28,38+$y+$ygp);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y+$ygp,18,$h1);

			   $obj->SetXY(46,38+$y+$ygp);			        		
       		   $obj->MultiCell(26,$h,utf8_decode($obj->umin),0,'L');
       		   $obj->Rect(46,38+$y+$ygp,26,$h1);

			   $obj->SetXY(72,38+$y+$ygp);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y+$ygp,58,$h1);

       		   $obj->SetXY(130,38+$y+$ygp);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y+$ygp,30,$h1);

			   $obj->SetXY(160,38+$y+$ygp);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y+$ygp,40,$h1);

			   $obj->SetXY(200,38+$y+$ygp);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y+$ygp,20,$h1);

			   $obj->SetXY(220,38+$y+$ygp);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y+$ygp,19,$h1);

			   $obj->SetXY(239,38+$y+$ygp);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y+$ygp,24,$h1);   

       		   $obj->SetXY(263,38+$y+$ygp);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y+$ygp,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}

	}

	/*---------------------------------------------------------------------------------------------*/

	public function printCdocumentario(Request $request){	

		//Hora  y Fecha
		$fec= Carbon::now();
		$fec=$fec->format('d-m-Y');
		$hor=Carbon::now();
		$hor=$hor->format('h:i A');

		if($request->input('ireporte')=='00001'){	

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}	
		
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
							 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}

		if($request->input('ireporte')=='00002'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos a la Fecha');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00003'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por GP');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00004'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por CD');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00005'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}				
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por Proyecto');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00006'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Transmittal Enviados');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}


		if($request->input('ireporte')=='00007'){		

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}				
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Transmittal Recibidos');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}

	}

	/* --------------------------------------------------------------------------------------- */

	public function printGestionHumana(Request $request){	

	

		//Hora  y Fecha
		$fec= Carbon::now();
		$fec=$fec->format('d-m-Y');
		$hor=Carbon::now();
		$hor=$hor->format('h:i A');

		if($request->input('ireporte')=='00001'){	

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}	
		
			$listaEmpleados=DB::table('tpersonadatosempleado as tpe')
			->join('tpersona as tp','tpe.cpersona','=','tp.cpersona')
			->leftJoin('tareas as ta','tpe.carea','=','ta.carea')
			->leftJoin('tcargos as tca','tpe.ccargo','=','tca.ccargo')
			->leftJoin('tcatalogo as tc1','tpe.estado','=','tc1.digide')
			->where('tc1.codtab','=','00039')
			->where('tpe.cpersona','=',$cpersona)
			->select('tpe.*','tp.nombre','ta.descripcion as area','tca.descripcion as cargo','tc1.descripcion as estado')
			->first();

			dd($listaEmpleados->rate3);

			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
							 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}

		if($request->input('ireporte')=='00002'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos a la Fecha');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00003'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por GP');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00004'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por CD');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00005'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}				
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por Proyecto');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00006'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Transmittal Enviados');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}


		if($request->input('ireporte')=='00007'){		

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}				
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Transmittal Recibidos');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}

		if($request->input('ireporte')=='00008'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}					
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por CD');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}

		if($request->input('ireporte')=='00009'){

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}				
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Lista de Programación de Proyectos por Proyecto');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;

		}	


		if($request->input('ireporte')=='00007'){		

			$cproyecto=$request->input('cproyecto');
			$cproye=null;

			if($cproyecto){
				$cproye=$cproyecto;
			}

			$cpersona=$request->input('cpersona');	
			$cperson=null;

			if($cpersona){
				$cperson=$cpersona;
			}				
			   
			$listaProyectos=DB::table('tproyecto as tp')			
			->leftJoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')			
			->leftJoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
			->leftJoin('tpersona as tpe','tp.cpersona_gerente','=','tpe.cpersona')			
			->leftJoin('testadoproyecto as te','tp.cestadoproyecto','=','te.cestadoproyecto')
			->leftJoin('tmedioentrega as tm','tp.cmedioentrega','=','tm.cmedioentrega')			
			->select('tp.cproyecto as cproyecto','tp.cunidadminera as cunidadminera','tu.nombre as uninombre','tp.nombre as prnombre','ts.descripcion as servicio','tpe.nombre as gerente','te.descripcion as estado','tp.codigo as codproyecto','tu.codigo as coduminera','tm.descripcion as medio','tp.finicio','tp.finicioreal','tp.fcierre','fcierrereal')		
			->orderBy('tp.codigo','ASC')
			->distinct('tp.cunidadminera')
			->where('tp.cpersona_gerente','=',$cperson)
	     	->orWhere('cproyecto','=',$cproye)
			->get();	
		
    		//Listado de Propuesta
			$obj= new ProyectoPdf('L');
			$obj->titulo=utf8_decode('Transmittal Recibidos');
			$obj->fecha='Fecha: '.$fec;
			$obj->hora='Hora:   '.$hor;		
			$obj->AddPage();


			$y=0;
						
			foreach ($listaProyectos as $lp) {		

				/* declaracion fechas 
				$finicial=$lp->finicio;
				$finicial=$finicial->format('d-m-Y');

				$finicialreal=Carbon::parse($lp->finicioreal);
				$finicialreal=$finicialreal->format('d-m-Y');

				$fcierre=Carbon::parse($lp->fcierre);
				$fcierre=$fcierre->format('d-m-Y');

				$fcierrereal=Carbon::parse($lp->fcierrereal);
				$fcierrereal=$fcierrereal->format('d-m-Y');*/

				$finicial=$lp->finicio;				
				$finicialreal=$lp->finicioreal;				
				$fcierre=$lp->fcierre;
				$fcierrereal=$lp->fcierrereal;
			
				/* fin fechas */

			   $obj->codProy=$lp->codproyecto;   
			   $obj->codUmin=$lp->coduminera; 
			   $obj->umin=$lp->uninombre; 
			   $obj->nombre=$lp->prnombre; 	
			   $obj->servicio=$lp->servicio;    
			   $obj->GP=$lp->gerente;			  
			   $obj->medio=$lp->medio;
			   $obj->estado=$lp->estado;   

			   if(!($finicialreal==null || strlen($finicialreal)<=0)){
			   	$obj->finicio=Carbon::parse($finicialreal);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');

			   //	$obj->finicio=substr($finicialreal, 0,10);
			   }
			   else{
			   	$obj->finicio=Carbon::parse($finicial);
			   	$obj->finicio=$obj->finicio->format('d-m-Y');
			   //	$obj->finicio=substr($finicial, 0,10);
			   }

			   if(!($fcierrereal==null || strlen($fcierrereal)<=0)){
			   	$obj->fcierre=Carbon::parse($fcierrereal);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierrereal, 0,10);
			   }
			   else{
			   	$obj->fcierre=Carbon::parse($fcierre);
			   	$obj->fcierre=$obj->fcierre->format('d-m-Y');

			   //	$obj->fcierre=substr($fcierre, 0,10);
			   }			   

			   $h=6;	

			   $hnonm=(ceil((strlen($obj->nombre)/24))*$h);
			   $humin=(ceil((strlen($obj->umin)/15))*$h);
			   $hserv=(ceil((strlen($obj->servicio)/9))*$h);
			   $hGP=(ceil((strlen($obj->GP)/16))*$h);	
			   $hmed=(ceil((strlen($obj->medio)/7))*$h);
			   $hest=(ceil((strlen($obj->estado)/7))*$h);	
			   $hfini=(ceil((strlen($obj->finicio)/18))*$h);
			   $hfcier=(ceil((strlen($obj->fcierre)/18))*$h);
			   $hcprop=(ceil((strlen($obj->codProy)/7))*$h);	
			   $hcuni=(ceil((strlen($obj->codUmin)/7))*$h);

			  
				
				if($hnonm>=$humin && $hnonm>=$hserv && $hnonm>=$hGP && $hnonm>=$hmed && $hnonm>=$hest && $hnonm>=$hfini && $hnonm>=$hcprop && $hnonm>=$hcuni){
					$h1=$hnonm;
				}
				elseif($humin>=$hnonm && $humin>=$hserv && $humin>=$hGP && $humin>=$hmed && $humin>=$hest && $humin>=$hfini && $humin>=$hcprop && $humin>=$hcuni){
					$h1=$humin;
				}

				elseif($hserv>=$hnonm && $hserv>=$humin && $hserv>=$hGP && $hserv>=$hmed && $hserv>=$hest && $hserv>=$hfini && $hserv>=$hcprop && $hserv>=$hcuni){
					$h1=$hserv;
				}

				elseif($hGP>=$hnonm && $hGP>=$hserv && $hGP>=$humin && $hGP>=$hmed && $hGP>=$hest && $hGP>=$hfini && $hGP>=$hcprop && $hGP>=$hcuni){
					$h1=$hGP;
				}

				elseif($hmed>=$hnonm && $hmed>=$hserv && $hmed>=$hGP && $hmed>=$humin && $hmed>=$hest && $hmed>=$hfini && $hmed>=$hcprop && $hmed>=$hcuni){
					$h1=$hmed;
				}

				elseif($hest>=$hnonm && $hest>=$hserv && $hest>=$hGP && $hest>=$humin && $hest>=$humin && $hest>=$hfini && $hest>=$hcprop && $hest>=$hcuni){
					$h1=$hest;
				}
				elseif($hcprop>=$hnonm && $hcprop>=$hserv && $hcprop>=$hGP && $hcprop>=$humin && $hcprop>=$hest && $hcprop>=$hfini && $hcprop>=$hmed && $hcprop>=$hcuni){
					$h1=$hcprop;
				}

				elseif($hcuni>=$hnonm && $hcuni>=$hserv && $hcuni>=$hGP && $hcuni>=$humin && $hcuni>=$humin && $hcuni>=$hfini && $hcuni>=$hcprop && $hcuni>=$hest){
					$h1=$hcuni;
				}

				else{
					$h1=$hfini;
				}
				
			 	 
			    if($y>=120){
			   		$y=0;
			   		$obj->AddPage();
			   }		  
			   $obj->SetFont('Times','',9);
			   $obj->SetFillColor(255,255,255); 
			   $obj->SetXY(10,38+$y);			      		
       		   $obj->MultiCell(18,$h,$obj->codProy,0,'L',false);  
       		   $obj->Rect(10,38+$y,18,$h1);     		   

			   $obj->SetXY(28,38+$y);			        		
       		   $obj->MultiCell(18,$h,$obj->codUmin,0,'L');
       		   $obj->Rect(28,38+$y,18,$h1);

			   $obj->SetXY(46,38+$y);			        		
       		   $obj->MultiCell(26,$h,$obj->umin,0,'L');
       		   $obj->Rect(46,38+$y,26,$h1);

			   $obj->SetXY(72,38+$y);			        		
       		   $obj->MultiCell(58,$h,utf8_decode($obj->nombre),0,'L');
       		   $obj->Rect(72,38+$y,58,$h1);

       		   $obj->SetXY(130,38+$y);			     		
       		   $obj->MultiCell(30,$h,$obj->servicio,0,'L');
       		   $obj->Rect(130,38+$y,30,$h1);

			   $obj->SetXY(160,38+$y);			       		
       		   $obj->MultiCell(40,$h,utf8_decode($obj->GP),0,'L');
       		   $obj->Rect(160,38+$y,40,$h1);

			   $obj->SetXY(200,38+$y);			      		
       		   $obj->MultiCell(20,$h,utf8_decode($obj->medio),0,'L');
       		   $obj->Rect(200,38+$y,20,$h1);

			   $obj->SetXY(220,38+$y);			   		
       		   $obj->MultiCell(19,$h,$obj->estado,0,'L');
       		   $obj->Rect(220,38+$y,19,$h1);

			   $obj->SetXY(239,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->finicio,0,'C');
       		   $obj->Rect(239,38+$y,24,$h1);   

       		   $obj->SetXY(263,38+$y);			       		
       		   $obj->MultiCell(24,$h,$obj->fcierre,0,'C');
       		   $obj->Rect(263,38+$y,24,$h1);   

       			$y=$y+$h1;      			
       		   
			}

			$obj->Output('D');
			exit;
		}

	}




	
}
