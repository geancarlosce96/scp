<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Auth;
use DB;
use App\Models\Tpersona;
use App\Models\Tpersonadatosempleado;
use App\Models\Tproyecto;
use App\Models\Tproyectopersona;
use App\Models\Tarea;
use App\Models\Tdisciplinaarea;

class ParticipacionController extends Controller
{
	//public 

	public function index()
	{
		$periodos = [];
		// $semanas = [];
		$periodo = date("Y");
		$semana_ini = date("W");
		$semana_fin = date("W");
		array_push($periodos, $periodo-2);
		array_push($periodos, $periodo-1);
		array_push($periodos, $periodo);
		// array_push($semanas, $semana_ini);
		// array_push($semanas, $semana_fin);
		$data["periodos"] = $periodos;
		$data["periodo"] = $periodo;
		$data["semana_ini"] = $semana_ini;
		$data["semana_fin"] = $semana_fin;
		return view("proyecto.participacion.listado", $data);
	}

	//ajax

	public function obtenerListadoParticipacion(Request $request)
	{
		$periodo = $request->input("periodo");
		$semana_ini = $request->input("semanaini");
		$semana_fin = $request->input("semanafin");
		$data = [];
		// dd($semana_ini,$semana_fin);

		$area = Tpersonadatosempleado::where('cpersona','=',Auth::user()->cpersona)->first();
		$disciplina = Tdisciplinaarea::where('carea','=',$area->carea)->first();
		// dd($disciplina->carea);
		//participacines		
		$listaSemanal = $this->_obtenerParticipacionSemanal(Auth::user()->nombre, $periodo,$semana_ini,$semana_fin, $disciplina->cdisciplina);
		$listaDiaria = $this->_obtenerParticipacionDiaria(Auth::user()->nombre, $periodo,$semana_ini,$semana_fin, $disciplina->cdisciplina);		
		$data["lista"] = array_merge($listaSemanal, $listaDiaria);		
		return $data;
	}

	//private

	private function _obtenerParticipacionSemanal($usuario, $periodo, $semana_ini, $semana_fin, $disciplina)
	{
		$lista = DB::table('planificacion as p')        
			->join('tproyecto as pro', 'pro.cproyecto', '=', 'p.cproyecto')
			->join('tproyectopersona as tp','tp.cproyecto', '=', 'p.cproyecto')
			->join('tpersona as per','per.cpersona','=','tp.cpersona')
			->join('tpersona as gp','gp.cpersona','=','pro.cpersona_gerente')
			->select("pro.codigo as codigoproyecto", "pro.nombre as descripcion", "p.periodo", "p.semana", 
				"pro.cproyecto", "p.updated_at",
				"p.id as planificacion_id", "p.planificadas as horas","per.abreviatura as lider", "gp.abreviatura as gerente")
			->where('p.usuario', $usuario)
			->where('p.periodo', $periodo)
			->where('p.planificadas', '>', 0)
			->where('tp.eslider', '=', 1)
			->where('tp.cdisciplina', '=', $disciplina)
			// ->whereBetween('p.semana',[$semana_ini, $semana_fin])
			->where('p.semana', '>=', $semana_ini)
			->where('p.semana', '<=', $semana_fin)
			->distinct('p.cproyecto')
			->orderBy('pro.codigo', "ASC")
			->get();

		foreach ($lista as $key => $e){
			$e->tipo = "semanal";
			$e->codigoactividad = "";
			$e->fecha = "";
		}

		return $lista;
	}

	private function _obtenerParticipacionDiaria($usuario, $periodo, $semana_ini, $semana_fin, $disciplina)
	{
		$lista = DB::table('planificacion_diaria as p')        
			->join('tproyectoactividades as proa', 'proa.cproyectoactividades', '=', 'p.cproyectoactividades')
			->join('tproyecto as pro', 'pro.cproyecto', '=', 'proa.cproyecto')
			->join('tproyectopersona as tp','tp.cproyecto', '=', 'proa.cproyecto')
			->join('tpersona as per','per.cpersona','=','tp.cpersona')
			->join('tpersona as gp','gp.cpersona','=','pro.cpersona_gerente')
			->select("proa.codigoactvidad as codigoactividad", "proa.cproyecto as codigoproyecto", 
				"pro.codigo as codigoproyecto",
				"proa.descripcionactividad as descripcion", "p.periodo", "p.semana", "proa.cproyectoactividades", 
				"p.updated_at", "p.id as planificacion_id", 
				"p.lun", "p.mar", "p.mie", "p.jue", "p.vie", "p.sab", "p.dom","per.abreviatura as lider", "gp.abreviatura as gerente"
			)
			->where('p.usuario', '=', $usuario)
			->where('p.periodo', '=', $periodo)
			// ->where('p.lun', '>', 0)
			->whereRaw('(p.lun > 0 or p.mar >0 or p.mie > 0 or p.jue > 0 or p.vie >0 or p.sab >0 or p.dom > 0)')
			->where('tp.eslider', '=', 1)
			->where('tp.cdisciplina', '=', $disciplina)
			// ->whereBetween('p.semana',[$semana_ini, $semana_fin])
			->where('p.semana', '>=', $semana_ini)
			->where('p.semana', '<=', $semana_fin)
			->distinct('p.cproyecto')
			->orderBy('pro.codigo', "ASC")
			->get();

		foreach ($lista as $key => $e){
			$e->tipo = "diaria";
			$e->horas = "";
			if($e->lun>0)
				$e->horas = "lun ($e->lun) ";
			if($e->mar>0)
				$e->horas .= "mar ($e->mar) ";
			if($e->mie>0)
				$e->horas .= "mie ($e->mie) ";
			if($e->jue>0)
				$e->horas .= "jue ($e->jue) ";
			if($e->vie>0)
				$e->horas .= "vie ($e->vie) ";
			if($e->sab>0)
				$e->horas .= "sab ($e->sab) ";
			if($e->dom>0)
				$e->horas .= "dom ($e->dom) ";
			$e->fecha = "";
		}

		return $lista;
	}

}
