<?php

namespace App\Http\Controllers\CargaMensual;

use Illuminate\Http\Request;

use App\Http\Requests;


use Auth;
use Carbon\Carbon;
use DB;
use Response;

use App\Http\Controllers\Controller;

class cargaMensualProyectosController extends Controller
{

    public function verAreas()
    {
        $careas = $this->areas();

        return view('proyecto.CargabilidadMensual.ListaProyectos.opcionesCargabilidad', compact('careas'));
    }

    public function  getproyectosCM(Request $request)
    {

        $mes = $this->mes($request->datepicker);
        $anio = $this->anio($request->datepicker);
        $carea = $request->careas;
        $tipo = $request->tipo;

        $proyectos =  $this->listaproyectosfacnofacadmin($carea,$mes,$anio,$tipo);

        //dd($proyectos);


        $empleados = $this->empleados($carea);
        $empleadosejecutados = $this->empleadosejecutados($carea,$mes,$anio);

        $emple = array_merge($empleados,$empleadosejecutados);
        $empledos_list = array_unique($emple);

        $empleados_lista =  $this->empleados_lista_real($carea,$mes,$anio);

        $arrayproyectos = [];

        foreach ( $proyectos as $py)
        {
            $col['cunidadminera'] = $py->cunidadmin;
            $col['nombrecliente'] = $py->nombrecliente;
            $col['nombreunidadminera'] = $py->nombreunidadminera;
            $col['codigo'] = $py->codigo;
            $col['cproyecto'] = $py->cproyecto;
            $col['descripcion'] = $py->description;
            $col['estado'] = $py->estadoo;
            $col['carea'] = $py->carea;
            $col['sum'] = floatval($py->sum);
            $col['mes'] =$mes;

            $arrayhe = [];
            foreach ($empledos_list as $em){

                $list = explode('-',$em);
                $cpersona = $list[0];

                $horas_empleado =  $this->listaproyectosfacnofacadminuser($py->cproyecto,$carea,$mes,$anio,$tipo,$cpersona);

                $colum['sumempleado'] = 0;
                if($horas_empleado != null)
                {
                    $colum['sumempleado'] = floatval($horas_empleado);
                }
                array_push($arrayhe,$colum);
            }

            $col['sumatotal_empleado'] =  $arrayhe;
            array_push($arrayproyectos,$col);

        }

       // dd($arrayproyectos);

        return view('proyecto.CargabilidadMensual.ListaProyectos.tablaProyectosCargaMensual', compact('arrayproyectos','empleados_lista'));


    }

//    Funciones REUTILIZABLES
    public function areas(){

        $user = Auth::user();

        $cargoid =  DB::table('tpersonadatosempleado as tpde')
            ->select('tpde.ccargo')
            ->where('tpde.cpersona','=',$user->cpersona)
            ->lists('tpde.ccargo');

        //Obtiene los cargos hijos
        $cargos_hijos=$this->obtener_cargos_hijos($cargoid);

        $array_cargos_hijos_todos=[];

        array_push($array_cargos_hijos_todos, $cargos_hijos);

        //obtiene los cargos nietos
        while ( $cargos_hijos != null) {

            $cargos_hijos=$this->obtener_cargos_hijos($cargos_hijos);
            array_push($array_cargos_hijos_todos,$cargos_hijos);
        }

        $careas = [''=>''];

        $cargos_hijos_todos=[];


        if ($array_cargos_hijos_todos) {

            //Coloca en un array unidimensional todos los cargos nietos e hijos encontrados
            foreach ($array_cargos_hijos_todos as $ach) {

                foreach ($ach as $ch) {
                    array_push($cargos_hijos_todos, $ch);
                }

            }

            //Obtiene todos los colaboradores con los cargos encontrados y lista sus areas
            $careas_colaboradores =DB::table('tpersonadatosempleado')
                ->where('estado','=','ACT')
                ->whereIn('ccargo',$cargos_hijos_todos)
                ->select('carea')
                ->distinct()
                ->lists('carea');

            //Obtiene las areas de los colaboradores encontrados en $careas_colaboradores
            $careas = DB::table('tareas')
                ->whereIn('carea',$careas_colaboradores)
                ->orderBy('descripcion','ASC')
                ->select('carea','descripcion','codigo_sig','careaparentdespliegue')
                ->get();

        }

        return $careas;
    }

    private function obtener_cargos_hijos($ccargo=[]){

        $cargos_hijos =  DB::table('tcargos as tc')
            ->select('tc.ccargo','tc.cargoparentdespliegue')
            ->whereIn('tc.cargoparentdespliegue',$ccargo)
            ->lists('tc.ccargo');

        return $cargos_hijos;

    }

    public  function mes($mes)
    {
        $monthCarbon = $mes;
       // dd(substr($monthCarbon, 6, 4));
        if (substr($monthCarbon, 2, 1) == '/' || substr($monthCarbon, 2, 1) == '-') {
            $monthCarbon = Carbon::create(substr($monthCarbon, 3, 4), substr($monthCarbon, 0, 2));

        } else {
            $monthCarbon = Carbon::create(substr($monthCarbon, 0, 4), substr($monthCarbon, 5, 2));
        }
        //dd($monthCarbon);


        $mes_seleccionado = $monthCarbon->format('m');

        return $mes_seleccionado;

    }

    public  function anio($anio)
    {
        $yearCarbon = $anio;
        // dd(substr($monthCarbon, 6, 4));
        if (substr($yearCarbon, 2, 1) == '/' || substr($yearCarbon, 2, 1) == '-') {
            $yearCarbon = Carbon::create(substr($yearCarbon, 3, 4), substr($yearCarbon, 0, 2));

        } else {
            $yearCarbon = Carbon::create(substr($yearCarbon, 0, 4), substr($yearCarbon, 5, 2));
        }


        $anio_seleccionado = $yearCarbon->format('Y');

        return $anio_seleccionado;

    }

    public function listaproyectosfacnofacadmin($carea,$mes,$anio,$tipo)
    {
        $proyectos =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tproyecto as tp', 'tp.cproyecto', '=', 'tpe.cproyecto')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->leftjoin('testadoproyecto as tep', 'tep.cestadoproyecto', '=', 'tp.cestadoproyecto')
            ->leftjoin('tpersona as tper', 'tp.cpersonacliente', '=', 'tper.cpersona')
            ->select(DB::raw("SUM(tpe.horasejecutadas) as sum"),'tp.codigo','tpe.cproyecto','tpe.carea','tpe.tipo','tum.codigo as cunidadmin','tum.nombre as nombreunidadminera','tep.descripcion as estadoo','tp.nombre as description','tper.nombre as nombrecliente')
            ->groupBy('tp.codigo','tpe.cproyecto','tpe.carea','tpe.tipo','cunidadmin','nombreunidadminera','estadoo','description','nombrecliente')
            ->where('tpe.carea','=',$carea)
            ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
            ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->where('tpe.tipo','=',$tipo)
            //->where('tpe.cproyecto','=','5354')
            ->wherenull('tpe.cactividad')
            ->orderBy('tum.codigo','ASC')
            ->get();

        return $proyectos;

    }
    public function empleados($careas)
    {
        $empleados =  DB::table('tpersonadatosempleado as tpers')
            ->join('tpersona as tpe', 'tpe.cpersona', '=', 'tpers.cpersona')
            ->join('tusuarios as tu','tu.cpersona','=','tpers.cpersona')
            ->join('tproyectoejecucion as tpry','tpry.cpersona_ejecuta','=','tpe.cpersona')
            ->leftjoin('ttipocontrato as tpc', 'tpc.ctipocontrato', '=', 'tpers.ctipocontrato')
            ->where('tpry.carea','=',$careas)
            ->where('tpers.estado','=','ACT')
            ->distinct()
            ->lists(DB::raw('CONCAT(tpers.cpersona,\'-\',tpe.abreviatura,\'-\',tpers.estado) as nombre'));

        return $empleados;

    }

    public function empleadosejecutados($careas,$mes,$anio)
    {
        $empleadosejecutados =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
            ->leftjoin('tpersonadatosempleado as tpde','tpde.cpersona','=','tp.cpersona')
            ->leftjoin('ttipocontrato as tpc', 'tpc.ctipocontrato', '=', 'tpde.ctipocontrato')
            ->where('tpe.carea','=',$careas)
            ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
            ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->distinct()
            ->lists(DB::raw('CONCAT(tp.cpersona,\'-\',tp.abreviatura,\'-\',tpde.estado) as nombre'));
        //dd($empleadosejecutados);

        return $empleadosejecutados;

    }

    public function empleados_lista_real($carea,$mes,$anio) {


        $empleados = $this->empleados($carea);
        $empleadosejecutados = $this->empleadosejecutados($carea,$mes,$anio);

        $emple = array_merge($empleados,$empleadosejecutados);
        $empledos_list = array_unique($emple);

        $empleados_lista = [];

        foreach ($empledos_list as $key => $value) {
            $list = explode('-',$value);
            $e['cpersona'] =  $cpersona = $list[0];
            $e['abreviatura'] =  $abreviatura = $list[1];
            $e['estado'] =  $estado = $list[2];

            array_push($empleados_lista, $e);

        }

        return $empleados_lista;
    }

    public function listaproyectosfacnofacadminuser($cproyecto,$carea,$mes,$anio,$tipo,$user)
    {
        $proyectos =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tproyecto as tp', 'tp.cproyecto', '=', 'tpe.cproyecto')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->leftjoin('testadoproyecto as tep', 'tep.cestadoproyecto', '=', 'tp.cestadoproyecto')
            ->leftjoin('tpersona as tper', 'tp.cpersonacliente', '=', 'tper.cpersona')
            ->where('tpe.carea','=',$carea)
            ->where('tpe.cproyecto','=',$cproyecto)
            ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
            ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.tipo','=',$tipo)
            ->where('tpe.cpersona_ejecuta','=',$user)
            ->sum('tpe.horasejecutadas');

        return $proyectos;

    }


}
