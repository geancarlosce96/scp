<?php

namespace App\Http\Controllers\CargaMensual;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Carbon\Carbon;
use DB;
use Response;

use App\Http\Controllers\Controller;

class cargaMensualGraficosController extends Controller
{

    public function verAreasGraficoMensual()
    {
        $careas = $this->areas();

        return view('proyecto.CargabilidadMensual.GraficosCM.opcionesgraficoCargabilidadMensual', compact('careas'));
    }

    public function graficoMensual(Request $request){

        $mes = $this->mes($request->fecha_final);
        $anio = $this->anio($request->fecha_final);
        $carea = $request->careas;


        $horasFF = $this->horas_mensuales($carea,$mes,$anio,1,'externa');
        $horasFN = $this->horas_mensuales($carea,$mes,$anio,2,'externa');
        $horasAND = $this->horas_mensuales($carea,$mes,$anio,3,'externa');
        $horasANDint = $this->horas_mensuales($carea,$mes,$anio,3,'interna');
        $mesesNombre =  $this->nombreMeses();
        $mesName = $mesesNombre[$mes-1];


       $horasportipo=[$mesName,floatval($horasFF),floatval($horasFN),floatval($horasAND),floatval($horasANDint)];

       // dd($horasportipo);
        return $horasportipo;
    }

    public function grafico3Mensual(Request $request)

    {
        $careas = $request->careas;

        $mes_inicial = $this->mes($request->fecha_ini);
        $anio_inicial = $this->anio($request->fecha_ini);

        $mes_final = $this->mes($request->fecha_final);
        $anio_final = $this->anio($request->fecha_final);

        $mesesNombre =  $this->nombreMeses();


        $num_sema = [];
        if ($anio_final == $anio_inicial) {

            for ($i = $mes_inicial; $i <= $mes_final; $i++) {

                array_push($num_sema, ['mesNombre' =>  $mesesNombre[$i -1], 'mes' => $i,'anio' => $anio_final]);
            }

        } else if ($anio_final > $anio_inicial) {
            //dd($fechaDesde,$fechaHasta,$semana_ini,$semana_fin,$anio_inicial,$anio_final,'OK');
            for ($i = $mes_inicial; $i <= 12; $i++) {

                array_push($num_sema, ['mesNombre' =>  $mesesNombre[$i -1],'mes' => $i, 'anio' => $anio_inicial]);
            }

            // dd($num_sema);

            for ($i = 1; $i <= $mes_final; $i++) {

                array_push($num_sema, ['mesNombre' => $mesesNombre[$i -1],'mes' => $i, 'anio' => $anio_final]);
            }

        } else {

        }
        //dd($num_sema);

        $arraysemana =[];
        foreach($num_sema as $ns)

        {
            $col['horasFF'] = floatval($this->horas_varios_meses($careas,$ns['mes'],$ns['anio'],1,'externa'));
            $col['horasFN'] =  floatval($this->horas_varios_meses($careas,$ns['mes'],$ns['anio'],2,'externa'));
            $col['horasAND'] =  floatval($this->horas_varios_meses($careas,$ns['mes'],$ns['anio'],3,'externa'));
            $col['horasINT'] =  floatval($this->horas_varios_meses($careas,$ns['mes'],$ns['anio'],3,'interna'));
            $col['mes'] = $ns['mesNombre'];

            array_push($arraysemana,$col);
        }

        // dd($arraysemana);

        return $arraysemana;

    }

    public function graficoMensual_porusuarios(Request $request)
    {
        $mes = $this->mes($request->fecha_final);
        $anio = $this->anio($request->fecha_final);
        $carea = $request->careas;

        $mesesNombre =  $this->nombreMeses();
        $mesName = $mesesNombre[$mes-1];

        $empleados = $this->empleados($carea);
        $empleadosejecutados = $this->empleadosejecutados($carea,$mes,$anio);

        $emple = array_merge($empleados,$empleadosejecutados);
        $empledos_list = array_unique($emple);

        //$empleados_lista =  $this->empleados_lista_real($carea,$mes,$anio);
       // dd($empleados_lista);


        $array_fac_nofac_adm = [];
        foreach ($empledos_list as $emp)
        {
            $list = explode('-',$emp);
            $cpersona = $list[0];
            $abreviatura = $list[1];

            $sumtotal_fac =  $this->horasxempleado($carea,$cpersona,$mes,$anio,1,'externa');
            $sumtotal_nofac =  $this->horasxempleado($carea,$cpersona,$mes,$anio,2,'externa');
            $sumtotal_admin =  $this->horasxempleado($carea,$cpersona,$mes,$anio,3,'externa');
            $sumtotal_admin_interna =  $this->horasxempleado($carea,$cpersona,$mes,$anio,3,'interna');

            $suma_total_horas = $sumtotal_fac+$sumtotal_nofac+$sumtotal_admin+$sumtotal_admin_interna;
            
            if ($suma_total_horas > 0) {
                array_push($array_fac_nofac_adm,[
                    'user' => $cpersona,
                    'abreviatura' => $abreviatura,
                    'sumuser_fac' => $sumtotal_fac,
                    'sumuser_nofac' => $sumtotal_nofac,
                    'sumuser_admin' => $sumtotal_admin,
                    'sumuser_admin_interna' => $sumtotal_admin_interna,
                    'mes'=>$mesName,
                ]);
            }

        }

        // dd($array_fac_nofac_adm);

        return $array_fac_nofac_adm;

    }

    public function graficomensual_nofacturable (Request $request)
    {
        $mes = $this->mes($request->fecha_final);
        $anio = $this->anio($request->fecha_final);
        $carea = $request->careas;

        $mesesNombre =  $this->nombreMeses();
        $mesName = $mesesNombre[$mes-1];


        // GRAFICA NO FACTURABLES

        $actividades = $this->listaactividadesnofacturables();
        // dd($actividades);

        $arrayhorasnofact_nuevo=[];

        foreach ($actividades as $act)
        {
            $ac['cactividad']=$act['pk'];
            $ac['descripcion']=$act['descripcion'];
            $ac['mes']=$mesName;
            $acumulativo_nuevo =  $this->obtener_horasnofacturables($carea,$mes,$anio,$act['pk']);
            $ac['sumatodos']=floatval($acumulativo_nuevo);

            if ($ac['sumatodos'] > 0) {
                array_push($arrayhorasnofact_nuevo,$ac);
            }

        }

        // dd($arrayhorasnofact_nuevo);
        return $arrayhorasnofact_nuevo;

    }

    public function graficomensual_admin(Request $request)
    {

        $mes = $this->mes($request->fecha_final);
        $anio = $this->anio($request->fecha_final);
        $carea = $request->careas;

        //dd(12132);
        $mesesNombre =  $this->nombreMeses();
        $mesName = $mesesNombre[$mes-1];

        // GRAFICO ADMINISTRATIVOS INTERNOS

        $actividadesinternas = $this->listaactividadesadministrativas();

        //dd($actividadesinternas);

        $arrayinternasactividades  = [];

        //dd("hoa",$actividadesinternas);
        foreach ($actividadesinternas  as $act)
        {

            $acti['cactividad']=$act['pk'];
            $acti['descripcion']=$act['descripcion'];
            $acti['mes']=$mesName;
            $acumulativo_nuevo = $this->obtener_horasnofacturablesadmin($carea,$mes,$anio,$act['pk']);

            $acti['sumatodos']=floatval($acumulativo_nuevo);

            if ($acti['sumatodos'] > 0) {
                array_push($arrayinternasactividades,$acti);
            }


        }

         //dd($arrayinternasactividades);

        return $arrayinternasactividades;

    }

    public function graficomensual_internos(Request $request)
    {

        $mes = $this->mes($request->fecha_final);
        $anio = $this->anio($request->fecha_final);
        $carea = $request->careas;

        //dd(12132);
        $mesesNombre =  $this->nombreMeses();
        $mesName = $mesesNombre[$mes-1];

        $internos = [];

        $proyectos = DB::table('tproyectoejecucion as tpe')
            ->join('tproyecto as tp','tp.cproyecto','=','tpe.cproyecto')
            ->select('tp.codigo','tp.descripcion as nombrepy','tpe.cproyecto','tp.nombre')
            ->where('tpe.carea','=',$carea)
             ->where(DB::raw('extract(month from fplanificado)'),'=',$mes)
             ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)

            ->where('tipo','=','3')
            ->wherenull('tpe.cactividad')
            ->distinct('tpe.cproyecto')
            ->get();

        // dd($proyectos);

        foreach ($proyectos as $key => $pry) {

            $horas = DB::table('tproyectoejecucion as tpe')
                ->where('tpe.carea','=',$carea)
                ->where(DB::raw('extract(month from fplanificado)'),'=',$mes)
                ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)

                ->where('tipo','=','3')
                ->where('cproyecto','=',$pry->cproyecto)
                ->wherenull('tpe.cactividad')
                ->sum('tpe.horasejecutadas');

            $py['codigo'] = $pry->codigo;
            $py['nombre'] = $pry->nombre;
            $py['suma_horas'] = floatval($horas);
            $py['mes']=$mesName;

            array_push($internos,$py);
        }


        return $internos;

    }

    public function graficoproductividadCargabilidad(Request $request)
    {
        $mes = $this->mes($request->fecha_final);
        $anio = $this->anio($request->fecha_final);
        $carea = $request->careas;

        $mesesNombre =  $this->nombreMeses();
        $mesName = $mesesNombre[$mes-1];
    }

    // Funciones Reutilizables

    public function areas(){

        $user = Auth::user();

        $cargoid =  DB::table('tpersonadatosempleado as tpde')
            ->select('tpde.ccargo')
            ->where('tpde.cpersona','=',$user->cpersona)
            ->lists('tpde.ccargo');

        //Obtiene los cargos hijos
        $cargos_hijos=$this->obtener_cargos_hijos($cargoid);

        $array_cargos_hijos_todos=[];

        array_push($array_cargos_hijos_todos, $cargos_hijos);

        //obtiene los cargos nietos
        while ( $cargos_hijos != null) {

            $cargos_hijos=$this->obtener_cargos_hijos($cargos_hijos);
            array_push($array_cargos_hijos_todos,$cargos_hijos);
        }

        $careas = [''=>''];

        $cargos_hijos_todos=[];


        if ($array_cargos_hijos_todos) {

            //Coloca en un array unidimensional todos los cargos nietos e hijos encontrados
            foreach ($array_cargos_hijos_todos as $ach) {

                foreach ($ach as $ch) {
                    array_push($cargos_hijos_todos, $ch);
                }

            }

            //Obtiene todos los colaboradores con los cargos encontrados y lista sus areas
            $careas_colaboradores =DB::table('tpersonadatosempleado')
                ->where('estado','=','ACT')
                ->whereIn('ccargo',$cargos_hijos_todos)
                ->select('carea')
                ->distinct()
                ->lists('carea');

            //Obtiene las areas de los colaboradores encontrados en $careas_colaboradores
            $careas = DB::table('tareas')
                ->whereIn('carea',$careas_colaboradores)
                ->orderBy('descripcion','ASC')
                ->select('carea','descripcion','codigo_sig','careaparentdespliegue')
                ->get();

        }

        return $careas;
    }
    private function obtener_cargos_hijos($ccargo=[]){

        $cargos_hijos =  DB::table('tcargos as tc')
            ->select('tc.ccargo','tc.cargoparentdespliegue')
            ->whereIn('tc.cargoparentdespliegue',$ccargo)
            ->lists('tc.ccargo');

        return $cargos_hijos;

    }
    public  function mes($mes)
    {
        $monthCarbon = $mes;
        // dd(substr($monthCarbon, 6, 4));
        if (substr($monthCarbon, 2, 1) == '/' || substr($monthCarbon, 2, 1) == '-') {
            $monthCarbon = Carbon::create(substr($monthCarbon, 3, 4), substr($monthCarbon, 0, 2));

        } else {
            $monthCarbon = Carbon::create(substr($monthCarbon, 0, 4), substr($monthCarbon, 5, 2));
        }
        //dd($monthCarbon);


        $mes_seleccionado = $monthCarbon->format('m');

        return $mes_seleccionado;

    }
    public  function anio($anio)
    {
        $yearCarbon = $anio;
        // dd(substr($monthCarbon, 6, 4));
        if (substr($yearCarbon, 2, 1) == '/' || substr($yearCarbon, 2, 1) == '-') {
            $yearCarbon = Carbon::create(substr($yearCarbon, 3, 4), substr($yearCarbon, 0, 2));

        } else {
            $yearCarbon = Carbon::create(substr($yearCarbon, 0, 4), substr($yearCarbon, 5, 2));
        }


        $anio_seleccionado = $yearCarbon->format('Y');

        return $anio_seleccionado;

    }
    private function horas_mensuales($carea,$mes,$anio,$tipo,$valor){


        $horas = DB::table('tproyectoejecucion as tpe')
            ->where('tpe.carea','=',$carea)
             ->where(DB::raw('extract(month from fplanificado)'),'=',$mes)
             ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)

            ->where('tipo','=',$tipo);
        if ($tipo=='3') {

            if ($valor == 'interna') {
                $horas = $horas->wherenull('tpe.cactividad');

            }
            else {
                $horas = $horas->wherenull('tpe.cproyectoactividades');
            }
        }

        $horas =$horas->sum('tpe.horasejecutadas');
        // dd($horas);


        return $horas;
    }

    private function horas_varios_meses($careas,$mes,$anio,$tipo,$valor){
        $horas = DB::table('tproyectoejecucion as tpe')
            ->where('tpe.carea','=',$careas)
            ->where('tipo','=',$tipo)
            ->where(DB::raw('extract(month from fplanificado)'),'=',$mes)
            ->where(DB::raw('extract(year from fplanificado)'),'=',$anio);
        if ($tipo=='3') {

            if ($valor == 'interna') {
                $horas = $horas->wherenull('tpe.cactividad');

            }
            else {
                $horas = $horas->wherenull('tpe.cproyectoactividades');
            }
        }
        $horas = $horas->sum('tpe.horasejecutadas');

        return $horas;
    }

    public function horasxempleado($careas,$userarea,$mes,$anio,$tipo,$valor)
    {
        $horasxempleado  = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tpersonadatosempleado as tpers', 'tpe.cpersona_ejecuta', '=', 'tpers.cpersona')
            ->leftjoin('tpersona as tpde', 'tpe.cpersona_ejecuta', '=', 'tpde.cpersona')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpers.carea')
            ->where(DB::raw('extract(month from fplanificado)'),'=',$mes)
            ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.tipo','=',$tipo)
            ->where('tpe.cpersona_ejecuta', '=', $userarea);

        if ($tipo=='3') {

            if ($valor == 'interna') {
                $horasxempleado = $horasxempleado->wherenull('tpe.cactividad');

            }
            else {
                $horasxempleado = $horasxempleado->wherenull('tpe.cproyectoactividades');
            }
        }


        $horasxempleado = $horasxempleado->sum('tpe.horasejecutadas');


        $sumatoria_horas = 0;
        if($horasxempleado != null)
        {
            $sumatoria_horas = floatval($horasxempleado);
        }


        return $sumatoria_horas;
    }

    public function listaactividadesnofacturables()
    {
        $actividadesNOFacturable = DB::table('ttiposnofacturables as ttnf')
            ->select('ttnf.ctiponofacturable','ttnf.descripcion')
            ->orderBy('ttnf.descripcion' ,'ASC')
            ->get();
        $arrayNOFACT = [];
        foreach ($actividadesNOFacturable as $act)
        {
            $col['pk'] = $act->ctiponofacturable;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayNOFACT,$col);
        }

        return $arrayNOFACT;
    }

    public function listaactividadesadministrativas()
    {
        $actividadesadministrativas = DB::table('tactividad as ta')
            ->select('ta.cactividad','ta.descripcion')
            ->wherenotNull('ta.cactividad_parent')
            ->where('tipoactividad','=','00002')
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->cactividad;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }

    public function obtener_horasnofacturables($careas,$mes,$anio,$actnofact)
    {

        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('ttiposnofacturables as ttnf', 'ttnf.ctiponofacturable', '=', 'tpe.ctiponofacturable')
            ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
            ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.tipo','=',2)
            ->where('tpe.ctiponofacturable','=',$actnofact)
            ->sum('tpe.horasejecutadas');

        return $horasxempleadoofacturables;

    }

    public function obtener_horasnofacturablesadmin($careas,$mes,$anio,$cactividad)
    {
        //dd($careas,$semana,$tiponofac,$userarea);
        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tactividad as tact', 'tact.cactividad', '=', 'tpe.cactividad')
             ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
             ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cactividad','=',$cactividad)
            ->where('tpe.tipo','=',3)
            ->whereNotNull('tpe.cactividad')
            ->sum('tpe.horasejecutadas');

        $sumatoria_horas = 0;
        if($horasxempleadoofacturables != null)
        {
            $sumatoria_horas = $horasxempleadoofacturables;
        }

        return $sumatoria_horas;
    }

    public function nombreMeses()
    {
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        return $meses;
    }
    // Funcion de Empleados Reutilizables

    public function empleados($careas)
    {
        $empleados =  DB::table('tpersonadatosempleado as tpers')
            ->join('tpersona as tpe', 'tpe.cpersona', '=', 'tpers.cpersona')
            ->join('tusuarios as tu','tu.cpersona','=','tpers.cpersona')
            ->join('tproyectoejecucion as tpry','tpry.cpersona_ejecuta','=','tpe.cpersona')
            ->leftjoin('ttipocontrato as tpc', 'tpc.ctipocontrato', '=', 'tpers.ctipocontrato')
            ->where('tpry.carea','=',$careas)
            ->where('tpers.estado','=','ACT')
            ->distinct()
            ->lists(DB::raw('CONCAT(tpers.cpersona,\'-\',tpe.abreviatura,\'-\',tpers.estado) as nombre'));

        return $empleados;

    }

    public function empleadosejecutados($careas,$mes,$anio)
    {
        $empleadosejecutados =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
            ->leftjoin('tpersonadatosempleado as tpde','tpde.cpersona','=','tp.cpersona')
            ->leftjoin('ttipocontrato as tpc', 'tpc.ctipocontrato', '=', 'tpde.ctipocontrato')
            ->where('tpe.carea','=',$careas)
            ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
            ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->distinct()
            ->lists(DB::raw('CONCAT(tp.cpersona,\'-\',tp.abreviatura,\'-\',tpde.estado) as nombre'));
        //dd($empleadosejecutados);

        return $empleadosejecutados;

    }

    public function empleados_lista_real($carea,$mes,$anio) {


        $empleados = $this->empleados($carea);
        $empleadosejecutados = $this->empleadosejecutados($carea,$mes,$anio);

        $emple = array_merge($empleados,$empleadosejecutados);
        $empledos_list = array_unique($emple);

        $empleados_lista = [];

        foreach ($empledos_list as $key => $value) {
            $list = explode('-',$value);
            $e['cpersona'] =  $cpersona = $list[0];
            $e['abreviatura'] =  $abreviatura = $list[1];
            $e['estado'] =  $estado = $list[2];

            array_push($empleados_lista, $e);

        }

        return $empleados_lista;
    }

    public function getCM_ProductividadCargabilidad(Request $request){


        $mes = $this->mes($request->fecha_final);
        $anio = $this->anio($request->fecha_final);
        $carea = $request->careas;
        $mesesNombre =  $this->nombreMeses();
        $mesName = $mesesNombre[$mes-1];

        $empleados = $this->empleados($carea);
        $empleadosejecutados = $this->empleadosejecutados($carea,$mes,$anio);

        $emple = array_merge($empleados,$empleadosejecutados);
        $empledos_list = array_unique($emple);


        //dd($empledos_list);

        $array_fac_nofac_adm = [];

/*        $acumulativofac = 0;
        $acumulativonofac = 0;
        $acumulativoadmin = 0;
        $acumulativointernas = 0;
        $acumuladoreal = 0;
        $acumuladoplaneado = 0;
        $acumuladonolaborables = 0;
        $acumuladonoproductivo = 0;*/

        foreach ($empledos_list as $emp)
        {
            $list = explode('-',$emp);
            $cpersona = $list[0];
            $abreviatura = $list[1];
          //  $horas = $list[3];


            $sumtotal_fac =  $this->horasxempleado($carea,$cpersona,$mes,$anio,1,'externa');
            $sumtotal_nofac =  $this->horasxempleado($carea,$cpersona,$mes,$anio,2,'externa');
            $sumtotal_admin =  $this->horasxempleado($carea,$cpersona,$mes,$anio,3,'externa');
            $sumtotal_internas =  $this->horasxempleado($carea,$cpersona,$mes,$anio,3,'interna');


            // acumulado

            /*$acumulativofac += $sumtotal_fac;
            $acumulativonofac += $sumtotal_nofac;
            $acumulativoadmin += $sumtotal_admin;
            $acumulativointernas += $sumtotal_internas;*/


            //Real
            $real = $sumtotal_fac + $sumtotal_nofac + $sumtotal_admin + $sumtotal_internas ;
            //acumulado

            //$acumuladoreal += $real;

            // Planeado
           // $acumuladoplaneado +=  $horas;
            // Cargabilidad

            // No Laborable

            $actividadesnofacturablesnolaborables =  $this->listaactividadesnofacturablesnolaborables();

            $array_sum_nofacturables_nolaborables = [];

            foreach ($actividadesnofacturablesnolaborables as $anol)
            {

                $horas_por_empleado_actividad_descanso_proyecto_2 = $this->obtener_horasnofacturablesuser($carea,$mes,$anio, $anol['pk'],$cpersona);
                array_push($array_sum_nofacturables_nolaborables,$horas_por_empleado_actividad_descanso_proyecto_2);
            }

            $horas_por_empleado_actividad_descanso_proyecto = array_sum($array_sum_nofacturables_nolaborables);

            $actividadesnolaborables = $this->listaactividadesadministrativasnolaborables();
            $array_sum_nofacturables = [];
            foreach ($actividadesnolaborables as $acnolabo)
            {
                $horas_por_empleado_actividad_admin_nolaborables =  $this->obtener_horasnofacturablesadminuser($carea,$mes,$anio, $acnolabo['pk'],$cpersona  );

                array_push($array_sum_nofacturables,$horas_por_empleado_actividad_admin_nolaborables);
            }

            $nolaborables_user =  array_sum($array_sum_nofacturables) + $horas_por_empleado_actividad_descanso_proyecto;
            // $acumulado

           // $acumuladonolaborables +=$nolaborables_user;


            // No productivo

            $actividadesnofacturablesnoproductivas =  $this->listaactividadesnofacturablesnoproductivas();

            $array_sum_nofacturables_noproductivas = [];
            foreach ($actividadesnofacturablesnoproductivas as $anfnp)
            {
                $horas_por_empleado_actividad_noproductivo_descanso_proyecto_retrabajos = $this->obtener_horasnofacturablesuser($carea,$mes,$anio, $anfnp['pk'],$cpersona);
                array_push($array_sum_nofacturables_noproductivas,$horas_por_empleado_actividad_noproductivo_descanso_proyecto_retrabajos);
            }

            $nopro_user = array_sum($array_sum_nofacturables_noproductivas);

            $horas_por_empleado_actividad_noproductivo_descanso_proyecto_ = $this->obtener_horasnofacturablesuser($carea,$mes,$anio, 4,$cpersona);
            //$horas_por_empleado_actividad_noproductivo_retrabajos_ = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, 11,$cpersona);



            $actividadesnoproductivos = $this->listaactividadesadministrativasnoproductivos();

            $array_sum_noproductivas = [];
            foreach ($actividadesnoproductivos as $acnoprod)
            {
                $horas_por_empleado_actividad_admin_noproductiva =  $this->obtener_horasnofacturablesadminuser($carea,$mes,$anio, $acnoprod['pk'],$cpersona );
                array_push($array_sum_noproductivas,$horas_por_empleado_actividad_admin_noproductiva);
            }

            $noproductiva_user =  array_sum($array_sum_noproductivas) + $nopro_user;
            //$acumuladonoproductivo += $noproductiva_user;

            // Productividad

            $sumfacnofacadmin_porusuario = $sumtotal_fac + $sumtotal_nofac + $sumtotal_admin + $sumtotal_internas ;
            $numerador_produ = $sumfacnofacadmin_porusuario - $noproductiva_user;
            $denominador_prod = $sumfacnofacadmin_porusuario - $nolaborables_user;

            $productividad_user = 0;
            if($denominador_prod  != 0)
            {
                $productividad_user =  round(($numerador_produ/$denominador_prod)*100);
            }

            // Cargabilidad

            $numerador_cargabilidad = $sumtotal_fac + $sumtotal_nofac - $horas_por_empleado_actividad_noproductivo_descanso_proyecto_;
            $denominador_cargabilidad = $real - $nolaborables_user;

            $cargabilidad_user = 0;
            if($denominador_cargabilidad != 0)
            {
                $cargabilidad_user = round(($numerador_cargabilidad/$denominador_cargabilidad)*100);
            }

            if ($productividad_user > 0 || $cargabilidad_user > 0) {

                array_push($array_fac_nofac_adm,[
                    'abreviatura' => $abreviatura,
                    'mes' => $mesName,
                    'productividad_user' =>$productividad_user,
                    'cargabilidad_user' =>$cargabilidad_user,
                ]);

            }

        }

        return $array_fac_nofac_adm;
    }

    public function listaactividadesnofacturablesnolaborables()
    {
        $actividadesadministrativas = DB::table('ttiposnofacturables as ta')
            ->select('ta.ctiponofacturable','ta.descripcion')
            ->where('ta.cactividadnofacturable_nolaborable','=',1)
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->ctiponofacturable;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }
    public function listaactividadesadministrativasnolaborables()
    {
        $actividadesadministrativas = DB::table('tactividad as ta')
            ->select('ta.cactividad','ta.descripcion')
            ->wherenotNull('ta.cactividad_parent')
            ->wherenotNull('ta.cactividad_nolaborable')
            ->orderBy('cactividad','ASC')
            ->where('tipoactividad','=','00002')
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->cactividad;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }
    public function listaactividadesnofacturablesnoproductivas()
    {
        $actividadesadministrativas = DB::table('ttiposnofacturables as ta')
            ->select('ta.ctiponofacturable','ta.descripcion')
            ->where('ta.cactividadnofacturable_noproductiva','=', 2)
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->ctiponofacturable;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }
    public function listaactividadesadministrativasnoproductivos()
    {
        $actividadesadministrativas = DB::table('tactividad as ta')
            ->select('ta.cactividad','ta.descripcion')
            ->wherenotNull('ta.cactividad_parent')
            ->wherenotNull('ta.cactividad_noproductiva')
            ->orderBy('cactividad','ASC')
            ->where('tipoactividad','=','00002')
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->cactividad;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }
    public function obtener_horasnofacturablesuser($careas,$mes,$anio,$actnofact,$user)
    {

        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('ttiposnofacturables as ttnf', 'ttnf.ctiponofacturable', '=', 'tpe.ctiponofacturable')
            ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
            ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cpersona_ejecuta','=',$user)
            ->where('tpe.tipo','=',2)
            ->where('tpe.ctiponofacturable','=',$actnofact)
            ->sum('tpe.horasejecutadas');

        return $horasxempleadoofacturables;

    }
    public function obtener_horasnofacturablesadminuser($careas,$mes,$anio,$cactividad,$user)
    {
        //dd($careas,$semana,$tiponofac,$userarea);
        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tactividad as tact', 'tact.cactividad', '=', 'tpe.cactividad')
            ->where(DB::raw("extract(month from tpe.fplanificado)"),'=',$mes)
            ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cactividad','=',$cactividad)
            ->where('tpe.cpersona_ejecuta','=',$user)
            ->where('tpe.tipo','=',3)
            ->whereNotNull('tpe.cactividad')
            ->sum('tpe.horasejecutadas');

        $sumatoria_horas = 0;
        if($horasxempleadoofacturables != null)
        {
            $sumatoria_horas = $horasxempleadoofacturables;
        }

        return $sumatoria_horas;
    }

}
