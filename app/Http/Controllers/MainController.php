<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Facades\Datatables;
use DB;
use Redirect;
use App\Models\Tproyecto;
use App\Models\Testadoproyecto;
use App\Models\Tproyectoentregable;
use App\Erp\ProyectoSupport;
use App\Erp\EmpleadoSupport;
use Session;
use Response;

class MainController extends Controller
{
    public function panelPrincipal(){
        $usuario=array();
        //nro de proyectos
        $usuario['nroproy'] =DB::table('tproyecto')->count();

        //proyectos por estado
        $tdatos_proy_est =DB::table('tproyecto')
        ->select(DB::raw('cestadoproyecto,count(*) as nro'))
        ->groupBy('cestadoproyecto')
        ->get();
        $usuario['proy_estado']=array();
        foreach($tdatos_proy_est as $est){
            $usuario['proy_estado'][$est->cestadoproyecto]=$est->nro;
        }

        //nro de entregables
        $usuario['nroent'] = DB::table('tproyectoentregables')->count();

        //entregables por estado
        $tdatos_ent_est = DB::table('tproyectoentregables as ep')
        ->join('testadoentregable as es_e','ep.cestadoentregable','=','es_e.cestadoentregable') 
        ->select(DB::raw('ep.cestadoentregable,count(*) as nro'))
        ->groupBy('ep.cestadoentregable')
        ->get();
        foreach($tdatos_ent_est as $est){
            $usuario['ent_estado'][$est->cestadoentregable]=$est->nro;

        }
        $tpersonal_cd = DB::table('tpersona as p')
        ->join('tpersonanaturalinformacionbasica as nat','nat.cpersona','=','p.cpersona')
        ->where('nat.esempleado','=','1')
        ->select('p.cpersona','p.nombre')
        ->get();
        $usuario['personal']=array();
        foreach($tpersonal_cd as $per){
            $tlaboral = DB::table('tpersonadatosempleado as dat')
            ->join('tareas as are','are.carea','=','dat.carea')
            ->where('cpersona','=',$per->cpersona)
            ->where('are.codigo','=','CD') /* especificar el codigo correcto de Control Documentario*/
            ->orderBy('dat.fingreso','DESC')
            ->first();
            if($tlaboral){
                $per_cd['cpersona']=$per->cpersona;
                $per_cd['nombre'] = $per->nombre;
                
                $tdatos_proy_estado =DB::table('tproyecto as p')
                ->join('tproyectoinformacionadicional as pia','p.cproyecto','=','pia.cproyecto')
                ->where('pia.cpersona_cdocumentario','=',$per->cpersona)
                ->select(DB::raw('p.cestadoproyecto,count(*) as nro'))
                ->groupBy('p.cestadoproyecto')
                ->get();
                foreach($tdatos_proy_estado as $esta){
                    $per_cd['proy_'.$esta->cestadoproyecto] = $esta->nro;

                    
                }
                $tdatos_ent_estado = DB::table('tproyectoentregables as ep')
                ->join('tproyectoinformacionadicional as pia','ep.cproyecto','=','pia.cproyecto')
                ->join('testadoentregable as es_e','ep.cestadoentregable','=','es_e.cestadoentregable') 
                ->where('pia.cpersona_cdocumentario','=',$per->cpersona)
                ->select(DB::raw('ep.cestadoentregable,count(*) as nro'))
                ->groupBy('ep.cestadoentregable')
                ->get();
                foreach($tdatos_ent_estado as $esta){
                    $per_cd['ent_'.$esta->cestadoentregable] = $esta->nro;

                    
                }

                $usuario['personal'][count($usuario['personal'])]=$per_cd;


            }
        }

       return view('views.welcome',compact('usuario')); 


    }

    public function listaPrincipal(){
        $objEmp = new EmpleadoSupport(); 
        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp;   
        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = ['' =>''] + $estadoproyecto;

        $proyecto = DB::table('tproyecto')->lists('nombre','cproyecto');
        $proyecto = [''=>''] + $proyecto;
        
        return view('views.listaWelcome',compact('estadoproyecto','personal_gp','proyecto'));
    }


    public function buscarListaPrincipal(Request $request){

        $query=DB::table('tproyecto as p')
        ->join('tpersona as per','per.cpersona','=','p.cpersonacliente')
        ->join('testadoproyecto as est','p.cestadoproyecto','=','est.cestadoproyecto')
        ->leftJoin('tpersona as gte','gte.cpersona','=','p.cpersona_gerente')
        ->leftJoin('tproyectoinformacionadicional as adi','adi.cproyecto','=','p.cproyecto')
        ->leftJoin('tpersona as controlproy','adi.cpersona_cproyecto','=','controlproy.cpersona')
        ->leftJoin('tpersona as controldoc','adi.cpersona_cdocumentario','=','controldoc.cpersona')
        ->select('p.codigo','p.nombre','per.nombre as cliente','gte.nombre as gerente','controlproy.nombre as controlproy','controldoc.nombre as controldoc','est.descripcion','adi.carpeta');
        if (strlen($request->input('cproyecto'))>0){
            $query= $query->where('p.cproyecto','=',$request->input('cproyecto'));
        }
        if (strlen($request->input('estadoproyecto'))>0){
            $query= $query->where('p.cestadoproyecto','=',$request->input('estadoproyecto'));
        }
        if (strlen($request->input('gteproyecto'))>0){
            $query= $query->where('p.cpersona_gerente','=',$request->input('gteproyecto'));
        }                
        $tproyectos=$query->get();
        $listaProy=array();
        foreach($tproyectos as $p){
            $pry['codigo'] = $p->codigo;
            $pry['nombre'] = $p->nombre;
            $pry['cliente'] = $p->cliente;
            $pry['gerente'] = $p->gerente;
            $pry['controlproy'] = $p->controlproy;
            $pry['controldoc'] = $p->controldoc;
            $pry['estado'] = $p->descripcion;
            $pry['carpeta'] = $p->carpeta;
            $listaProy[count($listaProy)]=$pry;

        }

        return view('partials.tableListaProyectosCD',compact('listaProy'));

    }

    public function funcionalidad_trabajando()
    {
        return view("layouts.funcionalidad_trabajando_test");
    }

}
