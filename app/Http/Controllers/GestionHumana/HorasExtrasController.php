<?php
namespace App\Http\Controllers\GestionHumana;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use App\Models\Thorasextras;
use App\Models\Tvacacionessolicitud;
use App\Models\Tvacacionaprobada;
use App\Models\Tusuario;
use App\Models\Tpersona;
use App\Models\Tcargo;
use App\Models\Tarea;
use App\Models\Tpersonadatosempleado;
use App\Models\Tmenupersona;
use App\Models\Erp_vacaciones;
use App\Models\Thorasextrassolicitud;
use App\Models\Thorasextrasaprobadas;
use App\Models\Thorasextrascontrol;
use App\Models\Tproyecto;
use App\Models\Tproyectopersona;
use App\Models\Horas_extras_log;
use Auth;
use DB;
use Response;
use Carbon\Carbon;
use App\Lib\EmailSend;

class HorasExtrasController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
	{
		//Creo array del usuario logueado 
		//$tusuario = Tusuario::find(Auth::user()->cusuario);
		$idusuariologueado = Auth::user()->cpersona;
		//dd($idusuariologueado);
		$tpersona = Tpersona::where('cpersona','=', $idusuariologueado)->first();
		$tpersonadatoempleado = Tpersonadatosempleado::where('cpersona','=', $idusuariologueado)->first();
		$tcargo = Tcargo::where('ccargo','=', $tpersonadatoempleado->ccargo)->first();
		$tarea = Tarea::where('carea','=', $tpersonadatoempleado->carea)->first();
		
		//Si la persona no tiene cargo parent despliegue no podra generar horas extras.
		$primerjefe = $tcargo->cargoparentdespliegue;
		//consigo si posee un cargo parent despliegue si no lo tiene controlo el error
		//dd($tpersonadatoempleado);
		if (is_null($primerjefe)) {
			$primerjefe = 0;
		}

		//Ingreso al Bucle
		//Consigo datos del Jefe Inmediato
		$tcargojefeinmediato = Tcargo::where('ccargo','=', $primerjefe)->first();
		//Salto al Gerente de Proyecto

		$jefedeljefe = $tcargojefeinmediato->ccargo_parent;
		$datosjefeinmediato = Tpersonadatosempleado::where('ccargo','=', $tcargojefeinmediato->ccargo)->first();
		$jefeinmediatopersona = Tpersona::where('cpersona','=', $datosjefeinmediato->cpersona)->first();
		//dd($jefeinmediatopersona);
		//Consigo el jefe del jefe
		$tcargojefejefe = Tcargo::where('ccargo','=', $jefedeljefe)->first();
		//dd($tcargojefejefe);

        $solicitudhorasextras = Thorasextrassolicitud::where('idempleado','=',$idusuariologueado)->get();
        //filtro proyectos por usuarios
        $gp = Tproyecto::where('cestadoproyecto','=','001')->where('cpersona_gerente','=', $idusuariologueado)->orderBy('codigo')->lists('cproyecto');
        $al = Tproyectopersona::where('cpersona','=',$idusuariologueado)->lists('cproyecto');
        if (count($gp)>0){
            $buscoproyecto = Tproyecto::wherein('cproyecto', $gp)->get();
        }
        else{
            $buscoproyecto = Tproyecto::wherein('cproyecto', $al)->get();
        }
        //fin de filtrado por proyectos

        //dd();
        //$buscoproyecto = Tproyecto::where('cestadoproyecto','=','001')->orderBy('codigo')->get();

		//$observacionsolicitud = Thorasextrasaprobadas::where('idhorasextrassolicitudint', '=', '5')->where('accionrealizada','=','Observaciones')->first();
		//dd($solicitudhorasextras[4]->id);
		//dd($solicitudhorasextras);
		for ($i=0; $i < count($solicitudhorasextras); $i++) { 
			$diasemana=DB::table('thorasextrascontrol')
			->select('thorasextrascontrol.dia')
			->where('idiso','=', $solicitudhorasextras[$i]->idfechaiso)
			->first();
			//dd($diasemana->dia);
			$solicitudhorasextras[$i]->diasemana =$diasemana? $diasemana->dia:null;

			if ($solicitudhorasextras[$i]->estadohe == 'Observaciones') {
				$observacionesaplicadas =DB::table('thorasextrasaprobadas')
				->select('thorasextrasaprobadas.observacion','thorasextrasaprobadas.id','idhorasextrassolicitudint')
				->where('idhorasextrassolicitudint','=', $solicitudhorasextras[$i]->id)
				->where('accionrealizada','=','Observaciones')
				->first();
				$solicitudhorasextras[$i]->observacion = $observacionesaplicadas->observacion;	
			} else {
				$solicitudhorasextras[$i]->observacion = 'En Observacion';	
			}
            $nombrpro=DB::table('tproyecto')
                ->where('cproyecto', '=', $solicitudhorasextras[$i]->cproyecto)
                ->first();
                $solicitudhorasextras[$i]->nombre_proyecto = $nombrpro->nombre;

		    //lardin validar estado de aprobacion de cada registro
            $shoras=Horas_extras_log::where('id_solicitud_horas_extras','=', $solicitudhorasextras[$i]->id)
                ->orderby('updated_at', 'Desc')
                ->first();

            $estadoh='Pendiete de aprobación';
            $persona_aprobada="no aprobado";
            if (count($shoras)>0){
                $estadoh = $shoras->estado_actual;
                $persona=Tpersona::where('cpersona','=',$shoras->persona_aprobada)
                ->first();
                $persona_aprobada=$persona->abreviatura;
            }
            if($solicitudhorasextras[$i]->estadohe =='Solicitado'){
                //$persona_aprobada="En espera";

            }
            $solicitudhorasextras[$i]->estado_actual =$estadoh;
            //$solicitudhorasextras[$i]->faprobacion= $shoras->created_at;
            $solicitudhorasextras[$i]->nombreaprobador=$persona_aprobada;
            //dd($shoras);
            //fin
		    }
		    //dd($solicitudhorasextras);

		
		$vauser = array();
		$vauser = [
			'iduser' => Auth::user()->cusuario,
			'idtpersonalogin' => $tpersona->cpersona,
			'tipopersona' => $tpersona->ctipopersona,
			'identificacion' =>  $tpersona->identificacion,
			'nombreusuario' => $tpersona->nombre,
			'abreviaturausuario' => $tpersona->abreviatura,
			'userdondetrabaja' => $tarea->descripcion,
			'cargouser' => $tcargo->descripcion,
			'niveldeaprobacion' => '2',
			'idjefeinmediato' => $jefeinmediatopersona->cpersona,
			'jefeinmediato' => $jefeinmediatopersona->nombre,
			'jefeinmediatoabreviatura' => $jefeinmediatopersona->abreviatura,
		];

		//dd($solicitudhorasextras);
		//dd($solicitudes);
		//dd($vauser);
		// $view = view('ghumana.printvaca');
		// $pdf=\App::make('dompdf.wrapper');
        // $pdf->loadHTML($view);
        // $pdf->setPaper('A4','portrait');
        // return $pdf->stream();

        $sinerror = 0;

		 return view('ghumana.horasextras',[
		 	'vauser' => $vauser,
		 	'solicitudes' => $solicitudhorasextras,
		 	'proyectos' => $buscoproyecto,
		 	'errorcargoparentdespliegue' => $sinerror
		 ]);

		 //return view('email.hextrasemail');
	}


	public function SolicitudHorasExtras(Request $request)
	{
	    //dd($request->all());
		//Transformo la fecha para sacar el dia de la semana
		$date = $request->fhorasextras;
		$transformofecha = explode('/', $date);
		$nuevafecha = $transformofecha[2].'-'.$transformofecha[1].'-'.$transformofecha[0];
		$dt = Carbon::parse($nuevafecha)->dayOfWeek;
		//$dt =  $request->fhorasextras;

		$jefeproyecto = Tproyecto::where('cproyecto', '=', $request->proyectoid)->first();
		//traigo el usuario logueado para encotrar el jefe inmediato y colocar el correo.
		$tusuario = Tusuario::find(Auth::user()->cusuario);
		$idusuariologueado = $tusuario->cpersona;
		//dd($idusuariologueado);
		$tpersona = Tpersona::where('cpersona','=', $idusuariologueado)->first();
		$tpersonadatoempleado = Tpersonadatosempleado::where('cpersona','=', $idusuariologueado)->first();
		$tcargo = Tcargo::where('ccargo','=', $tpersonadatoempleado->ccargo)->first();
		$tarea = Tarea::where('carea','=', $tpersonadatoempleado->carea)->first();
		//Si la persona no tiene cargo parent despliegue no podra generar horas extras.
		$primerjefe = $tcargo->cargoparentdespliegue;
		if (is_null($primerjefe)) {
			$primerjefe = 0;
		}else{

			//tpersonas encuentro nombres 
			$solicitante = Tpersona::where('cpersona','=', $request->idempleado)->first();
			$jefeinmediato = Tpersonadatosempleado::where('ccargo','=', $primerjefe)->first();
			// $jefesuperiror = Tpersona::where('cpersona','=', $request->idjefesuperior)->first();
			$diasemana = Thorasextrascontrol::where('idiso','=', $dt)->first();
			//tusuario armo correo
			$tusuarioempleado = Tusuario::where('cpersona','=', $request->idempleado)->first();
			$tusuariojefeinmediato = Tusuario::where('cpersona','=', $jefeinmediato->cpersona)->first();
			//dd($tusuariojefeinmediato);
			$tusuariojefesuperiror = Tusuario::where('cpersona','=', $jefeproyecto->cpersona_gerente)->first();
			//$correoempleado = $tusuarioempleado->nombre.'@anddes.com';
			$correojefeinmediato =  'fabio.colan@anddes.com'; //$tusuariojefeinmediato->nombre.'@anddes.com'; // 'fabio.colan@anddes.com';
			$correojefesuperior  =  'pedro.lopez@anddes.com'; //$tusuariojefesuperiror->nombre.'@anddes.com'; // 'pedro.lopez@anddes.com'; 
			//dd($solicitante['dia']);
			$dataemail =[
				'nombresolicitante'=> $solicitante['abreviatura'],
				'cantidadhoras'=>  $request->numerodehoras,
				'diasemana'=> $diasemana['dia'],
				'fechaextras'=> $request->fhorasextras,
				'correojefeinmediato' => $correojefeinmediato,
				'motivo' => $request->motivohorasextras,
				'correojefesuperior' => $correojefesuperior,
				'titulocorreo' => 'Solicitud de Horas Extras',
			];

			//dd($dataemail['nombresolicitante']);
			//return view('email.hextrasemail',['dataemail' =>$dataemail]);
			//$result = EmailSend::horasextras($dataemail);
		}
		//consigo si posee un cargo parent despliegue si no lo tiene controlo el error
		//Ingreso al Bucle
		//Consigo datos del Jefe Inmediato 
		$tcargojefeinmediato = Tcargo::where('ccargo','=', $primerjefe )->first();
		$datosjefeinmediato = Tpersonadatosempleado::where('ccargo','=', $tcargojefeinmediato->ccargo)->first();
		//dd($datosjefeinmediato);
		//dd($request->all());
		$thorasextrassolicitud = new Thorasextrassolicitud;
		$thorasextrassolicitud->fechahorasextras =  $request->fhorasextras;
		$thorasextrassolicitud->horassolicitadas = $request->numerodehoras;
		$thorasextrassolicitud->estadohe = 'Solicitado';
		$thorasextrassolicitud->cproyecto = $request->proyectoid;
		$thorasextrassolicitud->motivo = $request->motivohorasextras;
		$thorasextrassolicitud->idfechaiso = $dt;
		$thorasextrassolicitud->idempleado = $request->idempleado;
		$thorasextrassolicitud->idjefeinmediato = $request->idjefeinmediato;
		$thorasextrassolicitud->idgerenteproyecto = $jefeproyecto->cpersona_gerente;
		$thorasextrassolicitud->aprobado = '0';
		$thorasextrassolicitud->save();


        //dd($jefeproyecto);

		return Redirect::route('horasextrasindex');

	}


	//Entro a la vista de los jefes 
	public function AprueboSolicitudHorasExtras()
	{
		$tusuario = Tusuario::find(Auth::user()->cusuario);
		$idusuariologueado = $tusuario->cpersona;
		$tpersona = Tpersona::where('cpersona','=', $idusuariologueado)->first();
		$tpersonadatoempleado = Tpersonadatosempleado::where('cpersona','=', $idusuariologueado)->first();
		$tcargo = Tcargo::where('ccargo','=', $tpersonadatoempleado->ccargo)->first();
		$tarea = Tarea::where('carea','=', $tpersonadatoempleado->carea)->first();


		$solicitudes = Thorasextrassolicitud::join('tpersonadatosempleado','thorasextrassolicitud.idempleado','=','tpersonadatosempleado.cpersona')
                      ->join('tcargos','tpersonadatosempleado.ccargo','=','tcargos.ccargo')
                      ->join('tproyecto','thorasextrassolicitud.cproyecto','=','tproyecto.cproyecto')
                      ->where('tcargos.ccargo_parent','=', $tcargo->ccargo)
                      ->where('aprobado','=','0')
                      ->where('estadohe','<>', 'Observaciones')
                      ->orWhere('tproyecto.cpersona_gerente', '=', $tusuario->cpersona)
                      ->where('aprobado','=','1')
                      ->where('estadohe','<>', 'Observaciones')
                      ->get();

        //$solicitudes = Thorasextrassolicitud::where('idjefeinmediato','=', $idusuariologueado)->where('aprobado','=','0')->where('estadohe','<>', 'Observaciones')->orWhere('idgerenteproyecto', '=', $idusuariologueado)->where('aprobado','=','1')->where('estadohe','<>', 'Observaciones')->get();

        $solicitudesporaprobadas = Thorasextrassolicitud::join('tpersona', 'thorasextrassolicitud.idempleado', '=', 'tpersona.cpersona')->where('idjefeinmediato','=', $idusuariologueado)->whereIn('aprobado',array(1,2))->orWhere('idgerenteproyecto', '=', $idusuariologueado)->where('aprobado','=','2')->get();

        $solicitudesobservadas = Thorasextrassolicitud::join('tpersona', 'thorasextrassolicitud.idempleado', '=', 'tpersona.cpersona')->where('thorasextrassolicitud.estadohe','=', 'Observaciones')->get();

        for ($i=0; $i < count($solicitudesobservadas); $i++) {
            $observacion = Thorasextrasaprobadas::where('idhorasextrassolicitudint','=',$solicitudesobservadas[$i]->id)->first();
            $dia = Thorasextrascontrol::where('idiso','=', $solicitudesobservadas[$i]->idfechaiso)->first();
            $solicitudesobservadas[$i]->observacion = $observacion->observacion;
            $solicitudesobservadas[$i]->dia = $dia->dia;
        }
        for ($i=0; $i < count($solicitudesporaprobadas); $i++) {
            $dia = Thorasextrascontrol::where('idiso','=', $solicitudesporaprobadas[$i]->idfechaiso)->first();
            $solicitudesporaprobadas[$i]->dia = $dia->dia;
        }
        for ($i=0; $i < count($solicitudes); $i++) {
            $nombreempleado = Tpersona::where('cpersona','=', $solicitudes[$i]->idempleado)->first();
            $nombreproyecto = Tproyecto::where('cproyecto','=', $solicitudes[$i]->cproyecto)->first();
            $solicitudes[$i]->nombreempleado = $nombreempleado->abreviatura;
            $dia = Thorasextrascontrol::where('idiso','=', $solicitudes[$i]->idfechaiso)->first();
            $solicitudes[$i]->dia = $dia->dia;
            $solicitudes[$i]->proyecto = $nombreproyecto->nombre;
        }

        //dd($solicitudesobservadas);

        if (is_null($solicitudes)) {
            $solicitudes ='';
        }

        //dd($solicitudes);
        return view('ghumana.horasextrasaprueba',[
			'solicitudes' => $solicitudes,
			'solicitudesporaprobadas' => $solicitudesporaprobadas,
			'solicitudesobservadas' => $solicitudesobservadas
		]);

	}

	public function CambioStatusHorasExtras(Request $request)
	{
		//dd($request->all());
		$tusuario = Tusuario::find(Auth::user()->cusuario);
		$idusuariologueado = $tusuario->cpersona;
		$tpersona = Tpersona::where('cpersona','=', $idusuariologueado)->first();
		$tpersonadatoempleado = Tpersonadatosempleado::where('cpersona','=', $idusuariologueado)->first();
		$tcargo = Tcargo::where('ccargo','=', $tpersonadatoempleado->ccargo)->first();
		//dd($request->proyectoid);
		$solicitudes = Thorasextrassolicitud::where('id', '=', $request->idhorasextrassolicitud)->first();
		$jefeproyecto = Tproyecto::where('cproyecto', '=', $request->proyectoid)->first();
		$estadoanterior = $solicitudes->estadohe;
		//$actualizogp = 

		//dd($jefeproyecto);

		if($request->rechazar ==='true'){
			//sdd('es true');
			$horasextrasaprobadas = new Thorasextrasaprobadas;
			$horasextrasaprobadas->idhorasextrassolicitudint = $request->idhorasextrassolicitud;
			$horasextrasaprobadas->useraccion = $tusuario->cpersona;
			$horasextrasaprobadas->accionrealizada = 'Observaciones';
			$horasextrasaprobadas->observacion = $request->observacionrechazohorasextras;
			$horasextrasaprobadas->thorasextras = $solicitudes->horassolicitadas;
			$horasextrasaprobadas->save();
			$solicitudes->estadohe = 'Observaciones';
			if (is_null($solicitudes->idjefeinmediato)) {
				$solicitudes->idjefeinmediato = $tusuario->cpersona;
			} else {
				$solicitudes->idgerenteproyecto =  $tusuario->cpersona;
			}
			$solicitudes->aprobado = '0';
			$solicitudes->save();

		}elseif($request->rechazar ==='false'){
			if($solicitudes->aprobado == '0'){
				if($jefeproyecto->cpersona_gerente =='0'){
					$horasextrasaprobadas = new Thorasextrasaprobadas;
					$horasextrasaprobadas->idhorasextrassolicitudint = $request->idhorasextrassolicitud;
					$horasextrasaprobadas->useraccion = $tusuario->cpersona;
					$horasextrasaprobadas->accionrealizada = 'Aprobado JS';
					$horasextrasaprobadas->thorasextras = $solicitudes->horassolicitadas;
					$horasextrasaprobadas->save();
					$solicitudes->estadohe = 'Aprobado JS';
					$solicitudes->aprobado = '2';
					$solicitudes->idgerenteproyecto =  $tusuario->cpersona;
					$solicitudes->save();
				}else{
					$horasextrasaprobadas = new Thorasextrasaprobadas;
					$horasextrasaprobadas->idhorasextrassolicitudint = $request->idhorasextrassolicitud;
					$horasextrasaprobadas->useraccion = $tusuario->cpersona;
					$horasextrasaprobadas->accionrealizada = 'Aprobado JI';
					$horasextrasaprobadas->thorasextras = $solicitudes->horassolicitadas;
					$horasextrasaprobadas->save();
					$solicitudes->estadohe = 'Aprobado JI';
					$solicitudes->aprobado = '1';
					$solicitudes->idjefeinmediato = $tusuario->cpersona;
					$solicitudes->save();
				};
			}elseif($solicitudes->aprobado == '1'){
				$horasextrasaprobadas = new Thorasextrasaprobadas;
				$horasextrasaprobadas->idhorasextrassolicitudint = $request->idhorasextrassolicitud;
				$horasextrasaprobadas->useraccion = $tusuario->cpersona;
				$horasextrasaprobadas->accionrealizada = 'Aprobado JS';
				$horasextrasaprobadas->thorasextras = $solicitudes->horassolicitadas;
				$horasextrasaprobadas->save();
				$solicitudes->estadohe = 'Aprobado JS';
				$solicitudes->aprobado = '2';
				$solicitudes->idgerenteproyecto = $tusuario->cpersona;
				$solicitudes->save();
			}
		}
		//lardin estado de horas extras modficar
		$estadoactual =$solicitudes->estadohe;
		$horaslog = new Horas_extras_log();
		$horaslog->persona_aprobada= Auth::user()->cpersona;
        $horaslog->estado_actual=$estadoactual;
        $horaslog->estado_anterior=$estadoanterior;
        $horaslog->id_solicitud_horas_extras=$solicitudes->id;
		$horaslog->save();
        //dd($horaslog);
        //fin de codigo

				
		$solicitudes = Thorasextrassolicitud::join('tpersonadatosempleado','thorasextrassolicitud.idempleado','=','tpersonadatosempleado.cpersona')->join('tcargos','tpersonadatosempleado.ccargo','=','tcargos.ccargo')->join('tproyecto','thorasextrassolicitud.cproyecto','=','tproyecto.cproyecto')->where('tcargos.cargoparentdespliegue','=', $tcargo->ccargo)->where('aprobado','=','0')->where('estadohe','<>', 'Observaciones')->orWhere('tproyecto.cpersona_gerente', '=', $tusuario->cpersona)->where('aprobado','=','1')->where('estadohe','<>', 'Observaciones')->get();

		$solicitudesporaprobadas = Thorasextrassolicitud::join('tpersona', 'thorasextrassolicitud.idempleado', '=', 'tpersona.cpersona')->where('idjefeinmediato','=', $idusuariologueado)->whereIn('aprobado',array(1,2))->orWhere('idgerenteproyecto', '=', $idusuariologueado)->where('aprobado','=','2')->get();

		$solicitudesobservadas = Thorasextrassolicitud::join('tpersona', 'thorasextrassolicitud.idempleado', '=', 'tpersona.cpersona')->where('thorasextrassolicitud.estadohe','=', 'Observaciones')->get();
		
		for ($i=0; $i < count($solicitudesobservadas); $i++) { 
			$observacion = Thorasextrassolicitud::where('id','=',$solicitudesobservadas[$i]->id)->first();
			$dia = Thorasextrascontrol::where('idiso','=', $solicitudesobservadas[$i]->idfechaiso)->first();
			$solicitudesobservadas[$i]->observacion = $observacion->observacion;
			$solicitudesobservadas[$i]->dia = $dia->dia;
		}
		for ($i=0; $i < count($solicitudesporaprobadas); $i++) { 
			$dia = Thorasextrascontrol::where('idiso','=', $solicitudesporaprobadas[$i]->idfechaiso)->first();
			$solicitudesporaprobadas[$i]->dia = $dia->dia;
		}
		for ($i=0; $i < count($solicitudes); $i++) { 
			$nombreempleado = Tpersona::where('cpersona','=', $solicitudes[$i]->idempleado)->first();
			$solicitudes[$i]->nombre = $nombreempleado->abreviatura;
			$dia = Thorasextrascontrol::where('idiso','=', $solicitudes[$i]->idfechaiso)->first();
			$solicitudes[$i]->dia = $dia->dia;
		}

		//dd($solicitudes);
		if (is_null($solicitudes)) {
			$solicitudes ='';
		}

		//dd(count($solicitudes));

		return view('ghumana.horasextrasaprueba',[
			'solicitudes' => $solicitudes,
			'solicitudesporaprobadas' => $solicitudesporaprobadas,
			'solicitudesobservadas' => $solicitudesobservadas
		]);
	}

	public function ListadoHorasExtras()
	{
		//$solicitudes = Thorasextrassolicitud::where('','=')->get();
		//$solicitudesaprobadas = Thorasextrassolicitud::where()->get();
		//$solicitudesobservadas = Thorasextrassolicitud::where()->get();
		return view('ghumana.listadohorasextras');	
	}
	
	//verificar maketa del email
	public function controlindex()
	{
		return view('email.hextrasemail');
	}

	//mantenedor de tabla de horas 
	public function control()
	{
		
	}

	public function PrintPdfHorasExtras(Request $request,$id)
	{
		//dd($id);
		$solicitud = Thorasextrassolicitud::where('id','=',$id)->first();
		//dd($solicitud);
        $empleado = Tpersona::where('cpersona','=', $solicitud->idempleado)->first();
		$tdatosempleado = Tpersonadatosempleado::where('cpersona', '=', $solicitud->idempleado)->first();
		$areaempleado = Tarea::where('carea', '=', $tdatosempleado->carea)->first();
		//dd($areaempleado);
		$jefeinmediato = Tpersona::where('cpersona','=', $solicitud->idjefeinmediato)->first();
		$gerenteproyecto =  Tpersona::where('cpersona','=', $solicitud->idgerenteproyecto)->first();
		$proyecto =  Tproyecto::where('cproyecto','=', $solicitud->cproyecto)->first();
		$jefeproyecto = tpersona::where('cpersona','=', $proyecto->cpersona_gerente)->first();
		//dd($jefeproyecto);
		$now = $solicitud->created_at;
		$datoscomprobantevaca = array();
		$datoscomprobantevaca = [
			'nombreempleado'	=> $empleado->nombre,
			'firmacolaborador' 	=> $empleado->nombre,
			'firmajefearea' 	=> $jefeinmediato->nombre,
			'firmagerentearea' 	=> $gerenteproyecto->nombre,
			'codigoproyecto' 	=> $proyecto->codigo,
			'nombreproyecto'	=> $proyecto->descripcion,
			'motivo'			=> $solicitud->motivo,
			'fechahorasextras' 	=> $solicitud->fechahorasextras,
			'horassolicitadas' 	=> $solicitud->horassolicitadas,
			'gerenteproyecto' 	=> $jefeproyecto->abreviatura,
			'departamento' 		=> $areaempleado->descripcion,
			'fdesde' 			=> $solicitud->fdesde,
			'fhasta'			=> $solicitud->fhasta,
			'diassolicitados' 	=> $solicitud->diassolicitados,
			'periodovacacional' => 'Exclusivo Administracion',
			'fechaactual' 		=> $now->format('d/m/Y H:i:s'),
		];

		//dd($datoscomprobantevaca);
		//dd($vauser);

		$view = view('ghumana.printhorasextras',[
		 	'datoscomprovante' => $datoscomprobantevaca
		]);

		$pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','portrait');
        return $pdf->stream();
	}
	//funcion editar horas extras


    public function updateSoli($id){
        $solicitud = Thorasextrassolicitud::where('id','=',$id)->first();

        $idusuariologueado = Auth::user()->cpersona;
        //$proyecto =  Tproyecto::where('cproyecto','=', $solicitud->cproyecto)->first();
        $gp = Tproyecto::where('cestadoproyecto','=','001')->where('cpersona_gerente','=', $idusuariologueado)->orderBy('codigo')->lists('cproyecto');
        $al = Tproyectopersona::where('cpersona','=',$idusuariologueado)->lists('cproyecto');
        if (count($gp)>0 ){
            $buscoproyecto = Tproyecto::wherein('cproyecto', $gp)->get();
        }
        else{
            $buscoproyecto = Tproyecto::wherein('cproyecto', $al)->get();
        }
        //dd($buscoproyecto);
        //dd($solicitud);
        //return $solicitud;
        return view('ghumana.edithorasextras',[
            'solicitud'=>$solicitud,
            'proyectos'=>$buscoproyecto
        ]);
    }

    public function editsolicitud(Request $request){
        //dd($request->all());
        $solicitud_inicial = Thorasextrassolicitud::where('id','=',$request->idsolicitud)->first();
        $solicitud = Thorasextrassolicitud::where('id','=',$request->idsolicitud)->update([
            'horassolicitadas'=>$request->numerodehoras,
            'cproyecto'=>$request->proyectoid,
            'motivo'=>$request->motivohorasextras,
            'estadohe'=>'Solicitado'
        ]);

        //lardin estado de horas extras modficar

        $horaslog = new Horas_extras_log();
        $horaslog->persona_aprobada= Auth::user()->cpersona;
        $horaslog->estado_actual='Solicitado';
        $horaslog->estado_anterior=$solicitud_inicial->estadohe;
        $horaslog->id_solicitud_horas_extras=$solicitud_inicial->id;
        $horaslog->save();


        return Redirect::route('horasextrasindex');
    }
    //fin

}


