<?php
namespace App\Http\Controllers\GestionHumana;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use App\Models\Erp_vacaciones;
use App\Models\Tvacacionessolicitud;
use App\Models\Tvacacionaprobada;
use App\Models\Tusuario;
use App\Models\Tpersona;
use App\Models\Tcargo;
use App\Models\Tarea;
use App\Models\Tpersonadatosempleado;
use Auth;
use DB;
use PDF;

class VacacionesController extends Controller
{
	//public route
	public function index()
	{
		//Creo array del usuario logueado
		$tusuario = Tusuario::find(Auth::user()->cusuario);
		$idusuariologueado = $tusuario->cpersona;
		$tpersona = Tpersona::where('cpersona','=', $idusuariologueado)->first();
		$tpersonadatoempleado = Tpersonadatosempleado::where('cpersona','=', $idusuariologueado)->first();
		$tcargo = Tcargo::where('ccargo','=', $tpersonadatoempleado->ccargo)->first();
		//dd($tcargo);
		$tarea = Tarea::where('carea','=', $tpersonadatoempleado->carea)->first();

		 $primerjefe = $tcargo->ccargo_parent;
		 if (is_null($primerjefe)) {
		 	$primerjefe = 0;
		 }
		//dd($primerjefe) ;
		//$primerjefe = $tcargo->cargoparentdespliegue;

		//Consigo datos del Jefe Inmediato
		$tcargojefeinmediato = Tcargo::where('ccargo','=', $primerjefe)->first();
		$jefedeljefe = $tcargojefeinmediato->ccargo_parent;
		$datosjefeinmediato = Tpersonadatosempleado::where('ccargo','=', $tcargojefeinmediato->ccargo)->first();
		$jefeinmediatopersona = Tpersona::where('cpersona','=', $datosjefeinmediato->cpersona)->first();
		//dd($jefeinmediatopersona);
		//Consigo el jefe del jefe 
		$tcargojefejefe = Tcargo::where('ccargo','=', $jefedeljefe)->first();
		//dd($tcargojefejefe);
		if(is_null($tcargojefejefe)){ //Si no tiene jefe superior o es gerencia seteo un array vacio para crear funcion tenaria que controle la vista
			$datosjefejefe = 0;
			$jefejefearea = array();
			$jefejefetpersona = array();
			$jefejefearea['descripcion']  ='NoBoss';
			$jefejefetpersona['cpersona']  ='NoBoss';
			$jefejefetpersona['nombre'] ='NoBoss';
			$jefejefetpersona['abreviatura'] ='NoBoss';
		}else{
			$datosjefejefe = Tpersonadatosempleado::where('ccargo','=', $tcargojefejefe->ccargo)->first();
			$jefejefearea = Tarea::where('carea','=', $datosjefejefe->carea)->first();
			$jefejefetpersona = Tpersona::where('cpersona','=', $datosjefejefe->cpersona)->first();
		}

		$vacaciones = Erp_vacaciones::where('dni','=',$tpersona->identificacion)->where('tiposaldo','=','Vacaciones_Pendientes')->get();
		$anticipadas = Erp_vacaciones::where('dni','=',$tpersona->identificacion)->where('tiposaldo','=','Vacaciones_Adelantar')->get();

		//dd($vacaciones);

		$vauser = array();
		$vauser = [
			'iduser' => Auth::user()->cusuario,
			'idtpersonalogin' => $tpersona->cpersona,
			'tipopersona' => $tpersona->ctipopersona,
			'identificacion' =>  $tpersona->identificacion,
			'nombreusuario' => $tpersona->nombre,
			'abreviaturausuario' => $tpersona->abreviatura,
			'userdondetrabaja' => $tarea->descripcion,
			'cargouser' => $tcargo->descripcion,
			'niveldeaprobacion' => $tcargo->nivel_aprobacion_vacaciones,
			'idjefeinmediato' => ($jefeinmediatopersona->cpersona == 'NoBoss')?0:$jefeinmediatopersona->cpersona,
			'jefeinmediato' => $jefeinmediatopersona->nombre,
			'jefeinmediatoabreviatura' => $jefeinmediatopersona->abreviatura,
			'superjefearea'=>  is_null($tcargojefejefe) ? $jefejefearea['descripcion'] : $jefejefearea->descripcion,
			'idsuperjefe' =>  is_null($tcargojefejefe) ? $jefejefetpersona['cpersona'] : $jefejefetpersona->cpersona,
			'superjefe' =>  is_null($tcargojefejefe) ? $jefejefetpersona['nombre'] : $jefejefetpersona->nombre,
			'superabreviatura' =>  is_null($tcargojefejefe) ? $jefejefetpersona['abreviatura'] : $jefejefetpersona->abreviatura 
		];

		$solicitudes = Tvacacionessolicitud::leftjoin('tvacacionaprobada', 'tvacacionessolicitud.id', '=','tvacacionaprobada.idsolicitudint')->select('tvacacionessolicitud.id as idsolicitud','tvacacionessolicitud.fdesde','tvacacionessolicitud.fhasta','tvacacionessolicitud.diassolicitados','tvacacionessolicitud.estadova','tvacacionessolicitud.tipovaca','tvacacionaprobada.observacion')->where('idempleado','=',$idusuariologueado)->groupBy('tvacacionessolicitud.id','tvacacionessolicitud.fdesde','tvacacionessolicitud.fhasta','tvacacionessolicitud.diassolicitados','tvacacionessolicitud.estadova','tvacacionessolicitud.tipovaca','tvacacionaprobada.observacion')->get();

	
		//dd($solicitudes->all(), $vauser);
		//dd($vauser);
		// $view = view('ghumana.printvaca');
		// $pdf=\App::make('dompdf.wrapper');
        // $pdf->loadHTML($view);
        // $pdf->setPaper('A4','portrait');
        // return $pdf->stream();

		 return view('ghumana.vacaciones',[
		 	'vauser' => $vauser,
		 	'vacaciones'=> $vacaciones,
		 	'anticipadas' => $anticipadas,
		 	'solicitudes' => $solicitudes
		 ]);
	}

	public function SolicitudVacaciones(Request $request)
	{
		
		//Actualizo el saldo temporal
		$saldo_temp = Erp_vacaciones::where('tiposaldo','=',$request->tipovaca)->first();
		//dd(is_null($saldo_temp));
	    $saldo_temp->saldo_temp  = (is_null($saldo_temp->saldo_temp)? 0:$saldo_temp->saldo_temp) + $request->diassolicitados;
	    $saldo_temp->update();

		if ($request->idjefesuperior =='NoBoss') {
			$idjefesuperior =  0;
		}else{
			$idjefesuperior =   $request->idjefesuperior;
		}

		//dd($idjefesuperior);
	    $saldoactualizado = Erp_vacaciones::where('tiposaldo','=',$request->tipovaca)->first();

		$vacasolicitud = new Tvacacionessolicitud;
		$vacasolicitud->totalvacionesacumuladas =  $saldoactualizado->saldo - $saldoactualizado->saldo_temp;
		$vacasolicitud->diassolicitados = $request->diassolicitados;
		$vacasolicitud->estadova = $request->estadova;
		$vacasolicitud->tipovaca = $request->tipovaca;
		$vacasolicitud->fdesde = $request->fdesdeva;
		$vacasolicitud->fhasta = $request->fhastava;
		$vacasolicitud->idempleado = $request->idempleado;
		$vacasolicitud->idjefeinmediato = $request->idjefeinmediato;
		$vacasolicitud->idjefesuperior = $idjefesuperior;
		$vacasolicitud->aprobado = '0';
		$vacasolicitud ->save();
		
		return Redirect::route('vacacionesindex');
	}


	public function AprueboSolicitudVacaciones()
	{
		//dd(Auth::user()->cusuario);
		$tusuario = Tusuario::find(Auth::user()->cusuario);
		$idusuariologueado = $tusuario->cpersona;
		$tpersona = Tpersona::where('cpersona','=', $idusuariologueado)->first();
		$tpersonadatoempleado = Tpersonadatosempleado::where('cpersona','=', $idusuariologueado)->first();
		$tcargo = Tcargo::where('ccargo','=', $tpersonadatoempleado->ccargo)->first();
		$tarea = Tarea::where('carea','=', $tpersonadatoempleado->carea)->first();

		$solicitudes = Tvacacionessolicitud::join('tpersona', 'tvacacionessolicitud.idempleado', '=', 'tpersona.cpersona')->where('idjefeinmediato','=', $idusuariologueado)->where('aprobado','=','0')->where('estadova','<>', 'Observaciones')->orWhere('idjefesuperior', '=', $idusuariologueado)->where('aprobado','=','1')->where('estadova','<>', 'Observaciones')->get();

		//dd($solicitudes);

		$solicitudesporaprobadas = Tvacacionessolicitud::join('tpersona', 'tvacacionessolicitud.idempleado', '=', 'tpersona.cpersona')->where('idjefeinmediato','=', $idusuariologueado)->whereIn('aprobado',array(1,2))->orWhere('idjefesuperior', '=', $idusuariologueado)->where('aprobado','=','2')->get();

		$solicitudesobservadas = Tvacacionessolicitud::join('tpersona', 'tvacacionessolicitud.idempleado', '=', 'tpersona.cpersona')->where('estadova','=', 'Observaciones')->get();
		
		for ($i=0; $i < count($solicitudesobservadas); $i++) { 
			$observacion = Tvacacionaprobada::where('idsolicitudint','=',$solicitudesobservadas[$i]->id)->first();
			$solicitudesobservadas[$i]->observacion = $observacion->observacion;
		}

		//dd($solicitudesobservadas);
		if (is_null($solicitudes)) {
			$solicitudes ='';
		}
		return view('ghumana.vacacionesaprueba',[
			'solicitudes' => $solicitudes,
			'solicitudesporaprobadas' => $solicitudesporaprobadas,
			'solicitudesobservadas' => $solicitudesobservadas
		]);

	}

	public function CambioStatusVacaciones(Request $request)
	{
		//dd($request->all());
		$tusuario = Tusuario::find(Auth::user()->cusuario);
		$idusuariologueado = $tusuario->cpersona;
		$tpersona = Tpersona::where('cpersona','=', $idusuariologueado)->first();
		$tpersonadatoempleado = Tpersonadatosempleado::where('cpersona','=', $idusuariologueado)->first();
		$tcargo = Tcargo::where('ccargo','=', $tpersonadatoempleado->ccargo)->first();

		$solicitudes = Tvacacionessolicitud::where('id', '=', $request->idvacacionessolicitud)->first();

		//dd($solicitudes);

		if($request->rechazar ==='true'){
			//dd('es true');
			$vacacionaprobada = new Tvacacionaprobada;		
			$vacacionaprobada->idsolicitudint = $request->idvacacionessolicitud;
			$vacacionaprobada->useraccion = $tusuario->cpersona;
			$vacacionaprobada->accionrealizada = 'Observaciones';
			$vacacionaprobada->observacion = $request->observacionrechazo;
			$vacacionaprobada->save();
			$solicitudes->estadova = 'Observaciones';
			$solicitudes->aprobado = '0';
			$solicitudes->save();
			$idempleado = Tpersona::where('cpersona','=', $solicitudes->idempleado)->first();
			$erp_vacaciones = Erp_vacaciones::where('dni','=', $idempleado->identificacion)->where('tiposaldo','=',$solicitudes->tipovaca)->first();
			$erp_vacaciones->saldo = $erp_vacaciones->saldo + $solicitudes->diassolicitados;
			$erp_vacaciones->save();

		}elseif($request->rechazar ==='false'){
			if($solicitudes->aprobado == '0'){

				$vacacionaprobada = new Tvacacionaprobada;		
				$vacacionaprobada->idsolicitudint = $request->idvacacionessolicitud;
				$vacacionaprobada->useraccion = $tusuario->cpersona;
				$vacacionaprobada->accionrealizada = 'Aprobado JI';
				$vacacionaprobada->save();
				$solicitudes->estadova = 'Aprobado JI';
				$solicitudes->aprobado = '1';
				$solicitudes->save();

			}elseif($solicitudes->aprobado == '1'){
				$vacacionaprobada = new Tvacacionaprobada;		
				$vacacionaprobada->idsolicitudint = $request->idvacacionessolicitud;
				$vacacionaprobada->useraccion = $tusuario->cpersona;
				$vacacionaprobada->accionrealizada = 'Aprobado JS';
				$vacacionaprobada->save();
				$solicitudes->estadova = 'Aprobado JS';
				$solicitudes->aprobado = '2';
				$solicitudes->save();

			}
		}

		if($tcargo->nivel_aprobacion_vacaciones == '1' && $solicitudes->aprobado == '1'){

			$idempleado = Tpersona::where('cpersona','=', $solicitudes->idempleado)->first();
			$erp_vacaciones = Erp_vacaciones::where('dni','=', $idempleado->identificacion)->where('tiposaldo','=',$solicitudes->tipovaca)->first();
			//dd($erp_vacaciones);
			$erp_vacaciones->saldo = $erp_vacaciones->saldo - $solicitudes->diassolicitados;
			$erp_vacaciones->save();

		}elseif($tcargo->nivel_aprobacion_vacaciones = '2' && $solicitudes->aprobado == '2'){

			$idempleado = Tpersona::where('cpersona','=', $solicitudes->idempleado)->first();
			$erp_vacaciones = Erp_vacaciones::where('dni','=', $idempleado->identificacion)->where('tiposaldo','=',$solicitudes->tipovaca)->first();
			//dd($erp_vacaciones);
			$erp_vacaciones->saldo = $erp_vacaciones->saldo - $solicitudes->diassolicitados;
			$erp_vacaciones->save();

		}
		
		$solicitudes = Tvacacionessolicitud::join('tpersona', 'tvacacionessolicitud.idempleado', '=', 'tpersona.cpersona')->where('idjefeinmediato','=', $idusuariologueado)->where('aprobado','=','0')->where('estadova','<>', 'Observaciones')->orWhere('idjefesuperior', '=', $idusuariologueado)->where('aprobado','=','1')->where('estadova','<>', 'Observaciones')->get();

		$solicitudesporaprobadas = Tvacacionessolicitud::join('tpersona', 'tvacacionessolicitud.idempleado', '=', 'tpersona.cpersona')->where('idjefeinmediato','=', $idusuariologueado)->where('aprobado','=','1')->orWhere('idjefesuperior', '=', $idusuariologueado)->where('aprobado','=','2')->get();

		$solicitudesobservadas = Tvacacionessolicitud::join('tpersona', 'tvacacionessolicitud.idempleado', '=', 'tpersona.cpersona')->where('estadova','=', 'Observaciones')->get();

		if (is_null($solicitudes)) {
			$solicitudes ='';
		}

		return view('ghumana.vacacionesaprueba',[
			'solicitudes' => $solicitudes,
			'solicitudesporaprobadas' => $solicitudesporaprobadas,
			'solicitudesobservadas' => $solicitudesobservadas
		]);
	}

	public function PrintPdfVacaciones(Request $request,$id)
	{

		//dd($id);

		$solicitud = Tvacacionessolicitud::where('id','=',$id)->first();
		$empleado = Tpersona::where('cpersona','=', $solicitud->idempleado)->first();
		$tdatosempleado = Tpersonadatosempleado::where('cpersona', '=', $solicitud->idempleado)->first();
		$areaempleado = Tarea::where('carea', '=', $tdatosempleado->carea)->first();
		//dd($areaempleado);
		$jefeinmediato = Tpersona::where('cpersona','=', $solicitud->idjefeinmediato)->first();
		$jefesuperior =  Tpersona::where('cpersona','=', $solicitud->idjefesuperior)->first();
		$now = $solicitud->created_at;

		$datoscomprobantevaca = array();
		$datoscomprobantevaca = [
			'nombreempleado' => $empleado->nombre,
			'departamento' => $areaempleado->descripcion,
			'fdesde' => $solicitud->fdesde,
			'fhasta' => $solicitud->fhasta,
			'diassolicitados' => $solicitud->diassolicitados,
			'periodovacacional' => 'Exclusivo Administracion',
			'firmacolaborador' => $empleado->nombre,
			'firmajefearea' => $jefeinmediato->nombre,
			'firmagerentearea' => $jefesuperior->nombre,
			'fechaactual' => $now->format('d/m/Y H:i:s'),
		];

		//dd($datoscomprobantevaca);
		//dd($vauser);
		 $view = view('ghumana.printvaca',[
		 	'datoscomprovante' => $datoscomprobantevaca
		 ]);
		 $pdf=\App::make('dompdf.wrapper');
         $pdf->loadHTML($view);
         $pdf->setPaper('A4','portrait');
         return $pdf->stream();
	}
	
}
