<?php
namespace App\Http\Controllers\GestionHumana;

use Illuminate\Support\Collection;
use App\Http\Controllers\Controller;
use Redirect;
use Illuminate\Http\Request;
use App\Models\Erp_encuesta_cat;
use App\Models\Erp_encuesta_cab;
use App\Models\Erp_encuesta_det;
use App\Models\Tusuario;
use App\Models\Tpersona;
use App\Models\Tcargo;
use App\Models\Tpersonadatosempleado;
use App\Models\Tmenupersona;
use Auth;
use DB;
use Response;

class Evaluacion360Controller extends Controller
{
	//public route
	
	public function DatosImport(Request $request)
	{
		dd($request->file('file')->getRealPath());
	}

	public function periodoindex()
	{
		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();
		//dd($tpersona->identificacion);
		// $periodo= DB::table('erp_encuesta_cab')
		// 	->leftjoin('erp_encuesta_cat', 'erp_encuesta_cat.id', '=', 'erp_encuesta_cab.id_categoria')
		// 	->leftjoin('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion')
		// 	->leftjoin('tusuarios','tusuarios.cpersona','=','tpersona.cpersona')
		// 	->leftjoin('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
		// 	->leftjoin('tcargos', 'tcargos.ccargo', '=', 'tpersonadatosempleado.ccargo')
		// 	->select('erp_encuesta_cat.descripcion','erp_encuesta_cat.desde','erp_encuesta_cat.hasta','erp_encuesta_cat.id as idcat','erp_encuesta_cab.id_estado as estado_cab')
		// 	->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
		// 	->where('tpersonadatosempleado.estado','=','ACT')
		// 	->where('tusuarios.estado','=','ACT')
		// 	->groupBy('erp_encuesta_cat.descripcion','erp_encuesta_cat.desde','erp_encuesta_cat.hasta','erp_encuesta_cat.id','erp_encuesta_cab.id_estado')
		// 	->get();

		$periodo= DB::table('erp_encuesta_cat')
			->leftjoin('erp_encuesta_cab', 'erp_encuesta_cab.id_categoria', '=', 'erp_encuesta_cat.id')
			->select('erp_encuesta_cat.descripcion', 'erp_encuesta_cat.desde', 'erp_encuesta_cat.hasta', 'erp_encuesta_cat.id as idcat')
			->selectRaw('sum(erp_encuesta_cab.id_estado) as respuestascab, count(erp_encuesta_cab.*) as cuento_cab')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->groupBy('erp_encuesta_cat.descripcion', 'erp_encuesta_cat.desde', 'erp_encuesta_cat.hasta', 'erp_encuesta_cat.id')
			->orderBy('erp_encuesta_cat.id' ,'asc')
			->get();
		//dd($periodo);		

		for ($i=0; $i < count($periodo); $i++) { 
			$evaluados=DB::table('erp_encuesta_cab')
			->leftjoin('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion') 
			->leftjoin('tusuarios','tusuarios.cpersona','=','tpersona.cpersona')
			->leftjoin('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->leftjoin('tcargos', 'tcargos.ccargo', '=', 'tpersonadatosempleado.ccargo')
			->select('tpersona.abreviatura','tcargos.descripcion','tpersona.identificacion as dni_evaluado','erp_encuesta_cab.id as idcab','erp_encuesta_cab.id_estado as estado_cab')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('erp_encuesta_cab.id_categoria','=', $periodo[$i]->idcat)
			->where('tpersonadatosempleado.estado','=','ACT')
			->where('tusuarios.estado','=','ACT')
			->count();
			$periodo[$i]->totalevaluados = $evaluados;
		}
		//dd($periodo);
		
		return view("ghumana.evaluacion360periodo", [
			'periodos' => $periodo
		]);	
	}

	public function index(Request $resquest, $idcat)
	{
		//transformo dato
		$idcat = substr($idcat,0,7);

		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();
		//dd($tpersona->identificacion);
		$encuestacab = Erp_encuesta_cab::where('dni_evaluador','=', $tpersona->identificacion)->first();
		$encuestadet = Erp_encuesta_det::where('id_encuesta','=', $encuestacab->id)->get();
		$nombrevaluacion = DB::table('erp_encuesta_cab')
			->leftjoin('erp_encuesta_cat', 'erp_encuesta_cab.id_categoria', '=','erp_encuesta_cat.id')
			->select('erp_encuesta_cat.descripcion')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('erp_encuesta_cab.id_categoria','=', $idcat)
			->first();

		

		$evaluados=DB::table('erp_encuesta_cab')
			->leftjoin('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion') 
			->leftjoin('tusuarios','tusuarios.cpersona','=','tpersona.cpersona')
			->leftjoin('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->leftjoin('tcargos', 'tcargos.ccargo', '=', 'tpersonadatosempleado.ccargo')
			->select('tpersona.abreviatura','tcargos.descripcion','tpersona.identificacion as dni_evaluado','erp_encuesta_cab.id as idcab','erp_encuesta_cab.id_estado as estado_cab')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('erp_encuesta_cab.id_categoria','=', $idcat)
			->where('tpersonadatosempleado.estado','=','ACT')
			->where('tusuarios.estado','=','ACT')
			->get();
		//dd($evaluados);
			
		return view("ghumana.evaluacion360", [
			'evaluados' => $evaluados,
			'nombrevaluacion' => $nombrevaluacion->descripcion
		]);
	}


	public function evaluacion($idcab)
	{
		//transformo dato
		$idcab = substr($idcab,0,7);
		
		//Encuentro logueo usuario
		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();
		$validousuariodni = $tpersona->identificacion;
		//Evaluador
		$evaluador = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluador', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluador','tpersona.abreviatura','erp_encuesta_cab.tipo_evaluador','erp_encuesta_cab.id_categoria as idcat')
			->where('erp_encuesta_cab.id','=', $idcab)
			->first();
		//Evaluado
		$evaludo = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluado','tpersona.abreviatura')
			->where('erp_encuesta_cab.id','=', $idcab)
			->first();
		//Traigo Preguntas asociadas a la cabecera		
		$preguntas = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab)
			->orderBy('orden', 'asc')
			->get();
		//Traigo Preguntas asociadas a la cabecera	 AD	
		$preguntasevaluadoads = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->orderBy('orden', 'asc')
			->get();
		//Traigo Preguntas asociadas a la cabecera	 TEXTO
		$preguntasevaluadotextos = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->orderBy('orden', 'asc')
			->get();	
		//valido el usuario conectado
		//dd($evaluador);
			
		if (!is_null($evaluador)) {
			if ($validousuariodni == $evaluador->dni_evaluador) {
				$usuariopermitido = false;
				//dd('entre en el 1 if');
			}
		}else{
			$usuariopermitido = true;
		}

		$activobotonfinalizar = $this->ValidoRespuestasEncuesta($idcab,$evaluador->tipo_evaluador);
		//dd($activobotonfinalizar);

		$respuestas = $this->cuentopreguntas($idcab);

		//dd($preguntas);

		return view('ghumana.listadoevaluacion360', [
			'evaluadores' => $evaluador,
			'evaluados' => $evaludo,
			'preguntas' => $preguntas,
			'preguntasevaluadoads' => $preguntasevaluadoads,
			'preguntasevaluadotextos' => $preguntasevaluadotextos,
			'idcab' => $idcab,
			'usuariopermitido' => $usuariopermitido,
			'activobotonfinalizar' => $activobotonfinalizar,
			'preguntasad'=> $respuestas['preguntasAD'],
			'preguntastexto'=> $respuestas['preguntasTexto'],
			'idcat' => $evaluador->idcat

		]);
	}

	public function responderevaluacion(Request $resquest)
	{
		//Encuentro logueo usuario
		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();
		$validousuariodni = $tpersona->identificacion;

		$arrayrespuestas = $resquest->all();
		//Comprueba y actualiza preguntas cerradas 
		if (isset($arrayrespuestas['radio'])) {
			//dd('No estoy nulo debo actualizar RADIO');
			foreach ($arrayrespuestas['radio'] as $key => $respuesta) {
				$pregunta = Erp_encuesta_det::findOrFail($key);
	        	$pregunta->id_respuesta = $respuesta;
	        	$pregunta->update();
			}
		}
		//Comprueba y actualiza texto
		//dd($arrayrespuestas['respuestatexto']);
		if (isset($arrayrespuestas['respuestatexto'])) {
			foreach ($arrayrespuestas['respuestatexto'] as $key => $respuesta) {
				$pregunta = Erp_encuesta_det::findOrFail($key);
	        	$pregunta->texto_respuesta = $respuesta;
	        	$pregunta->update();
			}
		}

		//Traigo Elavuador y datos que sean necesarios
		$evaluador = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluador', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluador','tpersona.abreviatura','erp_encuesta_cab.tipo_evaluador','erp_encuesta_cab.id_categoria as idcat')
			->where('erp_encuesta_cab.id','=', $resquest->idcab)
			->first();
		//Traigo el Evaluado y datos que sean necesarios
		$evaludo = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluado','tpersona.abreviatura')
			->where('erp_encuesta_cab.id','=', $resquest->idcab)
			->first();
		//Traigo Preguntas asociadas a la cabecera	
		$preguntas = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $resquest->idcab)
			->orderBy('orden', 'asc')
			->get();
		//dd($evaluador);
		//Traigo Preguntas asociadas a la cabecera	 AD	
		$preguntasevaluadoads = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $resquest->idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->orderBy('orden', 'asc')
			->get();
		//Traigo Preguntas asociadas a la cabecera	 TEXTO
		$preguntasevaluadotextos = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $resquest->idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->orderBy('orden', 'asc')
			->get();		

		if (!is_null($evaluador)) {
			if ($validousuariodni == $evaluador->dni_evaluador) {
				$usuariopermitido = false;
				//dd('entre en el 1 if');
			}
		}else{
			$usuariopermitido = true;
		}

		$activobotonfinalizar = $this->ValidoRespuestasEncuesta($resquest->idcab,$evaluador->tipo_evaluador);
		$respuestas = $this->cuentopreguntas($resquest->idcab);	

		return view('ghumana.listadoevaluacion360', [
			'evaluadores' => $evaluador,
			'evaluados' => $evaludo,
			'preguntas' => $preguntas,
			'preguntasevaluadoads' => $preguntasevaluadoads,
			'preguntasevaluadotextos' => $preguntasevaluadotextos,
			'idcab' => $resquest->idcab,
			'usuariopermitido' => $usuariopermitido,
			'activobotonfinalizar' => $activobotonfinalizar,
			'preguntasad'=> $respuestas['preguntasAD'],
			'preguntastexto'=> $respuestas['preguntasTexto'],
			'idcat' => $evaluador->idcat
		]);
	}

	public function FinalizarEncuesta(Request $resquest)
	{	
		$idcab = $resquest->all();
		//dd($idcab['tipoevaluador']);
		//Encuentro logueo usuario
		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();
		$validousuariodni = $tpersona->identificacion;

		//Traigo Elavuador y datos que sean necesarios
		$evaluador = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluador', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluador','tpersona.abreviatura','erp_encuesta_cab.tipo_evaluador','erp_encuesta_cab.id_categoria as idcat')
			->where('erp_encuesta_cab.id','=', $idcab['idcab'])
			->first();
		//Traigo el Evaluado y datos que sean necesarios
		$evaludo = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluado','tpersona.abreviatura')
			->where('erp_encuesta_cab.id','=', $idcab['idcab'])
			->first();
		//Traigo Preguntas asociadas a la cabecera	
		$preguntas = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab['idcab'])
			->orderBy('orden', 'asc')
			->get();

		if (!is_null($evaluador)) {
			if ($validousuariodni == $evaluador->dni_evaluador) {
				$usuariopermitido = false;
				//dd('entre en el 1 if');
			}
		}else{
			$usuariopermitido = true;
		}		
		//dd($idcab['idcab']);
		
		$valido = $this->ValidoFinalEncuesta($idcab['idcab'],$usuariopermitido,$evaluador->tipo_evaluador);

		//dd($evaluador);
		$evaluados=DB::table('erp_encuesta_cab')
			->leftjoin('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion') 
			->leftjoin('tusuarios','tusuarios.cpersona','=','tpersona.cpersona')
			->leftjoin('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->leftjoin('tcargos', 'tcargos.ccargo', '=', 'tpersonadatosempleado.ccargo')
			->select('tpersona.abreviatura','tcargos.descripcion','tpersona.identificacion as dni_evaluado','erp_encuesta_cab.id as idcab','erp_encuesta_cab.id_estado as estado_cab')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('erp_encuesta_cab.id_categoria','=', $evaluador->idcat)
			->where('tpersonadatosempleado.estado','=','ACT')
			->where('tusuarios.estado','=','ACT')
			->get();

		$nombrevaluacion = DB::table('erp_encuesta_cab')
			->leftjoin('erp_encuesta_cat', 'erp_encuesta_cab.id_categoria', '=','erp_encuesta_cat.id')
			->select('erp_encuesta_cat.descripcion')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('erp_encuesta_cab.id_categoria','=', $evaluador->idcat)
			->first();
		//dd($evaluados);
		return view("ghumana.evaluacion360", [
			'evaluados' => $evaluados,
			'nombrevaluacion' => $nombrevaluacion->descripcion,
			'evaluadores' => $evaluador

		]);

	}

	public function ValidoFinalEncuesta($idcab,$usuariopermitido,$cargo)
	{
		//Obtengo los datos para Validaciones para crear los bucles de insert
		$cuentopreguntasad = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->count();
		$cuentorespuestasad = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->where('erp_encuesta_det.id_respuesta','<>', '0')
			->count();
		$cuentopreguntastexto = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->count();
		$cuentorespuestastexto = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->whereRaw('length(erp_encuesta_det.texto_respuesta) > 5')
			->count();

		//Traigo Elavuador y datos que sean necesarios
		$evaluador = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluador', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluador','tpersona.abreviatura','erp_encuesta_cab.tipo_evaluador')
			->where('erp_encuesta_cab.id','=', $idcab)
			->first();
		//Traigo el Evaluado y datos que sean necesarios
		$evaludo = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluado','tpersona.abreviatura')
			->where('erp_encuesta_cab.id','=', $idcab)
			->first();
		//Traigo Preguntas asociadas a la cabecera	
		$preguntas = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab)
			->orderBy('orden', 'asc')
			->get();
		//Traigo Preguntas asociadas a la cabecera	 AD	
		$preguntasevaluadoads = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->orderBy('orden', 'asc')
			->get();
		//Traigo Preguntas asociadas a la cabecera	 TEXTO
		$preguntasevaluadotextos = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->orderBy('orden', 'asc')
			->get();		

		$respuestas = $this->cuentopreguntas($idcab);		

		//Validaciones para crear los bucles de insert	
		if ($cuentopreguntasad <> $cuentorespuestasad){
			//dd('entre en las competencias error');
			$activobotonfinalizar = true;
			return view('ghumana.listadoevaluacion360', [
				'evaluadores' => $evaluador,
				'evaluados' => $evaludo,
				'preguntas' => $preguntas,
				'preguntasevaluadoads' => $preguntasevaluadoads,
				'preguntasevaluadotextos' => $preguntasevaluadotextos,
				'idcab' => $idcab['idcab'],
				'errorad' => 'Falta Preguntas de competencias.',
				'usuariopermitido' => $usuariopermitido,
				'activobotonfinalizar' => $activobotonfinalizar,
				'preguntasad'=> $respuestas['preguntasAD'],
				'preguntastexto'=> $respuestas['preguntasTexto']
			]);
		}elseif($cargo == 'Jefe Inmediato'){
			if($cuentopreguntastexto <> $cuentorespuestastexto)
			{	
				$activobotonfinalizar = true;
				return view('ghumana.listadoevaluacion360', [
					'evaluadores' => $evaluador,
					'evaluados' => $evaludo,
					'preguntas' => $preguntas,
					'preguntasevaluadoads' => $preguntasevaluadoads,
					'preguntasevaluadotextos' => $preguntasevaluadotextos,
					'idcab' => $idcab['idcab'],
					'errortexto' => 'Falta Comentarios o el comentario es muy corto.',
					'usuariopermitido' => $usuariopermitido,
					'activobotonfinalizar' => $activobotonfinalizar,
					'preguntasad'=> $respuestas['preguntasAD'],
					'preguntastexto'=> $respuestas['preguntasTexto']
				]);
			}
		}
		

		if ($cuentopreguntasad == $cuentorespuestasad && $cuentopreguntastexto == $cuentorespuestastexto && $cargo == 'Jefe Inmediato') {

				$respuestacompara = 'todo OK 1';
				//dd($respuestacompara);
				$encuesta = Erp_encuesta_cab::findOrFail($idcab);
	         	$encuesta->id_estado = '99';
	         	$encuesta->update();

		}elseif($cuentopreguntasad == $cuentorespuestasad){

				$respuestacompara = 'todo OK 2';
				//dd($idcab);
				$encuesta = Erp_encuesta_cab::findOrFail($idcab);
	         	$encuesta->id_estado = '99';
	         	$encuesta->update();
		}

		return true;
	}
	
	public function ValidoRespuestasEncuesta($idcab,$cargo)
	{

		//Obtengo los datos para Validaciones para crear los bucles de insert
		$cuentopreguntasad = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->count();
		$cuentorespuestasad = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->where('erp_encuesta_det.id_respuesta','<>', '0')
			->count();
		$cuentopreguntastexto = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->count();
		$cuentorespuestastexto = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->whereRaw('length(erp_encuesta_det.texto_respuesta) > 5')
			->count();

		//Validaciones para crear los bucles de insert	
		if ($cuentopreguntasad <> $cuentorespuestasad){
			//dd('entre en las competencias error');
			return $activobotonfinalizar = false;
		}elseif($cargo == 'Jefe Inmediato'){
			if($cuentopreguntastexto <> $cuentorespuestastexto)
			{
				return $activobotonfinalizar = false;
			}
		}

		if ($cuentopreguntasad == $cuentorespuestasad && $cuentopreguntastexto == $cuentorespuestastexto && $cargo == 'Jefe Inmediato') {
				return $activobotonfinalizar = true;

		}elseif($cuentopreguntasad == $cuentorespuestasad){
				return $activobotonfinalizar = true;
		}
		
		return $activobotonfinalizar;
	}

	public function cuentopreguntas($idcab)
	{
		//Obtengo los datos para Validaciones para crear los bucles de insert
		$cuentopreguntasad = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->count();
		$cuentorespuestasad = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'A al D')
			->where('erp_encuesta_det.id_respuesta','<>', '0')
			->count();
		$cuentopreguntastexto = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->count();
		$cuentorespuestastexto = DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $idcab)
			->where('erp_encuesta_det.tipo_respuesta','=', 'Textos')
			->whereRaw('length(erp_encuesta_det.texto_respuesta) > 5')
			->count();

		$preguntasAD = $cuentopreguntasad - $cuentorespuestasad;
		
		$preguntasTexto = $cuentopreguntastexto	- $cuentorespuestastexto;
		$respuestas =array();
		$respuestas = [ 'preguntasAD'=> $preguntasAD,
						'preguntasTexto'=>$preguntasTexto
					  ];
		//dd($respuestas);			  
		return $respuestas;
	}

	public function CerrarEncuesta(Request $resquest)
	{
		$idcab = $resquest->all();
		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();
		$validousuariodni = $tpersona->identificacion;

		

		//Traigo Elavuador y datos que sean necesarios
		$evaluador = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluador', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluador','tpersona.abreviatura','erp_encuesta_cab.tipo_evaluador','erp_encuesta_cab.id_categoria as idcat')
			->where('erp_encuesta_cab.id','=', $idcab['idcab_terminada'])
			->first();
		//Traigo el Evaluado y datos que sean necesarios
		$evaludo = DB::table('erp_encuesta_cab')
			->join('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion')
			->select('erp_encuesta_cab.dni_evaluado','tpersona.abreviatura')
			->where('erp_encuesta_cab.id','=', $idcab['idcab_terminada'])
			->first();
		//Traigo Preguntas asociadas a la cabecera	
		$preguntas = DB::table('erp_encuesta_cab')
			->join('erp_encuesta_det', 'erp_encuesta_cab.id', '=', 'erp_encuesta_det.id_encuesta')
			->select('erp_encuesta_cab.id_estado','erp_encuesta_cab.dni_evaluado','erp_encuesta_cab.dni_evaluador','erp_encuesta_det.*')
			->where('erp_encuesta_cab.id','=', $idcab['idcab_terminada'])
			->orderBy('orden', 'asc')
			->get();

		//dd($evaluador);
		$evaluados=DB::table('erp_encuesta_cab')
			->leftjoin('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion') 
			->leftjoin('tusuarios','tusuarios.cpersona','=','tpersona.cpersona')
			->leftjoin('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->leftjoin('tcargos', 'tcargos.ccargo', '=', 'tpersonadatosempleado.ccargo')
			->select('tpersona.abreviatura','tcargos.descripcion','tpersona.identificacion as dni_evaluado','erp_encuesta_cab.id as idcab','erp_encuesta_cab.id_estado as estado_cab')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('erp_encuesta_cab.id_categoria','=', $evaluador->idcat)
			->where('tpersonadatosempleado.estado','=','ACT')
			->where('tusuarios.estado','=','ACT')
			->get();

		$nombrevaluacion = DB::table('erp_encuesta_cab')
			->leftjoin('erp_encuesta_cat', 'erp_encuesta_cab.id_categoria', '=','erp_encuesta_cat.id')
			->select('erp_encuesta_cat.descripcion')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('erp_encuesta_cab.id_categoria','=', $evaluador->idcat)
			->first();

		$encuesta = Erp_encuesta_cab::findOrFail($idcab['idcab_terminada']);
		
		if ($encuesta->id_estado =='99') {
			//$respuestacompara = 'entre al primer finaliar';
			//dd($respuestacompara);
			
	        $encuesta->id_estado = '2';
	        $encuesta->update();
	        //dd($encuesta);
		}
		return Redirect::route('indexevaluacion',$evaluador->idcat);
	}

	public function AuditaDataEvaluaciones(Request $request)
	{
		
		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();

		$tmenupersona = Tmenupersona::where('cpersona','=', $tusuario->cpersona)->where('menuid','=','0607')->first();
		$tdatosemple = Tpersonadatosempleado::where('cpersona','=', $tpersona->cpersona)->first();
		$tcargo = Tcargo::where('ccargo','=', $tdatosemple->ccargo)->first();
		
		//dd($tusuario->cpersona);

		//dd($tmenupersona);

		if(is_null($tmenupersona))
		{
			//dd('entre en el 1');
			return Redirect::route('panel');
			
		}elseif(empty($tmenupersona)){
			//dd('entre en el null');
			return Redirect::route('panel');
		}

		//dd(isset($request->buscoevaluador));
		//Traigo Elavuador y datos que sean necesarios
			$evaluadores = DB::table('erp_encuesta_cab as cab')
			->join('tpersona as tp', 'cab.dni_evaluador', '=', 'tp.identificacion')
			->select('cab.dni_evaluador as dni_evaluador','tp.abreviatura as abreviatura_evaluador','tp.nombre as nombre_evaluador')
			->groupBy('cab.dni_evaluador','tp.abreviatura','tp.nombre')
			->get();

		if(isset($request->evaluador)){
			$evaluados = DB::table('erp_encuesta_cab as cab')
			->join('tpersona as tp', 'cab.dni_evaluador', '=', 'tp.identificacion')
			->join('tpersona as tp2', 'cab.dni_evaluado', '=', 'tp2.identificacion')
			->join('tpersonadatosempleado as templeado', 'tp2.cpersona','=', 'templeado.cpersona' )
			->join('tcargos as cargo', 'templeado.ccargo','=','cargo.ccargo')
			->select('cab.id as idcab','cab.tipo_evaluador','cab.dni_evaluador as dni_evaluador','tp.abreviatura as abreviatura_evaluador','tp.nombre as nombre_evaluador', 'cab.dni_evaluado as dni_evaluado', 'cargo.descripcion', 'tp2.abreviatura as abreviatura_evaluado', 'tp2.nombre as nombre_evaluado')
			->where('cab.dni_evaluador','ilike', '%'.$request->evaluador.'%')
			->get();	

			for ($i=0; $i < count($evaluados); $i++) { 
			$preguntas=DB::table('erp_encuesta_det')
			->where('erp_encuesta_det.id_encuesta','=', $evaluados[$i]->idcab)
			->orderBy('erp_encuesta_det.orden')
			->get();
				$evaluados[$i]->preguntas = $preguntas;
			}
		}else{
			$evaluados= null;
		}
			
		                                                                                                                                                   		                                                                                                           	
		//dd($evaluados);
		return view('ghumana.evaluacion360audita',[
			'evaluadores' => $evaluadores,
			'evaluados' => $evaluados
			]);                                                                                                                           
	}
	
}
