<?php

namespace App\Http\Controllers\Gastos;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tarea;
use App\Models\Tgastosejecucion;
use App\Models\Tproyecto;
use App\Models\Tpropuesta;
use App\Models\Tgastosejecuciondetalle;
use App\Models\Tconceptogasto;
use App\Models\Tpersona;
use Carbon\Carbon;
use Session;
use DB;
use Auth;
use Fpdf;
use Redirect;
use App\Erp\HelperSIGSupport;
use App\Erp\GastosPdf;
use App\Erp\FlowSupport;
use PDF;

class ReporteGastos extends Controller
{   
    public function reporteGastos(){

        /*$proyectos=[];


        $proyecto = DB::table('tproyecto')
            ->select(DB::raw('CONCAT(codigo,\'-\',nombre) as nombre'),'cproyecto')
            ->get();


        foreach ($proyecto as $key => $p) {
           array_push($proyectos,[$p->cproyecto=>$p->nombre]);
        }


        $areas = DB::table('tareas')
            ->select(DB::raw('CONCAT(codigo,\'-\',descripcion) as nombre'),'carea')
            ->get();

        foreach ($areas as $key => $a) {
           array_push($proyectos,[$a->carea=>$a->nombre]);
        }*/

        $areas = DB::table('tareas')
            ->lists(DB::raw('CONCAT(codigo,\' - \',descripcion) as nombre'),'carea');
        //$areas = ['N'=>'Seleccione un área'] + $areas;
        $areas = ['N'=>''] + $areas;

        $proyectos = DB::table('tproyecto')
            ->orderBy('codigo')
            ->lists(DB::raw('CONCAT(codigo,\' - \',nombre) as nombre'),'cproyecto');
        //$proyectos = ['N'=>'Seleccione un proyecto'] + $proyectos;
        $proyectos = ['N'=>''] + $proyectos;

        $persona = DB::table('tpersona as per')
            ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
            // ->where('emp.estado','=','ACT')
            ->orderBy('per.nombre','ASC')
            ->lists('per.abreviatura','per.cpersona');
        //$persona = ['N'=>'Seleccione un profesional'] + $persona;
        $persona = ['N'=>''] + $persona;
        //dd($persona);


        $conceptogasto = DB::table('tconceptogastos as g')
            ->leftjoin('tcatalogo as c','g.tipogasto','=','c.digide')
            ->where('c.codtab','=','00026')  
            ->where('g.estado','=','00001')   
            ->whereNotNull('g.cconcepto_parent')        
            ->orderBy('g.descripcion')
            ->lists(DB::raw('CONCAT(g.descripcion,\' - \',c.descripcion) as nombre'),'cconcepto');
        $conceptogasto = ['N'=>''] + $conceptogasto;

        $conceptogasto_parent = DB::table('tconceptogastos as g')
            ->leftjoin('tcatalogo as c','g.tipogasto','=','c.digide')
            ->where('c.codtab','=','00026')  
            ->where('g.estado','=','00001')   
            ->whereNull('g.cconcepto_parent')        
            ->orderBy('g.descripcion')
            ->lists(DB::raw('CONCAT(g.descripcion,\' - \',c.descripcion) as nombre'),'cconcepto');
        //$conceptogasto = ['N'=>'Seleccione un proyecto'] + $conceptogasto;
        $conceptogasto_parent = ['N'=>''] + $conceptogasto_parent;

        return view('gastos/reporteGastos',compact('proyectos','persona','areas','conceptogasto','conceptogasto_parent'));

    }

    public function verGastosPorProyecto(Request $request){



        $gastos=DB::table('tgastosejecuciondetalle as tgd');

            if ($request->cproyecto!='N') {
                $gastos=$gastos->where('tgd.cproyecto','=',$request->cproyecto);
            }
            if ($request->carea!='N') {
                $gastos=$gastos->where('tgd.carea','=',$request->carea);
            }
            if ($request->cpersona!='N') {
                $gastos=$gastos->where('tgd.cpersona_empleado','=',$request->cpersona);
            }
            if ($request->conceptogasto!='N') {
                $gastos=$gastos->where('tgd.cconcepto','=',$request->conceptogasto);
            }

            if ($request->conceptogasto_parent!='N') {


                $conceptos_hijos = DB::table('tconceptogastos')
                    ->where('cconcepto_parent','=',$request->conceptogasto_parent)        
                    ->lists('cconcepto');

                $gastos=$gastos->whereIn('tgd.cconcepto',$conceptos_hijos);
            }

        $gastos=$gastos->select('tgd.*',DB::raw("to_char(tgd.fdocumento,'DD-MM-YY') as fdocumento"),DB::raw("to_char(tgd.fregistro,'DD-MM-YY') as fregistro"))
                        ->get();

        $arrayGastos = [];


        foreach ($gastos as $key => $g) {

        
            $proyecto=Tproyecto::where('cproyecto','=',$g->cproyecto)->first();          
            $area=Tarea::where('carea','=',$g->carea)->first();
            $gastocab=Tgastosejecucion::where('cgastosejecucion','=',$g->cgastosejecucion)->first();
            $concepto=Tconceptogasto::where('cconcepto','=',$g->cconcepto)->first();
            $persona_emp=Tpersona::where('cpersona','=',$g->cpersona_empleado)->first();
            $moneda = DB::table('tmonedas')->where('cmoneda','=',$g->cmoneda)->first();

            $tcambiosoles= DB::table('tmonedacambio')
                ->where('cmoneda','=',$g->cmoneda)
                ->where('cmonedavalor','=','S/.')
                ->where('fecha','<=',$g->fdocumento)
                ->orderBy('fecha','DESC')
                ->first();

            //$this->obtenerValorVenta($g->cmoneda,'S/.',$g->fdocumento);

            $gc['cgastodetalle']=$g->cgastosejecuciondet;
            $gc['proyecto']=$proyecto ? $proyecto->codigo: '';
            $gc['area']=$area ? $area->descripcion: '';
            $gc['concepto']=$concepto ? $concepto->descripcion: '';
            $gc['asignado']=$persona_emp ? $persona_emp->abreviatura: '';
            //$gc['venta']=$tcambiosoles ? $tcambiosoles->valorventa: '';
            $gc['venta']=$this->obtenerValorVentaDolar($g->fdocumento);
            $gc['mon_total']=$g->total;
            $gc['subtotal']=$g->subtotal;

            $usd=1;
            if ($g->cmoneda=='USD') {
               $usd=$g->subtotal+$g->otroimpuesto;
            }
            elseif ($g->cmoneda=='S/.') {
                $usd=round(($g->subtotal+$g->otroimpuesto)/$gc['venta'],2);
            }
            else{

                $vcompra=$this->obtenerValorCompra($g->cmoneda,$g->fdocumento);
                $usd=round(($g->subtotal+$g->otroimpuesto)*$vcompra,2);

            }
            $gc['usd']=$usd;
            $gc['moneda']=$moneda ? $moneda->cmoneda: '';
            $gc['fechaRendicion']=$g->fregistro;
            $gc['fechaFactura']=$g->fdocumento;
            $gc['numeroFactura']=$g->numerodocumento;
            $gc['proveedor']=$g->razon_cliente;
            $gc['descripcion']=$g->descripcion;
            $gc['gastocab']=$gastocab ? $gastocab->descripcion: '';

            array_push($arrayGastos, $gc);
        }

        return view('gastos/tablaGastosReporte',compact('arrayGastos'));
    }

    private function obtenerValorCompra($moneda_actual,$fecha){

       

           $valorcompra= DB::table('tmonedacambio')
            ->where('cmoneda','=',$moneda_actual)
            ->where('cmonedavalor','=','USD')
            ->where('fecha','<=',$fecha)
            ->orderBy('fecha','DESC')
            ->first();

            return $valorcompra->valorcompra;
            



    }

    private function obtenerValorVentaDolar($fecha){

        $valorventa= DB::table('tmonedacambio')
            ->where('cmoneda','=','USD')
            ->where('cmonedavalor','=','S/.')
            ->where('fecha','<=',$fecha)
            ->orderBy('fecha','DESC')
            ->first();

        return $valorventa->valorventa;
            
    }
    


}
