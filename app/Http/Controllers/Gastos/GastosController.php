<?php

namespace App\Http\Controllers\Gastos;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tarea;
use App\Models\Tgastosejecucion;
use App\Models\Tproyecto;
use App\Models\Tpropuesta;
use App\Models\Tgastosejecuciondetalle;
use App\Models\Tcatalogo;
use Carbon\Carbon;
use Session;
use DB;
use Auth;
use Fpdf;
use Redirect;
use App\Erp\HelperSIGSupport;
use App\Erp\GastosPdf;
use App\Erp\FlowSupport;
use PDF;

class GastosController extends Controller
{


    public function listarGrid(){       
        //DB::raw('fn_getcondicionopgastos(tge.cgastosejecucion) as condicion'),
        return Datatables::queryBuilder(DB::table('tgastosejecucion as tge')                     
            ->join('testadorendiciongasto as est','tge.cestadorendiciongasto','=','est.cestadorendiciongasto')
            ->select('tge.cgastosejecucion','tge.numerorendicion','tge.descripcion','tge.frendicion','tge.total','est.descripcion as condicion',DB::raw('CONCAT(\'row_\',tge.cgastosejecucion)  as "DT_RowId"'))            
            ->where('tge.per_registrador','=',Auth::user()->cpersona)
            /*->whereExists(function($query){
                $query->select('tg.cpersona_empleado')
                ->from('tgastosejecuciondetalle as tg')
                ->where('tg.cpersona_empleado','=',Auth::user()->cpersona)
                ->whereRaw('tg.cgastosejecucion=tge.cgastosejecucion');
               
            })*/
             ->orderBy('tge.frendicion','DESC')
         )->make(true);


    }

    public function listaGastosProyecto(){        
        return view('gastos/listarGastosProyecto');
    } 



    public function gastosAdmProyecto(){
        return view('gastos/gastoAdmXProyecto');
    }

    public function gastosVariosProyecto(){
        return view('gastos/gastoVariosProyectos');
    }

    public function rendiciongastosProyecto(){
        $objHelper = new HelperSIGSupport();
        $tipoactividad = $objHelper->getListCatalogo('00036');
        $tipoactividad = [''=>''] + $tipoactividad;
        $areas = DB::table('tareas')
        ->lists('descripcion','carea');
        $areas = [''=>'']+ $areas;
        return view('gastos/rendicionUsuario',compact('tipoactividad','areas'));
    }

    public function aprobacionRendicion(){
        $objHelper = new HelperSIGSupport();   

        $tipoactividad = DB::table('tconceptogastos')
        ->whereNull('cconcepto_parent')
        ->orderBy('descripcion','ASC')
        ->lists('descripcion','cconcepto');
        $tipoactividad = [''=>''] + $tipoactividad;

        $areas = DB::table('tareas')
        ->lists('descripcion','carea');
        $areas = [''=>'']+ $areas;        

        $personal =DB::table('tpersona as tp')
        ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')
        ->where('tpi.esempleado','=','1')
        ->orderBy('tp.nombre','ASC')
        ->lists('tp.nombre','tp.cpersona');
        $personal = [''=>''] + $personal;         
        
        return view('gastos/aprobacionRendicionesGastos',compact('tipoactividad','areas','personal'));
    }

    public function historialgastosProyecto(){
        return view('gastos/historialAprobacionesGastos');
    }


    //Gasto Proyecto


    public function listaGastos(){

            return Datatables::queryBuilder(DB::table('tgastosejecucion')
            ->select('cgastosejecucion','numerorendicion','descripcion','valorcambio','total','tipo_rendicion',DB::raw('CONCAT(\'row_\',cgastosejecucion)  as "DT_RowId"'))
            ->where('tgastosejecucion.per_registrador','=',Auth::user()->cpersona)
            )->make(true);    
    }


    public function gastosProyecto(){

        $objHelper = new HelperSIGSupport();
        Session::forget('listgaspro');
        
        $tipoactividad = DB::table('tconceptogastos')
        ->whereNull('cconcepto_parent')
        ->where('estado','=','00001')
        ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
        ->get();
         /*$objHelper->getListCatalogo('00036');*/
         // dd($tipoactividad);
        $tipogasto = DB::table('tconceptogastos')
        ->whereNotNull('cconcepto_parent')
        ->where('estado','=','00001')
        ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
        ->get();
        /*$objHelper->getListCatalogo('00037');*/

        $tipodocumento = $objHelper->getListCatalogo('00038');       

        $numrendicion = $objHelper->getNumeroRendicion('006','GASTOS');

        $cat_tipogastos = DB::table('tcatalogo')
        ->where('codtab','=','00026')
        ->get();

        $area = DB::table('tareas')
        ->lists('descripcion','carea');
        $area = [''=>'']+ $area;

        $ccosto = DB::table('tcentrocostos')
        ->lists('descripcion','ccentrocosto');
        $ccosto = [''=>'']+ $ccosto;

        $personal =DB::table('tpersona as tp')
        ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')
        ->where('tpi.esempleado','=','1')
        ->orderBy('tp.nombre','ASC')
        ->lists('tp.abreviatura','tp.cpersona');
        $personal = [''=>''] + $personal; 


        $tmoneda = DB::table('tmonedas')->lists('descripcion','cmoneda');
        $tiporendicion = Tcatalogo::where('codtab', '=', '00060')->whereNotNull('valor')->OrderBy('digide','asc')->lists('descripcion','digide');
        //dd($tiporendicion);
        $tmonedacambio = DB::table('tmonedacambio')
        ->where('cmoneda','=','USD')
        ->where('cmonedavalor','=','S/.')
        ->where(DB::raw("to_char(fecha,'YYYYMMDD')"),'<=',date('Ymd'))
        ->orderBy('fecha','DESC')
        ->first();

        if($tmonedacambio){
             $valorcambio = $tmonedacambio->valorcompra;
        }

        $user = Auth::user();
       

        $frendicion = date('Y-m-d');
        return view('gastos/gastoProyecto',compact('numrendicion','user','tipoactividad','tipogasto','tipodocumento','tmoneda','area','ccosto','personal','tmonedacambio','frendicion','cat_tipogastos','valorcambio','tiporendicion'));
    }

    public function viewProy(Request $request,$idProyecto){
        $tproyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        $persona = DB::table('tpersona')->where('cpersona','=',$tproyecto->cpersonacliente)
        ->first();
        $tumineraproy = DB::table('tunidadminera')
        ->where('cpersona','=',$tproyecto->cpersonacliente)
        ->first();
        return view('partials.verProyecto',compact('tproyecto','tumineraproy','persona'));
    }

    public function viewPropuesta(Request $request,$idPropuesta){
        $tpropuesta = DB::table('tpropuesta')
        ->where('cpropuesta','=',$idPropuesta)
        ->first();
        $personaprop = DB::table('tpersona')->where('cpersona','=',$tpropuesta->cpersona)
        ->first();
        $tumineraprop = DB::table('tunidadminera')
        ->where('cpersona','=',$tpropuesta->cpersona)
        ->first();
        return view('partials.verPropuesta',compact('tpropuesta','tumineraprop','personaprop'));
    }

    public function viewProveedor(Request $request,$idProveedor){
        $tproveedor = DB::table('tpersona as tp')
        ->leftJoin('tpersonajuridicainformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        ->where('tp.cpersona','=',$idProveedor)
        ->first();
        $razonsoc = DB::table('tpersonajuridicainformacionbasica')
        ->where('razonsocial','=',$tproveedor->razonsocial)
        ->first();
       //dd($tproveedor);
        return view('partials.verProveedor',compact('tproveedor','razonsoc'));
    }


    public function editarGastoProyecto(Request $request,$idGastoProy){
        
        //Session::forget('listgaspro');

        $objHelper = new HelperSIGSupport();
        $user = Auth::user();
        /*$tipoactividad = $objHelper->getListCatalogo('00036');
        $tipoactividad = [''=>''] + $tipoactividad;

        $tipogasto = $objHelper->getListCatalogo('00037');
        $tipogasto = [''=>'']+$tipogasto;*/



        $tipoactividad = DB::table('tconceptogastos')
        ->whereNull('cconcepto_parent')
        ->where('estado','=','00001')
        ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
        ->get();        

        $tipogasto = DB::table('tconceptogastos')
        ->whereNotNull('cconcepto_parent')
        ->where('estado','=','00001')
        ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
        ->get();

       
        $cat_tipogastos = DB::table('tcatalogo')
        ->where('codtab','=','00026')
        ->get();
        
        $tipodocumento = $objHelper->getListCatalogo('00038');        
        $tipodocumento = [''=>''] + $tipodocumento;


        $area = DB::table('tareas')
        ->lists('descripcion','carea');
        $area = [''=>'']+ $area;

        $ccosto = DB::table('tcentrocostos')
        ->lists('descripcion','ccentrocosto');
        $ccosto = [''=>'']+ $ccosto;

        $personal =DB::table('tpersona as tp')
        ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')
        ->where('tpi.esempleado','=','1')
        ->orderBy('tp.nombre','ASC')
        ->lists('tp.abreviatura','tp.cpersona');
        $personal = [''=>''] + $personal;   

        $tmoneda = DB::table('tmonedas')->lists('descripcion','cmoneda');
        $tiporendicion = Tcatalogo::where('codtab', '=', '00060')->whereNotNull('valor')->lists('descripcion','digide');

        $tmonedacambio = DB::table('tmonedacambio')
        ->where('cmoneda','=','USD')
        ->where('cmonedavalor','=','S/.')
        ->where(DB::raw("to_char(fecha,'YYYYMMDD')"),'<=',date('Ymd'))
        ->orderBy('fecha','DESC')
        ->first();

        $gasto=DB::table('tgastosejecucion as tg')
        ->where('tg.cgastosejecucion','=',$idGastoProy)
        ->first();

        $numrendicion=$gasto->numerorendicion;
        $descripcions=$gasto->descripcion;
        $frendicion=$gasto->frendicion;
        $viatico=$gasto->viatico;

        if(!is_null($gasto->valorcambio)){
            $valorcambio=$gasto->valorcambio;
        }else{
            $valorcambio=$tmonedacambio->valorcompra;
        }
       
        $numTabla=0;

        $detgastoKey=DB::table('tgastosejecuciondetalle as tgd')
        ->where('tgd.cgastosejecucion','=',$idGastoProy)
        ->orderBy('cproyecto','ASC')
        ->orderBy('cpropuesta','ASC')
        ->orderBy('carea','ASC')
        ->orderBy('ccentrocosto','ASC')
        ->select('tgd.cproyecto','tgd.cpropuesta','tgd.carea','tgd.ccentrocosto','tgd.cpersona_empleado')
        ->distinct()
        ->get();
        $gastosKey=array();
        $idTabla=0;
        //dd($detgastoKey);
        $total=0;
        $totaldolar=0;
        $subtotal=0;
        $totalImpuestos=0;           
        foreach($detgastoKey as $key){
            $gKey['cgastoejecucion']=$idGastoProy;
            $gKey['cproyecto']='';
            $gKey['cproyecto_des']='';
            $gKey['cservicioproy']='';
            $gKey['cpropuesta']='';
            $gKey['cpropuesta_des']='';
            $gKey['cservicioprop']='';
            $gKey['carea']='';
            $gKey['carea_des']='';
            $gKey['ccentrocosto']='';
            $gKey['ccentrocosto_des']='';
            $idTabla++;
            $gKey['idTabla']=$idTabla;
            $gKey['numregistro'] = 0;
            $edgastopro=DB::table('tgastosejecuciondetalle as tgd')
            ->where('tgd.cgastosejecucion','=',$idGastoProy)
            ->orderBy('fdocumento','ASC');
            $titulo='';
            $de_personal='';
            if(!is_null($key->cproyecto) && strlen($key->cproyecto)>0 ){
                $edgastopro=$edgastopro->where('cproyecto','=',$key->cproyecto)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['cproyecto']=$key->cproyecto;
                $tab = DB::table('tproyecto as p')->where('p.cproyecto','=',$key->cproyecto)->first();
                $gKey['cproyecto_des']=$tab->nombre;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="PROY: " .$tab->codigo.'  '. $tab->nombre;
                if(!is_null($tab->cservicio)){
                    $gKey['cservicioproy']=$tab->cservicio." ";
                }
                /*$tipoact=null;
                if($tab->cservicio==2){
                    $tipoact='00001';
                }    
                else{
                    $tipoact='00003';
                } 
                //dd($proyecto);
                $tipoactividad = DB::table('tconceptogastos')
                ->whereNull('cconcepto_parent')
                ->whereIn('tipogasto',[$tipoact,'00002'])
               // ->orWhere('tipogasto','=','00002')
                ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
                ->get();*/
                //dd($tipoactividad,$tab);

            }
            if(!is_null($key->cpropuesta) && strlen($key->cpropuesta)>0){
                $edgastopro=$edgastopro->where('cpropuesta','=',$key->cpropuesta)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['cpropuesta']=$key->cpropuesta;
                $tab = DB::table('tpropuesta as p')->where('p.cpropuesta','=',$key->cpropuesta)->first();
                $gKey['cpropuesta_des']=$tab->nombre;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="PROPUESTA : " .$tab->ccodigo.'  '.  $tab->nombre." ";
                if(!is_null($tab->cservicio)){
                    $gKey['cservicioprop']=$tab->cservicio;
                }                
            }            
            if(!is_null($key->carea) && strlen($key->carea)>0){
                $edgastopro=$edgastopro->where('carea','=',$key->carea)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['carea']=$key->carea;
                $tab = DB::table('tareas as a')->where('a.carea','=',$key->carea)->first();
                $gKey['carea_des']=$tab->descripcion;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="AREA: " . $tab->descripcion." ";
            }                        
            if(!is_null($key->ccentrocosto) && strlen($key->ccentrocosto)>0){
                $edgastopro=$edgastopro->where('ccentrocosto','=',$key->ccentrocosto)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['ccentrocosto']=$key->ccentrocosto;
                $tab = DB::table('tcentrocostos as c')->where('c.ccentrocosto','=',$key->ccentrocosto)->first();
                $gKey['ccentrocosto_des']=$tab->descripcion;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="C. COSTO: " . $tab->descripcion." ";
            }                        
            $edgastopro=$edgastopro->get();
            
            $gKey['titulo']=$titulo." / "."De: ".$de_personal;;
            $num=0;
            $gastosProyDet=array();


           
           
            //dd($tipoactividad);
         
            foreach($edgastopro as $egp){
               // dd($tipoactividad,$proyecto,$egp->cconcepto_parent);

                $option_select = "";
                $option_select_tipo = "";
                $option_select="<option value=''></option>";
                foreach($tipoactividad as $act){
                    $option_select.="<option value='".$act->cconcepto."'".($egp->cconcepto_parent==$act->cconcepto?'selected':'').">";
                    $option_select.=$act->descripcion."</option>";
                }
                $tgas=DB::table('tconceptogastos')
                ->whereNotNull('cconcepto_parent')
                ->where('cconcepto_parent','=',$egp->cconcepto_parent)
                ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
                ->get();
                $option_select_tipo="<option value=''></option>";
                foreach($tgas as $g){
                    $option_select_tipo.="<option value='".$g->cconcepto."'".($egp->cconcepto==$g->cconcepto?'selected':'').">";
                    $option_select_tipo.=$g->descripcion."</option>";
                }          
                      
                $num++;

                $numdoc = explode('-', $egp->numerodocumento);
                $prefijo = null;
                $serie = null;
                $numero = null;
                // dd($numdoc);
                
                if (isset($numdoc[0])) {
                    $prefijo = $numdoc[0];
                }
                if (isset($numdoc[1])) {
                    $serie = $numdoc[1];
                }
                if (isset($numdoc[2])) {
                    $numero = $numdoc[2];
                }
                
                $gap['cgastosejecuciondet']= $egp->cgastosejecuciondet;        
                $gap['tipodocumento']= $egp->tipodocumento;
                $gap['ccentrocosto']= $egp->ccentrocosto;
                $gap['tipoactividad']= $egp->tipoactividad;                
                $gap['cconcepto_parent']=$egp->cconcepto_parent;
                $gap['tipoactividad_option']= $option_select;
                $gap['descripcion']= $egp->descripcion;
                $gap['cproyecto']= $egp->cproyecto;
                $gap['cpropuesta']= $egp->cpropuesta;
                $gap['carea'] = $egp->carea;   
                $gap['tipogasto']= $egp->tipogasto;
                $gap['cconcepto']=$egp->cconcepto;
                $gap['tipogasto_option']= $option_select_tipo;
                $gap['descripcion']= $egp->descripcion;
                $gap['reembolsable']= $egp->reembolsable;
                $gap['numerodocumentop'] = $prefijo;
                $gap['numerodocumentos'] = $serie;
                $gap['numerodocumenton'] = $numero;
                $gap['cpersona_cliente']= $egp->cpersona_cliente;
                $gap['ruc_cliente']= $egp->ruc_cliente;
                $gap['razon_cliente']= $egp->razon_cliente;
                $gap['valorcambio']= $egp->valorcambio;
                $gap['cmoneda']= $egp->cmoneda;
                $gap['cantidad'] = $egp->cantidad;     
                $gap['preciounitario']= $egp->preciounitario;
                $gap['subtotal']= $gap['cantidad']*$gap['preciounitario'];
                if($gap['tipodocumento']=='00001' || $gap['tipodocumento']=='00007'){
                    //Factura // Ticket con IGV
                    $gap['impuesto']= $gap['cantidad']*$gap['preciounitario']*0.18;
                }else{
                    $gap['impuesto']= 0;
                }
                // $gap['impuesto'] = $egp->impuesto;
                $gap['otrimp']= $egp->otroimpuesto; 
                 //$gap['subtotal']+$gap['impuesto'] + $gap['otrimp']; 

                $gap['totaldet']=$gap['subtotal']+$gap['impuesto'] + $gap['otrimp']; 

                $tcambiosoles= DB::table('tmonedacambio')
                ->where('cmoneda','=',$egp->cmoneda)
                ->where('cmonedavalor','=','S/.')
                ->where('fecha','<=',$egp->fdocumento)
                ->orderBy('fecha','DESC')
                ->first();

                $totsoles=1;
                if($tcambiosoles){
                  $totsoles=$tcambiosoles->valorcompra;
                }

                $gap['total']= $totsoles*$egp->total;

                $tcambiodolares= DB::table('tmonedacambio')
                ->where('cmoneda','=',$egp->cmoneda)
                ->where('cmonedavalor','=','USD')
                ->where('fecha','<=',$egp->fdocumento)
                ->orderBy('fecha','DESC')
                ->first();

                $totdolar=1;
                if($tcambiodolares){
                  $totdolar=$tcambiodolares->valorcompra;
                }
                
                $gap['totaldolares']= $totdolar*$egp->total;
                $gap['cpersona_empleado']= $egp->cpersona_empleado;
                $gap['ccondicionoperativa']= $egp->ccondicionoperativa;
                $gap['fdocumento']= $egp->fdocumento;      
                $gastosProyDet[count($gastosProyDet)]=$gap;
                $total+=round($gap['total'],2);
                $totaldolar+=round($gap['totaldolares'],2);
                $subtotal+=$gap['subtotal'];
                $totalImpuestos+=$gap['impuesto'];
            }
            $gKey['numregistro'] = $num;
            $gKey['tabla']=$gastosProyDet; //
            $gastosKey[count($gastosKey)]=$gKey; //agrega la tabla a esquema general
        }
        $numTabla = $idTabla;
        /*
        $subtotal=array_sum(array_map(function($i){
            return $i['subtotal'];
        },$gastosProyDet));

        $totalImpuestosProy=array_sum(array_map(function($i){
            return $i['impuesto'];
        },$gastosProyDet));
        
        $total=array_sum(array_map(function($i){
            return $i['subtotal']+$i['impuesto'];
        },$gastosProyDet));*/
        
           
        //Session::put('listgaspro',$gastosProyDet);

        $est_gasto= DB::table('tgastosejecucion')
        ->where('cgastosejecucion','=',$idGastoProy)
        ->first();

        $estado= 1;
        if ($est_gasto) {

            if ($est_gasto->cestadorendiciongasto=='3') {
                $estado=0;
            }
           
        }

        $saldo=floatval($viatico)-floatval($total);

        
        
        return view('gastos/gastoProyecto',compact('numrendicion','user','descripcions','frendicion','valorcambio','tipoactividad','tipogasto','tipodocumento','tmoneda','area','ccosto','personal','gastosKey','subtotal','total','totaldolar','gasto','numTabla','cat_tipogastos','viatico','estado','saldo','tiporendicion'));
    }

/*
*Agregar proyecto sin USO actualmente
*/
 public function agregarGastosProy(Request $request){
    $total=0;
    $subtotal=0;
    $totalImpuestosProy=0;

    $gastosProyDet=array();
    if(Session::get('listgaspro')){
        $gastosProyDet=Session::get('listgaspro',array());
    }
       
    $i= -1*(count($gastosProyDet)+1)*10;
    $cgastosejecuciondet = $i;
    $tipodocumento = $request->input('tipodocumento');
    $ccentrocosto = $request->input('ccentrocosto');
    $tipoactividad = $request->input('tipoactividad');
    $cproyecto = $request->input('cproyecto');
    $cpropuesta = $request->input('cpropuesta');
    $carea = $request->input('carea');
    $tipogasto = $request->input('tipogasto');
    $descripcion = $request->input('descripcion');
    $reembolsable = $request->input('reembolsable');
    $numerodocumento = $request->input('numerodocumento');
    $cpersona_cliente = $request->input('cpersona_cliente');
    $ruc_cliente = $request->input('ruc_cliente');
    $razon_cliente = $request->input('razon_cliente');
    $valorcambio = $request->input('valorcambio');
    $cmoneda = $request->input('cmoneda');
    $cantidad = $request->input('cantidad');
    $preciounitario = $request->input('preciounitario');
    //$subtotal = $request->input('subtotal');
    $impuesto = $cantidad*$preciounitario*0.18;
    //$total = $request->input('total');
    $cpersona_empleado = $request->input('cpersona_empleado');
    $ccondicionoperativa = $request->input('ccondicionoperativa'); 
    $fdocumento = $request->input('fdocumento'); 
    
    
        $gap['cgastosejecuciondet']= $cgastosejecuciondet;
        $gap['tipodocumento']= $tipodocumento;
        $gap['ccentrocosto']= $ccentrocosto;
        $gap['tipoactividad']= $tipoactividad;
        $gap['descripcion']= $descripcion;
        $gap['cproyecto']= $cproyecto;
        $gap['cpropuesta']= $cpropuesta;
        $gap['carea'] = $carea;   
        $gap['tipogasto']= $tipogasto;
        $gap['descripcion']= $descripcion;
        $gap['reembolsable']= $reembolsable;
        $gap['numerodocumento'] = $numerodocumento;     
        $gap['cpersona_cliente']= $cpersona_cliente;
        $gap['ruc_cliente']= $ruc_cliente;
        $gap['razon_cliente']= $razon_cliente;
        $gap['valorcambio']= $valorcambio;
        $gap['cmoneda']= $cmoneda;
        $gap['cantidad'] = $cantidad;     
        $gap['preciounitario']= $preciounitario;
        //$gap['subtotal']= $subtotal;
        $gap['subtotal']= $gap['cantidad']*$gap['preciounitario'];
        //$gap['impuesto']= $impuesto;
        $gap['impuesto']= $gap['cantidad']*$gap['preciounitario']*0.18;
        $gap['total']= $subtotal+$impuesto; 
        $gap['cpersona_empleado']= $cpersona_empleado;
        $gap['ccondicionoperativa']= $ccondicionoperativa;
        $gap['fdocumento']= $fdocumento;      
        $gastosProyDet[count($gastosProyDet)]=$gap;

        $subtotal=array_sum(array_map(function($i){
            return $i['subtotal'];
        },$gastosProyDet));

        $totalImpuestosProy=array_sum(array_map(function($i){
            return $i['impuesto'];
        },$gastosProyDet));
        
        $total=array_sum(array_map(function($i){
            return $i['subtotal']+$i['impuesto'];
        },$gastosProyDet));
        //dd( $gap['subtotal']);
              
        Session::put('listgaspro',$gastosProyDet);
    return view('partials.modalGastosProyectoDetalle',compact('gastosProyDet','subtotal','total','totalImpuestosProy')); 

 }    

 /*
 * Sin uso
 */
    public function delGastoProy($idGastoProy){
        $gastosProyDet = array();
        if(Session::get('listgaspro')){
            $gastosProyDet = Session::get('listgaspro');
        }
        if($idGastoProy > 0){
            $delGastoProy=array();
            if(Session::get('delGastoProy')){
                $delGastoProy = Session::get('delGastoProy');
            }
            $delGastoProy[count($delGastoProy)]=$idGastoProy;
            Session::put('delGastoProy',$delGastoProy);
        }        
        $newGastoProy= array();
        foreach($gastosProyDet as $gap){
            if ($gap['cgastosejecuciondet']!=$idGastoProy){
                $newGastoProy[count($newGastoProy)]=$gap;
            }            

        }        
        $gastosProyDet = $newGastoProy;
        Session::put('listgaspro',$gastosProyDet);
        return view('partials.modalGastosProyectoDetalle',compact('gastosProyDet'));  
    }

    /*
    * Grabar Gastos de renidiones de Proyectos.
    */
    public function saveGastoProy(Request $request){

        $cpersona=Auth::user()->cpersona;
        
        DB::beginTransaction();
        
        $cgastosejecucion=$request->input('cgastosejecucion');
        
        /*
        $listgaspro=array();
        if(Session::get('listgaspro')) {
            $listgaspro=Session::get('listgaspro');
        }*/
        //dd($listgaspro);
        $objHelper = new HelperSIGSupport();


        if(strlen($cgastosejecucion)<=0){
            $newGastoProy=new Tgastosejecucion();
            $newGastoProy->numerorendicion=$objHelper->genNumeroRendicion('006','GASTOS');
            $newGastoProy->frendicion=Carbon::now();     
            $newGastoProy->cestadorendiciongasto=1;
            $newGastoProy->per_registrador=$cpersona;
        }
        else{ $newGastoProy=Tgastosejecucion::where('cgastosejecucion','=',$cgastosejecucion)
            ->first();
        }
                

        $newGastoProy->valorcambio=$request->input('valorcambios');
        $newGastoProy->descripcion=$request->input('descripcions');
        $newGastoProy->tipo_rendicion = $request->input('ctipo_rendicion');  
        $newGastoProy->cmoneda_devolucion = $request->input('cmoneda_devolucion');
        $newGastoProy->viatico=$request->input('viatico');

        

        //$newGastoProy->total=$request->input('total');        
        $newGastoProy->save();
        $numTabla = $request->input('numtabla');
        $total=0;
        $totaldol=0;
        for($w=1;$w<=$numTabla;$w++){
            $nr = $request->input('numregistro_'.$w);
            
            for($t=1;$t<=$nr;$t++){
                if(!is_null($request->input('tipodocumento_'.$w.'_'.$t)) && strlen($request->input('tipodocumento_'.$w.'_'.$t) && !is_null($request->input('tipoactividad_'.$w.'_'.$t)) && strlen($request->input('tipoactividad_'.$w.'_'.$t)) >0 ) >0 ){
                    if(!is_null($request->input('cgastoejecuciondet_'.$w."_".$t))){
                        //dd($request->input('cgastoejecuciondet_'.$w."_".$t));
                        //Si existe informaciÃ³n
                        if($request->input('cgastoejecuciondet_'.$w."_".$t)<=0 || strlen($request->input('cgastoejecuciondet_'.$w."_".$t))<=0){                       

                            $newGastoProyDetalle=new Tgastosejecuciondetalle();
                            $newGastoProyDetalle->ccondicionoperativa='REG';
                            $newGastoProyDetalle->cgastosejecucion=$newGastoProy->cgastosejecucion; 

                            $newGastoProyDetalle->fregistro=Carbon::now();
                            //dd("1");
                        }
                        else{
                            $newGastoProyDetalle=Tgastosejecuciondetalle::where('cgastosejecuciondet','=',$request->input('cgastoejecuciondet_'.$w.'_'.$t))
                            ->first();
                            //dd("2");
                        }
                        $newGastoProyDetalle->tipodocumento=$request->input('tipodocumento_'.$w.'_'.$t);
                        
                        if(!is_null($request->input('ccentrocosto_'.$w)) && strlen($request->input('ccentrocosto_'.$w)) >0 ){
                            $newGastoProyDetalle->ccentrocosto = $request->input('ccentrocosto_'.$w);   
                        }
                        if(strlen($request->input('tipoactividad_'.$w.'_'.$t))>0){
                            $newGastoProyDetalle->cconcepto_parent=$request->input('tipoactividad_'.$w.'_'.$t);
                        }else{
                            $newGastoProyDetalle->cconcepto_parent=null;
                        }

                        if(!is_null($request->input('cproyecto_'.$w)) && strlen($request->input('cproyecto_'.$w))>0) {
                            $newGastoProyDetalle->cproyecto=$request->input('cproyecto_'.$w);
                        }
                        if(!is_null($request->input('cpropuesta_'.$w)) && strlen($request->input('cpropuesta_'.$w))>0 ){
                            $newGastoProyDetalle->cpropuesta=$request->input('cpropuesta_'.$w);
                        }

                        if(!is_null($request->input('carea_'.$w)) && strlen($request->input('carea_'.$w))>0 ){
                            $newGastoProyDetalle->carea=$request->input('carea_'.$w);
                        }
                            

                        if(strlen($request->input('tipogasto_'.$w.'_'.$t))>0){
                            $newGastoProyDetalle->cconcepto=$request->input('tipogasto_'.$w.'_'.$t);
                        }else{
                            $newGastoProyDetalle->cconcepto=null;
                        }
                        $newGastoProyDetalle->descripcion=$request->input('descripcion_'.$w.'_'.$t);
                        if ($request->input('descripcion_'.$w.'_'.$t)=='') {
                            return "descripcion";
                        }
                        if(strlen($request->input('reembolsable_'.$w.'_'.$t))>0){
                            $newGastoProyDetalle->reembolsable=$request->input('reembolsable_'.$w.'_'.$t);
                        }else{
                            $newGastoProyDetalle->reembolsable='00002';
                        }
                        
                        $newGastoProyDetalle->numerodocumento=$request->input('numerodocumento_p'.$w.'_'.$t)."-".$request->input('numerodocumento_s'.$w.'_'.$t)."-".$request->input('numerodocumento_n'.$w.'_'.$t);
                        
                        if(!is_null($request->input('cpersona_prov_'.$w.'_'.$t)) && strlen($request->input('cpersona_prov_'.$w.'_'.$t))>0){
                            $newGastoProyDetalle->cpersona_cliente=$request->input('cpersona_prov_'.$w.'_'.$t);
                        }
                        $newGastoProyDetalle->ruc_cliente=$request->input('ruc_'.$w.'_'.$t);
                        $newGastoProyDetalle->razon_cliente=$request->input('razon_'.$w.'_'.$t);
                        //if(!is_null($request->input('valorcambio')) && strlen($request->input('valorcambio'))>0 ){
                        $newGastoProyDetalle->valorcambio=$request->input('valorcambio');
                        //}

                        $newGastoProyDetalle->cmoneda=$request->input('cmoneda_'.$w.'_'.$t);
                        $newGastoProyDetalle->cantidad=$request->input('cantidad_'.$w.'_'.$t);
                        $newGastoProyDetalle->preciounitario=$request->input('preciou_'.$w.'_'.$t);
                        $newGastoProyDetalle->subtotal=$request->input('preciou_'.$w.'_'.$t)*$request->input('cantidad_'.$w.'_'.$t);
                        if(strlen($request->input('impuesto_'.$w.'_'.$t))>0 ){
                            $newGastoProyDetalle->impuesto=$request->input('impuesto_'.$w.'_'.$t);  
                        }else{
                            $newGastoProyDetalle->impuesto=0;  
                        }
                        if(strlen($request->input('otrimp_'.$w.'_'.$t))>0 ){
                            $newGastoProyDetalle->otroimpuesto=$request->input('otrimp_'.$w.'_'.$t);  
                        }else{
                            $newGastoProyDetalle->otroimpuesto=0;  
                        }                    
                        $newGastoProyDetalle->total=$request->input('total_'.$w.'_'.$t)/**$request->input('cantidad_'.$w.'_'.$t)+$request->input('impuesto_'.$w.'_'.$t) + $request->input('otrimp_'.$w.'_'.$t)*/;  

                        /*inicio conversiÃ³n de tipomoneda*/
                          //dd($newGastoProyDetalle->cmoneda);

                        $tcambiosoles= DB::table('tmonedacambio')
                        ->where('cmoneda','=',$newGastoProyDetalle->cmoneda)
                        ->where('cmonedavalor','=','S/.')
                        ->where('fecha','<=',$request->input('fdocumento_'.$w.'_'.$t))
                        ->orderBy('fecha','DESC')
                        ->first();
                        
                        $cambsoles=1;
                        if($tcambiosoles){
                          $cambsoles=$tcambiosoles->valorcompra;

                        }

                        $total =$total+ ($newGastoProyDetalle->total*$cambsoles);

                        //dd($total,$newGastoProyDetalle->total,$cambsoles);

                        $tcambiodolares= DB::table('tmonedacambio')
                        ->where('cmoneda','=',$newGastoProyDetalle->cmoneda)
                        ->where('cmonedavalor','=','USD')
                        ->where('fecha','<=',$request->input('fdocumento_'.$w.'_'.$t))
                        ->orderBy('fecha','DESC')
                        ->first();

                        $cambdolares=1;
                        if($tcambiodolares){
                          $cambdolares=$tcambiodolares->valorcompra;

                        }

                        $totaldol =$totaldol+ ($newGastoProyDetalle->total*$cambdolares);

                            //dd($totaldol,$newGastoProyDetalle->total,$cambdolares);          
                        /*fin conversiÃ³n de tipomoneda*/

                        if(!is_null($request->input('cpersona_empleado_'.$w.'_'.$t)) && strlen($request->input('cpersona_empleado_'.$w.'_'.$t))>0){
                            $newGastoProyDetalle->cpersona_empleado=$request->input('cpersona_empleado_'.$w.'_'.$t);
                        }else if(!is_null($request->input('cpersona_empleado')) && strlen($request->input('cpersona_empleado'))>0){
                            $newGastoProyDetalle->cpersona_empleado=$request->input('cpersona_empleado');
                        }else{
                            $newGastoProyDetalle->cpersona_empleado=Auth::user()->cpersona;
                        }
                    
                        $newGastoProyDetalle->fdocumento=$request->input('fdocumento_'.$w.'_'.$t);

                        $newGastoProyDetalle->save();

                    }
                }
            }
        }

        $newGastoProy->total=$total; 
        $newGastoProy->totaldolares=$totaldol;     
        //dd($newGastoProy);   
        $newGastoProy->save();

        
        $cgastosejecucion=$newGastoProy->cgastosejecucion;

        DB::commit();
        return Redirect::route('editargastoProyecto',$cgastosejecucion);
    }

    public function enviarGastoProy(Request $request){

         DB::beginTransaction();     
         $cgastosejecucion=$request->input('cgastosejecucion');
         $newGastoProy = Tgastosejecucion::where('cgastosejecucion','=',$cgastosejecucion)->first();
         if($newGastoProy){
            $newGastoProy->cestadorendiciongasto=3;
            $newGastoProy->save();
         }

        $ejecu=DB::table('tgastosejecuciondetalle')       
        ->where('cgastosejecucion','=',$cgastosejecucion) 
        ->get();

        $objFlow = new FlowSupport();
        foreach($ejecu as $ej){
            $peje=Tgastosejecuciondetalle::where('cgastosejecuciondet','=',$ej->cgastosejecuciondet)
            ->first();

            $resul = $objFlow->obtenerSiguienteCondicionAut("006","1",$peje->ccondicionoperativa);

            

            $objFlow->guardarAprobacionGastos($ej->cgastosejecuciondet,$peje->ccondicionoperativa,$resul[0][0],'1',Auth::user()->cpersona,1);
            $peje->ccondicionoperativa=$resul[0][0];
            
            $peje->save();
        }

        DB::commit();
     
        //return Redirect::route('listaGastosProyecto');
        return view('gastos/listarGastosProyecto');

          //return Redirect::route('editargastoProyecto',$cgastosejecucion);   
    }

    public function reabrirRendicionGasto($idRedicion){

        // DB::beginTransaction();     
         // $cgastosejecucion=$request->input('cgastosejecucion');
         $newGastoProy = Tgastosejecucion::where('cgastosejecucion','=',$idRedicion)->first();

         //print_r($newGastoProy);
         if($newGastoProy){
            if($newGastoProy->cestadorendiciongasto!=5){
            $newGastoProy->cestadorendiciongasto=1;
            $newGastoProy->save();
         }
         }

         $ejecu=DB::table('tgastosejecuciondetalle')       
        ->where('cgastosejecucion','=',$idRedicion) 
        ->get();

        foreach($ejecu as $ej){
            $peje=Tgastosejecuciondetalle::where('cgastosejecucion','=',$idRedicion)->first();
            if ($peje) {
            // dd($peje->ccondicionoperativa);
                if ($peje->ccondicionoperativa!='APR') {
                    $peje->ccondicionoperativa='REG';
                    $peje->save();
                }   
            }
        }

         DB::commit();
         return Redirect::route('listaGastosProyecto');

    }

    public function deleteRendicionGasto($idRedicion){

         $ejecu=DB::table('tgastosejecuciondetalle')       
        ->where('cgastosejecucion','=',$idRedicion) 
        ->get();

        foreach($ejecu as $ej){
            $peje=Tgastosejecuciondetalle::where('cgastosejecucion','=',$idRedicion)->first();
            if ($peje) {
            // dd($peje->ccondicionoperativa);
            if ($peje->ccondicionoperativa=='REG') {
                $peje->delete();
            }
            }
        }

         $newGastoProy = Tgastosejecucion::where('cgastosejecucion','=',$idRedicion)->first();
         if ($newGastoProy) {
          // dd($newGastoProy->cestadorendiciongasto);
         if($newGastoProy->cestadorendiciongasto==1){
            $newGastoProy->delete();
         }
         }

         return Redirect::route('listaGastosProyecto');

    }
    
    public function editargastoAdmXProyecto(Request $request,$idProyecto){

        Session::forget('listAdmProy');

        $tcliente =DB::table('tpersona as tp')
        ->leftJoin('tpersonajuridicainformacionbasica as pji','tp.cpersona','=','pji.cpersona')
        ->orderBy('tp.nombre','ASC')
        ->lists('tp.nombre','tp.cpersona');
        $tcliente = [''=>''] + $tcliente;

        $tuminera = DB::table('tunidadminera')
        ->lists('nombre','cunidadminera');
        $tuminera = [''=>'']+ $tuminera;

        
        $tproyecto = DB::table('tproyecto')
        ->lists('nombre','cproyecto');
        $tproyecto = [''=>'']+ $tproyecto;

        $tsubproyecto = DB::table('tproyectosub')
        ->lists('descripcionsub','cproyectosub');
        $tsubproyecto = [''=>'']+ $tsubproyecto;

        $admproy=array();

        Session::put('listAdmProy',$admproy);

        return view('gastos/gastoAdmXProyecto',compact('tcliente','tuminera','tproyecto','tsubproyecto'));
    }

    public function saveGastoAdmProy(Request $request){
        DB::beginTransaction();
        $cproyecto=$request->input('cproyecto');

        DB::commit();
        return Redirect::route('editargastoAdmXProyecto',$cproyecto);
    }

    public function btnNuevoGastoAdmProy(Request $request,$idProyecto){


         return view('gastos/GastoAdmProy');   
    }

    public function cancelGastoAdmProyecto(Request $request){


         return view('gastos/GastoVariosProyecto');   
    }

    public function enviarGastoAdmProyecto(Request $request){


         return view('gastos/GastoVariosProyecto');   
    }

   

    public function editargastoVariosProyecto(Request $request,$idProyecto){

        Session::forget('listAdmProy');

        $tcliente =DB::table('tpersona as tp')
        ->leftJoin('tpersonajuridicainformacionbasica as pji','tp.cpersona','=','pji.cpersona')
        ->orderBy('tp.nombre','ASC')
        ->lists('tp.nombre','tp.cpersona');
        $tcliente = [''=>''] + $tcliente;

        $tuminera = DB::table('tunidadminera')
        ->lists('nombre','cunidadminera');
        $tuminera = [''=>'']+ $tuminera;

        
        $tproyecto = DB::table('tproyecto')
        ->lists('nombre','cproyecto');
        $tproyecto = [''=>'']+ $tproyecto;

        $tsubproyecto = DB::table('tproyectosub')
        ->lists('descripcionsub','cproyectosub');
        $tsubproyecto = [''=>'']+ $tsubproyecto;

        $admproy=array();

        Session::put('listAdmProy',$admproy);

        return view('gastos/gastoVariosProyecto',compact('tcliente','tuminera','tproyecto','tsubproyecto'));
    }


   
    
    public function saveGastoVariosProyecto(Request $request){
        DB::beginTransaction();
        $cproyecto=$request->input('cproyecto');

        DB::commit();
        return Redirect::route('editargastoVariosProyecto',$cproyecto);
    }

    

    public function btnNuevoGastoVariosProyecto(Request $request,$idProyecto){


         return view('gastos/GastoVariosProyecto');   
    }

    public function cancelVariosProyecto(Request $request){


         return view('gastos/GastoVariosProyecto');   
    }

    public function enviarVariosProyecto(Request $request){


         return view('gastos/GastoVariosProyecto');   
    }

    //Gastos Aprobacion Rendiciones  

    
    public function RendicionUsuario(){
        $objUsu = new EmpleadoSupport(); 
        $usuario_ren = $objUsu->getPersonalRol('GP','001','2') ;
        $usuario_ren = [''=>''] + $usuario_ren; 

        $areas = Tarea::lists('descripcion','carea')->all();
        $areas = [''=>''] + $areas;   
        return view('gastos.rendicionUsuario',compact('areas'));
    }

    public function viewRendicionUsuario(Request $request){     
        $fdesde="";
        $fhasta=""; 
        if (strlen($request->input('fhasta'))>0 && strlen($request->input('fdesde'))>0){
            $fde= $request->input('fdesde');
            $fha= $request->input('fhasta');
            $fdesde = $fde;
            $fhasta = $fha;
            if(substr($fde,2,1)=='/'){    
                $fdesde = Carbon::createFromDate(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2));
            }else{
                $fdesde = Carbon::createFromDate(substr($fdesde,0,4),substr($fdesde,5,2),substr($fdesde,8,2));
            } 

            if(substr($fha,2,1)=='/'){    
                $fhasta = Carbon::createFromDate(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2));
            }else{
                $fhasta = Carbon::createFromDate(substr($fhasta,0,4),substr($fhasta,5,2),substr($fhasta,8,2));
            }        
        }

        $query=DB::table('tgastosejecucion as tg')
        ->join('tgastosejecuciondetalle as tgd','tgd.cgastosejecucion','=','tg.cgastosejecucion')  
        ->leftjoin('tproyecto as tpr','tgd.cproyecto','=','tpr.cproyecto')
        ->leftjoin('tmonedas as tm','tgd.cmoneda','=','tm.cmoneda')          
        ->select('tg.numerorendicion as rendicion',DB::raw(' date(tgd.fdocumento) as fecha'),'tpr.nombre as proyecto','tm.descripcion as moneda ','tgd.total','tgd.ccondicionoperativa','tgd.descripcion as comentario')
        ->orderBy("tg.frendicion","ASC")
        ->orderBy("tg.numerorendicion","ASC");

        if (strlen($request->input('tipoactividad'))>0){
            $query= $query->where('tgd.tipoactividad','=',$request->input('tipoactividad'));
        }
        if (strlen($request->input('carea'))>0){
            $query= $query->where('tgd.carea','=',$request->input('carea'));
        } 

        //dd($fdesde);
        //dd($fhasta);
        if (strlen($request->input('fhasta'))>0 && strlen($request->input('fdesde'))>0){
            //$query = $query->where('tgd.fdocumento','<=',$request->input('fhasta'));            
            $query = $query->whereBetween('tgd.fdocumento',array($fdesde,$fhasta));
        }


        $cgastosejecucion = $request->input('cgastosejecucion');
         
        $listaUsuario=array();
        $gastos_ren=$query->get();
        $it =1;
        foreach($gastos_ren as $gr){
            $usu['item']=$it;
            $usu['numerorendicion'] = $gr->rendicion;
            $usu['fdocumento'] = $gr->fecha;
            $usu['proyecto'] = $gr->proyecto;
            $usu['moneda'] = $gr->moneda;
            $usu['total'] = $gr->total;
            $usu['ccondicionoperativa'] = $gr->ccondicionoperativa;
            $usu['comentario'] = $gr->comentario;
            $it++;
            $listaUsuario[count($listaUsuario)]=$usu;

        }         
        
        return view('partials.tableRendicionUsuario',compact('listaUsuario','fdesde','fhasta','cgastosejecucion'));
    }

     public function viewProyRendicion(Request $request,$idProyecto){
        $tproyecto=DB::table('tproyecto')
        ->where('cproyecto','=',$idProyecto)
        ->first();      
        return view('partials.verProyAprobacionGasto',compact('tproyecto'));
    }

    public function viewAprobacionRendicion(Request $request){
       
        $fdesde="";
        $fhasta=""; 
        if (strlen($request->input('fhasta'))>0 && strlen($request->input('fdesde'))>0){
            $fde= $request->input('fdesde');
            $fha= $request->input('fhasta');
            $fdesde = $fde;
            $fhasta = $fha;
            if(substr($fde,2,1)=='/'){    
                $fdesde = Carbon::createFromDate(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2));
            }else{
                $fdesde = Carbon::createFromDate(substr($fdesde,0,4),substr($fdesde,5,2),substr($fdesde,8,2));
            } 

            if(substr($fha,2,1)=='/'){    
                $fhasta = Carbon::createFromDate(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2));
            }else{
                $fhasta = Carbon::createFromDate(substr($fhasta,0,4),substr($fhasta,5,2),substr($fhasta,8,2));
            }        
        }

        $query=DB::table('tgastosejecucion as tg')
        ->join('tgastosejecuciondetalle as tgd','tgd.cgastosejecucion','=','tg.cgastosejecucion')
        ->leftJoin('tpersona as p','tgd.cpersona_empleado','=','p.cpersona')  
        /*->leftjoin('tpropuesta as tp','tgd.cpropuesta','=','tp.cpropuesta')
        ->leftjoin('tproyecto as tpr','tgd.cproyecto','=','tpr.cproyecto')*/
        ->leftjoin('tmonedas as tm','tg.cmoneda_devolucion','=','tm.cmoneda')
        ->whereIn('ccondicionoperativa',['RCP','RGP'])         
        ->select('tg.cgastosejecucion','tg.numerorendicion as rendicion',DB::raw('date(tg.frendicion) as fecha'),'tg.descripcion as gasto','tm.descripcion as moneda ')
        ->addSelect('tg.total','tgd.cpersona_empleado','p.nombre')
        ->distinct();
        /*->orderBy("tg.frendicion","ASC")
        ->orderBy("tg.numerorendicion","ASC");*7


        /*if (strlen($request->input('tipoactividad'))>0){
            $query= $query->where('tgd.tipoactividad','=',$request->input('tipoactividad'));
        }*/

        /*if (strlen($request->input('tproyecto'))>0){
            $query= $query->where('tgd.cproyecto','=',$request->input('tproyecto'));
        } */

        /*if (strlen($request->input('carea'))>0){
            $query= $query->where('tgd.carea','=',$request->input('carea'));
        }        */

        if (strlen($request->input('cpersona_empleado'))>0){
            $query= $query->where('tgd.cpersona_empleado','=',$request->input('cpersona_empleado'));
        } 

         if (strlen($request->input('fhasta'))>0 && strlen($request->input('fdesde'))>0){
            //$query = $query->where('tgd.fdocumento','<=',$request->input('fhasta'));            
            $query = $query->whereBetween('tg.frendicion',array($fdesde,$fhasta));
        }

        
        $cgastosejecucion=$request->input('cgastosejecucion');
        $listaAprobacion=array();
        $aprobaciongastos_ren=$query->get();
       
        $it=1;
        foreach($aprobaciongastos_ren as $ar){               
            $ag['item']=$it;
            $ag['cgastosejecucion']=$ar->cgastosejecucion;
            $ag['numerorendicion'] = $ar->rendicion;
            $ag['fdocumento'] = $ar->fecha;
            $ag['moneda'] = $ar->moneda;
            $ag['total'] = $ar->total;
            $ag['cpersona_empleado']=$ar->cpersona_empleado;
            $ag['empleado']=$ar->nombre;
            $ag['gasto']= $ar->gasto;            
            $listaAprobacion[count($listaAprobacion)]=$ag;
            $it++;
        }       

        
           
         return view('partials.tableAprobacionRendicionGastos',compact('listaAprobacion','fdesde','fhasta','cgastosejecucion'));

    }    
    public function verDetalleGasto(Request $request){
        $cgastosejecucion = $request->input('cgastosejecucion');
        $listGastos = array(); 
        $w=0;
        if(strlen($cgastosejecucion)>0){
            $ldgas = DB::table('tgastosejecuciondetalle as det')
            ->leftJoin('tconceptogastos as g','g.cconcepto','=','det.cconcepto_parent')
            ->leftJoin('tproyecto as pry','det.cproyecto','=','pry.cproyecto')
            ->leftJoin('tpropuesta as pro','det.cpropuesta','=','pro.cpropuesta')
            ->leftJoin('tareas as ar','det.carea','=','ar.carea')
            ->leftJoin('tcentrocostos as cen','det.ccentrocosto','=','cen.ccentrocosto')
            ->where('det.cgastosejecucion','=',$cgastosejecucion)
            ->select('det.*','g.descripcion as gasto','pry.nombre as proyecto','pro.nombre as propuesta')
            ->orderBy('ar.carea','ASC')
            ->addSelect('ar.descripcion as area','cen.descripcion as ccosto')
            ->get();
            foreach($ldgas as $d){
                $w++;
                $destino = "";
                if($d->proyecto!=null){
                    $destino ="PROY: ". $d->proyecto;
                }
                if($d->propuesta!=null){
                    $destino.= " PROP: ". $d->propuesta;
                }
                if($d->area!=null){
                    $destino.= "AREA: ". $d->area;
                }
                if($d->ccosto!=null){
                    $destino .= "CCOSTO: ". $d->ccosto;
                }                                                
                $g=array();
                $g['item']=$w;
                $g['destino']=$destino;
                $g['tipogasto']=$d->gasto;
                $g['fecha']= $d->fdocumento;
                $g['proveedor']= $d->ruc_cliente ."-".$d->razon_cliente;
                $g['descripcion']= $d->descripcion;
                $g['moneda']= $d->cmoneda;
                $g['total']= $d->total;
                $g['cgastosejecuciondet']=$d->cgastosejecuciondet;
                $listGastos[count($listGastos)]=$g;
            }
        }
        return view('partials.modalVerGasto',compact('listGastos'));
    }
    public function aprobarGastoDetalle(Request $request){
        
        DB::beginTransaction();
        $checks = $request->input('cgastosejecuciondet') ;
        $objFlow = new FlowSupport();
        foreach($checks as $c){
            $detGasto = Tgastosejecuciondetalle::where('cgastosejecuciondet','=',$c)->first();
            if($detGasto){
                
                $resul = $objFlow->obtenerSiguienteCondicionAut("006","1",$detGasto->ccondicionoperativa);
                $objFlow->guardarAprobacionGastos($c,$detGasto->ccondicionoperativa,$resul[0][0],'1',Auth::user()->cpersona,1);
                $detGasto->ccondicionoperativa=$resul[0][0];
                $detGasto->save();

            }
        }
        
        DB::commit();
        
    }
    public function observarGastoDetalle(Request $request){
        DB::beginTransaction();
        $checks = $request->input('cgastosejecuciondet');
        $objFlow = new FlowSupport();        
        foreach($checks as $c){        
            $detGasto = Tgastosejecuciondetalle::where('cgastosejecuciondet','=',$c)->first();
            if($detGasto){

                $resul = $objFlow->obtenerSiguienteCondicionAut("006","1",$detGasto->ccondicionoperativa);
                $objFlow->guardarAprobacionGastos($c,$detGasto->ccondicionoperativa,$resul[1][0],'1',Auth::user()->cpersona,1);                
                $detGasto->ccondicionoperativa=$resul[1][0];
                $detGasto->save();

            }
        }
        DB::commit();        
    }    
    public function aprobarGastoDetalleSel(Request $request){
        DB::beginTransaction();
        $checks = $request->input('checks');
        $objFlow = new FlowSupport();
        foreach($checks as $k){
            $det = DB::table('tgastosejecuciondetalle')
            ->where('cgastosejecucion','=',$k)
            ->get();
            foreach($det as $d){
                $detGasto = Tgastosejecuciondetalle::where('cgastosejecuciondet','=',$d->cgastosejecuciondet)->first();
                if($detGasto){
                    $resul = $objFlow->obtenerSiguienteCondicionAut("006","1",$detGasto->ccondicionoperativa);
                    $objFlow->guardarAprobacionGastos($d->cgastosejecuciondet,$detGasto->ccondicionoperativa,$resul[0][0],'1',Auth::user()->cpersona,1);
                    $detGasto->ccondicionoperativa=$resul[0][0];
                    $detGasto->save();
                }
            }

        }
        
        DB::commit();
        
    }
    public function observarGastoDetalleSel(Request $request){
        DB::beginTransaction();
        $checks = $request->input('checks');
        $objFlow = new FlowSupport();
        foreach($checks as $k){
            $det = DB::table('tgastosejecuciondetalle')
            ->where('cgastosejecucion','=',$k)
            ->get();
            foreach($det as $d){            
                $detGasto = Tgastosejecuciondetalle::where('cgastosejecuciondet','=',$d->cgastosejecuciondet)->first();
                if($detGasto){
                    $resul = $objFlow->obtenerSiguienteCondicionAut("006","1",$detGasto->ccondicionoperativa);
                    $objFlow->guardarAprobacionGastos($d->cgastosejecuciondet,$detGasto->ccondicionoperativa,$resul[1][0],'1',Auth::user()->cpersona,1);                    
                    $detGasto->ccondicionoperativa=$resul[1][0];
                    $detGasto->save();
                }
            }

        }
        
        DB::commit();
    }    
    //Fin Gastos Aprobacion Rendiciones

    public function printGastoProy(Request $request){  

        $cgastosejecucion=$request->input('cgastosejecucion');            


        $rendiciongasto=DB::table('tgastosejecucion as tge')
        ->where('tge.cgastosejecucion','=',$cgastosejecucion)
        //->select('tge.descripcion')     
        ->first();

        //dd($rendiciongasto);

        $listRend=DB::table('tgastosejecuciondetalle as tg')        
        ->leftJoin('tpersona as tp','tg.cpersona_empleado','=','tp.cpersona')
        ->where('tg.cgastosejecucion','=',$cgastosejecucion)
        ->select('tp.identificacion as identificacion')
        ->first();
     

        $fecha= Carbon::now();
        $fecha=substr($fecha->format('d-m-Y'),0,10);

        $fec=substr($rendiciongasto->frendicion,0,10);
       // dd($rendiciongasto);

       
        $Nombre=substr($rendiciongasto->descripcion,0,55);    
        $DNI=$listRend->identificacion;
        $NumRendicion=$rendiciongasto->numerorendicion;

      

        $emp=DB::table('tpersona')
                ->where('cpersona','=',$rendiciongasto->per_registrador)
                ->first();
        $Empleado=$emp->abreviatura;
        $Recibido=$rendiciongasto->viatico;
        $Rendido=$rendiciongasto->total;
        $TotalRend=$Recibido - $Rendido;;

        $obj= new GastosPdf('P','mm','A4');   
        $obj->AddPage();
        // Inicio Cabecera

          $obj->SetDrawColor(179,179,179);
          $logo = storage_path('archivos')."/images/logorpt.jpg";
          $color= storage_path('archivos')."/images/colorAnddes.png";
          $obj->Image($logo,15,10,50,10);

          $obj->SetFont('Arial','B',10); 

          $obj->SetXY(15,20.5);
          $obj->Image($color,15,20.5,50,8.5);
          $obj->SetTextColor(255,255,255); 
          $obj->Cell(50,8.5,"SIG AND",1,0,'C');

          $obj->SetTextColor(0,0,0); 
          $obj->SetXY(15,10);
          $obj->Cell(50,19,'',1,0,'C');
        
          $obj->SetXY(75,10);
          $obj->MultiCell(60,5,"\n".utf8_decode("AdministraciÃ³n y Finanzas RendiciÃ³n de Gastos"),0,'C');
          $obj->Rect(65,10,80,19);

          $obj->SetXY(145,10);
          $obj->Cell(50,9.5,'',1,0,'C');

          $obj->SetXY(145,19.5);
          $obj->MultiCell(50,5,"10-AND-42-FOR-0100/R0/".$fecha,0,'C');
          $obj->Rect(145,19.5,50,9.5);

          /*--------------------------------------------*/

          $obj->SetFillColor(255,255,255); 
          $obj->SetXY(10,32);
          $obj->Cell(24,12,utf8_decode("NÂ° RendiciÃ³n"),0,0,'L');

          $obj->SetXY(34,32);
          $obj->Cell(31,12,$NumRendicion,0,0,'L'); 

          $obj->SetXY(10,42);
          $obj->Cell(24,6,"Registrado",0,0,'L');
          $obj->SetXY(10,46);
          $obj->Cell(24,6,"Por",0,0,'L');
          
          $obj->SetXY(34,45);
          $obj->MultiCell(31,4,$Empleado,0,'L');

          $obj->SetXY(68,32);
          $obj->Cell(30,12,utf8_decode("TÃ­tulo RendiciÃ³n"),0,0,'L');

          $obj->SetXY(100,34);
          $obj->MultiCell(53,4,utf8_decode($Nombre),0,'L');
     
          $obj->SetXY(68,41);
          $obj->Cell(30,12,"DNI Usuario",0,0,'L');

          $obj->SetXY(100,41);
          $obj->Cell(50,12,$DNI,0,0,'L');
        
          $obj->SetXY(162,32);
          $obj->Cell(35,12,"Fecha :     ".$fec,0,'C');  


          $obj->SetXY(15,53);
          $obj->Cell(60,3,"Monto Recibido ",0,0,'C');
        
          $obj->SetXY(15,57);
          $obj->MultiCell(60,3,$Recibido,0,'C');


          $obj->SetXY(75,53);
          $obj->Cell(60,3,"Monto Rendido",0,0,'C');
        
          $obj->SetXY(75,57);
          $obj->MultiCell(60,3,$Rendido,0,'C');


          $obj->SetXY(135,53);
          $obj->Cell(60,3,"Saldo",0,0,'C');         

          $obj->SetXY(135,57);
          $obj->MultiCell(60,3,$TotalRend,0,'C');


           $obj->Line(15,56,195,56);  

           $obj->Rect(15,52,180,9); 

          $obj->SetDrawColor(0,0,0); 

          //$obj->Line(15,56,195,56);


        // Fin Cabecera

        /*************************************************/

        // Inicio Body  

        $detgastoKey=DB::table('tgastosejecuciondetalle as tgd')
        ->where('tgd.cgastosejecucion','=',$cgastosejecucion)
        ->orderBy('cproyecto','ASC')
        ->orderBy('cpropuesta','ASC')
        ->orderBy('carea','ASC')
        ->orderBy('ccentrocosto','ASC')
        ->select('tgd.descripcion as desc_gast_det','tgd.cantidad as cantidad','tgd.preciounitario as preunit','tgd.reembolsable as reemb','tgd.fdocumento as fecha','tgd.numerodocumento as ndocumento','tgd.razon_cliente as proveedor','tgd.cpropuesta as propuesta','tgd.cproyecto as proyecto','tgd.cgastosejecuciondet','tgd.tipodocumento as tdocum','tgd.carea as area','tgd.ccentrocosto as costo','tgd.cproyecto','tgd.cpropuesta','tgd.carea','tgd.ccentrocosto','tgd.cpersona_empleado')
        ->distinct()
        ->get();

        $y=0;
        $yp=0;
        $titulotemp="";
        $nom="";        
        $h1=0;   
        $i=0;
        $suma=0;
        $lineaIni=0;
        $tit='';
        $inicionew=75;
        $inilinea1=65;
        $inititu=66;
        $inigteproy=67;
        $inilinea2=72.5;

        $descont=4;

        foreach($detgastoKey as $key){

            $edgastopro=DB::table('tgastosejecuciondetalle as tgd')
            ->where('tgd.cgastosejecucion','=',$cgastosejecucion);
            $titulo=''; 
            $cgerente='';
            $gerente='';
            $personal='';

            if(!is_null($key->cproyecto) && strlen($key->cproyecto)>0 ){
                $edgastopro=$edgastopro->where('cproyecto','=',$key->cproyecto);
                $tab = DB::table('tproyecto as p')->where('p.cproyecto','=',$key->cproyecto)->first();
                $titulo.="PROY: ".$tab->codigo.' / '. $tab->nombre ." ";

                $cgerente=$tab->cpersona_gerente;

                $persona=DB::table('tpersona')
                ->where('cpersona','=',$cgerente)
                ->first();
                $gerente=$persona->abreviatura;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();

                $personal=$de_person->abreviatura;                              
            }
            if(!is_null($key->cpropuesta) && strlen($key->cpropuesta)>0){
                $edgastopro=$edgastopro->where('cpropuesta','=',$key->cpropuesta);
                $tab = DB::table('tpropuesta as p')->where('p.cpropuesta','=',$key->cpropuesta)->first();
                $titulo.="PROPUESTA : ".$tab->ccodigo.'/ '.$tab->nombre ." ";
                $cgerente=$tab->cpersona_gteproyecto;

                $persona=DB::table('tpersona')
                ->where('cpersona','=',$cgerente)
                ->first();

                $gerente=$persona->abreviatura;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();

                $personal=$de_person->abreviatura;
                             
            }            
            if(!is_null($key->carea) && strlen($key->carea)>0){
                $edgastopro=$edgastopro->where('carea','=',$key->carea);
                $tab = DB::table('tareas as a')->where('a.carea','=',$key->carea)->first();
                $titulo.="AREA: " . $tab->descripcion ." ";

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();

                $personal=$de_person->abreviatura;
            }                        
            if(!is_null($key->ccentrocosto) && strlen($key->ccentrocosto)>0){
                $edgastopro=$edgastopro->where('ccentrocosto','=',$key->ccentrocosto);
                $tab = DB::table('tcentrocostos as c')->where('c.ccentrocosto','=',$key->ccentrocosto)->first();
                $titulo.="C. COSTO: " . $tab->descripcion . " ";

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();

                $personal=$de_person->abreviatura;
            }                        
            $edgastopro=$edgastopro->get();

            $h=4;               

            $empleado=$gerente;
            $codigo='0001';  
            if($titulo!=$tit){

               // $titulo='PROY: 1415.10.05 / INGENIERÃA DE DETALLE ASEGURAMIENTO DISPONIBILIDAD DE AGUA LAGUNAS, REFORZAMIENTO ESTRUCTURAL MUROS DE LAS PRESAS Y DISEÃ‘O DE ALIVIADEROS PARA LAS LAGUNAS';

               //dd($titulo,strlen($titulo),strlen($titulo)/100,ceil((strlen($titulo)/100))*$h);
   
                $hnonm=(ceil((strlen($titulo)/90))*$h); 

                $obj->SetLineWidth(.5);   
                $obj->SetDrawColor(0,0,0);
                   
                $obj->Line(15,$inilinea1+$lineaIni,195,$inilinea1+$lineaIni);   

                $obj->SetFont('Arial','',8);
                $obj->SetXY(18,$inititu+$lineaIni);      
                $obj->MultiCell(174,$h,utf8_decode(mb_strtoupper($titulo)),0,'L');

                $obj->SetXY(18,$inigteproy+$lineaIni+$hnonm);      
                $obj->MultiCell(174,$h,'Gerente Proyecto:  '.utf8_decode($empleado),0,'L');

                $obj->SetXY(80,$inigteproy+$lineaIni+$hnonm);      
                $obj->MultiCell(174,$h,'De:  '.utf8_decode($personal),0,'L');

                $obj->SetFont('Arial','B',8);
                $obj->SetDrawColor(179,179,179);

                $obj->SetLineWidth(.2); 

                $obj->Line(15,$inilinea2+$lineaIni+$hnonm,195,$inilinea2+$lineaIni+$hnonm);

                $obj->SetXY(18,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(20,3,utf8_decode('DescripciÃ³n'),0,'C');  
                $obj->Rect(18,$inicionew+$lineaIni+$hnonm,20,6); 

                $obj->SetXY(38,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(20,3,'Gasto',0,'C');  
                $obj->Rect(38,$inicionew+$lineaIni+$hnonm,20,6);    

                $obj->SetXY(58,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(9,3,'Cant',0,'L');  
                $obj->Rect(58,$inicionew+$lineaIni+$hnonm,9,6); 
 
                $obj->SetXY(67,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(10,3,'PU',0,'C');  
                $obj->Rect(67,$inicionew+$lineaIni+$hnonm,10,6); 
 
                $obj->SetXY(77,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(9,3,'Reemb.',0,'L');  
                $obj->Rect(77,$inicionew+$lineaIni+$hnonm,9,6); 
 
                $obj->SetXY(86,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(16,3,'Fecha',0,'C');  
                $obj->Rect(86,$inicionew+$lineaIni+$hnonm,16,6); 

                $obj->SetXY(102,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(10,3,'Moneda',0,'C');  
                $obj->Rect(102,$inicionew+$lineaIni+$hnonm,10,6); 

                $obj->SetXY(112,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(18,3,utf8_decode('NÂ° Documento'),0,'C');  
                $obj->Rect(112,$inicionew+$lineaIni+$hnonm,18,6); 

                $obj->SetXY(130,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(20,3,'Proveedor',0,'C');  
                $obj->Rect(130,$inicionew+$lineaIni+$hnonm,20,6);      

                $obj->SetXY(150,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(12,3,'Neto',0,'C');  
                $obj->Rect(150,$inicionew+$lineaIni+$hnonm,12,6);

                $obj->SetXY(162,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(8,3,'Otro Imp.',0,'C');  
                $obj->Rect(162,$inicionew+$lineaIni+$hnonm,8,6);

                $obj->SetXY(170,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(12,3,'IGV (18%)',0,'C');  
                $obj->Rect(170,$inicionew+$lineaIni+$hnonm,12,6);

                $obj->SetXY(182,$inicionew+$lineaIni+$hnonm);                       
                $obj->MultiCell(10,3,'Mont Total',0,'C');  
                $obj->Rect(182,$inicionew+$lineaIni+$hnonm,10,6);     

                $i++; 


                /*if($i<=1){

                    
                    $yp=$yp+6;  

                 // $i=1;
                   
                }   
                if($i==2){

                    
                   $yp=$yp+14+$hnonm;  

                 // $i=1;
                   
                }   
                if($i>2){

                ///$yp=$yp+14+$hnonm;    
                $yp=$yp+14+$hnonm-$descont;  

                 $descont=$descont+$descont;
                   
                }    */  
               

                if($i>1){

                    
                   $yp=$yp+14+$hnonm;  

                   //$i=1;
                   
                }       
                else{
                    $yp=6;                     
                    
                } 

                $y=$y+$h1;

             
                foreach ($edgastopro as $egp) {

                    $descripcion=$egp->descripcion;
                    $tgasto=DB::table('tconceptogastos as g')
                    ->where('cconcepto','=',$egp->cconcepto)
                    ->select('descripcion')
                    ->first();

                    $gasto='';
                    if($tgasto){
                        $gasto=$tgasto->descripcion;
                    }
                    
                    
                    $cant=$egp->cantidad;
                    $pu=$egp->preciounitario;
                    if ($egp->reembolsable=='00001'){
                        $reemb='Si';
                    }
                    else{
                        $reemb='No';
                    }

                   // $reemb=$egp->reembolsable;
                    $fecha=substr($egp->fdocumento,0,10);

                   /* $tmoneda=DB::table('tmonedas')
                    ->where('cmoneda','=',$egp->cmoneda)
                    ->select('descripcion')
                    ->first();
                    $moneda=$tmoneda->descripcion;*/
                    $moneda=$egp->cmoneda;

                    $NDoc=$egp->numerodocumento;
                    $prov=$egp->razon_cliente;
                    $neto=$cant*$pu;
                    $otroimp=$egp->otroimpuesto;

                    if($egp->tipodocumento=='00001'){
                        $igv=round($neto*0.18,2);
                    }
                    else{
                        $igv=0;
                    }

                    $mtot=$igv+$neto+$otroimp; 

                    $hdes=(ceil((strlen($descripcion)/10))*$h);
                    $hgas=(ceil((strlen($gasto)/14))*$h);
                    $hcan=(ceil((strlen($cant)/4))*$h);
                    $hpu=(ceil((strlen($pu)/5))*$h);   
                    $hreem=(ceil((strlen($reemb)/5))*$h);
                    $hfec=(ceil((strlen($fecha)/5))*$h);  
                    $hmon=(ceil((strlen($moneda)/5))*$h);
                    $hndoc=(ceil((strlen($NDoc)/10))*$h);    
                    $hprov=(ceil((strlen($prov)/10))*$h);
                    $hneto=(ceil((strlen($neto)/6))*$h);
                    $hotr=(ceil((strlen($otroimp)/6))*$h);  
                    $higv=(ceil((strlen($igv)/6))*$h);
                    $hmtot=(ceil((strlen($mtot)/6))*$h);

                    //dd($hdes,$hgas,$hcan,$hpu,$hreem,$hfec,$hmon,$hndoc,$hprov,$higv, $hmtot);

                    if($hdes>=$hgas && $hdes>=$hcan && $hdes>=$hpu && $hdes>=$hreem && $hdes>=$hfec && $hdes>=$hmon && $hdes>=$hndoc && $hdes>=$hprov && $hdes>=$hneto && $hdes>=$hotr && $hdes>=$higv){
                        $h1=$hdes;
                    }
                    elseif($hgas>=$hdes && $hgas>=$hcan && $hgas>=$hpu && $hgas>=$hreem && $hgas>=$hfec && $hgas>=$hmon && $hgas>=$hndoc && $hgas>=$hprov && $hgas>=$hneto && $hgas>=$hotr && $hgas>=$higv){
                            $h1=$hgas;
                        }
                    elseif($hcan>=$hdes && $hcan>=$hgas && $hcan>=$hpu && $hcan>=$hreem && $hcan>=$hfec && $hcan>=$hmon && $hcan>=$hndoc && $hcan>=$hprov && $hcan>=$hneto && $hcan>=$hotr && $hcan>=$higv){
                            $h1=$hcan;
                        }
                    
                    elseif($hpu>=$hgas && $hpu>=$hcan && $hpu>=$hdes && $hpu>=$hreem && $hpu>=$hfec && $hpu>=$hmon && $hpu>=$hndoc && $hpu>=$hprov && $hpu>=$hneto && $hpu>=$hotr && $hpu>=$higv){
                            $h1=$hpu;
                        }
                    
                    elseif($hreem>=$hgas && $hreem>=$hcan && $hreem>=$hpu && $hreem>=$hdes && $hreem>=$hfec && $hreem>=$hmon && $hreem>=$hndoc && $hreem>=$hprov && $hreem>=$hneto && $hreem>=$hotr && $hreem>=$higv){
                            $h1=$hreem;
                        }
                    
                    elseif($hfec>=$hgas && $hfec>=$hcan && $hfec>=$hpu && $hfec>=$hreem && $hfec>=$hdes && $hfec>=$hmon && $hfec>=$hndoc && $hfec>=$hprov && $hfec>=$hneto && $hfec>=$hotr && $hfec>=$higv){
                            $h1=$hfec;
                        }
                    
                    elseif($hmon>=$hgas && $hmon>=$hcan && $hmon>=$hpu && $hmon>=$hreem && $hmon>=$hfec && $hmon>=$hdes && $hmon>=$hndoc && $hmon>=$hprov && $hmon>=$hneto && $hmon>=$hotr && $hmon>=$higv){
                            $h1=$hmon;
                        }
                    
                    elseif($hndoc>=$hgas && $hndoc>=$hcan && $hndoc>=$hpu && $hndoc>=$hreem && $hndoc>=$hfec && $hndoc>=$hmon && $hndoc>=$hdes && $hndoc>=$hprov && $hndoc>=$hneto && $hndoc>=$hotr && $hndoc>=$higv){
                            $h1=$hndoc;
                        }
                    
                    elseif($hprov>=$hgas && $hprov>=$hcan && $hprov>=$hpu && $hprov>=$hreem && $hprov>=$hfec && $hprov>=$hmon && $hprov>=$hndoc && $hprov>=$hdes && $hprov>=$hneto && $hprov>=$hprov && $hprov>=$higv){
                            $h1=$hprov;
                        }
                    
                    elseif($hneto>=$hgas && $hneto>=$hcan && $hneto>=$hpu && $hneto>=$hreem && $hneto>=$hfec && $hneto>=$hmon && $hneto>=$hndoc && $hneto>=$hprov && $hneto>=$hdes && $hneto>=$hotr && $hneto>=$higv){
                            $h1=$hneto;
                        }
                    
                    elseif($hotr>=$hgas && $hotr>=$hcan && $hotr>=$hpu && $hotr>=$hreem && $hotr>=$hfec && $hotr>=$hmon && $hotr>=$hndoc && $hotr>=$hprov && $hotr>=$hneto && $hotr>=$hdes && $hotr>=$higv){
                            $h1=$hotr;
                        }
                    
                    else{
                            $h1=$higv;
                        }


                    $ordenadainicial=$inicionew+$y+$yp+$hnonm;

                    if($ordenadainicial>=230){  
/*
                        $obj->SetLineWidth(3);

                    $obj->Line(15,250,195,250);  

                        $obj->SetLineWidth(2);   
                    $obj->Line(15,$inilinea1+$lineaIni,195,$inilinea1+$lineaIni);  */   

                        $obj->AddPage();

                        $inilinea1=0;
                        $inititu=0;
                        $inigteproy=0;
                        $inilinea2=$hnonm;                   
                                       
                        $inicionew=10;
                        $y=0;
                        $yp=0;            
                      
                  
                        $descont=4;                    

                        $ordenadainicial=$inicionew+$y+$yp+$hnonm;

                        //$yp=6;

                    }

                    
                    $obj->SetFont('Arial','',7);
                    $obj->SetDrawColor(179,179,179); 

                    $obj->SetXY(18,$ordenadainicial);                       
                    $obj->MultiCell(20,3,utf8_decode($descripcion),0,'L');  
                    $obj->Rect(18,$ordenadainicial,20,$h1);            

                    $obj->SetXY(38,$ordenadainicial);                       
                    $obj->MultiCell(20,3,utf8_decode($gasto),0,'L');  
                    $obj->Rect(38,$ordenadainicial,20,$h1);    

                    $obj->SetXY(58,$ordenadainicial);                       
                    $obj->MultiCell(8,3,$cant,0,'L');  
                    $obj->Rect(58,$ordenadainicial,9,$h1); 

                    $obj->SetXY(67,$ordenadainicial);                       
                    $obj->MultiCell(10,3,$pu,0,'L');  
                    $obj->Rect(67,$ordenadainicial,10,$h1); 

                    $obj->SetXY(77,$ordenadainicial);                       
                    $obj->MultiCell(9,3,$reemb,0,'L');  
                    $obj->Rect(77,$ordenadainicial,9,$h1); 

                    $obj->SetXY(86,$ordenadainicial);                       
                    $obj->MultiCell(16,3,$fecha,0,'L');  
                    $obj->Rect(86,$ordenadainicial,16,$h1); 

                    $obj->SetXY(102,$ordenadainicial);                       
                    $obj->MultiCell(10,3,$moneda,0,'L');  
                    $obj->Rect(102,$ordenadainicial,10,$h1); 

                    $obj->SetXY(112,$ordenadainicial);                       
                    $obj->MultiCell(18,3,$NDoc,0,'L');  
                    $obj->Rect(112,$ordenadainicial,18,$h1); 

                    $obj->SetXY(130,$ordenadainicial);                       
                    $obj->MultiCell(20,3,utf8_decode($prov),0,'L');  
                    $obj->Rect(130,$ordenadainicial,20,$h1);      

                    $obj->SetXY(150,$ordenadainicial);                       
                    $obj->MultiCell(12,3,$neto,0,'L');  
                    $obj->Rect(150,$ordenadainicial,12,$h1);

                    $obj->SetXY(162,$ordenadainicial);                       
                    $obj->MultiCell(8,3,$otroimp,0,'L');  
                    $obj->Rect(162,$ordenadainicial,8,$h1);

                    $obj->SetXY(170,$ordenadainicial);                       
                    $obj->MultiCell(12,3,$igv,0,'L');  
                    $obj->Rect(170,$ordenadainicial,12,$h1);
     
                    $obj->SetXY(182,$ordenadainicial);                       
                    $obj->MultiCell(10,3,$mtot,0,'L');  
                    $obj->Rect(182,$ordenadainicial,10,$h1);                          
                    $suma=$suma+$mtot;    


                    $y=$y+$h1;  
                    $lineaIni=$y+$yp+$hnonm+$h1+$h;     
                   

                }  
                
                   $obj->Ln(20);

            }

            $tit=$titulo;
           
        }


        $obj->Output('D','Rendicion -'.$NumRendicion.'.pdf');
            exit;         
        

        // Fin Body      


       
    }

    public function printGastoProy1(Request $request){  

        $fPdf = new GastosPdf('P','mm','A4');
        $fPdf->AddPage();

    $logo = storage_path('archivos')."/images/logorpt.jpg";  //Obtener el logo      
    $fPdf->Image($logo,15,10,50,10);
    // $fPdf->Ln(20);// Salto de lÃ­nea
    $color = storage_path('archivos')."/images/colorAnddes.png"; //obtener el color de anddes en imagen
    $fPdf->Image($color,15,20.5,50,8.5);
    $fPdf->SetXY(15,20.5);
    $fPdf->SetTextColor(255,255,255); 
    $fPdf->Cell(50,8.5,"SIG AND",1,0,'C');
    $fPdf->Cell(60);
    $fPdf->SetFont('Arial','B',12);// Arial Negrita TamaÃ±o
    $fPdf->MultiCell(60,5,"\n".utf8_decode("AdministraciÃ³n y Finanzas RendiciÃ³n de Gastos"),0,'C');
    $fPdf->Cell(60);
    $fPdf->MultiCell(60,5,"10-AND-42-FOR-0100/R0/ 06-02-2018",0,'C');
    $fPdf->Ln(20);// Salto de lÃ­nea


        $fPdf->Output('D','Rendicion.pdf');
        exit();
    }


    public function calcularTotalDolares(Request $request){

        $moneda=null;
        $fecha=null;

        $moneda=$request->input('moneda');
        $fecha=$request->input('fecha');
        $total=$request->input('total');

       $tcambiodolares= DB::table('tmonedacambio')
        ->where('cmoneda','=',$moneda)
        ->where('cmonedavalor','=','USD')
        ->where('fecha','<=',$fecha)
        ->orderBy('fecha','DESC')
        ->first();

        $cambdolares=1;
        if($tcambiodolares){
          $cambdolares=$tcambiodolares->valorcompra;
        }

        $totalRend=round($cambdolares*$total,2);

        //dd($cambdolares);
        return $totalRend;
    }

    public function calcularTotalDolaresRendicion(Request $request){
       

        $moneda=null;
        $fecha=null;

        $moneda=$request->input('moneda');
        $fecha=$request->input('fecha');
        $total=$request->input('total');
       
       $tcambiodolares= DB::table('tmonedacambio')
        ->where('cmoneda','=',$moneda)
        ->where('cmonedavalor','=','USD')
        ->where('fecha','<=',$fecha)
        ->orderBy('fecha','DESC')
        ->first();

        $cambdolares=1;
        if($tcambiodolares){
          $cambdolares=$tcambiodolares->valorcompra;
        }

        $totalRend=round($cambdolares*$total,2);

        return $totalRend;
    }

    public function calcularTotalSolesRendicion(Request $request){
       

        $moneda=null;
        $fecha=null;

        $moneda=$request->input('moneda');
        $fecha=$request->input('fecha');
        $total=$request->input('total');
       
       $tcambiodolares= DB::table('tmonedacambio')
        ->where('cmoneda','=',$moneda)
        ->where('cmonedavalor','=','S/.')
        ->where('fecha','<=',$fecha)
        ->orderBy('fecha','DESC')
        ->first();

        $cambdolares=1;
        if($tcambiodolares){
          $cambdolares=$tcambiodolares->valorcompra;
        }

        $totalRend=round($cambdolares*$total,2);

        return $totalRend;
    }

    public function deleteDetGasto($Cgastosejecuciondet){
        DB::beginTransaction();

        $peje=Tgastosejecuciondetalle::where('cgastosejecuciondet','=',$Cgastosejecuciondet)->first();  
        $crendicion=$peje->cgastosejecucion;    

        try
        {
             $peje->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }

        DB::commit();

       return Redirect::route('editargastoProyecto',$crendicion);


    
    }
    public function dataGastoDetProyecto($idGastoProy){

        //dd($idProyecto);

        
        $tipoactividad = DB::table('tconceptogastos')
        ->whereNull('cconcepto_parent')
        ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
        ->get();

        $detgastoKey=DB::table('tgastosejecuciondetalle as tgd')
        ->where('tgd.cgastosejecucion','=',$idGastoProy)
        ->orderBy('cproyecto','ASC')
        ->orderBy('cpropuesta','ASC')
        ->orderBy('carea','ASC')
        ->orderBy('ccentrocosto','ASC')
        ->select('tgd.cproyecto','tgd.cpropuesta','tgd.carea','tgd.ccentrocosto','tgd.cpersona_empleado')
        ->distinct()
        ->get();
        $gastosKey=array();
        $idTabla=0;
        //dd($detgastoKey);
        $total=0;
        $totaldolar=0;
        $subtotal=0;
        $totalImpuestos=0;           
        foreach($detgastoKey as $key){
            $gKey['cgastoejecucion']=$idGastoProy;
            $gKey['cproyecto']='';
            $gKey['cproyecto_des']='';
            $gKey['cservicioproy']='';
            $gKey['cpropuesta']='';
            $gKey['cpropuesta_des']='';
            $gKey['cservicioprop']='';
            $gKey['carea']='';
            $gKey['carea_des']='';
            $gKey['ccentrocosto']='';
            $gKey['ccentrocosto_des']='';
            $idTabla++;
            $gKey['idTabla']=$idTabla;
            $gKey['numregistro'] = 0;
            $edgastopro=DB::table('tgastosejecuciondetalle as tgd')
            ->where('tgd.cgastosejecucion','=',$idGastoProy)
            ->orderBy('fdocumento','ASC');
            $titulo='';
            $de_personal='';

            $cgerente=null;
            $gerente=null;
          
            if(!is_null($key->cproyecto) && strlen($key->cproyecto)>0 ){
                $edgastopro=$edgastopro->where('cproyecto','=',$key->cproyecto)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['cproyecto']=$key->cproyecto;
                $tab = DB::table('tproyecto as p')->where('p.cproyecto','=',$key->cproyecto)->first();
                $gKey['cproyecto_des']=$tab->nombre;

                $cgerente=$tab->cpersona_gerente;

                $persona=DB::table('tpersona')
                ->where('cpersona','=',$cgerente)
                ->first();
                $gerente=$persona->abreviatura;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="PROY: " .$tab->codigo.'  '. $tab->nombre;
                if(!is_null($tab->cservicio)){
                    $gKey['cservicioproy']=$tab->cservicio." ";
                }
            }
            if(!is_null($key->cpropuesta) && strlen($key->cpropuesta)>0){
                $edgastopro=$edgastopro->where('cpropuesta','=',$key->cpropuesta)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['cpropuesta']=$key->cpropuesta;
                $tab = DB::table('tpropuesta as p')->where('p.cpropuesta','=',$key->cpropuesta)->first();
                $gKey['cpropuesta_des']=$tab->nombre;

                $cgerente=$tab->cpersona_gteproyecto;

                $persona=DB::table('tpersona')
                ->where('cpersona','=',$cgerente)
                ->first();
                $gerente=$persona->abreviatura;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="PROPUESTA : " .$tab->ccodigo.'  '.  $tab->nombre." ";
                if(!is_null($tab->cservicio)){
                    $gKey['cservicioprop']=$tab->cservicio;
                }                
            }            
            if(!is_null($key->carea) && strlen($key->carea)>0){
                $edgastopro=$edgastopro->where('carea','=',$key->carea)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['carea']=$key->carea;
                $tab = DB::table('tareas as a')->where('a.carea','=',$key->carea)->first();
                $gKey['carea_des']=$tab->descripcion;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="AREA: " . $tab->descripcion." ";
            }                        
            if(!is_null($key->ccentrocosto) && strlen($key->ccentrocosto)>0){
                $edgastopro=$edgastopro->where('ccentrocosto','=',$key->ccentrocosto)
                ->where('cpersona_empleado','=',$key->cpersona_empleado);
                $gKey['ccentrocosto']=$key->ccentrocosto;
                $tab = DB::table('tcentrocostos as c')->where('c.ccentrocosto','=',$key->ccentrocosto)->first();
                $gKey['ccentrocosto_des']=$tab->descripcion;

                $de_person=DB::table('tpersona')
                ->where('cpersona','=',$key->cpersona_empleado)
                ->first();
                $de_personal=$de_person->abreviatura;

                $titulo.="C. COSTO: " . $tab->descripcion." ";
            }                        
            $edgastopro=$edgastopro->get();
            
            $gKey['titulo']=$titulo." / "."De: ".$de_personal;
            $gKey['gerente']=$gerente;
            $num=0;
            $gastosProyDet=array();
         
            foreach($edgastopro as $egp){

                $option_select = "";
                $option_select_tipo = "";
                $option_select="<option value=''></option>";
                foreach($tipoactividad as $act){
                    $option_select.="<option value='".$act->cconcepto."'".($egp->cconcepto_parent==$act->cconcepto?'selected':'').">";
                    $option_select.=$act->descripcion."</option>";
                }
                $tgas=DB::table('tconceptogastos')
                ->whereNotNull('cconcepto_parent')
                ->where('cconcepto_parent','=',$egp->cconcepto_parent)
                ->select('cconcepto','descripcion','cconcepto_parent','tipogasto')
                ->get();
                $option_select_tipo="<option value=''></option>";
                foreach($tgas as $g){
                    $option_select_tipo.="<option value='".$g->cconcepto."'".($egp->cconcepto==$g->cconcepto?'selected':'').">";
                    $option_select_tipo.=$g->descripcion."</option>";
                }          
                      
                $num++;

                $gap['cgastosejecuciondet']= $egp->cgastosejecuciondet;        
                $gap['tipodocumento']= $egp->tipodocumento;
                $gap['ccentrocosto']= $egp->ccentrocosto;
                $gap['tipoactividad']= $egp->tipoactividad;

                $tipoacti = DB::table('tconceptogastos')
                ->where('cconcepto','=',$egp->cconcepto)
                ->first();        
                $descctividad=null;
                if($tipoacti){
                   $descctividad=  $tipoacti->descripcion;  
                }       

                $gap['descctividad']= $descctividad;


                $gap['cconcepto_parent']=$egp->cconcepto_parent;
                $gap['tipoactividad_option']= $option_select;
                $gap['descripcion']= $egp->descripcion;
                $gap['cproyecto']= $egp->cproyecto;
                $gap['cpropuesta']= $egp->cpropuesta;
                $gap['carea'] = $egp->carea;   
                $gap['tipogasto']= $egp->tipogasto;
                $gap['cconcepto']=$egp->cconcepto;
                $gap['tipogasto_option']= $option_select_tipo;
                $gap['descripcion']= $egp->descripcion;

                if($egp->reembolsable=='00001'){
                    $gap['reembolsable']= 'Si';
                }
                else{
                    $gap['reembolsable']= 'No';
                }
                
                // $guionfin = substr($egp->numerodocumento, -1);
                // $guionini = $egp->numerodocumento[0];
                // $guionmedio = str_replace('--','-',$egp->numerodocumento);
                // // dd($guionmedio);
                
                // if ($guionfin=='-') {
                //     $gap['numerodocumento'] = substr($egp->numerodocumento,0, -1);
                // }
                // elseif ($guionini=='-') {
                //     $gap['numerodocumento'] = substr($egp->numerodocumento,1, -1);
                // }
                // elseif ($guionmedio) {
                //     $gap['numerodocumento'] = $guionmedio;
                // }
                // else{
                //     $gap['numerodocumento'] = $egp->numerodocumento;     
                // }
                $gap['numerodocumento'] = $egp->numerodocumento;     
                $gap['cpersona_cliente']= $egp->cpersona_cliente;
                $gap['ruc_cliente']= $egp->ruc_cliente;
                $gap['razon_cliente']= $egp->razon_cliente;
                $gap['valorcambio']= $egp->valorcambio;
                $gap['cmoneda']= $egp->cmoneda;

                $tmoneda= DB::table('tmonedas')
                ->where('cmoneda','=',$egp->cmoneda)
                ->first();

                $gap['moneda']= $tmoneda->descripcion;

                $gap['cantidad'] = $egp->cantidad;     
                $gap['preciounitario']= $egp->preciounitario;
                $gap['subtotal']= $gap['cantidad']*$gap['preciounitario'];
                if($gap['tipodocumento']=='00001' || $gap['tipodocumento']=='00007'){
                    //Factura
                    $gap['impuesto']= round($gap['cantidad']*$gap['preciounitario']*0.18,2);
                }else{
                    $gap['impuesto']= 0;
                }
                $gap['otrimp']= $egp->otroimpuesto; 

                $gap['totaldeta']=$egp->total;

                $tcambiosoles= DB::table('tmonedacambio')
                ->where('cmoneda','=',$egp->cmoneda)
                ->where('cmonedavalor','=','S/.')
                ->where('fecha','<=',$egp->fdocumento)
                ->orderBy('fecha','DESC')
                ->first();

                $totsoles=1;
                if($tcambiosoles){
                  $totsoles=$tcambiosoles->valorcompra;
                }

                $gap['total']= $totsoles*$egp->total;

                $tcambiodolares= DB::table('tmonedacambio')
                ->where('cmoneda','=',$egp->cmoneda)
                ->where('cmonedavalor','=','USD')
                ->where('fecha','<=',$egp->fdocumento)
                ->orderBy('fecha','DESC')
                ->first();

                $totdolar=1;
                if($tcambiodolares){
                  $totdolar=$tcambiodolares->valorcompra;
                }
                
                $gap['totaldolares']= $totdolar*$egp->total;
                $gap['cpersona_empleado']= $egp->cpersona_empleado;
                $gap['ccondicionoperativa']= $egp->ccondicionoperativa;
                $gap['fdocumento']= substr($egp->fdocumento,0,10);     
                $gastosProyDet[count($gastosProyDet)]=$gap;
                $total+=round($gap['total'],2);
                $totaldolar+=round($gap['totaldolares'],2);
                $subtotal+=$gap['subtotal'];
                $totalImpuestos+=$gap['impuesto'];
            }
            $gKey['numregistro'] = $num;
            $gKey['tabla']=$gastosProyDet; //
            $gastosKey[count($gastosKey)]=$gKey; //agrega la tabla a esquema general

        }

       return $gastosKey;

     
        //return view('gastos/printRendicionGasto',compact('numrendicion','user','descripcions','frendicion','valorcambio','registrador','gastosKey','viatico','identificacion','total','totaldolar'));
        
        
    }

    public function dataRendicion($idGastoProy){

        $gasto=DB::table('tgastosejecucion as tg')
        ->where('tg.cgastosejecucion','=',$idGastoProy)
        ->first();

        $tmonedacambio = DB::table('tmonedacambio')
        ->where('cmoneda','=','USD')
        ->where('cmonedavalor','=','S/.')
        ->where(DB::raw("to_char(fecha,'YYYYMMDD')"),'<=',date('Ymd'))
        ->orderBy('fecha','DESC')
        ->first();

        $total=floatval($gasto->total);
        $totaldolares=floatval($gasto->totaldolares);

        $numrendicion=$gasto->numerorendicion;
        $descripcions=$gasto->descripcion;
        $frendicion=substr($gasto->frendicion,0,10);
        $viatico=floatval($gasto->viatico);

        if(!is_null($gasto->valorcambio)){
            $valorcambio=$gasto->valorcambio;
        }else{
            $valorcambio=$tmonedacambio->valorcompra;
        }

        $emp=DB::table('tpersona')
        ->where('cpersona','=',$gasto->per_registrador)
        ->first();
        $registrador=$emp->abreviatura;
        $identificacion=$emp->identificacion;

        // var_dump($total,$viatico);

        return array($numrendicion,$descripcions,$frendicion,$viatico,$total,$totaldolares,$registrador,$identificacion);
       
    }


    public function printPDF(Request $request,$idGastoProy){


        $gastosKey = $this->dataGastoDetProyecto($idGastoProy);
        $rendicion = $this->dataRendicion($idGastoProy);
        $newDataRend=array();

        foreach ($rendicion as $key =>$value) {
            $ng['key']=$key;
            $ng['valor']=$value;
            $newDataRend[count($newDataRend)]=$ng;
        }

        $numrendicion=$newDataRend[0]['valor'];
        $descripcions=$newDataRend[1]['valor'];
        $frendicion=$newDataRend[2]['valor'];
        $viatico=floatval($newDataRend[3]['valor']);
        $total=floatval($newDataRend[4]['valor']);
        $totaldolar=floatval($newDataRend[5]['valor']);
        $registrador=$newDataRend[6]['valor'];
        $identificacion=$newDataRend[7]['valor'];


        $saldo=$viatico - $total;
        // dd(var_dump($saldo));

        $view =  \View::make('gastos/printRendicionGasto', compact('numrendicion','descripcions','frendicion','registrador','gastosKey','viatico','identificacion','total','totaldolar','saldo'))->render();
        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','D');

      
    
        return $pdf->stream($numrendicion.'.pdf');
    }

}
