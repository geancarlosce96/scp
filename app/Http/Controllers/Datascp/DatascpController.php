<?php

namespace App\Http\Controllers\Datascp;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Ttiposnofacturable;
use App\Erp\HelperSIGSupport;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Tpropuestum;
use App\Models\Tpropuestapresentacion;
use App\Models\Tproyecto;
use App\Models\Tproyectoestructuraparticipante;
use App\Models\Tservicio;
use App\Models\Testadopropuestum;
use App\Models\Testadoproyecto;
use App\Models\Tproyectoactividade;
use App\Models\Tproyectoedt;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesfecha;
use App\Models\Tproyectodocumento;
use App\Models\Tentregable;
use App\Models\Ttiposentregable;
use App\Models\Troldespliegue;
use App\Models\Tproyectocronogramadetalle;
use App\Models\Tproyectocronograma;
use App\Models\Tproyectoejecucion;
use App\Models\Tproyectoejecucioncab;
use App\Models\Tdisciplina;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection as Collection;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Redirect;
use Carbon\Carbon;
use App\Erp\PropuestaSupport;
use App\Erp\ActividadSupport;
use App\Erp\FlowSupport; 
use App\Erp\EmpleadoSupport;
use Illuminate\Support\Facades\Input;
use DateTime;

class DatascpController extends Controller
{

    public function listadodataScp(){
        $esGP=0;

        $filtroestado=DB::table('tcatalogo')
        ->where('codtab','=','00057')
        ->whereNotIn('digide',['2','10'])
        ->wherenotNull('digide')
        ->orderBy('digide','asc')
        ->lists('descripcion','digide');
        $filtroestado = [''=>''] + $filtroestado;
        
        $columna_aprob = true;

        return view('datascp/datascp',compact('filtroestado','esGP','columna_aprob'));

    }

    public function listarHT(Request $request){

        $esGP=0;

        $columna_aprob = true;

        $filtroestado=DB::table('tcatalogo')
        ->where('codtab','=','00057')
        ->whereNotIn('digide',['2','10'])
        ->wherenotNull('digide')
        ->orderBy('digide','asc')
        ->lists('descripcion','digide');
        $filtroestado = [''=>''] + $filtroestado;

        $fechaact=Carbon::now();
        $anioact=substr($fechaact,0,4);
        $semanaact=$fechaact->weekOfYear;

        $anio=$request->input("anio");
 
    
        $listAprobar=array();       

        $tpersona=DB::table('tpersona as per')        
        ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
        ->join('tcargos as tc','dat.ccargo','=','tc.ccargo')
        ->join('tusuarios as tu','per.cpersona','=','tu.cpersona')
        ->join('tareas as ta','dat.carea','=','ta.carea')
        ->where('tu.estado','=','ACT')
        ->whereNotNull('tc.cargoparentdespliegue')
        ->orderBy('per.cpersona')
        ->get();   

        $x=1;

        $fingreso=null;

        foreach ($tpersona as $ap) {   

            $aprobador_area = DB::select('select public."fn_obtener_nombres_aprobadores"('.$ap->cargoparentdespliegue.')')[0]->fn_obtener_nombres_aprobadores;
            $aprobador_area = explode('/',$aprobador_area);
            $aprobador_area = explode('-',$aprobador_area[0]);
            $aprobador =$aprobador_area[0]?trim($aprobador_area[0]):'';
            $areaapr = $aprobador_area[0]?trim($aprobador_area[1]):'';

            $fingreso=$ap->fingreso;

            $diaing = substr($fingreso,8,2);
            $mesing = substr($fingreso,5,2);
            $anioing = substr($fingreso,0,4);
            $semanaing = date('W',  mktime(0,0,0,$mesing,$diaing,$anioing)); 

            $semanainicio="";
            $semanafin="";

            $ultimaSemanaDelAnio = new Carbon($anio.'-12-31');
            $ultimaSemanaDelAnio =$ultimaSemanaDelAnio->startOfWeek();

                if ($ultimaSemanaDelAnio->weekOfYear=='1') {
                        $ultimaSemanaDelAnio->addDays(-1);

                }
            $ultimaSemanaDelAnio=$ultimaSemanaDelAnio->weekOfYear;

            if($anio==$anioing && $anio==$anioact){
                $semanainicio= $semanaing; 
                $semanafin=$semanaact-1;                
            }    

            if($anio > $anioing && $anio < $anioact){
                $semanainicio= 1; 
                $semanafin=$ultimaSemanaDelAnio;                  
            } 

            if($anio > $anioing && $anio == $anioact){
                $semanainicio= 1; 
                $semanafin=$semanaact-1;                  
            } 

            if($anio == $anioing && $anio < $anioact){
                $semanainicio= $semanaing; 
                $semanafin=$ultimaSemanaDelAnio;                 
            }  

            if($anio < $anioing && $anio < $anioact){
                $semanainicio= $semanaing; 
                $semanafin=$ultimaSemanaDelAnio;                 
            }  


            for ($i=$semanainicio; $i <= $semanafin ; $i++) { 

                if($anio >= $anioing){

                /************************** INICIO HT PENDIENTES  **************************/
                    if($request->input("estadohoras")==6){

                        /*
                        $pendientes=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.cpersona','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['1','2','3','4','5'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();
                        */

                        //dd($aprobador_area,$aprobador,$areaapr);

                        $pendientes = DB::select('select public."fn_htvacias"('.$ap->cpersona.','.$i.','.$anio.')');
                        //$pendientes = DB::select('select public."fn_htvacias"('.$ap->cpersona.','.$i.','.'2020'.')');
                        //dd($pendientes[0]->fn_htvacias); 

                        if($pendientes[0]->fn_htvacias == 0){
                        // if(!$pendientes || is_null($pendientes)){

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='0';
                            $apro['etapa']='VACIO'; 
                            $apro['total']='0';
                            $apro['descripcion']=$ap->descripcion;   
                            $apro['estado']='Pendiente';                     
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            // $apro['aprobador']= DB::select('select public."fn_obtener_nombres_aprobadores_prueba"('.$ap->cargoparentdespliegue.',\'nombres\')')[0]->fn_obtener_nombres_aprobadores_prueba;
                            // $apro['areaapr']= DB::select('select public."fn_obtener_nombres_aprobadores_prueba"('.$ap->cargoparentdespliegue.',\'area\')')[0]->fn_obtener_nombres_aprobadores_prueba;
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;
                            $listAprobar[count($listAprobar)]=$apro;


                            $x++;
                        }
                    }
                /************************** FIN HT PENDIENTES  **************************/

                /************************** INICIO PLANIFICADAS  **************************/
                    if($request->input("estadohoras")==1){

                        $planificadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['1'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->whereNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($planificadas){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='1';
                            $apro['etapa']='PLA';                               
                            $apro['estado']='Planificadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;

                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['1'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasplanificadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['1'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasplanificadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['1'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasplanificadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }
                /************************** FIN PLANIFICADAS **************************/

                /************************** INICIO EJECUTADAS  **************************/

                    if($request->input("estadohoras")==9){

                        $Observadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['2'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','EJE')
                        ->whereNotNull('eje.horasejecutadas')
                        ->whereNull('eje.horasobservadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($Observadas){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='2';
                            $apro['etapa']='EJE';                               
                            $apro['estado']='No Enviadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;

                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['2'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['2'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['2'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }

                /************************** FIN EJECUTADAS **************************/

                /************************** INICIO ENVIADAS  **************************/

                    if($request->input("estadohoras")==3){

                        $enviadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['3'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','VJI')
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($enviadas){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='3';
                            $apro['etapa']='VJI';                               
                            $apro['estado']='Enviadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;

                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['3'])
                            ->where('eje.ccondicionoperativa','=','VJI')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','VJI')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','VJI')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }

                /************************** FIN ENVIADAS **************************/

                /************************** INICIO APROBADO POR  **************************/

                    if($request->input("estadohoras")==4){

                        $aprobadasJI=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['3'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','RGP')
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($aprobadasJI){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='3';
                            $apro['etapa']='RGP';                               
                            $apro['estado']='Aprobadas por JI';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;

                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['3'])
                            ->where('eje.ccondicionoperativa','=','RGP')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','RGP')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','RGP')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }


                /************************** FIN ENVIADAS **************************/

                /************************** INICIO APROBADO POR  **************************/

                    if($request->input("estadohoras")==5){

                        $aprobadasJI=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['5'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','APR')
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($aprobadasJI){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='5';
                            $apro['etapa']='APR';                               
                            $apro['estado']='HT Aprobadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;

                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['5'])
                            ->where('eje.ccondicionoperativa','=','APR')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['5'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','APR')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['5'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','APR')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }
 
                    /************************** FIN HT APROBADAS **************************/  

                    /************************** INICIO OBSERVADAS JI  **************************/

                    if($request->input("estadohoras")==7){

                       $horas= DB::table('tproyectoejecucion as eje')
                        ->whereIn('eje.estado',['2'])
                        ->where('cpersona_ejecuta','=',$ap->cpersona)
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->whereNotNull('horasejecutadas')  
                        ->whereNotNull('horasobservadas')
                        ->get();

                        $aobs=array();

                        foreach ($horas as $h) {

                            $flujoaprob=DB::table('tflujoaprobacion as tf')
                            ->where('tf.cproyectoejecucion','=',$h->cproyectoejecucion)
                            ->orderBy('tf.fregistro','desc')
                            ->limit(1)
                            ->first();                                 

                            if($flujoaprob){

                                if($flujoaprob->ccondicionoperativa_aprobado == 'VJI'){
                  
                                    $aobs[count($aobs)] = $h->cproyectoejecucion;                                           
                                }                       

                            }  


                        }                            


                        $Observadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')                         
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura','eje.cproyectoejecucion')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa,eje.cproyectoejecucion'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['2'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','EJE')
                        ->whereNotNull('eje.horasejecutadas')
                        ->whereNotNull('eje.horasobservadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)                        
                        ->whereIN('eje.cproyectoejecucion',$aobs)           
                        ->get();     



                        if($Observadas){                                

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='2';
                            $apro['etapa']='EJE';                               
                            $apro['estado']='Observadas por JI';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;



                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')    
                            ->whereIn('eje.cproyectoejecucion',$aobs) 
                            //->get();                
                            ->sum('horasobservadas');



                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)  
                            ->whereIn('eje.cproyectoejecucion',$aobs)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')
                           // ->get();                
                            ->sum('horasobservadas');
                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->whereNotNull('horasobservadas')
                            ->whereIn('eje.cproyectoejecucion',$aobs)  
                            //->get();                
                            ->sum('horasobservadas');                     
                            //dd($aobs,$flujoaprob,$nroFact,$nroNFact,$nroAdm,$ap->cpersona);
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }

                /************************** FIN OBSERVADAS JI **************************/


                /************************** INICIO OBSERVADAS GP  **************************/

                    if($request->input("estadohoras")==8){


                        $horas= DB::table('tproyectoejecucion as eje')
                        ->whereIn('eje.estado',['2'])
                        ->where('cpersona_ejecuta','=',$ap->cpersona)
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->whereNotNull('horasejecutadas')  
                        ->whereNotNull('horasobservadas')
                        ->get();

                        $aobs=array();

                        foreach ($horas as $h) {

                            $flujoaprob=DB::table('tflujoaprobacion as tf')
                            ->where('tf.cproyectoejecucion','=',$h->cproyectoejecucion)
                            ->orderBy('tf.fregistro','desc')
                            ->limit(1)
                            ->first();                                 

                            if($flujoaprob){

                                if($flujoaprob->ccondicionoperativa_aprobado == 'RGP'){
                  
                                    $aobs[count($aobs)] = $h->cproyectoejecucion;                                           
                                }                       

                            }  


                        }                            


                        $Observadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')                         
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura','eje.cproyectoejecucion')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa,eje.cproyectoejecucion'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['2'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','EJE')
                        ->whereNotNull('eje.horasejecutadas')
                        ->whereNotNull('eje.horasobservadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)                        
                        ->whereIN('eje.cproyectoejecucion',$aobs)           
                        ->get();     



                        if($Observadas){                                

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='2';
                            $apro['etapa']='EJE';                               
                            $apro['estado']='Observadas por GP';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));
                            $apro['aprobador'] = $aprobador;
                            $apro['areaapr'] = $areaapr;


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;



                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')    
                            ->whereIn('eje.cproyectoejecucion',$aobs) 
                            //->get();                
                            ->sum('horasobservadas');



                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)  
                            ->whereIn('eje.cproyectoejecucion',$aobs)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')
                           // ->get();                
                            ->sum('horasobservadas');
                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->whereNotNull('horasobservadas')
                            ->whereIn('eje.cproyectoejecucion',$aobs)  
                            //->get();                
                            ->sum('horasobservadas');                     
                            //dd($aobs,$flujoaprob,$nroFact,$nroNFact,$nroAdm,$ap->cpersona);
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }
                /************************** FIN OBSERVADAS JGP **************************/
          

                } 
            }
        }  


        
        return view('datascp/datascp',compact('listAprobar','anio','filtroestado','esGP','columna_aprob'));
    }

    public function listadodataScpJI(){
        $esGP=0;

        $filtroestadoji=DB::table('tcatalogo')
        ->where('codtab','=','00057')
        ->whereNotIn('digide',['6','9','10'])
        ->wherenotNull('digide')
        ->orderBy('digide','asc')
        ->lists('descripcion','digide');
        $filtroestadoji = [''=>''] + $filtroestadoji;

        $columna_aprob = false;

        return view('datascp/datascpji',compact('filtroestadoji','esGP','columna_aprob'));
    }

    public function listarHTJI(Request $request){
        $esGP=0;
        $user = Auth::user();

        $columna_aprob = false;

        $filtroestadoji=DB::table('tcatalogo')
        ->where('codtab','=','00057')
        ->whereNotIn('digide',['6','9','10'])
        ->wherenotNull('digide')
        ->orderBy('digide','asc')
        ->lists('descripcion','digide');
        $filtroestadoji = [''=>''] + $filtroestadoji;

        $fechaact=Carbon::now();
        $anioact=substr($fechaact,0,4);
        $semanaact=$fechaact->weekOfYear;

        $anio=$request->input("anio");
 
    
        $listAprobar=array();

        $tpersonaji=DB::table('tpersona as p')        
        ->join('tpersonadatosempleado as pe','p.cpersona','=','pe.cpersona')
        ->join('tcargos as c','pe.ccargo','=','c.ccargo')
        ->join('tusuarios as u','p.cpersona','=','u.cpersona')
        ->join('tareas as a','pe.carea','=','a.carea')
        // ->select('c.ccargo')
        ->where('u.estado','=','ACT')
        ->where('p.cpersona','=',$user->cpersona)
        // ->whereNotNull('c.cargoparentdespliegue')
        ->orderBy('p.cpersona')
        // ->get();
        ->lists('c.ccargo');
        // $tpersonaji = [''=>'']+ $tpersonaji;
        // dd($tpersonaji);

        $tpersona=DB::table('tpersona as per')        
        ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
        ->join('tcargos as tc','dat.ccargo','=','tc.ccargo')
        ->join('tusuarios as tu','per.cpersona','=','tu.cpersona')
        ->join('tareas as ta','dat.carea','=','ta.carea')
        ->where('tc.cargoparentdespliegue','=',$tpersonaji)
        ->where('tu.estado','=','ACT')
        ->whereNotNull('tc.cargoparentdespliegue')
        ->orderBy('per.cpersona')
        ->get();

        $x=1;

        $fingreso=null;

        foreach ($tpersona as $ap) {   

            $fingreso=$ap->fingreso;

            $diaing = substr($fingreso,8,2);
            $mesing = substr($fingreso,5,2);
            $anioing = substr($fingreso,0,4);
            $semanaing = date('W',  mktime(0,0,0,$mesing,$diaing,$anioing)); 

            $semanainicio="";
            $semanafin="";

            $ultimaSemanaDelAnio = new Carbon($anio.'-12-31');
            $ultimaSemanaDelAnio =$ultimaSemanaDelAnio->startOfWeek();

                if ($ultimaSemanaDelAnio->weekOfYear=='1') {
                        $ultimaSemanaDelAnio->addDays(-1);

                }
            $ultimaSemanaDelAnio=$ultimaSemanaDelAnio->weekOfYear;

            if($anio==$anioing && $anio==$anioact){
                $semanainicio= $semanaing; 
                $semanafin=$semanaact-1;                
            }    

            if($anio > $anioing && $anio < $anioact){
                $semanainicio= 1; 
                $semanafin=$ultimaSemanaDelAnio;                  
            } 

            if($anio > $anioing && $anio == $anioact){
                $semanainicio= 1; 
                $semanafin=$semanaact-1;                  
            } 

            if($anio == $anioing && $anio < $anioact){
                $semanainicio= $semanaing; 
                $semanafin=$ultimaSemanaDelAnio;                 
            }  

            if($anio < $anioing && $anio < $anioact){
                $semanainicio= $semanaing; 
                $semanafin=$ultimaSemanaDelAnio;                 
            }  



            for ($i=$semanainicio; $i <= $semanafin ; $i++) { 

                if($anio >= $anioing){

                /************************** INICIO HT PENDIENTES  **************************/
                    // if($request->input("estadohoras")==6){

                    //     $pendientes=DB::table('tproyectoejecucioncab as cab')
                    //     ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                    //     ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                    //     ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.cpersona','per.abreviatura')
                    //     ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                    //     ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                    //     ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                    //     ->orderBy(DB::raw('per.cpersona'),'ASC')
                    //     ->whereIn('eje.estado',['1','2','3','4','5'])     
                    //     ->where('per.cpersona','=',$ap->cpersona)  
                    //     ->whereNotNull('eje.horasejecutadas')
                    //     ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                    //     ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                    //     ->get(); 


                    //     if(!$pendientes || is_null($pendientes)){

                    //         $apro['item'] = $x;
                    //         $apro['semana'] =intval($i);
                    //         $apro['anio']= $anio;
                    //         $apro['cpersona'] = $ap->cpersona;
                    //         $apro['nomper']=$ap->abreviatura;
                    //         $apro['condicion']='0';
                    //         $apro['etapa']='VACIO'; 
                    //         $apro['total']='0';
                    //         $apro['descripcion']=$ap->descripcion;   
                    //         $apro['estado']='Pendiente';                     
                    //         $listAprobar[count($listAprobar)]=$apro;
                    //         $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));

                    //         $x++;
                    //     }
                    // }
                /************************** FIN HT PENDIENTES  **************************/

                /************************** INICIO EJECUTADAS  **************************/

                    if($request->input("estadohoras")==2){

                        $Observadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['2'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','EJE')
                        ->whereNotNull('eje.horasejecutadas')
                        ->whereNull('eje.horasobservadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($Observadas){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='2';
                            $apro['etapa']='EJE';                               
                            $apro['estado']='No enviadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));



                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['2'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['2'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['2'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }

                        else {
                            $pendientes=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.cpersona','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['1','2','3','4','5'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get(); 


                        if(!$pendientes || is_null($pendientes)){

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='0';
                            $apro['etapa']='VACIO'; 
                            $apro['total']='0';
                            $apro['descripcion']=$ap->descripcion;   
                            $apro['estado']='Vacia';       
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));              
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                        
                        }

                            // dd($fechaInicioSemana);
                    }

                /************************** FIN EJECUTADAS **************************/

                /************************** INICIO PLANIFICADAS  **************************/
                    if($request->input("estadohoras")==1){

                        $planificadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['1'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->whereNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($planificadas){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='1';
                            $apro['etapa']='PLA';                               
                            $apro['estado']='Planificadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['1'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasplanificadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['1'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasplanificadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['1'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasplanificadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }
                /************************** FIN PLANIFICADAS **************************/

                

                /************************** INICIO ENVIADAS  **************************/

                    if($request->input("estadohoras")==3){

                        $enviadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['3'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','VJI')
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($enviadas){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='3';
                            $apro['etapa']='VJI';                               
                            $apro['estado']='Enviadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['3'])
                            ->where('eje.ccondicionoperativa','=','VJI')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','VJI')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','VJI')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }

                /************************** FIN ENVIADAS **************************/

                /************************** INICIO APROBADO POR  **************************/

                    if($request->input("estadohoras")==4){

                        $aprobadasJI=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['3'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','RGP')
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($aprobadasJI){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='3';
                            $apro['etapa']='RGP';                               
                            $apro['estado']='Aprobadas por JI';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['3'])
                            ->where('eje.ccondicionoperativa','=','RGP')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','RGP')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['3'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','RGP')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }


                /************************** FIN ENVIADAS **************************/

                /************************** INICIO APROBADO POR  **************************/

                    if($request->input("estadohoras")==5){

                        $aprobadasJI=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['5'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','APR')
                        ->whereNotNull('eje.horasejecutadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->get();                   


                        if($aprobadasJI){                           

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='5';
                            $apro['etapa']='APR';                               
                            $apro['estado']='HT Aprobadas';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;

                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->whereIn('eje.estado',['5'])
                            ->where('eje.ccondicionoperativa','=','APR')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')                        
                            ->sum('horasejecutadas');

                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->whereIn('eje.estado',['5'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','APR')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                             ->sum('horasejecutadas');

                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->whereIn('eje.estado',['5'])
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where('eje.ccondicionoperativa','=','APR')
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->sum('horasejecutadas');                        
                           
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }
 
                    /************************** FIN HT APROBADAS **************************/  

                    /************************** INICIO OBSERVADAS JI  **************************/

                    if($request->input("estadohoras")==7){

                       $horas= DB::table('tproyectoejecucion as eje')
                        ->whereIn('eje.estado',['2'])
                        ->where('cpersona_ejecuta','=',$ap->cpersona)
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->whereNotNull('horasejecutadas')  
                        ->whereNotNull('horasobservadas')
                        ->get();

                        $aobs=array();

                        foreach ($horas as $h) {

                            $flujoaprob=DB::table('tflujoaprobacion as tf')
                            ->where('tf.cproyectoejecucion','=',$h->cproyectoejecucion)
                            ->orderBy('tf.fregistro','desc')
                            ->limit(1)
                            ->first();                                 

                            if($flujoaprob){

                                if($flujoaprob->ccondicionoperativa_aprobado == 'VJI'){
                  
                                    $aobs[count($aobs)] = $h->cproyectoejecucion;                                           
                                }                       

                            }  


                        }                            


                        $Observadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')                         
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura','eje.cproyectoejecucion')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa,eje.cproyectoejecucion'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['2'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','EJE')
                        ->whereNotNull('eje.horasejecutadas')
                        ->whereNotNull('eje.horasobservadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)                        
                        ->whereIN('eje.cproyectoejecucion',$aobs)           
                        ->get();     



                        if($Observadas){                                

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='2';
                            $apro['etapa']='EJE';                               
                            $apro['estado']='Observadas por JI';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;



                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')    
                            ->whereIn('eje.cproyectoejecucion',$aobs) 
                            //->get();                
                            ->sum('horasobservadas');



                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)  
                            ->whereIn('eje.cproyectoejecucion',$aobs)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')
                           // ->get();                
                            ->sum('horasobservadas');
                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->whereNotNull('horasobservadas')
                            ->whereIn('eje.cproyectoejecucion',$aobs)  
                            //->get();                
                            ->sum('horasobservadas');                     
                            //dd($aobs,$flujoaprob,$nroFact,$nroNFact,$nroAdm,$ap->cpersona);
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }

                /************************** FIN OBSERVADAS JI **************************/


                /************************** INICIO OBSERVADAS GP  **************************/

                    if($request->input("estadohoras")==8){


                        $horas= DB::table('tproyectoejecucion as eje')
                        ->whereIn('eje.estado',['2'])
                        ->where('cpersona_ejecuta','=',$ap->cpersona)
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                        ->whereNotNull('horasejecutadas')  
                        ->whereNotNull('horasobservadas')
                        ->get();

                        $aobs=array();

                        foreach ($horas as $h) {

                            $flujoaprob=DB::table('tflujoaprobacion as tf')
                            ->where('tf.cproyectoejecucion','=',$h->cproyectoejecucion)
                            ->orderBy('tf.fregistro','desc')
                            ->limit(1)
                            ->first();                                 

                            if($flujoaprob){

                                if($flujoaprob->ccondicionoperativa_aprobado == 'RGP'){
                  
                                    $aobs[count($aobs)] = $h->cproyectoejecucion;                                           
                                }                       

                            }  


                        }                            


                        $Observadas=DB::table('tproyectoejecucioncab as cab')
                        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
                        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')                         
                        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.abreviatura','eje.cproyectoejecucion')
                        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa,eje.cproyectoejecucion'))
                        ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
                        ->orderBy(DB::raw('per.cpersona'),'ASC')
                        ->whereIn('eje.estado',['2'])     
                        ->where('per.cpersona','=',$ap->cpersona)  
                        ->where('eje.ccondicionoperativa','=','EJE')
                        ->whereNotNull('eje.horasejecutadas')
                        ->whereNotNull('eje.horasobservadas')
                        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)                        
                        ->whereIN('eje.cproyectoejecucion',$aobs)           
                        ->get();     



                        if($Observadas){                                

                            $apro['item'] = $x;
                            $apro['semana'] =intval($i);
                            $apro['anio']= $anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='2';
                            $apro['etapa']='EJE';                               
                            $apro['estado']='Observadas por GP';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($i) , 2, '0', STR_PAD_LEFT)));


                            $nroFact=0;
                            $nroNFact=0; // verificar los sub -totales
                            $nroAdm=0;



                            $nroFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','1')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')    
                            ->whereIn('eje.cproyectoejecucion',$aobs) 
                            //->get();                
                            ->sum('horasobservadas');



                            $nroNFact = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','2')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)  
                            ->whereIn('eje.cproyectoejecucion',$aobs)   
                            ->whereNotNull('horasejecutadas')  
                            ->whereNotNull('horasobservadas')
                           // ->get();                
                            ->sum('horasobservadas');
                            $nroAdm = DB::table('tproyectoejecucion as eje')
                            ->where('tipo','=','3')
                            ->where('eje.estado','=','2')
                            ->where('cpersona_ejecuta','=',$ap->cpersona)
                            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$i)
                            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
                            ->whereNotNull('horasejecutadas')
                            ->whereNotNull('horasobservadas')
                            ->whereIn('eje.cproyectoejecucion',$aobs)  
                            //->get();                
                            ->sum('horasobservadas');                     
                            //dd($aobs,$flujoaprob,$nroFact,$nroNFact,$nroAdm,$ap->cpersona);
                            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);                   
                            $listAprobar[count($listAprobar)]=$apro;

                            $x++;
                        }
                    }
                /************************** FIN OBSERVADAS JGP **************************/
          

                } 
            }
        }  


        
        return view('datascp/datascpji',compact('listAprobar','anio','filtroestadoji','esGP','columna_aprob'));
    }

    public function listadodataScpGP(){
        $esGP=1;

        $filtroestadoji=DB::table('tcatalogo')
        ->where('codtab','=','00057')
        ->where('digide','=','10')        
        ->wherenotNull('digide')
        ->orderBy('digide','asc')
        ->lists('descripcion','digide');
        //$filtroestadoji = [''=>''] + $filtroestadoji;
        $columna_aprob = false;
        return view('datascp/datascpgp',compact('filtroestadoji','esGP','columna_aprob'));
    }

    public function listarHTGP(Request $request){

        $esGP=1;

        $cpersonaGP=Auth::user()->cpersona;
        $columna_aprob = false;

        $tproyectopersona = DB::table('tproyecto as pp')      
        ->where('pp.cpersona_gerente','=',$cpersonaGP)
        ->where('pp.cestadoproyecto','=','001')
        ->select('pp.cproyecto')
        ->get();
        $aProyectos = array();
        foreach($tproyectopersona as $o){
            $aProyectos[count($aProyectos)]=$o->cproyecto;
        }

        $filtroestadoji=DB::table('tcatalogo')
        ->where('codtab','=','00057')
        ->where('digide','=','10')        
        ->wherenotNull('digide')
        ->orderBy('digide','asc')
        ->lists('descripcion','digide');

        $fechaact=Carbon::now();
        $anioact=substr($fechaact,0,4);
        $semanaact=$fechaact->weekOfYear;

        $anio=$request->input("anio");
 
    
        $listAprobar=array();       

        $cargoap=array();
        $tcargo=DB::table('tcargos as tca')  
        ->join('tcargos as tc','tca.cargoparentdespliegue','=','tc.ccargo')
        ->select('tc.ccargo')
        ->distinct()
        ->get(); 
        

        foreach ($tcargo as $tc) {
          $cargoap[count($cargoap)]=$tc->ccargo;       
           
        }

        $tpersona=DB::table('tpersona as per')        
        ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
        ->join('tcargos as tc','dat.ccargo','=','tc.ccargo')
        ->join('tusuarios as tu','per.cpersona','=','tu.cpersona')
        ->join('tareas as ta','dat.carea','=','ta.carea')
        ->where('tu.estado','=','ACT')
        ->whereIn('tc.ccargo',$cargoap)
        ->whereNotNull('tc.cargoparentdespliegue')
        ->orderBy('per.cpersona')
        ->get();   

        //dd($tpersona);

        $x=1;

        foreach ($tpersona as $ap) { 

            $semanainicio=1;

            $semanafin = new Carbon($anio.'-12-31');
            $semanafin =$semanafin->startOfWeek();

                if ($semanafin->weekOfYear=='1') {
                        $semanafin->addDays(-1);

                }
            $semanafin=$semanafin->weekOfYear;

            
            // $semanafin=date('W',  mktime(0,0,0,'0','31',$anio));

            for ($i=$semanainicio; $i <= $semanafin ; $i++) { 

                if($request->input("estadohoras")==10){


                    $JIpend=DB::table('tproyectoejecucioncab as cab')
                    ->leftjoin('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
                    ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
                    ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')                 
                    ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto') 
                    ->whereIn('eje.estado',['3','4'])
                    ->where('ccondicionoperativa','=','VJI')
                    ->whereIn('pry.cproyecto',$aProyectos)
                    ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$ap->cpersona.' as bigint))'),'=',DB::raw('true'))
                    ->where(DB::raw("extract(YEAR from eje.fplanificado::date)"),'=',$anio)
                    ->where(DB::raw("extract(WEEK from eje.fplanificado::date)"),'=',$i)
                    ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio,sum(eje.horasejecutadas) as total'))
                    ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado)'))
                    ->orderby('anio','desc')
                    ->orderby('semana','desc')
                    ->get();
                    //dd($JIpend,$aProyectos,$i,$semanafin);
                   
                   if($ap->cpersona=='3928' && intval($i)==14){
                    //dd($JIpend,$aProyectos,$i);
                   }
                    
                    if($JIpend){
                        foreach ($JIpend as $jip) {                              

                            $apro['item'] = $x;
                            $apro['semana'] =$jip->semana;      
                            $apro['anio']= $jip->anio;
                            $apro['cpersona'] = $ap->cpersona;
                            $apro['nomper']=$ap->abreviatura;
                            $apro['condicion']='3';
                            $apro['etapa']='VJI';                               
                            $apro['estado']='Pendientes por aprobar como JI';  
                            $apro['descripcion']=$ap->descripcion;
                            $apro['fecha']  = date('Y-m-d', strtotime($anio . 'W' . str_pad(intval($jip->semana) , 2, '0', STR_PAD_LEFT)));
                            $apro['total'] = $jip->total; 
                            $listAprobar[count($listAprobar)]=$apro;

                             //dd($JIpend,$listAprobar);
                            
                            
                            $x++;
                            
                        } 
                    }
                }  
            }        
        }

        //dd($listAprobar);
        return view('datascp/datascpgp',compact('listAprobar','anio','filtroestadoji','esGP','columna_aprob'));
    }


    
   
}