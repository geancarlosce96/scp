<?php

namespace App\Http\Controllers\Mantenimientos;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Talergia;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tservicio;
use App\Models\Tentregable;
use App\Models\Ttiposentregable;
use App\Models\Tdisciplina;
use App\Models\Tcargo;
use App\Models\Tconceptogasto;
use App\Models\Tactividad;
use App\Models\Tcontactocargo;
use App\Models\Tareasconocimiento;
use App\Models\Tpai;
use App\Models\Tsede;
use App\Models\Tarea;
use App\Models\Tinstitucion;
use App\Models\Tfasesproyecto;
use App\Models\Tcatalogo;
use App\Models\Tchecklist;
use App\Models\Toficina;
use App\Models\Ttiposnofacturable;
use App\Models\Ttiposcontacto;
use App\Models\Ttiposinformacioncontacto;
use App\Models\Tenfermedad;
use App\Models\Tespecialidad;
use App\Models\Tmonedacambio;
use Redirect;
use Auth;
use Session;
use Response;
use DB;



class MantenimientosController extends Controller
{

	//Servicios

	public function listarAlergias(){
		return Datatables::queryBuilder(DB::table('talergias')			
			->select('calergias','descripcion',DB::raw('CONCAT(\'rowrdp_\',calergias)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoAlergias(){
		return view('mantenimiento/alergias');
	}

	
	public function editarAlergias(Request $request,$idAlergia){
		$alergias=DB::table('talergias')
		->where('calergias','=',$idAlergia)
		->first();

		$descripcion=DB::table('talergias')
		->where('calergias','=',$idAlergia)
		->first();

		return view('partials/addAlergias',compact('alergias','descripcion'));
	}

	public function agregarAlergia(Request $request){
		DB::beginTransaction();
		$calergias = $request->input('calergias');


		if(strlen($calergias)<=0){
			$alergias = new Talergia();	
			
 			
		}else{
			$alergias = Talergia::where('calergias','=',$calergias)
			->first();			
		}	
		$alergias->descripcion= $request->input('descripcion');
		$alergias->save();
	
		DB::commit(); 



		return Redirect::route('alergias');  
	}

	public function eliminarAlergias($idAlergias){

		 DB::beginTransaction();
		 $calergias = Talergia::where('calergias','=',$idAlergias)
		 ->first();
       
        try
        {
        	$calergias->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('alergias');  
      
    }

	public function listarServicios(){
		return Datatables::queryBuilder(DB::table('tservicio as ts')
			->select('ts.cservicio','ts.descripcion',DB::raw('CONCAT(\'rowrdp_\',ts.cservicio)   as "DT_RowId"'))
			)->make(true);
	}
	

	public function listadoserv(){

		return view('mantenimiento/servicios');
	}

	
	public function editarServicios(Request $request,$idServicio){
		

		$servicio= DB::table('tservicio')
		->where('cservicio','=',$idServicio)	
		->first();

		$descripcion= DB::table('tservicio')
		->where('cservicio','=',$idServicio)	
		->first();

		
			  
		return view('partials/addServicio',compact('servicio','descripcion'));    // adServicio --> modal

	}

	public function agregarServicio(Request $request){
		DB::beginTransaction();		
		$cservicio = $request->input('cservicio');


		if(strlen($cservicio)<=0){
			$servicio = new Tservicio();
				

		}else{
			$servicio = Tservicio::where('cservicio','=',$cservicio)
			->first();
			
		}
	
		$servicio->descripcion= $request->input('descripcion');
		
		$servicio->save();
		


		DB::commit();
		return Redirect::route('servicios');  
	}


	public function eliminarServicios($idServicio){

		 DB::beginTransaction();
		 $servicio = Tservicio::where('cservicio','=',$idServicio)
		 ->first();

	
        try
        {
        	$servicio->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('servicios');  
      
    }

	//Fin Servicios

	//Entregables

	public function listarEntregablesMantenimiento(){
		return Datatables::queryBuilder(DB::table('tentregables as te')
			->leftJoin('tdisciplina as td','te.cdisciplina','=','td.cdisciplina')	
			->leftJoin('ttiposentregables as tte','te.ctipoentregable','=','tte.ctiposentregable')		
			->select('te.centregables','te.codigo','te.descripcion','td.descripcion as disciplina','tte.descripcion as tipos',DB::raw('CONCAT(\'rowrdp_\',te.centregables)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoEntregablesMantenimiento(){

		$disciplina = DB::table('tdisciplina')
		->lists('descripcion','cdisciplina');
		$disciplina = [''=>''] + $disciplina;	

		$tipo = DB::table('ttiposentregables')        
        ->lists('descripcion','ctiposentregable');
        $tipo = [''=>''] + $tipo;	

		return view('mantenimiento/entregables',compact('entregable','disciplina','tipo'));		
		
	}

	

	public function editarEntregablesMantenimiento(Request $request,$idEntregable){
		$entregable= DB::table('tentregables')
		->where('centregables','=',$idEntregable)
		->first();

		$codigo= DB::table('tentregables')
		->where('centregables','=',$idEntregable)
		->first();

		$descripcion= DB::table('tentregables')
		->where('centregables','=',$idEntregable)
		->first();		

		$tipo = DB::table('ttiposentregables as tt')
		->leftJoin('tentregables as te','tt.ctiposentregable','=','te.ctipoentregable')     
        ->lists('tt.descripcion','tt.ctiposentregable');        	

        $disciplina = DB::table('tdisciplina as td')
        ->leftJoin('tentregables as te','td.cdisciplina','=','te.cdisciplina')
		->lists('td.descripcion','td.cdisciplina');

        return view('partials/addEntregableMantenimiento',compact('entregable','disciplina','tipo','codigo','descripcion'));


	}

	public function agregarEntregableMantenimiento(Request $request){
		
		DB::beginTransaction();		
		$centregables = $request->input('centregables');
		//$cdisciplina = $request->input('cdisciplina');
		//$ctipoentregable = $request->input('ctipoentregable');


		if(strlen($centregables)<=0){
			$entregable = new Tentregable();
			//$entregable->cdisciplina= $request->input('cdisciplina');
			//$entregable->save();


			//$centregables= $entregable->centregables;			
			

		}else{
			$entregable = Tentregable::where('centregables','=',$centregables)
			->first();
			
		}
		//$entregable->entregable= $request->input('centregables');
		$entregable->codigo= $request->input('codigo');				
		$entregable->descripcion= $request->input('descripcion');
		$entregable->ctipoentregable= $request->input('ctipoentregable');	
		$entregable->cdisciplina= $request->input('cdisciplina');		
		$entregable->save();	


		DB::commit();
		return Redirect::route('listaEntregables');
	}

	public function eliminarentregableMantenimiento($idEntregable){

		DB::beginTransaction();
		$centregables = Tentregable::where('centregables','=',$idEntregable)->first();

		 try
        {
        	$centregables->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
      

		DB::commit();
        return Redirect::route('listaEntregables');
	}


	//Fin Entregables


	//Cargos

	public function listarCargos(){
		return Datatables::queryBuilder(DB::table('tcargos as tc')				
			->leftJoin('tcargos as t','tc.ccargo_parent','=','t.ccargo')		
			->select('tc.ccargo','tc.codigo','tc.descripcion','tc.siglas','tc.ccargo_parent','t.descripcion as padre',DB::raw('CONCAT(\'rowrdp_\',tc.ccargo)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoCargos(){
		
		$cargo_parent=DB::table('tcargos as tc')				
		->leftJoin('tcargos as t','tc.ccargo_parent','=','t.ccargo')
		->lists('tc.descripcion','tc.ccargo');
		$cargo_parent = [''=>''] + $cargo_parent;
		
		return view('mantenimiento/cargos',compact('cargo_parent'));	
		
	}

	

	public function editarCargos(Request $request,$idCargo){
		

		$cargo= DB::table('tcargos')
		->where('ccargo','=',$idCargo)	
		->first();	

		$codigo= DB::table('tcargos')
		->where('ccargo','=',$idCargo)	
		->first();

		$descripcion= DB::table('tcargos')
		->where('ccargo','=',$idCargo)	
		->first();

		$siglas= DB::table('tcargos')
		->where('ccargo','=',$idCargo)	
		->first();

		$cargo_parent=DB::table('tcargos as tc')				
		->leftJoin('tcargos as t','tc.ccargo_parent','=','t.ccargo')
		->lists('tc.descripcion','tc.ccargo');
		

		
		return view('partials/addCargos',compact('cargo','cargo_parent','codigo','descripcion','siglas'));

	}

	public function agregarCargos(Request $request){
		
		DB::beginTransaction();		
		$ccargo = $request->input('ccargo');


		if(strlen($ccargo)<=0){
			$cargo = new Tcargo();
			
		}else{
			$cargo = Tcargo::where('ccargo','=',$ccargo)
			->first();
			
		}
		
		$cargo->codigo= $request->input('codigo');
		$cargo->descripcion= $request->input('descripcion');
		$cargo->siglas= $request->input('siglas');
		if($request->input('ccargo_parent')!=null || $request->input('ccargo_parent')>0){
			$cargo->ccargo_parent= $request->input('ccargo_parent');
			$cargo->cargoparentdespliegue = $request->input('ccargo_parent');
		}
		
		$cargo->save();	


		DB::commit();
		return Redirect::route('listaCargos');
	}

	public function eliminarCargo($idCargo){
		DB::beginTransaction();
		$cargo = Tcargo::where('ccargo','=',$idCargo)->first();
        
		try
        {
        	$cargo->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('listaCargos');
	}


	//Fin Cargos


	//Area

	public function listarArea(){
		return Datatables::queryBuilder(DB::table('tareas as ta')
			->leftJoin('tareas as t','ta.carea_parent','=','t.carea')	
			->select('ta.carea','ta.codigo','ta.descripcion','ta.siglas','ta.carea_parent','t.descripcion as padre','ta.esgeneral','ta.estecnica','ta.esarea',DB::raw('CONCAT(\'rowrdp_\',ta.carea)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoArea(){
		$area_parent= DB::table('tareas as ta')
		->leftJoin('tareas as t','ta.carea_parent','=','t.carea')
		->lists('ta.descripcion','ta.carea');
		$area_parent = [''=>''] + $area_parent;

		return view('mantenimiento/areas',compact('area','area_parent'));

		
	}
	
	public function editarArea(Request $request,$idArea){		

		$area= DB::table('tareas')
		->where('carea','=',$idArea)	
		->first();	

		$codigo= DB::table('tareas')
		->where('carea','=',$idArea)	
		->first();	

		$descripcion= DB::table('tareas')
		->where('carea','=',$idArea)	
		->first();	

		$siglas= DB::table('tareas')
		->where('carea','=',$idArea)	
		->first();	

		$esgeneral= DB::table('tareas')
		->where('carea','=',$idArea)	
		->first();	

		$estecnica= DB::table('tareas')
		->where('carea','=',$idArea)	
		->first();	

		$esarea= DB::table('tareas')
		->where('carea','=',$idArea)	
		->first();	

		$area_parent= DB::table('tareas as ta')
		->leftJoin('tareas as t','ta.carea_parent','=','t.carea')
		->lists('ta.descripcion','ta.carea');	
		$area_parent = [''=>''] + $area_parent;

		
		return view('partials/addArea',compact('area','area_parent','descripcion','siglas','esgeneral','estecnica','esarea','codigo'));

	}

	public function agregarArea(Request $request){
		
		DB::beginTransaction();		
		$carea = $request->input('carea');


		if(strlen($carea)<=0){
			$area = new Tarea();
			
			

		}else{
			$area = Tarea::where('carea','=',$carea)
			->first();
			
		}
		//$area->carea= $request->input('carea');
		$area->codigo= $request->input('codigo');
		$area->descripcion= $request->input('descripcion');
		$area->siglas= $request->input('siglas');
		if($request->input('carea_parent')!=null || $request->input('carea_parent')>0){
			$area->carea_parent= $request->input('carea_parent');
		}

		if($request->input('esgeneral')!=null || $request->input('esgeneral')>0){
			$area->esgeneral= $request->input('esgeneral');
		}
		
		if($request->input('estecnica')!=null || $request->input('estecnica')>0){
			$area->estecnica= $request->input('estecnica');
		}
		
		if($request->input('esarea')!=null || $request->input('esarea')>0){
			$area->esarea= $request->input('esarea');
		}
		
		
		$area->save();	


		DB::commit();
		return Redirect::route('listaAreas');
	}

	public function eliminarAreaMant($idArea){
		DB::beginTransaction();
		$area = Tarea::where('carea','=',$idArea)->first();
        
		 try
        {
        	$area->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('listaAreas');
	}


	//Fin Area

	//Inicio Actividad
	public function listarActividad(){
		return Datatables::queryBuilder(DB::table('tactividad as t')	
			->leftJoin('tactividad as ta','t.cactividad_parent','=','ta.cactividad')		
			->select('t.cactividad','t.codigo','t.descripcion as nombre','t.cactividad_parent','ta.descripcion as padre',DB::raw('CONCAT(\'rowrdp_\',t.cactividad)   as "DT_RowId"'))
			)->make(true);
	}


	public function listadoActividad(){

		$actividad_parent = DB::table('tactividad')
		->lists('descripcion','cactividad');
		$actividad_parent = [''=>''] + $actividad_parent;

		return view('mantenimiento/actividades',compact('actividad','actividad_parent'));
	}

	
	public function editarActividad(Request $request,$idActividad){

		$actividad_parent = DB::table('tactividad as t')
		->leftJoin('tactividad as ta','t.cactividad','=','ta.cactividad_parent')
		->lists('t.descripcion','t.cactividad');	
		$actividad_parent = [''=>''] + $actividad_parent;

		$actividad=DB::table('tactividad')
		->where('cactividad','=',$idActividad)
		->first();

		$descripcion=DB::table('tactividad')
		->where('cactividad',$idActividad)
		->first();

		$codigo=DB::table('tactividad')
		->where('cactividad',$idActividad)
		->first();

		

		return view('partials/addActividades',compact('actividad','actividad_parent','descripcion','codigo'));
	}

	public function agregarActividad(Request $request){
		DB::beginTransaction();
		$cactividad = $request->input('cactividad');

		if(strlen($cactividad)<=0){
			$actividad = new Tactividad();	
			
		}else{
			$actividad = Tactividad::where('cactividad','=',$cactividad)
			->first();			
		}	
		$actividad->codigo= $request->input('codigo');
		$actividad->descripcion= $request->input('descripcion');
		if($request->input('cactividad_parent')!= null || $request->input('cactividad_parent')>0){
			$actividad->cactividad_parent= $request->input('cactividad_parent');
		}
		
		$actividad->save();
	
		DB::commit(); 



		return Redirect::route('listaActividades');  
	}

	public function eliminarActividad($idActividad){
		DB::beginTransaction();
		 $actividad = Tactividad::where('cactividad','=',$idActividad)
		 ->first();
       
        try
        {
        	$actividad->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('listaActividades');
	}
	//Fin Actividad

	//Area Conocimiento

	public function listarAreasConocimiento(){
		return Datatables::queryBuilder(DB::table('tareasconocimiento as tco')				
			->select('tco.careaconocimiento','tco.descripcion',DB::raw('CONCAT(\'rowrdp_\',tco.careaconocimiento)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoAreasConocimiento(){
		
		

		return view('mantenimiento/areasConocimiento',compact('areacon'));		
		
	}

	
	public function editarAreasConocimiento(Request $request,$idAreaCon){
		

		$areacon= DB::table('tareasconocimiento')
		->where('careaconocimiento','=',$idAreaCon)	
		->first();	
		
		$descripcion= DB::table('tareasconocimiento')
		->where('careaconocimiento','=',$idAreaCon)	
		->first();

		
		return view('partials/addAreasConocimiento',compact('areacon','descripcion'));

	}

	public function agregarAreasConocimiento(Request $request){
		
		DB::beginTransaction();		
		$careaconocimiento = $request->input('careaconocimiento');


		if(strlen($careaconocimiento)<=0){
			$areacon = new Tareasconocimiento();
					
			

		}else{
			$areacon = Tareasconocimiento::where('careaconocimiento','=',$careaconocimiento)
			->first();
			
		}
	
		$areacon->descripcion= $request->input('descripcion');		
		
		$areacon->save();	


		DB::commit();
		return Redirect::route('listaAreasConocimiento');
	}

	public function eliminarAreasConocimiento($idAreaCon){
		DB::beginTransaction();
		$areacon = Tareasconocimiento::where('careaconocimiento','=',$idAreaCon)->first();

		try
        {
        	$areacon->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        
		DB::commit();
        return Redirect::route('listaAreasConocimiento');
	}


	//Fin Area



	//Inicio Concepto Gastos

	public function listarGastos(){
		return Datatables::queryBuilder(DB::table('tconceptogastos as tc')	
			->leftJoin('tmonedas as tm','tc.cmoneda','=','tm.cmoneda')	
			->leftJoin('tconceptogastos as tcg','tc.cconcepto_parent','=','tcg.cconcepto')	
			->leftJoin('tcatalogo as tca',function($join){
				$join->on('tc.tipogasto','=','tca.digide');
				$join->on('tca.codtab','=',DB::raw('\'00026\''));
			})	
			->leftJoin('tcatalogo as tca1',function($join){
				$join->on('tc.categoria_gasto','=','tca1.digide');
				$join->on('tca1.codtab','=',DB::raw('\'00027\''));
			})	
			->select('tc.cconcepto','tc.descripcion','tc.unidad','tm.cmoneda','tm.descripcion as moneda','tc.costounitario','tc.tipogasto','tca.descripcion as tipogasto','tcg.descripcion as padre','tc.categoria_gasto','tca1.descripcion as categoria',DB::raw('CONCAT(\'rowrdp_\',tc.cconcepto)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoGastos(){
		$conceptop = DB::table('tconceptogastos')
		->orderBy('descripcion','ASC')
		->lists('descripcion','cconcepto');
		$conceptop = [''=>''] + $conceptop;

		
		$tipo = DB::table('tcatalogo')
		->orderBy('descripcion','ASC')
		->where('codtab','=','00026')
		->lists('descripcion','digide');		
		$tipo = [''=>''] + $tipo;

		$categoria = DB::table('tcatalogo')
		->orderBy('descripcion','ASC')	
		->where('codtab','=','00027')
		->lists('descripcion','digide');		
		$categoria = [''=>''] + $categoria;
		

		$moneda = DB::table('tmonedas')
		->lists('descripcion','cmoneda');
		$moneda = [''=>''] + $moneda;

		return view('mantenimiento/conceptoGastos',compact('concepto','conceptop','moneda','tipo','categoria'));
	}

	public function editarGasto(Request $request,$idConcepto){

		$concepto=DB::table('tconceptogastos')
		->where('cconcepto',$idConcepto)
		->first();

		$costounitario=DB::table('tconceptogastos')
		->where('cconcepto',$idConcepto)
		->first();

		$descripcion=DB::table('tconceptogastos')
		->where('cconcepto',$idConcepto)
		->first();

		$unidad=DB::table('tconceptogastos')
		->where('cconcepto',$idConcepto)
		->first();
		
		$tipo = DB::table('tcatalogo as tca')
		->orderBy('tca.descripcion','ASC')
		->where('tca.codtab','=','00026')
		->lists('tca.descripcion','tca.digide');	
		$tipo = [''=>''] + $tipo;			

		$categoria = DB::table('tcatalogo as tca')
		->orderBy('tca.descripcion','ASC')
		->where('tca.codtab','=','00027')
		->lists('tca.descripcion','tca.digide');	
		$categoria = [''=>''] + $categoria;

		$conceptop = DB::table('tconceptogastos as tg')
		->leftJoin('tconceptogastos as tc','tg.cconcepto','=','tc.cconcepto_parent')
		->orderBy('tg.descripcion','ASC')
		->where('tc.cconcepto',$idConcepto)	
		->lists('tg.descripcion','tg.cconcepto');
		$conceptop = [''=>''] + $conceptop;
	

		$moneda = DB::table('tmonedas as tm')
		->leftJoin('tconceptogastos as tc','tm.cmoneda','=','tc.cmoneda')
		->lists('tm.descripcion','tm.cmoneda');
		$moneda = [''=>''] + $moneda;
		

		return view('partials/addConceptoGastos',compact('concepto','conceptop','moneda','tipo','categoria','descripcion','unidad','costounitario'));
	}

	public function agregarGasto(Request $request){
		DB::beginTransaction();
		$cconcepto = $request->input('cconcepto');

		if(strlen($cconcepto)<=0){
			$concepto = new Tconceptogasto();	
			
 			
		}else{
			$concepto = Tconceptogasto::where('cconcepto','=',$cconcepto)
			->first();			
		}	
		$concepto->descripcion= $request->input('descripcion');
		$concepto->unidad= $request->input('unidad');
		$concepto->cmoneda= $request->input('cmoneda');
		$concepto->costounitario= $request->input('costounitario');
		$concepto->tipogasto= $request->input('tipogasto');
		if($request->input('cconcepto_parent')!= null || $request->input('cconcepto_parent')>0){
			$concepto->cconcepto_parent= $request->input('cconcepto_parent');
		}
		
		$concepto->categoria_gasto= $request->input('categoria_gasto');


		$concepto->save();
	
		DB::commit(); 



		return Redirect::route('listaConceptoGastos');  
	}

	public function eliminarGasto($idGasto){

		 DB::beginTransaction();
		 $gasto = Tconceptogasto::where('cconcepto','=',$idGasto)
		 ->first();
       
        try
        {
        	$gasto->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('listaConceptoGastos');  
      
    }



	//Fin conceptpo Gastos


	//Sedes	

	public function listarSedes(){
		return Datatables::queryBuilder(DB::table('tsedes as tse')		
			->select('tse.csede','tse.codigosede','tse.descripcion',DB::raw('CONCAT(\'rowrdp_\',tse.csede)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoSedes(){		

		return view('mantenimiento/sedes',compact('sede'));		
		
	}			
		

	public function editarSede(Request $request,$idSede){
		

		$sede= DB::table('tsedes')
		->where('csede','=',$idSede)	
		->first();	

		$codigosede= DB::table('tsedes')
		->where('csede','=',$idSede)	
		->first();

		$descripcion= DB::table('tsedes')
		->where('csede','=',$idSede)	
		->first();

		
		return view('partials/addSedes',compact('sede','codigosede','descripcion'));

	}

	public function agregarSede(Request $request){
		
		DB::beginTransaction();
		$csede = $request->input('csede');

		if(strlen($csede)<=0){
			$sede = new Tsede();			
			

		}else{
			$sede = Tsede::where('csede','=',$csede)
			->first();
		}

		$sede->codigosede=$request->input('codigosede');
		$sede->descripcion=$request->input('descripcion');

		$sede->save();

		DB::commit();
		return Redirect::route('listaSedes');
		
	}

	public function eliminarSedeMant($idSede){
		DB::beginTransaction();
		$sede = Tsede::where('csede','=',$idSede)->first();
		
		try
		{
			$sede->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaSedes');
	}


	//Fin Sedes


	//Pais

	public function listarPais(){
		return Datatables::queryBuilder(DB::table('tpais as tpa')		
			->select('tpa.cpais','tpa.descripcion',DB::raw('CONCAT(\'rowrdp_\',tpa.cpais)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoPais(){	

		return view('mantenimiento/pais',compact('pais'));		
		
	}			
	

	public function editarPais(Request $request,$idPais){
		

		$pais= DB::table('tpais')
		->where('cpais','=',$idPais)	
		->first();	

		$descripcion= DB::table('tpais')
		->where('cpais','=',$idPais)	
		->first();

		
		return view('partials/addPais',compact('pais','descripcion'));

	}

	public function agregarPais(Request $request){
		
		DB::beginTransaction();
		$cpais = $request->input('cpais');

		$pais = Tpai::where('cpais','=',$cpais)->first();

		if(!$pais){
			$pais = new Tpai();		
			$pais->cpais=$cpais;
		}
		$pais->descripcion=$request->input('descripcion');

		$pais->save();

		DB::commit();
		return Redirect::route('listaPais');
		
	}

	public function eliminarPais($idPais){
		DB::beginTransaction();
		$pais = Tpai::where('cpais','=',$idPais)->first();
		
		try
		{
			$pais->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaPais');
	}



	//Fin Pais

	//Fase Proyecto


	public function listarFaseProyecto(){
		return Datatables::queryBuilder(DB::table('tfasesproyecto as tfp')	
			->select('tfp.cfaseproyecto','tfp.codigo','tfp.descripcion',DB::raw('CONCAT(\'rowrdp_\',tfp.cfaseproyecto)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoFaseProyecto(){


		return view('mantenimiento/fasesProyecto',compact('fase'));
		
	}			
	

	public function editarFaseProyecto(Request $request,$idFaseProyecto){
		

		$fase= DB::table('tfasesproyecto')
		->where('cfaseproyecto','=',$idFaseProyecto)	
		->first();	

		$codigo= DB::table('tfasesproyecto')
		->where('cfaseproyecto','=',$idFaseProyecto)	
		->first();

		$descripcion= DB::table('tfasesproyecto')
		->where('cfaseproyecto','=',$idFaseProyecto)	
		->first();
		
		return view('partials/addFasesProyecto',compact('fase','codigo','descripcion'));

	}

	public function agregarFaseProyecto(Request $request){
		
		DB::beginTransaction();
		$cfaseproyecto = $request->input('cfaseproyecto');

		if(strlen($cfaseproyecto)<=0){
			$fase = new Tfasesproyecto();		
			

		}else{
			$fase = Tfasesproyecto::where('cfaseproyecto','=',$cfaseproyecto)
			->first();
		}

		$fase->codigo=$request->input('codigo');
		$fase->descripcion=$request->input('descripcion');

		$fase->save();

		DB::commit();
		return Redirect::route('listaFasesProyecto');
		
	}

	public function eliminarFaseProyecto($idFaseProyecto){
		DB::beginTransaction();
		$fase = Tfasesproyecto::where('cfaseproyecto','=',$idFaseProyecto)->first();
		
		try
		{
			$fase->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaFasesProyecto');
	}


	//Fin Fase Proyecto


	//checklist

	public function listarCheckList(){
		return Datatables::queryBuilder(DB::table('tchecklist as tch')	
			->leftJoin('troldespliegue as trd','tch.crol_despliegue','=','trd.croldespliegue')	
			->leftJoin('tfasesproyecto as tfa','tch.cfaseproyecto','=','tfa.cfaseproyecto')	
			->select('tch.cchecklist','trd.descripcionrol as rol','tch.descripcionactvividad','tfa.descripcion as fase',DB::raw('CONCAT(\'rowrdp_\',tch.cchecklist)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoCheckList(){					

		$troldes = DB::table('troldespliegue')
		->lists('descripcionrol','croldespliegue');
		$troldes = [''=>''] + $troldes;	

		$tfase = DB::table('tfasesproyecto')
		->lists('descripcion','cfaseproyecto');
		$tfase = [''=>''] + $tfase;				


		return view('mantenimiento/checklist',compact('check','troldes','tfase'));		
		
	}

	public function editarCheckList(Request $request,$idChecklist){
		

		$check= DB::table('tchecklist')
		->where('cchecklist','=',$idChecklist)	
		->first();	

		$descripcionactvividad= DB::table('tchecklist')
		->where('cchecklist','=',$idChecklist)	
		->first();	

		$troldes = DB::table('troldespliegue as trd')
		->leftJoin('tchecklist as tc','trd.croldespliegue','=','tc.crol_despliegue')
		->lists('trd.descripcionrol','trd.croldespliegue');
		$troldes = [''=>''] + $troldes;	

		$tfase = DB::table('tfasesproyecto as tf')
		->leftJoin('tchecklist as tc','tf.cfaseproyecto','=','tc.cfaseproyecto')
		->lists('tf.descripcion','tf.cfaseproyecto');
		$tfase = [''=>''] + $tfase;		

		
		return view('partials/addCheckListMantenimiento',compact('check','descripcionactvividad','troldes','tfase'));

	}

	public function agregarCheckListMant(Request $request){
		DB::beginTransaction();
		$checklist_s = $request->input('checklist_s');

		if(strlen($checklist_s)<=0){
			$check = new Tchecklist();			
			
		}else{
			$check = Tchecklist::where('cchecklist','=',$checklist_s)
			->first();
		}
		$check->crol_despliegue=$request->input('crol_despliegue');		
		$check->descripcionactvividad=$request->input('descripcionactvividad');
		$check->cfaseproyecto=$request->input('cfaseproyecto');

		$check->save();

		DB::commit();
		return Redirect::route('listaCheckList');
		
	}


	public function eliminarCheckList($idChecklist){
		DB::beginTransaction();
		$check = Tchecklist::where('cchecklist','=',$idChecklist)->first();
		
		try
		{
			$check->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaCheckList');
	}


	//Fin Checklist



	//Disciplina

	public function listarDisciplina(){
		return Datatables::queryBuilder(DB::table('tdisciplina as tdi')		
			->select('tdi.cdisciplina','tdi.descripcion','tdi.abrevia',DB::raw('CONCAT(\'rowrdp_\',tdi.cdisciplina)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoDisciplina(){

		return view('mantenimiento/disciplina',compact('disciplina'));		
		
	}

	public function editarDisciplina(Request $request,$idDisciplina){
		

		$disciplina= DB::table('tdisciplina')
		->where('cdisciplina','=',$idDisciplina)	
		->first();	

		$descripcion= DB::table('tdisciplina')
		->where('cdisciplina','=',$idDisciplina)	
		->first();

		$abrevia= DB::table('tdisciplina')
		->where('cdisciplina','=',$idDisciplina)	
		->first();
		
		return view('partials/addDisciplina',compact('disciplina','descripcion','abrevia'));

	}

	public function agregarDisciplina(Request $request){
		
		DB::beginTransaction();
		$cdisciplina = $request->input('cdisciplina');

		if(strlen($cdisciplina)<=0){
			$disciplina = new Tdisciplina();

			
			
		}else{
			$disciplina = Tdisciplina::where('cdisciplina','=',$cdisciplina)
			->first();
		}

		$disciplina->descripcion=$request->input('descripcion');
		$disciplina->abrevia=$request->input('abrevia');

		$disciplina->save();

		DB::commit();
		return Redirect::route('listaDisciplina');
		
	}

	public function eliminarDisciplina($idDisciplina){
		DB::beginTransaction();
		$disciplina = Tdisciplina::where('cdisciplina','=',$idDisciplina)->first();
		
		try
		{
			$disciplina->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaDisciplina');
	}


	//Fin Disciplina


	//Contacto Cargo

	public function listarContactoCargo(){
		return Datatables::queryBuilder(DB::table('tcontactocargo as tcc')		
			->select('tcc.ccontactocargo','tcc.descripcion',DB::raw('CONCAT(\'rowrdp_\',tcc.ccontactocargo)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoContactoCargo(){					

		return view('mantenimiento/contactocargo',compact('contactocargo'));		
		
	}

	public function editarContactoCargo(Request $request,$idContactoCargo){		

		$contactocargo= DB::table('tcontactocargo')
		->where('ccontactocargo','=',$idContactoCargo)	
		->first();	

		$descripcion= DB::table('tcontactocargo')
		->where('ccontactocargo','=',$idContactoCargo)	
		->first();	

		
		return view('partials/addContactoCargo',compact('contactocargo','descripcion'));

	}

	public function agregarContactoCargo(Request $request){
		
		DB::beginTransaction();
		$ccontactocargo = $request->input('ccontactocargo');

		$contactocargo = Tcontactocargo::where('ccontactocargo','=',$ccontactocargo)->first();

		if(!$contactocargo){
			$contactocargo = new Tcontactocargo();		
			$contactocargo->ccontactocargo=$ccontactocargo;
		}
		$contactocargo->descripcion=$request->input('descripcion');

		$contactocargo->save();

		DB::commit();
		return Redirect::route('listaContactoCargo');
		
	}

	public function eliminarContactoCargo($idContactoCargo){
		DB::beginTransaction();
		$contactocargo = Tcontactocargo::where('ccontactocargo','=',$idContactoCargo)->first();
		
		try
		{
			$contactocargo->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaContactoCargo');
	}
	//Fin Contacto Cargo

//Inicio Concepto Institucion

	public function listarInstitucion(){
		return Datatables::queryBuilder(DB::table('tinstitucion as ti')	
			->leftJoin('tcatalogo as tca','ti.tipoinstitucion','=','tca.digide')					
			->where('tca.codtab','=','00028')
			->select('ti.cinstitucion','ti.abrevia','ti.descripcion','ti.tipoinstitucion','tca.descripcion as tipo',DB::raw('CONCAT(\'rowrdp_\',ti.cinstitucion)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoInstitucion(){	
	$tipoInstitucion = DB::table('tcatalogo')
		->orderBy('descripcion','ASC')
		->where('codtab','=','00028')
		->lists('descripcion','digide');		
		$tipoInstitucion = [''=>''] + $tipoInstitucion;
	

		return view('mantenimiento/institucion',compact('institucion','tipoInstitucion'));
	}


	//Oficina
	
	public function listarOficina(){
		return Datatables::queryBuilder(DB::table('toficinas as tof')
			->leftJoin('tsedes as tse','tof.csede','=','tse.csede')		
			->select('tof.coficinas','tof.csede','tof.codigoofi','tof.descripcion','tse.descripcion as sede',DB::raw('CONCAT(\'rowrdp_\',tof.coficinas)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoOficina(){

		$tsede = DB::table('tsedes')
		->lists('descripcion','csede');
		$tsede = [''=>''] + $tsede;					

		return view('mantenimiento/oficinas',compact('oficina','tsede'));		
		
	}

	public function eliminarInstitucion($idInstitucion){

		 DB::beginTransaction();
		 $institucion = Tinstitucion::where('cinstitucion','=',$idInstitucion)
		 ->first();
       
        try
        {
        	$institucion->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('listaInstitucion');  
      
    }

	public function editarOficina(Request $request,$idOficina){
		

		$oficina= DB::table('toficinas')
		->where('coficinas','=',$idOficina)	
		->first();

		$codigoofi= DB::table('toficinas')
		->where('coficinas','=',$idOficina)	
		->first();

		$descripcion= DB::table('toficinas')
		->where('coficinas','=',$idOficina)	
		->first();

		$tsede = DB::table('tsedes as t')
		->leftJoin('toficinas as to','t.csede','=','to.csede')
		->lists('t.descripcion','t.csede');
		$tsede = [''=>''] + $tsede;	

		
		return view('partials/addOficina',compact('oficina','tsede','codigoofi','descripcion'));

	}

	public function agregarOficina(Request $request){
		
		DB::beginTransaction();
		$coficinas = $request->input('coficinas');

		if(strlen($coficinas)<=0){
				$oficina = new Toficina();
			
	
			
		}else{
			$oficina = Toficina::where('coficinas','=',$coficinas)
			->first();
		}
		$oficina->csede=$request->input('csede');
		$oficina->codigoofi=$request->input('codigoofi');
		$oficina->descripcion=$request->input('descripcion');
		

		$oficina->save();

		DB::commit();
		return Redirect::route('listaOficina');
		
	}

	public function eliminarOficina($idOficina){
		DB::beginTransaction();
		$oficina = Toficina::where('coficinas','=',$idOficina)->first();
		
		try
		{
			$oficina->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaOficina');
	}


	//Fin Oficina


	//Tipo no Facturable

	public function listarTiposNoFacturables(){
		return Datatables::queryBuilder(DB::table('ttiposnofacturables as tnf')				
			->select('tnf.ctiponofacturable','tnf.descripcion','tnf.tipo',DB::raw('CONCAT(\'rowrdp_\',tnf.ctiponofacturable)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoTiposNoFacturables(){			
		

		return view('mantenimiento/tiposNoFacturables',compact('ttiponofac'));		
		
	}

	public function editarTiposNoFacturables(Request $request,$idTiposNoFacturables){
		

		$ttiponofac= DB::table('ttiposnofacturables')
		->where('ctiponofacturable','=',$idTiposNoFacturables)	
		->first();	

		$descripcion= DB::table('ttiposnofacturables')
		->where('ctiponofacturable','=',$idTiposNoFacturables)	
		->first();

		$tipo= DB::table('ttiposnofacturables')
		->where('ctiponofacturable','=',$idTiposNoFacturables)	
		->get();		

		
		return view('partials/addTiposNoFacturables',compact('ttiponofac','descripcion','tipo'));

	}

	public function agregarTiposNoFacturables(Request $request){
		
		DB::beginTransaction();
		$ctiponofacturable = $request->input('ctiponofacturable');

		if(strlen($ctiponofacturable)<=0){
				$ttiponofac = new Ttiposnofacturable();
			
	
			
		}else{
			$ttiponofac = Ttiposnofacturable::where('ctiponofacturable','=',$ctiponofacturable)
			->first();
		}
		
		$ttiponofac->descripcion=$request->input('descripcion');
		$ttiponofac->tipo=$request->input('tipo');

		$ttiponofac->save();

		DB::commit();
		return Redirect::route('listaTiposNoFacturables');
		
	}

	public function eliminarTiposNoFacturables($idTiposNoFacturables){
		DB::beginTransaction();
		$ttiponofac = Ttiposnofacturable::where('ctiponofacturable','=',$idTiposNoFacturables)->first();
		
		try
		{
			$ttiponofac->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaTiposNoFacturables');
	}
	//Fin Tipo no Facturable


	//TipoEntregables

	public function listarTiposEntregables(){
		return Datatables::queryBuilder(DB::table('ttiposentregables as ten')
			->select('ten.ctiposentregable','ten.descripcion',DB::raw('CONCAT(\'rowrdp_\',ten.ctiposentregable)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoTiposEntregables(){	


		return view('mantenimiento/tiposEntregables',compact('ttipoent'));
		
	}			
	

	public function editarTiposEntregables(Request $request,$idTipoEntregable){
		

		$ttipoent= DB::table('ttiposentregables')
		->where('ctiposentregable','=',$idTipoEntregable)	
		->first();	
		$descripcion= DB::table('ttiposentregables')
		->where('ctiposentregable','=',$idTipoEntregable)	
		->first();	

		
		return view('partials/addTiposEntregables',compact('ttipoent','descripcion'));

	}

	public function agregarTiposEntregables(Request $request){
		
		DB::beginTransaction();
		$ctiposentregable = $request->input('ctiposentregable');

		$ttipoent = Ttiposentregable::where('ctiposentregable','=',$ctiposentregable)->first();

		if(!$ttipoent){
			$ttipoent = new Ttiposentregable();		
			$ttipoent->ctiposentregable=$ctiposentregable;
		}
		$ttipoent->descripcion=$request->input('descripcion');

		$ttipoent->save();

		DB::commit();
		return Redirect::route('listaTiposEntregables');
		
	}

	public function eliminarTiposEntregables($idTipoEntregable){
		DB::beginTransaction();
		$ttipoent = Ttiposentregable::where('ctiposentregable','=',$idTipoEntregable)->first();
		
		try
		{
			$ttipoent->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaTiposEntregables');
	}



	//Fin TiposEntregables

		

	public function editarInstitucion($idInstitucion){	
		$institucion=DB::table('tinstitucion')
		->where('cinstitucion','=',$idInstitucion)
		->first();

		$descripcion=DB::table('tinstitucion')
		->where('cinstitucion','=',$idInstitucion)
		->first();

		$abrevia=DB::table('tinstitucion')
		->where('cinstitucion','=',$idInstitucion)
		->first();

		$tipoInstitucion = DB::table('tcatalogo as t')
		->leftJoin('tinstitucion as ti','t.digide','=','ti.tipoinstitucion')
		->orderBy('t.descripcion','ASC')
		->where('t.codtab','=','00028')
		->lists('t.descripcion','t.digide');		
		$tipoInstitucion = [''=>''] + $tipoInstitucion;

		return view('partials/addInstitucion',compact('institucion','tipoInstitucion','descripcion','abrevia'));
	}

	public function agregarInstitucion(Request $request){
		DB::beginTransaction();
		$cinstitucion = $request->input('cinstitucion');

		if(strlen($cinstitucion)<=0){
			$institucion = new Tinstitucion();	
			
 			
		}else{
			$institucion = Tinstitucion::where('cinstitucion','=',$cinstitucion)
			->first();			
		}
		$institucion->abrevia= $request->input('abrevia');	
		$institucion->descripcion= $request->input('descripcion');
		$institucion->tipoinstitucion= $request->input('tipoinstitucion');
		$institucion->save();
	
		DB::commit(); 

		return Redirect::route('listaInstitucion');  
	}

	public function listarTiposContacto(){
		return Datatables::queryBuilder(DB::table('ttiposcontacto')			
			->select('ctipocontacto','descripcion',DB::raw('CONCAT(\'rowrdp_\',ctipocontacto)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoTiposContacto(){
		return view('mantenimiento/tiposContacto');
	}

	public function editarTiposContacto(Request $request,$idTiposContacto){
		$tipocontacto=DB::table('ttiposcontacto')
		->where('ctipocontacto','=',$idTiposContacto)
		->first();

		$descripcion=DB::table('ttiposcontacto')
		->where('ctipocontacto','=',$idTiposContacto)
		->first();

		return view('partials/addTiposContacto',compact('tipocontacto','descripcion'));
	}

	public function agregarTiposContacto(Request $request){
		DB::beginTransaction();
		$ctipocontacto = $request->input('ctipocontacto');

		$tipocontacto = Ttiposcontacto::where('ctipocontacto','=',$ctipocontacto)
			->first();


		if(!$tipocontacto ){
			$tipocontacto = new Ttiposcontacto();	
			$tipocontacto->ctipocontacto= $ctipocontacto;
			
 			
		}
		$tipocontacto->descripcion= $request->input('descripcion');
		$tipocontacto->save();
	
		DB::commit(); 

		return Redirect::route('listaTiposContacto');  
	}

	public function eliminarTiposContacto($idTiposContacto){

		 DB::beginTransaction();
		 $tiposContacto = Ttiposcontacto::where('ctipocontacto','=',$idTiposContacto)
		 ->first();
       
        try
        {
        	$tiposContacto->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('listaTiposContacto');  
      
    }


public function listarTiposInformacionContacto(){
		return Datatables::queryBuilder(DB::table('ttiposinformacioncontacto')			
			->select('ctipoinformacioncontacto','descripcion',DB::raw('CONCAT(\'rowrdp_\',ctipoinformacioncontacto)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoTiposInformacionContacto(){
		return view('mantenimiento/tiposInformacionContacto');
	}

	public function editarTiposInformacionContacto(Request $request,$idTiposInformacionContacto){
		$tipoinformacioncontacto=DB::table('ttiposinformacioncontacto')
		->where('ctipoinformacioncontacto','=',$idTiposInformacionContacto)
		->first();

		$descripcion=DB::table('ttiposinformacioncontacto')
		->where('ctipoinformacioncontacto','=',$idTiposInformacionContacto)
		->first();

		return view('partials/addTiposInformacionContacto',compact('tipoinformacioncontacto','descripcion'));
	}

	public function agregarTiposInformacionContacto(Request $request){
		DB::beginTransaction();
		$ctipoinformacioncontacto = $request->input('ctipoinformacioncontacto');

		$tipoinformacioncontacto = Ttiposinformacioncontacto::where('ctipoinformacioncontacto','=',$ctipoinformacioncontacto)->first();	
		
			
		if(!$tipoinformacioncontacto) {

		$tipoinformacioncontacto = new Ttiposinformacioncontacto();
		$tipoinformacioncontacto->ctipoinformacioncontacto=$ctipoinformacioncontacto;

		}
		$tipoinformacioncontacto->descripcion= $request->input('descripcion');
		$tipoinformacioncontacto->save();
	
		DB::commit(); 

		return Redirect::route('listaTiposInformacionContacto');  
	}

	public function eliminarTiposInformacionContacto($idTiposInformacionContacto){

		 DB::beginTransaction();
		 $tiposInformacionContacto = Ttiposinformacioncontacto::where('ctipoinformacioncontacto','=',$idTiposInformacionContacto)
		 ->first();
       
        try
        {
        	$tiposInformacionContacto->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('listaTiposInformacionContacto');  
      
    }

	
	public function listarCatalogoGrupoTablas(){
		return Datatables::queryBuilder(DB::table('tcatalogo')			
			->select('catalogoid','codtab','descripcion','valor',DB::raw('CONCAT(\'rowrdp_\',catalogoid)   as "DT_RowId"'))
			->whereNull('digide')
			)->make(true);
	}

	public function listadoCatalogoGrupoTablas(){
		return view('mantenimiento/catalogoGrupoTablas');
	}

	//Enfermedad

	public function listarEnfermedad(){
		return Datatables::queryBuilder(DB::table('tenfermedad as tef')	
			->select('tef.cenfermedad','tef.descripcion',DB::raw('CONCAT(\'rowrdp_\',tef.cenfermedad)   as "DT_RowId"'))			
			)->make(true);
	}

	public function editarCatalogoGrupoTablas(Request $request,$idCaGrupoTabla){
		
		$catalogrtab=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaGrupoTabla)
		->first();

		$codtabla=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaGrupoTabla)
		->first();

		$descripcion=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaGrupoTabla)
		->first();

		return view('partials/addCatalogoGrupoTablas',compact('catalogrtab','codtabla','descripcion'));
	}

	public function listadoEnfermedad(){


		return view('mantenimiento/enfermedad',compact('tenf'));
		
	}			
	
	public function editarEnfermedad(Request $request,$idEnfermedad){
		

		$tenf= DB::table('tenfermedad')
		->where('cenfermedad','=',$idEnfermedad)	
		->first();	

		$descripcion= DB::table('tenfermedad')
		->where('cenfermedad','=',$idEnfermedad)	
		->first();
		
		return view('partials/addEnfermedad',compact('tenf','descripcion'));

	}

	public function agregarEnfermedad(Request $request){
		
		DB::beginTransaction();
		$cenfermedad = $request->input('cenfermedad');

		if(strlen($cenfermedad)<=0){
				$tenf = new Tenfermedad();
			
	
			
		}else{
			$tenf = Tenfermedad::where('cenfermedad','=',$cenfermedad)
			->first();
		}
		
		$tenf->descripcion=$request->input('descripcion');

		$tenf->save();

		DB::commit();
		return Redirect::route('listaEnfermedad');
		
	}

	public function eliminarEnfermedad($idEnfermedad){
		DB::beginTransaction();
		$tenf = Tenfermedad::where('cenfermedad','=',$idEnfermedad)->first();
		
		try
		{
			$tenf->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaEnfermedad');
	}



	//Fin Enfermedad


	//Especialidad

	public function listarEspecialidad(){
		return Datatables::queryBuilder(DB::table('tespecialidad as tes')	
			->select('tes.cespecialidad','tes.abrevia','tes.descripcion',DB::raw('CONCAT(\'rowrdp_\',tes.cespecialidad)   as "DT_RowId"'))			
			)->make(true);
	}

	public function listadoEspecialidad(){


		return view('mantenimiento/especialidad');
		
	}			
	
	public function editarEspecialidad(Request $request,$idEspecialidad){
		

		$espec= DB::table('tespecialidad')
		->where('cespecialidad','=',$idEspecialidad)	
		->first();

		$abrevia= DB::table('tespecialidad')
		->where('cespecialidad','=',$idEspecialidad)	
		->first();

		$descripcion= DB::table('tespecialidad')
		->where('cespecialidad','=',$idEspecialidad)	
		->first();	

		
		return view('partials/addEspecialidad',compact('espec','abrevia','descripcion'));

	}

	public function agregarEspecialidad(Request $request){
		
		DB::beginTransaction();
		$cespecialidad = $request->input('cespecialidad');

		if(strlen($cespecialidad)<=0){
			$espec = new Tespecialidad();			
			

		}else{
			$espec = Tespecialidad::where('cespecialidad','=',$cespecialidad)
			->first();
		}

		$espec->abrevia=$request->input('abrevia');
		$espec->descripcion=$request->input('descripcion');

		$espec->save();

		DB::commit();
		return Redirect::route('listaEspecialidad');
		
	}

	public function eliminarEspecialidad($idEspecialidad){
		DB::beginTransaction();
		$espec = Tespecialidad::where('cespecialidad','=',$idEspecialidad)->first();
		
		try
		{
			$espec->delete();
		}catch(\Illuminate\Database\QueryException $e){

		}
        
		DB::commit();
        return Redirect::route('listaEspecialidad');
	}


	//Fin Especialidad
		

	public function agregarCatalogoGrupoTablas(Request $request){
		DB::beginTransaction();
		$catalogoid = $request->input('catalogoid');


		if(strlen($catalogoid)<=0){
			$catalogrtab = new Tcatalogo();	
			
 			
		}else{
			$catalogrtab = Tcatalogo::where('catalogoid','=',$catalogoid)
			->first();			
		}	
		$catalogrtab->codtab= $request->input('codtab');
		$catalogrtab->descripcion= $request->input('descripcion');
		$catalogrtab->save();
	
		DB::commit(); 


		return Redirect::route('catalogoGrupoTablas');  
	}

	public function eliminarCatalogoGrupoTablas($idCatalogo){

		 DB::beginTransaction();
		 $catalogo = Tcatalogo::where('catalogoid','=',$idCatalogo)
		 ->first();
       
        try
        {
        	$catalogo->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('catalogoGrupoTablas');  
      
    }

	public function listarCatalogoTablas(){
				return Datatables::queryBuilder(DB::table('tcatalogo as t')
			->leftJoin('tcatalogo as c','t.codtab','=','c.codtab')		
			->select('t.catalogoid','t.codtab','t.descripcion','t.digide','t.valor','c.descripcion as tabla',DB::raw('CONCAT(\'rowrdp_\',t.catalogoid)   as "DT_RowId"'))
			->whereNull('c.digide')
			->whereNotNull('t.digide')
			//->orderBy('codtab','ASC')
			)->make(true);
	}	

	public function listadoCatalogoTablas(){

		$tabladesc = DB::table('tcatalogo')
		->whereNull('digide')
		->orderBy('descripcion')
		->lists('descripcion','codtab');
		$tabladesc = [''=>''] + $tabladesc;	
		

		return view('mantenimiento/catalogoTablas',compact('tabladesc'));
	}

	public function editarCatalogoTablas(Request $request,$idCaTabla){	

		$catalotab=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaTabla)
		->first();

		$tabladesc=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaTabla)
		->lists('descripcion');
		$tabladesc = [''=>''] + $tabladesc;	

		$digide=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaTabla)
		->first();

		$descripcion=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaTabla)
		->first();

		$valor=DB::table('tcatalogo')
		->where('catalogoid','=',$idCaTabla)
		->first();

		$tabladesc = DB::table('tcatalogo as t')
		->leftJoin('tcatalogo as c','t.codtab','=','c.codtab')
		->whereNull('t.digide')
		->orderBy('t.descripcion')
		->lists('t.descripcion','t.codtab');
		$tabladesc = [''=>''] + $tabladesc;	
			
		
		return view('partials/addCatalogoTablas',compact('tabladesc','catalotab','tabladesc','digide','descripcion','valor'));
	}

	public function agregarCatalogoTablas(Request $request){
		DB::beginTransaction();
		$catalogoid = $request->input('catalogoid');


		if(strlen($catalogoid)<=0){
			$catalotab = new Tcatalogo();	
			
 			
		}else{
			$catalotab = Tcatalogo::where('catalogoid','=',$catalogoid)
			->first();			
		}	

		$catalotab->codtab= $request->input('codtab');
		$catalotab->digide= $request->input('digide');
		$catalotab->descripcion= $request->input('descripcion');
		$catalotab->valor= $request->input('valor');
		$catalotab->save();
	
		DB::commit(); 

		return Redirect::route('catalogoTablas');  
	}

	public function eliminarCatalogoTablas($idCatalogo){

		 DB::beginTransaction();
		 $catalogo = Tcatalogo::where('catalogoid','=',$idCatalogo)
		 ->first();
       
        try
        {
        	$catalogo->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('catalogoTablas');  
      
    }


	/* Mantenimiento Moneda Cambio */

	public function listarMonedaCambio(){
		return Datatables::queryBuilder(DB::table('tmonedacambio as t')	
			->leftJoin('tmonedas as m','t.cmoneda','=','m.cmoneda')		
			->leftJoin('tmonedas as m2','t.cmonedavalor','=','m2.cmoneda')		
			->select('t.cmonedacambio','t.cmoneda','m.descripcion as moneda','t.cmonedavalor','m2.descripcion as monedavalor','t.fecha','t.valorcompra','t.valorventa',DB::raw('CONCAT(\'rowrdp_\',t.cmonedacambio)   as "DT_RowId"'))
			)->make(true);
	}

	public function listadoMonedaCambio(){

		$moneda=DB::table('tmonedas as t')			
		->lists('t.descripcion','t.cmoneda');
		$moneda = [''=>''] + $moneda;	

		$monedaVal=DB::table('tmonedas as t')			
		->lists('t.descripcion','t.cmoneda');	
		$monedaVal = [''=>''] + $monedaVal;


		return view('mantenimiento/monedaCambio',compact('moneda','monedaVal'));
		
	}	
	
	public function editarMonedaCambio(Request $request,$idMonedaCambio){

		$monedaCambio=DB::table('tmonedacambio as t')				
		->where('t.cmonedacambio','=',$idMonedaCambio)	
		->first();
		
		$moneda=DB::table('tmonedas as t')	
		->leftJoin('tmonedacambio as c','t.cmoneda','=','c.cmoneda')
		->lists('t.descripcion','t.cmoneda');
		

		$monedaVal=DB::table('tmonedas as t')
		->leftJoin('tmonedacambio as c','t.cmoneda','=','c.cmoneda')	
	    ->lists('t.descripcion','t.cmoneda');
		
		$fecha=DB::table('tmonedacambio as t')				
		->where('t.cmonedacambio','=',$idMonedaCambio)	
		->first();

		$valorcompra=DB::table('tmonedacambio as t')				
		->where('t.cmonedacambio','=',$idMonedaCambio)	
		->first();

		$valorventa=DB::table('tmonedacambio as t')				
		->where('t.cmonedacambio','=',$idMonedaCambio)	
		->first();

		return view('partials/addMonedaCambio',compact('monedaCambio','moneda','monedaVal','fecha','valorventa','valorcompra'));
	}

	public function agregarMonedaCambio(Request $request){
		DB::beginTransaction();

		$cmonedacambio = $request->input('cmonedacambio');	
		$alerta='0';

		if(strlen($cmonedacambio)<=0){
			
			$monedaCambio=DB::table('tmonedacambio')
			->where('cmoneda','=',$request->input('cmoneda'))
			->where('cmonedavalor','=',$request->input('cmonedavalor'))
			->where('fecha','=',$request->input('fecha'))
			->first();		
		    
		    $cmon='';
			$monval='';
			$fec='';			

			if($monedaCambio){					
		    	$cmon=$monedaCambio->cmoneda;
		    	$monval=$monedaCambio->cmonedavalor;
		    	$fec=$monedaCambio->fecha;
			}  

			if($cmon!=$request->input('cmoneda') && $monval!=$request->input('cmonedavalor') && $fec!=$request->input('fecha')){
				$monCambio = new TmonedaCambio();
			}	

			else{
				$alerta='1';
			   return $alerta;  
				
			}
		}

		else{
			$monCambio = Tmonedacambio::where('cmonedacambio','=',$cmonedacambio)
			->first();			
		}	
			
		$monCambio->cmoneda= $request->input('cmoneda');
		$monCambio->cmonedavalor= $request->input('cmonedavalor');
		$monCambio->fecha= $request->input('fecha');
		$monCambio->valorcompra= $request->input('valorcompra');
		$monCambio->valorventa= $request->input('valorventa');
    	$monCambio->save();
	
		DB::commit(); 

		return Redirect::route('listaMonedaCambio');  
	}

	public function eliminarMonedaCambio($idMonedaCambio){

		DB::beginTransaction();
		 $monedaCambio = Tmonedacambio::where('cmonedacambio','=',$idMonedaCambio)
		->first();
       
        try
        {
        	$monedaCambio->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();

        return Redirect::route('listaMonedaCambio');  
      
    }
}