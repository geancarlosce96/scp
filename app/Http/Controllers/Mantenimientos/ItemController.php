<?php

namespace App\Http\Controllers\Mantenimientos;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tconceptogasto;
use DB;

class ItemController extends Controller
{

	public function gasto_registrar(Request $request)
	{
		$g = new Tconceptogasto();
		$g->descripcion = $request->input("descripcion");
		$g->unidad = $request->input("unidad");
		$padre = $request->input("padre");
		$g->cconcepto_parent = ($padre != "0") ? $padre : null;
		$g->tipogasto = "00001";
		$g->save();
		$g->cantidad = 0;
		$g->costou = 0;
		$g->subtotal = 0;
		$g->comentario = "";
		return $g;
	}

}