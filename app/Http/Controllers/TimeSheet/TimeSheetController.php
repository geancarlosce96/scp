<?php

namespace App\Http\Controllers\TimeSheet;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Ttiposnofacturable;
use App\Erp\HelperSIGSupport;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Tpropuestum;
use App\Models\Tpropuestapresentacion;
use App\Models\Tproyecto;
use App\Models\Tproyectoestructuraparticipante;
use App\Models\Tservicio;
use App\Models\Testadopropuestum;
use App\Models\Testadoproyecto;
use App\Models\Tproyectoactividade;
use App\Models\Tproyectoedt;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesfecha;
use App\Models\Tproyectodocumento;
use App\Models\Tentregable;
use App\Models\Ttiposentregable;
use App\Models\Troldespliegue;
use App\Models\Tproyectocronogramadetalle;
use App\Models\Tproyectocronograma;
use App\Models\Tproyectoejecucion;
use App\Models\Tproyectoejecucioncab;
use App\Models\Tdisciplina;
use App\Models\Tflujoaprobacion;
use App\Models\Planificacion;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection as Collection;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Redirect;
use Carbon\Carbon;
use App\Erp\PropuestaSupport;
use App\Erp\ActividadSupport;
use App\Erp\FlowSupport;
use App\Erp\EmpleadoSupport;
use Illuminate\Support\Facades\Input;
use DateTime;
use App\Erp\FlujoLE;

class TimeSheetController extends Controller
{
    //
    public function registroHoras(){
        $nofacturables = Ttiposnofacturable::lists('descripcion','ctiponofacturable')->all();
        $nofacturables = [''=>'Seleccione Motivo No Facturable'] + $nofacturables;     
        $selec = "";     
        return view('timesheet/registroHoras',compact('nofacturables','selec'));
    }
    public function editarTodosRegistrodeHoras(Request $request){

       

        //$proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        //$gte = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersona_gerente)->first();
        $dia_act=date('Ymd');
        $reabrir=false;
        $aNivelReapertura=array();
        $treapertura = DB::table('tcatalogo')
        ->where('codtab','=','00054')
        ->where('digide','=','00001')
        ->first();
        if($treapertura){
            $aNivelReapertura = explode(';',$treapertura->valor);
        }
        $nroFila=0;
        $objAct  = new ActividadSupport();
        /* Obtener la Fecha para sacar la semana de trabajo*/
        
        $fec=$request->input('fecha');
        // dd($fec);
        $fecha = $fec;
        if(substr($fec,2,1)=='/' || substr($fec,2,1)=='-'){    
            $fecha = Carbon::create(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2),0,0,0);

        }else{
            $fecha = Carbon::create(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2),0,0,0);
        }
        $fecha1 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $fecha2 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        $fecp = Carbon::create($fecha1->year,$fecha1->month,$fecha1->day,0,0,0); // Fecha de Proceso
        $fecha_eje = Carbon::create($fecha1->year,$fecha1->month,$fecha1->day,0,0,0); // Fecha para realizar consulta en tproyectoejecucion
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.
        
        $user = Auth::user(); // usuario logueado
        $anio=$fecha->year;

        $historialApro=array(); 
        $historialCom=array();/* 
            [0] => arrayy('cproyectoejecucion'=> , 'fecha' => , 'comentariosPla' => , fplanificado, comentariosEje =>, 'comentarioObs' )

        */
        $cpersona = Auth::user()->cpersona;
        $tproyectopersona = DB::table('tproyectopersona as pp')
        ->leftJoin('tproyecto as tp','pp.cproyecto','=','tp.cproyecto')
        ->where('pp.cpersona','=',$cpersona)
        // ->where('tp.cestadoproyecto','=','001')
        ->wherein('tp.cestadoproyecto',['001','004','005'])
        ->select('pp.cproyecto')
        ->get();
        $aProyectos = array();
        foreach($tproyectopersona as $o){
            $aProyectos[count($aProyectos)]=$o->cproyecto;
        }

        $tarea = DB::table('tpersonadatosempleado as e')           
        ->where('cpersona','=',$cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')      
        ->first();
        $carea=0;
        
        
        if($tarea){
            $carea = $tarea->carea;
            
        }

        $cargo=DB::table('tpersonadatosempleado as per')
        ->leftjoin('tdisciplinaareas as dis','dis.carea','=','per.carea')
        ->where('per.cpersona','=',$cpersona)
        ->first();

        $percargo = null;
        $disciplina = null;

        if ($cargo) {
            $percargo = $cargo->ccargo;
            $disciplina = $cargo->cdisciplina;
        }

        // dd($disciplina);

        $tclientes = DB::table('tproyecto as pry')
        ->join('tpersona as per','pry.cpersonacliente','=','per.cpersona')
        ->join('tpersonajuridicainformacionbasica as jur','jur.cpersona','=','per.cpersona')
        ->where('jur.escliente','=','1')
        ->orderBy('pry.tipoproy','ASC')
        ->orderBy('per.nombre','ASC');
        // ->whereIn('pry.cproyecto',$aProyectos);
        if ($percargo != 162 || $percargo != 153 || $disciplina != 26 || $disciplina != 27 || $disciplina != 11 || $disciplina != 19 ) {
        $tclientes=$tclientes->whereIn('pry.cproyecto',$aProyectos);
        }
        $tclientes=$tclientes
        ->select('per.*','pry.tipoproy')
        ->distinct()
        ->get();
        $clientes = array();
        foreach($tclientes as $cli){
            $clientes[$cli->cpersona]=$cli->nombre;
        }
        /* Estructura Treeview de Actividades*/
        $actParent= DB::table('tproyectoactividades as pa')
        ->whereNull('cproyectoactividades_parent')
        ->get();
        $estructura="";

        
        foreach ($actParent as $act) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$act->cproyectoactividades. "',";
            $estructura=$estructura ."label: '".$act->descripcionactividad ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            $estructura=$estructura ."}\n\r";
        }
        
        /* Fin de estructura de Treeview Actividades*/        

        $estructuraAdm="";
        $estructuraTodosAdm="";
        /* Estructura Treeview de Actividades Todos Adm*/
        $actParent= DB::table('tactividad as act')
        ->where('act.tipoactividad','=','00002')
        ->whereNull('cactividad_parent')
        ->get();
        $estructuraTodosAdm="";
        $estructuraTodosAdm2="";

        
        foreach ($actParent as $act) {
            // if(!empty($estructuraTodosAdm)){
            //     $estructuraTodosAdm= $estructuraTodosAdm .",";
            // }
            // //$actividades[count($actividades)]=$act;
            // $estructuraTodosAdm=$estructuraTodosAdm ."{\n\r";
            // $estructuraTodosAdm=$estructuraTodosAdm ."item: {\n\r";
            // $estructuraTodosAdm=$estructuraTodosAdm ."id: '".$act->cactividad. "',";
            // $estructuraTodosAdm=$estructuraTodosAdm ."label: '".$act->descripcion ."',";
            // $estructuraTodosAdm=$estructuraTodosAdm ."checked: false ";
            // $estructuraTodosAdm=$estructuraTodosAdm ."}\n\r";
            
            // $est=$objAct->listarTreeviewTipo($act,'00002');
            // // dd($est, $activi);
            // if(!empty($est)){
            //     $estructuraTodosAdm=$estructuraTodosAdm .",children: [ ";
            //     $estructuraTodosAdm = $estructuraTodosAdm . $est;
            //     $estructuraTodosAdm=$estructuraTodosAdm ."]";
            // }
            
            // $estructuraTodosAdm=$estructuraTodosAdm ."}\n\r";

            
            if (!empty($estructuraTodosAdm2)) {
                $estructuraTodosAdm2 = $estructuraTodosAdm2 . ",";
            }
            $estructuraTodosAdm2 = $estructuraTodosAdm2 . "\n\r";
            $estructuraTodosAdm2 = $estructuraTodosAdm2 . "{\n\r";
            $estructuraTodosAdm2 = $estructuraTodosAdm2 . "text: '" . $act->descripcion . "',";
            $estructuraTodosAdm2 = $estructuraTodosAdm2 . "id: '" . $act->cactividad . "',";
            $estructuraTodosAdm2 = $estructuraTodosAdm2 . "state : {opened : true }";
            $estructuraTodosAdm2 = $estructuraTodosAdm2 . ",icon : 'glyphicon glyphicon-pushpin'";
            // $estructuraTodosAdm2 = $estructuraTodosAdm2 . "}\n\r";

            $activi = $objAct->adtAdmin($act, '00002');
            if (!empty($activi)) {
                $estructuraTodosAdm2 = $estructuraTodosAdm2 . ",children: [ ";
                $estructuraTodosAdm2 = $estructuraTodosAdm2 . $activi;
                $estructuraTodosAdm2 = $estructuraTodosAdm2 . "]";
            }
            $estructuraTodosAdm2 = $estructuraTodosAdm2 . "}\n\r";

        }
        
        // dd($estructuraTodosAdm,$estructuraTodosAdm2);
        /* Fin de estructura de Treeview Actividades Todos Adm*/            

        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array(""=>"");
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        /* mostrar programacion de la Fecha*/
        $alinea = array();
        $sumFF=0;
        $sumNF=0;
        $sumAN=0; 
        $w=0;

        //$filas=0;

        /* Tareas Administrativas*/
        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->leftJoin('tactividad as a ','cab.cactividad','=','a.cactividad');
        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->where('cab.cpersona_ejecuta','=',$user->cpersona)                 
        ->where('cab.tipo','=','3')
        ->whereNotNull('eje.cactividad')
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
        $tactividadesk=$tactividadesk->select('cab.cproyectoejecucioncab','cab.cpersona_ejecuta','cab.tipo')
        ->addSelect('cab.cactividad','cab.cactividad','a.codigo','a.descripcion','cab.ctiponofacturable')
       ->orderBy('a.codigo','ASC')
        ->distinct()
        ->get();
    
        foreach($tactividadesk as $k){
            $nroFila++;
            $w++;
            $pla= array();
            $pla['item'] = $w;
            $pla['codigopry']= "";
            $pla['codigoactividad']='';            
            $pla['cproyectoejecucioncab']=$k->cproyectoejecucioncab;
            $pla['personanombre']= '';
            $pla['cpersona_ejecuta'] = $k->cpersona_ejecuta;
            $pla['unidadminera']= '';
            $pla['nombreproy']='';
            $pla['cproyectoactividades'] = '';
            $pla['cactividad']=$k->cactividad;
            $pla['cproyecto'] = '';
            $pla['cestructuraproyecto']="";    
            $pla['entregable'] ="";
            $pla['responsable'] ='';     
            $pla['descripcionactividad'] = $k->descripcion;  
            $pla['ctiponofacturable']= $k->ctiponofacturable;
            $pla['horasPla'] = 0;
            $pla['ctiponofacturable'] = $k->ctiponofacturable;            
            $pla['horaseje'] = 0;
            $pla['porav'] = 0;
            $pla['porav_costo']=0;
            $pla['tipo']  = $k->tipo;                  
            $pla['gp']  = '';  
            $pla['tot']=0;
            $pla['mensajehoras']='';  
            $pla['mensajecosto']='';

            $categoriapry = $this->verCategoriaEnProyecto($k->cpersona_ejecuta,'Administrativa');
            $pla['categoriaproy'] = $categoriapry;
            


            $actividadesp = DB::table('tproyectoejecucion as eje')
            ->where('eje.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
            ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
            $actividadesp=$actividadesp->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'),DB::raw('to_char(eje.fplanificado,\'YYYY-MM-DD\' ) as fec'))
            ->orderBy('fpla','ASC')
            ->get();
            //dd($actividadesp);
            foreach($actividadesp as $ej){
                /*
                Obtener Historial de Aprobaciones
                */
                $taprob = DB::table('tflujoaprobacion as apro')
                ->leftJoin('tpersona as per','apro.cpersona_aprobado','=','per.cpersona')
                ->where('apro.cproyectoejecucion','=',$ej->cproyectoejecucion)
                ->select('apro.*','per.nombre as emp')
                ->get();
                foreach($taprob as $aprob){
                    $his['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $his['fecha']=$aprob->fregistro;
                    $his['ccondicionoperativa']=$aprob->ccondicionoperativa_aprobado;
                    $his['ccondicionoperativa_sig']=$aprob->ccondicionoperativa_siguiente;
                    $his['aprobado']=$aprob->emp;
                    $his['actividad']=$k->descripcion;
                    $his['horas']=$ej->horasejecutadas;                    
                    $historialApro[count($historialApro)] = $his;
                }
                /*  Fin Historial de aprobaciones*/

                /*
                Obtener Historial de Comentarios
                */
                if( strlen($ej->obsplanificada)>0 || strlen($ej->obsejecutadas)>0 || strlen($ej->obsobservadas)>0 ){
                    $hic['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $hic['actividad']=$k->descripcion;
                    $hic['fecha']=$ej->fplanificado;
                    $hic['comentariosPla']=$ej->obsplanificada;
                    $hic['comentariosEje']=$ej->obsejecutadas;
                    $hic['comentariosObs']=$ej->obsobservadas;
                    $hic['horas']=$ej->horasejecutadas;
                    $historialCom[count($historialCom)] = $hic;

                }

                /*  Fin Historial de comentarios*/ 
                
                $pla[$ej->fpla]['cproyectoejecucion']=$ej->cproyectoejecucion;
                $pla[$ej->fpla]['estado'] = $ej->estado;
                $pla[$ej->fpla]['horasplanificadas'] = (is_null($ej->horasplanificadas)?'':$ej->horasplanificadas);
                $pla[$ej->fpla]['obsplanificadas'] = $ej->obsplanificada;
                $pla[$ej->fpla]['horasejecutadas'] = (is_null($ej->horasejecutadas)?'':$ej->horasejecutadas);
                $pla[$ej->fpla]['obsejecutadas'] = $ej->obsejecutadas;
                $pla[$ej->fpla]['horasaprobadas'] = (is_null($ej->horasaprobadas)?'':$ej->horasaprobadas);
                $pla[$ej->fpla]['obsaprobadas'] = $ej->obsaprobadas;
                $pla[$ej->fpla]['horasobservadas'] = (is_null($ej->horasobservadas)?'':$ej->horasobservadas);
                $pla[$ej->fpla]['obsobservadas'] = $ej->obsobservadas;
                $pla[$ej->fpla]['cpersona_ejecuta'] = $ej->cpersona_ejecuta;
                $pla[$ej->fpla]['fejecuta'] = $ej->fejecuta;
                $pla[$ej->fpla]['fregistro'] =  $ej->fregistro;
                $pla[$ej->fpla]['faprobacion'] = $ej->faprobacion;
                $pla[$ej->fpla]['ordenaprobacion'] = $ej->ordenaprobacion;
                $pla[$ej->fpla]['crol_poraprobar'] = $ej->crol_poraprobar;
                $pla[$ej->fpla]['cpersona'] = $ej->cpersona;
                $pla[$ej->fpla]['fplanificado'] = $ej->fplanificado;
                $pla[$ej->fpla]['cestructuraproyecto'] = $ej->cestructuraproyecto; 
                $resReab = array_search($ej->ccondicionoperativa,$aNivelReapertura);
                
                if($resReab===FALSE){
                }else{
                    
                    $reabrir=true;
                }
                switch ($ej->estado) {
                            case '1':
                                # code...
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                            
                                }
                                break;
                            case '2':
                            case '3':
                            case '4':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;
                            
                            
                            case '5':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;                      
                        
                }                                
                
            }            

            $alinea[count($alinea)]=$pla;




        }
        /* Fin Tareas Administrativas*/
        $objEmp = new EmpleadoSupport();
        $nroHoras = $objEmp->getCargabilidadPlani($cpersona,$fecha1,$fecha2);
        $nroHorasCar = $objEmp->getCargabilidadEje($cpersona,$fecha1,$fecha2);        

        /* tareas de proyecto */
        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->leftJoin('tproyectoactividades as pryact',function($join){
            $join->on('eje.cproyectoactividades','=','pryact.cproyectoactividades');
            $join->on('eje.cproyecto','=','pryact.cproyecto');
        })        
        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
        ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')
        ->leftJoin('tpersona as gp','pry.cpersona_gerente','=','gp.cpersona')
        ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')
        ->where('cab.cpersona_ejecuta','=',$user->cpersona);       
        //->where('pry.cestadoproyecto','=','001');

        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2','3'])
        ->whereNull('eje.cactividad')
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
        $tactividadesk=$tactividadesk->select('cab.cproyectoejecucioncab','pry.nombre as nombreproy','cab.cpersona_ejecuta','pry.tipoproy')
        ->addSelect('cab.tipo','cab.cproyectoactividades','pry.cproyecto','cab.cactividad','pryact.descripcionactividad')
        ->addSelect('tpc.abreviatura as cliente','tum.nombre as uminera','cab.ctiponofacturable')
        ->addSelect('gp.cpersona as codgp','gp.abreviatura as gp','pry.codigo as codigopry','pryact.codigoactvidad')
        ->distinct()
        ->orderBy('pry.codigo','ASC')
        ->orderBy('pryact.codigoactvidad','ASC')
        ->get();        
        foreach($tactividadesk as $k){
            

            $nroFila++;
            $w++;
            $pla= array();
            $pla['item'] = $w;
            
            $pla['cproyectoejecucioncab']=$k->cproyectoejecucioncab;
            $pla['codigoactividad']=$k->codigoactvidad;
            $pla['personanombre']= $k->cliente;
            $pla['codigopry']= $k->codigopry;
            $pla['cpersona_ejecuta'] = $k->cpersona_ejecuta;
            $pla['unidadminera']= $k->uminera;
            $pla['nombreproy']=$k->nombreproy;
            $pla['cproyectoactividades'] = $k->cproyectoactividades;
            $pla['cactividad']=$k->cactividad;
            $pla['cproyecto'] = $k->cproyecto;
            $pla['cestructuraproyecto']="";    
            $pla['entregable'] ="";
            $pla['responsable'] ='';     
            $pla['descripcionactividad'] = $k->descripcionactividad;      
            $pla['horasPla'] = 0;
            $pla['ctiponofacturable'] = $k->ctiponofacturable;
            $pla['gp'] = $k->gp;      
            
            $cod_proyecto = ($k->tipoproy=='00002')?$k->cproyecto:'Administrativa';
            
            $categoriapry = $this->verCategoriaEnProyecto($k->cpersona_ejecuta,$cod_proyecto);

            $pla['categoriaproy'] = $categoriapry;


            if($cpersona==$k->codgp){

                $pla['horaseje'] = $objAct->sumHorasEjecutadasProy($k->cproyectoactividades);
                $tot = $objAct->sumHorasPresupuestadasProy($k->cproyectoactividades);
                $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100) );

                $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($k->cproyectoactividades,$k->cproyecto);
                $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($k->cproyectoactividades,$k->cproyecto);

                /* Inicio Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal) */

                    if($tot_costo_presup==0|| is_null($tot_costo_presup)){
                       // dd($k->cproyecto,$k->cproyectoactividades,$k->cproyectoejecucioncab);

                         $porcentajeCosto=-1;
                    }

                    else{
                        $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    }

                    /* Fin Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal)*/

               // $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;

                $pla['porav_costo']=round($porcentajeCosto);

                

                if($porcentajeCosto==-1){
                    $pla['mensajehoras']='Sin horas';
                    $pla['mensajecosto']='Sin monto';

                }
                else{
                    $pla['mensajecosto']=$pla['porav_costo'].'%';
                    $pla['mensajehoras']=$pla['porav'].'%';  
                }
                

                 

            }

            else{


                $pla['horaseje'] = $objAct->sumHorasEjecutadasProyArea($k->cproyectoactividades,$carea);
                $tot = $objAct->sumHorasPresupuestadasProyArea($k->cproyectoactividades,$carea);
                $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100) );

                $tot_costo_presup=$objAct->sumCostoPresupuestadoProyArea($k->cproyectoactividades,$k->cproyecto,$carea);
                $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProyArea($k->cproyectoactividades,$k->cproyecto,$carea);

               // dd($tot_costo_ejecutado,$tot_costo_presup, $pla['porav']);
                //dd($tot_costo_presup,$tot_costo_ejecutado);


                if($tot_costo_presup==0|| is_null($tot_costo_presup)){

                    $pla['horaseje'] = $objAct->sumHorasEjecutadasProy($k->cproyectoactividades);
                    $tot = $objAct->sumHorasPresupuestadasProy($k->cproyectoactividades);
                    $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100) );

                    $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($k->cproyectoactividades,$k->cproyecto);
                    $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($k->cproyectoactividades,$k->cproyecto);

                    /* Inicio Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal) */

                    if($tot_costo_presup==0|| is_null($tot_costo_presup)){
                       // dd($k->cproyecto,$k->cproyectoactividades,$k->cproyectoejecucioncab);

                         $porcentajeCosto=0;
                    }

                    else{
                        $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    }

                    /* Fin Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal)*/
                    

                    //$porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    $pla['porav_costo']=round($porcentajeCosto);
                   

                    $pla['mensajehoras']='Sin horas';  
                    $pla['mensajecosto']='Sin monto';

                    //$porcentajeCosto=100;

                }
                else{

                    $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    $pla['porav_costo']=round($porcentajeCosto);     

                    $pla['mensajehoras']=$pla['porav'].'%';  
                    $pla['mensajecosto']=$pla['porav_costo'].'%';                  

                }
              
                
            }

            
            $pla['tot']= $tot->suma;
            $pla['tipo']  = $k->tipo; 

            
            
            $actividadesp = DB::table('tproyectoejecucion as eje')
            ->where('eje.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
            ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
            $actividadesp=$actividadesp->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'))
            ->addSelect(DB::raw('to_char(eje.fplanificado,\'YYYY-MM-DD\' ) as fec'))
            ->orderBy('fpla','ASC')
            ->get();
            //dd($actividadesp);

            foreach($actividadesp as $ej){
                /*
                Obtener Historial de Aprobaciones
                */
                $taprob = DB::table('tflujoaprobacion as apro')
                ->leftJoin('tpersona as per','apro.cpersona_aprobado','=','per.cpersona')
                ->leftJoin('tproyectoejecucion as pe','apro.cproyectoejecucion','=','pe.cproyectoejecucion')
                ->leftJoin('tproyecto as p','pe.cproyecto','=','p.cproyecto')
                ->leftJoin('tunidadminera as tu','p.cunidadminera','=','tu.cunidadminera')
                ->leftJoin('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                ->where('apro.cproyectoejecucion','=',$ej->cproyectoejecucion)
                ->select('apro.*','per.nombre as emp','p.codigo as codproy','p.nombre as nomproy','tu.nombre as uminera','pa.codigoactvidad')
                ->get();


                foreach($taprob as $aprob){
               

                    $his['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $his['fecha']=$aprob->fregistro;
                    $his['ccondicionoperativa']=$aprob->ccondicionoperativa_aprobado;
                    $his['ccondicionoperativa_sig']=$aprob->ccondicionoperativa_siguiente;
                    $his['aprobado']=$aprob->emp;

                    if(is_null($aprob->codproy)||strlen($aprob->codproy)<=0){
                        $his['actividad']=$k->descripcionactividad;   
                    }
                    else{
                        $his['actividad']=$aprob->codproy.'//'.$k->uminera.'/'.$aprob->nomproy.'/'.$k->codigoactvidad.'/'.$k->descripcionactividad;
                    }
                   // $his['actividad']=$aprob->codproy.'/'.$k->descripcionactividad;;
                    $his['horas']=$ej->horasejecutadas;                    
                    $historialApro[count($historialApro)] = $his;
                }
                /*  Fin Historial de aprobaciones*/

                /*
                Obtener Historial de Comentarios
                */
                if( strlen($ej->obsplanificada)>0 || strlen($ej->obsejecutadas)>0 || strlen($ej->obsobservadas)>0 ){
                    $hic['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $hic['actividad']=$pla['codigopry'].'//'.$pla['unidadminera'] ."/".$pla['nombreproy'].'/'.$pla['codigoactividad'] . "/" .$k->descripcionactividad;
                    $hic['horas']=$ej->horasejecutadas;
                    $hic['fecha']=$ej->fplanificado;
                    $hic['comentariosPla']=$ej->obsplanificada;
                    $hic['comentariosEje']=$ej->obsejecutadas;
                    $hic['comentariosObs']=$ej->obsobservadas;
                    $historialCom[count($historialCom)] = $hic;
                }
                /*  Fin Historial de comentarios*/      
                
                

                $pla[$ej->fpla]['cproyectoejecucion']=$ej->cproyectoejecucion;
                $pla[$ej->fpla]['estado'] = $ej->estado;
                $pla[$ej->fpla]['horasplanificadas'] = (is_null($ej->horasplanificadas)?'':$ej->horasplanificadas);
                $pla[$ej->fpla]['obsplanificadas'] = $ej->obsplanificada;
                $pla[$ej->fpla]['horasejecutadas'] = (is_null($ej->horasejecutadas)?'':$ej->horasejecutadas);
                $pla[$ej->fpla]['obsejecutadas'] = $ej->obsejecutadas;
                $pla[$ej->fpla]['horasaprobadas'] = (is_null($ej->horasaprobadas)?'':$ej->horasaprobadas);
                $pla[$ej->fpla]['obsaprobadas'] = $ej->obsaprobadas;
                $pla[$ej->fpla]['horasobservadas'] = (is_null($ej->horasobservadas)?'':$ej->horasobservadas);
                $pla[$ej->fpla]['obsobservadas'] = $ej->obsobservadas;
                $pla[$ej->fpla]['cpersona_ejecuta'] = $ej->cpersona_ejecuta;
                $pla[$ej->fpla]['fejecuta'] = $ej->fejecuta;
                $pla[$ej->fpla]['fregistro'] =  $ej->fregistro;
                $pla[$ej->fpla]['faprobacion'] = $ej->faprobacion;
                $pla[$ej->fpla]['ordenaprobacion'] = $ej->ordenaprobacion;
                $pla[$ej->fpla]['crol_poraprobar'] = $ej->crol_poraprobar;
                $pla[$ej->fpla]['cpersona'] = $ej->cpersona;
                $pla[$ej->fpla]['fplanificado'] = $ej->fplanificado;
                $pla[$ej->fpla]['cestructuraproyecto'] = $ej->cestructuraproyecto; 
                $resReab = array_search($ej->ccondicionoperativa,$aNivelReapertura);
                
                if($resReab===FALSE){
                }else{
                    
                    $reabrir=true;
                }
                switch ($ej->estado) {
                            case '1':
                                # code...
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                            
                                }
                                break;
                            case '2':
                            case '3':
                            case '4':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;
                            
                            
                            case '5':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;                      
                        
                }                                
                
            }            

            $alinea[count($alinea)]=$pla;


        }
        // dd($alinea);
        /* fin tareas de proyecto */
      
//    dd($fecha1,$fecha2);
        
        $tipocontrato= DB::table('ttipocontrato as tc')
        ->leftJoin('tpersonadatosempleado as tp','tc.ctipocontrato','=','tp.ctipocontrato')
        ->where('tp.cpersona','=',$cpersona)
        ->select('tc.*',DB::raw("extract(week from tp.fingreso::date) as semana_ingreso"),DB::raw("extract(year from tp.fingreso::date) as anio_ingreso"))
        ->first();

        $semana_anio_ingreso='false';

        if ($tipocontrato) {

        if ($anio == $tipocontrato->anio_ingreso && $semana == $tipocontrato->semana_ingreso) {
        $semana_anio_ingreso = 'true';
        }
        else{ $semana_anio_ingreso='false'; }
        }
        
        // dd($semana_anio_ingreso);

        return view('timesheet/registroHoras',compact('reabrir','nofacturables','clientes','estructuraTodosAdm','estructuraTodosAdm2','estructuraAdm','nroFila','estructura','fecha','semana','fecha1','fecha2','sumNF','sumFF','sumAN','fecp','alinea','historialApro','historialCom','dia_act','nroHoras','nroHorasCar','anio','tipocontrato','semana_anio_ingreso'));
    }
    public function loadUmineraxCliente(Request $request){

        $cpersona = Auth::user()->cpersona;

        $tproyectos = DB::table('tproyecto as pry')
        ->join('tproyectopersona as tp','pry.cproyecto','=','tp.cproyecto')
       // ->where('pry.cunidadminera','=',$request->input('umineraT'))
        ->where('tp.cpersona','=',$cpersona)
        // ->where('pry.cestadoproyecto','=','001')
        ->wherein('pry.cestadoproyecto',['001','004','005'])
        ->orderBy('codigo','ASC')
        ->select('tp.cproyecto','pry.codigo','pry.nombre')
        ->distinct()
        ->get();
        $proyectos = array();
        foreach($tproyectos as $p){
            $py['cproyecto']=$p->cproyecto;
            $proyectos[count($proyectos)] = $py;
        }

        $cargo=DB::table('tpersonadatosempleado as per')
        ->leftjoin('tdisciplinaareas as dis','dis.carea','=','per.carea')
        ->where('per.cpersona','=',$cpersona)
        ->first();

        $percargo = null;
        $disciplina = null;

        if ($cargo) {
            $percargo = $cargo->ccargo;
            $disciplina = $cargo->cdisciplina;
        }

        $tumineraT = DB::table('tunidadminera as um')
        ->leftjoin('tproyecto as pry','um.cunidadminera','=','pry.cunidadminera')
        ->where('um.cpersona','=',$request->input('clientesT'));

        if ($percargo != 162 || $percargo != 153 || $disciplina != 26 || $disciplina != 27 || $disciplina != 11 || $disciplina != 19) {
        $tumineraT=$tumineraT->whereIn('pry.cproyecto',$proyectos);
        }
        $tumineraT=$tumineraT

        ->select('um.nombre','um.cunidadminera')
        ->get();
        foreach($tumineraT as $um){
            $umineraT[$um->cunidadminera]=$um->nombre;
        }
       
        return view('timesheet.divUmineraT',compact('umineraT'));
    }
    public function loadUmineraxClienteAdm(Request $request){
        
        

        $tumineraT = DB::table('tunidadminera as um')       
        ->where('um.cpersona','=',$request->input('clientesTAdm'))       
        ->get();
        $umineraT = array(''=>'');
        foreach($tumineraT as $um){
            $umineraT[$um->cunidadminera]=$um->nombre;
        }

        return view('timesheet.divUmineraTodosAdm',compact('umineraT'));
    }    
    public function loadProyxCliente(Request $request){
        $cpersona = Auth::user()->cpersona;

        $cargo=DB::table('tpersonadatosempleado as per')
        ->leftjoin('tdisciplinaareas as dis','dis.carea','=','per.carea')
        ->where('per.cpersona','=',$cpersona)
        ->first();

        $percargo = null;
        $disciplina = null;

        if ($cargo) {
            $percargo = $cargo->ccargo;
            $disciplina = $cargo->cdisciplina;
        }
        
        if ($percargo == 162 || $percargo == 153 || $disciplina == 26 || $disciplina == 27 || $disciplina == 11 || $disciplina == 19) {
        $tproyectos = DB::table('tproyecto as pry')
        ->where('pry.cunidadminera','=',$request->input('umineraT'))
        // ->where('pry.cestadoproyecto','=','001')
        ->wherein('pry.cestadoproyecto',['001','004','005'])
        ->orderBy('codigo','ASC')
        ->select('pry.cproyecto','pry.codigo','pry.nombre')
        ->distinct()
        ->get();
        }else{

        $tproyectos = DB::table('tproyecto as pry')
        ->join('tproyectopersona as tp','pry.cproyecto','=','tp.cproyecto')
        ->where('tp.cpersona','=',$cpersona)
        ->where('pry.cunidadminera','=',$request->input('umineraT'))
        // ->where('pry.cestadoproyecto','=','001')
        ->wherein('pry.cestadoproyecto',['001','004','005'])
        ->orderBy('codigo','ASC')
        ->select('tp.cproyecto','pry.codigo','pry.nombre')
        ->distinct()
        ->get();
        }
        $proyectos = array();
        foreach($tproyectos as $p){
            $py['cproyecto']=$p->cproyecto;
            $py['codigo']=$p->codigo;
            $py['nombre']=$p->nombre;
            $proyectos[count($proyectos)] = $py;
        }
       // dd($tproyectos);
        return view('timesheet.divProyTodosT',compact('proyectos'));        
    }
    public function loadProyxClienteAdm(Request $request){
        $tproyectos = DB::table('tproyecto as pry')
        ->where('pry.cunidadminera','=',$request->input('umineraTAdm'))
        ->get();
        $proyectos = array();
        foreach($tproyectos as $p){
            $py['cproyecto']=$p->cproyecto;
            $py['nombre']=$p->nombre;
            $proyectos[count($proyectos)] = $py;
        }
        return view('timesheet.divProyTodosAdm',compact('proyectos'));        
    }    
    public function buscarPadre($a){
        $tt = DB::table('tproyectoactividades as pryact')
        ->where('cproyectoactividades','=',$a)
        ->whereNotNull('cproyectoactividades_parent')
        ->select('cproyectoactividades_parent')
        ->first();
        if($tt){
            $a= $tt->cproyectoactividades_parent;
            $a = $this->buscarPadre($a);
        }
        return $a;

    }
    public function verTareaProyectoTimeSheet(Request $request){

        $cproyecto = $request->input('pryTodos');
        $objAct  = new ActividadSupport();
        $flgAct=false;
        $aActividades = array();
        $aActHijos = array();
        $aActPadres= array();
        $cpersona = Auth::user()->cpersona;

        $tproyectopersona = DB::table('tproyectoactividadeshabilitacion as ho')
        ->join('tproyectopersona as pp','ho.cpersona','=','pp.cpersona')
        ->where('pp.cproyecto','=',$cproyecto)
        ->where('pp.cpersona','=',$cpersona)
        ->where('ho.cproyecto','=',$cproyecto)
        ->where('ho.activo','=','1')
        ->select('ho.cproyectoactividades')
        ->distinct()
        ->get();

        // dd($cproyecto);
        foreach($tproyectopersona as $pho){
            $aActHijos[count($aActHijos)]=$pho->cproyectoactividades;
        }
        if(count($aActHijos)<=0){
            $flgAct=true;
        }

        $esGP=DB::table('tproyecto as pry')    
        ->where('pry.cproyecto','=',$cproyecto)
        ->first();

        if($esGP){
            if($esGP->cpersona_gerente==$cpersona){
                // dd("gp");
                $aActHijos=$objAct->getTareasProyectoGP($cproyecto);
            }       
        }

        $cargo=DB::table('tpersonadatosempleado as per')
        ->leftjoin('tdisciplinaareas as dis','dis.carea','=','per.carea')
        ->where('per.cpersona','=',$cpersona)
        ->first();

        $percargo = null;
        $disciplina = null;

        if ($cargo) {
            $percargo = $cargo->ccargo;
            $disciplina = $cargo->cdisciplina;
        }



        // if($cargo){
            // if($per_cargo->ccargo== 162 || $per_cargo->ccargo== 153 || $per_cargo->ccargo== 157 || $per_cargo->ccargo== 158){
            if ($percargo == 162 || $percargo == 153 || $disciplina == 26 || $disciplina == 27 || $disciplina == 11 || $disciplina == 19) {
                // dd("gg");
                $aActHijos=$objAct->getTareasProyectoGP($cproyecto);
            } 
        // }
        // dd($aActHijos);
        /*if($flgAct){
            //Si no tiene habilitacion obtiene todas las tareas del proyecto

            $aActHijos=$objAct->getTareasProyecto($cproyecto,
            Auth::user()->cpersona
            );
        }*/
        $tactividades = DB::table('tproyectoactividades as pryact')
        //->join('tproyectoactividades as pryact','act.cactividad','=','pryact.cactividad')
        ->whereIn('pryact.cproyectoactividades',$aActHijos)
        ->select('pryact.cproyectoactividades_parent')
        ->distinct()
        ->get();
        foreach($tactividades as $act){
            $aActividades[count($aActividades)] = $act->cproyectoactividades_parent;
        }
        
        foreach($aActividades as $t){
            $tt = DB::table('tproyectoactividades as pryact')
            ->where('cproyectoactividades','=',$t)
            ->whereNotNull('cproyectoactividades_parent')
            ->select('cproyectoactividades_parent')
            ->first();
            if($tt){
                $tx = $this->buscarPadre($tt->cproyectoactividades_parent);
            }else{
                $tx=$t;
            }
            $res = array_search($tx,$aActPadres,false);
            if($res===FALSE){
                $aActPadres[count($aActPadres)]=$tx;
            }
        }
        /* complementar hijos sin padres */
        $tpadres = DB::table('tproyectoactividades as pryact')
        ->whereIn('pryact.cproyectoactividades',$aActHijos)
        ->whereNull('pryact.cproyectoactividades_parent')
        ->select('pryact.cproyectoactividades')
        ->distinct()
        ->get();
        foreach($tpadres as $act){
            $aActPadres[count($aActPadres)] = $act->cproyectoactividades;
        }
        /* fin complementar hijos sin padres */

        $actParent= DB::table('tproyectoactividades as pa')
        //->join('tproyectoactividades as pa','act.cactividad','=','pa.cactividad')
        ->where('pa.cproyecto','=',$cproyecto)
        ->whereIn('pa.cproyectoactividades',$aActPadres)
        ->whereNull('cproyectoactividades_parent')
        ->orderBy('codigoactvidad','ASC')
        ->get();
        
        $estructura="";
        //dd($aActPadres);
        //dd($actParent);
        foreach ($actParent as $act) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$act->cproyectoactividades. "',";
            $estructura=$estructura ."label: '".$act->descripcionactividad ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$objAct->listarTreeviewProy($act,$cproyecto,$aActHijos);
            //dd($est);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }       
        //dd($estructura);
        return $estructura;
    }    
    public function editarRegistroHoras(Request $request,$idProyecto,$fec){

        
        //N0 usar solo todos
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        $selec = $request->input('consulta');
        $gte = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersona_gerente)->first();
        $nroFila=0;
        $objAct  = new ActividadSupport();
        /* Obtener la Fecha para sacar la semana de trabajo*/
        $fecha = $fec;
        if(substr($fec,2,1)=='-' || substr($fec,2,1)=='/'){    
            $fecha = Carbon::createFromDate(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2));
        }else{
            $fecha = Carbon::createFromDate(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2));
        }
        //dd($fecha);
        $fecha1 = Carbon::createFromDate($fecha->year,$fecha->month,$fecha->day);
        $fecha2 = Carbon::createFromDate($fecha->year,$fecha->month,$fecha->day);
        $semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        //dd($fecha1->startOfWeek());        
        $fecp = Carbon::createFromDate($fecha1->year,$fecha1->month,$fecha1->day); // Fecha de Proceso
        $fecha_eje = Carbon::createFromDate($fecha1->year,$fecha1->month,$fecha1->day); // Fecha para realizar consulta en tproyectoejecucion
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.
        //dd($fecha1);
        $user = Auth::user(); // usuario logueado



        /* Estructura Treeview de Actividades*/
        $actParent= DB::table('tactividad as act')
        ->join('tproyectoactividades as pa','act.cactividad','=','pa.cactividad')
        ->where('pa.cproyecto','=',$idProyecto)
        ->where('act.tipoactividad','=','00001')
        ->whereNull('cactividad_parent')
        ->get();
        $estructura="";

        
        foreach ($actParent as $act) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$act->cactividad. "',";
            $estructura=$estructura ."label: '".$act->descripcion ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$objAct->listarTreeviewProy($act,$idProyecto);
            //dd($est);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }
        
        /* Fin de estructura de Treeview Actividades*/ 

        /* Estructura Treeview de Actividades Administrativas*/
        $actParent= DB::table('tactividad as act')
        ->join('tproyectoactividades as pa','act.cactividad','=','pa.cactividad')
        ->where('pa.cproyecto','=',$idProyecto)
        ->where('act.tipoactividad','=','00002')
        ->whereNull('cactividad_parent')
        ->get();
        $estructuraAdm="";

        
        foreach ($actParent as $act) {
            if(!empty($estructuraAdm)){
                $estructuraAdm= $estructuraAdm .",";
            }
            //$actividades[count($actividades)]=$act;
            $estructuraAdm=$estructuraAdm ."{\n\r";
            $estructuraAdm=$estructuraAdm ."item: {\n\r";
            $estructuraAdm=$estructuraAdm ."id: '".$act->cactividad. "',";
            $estructuraAdm=$estructuraAdm ."label: '".$act->descripcion ."',";
            
            $estructuraAdm=$estructuraAdm ."checked: false ";
            $estructuraAdm=$estructuraAdm ."}\n\r";

            

            
            $est=$objAct->listarTreeviewProyAdm($act,$idProyecto);
            //dd($est);
            if(!empty($est)){
                $estructuraAdm=$estructuraAdm .",children: [ ";
                $estructuraAdm = $estructuraAdm . $est;
                $estructuraAdm=$estructuraAdm ."]";
            }

            $estructuraAdm=$estructuraAdm ."}\n\r";
        }
        
        /* Fin de estructura de Treeview Actividades Administrativas*/         


        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array();
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        /* mostrar programacion de la Fecha*/
        $alinea = array();
        $sumFF=0;
        $sumNF=0;
        $sumAN=0; 

        $pryactividad= DB::table('tproyectoactividades as pact')
        ->where('pact.cproyecto','=',$proyecto->cproyecto)
        ->select('pact.cproyectoactividades','pact.descripcionactividad')
        ->distinct()
        ->orderBy('pact.descripcionactividad')
        ->get();
        $filas=0;
        $historialApro=array(); /* 
            [0] => arrayy('cproyectoejecucion'=> , 'fecha' => , 'ccondicionoperativa' => , 'aprobado','condicionoperativa_sig')
        */
        $historialCom=array();/* 
            [0] => arrayy('cproyectoejecucion'=> , 'fecha' => , 'comentariosPla' => , fplanificado, comentariosEje =>, 'comentarioObs' )

        */
        
        //$tipo = [1,2,3]; // 1 : Factirables 2: No Factirables 3: Anddes - Administrativas
        foreach($pryactividad as $pryact){
            $ejecu = DB::table('tproyectoejecucion as eje')
            ->join('tproyectoactividades as pryact','eje.cproyectoactividades','=','pryact.cproyectoactividades')
            ->where('eje.cproyectoactividades','=',$pryact->cproyectoactividades)
            ->whereBetween('eje.fplanificado',array($fecha1,$fecha2))
            ->where('eje.cpersona_ejecuta','=',$user->cpersona) 
            ->whereIn('eje.estado',['1','2','3','4','5'])
            ->select('eje.*','pryact.cproyecto','pryact.codigoactvidad','pryact.descripcionactividad',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'))
            ->orderBy('eje.tipo','ASC')
            ->get();
            //dd($fecha1);
            foreach($ejecu as $ej){
                /*
                Obtener Historial de Aprobaciones
                */
                $taprob = DB::table('tflujoaprobacion as apro')
                ->leftJoin('tpersona as per','apro.cpersona_aprobado','=','per.cpersona')
                ->where('apro.cproyectoejecucion','=',$ej->cproyectoejecucion)
                ->select('apro.*','per.nombre as emp')
                ->get();
                foreach($taprob as $aprob){
                    $his['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $his['fecha']=$aprob->fregistro;
                    $his['ccondicionoperativa']=$aprob->ccondicionoperativa_aprobado;
                    $his['ccondicionoperativa_sig']=$aprob->ccondicionoperativa_siguiente;
                    $his['aprobado']=$aprob->emp;
                    $historialApro[count($historialApro)] = $his;
                }
                /*  Fin Historial de aprobaciones*/

                /*
                Obtener Historial de Comentarios
                */



                $hic['cproyectoejecucion']=$ej->cproyectoejecucion;
                $hic['actividad']=$ej->descripcionactividad;
                $hic['fecha']=$ej->fplanificado;
                $hic['comentariosPla']=$ej->obsplanificada;
                $hic['comentariosEje']=$ej->obsejecutadas;
                $hic['comentariosObs']=$ej->obsobservadas;
                $historialCom[count($historialCom)] = $hic;
                /*  Fin Historial de comentarios*/                
                $linea=1;
                
                $seguir = true;
                while ($seguir){
                    if(!isset($alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla])){
                        $seguir = false;
                        if(!isset($alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ])){
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['cproyectoactividades'] = $ej->cproyectoactividades;
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['cproyecto'] = $ej->cproyecto;
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['descripcionactividad'] = $ej->descripcionactividad;
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['cactividad_parent'] = 0;
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['cactividad'] = 0;
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['tipo'] = $ej->tipo;
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['ctiponofacturable'] =$ej->ctiponofacturable;
                            $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ]['gp'] = $gte->abreviatura;
                            $filas++;
                        }
                        break;
                    }
                    $linea++;

                }
                
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['cproyectoejecucion']=$ej->cproyectoejecucion;
                
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['estado'] = $ej->estado;
                
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['horasplanificadas'] = intval($ej->horasplanificadas);
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['obsplanificada'] = $ej->obsplanificada;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['horasejecutadas'] = intval($ej->horasejecutadas);
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['obsejecutadas'] = $ej->obsejecutadas;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['horasaprobadas'] = intval($ej->horasaprobadas);
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['obsaprobadas'] = $ej->obsaprobadas;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['horasobservadas'] = intval($ej->horasobservadas);
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['obsobservadas'] = $ej->obsobservadas;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['cpersona_ejecuta'] = $ej->cpersona_ejecuta;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['fejecuta'] = $ej->fejecuta;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['fregistro'] =  $ej->fregistro;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['faprobacion'] = $ej->faprobacion;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['ordenaprobacion'] = $ej->ordenaprobacion;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['crol_poraprobar'] = $ej->crol_poraprobar;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['cpersona'] = $ej->cpersona;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['fplanificado'] = $ej->fplanificado;
                $alinea[$ej->cproyectoactividades.'_'.$ej->tipo."_".$linea ][$ej->fpla]['cestructuraproyecto'] = $ej->cestructuraproyecto;                

                switch ($ej->estado) {
                            case '1':
                                # code...
                                switch ($ej->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                            
                                }
                                break;
                            case '2':
                            case '3':
                            case '4':
                                switch ($ej->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;
                            
                            
                            case '5':
                                switch ($ej->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasaprobadas)==1?$ej->horasaprobadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasaprobadas)==1?$ej->horasaprobadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasaprobadas)==1?$ej->horasaprobadas:0;
                                        break;                            
                                }
                                break;                      
                        
                        }



            }


        }        

        /* Fin de Mostrar programacion de la Fecha*/

        $nroFila = $filas;

        $tipocontrato= DB::table('ttipocontrato as tc')
        ->leftJoin('tpersonadatosempleado as tp','tc.ctipocontrato','=','tp.ctipocontrato')
        ->where('tp.cpersona','=',$user->cpersona)
        ->first();

        /* fin mostrar programacion de la Fecha*/
        return view('timesheet/registroHoras',compact('nofacturables','estructuraAdm','selec','nroFila','estructura','proyecto','fecha','semana','fecha1','fecha2','sumNF','sumFF','sumAN','fecp','alinea','historialApro','historialCom','tipocontrato'));
    }

    private function verCategoriaEnProyecto($cpersona,$cproyecto){

        $descripcion = "";

        $categoriaGH = DB::table('tpersonadatosempleado as tpp')
            ->leftjoin('tcategoriaprofesional as cppy','tpp.ccategoriaprofesional','=','cppy.ccategoriaprofesional')
            ->where('tpp.cpersona','=',$cpersona)
            ->select('cppy.descripcion')
            ->first();

        $descripcion = $categoriaGH->descripcion;

        if($cproyecto != 'Administrativa'){

            $categoriaPry = DB::table('tproyectopersona as tpp')
                ->leftjoin('tcategoriaprofesional as cppy','tpp.ccategoriaprofesional','=','cppy.ccategoriaprofesional')
                ->where('tpp.cpersona','=',$cpersona)
                ->where('tpp.cproyecto','=',$cproyecto)
                ->select('cppy.descripcion')
                ->orderby('tpp.cproyectopersona','desc')
                ->first();

            $descripcion = $categoriaPry?$categoriaPry->descripcion:$descripcion;
        }


       return $descripcion;

    }
    
    public function agregarRegistroTime(Request $request){
        //dd($request->input('selected_actividad'));
        $dia_act=date('Ymd');
        $obj = new ActividadSupport();
        $fec = $request->input('fecha');
        $fecha = $request->input('fecha');
        $nroFila = $request->input('nroFila');
        $nroFilaI = $request->input('nroFila');
        
        if(substr($fec,2,1)=='-'){    
            $fecha = Carbon::createFromDate(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2));

        }else{
            $fecha = Carbon::createFromDate(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2));
        }
        $fecha1 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $fecha2 = Carbon::create($fecha->year,$fecha->month,$fecha->day,23,59,59);
        $semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        $fecp = Carbon::create($fecha1->year,$fecha1->month,$fecha1->day,0,0,0); // Fecha de Proceso
        //$fecha_eje = Carbon::createFromDate($fecha1->year,$fecha1->month,$fecha1->day); // Fecha para realizar consulta en tproyectoejecucion
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.

        $user = Auth::user();
        


        $sele= $request->input('selected_actividad');
        $tmp = array();
        foreach($sele as $s){
            if($obj->isChild($s)){
                $tmp[count($tmp)]=$s;
            }
        }
        $sele = $tmp;
        $ejecu = DB::table('tproyectoactividades as pryact')
        //->join('tactividad as act','pryact.cactividad','=','act.cactividad')
        ->join('tproyecto as p','pryact.cproyecto','=','p.cproyecto')
        ->join('tpersona as cli','p.cpersonacliente','=','cli.cpersona')
        ->join('tpersona as per','per.cpersona','=','p.cpersona_gerente')
        ->leftJoin('tunidadminera as umin',function($join){
            $join->on('cli.cpersona','=','umin.cpersona');
            $join->on('umin.cunidadminera','=','p.cunidadminera');
        })
        ->where('pryact.cproyecto','=',$request->input('cproyecto'))    
        ->whereIn('pryact.cproyectoactividades',$sele)    
        ->select('pryact.cproyectoactividades','pryact.cproyecto','pryact.descripcionactividad','pryact.cactividad','p.tipoproy')
        ->addSelect('pryact.cproyectoactividades_parent','per.abreviatura as gp','p.nombre as proyecto','cli.abreviatura as cliente')
        ->addSelect('umin.nombre as uminera','p.codigo as codigopry','pryact.codigoactvidad')
        ->get();
        //dd($ejecu);
        $alinea=array();
        $linea=$nroFila + 1;
        foreach($ejecu as $ej){
                $al['cproyectoactividades'] = $ej->cproyectoactividades;
                $al['codigopry']= $ej->codigopry;
                $al['cproyectoejecucioncab'] = '';                
                $al['personanombre']= $ej->cliente;
                $al['unidadminera']= $ej->uminera;
                $al['nombreproy']=$ej->proyecto;
                $al['cproyecto'] = $ej->cproyecto;
                $al['descripcionactividad'] = $ej->descripcionactividad;
                $al['cactividad_parent'] = $ej->cproyectoactividades_parent;
                $al['codigoactividad']=$ej->codigoactvidad;
                $al['cactividad']=$ej->cactividad;
                if($ej->tipoproy=='00001'){
                    $al['tipo'] = '3';
                }
                else{
                    $al['tipo'] = '1';
                }
                
                $al['ctiponofacturable'] ='';
                $al['gp'] = $ej->gp;      

                $cod_proyecto = ($ej->tipoproy=='00002')?$ej->cproyecto:'Administrativa';
                $categoriapry = $this->verCategoriaEnProyecto($user->cpersona,$cod_proyecto);

                $al['categoriaproy'] = $categoriapry;
                $alinea[count($alinea)]=$al;
                $nroFila++;
        }
        
        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array(""=>"");
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/        

        return view('partials.lineaTableHoras',compact('nofacturables','alinea','nroFila','nroFilaI','fecp','fecha1','fecha2','dia_act'));        
    }
    public function agregarRegistroTimeAdm(Request $request){
        //dd($request->input('selected_actividad'));
        $dia_act=date('Ymd');
        $fec = $request->input('fecha');
        $fecha = $request->input('fecha');
        $nroFila = $request->input('nroFila');
        $nroFilaI = $request->input('nroFila');
        if(substr($fec,2,1)=='-'){    
            $fecha = Carbon::create(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2),0,0,0);

        }else{
            $fecha = Carbon::create(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2),0,0,0);
        }
        $fecha1 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $fecha2 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        $fecp = Carbon::create($fecha1->year,$fecha1->month,$fecha1->day,0,0,0); // Fecha de Proceso
        //$fecha_eje = Carbon::createFromDate($fecha1->year,$fecha1->month,$fecha1->day); // Fecha para realizar consulta en tproyectoejecucion
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.

        $user = Auth::user();
        $categoriapry = $this->verCategoriaEnProyecto($user->cpersona,'Administrativa');
           


        $sele= $request->input('selected_actividad_adm');
        $ejecu = DB::table('tactividad as act')
        ->whereIn('act.cactividad',$sele)    
        ->select('act.*','act.descripcion as descripcionactividad')
        ->get();
        //dd($ejecu);
        $alinea=array();
        $linea=$nroFila + 1;
        foreach($ejecu as $ej){
                $ali['cproyectoejecucioncab'] = '';
                $ali['cproyectoactividades'] = '';
                $ali['codigoactividad']='';
                $ali['codigopry']= '';
                $ali['unidadminera']= '';
                $ali['personanombre']= '';
                $ali['nombreproy']='';
                $ali['cproyecto'] = '';
                $ali['descripcionactividad'] = $ej->descripcionactividad;
                $ali['cactividad_parent'] = $ej->cactividad_parent;
                $ali['cactividad'] = $ej->cactividad;
                $ali['tipo'] = '3';
                $ali['ctiponofacturable'] ='';
                $ali['gp'] = '';    
                $ali['categoriaproy'] = $categoriapry;
                $alinea[count($alinea)]= $ali;
                $nroFila++;
        }
        
        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array(""=>"");
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        return view('partials.lineaTableHoras',compact('nofacturables','alinea','nroFila','nroFilaI','fecp','fecha1','fecha2','dia_act'));        
    }    

    public function grabarRegistroHoras(Request $request){
        $selec = $request->input('consulta');
        DB::beginTransaction();
        $fec= $request->input('fecha');
        $fecha = $fec;
        $user = Auth::user();

        $tarea = DB::table('tpersonadatosempleado as e')           
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')      
        ->first();

        $carea=null;
        if($tarea){
          $carea=$tarea->carea;
        }

        if(substr($fec,2,1)=='-'){    
            $fecha = Carbon::createFromDate(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2));

        }else{
            $fecha = Carbon::createFromDate(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2));
        }        
        //
        $ejecu = $request->input('ejecu');
        // dd($ejecu);
        foreach($ejecu as $e){
            if($e[53]=='N'){
                if(strlen($e[54])<=0){
                    $objCab = new Tproyectoejecucioncab();
                    //$objCab->semana='';
                    if(strlen($e[0])>0){
                        $objCab->cproyectoactividades=$e[0];
                    }
                    if(strlen($e[1])>0){
                        $objCab->cactividad=$e[1];                    
                    }
                    $objCab->tipo=$e[2];   
                    $objCab->fregistro=date('Y-m-d'); 
                    $objCab->estado='2';                                    
                     
                }else{
                    $objCab = Tproyectoejecucioncab::where('cproyectoejecucioncab','=',$e[54])->first();
                }
                if(!is_null($e[3]) && strlen($e[3])>0){
                    $objCab->ctiponofacturable=$e[3];
                }else{
                    $objCab->ctiponofacturable=null;
                }
                //$objCab->estado='1';
                

                $objCab->cpersona_ejecuta=$user->cpersona;
                $objCab->save();
                $cproyectoejecucioncab = $objCab->cproyectoejecucioncab;            

                $persona = DB::table('tpersonadatosempleado')
                        ->where('cpersona','=',$user->cpersona)
                        ->first();

                $categoria = $persona->ccategoriaprofesional;

                //Dia Lun
                if($e[5]=='1' || $e[5]=='2' || empty($e[5]) ){
                    if(!empty($e[8])){ //horas
                        if(!empty($e[4])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[4])->first();
                        }else{
                            $obj = new Tproyectoejecucion();
                            $obj->cproyectoejecucioncab = $cproyectoejecucioncab;
                            if(!is_null($e[0]) && strlen($e[0])>0){
                                $obj->cproyectoactividades = $e[0]; //cproyectoactividades
                            }
                          
                            $obj->tipo = $e[2]; //tipo
                            $obj->cpersona_ejecuta = $user->cpersona;
                            if(!is_null($e[39]) && strlen($e[39])>0){
                                $obj->cestructuraproyecto=$e[39];
                            }

                            $obj->fregistro= Carbon::now();
                            $obj->fplanificado = Carbon::createFromDate(substr($e[7],0,4),substr($e[7],4,2),substr($e[7],6,2));
                            $obj->csubsistema ='004';
                            if(!is_null($e[1]) && strlen($e[1])>0){
                                $obj->cactividad = $e[1]; //cactividad
                            }                              
                            if(!is_null($e[51]) && strlen($e[51])>0){
                                $obj->cproyecto =$e[51];

                            }
                            // Inicio de pase a producción #PAS19090001
                            // A = Registro agregado
                            $obj->tipo_registro = 'A'; 
                            // Fin del pase
                        }
                        if(!is_null($e[3]) && strlen($e[3])>0 ){ //ctiponofacturable
                            $obj->ctiponofacturable=$e[3];
                        }else{
                            $obj->ctiponofacturable=null;
                        } 

                        if(!is_null($e[51]) && strlen($e[51])>0){

                                $proyectopersona = DB::table('tproyectopersona')
                                    ->where('cpersona','=',$user->cpersona)
                                    ->where('cproyecto','=',$e[51])
                                    ->first();

                                if ($proyectopersona) {
                                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                                        $categoria = $proyectopersona->ccategoriaprofesional;
                                    }
                                }
                            }                       
                        $obj->ccondicionoperativa = 'EJE';
                        $obj->estado = '2'; //ejecutado
                        $obj->horasejecutadas=$e[8]; //horas
                        $obj->obsejecutadas = $e[6]; //obs
                        $obj->fejecuta=Carbon::now();
                        $obj->carea=$carea;
                        $obj->ccategoriaprofesional =$categoria;

                        $obj->save();
                    }else{
                        if(!empty($e[4])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[4])->first();
                            $obj->horasejecutadas = null;
                            $obj->estado='2';
                            $obj->obsejecutadas = null;
                            $obj->save();
                        }

                        
                    }
                }
                
                //Dia Mar
                if($e[10]=='1' || $e[10]=='2' || empty($e[10]) ){
                    if(!empty($e[13])){ //horas
                        if(!empty($e[9])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[9])->first();
                        }else{
                            $obj = new Tproyectoejecucion();
                            $obj->cproyectoejecucioncab = $cproyectoejecucioncab;
                            if(!is_null($e[0]) && strlen($e[0])>0){
                                $obj->cproyectoactividades = $e[0]; //cproyectoactividades
                            }
                            $obj->tipo = $e[2]; //tipo
                            $obj->cpersona_ejecuta = $user->cpersona;
                            if(!is_null($e[40]) && strlen($e[40])>0){
                                $obj->cestructuraproyecto=$e[40];
                            }

                            $obj->fregistro= Carbon::now();
                            $obj->fplanificado = Carbon::createFromDate(substr($e[12],0,4),substr($e[12],4,2),substr($e[12],6,2));
                            $obj->csubsistema ='004';
                            if(!is_null($e[1]) && strlen($e[1])>0){
                                $obj->cactividad = $e[1]; //cactividad
                            }                              
                            if(!is_null($e[51]) && strlen($e[51])>0){
                                $obj->cproyecto =$e[51];

                            }
                            // Inicio de pase a producción #PAS19090001
                            // A = Registro agregado
                            $obj->tipo_registro = 'A';
                            // Fin del pase
                        }
                        if(!is_null($e[3]) && strlen($e[3])>0){ //ctiponofacturable
                            $obj->ctiponofacturable=$e[3];
                        }else{
                            $obj->ctiponofacturable=null;
                        } 

                        if(!is_null($e[51]) && strlen($e[51])>0){

                                $proyectopersona = DB::table('tproyectopersona')
                                    ->where('cpersona','=',$user->cpersona)
                                    ->where('cproyecto','=',$e[51])
                                    ->first();
                                if ($proyectopersona) {
                                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                                        $categoria = $proyectopersona->ccategoriaprofesional;
                                    }
                                }
                            }                       
                        $obj->ccondicionoperativa = 'EJE';
                        $obj->estado = '2'; //ejecutado
                        $obj->horasejecutadas=$e[13]; //horas
                        $obj->obsejecutadas = $e[11]; //obs
                        $obj->fejecuta=Carbon::now();
                        $obj->carea=$carea;
                        $obj->ccategoriaprofesional =$categoria;

                        $obj->save();
                    }else{
                        if(!empty($e[9])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[9])->first();
                            $obj->horasejecutadas = null;
                            $obj->obsejecutadas = null;
                            $obj->estado='2';
                            $obj->save();
                        }

                        
                    }
                }
                //Dia Mie
                if($e[15]=='1' || $e[15]=='2' || empty($e[15])  ){
                    if(!empty($e[18])){ //horas
                        if(!empty($e[14])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[14])->first();
                        }else{
                            $obj = new Tproyectoejecucion();
                            $obj->cproyectoejecucioncab = $cproyectoejecucioncab;
                            if(!is_null($e[0]) && strlen($e[0])>0){
                                $obj->cproyectoactividades = $e[0]; //cproyectoactividades
                            }
                            $obj->tipo = $e[2]; //tipo
                            $obj->cpersona_ejecuta = $user->cpersona;
                            if(!is_null($e[41]) && strlen($e[41])>0){
                                $obj->cestructuraproyecto=$e[41];
                            }
                            /*if(!empty($e[3])){ //ctiponofacturable
                                $obj->ctiponofacturable=$e[3];
                            }*/
                            $obj->fregistro= Carbon::now();
                            $obj->fplanificado = Carbon::createFromDate(substr($e[17],0,4),substr($e[17],4,2),substr($e[17],6,2));
                            $obj->csubsistema ='004';
                            if(!is_null($e[1]) && strlen($e[1])>0){
                                $obj->cactividad = $e[1]; //cactividad
                            }                              
                            if(!is_null($e[51]) && strlen($e[51])>0){
                                $obj->cproyecto =$e[51];

                            }
                            // Inicio de pase a producción #PAS19090001
                            // A = Registro agregado
                            $obj->tipo_registro = 'A';
                            // Fin del pase
                        }
                        if(!is_null($e[3]) && strlen($e[3])>0){ //ctiponofacturable
                            $obj->ctiponofacturable=$e[3];
                        }else{
                            $obj->ctiponofacturable=null;
                        } 

                        if(!is_null($e[51]) && strlen($e[51])>0){

                                $proyectopersona = DB::table('tproyectopersona')
                                    ->where('cpersona','=',$user->cpersona)
                                    ->where('cproyecto','=',$e[51])
                                    ->first();
                                if ($proyectopersona) {
                                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                                        $categoria = $proyectopersona->ccategoriaprofesional;
                                    }
                                }
                            }                       
                        $obj->ccondicionoperativa = 'EJE';
                        $obj->estado = '2'; //ejecutado
                        $obj->horasejecutadas=$e[18]; //horas
                        $obj->obsejecutadas = $e[16]; //obs
                        $obj->fejecuta=Carbon::now();
                        $obj->carea=$carea;
                        $obj->ccategoriaprofesional =$categoria;
                        $obj->save();
                    }else{
                        if(!empty($e[14])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[14])->first();
                            $obj->horasejecutadas = null;
                            $obj->obsejecutadas = null;
                            $obj->estado='2';
                            $obj->save();
                        }

                        
                    }
                }
                //Dia Jue
                if($e[20]=='1' || $e[20]=='2' || empty($e[20]) ){
                    if(!empty($e[23])){ //horas
                        if(!empty($e[19])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[19])->first();
                        }else{
                            $obj = new Tproyectoejecucion();
                            $obj->cproyectoejecucioncab = $cproyectoejecucioncab;
                            if(!is_null($e[0]) && strlen($e[0])>0){
                                $obj->cproyectoactividades = $e[0]; //cproyectoactividades
                            }
                            $obj->tipo = $e[2]; //tipo
                            $obj->cpersona_ejecuta = $user->cpersona;
                            if(!is_null($e[42]) && strlen($e[42])>0){
                                $obj->cestructuraproyecto=$e[42];
                            }
                            /*if(!empty($e[3])){ //ctiponofacturable
                                $obj->ctiponofacturable=$e[3];
                            }*/
                            $obj->fregistro= Carbon::now();
                            $obj->fplanificado = Carbon::createFromDate(substr($e[22],0,4),substr($e[22],4,2),substr($e[22],6,2));
                            $obj->csubsistema ='004';
                            if(!is_null($e[1]) && strlen($e[1])>0){
                                $obj->cactividad = $e[1]; //cactividad
                            }                              
                            if(!is_null($e[51]) && strlen($e[51])>0){
                                $obj->cproyecto =$e[51];

                            }
                            // Inicio de pase a producción #PAS19090001
                            // A = Registro agregado
                            $obj->tipo_registro = 'A';
                            // Fin del pase
                        }
                        if(!is_null($e[3]) && strlen($e[3])>0){ //ctiponofacturable
                            $obj->ctiponofacturable=$e[3];
                        }else{
                            $obj->ctiponofacturable=null;
                        } 

                        if(!is_null($e[51]) && strlen($e[51])>0){

                                $proyectopersona = DB::table('tproyectopersona')
                                    ->where('cpersona','=',$user->cpersona)
                                    ->where('cproyecto','=',$e[51])
                                    ->first();
                                if ($proyectopersona) {
                                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                                        $categoria = $proyectopersona->ccategoriaprofesional;
                                    }
                                }
                            }                       
                        $obj->ccondicionoperativa = 'EJE';
                        $obj->estado = '2'; //ejecutado
                        $obj->horasejecutadas=$e[23]; //horas
                        $obj->obsejecutadas = $e[21]; //obs
                        $obj->fejecuta=Carbon::now();
                        $obj->carea=$carea;
                        $obj->ccategoriaprofesional =$categoria;
                        $obj->save();
                    }else{
                        if(!empty($e[19])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[19])->first();
                            $obj->horasejecutadas = null;
                            $obj->obsejecutadas = null;
                            $obj->estado='2';
                            $obj->save();
                        }

                        
                    }
                }
                //Dia vie
                if($e[25]=='1' || $e[25]=='2' || empty($e[25]) ){
                    if(!empty($e[28])){ //horas
                        if(!empty($e[24])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[24])->first();
                        }else{
                            $obj = new Tproyectoejecucion();
                            $obj->cproyectoejecucioncab = $cproyectoejecucioncab;
                            if(!is_null($e[0]) && strlen($e[0])>0){
                                $obj->cproyectoactividades = $e[0]; //cproyectoactividades
                            }
                            $obj->tipo = $e[2]; //tipo
                            $obj->cpersona_ejecuta = $user->cpersona;
                            if(!is_null($e[43]) && strlen($e[43])>0){
                                $obj->cestructuraproyecto=$e[43];
                            }
                            /*if(!empty($e[3])){ //ctiponofacturable
                                $obj->ctiponofacturable=$e[3];
                            }*/
                            $obj->fregistro= Carbon::now();
                            $obj->fplanificado = Carbon::createFromDate(substr($e[27],0,4),substr($e[27],4,2),substr($e[27],6,2));
                            $obj->csubsistema ='004';
                            if(!is_null($e[1]) && strlen($e[1])>0){
                                $obj->cactividad = $e[1]; //cactividad
                            }                              
                            if(!is_null($e[51]) && strlen($e[51])>0){
                                $obj->cproyecto =$e[51];

                            }
                            // Inicio de pase a producción #PAS19090001
                            // A = Registro agregado
                            $obj->tipo_registro = 'A';
                            // Fin del pase
                        }
                        if(!is_null($e[3]) && strlen($e[3])>0){ //ctiponofacturable
                            $obj->ctiponofacturable=$e[3];
                        }else{
                            $obj->ctiponofacturable=null;
                        } 

                        if(!is_null($e[51]) && strlen($e[51])>0){

                                $proyectopersona = DB::table('tproyectopersona')
                                    ->where('cpersona','=',$user->cpersona)
                                    ->where('cproyecto','=',$e[51])
                                    ->first();
                                if ($proyectopersona) {
                                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                                        $categoria = $proyectopersona->ccategoriaprofesional;
                                    }
                                }
                            }                       
                        $obj->ccondicionoperativa = 'EJE';
                        $obj->estado = '2'; //ejecutado
                        $obj->horasejecutadas=$e[28]; //horas
                        $obj->obsejecutadas = $e[26]; //obs
                        $obj->fejecuta=Carbon::now();
                        $obj->carea=$carea;
                        $obj->ccategoriaprofesional =$categoria;
                        $obj->save();
                    }else{
                        if(!empty($e[24])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[24])->first();
                            $obj->horasejecutadas = null;
                            $obj->obsejecutadas = null;
                            $obj->estado='2';
                            $obj->save();
                        }

                        
                    }
                }
                //Dia Sab
                if($e[30]=='1' || $e[30]=='2' || empty($e[30]) ){
                    if(!empty($e[33])){ //horas
                        if(!empty($e[29])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[29])->first();
                        }else{
                            $obj = new Tproyectoejecucion();
                            $obj->cproyectoejecucioncab = $cproyectoejecucioncab;
                            if(!is_null($e[0]) && strlen($e[0])>0){
                                $obj->cproyectoactividades = $e[0]; //cproyectoactividades
                            }
                            $obj->tipo = $e[2]; //tipo
                            $obj->cpersona_ejecuta = $user->cpersona;
                            if(!is_null($e[44]) && strlen($e[44])>0){
                                $obj->cestructuraproyecto=$e[44];
                            }
                            /*
                            if(!empty($e[3])){ //ctiponofacturable
                                $obj->ctiponofacturable=$e[3];
                            }*/
                            
                            $obj->fregistro= Carbon::now();
                            $obj->fplanificado = Carbon::createFromDate(substr($e[32],0,4),substr($e[32],4,2),substr($e[32],6,2));
                            $obj->csubsistema ='004';
                            if(!is_null($e[1]) && strlen($e[1])>0){
                                $obj->cactividad = $e[1]; //cactividad
                            }                              
                            if(!is_null($e[51]) && strlen($e[51])>0){
                                $obj->cproyecto =$e[51];

                            }
                            // Inicio de pase a producción #PAS19090001
                            // A = Registro agregado
                            $obj->tipo_registro = 'A';
                            // Fin del pase
                        }
                        if(!is_null($e[3]) && strlen($e[3])>0){ //ctiponofacturable
                            $obj->ctiponofacturable=$e[3];
                        }else{
                            $obj->ctiponofacturable=null;
                        } 

                        if(!is_null($e[51]) && strlen($e[51])>0){

                                $proyectopersona = DB::table('tproyectopersona')
                                    ->where('cpersona','=',$user->cpersona)
                                    ->where('cproyecto','=',$e[51])
                                    ->first();
                                if ($proyectopersona) {
                                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                                        $categoria = $proyectopersona->ccategoriaprofesional;
                                    }
                                }
                            }                       
                        $obj->ccondicionoperativa = 'EJE';
                        $obj->estado = '2'; //ejecutado
                        $obj->horasejecutadas=$e[33]; //horas
                        $obj->obsejecutadas = $e[31]; //obs
                        $obj->fejecuta=Carbon::now();
                        $obj->carea=$carea;
                        $obj->ccategoriaprofesional =$categoria;
                        $obj->save();
                    }else{
                        if(!empty($e[29])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[29])->first();
                            $obj->horasejecutadas = null;
                            $obj->obsejecutadas = null;
                            $obj->estado='2';
                            $obj->save();
                        }

                        
                    }
                }
                //Dia Dom
                
                if($e[35]=='1' || $e[35]=='2' || empty($e[35])  ){
                    if(!empty($e[38])){ //horas
                        if(!empty($e[34])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[34])->first();
                        }else{
                            $obj = new Tproyectoejecucion();
                            $obj->cproyectoejecucioncab = $cproyectoejecucioncab;
                            if(!is_null($e[0]) && strlen($e[0])>0){
                                $obj->cproyectoactividades = $e[0]; //cproyectoactividades
                            }
                            $obj->tipo = $e[2]; //tipo
                            $obj->cpersona_ejecuta = $user->cpersona;
                            if(!is_null($e[45]) && strlen($e[45])>0){
                                $obj->cestructuraproyecto=$e[45];
                            }
                            /*
                            if(!empty($e[3])){ //ctiponofacturable
                                $obj->ctiponofacturable=$e[3];
                            }*/
                            $obj->fregistro= Carbon::now();
                            $obj->fplanificado = Carbon::createFromDate(substr($e[37],0,4),substr($e[37],4,2),substr($e[37],6,2));
                            $obj->csubsistema ='004';
                            if(!is_null($e[1]) && strlen($e[1])>0){
                                $obj->cactividad = $e[1]; //cactividad
                            }                              
                            if(!is_null($e[51]) && strlen($e[51])>0){
                                $obj->cproyecto =$e[51];

                            }
                            // Inicio de pase a producción #PAS19090001
                            // A = Registro agregado
                            $obj->tipo_registro = 'A';
                            // Fin del pase
                        }
                        if(!is_null($e[3]) && strlen($e[3])>0){ //ctiponofacturable
                            $obj->ctiponofacturable=$e[3];
                        }else{
                            $obj->ctiponofacturable=null;
                        } 

                        if(!is_null($e[51]) && strlen($e[51])>0){

                                $proyectopersona = DB::table('tproyectopersona')
                                    ->where('cpersona','=',$user->cpersona)
                                    ->where('cproyecto','=',$e[51])
                                    ->first();
                                if ($proyectopersona) {
                                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                                        $categoria = $proyectopersona->ccategoriaprofesional;
                                    }
                                }
                            }                       
                        $obj->ccondicionoperativa = 'EJE';
                        $obj->estado = '2'; //ejecutado
                        $obj->horasejecutadas=$e[38]; //horas
                        $obj->obsejecutadas = $e[36]; //obs
                        $obj->fejecuta=Carbon::now();
                        $obj->carea=$carea;
                        $obj->ccategoriaprofesional =$categoria;
                        $obj->save();
                    }else{
                        if(!empty($e[34])){ //cproyectoejecucion
                            $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[34])->first();
                            $obj->horasejecutadas = null;
                            $obj->obsejecutadas = null;
                            $obj->estado='2';
                            $obj->save();
                        }

                        
                    }
                }      
            }else{
                // Si se va eliminar
                //54 cproyectoejecucioncab

                if(strlen($e[54])>0){

                    // dd($e);

                    $centregables=[];
                    foreach ($e as $ent_fila => $value) {

                        if ($e[4] != "") {

                            array_push($centregables, $e[4]);
                            
                        }
                        if ($e[9] != "") {

                            array_push($centregables, $e[9]);
                            
                        }
                        if ($e[14] != "") {

                            array_push($centregables, $e[14]);
                           
                        }
                        if ($e[19] != "") {

                            array_push($centregables, $e[19]);
                           
                        }
                        if ($e[24] != "") {

                            array_push($centregables, $e[24]);
                           
                        }
                        if ($e[29] != "") {

                            array_push($centregables, $e[29]);
                           
                        }if ($e[34] != "") {

                            array_push($centregables, $e[34]);
                           
                        }
                        
                    }
                    $objCab = Tproyectoejecucioncab::where('cproyectoejecucioncab','=',$e[54])->first();
                    // $teje = DB::table('tproyectoejecucion')
                    // ->where('cproyectoejecucioncab','=',$e[54])
                    // ->orderBy('fplanificado','asc')
                    // ->get();

                    $teje = DB::table('tproyectoejecucion')
                    ->whereIn('cproyectoejecucion',$centregables)
                    ->orderBy('fplanificado','asc')
                    ->get();


                    $eliminar=true;
                    $arrayflujap=array();

                    foreach ($teje as $t) {
                       if($t->estado==2){

                            if(strlen($t->horasobservadas)>=0||strlen($t->horasaprobadas)>=0){
                                $arrayflujap[count($arrayflujap)]=$t->cproyectoejecucion;          
                            }                                                 
                        }
                    }

                    $flujoaprob= Tflujoaprobacion::whereIn('cproyectoejecucion',$arrayflujap)
                    ->get(); 

                    foreach($teje as $ej){

                        if($ej->horasplanificadas!='' || !(is_null($ej->horasplanificadas))){
                            $eliminar = false;
                            break;
                        }                        
                    }   

                      
                   $semana = $fecha->weekOfYear; 
                     
                    if($eliminar==true){  


                        if($flujoaprob){
                            foreach ($flujoaprob as $fa) {
                                $objflujoaprob=Tflujoaprobacion::where('cflujoaprobacion','=',$fa->cflujoaprobacion)
                                //->where('ccondicionoperativa_siguiente','=','EJE')
                                ->first();  


                                if ($objflujoaprob) {
                                    $objflujoaprob->delete();
                                }
                            }
                        } 

                    
                       $tejborrar = DB::table('tproyectoejecucion')
                        ->whereIn('cproyectoejecucion',$centregables)                       
                        ->select('estado','fplanificado',DB::raw("extract(week from fplanificado::date)as semana"),'cproyectoejecucioncab','cproyectoejecucion')
                        ->get(); 

                        
                        foreach ($tejborrar as $tjb) {
                                
                            $deletedRows = Tproyectoejecucion::where('cproyectoejecucion', $tjb->cproyectoejecucion)
                            ->where(DB::raw("extract(week from fplanificado::date)"),'=',$semana)
                            ->where('estado','=','2')
                            ->delete();
                        }

                        $hrejecuciondet= DB::table('tproyectoejecucion')
                        ->where('cproyectoejecucioncab','=',$e[54])
                        ->get(); 

                        if(!($hrejecuciondet)){
                            if ($objCab) {
                                $objCab->delete();
                            }
                        }
                      
                    }
                }       
                        
            }                               

        }
        DB::commit();
        $this->grabasumaHorasxSemyPy($user->cpersona,$fecha,$user->nombre);
        /*
        if($selec=='P'){
            $cproyecto = $request->input('cproyecto');
            return Redirect::route('editarRegistroHoras',compact('cproyecto','fecha'));
        }else{
            return Redirect::route('editarTodosRegistrodeHoras',compact('fecha'));
        }*/
        return "OK";
    }

    public function grabasumaHorasxSemyPy($cpersona,$fecha,$usuario){

        $semana = $fecha->weekOfYear;
        $periodo = $fecha->year;
        
        $horas = DB::table('tproyectoejecucion as tp')
        ->select(DB::RAW('distinct tp.cproyecto,extract(week from tp.fplanificado) as semana,extract(year from tp.fplanificado) as periodo'))
        ->addSelect(DB::RAW('sum(tp.horasejecutadas)'))
        ->where('tp.cpersona_ejecuta','=',$cpersona)
        ->where(DB::raw("extract(week from tp.fplanificado)"),'=',$semana)
        ->where(DB::raw("extract(year from tp.fplanificado)"),'=',$periodo)
        ->whereNotNull('tp.horasejecutadas')
        ->groupby('tp.cproyecto','tp.cpersona_ejecuta',DB::raw("extract(week from tp.fplanificado)"),DB::raw("extract(year from tp.fplanificado)"))
        ->get();


        $dis = DB::table('tusuarios as us')
            ->join('tpersonadatosempleado as tpe','tpe.cpersona','=','us.cpersona')
            ->join('ttipocontrato as tc','tc.ctipocontrato','=','tpe.ctipocontrato')
            ->where('us.nombre','=',$usuario)->first();

        foreach ($horas as $value) {
            $horas = Planificacion::where("cproyecto", "=", $value->cproyecto)
                ->where("periodo", "=", $periodo)
                ->where("semana", "=", $semana)
                ->where("usuario", "=", $usuario)->first();


            if($horas == null){
            $horas = new Planificacion();
            $horas->periodo = $periodo;         
            $horas->cproyecto = $value->cproyecto;
            $horas->semana = $semana;
            $horas->usuario = $usuario;
            $horas->total = $dis->horas;            
            }       
            $horas->ejecutadas = $value->sum;
            $horas->save();
                   
        }
        
        // $disponibles = 0;

        //calcular disponibilidad
        $suma_planificadas = DB::table('planificacion')
                ->select(DB::raw('SUM(planificadas) as planificadas, SUM(planificadas_adm) as planificadas_adm'))                
                ->where("periodo", "=", $periodo)
                ->where("semana", "=", $semana)
                ->where("usuario", "=", $usuario)
                ->first();

        $horas_eje = Planificacion::where("periodo", "=", $periodo)
                ->where("semana", "=", $semana)
                ->where("usuario", "=", $usuario)->get();
        
        $suma_eje = 0;
        
        foreach ($horas_eje as $eje) {
            $suma_eje += $eje->ejecutadas;
        }   

        $ejecutadas = $suma_eje;

        $suma_plani = $suma_planificadas->planificadas+$suma_planificadas->planificadas_adm;
        
        $suma_horas=0;

        if ($suma_eje > $suma_plani) {
            $suma_horas = $suma_eje;
        }
        else {
            $suma_horas = $suma_plani;
        }
        
        if (!$horas) {
            $disponibles = $dis->horas;
        }

        else{
            $horas->disponibles = $disponibles = $horas->total - $suma_horas;
        }

        // dd($suma_horas,$suma_eje,$suma_plani,$disponibles);

        Planificacion::where("periodo", "=", $periodo)
            ->where("semana", "=", $semana)
            ->where("usuario", "=", $usuario)
            ->update(['disponibles' => $disponibles]);

            // $horas->ejecutadas = $suma_eje;

    }

    public function clonarHoras($obj){
        $fecha = substr($obj->fplanificado,0,4).substr($obj->fplanificado,5,2).substr($obj->fplanificado,8,2);        
        $tobj = DB::table('tproyectoejecucion as eje')
        ->where('cpersona_ejecuta','=',$obj->cpersona_ejecuta)
        ->where(DB::raw('to_char((eje.fplanificado + interval \' -7 days \'),\'YYYYMMDD\')'),'=',$fecha)
        ->where('eje.tipo','=',$obj->tipo);
        if(strlen($obj->ctiponofacturable)>0){
            $tobj=$tobj->where('ctiponofacturable','=',$obj->ctiponofacturable);
        }
        if($obj->tipo==3){

            if(is_null($obj->cactividad)){
                $tobj=$tobj->where('cproyecto','=',$obj->cproyecto)
                ->where('cproyectoactividades','=',$obj->cproyectoactividades);
            }
            else{
                $tobj=$tobj->where('cactividad','=',$obj->cactividad);
            }
            
        }else{
            $tobj=$tobj->where('cproyecto','=',$obj->cproyecto)
            ->where('cproyectoactividades','=',$obj->cproyectoactividades);
        }
        $tobj=$tobj->count();

        if($tobj<=0){
            //clonar
            
            $strfecha=Carbon::createFromDate(substr($fecha,0,4),substr($fecha,4,2),substr($fecha,6,2));
            $strfecha->addWeek();
            $newObj = new Tproyectoejecucion();

            $newObj->estado='2';
            $newObj->horasejecutadas=null; //clona las actividades en la sgte semana pero sin horas
            //$newObj->horasejecutadas=$obj->horasejecutadas;
            $newObj->cpersona_ejecuta = $obj->cpersona_ejecuta;
            $newObj->fejecuta= Carbon::now();
            $newObj->fregistro= Carbon::now();
            $newObj->fplanificado=$strfecha;
            $newObj->tipo=$obj->tipo;
            $newObj->ctiponofacturable=$obj->ctiponofacturable;
            $newObj->ccondicionoperativa='EJE';
            $newObj->csubsistema='004';
            // Inicio de pase a producción #PAS19090001
            // C = Registro clonado
            $newObj->tipo_registro='C';
            // Fin del pase
            if($obj->tipo==3){

                if(is_null($obj->cactividad)){
                    $newObj->cproyectoactividades=$obj->cproyectoactividades;
                    $newObj->cproyecto=$obj->cproyecto;               
                }
                else{
                    $newObj->cactividad=$obj->cactividad;
                }
                
            }else{
                $newObj->cproyectoactividades=$obj->cproyectoactividades;
                $newObj->cproyecto=$obj->cproyecto;
            }
            $newObj->cproyectoejecucioncab=$obj->cproyectoejecucioncab;
            $newObj->save();
        }        
    }

    public function enviarHH(Request $request){
        DB::beginTransaction();
        $objFlow = new FlowSupport();
        $ejecu = $request->input('ejecu');
        //dd($ejecu);
        foreach($ejecu as $e){
            //Dia Lun
            if($e[5]=='1' || $e[5]=='2' ){
                if(!empty($e[8])){ //horas
                    if(!empty($e[4])){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[4])->first();
                        $res= $objFlow->obtenerSiguienteCondicionAut('004','1',$obj->ccondicionoperativa);
                        $obj->ccondicionoperativa = $res[0][0];
                        $obj->estado = $res[0][1]; //por aprobar
                        $obj->save();       
                        $objFlow->guardarAprobacionTimeSheet($e[4],'EJE','ENV',1,Auth::user()->cpersona,0);
                        //clonar la siguiente semana.

                        $this->clonarHoras($obj);

                    }

                }
            }
            
            //Dia Mar
            if($e[10]=='1' || $e[10]=='2'  ){
                if(!empty($e[13])){ //horas
                    if(!empty($e[9])){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[9])->first();
                        $res= $objFlow->obtenerSiguienteCondicionAut('004','1',$obj->ccondicionoperativa);
                        $obj->ccondicionoperativa = $res[0][0];
                        $obj->estado = $res[0][1]; //por aprobar
                        $obj->save();      
                        //clonar la siguiente semana.
                        $objFlow->guardarAprobacionTimeSheet($e[9],'EJE','ENV',1,Auth::user()->cpersona,0);
                        $this->clonarHoras($obj);
                        
                    }

                }
            }
            //Dia Mie
            if($e[15]=='1' || $e[15]=='2'  ){
                if(!empty($e[18])){ //horas
                    if(!empty($e[14])){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[14])->first();
                        $res= $objFlow->obtenerSiguienteCondicionAut('004','1',$obj->ccondicionoperativa);
                        $obj->ccondicionoperativa = $res[0][0];
                        $obj->estado = $res[0][1]; //por aprobar
                        $obj->save();      
                        //clonar la siguiente semana.
                        $objFlow->guardarAprobacionTimeSheet($e[14],'EJE','ENV',1,Auth::user()->cpersona,0);
                        $this->clonarHoras($obj);
                        
                    }

                }
            }
            //Dia Jue
            if($e[20]=='1' || $e[20]=='2'  ){
                if(!empty($e[23])){ //horas
                    if(!empty($e[19])){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[19])->first();
                        $res= $objFlow->obtenerSiguienteCondicionAut('004','1',$obj->ccondicionoperativa);
                        $obj->ccondicionoperativa =$res[0][0];
                        $obj->estado = $res[0][1]; //por aprobar
                        $obj->save();    
                        //clonar la siguiente semana.
                        $objFlow->guardarAprobacionTimeSheet($e[19],'EJE','ENV',1,Auth::user()->cpersona,0);
                        $this->clonarHoras($obj);
                        
                    }

                }
            }
            //Dia vie
            if($e[25]=='1' || $e[25]=='2'  ){
                if(!empty($e[28])){ //horas
                    if(!empty($e[24])){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[24])->first();
                        $res= $objFlow->obtenerSiguienteCondicionAut('004','1',$obj->ccondicionoperativa);
                        $obj->ccondicionoperativa = $res[0][0];
                        $obj->estado = $res[0][1]; //por aprobar
                        $obj->save();   
                        //clonar la siguiente semana.
                        $objFlow->guardarAprobacionTimeSheet($e[24],'EJE','ENV',1,Auth::user()->cpersona,0);
                        $this->clonarHoras($obj);
                        
                    }

                }
            }
            //Dia Sab
            if($e[30]=='1' || $e[30]=='2' ){
                if(!empty($e[33])){ //horas
                    if(!empty($e[29])){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[29])->first();
                        $res= $objFlow->obtenerSiguienteCondicionAut('004','1',$obj->ccondicionoperativa);
                        $obj->ccondicionoperativa = $res[0][0];
                        $obj->estado = $res[0][1]; //por aprobar
                        $obj->save();   
                        //clonar la siguiente semana.
                        $objFlow->guardarAprobacionTimeSheet($e[29],'EJE','ENV',1,Auth::user()->cpersona,0);
                        $this->clonarHoras($obj);
                        
                    }

                }
            }
            //Dia Dom
            
            if($e[35]=='1' || $e[35]=='2'  ){
                if(!empty($e[38])){ //horas
                    if(!empty($e[34])){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[34])->first();
                        $res= $objFlow->obtenerSiguienteCondicionAut('004','1',$obj->ccondicionoperativa);
                        $obj->ccondicionoperativa = $res[0][0];
                        $obj->estado = $res[0][1]; //por aprobar
                        $obj->save();    
                        //clonar la siguiente semana.
                        $objFlow->guardarAprobacionTimeSheet($e[34],'EJE','ENV',1,Auth::user()->cpersona,0);
                        $this->clonarHoras($obj);
                        
                    }

                }
            }                                    

        }
        DB::commit();
        return "OK";/*Redirect::route('editarRegistroHoras',compact('cproyecto','fecha'));*/
    }

    
    public function reAbrirHoras(Request $request){
        DB::beginTransaction();
        $objFlow = new FlowSupport();
        $aNivelReapertura=array();
        $aNivelApertura="";
        $treapertura = DB::table('tcatalogo')
        ->where('codtab','=','00054')
        ->where('digide','=','00001')
        ->first();
        if($treapertura){
            $aNivelReapertura = explode(';',$treapertura->valor);
        }
        $treapertura = DB::table('tcatalogo')
        ->where('codtab','=','00054')
        ->where('digide','=','00002')
        ->first();
        if($treapertura){
            $aNivelApertura = explode(";",$treapertura->valor);
        }
        
        $ejecu = $request->input('ejecu');
        //dd($ejecu);
        foreach($ejecu as $e){
            //Dia Lun
            if(!empty($e[8])){ //horas
                if(!empty($e[4])){ //cproyectoejecucion
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[4])->first();
                    $res = array_search($obj->ccondicionoperativa, $aNivelReapertura);
                    if($res===FALSE){
                        $obj=null;
                    }else{
                        $obj->ccondicionoperativa = $aNivelApertura[1];
                        $obj->estado = $aNivelApertura[0]; //por aprobar
                        $obj->save();                        
                    }
                    $objFlow->guardarAprobacionTimeSheet($e[4],'ENV','EJE',1,Auth::user()->cpersona,0);

                }

            }

            
            //Dia Mar

            if(!empty($e[13])){ //horas
                if(!empty($e[9])){ //cproyectoejecucion
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[9])->first();
                    $res = array_search($obj->ccondicionoperativa, $aNivelReapertura);
                    if($res===FALSE){
                        $obj=null;
                    }else{
                        $obj->ccondicionoperativa = $aNivelApertura[1];
                        $obj->estado = $aNivelApertura[0]; //por aprobar
                        $obj->save();                        
                    }                   
                    $objFlow->guardarAprobacionTimeSheet($e[9],'ENV','EJE',1,Auth::user()->cpersona,0);      
                }

            }
            //Dia Mie
            if(!empty($e[18])){ //horas
                if(!empty($e[14])){ //cproyectoejecucion
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[14])->first();
                    $res = array_search($obj->ccondicionoperativa, $aNivelReapertura);
                    if($res===FALSE){
                        $obj=null;
                    }else{
                        $obj->ccondicionoperativa = $aNivelApertura[1];
                        $obj->estado = $aNivelApertura[0]; //por aprobar
                        $obj->save();                        
                    }                   
                    $objFlow->guardarAprobacionTimeSheet($e[14],'ENV','EJE',1,Auth::user()->cpersona,0); 
                }

            }
            
            //Dia Jue
            if(!empty($e[23])){ //horas
                if(!empty($e[19])){ //cproyectoejecucion
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[19])->first();
                    $res = array_search($obj->ccondicionoperativa, $aNivelReapertura);
                    if($res===FALSE){
                        $obj=null;
                    }else{
                        $obj->ccondicionoperativa = $aNivelApertura[1];
                        $obj->estado = $aNivelApertura[0]; //por aprobar
                        $obj->save();                        
                    }                   
                    $objFlow->guardarAprobacionTimeSheet($e[19],'ENV','EJE',1,Auth::user()->cpersona,0);   
                }

            }
            //Dia vie
            if(!empty($e[28])){ //horas
                if(!empty($e[24])){ //cproyectoejecucion
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[24])->first();
                    $res = array_search($obj->ccondicionoperativa, $aNivelReapertura);
                    if($res===FALSE){
                        $obj=null;
                    }else{
                        $obj->ccondicionoperativa = $aNivelApertura[1];
                        $obj->estado = $aNivelApertura[0]; //por aprobar
                        $obj->save();                        
                    }                   
                    $objFlow->guardarAprobacionTimeSheet($e[24],'ENV','EJE',1,Auth::user()->cpersona,0);  
                }

            }
            //Dia Sab
            if(!empty($e[33])){ //horas
                if(!empty($e[29])){ //cproyectoejecucion
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[29])->first();
                    $res = array_search($obj->ccondicionoperativa, $aNivelReapertura);
                    if($res===FALSE){
                        $obj=null;
                    }else{
                        $obj->ccondicionoperativa = $aNivelApertura[1];
                        $obj->estado = $aNivelApertura[0]; //por aprobar
                        $obj->save();                        
                    }                   
                    $objFlow->guardarAprobacionTimeSheet($e[29],'ENV','EJE',1,Auth::user()->cpersona,0);   
                }

            }
            //Dia Dom
            
            if(!empty($e[38])){ //horas
                if(!empty($e[34])){ //cproyectoejecucion
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e[34])->first();
                    $res = array_search($obj->ccondicionoperativa, $aNivelReapertura);
                    if($res===FALSE){
                        $obj=null;
                    }else{
                        $obj->ccondicionoperativa = $aNivelApertura[1];
                        $obj->estado = $aNivelApertura[0]; //por aprobar
                        $obj->save();                        
                    }                  
                    $objFlow->guardarAprobacionTimeSheet($e[34],'ENV','EJE',1,Auth::user()->cpersona,0);
                }

            }
                                   

        }
        DB::commit();
        return "OK";/*Redirect::route('editarRegistroHoras',compact('cproyecto','fecha'));*/
    }    
    public function clonarregistro(Request $request,$id){
        $nofacturables = Ttiposnofacturable::lists('descripcion','ctiponofacturable')->all();
        $nofacturables = [''=>'Seleccione Motivo No Facturable'] + $nofacturables;

        $ej = DB::table('tproyectocronogramadetalle as deta')
        ->join('tproyectoactividades as pryact','deta.cproyectoactividades','=','pryact.cproyectoactividades')
        //->join('tactividad as act','pryact.cactividad','=','act.cactividad')
        ->where('deta.cproyectocronogramadetalle','=',$id)        
        ->select('deta.*','pryact.descripcionactividad','pryact.cactividad','act.cactividad_parent')
        ->first();
        $alinea=array();
        $linea=100;
        if ($ej){
                $alinea[$ej->cproyectocronogramadetalle.'_2_'.$linea ]['cproyectocronogramadetalle'] = $ej->cproyectocronogramadetalle;
                $alinea[$ej->cproyectocronogramadetalle.'_2_'.$linea ]['descripcionactividad'] = $ej->descripcionactividad;
                $alinea[$ej->cproyectocronogramadetalle.'_2_'.$linea ]['cactividad_parent'] = $ej->cproyectoactividades_parent;
                $alinea[$ej->cproyectocronogramadetalle.'_2_'.$linea ]['cactividad'] = $ej->cactividad;
                $alinea[$ej->cproyectocronogramadetalle.'_2_'.$linea ]['tipo'] = '2';
                $alinea[$ej->cproyectocronogramadetalle.'_2_'.$linea ]['ctiponofacturable'] ='';
                $alinea[$ej->cproyectocronogramadetalle.'_2_'.$linea ]['gp'] = '';            
        }
        

        return view('partials.lineaTableHoras',compact('nofacturables','alinea'));

    }
    public function aprobacionHoras($valor){
        return view('timesheet/aprobacionHoras',compact('valor'));
    }

    public function viewAprobacionHoras(Request $request){
        //$proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        $user = Auth::user();
        $objFlow = new FlowSupport();

        $fechaDesde=$request->input('fdesde');
        $fechaHasta=$request->input('fhasta');

        if(substr($fechaDesde,2,1)=='/' || substr($fechaDesde,2,1)=='-'){    
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2));
            $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2));

        }else{
            $fechaDesde = Carbon::create(substr($fechaDesde,0,4),substr($fechaDesde,5,2),substr($fechaDesde,8,2));
            $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2));
        }

        $fechaDesde = $fechaDesde->startOfWeek();
        $fechaHasta = $fechaHasta->endOfWeek();



        //dd($fechaHasta->toDateString());

        $fec_fin=$fechaHasta->toDateString();
        //dd($fechaHasta);
        //$fec_fin='2018-03-23';
        $semana_ini = $fechaDesde->weekOfYear;
        $semana_fin = $fechaHasta->weekOfYear;

        $cantSemanas=0;
        $cantSemanas=($semana_fin-$semana_ini);

        $datainicio=$fechaDesde->toDateString();
        $anio=$fechaHasta->year;
        //dd($fechaHasta);
        $fechaHasta=$fechaHasta->addDay(1);
        $dataFin=$fechaHasta->toDateString(); 

        

        $semana="";
        //$anio="";
        $aprobar = DB::table('tproyectoejecucioncab as cab')
        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
        ->whereBetween('fplanificado',[$datainicio,$dataFin])
        //->where('eje.fplanificado','>=',$datainicio)
        //->where('eje.fplanificado','<=',$dataFin)
        ->whereIn('eje.estado',['3','4'])
        //->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))
        ->select(DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'per.abreviatura','per.cpersona',DB::raw('count(*)'))
        ->groupBy('anio','per.abreviatura','per.cpersona')
        ->orderBy('per.abreviatura')
        ->distinct()
        ->get();

         /*$aprobar = DB::table('tproyectoejecucioncab as cab')
        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')       
        ->where('eje.fplanificado','>=',$fechaDesde)
        ->where('eje.fplanificado','<=',$fechaHasta)   
        ->whereIn('eje.estado',['3','4'])
        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))
        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'per.abreviatura','per.cpersona',DB::raw('count(*)'))
        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.cpersona'))
        ->orderBy('per.cpersona')
        ->get();*/
        //dd($aprobar,$anio,$fechaHasta);
        $i=1;



        $listAprobar=array();
        $condicion="";
        foreach($aprobar as $ap){
            $apro['item'] = $i;
            //$apro['semana'] = $ap->semana;
            $apro['semana'] = '';
            $apro['anio']= $ap->anio;
            $apro['cpersona'] = $ap->cpersona;
            $apro['nomper']=$ap->abreviatura;
            $apro['condicion']='';
            $nroFact=0;
            $nroNFact=0; // verificar los sub -totales
            $nroAdm=0;
            $nroFact = DB::table('tproyectoejecucion as eje')
            ->where('tipo','=','1')
            ->whereIn('eje.estado',['3','4'])
            ->where('cpersona_ejecuta','=',$ap->cpersona)
            //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$ap->semana)
            // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'>=',$semana_ini)
            // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'<=',$semana_fin)
            // ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$ap->anio)
            ->where('eje.fplanificado','>=',$datainicio)
            ->where('eje.fplanificado','<=',$dataFin)

            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                    
            ->sum('horasejecutadas');
            $nroNFact = DB::table('tproyectoejecucion as eje')
            ->where('tipo','=','2')
            ->whereIn('eje.estado',['3','4'])
            ->where('cpersona_ejecuta','=',$ap->cpersona)
            //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$ap->semana)
            // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'>=',$semana_ini)
            // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'<=',$semana_fin)
            // ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$ap->anio)
            ->where('eje.fplanificado','>=',$datainicio)
            ->where('eje.fplanificado','<=',$dataFin)
            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                                
            ->sum('horasejecutadas');
            $nroAdm = DB::table('tproyectoejecucion as eje')
            ->where('tipo','=','3')
            ->whereIn('eje.estado',['3','4'])
            ->where('cpersona_ejecuta','=',$ap->cpersona)
            //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$ap->semana)
            // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'>=',$semana_ini)
            // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'<=',$semana_fin)
            // ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$ap->anio)
            ->where('eje.fplanificado','>=',$datainicio)
            ->where('eje.fplanificado','<=',$dataFin)
            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                                
            ->sum('horasejecutadas');                        
            $apro['nrofact'] = $nroFact;
            $apro['nronfact']  =$nroNFact;
            $apro['nroand'] = $nroAdm;
            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);
            $listAprobar[count($listAprobar)]=$apro;
            $i++;


        }

        $valor='aprobar';

        //dd($cantSemanas);
        if($cantSemanas<2){
            return view('timesheet/aprobacionHoras',compact('listAprobar','semana','anio','semana_ini','semana_fin','valor'));   

        }
        else{
            return 'TotSemanaInvalida';
        }

           
    }

    public function viewRechazarHoras(Request $request){
        $user = Auth::user();
        $objFlow = new FlowSupport();

        $fechaDesde=$request->input('fdesde');
        $fechaHasta=$request->input('fhasta');

        if(substr($fechaDesde,2,1)=='/' || substr($fechaDesde,2,1)=='-'){    
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2));
            $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2));

        }else{
            $fechaDesde = Carbon::create(substr($fechaDesde,0,4),substr($fechaDesde,5,2),substr($fechaDesde,8,2));
            $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2));
        }

        $fechaDesde = $fechaDesde->startOfWeek();
        $fechaHasta = $fechaHasta->endOfWeek();

        $fec_fin=$fechaHasta->toDateString();

        $semana_ini = $fechaDesde->weekOfYear;
        $semana_fin = $fechaHasta->weekOfYear;

        $cantSemanas=0;
        $cantSemanas=($semana_fin-$semana_ini);

        $datainicio=$fechaDesde->toDateString();
        $anio=$fechaHasta->year;
        //dd($fechaHasta);
        $fechaHasta=$fechaHasta->addDay(1);
        $dataFin=$fechaHasta->toDateString(); 



        $cargo= DB::table('tpersonadatosempleado as temp')
            ->where('cpersona','=',$user->cpersona)
            ->first();

        $cargos_aprobar = DB::table('tcargos')       
            ->where('cargoparentdespliegue','=',$cargo->ccargo)
            ->lists('ccargo');

        $proyectos=DB::table('tproyecto as tp')
            ->where('tp.cpersona_gerente', '=' ,$user->cpersona)
            ->lists('cproyecto');

        $aprobarJI=[];
        $aprobarGP=[];

        $semana="";

        if ($proyectos) {

            $aprobarGP = DB::table('tproyectoejecucioncab as cab')
                ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
                ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
                ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
                ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
                ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')
                ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto') 
                ->whereBetween('fplanificado',[$datainicio,$dataFin])   
                ->whereIn('eje.estado',['5']) 
                ->where('eje.tipo','!=','3')  
                ->whereIn('eje.cproyecto',$proyectos)   
                ->select(DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),DB::raw('EXTRACT(week FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(year FROM eje.fplanificado) as anio'),DB::raw('EXTRACT(year FROM eje.fplanificado) as anio'),'per.abreviatura','per.cpersona',DB::raw('count(*)'))
                ->groupBy('anio','per.abreviatura','per.cpersona',DB::raw('EXTRACT(week FROM eje.fplanificado)'))
                ->orderBy('per.abreviatura')
                ->distinct()
                ->get();

            foreach ($aprobarGP as $key => $item) {
                   $item->aprobador='GP';
            }


        }
        
        if ($cargos_aprobar) {

            $aprobarJI = DB::table('tproyectoejecucioncab as cab')
                ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
                ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
                ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
                ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
                ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')
                ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto') 
                ->whereBetween('fplanificado',[$datainicio,$dataFin])   
                ->whereIn('eje.estado',['5']) 
                ->where('eje.tipo','=','3')  
                ->whereIn('tpem.ccargo',$cargos_aprobar)   
                ->select(DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),DB::raw('EXTRACT(week FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(year FROM eje.fplanificado) as anio'),'per.abreviatura','per.cpersona',DB::raw('EXTRACT(year FROM eje.fplanificado) as anio'),DB::raw('count(*)'))
                ->groupBy('anio','per.abreviatura','per.cpersona',DB::raw('EXTRACT(week FROM eje.fplanificado)'))
                ->orderBy('per.abreviatura')
                ->distinct()
                ->get();

            foreach ($aprobarJI as $key => $item) {
                   $item->aprobador='JI';
            }

        }

        $aprobar = array_merge($aprobarJI,$aprobarGP);
        
        //$aprobar = array_unique($aprobar);


        $i=1;



        $listAprobar=array();
        $condicion="";
        foreach($aprobar as $ap){
            $apro['item'] = $i;
            //$apro['semana'] = $ap->semana;
            $apro['semana'] = '';
            $apro['anio']= $ap->anio;
            $apro['cpersona'] = $ap->cpersona;
            $apro['nomper']=$ap->abreviatura;
            $apro['condicion']='';
           
            $nroFactJI = 0;
            $nroFactGP = 0;
            $nroNFactJI = 0;
            $nroNFactGP = 0;
            $nroAdmJI = 0;
            $nroAdmGP = 0;

            if ($ap->aprobador=='JI') {

                $nroFactJI = DB::table('tproyectoejecucion as eje')
                ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')
                ->where('tipo','=','1')
                ->whereIn('eje.estado',['5'])
                ->where('cpersona_ejecuta','=',$ap->cpersona)
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->whereIn('tpem.ccargo',$cargos_aprobar)
                ->sum('horasejecutadas');
                $nroNFactJI = DB::table('tproyectoejecucion as eje')
                ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')
                ->where('tipo','=','2')
                ->whereIn('eje.estado',['5'])
                ->where('cpersona_ejecuta','=',$ap->cpersona)
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->whereIn('tpem.ccargo',$cargos_aprobar)
                ->sum('horasejecutadas');
                $nroAdmJI = DB::table('tproyectoejecucion as eje')
                ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')
                ->where('tipo','=','3')
                ->whereIn('eje.estado',['5'])
                ->where('cpersona_ejecuta','=',$ap->cpersona)
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->whereIn('tpem.ccargo',$cargos_aprobar)
                ->sum('horasejecutadas'); 
            }

            if ($ap->aprobador=='GP') {

                $nroFactGP = DB::table('tproyectoejecucion as eje')
                ->where('tipo','=','1')
                ->whereIn('eje.estado',['5'])
                ->where('cpersona_ejecuta','=',$ap->cpersona)
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->whereIn('eje.cproyecto',$proyectos)  
                ->sum('horasejecutadas');
                $nroNFactGP = DB::table('tproyectoejecucion as eje')
                ->where('tipo','=','2')
                ->whereIn('eje.estado',['5'])
                ->where('cpersona_ejecuta','=',$ap->cpersona)
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->whereIn('eje.cproyecto',$proyectos)  
                ->sum('horasejecutadas');
                $nroAdmGP = DB::table('tproyectoejecucion as eje')
                ->where('tipo','=','3')
                ->whereIn('eje.estado',['5'])
                ->where('cpersona_ejecuta','=',$ap->cpersona)
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->whereIn('eje.cproyecto',$proyectos)  
                ->sum('horasejecutadas');  
            }

            $apro['nrofact'] = $nroFactJI + $nroFactGP;
            $apro['nronfact']  =$nroNFactJI + $nroNFactGP;
            $apro['nroand'] = $nroAdmJI + $nroAdmGP ;
            $apro['total'] = ($apro['nrofact'] + $apro['nronfact'] + $apro['nroand']);
            $listAprobar[count($listAprobar)]=$apro;
            $i++;

        }

        $valor='historial';

        //dd($cantSemanas);
        return view('timesheet/aprobacionHoras',compact('listAprobar','semana','anio','semana_ini','semana_fin','valor')); 
        // if($cantSemanas<2){
        //     return view('timesheet/aprobacionHoras',compact('listAprobar','semana','anio','semana_ini','semana_fin'));   

        // }
        // else{
        //     return 'TotSemanaInvalida';
        // }






    }


    public function verTodasAprobacionHoras(Request $request){
        //$proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();

        $user = Auth::user();
        $objFlow = new FlowSupport();

      
        $aprobar = DB::table('tproyectoejecucioncab as cab')
        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')    
        ->whereIn('eje.estado',['3','4'])     
        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))
        ->select(DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),DB::raw('EXTRACT(week FROM eje.fplanificado) as semana'),'per.abreviatura','per.cpersona',DB::raw('count(*)'))
        ->groupBy('anio','per.abreviatura','per.cpersona',DB::raw('EXTRACT(week FROM eje.fplanificado)'))
        ->orderBy('per.abreviatura')
        ->distinct()
        ->get();

    

        $i=1;



        $listAprobar=array();
        $condicion="";
        foreach($aprobar as $ap){
            $apro['item'] = $i;
            $apro['semana'] = $ap->semana;
          
            $apro['anio']= $ap->anio;
            $apro['cpersona'] = $ap->cpersona;
            $apro['nomper']=$ap->abreviatura;
            $apro['condicion']='';
            $nroFact=0;
            $nroNFact=0; // verificar los sub -totales
            $nroAdm=0;
            $nroFact = DB::table('tproyectoejecucion as eje')
            ->where('tipo','=','1')
            ->whereIn('eje.estado',['3','4'])
            ->where('cpersona_ejecuta','=',$ap->cpersona)
            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$ap->semana)            
            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$ap->anio)   
            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                    
            ->sum('horasejecutadas');

            $nroNFact = DB::table('tproyectoejecucion as eje')
            ->where('tipo','=','2')
            ->whereIn('eje.estado',['3','4'])
            ->where('cpersona_ejecuta','=',$ap->cpersona)
            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$ap->semana)            
            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$ap->anio)   
            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                               
            ->sum('horasejecutadas');

            $nroAdm = DB::table('tproyectoejecucion as eje')
            ->where('tipo','=','3')
            ->whereIn('eje.estado',['3','4'])
            ->where('cpersona_ejecuta','=',$ap->cpersona)
            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$ap->semana)            
            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$ap->anio)   
            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                                
            ->sum('horasejecutadas');    

            

            $apro['nrofact'] = $nroFact;
            $apro['nronfact']  =$nroNFact;
            $apro['nroand'] = $nroAdm;
            $apro['total'] = ($nroFact + $nroNFact + $nroAdm);
            $listAprobar[count($listAprobar)]=$apro;
            $i++;


        }

        $semanatodas='SI';

        $valor='aprobar';


     

         return view('timesheet/aprobacionHoras',compact('listAprobar','semanatodas','valor'));

           
    }

    public function getFechaParaDetalle(Request $request){
        $user = Auth::user();


        $fechaPlani=DB::table('tproyectoejecucion as eje')  
        ->whereIn('eje.estado',['3','4'])
        ->where('cpersona_ejecuta','=',$request->input('cpersona'))
        ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$request->input('semana'))            
        ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$request->input('anio'))   
        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true')) 
        ->first();

        //dd($fechaPlani);

        $fechaDesde=$fechaPlani->fplanificado;
        $fechaHasta=$fechaPlani->fplanificado;

        if(substr($fechaDesde,2,1)=='/' || substr($fechaDesde,2,1)=='-'){    
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2));
            $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2));
        }
        else{
                $fechaDesde = Carbon::create(substr($fechaDesde,0,4),substr($fechaDesde,5,2),substr($fechaDesde,8,2));
                $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2));
            }

            $fechaDesde = $fechaDesde->startOfWeek();
            $fechaHasta = $fechaHasta->endOfWeek();

            $fechaDesde = $fechaDesde->format('d/m/Y');
            $fechaHasta = $fechaHasta->format('d/m/Y');

           
     return array($fechaDesde,$fechaHasta);
        

    }
    
    public function aprobarHorasIn(Request $request){

        $usuario=Auth::user()->nombre;
        $nom=explode('.',$usuario);
        $nombreperson=ucwords($nom[0]).' '.ucwords($nom[1]);
        $objFlow = new FlowSupport();

        DB::beginTransaction();
        $check = $request->input('cproyectoejecucion');
        $n_check = $request->input('n_cproyectoejecucion');
        $txt_obs = $request->input('txt_obs');
        //dd($txt_obs);
        //$textObs = $request->input('textObs');
        //$textObs =  "| Aprobado por ".Auth::user()->nombre." * ".date('d-m-Y')." * => " .$textObs;      
        $textObs="";  
        if($check){
            foreach($check as $chk){
                $tab = DB::table('tproyectoejecucion as eje')
                ->where('eje.cproyectoejecucion','=',$chk)
                ->get();
                foreach($tab as $e){
                    $res = $objFlow->obtenerSiguienteCondicionAut('004','1',$e->ccondicionoperativa);
                    if(count($res)>0){
                        /*DB::table('tproyectoejecucion')
                        ->where('cproyectoejecucion','=',$e->cproyectoejecucion)
                        ->update(['ccondicionoperativa' => $res[0][0] , 'estado' => $res[0][1] ]);*/
                        $conNueva =$res[0][0];
                        if($e->ccondicionoperativa=='VJI'  && $e->tipo=='3'){
                                $conNueva='APR';
                        }
                        /* verificar proximo condicion */
                        if($conNueva=='RGP' && strlen($e->cproyecto)>0 ){
                            $cpersona_gp=0;
                            $tpersona_gp = DB::table('tproyecto')
                            ->where('cproyecto','=',$e->cproyecto)
                            ->first();
                            if($tpersona_gp){
                                $cpersona_gp=$tpersona_gp->cpersona_gerente;
                                if($cpersona_gp==$e->cpersona_ejecuta){
                                    $res=$objFlow->obtenerSiguienteCondicionAut('004','1',$conNueva);
                                }
                            }

                        }
                        /* Verificar proxima condición*/
                        $objFlow->guardarAprobacionTimeSheet($e->cproyectoejecucion,$e->ccondicionoperativa,$conNueva,1,Auth::user()->cpersona,0);                    
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e->cproyectoejecucion)->first();
                        $res2 = array_search($e->cproyectoejecucion,array_map(function($e){return $e[0];},$txt_obs));
                        if(!($res2===FALSE)){
                            $textObs = $nombreperson.' :'.$txt_obs[$res2][1].'||';
                        }
                        $textObs = trim($obj->obsaprobadas) . " ".$textObs;
                        $obj->ccondicionoperativa= $res[0][0];
                        $obj->obsaprobadas = $textObs;
                        $obj->estado = $res[0][1] ;
                        if($e->ccondicionoperativa=='VJI'  && $e->tipo=='3'){
                            $obj->ccondicionoperativa= 'APR';
                            $obj->estado = '5' ;
                        }
                        $obj->horasaprobadas=$e->horasejecutadas;
                        $obj->faprobacion=Carbon::now();
                        $obj->save();                    
                    }
                }

            }
        }
        if($n_check){
            foreach($n_check as $chk){
                $tab = DB::table('tproyectoejecucion as eje')
                ->where('eje.cproyectoejecucion','=',$chk)
                ->get();
                foreach($tab as $e){
                    $res = $objFlow->obtenerSiguienteCondicionAut('004','1',$e->ccondicionoperativa);
                    if(count($res)>0){

                        $objFlow->guardarAprobacionTimeSheet($e->cproyectoejecucion,$e->ccondicionoperativa,$res[1][0],1,Auth::user()->cpersona,0);                    
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e->cproyectoejecucion)->first();
                        $res2 = array_search($e->cproyectoejecucion,array_map(function($e){return $e[0];},$txt_obs));
                        if(!($res2===false)){
                           $textObs = $nombreperson.' :'.$txt_obs[$res2][1].'||';
                        }                        
                        $textObs = $obj->obsaprobadas . " ".$textObs;
                        $obj->ccondicionoperativa= $res[1][0];
                        $obj->obsaprobadas =$textObs;
                        $obj->estado = $res[1][1] ;
                        $obj->horasobservadas=$e->horasejecutadas;
                        $obj->faprobacion=Carbon::now();   
                        $obj->save();                    
                    }
                }
            
            }
        }
        DB::commit();
        
        return ;
    }
    /*public function observarHorasIn(Request $request){
        DB::beginTransaction();
        $check = $request->input('cproyectoejecucion');
        $objFlow = new FlowSupport();
        $textObs = $request->input('textObs');
        $textObs =  "| Observado  ".Auth::user()->nombre." * ".date('d-m-Y')." * => " .$textObs;
        foreach($check as $chk){
            $tab = DB::table('tproyectoejecucion as eje')
            ->where('eje.cproyectoejecucion','=',$chk)
            ->get();
            foreach($tab as $e){
                $res = $objFlow->obtenerSiguienteCondicionAut('004','1',$e->ccondicionoperativa);
                if(count($res)>0){

                    $objFlow->guardarAprobacionTimeSheet($e->cproyectoejecucion,$e->ccondicionoperativa,$res[1][0],1,Auth::user()->cpersona,0);                    
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e->cproyectoejecucion)->first();
                    $textObs = $obj->obsobservadas . " ".$textObs;
                    $obj->ccondicionoperativa= $res[1][0];
                    $obj->obsobservadas = $textObs;
                    $obj->estado = $res[1][1] ;
                    $obj->save();                    
                }
            }

        }
        
        DB::commit();
        
        return ;
    }*/

    public function aprobarHorasIn_rechazar(Request $request){

        $usuario=Auth::user()->nombre;
        $nom=explode('.',$usuario);
        $nombreperson=ucwords($nom[0]).' '.ucwords($nom[1]);
        $objFlow = new FlowSupport();

        DB::beginTransaction();
        $check = $request->input('cproyectoejecucion');
        $n_check = $request->input('n_cproyectoejecucion');
        $txt_obs = $request->input('txt_obs');


        //dd($txt_obs);
        //$textObs = $request->input('textObs');
        //$textObs =  "| Aprobado por ".Auth::user()->nombre." * ".date('d-m-Y')." * => " .$textObs;      
        $textObs="";  
        if($n_check){
            foreach($n_check as $chk){
                $tab = DB::table('tproyectoejecucion as eje')
                ->where('eje.cproyectoejecucion','=',$chk)
                ->get();
        //dd($n_check,$tab);
                foreach($tab as $e){
                    $res = $objFlow->obtenerSiguienteCondicionAut('004','1',$e->ccondicionoperativa);
                    if(count($res)>0){

                        $objFlow->guardarAprobacionTimeSheet($e->cproyectoejecucion,$e->ccondicionoperativa,$res[1][0],1,Auth::user()->cpersona,0);                    
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e->cproyectoejecucion)->first();
                        $res2 = array_search($e->cproyectoejecucion,array_map(function($e){return $e[0];},$txt_obs));
                        if(!($res2===false)){
                           $textObs = $nombreperson.' :'.$txt_obs[$res2][1].'||';
                        }                        
                        $textObs = $obj->obsaprobadas . " ".$textObs;
                        $obj->ccondicionoperativa= $res[1][0];
                        $obj->obsobservadas =$textObs;
                        $obj->estado = $res[1][1] ;
                        $obj->horasobservadas=$e->horasejecutadas;
                        $obj->faprobacion=Carbon::now();   
                        $obj->save();                    
                    }
                }
            
            }
        }
      
        DB::commit();
        
        return ;
    }
    
    public function verDetalleHoras(Request $request){
       

        $objAct  = new ActividadSupport();
        $user = Auth::user();
        //$semana = $request->input('semana');
        $anio = $request->input('anio');
        $cpersona = $request->input('cpersona');
      
        $semana_ini = $request->input('semana_ini');
        $semana_fin = $request->input('semana_fin');

        $finicio= $request->input('fecha_inicio');
        $fultim=$request->input('fecha_fin');

        $finicio=substr($finicio,6,4).'-'.substr($finicio,3,2).'-'.substr($finicio,0,2);
        $fultim=substr($fultim,6,4).'-'.substr($fultim,3,2).'-'.substr($fultim,0,2);

   

        $week_start = new DateTime($finicio);
        //$week_start->setISODate($anio,$semana_ini);
        $fdesde = $week_start->format('d-m-Y');     
        

        $week_end = new DateTime($fultim);
        //$week_end->setISODate($anio,$semana_fin);        
        $fhasta = $week_end->format('d-m-Y'); 



         //7$fdesde = $request->input('fecha_inicio');
        // $fhasta = $request->input('fecha_fin');


       

        $tarea = DB::table('tpersonadatosempleado as e')           
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')      
        ->first();
        $carea=0;
        
        
        if($tarea){
            $carea = $tarea->carea;
            
        }  

        
        $fdesde = Carbon::create(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2),0,0,0);
        $fdesde = $fdesde->startOfWeek();
        $fhasta = Carbon::create(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2),23,59,59);
        $fhasta = $fhasta->endOfWeek();
        
        $datainicio=$fdesde->toDateString(); 
   
        $seguir = true;
        $dias= array();
        $dia = $fdesde;
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        while($seguir){
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek]);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT) ."". str_pad($dia->day,2,'0',STR_PAD_LEFT)) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT) ."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT) ) ) ) {
                $seguir=false;    
            }

            
        } 
        //dd($dias);
                   
        $tpersona = DB::table('tpersona as per')
        ->where('per.cpersona','=',$cpersona)
        ->first();
        $per_nombre="";
        if($tpersona){
            $per_nombre = $tpersona->abreviatura;
        }

        $listHoras = array();
        
        $fechafin_temp=$fhasta->addDay(1);
        $dataFin=$fhasta->toDateString();

        $tab = DB::table('tproyectoejecucioncab as cab')
        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
        ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')        
        ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')        
        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
        //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$semana)
        //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'>=',$semana_ini)
        //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'<=',$semana_fin)
        //->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
        //->whereBetween('eje.fplanificado',[$finicio,$fultim])
        ->where('eje.fplanificado','>=',$datainicio)
        ->where('eje.fplanificado','<=',$dataFin)
        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))        
        ->where('eje.cpersona_ejecuta','=',$cpersona)
        ->whereIn('eje.estado',[3,4])
        ->select('eje.cproyectoactividades','eje.tipo','eje.ctiponofacturable','pryact.descripcionactividad','eje.ccondicionoperativa')
        ->addSelect('tpc.abreviatura as cliente','tum.nombre as uminera','eje.cactividad','act.descripcion')
        ->addSelect('pry.cproyecto','pry.nombre','pry.codigo','pryact.codigoactvidad','cab.cproyectoejecucioncab','pry.cpersona_gerente as codgp')
        ->distinct()
        ->orderBy("eje.tipo","DESC")
        ->orderBy("pry.codigo","ASC")
        ->orderBy('pryact.codigoactvidad','ASC')   
        ->get();   

      //  print_r($tab);
        //dd('',$tab,$datainicio,$dataFin,$fdesde,$fhasta,$finicio,$fultim);
        

        $historialApro=array();
        $historialCom=array(); 

        $i=0;

        $dia1=0;
        $dia2=0;
        $dia3=0;
        $dia4=0;
        $dia5=0;
        $dia6=0;
        $dia7=0;
        $total=0;
        $totales=array();   
         $sumdias=array();
              
        foreach($tab as $e){
            $i++;
             $sumhoract=0;

            $h['item'] = $i;
            $h['cproyecto'] = $e->cproyecto;
            $h['codigo'] = $e->codigo;
            $h['nombreproy'] = $e->nombre;
            $h['ccondicionoperativa'] = $e->ccondicionoperativa;
            $h['codigoactividad']=$e->codigoactvidad;
            $h['personanombre']= $e->cliente;   
            $h['unidadminera']= $e->uminera;                     
            $h['descripcionactividad'] = ($e->nombre==null?'':$e->nombre.' / ') . ($e->codigoactvidad==null?'':$e->codigoactvidad.' / ').($e->descripcionactividad==null?$e->descripcion:$e->descripcionactividad);
            $h['tipo'] = $e->tipo;
            $h['ctiponofacturable'] = $e->ctiponofacturable;
            $h['des_ctiponofacturable'] = '';
            if(!is_null($e->ctiponofacturable)){
                $nf = DB::table('ttiposnofacturables as nf')
                ->where('ctiponofacturable','=',$e->ctiponofacturable)
                ->first();
                if($nf){
                    $h['des_ctiponofacturable'] = $nf->descripcion;
                }
            }
            //dd($h);
            //-----Inicio Barras de avance-----//

           /* if($e->tipo!=3){

                $h['horaseje'] = $objAct->sumHorasEjecutadasProy($e->cproyectoactividades);
                $tot = $objAct->sumHorasPresupuestadasProy($e->cproyectoactividades);
                $h['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($h['horaseje']/$tot->suma)*100,2) );

                $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($e->cproyectoactividades,$e->cproyecto);
                $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($e->cproyectoactividades,$e->cproyecto);

                $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;

                $h['porav_costo']=round($porcentajeCosto,2);

            }

            else{
                $h['horaseje'] = 0;
                $h['porav'] = 0;
                $h['porav_costo']=0;
            }*/

            if($e->tipo!=3){

                if($user->cpersona==$e->codgp){

                    $h['horaseje'] = $objAct->sumHorasEjecutadasProy($e->cproyectoactividades);
                    $tot = $objAct->sumHorasPresupuestadasProy($e->cproyectoactividades);
                    $h['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($h['horaseje']/$tot->suma)*100) );

                    $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($e->cproyectoactividades,$e->cproyecto);
                    $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($e->cproyectoactividades,$e->cproyecto);

                    /* Inicio Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal) */

                        if($tot_costo_presup==0|| is_null($tot_costo_presup)){
                           // dd($k->cproyecto,$k->cproyectoactividades,$k->cproyectoejecucioncab);

                             $porcentajeCosto=-1;
                        }

                        else{
                            $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                        }

                        /* Fin Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal)*/

                    $h['porav_costo']=round($porcentajeCosto);

                    

                    if($porcentajeCosto==-1){
                        $h['mensajehoras']='Sin horas';
                        $h['mensajecosto']='Sin monto';

                    }
                    else{
                        $h['mensajecosto']=$h['porav_costo'].'%';
                        $h['mensajehoras']=$h['porav'].'%';  
                    }

                }

                else{


                    $h['horaseje'] = $objAct->sumHorasEjecutadasProyArea($e->cproyectoactividades,$carea);
                    $tot = $objAct->sumHorasPresupuestadasProyArea($e->cproyectoactividades,$carea);
                    $h['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($h['horaseje']/$tot->suma)*100) );

                    $tot_costo_presup=$objAct->sumCostoPresupuestadoProyArea($e->cproyectoactividades,$e->cproyecto,$carea);
                    $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProyArea($e->cproyectoactividades,$e->cproyecto,$carea);

                   // dd($tot_costo_ejecutado,$tot_costo_presup, $h['porav']);

                    if($tot_costo_presup==0|| is_null($tot_costo_presup)){

                        $h['horaseje'] = $objAct->sumHorasEjecutadasProy($e->cproyectoactividades);
                        $tot = $objAct->sumHorasPresupuestadasProy($e->cproyectoactividades);
                        $h['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($h['horaseje']/$tot->suma)*100) );

                        $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($e->cproyectoactividades,$e->cproyecto);
                        $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($e->cproyectoactividades,$e->cproyecto);

                        /* Inicio Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal) */

                        if($tot_costo_presup==0|| is_null($tot_costo_presup)){
                           // dd($k->cproyecto,$k->cproyectoactividades,$k->cproyectoejecucioncab);

                             $porcentajeCosto=-1;
                        }

                        else{
                            $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                        }

                    /* Fin Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal)*/

                        $h['porav_costo']=round($porcentajeCosto);

                

                        if($porcentajeCosto==-1){
                            $h['mensajehoras']='Sin horas';
                            $h['mensajecosto']='Sin monto';

                        }
                        else{
                            $h['mensajecosto']=$h['porav_costo'].'%';
                            $h['mensajehoras']=$h['porav'].'%';  
                        }

                        //$porcentajeCosto=100;

                    }
                    else{

                        $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                        $h['porav_costo']=round($porcentajeCosto);
                        

                        $h['mensajehoras']=$h['porav'].'%';  
                        $h['mensajecosto']=$h['porav_costo'].'%';       

                    }              
                
                }

            }

            else{
                $h['horaseje'] = 0;
                $h['porav'] = 0;
                $h['porav_costo']=0;
                $h['mensajehoras']='';  
                $h['mensajecosto']=''; 
            }

            //-----Fin Barras de avance-----//
            
            $h['deta']= array();
            $tab_d = DB::table('tproyectoejecucion as eje')
            ->where('eje.fplanificado','>=',$datainicio)
            ->where('eje.fplanificado','<=',$dataFin)
            //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'>=',$semana_ini)
            //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'<=',$semana_fin)
            //->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$semana)
            //->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$anio)
            ->where('eje.cpersona_ejecuta','=',$cpersona);
            if($h['cproyecto']){
                $tab_d=$tab_d->where('eje.cproyectoactividades','=',$e->cproyectoactividades);
                $tab_d=$tab_d->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
            }else{
                $tab_d=$tab_d->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
                $tab_d=$tab_d->where('eje.cactividad','=',$e->cactividad);
            }
            

            $tab_d=$tab_d->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))              
            ->where('eje.tipo','=',$e->tipo);
            if(is_null($e->ctiponofacturable)){
                $tab_d = $tab_d->whereNull('eje.ctiponofacturable');
            }else{
                $tab_d = $tab_d->where('eje.ctiponofacturable','=',$e->ctiponofacturable);
            }

            $tab_d=$tab_d->whereIn('eje.estado',[3,4])
            ->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as key'),DB::raw('to_char(eje.fplanificado,\'D\' ) as dia'))
            ->get();    

            //dd('',$tab_d);

           
            foreach($tab_d as $d){

                 /*
                Obtener Historial de Aprobaciones
                */
                $taprob = DB::table('tflujoaprobacion as apro')
                ->leftJoin('tpersona as per','apro.cpersona_aprobado','=','per.cpersona')
                ->leftJoin('tproyectoejecucion as pe','apro.cproyectoejecucion','=','pe.cproyectoejecucion')
                ->leftJoin('tproyecto as p','pe.cproyecto','=','p.cproyecto')
                ->leftJoin('tunidadminera as tu','p.cunidadminera','=','tu.cunidadminera')
                ->leftJoin('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                ->where('apro.cproyectoejecucion','=',$d->cproyectoejecucion)
                ->select('apro.*','per.nombre as emp','p.codigo as codproy','p.nombre as nomproy','tu.nombre as uminera','pa.codigoactvidad')
                ->orderBy('apro.fregistro','asc')
                ->get();
                //dd('',$taprob);
                foreach($taprob as $aprob){
                    $his['cproyectoejecucion']=$d->cproyectoejecucion;
                    $his['fecha']=$aprob->fregistro;
                    $his['ccondicionoperativa']=$aprob->ccondicionoperativa_aprobado;
                    $his['ccondicionoperativa_sig']=$aprob->ccondicionoperativa_siguiente;
                    $his['aprobado']=$aprob->emp;

                     if(is_null($aprob->codproy)||strlen($aprob->codproy)<=0){
                        $his['actividad']=$e->descripcionactividad;   
                    }
                    else{
                        $his['actividad']=$aprob->codproy.'//'.$e->uminera.'/'.$e->nombre.'/'.$e->codigoactvidad.'/'.$e->descripcionactividad;
                    }
                    //$his['actividad']=($e->descripcionactividad==null?$e->descripcion:$e->descripcionactividad);
                    $his['horas']=$d->horasejecutadas;   

                    $historialApro[count($historialApro)] = $his;

                }
        
                /*  Fin Historial de aprobaciones*/

                /*
                Obtener Historial de Comentarios
                */
                // print_r($d);
                if( strlen($d->obsplanificada)>0 || strlen($d->obsejecutadas)>0 || strlen($d->obsobservadas)>0 ){
                    $hic['cproyectoejecucion']=$d->cproyectoejecucion;
                    $hic['actividad'] = ($e->codigo==null?'':$e->codigo.' / ') . ($e->cliente==null?'':$e->cliente.' / ') . ($e->uminera==null?'':$e->uminera.' / ') . ($e->nombre==null?'':$e->nombre.' / ') . ($e->codigoactvidad==null?'':$e->codigoactvidad.' : ').($e->descripcionactividad==null?$e->descripcion:$e->descripcionactividad);
                    $hic['fecha']=$d->fplanificado;
                    $hic['comentariosPla']=$d->obsplanificada;
                    $hic['comentariosEje']=$d->obsejecutadas;
                    $hic['comentariosObs']=$d->obsobservadas;
                    $hic['horas']=$d->horasejecutadas;
                    $historialCom[count($historialCom)] = $hic;
                }
                /*  Fin Historial de comentarios*/ 



                $hh['key']= $d->key;
                $hh['fecha'] = $d->fplanificado;
                $hh['nrohoras_pla'] = $d->horasplanificadas;
                $hh['obsplanificada'] = $d->obsplanificada;
                $hh['nrohoras_eje'] = $d->horasejecutadas;
                $hh['obsejecutadas'] = $d->obsejecutadas;

                $hh['obsaprobadas'] = $d->obsaprobadas;
                $hh['obsobservadas'] = $d->obsobservadas;
                $hh['cproyectoejecucion'] = $d->cproyectoejecucion;
                $h['deta'][$d->key]=$hh;
            
                $sumhoract=$sumhoract+$hh['nrohoras_eje'];                  

                if($d->dia==1){
                    $dia1=$dia1+$hh['nrohoras_eje'];
                }
                if($d->dia==2){
                    $dia2=$dia2+$hh['nrohoras_eje'];
                }
                if($d->dia==3){
                    $dia3=$dia3+$hh['nrohoras_eje'];
                }
                if($d->dia==4){
                    $dia4=$dia4+$hh['nrohoras_eje'];
                }
                if($d->dia==5){
                    $dia5=$dia5+$hh['nrohoras_eje'];
                }
                if($d->dia==6){
                    $dia6=$dia6+$hh['nrohoras_eje'];
                }
                if($d->dia==7){
                    $dia7=$dia7+$hh['nrohoras_eje'];
                }

            }

            $total=$dia1+$dia2+$dia3+$dia4+$dia5+$dia6+$dia7;

            $h['thoraact']=$sumhoract;
            $listHoras[count($listHoras)] = $h;
            

            /* INICIO ACUMULADO DE HORAS POR DIA */
            
            foreach ($dias as $di) {       
             
                $tab = DB::table('tproyectoejecucion as eje')
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->where('eje.cpersona_ejecuta','=',$cpersona);
                if($h['cproyecto']){
                    $tab=$tab->where('eje.cproyectoactividades','=',$e->cproyectoactividades);
                    $tab=$tab->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
                }else{
                    $tab=$tab->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
                    $tab=$tab->where('eje.cactividad','=',$e->cactividad);
                }
                $tab=$tab->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))              
                ->where('eje.tipo','=',$e->tipo);
                if(is_null($e->ctiponofacturable)){
                    $tab = $tab->whereNull('eje.ctiponofacturable');
                }else{
                    $tab = $tab->where('eje.ctiponofacturable','=',$e->ctiponofacturable);
                }

                $tab=$tab->whereIn('eje.estado',[3,4])
                ->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as key'),DB::raw('to_char(eje.fplanificado,\'D\' ) as dia'))
                ->get();  

                 //dd('',$e,$tab); 
            
                $dd['key'] = str_replace('-','',$di[0]);
                $dd['dia'] = $di[0];
                $dd['sumdia'] =0;

                $dia=0;

                foreach ($tab as $t) {

                    if ($t->key==str_replace('-','',$di[0])){     
                        //dd('',$t);                     
                        $dd['sumdia'] = $dia+$t->horasejecutadas;  
                        $dia=$dd['sumdia'];
                                            
                    }                    
                } 
                 $sumdias[count($sumdias)]=$dd;             
            }
        }
        /* FIN ACUMULADO DE HORAS POR DIA */

        /* INICIO REORDENANDO ARRAY SEGUN KEY*/
        foreach ($sumdias as $key => $orden) {            
            $aux[$key]=$orden['key'];            
        }
        $array=array_multisort($aux,SORT_ASC,$sumdias);
        /* FIN REORDENANDO ARRAY SEGUN KEY*/

        /* INICIO CONTEO DE HORAS SEGUN KEY REPETIDO */      
        $k='';

        foreach ($sumdias as $sd) {           

            if($k!=$sd['key']){                 

                 $td['key']=$sd['key'];     
                 $td['dia']=$sd['dia'];
                 $horadia=0;        

                foreach ($sumdias as $d) {

                    if($sd['key']==$d['key']){

                        $horadia=$d['sumdia']+$horadia;                                                      

                    }                
                }

                $td['sumdia']=$horadia;
                $totales[count($totales)]=$td;
            }

            $k=$sd['key'];            
        }
        /* FIN CONTEO DE HORAS SEGUN KEY REPETIDO */
        
        return view('partials.modalVerHoras',compact('listHoras','dias','per_nombre','anio','semana_ini','semana_fin','dia1','dia2','dia3','dia4','dia5','dia6','dia7','total','historialApro','historialCom','totales'));
    }

    public function verDetalleHorasRechazar(Request $request){

        $objAct  = new ActividadSupport();
        $user = Auth::user();
        //$semana = $request->input('semana');
        $anio = $request->input('anio');
        $cpersona = $request->input('cpersona');
      
        $semana_ini = $request->input('semana_ini');
        $semana_fin = $request->input('semana_fin');

        $finicio= $request->input('fecha_inicio');
        $fultim=$request->input('fecha_fin');

        $finicio=substr($finicio,6,4).'-'.substr($finicio,3,2).'-'.substr($finicio,0,2);
        $fultim=substr($fultim,6,4).'-'.substr($fultim,3,2).'-'.substr($fultim,0,2);
        $week_start = new DateTime($finicio);
        //dd($week_start);
        $fdesde = $week_start->format('d-m-Y');     
        
        $week_end = new DateTime($fultim);
        $fhasta = $week_end->format('d-m-Y'); 
       

        $tarea = DB::table('tpersonadatosempleado as e')           
        ->where('cpersona','=',$user->cpersona)
        ->orderBy('fingreso','DESC')      
        ->first();
        $carea=0;
        
        if($tarea){
            $carea = $tarea->carea;
            
        }  

        
        $fdesde = Carbon::create(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2),0,0,0);
        $fdesde = $fdesde->startOfWeek();
        $fhasta = Carbon::create(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2),23,59,59);
        $fhasta = $fhasta->endOfWeek();
        
        $datainicio=$fdesde->toDateString(); 
   
        $seguir = true;
        $dias= array();
        $dia = $fdesde;
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        while($seguir){
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek]);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT) ."". str_pad($dia->day,2,'0',STR_PAD_LEFT)) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT) ."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT) ) ) ) {
                $seguir=false;    
            }
        } 

        $tpersona = DB::table('tpersona as per')
        ->where('per.cpersona','=',$cpersona)
        ->first();
        $per_nombre="";
        if($tpersona){
            $per_nombre = $tpersona->abreviatura;
        }

        $cargo= DB::table('tpersonadatosempleado as temp')
            ->where('cpersona','=',$user->cpersona)
            ->first();

        $cargos_aprobar = DB::table('tcargos')       
            ->where('cargoparentdespliegue','=',$cargo->ccargo)
            ->lists('ccargo');

        $proyectos=DB::table('tproyecto as tp')
            ->where('tp.cpersona_gerente', '=' ,$user->cpersona)
            ->lists('cproyecto');

        
                   

        $listHoras = array();
        
        $fechafin_temp=$fhasta->addDay(1);
        $dataFin=$fhasta->toDateString();

        $aprobarJI=[];
        $aprobarGP=[];

        if ($proyectos) {

            $aprobarGP = DB::table('tproyectoejecucioncab as cab')
            ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
            ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
            ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
            ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')        
            ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')        
            ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
                    ->where('eje.fplanificado','>=',$datainicio)
            ->where('eje.fplanificado','<=',$dataFin)
            ->where('eje.cpersona_ejecuta','=',$cpersona)
            ->where('eje.tipo','!=','3')
            ->whereIn('eje.estado',[5])
            ->whereIn('eje.cproyecto',$proyectos)   
            ->select('eje.cproyectoactividades','eje.tipo','eje.ctiponofacturable','pryact.descripcionactividad','eje.ccondicionoperativa')
            ->addSelect('tpc.abreviatura as cliente','tum.nombre as uminera','eje.cactividad','act.descripcion')
            ->addSelect('pry.cproyecto','pry.nombre','pry.codigo','pryact.codigoactvidad','cab.cproyectoejecucioncab','pry.cpersona_gerente as codgp')
            ->distinct()
            ->orderBy("eje.tipo","DESC")
            ->orderBy("pry.codigo","ASC")
            ->orderBy('pryact.codigoactvidad','ASC')   
            ->get();   

            foreach ($aprobarGP as $key => $item) {
                   $item->aprobador='GP';
            }

        }

        if ($cargos_aprobar) {

            $aprobarJI = DB::table('tproyectoejecucioncab as cab')
            ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
            ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
            ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
            ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')   
            ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')     
            ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')
            ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
            ->where('eje.fplanificado','>=',$datainicio)
            ->where('eje.fplanificado','<=',$dataFin)
            ->where('eje.cpersona_ejecuta','=',$cpersona)
            ->whereIn('eje.estado',[5])
            ->where('eje.tipo','=','3')
            ->whereIn('tpem.ccargo',$cargos_aprobar) 
            ->select('eje.cproyectoactividades','eje.tipo','eje.ctiponofacturable','pryact.descripcionactividad','eje.ccondicionoperativa')
            ->addSelect('tpc.abreviatura as cliente','tum.nombre as uminera','eje.cactividad','act.descripcion')
            ->addSelect('pry.cproyecto','pry.nombre','pry.codigo','pryact.codigoactvidad','cab.cproyectoejecucioncab','pry.cpersona_gerente as codgp')
            ->distinct()
            ->orderBy("eje.tipo","DESC")
            ->orderBy("pry.codigo","ASC")
            ->orderBy('pryact.codigoactvidad','ASC')   
            ->get();   

            foreach ($aprobarJI as $key => $item) {
                   $item->aprobador='JI';
            }
            
        }

        $tab = array_merge($aprobarJI,$aprobarGP);


        $historialApro=array();
        $historialCom=array(); 

        $i=0;

        $dia1=0;
        $dia2=0;
        $dia3=0;
        $dia4=0;
        $dia5=0;
        $dia6=0;
        $dia7=0;
        $total=0;
        $totales=array();   
         $sumdias=array();
              
        foreach($tab as $e){
            $i++;
             $sumhoract=0;

            $h['item'] = $i;
            $h['cproyecto'] = $e->cproyecto;
            $h['codigo'] = $e->codigo;
            $h['nombreproy'] = $e->nombre;
            $h['ccondicionoperativa'] = $e->ccondicionoperativa;
            $h['codigoactividad']=$e->codigoactvidad;
            $h['personanombre']= $e->cliente;   
            $h['unidadminera']= $e->uminera;                     
            $h['descripcionactividad'] = ($e->nombre==null?'':$e->nombre.' / ') . ($e->codigoactvidad==null?'':$e->codigoactvidad.' / ').($e->descripcionactividad==null?$e->descripcion:$e->descripcionactividad);
            $h['tipo'] = $e->tipo;
            $h['ctiponofacturable'] = $e->ctiponofacturable;
            $h['des_ctiponofacturable'] = '';
            if(!is_null($e->ctiponofacturable)){
                $nf = DB::table('ttiposnofacturables as nf')
                ->where('ctiponofacturable','=',$e->ctiponofacturable)
                ->first();
                if($nf){
                    $h['des_ctiponofacturable'] = $nf->descripcion;
                }
            }
            
            if($e->tipo!=3){

                if($user->cpersona==$e->codgp){

                $h['horaseje'] = $objAct->sumHorasEjecutadasProy($e->cproyectoactividades);
                $tot = $objAct->sumHorasPresupuestadasProy($e->cproyectoactividades);
                $h['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($h['horaseje']/$tot->suma)*100) );

                $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($e->cproyectoactividades,$e->cproyecto);
                $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($e->cproyectoactividades,$e->cproyecto);

                /* Inicio Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal) */

                    if($tot_costo_presup==0|| is_null($tot_costo_presup)){
                       // dd($k->cproyecto,$k->cproyectoactividades,$k->cproyectoejecucioncab);

                         $porcentajeCosto=-1;
                    }

                    else{
                        $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    }

                    /* Fin Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal)*/

                $h['porav_costo']=round($porcentajeCosto);

                

                if($porcentajeCosto==-1){
                    $h['mensajehoras']='Sin horas';
                    $h['mensajecosto']='Sin monto';

                }
                else{
                    $h['mensajecosto']=$h['porav_costo'].'%';
                    $h['mensajehoras']=$h['porav'].'%';  
                }


                }

                else{


                    $h['horaseje'] = $objAct->sumHorasEjecutadasProyArea($e->cproyectoactividades,$carea);
                    $tot = $objAct->sumHorasPresupuestadasProyArea($e->cproyectoactividades,$carea);
                    $h['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($h['horaseje']/$tot->suma)*100) );

                    $tot_costo_presup=$objAct->sumCostoPresupuestadoProyArea($e->cproyectoactividades,$e->cproyecto,$carea);
                    $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProyArea($e->cproyectoactividades,$e->cproyecto,$carea);

                   // dd($tot_costo_ejecutado,$tot_costo_presup, $h['porav']);

                    if($tot_costo_presup==0|| is_null($tot_costo_presup)){

                        $h['horaseje'] = $objAct->sumHorasEjecutadasProy($e->cproyectoactividades);
                        $tot = $objAct->sumHorasPresupuestadasProy($e->cproyectoactividades);
                        $h['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($h['horaseje']/$tot->suma)*100) );

                        $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($e->cproyectoactividades,$e->cproyecto);
                        $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($e->cproyectoactividades,$e->cproyecto);

                        /* Inicio Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal) */

                        if($tot_costo_presup==0|| is_null($tot_costo_presup)){
                           // dd($k->cproyecto,$k->cproyectoactividades,$k->cproyectoejecucioncab);

                             $porcentajeCosto=-1;
                        }

                        else{
                            $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                        }

                    /* Fin Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal)*/

                        $h['porav_costo']=round($porcentajeCosto);

                

                        if($porcentajeCosto==-1){
                            $h['mensajehoras']='Sin horas';
                            $h['mensajecosto']='Sin monto';

                        }
                        else{
                            $h['mensajecosto']=$h['porav_costo'].'%';
                            $h['mensajehoras']=$h['porav'].'%';  
                        }

                        //$porcentajeCosto=100;

                    }
                    else{

                        $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                        $h['porav_costo']=round($porcentajeCosto);
                        

                        $h['mensajehoras']=$h['porav'].'%';  
                        $h['mensajecosto']=$h['porav_costo'].'%';       

                    }              
                
                }

            }

            else{
                $h['horaseje'] = 0;
                $h['porav'] = 0;
                $h['porav_costo']=0;
                $h['mensajehoras']='';  
                $h['mensajecosto']=''; 
            }

            //-----Fin Barras de avance-----//
            
            $h['deta']= array();
            $tab_d = DB::table('tproyectoejecucion as eje')
            ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')
            ->where('eje.fplanificado','>=',$datainicio)
            ->where('eje.fplanificado','<=',$dataFin)
            ->where('eje.cpersona_ejecuta','=',$cpersona);
            if($h['cproyecto']){
                $tab_d=$tab_d->where('eje.cproyectoactividades','=',$e->cproyectoactividades);
                $tab_d=$tab_d->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
            }else{
                $tab_d=$tab_d->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
                $tab_d=$tab_d->where('eje.cactividad','=',$e->cactividad);
            }
            
            if ($e->aprobador=='JI') {
                $tab_d=$tab_d->whereIn('tpem.ccargo',$cargos_aprobar)
                ->where('eje.tipo','=','3');
            }
            if ($e->aprobador=='GP') {
                $tab_d=$tab_d->whereIn('eje.cproyecto',$proyectos)
                ->where('eje.tipo','!=','3');
            }

                     
            $tab_d=$tab_d->where('eje.tipo','=',$e->tipo);
            if(is_null($e->ctiponofacturable)){
                $tab_d = $tab_d->whereNull('eje.ctiponofacturable');
            }else{
                $tab_d = $tab_d->where('eje.ctiponofacturable','=',$e->ctiponofacturable);
            }

            $tab_d=$tab_d->whereIn('eje.estado',[5])
            ->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as key'),DB::raw('to_char(eje.fplanificado,\'D\' ) as dia'))
            ->get();    


           
            foreach($tab_d as $d){

                 /*
                Obtener Historial de Aprobaciones
                */
                $taprob = DB::table('tflujoaprobacion as apro')
                ->leftJoin('tpersona as per','apro.cpersona_aprobado','=','per.cpersona')
                ->leftJoin('tproyectoejecucion as pe','apro.cproyectoejecucion','=','pe.cproyectoejecucion')
                ->leftJoin('tproyecto as p','pe.cproyecto','=','p.cproyecto')
                ->leftJoin('tunidadminera as tu','p.cunidadminera','=','tu.cunidadminera')
                ->leftJoin('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                ->where('apro.cproyectoejecucion','=',$d->cproyectoejecucion)
                ->select('apro.*','per.nombre as emp','p.codigo as codproy','p.nombre as nomproy','tu.nombre as uminera','pa.codigoactvidad')
                ->orderBy('apro.fregistro','asc')
                ->get();
                //dd('',$taprob);
                foreach($taprob as $aprob){
                    $his['cproyectoejecucion']=$d->cproyectoejecucion;
                    $his['fecha']=$aprob->fregistro;
                    $his['ccondicionoperativa']=$aprob->ccondicionoperativa_aprobado;
                    $his['ccondicionoperativa_sig']=$aprob->ccondicionoperativa_siguiente;
                    $his['aprobado']=$aprob->emp;

                     if(is_null($aprob->codproy)||strlen($aprob->codproy)<=0){
                        $his['actividad']=$e->descripcionactividad;   
                    }
                    else{
                        $his['actividad']=$aprob->codproy.'//'.$e->uminera.'/'.$e->nombre.'/'.$e->codigoactvidad.'/'.$e->descripcionactividad;
                    }
                    //$his['actividad']=($e->descripcionactividad==null?$e->descripcion:$e->descripcionactividad);
                    $his['horas']=$d->horasejecutadas;   

                    $historialApro[count($historialApro)] = $his;

                }
        
                /*  Fin Historial de aprobaciones*/

                /*
                Obtener Historial de Comentarios
                */
                // print_r($d);
                if( strlen($d->obsplanificada)>0 || strlen($d->obsejecutadas)>0 || strlen($d->obsobservadas)>0 ){
                    $hic['cproyectoejecucion']=$d->cproyectoejecucion;
                    $hic['actividad'] = ($e->codigo==null?'':$e->codigo.' / ') . ($e->cliente==null?'':$e->cliente.' / ') . ($e->uminera==null?'':$e->uminera.' / ') . ($e->nombre==null?'':$e->nombre.' / ') . ($e->codigoactvidad==null?'':$e->codigoactvidad.' : ').($e->descripcionactividad==null?$e->descripcion:$e->descripcionactividad);
                    $hic['fecha']=$d->fplanificado;
                    $hic['comentariosPla']=$d->obsplanificada;
                    $hic['comentariosEje']=$d->obsejecutadas;
                    $hic['comentariosObs']=$d->obsobservadas;
                    $hic['horas']=$d->horasejecutadas;
                    $historialCom[count($historialCom)] = $hic;
                }
                /*  Fin Historial de comentarios*/ 



                $hh['key']= $d->key;
                $hh['fecha'] = $d->fplanificado;
                $hh['nrohoras_pla'] = $d->horasplanificadas;
                $hh['obsplanificada'] = $d->obsplanificada;
                $hh['nrohoras_eje'] = $d->horasejecutadas;
                $hh['obsejecutadas'] = $d->obsejecutadas;

                $hh['obsaprobadas'] = $d->obsaprobadas;
                $hh['obsobservadas'] = $d->obsobservadas;
                $hh['cproyectoejecucion'] = $d->cproyectoejecucion;
                $h['deta'][$d->key]=$hh;
            
                $sumhoract=$sumhoract+$hh['nrohoras_eje'];                  

                if($d->dia==1){
                    $dia1=$dia1+$hh['nrohoras_eje'];
                }
                if($d->dia==2){
                    $dia2=$dia2+$hh['nrohoras_eje'];
                }
                if($d->dia==3){
                    $dia3=$dia3+$hh['nrohoras_eje'];
                }
                if($d->dia==4){
                    $dia4=$dia4+$hh['nrohoras_eje'];
                }
                if($d->dia==5){
                    $dia5=$dia5+$hh['nrohoras_eje'];
                }
                if($d->dia==6){
                    $dia6=$dia6+$hh['nrohoras_eje'];
                }
                if($d->dia==7){
                    $dia7=$dia7+$hh['nrohoras_eje'];
                }

            }

            $total=$dia1+$dia2+$dia3+$dia4+$dia5+$dia6+$dia7;

            $h['thoraact']=$sumhoract;
            $listHoras[count($listHoras)] = $h;
            

            /* INICIO ACUMULADO DE HORAS POR DIA */
            
            foreach ($dias as $di) {       
             
                $tab = DB::table('tproyectoejecucion as eje')
                ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')
                ->where('eje.fplanificado','>=',$datainicio)
                ->where('eje.fplanificado','<=',$dataFin)
                ->where('eje.cpersona_ejecuta','=',$cpersona);
                if($h['cproyecto']){
                    $tab=$tab->where('eje.cproyectoactividades','=',$e->cproyectoactividades);
                    $tab=$tab->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
                }else{
                    $tab=$tab->where('eje.cproyectoejecucioncab','=',$e->cproyectoejecucioncab);
                    $tab=$tab->where('eje.cactividad','=',$e->cactividad);
                }
               if ($e->aprobador=='JI') {
                    $tab=$tab->whereIn('tpem.ccargo',$cargos_aprobar)
                    ->where('eje.tipo','=','3');
                }
                if ($e->aprobador=='GP') {
                    $tab=$tab->whereIn('eje.cproyecto',$proyectos)
                    ->where('eje.tipo','!=','3');
                }              
                $tab=$tab->where('eje.tipo','=',$e->tipo);
                if(is_null($e->ctiponofacturable)){
                    $tab = $tab->whereNull('eje.ctiponofacturable');
                }else{
                    $tab = $tab->where('eje.ctiponofacturable','=',$e->ctiponofacturable);
                }

                $tab=$tab->whereIn('eje.estado',[5])
                ->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as key'),DB::raw('to_char(eje.fplanificado,\'D\' ) as dia'))
                ->get();  

                 //dd('',$e,$tab); 
            
                $dd['key'] = str_replace('-','',$di[0]);
                $dd['dia'] = $di[0];
                $dd['sumdia'] =0;

                $dia=0;

                foreach ($tab as $t) {

                    if ($t->key==str_replace('-','',$di[0])){     
                        //dd('',$t);                     
                        $dd['sumdia'] = $dia+$t->horasejecutadas;  
                        $dia=$dd['sumdia'];
                                            
                    }                    
                } 
                 $sumdias[count($sumdias)]=$dd;             
            }
        }
        /* FIN ACUMULADO DE HORAS POR DIA */

        /* INICIO REORDENANDO ARRAY SEGUN KEY*/
        foreach ($sumdias as $key => $orden) {            
            $aux[$key]=$orden['key'];            
        }
        $array=array_multisort($aux,SORT_ASC,$sumdias);
        /* FIN REORDENANDO ARRAY SEGUN KEY*/

        /* INICIO CONTEO DE HORAS SEGUN KEY REPETIDO */      
        $k='';

        foreach ($sumdias as $sd) {           

            if($k!=$sd['key']){                 

                 $td['key']=$sd['key'];     
                 $td['dia']=$sd['dia'];
                 $horadia=0;        

                foreach ($sumdias as $d) {

                    if($sd['key']==$d['key']){

                        $horadia=$d['sumdia']+$horadia;                                                      

                    }                
                }

                $td['sumdia']=$horadia;
                $totales[count($totales)]=$td;
            }

            $k=$sd['key'];            
        }
        /* FIN CONTEO DE HORAS SEGUN KEY REPETIDO */
        
        return view('partials.modalVerHoras',compact('listHoras','dias','per_nombre','semana','anio','semana_ini','semana_fin','dia1','dia2','dia3','dia4','dia5','dia6','dia7','total','historialApro','historialCom','totales'));
    }

    public function aprobarHoras(Request $request){         
        DB::beginTransaction();
        $check = $request->input('checkbox_sel');
        // dd($request);
        $objFlow = new FlowSupport();
        $user = Auth::user();
        foreach($check as $chk){
            $valor = explode('_',$chk); //semana - anio - persona

            $tab = DB::table('tproyectoejecucion as eje')
           // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$valor[0])
            ->whereBetween(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),[$valor[0],$valor[1]])
            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$valor[2])
            ->where('eje.cpersona_ejecuta','=',$valor[3])
            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                    
            ->whereIn('eje.estado',[3,4])
            ->get();
         
            foreach($tab as $e){
                $res = $objFlow->obtenerSiguienteCondicionAut('004','1',$e->ccondicionoperativa);

                if(count($res)>0){
                    $conNueva =$res[0][0];
                    if($e->ccondicionoperativa=='VJI'  && $e->tipo=='3'){
                            $conNueva='APR';
                    }     
                    /* verificar proximo condicion */
                    if($conNueva=='RGP' && strlen($e->cproyecto)>0 ){
                        $cpersona_gp=0;
                        $tpersona_gp = DB::table('tproyecto')
                        ->where('cproyecto','=',$e->cproyecto)
                        ->first();
                        if($tpersona_gp){
                            $cpersona_gp=$tpersona_gp->cpersona_gerente;
                            if($cpersona_gp==$e->cpersona_ejecuta){
                                $res=$objFlow->obtenerSiguienteCondicionAut('004','1',$conNueva);
                            }
                        }
                    }
                    /* Verificar proxima condición*/                                   
                    $objFlow->guardarAprobacionTimeSheet($e->cproyectoejecucion,$e->ccondicionoperativa,$conNueva,1,Auth::user()->cpersona,0);
                    if($e->ccondicionoperativa=='VJI' && $e->tipo=='3'){
                        DB::table('tproyectoejecucion')
                        ->where('cproyectoejecucion','=',$e->cproyectoejecucion)
                        ->update(['ccondicionoperativa' =>'APR' , 'estado' => '5' , 'horasaprobadas' => $e->horasejecutadas]);
                    }else{
                        DB::table('tproyectoejecucion')
                        ->where('cproyectoejecucion','=',$e->cproyectoejecucion)
                        ->update(['ccondicionoperativa' => $res[0][0] , 'estado' => $res[0][1] , 'horasaprobadas' => $e->horasejecutadas]);
                    }

                }
            }

        }
        
        DB::commit();
        
        //return Redirect::route('verHorasAprobaciones');
    }
    public function observarHoras(Request $request){

        DB::beginTransaction();
        $check = $request->input('checkbox_sel');

        $objFlow = new FlowSupport();
        $obs = $request->input('obs_txt');
        $obs =  "| Observado  ".Auth::user()->nombre." * ".date('d-m-Y')." * => " .$obs;
        $user = Auth::user();
        foreach($check as $chk){
            $valor = explode('_',$chk); //semana - anio - persona
             $tab = DB::table('tproyectoejecucion as eje')
           // ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$valor[0])
            ->whereBetween(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),[$valor[0],$valor[1]])
            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$valor[2])
            ->where('eje.cpersona_ejecuta','=',$valor[3])
            ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$user->cpersona.' as bigint))'),'=',DB::raw('true'))                    
            ->whereIn('eje.estado',[3,4])
            ->get();

            //dd($tab);

            foreach($tab as $e){
                $res = $objFlow->obtenerSiguienteCondicionAut('004','1',$e->ccondicionoperativa);
                if(count($res)>0){
                    /*DB::table('tproyectoejecucion')
                    ->where('cproyectoejecucion','=',$e->cproyectoejecucion)
                    ->update(['ccondicionoperativa' => $res[1][0],'estado' => $res[1][1] ]);*/
                    $objFlow->guardarAprobacionTimeSheet($e->cproyectoejecucion,$e->ccondicionoperativa,$res[1][0],1,Auth::user()->cpersona,0);
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e->cproyectoejecucion)->first();
                    $obs_hora = $obj->obsobservadas . " ".$obs;
                    $obj->ccondicionoperativa= $res[1][0];
                    $obj->obsobservadas = $obs_hora;
                    $obj->estado = $res[1][1] ;
                    $obj->horasobservadas=$e->horasejecutadas;   
                    $obj->save();
                }
            }

        }
        
        DB::commit();        
       // return Redirect::route('verHorasAprobaciones');

    }    

    public function observarHoras_rechazar(Request $request){

        DB::beginTransaction();
        $check = $request->input('checkbox_sel');

        $objFlow = new FlowSupport();
        $obs = $request->input('obs_txt');
        $obs =  "| Observado  ".Auth::user()->nombre." * ".date('d-m-Y')." * => " .$obs;
        $user = Auth::user();

        $cargo= DB::table('tpersonadatosempleado as temp')
            ->where('cpersona','=',$user->cpersona)
            ->first();

        $cargos_aprobar = DB::table('tcargos')       
            ->where('cargoparentdespliegue','=',$cargo->ccargo)
            ->lists('ccargo');

        $proyectos=DB::table('tproyecto as tp')
            ->where('tp.cpersona_gerente', '=' ,$user->cpersona)
            ->lists('cproyecto');

        $aprobarJI=[];
        $aprobarGP=[];

        foreach($check as $chk){
            $valor = explode('_',$chk); //semana - anio - persona


            if ($proyectos) {

                $aprobarGP = DB::table('tproyectoejecucion as eje')
                    ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')

                    ->whereBetween(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),[$valor[0],$valor[1]])
                    ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$valor[2])
                    ->where('eje.cpersona_ejecuta','=',$valor[3])
                    ->whereIn('eje.cproyecto',$proyectos)                    
                    ->whereIn('eje.estado',[5])
                    ->where('eje.tipo','!=','3')
                    ->get();

                    foreach ($aprobarGP as $key => $item) {
                        $item->aprobador='GP';
                    }
            }

            if ($cargos_aprobar) {

                $aprobarJI = DB::table('tproyectoejecucion as eje')
                    ->join('tpersonadatosempleado as tpem','eje.cpersona_ejecuta','=','tpem.cpersona')

                    ->whereBetween(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),[$valor[0],$valor[1]])
                    ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$valor[2])
                    ->where('eje.cpersona_ejecuta','=',$valor[3])
                    ->whereIn('tpem.ccargo',$cargos_aprobar)                 
                    ->whereIn('eje.estado',[5])
                    ->where('eje.tipo','=','3')
                    ->get();

                    foreach ($aprobarJI as $key => $item) {
                        $item->aprobador='JI';
                    }

            }

            $tab = array_merge($aprobarJI,$aprobarGP);

            foreach($tab as $e){
                $res = $objFlow->obtenerSiguienteCondicionAut('004','1',$e->ccondicionoperativa);
                if(count($res)>0){
                    /*DB::table('tproyectoejecucion')
                    ->where('cproyectoejecucion','=',$e->cproyectoejecucion)
                    ->update(['ccondicionoperativa' => $res[1][0],'estado' => $res[1][1] ]);*/
                    $objFlow->guardarAprobacionTimeSheet($e->cproyectoejecucion,$e->ccondicionoperativa,$res[1][0],1,Auth::user()->cpersona,0);
                    $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$e->cproyectoejecucion)->first();
                    $obs_hora = $obj->obsobservadas . " ".$obs;
                    $obj->ccondicionoperativa= $res[1][0];
                    $obj->obsobservadas = $obs_hora;
                    $obj->estado = $res[1][1] ;
                    $obj->horasobservadas=$e->horasejecutadas;   
                    $obj->save();
                }
            }

        }
        
        DB::commit();        
       // return Redirect::route('verHorasAprobaciones');

    }    


    public function historialAprobaciones(){
        return view('timesheet/historialAprobaciones');
    }

   
}
