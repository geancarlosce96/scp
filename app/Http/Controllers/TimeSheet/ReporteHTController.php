<?php

namespace App\Http\Controllers\TimeSheet;

use Illuminate\Http\Request;

use App\Http\Requests;

use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Tproyectoejecucion;
use DB;
use Redirect;


class ReporteHTController extends Controller
{
    public function index()
    {
        $proyectos = DB::table('tproyecto')
            ->orderBy('codigo')
            ->where('codigo','!=',"''")
            ->lists(DB::raw('CONCAT(codigo,\' - \',nombre) as nombre'),'cproyecto');
        
        $persona = DB::table('tpersona as per')
            ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
            // ->where('emp.estado','=','ACT')
            ->orderBy('per.nombre','ASC')
            ->lists('per.abreviatura','per.cpersona');
        $persona = [''=>''] + $persona;

        $condicionoperativa = DB::table('tcondicionesoperativas')
            ->where('csubsistema','=','004')
            ->lists('descripcion','ccondicionoperativa');

        $fdesde = Carbon::now()->startOfWeek();
        $semanadesde = $fdesde->weekOfYear;
        $fdesde = $fdesde->format('d-m-Y');

        $fhasta = Carbon::now()->endOfWeek();
        $semanahasta = $fhasta->weekOfYear;
        $fhasta = $fhasta->format('d-m-Y');

        $rango_semanas = $semanadesde.' - '.$semanahasta;

        return view('timesheet/reportetimesheet',compact('proyectos','persona','condicionoperativa','fdesde','fhasta','rango_semanas'));

    }

    public function obtnerdatosht(Request $request)
    {
        $firma_posicion = $request->firma_posicion;
        $dividir_semanas = $request->dividir_semanas?$request->dividir_semanas:1;
        $proyecto = $request->proyecto?implode(',',$request->proyecto):'';
        $cantProyecto = $request->proyecto?count($request->proyecto):0;
        $tipoact = $request->tipoact?implode(',',$request->tipoact):'';
        $cantTipoact = $request->tipoact?count($request->tipoact):0;
        $persona = $request->persona?$request->persona:null;
        $persona_nombre = DB::table('tpersona')->where('cpersona',$persona)->first();
        $condicionoperativa = $request->condicionoperativa?implode(',',$request->condicionoperativa):'';
        $cantCondicionoperativa = $request->condicionoperativa?count($request->condicionoperativa):0;
        $fdesde_r = $request->fdesde?$request->fdesde:null;
        $fhasta_r = $request->fhasta?$request->fhasta:null;
        $fdesde = $this->convertirFechasCarbon($fdesde_r);
        $fdesde = $fdesde->startOfWeek();
        $fhasta = $this->convertirFechasCarbon($fhasta_r);
        $fhasta = $fhasta->endOfWeek();

   
        $cantidad_valores = DB::select('select optimiza."fn_getfechas"(\''.$proyecto.'\',\''.$cantProyecto.'\',\''.$persona.'\',\''.$tipoact.'\',\''.$cantTipoact.'\',\''.$condicionoperativa.'\',\''.$cantCondicionoperativa.'\',\''.$fdesde.'\',\''.$fhasta.'\')')[0]->fn_getfechas;

        $cantidad_valores = strlen($cantidad_valores);

        if ($cantidad_valores > 0) {

            $consulta_text = DB::select('select optimiza."fn_obtenerreporteht"(\''.$proyecto.'\',\''.$cantProyecto.'\',\''.$persona.'\',\''.$tipoact.'\',\''.$cantTipoact.'\',\''.$condicionoperativa.'\',\''.$cantCondicionoperativa.'\',\''.$fdesde.'\',\''.$fhasta.'\')')[0]->fn_obtenerreporteht;
    
            $datos_reporteht = DB::select($consulta_text);
            
            // Cabecera de semanas
            $datadesde = $this->convertirFechasCarbon($fdesde_r);
            $datadesde = $datadesde->startOfWeek();
            $datahasta = $this->convertirFechasCarbon($fhasta_r);
            $datahasta = $datahasta->endOfWeek();
    
            $seguir = true;
            $dias= array();
            $dia_desde = $datadesde;
          
            $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
            while($seguir){
                $dias[count($dias)] = array($dia_desde->toDateString(),$dia_desde->day,$aDia[$dia_desde->dayOfWeek],$dia_desde->year,$dia_desde->weekOfYear,'t_'.$dia_desde->format('Ymd'),$dia_desde->format('d-m-Y'));
                $dia_desde = $datadesde->addDay(1);
    
                 if ( !( ($dia_desde->year ."".str_pad($dia_desde->month,2,'0',STR_PAD_LEFT) ."". str_pad($dia_desde->day,2,'0',STR_PAD_LEFT)) <= ($datahasta->year ."".str_pad($datahasta->month,2,'0',STR_PAD_LEFT) ."". str_pad($datahasta->day,2,'0',STR_PAD_LEFT) ) ) ) {
                    $seguir=false;    
                }
            } 
            // Fin de cabecaera de semanas
    
            $semanadesde = $fdesde->weekOfYear;
            $semanahasta = $fhasta->weekOfYear;
            
            $cant_semanas = $semanahasta - $semanadesde +1;
            $cantidad_dias = $dividir_semanas * 7;
            $dia_ini_temp = 0;
            $dia_fin_temp = $cantidad_dias;
            $horas_array = [];
    
            while ($cant_semanas > 0) {
    
                $horas_seccion = [];
                $cabecera_dias = [];
                $cont_cab = 0;
    
                foreach($datos_reporteht as $dht){
    
                    // obtener horas cargadas por dias
                    $dias_ht = [];
                    $cont_cab ++;
                    $totalXactividad = 0;
                    // devuelve los indices de $dht
                    $index_fechas_dht = array_keys(get_object_vars($dht));
                    for ($i = $dia_ini_temp; $i < $dia_fin_temp ; $i++) {
                        
                        if ( $i < count($dias) ) {
                            
                            $dia_cab = $dias[$i];
                            $fecha_temp = $this->convertirFechasCarbon($dia_cab[0]);
                            $fecha_temp = $fecha_temp->format('d-m-Y');
                            $t_dia = $dia_cab[5];
                            
                            
                            // busca si la fecha consultada existe dentro de $dht
                            $existe_fecha = in_array($t_dia,$index_fechas_dht);
    
                            $horas_cargadas = $existe_fecha? $dht->$t_dia : '';
                            // $horas_cargadas = $existe_fecha? $dht->$t_dia : 0;
                            //$horas_cargadas = number_format($horas_cargadas,2,'.','');
                            
                            $dias_ht[$dia_cab[0]] = [$fecha_temp,$horas_cargadas];
                            
                            $totalXactividad += floatval($horas_cargadas);
    
                            if ($cont_cab <= 1) {
                                array_push($cabecera_dias,$dia_cab);
                            }
                        }
                        else {
                            break;
                        }
                    }
    
                    $datos_fila = explode('|',$dht->actividad);
                    
                    $h['actividad'] = $datos_fila[0] != 'sin_datos' ? $datos_fila[0] : '';
                    $h['categoria_proy'] = $datos_fila[0] != 'sin_datos' ? $datos_fila[1] : '';
                    $h['tipo'] = $datos_fila[0] != 'sin_datos' ? ($datos_fila[2] == '1' ? 'FF' : ($datos_fila[2] == '2' ? 'NF' : 'AND')) : '';
                    $h['tipo_nf'] = $datos_fila[0] != 'sin_datos' ? $datos_fila[3] : '';
                    $h['gerente'] = $datos_fila[0] != 'sin_datos' ? $datos_fila[4] : '';
                    $h['horas'] = $datos_fila[0] != 'sin_datos' ? $dias_ht : 'sin_datos';
                    $h['sumaXactividad'] = number_format($totalXactividad,2,'.','');
    
                    if ($totalXactividad > 0) {
                        array_push($horas_seccion,$h);
                    }
    
                }
    
                $dia_ini_temp += $cantidad_dias;
                $dia_fin_temp += $cantidad_dias;
                
                $horas_seccion_temp = array_map(function($dias_horas){
                    return $dias_horas['horas'];
                },$horas_seccion);
    
                $sumatoria = [];
                
                foreach ($cabecera_dias as $key => $d) {
                    
                    $horas_dia = array_map(function($dia)use($d,$horas_seccion_temp){
                        
                        $hora_dia = $horas_seccion_temp[0] != 'sin_datos' ? $dia[$d[0]][1]:0;
    
                        return floatval($hora_dia);
    
                    },$horas_seccion_temp);
                    
                    $suma = array_sum($horas_dia);
                    $suma = number_format($suma,2,'.','');
                    array_push($sumatoria,$suma);
                }
                
                $ht_sec['ht_seccion'] = $horas_seccion;
                $ht_sec['dias'] = $cabecera_dias;
                $ht_sec['dia_inicio'] = $cabecera_dias[0];
                $ht_sec['dia_fin'] = array_pop($cabecera_dias);
                $ht_sec['ht_sumatoria'] = $sumatoria;
                $ht_sec['ht_sumatoria_total'] = number_format(array_sum($sumatoria),2,'.','');
    
                if (array_sum($sumatoria) > 0) {
                    array_push($horas_array,$ht_sec);
                }
                $cant_semanas = $cant_semanas - $dividir_semanas;
            } 
        }
        else{
            $horas_array = [];
            $dias = [];
        }

        $view =  \View::make('timesheet/printreporteHT',compact('horas_array','dias','persona_nombre','firma_posicion'))->render();
    
        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','landscape');
    
        return $pdf->stream('HH.pdf');

    }
    
    private function convertirFechasCarbon($fecha){

        if(substr($fecha,2,1)=='-'){    
            $fecha = Carbon::createFromDate(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2));

        }else{
            $fecha = Carbon::createFromDate(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2));
        }
        return $fecha;
    }

    public function obtenerdatosconsulta($proyecto,$persona,$tipoact,$cactividad,$cproyectoactividad,$condicionoperativa,$fdesde,$fhasta,$buscar){

        $ejecucion = DB::table('public.v_reporteht');
                    
            if ($proyecto) {
                $ejecucion = $ejecucion->whereIn('cproyecto',$proyecto);
            }
            if ($persona) {
                $ejecucion = $ejecucion->where('cpersona_ejecuta',$persona);
            }
            if ($tipoact) {
                $ejecucion = $ejecucion->whereIn('tipo',$tipoact);
            }
            if ($cactividad) {
                $ejecucion = $ejecucion->where('cactividad',$cactividad);
            }
            if ($cproyectoactividad) {
                $ejecucion = $ejecucion->where('cproyectoactividades',$cproyectoactividad);
            }
            if ($condicionoperativa) {
                $ejecucion = $ejecucion->whereIn('ccondicionoperativa',$condicionoperativa);
            }
            if ($buscar == 'Actividades') {
                $ejecucion = $ejecucion->where('fplanificado','>=',$fdesde)->where('fplanificado','<=',$fhasta);
            }
            if ($buscar == 'HorasXfecha') {
                $ejecucion = $ejecucion->where('fecha','=',$fdesde);
            }
                

        $ejecucion = $ejecucion->whereNotNull('horasejecutadas');

        return $ejecucion;
    }

    public function obtenerrangosemanas(Request $request){



        $fdesde = $request->fdesde;
        $fhasta = $request->fhasta;

        
        $fdesde = $this->convertirFechasCarbon($request->fdesde);
        $fdesde = $fdesde->startOfWeek();
        
        $fhasta = $this->convertirFechasCarbon($request->fhasta);
        $fhasta = $fhasta->endOfWeek();
       
        $diferencia_meses = $fhasta->diffInMonths($fdesde->addDay());
        
        //$semanadesde = $fdesde->weekOfYear;
        //$semanahasta = $fhasta->weekOfYear;

        return $diferencia_meses;
        //return $fdesde.' a '.$fhasta;



    }

}
