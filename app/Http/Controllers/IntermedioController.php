<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IntermedioController extends Controller
{
    public function index(Request $request)
    {
    	$enlace = $request->input("url");
    	$data["enlace"] = $enlace;
    	return view("layouts.intermedio", $data);
    }
}
