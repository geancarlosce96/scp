<?php

namespace App\Http\Controllers\Configuracion;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Erp\HelperSIGSupport;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tconfiguracion;
use App\Models\Tusuario;
use App\Models\Tmenupersona;
use App\Models\Tmenu;
use App\Models\Tusuariosesione;
use Redirect;
use Auth;
use Session;
use Response;
use DB;

class ConfiguracionController extends Controller
{
    //Usuarios Conectados


    

	public function listarUsuarioConectados(){

		return Datatables::queryBuilder(DB::table('tusuariosesiones as tus')
			->leftJoin('tusuarios as tu','tus.cusuario','=','tu.cusuario')          
            ->leftJoin('tpersona as tp','tu.cpersona','=','tp.cpersona')
            ->select('tus.cusuario','tp.nombre as persona','tus.terminal','tu.nombre as usuario','tus.fdesde','tus.fhasta',DB::raw('CONCAT(\'rowprov_\',tu.cusuario)   as "DT_RowId"'))     
            )->make(true); 
	}
	

	public function listadousuarioConectado(){

		return view('sistema/usuariosConectados');
	}

	public function registroUsuarios(){

	}

	public function mostrarDatosConfiguracion(){

		$sevidorsmtp=DB::table('tconfiguracion')
		->first();

		$correo_remi_ale=DB::table('tconfiguracion')
		->first();

		$correo_dest_not=DB::table('tconfiguracion')
		->first();

		$tiemposesion=DB::table('tconfiguracion')
		->first();

		$tiempocaducidad=DB::table('tconfiguracion')
		->first();

		$tiempoper=DB::table('tconfiguracion')
		->first();

		return view('sistema.configuracionSistemas',compact('sevidorsmtp','correo_remi_ale','correo_dest_not','tiemposesion','tiempocaducidad','tiempoper'));
	}
	public function grabarConfiguracion(Request $request){
		DB::beginTransaction();
		$tconfiguracion=Tconfiguracion::where('cconfiguracion','=','1')
		->first();

		
		$tconfiguracion->sevidorsmtp=$request->input('sevidorsmtp');
		$tconfiguracion->correo_remi_ale=$request->input('correo_remi_ale');
		$tconfiguracion->correo_dest_not=$request->input('correo_dest_not');
		$tconfiguracion->tiemposesion=$request->input('tiemposesion');
		$tconfiguracion->tiempocaducidad=$request->input('tiempocaducidad');
		$tconfiguracion->tiempoper=$request->input('tiempoper');
		$tconfiguracion->save();
		DB::commit();

		return Redirect::route('confSistema'); 
	}

	public function listaAsignacionPermiso(){
		$menu=DB::table('tmenu')
		 ->lists('descripcion','menuid');

		$menuParent=DB::table('tmenu')	
		->where(DB::raw('LENGTH(menuid)'),'<',DB::raw('3'))
		->orderBy('menuid','ASC')	
		->get();


		$estructura="";
		$objMenu  = new HelperSIGSupport();
        foreach ($menuParent as $m) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$m->menuid. "',";
            $estructura=$estructura ."label: '".$m->descripcion ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";
 		

            $est=$objMenu->listarTreeview($m);
				
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }
	

		return view('sistema.asignacionPermisos',compact('menu','estructura'));
	}

	public function listarUsuario(){
		return Datatables::queryBuilder(
		DB::table('tpersona as tp')
        ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')       
        ->orderBy('tp.nombre','ASC')
        ->select('tu.cusuario','tp.abreviatura','tp.cpersona','tu.nombre as usuario',DB::raw('CONCAT(\'row_\',tu.cusuario) as "DT_RowId"'))
        ->where('tpi.esempleado','=','1')
        ->where('tu.estado','=','ACT')
        ->whereNotNull('tu.cusuario')
        )->make(true);	

	}

	public function editarAsignacionPermiso(Request $request,$idUsuario){
		$usuario=Db::table('tusuarios')
		->where('cusuario','=',$idUsuario)
		->first();

		$menupersona=DB::table('tmenupersona as mp')		
		 ->leftJoin('tmenu as m','mp.menuid','=','m.menuid')
		 ->leftJoin('tpersona as tp','mp.cpersona','=','tp.cpersona')
		 ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')		
		 ->where('tu.cusuario','=',$idUsuario)
		 ->select('mp.menuid','m.descripcion as descripcion','mp.cmenupersona','tp.cpersona')
		 ->orderBy('m.menuid','ASC')
		 ->get();
		
		$menuParent=DB::table('tmenu')	
		->where(DB::raw('LENGTH(menuid)'),'<','3')
		->orderBy('menuid','ASC')	
		->get();	

		$estructura="";
		$objMenu  = new HelperSIGSupport();
        foreach ($menuParent as $m) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
          
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$m->menuid. "',";
            $estructura=$estructura ."label: '".$m->descripcion ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";
 		
            
            $est=$objMenu->listarTreeview($m);
				
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }
	
	return view('sistema.asignacionPermisos',compact('usuario','menupersona','menuParent','estructura'));
	}

	public function viewUsuarioPermisos($idUsuario){

		 $menupersona=DB::table('tmenupersona as mp')		
		 ->leftJoin('tmenu as m','mp.menuid','=','m.menuid')
		 ->leftJoin('tpersona as tp','mp.cpersona','=','tp.cpersona')			
		 ->where('mp.cpersona','=',$idUsuario)
		 ->select('mp.menuid','m.descripcion as descripcion','mp.cmenupersona','tp.cpersona')
		 ->orderBy('m.menuid','ASC')
		 ->get();
	
	return view('partials.tableUsuarioPermisos',compact('menupersona'));
	}

	public function viewUsuario($idUsuario){
		$usuario=Db::table('tusuarios')
		->where('cusuario','=',$idUsuario)
		->first();				
	
	return view('partials.verUsuariosAsignacionPermisos',compact('usuario'));
	}
	public function agregarPermisos(Request $request){
		DB::beginTransaction();		   
        $cpersona=$request->input('usuario'); 
        $menuid=$request->input('selected_permiso');         
        
		foreach($menuid as $v){  

			$menupersona=Tmenupersona::where('cpersona','=',$cpersona)
		    ->where('menuid','=',$v)
	        ->first();

	        if($menupersona){
	          	$permiso='01';
	         	$menupersona->save();
         	}else{
				if(strlen($v)>2){    
						$menupersona=new Tmenupersona;         		
						$menupersona->menuid=$v;
						$menupersona->cpersona=$cpersona;
						$menupersona->permiso='01';
						$menupersona->save();   
				}
			}
			$parentmenu = DB::table('tmenu')
			->where('menuid','=',$v)->first();
			if($parentmenu){
				$menup = Tmenupersona::where('menuid','=',$parentmenu->modulo)
				->where('cpersona','=',$cpersona)->first();
				if(!$menup){
					$obj = new Tmenupersona();
					$obj->menuid=$parentmenu->modulo;
					$obj->cpersona=$cpersona;
					$obj->permiso='01';
					$obj->save();  					

				}
			}
        }

     
	    DB::commit();
		return Redirect::route('viewUsuarioPermisos',$cpersona); 
	  
	}

	 public function delAsignacionPermiso(Request $request,$idPermiso){
       DB::beginTransaction();
      
        $idPermiso = $request->input('chkselec');
        $cpersona=$request->input('usuario'); 

        foreach($idPermiso as $v){
           $menupersona= Tmenupersona::where('cmenupersona','=',$v)	 
           ->where('cpersona','=',$cpersona)       
	       ->first();
	       try{
	       		$menupersona->delete();
	       }catch(\Illuminate\Database\QueryException $e){
	       }
	     
       }
         
	 DB::commit();
	 return Redirect::route('viewUsuarioPermisos',$cpersona); 
    }

}
