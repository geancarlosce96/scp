<?php

namespace App\Http\Controllers\MasterProyectos;

use Illuminate\Http\Request;
use Auth;
use DB;
use Response;
use Carbon\Carbon;
use App\Models\TFianzaSeguro;
use App\Models\TMonitoreo;
use App\Models\TPlanificacion;
use App\Models\TCierre;
use App\Models\TContrato;
use App\Models\Tseccionmasterpersona;
use App\Models\Tvistastablaspersona;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Http\Requests\FianzaSeguro;
use Yajra\Datatables\Facades\Datatables;

class ListaProyectosController extends Controller
{
    public function listadeProyectos()
    {
        $tproyectos = $this->CapturarProyectosPorCargo();

        return view('proyecto.MasterProyectos.01-OpcionesListaProyectos', compact('tproyectos'));
    }

    public function CapturarProyectosPorCargo()
    {
        $user = Auth::user();

        $lider_or_gerente = $this->lider_or_gerente();
        //dd($lider_or_gerente);

        $tproyectos = DB::table('tproyecto as tp')
            ->leftjoin('tproyectopersona as tpyp', 'tpyp.cproyecto', '=', 'tp.cproyecto')
            ->leftjoin('tproyectodisciplina as tda', 'tp.cproyecto', '=', 'tda.cproyecto');

        if ($lider_or_gerente == 'lider') {

            $tproyectos = $tproyectos->where('tpyp.cpersona','=', $user->cpersona)
                ->where('tpyp.eslider', '=', '1');

        }
        if($lider_or_gerente == 'gerente')
        {
            $tproyectos = $tproyectos->where('tp.cpersona_gerente', '=', $user->cpersona);
        }
        if($lider_or_gerente == 'jefe') {

        }

        $tproyectos = $tproyectos->select('tp.cproyecto', 'tp.codigo', 'tp.nombre')
            ->distinct()
            ->orderBy('tp.cproyecto', 'ASC')
            ->get();

        return $tproyectos;
    }

    public function CapturarProyectoMaster(Request $request)
    {
        // dd($request->all());
        $cproyecto = $request->cproyecto;
        $arrayProyectoSQL = $this->listadoPreliminarProyecto($cproyecto);
        $faPy = $this->faseProyecto($cproyecto);

        $horasPresupuestados = $this->horasPresupuestadas($cproyecto);

        $sumaMontoPresupuestadasNivelProyecto = $this->montoPresupuestado($cproyecto);

        $numdoc  = $this->numdoc($cproyecto);
        $numplan = $this->numpla($cproyecto) ;
        $numfig  = $this->numfig($cproyecto);
        $nummap = $this->nummap($cproyecto);
        $f__inicio =  $arrayProyectoSQL->finicio;
        $f__cierre =  $arrayProyectoSQL->fcierre;
        $diasPPlazo = $this->pplazo($f__inicio,$f__cierre);
        $fianzaGastos =   $this->fianzaGastos($cproyecto);
        $tcontrato =   $this->contratos($cproyecto);
        $tlanificacion =   $this->planificacion($cproyecto);
        $pryEDT = $this->pryEDT($cproyecto);
        $pryLE = $this->pryLE($cproyecto);
        $pryPer = $this->performance_curvaS($cproyecto);
        $monitoreoControl = $this->monitoreoControl($cproyecto);
        $pryLA =  $this->listaentregables($cproyecto);
        $tcierre =  $this->cierreProyecto($cproyecto);

        $arrayProyecto = [];

            $pryMP['nomcp'] = $arrayProyectoSQL->nomcp;
            $pryMP['identificacioncp'] = $arrayProyectoSQL->identificacioncp;
            $pryMP['nomcd'] = $arrayProyectoSQL->nomcd;
            $pryMP['identificacioncd'] = $arrayProyectoSQL->identificacioncd;
            $pryMP['cliente'] = $arrayProyectoSQL->cliente;
            $pryMP['abrv'] = $arrayProyectoSQL->abrv;
            $pryMP['auditado'] = $arrayProyectoSQL->auditado ? 'Si' : 'No';
            $pryMP['nombreclientefinal'] = $arrayProyectoSQL->nombreclientefinal;
            $pryMP['nombreservicio'] = $arrayProyectoSQL->nombreservicio;
            $pryMP['tipoproynombre'] = $arrayProyectoSQL->tipoproynombre;
            $pryMP['nombretipogestion'] = $arrayProyectoSQL->nombretipogestion;
            $pryMP['uminera'] = $arrayProyectoSQL->uminera;
            $pryMP['codigo'] = $arrayProyectoSQL->codigo;
            $pryMP['cproyecto'] = $cproyecto;
            $pryMP['nombrepry'] = $arrayProyectoSQL->nombrepry;
            $pryMP['descpry'] = $arrayProyectoSQL->descpry;
            $pryMP['finicio'] = $arrayProyectoSQL->finicio;
            $pryMP['fcierre'] = $arrayProyectoSQL->fcierre;
            $pryMP['gerente'] = $arrayProyectoSQL->gerente;
            $pryMP['identificaciongp'] = $arrayProyectoSQL->identificaciongp;
            $pryMP['cproyecto'] = $arrayProyectoSQL->cproyecto;
            $pryMP['descripcion'] = $arrayProyectoSQL->descripcion;
            $pryMP['descripcion_estado'] = $arrayProyectoSQL->descripcion_estado;
            $pryMP['cpropuesta'] = $arrayProyectoSQL->cpropuesta;
            $pryMP['fase'] = $faPy;

            $pryMP['cf'] = $fianzaGastos ? $fianzaGastos->cf : '';
            $pryMP['cf_inicio'] = $fianzaGastos  ?$fianzaGastos->cf_inicio : '';
            $pryMP['cf_vigencia'] = $fianzaGastos ?$fianzaGastos->cf_vigencia : '';
            $pryMP['srg'] = $fianzaGastos ?$fianzaGastos->srg : '';
            $pryMP['srg_inicio'] = $fianzaGastos ?$fianzaGastos->srg_inicio : '';
            $pryMP['srg_vigencia'] = $fianzaGastos ?$fianzaGastos->srg_vigencia : '';
            $pryMP['srp'] = $fianzaGastos ?$fianzaGastos->srp : '';
            $pryMP['srp_inicio'] = $fianzaGastos ?$fianzaGastos->srp_inicio : '';
            $pryMP['srp_vigencia'] = $fianzaGastos ?$fianzaGastos->srp_vigencia : '';

            $pryMP['encuesta'] = $monitoreoControl ?$monitoreoControl->encuesta : null;
            $pryMP['encuesta1'] = $monitoreoControl ?$monitoreoControl->encuesta1 : null;
            $pryMP['encuesta2'] = $monitoreoControl ?$monitoreoControl->encuesta2 : null;
            $pryMP['encuestafecha'] = $monitoreoControl ?$monitoreoControl->encuestafecha : null;
            $pryMP['encuestafecha1'] = $monitoreoControl ?$monitoreoControl->encuestafecha1 : null;
            $pryMP['encuestafecha2'] = $monitoreoControl ?$monitoreoControl->encuestafecha2 : null;

            $pryMP['komcliente'] =   $tlanificacion ?$tlanificacion->komcliente : null;
            $pryMP['kominterno'] =   $tlanificacion ?$tlanificacion->kominterno : null;
            $pryMP['lc'] =           $tlanificacion ?$tlanificacion->lc : null;
            $pryMP['alc'] =          $tlanificacion ?$tlanificacion->alc : null;
            $pryMP['croproyecto'] =  $tlanificacion ?$tlanificacion->croproyecto : null;
            $pryMP['ram'] =          $tlanificacion ?$tlanificacion->ram : null;
            $pryMP['listariesgos'] = $tlanificacion ?$tlanificacion->listariesgos : null;

            $pryMP['acs'] = $tcierre ?$tcierre->acs : null;
            $pryMP['acscodigo'] = $tcierre ?$tcierre->acscodigo : null;
            $pryMP['reunioncierre'] = $tcierre ?$tcierre->reunioncierre : null;
            $pryMP['correofin'] = $tcierre ?$tcierre->correofin : null;
            $pryMP['acsfechaenvio'] = $tcierre ?$tcierre->acsfechaenvio : null;
            $pryMP['acsfecharecepcion'] = $tcierre ?$tcierre->acsfecharecepcion : null;

            $pryMP['fechafirma'] = $tcontrato ?$tcontrato->fechafirma : null;
            $pryMP['fechainicio'] = $tcontrato ?$tcontrato->fechainicio : null;
            $pryMP['fechatermino'] = $tcontrato ?$tcontrato->fechatermino : null;
            $pryMP['conos'] = $tcontrato ?$tcontrato->conos : null;



            $pryMP['fadjucicacion'] = $arrayProyectoSQL->fadjucicacion;
            $pryMP['descripcion_contrato'] = $arrayProyectoSQL->descripcion_contrato;
            $pryMP['descripcion_lugar'] = $arrayProyectoSQL->descripcion_lugar;
            $pryMP['descripcion_moneda'] = $arrayProyectoSQL->descripcion_moneda;
            $pryMP['diascorte'] = $arrayProyectoSQL->diascorte;
            $pryMP['plazopagodias'] = $arrayProyectoSQL->plazopagodias;
            $pryMP['pplazo'] =$diasPPlazo;
            $pryMP['correoinicio'] = $arrayProyectoSQL->correoinicio;
            $pryMP['nrodocaprobacion'] = $arrayProyectoSQL->nrodocaprobacion;
            $pryMP['horaspresupuestadas'] = $horasPresupuestados == null ? 0 : $horasPresupuestados->horaspresupuestadas;
            $pryMP['numeroplanos'] = $numplan ? $numplan : 0 ;
            $pryMP['numerodocumentos'] = $numdoc  ?  $numdoc : 0;
            $pryMP['numerofiguras'] = $numfig  ?  $numfig : 0;
            $pryMP['numeromapas'] = $nummap  ?  $nummap : 0;
            $pryMP['correoinicio'] = $arrayProyectoSQL->correoinicio == TRUE ? 'Si' : 'No';
            $pryMP['hoja_resumen'] = $arrayProyectoSQL->hoja_resumen == null ? 'No' : 'Si';
            $pryMP['edt'] = $pryEDT  == null ? 'No' : 'Si';
            $pryMP['le'] = $pryLE  == null ? 'No' : 'Si';
            $pryMP['performance'] = $pryPer  == null ? 'No' : 'Si';
            $pryMP['curva-s'] = $pryPer  == null ? 'No' : 'Si';
            $pryMP['la'] = $pryLA  == null ? 'No' : 'Si';
            $pryMP['montopresupuestados'] = $sumaMontoPresupuestadasNivelProyecto ;
            $pryMP['nrodocaprobacion'] = $arrayProyectoSQL->nrodocaprobacion;


            array_push($arrayProyecto,$pryMP);

            $permiso = Tseccionmasterpersona::where('cpersona', '=',Auth::user()->cpersona)
                                              ->where('estado', '=',1)->get();


          //  dd($permiso);
        //dd($arrayProyecto);

        return view('proyecto.MasterProyectos.02-ListaProyectos', compact('arrayProyecto','permiso'));

    }

    public function lider_or_gerente()
    {
        $user = Auth::user();

        $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user->cpersona)
            ->get();
        //dd($esgerente);

        $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.cpersona', '=', $user->cpersona)
            ->where('tpyp.eslider','=','1')
            ->get();

        $cargoId = DB::table('tpersonadatosempleado as e')
            ->where('cpersona', '=', $user->cpersona)
            ->whereNull('ftermino')
            ->orderBy('fingreso', 'DESC')
            ->select('cpersona','ccargo')
            ->first();

        $esjefe = DB::table('tcargos as tcar')
            ->where('tcar.cargoparentdespliegue', '=', $cargoId->ccargo)
            ->get();


        $lider_or_gerente = '';

        if($eslider)
        {
            $lider_or_gerente = 'lider';
        }
        if($esgerente)
        {
            $lider_or_gerente = 'gerente';
        }
        if($esjefe)
        {
            $lider_or_gerente = 'jefe';
        }

        return $lider_or_gerente;

    }
    public function listadeActividadesHijosDirectosporProyecto($cproyecto)
    {
        $actividades = DB::table('tproyectoactividades')
            ->select('cproyectoactividades','codigoactvidad','descripcionactividad','cproyectoactividades_parent')
            ->where('cproyecto', '=',$cproyecto)
            ->where('cproyectoactividades_parent', '=',null)
            ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
            ->get();
        return $actividades;
    }
    private function actividad_padre_or_hijo($cactividad)
    {
        $activityfather_or_son = DB::table('tproyectoactividades as tpa')
            ->select('tpa.cproyectoactividades','tpa.codigoactvidad','tpa.descripcionactividad','tpa.cproyecto','tpa.cproyectoactividades_parent')
            ->orderBy('tpa.codigoactvidad','ASC')
            ->where('tpa.cproyectoactividades_parent', '=', $cactividad)
            ->get();

        return $activityfather_or_son;

    }
    private function obteneratepresuestadas_condetalle($cactividad,$disciplina,$cproyecto)
    {
        $cproyecto = $cproyecto;

        $cabecera_participante = $this->getCabezera($cproyecto, $disciplina);
        //dd($cabecera_participante);
        $suma_rate_detalle_activity = 0;
       // $arraysumadetalleactivity   = [];
         foreach ($cabecera_participante as $cab) {
            // dd($cab);
            $sumatoria_rate_hora_detalle = $this->obtenerhorasrate($cactividad, $disciplina, $cab->croldespliegue);


            if ($sumatoria_rate_hora_detalle) {

                foreach ($sumatoria_rate_hora_detalle as $sr) {
                    //dd($sr);
                    $suma_actividad = $sr->horas * $sr->rate;
                    $suma_rate_detalle_activity += $suma_actividad;

                }
            }
       }

        return array("sumtotalPresupuestadas" => $suma_rate_detalle_activity);
    }
    public function getCabezera($cproyecto, $cdisciplina)
    {
        /*----------  Lista de cabezera  ----------*/

        $project_ingresado = DB::table('tproyecto as tp')
            ->leftjoin('tproyectoestructuraparticipantes as tpe', 'tp.cproyecto', '=', 'tpe.cproyecto')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpe.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpe.croldespliegue')
            ->select('tpe.croldespliegue', 'trd.descripcionrol', 'trd.orden');

        if ($cdisciplina == 'Nuevo') {

            $project_ingresado = $project_ingresado->where('tpe.cproyecto', '=', $cproyecto);
        } else {

            $project_ingresado = $project_ingresado->where('tpe.cproyecto', '=', $cproyecto)
                ->where('tph.cdisciplina', '=', $cdisciplina);
        }

        $project_ingresado = $project_ingresado->orderBy('trd.orden', 'ASC')
            ->distinct()
            ->get();

        // dd($project_ingresado);

        return $project_ingresado;

        /*----------  End Lista de cabezera  ----------*/
    }
    private function obtenerhorasrate($cactividad, $disciplina, $roldespliegue)
    {
        //dd($disciplina);
        $sumatoria_rate_hora = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpep.croldespliegue')
            ->select('tpa.cproyectoactividades', 'trd.descripcionrol', DB::raw("to_number(tph.horas, '99G999D9S') as horas"), 'tpep.rate', 'trd.croldespliegue', 'tpa.cproyectoactividades_parent');

        if ($roldespliegue != 'Todos') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('trd.croldespliegue', '=', $roldespliegue);
        }

        if ($disciplina == 'Nuevo') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tpa.cproyectoactividades', '=', $cactividad);
        } else {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tph.cdisciplina', '=', $disciplina)
                ->where('tph.cproyectoactividades', '=', $cactividad);
        }

        $sumatoria_rate_hora = $sumatoria_rate_hora->orderBy('trd.orden', 'ASC')->get();
        //dd($sumatoria_rate_hora);

        return $sumatoria_rate_hora;
    }
    public function  listaProyectos($cproyecto)
    {
        $Proyectos = DB::table('tproyecto as pry')
            ->join('testadoproyecto as te','pry.cestadoproyecto','=','te.cestadoproyecto')
            ->leftjoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')
            ->leftjoin('tpersona as tpg','pry.cpersona_gerente','=','tpg.cpersona')
            ->leftjoin('tservicio as ts','ts.cservicio','=','pry.cservicio')
            ->leftjoin('ttipoproyecto as tt','tt.ctipoproyecto','=','pry.ctipoproyecto')
            ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')
            ->select('cli.nombre as cliente','tu.nombre as uminera','pry.codigo','pry.nombre as nombrepry','fproyecto',DB::raw("to_char(pry.finicio,'dd-mm-yy') as finicio"),DB::raw("to_char(pry.fcierre,'dd-mm-yy') as fcierre"),'tpg.abreviatura as gerente','pry.cproyecto','te.descripcion','ts.descripcion as portafolio','tt.descripcion as tipo_proyecto',DB::raw('CONCAT(\'row_\',pry.cproyecto)  as "DT_RowId"'))
            ->addSelect(DB::raw("(select case when 
(select percd.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
(select percd.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcd"))
            ->addSelect(DB::raw("(select case when 
(select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
(select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcp"))
            //->where('pry.cestadoproyecto','!=','000')
            ->where('pry.cproyecto','=',$cproyecto)
            ->first();

        return $Proyectos;


       // dd($infAdicional);
    }
    public function  guardardataFianzaSeguro(Request $request)
    {
       // dd($request->all());
        $tfianzaSeguro = TFianzaSeguro::where('cproyecto', '=', $request->get('cproyecto'))->first();
        if (!$tfianzaSeguro) {
            $tfianzaSeguro = new TFianzaSeguro();
        }
        $tfianzaSeguro->cproyecto     = $request->get('cproyecto');
        $tfianzaSeguro->cf            = $request->get('cf') ==  null ? null :  $request->get('cf');
        $tfianzaSeguro->srg           = $request->get('srg')  ==  null ? null :  $request->get('srg');
        $tfianzaSeguro->srp           = $request->get('srp')  ==  null ? null :  $request->get('srp');
        $tfianzaSeguro->cf_inicio     = $request->get('cfinicio') ==  null ? null :  $request->get('cfinicio');
        $tfianzaSeguro->cf_vigencia   = $request->get('cfvigencia') ==  null ? null :  $request->get('cfvigencia');
        $tfianzaSeguro->srg_inicio    = $request->get('srginicio') ==  null ? null :  $request->get('srginicio');
        $tfianzaSeguro->srg_vigencia  = $request->get('srgvigencia') ==  null ? null :  $request->get('srgvigencia');
        $tfianzaSeguro->srp_inicio    = $request->get('srpinicio') ==  null ? null :  $request->get('srpinicio');
        $tfianzaSeguro->srp_vigencia  = $request->get('srpvigencia') ==  null ? null :  $request->get('srpvigencia');
        $tfianzaSeguro->save();

        return $tfianzaSeguro;

    }
    public function guardardataMonitoreo(Request $request)
    {
        // dd($request->all());
        $tmonitoreControl = TMonitoreo::where('cproyecto', '=', $request->get('cproyecto'))->first();

        if (!$tmonitoreControl) {
            $tmonitoreControl = new TMonitoreo();
        }

        $tmonitoreControl->cproyecto     = $request->get('cproyecto');
        $tmonitoreControl->encuesta      = $request->get('radioencuesta');
        $tmonitoreControl->encuesta1     = $request->get('radioencuesta1');
        $tmonitoreControl->encuesta2     = $request->get('radioencuesta2');

        /*$tmonitoreControl->encuestafecha = $request->get('fechaencuesta')== null ? null :  $request->get('fechaencuesta');
        $tmonitoreControl->encuestafecha1 = $request->get('fechaencuesta')== null ? null : $request->get('fechaencuesta1');
        $tmonitoreControl->encuestafecha2 = $request->get('fechaencuesta')== null ? null : $request->get('fechaencuesta2');*/

       $request->get('fechaencuesta')== null ? null :  $tmonitoreControl->encuestafecha=$request->get('fechaencuesta');
       $request->get('fechaencuesta1')== null ? null :  $tmonitoreControl->encuestafecha1=$request->get('fechaencuesta1');
       $request->get('fechaencuesta2')== null ? null : $tmonitoreControl->encuestafecha2=$request->get('fechaencuesta2');

        $tmonitoreControl->save();

        return $tmonitoreControl;


    }
    public  function guardardataPlanificacion(Request $request)
    {
        $tplanificacion = TPlanificacion::where('cproyecto', '=', $request->get('cproyecto'))->first();

        if (!$tplanificacion) {
            $tplanificacion = new TPlanificacion();
        }

        $tplanificacion->cproyecto      = $request->get('cproyecto');
        $tplanificacion->komcliente     = $request->get('komcliente');
        $tplanificacion->kominterno     = $request->get('kominterno');
        $tplanificacion->lc             = $request->get('lc');
        $tplanificacion->alc            = $request->get('alc');
        $tplanificacion->croproyecto    = $request->get('croproyecto');
        $tplanificacion->ram            = $request->get('ram');
        $tplanificacion->listariesgos   = $request->get('listariesgos');
        $tplanificacion->save();

        return $tplanificacion;
    }
    public function guardardataCierre(Request $request)
    {
       // dd($request->all());

        $tcierre = TCierre::where('cproyecto', '=', $request->get('cproyecto'))->first();

        if (!$tcierre) {
            $tcierre = new TCierre();
        }

        $tcierre->cproyecto         = $request->get('cproyecto');
        $tcierre->acs               = $request->get('acs');
        $tcierre->acscodigo         = $request->get('acscodigo');
        $request->get('acsfechaenvio')== null ? null : $tcierre->acsfechaenvio=$request->get('acsfechaenvio');
        $request->get('acsfecharecepcion')== null ? null : $tcierre->acsfecharecepcion=$request->get('acsfecharecepcion');

        $tcierre->reunioncierre     = $request->get('reunioncierre');
        $tcierre->correofin         = $request->get('correofin');
        $tcierre->save();

        return $tcierre;
    }
    public function guardardataContrato(Request $request)
    {
        $tcontrato = TContrato::where('cproyecto', '=', $request->get('cproyecto'))->first();

        if (!$tcontrato) {
            $tcontrato = new TContrato();
        }

        $tcontrato->cproyecto           = $request->get('cproyecto');
        $tcontrato->conos               = $request->get('conos');
        $request->get('fechafirma')== null ? null : $tcontrato->fechafirma=$request->get('fechafirma');
        $request->get('fechainicio')== null ? null : $tcontrato->fechainicio=$request->get('fechainicio');
        $request->get('fechatermino')== null ? null : $tcontrato->fechatermino=$request->get('fechatermino');
        $tcontrato->save();

        return $tcontrato;
    }
    public function listadoSecciones()
      {
          $userautenticado = Auth::user()->cpersona;

/*          $listasecciones = DB::table('tseccionmaster')
                          ->select('descripcion','ctseccionmaster')
                            ->orderBy('ctseccionmaster','ASC')
                          ->get();*/


          return view('proyecto.MasterProyectos.Privilegios.seccionPermisos', compact('userautenticado'));

      }
    public function listarUsuario(){

        return Datatables::queryBuilder(
            DB::table('tpersona as tp')
                ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
                ->leftJoin('tpersonadatosempleado as tpde','tpde.cpersona','=','tp.cpersona')
                ->leftJoin('tareas as tar','tar.carea','=','tpde.carea')
                ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')
                ->orderBy('tp.nombre','ASC')
                ->select('tp.abreviatura','tp.cpersona','tar.descripcion as descr','tu.nombre as usuario',DB::raw('CONCAT(\'row_\',tu.cusuario) as "DT_RowId"'))
                ->where('tpi.esempleado','=','1')
                ->where('tu.estado','=','ACT')
                ->whereNotNull('tu.cusuario')
        )->make(true);

    }
    public function viewUsuario($idUsuario){

        $usuarioASIG=Db::table('tusuarios as tuser')
            ->leftjoin('tpersona as tper','tuser.cpersona','=','tper.cpersona')
            ->select('tper.nombre','tper.cpersona')
            ->where('tuser.cusuario','=',$idUsuario)
            ->first();




        $nombre =  $usuarioASIG->nombre;
        $cpersona =  $usuarioASIG->cpersona;



        $listasecciones = DB::table('tseccionmaster')
            ->select('descripcion','ctseccionmaster')
            // ->where('ctseccionmaster','=',3)
            ->orderBy('ctseccionmaster','ASC')
            ->get();

        $ola2=[];
        foreach ($listasecciones as $las) {

            $usuario=Db::table('tusuarios as tuser')
                ->leftjoin('tseccionmasterpersona as tpde','tpde.cpersona','=','tuser.cpersona')
                ->leftjoin('tpersona as tper','tpde.cpersona','=','tper.cpersona')
                ->select('tper.nombre','tuser.cpersona','tpde.ctseccionmaster','tpde.estado','tpde.lecescr')
                ->where('cusuario','=',$idUsuario)
                ->where('ctseccionmaster','=',$las->ctseccionmaster)
                ->first();
            //dd($usuario);

            $xy['descripcion'] = $las->descripcion;
            $xy['ctseccionmaster'] = $las->ctseccionmaster;
            $xy['cpersonaautenticada'] = Auth::user()->cpersona;
            $xy['cpersona'] = $usuario ? $usuario->cpersona:'';

            if ($usuario) {

                $xy['estado'] = $usuario? ($usuario->estado == 1 ? 'checked': ''):'';
                $xy['lecescr'] = $usuario->lecescr == null ? null :  $usuario->lecescr ;

                array_push($ola2,$xy);
            }


            else{
                $xy['estado'] = '';
                $xy['lecescr'] = '';

                array_push($ola2,$xy);


            }



        }
       //dd($ola2);

        return view('proyecto.MasterProyectos.Privilegios.listaseccionesBD',compact('ola2','nombre','cpersona'));
    }
    public function guardar_seccion(Request $request)
    {
       // dd($request->all());
        $seccionmasterpersona = Tseccionmasterpersona::where('cpersona', '=',$request->get('cpersonaasignada'))
                                                        ->where('ctseccionmaster', '=',$request->get('ctseccionmaster'))
                                                        ->first();

        if (!$seccionmasterpersona) {
            $seccionmasterpersona = new Tseccionmasterpersona();
            $seccionmasterpersona->created_user    = Auth::user()->cpersona;
        }

    else {

            $seccionmasterpersona->updated_user    = Auth::user()->cpersona;
        }


        $seccionmasterpersona->cpersona           = $request->get('cpersonaasignada');
        $seccionmasterpersona->ctseccionmaster    = $request->get('ctseccionmaster');
        $seccionmasterpersona->estado    = $request->get('estado');

        $seccionmasterpersona->save();

        return $seccionmasterpersona;
    }
    public function guardar_seccion_radio(Request $request)
    {
       //  dd($request->all());
        $seccionmasterpersona = Tseccionmasterpersona::where('cpersona', '=',$request->get('cpersonaasignada'))
            ->where('ctseccionmaster', '=',$request->get('ctseccionmaster'))
            ->first();

        if (!$seccionmasterpersona) {
            $seccionmasterpersona = new Tseccionmasterpersona();
            $seccionmasterpersona->created_user    = Auth::user()->cpersona;
        }

        else {

            $seccionmasterpersona->updated_user    = Auth::user()->cpersona;
        }


        $seccionmasterpersona->cpersona           = $request->get('cpersonaasignada');
        $seccionmasterpersona->ctseccionmaster    = $request->get('ctseccionmaster');
        $seccionmasterpersona->estado            = $request->get('estado');
        $seccionmasterpersona->lecescr          =          $request->get('lecescr');

        $seccionmasterpersona->save();

        return $seccionmasterpersona;
    }
    public function listadoBDProyectos()
    {
        $tproyectos = $this->CapturarProyectosPorCargo();
       // dd($tproyectos);

        foreach ($tproyectos as $key =>$tp )
        {
            /***************************
             * 01 Lista Preliminar
             ****************************/
            $listaCompletaBDpry = $this->listadoPreliminarProyecto($tp->cproyecto);
            $tp->listaCompletaBD=[$listaCompletaBDpry];
            $tp->listaCompletaBD[0]->auditado = $tp->listaCompletaBD[0]->auditado == true ? 'Si' : 'No';
            $tp->listaCompletaBD[0]->codigoCliente = substr($tp->listaCompletaBD[0]->codigo,0,4);
            $fasePry = $this->faseProyecto($tp->cproyecto);
            $tp->listaCompletaBD[0]->fase=$fasePry;


            /***************************
             * 02 Etapa del Proyecto - Inicio
             ****************************/

            $tp->listaCompletaBD[0]->correoinicio=$tp->listaCompletaBD[0]->correoinicio==  true ? 'Si' : 'No';
            $sumaMontoPresupuestadasNivelProyecto = $this->montoPresupuestado($tp->cproyecto);
            $tp->listaCompletaBD[0]->montopresupuestado=$sumaMontoPresupuestadasNivelProyecto ? $sumaMontoPresupuestadasNivelProyecto : '-';
            $tp->listaCompletaBD[0]->cpropuesta=$tp->listaCompletaBD[0]->cpropuesta !=  null ? $tp->listaCompletaBD[0]->cpropuesta : '-';
            $tp->listaCompletaBD[0]->fadjucicacion=$tp->listaCompletaBD[0]->fadjucicacion !=  null ? $tp->listaCompletaBD[0]->fadjucicacion : '-';
            $tp->listaCompletaBD[0]->fadjucicacion=$tp->listaCompletaBD[0]->fadjucicacion !=  null ? $tp->listaCompletaBD[0]->fadjucicacion : '-';
            $tp->listaCompletaBD[0]->finicio=$tp->listaCompletaBD[0]->finicio !=  null ? $tp->listaCompletaBD[0]->finicio : '-';
            $tp->listaCompletaBD[0]->fcierre=$tp->listaCompletaBD[0]->fcierre !=  null ? $tp->listaCompletaBD[0]->fcierre : '-';
            $tp->listaCompletaBD[0]->descripcion_contrato=$tp->listaCompletaBD[0]->descripcion_contrato !=  null ? $tp->listaCompletaBD[0]->descripcion_contrato : '-';
            $tp->listaCompletaBD[0]->descripcion_lugar=$tp->listaCompletaBD[0]->descripcion_lugar !=  null ? $tp->listaCompletaBD[0]->descripcion_lugar : '-';
            $tp->listaCompletaBD[0]->descripcion_moneda=$tp->listaCompletaBD[0]->descripcion_moneda !=  null ? $tp->listaCompletaBD[0]->descripcion_moneda : '-';
            $hoPresupuestadas = $this->horasPresupuestadas($tp->cproyecto);
            $tp->listaCompletaBD[0]->horaspresupuestadas= $hoPresupuestadas ==  null ? 0 : $hoPresupuestadas->horaspresupuestadas;
            $numdoc  = $this->numdoc($tp->cproyecto);
            $tp->listaCompletaBD[0]->numdoc = $numdoc ? $numdoc : '-' ;
            $numplan = $this->numpla($tp->cproyecto) ;
            $tp->listaCompletaBD[0]->numplan= $numplan ? $numplan : '-' ;
            $numfig  = $this->numfig($tp->cproyecto);
            $tp->listaCompletaBD[0]->numfig= $numfig ? $numfig : '-' ; ;
            $nummap = $this->nummap($tp->cproyecto);
            $tp->listaCompletaBD[0]->nummap= $nummap ? $nummap : '-' ; ;
            $tp->listaCompletaBD[0]->plazopagodias= $tp->listaCompletaBD[0]->plazopagodias==null? '-' :$tp->listaCompletaBD[0]->plazopagodias;
            $diasPPlazo = $this->pplazo($tp->listaCompletaBD[0]->finicio,$tp->listaCompletaBD[0]->fcierre);
            $tp->listaCompletaBD[0]->pplazo = $diasPPlazo;
            $tp->listaCompletaBD[0]->diascorte= $tp->listaCompletaBD[0]->diascorte ==  null ? '-' : $tp->listaCompletaBD[0]->diascorte;
            $tp->listaCompletaBD[0]->hoja_resumen= $tp->listaCompletaBD[0]->hoja_resumen == null ? 'No' : 'Si';

            /***************************
             * 03 - Etapa del Proyecto: Inicio - Carta Fianza y Seguros
             ****************************/
            $fianzaGastos =   $this->fianzaGastos($tp->cproyecto);
            $tp->listaCompletaBD[0]->cf= $fianzaGastos ?  $fianzaGastos->cf : '-';
            $tp->listaCompletaBD[0]->cf_inicio= $fianzaGastos ?  $fianzaGastos->cf_inicio : '-';
            $tp->listaCompletaBD[0]->cf_vigencia= $fianzaGastos ?  $fianzaGastos->cf_vigencia : '-';
            $tp->listaCompletaBD[0]->srg= $fianzaGastos ?  $fianzaGastos->srg : '-';
            $tp->listaCompletaBD[0]->srg_inicio= $fianzaGastos ?  $fianzaGastos->srg_inicio : '-';
            $tp->listaCompletaBD[0]->srg_vigencia= $fianzaGastos ?  $fianzaGastos->srg_vigencia : '-';
            $tp->listaCompletaBD[0]->srp= $fianzaGastos ?  $fianzaGastos->srp : '-';
            $tp->listaCompletaBD[0]->srp_inicio= $fianzaGastos ?  $fianzaGastos->srp_inicio : '-';
            $tp->listaCompletaBD[0]->srp_vigencia= $fianzaGastos ?  $fianzaGastos->srp_vigencia : '-';

            /***************************
             * 04 - Etapa del Proyecto: Inicio - Contratos
             ****************************/
            $tcontrato =   $this->contratos($tp->cproyecto);
            $tp->listaCompletaBD[0]->conos= $tcontrato ?  $tcontrato->conos : '-';
            $tp->listaCompletaBD[0]->fechafirma= $tcontrato ?  $tcontrato->fechafirma : '-';
            $tp->listaCompletaBD[0]->fechainicio= $tcontrato ?  $tcontrato->fechainicio : '-';
            $tp->listaCompletaBD[0]->fechatermino= $tcontrato ?  $tcontrato->fechatermino : '-';
            $tp->listaCompletaBD[0]->fechatermino= $tcontrato ?  $tcontrato->fechatermino : '-';
            $tp->listaCompletaBD[0]->fechatermino= $tcontrato ?  $tcontrato->fechatermino : '-';
            $tp->listaCompletaBD[0]->fechatermino= $tcontrato ?  $tcontrato->fechatermino : '-';
            $tp->listaCompletaBD[0]->fechatermino= $tcontrato ?  $tcontrato->fechatermino : '-';
            $tp->listaCompletaBD[0]->fechatermino= $tcontrato ?  $tcontrato->fechatermino : '-';

            /***************************
             * 05 - Etapa del Proyecto: Planificación
             ****************************/
            $tlanificacion =   $this->planificacion($tp->cproyecto);
            $pryEDT = $this->pryEDT($tp->cproyecto);
            $pryLE = $this->pryLE($tp->cproyecto);

            $tp->listaCompletaBD[0]->komcliente     = $tlanificacion ?$tlanificacion->komcliente : '-';
            $tp->listaCompletaBD[0]->kominterno     = $tlanificacion ?$tlanificacion->kominterno : '-';
            $tp->listaCompletaBD[0]->lc     = $tlanificacion ?$tlanificacion->lc : '-';
            $tp->listaCompletaBD[0]->alc     = $tlanificacion ?$tlanificacion->alc : '-';
            $tp->listaCompletaBD[0]->croproyecto     = $tlanificacion ?$tlanificacion->croproyecto : '-';
            $tp->listaCompletaBD[0]->edt     =  $pryEDT  == null ? 'No' : 'Si';
            $tp->listaCompletaBD[0]->le     =$pryLE  == null ? 'No' : 'Si';
            $tp->listaCompletaBD[0]->ram     = $tlanificacion ?$tlanificacion->ram : '-';
            $tp->listaCompletaBD[0]->listariesgos     = $tlanificacion ?$tlanificacion->listariesgos : '-';

            /***************************
             * 06 - Etapa del Proyecto: Monitoreo y Control
             ****************************/

            $pryPer = $this->performance_curvaS($tp->cproyecto);
            $monitoreoControl = $this->monitoreoControl($tp->cproyecto);
            $tp->listaCompletaBD[0]->performance     =  $pryPer  == null ? 'No' : 'Si';
            $tp->listaCompletaBD[0]->curvas          =  $pryPer  == null ? 'No' : 'Si';
            $tp->listaCompletaBD[0]->encuesta        = $monitoreoControl ?$monitoreoControl->encuesta : '-';
            $tp->listaCompletaBD[0]->encuesta1       = $monitoreoControl ?$monitoreoControl->encuesta1 : '-';
            $tp->listaCompletaBD[0]->encuesta2       = $monitoreoControl ?$monitoreoControl->encuesta2 : '-';
            $tp->listaCompletaBD[0]->encuestafecha   = $monitoreoControl ?$monitoreoControl->encuestafecha : '-';
            $tp->listaCompletaBD[0]->encuestafecha1  = $monitoreoControl ?$monitoreoControl->encuestafecha1 : '-';
            $tp->listaCompletaBD[0]->encuestafecha2  = $monitoreoControl ?$monitoreoControl->encuestafecha2 : '-';

            /***************************
             * 07 - Etapa del Proyecto: Cierre
             ****************************/

            $pryLA =  $this->listaentregables($tp->cproyecto);
            $tcierre =  $this->cierreProyecto($tp->cproyecto);
            $tp->listaCompletaBD[0]->la            = $pryLA  == null ? 'No' : 'Si';
            $tp->listaCompletaBD[0]->acs      = $tcierre ?$tcierre->acs : '-';
            $tp->listaCompletaBD[0]->acscodigo  = $tcierre ?$tcierre->acscodigo : '-';
            $tp->listaCompletaBD[0]->reunioncierre  = $tcierre ?$tcierre->reunioncierre : '-';
            $tp->listaCompletaBD[0]->correofin  = $tcierre ?$tcierre->correofin : '-';
            $tp->listaCompletaBD[0]->acsfechaenvio  = $tcierre ?$tcierre->acsfechaenvio : '-';
            $tp->listaCompletaBD[0]->acsfecharecepcion  = $tcierre ?$tcierre->acsfecharecepcion : '-';


        }

        $cpersona = Auth::user()->cpersona;
        $columnas_nombre = Tvistastablaspersona::columnas_nombre($cpersona,4);
        $columnas_id = Tvistastablaspersona::columnas_id($cpersona,4);
        $selecion_opcion = Tvistastablaspersona::selecion_opcion($cpersona,4,'selected');


        // dd($columnas_nombre);
        return view('proyecto.MasterProyectos.BD_ListadoProyectos.bdListadoProyectos', compact('tproyectos','columnas_nombre','columnas_id','selecion_opcion'));


    }
    private function listadoPreliminarProyecto($cproyecto)
    {

        $arrayProyectoSQL =  DB::table('tproyecto as pry')
            ->join('testadoproyecto as te','pry.cestadoproyecto','=','te.cestadoproyecto')
            ->leftjoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')
            ->leftjoin('tpersona as tpg','pry.cpersona_gerente','=','tpg.cpersona')
            ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')
            ->leftjoin('tproyectopersona as pcp','pry.cproyecto','=','pcp.cproyecto')
            ->leftjoin('tpersona as percp','pcp.cpersona','=','percp.cpersona')
            ->leftjoin('tproyectopersona as pcd','pry.cproyecto','=','pcd.cproyecto')
            ->leftjoin('tpersona as percd','pcd.cpersona','=','percd.cpersona')
            ->leftjoin('testadoproyecto as testp','testp.cestadoproyecto','=','pry.cestadoproyecto')
            ->leftjoin('tproyectoinformacionadicional as tproyinfadi','tproyinfadi.cproyecto','=','pry.cproyecto')
            ->leftjoin('tpersona as tperso','tproyinfadi.cliente_final','=','tperso.cpersona')
            ->leftjoin('ttipogestionproyecto as ttipogesti','ttipogesti.ctipogestionproyecto','=','pry.ctipogestionproyecto')
            ->leftjoin('ttipoproyecto as ttipoproy','ttipoproy.ctipoproyecto','=','pry.ctipoproyecto')
            ->leftjoin('tservicio as tser','tser.cservicio','=','ttipoproy.cservicio')
            ->leftjoin('tpropuesta as tpuesta','tpuesta.cproyecto','=','pry.cproyecto')
            ->leftjoin('tformacotizacion as tforCoti','tforCoti.cformacotizacion','=','tpuesta.cformacotizacion')
            ->leftjoin('tproyectoinformacionadicional as tpryinfadic','tpryinfadic.cproyecto','=','pry.cproyecto')
            ->leftjoin('tlugartrabajo as tlugtra','tlugtra.clugartrabajo','=','tpryinfadic.clugartrabajo')
            ->leftjoin('tmonedas as tmon','tmon.cmoneda','=','tpuesta.cmoneda')
            ->leftjoin('tproyectoactividades as tpacti','tpacti.cproyecto','=','pry.cproyecto')
            ->leftjoin('tproyectohonorarios as tpyh','tpyh.cproyectoactividades','=','tpacti.cproyectoactividades')
            ->leftjoin('tproyectodocumentos as tpydoc','tpydoc.cproyecto','=','pry.cproyecto')
            ->leftjoin('tproyectoedt as tpryedt','tpryedt.cproyecto','=','pry.cproyecto')


            ->select('cli.nombre as cliente')
            ->addSelect(DB::raw("(select case when 
            (select percd.abreviatura as nomcd from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
            (select percd.abreviatura as nomcd from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcd"))
            ->addSelect(DB::raw("(select case when 
            (select percd.identificacion as identificacioncd from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
            (select percd.identificacion as identificacioncd from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as identificacioncd"))
            ->addSelect(DB::raw("(select case when 
            (select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
            (select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcp"))
            ->addSelect(DB::raw("(select case when 
            (select percp.identificacion as identificacioncp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
            (select percp.identificacion as identificacioncp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as identificacioncp"))
            ->addSelect('tperso.nombre as nombreclientefinal','tser.descripcion as nombreservicio','ttipoproy.descripcion as tipoproynombre','ttipogesti.descripcion as nombretipogestion','tu.nombre as uminera','pry.codigo','pry.nombre as nombrepry','pry.descripcion as descpry','tpg.abreviatura as gerente','tpg.identificacion as identificaciongp','pry.cproyecto','te.descripcion','testp.descripcion as descripcion_estado')
            ->addSelect('tpuesta.cpropuesta',DB::raw("to_char(tpuesta.fadjucicacion,'dd-mm-yy') as fadjucicacion"))
            ->addSelect(DB::raw("to_char(pry.finicio,'dd-mm-yyyy') as finicio"),DB::raw("to_char(pry.fcierre,'dd-mm-yyyy') as fcierre"),'tforCoti.descripcion as descripcion_contrato','tlugtra.descripcion as descripcion_lugar')
            ->addSelect('tmon.descripcion as descripcion_moneda','tpryinfadic.diascorte','tpryinfadic.plazopagodias')
            ->addSelect('tpryinfadic.correoinicio','tpydoc.cproyecto as hoja_resumen','tpryinfadic.nrodocaprobacion','cli.abreviatura as abrv','tproyinfadi.auditado')
            ->where('pry.cproyecto','=',$cproyecto)
            ->first();

        return $arrayProyectoSQL;
    }
    private function faseProyecto($cproyecto)
    {
        $faseProyecto = DB::table('tproyectoentregables as pryEnt')
            ->leftjoin('tfasesproyecto_gestion as tfpg','pryEnt.cfaseproyecto','=','tfpg.cfaseproyecto_gestion')
            ->select('tfpg.codigo')
            ->where('pryEnt.cproyecto','=',$cproyecto)
            ->orderBy('tfpg.codigo','ASC')
            ->distinct()
            ->get();

        $abc= [];
        foreach ($faseProyecto as $fpr) {
            $fasedelproyecto = $fpr->codigo;
            array_push($abc,$fasedelproyecto);
        }
        if ($abc) {
            $faPy =   implode("-",$abc);
        }

        else{
            $faPy = '';
        }

        return $faPy;
    }
    private function montoPresupuestado($cproyecto)
    {

        // Monto Presupuestados

        $hijosdirectos = $this->listadeActividadesHijosDirectosporProyecto($cproyecto);
        $sumaMontoPresupuestadasNivelProyecto  = 0;
        foreach ($hijosdirectos as $hd)
        {
            $acumulativomontopresupuestadasHijos = 0;
            $acumulativomontopresupuestadasNietos = 0;
            $acumulativomontopresupuestadasBisnieto = 0;

            $listadeactividadesHijos = $this->actividad_padre_or_hijo($hd->cproyectoactividades);

            foreach ($listadeactividadesHijos as $hdH)
            {
                $listadeactividadesNietos = $this->actividad_padre_or_hijo($hdH->cproyectoactividades);
                $montopresupuestdosHijos = $this->obteneratepresuestadas_condetalle($hdH->cproyectoactividades,'Nuevo',$cproyecto)['sumtotalPresupuestadas'];
                $acumulativomontopresupuestadasHijos +=  $montopresupuestdosHijos;

                foreach ($listadeactividadesNietos as $hdN)
                {
                    $listadeactividadesBisnietos = $this->actividad_padre_or_hijo($hdN->cproyectoactividades);
                    $montopresupuestdosNietos = $this->obteneratepresuestadas_condetalle($hdN->cproyectoactividades,'Nuevo',$cproyecto)['sumtotalPresupuestadas'];
                    $acumulativomontopresupuestadasNietos += $montopresupuestdosNietos;
                    foreach ($listadeactividadesBisnietos as $hdB)
                    {
                        $montopresupuestdosBisnietos = $this->obteneratepresuestadas_condetalle($hdB->cproyectoactividades,'Nuevo',$cproyecto)['sumtotalPresupuestadas'];
                        $acumulativomontopresupuestadasBisnieto += $montopresupuestdosBisnietos;

                    }
                }
            }

            $subtotalMontoHijosDirectos =$acumulativomontopresupuestadasHijos + $acumulativomontopresupuestadasNietos + $acumulativomontopresupuestadasBisnieto;
            $sumaMontoPresupuestadasNivelProyecto+=$subtotalMontoHijosDirectos;
        }

        return $sumaMontoPresupuestadasNivelProyecto;
    }

    public function horasPresupuestadas($cproyecto)
    {
        $horasPresupuestados = DB::table('tproyecto as tpy')
            ->leftjoin('tproyectoactividades as tpa','tpa.cproyecto','=','tpy.cproyecto')
            ->leftjoin('tproyectohonorarios as tph','tph.cproyectoactividades','=','tpa.cproyectoactividades')
            ->select(DB::raw("sum(to_number(tph.horas, '999.99')) as horaspresupuestadas"))
            ->where('tpy.cproyecto','=',$cproyecto)
            ->first();



        return $horasPresupuestados;
    }
    public function ultimorevision($cproyecto){
        $ultimarevision =  DB::table('tproyectoedt')
            ->select('cproyectoent_rev')
            ->where('cproyecto','=',$cproyecto)
            ->orderBy('cproyectoent_rev', 'desc')
            ->take(1)
            ->first();

        $listaedtultimarevision =  DB::table('tproyectoedt')
            ->select('cproyecto','cproyectoedt')
            ->where('cproyecto','=',$cproyecto)
            ->where('cproyectoent_rev','=',$ultimarevision ? $ultimarevision->cproyectoent_rev : null)
            ->lists('cproyectoedt');

        $listaentregables =  DB::table('tproyectoentregables')
            ->select('centregable')
            ->whereIn('cproyectoedt',$listaedtultimarevision)
            ->lists('centregable');

        return $listaentregables;

    }
    public function numdoc($cproyecto){

        $listaentregables = $this->ultimorevision($cproyecto);
        $numdoc =  DB::table('tentregables as tentreg')
            ->whereIn('centregables',$listaentregables)
            ->where('ctipoentregable','=','001')
            ->count();
        return $numdoc;
    }
    public function numpla($cproyecto){
        $listaentregables = $this->ultimorevision($cproyecto);
        $numplan =  DB::table('tentregables as tentreg')
            ->whereIn('centregables',$listaentregables)
            ->where('ctipoentregable','=','002')
            ->count();
        return $numplan;
    }
    public function numfig($cproyecto){
        $listaentregables = $this->ultimorevision($cproyecto);
        $numfig = DB::table('tentregables as tentreg')
            ->whereIn('centregables',$listaentregables)
            ->where('ctipoentregable','=','003')
            ->count();
        return $numfig;
    }
    public function nummap($cproyecto){
        $listaentregables = $this->ultimorevision($cproyecto);
        $nummap = DB::table('tentregables as tentreg')
            ->whereIn('centregables',$listaentregables)
            ->where('ctipoentregable','=','004')
            ->count();
        return $nummap;
    }

    public function pplazo($f__inicio,$f__cierre)
    {
       /* $finicio = $arrayProyectoSQL->finicio == null ? null : Carbon::parse($arrayProyectoSQL->finicio);
        $fcierre = $arrayProyectoSQL->fcierre == null ? null : Carbon::parse($arrayProyectoSQL->fcierre);*/

        $finicio = $f__inicio == '-' ? '-'  : Carbon::parse($f__inicio);
        $fcierre = $f__cierre == '-' ? '-'  : Carbon::parse($f__cierre);

        if ($finicio == '-'  || $fcierre == '-' ) {
            $diasPPlazo = 'No se puede determinar .';
        }

        elseif ($finicio == '-'  && $fcierre == '-' ) {
            $diasPPlazo = 'No se puede determinar .';
        }

        else{
            $diasPPlazo = $fcierre->diffInDays($finicio);
        }
        return $diasPPlazo;

    }

    public function fianzaGastos($cproyecto)
    {

        $fianzaGastos =   DB::table('tfianzaseguros as tfs')
            ->select('cf','srg','srp')
            ->addSelect(DB::raw("to_char(cf_inicio,'dd-mm-yy') as cf_inicio"))
            ->addSelect(DB::raw("to_char(cf_vigencia,'dd-mm-yy') as cf_vigencia"))
            ->addSelect(DB::raw("to_char(srg_inicio,'dd-mm-yy') as srg_inicio"))
            ->addSelect(DB::raw("to_char(srg_vigencia,'dd-mm-yy') as srg_vigencia"))
            ->addSelect(DB::raw("to_char(srp_inicio,'dd-mm-yy') as srp_inicio"))
            ->addSelect(DB::raw("to_char(srp_vigencia,'dd-mm-yy') as srp_vigencia"))
            ->where('tfs.cproyecto','=',$cproyecto)
            ->first();
        return $fianzaGastos;

    }

    public function contratos ($cproyecto)
    {
        $tcontrato =  DB::table('tcontrato as tcont')
            ->select('conos')
            ->addSelect(DB::raw("to_char(fechafirma,'dd-mm-yy') as fechafirma"))
            ->addSelect(DB::raw("to_char(fechainicio,'dd-mm-yy') as fechainicio"))
            ->addSelect(DB::raw("to_char(fechatermino,'dd-mm-yy') as fechatermino"))
            ->where('tcont.cproyecto','=',$cproyecto)
            ->first();
        return $tcontrato;

    }

    public function planificacion($cproyecto)
    {
        $tlanificacion  = DB::table('tplanificacion as tpla')
            ->select('komcliente','kominterno','lc','alc','croproyecto','ram','listariesgos')
            ->where('tpla.cproyecto','=',$cproyecto)
            ->first();

        return $tlanificacion;

    }

    public function pryEDT($cproyecto)
    {
        $pryEDT =  DB::table('tproyectoedt as pryEDT')
            ->select('pryEDT.estado','pryEDT.cproyectoent_rev','pryEDT.cproyectoedt')
            ->where('pryEDT.cproyecto','=',$cproyecto)
            ->where('pryEDT.estado','=',1)
            ->orderBy('pryEDT.cproyectoent_rev','DESC')
            ->get();
        return $pryEDT;
    }

    public function pryLE($cproyecto)
    {
        $pryLE = DB::table('tproyectoentregables as pryLE')
            ->select('pryLE.cproyectoentregables','pryLE.cproyecto','pryLE.cestadoentregable')
            ->where('pryLE.cestadoentregable','!=','002')
            ->where('pryLE.cproyecto','=',$cproyecto)
            ->get();
        return $pryLE;
    }

    public function performance_curvaS($cproyecto)
    {
        $pryPer = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyecto as tpy','tpa.cproyecto','=','tpy.cproyecto')
            ->leftjoin('tperformance as tper','tper.cproyectoactividades','=','tpa.cproyectoactividades')
            ->select('tper.cperformance','tpy.cproyecto')
            ->where('tpy.cproyecto','=',$cproyecto)
            ->whereNotNull('tper.cperformance')
            ->get();
        return $pryPer;
    }

    public function monitoreoControl($cproyecto)
    {
        $monitoreoControl = DB::table('tmonitoreo as tmn')
            ->select('encuesta','encuesta1','encuesta2')
            ->addSelect(DB::raw("to_char(encuestafecha,'dd-mm-yy') as encuestafecha"))
            ->addSelect(DB::raw("to_char(encuestafecha1,'dd-mm-yy') as encuestafecha1"))
            ->addSelect(DB::raw("to_char(encuestafecha2,'dd-mm-yy') as encuestafecha2"))
            ->where('tmn.cproyecto','=',$cproyecto)
            ->first();

        return $monitoreoControl;
    }

    public function listaentregables($cproyecto)
    {
        $pryLA =  DB::table('tleccionesaprendidas as tla')
            ->select('tla.cleccionaprendida','tla.cproyecto')
            ->leftjoin('tproyecto as tpy','tla.cproyecto','=','tpy.cproyecto')
            ->where('tla.cproyecto','=',$cproyecto)
            ->get();
        return $pryLA;

    }

    public function cierreProyecto($cproyecto)
    {
        $tcierre =  DB::table('tcierre as tcr')
            ->select('acs','acscodigo','reunioncierre','correofin')
            ->addSelect(DB::raw("to_char(acsfechaenvio,'dd-mm-yy') as acsfechaenvio"))
            ->addSelect(DB::raw("to_char(acsfecharecepcion,'dd-mm-yy') as acsfecharecepcion"))
            ->where('tcr.cproyecto','=',$cproyecto)
            ->first();
        return $tcierre;
    }











}
