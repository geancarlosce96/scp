<?php

namespace App\Http\Controllers\Usuario;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tcatalogo;
use App\Models\Tarea;
use App\Models\Tpersona;
use App\Models\Tusuario;
use App\Models\Tusuariopassword;
use App\User;
use Carbon\Carbon;
use Redirect;
use Auth;
use Session;
use Response;
use DB;

class UsuarioController extends Controller
{

	public function listarUsuarios(){
		return Datatables::queryBuilder(DB::table('tusuarios as tu')
			->join('tpersona as tp','tu.cpersona','=','tp.cpersona')
			->join('tpersonanaturalinformacionbasica as tpn','tu.cpersona','=','tpn.cpersona')
			->leftJoin('tcatalogo as tc','tu.estado','=','tc.digide')
			->leftJoin('tpersonadatosempleado as tpe','tu.cpersona','=','tpe.cpersona')
			->leftJoin('tareas as tca','tpe.carea','=','tca.carea')
			->select('tp.cpersona','tu.nombre','tp.identificacion','tp.nombre as nombrepersona','tc.descripcion as estado','tca.descripcion as area',DB::raw('CONCAT(\'rowrdp_\',tu.cusuario)   as "DT_RowId"'))
			->where('tpn.esempleado','=','1')
			->where('tc.codtab','=','00024')
			)->make(true);
	}

	public function listadoUsuarios(){

		return view('sistema/registroUsuario');
	}


	public function registroUsuarios(){

		$persona = DB::table('tpersona')
		->first();
		
		$usuario= DB::table('tusuarios')
		->first();

		$usuariopassword=DB::table('tusuariopassword')
		->first();

		//combos
		$testado= Tcatalogo::where('codtab','=','00024')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$testado = [''=>''] + $testado;

		$tarea = DB::table('tareas')
		->lists('descripcion','carea');
		$tarea = [''=>''] + $tarea;

		$tnombrepersona =DB::table('tpersona as tp')
        ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')
        ->where('tpi.esempleado','=','1')
       ->whereNull('tu.cusuario')
        ->orderBy('tp.nombre','ASC')
        ->lists('tp.nombre','tp.cpersona');
        $tnombrepersona = [''=>''] + $tnombrepersona; 

    

		return view('sistema/registroUsuario',compact('testado','tarea','tnombrepersona','tusuarios'));
	}

	public function editarUsuarios(Request $request,$idUsuario){

		$persona = DB::table('tpersona')
		->first();

		$usuario= DB::table('tusuarios')
		->where('cusuario','=',$idUsuario)	
		->first();

		$usuariopassword=DB::table('tusuariopassword')
		->where('cusuario','=',$idUsuario)
		->first();

		//Combos
		$testado= Tcatalogo::where('codtab','=','00024')		
		->lists('descripcion','digide')->all();  
		$testado = [''=>''] + $testado;

		$tarea = DB::table('tareas')
		->lists('descripcion','carea');
		$tarea = [''=>''] + $tarea;

		//Fin Combos
		
		$tnombrepersona =DB::table('tpersona as tp')
        ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        ->leftJoin('tusuarios as tu','tp.cpersona','=','tu.cpersona')
        ->where('tpi.esempleado','=','1')
 //       ->whereNull('tu.cusuario')
        ->orderBy('tp.nombre','ASC')
        ->lists('tp.nombre','tp.cpersona');
        $tnombrepersona = [''=>''] + $tnombrepersona;  

      	$tusuarios=DB::table('tusuarios as tu')
	
			->join('tpersona as tp','tu.cpersona','=','tp.cpersona')
			->join('tpersonanaturalinformacionbasica as tpn','tu.cpersona','=','tpn.cpersona')
			->leftJoin('tcatalogo as tc','tu.estado','=','tc.digide')
			->leftJoin('tpersonadatosempleado as tpe','tu.cpersona','=','tpe.cpersona')
			->leftJoin('tareas as tca','tpe.carea','=','tca.carea')
			->select('tu.cusuario','tu.nombre','tp.nombre as nombrepersona','tc.descripcion as estado','tca.descripcion as area')
			->where('tpn.esempleado','=','1')
			->where('tu.cusuario','=',$idUsuario)
			->orderBy('tu.nombre')
			->where('tc.codtab','=','00024')
			->get(); 
		
    return view('partials/addUsuario',compact('persona','testado','tarea','tnombrepersona','usuario','usuariopassword'));

	}
	public function agregarUsuario(Request $request){
		DB::beginTransaction();
		$cpersona = $request->input('cpersona');
		$cusuario = $request->input('cusuario');

		if(strlen($cusuario)<=0){
			$usuario = new Tusuario();
			$usuario->cpersona= $request->input('cpersona');
			$usuario->save();
			
			$cusuario= $usuario->cusuario;
			
			$usuariopassword=new Tusuariopassword();	
			$usuariopassword->cusuario = $cusuario;
 
			
		}else{
			$usuario = Tusuario::where('cusuario','=',$cusuario)
			->first();
			$usuariopassword=Tusuariopassword::where('cusuario','=',$cusuario)
			->first();


		}

		$usuario->nombre= $request->input('nombre');
		$usuario->estado= $request->input('estado');				
		$usuariopassword->fhasta= $request->input('fhasta');
		$usuariopassword->fdesde= $request->input('fdesde');
		if(strlen($request->input('password')!=null)){
			$usuariopassword->password= md5($request->input('password'));
			$usuario->remember_token=null;
		}		
		$usuariopassword->caduca= $request->input('caduca');
		$usuario->save();
		$usuariopassword->save();
		
		DB::commit();    

	return Redirect::route('registroUsuario');  
	}	

	public function passwordUsuario(){



		$usuario = Auth::user()->cpersona;
		$nuevo='no';

		$cusuario=Tusuario::where('cpersona','=',$usuario)
		->first();

		$usuariopassword=Tusuariopassword::where('cusuario','=',$cusuario->cusuario)
		->first();

		$nomusuario=$cusuario->nombre;

		//$usuario=$cusuario->cusuario;

		$passwordActual=$usuariopassword->password;
		return view('sistema.registroPassword',compact('usuario','nuevo','passwordActual','nomusuario'));
		  
	}

	public function agregarPassword(Request $request){

		
		DB::beginTransaction();


		$idUsuario=$request->input('cusuario');
		$newclave=$request->input('password');

			$cusuario=Tusuario::where('cpersona','=',$idUsuario)
			->first();

			$usuariopassword=Tusuariopassword::where('cusuario','=',$cusuario->cusuario)
			->first();
		$usuario=$cusuario->cusuario;
		// dd($usuariopassword);

		if(strlen($request->input('password')!=null)){
			$usuariopassword->password= md5($request->input('password'));
			$usuariopassword->save();
		}
				
		Auth::logout();


		DB::commit();  

		return 'ok';  


        //return Redirect::route('login');
	}

	public function getPasswordEncrip(Request $request){
		$pass='';

		if ($request->input('passwordAct')!='') {
			$pass=	md5($request->input('passwordAct'));	
			
			return $pass;
		}


	}
    
}
