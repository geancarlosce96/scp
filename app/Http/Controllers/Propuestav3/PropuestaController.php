<?php

namespace App\Http\Controllers\Propuestav3;

use App\Http\Controllers\Controller;
use App\Models\Tpersona;
use App\Models\Contacto;
use App\Models\Invitacion;
use App\Models\Prepropuesta;
use App\Models\Propuesta;
use App\Models\TagEstructura;
use App\Models\TagServicio;
use App\Models\Tunidadminera;
use App\Models\TagServicioGrupo;
use App\Models\TipoServicio;
use Auth;
use DB;
use Illuminate\Http\Request;

class PropuestaController extends Controller
{
	//public functions 

	public function inicio()
	{
		$data["umineras"] = Tunidadminera::AllWithOrder();
		$data["clientes"] = Tpersona::AllClientsWithOrder();
		$data["tag_estructuras"] = TagEstructura::all();
		$data["tag_servicios"] = TagServicioGrupo::allWithServicios();
		$data["tipo_servicio"] = TipoServicio::AllWithOrder();
		
		return view("propuestav3.inicio", $data);
	}

	public function planificacion()
	{
		$data["umineras"] = Tunidadminera::AllWithOrder();
		$data["clientes"] = Tpersona::AllClientsWithOrder();
		$data["tag_estructuras"] = TagEstructura::all();
		$data["tag_servicios"] = TagServicioGrupo::allWithServicios();
		$data["tipo_servicio"] = TipoServicio::AllWithOrder();

		return view("propuestav3.planificacion", $data);
	}

	public function ejecucion()
	{
		return view("propuestav3.ejecucion");
	}

	public function listas()
	{
		$data["tipo_servicio"] = TipoServicio::AllWithOrder();
		$data["lista"] = Propuesta::allFields();

		return view("propuestav3.listas", $data);
	}



	public function inicioPost(Request $request)
	{	
		$chk_tag_estructuras = $request->input("chk_tag_estructuras");
		$chk_tag_servicios = $request->input("chk_tag_servicios");

		$propuesta = new Propuesta();
		$propuesta->generarCodigo();
		$propuesta->cliente_id = $request->input("cliente_id");
		$propuesta->tipo_servicio_id = $request->input("tipo_servicio_id");
		$propuesta->unidad_minera_id = $request->input("unidad_minera_id");
		$propuesta->tipo_registro = $request->input("tipo_registro");
		$propuesta->propuesta = $request->input("propuesta");
		$propuesta->save();

		$propuesta->guardarTag($chk_tag_estructuras, $chk_tag_servicios);
		
		return redirect("propuestas/listas");
	}

	public function planificacionPost(Request $request)
	{
		//unset($request->all()['_token']);

		$propuesta = Propuesta::where('id', $request->id )
          ->update($request->except('_token'));

		/*$propuesta = Propuesta::updated(
		    $request->except('_token')
		);

		$propuesta->save();*/

		return $propuesta;

		return redirect("propuestas/listas") ;
	}

	//public ajax functions

	public function obtenerLista(Request $request)
	{
		return Propuesta::allFields(); //*temp

		$filtros = $request->input("fitros");		
		//filtros 
		$lista = Propuesta::orderBy("id", "ASC");
		//obtener 
		/*foreach ($filtros as $key => $filtro) {
			
		}*/

		$lista = $lista->get();
		
		$data["lista"] = $lista;
		return $data;
	}

	//private function 

	private function valor($val)
	{
		if($val == "")
			return NULL;
	}
}
