<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\TagEstructura;
use App\Models\TagServicioGrupal;
use App\Models\TagServicio;
use Auth;
use DB;

class TagController extends Controller
{
	public function obtenerTagEstructura()
	{		
		$tags = [];
		$lista = TagEstructura::all(); 

		foreach ($lista as $key => $item)
			array_push($tags, $item->tag);

		return $tags;
	}

	public function obtenerTagServicioGrupal()
	{
		return TagServicioGrupal::all();
	}

	public function obtenerTagServicio($id = 0)
	{
		if($id!=0)
			return TagServicio::where("tag_servicio_grupal", "=", $id)->get();

		$tags = [];
		$lista = TagServicio::all(); 

		foreach ($lista as $key => $item)
			array_push($tags, $item->tag);

		return $tags;
	}

}
