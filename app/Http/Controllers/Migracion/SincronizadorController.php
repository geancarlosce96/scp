<?php 
namespace App\Http\Controllers\Migracion;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use DB;
use App\Models\Erp_colaborador_usuario;
use App\Models\Tpersona;
use App\Models\Tpersonanaturalinformacionbasica;
use App\Models\Tpersonadatosempleado;
use App\Models\Tusuario;
use App\Models\Tusuariopassword;
use App\Models\Tmenupersona;
use App\Models\Tcatalogo;
use App\Models\Tarea;
use App\Models\Tcargo;
use App\Models\Tcategoriaprofesional;
use App\Models\Ttipocontrato;



class SincronizadorController extends Controller
{

	public function list(){
		// $colaborador_usuario = Erp_colaborador_usuario::where('dni','=','72182379')->get();
		$colaborador_usuario = Erp_colaborador_usuario::all();
		// dd($colaborador_usuario);

		foreach ($colaborador_usuario as $key => $value) {

			$existepersona = Tpersona::where('identificacion','=',$value->dni)->first();//verificando si existe en la tabla tpersona
			
			if ($existepersona) {//si existe update de datos
				$persona = $this->updatepersona($existepersona->cpersona,$value->dni,$value->apellido_paterno,$value->apellido_materno,$value->nombres,$value->alias);

				$existepersonanaturalinformacionbasica = Tpersonanaturalinformacionbasica::where('cpersona','=',$existepersona->cpersona)->first();//verificando si existe en la tabla tpersona

				if ($existepersonanaturalinformacionbasica) {
					$personabasica = $this->updatepersonanaturalinformacionbasica($existepersona->cpersona,$value->dni,$value->apellido_paterno,$value->apellido_materno,$value->nombres,$value->genero,$value->estado_civil);

					$existepersonaempleado = Tpersonadatosempleado::where('cpersona','=',$existepersona->cpersona)->first();

					if ($existepersonaempleado) {
						$personaempleado = $this->updatetpersonadatosempleado($existepersona->cpersona,$value->codigo_sig,$value->cargo,$value->fecha_ingreso,$value->categoria,$value->tipo_contrato);

						$existeusuario = Tusuario::where('cpersona','=',$existepersona->cpersona)->first();

						if ($existeusuario) {
							$usuario = $this->updateusuario($existepersona->cpersona,$value->usuario,$value->estado_usuario);

							$existeusuariopassword = Tusuariopassword::where('cusuario','=',$usuario->cusuario)->first();

							if ($existeusuariopassword) {
								$usuariopassword = $this->updateusuariopassword($usuario->cusuario,$value->fecha_ingreso,$value->password);
							} else {
								$usuariopassword = $this->createusuariopassword($usuario->cusuario,$value->fecha_ingreso,$value->password);
							}

						} else {
							$usuario = $this->createusuario($existepersona->cpersona,$value->usuario,$value->estado_usuario);
						}
					}
					else {
						$personaempleado = $this->createtpersonadatosempleado($existepersona->cpersona,$value->codigo_sig,$value->cargo,$value->fecha_ingreso,$value->categoria,$value->tipo_contrato);
					}
				}
				else {
					$personabasica = $this->createpersonanaturalinformacionbasica($existepersona->cpersona,$value->dni,$value->apellido_paterno,$value->apellido_materno,$value->nombres,$value->genero,$value->estado_civil);
				}

				// return $persona->cpersona;
			}
			else {//si no existe create de persona

				$persona = $this->createpersona($value->dni,$value->apellido_paterno,$value->apellido_materno,$value->nombres,$value->alias);
				$personabasica = $this->createpersonanaturalinformacionbasica($persona->cpersona,$value->dni,$value->apellido_paterno,$value->apellido_materno,$value->nombres,$value->genero,$value->estado_civil);
				$personaempleado = $this->createtpersonadatosempleado($personabasica->cpersona,$value->area,$value->cargo,$value->fecha_ingreso,$value->categoria,$value->tipo_contrato);
				$usuario = $this->createusuario($existepersona->cpersona,$value->usuario,$value->estado_usuario);
				$usuariopassword = $this->createusuariopassword($usuario->cusuario,$value->fecha_ingreso,$value->password);
				// return $persona->cpersona;
			}
		
		}
		return "OK";


	}

	private function createpersona($dni,$apellido_paterno,$apellido_materno,$nombres,$alias){

		$persona = new Tpersona;
		$persona->ctipopersona='NAT';
		$persona->ctipoidentificacion='2';
		$persona->identificacion=$dni;
		$persona->nombre=ucwords(strtolower($apellido_paterno." ".$apellido_materno." ".$nombres));
		$persona->abreviatura=ucwords(strtolower($alias));
		$persona->save();

		return $persona;

	}

	private function updatepersona($cpersona,$dni,$apellido_paterno,$apellido_materno,$nombres,$alias){

		$persona = Tpersona::where('cpersona','=',$cpersona)->first();
		$persona->ctipopersona='NAT';
		$persona->ctipoidentificacion='2';
		$persona->identificacion=$dni;
		$persona->nombre=ucwords(strtolower($apellido_paterno." ".$apellido_materno." ".$nombres));
		$persona->abreviatura=ucwords(strtolower($alias));
		$persona->save();

		return $persona;

	}

	private function createpersonanaturalinformacionbasica($cpersona,$dni,$apellido_paterno,$apellido_materno,$nombres,$genero,$estadocivil){

		$testadocivil = Tcatalogo::where('codtab','=','00015')->whereNotNull('digide')->where('descripcion','=',strtoupper($estadocivil))->get();
		// dd($testadocivil[0]->digide);

		$personabasica = new Tpersonanaturalinformacionbasica;
		$personabasica->cpersona = $cpersona;
		$personabasica->apaterno = ucwords(strtolower($apellido_paterno));
		$personabasica->amaterno = ucwords(strtolower($apellido_materno));
		$personabasica->nombres = ucwords(strtolower($nombres));
		$personabasica->esempleado = '1';
		$personabasica->ctipoidentificacion = '2';
		$personabasica->identificacion = $dni;
		if ($genero=='H') $genero_ok='M';
		if ($genero=='M') $genero_ok='F';
		$personabasica->genero = $genero_ok;
		$personabasica->estadocivil = $testadocivil[0]->digide;
		$personabasica->save();

		return $personabasica;

	}

	private function updatepersonanaturalinformacionbasica($cpersona,$dni,$apellido_paterno,$apellido_materno,$nombres,$genero,$estadocivil){

		$testadocivil = Tcatalogo::where('codtab','=','00015')->whereNotNull('digide')->where('descripcion','=',strtoupper($estadocivil))->get();
		// dd($testadocivil);

		$personabasica = Tpersonanaturalinformacionbasica::where('cpersona','=',$cpersona)->first();
		$personabasica->cpersona = $cpersona;
		$personabasica->apaterno = ucwords(strtolower($apellido_paterno));
		$personabasica->amaterno = ucwords(strtolower($apellido_materno));
		$personabasica->nombres = ucwords(strtolower($nombres));
		$personabasica->esempleado = '1';
		$personabasica->ctipoidentificacion = '2';
		$personabasica->identificacion = $dni;
		if ($genero=='H') $genero_ok='M';
		if ($genero=='M') $genero_ok='F';
		$personabasica->genero = $genero_ok;
		$personabasica->estadocivil = $testadocivil[0]->digide;
		$personabasica->save();

		return $personabasica;

	}

	private function createtpersonadatosempleado($cpersona,$area,$cargo,$fechaingreso,$categoria,$contrato){

		$tarea = Tarea::where('codigo','ilike','%'.$area.'%')->get();
		$tcargo = Tcargo::where('descripcion','ilike','%'.$cargo.'%')->get();
		if ($categoria == "") {
			$cat = "0";
		}
		else { 
			$tcategoria = Tcategoriaprofesional::where('descripcion','ilike','%'.$categoria.'%')->get();
			$cat = $tcategoria[0]->ccategoriaprofesional;
		}
		$tcontrato = Ttipocontrato::where('descripcion','ilike','%'.$contrato.'%')->get();

		$personaempleado = Tpersonadatosempleado::where('cpersona','=',$cpersona)->first();
		$personaempleado->cpersona = $cpersona;
		$personaempleado->carea = $tarea[0]->carea;
		$personaempleado->ccargo = $tcargo[0]->ccargo;
		$personaempleado->fingreso = $fechaingreso;
		$personaempleado->estado = "ACT";
		$personaempleado->ccategoriaprofesional = $cat;
		$personaempleado->ctipocontrato = $tcontrato[0]->ctipocontrato;
		$personaempleado->save();

		return $personaempleado;

	}

	private function updatetpersonadatosempleado($cpersona,$area,$cargo,$fechaingreso,$categoria,$contrato){

		$tarea = Tarea::where('codigo','ilike','%'.$area.'%')->get();
		$tcargo = Tcargo::where('descripcion','ilike','%'.$cargo.'%')->get();
		if ($categoria == "") {
			$cat = "0";
		}
		else { 
			$tcategoria = Tcategoriaprofesional::where('descripcion','ilike','%'.$categoria.'%')->get();
			$cat = $tcategoria[0]->ccategoriaprofesional;
		}
		$tcontrato = Ttipocontrato::where('descripcion','ilike','%'.$contrato.'%')->get();

		$personaempleado = Tpersonadatosempleado::where('cpersona','=',$cpersona)->first();
		$personaempleado->cpersona = $cpersona;
		$personaempleado->carea = $tarea[0]->carea;
		$personaempleado->ccargo = $tcargo[0]->ccargo;
		$personaempleado->fingreso = $fechaingreso;
		$personaempleado->estado = "ACT";
		$personaempleado->ccategoriaprofesional = $cat;
		$personaempleado->ctipocontrato = $tcontrato[0]->ctipocontrato;
		$personaempleado->save();

		return $personaempleado;

	}

	private function createusuario($cpersona,$usuario,$estado){

		$user = explode(".", $usuario);

		$usuario = new Tusuario();
		$usuario->nombre= strtolower($user[0].".".$user[1]);
		$usuario->estado= substr($estado,0,3);
		$usuario->cpersona= $cpersona;
		$usuario->save();

		return $usuario;
	}

	private function updateusuario($cpersona,$usuario,$estado){

		$user = explode(".", $usuario);

		$usuario = Tusuario::where('cpersona','=',$cpersona)->first();
		$usuario->nombre= strtolower($user[0].".".$user[1]);
		$usuario->estado= substr($estado,0,3);
		$usuario->cpersona= $cpersona;
		$usuario->save();

		return $usuario;
	}

	private function createusuariopassword($cusuario,$fechaingreso,$clave){

		$usuariopassword=new Tusuariopassword();
		$usuariopassword->cusuario= $cusuario;
		$usuariopassword->fhasta= $fechaingreso;
		$usuariopassword->fdesde= $fechaingreso;
		$usuariopassword->password= md5($clave);
		// $usuariopassword->caduca= $request->input('fechaingreso');
		$usuariopassword->save();
		
	}

	private function updateusuariopassword($cusuario,$fechaingreso,$clave){

		$existeusuariopassword = Tusuariopassword::where('cusuario','=',$cusuario)->first();
		$usuariopassword->cusuario= $cusuario;
		$usuariopassword->fhasta= $fechaingreso;
		$usuariopassword->fdesde= $fechaingreso;
		// $usuariopassword->password= md5($clave);
		// $usuariopassword->caduca= $request->input('fechaingreso');
		$usuariopassword->save();
		
	}

}


