<?php

namespace App\Http\Controllers\Migracion;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;

use Session;
use Carbon\Carbon;
use App\Models\Tconceptogasto;
use App\Models\Tactividad;
use App\Models\Tarea;
use App\Models\Tcargo;
use App\Models\Tentregable;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tunidadminera;
use App\Models\Tunidadmineracontacto;
use App\Models\Tproyecto;
use App\Models\Tproyectodisciplina;
use App\Models\Tproyectoejecucion;
use App\Models\Tproyectoejecucioncab;
use App\Models\Tgastosejecucion;
use App\Models\Tgastosejecuciondetalle;
use App\Models\Tproyectoinformacionadicional;
use App\Models\Tpersonadatosempleado;
use App\Models\Tproyectoactividade;
use App\Models\Tproyectoactividadeshabilitacion;
use App\Models\Tpersonanaturalinformacionbasica;
use App\Models\Tproyectopersona;
use App\Models\Tusuario;
use App\Models\Tmonedacambio;
use App\Models\Tmenupersona;
use App\Models\Tdocumentosparaproyecto; 
use App\Models\Tproyectohonorario; 
use App\Models\Tproyectoestructuraparticipante; 
use App\Models\Tpropuestum;

use App\Erp\chunkReadFilter;
use App\Erp\ExcelSupport;

use App\Models\XX_Migracion;



use DB;

class MigracionController extends Controller
{
    
    public function migracionGastos(){
    DB::beginTransaction();
    $inputFileType = 'Excel2007';
    $ruta = storage_path('archivos') . "/" . 'Lista_de_gastos1.xlsx';
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($ruta);
    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
    $datos=array();
    $datos_c = array();
    $i=0;
    foreach($sheetData as $row){
        $r['A']=$row['A'];
        $r['B']=$row['B'];
        $r['C']=$row['C'];
        $r['D']=$row['D'];
        $r['E']=$row['E'];
        $r['F']=$row['F'];
        $r['G']=$row['G'];
        $r['H']=$row['H'];
        $r['I']=$row['I'];
        $r['J']=$row['J'];
        $r['K']=$row['K'];       

        if($i>0){

          /* $concepto_gasto=DB::table('tconceptogastos as g')
                ->join('tcatalogo as c','g.tipogasto','=','c.digide')
                ->where('codtab','=','00026')             
                ->where('c.descripcion','ilike','%'.$r['D'].'%')
                ->where('g.descripcion','=',trim($r['B']))
           //     ->where('g.cconcepto_parent','=','')
                ->select('g.cconcepto','c.descripcion as nombre','g.descripcion as tipo','g.cconcepto_parent')
                ->first();
   
                  $cconcepto=0;

                  if($concepto_gasto){
                    $cconcepto=$concepto_gasto->cconcepto;
                  }

                 // dd($concepto_gasto,$r['B'],$r['D']); 

            $gasto=Tconceptogasto::where('cconcepto','=',$cconcepto)->first();*/

         //   if(is_null($gasto)||strlen($gasto)<=0){


            $g= new Tconceptogasto();
           
            $g->descripcion= $r['B'];
           
            $g->unidad='GLO';

            if($r['I']=='A'){
                $g->estado='00001';
            }
            if($r['I']=='I'){
                $g->estado='00002';
            }       


            if($r['K']=='D'){
            $g->cmoneda='USD';
            }
            else{
             $g->cmoneda==$r['K'];

            }

            if(is_null($r['H']) || strlen($r['H'])<=0){
                 $g->costounitario=0;
            }
            else{
                $g->costounitario=$r['H'];
            }

            if($r['D']!=null || strlen($r['D'])>0){

                $tipogasto=DB::table('tcatalogo')
                ->where('codtab','=','00026')
                ->where('descripcion','ilike','%'.$r['D'].'%')
                ->first();
                $idtipgast='';

                if($tipogasto){
                   $idtipgast=$tipogasto->digide;
                }   
                   

                $g->tipogasto=$idtipgast;

            }

            if($r['E']!=null || strlen($r['E'])>0){

                $categoria=DB::table('tcatalogo')
                ->where('codtab','=','00025')
                ->where('descripcion','ilike','%'.$r['E'].'%')
                ->first();
                $idcategoria='';

                if($categoria){
                   $idcategoria=$categoria->digide;
                }   
                   

                $g->categoria_gasto=$idcategoria;

            }

            $g->save();

            $r['id']=$g->cconcepto;

            $datos[count($datos)]=$r;
            $datos_c[count($datos_c)]=$r;
                   
           // }  
               
        }

        $i++;        
       
    }

    foreach($datos as $k => $d){

        $tipogast=DB::table('tcatalogo')
        ->where('codtab','=','00026')
        ->where('descripcion','ilike','%'.$d['D'].'%')
        ->first();    

        $idtipgas='';

        if($tipogast){
            $idtipgas=$tipogast->digide;                   
        } 

        $concgasto=Tconceptogasto::where('cconcepto','=',$d['id'])->first();

        $tipgasto='';

        if($concgasto){
          $tipgasto=$concgasto->tipogasto;
        }


        if(!(is_null($d['C']) || strlen($d['C'])<=0)&&$d['D']=='construccion'){

            $res2=array_map(function($e){if($e['D']=='construccion'){return trim($e['A']);}},$datos_c);     
            //dd($datos_c,$res2);  
            
            $res = array_search(trim($d['C']), array_map(function($e){return trim($e);},$res2));                 


            if(!($res===FALSE)){
                //dd($res);
                $d['cconcepto_parent']= $datos_c[$res]['id'];
                $datos[$k]=$d;
                $gas = Tconceptogasto::where('cconcepto','=',$d['id'])->first();

                $gas->cconcepto_parent=$d['cconcepto_parent'];

                $gas->save();
            }

        }

        if(!(is_null($d['C']) || strlen($d['C'])<=0)&&$d['D']=='laboratorio'){

            $res2=array_map(function($e){if($e['D']=='laboratorio'){return trim($e['A']);}},$datos_c);     
            //dd($datos_c,$res2);  
            
            $res = array_search(trim($d['C']), array_map(function($e){return trim($e);},$res2));
        
            if(!($res===FALSE)){
                //dd($res);
                $d['cconcepto_parent']= $datos_c[$res]['id'];
                $datos[$k]=$d;
                $gas = Tconceptogasto::where('cconcepto','=',$d['id'])->first();

                $gas->cconcepto_parent=$d['cconcepto_parent'];

                $gas->save();
            }

        }

        if(!(is_null($d['C']) || strlen($d['C'])<=0)&&$d['D']=='operaciones'){

            $res2=array_map(function($e){if($e['D']=='operaciones'){return trim($e['A']);}},$datos_c);     
            //dd($datos_c,$res2);  
            
            $res = array_search(trim($d['C']), array_map(function($e){return trim($e);},$res2));
                      


            if(!($res===FALSE)){
                //dd($res);
                $d['cconcepto_parent']= $datos_c[$res]['id'];
                $datos[$k]=$d;
                $gas = Tconceptogasto::where('cconcepto','=',$d['id'])->first();

                $gas->cconcepto_parent=$d['cconcepto_parent'];

                $gas->save();
            }

        }
    }
    
    //dd($datos);

     DB::commit();

        echo '<script language="javascript">alert("Migración realizada con éxito :D");</script>'; 
    }
         
  
    public function migracionActividades(){
    DB::beginTransaction();
    $inputFileType = 'Excel2007';
    $ruta = storage_path('archivos') . "/" . 'Lista_de_Actividades.xlsx';
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($ruta);
    $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
    $datos=array();
    $datos_c = array();
    $i=0;
    foreach($sheetData as $row){
        $r['A']=$row['A'];
        $r['B']=$row['B'];
        $r['C']=$row['C'];   
            
        if($i>7){

            $actividad=DB::table('tactividad')
            ->where('descripcion','=',trim($r['B']))
            ->where('codigo','=',trim($r['A']))  
            ->first(); 

            $cactividad=0; 

           if($actividad){

            $cactividad=$actividad->cactividad;

           }

            $acti=Tactividad::where('cactividad','=',$cactividad)->first();

            if(is_null($acti) || strlen($acti)<=0){
               
                 $a= new Tactividad();

                if(!(is_null($r['A']) || strlen($r['A'])<=0)){
                    $a->codigo= $r['A'];
                    $a->descripcion= $r['B'];  
                    $a->save();                  
                }                                  
               
                $r['id']=$a->cactividad;               
            }    
            else{
                 
                $r['id']=null;               
                 
            } 

            $datos[count($datos)]=$r;
            $datos_c[count($datos_c)]=$r;      

        }

        $i++;        
       
    }

  foreach($datos as $k => $d){

        if(!(is_null($d['C']) || strlen($d['C'])<=0)){

            $res = array_search(trim($d['C']), array_map(function($e){return trim($e['A']);},$datos_c));           

            if(!($res===FALSE)){
                if(!(is_null($d['id']))){

                    $padre=DB::table('tactividad')
                    ->where('descripcion','=',$datos_c[$res]['B'])
                    ->where('codigo','=',$datos_c[$res]['A'])
                    ->first();

                    $cpadre='';

                    if($padre){
                        $cpadre=$padre->cactividad;
                    }
                    //$d['cactividad_parent']= $datos_c[$res]['id'];

                    $d['cactividad_parent']=$cpadre;                    
                    $datos[$k]=$d;
                    $act = Tactividad::where('cactividad','=',$d['id'])->first();
                    $act->cactividad_parent=$d['cactividad_parent'];
                    $act->save();                   
           
                }

            }
               

        }
    }    
  

    DB::commit();
    }


    public function migracionAreas(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Area.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();
        $datos_c = array();
        $i=0;
        foreach($sheetData as $row){
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['F']=$row['F'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];    

            if($i>0){

                $area=DB::table('tareas')
                ->where('descripcion','=',trim($r['C']))
                ->where('codigo','=',trim($r['D']))  
                ->first(); 

               // dd($area);

                if(is_null($area)){

                    $a= new Tarea();
            
                    $a->descripcion= $r['C'];
                    $a->codigo= $r['D'];
                    $a->esgeneral= $r['F'];
                    $a->estecnica= $r['G'];
                    $a->esarea= $r['H']; 
                    $a->save();
                   
                    $padre=DB::table('tareas')                   
                    ->where('codigo','=',trim($r['E']))  
                    ->first();

                    $cpadre='';

                    if($padre){

                        $cpadre=$padre->carea;
                        $ar=Tarea::where('carea','=',$a->carea)->first();
                        $ar->carea_parent=$cpadre;
                        $ar->save();                       
                    }                       

                    $r['id']=$a->carea;
                   
                    $datos[count($datos)]=$r;
                    $datos_c[count($datos_c)]=$r;  

                }                        
                
            }

                $i++;        
       
            }

           /* foreach($datos as $k => $d){
                if(!(is_null($d['E']) || strlen($d['E'])<=0)){
                    
                    $res = array_search(trim($d['E']), array_map(function($e){return trim($e['D']);},$datos_c));

                    if(!($res===FALSE)){
                        
                        $d['carea_parent']= $datos_c[$res]['id'];
                        $datos[$k]=$d;
                        $gas = Tarea::where('carea','=',$d['id'])->first();
                        dd($res,$gas,$d);
                        $gas->carea_parent=$d['carea_parent'];

                        $gas->save();
                    }

                }
            }*/
    

            DB::commit();
    }


    public function migracionCargo(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Cargo.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();
      
        $i=0;
        foreach($sheetData as $row){
            $r['A']=$row['A'];
            $datos[count($datos)]=$r;
                        
            if($i>0){

                $cargo=DB::table('tcargos')
                ->where('descripcion','=',trim($r['A']))               
                ->first();

                if(is_null($cargo)){

                    $ultimo_cargo=DB::table('tcargos')    
                    ->orderby('ccargo','DESC')         
                    ->first();

                    $codigo=0;


                    if($ultimo_cargo){

                        $codigo=$ultimo_cargo->codigo;
                    }

                        $a= new Tcargo();                        

                        $a->codigo= '000'.($codigo+1);

                        $a->descripcion= trim($r['A']); 

                        $a->save();
                     
                }                          
            
            }

                $i++;        
        
        }

        DB::commit();
    }
         
    public function migracionMasterEntregables(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Master_Entregables.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();

        $i=0;
        foreach($sheetData as $row){
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['F']=$row['F'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
                
            $datos[count($datos)]=$r;
            if($i>0){

                //if(strlen($r['G'])<=0)
                if(strlen($r['G'])>0)

                {

                    $tipo=DB::table('ttiposentregables')
                    ->where('descripcion','ilike','%'.$r['C'].'%')
                    ->first();
                

                    $ctip=null;

                    if($tipo){
                        $ctip=$tipo->ctiposentregable;               
                    }

                   $entregable=DB::table('tentregables')
                    ->where('descripcion','=',trim($r['B']))
                    ->where('descripcion','=',$ctip)      
                    ->first();

                    //dd($entregable);
                    $e= new Tentregable();

                    if(!(is_null($r['B'])) || strlen($r['B'])>0){
                   
                        $e->descripcion= $r['B'];
                       
                        $tipoentregable=DB::table('ttiposentregables')
                        ->where('descripcion','ilike','%'.$r['C'].'%')            
                        ->first();

                        $ctipent=null;

                        if($tipoentregable){
                            $ctipent=$tipoentregable->ctiposentregable;
                            $e->ctipoentregable=$ctipent;
                           
                        }

                        $disciplina=DB::table('tdisciplina')
                        ->where('descripcion','ilike','%'.$r['D'].'%')               
                        ->first();

                        $cdisc=null;

                        if($disciplina){
                            $cdisc=$disciplina->cdisciplina;
                            $e->cdisciplina=$cdisc;
                        }

                        $e->nivel= $r['E'];
                        $e->codigo= $r['F'];

                        $entregableParent=DB::table('tentregables')
                        ->where('codigo','=',$r['G'])
                        ->first();
                    
                        $centregableparent=null;

                        if($entregableParent){
                            $centregableparent=$entregableParent->centregables;
                        }

                        $e->entregable_parent=$centregableparent;

                        if (strlen($r['H'])>0 || $r['H']!='') {
                           
                            $tipodoc=Tdocumentosparaproyecto::where('codigo','=',trim($r['H']))
                            ->first();

                            if (!$tipodoc) {
                                $tipodoc=new Tdocumentosparaproyecto();
                                $tipodoc->codigo=trim($r['H']);
                                $tipodoc->save();
                            }
                           
                            /*$tipodoc=DB::table('tdocumentosparaproyecto')
                            ->where('codigo','=',trim($r['H']))
                            ->first();*/

                            //$cdocumento=null;
                            //$cdocumento=$tipodoc->cdocumentoparapry;

                            $e->cdocumentoparapry=$tipodoc->cdocumentoparapry;
                        }
                       
                        $e->tipo_paquete=trim($r['I']);
                        $e->save();
                    }
                }      
            }

            $i++;
        }

    //dd($datos);

     DB::commit();
    }
      
    public function migracionProveedores(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Proveedores.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        //retorne nulos si no existe
        //fomulas
        //formato
        //
        $datos=array();

        $i=0;
    foreach($sheetData as $row){
        $r['B']=$row['B'];
        $r['C']=$row['C'];
        $r['D']=$row['D'];
        $r['H']=$row['H'];
        $r['I']=$row['I'];
        $r['J']=$row['J'];
        $r['K']=$row['K'];
             
        $datos[count($datos)]=$r;
        if($i>5){

            $p= new Tpersona();
            $pj=new Tpersonajuridicainformacionbasica;
            $pd=new Tpersonadireccione;

            if(!(is_null($r['D']) || strlen($r['D'])<=0)){
           
                $p->nombre= $r['D'];
                $p->ctipopersona='JUR';
                $p->ctipoidentificacion='1';
                $p->identificacion=$r['C'];          

                $p->save(); 

                $pj->cpersona=$p->cpersona;
                $pj->razonsocial=$r['D'];
                $pj->nombrecomercial=$r['D'];
                $pj->esproveedor='1';


                $pj->save();
                /* Inicio Ubigeo */
                 $departamento=DB::table('tubigeo')
                        ->where('descripcion','ilike','%'.$r['H'].'%')
                        ->where('cubigeo','like','__0000')
                        ->first(); 
                  

                $provincia=DB::table('tubigeo')
                        ->where('descripcion','ilike','%'.$r['I'].'%')  
                        ->where('cubigeo','like','____00')       
                        ->where('cubigeo','not like','__00__')          
                        ->first(); 

                        
               
                $distrito=DB::table('tubigeo')
                        ->where('descripcion','ilike','%'.$r['J'].'%')
                        ->where('cubigeo','not like','____00')
                        ->first();
                
                $coddep=null;
                $codprov=null;
                $coddist=null;

                 if($departamento){
                    $coddep=substr($departamento->cubigeo, 0,2);              
                 }

                 if($provincia){
                    $codprov=substr($provincia->cubigeo, 2,2);              
                 }                 

                 if($distrito){
                    $coddist=substr($distrito->cubigeo, 4,2);              
                 }

                 $ubigeo=$coddep.$codprov.$coddist;
                 /* Fin Ubigeo */
                 
                $pd->cpersona=$p->cpersona;
                 if(!(is_null($r['K']) || strlen($r['K'])<=0)){
                    $pd->ctipodireccion='DO';
                    $pd->numerodireccion='1';
                    $pd->direccion=$r['K'];

                    if(trim(mb_strtoupper($r['B'])=='PERÚ')){
                        $pd->cpais='051';
                     }

                    elseif(trim(mb_strtoupper($r['B'])=='CHILE')){
                        $pd->cpais='056';
                     }

                     elseif(trim(mb_strtoupper($r['B'])=='ARGENTINA')){
                        $pd->cpais='054';
                     }

                     else{
                         $pd->cpais='055';
                     }


                    if($pd->cpais=='051'&&!(is_null($r['H']) || strlen($r['H'])<=0)&&
                        !(is_null($r['I']) || strlen($r['I'])<=0) && !(is_null($r['J']) || strlen($r['J'])<=0)){

                        $pd->cubigeo=$ubigeo;
                    }    
                    else {
                        $pd->cubigeo=null;
                    }
                  
                     $pd->save();   
                    
                }

                       
            
          }
                           
        }

        $i++;
                      
    }
      
     DB::commit();
    }   

     public function migracionClientes(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Clientes.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        
        $datos=array();

        $i=0;
    foreach($sheetData as $row){
        $r['A']=$row['A'];
        $r['B']=$row['B'];
        $r['C']=$row['C'];
        $r['D']=$row['D'];
        $r['F']=$row['F'];
        $r['G']=$row['G'];
        $r['H']=$row['H'];
        $r['J']=$row['J'];
        $r['K']=$row['K'];
        $r['L']=$row['L'];
        $r['M']=$row['M'];
        $r['N']=$row['N'];
        $r['O']=$row['O'];
        $r['P']=$row['P'];
        $r['Q']=$row['Q'];
      
        $datos[count($datos)]=$r;
        if($i>5){

            $p= new Tpersona();
            $pj=new Tpersonajuridicainformacionbasica;
            $pd1=new Tpersonadireccione;
            $pd2=new Tpersonadireccione;     
            $pd3=new Tpersonadireccione;          

            if(!(is_null($r['G']) || strlen($r['G'])<=0) && $r['B']=='Cliente'){
            //Inicio Migracion Tpersona
                $p->nombre= $r['F'];
                $p->ctipopersona='JUR';
                $p->ctipoidentificacion='1';
                $p->identificacion=$r['D'];  
                
                $p->save(); 
            //Fin Migracion Tpersona

            //Inicio Migracion Tpersonajuridicainformacionbasica
                $pj->cpersona=$p->cpersona;
                $pj->razonsocial=$r['G'];
                $pj->nombrecomercial=$r['F'];
                $pj->escliente='1';                       
               
                $pj->save();         
            //Fin Migracion Tpersonajuridicainformacionbasica  

            //Inicio Migracion Tpersonadireccione  

                    /* Inicio Ubigeo */
                     $departamento=DB::table('tubigeo')
                            ->where('descripcion','ilike','%'.$r['J'].'%')
                            ->where('cubigeo','like','__0000')
                            ->first(); 
                      

                    $provincia=DB::table('tubigeo')
                            ->where('descripcion','ilike','%'.$r['K'].'%')  
                            ->where('cubigeo','like','____00')       
                            ->where('cubigeo','not like','__00__')          
                            ->first(); 

                            
                   
                    $distrito=DB::table('tubigeo')
                            ->where('descripcion','ilike','%'.$r['L'].'%')
                            ->where('cubigeo','not like','____00')
                            ->first();
                    
                    $coddep=null;
                    $codprov=null;
                    $coddist=null;
                     if($departamento){
                        $coddep=substr($departamento->cubigeo, 0,2);              
                     }

                     if($provincia){
                        $codprov=substr($provincia->cubigeo, 2,2);              
                     }                 

                     if($distrito){
                        $coddist=substr($distrito->cubigeo, 4,2);              
                     }

                     $ubigeo=$coddep.$codprov.$coddist;
                     /* Fin Ubigeo */

                $num=0;
                 if(!(is_null($r['O']) || strlen($r['O'])<=0)){
                    $pd1->cpersona=$p->cpersona;
                    $pd1->ctipodireccion='COM';
                    $pd1->direccion=$r['O'];


                     if(trim(mb_strtoupper($r['C']))=='PERÚ'){
                        $pd1->cpais='051';
                     }

                     elseif(trim(mb_strtoupper($r['C']))=='CHILE'){
                        $pd1->cpais='056';
                     }

                     elseif(trim(mb_strtoupper($r['C'])=='ARGENTINA')){
                        $pd1->cpais='054';
                     }

                     else{
                         $pd1->cpais='055';
                     }

                    if($pd1->cpais=='051'&&!(is_null($r['J']) || strlen($r['J'])<=0)&&
                        !(is_null($r['K']) || strlen($r['K'])<=0) && !(is_null($r['L']) || strlen($r['L'])<=0)){
                    $pd1->cubigeo=$ubigeo;
                    }    
                    else {
                        $pd1->cubigeo=null;
                    }


                     $num++;
                     $pd1->numerodireccion=$num;
                     $pd1->save();   
                 }

                  if(!(is_null($r['N']) || strlen($r['N'])<=0)){
                    $pd2->cpersona=$p->cpersona;
                    $pd2->ctipodireccion='DO';
                    $pd2->direccion=$r['N'];
                     
                 
                     if(trim(mb_strtoupper($r['C']))=='PERÚ'){
                        $pd2->cpais='051';
                     }

                     elseif(trim(mb_strtoupper($r['C']))=='CHILE'){
                        $pd2->cpais='056';
                     }

                     elseif(trim(mb_strtoupper($r['C'])=='ARGENTINA')){
                        $pd2->cpais='054';
                     }

                     else{
                         $pd2->cpais='055';
                     }  

                    if($pd2->cpais=='051'&&!(is_null($r['J']) || strlen($r['J'])<=0)&&
                        !(is_null($r['K']) || strlen($r['K'])<=0) && !(is_null($r['L']) || strlen($r['L'])<=0)){
                        $pd2->cubigeo=$ubigeo;
                    }    
                    else {
                        $pd2->cubigeo=null;
                    }  

                     $num++;
                     $pd2->numerodireccion=$num;
                     $pd2->save();              
                  }

                  if(!(is_null($r['M']) || strlen($r['M'])<=0) && strlen($r['M'])>2){
                    $pd3->cpersona=$p->cpersona;
                    $pd3->ctipodireccion='CAM';
                    $pd3->direccion=$r['M'];
                      
                 
                     if(trim(mb_strtoupper($r['C']))=='PERÚ'){
                        $pd3->cpais='051';
                     }

                     elseif(trim(mb_strtoupper($r['C']))=='CHILE'){
                        $pd3->cpais='056';
                     }


                     elseif(trim(mb_strtoupper($r['C'])=='ARGENTINA')){
                        $pd3->cpais='054';
                     }

                     else{
                         $pd3->cpais='055';
                     }    

                     if($pd3->cpais=='051'&&!(is_null($r['J']) || strlen($r['J'])<=0)&&
                        !(is_null($r['K']) || strlen($r['K'])<=0) && !(is_null($r['L']) || strlen($r['L'])<=0)){
                        $pd3->cubigeo=$ubigeo;
                    }    
                    else {
                        $pd3->cubigeo=null;
                    }                  
                     $num++;
                     $pd3->numerodireccion=$num;
                     $pd3->save();              
                  }


                //Fin Migracion Tpersonadireccione  

                //Inicio Migracion Tunidadminera  

                if(!(is_null($r['H']) || strlen($r['H'])<=0)){

                     $uminera=DB::table('tunidadminera')
                    ->where('nombre','ilike','%'.$r['H'].'%')
                    ->select('nombre')
                    ->first(); 

                    $um='';
                    if($uminera){
                          $um= mb_strtoupper($uminera->nombre);                          
                          
                    }
                                     
                        if(!(trim(mb_strtoupper($r['H']))==$um)){                         
                            $u=new Tunidadminera;
                            $u->nombre=trim(ucwords(mb_strtolower($r['H'])));
                            $u->cpersona=$p->cpersona;
                            $u->cumineraestado='001';
                             if(trim(mb_strtoupper($r['C']))=='PERÚ'){
                                    $u->cpais='051';
                                 }

                            elseif(trim(mb_strtoupper($r['C']))=='CHILE'){
                                    $u->cpais='056';
                                 }


                             elseif(trim(mb_strtoupper($r['C'])=='ARGENTINA')){
                                $u->cpais='054';
                             }

                            else{
                                     $u->cpais='055';
                                 }  
                           
                            $u->save();
                        }              
                   
                }
                //Fin Migracion Tunidadminera 
            }              
                                    
        }

        $i++;
                      
    }
 
     DB::commit();
    }   

    public function migracionContactosUminera(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Contactos.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        $datos=array();
      
        $i=0;
        foreach($sheetData as $row){
           
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['F']=$row['F'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
            $r['J']=$row['J'];
            $r['K']=$row['K'];
            $r['L']=$row['L'];
            $r['M']=$row['M'];
            $r['N']=$row['N'];
            $r['O']=$row['O'];
            $datos[count($datos)]=$r;
                        
            if($i>5){

                $c= new Tunidadmineracontacto;

                if(!(is_null($r['H']) || strlen($r['H'])<=0)){
               
                    $c->apaterno= $r['I'];
                    $c->amaterno= $r['J'];
                    $c->nombres= $r['H'];
                    $c->email= $r['K'];
                    $c->telefono= $r['L'];
                    $c->anexo= $r['M'];
                    $c->cel1= $r['N'];
                    $c->cel2= $r['O'];

                    /* Inicio Unidad minera */

                    $uminera=DB::table('tunidadminera')
                    ->where('nombre','ilike',$r['E'].'%')
                    ->first(); 

                    $um='';
                    $codum='';
                    if($uminera){                         
                          $um= mb_strtoupper($uminera->nombre);
                          $codum= $uminera->cunidadminera;   
                    }
                    
                   
                    if(trim(mb_strtoupper($r['E']))==$um){
                        $c->cunidadminera=$codum;
                    }
                    /* Fin minera */


                    /* Inicio Cargo */

                    $ccargo=DB::table('tcontactocargo')
                    ->where('descripcion','ilike',$r['G'].'%')
                    ->first(); 

                    $ca='';
                    $codca='';
                    if($ccargo){                         
                          $ca= mb_strtoupper($ccargo->descripcion);
                          $codca= $ccargo->ccontactocargo;  
  
                    }

                    if(trim(mb_strtoupper($r['G']))==$ca){

                        $c->ccontactocargo=$codca; 

                    }
                    else{
                        $c->ccontactocargo=null; 

                    }                   
                                               
                    $c->save(); 
                
                                       
                }              
                                    
            }

                $i++;        
        
        }

        DB::commit();
    }


     /************************************************************************************************************************************************************/

    public function migracionClientesOtro(){
            DB::beginTransaction();
            $inputFileType = 'Excel2007';
            $ruta = storage_path('archivos') . "/" . 'ClientesOtro.xlsx';
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($ruta);
            $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);            
            $datos=array();

            $i=0;
        foreach($sheetData as $row){
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
            $r['J']=$row['J'];
            $r['K']=$row['K'];
            $r['P']=$row['P'];
            //$r['Q']=$row['Q'];
            $r['R']=$row['R'];
            $r['T']=$row['T'];
            $r['U']=$row['U'];
            $r['V']=$row['V'];
            $r['X']=$row['X'];
            $r['Y']=$row['Y'];
            $r['Z']=$row['Z'];
            $r['AA']=$row['AA'];
            $r['AB']=$row['AB'];           

          
            $datos[count($datos)]=$r;
            if($i>0){

                $p= new Tpersona;
                $pj=new Tpersonajuridicainformacionbasica;
                $pd=new Tpersonadireccione;
               

                if(!(is_null($r['A']) || strlen($r['A'])<=0)){
                //Inicio Migracion Persona

                    $persona=DB::table('tpersona')
                    ->where(DB::raw("REPLACE(nombre,'.','')"),'ilike','%'.$r['B'].'%')
                    ->first();

                    if($persona){
                        $cperson=$persona->cpersona;

                    }
                    if(!($persona!=null || strlen($persona)>0)){
                        $p->nombre= $r['B'];
                        $p->ctipopersona='JUR';
                        $p->ctipoidentificacion='1';
                        $p->identificacion=$r['A'];  
                        $p->abreviatura=substr($r['P'],0,10);
                        $p->save(); 

                        //Migracion Tpersonajuridicainformacionbasica
                        $pj->cpersona=$p->cpersona;
                        $pj->razonsocial=$r['B'];
                        $pj->nombrecomercial=$r['B'];
                        $pj->escliente='1'; 
                        $pj->save();

                        //Inicio Migracion Tpersonadirecciones

                        //Inicio Ubigeo

                        $ubigeo=null;

                        if(!(is_null($r['I'])||strlen($r['I'])<=0 || $r['I']=='null') && !(is_null($r['J']) || strlen($r['J'])<=0 || $r['J']=='null') && !(is_null($r['K']) || strlen($r['K'])<=0 || $r['K']=='null')){

                            $departamento=DB::table('tubigeo')
                                    ->where('descripcion','ilike',$r['I'].'%')
                                    ->where('cubigeo','like','__0000')
                                    ->first(); 
                            $coddep=null; 

                             if($departamento){
                                $coddep=substr($departamento->cubigeo, 0,2);              
                             }



                            $provincia=DB::table('tubigeo')
                                    ->where('descripcion','ilike',$r['J'].'%')  
                                    ->where('cubigeo','like','____00')       
                                    ->where('cubigeo','not like','__00__') 
                                    ->where('cubigeo','like',$coddep.'%')               
                                    ->first(); 
                            $codprov=null;

                            if($provincia){
                                $codprov=substr($provincia->cubigeo, 2,2);              
                             }   
                                    
                           
                            $distrito=DB::table('tubigeo')
                                    ->where('descripcion','ilike',$r['K'].'%')
                                    ->where('cubigeo','not like','____00')
                                    ->where('cubigeo','like',$coddep.$codprov.'%')
                                     ->first();             
                            $coddist=null;     

                             if($distrito){
                                $coddist=substr($distrito->cubigeo, 4,2);              
                             }

                             $cubigeo=$coddep.$codprov.$coddist;   


                             $buscarubigeo=DB::table('tubigeo')           
                             ->where('cubigeo','=',$cubigeo)
                             ->first();   

                             
                             if($buscarubigeo){
                                $ubigeo=$buscarubigeo->cubigeo;
                             } 

                              
                        }       
                             //Fin Ubigeo 
                       
                        $num=0;

                        if(!(is_null($r['H']) || strlen($r['H'])<=0 || $r['H']=='null' ) ){
                            $pd->cpersona=$p->cpersona;
                            $pd->ctipodireccion='COM';

                            if(!(is_null($r['G']) || strlen($r['G'])<=0 || $r['G']=='null' ) ){

                            $pd->direccion=$r['G'];
                            }

                            $pais=DB::table('tpais')
                            ->where('descripcion','ilike','%'.$r['H'].'%')
                            ->first();

                            $cpais=null;

                            if($pais){
                                $cpais=$pais->cpais;

                            }                         

                            $pd->cpais=$cpais;


                            if($pd->cpais=='PER'&&!(is_null($r['J']) || strlen($r['J'])<=0)&&
                                !(is_null($r['K']) || strlen($r['K'])<=0) && !(is_null($r['I']) || strlen($r['I'])<=0)){
                           $pd->cubigeo=$ubigeo;
                            }    
                            else {
                                $pd->cubigeo=null;
                            }

                             $num++;
                             $pd->numerodireccion=$num;                    
                             $pd->save();   
                            
                         }

                        //Fin Migracion Tpersonadirecciones

                    }          
                //Fin Migracion Persona

                //Inicio Migracion Tunidadminera  

                    if(!(is_null($r['V']) || strlen($r['V'])<=0)){

                         $uminera=DB::table('tunidadminera')
                        ->where(DB::raw("REPLACE(nombre,'.','')"),'ilike','%'.$r['V'].'%')
                        ->first(); 
                      
                        $um='';
                        if($uminera){
                              $um= $uminera->nombre;                          
                              
                        }                                         
                            if(!(trim(mb_strtoupper($r['V']))==$um)){                         
                                $u=new Tunidadminera;

                                if($persona){

                                     $u->cpersona=$cperson;
                                }

                                else{
                                    $u->cpersona=$p->cpersona;
                                }                                      
                                $u->nombre=trim(ucwords(mb_strtolower($r['V'])));
                                $u->cumineraestado='001';
                                $u->codigo=$r['T'];

                                if(!($r['X']==null || strlen($r['X'])<=0 || $r['X']=='null' || $r['X']=='undefined')){
                                    $u->direccion=$r['X'];

                                }

                                $cpaisum=null; 
                       
                                if(!($r['Y']==null || strlen($r['Y'])<=0 || $r['Y']=='null')){

                                $paisum=DB::table('tpais')
                                ->where('descripcion','ilike','%'.$r['Y'].'%')
                                ->first();                                                                                       

                                    if($paisum){


                                       $u->cpais=$paisum->cpais;
                                    }  

                                    //Inicio Ubigeo Unidad Minera                             
                                    $ubigeoum=null;
                                    if(!(is_null($r['Z'])||strlen($r['Z'])<=0 || $r['Z']=='null') && !(is_null($r['AA']) || strlen($r['AA'])<=0 || $r['AA']=='null') && !(is_null($r['AB']) || strlen($r['AB'])<=0 || $r['AB']=='null')){

                                         

                                        $departamentoum=DB::table('tubigeo')
                                                ->where('descripcion','ilike',$r['Z'].'%')
                                                ->where('cubigeo','like','__0000')
                                                ->first(); 
                                        $coddepum=null; 

                                         if($departamentoum){
                                            $coddepum=substr($departamentoum->cubigeo, 0,2);        
                                         }

                                        $provinciaum=DB::table('tubigeo')
                                                ->where('descripcion','ilike',$r['AA'].'%')  
                                                ->where('cubigeo','like','____00')       
                                                ->where('cubigeo','not like','__00__') 
                                                ->where('cubigeo','like',$coddepum.'%')               
                                                ->first(); 
                                        $codprovum=null;

                                        if($provinciaum){
                                            $codprovum=substr($provinciaum->cubigeo, 2,2);          
                                         }                                                   
                                       
                                        $distritoum=DB::table('tubigeo')
                                                ->where('descripcion','ilike',$r['AB'].'%')
                                                ->where('cubigeo','not like','____00')
                                                ->where('cubigeo','like',$coddepum.$codprovum.'%')
                                                 ->first();             
                                        $coddistum=null;     

                                         if($distritoum){
                                            $coddistum=substr($distritoum->cubigeo, 4,2);           
                                         }

                                         $cubigeoum=$coddepum.$codprovum.$coddistum;   

                                         $buscarubigeoum=DB::table('tubigeo')           
                                         ->where('cubigeo','=',$cubigeoum)
                                         ->first();   
                                         
                                         if($buscarubigeoum){
                                            $ubigeoum=$buscarubigeoum->cubigeo;
                                         } 

                                         if($u->cpais=='PER'){
                                               $u->cubigeo=$ubigeoum;
                                                }                                                                                            
                                    }       
                                    //Fin Ubigeo Unidad Minera    

                                }                               

                                $u->save();
                            }    
                                     
                       
                    }
                    //Fin Migracion Tunidadminera 
              
                }       

                                        
            }

            $i++;

        }
     
         DB::commit();
        }   

    /************************************************************************************************************************************************************/

    public function migracionProyectos(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Proyectos.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        $datos=array();
        $i=0;


        foreach($sheetData as $row){
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
            $r['J']=$row['J'];
            $r['K']=$row['K'];
            $r['L']=$row['L'];
            $r['M']=$row['M'];
            $r['N']=$row['N'];
            $r['O']=$row['O'];
            $r['P']=$row['P'];
            $r['R']=$row['R'];
            $r['U']=$row['U'];
            $r['V']=$row['V'];
                     
            $datos[count($datos)]=$r;        


            if($i>0){

                $p= new Tproyecto;
                $pd1=new Tproyectodisciplina;   
                $pd2=new Tproyectodisciplina;  
                $pd3=new Tproyectodisciplina;  
                $pd4=new Tproyectodisciplina;  
                
                if(!(is_null($r['B']) || strlen($r['B'])<=0)){

                    //Inicio Migracion Tunidadminera  


                     $cliente=DB::table('tpersona')
                    ->where(DB::raw("REPLACE(nombre,'.','')"),'ilike','%'.$r['L'].'%')
                    ->first();

                    $cli=null;

                    if($cliente){
                     $cli=$cliente->cpersona;
                    }

                    if(!(is_null($r['M']) || strlen($r['M'])<=0)){

                        $uminera=DB::table('tunidadminera')
                        ->where(DB::raw("REPLACE(nombre,'.','')"),'ilike','%'.$r['M'].'%')
                        ->where('cpersona','=',$cli)
                        ->first();                    
                   
                   //dd($uminera,$r['L']);

                        $um='';
                        if($uminera){
                              $um= $uminera->nombre;                          
                              
                        }                             
                            if(!(trim(mb_strtoupper($r['M']))==mb_strtoupper($um))){                         
                                $u=new Tunidadminera;
                                $u->nombre=trim($r['M']);

                                $persona=DB::table('tpersona')
                                ->where(DB::raw("REPLACE(nombre,'.','')"),'ilike','%'.trim($r['L']).'%')
                                ->first();

                                $cperson='';

                                if($persona){
                                    $cperson=$persona->cpersona;
                                }

                                $u->cpersona=$cperson; 
                                $u->cumineraestado='001'; 

                                
                                $u->save();
                            }              
                        
                    }
                    //Fin Migracion Tunidadminera 

                    //Inicio Migracion Proyecto
                    $proyect=DB::table('tproyecto')
                    ->where('codigo','=',trim($r['B']))
                    ->first(); 
                    $codproy='';
                    $cproyecto='';
                    if($proyect){
                        $codproy=$proyect->codigo;
                         $cproyecto=$proyect->cproyecto;
                    }

                    if(!(trim($r['B'])==$codproy)){

                        $p->codigo= $r['B'];

                        $umin=DB::table('tunidadminera')
                            ->where(DB::raw("REPLACE(nombre,'.','')"),'ilike','%'.trim($r['M']).'%')
                            ->first();                    

                            $unm='';
                            if($umin){
                                  $unm= $umin->cunidadminera;    

                            }  

                            

                        $p->cunidadminera=$unm;



                        $pergerente=explode('.',$r['D']);
                        

                
                        if($r['D']){

                            $gerente=DB::table('tpersonanaturalinformacionbasica')
                            ->where(DB::raw("translate(nombres,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'ilike','%'.$pergerente[0].'%')
                            ->where(DB::raw("translate(apaterno,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'ilike','%'.$pergerente[1].'%')
                            ->first();

                            $cgerente='';
                            if($gerente){
                                $cgerente=$gerente->cpersona;
                            }

                            $p->cpersona_gerente=$cgerente;
                   
                        }



                        $cliente=DB::table('tpersona')                   
                        ->where(DB::raw("REPLACE(nombre,'.','')"),'ilike','%'.$r['L'].'%')
                        ->first();

                        $cpercli=null;
                        if($cliente){
                            $cpercli=$cliente->cpersona;                        
                        }

                        $p->cpersonacliente= $cpercli;
                        $p->nombre=$r['N'];
                        $p->descripcion=$r['N'];
                        $p->cestadoproyecto='001';

                        $servicio=DB::table('tservicio')
                        ->where('descripcion','ilike','%'.$r['P'].'%')
                        ->first();

                        $cserv=null;

                        if($servicio){
                            $cserv=$servicio->cservicio;
                        }

                        $p->cservicio=$cserv;                   

                        
                    /*    if(strlen($r['R'])==10){
                            $p->finicio=$r['R'];
                            $p->finicioreal=$r['R'];
                            // $fechaini='20'.substr($r['R'],6,2).'-'.substr($r['R'],3,2).'-'.substr($r['R'],0,2);
                        }

                        if(strlen($r['U'])==10){
                            $p->fcierre=$r['U'];
                            $p->fcierrereal=$r['U'];
                            $p->fcierreadministrativo=$r['U'];
                        }  */   
           
                      
                        $p->save(); 
                        //Fin Migracion Proyecto  

                         //Inicio  de migracion a Tproyectoinformacionadicional

                        $pia=new Tproyectoinformacionadicional();

                        $pia->cproyecto=$p->cproyecto;

                        $percoordinador=explode('.',$r['E']);
                        

                        if($r['E']){
                            $coordinador=DB::table('tpersonanaturalinformacionbasica')
                            ->where(DB::raw("translate(nombres,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'ilike','%'.$percoordinador[0].'%')
                            ->where(DB::raw("translate(apaterno,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'ilike','%'.$percoordinador[1].'%')
                            ->first();

                            $ccoordin=null;

                            if($coordinador){
                                $ccoordin=$coordinador->cpersona;
                            }
                              $pia->cpersona_cproyecto=$ccoordin;             
                        }  

                        $perdocum=explode('.',$r['V']);

                        if($r['V']){
                            $documenta=DB::table('tpersonanaturalinformacionbasica')
                            ->where(DB::raw("translate(nombres,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'ilike','%'.$perdocum[0].'%')
                            ->where(DB::raw("translate(apaterno,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'ilike','%'.$perdocum[1].'%')
                            ->first();

                            $cdocument=null;

                            if($documenta){
                                $cdocument=$documenta->cpersona;
                            }
                              $pia->cpersona_cdocumentario=$cdocument;             
                        }  

                        

                        $pia->save();

                       


                        //Fin  de migracion a Tproyectoinformacionadicional 

                        //Inicio Migracion Tproyectodisciplina

                        if(!(is_null($r['H']) || strlen($r['H'])<=0)){

                            $pd1->cproyecto=$p->cproyecto;
                            $pd1->cdisciplina='1';

                            $pers_rdp1=explode(' ',$r['H']);

                            $persona_rdp1=DB::table('tpersonanaturalinformacionbasica')
                            ->where('nombres','ilike','%'.$pers_rdp1[0].'%')
                            ->where('apaterno','ilike','%'.$pers_rdp1[1].'%')
                            ->first();

                            $cper_rdp1=null;
                            if($persona_rdp1){

                                $cper_rdp1=$persona_rdp1->cpersona;
                            }

                            $pd1->cpersona_rdp=$cper_rdp1;
                           // dd($pd1);
                           
                            $pd1->save();
                        }

                        if(!(is_null($r['I']) || strlen($r['I'])<=0)){

                            $pd2->cproyecto=$p->cproyecto;
                            $pd2->cdisciplina='4';

                            $pers_rdp2=explode(' ',$r['I']);

                            $persona_rdp2=DB::table('tpersonanaturalinformacionbasica')
                            ->where('nombres','ilike','%'.$pers_rdp2[0].'%')
                            ->where('apaterno','ilike','%'.$pers_rdp2[1].'%')
                            ->first();

                            $cper_rdp2=null;
                            if($persona_rdp2){

                                $cper_rdp2=$persona_rdp2->cpersona;
                            }
                            $pd2->cpersona_rdp=$cper_rdp2;
                           
                            $pd2->save();
                           
                        }

                        if(!(is_null($r['J']) || strlen($r['J'])<=0)){

                            $pd3->cproyecto=$p->cproyecto;
                            $pd3->cdisciplina='3';

                            $pers_rdp3=explode(' ',$r['J']);

                            $persona_rdp3=DB::table('tpersonanaturalinformacionbasica')
                            ->where('nombres','ilike','%'.$pers_rdp3[0].'%')
                            ->where('apaterno','ilike','%'.$pers_rdp3[1].'%')
                            ->first();

                            $cper_rdp3=null;
                            if($persona_rdp3){

                                $cper_rdp3=$persona_rdp3->cpersona;
                            }
                            $pd3->cpersona_rdp=$cper_rdp3;


                            $pd3->save();
                           
                        }

                        if(!(is_null($r['K']) || strlen($r['K'])<=0)){

                            $pd4->cproyecto=$p->cproyecto;
                            $pd4->cdisciplina='2';

                            $pers_rdp4=explode(' ',$r['K']);

                            $persona_rdp4=DB::table('tpersonanaturalinformacionbasica')
                            ->where('nombres','ilike','%'.$pers_rdp4[0].'%')
                            ->where('apaterno','ilike','%'.$pers_rdp4[1].'%')
                            ->first();

                            $cper_rdp4=null;
                            if($persona_rdp4){

                                $cper_rdp4=$persona_rdp4->cpersona;
                            }
                            $pd4->cpersona_rdp=$cper_rdp4;
                           
                            $pd4->save();
                                               
                        }
                        //Fin Migracion Tproyectodisciplina       
                    }
                                           
                }              
                                        
        }

        $i++;
                      
    }
 
     DB::commit();

    }

     public function migracionProyectoActividades(){
   
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'pruebas1.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        $datos=array();
        
        $i=0;         

        foreach($sheetData as $row){
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['E']=$row['E'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
            $r['J']=$row['J'];
            $r['K']=$row['K'];
                                     

            $pa=new Tproyectoactividade;

            if($i>0){              

                if(!(is_null($r['E']) || strlen($r['E'])<=0) && $r['K']=='S'){

                    /*$proyecto=DB::table('tproyecto')
                    ->where('codigo','ilike','%'.$r['E'].'%')
                    ->first();  

                    $cproyecto='';
                    if($proyecto){
                        $cproyecto=$proyecto->cproyecto;

                        $pa->cproyecto=$cproyecto;
                        $pa->descripcionactividad=$r['G'];
                        $pa->codigoactvidad=$r['A'];
                        $pa->save();
                    }   


                    $padre=DB::table('tproyectoactividades')                  
                    ->where('cproyecto','=',$pa->cproyecto)  
                    ->where('codigoactvidad','=',$r['C']) 
                    ->first();

                    $cpadre='';

                    if($padre){


                        $cpadre=$padre->cproyectoactividades;
                        $pap=Tproyectoactividade::where('cproyectoactividades','=',$pa->cproyectoactividades)->first();
                        $pap->cproyectoactividades_parent=$cpadre;
                        $pap->save(); 
                                                            
                    }  */

                    $proyecto=DB::table('tproyecto')
                    ->where('codigo','=',trim($r['E']))
                    ->first();  

                    $cproyecto=null;
                    if($proyecto){
                        $cproyecto=$proyecto->cproyecto;
                    }

                    $poryActi=Tproyectoactividade::where('cproyecto','=',$cproyecto)
                    ->where('codigoactvidad','=',$r['A'])->first();

                    if($poryActi){
                         $padre=DB::table('tproyectoactividades')  
                        // ->where('cproyecto','=','4289')  
                        //->where('codigoactvidad','=','01.01.01')                 
                        ->where('cproyecto','=',$cproyecto)  
                        ->where('codigoactvidad','=',$r['C']) 
                        ->first();

                        //if($cproyecto=='4289'&&$r['A']=='01.01.01'){
                            if($i=='228'){

                            // dd($cproyecto,$padre,$r['A'],$r['C'],$i);
                        }
                       


                        $cpadre=null;

                        if($padre){
                            $cpadre=$padre->cproyectoactividades;           
                                                            
                        } 

                            $poryActi->cproyectoactividades_parent=$cpadre;
                            $poryActi->save(); 
                    }

                    else{
                         dd($r['A'],$proyecto,$r['E']);

                    }
                    
                    
                }    

                $datos[count($datos)]=$r; 


                                  
            } 

            $i++;

                      
        }


    DB::commit();

    echo '<script language="javascript">alert("Migración realizada con éxito :D");</script>'; 

    }







     public function migracionHoras(){
        DB::beginTransaction();

        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Horas1.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $chunkSize = 20;
        $chunkFilter = new ChunkReadFilter();
        $objReader->setReadFilter($chunkFilter);
     
        for ($startRow = 1; $startRow <= 200; $startRow += $chunkSize) {

        $chunkFilter->setRows($startRow,$chunkSize);      
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
     
        }               
       
        $datos=array();
        $i=0;

       
        foreach($sheetData as $row){
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
            $r['J']=$row['J'];
            $r['K']=$row['K'];
            $r['L']=$row['L'];
            $r['M']=$row['M'];
            $r['N']=$row['N'];
            $r['O']=$row['O'];
            $r['P']=$row['P'];
            $r['Q']=$row['Q'];
            $r['R']=$row['R'];
            $r['S']=$row['S'];
            $r['T']=$row['T'];
            $r['U']=$row['U'];
            $r['V']=$row['V'];
            $r['W']=$row['W'];
            $r['X']=$row['X'];
            $r['Y']=$row['Y'];
            $r['Z']=$row['Z'];
                     
            $datos[count($datos)]=$r;     

            $pe=new Tproyectoejecucion;
            $pec=new Tproyectoejecucioncab;

            if($i>0){

               dd($r['A']);

                if(!(is_null($r['A']) || strlen($r['A'])<=0)){

                     dd($r['A']);

                    $pec->estado=$r['H'];
                
                    if($r['K']==trim('ANDDES')){

                        $pec->tipo='3';

                    }

                    elseif($r['K']==trim('FACTURABLE')){

                        $pec->tipo='1';

                    }

                    else{   

                        $pec->tipo='2';

                    }

                    $pec->fregistro=$r['C'];

                     $usuario=DB::table('tusuarios')
                    ->where(DB::raw("translate(nombre,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'=',$r['N'])
                    ->first();

                    if($usuario){
                        $usua=$usuario->cpersona;
                    }                    
                    $pec->cpersona_ejecuta=$usua;

                  
                    if(!($r['T']==null || strlen($r['T'])<=0)){

                        $tiposnofact=DB::table('ttiposnofacturables')
                        ->where('descripcion','ilike','%'.$r['T'].'%')
                        ->first();
                    
                        if($tiposnofact){
                            $ctipnofac=$tiposnofact->ctiponofacturable;
                            $pec->ctiponofacturable=$ctipnofac;
                        }

                        else{

                        }
                       
                    }        

                    $pec->save();
                    //Inicio Migracion tproyectoejecucion

                    $pe->cproyectoejecucioncab=$pec->cproyectoejecucioncab;
                    $pe->cpersona_ejecuta=$pec->cpersona_ejecuta;
                    
                    if($r['M']=='NO ENVIADO'){
                        $pe->estado='2';
                        $pe->horasejecutadas=$r['I'];
                        $pe->ccondicionoperativa='EJE';
                    }
                    if($r['M']=='Enviado'){
                        $pe->estado='3';
                        $pe->horasejecutadas=$r['I'];  
                        $pe->ccondicionoperativa='VJI';                      
                      
                    }
                    if($r['M']=='Aprobado Jefe'||$r['M']=='Aprobado GP'){
                        $pe->estado='5';
                        $pe->horasejecutadas=$r['I'];
                        $pe->horasaprobadas=$r['I'];
                        $pe->ccondicionoperativa='APR';
                    }
                    if($r['M']=='Observado Jefe'||$r['M']=='Observado GP'){
                        $pe->estado='4';
                        $pe->horasejecutadas=$r['I'];
                        $pe->horasaprobadas=$r['I'];
                        $pe->horasobservadas=$r['I'];
                        $pe->ccondicionoperativa='EJE';
                    }

                    $pe->cpersona_ejecuta=$pec->cpersona_ejecuta;
                    $pe->fejecuta=$r['C'];
                    $pe->fregistro=$r['C'];               
                    $pe->tipo=$pec->tipo;
                    $pe->ctiponofacturable=$pec->ctiponofacturable;
                    $pe->csubsistema='004';


                    $proyecto=DB::table('tproyecto')
                    ->where('codigo','ilike','%'.$r['A'].'%')
                    ->first();  

                    $cproyecto='';
                    if($proyecto){
                        $cproyecto=$proyecto->cproyecto;
                    }

                    $pe->cproyecto=$cproyecto;
                    dd($pec,$pe);
                    $pe->save();


                   
                    
                }            
                                        
            } 

            $i++;
                      
        }
 
    DB::commit();

    echo '<script language="javascript">alert("Migración realizada con éxito :D");</script>'; 

    }

    public function migracionRendicionGasto(){

        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'RendicionGasto1.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();
        $i=0;


      foreach($sheetData as $row){
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['F']=$row['F'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
            $r['J']=$row['J'];
            $r['K']=$row['K'];
            $r['L']=$row['L'];
            $r['M']=$row['M'];
            $r['N']=$row['N'];
            $r['O']=$row['O'];
            $r['P']=$row['P'];
            $r['Q']=$row['Q'];
            $r['R']=$row['R'];
            $r['S']=$row['S'];
            $r['T']=$row['T'];
            $r['U']=$row['U'];
            $r['V']=$row['V'];
            $r['W']=$row['W'];
            $r['X']=$row['X'];
            $r['Y']=$row['Y'];
            $r['Z']=$row['Z'];
            $r['AA']=$row['AA'];
            $r['AB']=$row['AB'];
            $r['AC']=$row['AC'];
            $r['AD']=$row['AD'];
            $r['AE']=$row['AE'];
            $r['AF']=$row['AF'];
            $r['AG']=$row['AG'];
            $r['AH']=$row['AH'];
            $r['AI']=$row['AI'];
            $r['AJ']=$row['AJ'];
            $r['AK']=$row['AK'];
            $r['AL']=$row['AL'];
            $r['AM']=$row['AM'];  
            $r['AN']=$row['AN'];    
                     
            $datos[count($datos)]=$r;    
            $g=new Tgastosejecucion;
            $gd=new Tgastosejecuciondetalle;          


            if($i>0){

                $total=0;

                if(!(is_null($r['A']) || strlen($r['A'])<=0) && !($r['B']=='GADM')){
                    //Inicio Migracion Tgastosejecucion
                    $rendgasto=DB::table('tgastosejecucion')
                    ->where('numerorendicion','=',trim($r['AJ']))
                    ->first();
                    $cgasto='';

                    if($rendgasto){
                        $cgasto=$rendgasto->numerorendicion;
                    }

                    if(!(trim($r['AJ'])== $cgasto)){
                        $g->numerorendicion=$r['AJ'];
                        $g->descripcion=$r['AE'];
                                             
                        if($r['O']=='Soles'){
                            $g->total=$r['AF'];
                        }
                        else{
                             $g->total=$r['AK'];
                        }
                        $g->frendicion=$r['P'];
                        //$g->cmoneda_devolucion='';
                        $g->cestadorendiciongasto='3';
                        $g->save();                    
                    }
                    //Fin Migracion Tgastosejecucion

                    //Inicio Migracion Tgastosejecuciondetalle

                    $crendicion=Tgastosejecucion::where('numerorendicion','=',trim($r['AJ']))->first();
                    $gd->cgastosejecucion=$crendicion->cgastosejecucion;
                    
                   // $gd->tipoactividad='00001';

                    $proyecto=DB::table('tproyecto')
                    ->where('codigo','=',trim($r['A']))
                    ->first();  

                    $cproyecto='';
                    if($proyecto){
                        $cproyecto=$proyecto->cproyecto;
                    }
                    $gd->cproyecto=$cproyecto;             

                    $area=DB::table('tareas')
                    ->where('codigo','=',trim($r['T']))
                    ->first();
                    $carea='';
                    if($area){
                        $carea=$area->carea;
                    }

                    $gd->carea=$carea;
                    
                    $gd->descripcion=$r['AA'];

                    if(!($r['V']==trim('t'))){
                        $gd->reembolsable='00001';
                    }
                    else{
                        $gd->reembolsable='00002';
                    }
                    
                    if(!(is_null($r['K']) || strlen($r['K'])<=0)){
                        $gd->tipodocumento='00001';
                    }
                    else{
                        $gd->tipodocumento='00002';
                    }

                    if(!(is_null($r['AJ']) || strlen($r['AJ'])<=0)){
                        $gd->numerodocumento=trim($r['AJ']);
                    }                
                    
                    //$gd->cpersona_cliente='';
                    //$gd->ruc_cliente='';
                    //$gd->razon_cliente='';
                   

                    if($r['O']=='Dolar Americano'){
                         $gd->cmoneda='USD';
                    }

                    elseif($r['O']=='Dolar Canadiense'){
                         $gd->cmoneda='CAD';
                    }

                    elseif($r['O']=='Soles'){
                         $gd->cmoneda='S/.';
                    }

                    elseif($r['O']=='Peso Chileno'){
                         $gd->cmoneda='$';
                    }

                    elseif($r['A']=='Real'){
                         $gd->cmoneda='BRL';
                }

                    elseif($r['A']=='Peso Argentino'){
                             $gd->cmoneda='ARS';
                    }
                    else{                       
                    }   

                    if($gd->cmoneda=='S/.'){

                        $valorcambio=Tmonedacambio::where('fecha','=',$r['Q'])
                        ->where('cmoneda','=','USD')
                        ->first();
                    }
                    else{

                        $valorcambio=Tmonedacambio::where('fecha','=',$r['Q'])
                        ->where('cmoneda','=',$gd->cmoneda)
                        ->first();
                    }

                    $gd->valorcambio=$valorcambio->cmonedacambio;

                    $gastoejecucioncab=Tgastosejecucion::where('cgastosejecucion','=',$g->cgastosejecucion)
                    ->first();
                    $g->valorcambio=$valorcambio->cmonedacambio;
                    $g->save();
                                       
         
                    ////////////////////////////////////////////////////////                                      
                    $gasto=DB::table('tconceptogastos')
                    ->where('descripcion','ilike','%'.$r['F'].'%');   
                   
                        if($r['D']==null || strlen($r['D'])<=0) {

                            $categoria=DB::table('tcatalogo')
                            ->where('codtab','=','00025')
                            ->where('descripcion','ilike','%'.$r['E'].'%')
                            ->first();
                            $idcategoria='';

                            if($categoria){
                               $idcategoria=$categoria->digide;
                            }   
                             
                        $gasto=$gasto->where('categoria_gasto','=',$idcategoria)
                        ->where('estado','=','A');

                        }  

                        else{                            
                            $nomtipo=explode('-',$r['C']);

                            $tipo=DB::table('tcatalogo')
                            ->where('codtab','=','00026')
                            ->where('descripcion','ilike','%'.$nomtipo[1].'%')
                            ->first();
                            $idtipo='';

                            if($tipo){
                               $idtipo=$tipo->digide;
                            }   

                             $padre=DB::table('tconceptogastos')
                             ->where('descripcion','ilike','%'.$r['E'].'%')
                             ->where('tipogasto','=',$idtipo)
                             ->first();
                             $idpadre='';
                             if($padre){
                                $idpadre=$padre->cconcepto;
                             }      

                              dd($tipo,$nomtipo);        
                          
                        $gasto=$gasto->where('cconcepto_parent','=',$idpadre);

                        }

                        $gasto=$gasto->first();

                        dd($gasto,$r['F']);

                        $gd->cconcepto=$gasto->cconcepto;
                        $gd->cconcepto_parent=$gasto->cconcepto_parent;

                  
                    ////////////////////////////////////////////////////////

                    if($gasto->tipogasto=='00002'){

                         $gd->cantidad=$r['Y'];
                         $gd->preciounitario=$r['Z']; 
                    }                   
                   
                    $gd->impuesto=$r['K'];
                    $gd->otroimpuesto=$r['L'];
                    $gd->subtotal=$r['J'];
                    $gd->total=$r['I'];
                    $gd->fregistro=$r['P'];

                    $empleado=DB::table('tusuarios')
                    ->where(DB::raw("translate(nombre,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn')"),'=',$r['G'])
                    ->first();

                    $cempleado='';
                    if($empleado){
                        $cempleado=$empleado->cpersona;
                        $gd->cpersona_empleado=$cempleado;
                    }
                    else{
                        if($gasto->tipogasto=='00002'){
                            if($r['G']=='administracion1' ||$r['G']=='administracion2'||$r['G']=='administracion3'||$r['G']=='administracion4'){
                                $admlab=Tusuario::where('nombre','ilike','%'.'bruno.chumbes'.'%')->first();                                
                                $gd->cpersona_empleado=$admlab->cpersona;
                            }

                        }
                        else{
                            if($r['G']=='administracion1'){
                                $adm1=Tusuario::where('nombre','ilike','%'.'bruno.chumbes'.'%')->first();
                                $gd->cpersona_empleado=$adm1->cpersona;
                            }
                            if($r['G']=='administracion2'){
                                $adm2=Tusuario::where('nombre','ilike','%'.'silvia.vásquez'.'%')->first();
                                $gd->cpersona_empleado=$adm2->cpersona;
                            }
                            if($r['G']=='administracion3'){
                                $adm3=Tusuario::where('nombre','ilike','%'.'sara.calderon'.'%')->first();
                                $gd->cpersona_empleado=$adm3->cpersona;
                            }
                            if($r['G']=='administracion4'){
                                $adm4=Tusuario::where('nombre','ilike','%'.'jhon.gutiérrez'.'%')->first();
                                $gd->cpersona_empleado=$adm4->cpersona;
                            }
                        }
                    }                     

                    if(!($r['Q']==null || strlen($r['G'])<=0)){
                        $gd->fdocumento=$r['Q'];
                    }
                    $gd->fregistro=$r['P'];
                    $gd->save();
                 //Fin Migracion Tgastosejecuciondetalle  
                }                                                    
            }

            $i++;
                      
        }
 
    DB::commit();

    echo '<script language="javascript">alert("Migración realizada con éxito :D");</script>'; 
    }   

    public function migracionMonedaCambio(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'TipoCambio.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();
      
        $i=0;
        foreach($sheetData as $row){
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $datos[count($datos)]=$r;
                        
            if($i>0){
                $m= new Tmonedacambio();


                $moneda=DB::table('tmonedas')
                ->where('descripcion','ilike','%'.trim($r['A']).'%')
                ->first();

                $cmoneda=null;

                if($moneda){
                    $cmoneda=$moneda->cmoneda;
                }

                $m->cmoneda=$cmoneda;
                $m->cmonedavalor='S/.';
                $m->fecha=$r['B'];
                $m->valorcompra=$r['C'];
                $m->valorventa=$r['D'];
              
                $m->save();
                                            
            
            }

                $i++;        
        
        }

        DB::commit();
    }


    public function migracionData(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Data.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();
      
        $i=0;
        foreach($sheetData as $row){            
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['F']=$row['F'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
            $r['J']=$row['J'];
            $r['K']=$row['K'];
            $r['L']=$row['L'];
            $r['M']=$row['M'];
            $r['N']=$row['N'];
            $r['O']=$row['O'];
            $r['P']=$row['P'];
            $r['Q']=$row['Q'];
            $r['R']=$row['R'];
           
            $datos[count($datos)]=$r;
                        
            if($i>0){
               

                $persona=DB::table('tpersona as p')                
                ->where('p.identificacion','ilike','%'.$r['C'].'%')  
                ->where('p.ctipopersona','=','NAT')              
                ->first();

                $cpersona=null;

                if($persona){
                    $cpersona=$persona->cpersona;
                }

                if((strlen($cpersona)<=0 || is_null($cpersona)) &&  trim($r['B'])=='Activo'){
                    $p=new Tpersona();

                    
                   
                    

                    $pn=new Tpersonanaturalinformacionbasica();
                    //$pe=new Tpersonadatosempleado();
                    $pn->cpersona=$p->cpersona;
                    //$pe->cpersona=$p->cpersona;

                    //dd($p,$pe,$pn);
                

                    
                    //Inicio Migracion Tpersona
                    $p->ctipopersona='NAT';
                    $p->ctipoidentificacion='2';
                    $p->identificacion=$r['C'];
                    $p->nombre=$r['F'];
                    $p->save();

                    //Fin Migracion Tpersona

                    //Inicio Migracion Tpersonadatosempleado


                     $empleado=DB::table('tpersona as p')                
                    ->where('p.cpersona','=',$p->cpersona)  
                    ->where('p.ctipopersona','=','NAT')  
                    ->first();            

                    
              /*    $area=DB::table('tareas')                
                    //->where('descripcion','ilike','%'.$r['H'].'%')     
                    ->where('descripcion','=',$r['H'])              

                    ->first();

                    $cempl=null;

                    if($empleado){
                        $pe=new Tpersonadatosempleado();
                        $pe->cpersona=$empleado->cpersona;
                        $pe->estado=$r['B'];

                        $area=DB::table('tareas')                
                        //->where('descripcion','ilike','%'.$r['H'].'%')     
                        ->where('descripcion','=',$r['I'])              
                        ->first();
                        $carea=null;
                       

                        if($area){
                            $carea=$area->carea;
                           
                        }   
                        $pe->carea=$carea;              

                        $cargo=DB::table('tcargos')                
                        //->where('descripcion','ilike','%'.$r['L'].'%')  
                        ->where('descripcion','=',$r['M'])                
                        ->first();
                        $ccargo=null;
                        
                         if($cargo){
                            $ccargo=$cargo->ccargo;
                            
                        }
                        $pe->ccargo=$ccargo;

                        $categoria=DB::table('tcategoriaprofesional')                
                        //->where('descripcion','ilike','%'.$r['M'].'%')
                        ->where('descripcion','=',$r['N'])                  
                        ->first();
                        $ccategoria=null;

                        if($categoria){
                            $ccategoria=$categoria->ccategoriaprofesional;
                            
                        } 
                        $pe->ccategoriaprofesional=$ccategoria;

                        $pe->fingreso=$r['P'];               



                        $pe->save();    
                       
                    }

                    $pe->save();    */


                    //Fin Migracion Tpersonadatosempleado  

                    //Inicio Migracion Tpersonanaturalinformacionbasica
                     $pernat=DB::table('tpersona as p')                
                    ->where('p.cpersona','=',$p->cpersona)  
                    ->where('p.ctipopersona','=','NAT')              
                    ->first(); 
                    if($pernat){
                        $pn=new Tpersonanaturalinformacionbasica();
                        $pn->cpersona=$p->cpersona;                   
                        $apellidos=explode(' ', $r['D']);
                        $pn->apaterno=$apellidos[0];
                        $pn->amaterno=$apellidos[1];
                        $pn->esempleado='1';
                        $pn->nombres=$r['E'];
                        $pn->identificacion=$r['C'];
                        $pn->ctipoidentificacion='2';
                        $pn->save();
                    }
                    

                    //Fin Migracion Tpersonanaturalinformacionbasica  
                }
            }

                $i++;        
        
        }

        DB::commit();
    }


    public function migracionUsuariosAbreviatura(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Usuarios.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();
      
        $i=0;
        foreach($sheetData as $row){            
            $r['A']=$row['A'];
            $r['B']=$row['B'];           
            $datos[count($datos)]=$r;
                        
            if($i>0){
               

                $persona=DB::table('tpersona as p')                
                ->where('p.identificacion','ilike','%'.$r['B'].'%')  
                ->where('p.ctipopersona','=','NAT')              
                ->first();
                $cpersona=null;

                if($persona){
                    $cpersona=$persona->cpersona;

                    $usuario=explode('.', $r['A']);

                   

                     $p=Tpersona::where('cpersona','=',$cpersona)->first();
                     $p->abreviatura=ucwords($usuario[0]).' '.ucwords($usuario[1]);

                     $p->save();
                 
                }                                           
            
            }

                $i++;        
        
        }

        DB::commit();
    }


     public function migracionProyectosDisciplina(){
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'ProyectosDisciplina.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $datos=array();
      
        $i=0;
        foreach($sheetData as $row){            
            $r['B']=$row['B'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['V']=$row['V'];   
            $r['W']=$row['W'];
            $r['X']=$row['X'];
            $r['Y']=$row['Y'];
            $r['Z']=$row['Z'];
            $r['AA']=$row['AA'];
            $r['AB']=$row['AB'];
            $r['AC']=$row['AC'];
            $r['AD']=$row['AD'];
            $datos[count($datos)]=$r;
                        
            if($i>0){               

                $proyecto=DB::table('tproyecto')
                ->where('codigo','=',trim($r['B']))
                ->first(); 

                $cproyecto=null;
                if($proyecto){
                    $cproyecto=$proyecto->cproyecto;
                }

                else{
                    dd('proyecto no existe'.$r['B']);
                }


                /**************** INICIO MIGRACION Tproyectopersona ****************/                
               

                if(!(is_null(trim($r['D'])) || strlen(trim($r['D']))<=0)){

                            $pp=new Tproyectopersona;

                            $pp->cproyecto=$cproyecto;
                          
                            $codpersona=explode('.',$r['D']);
                           
                            $persona=DB::table('tusuarios as tu')
                            ->leftjoin('tpersonadatosempleado as te','tu.cpersona','=','te.cpersona')
                            ->where('tu.nombre','=',trim($r['D']))
                            ->first();

                            $cpersona=null;
                            $categoria=null;
                            $carea=null;
                            if($persona){

                                $cpersona=$persona->cpersona;
                                $categoria=$persona->ccategoriaprofesional;
                                $carea=$persona->carea;
                            }

                            $pp->cpersona=$cpersona;
                            $pp->ccategoriaprofesional=$categoria;

                            $tdisciplina = DB::table('tdisciplinaareas as ta')
                            ->leftjoin('tdisciplina as d','ta.cdisciplina','=','d.cdisciplina')
                            ->where('ta.carea','=',$carea)        
                            ->first(); 

                            $cdisciplina=null;
                            if($tdisciplina){
                                $cdisciplina=$tdisciplina->cdisciplina;
                            }   

                            $pp->cdisciplina=$cdisciplina;
                            $pp->eslider='0'; 
                                 
                            $pp->save();
                }

                if(!(is_null(trim($r['E'])) || strlen(trim($r['E']))<=0)){

                            $pp=new Tproyectopersona;

                            $pp->cproyecto=$cproyecto;
                          
                            $codpersona=explode('.',$r['E']);
                           
                            $persona=DB::table('tusuarios as tu')
                            ->leftjoin('tpersonadatosempleado as te','tu.cpersona','=','te.cpersona')
                            ->where('tu.nombre','=',trim($r['E']))
                            ->first();

                             $cpersona=null;
                            $categoria=null;
                            $carea=null;
                            if($persona){

                                $cpersona=$persona->cpersona;
                                $categoria=$persona->ccategoriaprofesional;
                                $carea=$persona->carea;
                            }

                            $pp->cpersona=$cpersona;
                            $pp->ccategoriaprofesional=$categoria;

                            $tdisciplina = DB::table('tdisciplinaareas as ta')
                            ->leftjoin('tdisciplina as d','ta.cdisciplina','=','d.cdisciplina')
                            ->where('ta.carea','=',$carea)        
                            ->first(); 

                            $cdisciplina=null;
                            if($tdisciplina){
                                $cdisciplina=$tdisciplina->cdisciplina;
                            }   

                            $pp->cdisciplina=$cdisciplina;
                            $pp->eslider='1'; 
                                 
                            $pp->save();
                }

                if(!(is_null(trim($r['V'])) || strlen(trim($r['V']))<=0)){

                            $pp=new Tproyectopersona;

                            $pp->cproyecto=$cproyecto;
                          
                            $codpersona=explode('.',$r['V']);
                           
                            $persona=DB::table('tusuarios as tu')
                            ->leftjoin('tpersonadatosempleado as te','tu.cpersona','=','te.cpersona')
                            ->where('tu.nombre','=',trim($r['V']))
                            ->first();

                             $cpersona=null;
                            $categoria=null;
                            $carea=null;
                            if($persona){

                                $cpersona=$persona->cpersona;
                                $categoria=$persona->ccategoriaprofesional;
                                $carea=$persona->carea;
                            }

                            $pp->cpersona=$cpersona;
                            $pp->ccategoriaprofesional=$categoria;

                            $tdisciplina = DB::table('tdisciplinaareas as ta')
                            ->leftjoin('tdisciplina as d','ta.cdisciplina','=','d.cdisciplina')
                            ->where('ta.carea','=',$carea)        
                            ->first(); 


                            $cdisciplina=null;
                            if($tdisciplina){
                                $cdisciplina=$tdisciplina->cdisciplina;
                            }   

                            $pp->cdisciplina=$cdisciplina;
                            $pp->eslider='1'; 
                                 
                            $pp->save();
                }

                

                /**************** FIN MIGRACION Tproyectopersona ****************/

                /**************** INICIO MIGRACION Tproyectodisciplina ****************/
         
               


                if(!(is_null(trim($r['E'])) || strlen(trim($r['E']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='10';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['E']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }


                if(!(is_null(trim($r['V'])) || strlen(trim($r['V']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='11';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['V']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }
                if(!(is_null(trim($r['W'])) || strlen(trim($r['W']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='1';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['W']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }

                
                if(!(is_null(trim($r['X'])) || strlen(trim($r['X']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='4';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['X']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }
                if(!(is_null(trim($r['Y'])) || strlen(trim($r['Y']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='2';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['Y']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }

                
                if(!(is_null(trim($r['Z'])) || strlen(trim($r['Z']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='3';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['Z']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }
                if(!(is_null(trim($r['AA'])) || strlen(trim($r['AA']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='6';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['AA']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }

                
                if(!(is_null(trim($r['AB'])) || strlen(trim($r['AB']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='7';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['AB']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }
                if(!(is_null(trim($r['AC'])) || strlen(trim($r['AC']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='13';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['AC']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }

                
                if(!(is_null(trim($r['AD'])) || strlen(trim($r['AD']))<=0)){

                            $pd=new Tproyectodisciplina;  

                            $pd->cproyecto=$cproyecto;
                            $pd->cdisciplina='9';
                      

                            $persona=DB::table('tusuarios as tu')                 
                            ->where('tu.nombre','=',trim($r['AD']))
                            ->first();

                            $cpersona=null; 
                            if($persona){
                                $cpersona=$persona->cpersona;
                            }

                            $pd->cpersona_rdp=$cpersona;
                            //dd($pd);
                           
                            $pd->save();
                }

                        $pd=new Tproyectodisciplina; 


                        $pd->cproyecto=$cproyecto;
                        $pd->cdisciplina='12';
                        $pd->cpersona_rdp='3859';
                           
                        $pd->save();


            
            }

                $i++;        
        
        }

        DB::commit();
    }

    public function migracionProyActivHabilitacion(){
   
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'pruebas1.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        $datos=array();
        
        $i=0;         

        foreach($sheetData as $row){
            $r['A']=$row['A'];
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['E']=$row['E'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $datos[count($datos)]=$r;                                   

           
            if($i>0){              

                if(!(is_null($r['A']) || strlen($r['A'])<=0)){

                    $proyecto=DB::table('tproyecto')
                    ->where('codigo','ilike','%'.$r['A'].'%')
                    ->first(); 
                   
                    $cproyecto='';
                    if($proyecto){
                        $cproyecto=$proyecto->cproyecto;
                    }  

                  /*  else{
                        dd('proyecto no existe'.$r['A']);
                    }*/

                    $persona=DB::table('tusuarios as tu')                 
                            ->leftjoin('tpersonadatosempleado as te','tu.cpersona','=','te.cpersona')
                            ->where('tu.nombre','=',trim($r['C']))
                            ->first();
                            $carea=null;
                            $categoria=null;
                            $cpersona=null; 
                    if($persona){
                                $cpersona=$persona->cpersona;
                                $carea=$persona->carea;
                                $categoria=$persona->ccategoriaprofesional;
                    }
                   /* else{
                         dd('persona no existe'.$r['C']);
                    }*/

                    $proyectoactividad=DB::table('tproyectoactividades')                  
                    ->where('cproyecto','=',$cproyecto)  
                    ->where('codigoactvidad','=',trim($r['B'])) 
                    ->first();

                    $cproyectoactiv=null;

                    if($proyectoactividad){
                        $cproyectoactiv=$proyectoactividad->cproyectoactividades;
                    }

                   /* else{
                         dd('actvidad no existe'.$r['B']);
                    }*/


                    $tdisciplina = DB::table('tdisciplinaareas as ta')
                            ->leftjoin('tdisciplina as d','ta.cdisciplina','=','d.cdisciplina')
                            ->where('ta.carea','=',$carea)        
                            ->first(); 


                            $cdisciplina=null;
                            if($tdisciplina){
                                $cdisciplina=$tdisciplina->cdisciplina;
                            }   




                    $ph=Tproyectoactividadeshabilitacion::where('cproyecto','=',$cproyecto)
                    ->where('cproyectoactividades','=',$cproyectoactiv)
                    ->where('cpersona','=',$cpersona)
                    ->first();


                    if(is_null($ph) || strlen($ph)<=0){

                        $ph=new Tproyectoactividadeshabilitacion();

                        $ph->cproyectoactividades=$cproyectoactiv;
                        $ph->cpersona=$cpersona;
                        $ph->cproyecto=$cproyecto;
                        $ph->activo='1';


                        $ph->save();

                       

                    }

                   /* else{
                        dd('ya existe'.' '.'proy'.$r['A'].' '.'proact'.$r['B'].' '.'per'.$r['C']);
                    }*/

                   $pp=Tproyectopersona::where('cproyecto','=',$cproyecto)                    
                    ->where('cpersona','=',$cpersona)
                    ->first();

                    if(is_null($pp) || strlen($pp)<=0){

                        $pp=new Tproyectopersona();

                       // $pp->cproyectoactividades=$cproyectoactiv;
                        $pp->cpersona=$cpersona;
                        $pp->cproyecto=$cproyecto;
                        $pp->cdisciplina=$cdisciplina;
                        $pp->ccategoriaprofesional=$categoria;
                        $pp->eslider='0';


                        $pp->save();

                       

                    }
                    
                }    

                


                                  
            } 

            $i++;

                      
        }


    DB::commit();

    

    }

    public function migracionProyEstados(){
   
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'ProyectoEstados.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        $datos=array();
        
        $i=0;         

        foreach($sheetData as $row){
            
            $r['B']=$row['B'];
            $r['C']=$row['C'];
           
            $datos[count($datos)]=$r;                                   

           
            if($i>0){              

                if(!(is_null($r['B']) || strlen($r['B'])<=0)){

                    $proyecto=DB::table('tproyecto')
                    ->where('codigo','=',trim($r['B']))
                    ->first(); 
                   
                    $cproyecto=null;
                    if($proyecto){
                        $cproyecto=$proyecto->cproyecto;
                    }  
                    

                    if(!(is_null($cproyecto) || strlen($cproyecto)<=0)){

                        $ph=Tproyecto::where('cproyecto','=',$cproyecto)
                        ->first();
                      
                        if($r['C']=='ACTIVO'){
                            $ph->cestadoproyecto='001';
                        }

                        if($r['C']=='Cancelado'){
                            $ph->cestadoproyecto='002';
                        }

                        if($r['C']=='Cierre ADM'){
                            $ph->cestadoproyecto='005';
                        }

                        if($r['C']=='Cierre-TEC'){
                            $ph->cestadoproyecto='004';
                        }

                        if($r['C']=='Paralizado'){
                            $ph->cestadoproyecto='003';
                        }

                      //  dd($ph);
                        $ph->save();
                       

                    }                   
                    
                }    

            } 

            $i++;

                      
        }


    DB::commit();

    

    }

    public function migracionAreaCargoDespliegue(){
   
        DB::beginTransaction();
        $inputFileType = 'Excel2007';
        $ruta = storage_path('archivos') . "/" . 'Formato_despliegue.xlsx';
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);
        $objPHPExcel = $objReader->load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,false,true,true);
        $datos=array();
        
        $i=0;         

        foreach($sheetData as $row){
            
            $r['B']=$row['B'];
            $r['C']=$row['C'];
            $r['D']=$row['D'];
            $r['G']=$row['G'];
            $r['H']=$row['H'];
            $r['I']=$row['I'];
           
            $datos[count($datos)]=$r;                                   

           
            if($i>1){              

                if(!(is_null($r['B']) || strlen($r['B'])<=0)){

                    $a=Tarea::where('codigo','=',$r['B'])
                    ->first(); 

                    if($a){

                        if(!(is_null($r['D'])||strlen($r['D'])<0)){

                            $area=DB::table('tareas')
                            ->where('codigo','=',trim($r['D']))
                            ->first();
                            $carea=null;
                            if($area){
                                $carea=$area->carea;
                            }

                            $a->careaparentdespliegue=$carea;
                            $a->save();
                        }
                    }
                   
                }    




                if(!(is_null($r['H']) || strlen($r['H'])<=0)){

                    $c=Tcargo::where('codigo','=',$r['G'])
                    ->first(); 

                   
                    if($c){

                        if(!(is_null($r['I'])||strlen($r['I'])<0)){
                            $cargo=DB::table('tcargos')                        
                            ->where('codigo','=',$r['I'])                
                            ->first();
                            $ccargo=null;

                                        
                             if($cargo){
                                $ccargo=$cargo->ccargo;
                                
                            }
                            
                            $c->cargoparentdespliegue=$ccargo;
                            $c->save();
                        }
                    }
                   
                }  

            } 

            $i++;                      
        }
    }


    public function asignarpermisoListaProy(){

    DB::beginTransaction();

     $tusuarios=DB::table('tusuarios')     
     ->get();

     foreach ($tusuarios as $u) {

        $tmp=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','03')
        ->first();

        if(!$tmp){

            $tmenupersona= new Tmenupersona();
            $tmenupersona->menuid='03';
            $tmenupersona->cpersona=$u->cpersona;
            $tmenupersona->permiso='01';
            $tmenupersona->save();            
        }

        $tmp1=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','0368')
        ->first();        

        if(!$tmp1){

           // dd($tmp1);

            $tmenupersona1= new Tmenupersona();
            $tmenupersona1->menuid='0368';
            $tmenupersona1->cpersona=$u->cpersona;
            $tmenupersona1->permiso='01';
            $tmenupersona1->save();
            
        }    
        
     }

      DB::commit();


    }

    public function asignarpermisoGastos(){

    DB::beginTransaction();

     $tusuarios=DB::table('tusuarios')     
     ->get();

     foreach ($tusuarios as $u) {

        $tmp=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','05')
        ->first();

        if(!$tmp){

            $tmenupersona= new Tmenupersona();
            $tmenupersona->menuid='05';
            $tmenupersona->cpersona=$u->cpersona;
            $tmenupersona->permiso='01';
            $tmenupersona->save();            
        }

        $tmp1=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','0501')
        ->first();        

        if(!$tmp1){

           // dd($tmp1);

            $tmenupersona1= new Tmenupersona();
            $tmenupersona1->menuid='0501';
            $tmenupersona1->cpersona=$u->cpersona;
            $tmenupersona1->permiso='01';
            $tmenupersona1->save();
            
        }    

       /* $tmp2=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','0502')
        ->delete(); */  

      
        
     }

      DB::commit();


    }

    public function asignarArea(){

         $fecha = Carbon::createFromFormat('Y-m-d H:i:s','2018-04-30 10:00:00');
    
        //for ($i=3; $i < 6; $i++) { 

            $tusuarios=DB::table('tproyectoejecucion')  
           
            //->where(DB::raw('EXTRACT(YEAR FROM fregistro)'),'=','2018')
            //->where(DB::raw('EXTRACT(WEEK FROM fregistro)'),'=','4') 
            ->where('cpersona_ejecuta','=','3918')
            ->where('fregistro','<=',$fecha)
            ->get();

           // dd($tusuarios,$fecha);          
            
            foreach ($tusuarios as $u) {

                $tarea = DB::table('tpersonadatosempleado as e') 
                ->leftjoin('tusuarios as u','e.cpersona','=','u.cpersona')          
                ->where('e.cpersona','=',$u->cpersona_ejecuta)
                ->where('u.estado','=','ACT')
                ->first();

                $carea=null;
                if($tarea){
                  $carea=$tarea->carea;
                }  

                if($u->fregistro>=$fecha){
                    //dd($u,'valores mayores');
                }

                if($u->fregistro<=$fecha){
                    //dd($u,'valores menores');
                }
              
               
                    DB::beginTransaction();

                        $tmp1=Tproyectoejecucion::where('cproyectoejecucion','=',$u->cproyectoejecucion)                   
                        ->first();        

                        $tmp1->carea=$carea;
                        $tmp1->save();  

                    DB::commit();             
            }
       // }
    }


    public function asignarpermisoLecAprend(){

    DB::beginTransaction();

     $tusuarios=DB::table('tusuarios')     
     ->get();

     foreach ($tusuarios as $u) {

        $tmp=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','03')
        ->first();

        if(!$tmp){

            $tmenupersona= new Tmenupersona();
            $tmenupersona->menuid='03';
            $tmenupersona->cpersona=$u->cpersona;
            $tmenupersona->permiso='01';
            $tmenupersona->save();            
        }

        $tmp1=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','0339')
        ->first();        

        if(!$tmp1){

           // dd($tmp1);

            $tmenupersona1= new Tmenupersona();
            $tmenupersona1->menuid='0339';
            $tmenupersona1->cpersona=$u->cpersona;
            $tmenupersona1->permiso='01';
            $tmenupersona1->save();
            
        }   

        $tmp1=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','0369')
        ->first();        

        if(!$tmp1){

           // dd($tmp1);

            $tmenupersona1= new Tmenupersona();
            $tmenupersona1->menuid='0369';
            $tmenupersona1->cpersona=$u->cpersona;
            $tmenupersona1->permiso='01';
            $tmenupersona1->save();
            
        }   
        
     }

      DB::commit();


    }

    public function asignarpermisoVerHR(){

    DB::beginTransaction();

     $tusuarios=DB::table('tusuarios')     
     ->get();

     foreach ($tusuarios as $u) {

        $tmp=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','03')
        ->first();

        if(!$tmp){

            $tmenupersona= new Tmenupersona();
            $tmenupersona->menuid='03';
            $tmenupersona->cpersona=$u->cpersona;
            $tmenupersona->permiso='01';
            $tmenupersona->save();            
        }

        $tmp1=Tmenupersona::where('cpersona','=',$u->cpersona)
        ->where('menuid','=','0359')
        ->first();        

        if($tmp1){

           // dd($tmp1);

            $tmenupersona1= new Tmenupersona();
            $tmenupersona1->menuid='0317';
            $tmenupersona1->cpersona=$u->cpersona;
            $tmenupersona1->permiso='01';
            $tmenupersona1->save();
            
        }   

     }

      DB::commit();


    }


    public function MigracionHomologaDataIndex_Old()
    {   
        $prueba = XX_Migracion::all();

        //dd($prueba[0]['nombre_gasto']);

        $totalprecio = 0;
        $totalpreciodolares = 0;

        for ($i=0; $i < count($prueba); $i++) { 
            
            
            $codigo = $prueba[$i]['proyectoid'];
            // valido laboratorio
            if ($prueba[$i]['laboratorio_cantidad'] == '') {
                $prueba[$i]['laboratorio_cantidad'] = 1; 
            } 
            //valido precio unitario
            if ($prueba[$i]['laboratorio_pu'] == '') {
                $prueba[$i]['laboratorio_pu'] = $prueba[$i]['monto_igv']; 
            } 
            //valido  IVG
            if ($prueba[$i]['ivg'] == '') {
                $prueba[$i]['ivg'] = 0; 
            }
            //valido oTROS Impuestos
            if ($prueba[$i]['otro_impuesto'] == '') {
                $prueba[$i]['otro_impuesto'] = 0; 
            }

            //dd($prueba[$i]['monto_igv']);
            //valido el proyecto si existe
            $proyecto = Tproyecto::where('codigo','=',$codigo)->get();   
            //dd($prueba[$i]['monto_igv']);
            $laboratorio = $prueba[$i]['laboratorio_cantidad'];
            $labopusincoma = $prueba[$i]['laboratorio_pu'];
            $laboratorio_pu = $labopusincoma;


            //dd($laboratorio_pu);
            $ivg = $prueba[$i]['ivg'];
            $otro_impuesto = $prueba[$i]['otro_impuesto'];
            //dd($prueba[$i]['otro_impuesto']);
            //$cantidad = str_replace(',', '.', $laboratorio);
            $cantidad = $laboratorio;
            $cantidadEntero = $prueba[$i]['laboratorio_cantidad'];
            //dd($cantidadEntero);
            $preciounitario = $laboratorio_pu;
            //$preciounitario2 = number_format($preciounitario, 2, '.', ',');
            //dd($preciounitario2);
            $subtotal = $cantidad * $preciounitario;
            //dd($subtotal);
            $impuesto = $ivg;
            $impuesto2 = $impuesto;
            
            
            $montoivgreplase = $prueba[$i]['monto_igv'];
            $supantomonto_igv = $montoivgreplase;
            $monto_igv =  $prueba[$i]['monto_igv'];
            //if ($monto_igv>1500) {
            //dd($supantomonto_igv);
                # code...
            //}
            //precio unitario
            //$preciounitario = ($prueba[$i]['laboratorio_pu'] == 0) ? str_replace(',', '.',$monto_igv): $laboratorio_pu;
            //dd($preciounitario);
            //dd($monto_igv);
            //$otroimpuesto = number_format($otro_impuesto, 2, '.', ',');
            //dd($otroimpuesto);
            $total = $subtotal + $impuesto2 + $otro_impuesto;
           
            $valor_cambio = $prueba[$i]['venta'];
            $valor_cambio2 = $valor_cambio;
            //dd($valor_cambio2);
            $total_soles1 = ($prueba[$i]['moneda']=='S/.') ? $total : $total*$valor_cambio2;
            $total_soles = $total_soles1;

            //dd($total_soles);
            $total_dolares1 = ($prueba[$i]['moneda']=='USD') ? $total : $total/$valor_cambio2;  
            $total_dolares = $total_dolares1;


            $concepto = Tconceptogasto::where('descripcion', '=',$prueba[$i]['nombre_gasto'])->where('estado','=', '00001')->first();
            //dd($total_soles);

            //dd(number_format($valor_cambio, 2, '.', ','));

            $existe_gasto_ejecucion = Tgastosejecucion::where('numerorendicion','=',str_replace('10 - ', '', $prueba[$i]['numero_completo']))->get();   
            //dd(count($existe_gasto_ejecucion));
            //ejecucion cuando pasa el primer registro no insertado en la tgastosejecion

            if (count($existe_gasto_ejecucion) == 0) {
                //busco proyecto
                $codigo = $prueba[$i]['proyectoid'];
                $proyecto = Tproyecto::where('codigo','=',$codigo)->first();
                $cgastosejecucion = Tgastosejecucion::where('numerorendicion','=',str_replace('10 - ', '', $prueba[$i]['numero_completo']))->first(); 
                $concepto = Tconceptogasto::where('descripcion', '=',$prueba[$i]['nombre_gasto'])->where('estado','=', '00001')->get();
                //dd($cgastosejecucion);     
                //Tabla gastos ejecucion
                $tgastosejecucion = new Tgastosejecucion();
                //$tgastosejecucion->cgastosejecucion = 
                $tgastosejecucion->numerorendicion =  str_replace('10 - ', '', $prueba[$i]['numero_completo']);
                $tgastosejecucion->descripcion = $prueba[$i]['nombre_rendicion'];
                //$tgastosejecucion->valorcambio =  no se llena
                $tgastosejecucion->total = $total_soles;
                $tgastosejecucion->totaldolares = $total_dolares;
                $tgastosejecucion->save();
                //dd($tgastosejecucion->cgastosejecucion);
                //Tabla de gastos ejecuion detalle nuevo registro
                $Tgastosejecuciondetalle = new Tgastosejecuciondetalle();
                //$Tgastosejecuciondetalle->cgastosejecuciondet = correlativo - PK
                $Tgastosejecuciondetalle->cgastosejecucion = $tgastosejecucion->cgastosejecucion;
                // $Tgastosejecuciondetalle->ccentrocosto = no se llena
                // $Tgastosejecuciondetalle->tipoactividad = no se llena
                $Tgastosejecuciondetalle->cproyecto = $proyecto->cproyecto;
                // $Tgastosejecuciondetalle->cpropuesta = no se llena
                // $Tgastosejecuciondetalle->carea = no se llena
                // $Tgastosejecuciondetalle->tipogasto = no se llena
                $Tgastosejecuciondetalle->descripcion = $prueba[$i]['descripcion'];
                $Tgastosejecuciondetalle->reembolsable = '00002';
                $Tgastosejecuciondetalle->tipodocumento = ($prueba[$i]['ivg'] == 0) ? '00002' : '00001'; //si no tiene igv (00002), si tiene igv (00001)
                $Tgastosejecuciondetalle->numerodocumento = $prueba[$i]['num_factura'];
                // $Tgastosejecuciondetalle->cpersona_cliente = 
                // $Tgastosejecuciondetalle->ruc_cliente = no se llena
                $Tgastosejecuciondetalle->razon_cliente = $prueba[$i]['proveedor'];
                $Tgastosejecuciondetalle->valorcambio = $valor_cambio2;
                $Tgastosejecuciondetalle->cmoneda = $prueba[$i]['moneda']; //buscar pk de la tabla tmonedas segun la moneda
                $Tgastosejecuciondetalle->cantidad = ($prueba[$i]['laboratorio_cantidad'] == 0) ? 1 : str_replace(',', '', $cantidadEntero);//si no tiene laboratorio_cantidad = 1 y si tiene = laboratorio_cantidad
                //dd($preciounitario);
                $Tgastosejecuciondetalle->preciounitario = $preciounitario;//si tiene laboratorio_pu = laboratorio_pu sino tiene laboratorio_pu = monto_igv
                $Tgastosejecuciondetalle->subtotal = $subtotal;
                $Tgastosejecuciondetalle->impuesto = $ivg;
                $Tgastosejecuciondetalle->total = $total;
                // $Tgastosejecuciondetalle->cpersona_empleado =
                // $Tgastosejecuciondetalle->ccondicionoperativa =
                $Tgastosejecuciondetalle->fdocumento = $prueba[$i]['fecha_factura'];
                $Tgastosejecuciondetalle->fregistro = $prueba[$i]['fecha_rendicion'];
                $Tgastosejecuciondetalle->cconcepto = $concepto[0]['cconcepto']; //buscar el pk en la tabla tconceptogastos segun descripcion si no exite colocar segun estado 00001
                $Tgastosejecuciondetalle->cconcepto_parent = $concepto[0]['cconcepto_parent'];//concepto_parent de $Tgastosejecuciondetalle->cconcepto
                $Tgastosejecuciondetalle->otroimpuesto = str_replace(',', '.', $prueba[$i]['otro_impuesto']);
                $Tgastosejecuciondetalle->save();
            }
            
            $cuentogastocab = Tgastosejecucion::where('numerorendicion','=',str_replace('10 - ', '', $prueba[$i]['numero_completo']))->get();
            //dd($cuentogastocab);
            if (count($cuentogastocab) == 1) {
                //busco proyecto
                $codigo = $prueba[$i]['proyectoid'];
                $proyecto = Tproyecto::where('codigo','=',$codigo)->first();
                $cgastosejecucion = Tgastosejecucion::where('numerorendicion','=',str_replace('10 - ', '', $prueba[$i]['numero_completo']))->first();
                $concepto = Tconceptogasto::where('descripcion', '=',$prueba[$i]['nombre_gasto'])->where('estado','=', '00001')->get(); 
                //dd($concepto[0]['cconcepto']);

                //dd($cgastosejecucion);  
                //Tabla de gastos ejecuion detalle nuevo registro
                $Tgastosejecuciondetalle = new Tgastosejecuciondetalle();
                //$Tgastosejecuciondetalle->cgastosejecuciondet = correlativo - PK
                $Tgastosejecuciondetalle->cgastosejecucion = $cgastosejecucion->cgastosejecucion;
                // $Tgastosejecuciondetalle->ccentrocosto = no se llena
                // $Tgastosejecuciondetalle->tipoactividad = no se llena
                $Tgastosejecuciondetalle->cproyecto = $proyecto->cproyecto;
                // $Tgastosejecuciondetalle->cpropuesta = no se llena
                // $Tgastosejecuciondetalle->carea = no se llena
                // $Tgastosejecuciondetalle->tipogasto = no se llena
                $Tgastosejecuciondetalle->descripcion = $prueba[$i]['descripcion'];
                $Tgastosejecuciondetalle->reembolsable = '00002';
                $Tgastosejecuciondetalle->tipodocumento = ($prueba[$i]['ivg'] == 0) ? '00002' : '00001'; //si no tiene igv (00002), si tiene igv (00001)
                $Tgastosejecuciondetalle->numerodocumento = $prueba[$i]['num_factura'];
                // $Tgastosejecuciondetalle->cpersona_cliente = 
                // $Tgastosejecuciondetalle->ruc_cliente = no se llena
                $Tgastosejecuciondetalle->razon_cliente = $prueba[$i]['proveedor'];
                $Tgastosejecuciondetalle->valorcambio = $valor_cambio2;
                $Tgastosejecuciondetalle->cmoneda = $prueba[$i]['moneda']; //buscar pk de la tabla tmonedas segun la moneda
                $Tgastosejecuciondetalle->cantidad = ($prueba[$i]['laboratorio_cantidad'] == 0) ? 1 : str_replace(',', '', $cantidadEntero);//si no tiene laboratorio_cantidad = 1 y si tiene = laboratorio_cantidad
               
                $Tgastosejecuciondetalle->preciounitario =$preciounitario;//si tiene laboratorio_pu = laboratorio_pu sino tiene laboratorio_pu = monto_igv
                $Tgastosejecuciondetalle->subtotal = $subtotal;

                $Tgastosejecuciondetalle->impuesto = $ivg;
                $Tgastosejecuciondetalle->total = $total;
                // $Tgastosejecuciondetalle->cpersona_empleado =
                // $Tgastosejecuciondetalle->ccondicionoperativa =
                $Tgastosejecuciondetalle->fdocumento = $prueba[$i]['fecha_factura'];
                $Tgastosejecuciondetalle->fregistro = $prueba[$i]['fecha_rendicion'];
                $Tgastosejecuciondetalle->cconcepto = $concepto[0]['cconcepto']; //buscar el pk en la tabla tconceptogastos segun descripcion si no exite colocar segun estado 00001
                $Tgastosejecuciondetalle->cconcepto_parent = $concepto[0]['cconcepto_parent']; //concepto_parent de $Tgastosejecuciondetalle->cconcepto
                //dd($prueba[$i]['otro_impuesto']);
                $Tgastosejecuciondetalle->otroimpuesto = str_replace(',', '.', $prueba[$i]['otro_impuesto']);
                $totalanteriorguardado = $Tgastosejecuciondetalle->total;
                $totalanteriorguardadodolares = $total_dolares;

                $Tgastosejecuciondetalle->save();

                $sumogastocab = Tgastosejecucion::where('numerorendicion','=',str_replace('10 - ', '', $prueba[$i]['numero_completo']))->first();
                $sumogastocab->total = $sumogastocab->total + $totalanteriorguardado;
                $sumogastocab->totaldolares = $sumogastocab->totaldolares + $totalanteriorguardadodolares;
                //dd($totalanteriorguardado);
                $sumogastocab->update();
            } else {
                 return false;
            }
            
        }
        return 'Finalice la migracion';
    }    

    public function MigracionHomologaData(Request $request)
    {


    }


    public function MigracionHomologaDataIndex()
    {
        //$descripcion_concepto=$this->elimina_acentos('éxc++\'"');
        //dd($descripcion_concepto);

        $proyectos_todos=DB::table('xx_migracion')->select('proyectoid')->orderby('proyectoid','asc')->distinct()->get();
        //dd($proyectos_todos);


        //$prueba=DB::table('xx_migracion')->get();
       // dd($prueba);

        foreach ($proyectos_todos as $pry) {

            DB::beginTransaction();
            $totalprecio = 0;
            $totalpreciodolares = 0;
            $contar=0;
            $prueba = XX_Migracion::where('proyectoid','=',$pry->proyectoid)->get();

            for ($i=0; $i < count($prueba); $i++) { 
                $contar++;
                $proyecto = Tproyecto::where('codigo','=',$prueba[$i]['proyectoid'])->first();

                $codigo = $prueba[$i]['proyectoid'];
                // valido laboratorio
                if ($prueba[$i]['laboratorio_cantidad'] == '') {
                    $prueba[$i]['laboratorio_cantidad'] = 1; 
                } 
                //valido precio unitario
                if ($prueba[$i]['laboratorio_pu'] == '') {
                    $prueba[$i]['laboratorio_pu'] = $prueba[$i]['monto_igv']; 
                } 
                //valido  IVG
                if ($prueba[$i]['ivg'] == '') {
                    $prueba[$i]['ivg'] = 0; 
                }
                //valido oTROS Impuestos
                if ($prueba[$i]['otro_impuesto'] == '') {
                    $prueba[$i]['otro_impuesto'] = 0; 
                }

                //$proyecto = Tproyecto::where('codigo','=',$codigo)->get();   
                //dd($prueba[$i]['monto_igv']);
                $laboratorio = $prueba[$i]['laboratorio_cantidad'];
                $labopusincoma = $prueba[$i]['laboratorio_pu'];
                $laboratorio_pu = $labopusincoma;


                //dd($laboratorio_pu);
                $ivg = $prueba[$i]['ivg'];
                $otro_impuesto = $prueba[$i]['otro_impuesto'];
                //dd($prueba[$i]['otro_impuesto']);
                //$cantidad = str_replace(',', '.', $laboratorio);
                $cantidad = $laboratorio;
                $cantidadEntero = $prueba[$i]['laboratorio_cantidad'];
                //dd($cantidadEntero);
                $preciounitario = $laboratorio_pu;
                //$preciounitario2 = number_format($preciounitario, 2, '.', ',');
                //dd($preciounitario2);
                $subtotal = floatval($cantidad) * floatval($preciounitario);
                //dd($subtotal);
                $impuesto = $ivg;
                $impuesto2 = $impuesto;
                
                
                $montoivgreplase = $prueba[$i]['monto_igv'];
                $supantomonto_igv = $montoivgreplase;
                $monto_igv =  $prueba[$i]['monto_igv'];

                $total = $subtotal + $impuesto2 + $otro_impuesto;
               
                $valor_cambio = $prueba[$i]['venta'];
                $valor_cambio2 = $valor_cambio;
                //dd($valor_cambio2);
                $total_soles1 = ($prueba[$i]['moneda']=='S/.') ? $total : $total*$valor_cambio2;
                $total_soles = $total_soles1;

                //dd($total_soles);
                $total_dolares1 = ($prueba[$i]['moneda']=='USD') ? $total : $total/$valor_cambio2;  
                $total_dolares = $total_dolares1;


                $descripcion_concepto=$this->elimina_acentos($prueba[$i]['nombre_gasto']);

                //dd($descripcion_concepto);

                $concepto = Tconceptogasto::where(DB::raw("lower(translate(descripcion,'áéíóúÁÉÍÓÚäëïöüÄËÏÖÜÑñ','aeiouAEIOUaeiouAEIOUNn'))"),'=',$descripcion_concepto)->where('estado','=', '00001')->first();



               
                //dd($concepto);


                

                if (!$concepto) {

                    dd($prueba[$i]['nombre_gasto'],$descripcion_concepto);

                }

                $tgastosejecucion = Tgastosejecucion::where('numerorendicion','=',str_replace('10 - ', '', $prueba[$i]['numero_completo']))->first(); 

                if (!$tgastosejecucion) {
                  
                    $tgastosejecucion = new Tgastosejecucion();
                    $tgastosejecucion->numerorendicion =  str_replace('10 - ', '', $prueba[$i]['numero_completo']);
                    $tgastosejecucion->descripcion = $prueba[$i]['nombre_rendicion'];
                    //$tgastosejecucion->valorcambio =  no se llena
                    $tgastosejecucion->total = $total_soles;
                    $tgastosejecucion->totaldolares = $total_dolares;
                    $tgastosejecucion->version = 'v1';
                    $tgastosejecucion->save();
                }

                $Tgastosejecuciondetalle = new Tgastosejecuciondetalle();
                    //$Tgastosejecuciondetalle->cgastosejecuciondet = correlativo - PK
                    $Tgastosejecuciondetalle->cgastosejecucion = $tgastosejecucion->cgastosejecucion;
                    // $Tgastosejecuciondetalle->ccentrocosto = no se llena
                    // $Tgastosejecuciondetalle->tipoactividad = no se llena
                    $Tgastosejecuciondetalle->cproyecto = $proyecto->cproyecto;
                    // $Tgastosejecuciondetalle->cpropuesta = no se llena
                    // $Tgastosejecuciondetalle->carea = no se llena
                    // $Tgastosejecuciondetalle->tipogasto = no se llena
                    $Tgastosejecuciondetalle->descripcion = $prueba[$i]['descripcion'];
                    $Tgastosejecuciondetalle->reembolsable = '00002';
                    $Tgastosejecuciondetalle->tipodocumento = ($prueba[$i]['ivg'] == 0) ? '00002' : '00001'; //si no tiene igv (00002), si tiene igv (00001)
                    $Tgastosejecuciondetalle->numerodocumento = $prueba[$i]['num_factura'];
                    // $Tgastosejecuciondetalle->cpersona_cliente = 
                    // $Tgastosejecuciondetalle->ruc_cliente = no se llena
                    $Tgastosejecuciondetalle->razon_cliente = $prueba[$i]['proveedor'];
                    $Tgastosejecuciondetalle->valorcambio = $valor_cambio2;
                    $Tgastosejecuciondetalle->cmoneda = $prueba[$i]['moneda']; //buscar pk de la tabla tmonedas segun la moneda
                    $Tgastosejecuciondetalle->cantidad = ($prueba[$i]['laboratorio_cantidad'] == 0) ? 1 : str_replace(',', '', $cantidadEntero);//si no tiene laboratorio_cantidad = 1 y si tiene = laboratorio_cantidad
                   
                    $Tgastosejecuciondetalle->preciounitario =$preciounitario;//si tiene laboratorio_pu = laboratorio_pu sino tiene laboratorio_pu = monto_igv
                    $Tgastosejecuciondetalle->subtotal = $subtotal;

                    $Tgastosejecuciondetalle->impuesto = $ivg;
                    $Tgastosejecuciondetalle->total = $total;

                    $persona_asignado=DB::table('tusuarios')
                    ->where('nombre','ilike','%'.$prueba[$i]['asignado'].'%')
                    ->first();

                    if (!$persona_asignado) {
                        
                        dd($prueba[$i]['asignado']);
                    }
                    // dd($persona_asignado);
                    $Tgastosejecuciondetalle->cpersona_empleado = $persona_asignado? $persona_asignado->cpersona:null;
                    // $Tgastosejecuciondetalle->ccondicionoperativa =
                    $Tgastosejecuciondetalle->fdocumento = $prueba[$i]['fecha_factura'];
                    $Tgastosejecuciondetalle->fregistro = $prueba[$i]['fecha_rendicion'];
                    $Tgastosejecuciondetalle->cconcepto = $concepto->cconcepto; //buscar el pk en la tabla tconceptogastos segun descripcion si no exite colocar segun estado 00001
                    $Tgastosejecuciondetalle->cconcepto_parent = $concepto->cconcepto_parent; //concepto_parent de $Tgastosejecuciondetalle->cconcepto
                    //dd($prueba[$i]['otro_impuesto']);
                    $Tgastosejecuciondetalle->otroimpuesto = str_replace(',', '.', $prueba[$i]['otro_impuesto']);
                    $totalanteriorguardado = $Tgastosejecuciondetalle->total;
                    $totalanteriorguardadodolares = $total_dolares;

                    $Tgastosejecuciondetalle->version = 'v1';

                    $Tgastosejecuciondetalle->save();



                    $tgastosejecucion->total = $tgastosejecucion->total + $totalanteriorguardado;
                    $tgastosejecucion->totaldolares = $tgastosejecucion->totaldolares + $totalanteriorguardadodolares;
                    //dd($totalanteriorguardado);
                    $tgastosejecucion->update();
                


            }

                DB::commit();
        }



        return 'Finalice la migracion '.$prueba[0]['proyectoid'].'  Filas:'. $contar;

    }

    function elimina_acentos($text)
    {
        $text = htmlentities($text, ENT_NOQUOTES, 'UTF-8');
        $text = strtolower($text);
        $patron = array (
            // Espacios, puntos y comas por guion
            //'/[\., ]+/' => ' ',
 
            // Vocales
            '/\+/' => '+',
            '/&agrave;/' => 'a',
            '/&egrave;/' => 'e',
            '/&igrave;/' => 'i',
            '/&ograve;/' => 'o',
            '/&ugrave;/' => 'u',
 
            '/&aacute;/' => 'a',
            '/&eacute;/' => 'e',
            '/&iacute;/' => 'i',
            '/&oacute;/' => 'o',
            '/&uacute;/' => 'u',
 
            '/&acirc;/' => 'a',
            '/&ecirc;/' => 'e',
            '/&icirc;/' => 'i',
            '/&ocirc;/' => 'o',
            '/&ucirc;/' => 'u',
 
            '/&atilde;/' => 'a',
            '/&etilde;/' => 'e',
            '/&itilde;/' => 'i',
            '/&otilde;/' => 'o',
            '/&utilde;/' => 'u',
 
            '/&auml;/' => 'a',
            '/&euml;/' => 'e',
            '/&iuml;/' => 'i',
            '/&ouml;/' => 'o',
            '/&uuml;/' => 'u',
 
            // Otras letras y caracteres especiales
            '/&aring;/' => 'a',
            '/&ntilde;/' => 'n',
 
            // Agregar aqui mas caracteres si es necesario
 
        );
 
        $text = preg_replace(array_keys($patron),array_values($patron),$text);
    //dd($text,'dd');
        return $text;
    }

    function aHtml($cadena){



    }


    public function ejecucion2(Request $request)
    {

        $horas1=DB::table('tproyectoejecucion_1')->orderby('cproyectoejecucion','asc')->get();

        foreach ($horas1 as $h) {
            $hora=Tproyectoejecucion::where('cproyectoejecucion','=',$h->cproyectoejecucion)
            ->where('estado','=','3')
            ->where('ccondicionoperativa','=','RGP')
            ->first();
            if($hora){
                $hora->estado=$h->estado;
                $hora->ccondicionoperativa=$h->ccondicionoperativa;
                $hora->save();
            }
            else{
                //dd('no existe',$h);
            }


        }


    }


    public function ejecucion(Request $request)
    {

        $hora=Tproyectoejecucion::where('estado','=','3')
            ->where('ccondicionoperativa','=','RGP')
            ->where('cproyectoejecucion','>=','314193')
            ->get();

        $aflujo=[];
        foreach ($hora as $key => $h) {
            $flujo=DB::table('tflujoaprobacion')
            ->where('cproyectoejecucion','=',$h->cproyectoejecucion)
            ->get();

            if (!$flujo) {
                //array_push($aflujo, $h->cproyectoejecucion);

                $horas_encontradas=Tproyectoejecucion::where('cproyectoejecucion','=',$h->cproyectoejecucion)
                    ->where('estado','=','3')
                    ->where('ccondicionoperativa','=','RGP')
                    ->first();

                    $horas_encontradas->estado='2';
                    $horas_encontradas->ccondicionoperativa='EJE';
                    //dd($horas_encontradas);
                    $horas_encontradas->save();


            }

        }
        //dd($aflujo);
    

    }

    public function asignarCC(){
        // dd(123);
 
        DB::beginTransaction();
 
        $cc = DB::table('tpersonadatosempleado as tpde')
        ->join('tpersona as tp','tp.cpersona','=','tpde.cpersona')
        ->get();
        foreach ($cc as $key => $value) {
            $per = DB::table('erp_centrocostosub_usuario')->where('dni','=',$value->identificacion)->first();


            if ($per) {

                $sub_centro= DB::table('erp_centrocostosub')->where('codigo','=',$per->codigo)->first();

                if ($sub_centro) {
                   
                    $perc = Tpersonadatosempleado::where('cpersona','=',$value->cpersona)->first();
                    $perc->centrocostosub = $sub_centro->id;
                    $perc->save();
                }


            }
        }
// dd($per,$value->identificacion);
 
        DB::commit();
 
}
    public function conectarPropuestaActividades(){

        $propuestaactividades = DB::table('tpropuestaactividades as tpa')
            ->leftjoin('tpropuesta as prop','tpa.cpropuesta','=','prop.cpropuesta')
            ->where('tpa.cpropuesta','=',236)
            ->select('tpa.*','prop.cproyecto')
            ->get();

        foreach ($propuestaactividades as $pa) {
            $proyActividad= Tproyectoactividade::where('cproyecto','=',$pa->cproyecto)
            ->where('codigoactvidad','=',$pa->codigoactividad)
            ->first();

            if ($proyActividad) {
                $proyActividad->cpropuestaactividades=$pa->cpropuestaactividades;
                $proyActividad->save();
            }

        }

    }

    public function copiarHonorariosDePropuesta(){


        $propuestaactividades = DB::table('tpropuestaactividades as tpa')
            ->join('tpropuestahonorarios as tph','tpa.cpropuestaactividades','=','tph.cpropuestaactividades')
            ->join('testructuraparticipante as tep','tph.cestructurapropuesta','=','tep.cestructurapropuesta')
            ->join('tproyectoestructuraparticipantes as te','te.cestructurapropuesta','=','tep.cestructurapropuesta')
            ->select('tpa.*','tph.*','te.cestructuraproyecto')
            ->where('tpa.cpropuesta','=',236)
            // ->orderby('tpa.cpropuestaactividades','asc')
            ->orderby('tep.cestructurapropuesta','asc')
            ->get();
        foreach ($propuestaactividades as $pac) {

            $proyActividad= DB::table('tproyectoactividades as tpa')
                ->join('tproyectoestructuraparticipantes as tp','tpa.cproyecto','=','tp.cproyecto')
                //->where('tp.cestructurapropuesta','=',$pac->cestructurapropuesta)
                ->where('tpa.cpropuestaactividades','=',$pac->cpropuestaactividades)
                ->select('tpa.*')
                ->orderby('tpa.cproyectoactividades','asc')
                ->distinct()
                ->get();


                foreach ($proyActividad as $pa) {

                    $phono=Tproyectohonorario::where('cproyectoactividades','=',$pa->cproyectoactividades)
                    ->where('cestructuraproyecto','=',$pac->cestructuraproyecto)
                    ->first();

                    if (!$phono) {
                       $phono=new Tproyectohonorario();
                       $phono->horas= 0;
                    }

                    $phono->cproyectoactividades = $pa->cproyectoactividades;
                    $phono->horas= $pac->horas;
                    $phono->xmlareadetalle = $pac->xmlareadetalle;
                    $phono->cdisciplina = $pac->cdisciplina;

                    if(!empty( $pac->cpropuestapaquete)){
                        $propaq = Tpropuestapaquete::where('cpropuestapaquete','=',$pac->cpropuestapaquete)->first();
                        if($propaq){
                            $prosub = Tproyectosub::where('cproyecto','=',$pa->cproyecto)
                            ->where('codigosub','=',$propaq->codigopaquete)
                            ->first();
                            if($prosub){
                                $phono->cproyectosub = $prosub->cproyectosub;
                            }
                        }
                    }

                    $proest = Tproyectoestructuraparticipante::where('cestructurapropuesta','=',$pac->cestructurapropuesta)->first();

                    if($proest){
                      $phono->cestructuraproyecto= $proest->cestructuraproyecto;  
                    }
                    //dd($phono,$pac);
                    $phono->save();


                   

                }

        }


    }

    public function actualizarCategoriaEjecucion(){

        DB::beginTransaction();

        $horasPry = DB::table('tproyectoejecucion')->orderby('cproyectoejecucion','asc')->get();

        foreach ($horasPry as $key => $hp) {

            $persona = DB::table('tpersonadatosempleado')
                ->where('cpersona','=',$hp->cpersona_ejecuta)
                ->first();

            $categoria = $persona->ccategoriaprofesional;

            if (!is_null($hp->cproyecto)) {
                $proyectopersona = DB::table('tproyectopersona')
                    ->where('cpersona','=',$hp->cpersona_ejecuta)
                    ->where('cproyecto','=',$hp->cproyecto)
                    ->first();
                if ($proyectopersona) {

                    if (!is_null($proyectopersona->ccategoriaprofesional) || strlen($proyectopersona->ccategoriaprofesional)>0) {
                            $categoria = $proyectopersona->ccategoriaprofesional;
                    }
                }
            }

            Tproyectoejecucion::where('cproyectoejecucion')->update(['ccategoriaprofesional' => $categoria]);
        }

        DB::commit();

        dd('fin de actualización');

    }

    public function permisosLideres(){



       DB::beginTransaction();

        $cargos=DB::table('tcargos')
            ->whereNotNull('cargoparentdespliegue')
            ->distinct()
            ->lists('cargoparentdespliegue');
           
        //$tusuarios=DB::table('tpersonadatosempleado as te')->join('tpersona as tp','te.cpersona','=','tp.cpersona')->whereIn('te.ccargo',$cargos)->select('tp.abreviatura','te.cpersona')->get();
        //dd($tusuarios);


        $tusuarios = DB::table('tproyectopersona')->select('cpersona')->where('eslider','=','1')->distinct()->get();
        // dd($tusuarios);

        foreach ($tusuarios as $u) {

            $tmp=Tmenupersona::where('cpersona','=',$u->cpersona)
            ->where('menuid','=','0336')
            ->first();

            if(!$tmp){

                $tmenupersona= new Tmenupersona();
                $tmenupersona->menuid='0336';
                $tmenupersona->cpersona=$u->cpersona;
                $tmenupersona->permiso='01';
                $tmenupersona->save();            
            }

            $tmp1=Tmenupersona::where('cpersona','=',$u->cpersona)
            ->where('menuid','=','0337')
            ->first();        

            if(!$tmp1){

                $tmenupersona1= new Tmenupersona();
                $tmenupersona1->menuid='0337';
                $tmenupersona1->cpersona=$u->cpersona;
                $tmenupersona1->permiso='01';
                $tmenupersona1->save();
                
            }    

            $tmp2=Tmenupersona::where('cpersona','=',$u->cpersona)
            ->where('menuid','=','0373')
            ->first();        

            if(!$tmp2){

                $tmenupersona1= new Tmenupersona();
                $tmenupersona1->menuid='0373';
                $tmenupersona1->cpersona=$u->cpersona;
                $tmenupersona1->permiso='01';
                $tmenupersona1->save();
                
            }    
            
         }

          DB::commit();

    }

    public function registrarPropuestaDeProyecto(){

        DB::beginTransaction();

            $proyectoEnPropuesta = DB::table('tpropuesta')
                ->distinct()
                ->orderby('cproyecto')
                ->lists('cproyecto');
                //->distinct();
               
            $proyectosSinPropuesta = DB::table('tproyecto')
                //->whereNotIn('cproyecto',$proyectoEnPropuesta)
                //->get();
                ->orderby('cproyecto')
                ->lists('cproyecto');
                //->count();
            $diferencia = array_diff($proyectosSinPropuesta,$proyectoEnPropuesta);
        
            //dd($proyectoEnPropuesta,$proyectosSinPropuesta,$diferencia);

        foreach ($diferencia  as $key => $cpry) {
           

            $proyecto = Tproyecto::where('cproyecto','=',$cpry)->first();

            $propuesta = new Tpropuestum;
            //dd($proyecto,$propuesta);
            $propuesta->cproyecto = $proyecto->cproyecto;
            $propuesta->cpersona = $proyecto->cpersonacliente;
            $propuesta->cunidadminera = $proyecto->cunidadminera;
            $propuesta->ccodigo = $proyecto->codigo.'_p';
            $propuesta->nombre = $proyecto->nombre;
            $propuesta->descripcion = $proyecto->descripcion;
            $propuesta->cservicio = $proyecto->cservicio;
            $propuesta->cpersona_gteproyecto = $proyecto->cpersona_gerente;
            $propuesta->cestadopropuesta = '007';
            $propuesta->ccondicionoperativa = 'ELA';
            $propuesta->cmoneda = $proyecto->cmoneda;
            $propuesta->cmedioentrega = $proyecto->cmedioentrega;
            $propuesta->fpropuesta = Carbon::now();
            $propuesta->save();
            //dd($proyecto,$propuesta);

        }
        DB::commit();

        dd('Fin migración :)');

    }

    public function actualizarCodigoPropuesta(){

        $propuesta = Tpropuestum::whereIn('ccodigo',['17','18'])->get();

        foreach ($propuesta as $key => $prop) {

            $proyecto = Tproyecto::where('cproyecto','=',$prop->cproyecto)->first();
            if($proyecto){
                
                $propuesta_actualizar = Tpropuestum::where('cpropuesta','=',$prop->cpropuesta)->first();
                $propuesta_actualizar->ccodigo = $proyecto->codigo.'_c';
                $propuesta_actualizar->save();
            }


        }

        dd('Actualización terminada');

    }



}