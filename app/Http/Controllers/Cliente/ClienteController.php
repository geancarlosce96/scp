<?php

namespace App\Http\Controllers\Cliente;

use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tpai;
use App\Models\Tubigeo;
use App\Models\Tunidadmineraestado;
use App\Models\Tunidadminera;
use App\Models\Tunidadmineracontacto;
use App\Models\Ttiposcontacto;
use App\Models\Tcontactocargo;
use DB;
use Auth;
use Redirect;
use Response;
use PDF;


class ClienteController extends Controller
{
    //
    public function buscar(Request $request){
        return Datatables::queryBuilder(
            DB::table('tpersona')
            ->join('tpersonajuridicainformacionbasica as tjur','tpersona.cpersona','=','tjur.cpersona')
            ->join('tunidadminera as tuni','tpersona.cpersona','=','tuni.cpersona')
            ->select('tjur.codigosig as codigo','tuni.codigo as codigosig','tpersona.nombre as nombre','tuni.nombre as uminera',DB::raw('CONCAT(\'row_\',tpersona.cpersona,\'_\',tuni.cunidadminera)   as "DT_RowId"'))
            ->where('tpersona.ctipopersona','=','JUR')
            ->where('tjur.escliente','=','1')
            )->make(true); 
    }

    public function listarProveedor(){
        return Datatables::queryBuilder(
            DB::table('tpersona')
            ->leftJoin('tpersonajuridicainformacionbasica as tjur','tpersona.cpersona','=','tjur.cpersona')
            ->select('tpersona.cpersona','tjur.codigosig as codigo','tjur.razonsocial','tpersona.identificacion','tpersona.nombre as nombre',DB::raw('CONCAT(\'rowprov_\',tpersona.cpersona)   as "DT_RowId"'))
            ->where('tpersona.ctipopersona','=','JUR')
            ->where('tjur.esproveedor','=','1')
            )->make(true); 
    }

    public function buscarContactos($idProyecto){
        $cproyecto = $idProyecto;
        return Datatables::queryBuilder(
            DB::table('tproyecto as pry')
        ->join('tunidadmineracontactos as umc','pry.cunidadminera','=','umc.cunidadminera')
        ->leftJoin('tcontactocargo as con','umc.ccontactocargo','=','con.ccontactocargo')
        ->select(DB::raw('CONCAT(\'rowcon_\',umc.cunidadmineracontacto) as "DT_RowId"'),'umc.apaterno','umc.amaterno','umc.nombres','con.descripcion')
        ->where('pry.cproyecto','=',$cproyecto)
        )->make(true);


    }

    public function AddContactosCliente(Request $request){
        // dd($request->all());
             DB::beginTransaction();
            $ccontact_c = $request->input('cunidadminera_c');
            $cunidadminera = $request->input('cuminera_con');
            if(strlen($ccontact_c)<=0){
                $contact = new Tunidadmineracontacto();
                $contact->cunidadminera=$cunidadminera;
            }else{
                $contact = Tunidadmineracontacto::where('cunidadmineracontacto','=',$ccontact_c)
                ->first();
            }
            //$ccontact->cunidadminera= $request->input('umincontacto_um');           
            
            $contact->apaterno= $request->input('apaterno');
            $contact->amaterno= $request->input('amaterno');
            $contact->nombres= $request->input('nombres');
            $contact->email= $request->input('email');
            $contact->telefono= $request->input('telefono');
            $contact->cel1= $request->input('cel1');
            $contact->cel2= $request->input('cel2');
            $contact->anexo= $request->input('anexo');
            $contact->ctipocontacto= $request->input('ctipocontacto');
            $contact->ccontactocargo= $request->input('ccontactocargo');
            $contact->direccion= $request->input('direccion');
            $contact->save();
            DB::commit();
            return Redirect::route('verUmineraContacto',$cunidadminera);
    }

    public function verUmineraContacto($cunidadminera){
        
        //Unidad Minera Contacto 
        $tipocontacto = Ttiposcontacto::lists('descripcion','ctipocontacto')
        ->all();
        $contactocargo = Tcontactocargo::lists('descripcion','ccontactocargo')
        ->all();
        /* Fin Unidad Minera Contacto */

    /* Unidad Minera Contacto */
         $tunidadmineracontactos = DB::table('tunidadmineracontactos as umc')
        ->leftJoin('tunidadminera as t','umc.cunidadminera','=','t.cunidadminera')
        ->leftJoin('ttiposcontacto as tc','umc.ctipocontacto','=','tc.ctipocontacto')  
        ->leftJoin('tcontactocargo as tco','umc.ccontactocargo','=','tco.ccontactocargo') 
        ->where('umc.cunidadminera','=',$cunidadminera)
        ->orderBy('nombres','ASC')
        ->select('umc.*','tco.descripcion as des_cargo','t.nombre as uminera','tc.descripcion as des_tipo')
        ->get();
    /* Fin Unidad Mineras */

    return view('partials.modalContactoUnidadMinera',compact('tunidadmineracontactos','nombre_um'));
    }
        
     public function eliminarUmineraContacto($idcontact){
        //dd($idcontact);
        DB::beginTransaction();
        $contact = Tunidadmineracontacto::where('cunidadmineracontacto','=',$idcontact)->first();
       
        $cunidadminera = $contact->cunidadminera;
        try
        {
            $contact->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('verUmineraContacto',$cunidadminera);
    }

    public function getContacto(Request $request,$idContacto){
        $res = array();
        $tcontacto = DB::table('tunidadmineracontactos as umcon')
        ->where('umcon.cunidadmineracontacto','=',$idContacto)
        ->first();
        if($tcontacto){
            $r['unidadmineracontacto'] = $tcontacto->cunidadmineracontacto;
            $r['apaterno']= $tcontacto->apaterno;
            $r['amaterno']= $tcontacto->amaterno;
            $r['nombres']= $tcontacto->nombres;
            $res[count($res)]=$r;
        }
        return response()->json($res);
    }

    public function listadoClientes(){

        return view('cliente/listadoClientes');

    }
    public function listarClientes(){
        return Datatables::queryBuilder(DB::table('tpersona as tper')
            ->join('tpersonajuridicainformacionbasica as tjur','tper.cpersona','=','tjur.cpersona')
            ->leftJoin('tunidadminera as tu','tper.cpersona','=','tu.cpersona')
            ->where('tjur.escliente','=','1')
            ->select('tper.identificacion','tper.nombre as cliente','tu.nombre as uminera','tper.cpersona'
            ,DB::raw('CONCAT(\'row_\',tper.cpersona)  as "DT_RowId"'))
            )->make(true);        
    }
    public function registroClientes(){
        $tpais = Tpai::lists('descripcion','cpais')->all();
        $tpais = [''=>''] + $tpais;

        $tpais_um = Tpai::lists('descripcion','cpais')->all();
        $tpais_um = [''=>''] + $tpais;

        $umineraestado = Tunidadmineraestado::lists('descripcion','cumineraestado')
        ->all();
        $umineraestado = [''=>''] + $umineraestado;

        //dd($tpais_um);
        return view('cliente/registroClientes',compact('tpais','tpais_um','umineraestado'));
    }
    public function editarCliente(Request $request,$idCliente){
        $persona = DB::table('tpersona')
        ->where('cpersona','=',$idCliente)
        ->first();
        $personajur = DB::table('tpersonajuridicainformacionbasica')
        ->where('cpersona','=',$idCliente)
        ->first();

        $personadir = DB::table('tpersonadirecciones')
        ->where('cpersona','=',$idCliente)
        ->where('ctipodireccion','=','DO')
        ->first();
        $cubigeo="000000";
        $cpais=0;
        $cdpto="000000";
        $cprov="000000";
        if($personadir){
            $cpais = $personadir->cpais;
            $cubigeo = $personadir->cubigeo;
            if(strlen($cubigeo)==6){
                $cdpto=substr($cubigeo,0,2)."0000";
                $cprov=substr($cubigeo,0,4)."00";

            }
        }
        
        /* Unidad Minera */
       /* $tunidadminera = DB::table('tunidadminera as um')
        ->leftJoin('tunidadmineraestado as ume','um.cumineraestado','=','ume.cumineraestado')
        ->leftJoin('tubigeo as ubi',function($join){
            $join->on('um.cpais','=','ubi.cpais')->on('um.cubigeo','=','ubi.cubigeo');

        })
        ->where('cpersona','=',$idCliente)
        ->orderBy('nombre','ASC')
        ->select('um.*','ubi.descripcion as ubigeo','ume.descripcion as destado')
        ->get();*/

        $unidadminera = DB::table('tunidadminera as um')
        ->leftJoin('tunidadmineraestado as ume','um.cumineraestado','=','ume.cumineraestado')       
        ->where('cpersona','=',$idCliente)
        ->orderBy('nombre','ASC')
        ->select('um.*','ume.descripcion as destado')
        ->get();
        $tunidadminera=array();

        foreach ($unidadminera as $um) {
           
            $tum['cunidadminera'] = $um->cunidadminera;
            $tum['cpersona'] = $um->cpersona;

            $tpais = Tpai::where('cpais',$um->cpais)->first();
            $pais=null;

            if($tpais){    
                $pais=$tpais->descripcion;            
            }
            $tum['pais'] = $pais;
           

            if(strlen($um->cubigeo)>0 && $um->cpais=='PER' ){           
                $dpto=substr($um->cubigeo,0,2).'0000';
                $prov=substr($um->cubigeo,0,4).'00';

                $tdpto = Tubigeo::where('cpais','=',$um->cpais)
                ->where('cubigeo','=',$dpto)
                ->first();

                if($tdpto){
                    $tum['departamento'] = ' - '.$tdpto->descripcion;
                }

                $tprov= Tubigeo::where('cpais','=',$um->cpais)
                ->where('cubigeo','=',$prov)
                ->first();

                if($tprov){
                    $tum['provincia'] = ' - '.$tprov->descripcion;
                }

                $tdis= Tubigeo::where('cpais','=',$um->cpais)
                ->where('cubigeo','=',$um->cubigeo)
                ->first();

                if($tdis){
                    $tum['distrito'] =' - '.$tdis->descripcion;
                }

            }

            else{

                $tum['departamento'] ='';
                $tum['provincia'] = '';
                $tum['distrito'] = '';

            }

            $tum['ubigeo'] =  $tum['pais'].$tum['departamento'].$tum['provincia'].$tum['distrito'];            
            $tum['direccion'] = $um->direccion;
            $tum['telefono'] = $um->telefono;
            $tum['nombre'] = $um->nombre;
            $tum['codigo'] = $um->codigo;
            $tum['destado'] = $um->destado;

           

            $tunidadminera[count($tunidadminera)] = $tum;
        }

        //dd($tunidadminera);
        /* Fin unidad mineras*/

        //ubicacion de Unidad Minera
        

        $tpais_um = Tpai::lists('descripcion','cpais')->all();
        $tpais_um = [''=>''] + $tpais_um;    

        $tdpto_um = Tubigeo::where(DB::raw('substr(cubigeo,3,4)'),'=','0000')
        ->lists('descripcion','cubigeo')->all();
        $tdpto_um = [''=>''] + $tdpto_um;

        $tprov_um = Tubigeo::where(DB::raw('substr(cubigeo,5,2)'),'=','00')
        ->where(DB::raw('substr(cubigeo,3,2)'),'!=','00')        
        ->lists('descripcion','cubigeo')->all();
        $tprov_um = [''=>''] + $tprov_um;

        $tdis_um = Tubigeo::where(DB::raw('substr(cubigeo,5,2)'),'!=','00')        
        ->lists('descripcion','cubigeo')->all();                
        $tdis_um = [''=>''] + $tdis_um;    

        /* Fin ubicación de Unidad minera*/

        /* Unidad Minera Estado*/
        $umineraestado = Tunidadmineraestado::lists('descripcion','cumineraestado')
        ->all();
        /* Fin Unidad Minera Estado*/


        

        /* Unidad Minera Contactos */
        $tcargo= DB::table('tcontactocargo')
        ->lists('descripcion','ccontactocargo');
        $tcargo = [''=>''] + $tcargo;

        $ttipo= DB::table('ttiposcontacto')
        ->lists('descripcion','ctipocontacto');
        $ttipo = [''=>''] + $ttipo;

        /* Cargar los datos de contactos */
        $umin=DB::table('tunidadminera as um')
        ->leftJoin('tunidadmineracontactos as umc','um.cunidadminera','=','umc.cunidadminera')
        //->select('um.nombre')
        ->where('um.cpersona','=',$idCliente)
        ->first();

        $tuminera=DB::table('tunidadminera as t')
        ->leftJoin('tpersona as p','t.cpersona','=','p.cpersona')
        ->where('t.cpersona','=',$idCliente)
        ->first();
        $codumin=null;
        if($umin){
            $codumin=$tuminera->cunidadminera;
        }

        $tunidadmineracontactos = DB::table('tunidadmineracontactos as umc')
        ->leftJoin('tunidadminera as t','umc.cunidadminera','=','t.cunidadminera')
        ->leftJoin('ttiposcontacto as tc','umc.ctipocontacto','=','tc.ctipocontacto')  
        ->leftJoin('tcontactocargo as tco','umc.ccontactocargo','=','tco.ccontactocargo')      
        ->where('umc.cunidadminera','=',$codumin)
        ->orderBy('nombres','ASC')
        ->select('umc.*','t.nombre as uminera','tco.descripcion as des_cargo','tc.descripcion as des_tipo')
        ->get();

        //busco la compañia con abreviatura y creao abrevia en el array de peronajur
        $cpersona = Tpersona::where('cpersona', '=', $personajur->cpersona)->first();
        $personajur->abrevia = $cpersona->abreviatura;
        //dd($tpais);

        //Coloco el pais por que si se coloca arriva se pisa con la de la unidad minera
        $tpais = Tpai::lists('descripcion','cpais')->all();
        $tpais = [''=>''] + $tpais;        
        $tdpto = Tubigeo::where('cpais','=',$cpais)
        ->where(DB::raw('substr(cubigeo,3,4)'),'=','0000')
        ->lists('descripcion','cubigeo')->all();

        $tdpto = [''=>''] + $tdpto;
        $tprov = Tubigeo::where('cpais','=',$cpais)
        ->where(DB::raw('substr(cubigeo,5,2)'),'=','00')
        ->where(DB::raw('substr(cubigeo,1,2)'),'=',substr($cdpto,0,2))
        ->where(DB::raw('substr(cubigeo,3,2)'),'!=','00')        
        ->lists('descripcion','cubigeo')->all();
        $tprov = [''=>''] + $tprov;
        $tdis = Tubigeo::where('cpais','=',$cpais)
        ->where(DB::raw('substr(cubigeo,1,4)'),'=',substr($cprov,0,4))
        ->where(DB::raw('substr(cubigeo,5,2)'),'!=','00')        
        ->lists('descripcion','cubigeo')->all();                
        $tdis = [''=>''] + $tdis;
        //dd($tpais);


        return view('cliente/registroClientes',compact('tpais','persona','personajur','personadir','tdpto','tprov','tdis','cubigeo','cdpto','cprov','tpais_um','tdpto_um','tprov_um','tdis_um','umineraestado','tunidadminera','tcargo','ttipo','tunidadmineracontactos','umin'));
    }
    public function grabarCliente(Request $request){

        DB::beginTransaction();
        $idCliente = $request->input('cpersona');
        if(strlen($idCliente)<=0){
            $persona = new Tpersona();            
            $persona->ctipopersona='JUR';
            $persona->ctipoidentificacion ='1';
            $persona->save();
            $idCliente = $persona->cpersona;
            $cnt = 0;
            $personadir = new Tpersonadireccione();
            $personadir->numerodireccion=++$cnt;
            $personadir->ctipodireccion='DO';
            $personadir->cpersona = $idCliente;
            $personajur = new Tpersonajuridicainformacionbasica();
            $personajur->cpersona = $idCliente;
            $personajur->escliente='1';

        }else{
            
            $persona = Tpersona::where('cpersona','=',$idCliente)->first();
            $personadir = Tpersonadireccione::where('cpersona','=',$idCliente)
            ->where('ctipodireccion','=','DO')
            ->first();
            if(!$personadir){
                $cnt = Tpersonadireccione::where('cpersona','=',$idCliente)->count();
                $personadir = new Tpersonadireccione();
                $personadir->numerodireccion=++$cnt;
                $personadir->ctipodireccion='DO';
                $personadir->cpersona = $idCliente;
                
            }
            $personajur = Tpersonajuridicainformacionbasica::where('cpersona','=',$idCliente)->first();
            if(!$personajur){
                $personajur = new Tpersonajuridicainformacionbasica();
                $personajur->cpersona = $idCliente;    
            }
            
        }
        $persona->identificacion=$request->input('identificacion');
        $persona->nombre=$request->input('nombrecomercial');
        $persona->abreviatura=$request->input('abrevia');
        $persona->save();

        $personadir->cpais=$request->input("pais");
        if($request->input("dis") && $request->input("dis")!=''){
            $personadir->cubigeo = $request->input("dis");
        }
        $personadir->direccion = $request->input('direccion');
        $personadir->save();

        $personajur->razonsocial= $request->input('razonsocial');
        $personajur->nombrecomercial= $request->input('nombrecomercial');
        $personajur->razonsocial= $request->input('razonsocial');
        $personajur->tag= $request->input('tag');
        $personajur->web= $request->input('web');
        $personajur->save();
        DB::commit();

        if ($request->input('vista')=='invitacion') {
            return $persona->cpersona;
        }
        else{
            return Redirect::route('editarCliente',$idCliente);
        }
    }
    public function listDpto($idPais){
        $tdpto = Tubigeo::where('cpais','=',$idPais)
        ->where(DB::raw('substr(cubigeo,3,4)'),'=','0000')
        ->orderBy('descripcion','ASC')
        ->lists('descripcion','cubigeo')->all();
        
        //$tdpto = [''=>''] + $tdpto;
        return Response::json($tdpto);

    }
    public function listProv($idPais,$idDpto){
        $tprov = Tubigeo::where('cpais','=',$idPais)
        ->where(DB::raw('substr(cubigeo,5,2)'),'=','00')
        ->where(DB::raw('substr(cubigeo,3,2)'),'!=','00')
        ->where(DB::raw('substr(cubigeo,1,2)'),'=',substr($idDpto,0,2))
        ->orderBy('descripcion','ASC')
        ->lists('descripcion','cubigeo')->all();
        //$tprov = [''=>''] + $tprov;
        return Response::json($tprov);
    }
    public function listDis($idPais,$idProv){
        $tdis = Tubigeo::where('cpais','=',$idPais)
        ->where(DB::raw('substr(cubigeo,5,2)'),'!=','00')
        ->where(DB::raw('substr(cubigeo,1,4)'),'=',substr($idProv,0,4))
        ->orderBy('descripcion','ASC')
        ->lists('descripcion','cubigeo')->all(); 
        //$tdis = [''=>''] + $tdis;
        return Response::json($tdis);
    }  

    public function addUmineraCliente(Request $request){
         DB::beginTransaction();

         //dd($request);


        $tpais_um = Tpai::lists('descripcion','cpais')->all();
        $tpais_um = [''=>''] + $tpais_um; 

        $cuminera_u = $request->input('cuminera_u');
        $cpersona = $request->input('cpersona_u');

        if(strlen($cuminera_u)<=0){
            $uminera = new Tunidadminera();
            $uminera->cpersona=$cpersona;
        }else{
            $uminera = Tunidadminera::where('cunidadminera','=',$cuminera_u)
            ->first();
        }
        $uminera->cumineraestado = $request->input('umineraestado') ;
        $uminera->cpais= $request->input('pais_um');
        $uminera->cubigeo= $request->input('dis_um');
        $uminera->direccion= $request->input('direccion_um');
        $uminera->telefono= $request->input('telefono_um');
        $uminera->nombre= $request->input('nombre_um');
        $uminera->codigo= $request->input('codigo_um');
        $uminera->save();
        DB::commit();        
        return Redirect::route('verUmineraCliente',$cpersona);
    }  
    public function verUmineraCliente($cpersona){

        $tpais_um = Tpai::lists('descripcion','cpais')->all();
        $tpais_um = [''=>''] + $tpais_um; 

        /* Unidad Minera Estado*/
        $umineraestado = Tunidadmineraestado::lists('descripcion','cumineraestado')
        ->all();
        /* Fin Unidad Minera Estado*/

        /* Unidad Minera */
       /* $tunidadminera = DB::table('tunidadminera as um')
        ->leftJoin('tunidadmineraestado as ume','um.cumineraestado','=','ume.cumineraestado')
        ->leftJoin('tubigeo as ubi',function($join){
            $join->on('um.cpais','=','ubi.cpais')->on('um.cubigeo','=','ubi.cubigeo');

        })
        ->where('cpersona','=',$cpersona)
        ->orderBy('nombre','ASC')
        ->select('um.*','ubi.descripcion as ubigeo','ume.descripcion as destado')
        ->get();*/


        $unidadminera = DB::table('tunidadminera as um')
        ->leftJoin('tunidadmineraestado as ume','um.cumineraestado','=','ume.cumineraestado')       
        ->where('cpersona','=',$cpersona)
        ->orderBy('nombre','ASC')
        ->select('um.*','ume.descripcion as destado')
        ->get();
        $tunidadminera=array();

        foreach ($unidadminera as $um) {
           
            $tum['cunidadminera'] = $um->cunidadminera;
            $tum['cpersona'] = $um->cpersona;

            $tpais = Tpai::where('cpais',$um->cpais)->first();
            $pais=null;

            if($tpais){    
                $pais=$tpais->descripcion;            
            }
            $tum['pais'] = $pais;
           

            if(strlen($um->cubigeo)>0 && $um->cpais=='PER' ){           
                $dpto=substr($um->cubigeo,0,2).'0000';
                $prov=substr($um->cubigeo,0,4).'00';

                $tdpto = Tubigeo::where('cpais','=',$um->cpais)
                ->where('cubigeo','=',$dpto)
                ->first();

                if($tdpto){
                    $tum['departamento'] = ' - '.$tdpto->descripcion;
                }

                $tprov= Tubigeo::where('cpais','=',$um->cpais)
                ->where('cubigeo','=',$prov)
                ->first();

                if($tprov){
                    $tum['provincia'] = ' - '.$tprov->descripcion;
                }

                $tdis= Tubigeo::where('cpais','=',$um->cpais)
                ->where('cubigeo','=',$um->cubigeo)
                ->first();

                if($tdis){
                    $tum['distrito'] =' - '.$tdis->descripcion;
                }

            }

            else{

                $tum['departamento'] ='';
                $tum['provincia'] = '';
                $tum['distrito'] = '';

            }

            $tum['ubigeo'] =  $tum['pais'].$tum['departamento'].$tum['provincia'].$tum['distrito'];            
            $tum['direccion'] = $um->direccion;
            $tum['telefono'] = $um->telefono;
            $tum['nombre'] = $um->nombre;
            $tum['codigo'] = $um->codigo;
            $tum['destado'] = $um->destado;

           

            $tunidadminera[count($tunidadminera)] = $tum;
        }

        /* Fin unidad mineras*/

        return view('partials.modalUnidadMinera',compact('tunidadminera'));
    }

    public function editUmineraCliente($iduminera){
        $tunidadminera=DB::table('tunidadminera')
        ->where('cunidadminera','=',$iduminera)
        ->first();
        return Response::json($tunidadminera);
        
    }

    public function editUmineraContacto($idContacto){
        $tunidadmineracontacto=DB::table('tunidadmineracontactos')
        ->where('cunidadmineracontacto','=',$idContacto)
        ->first();
        return Response::json($tunidadmineracontacto);
        
    }

    public function eliminarUmineraCliente($iduminera){
      
        DB::beginTransaction();
        $uminera = Tunidadminera::where('cunidadminera','=',$iduminera)->first();
        $cpersona = $uminera->cpersona;

        $catchmsj='0';

        try{
            $uminera->delete();
        }
        catch(\Illuminate\Database\QueryException $e){

            $catchmsj='1';
          
        }
       
        DB::commit();

       //return Redirect::route('verUmineraCliente',$cpersona);

         return array($cpersona,$catchmsj);
        //return Redirect::route('getRespuestaEliminarUminera',array($cpersona,$catchmsj));
    }
    public function getRespuestaEliminarUminera($iduminera){
        $res=$this->eliminarUmineraCliente($iduminera);
        $msj='';
        $cpersona =$res[0];

        if($res[1]=='1'){
            $msj='1';
            return $msj;
        }

        else{
            
            return Redirect::route('verUmineraCliente',$cpersona);

        }
     
    }

    public function dataClientes($idCliente){   
        
        $personacli = DB::table('tpersona')
        ->where('cpersona','=',$idCliente)
        ->first();

         $personadir = DB::table('tpersonadirecciones')
        ->where('cpersona','=',$idCliente)
        ->where('ctipodireccion','=','COM')
        ->first();

        $tpais=null;

        if($personadir){
            $tpais = Tpai::where('cpais',$personadir->cpais)->select('descripcion')->first();
        }

             

        $unidadminera = DB::table('tunidadminera as um')
        ->leftJoin('tunidadmineraestado as ume','um.cumineraestado','=','ume.cumineraestado')       
        ->where('cpersona','=',$idCliente)
        ->orderBy('nombre','ASC')
        ->select('um.*','ume.descripcion as destado')
        ->get();

        $aunidadminera=array();

        foreach ($unidadminera as $um) {

            $tum['nombre'] = $um->nombre;
            $tum['codigo'] = $um->codigo;
            $tum['destado'] = $um->destado;           


            $contacto = DB::table('tunidadmineracontactos as umcon')
            ->leftJoin('ttiposcontacto as tc','umcon.ctipocontacto','=','tc.ctipocontacto')  
            ->leftJoin('tcontactocargo as tco','umcon.ccontactocargo','=','tco.ccontactocargo') 
            ->where('umcon.cunidadminera','=',$um->cunidadminera)
            ->select('umcon.*','tco.descripcion as des_cargo','tc.descripcion as des_tipo')
            ->get();

            $acontactos=array();
            $i=0;

            foreach ($contacto as $con) {
                $i++;
                $acon['item']= $i;
                $acon['apaterno']= $con->apaterno;
                $acon['amaterno']= $con->amaterno;
                $acon['nombres']= $con->nombres;
                $acon['tipo']= $con->des_tipo;
                $acon['cargo']= $con->des_cargo;
                $acon['email']= $con->email;
                $acon['anexo'] = $con->anexo;
                $acon['telefono']= $con->telefono;
                $acon['cel1'] = $con->cel1;
                $acon['cel2'] = $con->cel2;

                $acontactos[count($acontactos)]=$acon;               

            }
            $tum['contactos'] = $acontactos;   
            $aunidadminera[count($aunidadminera)]=$tum;         
        }        

         //dd($personacli,$aunidadminera);
         return array($personacli,$aunidadminera,$tpais);
    }

    public function printClientes($idCliente){

        $getDataClientes=$this->dataClientes($idCliente);

        $clientes=$getDataClientes[0];
        $aunidadminera=$getDataClientes[1];
        $pais=$getDataClientes[2];


        $view =  \View::make('cliente/printClientes', compact('clientes','aunidadminera','pais'))->render();
        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','D');

         return $pdf->stream($clientes->nombre.'.pdf');

    } 

    //Ingreso Logo para la unidad minera 

    public function grabarLogoCliente(Request $request)
    {
        //dd($request->file('file'));
        $idunidadminera = $request->cuminera_u_1;    
        // ruta de las imagenes guardadas
        $ruta = public_path().'/images/logounidadminera/';
        // recogida del form
        $imagenOriginal = $request->file('file');
        $nombre_original=$imagenOriginal->getClientOriginalName();
        $extension=$imagenOriginal->getClientOriginalExtension();
        // generar un nombre aleatorio para la imagen
        $temp_name = $idunidadminera. '.' . $extension; // 
        $tunidadminera = Tunidadminera::where('cunidadminera','=',$idunidadminera)->first();
        // Borro Imagen
        $image_path = $ruta. '/' .$tunidadminera->logo_unidadminera;
        if (!is_null($tunidadminera->logo_unidadminera)) {
            //dd($tunidadminera);
            unlink($image_path);
        }
        $tunidadminera->logo_unidadminera = $temp_name;
        $tunidadminera->update();
        //Guardado el Archivo
        $r1=Storage::disk('logounidadminera')->put($temp_name,  \File::get($imagenOriginal) );
        return redirect('int?url=listadoClientes');
    }

    
}

