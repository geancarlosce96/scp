<?php

namespace App\Http\Controllers\ControldeHoras;

use App\Models\Tperformance;
use App\Models\Tproyectoactividade;
use Doctrine\DBAL\Driver\AbstractDB2Driver;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use DB;
use Response;

use App\Http\Requests;
use App\Http\Controllers\Controller;


class controldeHorasController extends Controller
{

    public function listadeProyectos()
    {



        $user = Auth::user();

        // conocemos el area de la persona
        $tarea = DB::table('tpersonadatosempleado as e')
            ->leftjoin('tareas as ta', 'e.carea', '=', 'ta.carea')
            ->leftjoin('tdisciplinaareas as tda', 'ta.carea', '=', 'tda.carea')
            ->where('cpersona', '=', $user->cpersona)
            ->whereNull('ftermino')
            ->orderBy('fingreso', 'DESC')
            ->select('e.cpersona', 'e.carea', 'e.ccargo', 'ta.carea', 'ta.descripcion', 'tda.cdisciplina')
            ->first();


        $cdisciplina = null;
        if ($tarea) {
            $cdisciplina = $tarea->cdisciplina;
        }

        $esjefe = $this->esjefeinmediato( $user->cpersona);
        $lider_or_gerente = $this->lider_or_gerente();
       // dd($lider_or_gerente);

        $explodelideroGerente = explode("-",$lider_or_gerente);
        $esgp = '';
        if (in_array("gerente",$explodelideroGerente)) {
            $esgp = 'gerente';
          }

        //dd($esgp);



        //if (in_array("gerente",$explodelideroGerente))

        $tproyectos = DB::table('tproyecto as tp')
            ->leftjoin('tproyectopersona as tpyp', 'tpyp.cproyecto', '=', 'tp.cproyecto')
            ->leftjoin('tproyectodisciplina as tda', 'tp.cproyecto', '=', 'tda.cproyecto')
            ->select('tp.cproyecto', 'tp.codigo', 'tp.nombre')
            ->Where( 'tp.cestadoproyecto','!=','000')
            //->Where('tp.codigo','not ilike','%-%')
            ->Where('tp.codigo','NOT LIKE', '%'.'-'.'%' );
            //->get();
       // dd($tproyectoss);    

        if ($esjefe == true) {
            // $tproyectos = $tproyectos->where('tda.cdisciplina', '=', $cdisciplina);
         } 
        elseif($lider_or_gerente == 'lider')
        {
            $tproyectos = $tproyectos->where('tpyp.cpersona','=', $user->cpersona)
            ->where('tpyp.eslider', '=', '1');
        }
        elseif ($esgp == 'gerente') {
            $tproyectos = $tproyectos->where('tp.cpersona_gerente', '=', $user->cpersona);

        }
        else {
            $tproyectos = $tproyectos->where('tda.cdisciplina', '=', $cdisciplina);
        }

        $tproyectos = $tproyectos->select('tp.cproyecto', 'tp.codigo', 'tp.nombre')
            ->distinct()
            ->orderBy('tp.codigo', 'ASC')
            ->get();

        // dd($tproyectos,$lider_or_gerente);

        //return $tproyectos;

    //     $tproyectosCONTRAC = DB::table('tproyecto as tp')
    //     ->select('tp.codigo')
    //     ->Where( 'tp.cestadoproyecto','!=','000')
    //     ->Where('tp.codigo','NOT LIKE', '%'.'-'.'%' )
    //     ->orderBy('tp.codigo', 'ASC')
    //     ->lists('tp.codigo');

    //     $tproyectosSOC = DB::table('tproyecto as tp')
    //     ->select('tp.codigo')
    //     ->Where( 'tp.cestadoproyecto','!=','000')
    //     ->Where('tp.codigo','LIKE', '%'.'-'.'%' )
    //     ->orderBy('tp.codigo', 'ASC')
    //     ->lists('tp.codigo');

    //     $ac = [];
    //     foreach ($tproyectosSOC as $key => $value) {
    //        $ola =  explode("-",$value);
    //        array_push($ac,$ola[0]);
    //     }
    //     $parent = array_unique($ac);
    //     $abd = [];
    //     foreach ($parent as $key1 => $par) {
    //         foreach($tproyectosCONTRAC as $key2 => $con ):
    //                 if($con == $par){
    //                     continue 2; 
    //                 }
    //         endforeach; 
    //         array_push($abd, $par);
    //      }   
    //    dd($abd);
       
       

        return view('proyecto.ControldeHoras.opcionesControlHoras', compact('tproyectos','esgp','lider_or_gerente','cdisciplina'));

    }
    public function listadeProyectosresumenCCHH()
    {
        $user = Auth::user();


        $lider_or_gerente = $this->lider_or_gerente();

        $tproyectos = DB::table('tproyecto as tp')
            ->leftjoin('tproyectopersona as tpyp', 'tpyp.cproyecto', '=', 'tp.cproyecto')
            ->leftjoin('tpersona as tper', 'tp.cpersona_gerente', '=', 'tper.cpersona')
            ->leftjoin('tproyectodisciplina as tda', 'tp.cproyecto', '=', 'tda.cproyecto')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->leftjoin('testadoproyecto as tespro', 'tespro.cestadoproyecto', '=', 'tp.cestadoproyecto')
            ->leftjoin('ttipogestionproyecto as ttipogesti','ttipogesti.ctipogestionproyecto','=','tp.ctipogestionproyecto')
            ->where('tp.cestadoproyecto','=','001');

        if ($lider_or_gerente == 'gerente') {

            $tproyectos = $tproyectos->where('tp.cpersona_gerente', '=', $user->cpersona);
             }

        elseif ($lider_or_gerente == 'gerente-lider') {
            $tproyectos__gerente = DB::table('tproyecto as tp')
                ->select('tp.cproyecto')
                ->where('tp.cpersona_gerente', '=', $user->cpersona)
                ->where('tp.cestadoproyecto','=','001')
                ->orderBy('tp.cproyecto', 'ASC')
                ->get();


            $tproyectos__lider  =  DB::table('tproyectopersona as tpp')
                ->leftjoin('tproyecto as tproy', 'tproy.cproyecto', '=', 'tpp.cproyecto')
                ->select('tpp.cproyecto')
                ->where('tpp.cpersona', '=', $user->cpersona)
                ->where('tproy.cestadoproyecto','=','001')
                ->where('tpp.eslider', '=', '1')
                ->orderBy('tpp.cproyecto', 'ASC')
                ->get();

            $tproyectos__gerente_lider = array_merge($tproyectos__gerente,$tproyectos__lider);
            $array_tproyectos__gerente_lider = [];
            foreach ($tproyectos__gerente_lider as $proyect)
            {
                array_push($array_tproyectos__gerente_lider,$proyect->cproyecto);
            }
            $ar_diff = array_unique($array_tproyectos__gerente_lider);

            $tproyectos = $tproyectos->whereIn('tp.cproyecto', $ar_diff);
        }


            /*

        }*/
        elseif($lider_or_gerente == 'lider')
        {
            $tproyectos = $tproyectos->where('tpyp.cpersona','=', $user->cpersona)
                ->where('tpyp.eslider', '=', '1');
        }

        $tproyectos = $tproyectos->select('tp.cproyecto', 'tp.codigo', 'tp.nombre','tper.abreviatura','tum.nombre as nombreunidadminera','tespro.descripcion','ttipogesti.descripcion as nombretipogestion')
            ->orderBy('tp.cproyecto', 'ASC')
            ->distinct()
            ->get();

        return $tproyectos;



    }
    public function listaProyectosGP()
    {
        $user = Auth::user();
        $listaProyectosGP =  DB::table('tproyecto as tpy')
            ->select('tpy.cproyecto')
            ->where('tpy.cpersona_gerente','=',$user->cpersona)
            ->where('tpy.cestadoproyecto','=','001')
            ->orderBy('tpy.cproyecto','ASC')
            ->lists('tpy.cproyecto');
        return $listaProyectosGP;

    }
    public function listadeAreasresumenCCHH()
    {
       // dd("ola");
        $user = Auth::user();

        //$listadeProyectosGerente_or_Lider = $this->listadeProyectosresumenCCHH();
        $lideroGerente = $this->lider_or_gerente();
        $explodelideroGerente = explode("-",$lideroGerente);


        //$listadeProyectosGerente = $this->listaProyectosGP();
        //dd($explodelideroGerente);

        $unique_areas  = $this->aCargo($user->cpersona);
        //dd($unique_areas);
        //Obtiene las areas de los colaboradores encontrados en $careas_colaboradores
        $careas = DB::table('tareas as ta')
            ->leftjoin('tdisciplinaareas as tda', 'ta.carea', '=', 'tda.carea')
            ->leftjoin('tdisciplina as tis', 'tis.cdisciplina', '=', 'tda.cdisciplina')
            ->whereIn('ta.carea',$unique_areas)
            ->orderBy('ta.codigo_sig','ASC')
            ->select('tda.cdisciplina','ta.descripcion','ta.codigo_sig')
            ->get();

        // basta que tenga un cargo de gerente le muestra todas las areas
        if (in_array("gerente",$explodelideroGerente)) {

            $listadeProyectosGerente = $this->listaProyectosGP();
            $careas = DB::table('tproyectoejecucion as tpye')
                ->leftjoin('tareas as ta', 'tpye.carea', '=', 'ta.carea')
                ->leftjoin('tdisciplinaareas as tda', 'ta.carea', '=', 'tda.carea')
                ->leftjoin('tdisciplina as tis', 'tis.cdisciplina', '=', 'tda.cdisciplina')
                ->distinct()->select('tda.cdisciplina','ta.descripcion','ta.codigo_sig')
                ->whereIn('tpye.cproyecto',$listadeProyectosGerente)
                ->whereNotNUll('tda.cdisciplina')
                ->orderBy('tda.cdisciplina','ASC')
                ->get();
           }

         // solo Control de proyectos tiene todas las areas
         elseif ($unique_areas[0] == 56)
         {
             $careas = DB::table('tareas as ta')
                 ->leftjoin('tdisciplinaareas as tda', 'ta.carea', '=', 'tda.carea')
                 ->leftjoin('tdisciplina as tis', 'tis.cdisciplina', '=', 'tda.cdisciplina')
                 ->distinct()->select('tda.cdisciplina','ta.descripcion','ta.codigo_sig')
                 ->orderBy('tda.cdisciplina','ASC')
                 ->get();
         }

       // dd($careas,$unique_areas[0]);

/*        if ($lideroGerente == 'lider')
        {

            $unique_areas  = $this->aCargo($user->cpersona);
                //Obtiene las areas de los colaboradores encontrados en $careas_colaboradores
                $careas = DB::table('tareas as ta')
                    ->leftjoin('tdisciplinaareas as tda', 'ta.carea', '=', 'tda.carea')
                    ->leftjoin('tdisciplina as tis', 'tis.cdisciplina', '=', 'tda.cdisciplina')
                    ->whereIn('ta.carea',$unique_areas)
                    ->orderBy('ta.codigo_sig','ASC')
                    ->select('tda.cdisciplina','ta.descripcion','ta.codigo_sig')
                    ->get();

          // dd($careas,"sss");

        }
        else
        {
            $listadeProyectosGerente = $this->listaProyectosGP();


            $careas = DB::table('tproyectoejecucion as tpye')
                ->leftjoin('tareas as ta', 'tpye.carea', '=', 'ta.carea')
                ->leftjoin('tdisciplinaareas as tda', 'ta.carea', '=', 'tda.carea')
                ->leftjoin('tdisciplina as tis', 'tis.cdisciplina', '=', 'tda.cdisciplina')
                ->distinct()->select('tda.cdisciplina','ta.descripcion','ta.codigo_sig')
                ->whereIn('tpye.cproyecto',$listadeProyectosGerente)
                //->whereNotNUll('tpye.carea')
                ->orderBy('tda.cdisciplina','ASC')
                ->get();
        }*/
          //dd($careas,$lideroGerente);


        //dd($careas,$listadeProyectosGerente_or_Lider);
       // $lideroGerente = $this->lider_or_gerente();

        return view('proyecto.ControldeHoras.resumenopcionesCCHH',compact('lideroGerente','careas','explodelideroGerente','unique_areas'));
    }

    private function obtener_cargos_hijos($carea=[]){

        $area_hijos =  DB::table('tareas as tc')
            ->select('tc.carea','tc.carea_parent')
            ->whereIn('tc.carea_parent',$carea)
            ->lists('tc.carea');

        return $area_hijos;

    }

    private function aCargo($user)
    {
        $cargoid =  DB::table('tproyectopersona as tpde')
            ->leftjoin('tdisciplinaareas as tda', 'tpde.cdisciplina', '=', 'tda.cdisciplina')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea')
            ->select('ta.carea')
            ->where('tpde.cpersona','=',$user)
            ->distinct()
            ->lists('ta.carea');

        //Obtiene los cargos hijos
        $cargos_hijos = $this->obtener_cargos_hijos($cargoid);
        // dd($cargoid,$cargos_hijos);

        $array_cargos_hijos_todos=[];

        array_push($array_cargos_hijos_todos, $cargos_hijos);

        //obtiene los cargos nietos

        while ( $cargos_hijos != null) {
            $cargos_hijos=$this->obtener_cargos_hijos($cargos_hijos);
            array_push($array_cargos_hijos_todos,$cargos_hijos);
        }

        //$careas = [''=>''];

        $cargos_hijos_todos=[];

        if ($array_cargos_hijos_todos) {

            //Coloca en un array unidimensional todos los cargos nietos e hijos encontrados
            foreach ($array_cargos_hijos_todos as $ach) {

                foreach ($ach as $ch) {
                    array_push($cargos_hijos_todos, $ch);
                }

            }

            $merge_hijos_parent = array_merge($cargoid, $cargos_hijos_todos);
            $unique_areas = array_unique($merge_hijos_parent);
            return $unique_areas;
        }

    }

    public function getProyectosResumenCCHH(Request $request)
    {
        //dd($request->all());


        $user = Auth::user();
        $disciplina = $request->disciplina;
        $fdesde = $this->formatoFecha($request->fdesde);
        $fhasta = $this->formatoFecha($request->fhasta);
        $semana_fin = $this->formatoFechaNumSemana($request->fhasta);
        // $semana_fin = $fhasta->weekOfYear;
       // dd($disciplina);

        $nombrelider = DB::table('tpersona as tpers')
            ->select('tpers.abreviatura')
            ->where('tpers.cpersona','=', $user->cpersona)
            ->first();
        //$a = $this->getCapturarLiderDisciplina(4574,$disciplina);
       // dd($a);

        $lideroGerente = $this->lider_or_gerente();

        $listadeProyectosGerente_or_Lider = $this->listadeProyectosresumenCCHH();
        // dd($lideroGerente,$listadeProyectosGerente_or_Lider,$user->cpersona);

        $arrayProyectos = [];

        $proy['suma_total_horas_rate_presupuestadas__proyecto'] =0;
        $proy['suma_total_horas_rate_cargadas__proyecto'] =0;
        $proy['suma_total_horas_cargadas_saldo__proyecto'] =0;

        foreach ($listadeProyectosGerente_or_Lider as $tproyecto) {
            $proy['cproyecto'] = $tproyecto->cproyecto;
            $proy['codigo'] = $tproyecto->codigo;
            $proy['nombre'] = $tproyecto->nombre;
            $proy['lider'] = $this->getCapturarLiderDisciplina($tproyecto->cproyecto,$disciplina);
            $proy['senior'] = $this->getCapturarSenior($tproyecto->cproyecto,$disciplina);
            $proy['abreviatura'] = $tproyecto->abreviatura;
            $proy['nombreunidadminera'] = $tproyecto->nombreunidadminera;
            $proy['descripcion'] = $tproyecto->descripcion;
           /* $numdoc  = $this->numdoc($tproyecto->cproyecto);
            $proy['numdoc'] = $numdoc ? $numdoc : '-' ;
            $numpla  = $this->numpla($tproyecto->cproyecto);
            $proy['numpla'] = $numpla ? $numpla : '-' ;
            $numdocPresupuestados = $this->numdocPresupuestados($tproyecto->cproyecto);
            $proy['numdocPresupuestados'] = $numdocPresupuestados ? $numdocPresupuestados : '-';
            $numplaPresupuestados = $this->numplaPresupuestados($tproyecto->cproyecto);
            $proy['numplaPresupuestados'] = $numplaPresupuestados ? $numplaPresupuestados : '-';*/
            $proy['nombretipogestion'] = $tproyecto->nombretipogestion;

            //  $proy['HHPresupuestadasTotales'] = $this->obtenerhoraspresuestadasResumenHH($tproyecto->cproyecto);
            $proy['HHPresupuestadasTotales'] = $this->obtenerhoraspresuestadasResumenHH($disciplina,$tproyecto->cproyecto);
            // dd($proy['HHPresupuestadasTotales']);
            $proy['HHConsumidasTotales'] = $this->obtenerhorasConsumidasResumenHH($fdesde,$fhasta,$disciplina,$tproyecto->cproyecto);
            $proy['HHSaldoTotales'] =  $proy['HHPresupuestadasTotales'] - $proy['HHConsumidasTotales'];


            $proy['avanceTotalHH'] = 0;
            if ($proy['HHPresupuestadasTotales'] != 0)
            {
                $proy['avanceTotalHH'] = round(( $proy['HHConsumidasTotales']/ $proy['HHPresupuestadasTotales'])*100);
            }


            $listadeActividadesporProyecto = $this->listadeActividadesporProyecto($tproyecto->cproyecto);
            $array_actividades_principal = [];

            $totalMontopresuestadasporProyecto = 0;
            $totalMontoCargadasporProyecto = 0;
            $totalMontoSaldoporProyecto = 0;

            foreach ($listadeActividadesporProyecto as $listaProy)
            {
                $padre1['familia']                     = 'padre';
                $buscarhijos = $this->activityfather_or_son($listaProy->cproyectoactividades);
                $array_actividades =[];


                $acumulado_rate_presuestadas_hijo = 0;
                $acumulado_rate_presuestadas_nieto = 0;
                $acumulado_rate_presuestadas_bisnieto = 0;

                $acumulado_rate_cargadas_hijo = 0;
                $acumulado_rate_cargadas_nieto = 0;
                $acumulado_rate_cargadas_bisnieto = 0;

                foreach ($buscarhijos as $bhh)
                {
                    // Monto Presupuestados
                    $a['montopresupuestadosshijo'] = $this->obtenerRateHorasPresupuestadas($bhh->cproyectoactividades,$disciplina);
                    $a['montoCargadoshijo'] = $this->obtenerRateHorasCargadas($fdesde,$fhasta,$bhh->cproyectoactividades,$tproyecto->cproyecto,$disciplina);
                    $a['montoCargadoshijo_saldo'] = $a['montopresupuestadosshijo'] - $a['montoCargadoshijo'];
                    $acumulado_rate_presuestadas_hijo +=  $a['montopresupuestadosshijo'];
                    $acumulado_rate_cargadas_hijo += $a['montoCargadoshijo'];

                    $a['avancerate_hijo'] = 0;
                    if ($a['montopresupuestadosshijo'] != 0)
                    {
                        $a['avancerate_hijo'] = round(( $a['montoCargadoshijo']/$a['montopresupuestadosshijo'])*100);
                    }

                    // Nivel 2

                    $nivel2 = $this->activityfather_or_son($bhh->cproyectoactividades);
                    // dd($nivel2);

                    $array_actividades_nivel2 =[];

                    foreach ($nivel2 as $n2)
                    {
                        $nn2['montopresupuestadossnieto'] = $this->obtenerRateHorasPresupuestadas($n2->cproyectoactividades,$disciplina);
                        $nn2['montoCargadosnieto'] = $this->obtenerRateHorasCargadas($fdesde,$fhasta,$n2->cproyectoactividades,$tproyecto->cproyecto,$disciplina);
                        $nn2['montoCargadosnieto_saldo'] = $nn2['montopresupuestadossnieto'] - $nn2['montoCargadosnieto'];
                        $acumulado_rate_presuestadas_nieto +=  $nn2['montopresupuestadossnieto'];
                        $acumulado_rate_cargadas_nieto += $nn2['montoCargadosnieto'];

                        $nn2['avance_nieto'] =0;
                        if ( $nn2['montopresupuestadossnieto'] != 0)
                        {
                            $nn2['avance_nieto'] = round(($nn2['montoCargadosnieto']/ $nn2['montopresupuestadossnieto'])*100);
                        }




                        //Nivel 3

                        $nivel3 = $this->activityfather_or_son($n2->cproyectoactividades);
                        $array_actividades_nivel3 =[];
                        foreach ($nivel3 as $n3)
                        {

                            $nn3['montopresupuestadossbisnieto'] = $this->obtenerRateHorasPresupuestadas($n3->cproyectoactividades,$disciplina);
                            $nn3['montoCargadosbisnieto'] = $this->obtenerRateHorasCargadas($fdesde,$fhasta,$n3->cproyectoactividades,$tproyecto->cproyecto,$disciplina);
                            $nn3['montoCargadosbisnieto_saldo'] = $nn3['montopresupuestadossbisnieto'] - $nn3['montoCargadosbisnieto'];
                            $acumulado_rate_presuestadas_bisnieto += $nn3['montopresupuestadossbisnieto'];
                            $acumulado_rate_cargadas_bisnieto += $nn3['montoCargadosbisnieto'];

                            $nn3['avance_bisnieto'] = 0;
                            if ($nn3['montopresupuestadossbisnieto'] != 0)
                            {
                                $nn3['avance_bisnieto'] = round(($nn3['montoCargadosbisnieto']/$nn3['montopresupuestadossbisnieto'])*100);
                            }


                            array_push($array_actividades_nivel3,$nn3);
                        }

                        $nn2['nivel3']  =$array_actividades_nivel3;
                        array_push($array_actividades_nivel2,$nn2);
                    }


                    $a['nivel2']  = $array_actividades_nivel2;
                    array_push($array_actividades,$a);
                }
                $padre1['nivel0'] = $array_actividades;


                // Rates

                $padre1['suma_total_horas_rate_presupuestadas'] = $acumulado_rate_presuestadas_hijo + $acumulado_rate_presuestadas_nieto + $acumulado_rate_presuestadas_bisnieto;
                $totalMontopresuestadasporProyecto += $padre1['suma_total_horas_rate_presupuestadas'];

                $padre1['suma_total_horas_rate_cargadas'] = $acumulado_rate_cargadas_hijo + $acumulado_rate_cargadas_nieto + $acumulado_rate_cargadas_bisnieto;
                $totalMontoCargadasporProyecto += $padre1['suma_total_horas_rate_cargadas'];

                $padre1['suma_total_horas_cargadas_saldo'] = $padre1['suma_total_horas_rate_presupuestadas'] -  $padre1['suma_total_horas_rate_cargadas'];
                $totalMontoSaldoporProyecto += $padre1['suma_total_horas_cargadas_saldo'];



                $padre1['avance_total_padre'] = 0;
                if ($padre1['suma_total_horas_rate_presupuestadas'] != 0)
                {
                    $padre1['avance_total_padre'] = round(( $padre1['suma_total_horas_rate_cargadas']/ $padre1['suma_total_horas_rate_presupuestadas'])*100);
                }

                /*   $avancetotalMontoporProyecto = 0;
                   if ($totalMontopresuestadasporProyecto != 0)
                   {
                       $avancetotalMontoporProyecto = round(($totalMontoCargadasporProyecto / $totalMontopresuestadasporProyecto)*100);
                   }*/

                array_push($array_actividades_principal,$padre1);

            }

            $proy['listaactividades'] = $array_actividades_principal;
            $proy['suma_total_horas_rate_presupuestadas__proyecto'] = $totalMontopresuestadasporProyecto;
            $proy['suma_total_horas_rate_cargadas__proyecto'] = $totalMontoCargadasporProyecto;
            $proy['suma_total_horas_cargadas_saldo__proyecto'] = $totalMontoSaldoporProyecto;

            $proy['avance_total_padre_proyecto'] = 0;
            if ($proy['suma_total_horas_rate_presupuestadas__proyecto'] != 0)
            {
                $proy['avance_total_padre_proyecto'] = round(( $proy['suma_total_horas_rate_cargadas__proyecto']/ $proy['suma_total_horas_rate_presupuestadas__proyecto'])*100);
            }

            /*$performanceProyectoEncontrado = Tperformance::where('cproyecto',$tproyecto->cproyecto)->get();
            //dd($performanceProyectoEncontrado,count($performanceProyectoEncontrado) );
            if (count($performanceProyectoEncontrado) > 0) {

                $resumenProyectosPorcentaje= $this->getActividadescurva($tproyecto->cproyecto, $disciplina,$fdesde,$fhasta);
                $resumenProFinal = $resumenProyectosPorcentaje[0]['resumenTotal'];
                foreach ($resumenProFinal as $rpF)
                {
                    if ($semana_fin == $rpF['semana'] ) {
                        $proy['resumenPorcentajeFinal'] =  $rpF['resumenFinal'];
                    }
                }
                if ($proy['HHConsumidasTotales'] != 0) {

                    $proy['cpiHH'] =  round($proy['HHPresupuestadasTotales'] *  ($proy['resumenPorcentajeFinal']/ 100) / $proy['HHConsumidasTotales'],2) ;
                }
                else
                {
                    $proy['cpiHH'] = 0;
                }
            }

            else{

                $proy['resumenPorcentajeFinal'] =  0;
                $proy['cpiHH']                  =  0;

            }*/


            array_push($arrayProyectos,$proy);
        }
        //dd($arrayProyectos,"dddd");


        return view('proyecto.ControldeHoras.tablaProyectosCCHH', compact('arrayProyectos','lideroGerente','nombrelider','disciplina'));
    }
    public function getCapturarLiderDisciplina($proyecto,$disciplina)
    {
        $liderP =  DB::table('tproyectopersona as typ')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'typ.cpersona')
            ->select('tpr.abreviatura')
            ->where('typ.cproyecto', '=',$proyecto );
                if ( $disciplina == 'Nuevo') {
                    	return '-';
                    }

                else{
                    $liderP = $liderP->where('typ.cdisciplina', '=', $disciplina)
                                      ->where('typ.eslider', '=', 1)
                                     ->first();

                    return  $liderP  ? $liderP->abreviatura  : '-';
                }
    }

    public function getCapturarSenior($proyecto,$disciplina)
    {
        $seniorP =  DB::table('tproyectopersona as typ')
                            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'typ.cpersona')
                            ->select('tpr.abreviatura')
                            ->where('typ.cproyecto', '=',$proyecto);
                            if ($disciplina == 'Nuevo')
                            {
                                return '-';
                            }
                            else{
                                $seniorP = $seniorP->where('typ.seniors', '=',$disciplina)
                                            ->first();
                                return $seniorP  ? $seniorP->abreviatura  : '-';
                            }
    }

    public function getProyectos_con_ActividadesCCHH(Request $request)
    {
    //    $prueba =  $this->obtenerhoraspresuestadasTotalesHijos($request->disciplina,38401,4313);
       // dd($request->all());
       // dd("123456789");
       //
        $cproyecto = $request->cproyecto;
        $disciplina = $request->disciplina;
        $fdesde = $this->formatoFecha( $request->fdesde);
        $fhasta = $this->formatoFecha( $request->fhasta);
        $soc =  $request->soc;
        $multipleProyectosSOC = $request->cbo_selectSOC;
       
        $listaproyect = $request->soc == "true" ? $this->listadeProyectosconSOC($cproyecto) : $this->listadeProyectossinSOC($multipleProyectosSOC);
       // $listaproyect = $request->soc == "true" ? 'con soc' : 'sin soc';
        
       //dd($listaproyect);

        $listadeActividadesporProyecto = $this->listadeActividadesporProyecto($cproyecto);
        
        //  $pruebarate = $this->obtenerRateHorasPresupuestadas(56738,$disciplina)
        //  dd($pruebarate);

        $arrayProyectos = [];
        
        $totalpresuestadasporProyecto_final = 0;
        $totalCargadasporProyecto_final = 0;
        $totalSaldoporProyecto_final = 0;

        $totalMontopresuestadasporProyecto_final = 0;
        $totalMontoCargadasporProyecto_final = 0;
        $totalMontoSaldoporProyecto_final = 0;

        foreach ($listaproyect as $tproyecto) {
            $proy['cproyecto'] = $tproyecto->cproyecto;
            $proy['codigo'] = $tproyecto->codigo;
            $proy['nombreProyecto'] = $tproyecto->nombre;
            $proy['nombreMinera'] = $tproyecto->nombreunidadMinera;

            $listadeActividadesporProyecto = $this->listadeActividadesporProyecto($tproyecto->cproyecto);

           // dd($listadeActividadesporProyecto);

            $array_actividades_principal = [];

            $totalpresuestadasporProyecto = 0;
            $totalCargadasporProyecto = 0;
            $totalSaldoporProyecto = 0;

            $totalMontopresuestadasporProyecto = 0;
            $totalMontoCargadasporProyecto = 0;
            $totalMontoSaldoporProyecto = 0;

            $acumulado_total_horas_doc =0;
            $acumulado_total_horas_plano =0;

            foreach ($listadeActividadesporProyecto as $listaProy)
            {
                $padre1['cproyecto']        =             $tproyecto->cproyecto;
                $padre1['cproyectoactividades']        = $listaProy->cproyectoactividades;
                $padre1['codigoactividad']             = $listaProy->codigoactvidad;
                $padre1['descripcionactividad']        = $listaProy->descripcionactividad;
                $padre1['familia']                     = 'padre';

                $buscarhijos = $this->activityfather_or_son($listaProy->cproyectoactividades);
                //dd($listadeActividadesporProyecto);
                $array_actividades =[];
                $acumulado_hijo = 0;
                // $acumulado_nieto = 0;
                // $acumulado_bisnieto = 0;

                $acumulado_cargada_hijo = 0;
                //$acumulado_cargada_nieto = 0;
                // $acumulado_cargada_bisnieto = 0;

                $acumulado_rate_presuestadas_hijo = 0;
                //  $acumulado_rate_presuestadas_nieto = 0;
                // $acumulado_rate_presuestadas_bisnieto = 0;

                $acumulado_rate_cargadas_hijo = 0;
                // $acumulado_rate_cargadas_nieto = 0;
                //  $acumulado_rate_cargadas_bisnieto = 0;

                $acumuladohoras_doc_hijo = 0;
                $acumuladohoras_doc_tataranieto = 0;

                $acumuladohoras_planos_hijo = 0;
                $acumuladohoras_planos_tataranieto = 0;

                foreach ($buscarhijos as $bhh)
                {
                    $acumulado_nieto = 0;
                    $acumulado_cargada_nieto = 0;

                    $acumulado_rate_cargadas_nieto = 0;
                    $acumulado_rate_presuestadas_nieto = 0;

                    $acumuladohoras_doc_nieto = 0;
                    $acumuladohoras_planos_nieto = 0;
                    $a['cproyecto']        =            $bhh->cproyecto;
                    $a['cproyectoactividades']        = $bhh->cproyectoactividades;
                    $a['codigoactividad']             = $bhh->codigoactvidad;
                    $a['cproyectoactividades_parent'] = $bhh->cproyectoactividades_parent;
                    $a['descripcionactividad']        = $bhh->descripcionactividad;

                    $a['familia']  = 'hijo';


                    // Nivel 2

                    $nivel2 = $this->activityfather_or_son($bhh->cproyectoactividades);
                   // dd($nivel2);

                    $array_actividades_nivel2 =[];

                    foreach ($nivel2 as $n2)
                    {
                        $acumulado_cargada_bisnieto = 0;
                        $acumulado_bisnieto = 0;

                        $acumulado_rate_presuestadas_bisnieto = 0;
                        $acumulado_rate_cargadas_bisnieto = 0;
                        $acumuladohoras_doc_bisnieto = 0;
                        $acumuladohoras_planos_bisnieto = 0;

                        $nn2['cproyecto']        =            $n2->cproyecto;
                        $nn2['cproyectoactividades']        = $n2->cproyectoactividades;
                        $nn2['codigoactividad']             = $n2->codigoactvidad;
                        $nn2['cproyectoactividades_parent'] = $bhh->cproyectoactividades_parent;
                        $nn2['cproyectoactividadeshijo_parent'] = $n2->cproyectoactividades_parent;
                        $nn2['descripcionactividad']        = $n2->descripcionactividad;
                        $nn2['familia']  = 'nieto';


                        //Nivel 3

                        $nivel3 = $this->activityfather_or_son($n2->cproyectoactividades);
                        $array_actividades_nivel3 =[];
                        foreach ($nivel3 as $n3)
                        {
                            $acumulado_cargada_tataranieto = 0;
                            $acumulado_tataranieto = 0;

                            $acumulado_rate_presuestadas_tataranieto = 0;
                            $acumulado_rate_cargadas_tataranieto = 0;

                            $nn3['cproyecto']        =            $n3->cproyecto;
                            $nn3['cproyectoactividades']        = $n3->cproyectoactividades;
                            $nn3['codigoactividad']             = $n3->codigoactvidad;
                            $nn3['cproyectoactividades_parent'] = $bhh->cproyectoactividades_parent;
                            $nn3['cproyectoactividadeshijo_parent'] = $n2->cproyectoactividades_parent;
                            $nn3['cproyectoactividadeshijodirecto_parent'] = $n3->cproyectoactividades_parent;
                            $nn3['descripcionactividad']        = $n3->descripcionactividad;

                            $nn3['familia']  = 'bisnieto';

                            $nivel4 = $this->activityfather_or_son($n3->cproyectoactividades);
                            //dd($nivel4);
                            $array_actividades_nivel4 =[];
                             foreach($nivel4 as $n4):
                                 $nn4['cproyecto']        =            $n4->cproyecto;
                                 $nn4['cproyectoactividades']        = $n4->cproyectoactividades;
                                 $nn4['codigoactividad']             = $n4->codigoactvidad;
                                 $nn4['cproyectoactividades_parent'] = $bhh->cproyectoactividades_parent;
                                 $nn4['cproyectoactividadeshijo_parent'] = $n2->cproyectoactividades_parent;
                                 $nn4['cproyectoactividadeshijodirecto_parent'] = $n3->cproyectoactividades_parent;
                                 $nn4['cproyectoactividadestataranieto_parent'] = $n4->cproyectoactividades_parent;
                                 $nn4['descripcionactividad']        = $n4->descripcionactividad;
                                 $nn4['numdoc']                       = $n4->numerodocumentos==null ? 0:  $n4->numerodocumentos;
                                 $nn4['numplanos']                    = $n4->numeroplanos==null ? 0: $n4->numeroplanos;
                                 $nn4['familia']  = 'tataranieto';

                                 $nn4['horaspresuestadasbisnieto']  =  $this->obtenerhoraspresuestadasTotalesHijos($disciplina,$n4->cproyectoactividades,$tproyecto->cproyecto);
                                 $nn4['horascargadasbisnieto'] = $this->obtenerhorasConsumidasTotalesHijos($fdesde,$fhasta,$disciplina,$n4->cproyectoactividades,$tproyecto->cproyecto);
                                 $acumulado_tataranieto += $nn4['horaspresuestadasbisnieto'];
                                 $acumulado_cargada_tataranieto += $nn4['horascargadasbisnieto'];

                                 $nn4['avance'] = 0;
                                if ($nn4['horaspresuestadasbisnieto'] != 0)
                                {
                                    $nn4['avance'] = round(($nn4['horascargadasbisnieto']/$nn4['horaspresuestadasbisnieto'])*100);
                                }
                                 $nn4['montopresupuestadossbisnieto'] = $this->obtenerRateHorasPresupuestadas($n4->cproyectoactividades,$disciplina);
                                 $nn4['montoCargadosbisnieto'] =  $this->obtenerRateHorasCargadas($fdesde,$fhasta,$n4->cproyectoactividades,$tproyecto->cproyecto,$disciplina);
                                 $nn4['montoCargadosbisnieto_saldo'] = $nn4['montopresupuestadossbisnieto'] - $nn4['montoCargadosbisnieto'];
                                 $acumulado_rate_presuestadas_tataranieto += $nn4['montopresupuestadossbisnieto'];
                                 $acumulado_rate_cargadas_tataranieto += $nn4['montoCargadosbisnieto'];

                                 $nn4['avance_bisnieto'] = 0;
                                 if ($nn4['montopresupuestadossbisnieto'] != 0)
                                 {
                                     $nn4['avance_bisnieto'] = round(($nn4['montoCargadosbisnieto']/$nn4['montopresupuestadossbisnieto'])*100);
                                 }

                                 $acumuladohoras_doc_tataranieto   +=  $nn4['numdoc'] ;
                                 $acumuladohoras_planos_tataranieto   +=  $nn4['numplanos'] ;
                                 array_push($array_actividades_nivel4,$nn4);

                             endforeach;
                            $nn3['numdoc']                       =  $nivel4 == null ? ( $n3->numerodocumentos==null ? 0:  $n3->numerodocumentos) : $acumuladohoras_doc_tataranieto;
                            $nn3['numplanos']                    =  $nivel4 ==  null ? ($n3->numeroplanos==null ? 0: $n3->numeroplanos):$acumuladohoras_planos_tataranieto ;
                           // $nivel3 == null ? ( $n2->numerodocumentos==null ? 0:  $n2->numerodocumentos) : $acumuladohoras_doc_bisnieto;
                            $nn3['horaspresuestadasbisnieto']  =  $nivel4 == null ?   $this->obtenerhoraspresuestadasTotalesHijos($disciplina,$n3->cproyectoactividades,$tproyecto->cproyecto) :  $acumulado_tataranieto ;
                            $nn3['horascargadasbisnieto'] = $nivel4 == null ? $this->obtenerhorasConsumidasTotalesHijos($fdesde,$fhasta,$disciplina,$n3->cproyectoactividades,$tproyecto->cproyecto) : $acumulado_cargada_tataranieto  ;
                            $acumulado_bisnieto += $nn3['horaspresuestadasbisnieto'];
                            $acumulado_cargada_bisnieto += $nn3['horascargadasbisnieto'];

                            $nn3['avance'] = 0;
                            if ($nn3['horaspresuestadasbisnieto'] != 0)
                            {
                                $nn3['avance'] = round(($nn3['horascargadasbisnieto']/$nn3['horaspresuestadasbisnieto'])*100);
                            }


                            $nn3['montopresupuestadossbisnieto'] =  $nivel4 == null ?  $this->obtenerRateHorasPresupuestadas($n3->cproyectoactividades,$disciplina) :$acumulado_rate_presuestadas_tataranieto;
                            $nn3['montoCargadosbisnieto'] =  $nivel4 == null ?  $this->obtenerRateHorasCargadas($fdesde,$fhasta,$n3->cproyectoactividades,$tproyecto->cproyecto,$disciplina) :$acumulado_rate_cargadas_tataranieto;
                            $nn3['montoCargadosbisnieto_saldo'] = $nn3['montopresupuestadossbisnieto'] - $nn3['montoCargadosbisnieto'];
                            $acumulado_rate_presuestadas_bisnieto += $nn3['montopresupuestadossbisnieto'];
                            $acumulado_rate_cargadas_bisnieto += $nn3['montoCargadosbisnieto'];

                            $nn3['avance_bisnieto'] = 0;
                            if ($nn3['montopresupuestadossbisnieto'] != 0)
                            {
                                $nn3['avance_bisnieto'] = round(($nn3['montoCargadosbisnieto']/$nn3['montopresupuestadossbisnieto'])*100);
                            }

                            $acumuladohoras_doc_bisnieto  +=  $nn3['numdoc'] ;
                            $acumuladohoras_planos_bisnieto  +=  $nn3['numplanos'] ;
                            $nn3['nivel4']  =$array_actividades_nivel4;
                            array_push($array_actividades_nivel3,$nn3);
                        }
                        $nn2['numdoc']                       =  $nivel3 == null ? ( $n2->numerodocumentos==null ? 0:  $n2->numerodocumentos) : $acumuladohoras_doc_bisnieto;
                        $nn2['numplanos']                    =  $nivel3 == null ? ( $n2->numeroplanos==null ? 0:  $n2->numeroplanos) :$acumuladohoras_planos_bisnieto;
                        $nn2['horaspresuestadasnieto']  = $nivel3 == null ? $this->obtenerhoraspresuestadasTotalesHijos($disciplina,$n2->cproyectoactividades,$tproyecto->cproyecto) : $acumulado_bisnieto;
                        $nn2['horascargadasnieto'] = $nivel3 == null ? $this->obtenerhorasConsumidasTotalesHijos($fdesde,$fhasta,$disciplina,$n2->cproyectoactividades,$tproyecto->cproyecto) : $acumulado_cargada_bisnieto;
                        $acumulado_nieto +=  $nn2['horaspresuestadasnieto'];
                        $acumulado_cargada_nieto +=  $nn2['horascargadasnieto'];

                        $nn2['avance'] =0;
                        if ( $nn2['horaspresuestadasnieto'] != 0)
                        {
                            $nn2['avance'] = round(($nn2['horascargadasnieto']/ $nn2['horaspresuestadasnieto'])*100);
                        }

                        $nn2['montopresupuestadossnieto'] =  $nivel3 == null ? $this->obtenerRateHorasPresupuestadas($n2->cproyectoactividades,$disciplina):$acumulado_rate_presuestadas_bisnieto;
                        $nn2['montoCargadosnieto'] =  $nivel3 == null ? $this->obtenerRateHorasCargadas($fdesde,$fhasta,$n2->cproyectoactividades,$tproyecto->cproyecto,$disciplina):$acumulado_rate_cargadas_bisnieto;
                        $nn2['montoCargadosnieto_saldo'] = $nn2['montopresupuestadossnieto'] - $nn2['montoCargadosnieto'];
                        $acumulado_rate_presuestadas_nieto +=  $nn2['montopresupuestadossnieto'];
                        $acumulado_rate_cargadas_nieto += $nn2['montoCargadosnieto'];

                        $nn2['avance_nieto'] =0;
                        if ( $nn2['montopresupuestadossnieto'] != 0)
                        {
                            $nn2['avance_nieto'] = round(($nn2['montoCargadosnieto']/ $nn2['montopresupuestadossnieto'])*100);
                        }



                        $acumuladohoras_doc_nieto  +=  $nn2['numdoc'] ;
                        $acumuladohoras_planos_nieto += $nn2['numplanos'];

                        $nn2['nivel3']  =$array_actividades_nivel3;
                        array_push($array_actividades_nivel2,$nn2);
                    }


                    $a['numdoc']                  = $nivel2 == null ? ( $bhh->numerodocumentos==null ? 0:  $bhh->numerodocumentos) : $acumuladohoras_doc_nieto;
                    $a['numplanos']               = $nivel2 == null ? ( $bhh->numeroplanos==null ? 0:  $bhh->numeroplanos):  $acumuladohoras_planos_nieto;
                    $a['horaspresuestadashijo']   = $nivel2 == null ? $this->obtenerhoraspresuestadasTotalesHijos($disciplina,$bhh->cproyectoactividades,$tproyecto->cproyecto) : $acumulado_nieto ;
                    $a['horascargadashijo'] = $nivel2 == null ? $this->obtenerhorasConsumidasTotalesHijos($fdesde,$fhasta,$disciplina,$bhh->cproyectoactividades,$tproyecto->cproyecto) :  $acumulado_cargada_nieto;
                    $acumulado_hijo +=  $a['horaspresuestadashijo'];
                    $acumulado_cargada_hijo += $a['horascargadashijo'];

                    $a['avance'] = 0;
                    if ($a['horaspresuestadashijo'] != 0)
                    {
                        $a['avance'] = round(( $a['horascargadashijo']/$a['horaspresuestadashijo'])*100);
                    }


                    // Monto Presupuestados
                    $a['montopresupuestadosshijo'] = $nivel2 == null ?  $this->obtenerRateHorasPresupuestadas($bhh->cproyectoactividades,$disciplina):$acumulado_rate_presuestadas_nieto ;
                    $a['montoCargadoshijo'] =    $nivel2 == null ?  $this->obtenerRateHorasCargadas($fdesde,$fhasta,$bhh->cproyectoactividades,$tproyecto->cproyecto,$disciplina): $acumulado_rate_cargadas_nieto;
                    $a['montoCargadoshijo_saldo'] = $a['montopresupuestadosshijo'] - $a['montoCargadoshijo'];
                    $acumulado_rate_presuestadas_hijo +=  $a['montopresupuestadosshijo'];
                    $acumulado_rate_cargadas_hijo += $a['montoCargadoshijo'];

                    $a['avancerate_hijo'] = 0;
                    if ($a['montopresupuestadosshijo'] != 0)
                    {
                        $a['avancerate_hijo'] = round(( $a['montoCargadoshijo']/$a['montopresupuestadosshijo'])*100);
                    }


                    $a['nivel2']  = $array_actividades_nivel2;
                    array_push($array_actividades,$a);

                    $acumuladohoras_doc_hijo += $a['numdoc'];
                    $acumuladohoras_planos_hijo  += $a['numplanos'];

                }
                //dd($nivel2);
                $padre1['acumuladohoras_doc'] = $acumuladohoras_doc_hijo;
                $acumulado_total_horas_doc +=  $padre1['acumuladohoras_doc'];
                $padre1['acumuladohoras_plano'] = $acumuladohoras_planos_hijo;
                $acumulado_total_horas_plano += $padre1['acumuladohoras_plano'];
               // $padre1['acumuladohoras_plano'] = $acumuladohoras_planos_hijo + $acumuladohoras_planos_nieto + $acumuladohoras_planos_bisnieto + $acumuladohoras_planos_tataranieto;
                $padre1['nivel0'] = $array_actividades;

                //dd($buscarhijos,$listaProy->cproyectoactividades,$tproyecto->cproyecto);
                // Horas

                $padre1['suma_total_horas_presupuestadas'] = $buscarhijos == null ? $this->obtenerhoraspresuestadasTotalesHijos($disciplina,$listaProy->cproyectoactividades,$tproyecto->cproyecto) : $acumulado_hijo;//$nivel2 == null ? $this->obtenerhoraspresuestadasTotalesHijos($disciplina,$bhh->cproyectoactividades,$tproyecto->cproyecto) : $acumulado_nieto ;$acumulado_hijo;
                $totalpresuestadasporProyecto += $padre1['suma_total_horas_presupuestadas'];
                
                // $listaProy->cproyectoactividades_parent != null 
                //                                 $padre1['suma_total_horas_presupuestadas'] : 
                //                                 '';

                $padre1['suma_total_horas_cargadas'] =  $buscarhijos == null ? $this->obtenerhorasConsumidasTotalesHijos($fdesde,$fhasta,$disciplina,$listaProy->cproyectoactividades,$tproyecto->cproyecto) : $acumulado_cargada_hijo;
                //dd($acumulado_cargada_hijo,$acumulado_cargada_nieto);
                $totalCargadasporProyecto += $padre1['suma_total_horas_cargadas'] ;


                $padre1['suma_total_horas_saldo'] = $padre1['suma_total_horas_presupuestadas'] -  $padre1['suma_total_horas_cargadas'];
                $totalSaldoporProyecto += $padre1['suma_total_horas_saldo'];

                $padre1['avance'] = 0;
                if ($padre1['suma_total_horas_presupuestadas'] != 0)
                {
                    $padre1['avance'] = round(( $padre1['suma_total_horas_cargadas']/ $padre1['suma_total_horas_presupuestadas'])*100);
                }
                $avancetotalporProyecto = 0;
                if ($totalpresuestadasporProyecto != 0)
                {
                    $avancetotalporProyecto = round(($totalCargadasporProyecto / $totalpresuestadasporProyecto)*100);
                }


                // Rates

                $padre1['suma_total_horas_rate_presupuestadas'] = $buscarhijos == null ? $this->obtenerRateHorasPresupuestadas($listaProy->cproyectoactividades,$disciplina) : $acumulado_rate_presuestadas_hijo;
                $totalMontopresuestadasporProyecto += $padre1['suma_total_horas_rate_presupuestadas'];

                $padre1['suma_total_horas_rate_cargadas'] =  $buscarhijos == null ? $this->obtenerRateHorasCargadas($fdesde,$fhasta,$listaProy->cproyectoactividades,$tproyecto->cproyecto,$disciplina) : $acumulado_rate_cargadas_hijo;
                $totalMontoCargadasporProyecto += $padre1['suma_total_horas_rate_cargadas'];

                $padre1['suma_total_horas_cargadas_saldo'] = $padre1['suma_total_horas_rate_presupuestadas'] -  $padre1['suma_total_horas_rate_cargadas'];
                $totalMontoSaldoporProyecto += $padre1['suma_total_horas_cargadas_saldo'];



                $padre1['avance_total_padre'] = 0;
                if ($padre1['suma_total_horas_rate_presupuestadas'] != 0)
                {
                    $padre1['avance_total_padre'] = round(( $padre1['suma_total_horas_rate_cargadas']/ $padre1['suma_total_horas_rate_presupuestadas'])*100);
                }

                $avancetotalMontoporProyecto = 0;
                if ($totalMontopresuestadasporProyecto != 0)
                {
                    $avancetotalMontoporProyecto = round(($totalMontoCargadasporProyecto / $totalMontopresuestadasporProyecto)*100);
                }

                array_push($array_actividades_principal,$padre1);

            }

            $proy['listaactividades'] = $array_actividades_principal;
           
            $proy['total_horas_presupuestadas_por_proyecto'] = $totalpresuestadasporProyecto;
            $proy['total_horas_consumidas_por_proyecto'] = $totalCargadasporProyecto;
            $proy['total_horas_saldo_por_proyecto'] = $totalSaldoporProyecto;
            // avance total por proyecto horas
            $proy['avance_total_por_proyecto'] = 0;
            if ($proy['total_horas_presupuestadas_por_proyecto'] != 0)
            {
                $proy['avance_total_por_proyecto'] = round(( $proy['total_horas_consumidas_por_proyecto']/ $proy['total_horas_presupuestadas_por_proyecto'])*100);
            }

            $proy['total_monto_presupuestados_por_proyecto'] = $totalMontopresuestadasporProyecto;
            $proy['total_monto_consumidos_por_proyecto'] = $totalMontoCargadasporProyecto;
            $proy['total_monto_saldo_por_proyecto'] = $totalMontoSaldoporProyecto;

            $proy['avance_total_monto_por_proyecto'] = 0;
            if ($proy['total_monto_presupuestados_por_proyecto'] != 0)
            {
                $proy['avance_total_monto_por_proyecto'] = round(( $proy['total_monto_consumidos_por_proyecto']/ $proy['total_monto_presupuestados_por_proyecto'])*100);
            }

            $totalpresuestadasporProyecto_final +=  $proy['total_horas_presupuestadas_por_proyecto'];
            $totalCargadasporProyecto_final += $proy['total_horas_consumidas_por_proyecto'];
            $totalSaldoporProyecto_final += $proy['total_horas_saldo_por_proyecto'];

            $totalavanceporProyecto_final = 0;
            if ($totalpresuestadasporProyecto_final != 0)
            {
                $totalavanceporProyecto_final = round(( $totalCargadasporProyecto_final/ $totalpresuestadasporProyecto_final)*100);
            }
            
            // avance total por proyecto horas

            $totalMontopresuestadasporProyecto_final += $proy['total_monto_presupuestados_por_proyecto'];
            $totalMontoCargadasporProyecto_final += $proy['total_monto_consumidos_por_proyecto'];
            $totalMontoSaldoporProyecto_final += $proy['total_monto_saldo_por_proyecto'];

            $totalavancemontoporProyecto_final = 0;
            if ($totalMontopresuestadasporProyecto_final != 0)
            {
                $totalavancemontoporProyecto_final = round(( $totalMontoCargadasporProyecto_final/ $totalMontopresuestadasporProyecto_final)*100);
            }

            //dd($arrayProyectos);
            array_push($arrayProyectos,$proy);
        }
       // dd($totalpresuestadasporProyecto_final);

        /*----------  Lista de cabezera HHPP ----------*/
        $listaCategoriaHHPP = $this->obtenerCabeceraCategoriasHorasPresupuestadas($cproyecto,$disciplina);
        $listarCategoriaHHPP = [];
        foreach ($listaCategoriaHHPP as $listaCCHHPP){
            $listaPP['descripcionrol'] =  $listaCCHHPP->descripcionrol;
            array_push($listarCategoriaHHPP,$listaPP);
        }
         // dd($arrayProyectos);

        /*----------  End Lista de cabezera HHPP ----------*/

        return view('proyecto.ControldeHoras.actividadesProyectos', compact('arrayProyectos','totalpresuestadasporProyecto_final','totalCargadasporProyecto_final','totalSaldoporProyecto_final','totalavanceporProyecto_final',
            'totalMontopresuestadasporProyecto_final','totalMontoCargadasporProyecto_final','totalMontoSaldoporProyecto_final','totalavancemontoporProyecto_final','listarCategoriaHHPP','acumulado_total_horas_doc','acumulado_total_horas_plano' ));

    }

    // Funciones REUTILIZABLES

    public function lider_or_gerente()
    {
        $user = Auth::user();

        $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user->cpersona)
            ->get();

        $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.cpersona', '=', $user->cpersona)
            ->where('tpyp.eslider','=','1')
            ->get();

        $esjefe = $this->esjefeinmediato($user->cpersona);
       // dd($esjefe);

        $lider_or_gerente = '';

        if($esgerente && $eslider && $esjefe)
        {
            $lider_or_gerente = 'gerente-lider-jefe';
        }
        elseif($esgerente && $eslider)
        {
            $lider_or_gerente = 'gerente-lider';
        }
        elseif($esgerente && $esjefe)
        {
            $lider_or_gerente = 'gerente-jefe';
        }
        elseif($eslider && $esjefe)
        {
            $lider_or_gerente = 'lider-jefe';
        }
        elseif($esjefe)
        {
            $lider_or_gerente = 'jefe';
        }
        elseif($eslider)
        {
            $lider_or_gerente = 'lider';
        }
        elseif($esgerente)
        {
            $lider_or_gerente = 'gerente';
        }


        return $lider_or_gerente;

    }

    private function esjefeinmediato($cpersona)
    {
        $esjefe = DB::table('tpersonadatosempleado as e')
            ->leftjoin('tcargos as c', 'e.ccargo', '=', 'c.ccargo')
            ->where('e.cpersona', '=', $cpersona)
            ->where('c.esjefaturaarea', '=', 1)
            // ->select('e.cpersona','c.esjefaturaarea')
            ->first();

        // dd($esjefe->ccargo);

      /*  $jefeinmediato = DB::table('tcargos')
            ->where('cargoparentdespliegue', '=', $esjefe->ccargo)

            ->first();*/

        if ($esjefe) {
            $ji = true;
        } else {
            $ji = false;
        }

        return $ji;
    }



    public function obtenerAreasCCHH(Request $request)
    {

        $cproyecto = $request->cproyecto;
        $tareas = $this->getAreas($cproyecto);
        //dd($tareas);
        return $tareas;

    }
    public function getfecha(Request $request)
    {
        $cproyecto = $request->cproyecto;
        $fecha = DB::table('tproyecto as tpy')
            ->where('tpy.cproyecto', '=', $cproyecto)
            ->select('tpy.cproyecto', 'tpy.finicio as finicio', 'tpy.fcierre as fcierre')
            ->get();
        $fecha_array = [];
        foreach ($fecha as $fch)
        {
            $fec['finicio'] = $fch->finicio;
            $fec['fcierre'] = $fch->fcierre;
            array_push($fecha_array,$fec);
        }

        return $fecha_array;
    }

    public function obtenerSOCProyectos(Request $request)
    {
        $cproyecto = $request->cproyecto;
        $listaproyectos = $this->listadeProyectosconSOC($cproyecto);
        //dd($tareas);
        return $listaproyectos;

    }
    public function listadeActividadesporProyecto($cproyecto)
    {
        $actividades = DB::table('tproyectoactividades')
            ->select('cproyectoactividades','codigoactvidad','descripcionactividad','cproyectoactividades_parent')
            ->where('cproyecto', '=',$cproyecto)
            ->where('cproyectoactividades_parent', '=',null)
            ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
            ->get();
        return $actividades;
    }
    private function activityfather_or_son($cactividad)
    {
        $activityfather_or_son = DB::table('tproyectoactividades as tpa')
            ->select('tpa.cproyecto','tpa.cproyectoactividades','tpa.codigoactvidad','tpa.cproyectoactividades_parent','tpa.descripcionactividad','tpa.numerodocumentos','tpa.numeroplanos')
            ->orderBy('tpa.codigoactvidad','ASC')
            ->where('tpa.cproyectoactividades_parent', '=', $cactividad)
            ->get();
        /*
        if($activityfather_or_son)
        {
            $familia = 'padre';
        }
        else
        {
            $familia = 'hijo';
        }*/


        return $activityfather_or_son;

    }
    public function listadeProyectosconSOC($cproyecto)
    {
        $project_ingresado = DB::table('tproyecto as tp')
            ->select('tp.codigo')
            ->where('tp.cproyecto', '=', $cproyecto)
            ->first();

        //dd($project_ingresado);
        $codproject = explode('-', $project_ingresado->codigo);

         $listaproyect = DB::table('tproyecto as tp')
             ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
             ->select('tp.codigo','tp.cproyecto','tp.nombre','tum.nombre as nombreunidadMinera','tp.cestadoproyecto')
             ->where('tp.codigo', 'ilike', $codproject[0] . '%')
             ->Where( 'tp.cestadoproyecto','!=','000')
             ->orderBy('tp.codigo', 'ASC')
             ->get();

        return $listaproyect;

    }

    public function listadeProyectossinSOC($cproyecto)
    {
        $projectsinsoc = DB::table('tproyecto as tp')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->select('tp.codigo','tp.cproyecto','tp.nombre','tum.nombre as nombreunidadMinera','tp.cestadoproyecto')
            ->whereIn('tp.cproyecto',$cproyecto)
            ->get();
      
        return $projectsinsoc;

    }


    public function obtenerhoraspresuestadasTotalesHijos($disciplina,$cactividad,$cproyecto)
    {
        $activity_sum = DB::table('tproyectohonorarios as tph')
            ->leftjoin('tproyectoactividades as tpa', 'tpa.cproyectoactividades', '=', 'tph.cproyectoactividades')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as sum"));

        if ($disciplina[0] == 'Nuevo') {

            $activity_sum = $activity_sum->where('tph.cproyectoactividades', '=', $cactividad);
        } else {

            $activity_sum = $activity_sum->whereIn('tph.cdisciplina', $disciplina)
                ->where('tph.cproyectoactividades', '=',$cactividad);
        }

        $activity_sum = $activity_sum->where('tpa.cproyecto', '=',$cproyecto)
            ->first();

        $var = 0;

        if ($activity_sum->sum != null) {
            $var = floatval($activity_sum->sum);
        }

        return $var;
    }
    private function obtenerhorasConsumidasTotalesHijos($fecha_inicial,$fecha_final,$disciplina,$cactividad,$cproyecto)
    {
        $horas_ejecutadas_sum = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->select(DB::raw("sum(tpe.horasejecutadas) as sum"));

        if ($disciplina[0] == 'Nuevo') {

            $horas_ejecutadas_sum = $horas_ejecutadas_sum->where('tpe.cproyectoactividades', '=', $cactividad);
        } else {

            $horas_ejecutadas_sum = $horas_ejecutadas_sum->whereIn('tda.cdisciplina', $disciplina)
                ->where('tpe.cproyectoactividades', '=',$cactividad );
        }


        $horas_ejecutadas_sum = $horas_ejecutadas_sum->where('tpe.cproyecto', '=',$cproyecto)

            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->first();

        $var = 0;

        if ($horas_ejecutadas_sum->sum != null) {
            $var = floatval($horas_ejecutadas_sum->sum);
        }

        return $var;

    }
    public function  formatoFecha($fecha)
    {
        $date = $fecha;
        if (substr($date, 2, 1) == '/' || substr($date, 2, 1) == '-') {
            $date = Carbon::create(substr($date, 6, 4), substr($date, 3, 2), substr($date, 0, 2));

        } else {
            $date = Carbon::create(substr($date, 0, 4), substr($date, 5, 2), substr($date, 8, 2));
        }


        $fecha_convertida = $date->toDateString();;

        return $fecha_convertida;
    }
    public function  formatoFechaNumSemana($fecha)
    {
        $date = $fecha;
        if (substr($date, 2, 1) == '/' || substr($date, 2, 1) == '-') {
            $date = Carbon::create(substr($date, 6, 4), substr($date, 3, 2), substr($date, 0, 2));

        } else {
            $date = Carbon::create(substr($date, 0, 4), substr($date, 5, 2), substr($date, 8, 2));
        }


        $fecha_convertida = $date->weekOfYear;

        return $fecha_convertida;
    }
    private function getAreas($cproyecto)
    {
        $proyectoSOC = $this->listadeProyectosconSOC($cproyecto);

        $arraylP = [];
        foreach ($proyectoSOC as  $proyecto): 
             array_push($arraylP,$proyecto->cproyecto);
        endforeach;

         
        //Areas que intervienes en los consumidos
        $tareaConsumidos = DB::table('tproyectoejecucion as tpe')
                              ->select('tpe.carea')
                              ->groupBy('tpe.carea')
                              ->whereIn('tpe.cproyecto',$arraylP)
                              ->whereNotNull('tpe.carea')
                              ->orderBy('tpe.carea')
                              ->lists('tpe.carea');
      //  dd( $tareaConsumidos);                      

        $tareaPresupuestada =  DB::table('tproyectoestructuraparticipantes as tpe')
                                ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpe.cestructuraproyecto')
                                ->leftjoin('tdisciplina as tdis', 'tdis.cdisciplina', '=', 'tph.cdisciplina')
                                ->leftjoin('tdisciplinaareas as tdisa', 'tdisa.cdisciplina', '=', 'tdis.cdisciplina')
                                ->leftjoin('tareas as ta', 'ta.carea', '=', 'tdisa.carea')
                                ->select('ta.carea')
                                ->whereIn('tpe.cproyecto',$arraylP)
                                ->whereNotNull('ta.carea')
                                ->groupBy('ta.carea')
                                ->orderBy('ta.carea')
                                ->lists('tpe.carea');
                                
        $math =  array_unique(array_merge($tareaConsumidos,  $tareaPresupuestada));

        $detalleMath =  DB::table('tareas as ta')
                        ->leftjoin('tdisciplinaareas as tdisa', 'tdisa.carea', '=', 'ta.carea')
                        ->leftjoin('tdisciplina as tdis', 'tdis.cdisciplina', '=', 'tdisa.cdisciplina')
                        ->select('tdis.cdisciplina as disciplina','ta.descripcion as area','ta.carea','ta.codigo_sig')
                        ->whereIn('ta.carea',$math)
                        ->orderBy('ta.codigo_sig')
                        ->get();

        return $detalleMath;

    }
    public function getAreasTotales()
    {
        $tareas = DB::table('tareas as ta')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->select('tda.cdisciplina')
            ->whereNotNull('tda.cdisciplina')
            ->distinct()
            ->get();

        return $tareas;


    }
    public function getMostrarTodaslasAreasporActividad(Request $request)
    {
       // dd('ola 2');
        $cproyecto     = $request->cproyecto;
        $disciplina    = $request->disciplina;
        $fecha_inicial = $this->formatoFecha($request->fdesde);
        $fecha_final = $this->formatoFecha($request->fhasta);
        $cproyectoactividad   = $request->cproyectoactividades;

        $listadeDisciplina = $this->CapturarAreasTotales($disciplina);
        //dd($listadeDisciplina,$disciplina);
        $horas_ejecutadas_sum = $this->CapturarHorasConsumidosEmpleadosArea($cproyectoactividad,$cproyecto,$listadeDisciplina,$fecha_inicial,$fecha_final);
        $horas_ejecutadas_sum =  $horas_ejecutadas_sum->addSelect('tpe.cproyectoactividades')->get();
        
        // dd($horas_ejecutadas_sum);

        // $capRoles =  $this->capturarRolesPresupuestados(5472,54370,1);
        // dd($capRoles,$horas_presupuestadas_sum);
        // dd($horas_ejecutadas_sum);

        $arrayAreaConsumidas = [];
        foreach ($horas_ejecutadas_sum as $hes)
        {
            $arrayCategorias = [];
            $categoriaprof = $this->capturarcategorias($hes->carea,$cproyecto,$cproyectoactividad);
            // dd($categoriaprof);
            foreach ($categoriaprof as $catp)
            {
                $capro['drilldown'] = intval($catp->ccategoriaprofesional.$hes->carea);
                if($catp->ccategoriaprofesional === 0)
                {
                    $capro['drilldown'] = intval('999'.$hes->carea);
                }

                $empleadosporCategoria =  $this->capturarEmpleados($hes->carea,$cproyecto,$cproyectoactividad);
                $arrayEmpleados = [];
                $sumaporcategoria = 0;

                foreach ($empleadosporCategoria as $eporcategoria)
                {
                    if($catp->ccategoriaprofesional ==  $eporcategoria->ccategoriaprofesional)
                    {
                        $caper['id'] =  intval($eporcategoria->ccategoriaprofesional.$hes->carea);
                        if($catp->ccategoriaprofesional === 0)
                        {
                            $caper['id'] =  intval('999'.$hes->carea);
                        }
                        $caper['name'] =  $eporcategoria->abreviatura;
                        $caper['cpersona'] =  $eporcategoria->cpersona;
                        $caper['y'] =  $this->sumaempleados($cproyectoactividad,$eporcategoria->cpersona,$hes->carea,$fecha_inicial,$fecha_final);
                        $sumaporcategoria +=  $caper['y'];
                        array_push($arrayEmpleados,$caper);
                    }

                }

                $capro['name'] =  $catp->descripcion;
                $capro['carea'] =  $catp->carea;
                $capro['empleadoCategoria'] =  $arrayEmpleados;
                $capro['y'] =  $sumaporcategoria;

                array_push($arrayCategorias,$capro);
            }

            $abd['carea'] = $hes->carea;
            $abd['categorias'] = $arrayCategorias;
            $abd['descripcion'] = $hes->descripcion;
            $abd['sumatorio'] = floatval($hes->sum);
            array_push($arrayAreaConsumidas,$abd);
        }
        //dd($arrayAreaConsumidas);

        // Presupuestadas

        $horas_presupuestadas_sum = $this->CapturarHorasPresupuestadosEmpleadosArea($cproyecto,$cproyectoactividad,$listadeDisciplina);
        $arrayAreaPres = [];
        foreach ($horas_presupuestadas_sum as $hp) {
            $arrayRoles = [];
            $rolesPresupuestadas =  $this->capturarRolesPresupuestados($cproyecto,$cproyectoactividad,$hp->cdisciplina);

            foreach ($rolesPresupuestadas as $rp) {
                // $rolPre['drilldown'] =   intval($rp->croldespliegue.$hp->carea.'-');
                $rolPre['name'] =  $rp->descripcionrol;
                $rolPre['carea'] =  intval($rp->carea.'123');
                $rolPre['empleadoCategoria'] = [

                ];
                $rolPre['y'] = floatval($rp->sumhorasrol);
                //$rolPre['empleadoroles'] =  [];
                array_push($arrayRoles,$rolPre);
            }
            $areaPre['carea'] = intval($hp->carea.'123');
            $areaPre['roles'] = $arrayRoles;
            //$areaPre['categorias'] = $arrayRoles;
            $areaPre['descripcion'] = $hp->descripcion;
            $areaPre['sumatorio'] = floatval($hp->horas);
            array_push($arrayAreaPres,$areaPre);

        }
        //dd($arrayAreaPres);

        $arrayArea = [];
        array_push($arrayArea,[
            'consumidas' => $arrayAreaConsumidas,
            'presupuestadas' => $arrayAreaPres
        ]);
        // dd($arrayArea);

        return $arrayArea;

    }
    public function getMostrarTodaslasAreasporActividadCCHH(Request $request)
    {
        //dd('lolo');
        $cproyecto     = $request->cproyecto;
        $disciplina    = $request->disciplina;
        $fecha_inicial = $this->formatoFecha( $request->fdesde);
        $fecha_final = $this->formatoFecha( $request->fhasta);
        $cproyectoactividad   = $request->cproyectoactividades;


        $listadeDisciplina = $this->CapturarAreasTotales($disciplina);
        $arrayAreaCCHH_New = $this->CapturarMontosConsumidosHH($cproyectoactividad,$cproyecto,$listadeDisciplina,$fecha_inicial,$fecha_final);
        $horasMontoPres = $this->CapturarMontosPresuestadosHH($cproyecto,$cproyectoactividad,$listadeDisciplina);

        $arrayAreaConsumidasMonto = [];
        foreach ($arrayAreaCCHH_New as $hes)
        {
            $arrayCategorias = [];

            $categoriaprof = $this->capturarcategorias($hes['carea'],$cproyecto,$cproyectoactividad);

            foreach ($categoriaprof as $catp)
            {
                $capro['drilldown'] = intval($catp->ccategoriaprofesional. $hes['carea']);
                if($catp->ccategoriaprofesional === 0)
                {
                    $capro['drilldown'] = intval('999'.$hes['carea']);
                }

                $empleadosporCategoria =  $this->capturarEmpleados($hes['carea'],$cproyecto,$cproyectoactividad);
                //dd($empleadosporCategoria);

                $arrayEmpleados = [];
                $sumaporcategoria = 0;
                foreach ($empleadosporCategoria as $eporcategoria)
                {
                    if($catp->ccategoriaprofesional ==  $eporcategoria->ccategoriaprofesional)
                    {
                        $caper['id'] =  intval($eporcategoria->ccategoriaprofesional. $hes['carea']);
                        if($catp->ccategoriaprofesional === 0)
                        {
                            $caper['id'] =  intval('999'.$hes['carea']);
                        }
                        $caper['name'] =  $eporcategoria->abreviatura;
                        $caper['cpersona'] =  $eporcategoria->cpersona;
                        $caper['y'] =  $this->sumaempleadoMonto($cproyectoactividad,$cproyecto,$eporcategoria->cpersona,$fecha_inicial,$fecha_final,$hes['carea']);
                        $sumaporcategoria +=  $caper['y'];
                        array_push($arrayEmpleados,$caper);
                    }

                }
                $capro['name'] =  $catp->descripcion;
                $capro['carea'] =  $catp->carea;
                $capro['empleadoCategoria'] =  $arrayEmpleados;
                $capro['y'] =  $sumaporcategoria;


                array_push($arrayCategorias,$capro);
            }
            $abd['carea'] = $hes['carea'];
            $abd['categorias'] = $arrayCategorias;
            $abd['descripcion'] = $hes['descripcion'];
            $abd['sumatorio'] = floatval($hes['multiporarea']);
            array_push($arrayAreaConsumidasMonto,$abd);
        }

        $arrayAreaPresMonto = [];
        foreach ($horasMontoPres as $hp) {
            $arrayRoles = [];
            $rolesPresupuestadas =  $this->capturarRolesPresupuestados($cproyecto,$cproyectoactividad,$hp['cdisciplina']);

            foreach ($rolesPresupuestadas as $rp) {
                // $rolPre['drilldown'] =   intval($rp->croldespliegue.$hp->carea.'-');
                $rolPre['name'] =  $rp->descripcionrol;
                $rolPre['carea'] =  intval($rp->carea.'123');
                $rolPre['empleadoCategoria'] = [

                ];
                //$roldespliegueSumaMonto = $this->CapturarMontosPresuestadosHHRoles($cproyecto,$cproyectoactividad,$hp['cdisciplina'],$rp->croldespliegue);
                //$rolPre['y'] = floatval($roldespliegueSumaMonto['multiporarea']);
                $rolPre['y'] =  $this->CapturarMontosPresuestadosHHRoles($cproyecto,$cproyectoactividad,$hp['cdisciplina'],$rp->croldespliegue);
                //$rolPre['empleadoroles'] =  [];
                array_push($arrayRoles,$rolPre);
            }
            $areaPre['carea'] = intval($hp['carea'].'123');
            $areaPre['roles'] = $arrayRoles;
            //$areaPre['categorias'] = $arrayRoles;
            $areaPre['descripcion'] = $hp['descripcion'];
            $areaPre['sumatorio'] = floatval($hp['multiporarea']);
            array_push($arrayAreaPresMonto,$areaPre);

        }

        // dd($arrayAreaPres);

        $arrayArea = [];
        array_push($arrayArea,[
            'consumidas' => $arrayAreaConsumidasMonto,
            'presupuestadas' => $arrayAreaPresMonto
        ]);
        // dd($arrayArea);


        return $arrayArea;

    }
    private function  capturarcategorias($carea,$cproyecto,$codigoactividad)
    {

        $CategoriadelArea =  DB::table('tproyectoejecucion as tperd')
            ->leftjoin('tproyectopersona as tpp', 'tpp.cpersona', '=', 'tperd.cpersona_ejecuta')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tpp.ccategoriaprofesional')
            ->select('tcp.ccategoriaprofesional', 'tcp.descripcion','tperd.carea')
            ->orderBy('tcp.descripcion','ASC')
            ->where('tperd.carea',$carea)
           // ->where('tperd.cproyecto', '=', $cproyecto)
            ->where('tpp.cproyecto', '=', $cproyecto)
            ->where('tperd.cproyectoactividades', '=', $codigoactividad)
            //->orderBy('tcp.descripcion','ASC')
            ->whereNotNull('tcp.ccategoriaprofesional')
            ->distinct()
            ->get();

        return $CategoriadelArea;
    }
    private function  capturarcategoriasResumen($carea,$cproyecto)
    {

        $CategoriadelArea =  DB::table('tproyectoejecucion as tperd')
            ->leftjoin('tproyectopersona as tpp', 'tpp.cpersona', '=', 'tperd.cpersona_ejecuta')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tperd.ccategoriaprofesional')
            ->select('tcp.ccategoriaprofesional', 'tcp.descripcion','tperd.carea')
            ->orderBy('tcp.descripcion','ASC')
            ->where('tperd.carea', '=', $carea)
            //->whereIn('tpp.cproyecto', $cproyecto)
            ->whereIn('tperd.cproyecto', $cproyecto)
           // ->where('tperd.cproyecto', '=', $cproyecto)
            //->where('tperd.cproyectoactividades', '=', $codigoactividad)
            //->orderBy('tcp.descripcion','ASC')
            ->whereNotNull('tcp.ccategoriaprofesional')
            ->distinct()
            ->get();

        return $CategoriadelArea;
    }
    private function capturarRolesPresupuestados($cproyecto,$codigoactividad,$disciplina)
    {
        $rolesdelArea =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpyep.croldespliegue')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as tar', 'tar.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as sumhorasrol"),'trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
            ->where('tpyep.cproyecto','=',$cproyecto)
            ->where('tph.cproyectoactividades','=',$codigoactividad)
            ->where('tph.cdisciplina','=',$disciplina)
            ->groupBy('trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
            ->orderBy('trd.descripcionrol','ASC')
            ->get();

        return  $rolesdelArea;

    }

    private function capturarRolesPresupuestadosResumen($cproyecto,$disciplina)
    {
        $rolesdelArea =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpyep.croldespliegue')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as tar', 'tar.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as sumhorasrol"),'trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
            ->whereIn('tpyep.cproyecto',$cproyecto)
            //->where('tph.cproyectoactividades','=',$codigoactividad)
            ->where('tph.cdisciplina','=',$disciplina)
            ->groupBy('trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
            ->orderBy('trd.descripcionrol','ASC')
            ->get();

        return  $rolesdelArea;

    }


    private function capturarRolesPresupuestadosResumenSOC($cproyecto,$disciplina)
    {
        $rolesdelArea =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpyep.croldespliegue')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as tar', 'tar.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as sumhorasrol"),'trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
            ->whereIn('tpyep.cproyecto',$cproyecto)
            //->where('tph.cproyectoactividades','=',$codigoactividad)
            ->where('tph.cdisciplina','=',$disciplina)
            ->groupBy('trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
            ->orderBy('trd.descripcionrol','ASC')
            ->get();

        return  $rolesdelArea;

    }

    /*    private function capturarRolesPresupuestadosResumen($cproyecto,$disciplina)
        {
            $rolesdelArea =  DB::table('tproyectoestructuraparticipantes as tpyep')
                ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
                ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpyep.croldespliegue')
                ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
                ->leftjoin('tareas as tar', 'tar.carea', '=', 'tda.carea')
                ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as sumhorasrol"),'trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
                ->where('tpyep.cproyecto','=',$cproyecto)
                //->where('tph.cproyectoactividades','=',$codigoactividad)
                ->where('tph.cdisciplina','=',$disciplina)
                ->groupBy('trd.croldespliegue','trd.descripcionrol','tph.cdisciplina','tar.descripcion','tar.carea')
                ->orderBy('trd.descripcionrol','ASC')
                ->get();

            return  $rolesdelArea;

        }*/
    private function  capturarEmpleados($carea,$cproyecto,$codigoactividad)
    {


        $CategoriadelArea =  DB::table('tproyectoejecucion as tperd')
            ->leftjoin('tpersona as tper', 'tper.cpersona', '=', 'tperd.cpersona_ejecuta')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tperd.ccategoriaprofesional')
            ->select('tperd.cpersona_ejecuta as cpersona', 'tperd.ccategoriaprofesional','tcp.descripcion','tper.abreviatura')
            // ->orderBy('tcp.descripcion','ASC')
            ->where('tperd.carea', $carea)
            ->where('tperd.cproyecto', '=', $cproyecto)
            ->where('tperd.cproyectoactividades', '=', $codigoactividad)
            //->whereNotNull('tcp.ccategoriaprofesional')
            ->distinct()
            ->get();

        return $CategoriadelArea;
    }
    private function  capturarEmpleadosResumen($carea,$cproyecto)
    {
        $CategoriadelArea =  DB::table('tproyectoejecucion as tperd')
            ->leftjoin('tpersona as tper', 'tper.cpersona', '=', 'tperd.cpersona_ejecuta')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tperd.ccategoriaprofesional')
            ->select('tperd.cpersona_ejecuta as cpersona', 'tperd.ccategoriaprofesional','tcp.descripcion','tper.abreviatura')
            // ->orderBy('tcp.descripcion','ASC')
            ->where('tperd.carea', '=', $carea)
            ->whereIn('tperd.cproyecto', $cproyecto)
            //->whereNotNull('tcp.ccategoriaprofesional')
            ->distinct()
            ->get();

        return $CategoriadelArea;
    }
    private function CapturarHorasConsumidosEmpleadosArea($cproyectoactividad,$cproyecto,$listadeDisciplina,$fecha_inicial,$fecha_final)
    {
        $horas_ejecutadas_sum = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->select(DB::raw(" sum(tpe.horasejecutadas) as sum"),'ta.descripcion','ta.carea','tda.cdisciplina'/*,'tpe.cproyectoactividades'*/)
            ->where('tpe.cproyectoactividades', '=', $cproyectoactividad)
            ->where('tpe.cproyecto','=',$cproyecto)
            ->whereIn('tda.cdisciplina', $listadeDisciplina )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->whereNotNull('ta.descripcion')
            ->orderBy('ta.descripcion','ASC')
            ->groupBy('ta.descripcion','ta.carea','tda.cdisciplina','tpe.cproyectoactividades')
            ->havingRaw('sum(tpe.horasejecutadas)>0');
        //->get();
        return $horas_ejecutadas_sum;
    }
    private function CapturarHorasConsumidosEmpleadosAreaFinal($cproyectoactividad,$listadeDisciplina,$fecha_inicial,$fecha_final)
    {
        $horas_ejecutadas_sum = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->select(DB::raw(" sum(tpe.horasejecutadas) as sum"),'ta.descripcion','ta.carea','tda.cdisciplina'/*,'tpe.cproyectoactividades'*/)
            ->where('tpe.cproyectoactividades', '=', $cproyectoactividad)
            ->whereIn('tda.cdisciplina', $listadeDisciplina )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->whereNotNull('ta.descripcion')
            ->orderBy('ta.descripcion','ASC')
            ->groupBy('ta.descripcion','ta.carea','tda.cdisciplina','tpe.cproyectoactividades')
            ->havingRaw('sum(tpe.horasejecutadas)>0');
        //->get();
        return $horas_ejecutadas_sum;
    }

    private function CapturarHorasPresupuestadosEmpleadosArea($cproyecto,$cproyectoactividad,$listadeDisciplina)
    {
        $horas_ejecutadas_sum_Presupuestadas =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as horas"),'ta.descripcion','ta.carea','tph.cdisciplina','tph.cproyectoactividades')
            ->where('tpyep.cproyecto','=',$cproyecto)
            ->where('tph.cproyectoactividades','=',$cproyectoactividad)
            ->whereIn('tph.cdisciplina', $listadeDisciplina )
            ->groupBy('ta.descripcion','ta.carea','tph.cdisciplina','tph.cproyectoactividades')
            ->get();
        return $horas_ejecutadas_sum_Presupuestadas;

    }

    private function CapturarHorasPresupuestadosEmpleadosAreaResumen($cproyecto,$listadeDisciplina)
    {
        $horas_ejecutadas_sum_Presupuestadas =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as horas"),'ta.descripcion','ta.carea','tph.cdisciplina')
            ->where('tpyep.cproyecto','=',$cproyecto)
            // ->where('tph.cproyectoactividades','=',$cproyectoactividad)
            ->whereIn('tph.cdisciplina', $listadeDisciplina )
            ->groupBy('ta.descripcion','ta.carea','tph.cdisciplina')
            //->distinct()
            ->get();
        return $horas_ejecutadas_sum_Presupuestadas;

    }



    private function CapturarMontosConsumidosHH($cproyectoactividad,$cproyecto,$listadeDisciplina,$fecha_inicial,$fecha_final)
    {
        $rateshorasejecutadasCargadas = DB::table('tproyectoejecucion as tpe')
            //->leftjoin('tproyectoactividades as tpa', 'tpe.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tpe.ccategoriaprofesional')
            ->select('tpe.cproyectoactividades',DB::raw(" sum(tpe.horasejecutadas) as sum"),'tcp.rate','ta.descripcion','ta.carea','tda.cdisciplina')
            ->where('tpe.cproyectoactividades', '=',$cproyectoactividad)
            ->where('tpe.cproyecto', '=',$cproyecto)
            ->whereIn('tda.cdisciplina', $listadeDisciplina )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->groupBy('tpe.cproyectoactividades','tcp.rate','ta.descripcion','ta.carea','tda.cdisciplina')
            ->orderBy('ta.descripcion','ASC')
            ->distinct()
            ->get();


        $arrayAreaCCHH = [];
        foreach ($rateshorasejecutadasCargadas as $s_chh)
        {
            $catg['carea'] = $s_chh->carea;
            $catg['descripcion'] = $s_chh->descripcion;
            $catg['cdisciplina'] = $s_chh->cdisciplina;
            $sumahoraspresuestadasCargadas = $s_chh->sum * $s_chh->rate;
            $catg['multiporarea'] =$sumahoraspresuestadasCargadas;
            array_push($arrayAreaCCHH,$catg);

        }
        // dd($rateshorasejecutadasCargadas,$arrayAreaCCHH);

        $arrayAreaCCHH_New = array();
        foreach($arrayAreaCCHH as $t) {
            $repeat=false;
            for($i=0;$i<count($arrayAreaCCHH_New);$i++)
            {
                if($arrayAreaCCHH_New[$i]['carea']==$t['carea'])
                {
                    $arrayAreaCCHH_New[$i]['multiporarea']+=$t['multiporarea'];
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $arrayAreaCCHH_New[] = array(
                    'carea' => $t['carea'],
                    'descripcion' => $t['descripcion'],
                    'cdisciplina' => $t['cdisciplina'],
                    'multiporarea' => $t['multiporarea'],
                );
        }

        return $arrayAreaCCHH_New;
    }

    private function CapturarResumenMontosConsumidosHH($cproyectoactividad,$cproyecto,$listadeDisciplina,$fecha_inicial,$fecha_final)
    {
        $rateshorasejecutadasCargadas = DB::table('tproyectoejecucion as tpe')
            //->leftjoin('tproyectoactividades as tpa', 'tpe.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tpe.ccategoriaprofesional')
            ->select(DB::raw(" sum(tpe.horasejecutadas) as sum"),'tcp.rate','ta.descripcion','ta.carea','tda.cdisciplina')
            ->where('tpe.cproyectoactividades', '=',$cproyectoactividad)
            ->where('tpe.cproyecto', '=',$cproyecto)
            ->whereIn('tda.cdisciplina', $listadeDisciplina )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->groupBy('tcp.rate','ta.descripcion','ta.carea','tda.cdisciplina')
            ->orderBy('ta.descripcion','ASC')
            ->distinct()
            ->get();


        $arrayAreaCCHH = [];
        foreach ($rateshorasejecutadasCargadas as $s_chh)
        {
            $catg['carea'] = $s_chh->carea;
            $catg['descripcion'] = $s_chh->descripcion;
            $catg['cdisciplina'] = $s_chh->cdisciplina;
            $sumahoraspresuestadasCargadas = $s_chh->sum * $s_chh->rate;
            $catg['multiporarea'] =$sumahoraspresuestadasCargadas;
            // $catg['sum'] = $s_chh->sum;

            array_push($arrayAreaCCHH,$catg);

        }
        // dd($rateshorasejecutadasCargadas,$arrayAreaCCHH);

        $arrayAreaCCHH_New = array();
        foreach($arrayAreaCCHH as $t) {
            $repeat=false;
            for($i=0;$i<count($arrayAreaCCHH_New);$i++)
            {
                if($arrayAreaCCHH_New[$i]['carea']==$t['carea'])
                {
                    $arrayAreaCCHH_New[$i]['multiporarea']+=$t['multiporarea'];
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $arrayAreaCCHH_New[] = array(
                    'carea' => $t['carea'],
                    'descripcion' => $t['descripcion'],
                    'cdisciplina' => $t['cdisciplina'],
                    'multiporarea' => $t['multiporarea'],
                    // 'sum' => $t['sum']

                );
        }

        return $arrayAreaCCHH_New;
    }

    private function CapturarResumenMontosConsumidosHHFinal($cproyectoactividad,$listadeDisciplina,$fecha_inicial,$fecha_final)
    {
        $rateshorasejecutadasCargadas = DB::table('tproyectoejecucion as tpe')
            //->leftjoin('tproyectoactividades as tpa', 'tpe.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tpe.ccategoriaprofesional')
            ->select(DB::raw(" sum(tpe.horasejecutadas) as sum"),'tcp.rate','ta.descripcion','ta.carea','tda.cdisciplina')
            ->where('tpe.cproyectoactividades', '=',$cproyectoactividad)
           
            ->whereIn('tda.cdisciplina', $listadeDisciplina )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->groupBy('tcp.rate','ta.descripcion','ta.carea','tda.cdisciplina')
            ->orderBy('ta.descripcion','ASC')
            ->distinct()
            ->get();


        $arrayAreaCCHH = [];
        foreach ($rateshorasejecutadasCargadas as $s_chh)
        {
            $catg['carea'] = $s_chh->carea;
            $catg['descripcion'] = $s_chh->descripcion;
            $catg['cdisciplina'] = $s_chh->cdisciplina;
            $sumahoraspresuestadasCargadas = $s_chh->sum * $s_chh->rate;
            $catg['multiporarea'] =$sumahoraspresuestadasCargadas;
            // $catg['sum'] = $s_chh->sum;

            array_push($arrayAreaCCHH,$catg);

        }
        // dd($rateshorasejecutadasCargadas,$arrayAreaCCHH);

        $arrayAreaCCHH_New = array();
        foreach($arrayAreaCCHH as $t) {
            $repeat=false;
            for($i=0;$i<count($arrayAreaCCHH_New);$i++)
            {
                if($arrayAreaCCHH_New[$i]['carea']==$t['carea'])
                {
                    $arrayAreaCCHH_New[$i]['multiporarea']+=$t['multiporarea'];
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $arrayAreaCCHH_New[] = array(
                    'carea' => $t['carea'],
                    'descripcion' => $t['descripcion'],
                    'cdisciplina' => $t['cdisciplina'],
                    'multiporarea' => $t['multiporarea'],
                    // 'sum' => $t['sum']

                );
        }

        return $arrayAreaCCHH_New;
    }

    private function CapturarMontosPresuestadosHH($cproyecto,$cproyectoactividad,$listadeDisciplina)
    {
        $horas_ejecutadas_sum_Presupuestadas =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as horas"),'ta.descripcion','ta.carea','tph.cdisciplina','tph.cproyectoactividades','tpyep.rate')
            ->where('tpyep.cproyecto','=',$cproyecto)
            ->where('tph.cproyectoactividades','=',$cproyectoactividad)
            ->whereIn('tph.cdisciplina', $listadeDisciplina )
            ->groupBy('ta.descripcion','ta.carea','tph.cdisciplina','tph.cproyectoactividades','tpyep.rate')
            ->get();

        $arrayAreaCCHH = [];
        foreach ($horas_ejecutadas_sum_Presupuestadas as $s_phh)
        {
            $catgP['carea'] = $s_phh->carea;
            $catgP['descripcion'] = $s_phh->descripcion;
            $catgP['cdisciplina'] = $s_phh->cdisciplina;
            $sumahoraspresuestadasCargadas = $s_phh->horas * $s_phh->rate;
            $catgP['multiporarea'] =$sumahoraspresuestadasCargadas;
            array_push($arrayAreaCCHH,$catgP);

        }

        $arrayAreaCCHHMonto_New = array();
        foreach($arrayAreaCCHH as $t) {
            $repeat=false;
            for($i=0;$i<count($arrayAreaCCHHMonto_New);$i++)
            {
                if($arrayAreaCCHHMonto_New[$i]['carea']==$t['carea'])
                {
                    $arrayAreaCCHHMonto_New[$i]['multiporarea']+=$t['multiporarea'];
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $arrayAreaCCHHMonto_New[] = array(
                    'carea' => $t['carea'],
                    'descripcion' => $t['descripcion'],
                    'cdisciplina' => $t['cdisciplina'],
                    'multiporarea' => $t['multiporarea'],
                );
        }

        return $arrayAreaCCHHMonto_New;

        //return $horas_ejecutadas_sum_Presupuestadas;

    }

    private function CapturarMontosPresuestadosHHResumen($cproyecto,$listadeDisciplina)
    {
        $horas_ejecutadas_sum_Presupuestadas =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as horas"),'ta.descripcion','ta.carea','tph.cdisciplina','tpyep.rate')
            ->where('tpyep.cproyecto','=',$cproyecto)
            //->where('tph.cproyectoactividades','=',$cproyectoactividad)
            ->whereIn('tph.cdisciplina', $listadeDisciplina )
            ->groupBy('ta.descripcion','ta.carea','tph.cdisciplina','tpyep.rate')
            ->get();

        $arrayAreaCCHH = [];
        foreach ($horas_ejecutadas_sum_Presupuestadas as $s_phh)
        {
            $catgP['carea'] = $s_phh->carea;
            $catgP['descripcion'] = $s_phh->descripcion;
            $catgP['cdisciplina'] = $s_phh->cdisciplina;
            $sumahoraspresuestadasCargadas = $s_phh->horas * $s_phh->rate;
            $catgP['multiporarea'] =$sumahoraspresuestadasCargadas;
            array_push($arrayAreaCCHH,$catgP);

        }

        $arrayAreaCCHHMonto_New = array();
        foreach($arrayAreaCCHH as $t) {
            $repeat=false;
            for($i=0;$i<count($arrayAreaCCHHMonto_New);$i++)
            {
                if($arrayAreaCCHHMonto_New[$i]['carea']==$t['carea'])
                {
                    $arrayAreaCCHHMonto_New[$i]['multiporarea']+=$t['multiporarea'];
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $arrayAreaCCHHMonto_New[] = array(
                    'carea' => $t['carea'],
                    'descripcion' => $t['descripcion'],
                    'cdisciplina' => $t['cdisciplina'],
                    'multiporarea' => $t['multiporarea'],
                );
        }

        return $arrayAreaCCHHMonto_New;

        //return $horas_ejecutadas_sum_Presupuestadas;

    }


    private function CapturarMontosPresuestadosHHRoles($cproyecto,$cproyectoactividad,$listadeDisciplina,$roldliegue)
    {
        $horas_ejecutadas_sum_Presupuestadas =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as horas"),'ta.descripcion','ta.carea','tph.cdisciplina','tph.cproyectoactividades','tpyep.rate','tpyep.croldespliegue')
            ->where('tpyep.cproyecto','=',$cproyecto)
            ->where('tph.cproyectoactividades','=',$cproyectoactividad)
            ->where('tph.cdisciplina','=',$listadeDisciplina )
            ->where('tpyep.croldespliegue','=',$roldliegue )
            ->groupBy('ta.descripcion','ta.carea','tph.cdisciplina','tph.cproyectoactividades','tpyep.rate','tpyep.croldespliegue')
            ->first();

        $mullti = 0;
        if (!$horas_ejecutadas_sum_Presupuestadas){

            $mullti = 0;

        }
        else
        {
            if ($horas_ejecutadas_sum_Presupuestadas->horas != null)
            {
                $mullti = floatval($horas_ejecutadas_sum_Presupuestadas->horas * $horas_ejecutadas_sum_Presupuestadas->rate);
            }
        }


        return $mullti;

    }

    private function CapturarMontosPresuestadosHHRolesResumen($cproyecto,$listadeDisciplina,$roldliegue)
    {
        $horas_ejecutadas_sum_Presupuestadas =  DB::table('tproyectoestructuraparticipantes as tpyep')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpyep.cestructuraproyecto')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tph.cdisciplina')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as horas"),'ta.descripcion','ta.carea','tph.cdisciplina','tpyep.rate','tpyep.croldespliegue')
            ->whereIn('tpyep.cproyecto',$cproyecto)
            //->where('tph.cproyectoactividades','=',$cproyectoactividad)
            ->where('tph.cdisciplina','=',$listadeDisciplina )
            ->where('tpyep.croldespliegue','=',$roldliegue )
            ->groupBy('ta.descripcion','ta.carea','tph.cdisciplina','tpyep.rate','tpyep.croldespliegue')
            ->first();

        $mullti = 0;
        if (!$horas_ejecutadas_sum_Presupuestadas){

            $mullti = 0;

        }
        else
        {
            if ($horas_ejecutadas_sum_Presupuestadas->horas != null)
            {
                $mullti = floatval($horas_ejecutadas_sum_Presupuestadas->horas * $horas_ejecutadas_sum_Presupuestadas->rate);
            }
        }


        return $mullti;

    }

    private function CapturarAreasTotales($disciplina)
    {
        $listarAreas = $this->getAreasTotales();
        $listadeDisciplina = [];
        if ($disciplina[0] == 'Nuevo') {
            foreach ($listarAreas as $la => $value)
            {
               // $areadisc['cdisciplina'] = $la->cdisciplina;
                array_push($listadeDisciplina,$value->cdisciplina);
            }

            return $listadeDisciplina;
        }
        else
        {
           return $disciplina;
        }
       

    }
    private function sumaempleados($cproyectoactividades,$user,$carea,$fecha_inicial,$fecha_final)
    {
        $sumporEmpleado =  DB::table('tproyectoejecucion as tpe')
            ->where('tpe.carea',$carea)
            ->where('tpe.cpersona_ejecuta', '=', $user)
            ->where('tpe.cproyectoactividades', '=', $cproyectoactividades)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->select(DB::raw("sum(tpe.horasejecutadas) as sum"))
            ->first();

        $var = 0;

        if ($sumporEmpleado->sum != null) {
            $var = floatval($sumporEmpleado->sum);
        }

        return $var;
    }

    private function sumaempleadosResumen($cproyecto,$user,$carea,$fecha_inicial,$fecha_final)
    {
        $sumporEmpleado =  DB::table('tproyectoejecucion as tpe')
            ->where('tpe.carea', '=', $carea)
            ->where('tpe.cpersona_ejecuta', '=', $user)
            ->whereIn('tpe.cproyecto', $cproyecto)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->select(DB::raw("sum(tpe.horasejecutadas) as sum"))
            ->first();

        $var = 0;

        if ($sumporEmpleado->sum != null) {
            $var = floatval($sumporEmpleado->sum);
        }

        return $var;
    }

    private function sumaempleadoMonto($cproyectoactividades,$cproyecto,$user,$fecha_inicial,$fecha_final,$carea)
    {
        $sumporEmpleado =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tpe.ccategoriaprofesional')
            ->select('tpe.cproyectoactividades',DB::raw(" sum(tpe.horasejecutadas) as sum"),'tpe.carea','tpe.cpersona_ejecuta','tcp.rate')
            ->where('tpe.cproyectoactividades', '=',$cproyectoactividades)
            ->where('tpe.cpersona_ejecuta', '=', $user)
            ->where('tpe.cproyecto', '=',$cproyecto )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->where('tpe.carea', '=', $carea)
            ->groupBy('tpe.cproyectoactividades','tpe.carea','tpe.cpersona_ejecuta','tcp.rate')
            ->first();



        $mullti = $sumporEmpleado ? floatval($sumporEmpleado->sum * $sumporEmpleado->rate) : 0;
        //dd($mullti);


        return $mullti;
    }


    private function sumaempleadoMontoResumen($cproyecto,$user,$carea,$fecha_inicial,$fecha_final)
    {
        $sumporEmpleado =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tpe.ccategoriaprofesional')
            ->select('tpe.cproyecto',DB::raw(" sum(tpe.horasejecutadas) as sum"),'tpe.carea','tpe.cpersona_ejecuta','tcp.rate')
            //->whereIn('tpe.cproyectoactividades',$cproyectoactividades)
            ->where('tpe.cpersona_ejecuta', '=', $user)
            ->whereIn('tpe.cproyecto',$cproyecto )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->where('tpe.carea', '=', $carea)
            ->groupBy('tpe.cproyecto','tpe.carea','tpe.cpersona_ejecuta','tcp.rate')
            ->first();

        /* $obtenerRate = DB::table('tproyectopersona as tper')
             ->leftjoin('tcategoriaprofesional as tcp', 'tper.ccategoriaprofesional', '=', 'tcp.ccategoriaprofesional')
             ->where('tper.cproyecto', '=',$cproyecto )
             ->where('tper.cpersona', '=', $user)
             ->select('tcp.rate')
             ->first();*/


        $mullti = $sumporEmpleado ? floatval($sumporEmpleado->sum * $sumporEmpleado->rate) : 0;
        //dd($mullti);


        return $mullti;
    }

    private function obtenerRateHorasPresupuestadas($cactividad, $disciplina)
    {
        $sumatoria_rate_hora = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->select(DB::raw("to_number(tph.horas, '999999.99') as horas"), 'tpep.rate','tph.cproyectoactividades','tph.cdisciplina');


        if ($disciplina[0] == 'Nuevo') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tpa.cproyectoactividades', '=', $cactividad);
        } else {

            $sumatoria_rate_hora = $sumatoria_rate_hora->whereIn('tph.cdisciplina', $disciplina)
                ->where('tph.cproyectoactividades', '=', $cactividad);
        }

        $sumatoria_rate_hora = $sumatoria_rate_hora->get();

        $suma_horaspresuestadas_activity = 0;
        foreach ($sumatoria_rate_hora as $s_r)
        {
            $sumahoraspresuestadas = $s_r->horas * $s_r->rate;
            $suma_horaspresuestadas_activity +=$sumahoraspresuestadas;
        }

        return $suma_horaspresuestadas_activity;

    }
    private function obtenerRateHorasCargadas ($fecha_inicial,$fecha_final,$cactividad,$cproyecto,$disciplina)
    {

        $rateshorasejecutadasCargadas = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tproyectoactividades as tpa', 'tpe.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            //->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
            //->leftjoin('tproyectopersona as tpp', 'tp.cpersona', '=', 'tpp.cpersona')
            ->leftjoin('tcategoriaprofesional as tcp', 'tcp.ccategoriaprofesional', '=', 'tpe.ccategoriaprofesional')

            ->select('tpe.cproyectoactividades','tpe.horasejecutadas','tcp.rate');

        if ($disciplina[0] == 'Nuevo') {

            $rateshorasejecutadasCargadas = $rateshorasejecutadasCargadas->where('tpe.cproyectoactividades', '=',$cactividad);

        } else {

            $rateshorasejecutadasCargadas = $rateshorasejecutadasCargadas->where('tpe.cproyectoactividades', '=',$cactividad)
                ->whereIn('tda.cdisciplina',$disciplina);
        }

        //  $rateshorasejecutadasCargadas = $rateshorasejecutadasCargadas->where('tpe.cproyecto', '=',$cproyecto)
        $rateshorasejecutadasCargadas = $rateshorasejecutadasCargadas->where('tpe.cproyecto', '=',$cproyecto )
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final)
            ->get();

        $suma_horasCargadas_activity = 0;

        foreach ($rateshorasejecutadasCargadas as $s_c)
        {
            $sumahoraspresuestadasCargadas = $s_c->horasejecutadas * $s_c->rate;

            $suma_horasCargadas_activity +=$sumahoraspresuestadasCargadas;
        }

        return $suma_horasCargadas_activity;


    }
    private function obtenerCabeceraCategoriasHorasPresupuestadas($cproyecto, $cdisciplina)
    {

        /*----------  Lista de cabezera  ----------*/

        $cabecera_horaspropuestas = DB::table('tproyectoestructuraparticipantes as tpe')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpe.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpe.croldespliegue')
            ->select('tpe.croldespliegue', 'trd.descripcionrol', 'trd.codigorol');

        if ($cdisciplina[0] == 'Nuevo') {
            $cabecera_horaspropuestas = $cabecera_horaspropuestas->where('tpe.cproyecto', '=', $cproyecto);
        } else {

            $cabecera_horaspropuestas = $cabecera_horaspropuestas->where('tpe.cproyecto', '=', $cproyecto)
                ->whereIn('tph.cdisciplina', $cdisciplina);
        }

        $cabecera_horaspropuestas = $cabecera_horaspropuestas->groupBy('tpe.croldespliegue', 'trd.descripcionrol', 'trd.codigorol')
            ->orderBy('trd.codigorol', 'ASC')
            ->get();


        return $cabecera_horaspropuestas;

        /*----------  End Lista de cabezera  ----------*/

    }
    public function numdoc($cproyecto){

        $listaentregables = $this->ultimorevision($cproyecto);
        $numdoc =  DB::table('tentregables as tentreg')
            ->whereIn('centregables',$listaentregables)
            ->where('ctipoentregable','=','001')
            ->count();
        return $numdoc;
    }
    public function numpla($cproyecto){
        $listaentregables = $this->ultimorevision($cproyecto);
        $numplan =  DB::table('tentregables as tentreg')
            ->whereIn('centregables',$listaentregables)
            ->where('ctipoentregable','=','002')
            ->count();
        return $numplan;
    }
    public function ultimorevision($cproyecto){
        $ultimarevision =  DB::table('tproyectoedt')
            ->select('cproyectoent_rev')
            ->where('cproyecto','=',$cproyecto)
            ->orderBy('cproyectoent_rev', 'desc')
            ->take(1)
            ->first();

        $listaedtultimarevision =  DB::table('tproyectoedt')
            ->select('cproyecto','cproyectoedt')
            ->where('cproyecto','=',$cproyecto)
            ->where('cproyectoent_rev','=',$ultimarevision ? $ultimarevision->cproyectoent_rev : null)
            ->lists('cproyectoedt');

        $listaentregables =  DB::table('tproyectoentregables')
            ->select('centregable')
            ->whereIn('cproyectoedt',$listaedtultimarevision)
            ->lists('centregable');

        return $listaentregables;

    }
    public function numdocPresupuestados($cproyecto)
    {
        $numdocPresupuestados = DB::table('tpropuesta as tprop')
            ->leftjoin('tpropuestaactividades as tpa', 'tpa.cpropuesta', '=', 'tprop.cpropuesta')
            ->where('tprop.cproyecto','=',$cproyecto)
            ->select(DB::raw("sum(to_number(tpa.numerodocumentos, '999.99')) as numerodocumentos"))
            ->first();
        // dd($numdocPresupuestados);
        return $numdocPresupuestados->numerodocumentos;

    }
    public function numplaPresupuestados($cproyecto)
    {
        $numplaPresupuestados = DB::table('tpropuesta as tprop')
            ->leftjoin('tpropuestaactividades as tpa', 'tpa.cpropuesta', '=', 'tprop.cpropuesta')
            ->where('tprop.cproyecto','=',$cproyecto)
            ->select(DB::raw("sum(to_number(tpa.numeroplanos, '999.99')) as numeroplanos"))
            ->first();

        return $numplaPresupuestados->numeroplanos;

    }
    public function obtenerhoraspresuestadasResumenHH($disciplina,$cproyecto)
    {
        $activity_sum = DB::table('tproyectohonorarios as tph')
            ->leftjoin('tproyectoactividades as tpa', 'tpa.cproyectoactividades', '=', 'tph.cproyectoactividades')
            ->select(DB::raw("sum(to_number(tph.horas, '999999.99')) as sum"))
            ->where('tpa.cproyecto', '=',$cproyecto);
            if ($disciplina != 'Nuevo') {

                $activity_sum = $activity_sum->where('tph.cdisciplina','=',$disciplina);
            }
        $activity_sum = $activity_sum->first();
        $var = 0;

        if ($activity_sum->sum != null) {
            $var = floatval($activity_sum->sum);
        }
        return $var;
    }
    private function obtenerhorasConsumidasResumenHH($fecha_inicial,$fecha_final,$disciplina,$cproyecto)
    {
        $horas_ejecutadas_sum = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tdisciplinaareas as tda', 'tpe.carea', '=','tda.carea')
            ->select(DB::raw("sum(tpe.horasejecutadas) as sum"))
            ->where('tpe.cproyecto', '=',$cproyecto)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'>=',$fecha_inicial)
            ->where(DB::raw("to_char(tpe.fplanificado,'yyyy-mm-dd')"),'<=',$fecha_final);
            if ($disciplina != 'Nuevo') {

                $horas_ejecutadas_sum = $horas_ejecutadas_sum->where('tda.cdisciplina','=',$disciplina);
            }
            $horas_ejecutadas_sum = $horas_ejecutadas_sum->first();

        $var = 0;
        if ($horas_ejecutadas_sum->sum != null) {
            $var = floatval($horas_ejecutadas_sum->sum);
        }

        return $var;

    }

    public function getMostrarResumenProyecto(Request $request)
    {
       // dd("123");
        $cproyecto     = $request->cproyecto;
        $disciplina    = $request->disciplina;
        //$disciplina    = 'Nuevo';
        $fecha_inicial = $this->formatoFecha( $request->fdesde);
        $fecha_final = $this->formatoFecha( $request->fhasta);

       /* $categoriaprofResu = $this->capturarcategoriasResumen(53,5472);
        dd($categoriaprofResu);*/
        $cproyectoArray  = explode(',', $cproyecto);
        //dd($cproyectoArray);

        $listadeDisciplina = $this->CapturarAreasTotales($disciplina);
        $listadActividadesSoloHijas = $this->getActividadesHijosProyecto($cproyecto);
       //dd($listadActividadesSoloHijas);

        $listadeActividades = [];
        foreach ($listadActividadesSoloHijas as $key => $lactividad){
            $horas_ejecutadas_sum = $this->CapturarHorasConsumidosEmpleadosArea($lactividad,$cproyecto,$listadeDisciplina,$fecha_inicial,$fecha_final);
            $horas_ejecutadas_sum = $horas_ejecutadas_sum->get();


            if ($horas_ejecutadas_sum) {
                array_push($listadeActividades,$horas_ejecutadas_sum);
            }
        }
       // dd($listadeActividades);
        $merge = [];
        foreach ($listadeActividades as $lis) {
            foreach ($lis as $indlist):
                array_push($merge,$indlist);
            endforeach;
        }
        $result = array();
        foreach($merge as $t) {
            $repeat=false;
            for($i=0;$i<count($result);$i++)
            {
                if($result[$i]['descripcion']==$t->descripcion && $result[$i]['carea']==$t->carea && $result[$i]['cdisciplina']==$t->cdisciplina)
                {
                    $result[$i]['sum']+= floatval($t->sum) ;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result[] = array('descripcion' => $t->descripcion,
                    'carea' => $t->carea,
                    'cdisciplina' => $t->cdisciplina,
                    'sum' => floatval($t->sum));
        }

       // dd($result);
        // Armar el array para el grafico
        $arrayAreaConsumidasResumen = [];


        foreach ($result as $ar):
            $arrayCategorias = [];
            $categoriaprofResu = $this->capturarcategoriasResumen($ar['carea'],$cproyectoArray);
                foreach ($categoriaprofResu as $catP):
                $categP['drilldown'] = intval($catP->ccategoriaprofesional.$ar['carea']);
                if($catP->ccategoriaprofesional === 0)
                {
                    $categP['drilldown'] = intval('999'.$ar['carea']);
                }
                $empleadosporCategoria =  $this->capturarEmpleadosResumen($ar['carea'],$cproyectoArray);
                $arrayEmpleados = [];
                $sumaporcategoria = 0;

                foreach ($empleadosporCategoria as $eporcategoria)
                {
                    if($catP->ccategoriaprofesional ==  $eporcategoria->ccategoriaprofesional)
                    {
                        $caper['id'] =  intval($eporcategoria->ccategoriaprofesional.$ar['carea']);
                        if($catP->ccategoriaprofesional === 0)
                        {
                            $caper['id'] =  intval('999'.$ar['carea']);
                        }
                        $caper['name'] =  $eporcategoria->abreviatura;
                        $caper['cpersona'] =  $eporcategoria->cpersona;
                        $caper['y'] =  $this->sumaempleadosResumen($cproyectoArray,$eporcategoria->cpersona,$ar['carea'],$fecha_inicial,$fecha_final);
                        $sumaporcategoria +=  $caper['y'];
                        array_push($arrayEmpleados,$caper);
                    }

                }

                $categP['name'] =  $catP->descripcion;
                $categP['carea'] =  $catP->carea;
                $categP['empleadoCategoria'] =  $arrayEmpleados;
                $categP['y'] =  $sumaporcategoria;

                array_push($arrayCategorias,$categP);

            endforeach;
            $abd['carea'] = $ar['carea'];
            $abd['categorias'] =$arrayCategorias;
            $abd['descripcion'] = $ar['descripcion'];
            $abd['sumatorio'] = floatval($ar['sum']);
            array_push($arrayAreaConsumidasResumen,$abd);
        endforeach;
       // dd($arrayAreaConsumidasResumen);
        

        $horas_presupuestadas_sum = $this->CapturarHorasPresupuestadosEmpleadosAreaResumen($cproyecto,$listadeDisciplina);
        //$horas_presupuestadas_sum = $this->CapturarHorasPresupuestadosEmpleadosAreaResumen(5472,$listadeDisciplina);
        // dd($horas_presupuestadas_sum);

        $arrayAreaPres = [];
        foreach ($horas_presupuestadas_sum as $hp) {
            $arrayRoles = [];
            $rolesPresupuestadas =  $this->capturarRolesPresupuestadosResumen($cproyectoArray,$hp->cdisciplina);

            foreach ($rolesPresupuestadas as $rp) {
                // $rolPre['drilldown'] =   intval($rp->croldespliegue.$hp->carea.'-');
                $rolPre['name'] =  $rp->descripcionrol;
                $rolPre['carea'] =  intval($rp->carea.'123');
                $rolPre['empleadoCategoria'] = [

                ];
                $rolPre['y'] = floatval($rp->sumhorasrol);
                //$rolPre['empleadoroles'] =  [];
                array_push($arrayRoles,$rolPre);
            }
            $areaPre['carea'] = intval($hp->carea.'123');
            $areaPre['roles'] = $arrayRoles;
            //$areaPre['categorias'] = $arrayRoles;
            $areaPre['descripcion'] = $hp->descripcion;
            $areaPre['sumatorio'] = floatval($hp->horas);
            array_push($arrayAreaPres,$areaPre);

        }

       // dd($arrayAreaPres);

        $arrayArea = [];
        array_push($arrayArea,[
            'consumidas' => $arrayAreaConsumidasResumen,
            'presupuestadas' => $arrayAreaPres
        ]);
        // dd($arrayArea["consumidas"]);

        return $arrayArea;
        //  dd($arrayAreaConsumidasResumen);


    }

    public function getMostrarResumenProyectoSOC(Request $request)
    {
        $cproyecto     = $request->cproyecto;
        $disciplina    = $request->disciplina;
        $fecha_inicial = $this->formatoFecha( $request->fdesde);
        $fecha_final = $this->formatoFecha( $request->fhasta);
        $listadeDisciplina = $this->CapturarAreasTotales($disciplina);
       // $listaproyectos = $this->listadeProyectosconSOC($cproyecto);
        $multipleProyectosSOC = $request->cbo_selectSOC;
        $listaproyectos = $request->soc == "true" ? $this->listadeProyectosconSOC($cproyecto) : $this->listadeProyectossinSOC($multipleProyectosSOC);

        
       
        //dd($listaproyectos,$request->soc);

        $arrayactividades = [];
        $arraylP = [];
        
        foreach ($listaproyectos as  $proyecto): 
            $cc_actividad = $this->getActividadesHijosProyecto($proyecto->cproyecto);
            $arrayactividades =   array_merge($arrayactividades,$cc_actividad);
             array_push($arraylP,$proyecto->cproyecto);

        endforeach;
        //$listadeProyectoString  = implode(",",$arraylP);
        //dd($arraylP);
        
        //dd( $rolesPresupuestadas);

        //dd($listadeProyectoString);
       //dd($arrayactividades,"sss");
        $listadeActividades = [];
        foreach ($arrayactividades as $key => $value){
            $horas_ejecutadas_sum = $this->CapturarHorasConsumidosEmpleadosAreaFinal($value,$listadeDisciplina,$fecha_inicial,$fecha_final);
            $horas_ejecutadas_sum = $horas_ejecutadas_sum->get();

            if ($horas_ejecutadas_sum) {
                array_push($listadeActividades,$horas_ejecutadas_sum);
            }
        }

        $merge = [];
        foreach ($listadeActividades as $lis) {
            foreach ($lis as $indlist):
                array_push($merge,$indlist);
            endforeach;
        }
        $result = array();
        foreach($merge as $t) {
            $repeat=false;
            for($i=0;$i<count($result);$i++)
            {
                if($result[$i]['descripcion']==$t->descripcion && $result[$i]['carea']==$t->carea && $result[$i]['cdisciplina']==$t->cdisciplina)
                {
                    $result[$i]['sum']+= floatval($t->sum) ;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result[] = array('descripcion' => $t->descripcion,
                    'carea' => $t->carea,
                    'cdisciplina' => $t->cdisciplina,
                    'sum' => $t->sum);
        }

       // dd($result);
        // Armar el array para el grafico
        $arrayAreaConsumidasResumen = [];


        foreach ($result as $ar):
            $arrayCategorias = [];
            $categoriaprofResu = $this->capturarcategoriasResumen($ar['carea'],$arraylP);
                foreach ($categoriaprofResu as $catP):
                $categP['drilldown'] = intval($catP->ccategoriaprofesional.$ar['carea']);
                if($catP->ccategoriaprofesional === 0)
                {
                    $categP['drilldown'] = intval('999'.$ar['carea']);
                }
                $empleadosporCategoria =  $this->capturarEmpleadosResumen($ar['carea'],$arraylP);
                $arrayEmpleados = [];
                $sumaporcategoria = 0;

                foreach ($empleadosporCategoria as $eporcategoria)
                {
                    if($catP->ccategoriaprofesional ==  $eporcategoria->ccategoriaprofesional)
                    {
                        $caper['id'] =  intval($eporcategoria->ccategoriaprofesional.$ar['carea']);
                        if($catP->ccategoriaprofesional === 0)
                        {
                            $caper['id'] =  intval('999'.$ar['carea']);
                        }
                        $caper['name'] =  $eporcategoria->abreviatura;
                        $caper['cpersona'] =  $eporcategoria->cpersona;
                        $caper['y'] =  $this->sumaempleadosResumen($arraylP,$eporcategoria->cpersona,$ar['carea'],$fecha_inicial,$fecha_final);
                        $sumaporcategoria +=  $caper['y'];
                        array_push($arrayEmpleados,$caper);
                    }

                }

                $categP['name'] =  $catP->descripcion;
                $categP['carea'] =  $catP->carea;
                $categP['empleadoCategoria'] =  $arrayEmpleados;
                $categP['y'] =  $sumaporcategoria;

                array_push($arrayCategorias,$categP);

            endforeach;
            $abd['carea'] = $ar['carea'];
            $abd['categorias'] =$arrayCategorias;
            $abd['descripcion'] = $ar['descripcion'];
            $abd['sumatorio'] = floatval($ar['sum']);
            array_push($arrayAreaConsumidasResumen,$abd);
        endforeach;

        //dd($arrayAreaConsumidasResumen);

       // $horas_presupuestadas_sum = $this->CapturarHorasPresupuestadosEmpleadosAreaResumen($cproyecto,$listadeDisciplina);
        //$horas_presupuestadas_sum = $this->CapturarHorasPresupuestadosEmpleadosAreaResumen(5472,$listadeDisciplina); 
        $arrayareasP = [];
        foreach ($listaproyectos as  $proyecto):
            $horas_presupuestadas_sum = $this->CapturarHorasPresupuestadosEmpleadosAreaResumen($proyecto->cproyecto,$listadeDisciplina);
            $arrayareasP  = array_merge($arrayareasP,$horas_presupuestadas_sum);
        endforeach;    

        //dd($arrayareasP,"123",$listaproyectos);

         // Simplificar areas

         $resultP = array();
         foreach($arrayareasP as $t) {
             $repeat=false;
             for($i=0;$i<count($resultP);$i++)
             {
                 if($resultP[$i]['descripcion']==$t->descripcion && $resultP[$i]['carea']==$t->carea && $resultP[$i]['cdisciplina']==$t->cdisciplina)
                 {
                     $resultP[$i]['sum']+= floatval($t->horas) ;
                     $repeat=true;
                     break;
                 }
             }
             if($repeat==false)
                 $resultP[] = array('descripcion' => $t->descripcion,
                     'carea' => $t->carea,
                     'cdisciplina' => $t->cdisciplina,
                     'sum' => floatval($t->horas));
         }


        // dd($resultP);


        $arrayAreaPres = [];
        foreach ($resultP as $hp) {
            $arrayRoles = [];
            $rolesPresupuestadas =  $this->capturarRolesPresupuestadosResumenSOC($arraylP,$hp['cdisciplina']);

           

            foreach ($rolesPresupuestadas as $rp) {
                // $rolPre['drilldown'] =   intval($rp->croldespliegue.$hp->carea.'-');
                $rolPre['name'] =  $rp->descripcionrol;
                $rolPre['carea'] =  intval($rp->carea.'123');
                $rolPre['empleadoCategoria'] = [

                ];
                $rolPre['y'] = floatval($rp->sumhorasrol);
                //$rolPre['empleadoroles'] =  [];
                array_push($arrayRoles,$rolPre);
            }
            $areaPre['carea'] = intval($hp['carea'].'123');
            $areaPre['roles'] = $arrayRoles;
            //$areaPre['categorias'] = $arrayRoles;
            $areaPre['descripcion'] = $hp['descripcion'];
            $areaPre['sumatorio'] = floatval($hp['sum']);
            array_push($arrayAreaPres,$areaPre);

        }

      

        $arrayArea = [];
        array_push($arrayArea,[
            'consumidas' => $arrayAreaConsumidasResumen,
            'presupuestadas' => $arrayAreaPres
        ]);
         //dd($arrayArea);

        return $arrayArea;
        //  dd($arrayAreaConsumidasResumen);


    }

    public function getmostrarResumenMontoProyecto(Request $request)
    {
        $cproyecto     = $request->cproyecto;
        $disciplina    = $request->disciplina;
        $fecha_inicial = $this->formatoFecha( $request->fdesde);
        $fecha_final = $this->formatoFecha( $request->fhasta);

        $listadeDisciplina = $this->CapturarAreasTotales($disciplina);
        $listadActividadesSoloHijas = $this->getActividadesHijosProyecto($cproyecto);

       
        $cproyectoArray  = explode(',', $cproyecto);
        $listadeActividades = [];
        foreach ($listadActividadesSoloHijas as $key => $lactividad){
            $horas_ejecutadas_sum = $this->CapturarResumenMontosConsumidosHH($lactividad,$cproyecto,$listadeDisciplina,$fecha_inicial,$fecha_final);



            if ($horas_ejecutadas_sum) {
                array_push($listadeActividades,$horas_ejecutadas_sum);
            }
        }
        // dd($listadeActividades);
        $merge = [];
        foreach ($listadeActividades as $lis) {
            foreach ($lis as $indlist):
                array_push($merge,$indlist);
            endforeach;
        }
        //dd($merge);

        $result = array();
        foreach($merge as $t) {
            $repeat=false;
            for($i=0;$i<count($result);$i++)
            {
                if($result[$i]['carea']==$t['carea'])
                {
                    $result[$i]['multiporarea']+= floatval($t['multiporarea']) ;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result[] = array('descripcion' => $t['descripcion'],
                    'carea' => $t['carea'],
                    'multiporarea' => $t['multiporarea']);
        }
        // dd($result);


        $arrayAreaConsumidasResumen = [];


        foreach ($result as $ar):
            $arrayCategorias = [];
            $categoriaprofResu = $this->capturarcategoriasResumen($ar['carea'],$cproyectoArray);
            foreach ($categoriaprofResu as $catP):
                $categP['drilldown'] = intval($catP->ccategoriaprofesional.$ar['carea']);
                if($catP->ccategoriaprofesional === 0)
                {
                    $categP['drilldown'] = intval('999'.$ar['carea']);
                }
                $empleadosporCategoria =  $this->capturarEmpleadosResumen($ar['carea'],$cproyectoArray);
                $arrayEmpleados = [];
                $sumaporcategoria = 0;

                foreach ($empleadosporCategoria as $eporcategoria)
                {
                    if($catP->ccategoriaprofesional ==  $eporcategoria->ccategoriaprofesional)
                    {
                        $caper['id'] =  intval($eporcategoria->ccategoriaprofesional.$ar['carea']);
                        if($catP->ccategoriaprofesional === 0)
                        {
                            $caper['id'] =  intval('999'.$ar['carea']);
                        }
                        $caper['name'] =  $eporcategoria->abreviatura;
                        $caper['cpersona'] =  $eporcategoria->cpersona;
                        $caper['y'] =  $this->sumaempleadoMontoResumen( $cproyectoArray,$eporcategoria->cpersona,$ar['carea'],$fecha_inicial,$fecha_final);
                        $sumaporcategoria +=  $caper['y'];
                        array_push($arrayEmpleados,$caper);
                    }
                }

                $categP['name'] =  $catP->descripcion;
                $categP['carea'] =  $catP->carea;
                $categP['empleadoCategoria'] =  $arrayEmpleados;
                $categP['y'] =  $sumaporcategoria;

                array_push($arrayCategorias,$categP);

            endforeach;
            $abd['carea'] = $ar['carea'];
            $abd['categorias'] =$arrayCategorias;
            $abd['descripcion'] = $ar['descripcion'];
            $abd['sumatorio'] = floatval($ar['multiporarea']);
            array_push($arrayAreaConsumidasResumen,$abd);
        endforeach;

       // dd($arrayAreaConsumidasResumen);


        // Presupuestados


        $horasMontoPres= $this->CapturarMontosPresuestadosHHResumen($cproyecto,$listadeDisciplina);
        //dd($horasMontoPres);

        $arrayAreaPresMonto = [];
        foreach ($horasMontoPres as $hp) {
            $arrayRoles = [];
            $rolesPresupuestadas =  $this->capturarRolesPresupuestadosResumen($cproyectoArray,$hp['cdisciplina']);

            foreach ($rolesPresupuestadas as $rp) {
                // $rolPre['drilldown'] =   intval($rp->croldespliegue.$hp->carea.'-');
                $rolPre['name'] =  $rp->descripcionrol;
                $rolPre['carea'] =  intval($rp->carea.'123');
                $rolPre['empleadoCategoria'] = [

                ];
                //$roldespliegueSumaMonto = $this->CapturarMontosPresuestadosHHRoles($cproyecto,$cproyectoactividad,$hp['cdisciplina'],$rp->croldespliegue);
                //$rolPre['y'] = floatval($roldespliegueSumaMonto['multiporarea']);
                $rolPre['y'] =  $this->CapturarMontosPresuestadosHHRolesResumen($cproyectoArray,$hp['cdisciplina'],$rp->croldespliegue);
                //$rolPre['empleadoroles'] =  [];
                array_push($arrayRoles,$rolPre);
            }
            $areaPre['carea'] = intval($hp['carea'].'123');
            $areaPre['roles'] = $arrayRoles;
            //$areaPre['categorias'] = $arrayRoles;
            $areaPre['descripcion'] = $hp['descripcion'];
            $areaPre['sumatorio'] = floatval($hp['multiporarea']);
            array_push($arrayAreaPresMonto,$areaPre);

        }

        $arrayArea = [];
        array_push($arrayArea,[
            'consumidas' => $arrayAreaConsumidasResumen,
            'presupuestadas' => $arrayAreaPresMonto
        ]);
        // dd($arrayArea);

        return $arrayArea;

        //  dd($arrayAreaPresMonto);


    }

    public function getmostrarResumenMontoProyectoSOC(Request $request)
    {
        $cproyecto     = $request->cproyecto;
        $disciplina    = $request->disciplina;
        $fecha_inicial = $this->formatoFecha( $request->fdesde);
        $fecha_final = $this->formatoFecha( $request->fhasta);
        $multipleProyectosSOC = $request->cbo_selectSOC;
        $listadeDisciplina = $this->CapturarAreasTotales($disciplina);
        $listaproyectos = $request->soc == "true" ? $this->listadeProyectosconSOC($cproyecto) : $this->listadeProyectossinSOC($multipleProyectosSOC);

        //$listadActividadesSoloHijas = $this->getActividadesHijosProyecto($cproyecto);

        $arrayactividades = [];
        $arraylP = [];
        
        foreach ($listaproyectos as  $proyecto): 
            $cc_actividad = $this->getActividadesHijosProyecto($proyecto->cproyecto);
            $arrayactividades =   array_merge($arrayactividades,$cc_actividad);
             array_push($arraylP,$proyecto->cproyecto);

        endforeach;


        // dd($listadeDisciplina,$listadActividadesSoloHijas);

        $listadeActividades = [];
        foreach ($arrayactividades as $key => $lactividad){
            $horas_ejecutadas_sum = $this->CapturarResumenMontosConsumidosHHFinal($lactividad,$listadeDisciplina,$fecha_inicial,$fecha_final);

            if ($horas_ejecutadas_sum) {
                array_push($listadeActividades,$horas_ejecutadas_sum);
            }
        }


        // dd($listadeActividades);
        $merge = [];
        foreach ($listadeActividades as $lis) {
            foreach ($lis as $indlist):
                array_push($merge,$indlist);
            endforeach;
        }
        //dd($merge);

        $result = array();
        foreach($merge as $t) {
            $repeat=false;
            for($i=0;$i<count($result);$i++)
            {
                if($result[$i]['carea']==$t['carea'])
                {
                    $result[$i]['multiporarea']+= floatval($t['multiporarea']) ;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $result[] = array('descripcion' => $t['descripcion'],
                    'carea' => $t['carea'],
                    'multiporarea' => $t['multiporarea']);
        }
        // dd($result);


        $arrayAreaConsumidasResumen = [];


        foreach ($result as $ar):
            $arrayCategorias = [];
            $categoriaprofResu = $this->capturarcategoriasResumen($ar['carea'],$arraylP);
            foreach ($categoriaprofResu as $catP):
                $categP['drilldown'] = intval($catP->ccategoriaprofesional.$ar['carea']);
                if($catP->ccategoriaprofesional === 0)
                {
                    $categP['drilldown'] = intval('999'.$ar['carea']);
                }
                $empleadosporCategoria =  $this->capturarEmpleadosResumen($ar['carea'],$arraylP);
                $arrayEmpleados = [];
                $sumaporcategoria = 0;

                foreach ($empleadosporCategoria as $eporcategoria)
                {
                    if($catP->ccategoriaprofesional ==  $eporcategoria->ccategoriaprofesional)
                    {
                        $caper['id'] =  intval($eporcategoria->ccategoriaprofesional.$ar['carea']);
                        if($catP->ccategoriaprofesional === 0)
                        {
                            $caper['id'] =  intval('999'.$ar['carea']);
                        }
                        $caper['name'] =  $eporcategoria->abreviatura;
                        $caper['cpersona'] =  $eporcategoria->cpersona;
                        $caper['y'] =  $this->sumaempleadoMontoResumen($arraylP,$eporcategoria->cpersona,$ar['carea'],$fecha_inicial,$fecha_final);
                        $sumaporcategoria +=  $caper['y'];
                        array_push($arrayEmpleados,$caper);
                    }
                }

                $categP['name'] =  $catP->descripcion;
                $categP['carea'] =  $catP->carea;
                $categP['empleadoCategoria'] =  $arrayEmpleados;
                $categP['y'] =  $sumaporcategoria;

                array_push($arrayCategorias,$categP);

            endforeach;
            $abd['carea'] = $ar['carea'];
            $abd['categorias'] =$arrayCategorias;
            $abd['descripcion'] = $ar['descripcion'];
            $abd['sumatorio'] = floatval($ar['multiporarea']);
            array_push($arrayAreaConsumidasResumen,$abd);
        endforeach;

        // Presupuestadas


        //$horasMontoPres= $this->CapturarMontosPresuestadosHHResumen($proyecto->cproyecto,$listadeDisciplina);

        $arrayareasP = [];
        foreach ($listaproyectos as  $proyecto):
            $horas_presupuestadas_sum = $this->CapturarMontosPresuestadosHHResumen($proyecto->cproyecto,$listadeDisciplina);
            $arrayareasP  = array_merge($arrayareasP,$horas_presupuestadas_sum);
        endforeach;   
        
        $resultP = array();
        foreach($arrayareasP as $t) {
            $repeat=false;
            for($i=0;$i<count($resultP);$i++)
            {
                if($resultP[$i]['descripcion']==$t['descripcion']&& $resultP[$i]['carea']==$t['carea']&& $resultP[$i]['cdisciplina']==$t['cdisciplina'])
                {
                    $resultP[$i]['multiporarea']+= floatval($t['multiporarea']) ;
                    $repeat=true;
                    break;
                }
            }
            if($repeat==false)
                $resultP[] = array('descripcion' => $t['descripcion'],
                    'carea' => $t['carea'],
                    'cdisciplina' => $t['cdisciplina'],
                    'multiporarea' => floatval($t['multiporarea']));
        }

       // dd($resultP);

        $arrayAreaPresMonto = [];
        foreach ($resultP as $hp) {
            $arrayRoles = [];
            $rolesPresupuestadas =  $this->capturarRolesPresupuestadosResumen($arraylP,$hp['cdisciplina']);

            foreach ($rolesPresupuestadas as $rp) {
                // $rolPre['drilldown'] =   intval($rp->croldespliegue.$hp->carea.'-');
                $rolPre['name'] =  $rp->descripcionrol;
                $rolPre['carea'] =  intval($rp->carea.'123');
                $rolPre['empleadoCategoria'] = [

                ];
                //$roldespliegueSumaMonto = $this->CapturarMontosPresuestadosHHRoles($cproyecto,$cproyectoactividad,$hp['cdisciplina'],$rp->croldespliegue);
                //$rolPre['y'] = floatval($roldespliegueSumaMonto['multiporarea']);
                $rolPre['y'] =  $this->CapturarMontosPresuestadosHHRolesResumen($arraylP,$hp['cdisciplina'],$rp->croldespliegue);
                //$rolPre['empleadoroles'] =  [];
                array_push($arrayRoles,$rolPre);
            }
            $areaPre['carea'] = intval($hp['carea'].'123');
            $areaPre['roles'] = $arrayRoles;
            //$areaPre['categorias'] = $arrayRoles;
            $areaPre['descripcion'] = $hp['descripcion'];
            $areaPre['sumatorio'] = floatval($hp['multiporarea']);
            array_push($arrayAreaPresMonto,$areaPre);

        }





        $arrayArea = [];
        array_push($arrayArea,[
            'consumidas' => $arrayAreaConsumidasResumen,
            'presupuestadas' =>$arrayAreaPresMonto
        ]);
         //dd($arrayArea);

        return $arrayArea;

    }



    public function getActividadesHijosProyecto($cproyecto)
    {
        $ActividadesPadre = Tproyectoactividade::where('cproyecto',$cproyecto)->get()->pluck('cproyectoactividades');
        $ActividadesPadreArray = $ActividadesPadre->toArray();

        // Obtengo los hijos directos a segundo nivel
        $actividadesHijosDirectos = $this->actividad_padre_or_hijoResumen($ActividadesPadreArray);
        $actividadesPadresdelProyecto =  $this->actividad_todos_Padre($cproyecto);

        $actividadeshijasAcontar = [];
        foreach ($actividadesHijosDirectos as $ahd)
        {
            foreach ($actividadesPadresdelProyecto as $app)
            {
                if ($app->cproyectoactividades == $ahd->cproyectoactividades) {
                    continue 2;
                }
            }
            array_push($actividadeshijasAcontar,$ahd->cproyectoactividades);
        }

        $arrayhijosDirectos = $actividadeshijasAcontar ? $actividadeshijasAcontar : $this->getActividades_sinHijo($cproyecto); 

        return $arrayhijosDirectos;
    }

    public function  getActividades_sinHijo($cproyecto)
    {
        $ActividadesPadre = Tproyectoactividade::where('cproyecto',$cproyecto)->get()->pluck('cproyectoactividades');
        $ActividadesPadreArray = $ActividadesPadre->toArray();

        return $ActividadesPadreArray;
    }



    /****************************
    Performance
     *****************************/

    public function getActividadescurva($cproyecto, $disciplina,$fechaDesde,$fechaHasta)
    {

        $cproyecto  = $cproyecto;
        $disciplina = $disciplina;

        $fechaDesde =  $fechaDesde;
        $fechaHasta = $fechaHasta;
        //dd($fechaHasta->date);



        $arrayProyectos = [];

        $proyUnico = $this->identificarProyecto($cproyecto);


        $proy['cproyecto'] = $proyUnico->cproyecto;
        $proy['codigo']    = $proyUnico->codigo;
        $proy['nombre']    = $proyUnico->nombre;

        $hijosdirectos = $this->listadeActividadesHijosDirectosporProyecto($cproyecto);
        // dd($hijosdirectos);

        $actividadesArray    = [];

        $sumahorasPresupuestadasNivelProyecto  = 0;
        $sumaMontoPresupuestadasNivelProyecto  = 0;


        $sumahoras_rate_hora = 0;

        $result_multiplicacion_monto_porcentjae_Hijos =[];
        $result_multiplicacion_monto_porcentjae_Nietos =[];
        $result_multiplicacion_monto_porcentjae_Bisnietos =[];



        foreach ($hijosdirectos as $hd)
        {
            $result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos =[];

            $actPadre['codigoactividad'] = $hd->codigoactvidad;
            $actPadre['nombreAct'] = $hd->descripcionactividad;
            $actPadre['cproyectoactividadesPadre'] = $hd->cproyectoactividades;
            $listadeactividadesHijos = $this->actividad_padre_or_hijo($hd->cproyectoactividades);

            $actividadesHijoArray    = [];

            $acumulativohoraspresupuestadasHijos = 0;
            $acumulativohoraspresupuestadasNietos = 0;
            $acumulativohoraspresupuestadasBisnieto = 0;

            $acumulativomontopresupuestadasHijos = 0;
            $acumulativomontopresupuestadasNietos = 0;
            $acumulativomontopresupuestadasBisnieto = 0;



            foreach ($listadeactividadesHijos as $hdH)
            {
                $actHijo['codigoactividad'] = $hdH->codigoactvidad;
                $actHijo['nombreAct'] = $hdH->descripcionactividad;
                $actHijo['cproyectoactividades'] = $hdH->cproyectoactividades;
                $actHijo['cproyecto'] = $cproyecto;
                $actHijo['cproyectoactividades_parent'] = $hdH->cproyectoactividades_parent;
                $actHijo['horapresupuestadasperformance'] = $this->obtenerhoraspresuuestadasPerformance($hdH->cproyectoactividades,$disciplina);
                $actHijo['monto_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdH->cproyectoactividades,$disciplina,$cproyecto)['sumtotalPresupuestadas'];
                $actHijo['array_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdH->cproyectoactividades,$disciplina,$cproyecto)['estructuraParticipantes'];
                $actHijo['array_performance'] = $this->numerosemana($fechaDesde,$fechaHasta,$hdH->cproyectoactividades,$disciplina, $actHijo['monto_Presupuestados'],$hdH->cproyectoactividades_parent);


                foreach($actHijo['array_performance'] as $t) {
                    $repeat=false;
                    for($x=0;$x<count($result_multiplicacion_monto_porcentjae_Hijos);$x++)
                    {
                        if($result_multiplicacion_monto_porcentjae_Hijos[$x]['semana']== $t['semana'] && $result_multiplicacion_monto_porcentjae_Hijos[$x]['anio']== $t['anio'] && $result_multiplicacion_monto_porcentjae_Hijos[$x]['pk_actividadPadre']== $t['pk_actividadPadre'])
                        {
                            $result_multiplicacion_monto_porcentjae_Hijos[$x]['porcentaje_por_montoPre']+=$t['porcentaje_por_montoPre'];
                            $repeat=true;
                            break;
                        }
                    }
                    if($repeat==false)
                        $result_multiplicacion_monto_porcentjae_Hijos[] = array(    'anio'                      => $t['anio'],
                            'semana'                    => $t['semana'],
                            'pk_actividadPadre'         => $t['pk_actividadPadre'],
                            'porcentaje_por_montoPre'   => $t['porcentaje_por_montoPre']
                        );
                }
                //dd($result_multiplicacion_monto_porcentjae_Hijos);



                $acumulativohoraspresupuestadasHijos += $actHijo['horapresupuestadasperformance'];
                $acumulativomontopresupuestadasHijos +=  $actHijo['monto_Presupuestados'];


                $listadeactividadesNietos = $this->actividad_padre_or_hijo($hdH->cproyectoactividades);


                $actividadesNietosArray    = [];
                foreach ($listadeactividadesNietos as $hdN)
                {
                    $actNieto['codigoactividad'] = $hdN->codigoactvidad;
                    $actNieto['nombreAct'] = $hdN->descripcionactividad;
                    $actNieto['cproyectoactividades'] = $hdN->cproyectoactividades;
                    $actNieto['cproyecto'] = $cproyecto;
                    $actNieto['cproyectoactividades_parent'] = $hdH->cproyectoactividades_parent;
                    $actNieto['horapresupuestadasperformance'] = $this->obtenerhoraspresuuestadasPerformance($hdN->cproyectoactividades,$disciplina);
                    $actNieto['monto_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdN->cproyectoactividades,$disciplina,$cproyecto)['sumtotalPresupuestadas'];
                    $actNieto['array_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdN->cproyectoactividades,$disciplina,$cproyecto)['estructuraParticipantes'];
                    $actNieto['array_performance'] = $this->numerosemana($fechaDesde,$fechaHasta,$hdN->cproyectoactividades,$disciplina, $actNieto['monto_Presupuestados'],$hdH->cproyectoactividades_parent);

                    foreach($actNieto['array_performance'] as $n) {
                        $repeat=false;
                        for($i=0;$i<count($result_multiplicacion_monto_porcentjae_Nietos);$i++)
                        {
                            if($result_multiplicacion_monto_porcentjae_Nietos[$i]['semana']== $n['semana'] && $result_multiplicacion_monto_porcentjae_Nietos[$i]['anio']== $n['anio'] &&  $result_multiplicacion_monto_porcentjae_Nietos[$i]['pk_actividadPadre']== $n['pk_actividadPadre'])
                            {
                                $result_multiplicacion_monto_porcentjae_Nietos[$i]['porcentaje_por_montoPre']+=$n['porcentaje_por_montoPre'];
                                $repeat=true;
                                break;
                            }
                        }
                        if($repeat==false)
                            $result_multiplicacion_monto_porcentjae_Nietos[] = array( 'anio'                    => $n['anio'],
                                'semana'                  => $n['semana'],
                                'pk_actividadPadre'       => $n['pk_actividadPadre'],
                                'porcentaje_por_montoPre' => $n['porcentaje_por_montoPre']);
                    }


                    $listadeactividadesBisnietos = $this->actividad_padre_or_hijo($hdN->cproyectoactividades);

                    $acumulativohoraspresupuestadasNietos += $actNieto['horapresupuestadasperformance'];
                    $acumulativomontopresupuestadasNietos +=  $actNieto['monto_Presupuestados'];



                    $actividadesBisnietosArray    = [];
                    foreach ($listadeactividadesBisnietos as $hdB)
                    {
                        $actBisnieto['codigoactividad'] = $hdB->codigoactvidad;
                        $actBisnieto['nombreAct'] = $hdB->descripcionactividad;
                        $actBisnieto['cproyectoactividades'] = $hdB->cproyectoactividades;
                        $actBisnieto['cproyecto'] = $cproyecto;
                        $actBisnieto['cproyectoactividades_parent'] = $hdH->cproyectoactividades_parent;
                        $actBisnieto['horapresupuestadasperformance'] = $this->obtenerhoraspresuuestadasPerformance($hdB->cproyectoactividades,$disciplina);
                        $actBisnieto['monto_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdB->cproyectoactividades,$disciplina,$cproyecto)['sumtotalPresupuestadas'];
                        $actBisnieto['array_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdB->cproyectoactividades,$disciplina,$cproyecto)['estructuraParticipantes'];
                        $actBisnieto['array_performance'] = $this->numerosemana($fechaDesde,$fechaHasta,$hdB->cproyectoactividades,$disciplina,$actBisnieto['monto_Presupuestados'],$hdH->cproyectoactividades_parent);

                        foreach($actBisnieto['array_performance'] as $b) {
                            $repeat=false;
                            for($y=0;$y<count($result_multiplicacion_monto_porcentjae_Bisnietos);$y++)
                            {
                                if($result_multiplicacion_monto_porcentjae_Bisnietos[$y]['semana']== $b['semana'] && $result_multiplicacion_monto_porcentjae_Bisnietos[$y]['anio']== $b['anio'] && $result_multiplicacion_monto_porcentjae_Bisnietos[$y]['pk_actividadPadre']== $b['pk_actividadPadre'] )
                                {
                                    $result_multiplicacion_monto_porcentjae_Bisnietos[$y]['porcentaje_por_montoPre']+=$b['porcentaje_por_montoPre'];
                                    $repeat=true;
                                    break;
                                }
                            }
                            if($repeat==false)
                                $result_multiplicacion_monto_porcentjae_Bisnietos[] = array( 'anio'   => $b['anio'],
                                    'semana' => $b['semana'],
                                    'pk_actividadPadre'       => $b['pk_actividadPadre'],
                                    'porcentaje_por_montoPre' => $b['porcentaje_por_montoPre']);
                        }

                        $acumulativohoraspresupuestadasBisnieto += $actBisnieto['horapresupuestadasperformance'];
                        $acumulativomontopresupuestadasBisnieto +=  $actBisnieto['monto_Presupuestados'];



                        array_push($actividadesBisnietosArray,$actBisnieto);
                    }

                    $actNieto['actBisnieto'] = $actividadesBisnietosArray;
                    array_push($actividadesNietosArray,$actNieto);
                }

                $actHijo['actNieto'] = $actividadesNietosArray;
                array_push($actividadesHijoArray,$actHijo);
            }

            //dd($result_multiplicacion_monto_porcentjae_Hijos);

            $actPadre['sumtotalhoraspresupuestadasPadre'] =$actividadesHijoArray;
            $actPadre['actHijo'] =$actividadesHijoArray;
            $actPadre['Suma__totalHorasPresupuestadas'] =$acumulativohoraspresupuestadasHijos + $acumulativohoraspresupuestadasNietos + $acumulativohoraspresupuestadasBisnieto;
            $sumahorasPresupuestadasNivelProyecto +=  $actPadre['Suma__totalHorasPresupuestadas'];
            $actPadre['Suma__totalMontoPresupuestadas'] =$acumulativomontopresupuestadasHijos + $acumulativomontopresupuestadasNietos + $acumulativomontopresupuestadasBisnieto;
            $sumaMontoPresupuestadasNivelProyecto += $actPadre['Suma__totalMontoPresupuestadas'];


            $arrraymerge_hijos_nietos_bisnietos = array_merge($result_multiplicacion_monto_porcentjae_Hijos,$result_multiplicacion_monto_porcentjae_Nietos,$result_multiplicacion_monto_porcentjae_Bisnietos);

            //dd($arrraymerge_hijos_nietos_bisnietos,$hd);

            foreach($arrraymerge_hijos_nietos_bisnietos as $w) {
                // dd($hd->cproyectoactividades);

                if ($w['pk_actividadPadre'] ==  $hd->cproyectoactividades ) {
                    $pkpadreperformance['pkpadre'] = $hd->cproyectoactividades;
                    $pkpadreperformance['semana'] = $w['semana'];
                    $pkpadreperformance['anio'] = $w['anio'];
                    $pkpadreperformance['porcentaje_por_montoPre'] = $actPadre['Suma__totalMontoPresupuestadas']== 0 ? 0:round(($w['porcentaje_por_montoPre'] /$actPadre['Suma__totalMontoPresupuestadas']),2) ;
                    array_push($result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos,$pkpadreperformance);
                }

            }


            $actPadre['porcentaje_por_montoPre__semana'] = $result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos;

            array_push($actividadesArray,$actPadre);
        }
        //dd($actividadesArray);




        $proy['sumahorasPresupuestadasNivelProyecto']    = $sumahorasPresupuestadasNivelProyecto;
        $proy['sumaMontoPresupuestadasNivelProyecto']    = $sumaMontoPresupuestadasNivelProyecto;



        $proy['sum_act_total']                           = $sumahoras_rate_hora;
        $proy['actPadre']                                = $actividadesArray;

        $arr2 = [];
        foreach ($actividadesArray as $posicion => $valor) {
            $arr=[];
            $montoPadre = $valor['Suma__totalMontoPresupuestadas'];
            $sub['nombreAct']  = $valor['nombreAct'];
            foreach ($valor['porcentaje_por_montoPre__semana'] as $key => $val) {
                $sub['sub'] = $val['porcentaje_por_montoPre'] * $montoPadre;
                $sub['semana'] = $val['semana'];
                $sub['anio'] = $val['anio'];

                array_push($arr,$sub);
            }
            array_push($arr2,$arr);

        }
        $semana_anio=$actividadesArray[0]['porcentaje_por_montoPre__semana'];
        // dd($semana_anio,$arr2);



        $sumatotal=[];

        foreach ($semana_anio as $sa){

            $acumulado=0;

            for($a=0;$a<count($arr2);$a++) {

                foreach ($arr2[$a] as $abd){
                    //dd($abd);

                    if($sa['semana']==$abd['semana'] && $sa['anio']==$abd['anio']) {

                        $acumulado    +=$abd['sub'];
                        $resumenFinal = ($sumaMontoPresupuestadasNivelProyecto == 0 ? 0 :$acumulado / $sumaMontoPresupuestadasNivelProyecto);

                    }
                }
            }

            array_push($sumatotal,[
                'resumenFinal' => round($resumenFinal,2),
                'semana' => $sa['semana'],
                'anio' =>  $sa['anio'],
            ]);

        }
        // dd($sumatotal);
        $proy['resumenTotal']                            = $sumatotal;
        $arrayProyectos[count($arrayProyectos)]          = $proy;

        //dd($arrayProyectos);
        return $arrayProyectos;

    }

    /* Performance FUNCIONES REUTILIZABLES*/

    public function identificarProyecto($cproyecto)
    {
        $project_ingresado = DB::table('tproyecto as tp')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->select('tp.codigo', 'tp.cproyecto', 'tp.nombre', 'tum.nombre as nombreunidadMinera')
            ->where('tp.cproyecto', '=', $cproyecto)
            ->first();

        return $project_ingresado;
    }
    public function listadeActividadesHijosDirectosporProyecto($cproyecto)
    {
        $actividades = DB::table('tproyectoactividades')
            ->select('cproyectoactividades','codigoactvidad','descripcionactividad','cproyectoactividades_parent')
            ->where('cproyecto', '=',$cproyecto)
            ->where('cproyectoactividades_parent', '=',null)
            ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
            ->get();
        return $actividades;
    }
    private function actividad_padre_or_hijo($cactividad)
    {
        $activityfather_or_son = DB::table('tproyectoactividades as tpa')
            ->select('tpa.cproyectoactividades','tpa.codigoactvidad','tpa.descripcionactividad','tpa.cproyecto','tpa.cproyectoactividades_parent')
            ->orderBy('tpa.codigoactvidad','ASC')
            ->where('tpa.cproyectoactividades_parent', '=', $cactividad)
            ->get();

        return $activityfather_or_son;

    }
    private function actividad_padre_or_hijoResumen($cactividad)
    {
        $activityfather_or_son = DB::table('tproyectoactividades')
            ->select('cproyectoactividades','codigoactvidad')
            ->whereIn('cproyectoactividades_parent', $cactividad)
            ->orderBy('cproyectoactividades','ASC')
            ->get();
        return $activityfather_or_son;
    }
    private function actividad_todos_Padre($cproyecto)
    {
        $activityall_Father = DB::table('tproyectoactividades')
            ->select('cproyectoactividades_parent as cproyectoactividades')
            ->where('cproyecto','=', $cproyecto)
            // ->orderBy('cproyectoactividades_parent','ASC')
            ->distinct('cproyectoactividades_parent')
            ->whereNotNull('cproyectoactividades_parent')
            ->orderBy('cproyectoactividades','ASC')
            ->get();
        return $activityall_Father;
    }
    private function obtenerhoraspresuuestadasPerformance($cactividad,$disciplina)
    {
        $activity_sum = DB::table('tproyectohonorarios as tph')
            ->select(DB::raw("sum(to_number(horas, '999999.99')) as sum"));

        if ($disciplina == 'Nuevo') {

            $activity_sum = $activity_sum->where('tph.cproyectoactividades', '=', $cactividad);
        } else {

            $activity_sum = $activity_sum->where('tph.cdisciplina', '=', $disciplina)
                ->where('tph.cproyectoactividades', '=',$cactividad);
        }

        $activity_sum = $activity_sum->first();

        // dd($activity_sum);

        $var = 0;

        if ($activity_sum->sum != null) {
            $var = $activity_sum->sum;
        }
        return $var;

    }
    //Otras Funciones
    private function obteneratepresuestadas_condetalle($cactividad,$disciplina,$cproyecto)
    {
        $cproyecto = $cproyecto;

        $cabecera_participante = $this->getCabezera($cproyecto, $disciplina);
        $suma_rate_detalle_activity = 0;
        $arraysumadetalleactivity   = [];

        foreach ($cabecera_participante as $cab) {
            // dd($cab);
            $sumatoria_rate_hora_detalle = $this->obtenerhorasrate($cactividad, $disciplina, $cab->croldespliegue);

            $var1 = 0;
            if ($sumatoria_rate_hora_detalle) {

                foreach ($sumatoria_rate_hora_detalle as $sr) {
                    //dd($sr);
                    $suma_actividad = $sr->horas * $sr->rate;
                    $suma_rate_detalle_activity += $suma_actividad;


                    if ($suma_actividad != null) {
                        $var1 = $suma_actividad;
                    }
                }
            }
            array_push($arraysumadetalleactivity, ['suma' => $var1]);
        }


        return array("estructuraParticipantes" => $arraysumadetalleactivity, "sumtotalPresupuestadas" => $suma_rate_detalle_activity);
    }
    public function getCabezera($cproyecto, $cdisciplina)
    {
        /*----------  Lista de cabezera  ----------*/

        $project_ingresado = DB::table('tproyecto as tp')
            ->leftjoin('tproyectoestructuraparticipantes as tpe', 'tp.cproyecto', '=', 'tpe.cproyecto')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpe.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpe.croldespliegue')
            ->select('tpe.croldespliegue', 'trd.descripcionrol', 'trd.orden');

        if ($cdisciplina == 'Nuevo') {

            $project_ingresado = $project_ingresado->where('tpe.cproyecto', '=', $cproyecto);
        } else {

            $project_ingresado = $project_ingresado->where('tpe.cproyecto', '=', $cproyecto)
                ->where('tph.cdisciplina', '=', $cdisciplina);
        }

        $project_ingresado = $project_ingresado->orderBy('trd.orden', 'ASC')
            ->distinct()
            ->get();

        // dd($project_ingresado);

        return $project_ingresado;

        /*----------  End Lista de cabezera  ----------*/
    }
    private function obtenerhorasrate($cactividad, $disciplina, $roldespliegue)
    {
        //dd($disciplina);
        $sumatoria_rate_hora = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpep.croldespliegue')
            ->select('tpa.cproyectoactividades', 'trd.descripcionrol', DB::raw("to_number(tph.horas, '999999.99') as horas"), 'tpep.rate', 'trd.croldespliegue', 'tpa.cproyectoactividades_parent');

        if ($roldespliegue != 'Todos') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('trd.croldespliegue', '=', $roldespliegue);
        }

        if ($disciplina == 'Nuevo') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tpa.cproyectoactividades', '=', $cactividad);
        } else {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tph.cdisciplina', '=', $disciplina)
                ->where('tph.cproyectoactividades', '=', $cactividad);
        }

        $sumatoria_rate_hora = $sumatoria_rate_hora->orderBy('trd.orden', 'ASC')->get();
        //dd($sumatoria_rate_hora);

        return $sumatoria_rate_hora;
    }
    public function numerosemana($fDesde,$fHasta,$cactividad,$disciplina,$montoPresupuestad,$cactividadPadre)
    {
        $fechaDesde = $fDesde;
        $fechaHasta = $fHasta;
        $cactividad = $cactividad;
        $montoPresupuestad = $montoPresupuestad;
        $cactividadPadre = $cactividadPadre;
        //$montoTotalPrespuestosPadre = $montoTotalPrespuestosPadre;

        if (substr($fechaDesde, 2, 1) == '/' || substr($fechaDesde, 2, 1) == '-') {
            $fechaDesde = Carbon::create(substr($fechaDesde, 6, 4), substr($fechaDesde, 3, 2), substr($fechaDesde, 0, 2));
            $fechaHasta = Carbon::create(substr($fechaHasta, 6, 4), substr($fechaHasta, 3, 2), substr($fechaHasta, 0, 2));

        } else {
            $fechaDesde = Carbon::create(substr($fechaDesde, 0, 4), substr($fechaDesde, 5, 2), substr($fechaDesde, 8, 2));
            $fechaHasta = Carbon::create(substr($fechaHasta, 0, 4), substr($fechaHasta, 5, 2), substr($fechaHasta, 8, 2));
        }




        $semana_ini = $fechaDesde->weekOfYear;
        $semana_fin =   $fechaHasta->weekOfYear;

        //dd($semana_ini,$semana_fin);

        $anio_inicial = $fechaDesde->format('Y');
        $anio_final   = $fechaHasta->format('Y');

        $anio_inicial = intval($anio_inicial);
        $anio_final   = intval($anio_final);


        //dd($fechaDesde,$fechaHasta,$semana_ini,$semana_fin,$anio_inicial,$anio_final,'OK');
        $num_sema = [];
        if ($anio_final == $anio_inicial) {

            for ($i = $semana_ini; $i <= $semana_fin; $i++) {

                //array_push($num_sema, $i);

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }

        } else if ($anio_final > $anio_inicial) {

            for ($i = $semana_ini; $i <= 52; $i++) {

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_inicial]);
            }

            // dd($num_sema);

            for ($i = 1; $i <= $semana_fin; $i++) {

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }
        }

        $array_porcentaje          = [];
        $semana_start              = Carbon::now()->weekOfYear;


        foreach ($num_sema as $ns) {
            $user = Auth::user();
            $porcentaje = $this->visualizar_porcentaje_for_semana($cactividad, $ns['semana'], $ns['anio'],$user->cpersona,$disciplina);

            $liderorgerente =  $this->liderorgerente($user->cpersona);

            $arrayporcentaje = [];
            $pesototal = 0;
            $sumaareas = 0;
            $var2 = 0;
            if($liderorgerente == 'gerente' && $disciplina == 'Nuevo'){
                if($porcentaje){
                    foreach ($porcentaje as $kpp) {
                        $c['cdisciplina'] = $kpp->cdisciplina;
                        $c['cperformance'] = $kpp->cperformance;
                        $c['cproyectoactividades'] = $kpp->cproyectoactividades;
                        $c['semana'] = $kpp->semana;
                        $c['porcentaje'] = $kpp->porcentaje;
                        $acum = 0;
                        $data = $this->obtenerhorasrateperformance($kpp->cproyectoactividades,$kpp->cdisciplina);

                        foreach ($data as $dt) {
                            $producto =  $dt->horas *  $dt->rate;
                            $acum  += $producto;
                        }
                        //dd($data);

                        $c['montoarea'] =  $acum ? $acum : 0;
                        $c['multiplicacion'] =  $c['montoarea'] *  $c['porcentaje'] ;
                        $sumaareas +=  $c['montoarea'];
                        $pesototal +=  $c['multiplicacion'];
                        $var2 =   $sumaareas != 0 ?  $pesototal/$sumaareas : 0 ;
                        array_push($arrayporcentaje,$c);
                    }
                }
            }

            if ($liderorgerente == 'lider' || ($liderorgerente == 'gerente' && $disciplina != 'Nuevo')) {
                if ($porcentaje != null) {
                    $var2 = $porcentaje->porcentaje;
                }

            }

            $habilitado = true;
            if ($anio_inicial == $anio_final) {
                if ($ns['semana'] < $semana_start &&  $ns['semana'] < $semana_start - 2 ) {
                    $habilitado = false;
                }
                else if ($ns['semana'] > $semana_start)
                {
                    $habilitado = false;
                }

            } else if ($anio_final > $anio_inicial) {
                if ($ns['semana'] < $semana_start ) {
                    $habilitado = false;
                }

                else if ($ns['semana'] > $semana_start) {
                    $habilitado = false;
                }
            }

            array_push($array_porcentaje, ['porcentaje' => round($var2,2),
                'habilitado'                                => $habilitado,
                'semana'                                    => $ns['semana'],
                'anio'                                      => $ns['anio'],
                'porcentaje_por_montoPre'                   => round($var2,2) * $montoPresupuestad ,
                'pk_actividadPadre'                         => $cactividadPadre
                /* 'pk_parent'                                 => $act->cproyectoactividades_parent,
                 'pk_actividad'                                 => $act->cproyectoactividades*/

            ]);
        }

        return $array_porcentaje;


    }

    private function visualizar_porcentaje_for_semana($cactividad, $num_sem, $periodo,$user,$disciplina)
    {


        $lidergerente = $this->liderorgerente($user);

        $porcentaje = DB::table('tperformance as tper')
            ->leftjoin('tproyectoactividades as tpa', 'tper.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')

            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tper.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->where('tper.cproyectoactividades', '=', $cactividad)
            ->where('tper.semana', '=', $num_sem)
            ->where('tper.periodo', '=', $periodo);

        if($lidergerente == 'gerente' && $disciplina == 'Nuevo')
        {
            //dd($esgerente,'GP','Nuevo');
            $porcentaje = $porcentaje
                ->select('tper.cperformance','tper.porcentaje','tper.semana','tpa.cproyectoactividades','tda.cdisciplina')
                // ->where('tda.cdisciplina','=', $disciplina)
                ->orderBy('tper.semana', 'asc')
                ->distinct()
                ->get();

        }

        elseif($lidergerente == 'gerente'   && $disciplina != 'Nuevo')
        { //dd($esgerente,'GP','Disc');
            $porcentaje = $porcentaje->select('tper.cperformance','tper.porcentaje','tper.semana','tpa.cproyectoactividades','tda.cdisciplina')
                ->where('tda.cdisciplina','=', $disciplina)
                ->orderBy('tper.semana', 'asc')
                ->distinct()
                ->first();
        }
        elseif($lidergerente == 'lider'){
            //dd($eslider,'lider');
            $porcentaje = $porcentaje->where('tper.created_user', '=', $user)
                ->select('tper.porcentaje', 'tper.semana','tpa.cproyectoactividades','tpa.cproyectoactividades_parent')
                ->orderBy('tper.semana', 'asc')
                ->first();
        }

        //dd($porcentaje);

        return $porcentaje;
    }
    public function liderorgerente($user)
    {

        $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.eslider','=','1')
            ->where('tpyp.cpersona', '=', $user)
            ->get();

        $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user)
            ->get();


        if($esgerente){

            $lidergerente = 'gerente';
        }
        else{
            $lidergerente = 'lider';

        }

        return  $lidergerente;
    }
    private function obtenerhorasrateperformance($cactividad, $disciplina)
    {
        //dd($disciplina);
        $sumatoria_rate_hora = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpep.croldespliegue')
            ->select('tpa.cproyectoactividades', 'trd.descripcionrol', DB::raw("to_number(tph.horas, '999999.99') as horas"), 'tpep.rate', 'trd.croldespliegue', 'tpa.cproyectoactividades_parent');


        if ($disciplina == 'Nuevo') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tpa.cproyectoactividades', '=', $cactividad);
        } else {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tph.cdisciplina', '=', $disciplina)
                ->where('tph.cproyectoactividades', '=', $cactividad);
        }

        $sumatoria_rate_hora = $sumatoria_rate_hora->orderBy('trd.orden', 'ASC')->get();
        //dd($sumatoria_rate_hora);

        return $sumatoria_rate_hora;
    }

    /****************************
    End Performance
     *****************************/






}
