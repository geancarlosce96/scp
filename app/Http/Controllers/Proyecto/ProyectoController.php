<?php

namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Erp\HelperSIGSupport;
use spec\Prophecy\Argument\Token\ArrayCountTokenSpec;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Tpropuestum;
use App\Models\Tpropuestapresentacion;
use App\Models\Tpropuestaactividade;
use App\Models\Tpropuestacronogramaconstruccion;
use App\Models\Tpropuestadisciplina;
use App\Models\Tpropuestagasto;
use App\Models\Tpropuestagastoslaboratorio;
use App\Models\Tpropuestahonorario;
use App\Models\Tpropuestapaquete;
use App\Models\Testructuraparticipante;
use App\Models\Tproyecto;
use App\Models\Tproyectoestructuraparticipante;
use App\Models\Tservicio;
use App\Models\Testadopropuestum;
use App\Models\Testadoproyecto;
use App\Models\Tproyectoactividade;
use App\Models\Tproyectoedt;
use App\Models\Tproyectodisciplina;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesfecha;
use App\Models\Tproyectodocumento;
use App\Models\Tdocumentosparaproyecto;
use App\Models\Tconceptogasto;
use App\Models\Tproyectogasto;
use App\Models\Tproyectogastoslaboratorio;
use App\Models\Tproyectohonorario;
use App\Models\Tentregable;
use App\Models\Ttiposentregable;
use App\Models\Troldespliegue;
use App\Models\Tproyectocronogramadetalle;
use App\Models\Tproyectocronograma;
use App\Models\Tproyectochecklist;
use App\Models\Tproyectoejecucion;
use App\Models\Tproyectosub;
use App\Models\Tdisciplina;
use App\Models\Tcontactocargo;
use App\Models\Tcatalogo;
use App\Models\Tformacotizacion;
use App\Models\Tpersona;
use App\Models\Tproyectoinformacionadicional;
use App\Models\Tproyectopersona;
use App\Models\Tconstrucciondetafecha;
use App\Models\Tproyectoejecucionconstruccion;
use App\Models\Tarea;
use App\Models\Tareasconocimiento;
use App\Models\Tleccionesaprendida;
use App\Models\Tproyectoejecucioncab;
use App\Models\Tcategoriaentregable;
use App\Models\Tproyectoactividadeshabilitacion;
use App\Models\Ttiposcontacto;
use App\Models\Tpropuestainformacionadicional;
use App\Models\Tproyectoentregablesrevisiones;
use App\Models\Tpersonadatosempleado;
use App\Models\Tvistastablaspersona;
use App\Models\Tcargo;


use App\Erp\GastosSupport;
use DB;
use Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Collection as Collection;
//use Maatwebsite\Excel\Facades\Excel;
use Session;
use Redirect;
use Carbon\Carbon;
use App\Erp\PropuestaSupport;
use App\Erp\ActividadSupport;
use Illuminate\Support\Facades\Input;
use PHPExcel;
use PHPExcel_IOFactory; 
use App\Erp\EnumSubsistema;
use App\Erp\ProyectoSupport;
use App\Erp\EmpleadoSupport;
use Response;
use PDF;
use App\Lib\EmailSend;
use App\Erp\FlujoLE;

class ProyectoController extends Controller
{

    public function listaGrid(){
       
            return Datatables::queryBuilder(DB::table('tproyecto as pry')
            ->join('testadoproyecto as te','pry.cestadoproyecto','=','te.cestadoproyecto')
            ->leftjoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')
            ->leftjoin('tpersona as tpg','pry.cpersona_gerente','=','tpg.cpersona')
            ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')
            ->leftjoin('tproyectopersona as pcp','pry.cproyecto','=','pcp.cproyecto')
            ->leftjoin('tpersona as percp','pcp.cpersona','=','percp.cpersona')
            ->leftjoin('tproyectopersona as pcd','pry.cproyecto','=','pcd.cproyecto')
            ->leftjoin('tpersona as percd','pcd.cpersona','=','percd.cpersona')
           
            ->whereIn('pcp.cpersona',function($query){
                $query->select('pcp.cpersona')
                ->from('tproyectopersona as pcp')
                ->where('pcp.cdisciplina','=','10')
                ->where('pcp.eslider','=','1')
                ->whereRaw('pcp.cproyecto = pry.cproyecto');
             }
            )          
            ->whereIn('pcd.cpersona',function($query){
                $query->select('pcd.cpersona')
                ->from('tproyectopersona as pcd')
                ->where('pcd.cdisciplina','=','11') 
                ->where('pcd.eslider','=','1')
                ->whereRaw('pcd.cproyecto = pry.cproyecto');
             }
            ) 

            ->where('pry.cestadoproyecto','!=','000')
            ->select('percp.abreviatura as nomcp','cli.nombre as cliente','tu.nombre as uminera','pry.codigo','pry.nombre as nombrepry','pry.finicio','pry.fcierre','tpg.abreviatura as gerente','pry.cproyecto','te.descripcion',
                DB::raw('CONCAT(\'row_\',pry.cproyecto)  as "DT_RowId"'))
             ->addSelect('percd.abreviatura as nomcd'))
            ->make(true); 
          
            /********************************************************************/
//            return Datatables::queryBuilder(DB::table('tproyecto as pry')
//             ->join('testadoproyecto as te','pry.cestadoproyecto','=','te.cestadoproyecto')
//             ->leftjoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')
//             ->leftjoin('tpersona as tpg','pry.cpersona_gerente','=','tpg.cpersona')
//             ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')          
//            ->select('cli.nombre as cliente','tu.nombre as uminera','pry.codigo','pry.nombre as nombrepry','pry.finicio','pry.fcierre','tpg.abreviatura as gerente','pry.cproyecto','te.descripcion',DB::raw('CONCAT(\'row_\',pry.cproyecto)  as "DT_RowId"'))
//            ->addSelect(DB::raw("(select case when 
// (select percd.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
// (select percd.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcd"))
//            ->addSelect(DB::raw("(select case when 
// (select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
// (select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcp"))
//        )->make(true); 
           // ->get();
       
    }

   public function listaPryTodos(){

         return Datatables::queryBuilder(DB::table('tproyecto')
                    ->join('testadoproyecto as te','tproyecto.cestadoproyecto','=','te.cestadoproyecto')
                    ->join('tpersona as tper','tproyecto.cpersonacliente','=','tper.cpersona')
                    ->join('tunidadminera as tu','tproyecto.cunidadminera','=','tu.cunidadminera')
                    ->leftJoin('tpersona as p','tproyecto.cpersona_gerente','=','p.cpersona')
                    ->leftJoin('tpersona as c','tproyecto.cpersona_coordinador','=','c.cpersona')
                    ->where('tproyecto.cestadoproyecto','!=','000')
                    ->select('tproyecto.cproyecto','tproyecto.codigo'
                        ,'tper.nombre as cliente','tu.nombre as uminera','tproyecto.nombre',
                    'p.abreviatura as GteProyecto','te.descripcion',
                    DB::raw('CONCAT(\'row_\',tproyecto.cproyecto)  as "DT_RowId"'))
                    )->make(true);
    }

   public function listarGrid(){

        return Datatables::queryBuilder(DB::table('tproyecto')
            ->join('testadoproyecto as te','tproyecto.cestadoproyecto','=','te.cestadoproyecto')
            ->join('tpersona as tper','tproyecto.cpersonacliente','=','tper.cpersona')
            ->join('tunidadminera as tu','tproyecto.cunidadminera','=','tu.cunidadminera')
            ->leftJoin('tpersona as p','tproyecto.cpersona_gerente','=','p.cpersona')
            ->leftJoin('tpersona as c','tproyecto.cpersona_coordinador','=','c.cpersona')
            // ->where('tproyecto.cestadoproyecto','=','001')
            ->whereIn('tproyecto.cestadoproyecto',['001','004','005'])
            ->select('tproyecto.cproyecto','tproyecto.codigo'
                ,'tper.nombre as cliente','tu.nombre as uminera','tproyecto.nombre', 
            'p.abreviatura as GteProyecto','te.descripcion',DB::raw('CONCAT(\'row_\',tproyecto.cproyecto)  as "DT_RowId"'))
            )->make(true);
    }
    public function listaProyectoEstados(){
        
    }

    public function listar(Request $request){

        if($request->estadoproyecto){
            $estadoinicial = $request->estadoproyecto;
            
        }
        else{
            $estadoinicial = '001';
        }
        //lardin 
        $proyge= ['00002','ninguna'];//carga incial de los proyectos que generan ingresos        
        $genera=Tcatalogo::where('codtab','=','00056')//consulta de tcatalogo valiendo si genera o no ingresos
            //->where('descripcion','=','si')
            ->orderby('descripcion', 'desc')
            ->whereNotNull('digide')
            ->get();

        $ttipo=['1','2','3','4','5','6','7','8','9','10','11','12','13'];

        $tiproy= DB::table('ttipoproyecto as tpr') 
                ->select('tpr.ctipoproyecto','tpr.descripcion')
                ->get();   

        $Proyectos = $this->datosProyectos($estadoinicial, $proyge, $ttipo);

        $estadoproyecto = Testadoproyecto::whereNotIn('cestadoproyecto',['000'])//Busco el estado proyecto
                        ->orderby('descripcion', 'asc')//y ordeno por orden alfabetico
                        ->get();//fin

        $buscocargos2 = $this->obtenercabecera();

        $lidersis= $this->obtenerliderescabecera();
        //dd($lidersis);
        //fin


        return view('proyecto/listaProyecto',compact('Proyectos', 'buscocargos2', 'estadoproyecto', 'genera','tiproy','lidersis'));

   }
    private function obtenerliderescabecera(){
        $lideresarea = DB::table('tproyectopersona as tpers')
            ->leftjoin('tdisciplinaareas as tdisp','tpers.cdisciplina','=','tdisp.cdisciplina')
            ->leftjoin('tareas as tre','tdisp.carea','=','tre.carea')
            //->where('tpers.cdisciplina')
            ->where('tpers.seniors','=', null)
            ->wherenull('essoporte')
            ->where('tpers.eslider','=', '1')
            ->wherenotnull('tpers.cdisciplina')
            ->select('tpers.cdisciplina','tre.descripcion','tpers.eslider')->distinct()
            ->get();

        //dd($lideresarea);

        return $lideresarea;
    }

   private function obtenercabecera(){

       $cabecera = DB::table('tcargos as tcar') /*cabezera agrupada por disciplina*/
       ->wherenotnull('tcar.seniors')
       ->leftjoin('tdisciplina as tdisp','tcar.seniors','=','tdisp.cdisciplina')
       ->select('tcar.seniors','tdisp.descripcion','tdisp.cdisciplina')
       ->distinct()
       ->orderby('tdisp.descripcion', 'asc')
       ->get();

       foreach($cabecera as $Key => $cab){
           $cabecera[$Key]->cabecera = "Senior ".$cab->descripcion;
        }
       
       return $cabecera;
   }

   private function datosProyectos($estadoinicial, $proyge, $ttipo){ //Se creo una nueva funcion para poder mantener la seleccion del combox

            if($proyge[0] == 'ninguna' && $proyge[1] == 'ninguna'){
                $proyge=['00002','00001'];
            }

            if(empty($ttipo)){//validamos si hay un arrays vacio si es 
                $ttipo=['1','2','3','4','5','6','7','8','9','10','11','12','13'];/* muestra todo los tipos de proyectos */
            }
           
            $Proyectos = DB::table('tproyecto as pry')
                ->join('testadoproyecto as te','pry.cestadoproyecto','=','te.cestadoproyecto')
                ->leftjoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')
                ->leftjoin('tpersona as tpg','pry.cpersona_gerente','=','tpg.cpersona')
                ->leftjoin('tservicio as ts','ts.cservicio','=','pry.cservicio')
                ->leftjoin('ttipoproyecto as tt','tt.ctipoproyecto','=','pry.ctipoproyecto')
                ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')          
                ->select('cli.nombre as cliente','tu.nombre as uminera','pry.codigo','pry.nombre as nombrepry','fproyecto',
                    DB::raw("to_char(pry.finicio,'dd-mm-yy') as finicio"),
                    DB::raw("to_char(pry.fcierre,'dd-mm-yy') as fcierre"),'tpg.abreviatura as gerente','pry.cproyecto','te.descripcion','ts.descripcion as portafolio','tt.descripcion as tipo_proyecto',
                    DB::raw('CONCAT(\'row_\',pry.cproyecto)  as "DT_RowId"'))
                ->addSelect(DB::raw("(select case when 
                (select percd.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
                (select percd.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percd on pcp.cpersona=percd.cpersona where pcp.cdisciplina = 11 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcd"))
                ->addSelect(DB::raw("(select case when 
                (select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) is null then 'Sin asignar' else 
                (select percp.abreviatura as nomcp from tproyectopersona as pcp left join tpersona as percp on pcp.cpersona=percp.cpersona where pcp.cdisciplina = 10 and  pcp.eslider = '1' and pcp.cproyecto=pry.cproyecto) end) as nomcp"))
                ->addSelect('pry.tipoproy')
                ->addSelect('pry.ctipoproyecto')
                ->where('pry.cestadoproyecto','=',$estadoinicial)
                ->whereIn('pry.tipoproy',$proyge)
                ->whereIn('pry.ctipoproyecto',$ttipo)
                //->where('pry.cproyecto','=',4261)
                ->get();


       for ($a=0; $a < count($Proyectos); $a++) {
           $buscocargos = DB::table('tcargos as tcar')
                          ->wherenotnull('tcar.seniors')
                          ->leftjoin('tdisciplina as tdisp','tcar.seniors','=','tdisp.cdisciplina')
                          ->select('tcar.seniors','tdisp.descripcion','tdisp.cdisciplina')->distinct()
                          //->groupBy('tcar.seniors','tdisp.descripcion')
                          ->orderby('tdisp.descripcion', 'asc')
                          ->get();


           /** funcion obtener usuarios por cargo senior  y mostrar en la vista 'backend lardin'*/
           foreach($buscocargos as $tec ){

               //$codigo_cargo=$tec->ccargo;
               $codigo_descicplina='sen_'.$tec->seniors;

               $cdatos = DB::table('tproyectopersona as tps ')
                                ->leftjoin('tpersonadatosempleado as tper','tps.cpersona','=','tper.cpersona')
                                ->leftjoin('tpersona as tp','tper.cpersona','=','tp.cpersona')
                                ->leftjoin('tdisciplinaareas as tdispc','tper.carea','=','tdispc.cdisciplina')
                                ->where('tps.seniors','=',$tec->cdisciplina)
                                //->where('tper.ccargo', '=', $tec->ccargo)
                                ->where('tps.cproyecto','=',$Proyectos[$a]->cproyecto)
                              // ->where('tps.eslider','=','1')//modificacion
                                //->where('tps.seniors','=',$tec->seniors)
                                ->first();

               $Proyectos[$a]->$codigo_descicplina=$cdatos?$cdatos->abreviatura:'-'; /** fin de funcion 'backendlardin'*/

               }

           $lidersis= $this->obtenerliderescabecera();

           foreach ($lidersis as $lid){
               $codigo_descicplina_lideres ='ld_'.$lid->cdisciplina;
               //dd($lideres);
               $lider = DB::table('tproyectopersona as tps ')
                   ->leftjoin('tpersonadatosempleado as tper', 'tps.cpersona', '=', 'tper.cpersona')
                   ->leftjoin('tpersona as tp', 'tper.cpersona', '=', 'tp.cpersona')
                   ->leftjoin('tdisciplinaareas as tdispc', 'tper.carea', '=', 'tdispc.cdisciplina')
                   ->where('tps.seniors', '=', null)
                   //->where('tper.ccargo', '=', $tec->ccargo)
                   ->where('tps.cproyecto', '=', $Proyectos[$a]->cproyecto)
                   ->where('tps.eslider', '=', '1')
                   ->where('tps.cdisciplina', '=', $lid->cdisciplina)
                   ->first();

               $Proyectos[$a]->$codigo_descicplina_lideres= $lider ? $lider->abreviatura: '-';

           }
           //dd($Proyectos);



       }
           return $Proyectos;
   }
   //funcion pàra poder mantener la el estado del combo con todo el contenido de la tabla 'backend lardin'
    public function resultadosTablaProyectos(Request $request, $estado ){

        $buscocargos2 = $this->obtenercabecera();
        $lidersis= $this->obtenerliderescabecera();
        $proyge=$request->valor;
        $ttipo=$request->tipoproyecto;
        $Proyectos = $this->datosProyectos($estado, $proyge, $ttipo);

        return view('proyecto/listatable',compact('Proyectos', 'buscocargos2','lidersis')); //fin de combo manteniendo contenido
        
        }

    /* Aprobacion Propuesta */
    public function aprobacion(){
        $editar = false;
        return view('proyecto/aprobacionPropuesta',compact('editar'));
    }

    public function editarAprobacion(Request $request,$idPropuesta){
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->get();
        $propuesta = $propuesta[0];
        $persona  = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();
        $estadopropuesta = Db::table('testadopropuesta')
        ->orderBy('cestadopropuesta','ASC')
        ->lists('descripcion','cestadopropuesta');

        $objEmp = new EmpleadoSupport();


        $personal = $objEmp->getPersonalRol('GP','001','2') ;
        $personal = [''=>''] + $personal; 

       $editar = true;
        return view('proyecto/aprobacionPropuesta',compact('propuesta','persona','uminera','estadopropuesta','personal','editar'));
    }
    public function grabarAprobacion(Request $request){
        DB::beginTransaction();
        $cpropuesta=0;
        $cpropuesta= $request->input('cpropuesta');
        $propuesta = Tpropuestum::where('cpropuesta','=',$cpropuesta)->first();
        $propuesta->cpersona_gteproyecto = $request->input('gtepropuesta');
        $propuesta->descripcion= $request->propuestadescripcion;
        $propuesta->save();

        /* Grabar Proyecto */
        $proyecto=Tproyecto::where('cproyecto','=',$propuesta->cproyecto)->first();
        //dd($proyecto);
        //$proyecto = new Tproyecto();
        $proyecto->cpersonacliente = $propuesta->cpersona;
        $proyecto->codigo ="";
        $proyecto->cunidadminera = $propuesta->cunidadminera;
        $proyecto->nombre = $propuesta->nombre;
        $proyecto->descripcion = $propuesta->descripcion;
        /*$proyecto->cpersona_coordinardor = */
        $proyecto->fproyecto = Carbon::now();
        $proyecto->cpersona_gerente = $request->input('gtepropuesta');
        $proyecto->cmoneda = $propuesta->cmoneda;
        $proyecto->cmedioentrega = $propuesta->cmedioentrega;
        $proyecto->cservicio = $propuesta->cservicio;
        $proyecto->cestadoproyecto = "001"; 
        $proyecto->save();
        $cproyecto=$proyecto->cproyecto;

        //tproyectosub
        $proyectosub = DB::table('tpropuestapaquetes')
        ->where('cpropuesta','=',$cpropuesta)->get();
        foreach($proyectosub as $sub){
            $psub = new Tproyectosub();
            $psub->cproyecto = $cproyecto;
            $psub->codigosub = $sub->codigopaquete;
            $psub->descripcionsub= $sub->nombrepaquete;
            $psub->save();
        }

        //tproyectoestructuraparticipante
        $proestructura = DB::table('testructuraparticipante')
        ->where('cpropuesta','=',$cpropuesta)->get();

        foreach($proestructura as $est){
            $pest = new Tproyectoestructuraparticipante();
            $pest->cproyecto = $cproyecto;
            $pest->croldespliegue = $est->croldespliegue;
            if (!empty($est->cpersona_rol)){
                $pest->cpersona_rol = $est->cpersona_rol;
            }
            $pest->cdisciplina = $est->cdisciplina;
            $pest->tipoestructura = $est->tipoestructura;
            $pest->cestructurapropuesta = $est->cestructurapropuesta;
            $pest->rate = $est->rate;
            $pest->save();
        }

        //tproyectoactividades parent
        $proactividades = DB::table('tpropuestaactividades')
        ->where('cpropuesta','=',$cpropuesta)
        ->whereNull('cpropuestaactividades_parent')
        ->get();
        //dd($proactividades);
        $this->saveProyectoActividades($proactividades,$cproyecto,$cpropuesta);
        //tproyectoactividades 
        $proactividades = DB::table('tpropuestaactividades')
        ->where('cpropuesta','=',$cpropuesta)
        ->whereNotNull('cpropuestaactividades_parent')
        ->orderBy('cpropuestaactividades_parent','ASC')
        ->get();
        $this->saveProyectoActividades($proactividades,$cproyecto,$cpropuesta);
        //dd($proactividades);
        
        //tproyectocronogramaconstruccion
        //tproyectodisciplina
        $prodisciplina = DB::table('tpropuestadisciplina')
        ->where('cpropuesta','=',$cpropuesta)->get();
        foreach($prodisciplina as $dis){
            $pdis = new Tproyectodisciplina();
            $pdis->cproyecto = $cproyecto;
            $pdis->cdisciplina= $dis->cdisciplina;
            $pdis->cpersona_rdp= $dis->cpersona_rdp;
            $pdis->escontratista= $dis->escontratista;
            if(!empty($dis->cpersona_proveedor)){
                $pdis->cpersona_proveedor= $dis->cpersona_proveedor;
            }
            $pdis->save();
        }
        //tproyectogastos
        $progastos = DB::table('tpropuestagastos')
        ->where('cpropuesta','=',$cpropuesta)->get();
        foreach($progastos as $gas){
            $pgas = new Tproyectogasto();
            $pgas->cproyecto= $cproyecto;
            $pgas->item= $gas->item;
            $pgas->concepto= $gas->concepto;
            $pgas->cantidad = $gas->cantidad;
            $pgas->unidad= $gas->unidad;
            $pgas->costou= $gas->costou;
            $pgas->comentario= $gas->comentario;
            if(!empty($gas->cpersona_proveedor)){
                $pgas->cpersona_proveedor=$gas->cpersona_proveedor;
            }
            if(!empty($gas->cpropuestapaquete)){
                $pgas->cproyectosub= $gas->cpropuestapaquete;
            }
            $pgas->save();
        }
        //tproyectogastoslaboratorio
        $progastoslab = DB::table('tpropuestagastoslaboratorio')
        ->where('cpropuesta','=',$cpropuesta)->get();
        foreach($progastoslab as $gasl){
            $pgasl = new Tproyectogastoslaboratorio();
            $pgasl->cproyecto = $cproyecto;
            $pgasl->item = $gasl->item;
            $pgasl->concepto = $gasl->concepto;
            $pgasl->preciou = $gasl->preciou;
            if( !empty($gasl->cmoneda)){
                $pgasl->cmoneda = $gasl->cmoneda;
            }
            $pgasl->cantcimentacion = $gasl->cantcimentacion;
            $pgasl->cantcanteras = $gasl->cantcanteras;
            $pgasl->cantbotadero = $gasl->cantbotadero;
            if(!empty($gasl->cpersona_proveedor)){
                $pgasl->cpersona_proveedor = $gasl->cpersona_proveedor;
            }
            if(!empty($gasl->cpropuestapaquete)){
                $pgasl->cproyectosub = $gasl->cpropuestapaquete;
            }

            $pgasl->save();
        }

        //confirma propuesta
        //$propuesta->cproyecto=$proyecto->cproyecto;
        $propuesta->cestadopropuesta ='007';
        $propuesta->save();

        /* Fin Grabar Proyecto */
        DB::commit();
        // return Redirect::route('editarAprobacionPropuesta',$cpropuesta);
        $editar = false;
        return view('proyecto/aprobacionPropuesta',compact('editar'));
    }
    public function saveProyectoActividades($proactividades,$cproyecto,$cpropuesta){
        foreach($proactividades as $act){
            $pact = new Tproyectoactividade();
            $pact->cproyecto = $cproyecto;
            $pact->cactividad= $act->cactividad;
            $pact->codigoactvidad= $act->codigoactividad;
            $pact->descripcionactividad= $act->descripcionactividad;
            $pact->numerodocumentos= $act->numerodocumentos;
            $pact->numeroplanos = $act->numeroplanos;
            if(!empty($act->cpersona_proveedor)){
                $pact->cpersona_proveedor=$act->cpersona_proveedor;
            }
            if(!empty($act->cpropuestapaquete)){
                $propaq = Tpropuestapaquete::where('cpropuestapaquete','=',$act->cpropuestapaquete)->first();
                    if($propaq){
                        $prosub = Tproyectosub::where('cproyecto','=',$cproyecto)
                        ->where('codigosub','=',$propaq->codigopaquete)
                        ->first();
                        if($prosub){
                            $pact->cproyectosub= $prosub->cproyectosub;
                        }
                    }
            }
            if(!is_null($act->cpropuestaactividades_parent)){
                $tt = DB::table('tpropuestaactividades')
                ->where('cpropuesta','=',$cpropuesta)
                ->where('cpropuestaactividades','=',$act->cpropuestaactividades_parent)
                ->first();
                if($tt){
                    $pp=DB::table('tproyectoactividades')
                    ->where('cproyecto','=',$cproyecto)
                    ->where('codigoactvidad','=',$tt->codigoactividad)
                    ->first();
                    if($pp){
                        $pact->cproyectoactividades_parent = $pp->cproyectoactividades;
                    }
                }
            }
            $pact->save();
            //Tproyectohonorarios
            $prohonorarios = DB::table('tpropuestahonorarios')
            ->where('cpropuestaactividades','=',$act->cpropuestaactividades)->get();
            foreach($prohonorarios as $hono){
                $phono = new Tproyectohonorario();
                $phono->cproyectoactividades = $pact->cproyectoactividades;
                $phono->horas= $hono->horas;
                $phono->xmlareadetalle = $hono->xmlareadetalle;
                if(!empty($hono->cdisciplina)){
                    $phono->cdisciplina = $hono->cdisciplina;
                }
                if(!empty( $hono->cpropuestapaquete)){
                    $propaq = Tpropuestapaquete::where('cpropuestapaquete','=',$hono->cpropuestapaquete)->first();
                    if($propaq){
                        $prosub = Tproyectosub::where('cproyecto','=',$cproyecto)
                        ->where('codigosub','=',$propaq->codigopaquete)
                        ->first();
                        if($prosub){
                            $phono->cproyectosub = $prosub->cproyectosub;
                        }
                    }
                }
                if(!empty($hono->cproyecto_proveedor)){
                    $phono->cproyecto_proveedor= $hono->cproyecto_proveedor;
                }
                $proest = Tproyectoestructuraparticipante::where('cestructurapropuesta','=',$hono->cestructurapropuesta)->first();
                if($proest){
                  $phono->cestructuraproyecto= $proest->cestructuraproyecto;  
                }
                $phono->save();
            }
        }
        
    }
    /* Fin Aprobación de Propuesta */

    /* Preproyecto*/
    public function preproyecto(){
        
        /*$servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = [''=>''] + $servicio;  */

        $servicio = DB::table('tservicio')->orderBy('orden','ASC')->lists('descripcion','cservicio');
        $servicio = [''=>''] + $servicio;

        //$tipoproyecto = DB::table('ttipoproyecto')->lists('descripcion','ctipoproyecto');
        //$tipoproyecto = [''=>''] + $tipoproyecto;         
 
     

        $tipogestionproyecto = DB::table('ttipogestionproyecto')->lists('descripcion','ctipogestionproyecto');
        $tipogestionproyecto = [''=>''] + $tipogestionproyecto; 

        $formacotiza = DB::table('tformacotizacion')->get();
        $estadopropuesta = Testadopropuestum::lists('descripcion','cestadopropuesta')->all();
        $estadopropuesta = ['' => ''] +$estadopropuesta;
        $objEmp = new EmpleadoSupport();


        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp; 

        $personal_cp = $objEmp->getPersonalRol('CP','001','2') ;
        $personal_cp = [''=>''] + $personal_cp; 

        $tipopropresentacion = DB::table('ttipospropuestapresentacion')->get();
        $revision="0";  //Se aprueba directamente para la creación del Proyecto.
        

        //$monedas = DB::table('tmonedas')->get();
        $monedas = DB::table('tmonedas')
        ->lists('descripcion','cmoneda');
        $monedas = [''=>''] + $monedas;

        $medioentrega = DB::table('tmedioentrega')->get();

        $aniocod= Carbon::now();
        $aniocod = $aniocod->year;
        $aniocod=substr($aniocod, 2,2);
        //DD($aniocod);


        $tsedes=DB::table('tsedes')
        ->lists('descripcion','codigosede');
        //$tsedes = [''=>''] + $tsedes;

        $codsede=null;;


        $correlativo=null;

        $tdocu = Tcatalogo::where('codtab','=','00012')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();
        $tdocu = [''=>''] + $tdocu;

        $editar=false;        

        
        return view('proyecto/registroPreProyecto',compact('servicio','formacotiza','estadopropuesta','personal_gp','personal_cp','tipopropresentacion','revision','monedas','medioentrega','editar','aniocod','tsedes','correlativo','codsede','tipogestionproyecto','tdocu'));
    }

    public function editarPreproyecto(Request $request,$idPropuesta){
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->get();
        $propuesta = $propuesta[0];
        $persona  = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();
        $servicio = DB::table('tservicio')->lists('descripcion','cservicio');
        $tipoproyecto = DB::table('ttipoproyecto')->lists('descripcion','ctipoproyecto');

        $tipogestionproyecto = DB::table('ttipogestionproyecto')->lists('descripcion','ctipogestionproyecto');      
        $formacotiza = DB::table('tformacotizacion')->get();
        //$estadopropuesta = Db::table('testadopropuesta')->lists('descripcion','cestadopropuesta');

        $objEmp = new EmpleadoSupport();


        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp; 

        $personal_cp = $objEmp->getPersonalRol('CP','001','2') ;
        $personal_cp = [''=>''] + $personal_cp; 


        $tipopropresentacion = DB::table('ttipospropuestapresentacion')->get();

        //pendiente devolver los valores de tpropuestapresentacion
        //$monedas = DB::table('tmonedas')->get();

        $monedas = DB::table('tmonedas')
        ->lists('descripcion','cmoneda');
        $monedas = [''=>''] + $monedas; 
      
        $medioentrega = DB::table('tmedioentrega')->get();
        $propresentacion = DB::table('tpropuestapresentacion')->where('cpropuesta','=',$propuesta->cpropuesta)->get();
        $revision=""; // se utiliza en la vista si no existe propuesta->revision 
        $editar=true;


        //Estado 
        $testado = DB::table('tproyecto')
        ->lists('descripcion','cestadoproyecto');
        $testado = [''=>''] + $testado;
        
        $tfase = DB::table('tfasesproyecto')
        ->lists('descripcion','cfaseproyecto');
        $tfase = [''=>''] + $tfase;

        $tsedes=DB::table('tsedes')       
        ->lists('descripcion','codigosede');

        //dd($propuesta->ccodigo,strlen($propuesta->ccodigo));

        $aniocod=null;
        $codsede=null;
        $correlativo=null;

        if(strlen($propuesta->ccodigo)>0){

            $codigopropuesta=explode('.', $propuesta->ccodigo);       

            $aniocod='20'.$codigopropuesta[0];
            $aniocod=intval($aniocod);        

            $codsede=$codigopropuesta[1];

            $correlativo=$codigopropuesta[2];

        }


        return view('proyecto/registroPreProyecto',compact('propuesta','persona','editar','uminera','servicio','formacotiza','personal_gp','personal_cp','tipopropresentacion','monedas','medioentrega','revision','propresentacion','testado','tfase','aniocod','tsedes','correlativo','codsede','tipoproyecto','tipogestionproyecto'));        
        
    }
    public function grabarPreproyecto(Request $request){

        //dd($request);
        DB::beginTransaction();
        $cpropuesta= $request->input('cpropuesta');
        if(strlen($cpropuesta)>0){
            $propuesta = Tpropuestum::where('cpropuesta','=',$cpropuesta)->first();
            $propresentacion = Tpropuestapresentacion::where('cpropuesta','=',$cpropuesta)->delete();

        }else{
            $propuesta = new Tpropuestum();

        }

        if(strlen(trim($propuesta->ccodigo)) <= 0 ){
            $prop = new HelperSIGSupport();
           // $propuesta->ccodigo =  $prop->getCodigoPropuesta();
        }

        /** Asignar Valores de los Campos
        **/
        $propuesta->cpersona = $request->input('cpersona');
        $propuesta->cunidadminera = $request->input('cunidadminera');
        $propuesta->ccodigo = $request->input('codigoprop');
        $propuesta->nombre = $request->input('propuestanombre');
        $propuesta->descripcion = $request->input('propuestadescripcion');

        $propuesta->cestadopropuesta ='006';
        // $propuesta->cestadopropuesta ='001';
        $propuesta->tag ='';
        $propuesta->revision = '0';
        $propuesta->ftipo = '1';
        $propuesta->cformacotizacion = '1';
        $propuesta->cservicio = $request->input('servicio');

        if(strlen($request->input('tipoproyecto')>0)){
            $propuesta->ctipoproyecto= $request->input('tipoproyecto');
        }else{
            $propuesta->ctipoproyecto=null;
        }

        if(strlen($request->input('tipgestionproy')>0)){
            $propuesta->ctipogestionproyecto= $request->input('tipgestionproy');
        }else{
            $propuesta->ctipogestionproyecto=null;
        }

        //$propuesta->cpersona_revprincipal = $request->input('revprincipal');
        $propuesta->cpersona_coordinadorpropuesta = $request->input('coorpropuesta');
        //$propuesta->cpersona_resppropuesta = $request->input('respropuesta');
        $propuesta->cpersona_gteproyecto = $request->input('gtepropuesta');
        //$propuesta->observaciones = $request->input('propuestaobservaciones');

        $tsedes=DB::table('tsedes')
        ->where('codigosede','=',$request->input('sedecod'))
        ->first();

        $csede=null;


        if ($tsedes) {
             $csede=$tsedes->csede;
           
        }       
        $propuesta->csede = $csede;

        $propuesta->cmoneda = $request->input('moneda');
        //$propuesta->cmedioentrega = $request->input('mentrega');

        /* Fin Asignación de Campos ----*/    

 

        $propuesta->save();


        /* Grabar paquete */
        $paq = new Tpropuestapaquete();       
        $paq->cpropuesta=$propuesta->cpropuesta;
        $paq->codigopaquete='000';
        $paq->nombrepaquete='Generales';
        $paq->save();
        
        /* Grabar Propuesta información adicional */
        $prop_infoad=new Tpropuestainformacionadicional();
        $prop_infoad->cpropuesta=$propuesta->cpropuesta;

        if(!empty($request->input('docaproba'))){
            $prop_infoad->cdocaprobacion = $request->input('docaproba');
        }
        if(!empty($request->input('nrodocaproba'))){
            $prop_infoad->nrodocaprobacion = $request->input('nrodocaproba');
        }

        $prop_infoad->save();

        // Array presentacion de Propuesta;
      /*  $presentacion = Input::get('presentacion');
        $i=0;
        foreach($presentacion as $p){
            $i++;
            DB::table('tpropuestapresentacion')->insert(
                ['cpropuesta' => $propuesta->cpropuesta, 'cpresentacion' => $p,'item' => $i]
            );
        } */


        
        /* Grabar Proyecto */
        $proyecto = new Tproyecto();
        $proyecto->cpersonacliente = $propuesta->cpersona;
        $proyecto->codigo ="";
        $proyecto->cunidadminera = $propuesta->cunidadminera;
        $proyecto->nombre = $propuesta->nombre;
        $proyecto->descripcion = $propuesta->descripcion;
        /*$proyecto->cpersona_coordinardor = */
        
        $proyecto->fproyecto = Carbon::now();
        $proyecto->cpersona_gerente = $request->input('gtepropuesta');
        $proyecto->cmoneda = $propuesta->cmoneda;
        $proyecto->cmedioentrega = $propuesta->cmedioentrega;
        $proyecto->cservicio = $propuesta->cservicio;
        $proyecto->ctipoproyecto = $propuesta->ctipoproyecto;
        $proyecto->ctipogestionproyecto=$propuesta->ctipogestionproyecto;
        $proyecto->cestadoproyecto = "001"; 

        if($request->input('servicio')==7){
           // dd($request->input('servicio'));
            $proyecto->tipoproy='00001';
            
        }
        else{
            $proyecto->tipoproy='00002';

        }
        $proyecto->save();


        $subp = new Tproyectosub();
        $subp->cproyecto =$proyecto->cproyecto;
        $subp->codigosub='000';
        $subp->descripcionsub='Generales';
        $subp->save();
        $propuesta->cproyecto=$proyecto->cproyecto;
        $propuesta->save();
        /* Fin Grabar Proyecto */

        
        DB::commit();
        $cpropuesta = $propuesta->cpropuesta;

        return Redirect::route('editarPreproyecto',$cpropuesta);
    }  

    public function listTipoPry($idServicio){
    
        $tipoproyecto = DB::table('ttipoproyecto')
        ->where('cservicio','=',$idServicio)
        ->lists('descripcion','ctipoproyecto');

         return Response::json($tipoproyecto);

    }

    /* Fin de Preproyecto*/


    public function disciplinaProyecto(){
        //$disciplinas = DB::table('tdisciplina')->get();
        
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;
        $bsave=false;
        return view('proyecto/disciplinaProyecto',compact('servicios','bsave'));
    }   

    public function editarDisciplinaProyecto(Request $request,$idProyecto){


        Session::forget('proyectodis');
        Session::forget('delDisciplinas');
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        //dd($proyecto);
        $persona = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();



        $tdisciplinas = DB::table('tdisciplina as d')
        ->join('tdisciplinaareas as da','da.cdisciplina','=','d.cdisciplina')
        ->select('d.*')
        ->distinct()
        ->get();
        $disciplinas = array();
        foreach($tdisciplinas as $tdis){
            $d['cdisciplina'] = $tdis->cdisciplina;
            $d['descripcion'] = $tdis->descripcion;
            $d['tipodisciplina'] = $tdis->tipodisciplina;
            $da = array();
            $dis_area = DB::table('tdisciplinaareas as da')
            ->join('tareas as a','da.carea','=','a.carea')
            ->join('tpersonadatosempleado as pd','a.carea','=','pd.carea')
            ->join('tpersona as p','pd.cpersona','=','p.cpersona')
            ->where('da.cdisciplina','=',$d['cdisciplina'])
            ->select('p.*','pd.carea','da.cdisciplina')
            ->get();
            $d['da']= $dis_area;
            $disciplinas[count($disciplinas)] = $d;
        }
        
        $tproyectodisciplina = DB::table('tproyectodisciplina as p')
        ->join('tdisciplina as d','p.cdisciplina','=','d.cdisciplina')
        ->leftJoin('tpersona as rdp','p.cpersona_rdp','=','rdp.cpersona')
        ->where('cproyecto','=',$proyecto->cproyecto)
        ->select("p.*",'d.descripcion','rdp.nombre as name_rdp')
        ->get();
        $proyectodisciplina = array();
        foreach($tproyectodisciplina as $dis){
            $pd['cproyectodisciplina']=$dis->cproyectodisciplina;
            $pd['cdisciplina']=$dis->cdisciplina;
            $pd['cpersona_rdp']=$dis->cpersona_rdp;
            $pd['descripcion'] = $dis->descripcion;
            $pd['name_rdp'] = $dis->name_rdp;
            $proyectodisciplina[count($proyectodisciplina)]=$pd;
        }
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;        

        $bsave=true;
        Session::put('proyectodis',$proyectodisciplina);
        return view('proyecto/disciplinaProyecto',compact('servicios','proyecto','persona','uminera','proyectodisciplina','bsave','disciplinas'));
    }
    public function verDisciplinas(Request $request){
        $proyectodisciplina=array();
        if(Session::get('proyectodis')){
            $proyectodisciplina = Session::get('proyectodis');
        }        
        return view('proyecto.tableDisciplinaProyecto',compact('proyectodisciplina'));
    }
    public function agregarDisciplinasProyecto(Request $request){
        $proyectodisciplina=array();
        if(Session::get('proyectodis')){
            $proyectodisciplina = Session::get('proyectodis');
        }
        $tdisciplinas = DB::table('tdisciplina')->get();


        foreach($tdisciplinas as $dis){
            if(!is_null($request->input('rad_'.$dis->cdisciplina)) ){
             
                $i = -1 * (count($proyectodisciplina) + 10)*2;
                $pd['cproyectodisciplina']= $i;
                $pd['cdisciplina']=$dis->cdisciplina;
                $valor = $request->input('rad_'.$dis->cdisciplina);
                $aValor  = explode('_',$valor);
                $pd['cpersona_rdp'] = $aValor[1];
                $tpersona = DB::table('tpersona')
                ->where('cpersona','=',$pd['cpersona_rdp'])
                ->first();
                $pd['descripcion'] = $dis->descripcion;
                $pd['name_rdp'] = $tpersona->nombre;
                $proyectodisciplina[count($proyectodisciplina)] = $pd;
            }
        }

        Session::put('proyectodis',$proyectodisciplina);
        
        return view('proyecto.tableDisciplinaSeleccionado',compact('proyectodisciplina'));
    }
    public function borrarDisciplina(Request $request){
        $proyectodisciplina=array();
        $delDisciplinas = array();
        if(Session::get('delDisciplinas')){
            $delDisciplinas = Session::get('delDisciplinas');
        }
        $aDel = $request->input('chk');
        foreach($aDel as $del){
            if($del > 0){
                $delDisciplinas[count($delDisciplinas)] = $del;
            }

        }
        Session::put('delDisciplinas',$delDisciplinas);  

        if(Session::get('proyectodis')){
            $proyectodisciplina = Session::get('proyectodis');
        }

        $new = array();
   
        foreach($proyectodisciplina as $pd){
            foreach($aDel as $del){
                        $new[count($new)]=$pd;
                        
                
            }

        }        
        $proyectodisciplina = $new;

        Session::put('proyectodis',$proyectodisciplina);                
        return view('proyecto.tableDisciplinaSeleccionado',compact('proyectodisciplina'));
    }    

    public function grabardisciplinaProyecto(Request $request){
        DB::beginTransaction();
        $cproyecto= $request->input('cproyecto');
        
        //dd($disciplina);
        $proyectodisciplina=array();
        $delDisciplinas = array();
        if(Session::get('delDisciplinas')){
            $delDisciplinas = Session::get('delDisciplinas');
        }
        if(Session::get('proyectodis')){
            $proyectodisciplina = Session::get('proyectodis');
        }
        
        foreach($delDisciplinas as $d){
            Tproyectodisciplina::where('cpropuestadisciplina','=',$d)->delete();
        }     
        foreach($proyectodisciplina as $p){
            if($p['cproyectodisciplina']<=0){
                $prod = new Tproyectodisciplina();
            }else{
                $prod = Tproyectodisciplina::where('cproyectodisciplina','=',$p['cproyectodisciplina'])->first();
            }
            $prod->cproyecto=$cproyecto;
            $prod->cdisciplina = $p['cdisciplina'];
            $prod->cpersona_rdp = $p['cpersona_rdp'];
            $prod->save();
        }           
        DB::commit();
        
        return Redirect::route('editarDisciplinaProyecto',$cproyecto);
    }



    /* Estructura de Proyecto*/
    public function estructuraProyecto(){
        Session::forget('pactividades');
        $actiParent= DB::table('tactividad')->whereNull('cactividad_parent')->get();
        $actividades = array();
        $objAct  = new ActividadSupport();
        foreach ($actiParent as $act) {
            $actividades[count($actividades)]=$act;
            $actividades=$objAct->listarActividad($actividades,$act);
        }
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;         
        return view('proyecto/estructuraActividades',compact('servicios','actividades'));
    }
    public function editarEstructuraProyecto(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->get();
        $proyecto = $proyecto[0];
        Session::forget('pactividades');
        Session::forget('participantes');
        Session::forget('delActividad');
        $actiParent= DB::table('tactividad')->whereNull('cactividad_parent')->get();
        $actividades = array();
        $objAct  = new ActividadSupport();
        foreach ($actiParent as $act) {
            $actividades[count($actividades)]=$act;
            $actividades=$objAct->listarActividad($actividades,$act);
        }

        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = [''=>''] + $servicio;  
        
        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;
        /*$personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); */

        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); 
        $personal = [''=>''] + $personal;

        $personal_rate = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->get();        
        
        $subproyecto = DB::table('tproyectosub')->where('cproyecto','=',$proyecto->cproyecto)->lists('descripcionsub','cproyectosub');
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $revision=""; 
        $editar=true;
        
        /*$actividadesp = DB::table('tproyectoactividades')->where('cproyecto','=',$idProyecto)->get();
        Session::put('pactividades',$actividadesp);*/
        $tactividades = DB::table('tproyectoactividades as proact')
        ->join('tactividad as act','proact.cactividad','=','act.cactividad')
        ->where('act.tipoactividad','=','00001')
        ->where('cproyecto','=',$proyecto->cproyecto)
        ->select('proact.*','act.cactividad_parent')
        ->get();
        $actividadesp = array();
        foreach($tactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] ='';
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cactividad_parent;

            $actividadesp[count($actividadesp)] = $a;
        }        
        $actividadesp = $objAct->orderParentActividadEjecucion($actividadesp);
        Session::put('pactividades',$actividadesp);

        /* Estructura Treeview de Actividades*/
        $actParent= DB::table('tactividad as act')
        ->where('act.tipoactividad','=','00001')
        ->whereNull('cactividad_parent')
        ->get();
        $estructura="";

        
        foreach ($actParent as $act) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$act->cactividad. "',";
            $estructura=$estructura ."label: '".$act->descripcion ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$objAct->listarTreeview($act);
            //dd($est);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }
        /* Fin de estructura de Treeview Actividades*/


        $categorias = Troldespliegue::select('croldespliegue',DB::raw('CONCAT(codigorol,\'-\',descripcionrol) as descripcion'))
        ->where('csubsistema','=','001'/*new EnumSubsistema::PROPUESTA*/)
        ->where('tiporol','=','1')        
        ->lists('descripcion','croldespliegue')->all();
        $categorias = [''=>'']+ $categorias;

        $participantes= array();
        $tparticipantes = DB::table('tproyectoestructuraparticipantes as testru')
        ->join('troldespliegue as rol','rol.croldespliegue','=','testru.croldespliegue')
        ->leftJoin('tpersona as tper','tper.cpersona','=','testru.cpersona_rol')
        ->select('testru.cestructuraproyecto','testru.cproyecto','testru.croldespliegue','testru.cpersona_rol','testru.rate','tper.nombre','rol.descripcionrol')
        ->where('testru.cproyecto','=',$idProyecto)
        ->where('rol.csubsistema','=','001'/*new EnumSubsistema::PROYECTO*/)
        ->where('rol.tiporol','=','1')        
        /*->where('testru.tipoestructura','=','1')*/
        ->get();

        foreach($tparticipantes as $tpar){
            $participantes[count($participantes)] = array(
                'cestructuraproyecto' => $tpar->cestructuraproyecto,
                'cproyecto' => $tpar->cproyecto,
                'croldespliegue' => $tpar->croldespliegue,
                'cpersona_rol' => $tpar->cpersona_rol,
                'rate' => $tpar->rate,
                'nombre' => $tpar->nombre,
                'descripcionrol' => $tpar->descripcionrol
                );
        }

        Session::put('participantes',$participantes);
        $actividadesp = Session::get('pactividades',$actividadesp);
             
        return view('proyecto/estructuraActividades',compact('proyecto','persona','editar','uminera','servicio','personal','monedas','medioentrega','revision','estadoproyecto','participantes','estructura','categorias','actividadesp','subproyecto','actividades','personal_rate')); 
    }

    public function listarActividades(Request $request,$idProyecto){
        if (Session::get('pactividades')){
            $actividadesp= Session::get('pactividades');
            
        }else{
            if(strlen($idProyecto)> 0 && $idProyecto!='0'){
                $actividadesp=DB::table('tproyectoactividades')->where('cproyecto','=',$idProyecto)->get();
                
            }else{
                
                $actividadesp=array();

            }
        }
        return view('partials.tableActividadPropuesta',compact('actividadesp'));        
    }

    public function addActividad(Request $request){
        $acti = $request->input('selected_actividad');
        $tactividad = DB::table('tactividad')->whereIn('cactividad',$acti)->get();
        //dd($tactividad);
        $actividadesp = array();
        if(Session::get('pactividades')){
            $actividadesp = Session::get('pactividades');
        }
        $objAct = new ActividadSupport();
        foreach($tactividad as $a){
            $res = array_search($a->cactividad, array_map(function($e){return $e['cactividad'];},$actividadesp));
            //dd(array_map(function($e){return $e->cactividad;},$actividadesp));
            if( $res===false){
                //$wact = new Tproyectoactividade();
                $i= -1*(count($actividadesp)+1)*10;
                $wact['cproyectoactividades'] =$i;
                $wact['cactividad'] = $a->cactividad;
                $wact['codigoactividad'] = $a->codigo;
                $wact['descripcionactividad'] = $a->descripcion;
                $wact['numerodocumentos'] = 0;
                $wact['numeroplanos'] = 0;
                $wact['cproyectosub'] = $request->input('subproyecto');
                $wact['cactividad_parent'] = $a->cactividad_parent;
                $wact['item']='';
                $actividadesp[count($actividadesp)] = $wact;

                //Session::put('pactividades',$actividadesp); 
            }
        }
        $actividadesp = $objAct->orderParentActividadEjecucion($actividadesp);
        Session::put('pactividades',$actividadesp);
        //$actividadesp = Session::get('pactividades',array());
        return view('partials.tableActividadProyecto',compact('actividadesp'));  
    }  
    public function delActividad($idActividad){
        $actividadesp = array();
        if(Session::get('pactividades')){
            $actividadesp = Session::get('pactividades');
        }
        if($idActividad > 0){
            $delActividad=array();
            if(Session::get('delActividad')){
                $delActividad = Session::get('delActividad');
            }
            $delActividad[count($delActividad)]=$idActividad;
            Session::put('delActividad',$delActividad);
        }        
        $newactiv = array();
        //dd($actividadesp);
        foreach($actividadesp as $act){
            if ($act['cproyectoactividades']!=$idActividad){
                $newactiv[count($newactiv)]=$act;
            }
            

        }        
        $actividadesp = $newactiv;
        Session::put('pactividades',$actividadesp);
        return view('partials.tableActividadProyecto',compact('actividadesp'));  
    }
    public function addEstructura(Request $request){
        $categ= $request->input('categorias');
        $profe= $request->input('profesional');
        $rate = $request->input('rate');
        if(!empty($profe)){
            $personal = DB::table('tpersona')->where('cpersona','=',$profe)->first();
        }
        $rol= DB::table('troldespliegue')->where('croldespliegue','=',$categ)->first();
        if(empty($rate)){
            $rate=0;
        }

        $participantes=array();
        if(Session::get('participantes')){
            $participantes = Session::get('participantes',array());
        }
        $i= -1*(count($participantes)+1)*10;


        $participantes[count($participantes)] = array(
            'cestructuraproyecto' => $i ,
            'croldespliegue' => $categ,
            'cpersona_rol' => $profe,
            'rate' => $rate,
            'nombre' => isset($personal->nombre)==1?$personal->nombre:'',
            'descripcionrol' => $rol->descripcionrol
            );

        Session::put('participantes',$participantes);
        $participantes = Session::get('participantes',array());
        //dd($participantes);
        return view('partials.tableEstructuraProyecto',compact('participantes'));
    }

    public function delEstructura($idestructura){
        $participantes=array();
        if(Session::get('participantes')){
            $participantes = Session::get('participantes');
        }
        if($idestructura > 0){
            $delEstructura=array();
            if(Session::get('delEstructura')){
                $delEstructura = Session::get('delEstructura');
            }
            $delEstructura[count($delEstructura)]=$idestructura;
            Session::put('delEstructura',$delEstructura);
        }
        $newestr=array();
        foreach($participantes as $par){
            if($par['cestructuraproyecto']!=$idestructura){
                $newestr[count($newestr)]=$par;
            }
        }
        $participantes=$newestr;
        Session::put('participantes',$participantes);

        return view('partials.tableEstructuraProyecto',compact('participantes'));
    }

    public function grabarEstructuraProyecto(Request $request){
        DB::beginTransaction();
        $idProyecto = $request->input('cproyecto');
        $proyecto = Tproyecto::where('cproyecto','=',$idProyecto)->first();
        //$proyecto = $proyecto[0];

        $proyecto->cestadoproyecto= '001';
        $proyecto->finicio = $request->input('finicio');
        $proyecto->fcierre = $request->input('fcierre');

        $proyecto->save();
        $cproyecto=$proyecto->cproyecto;

        $actividadesp = Session::get('pactividades');
        //dd($request->input('txtnrodoc'));
        foreach($actividadesp as $act){     
            if ($act['cproyectoactividades'] <=0){
                $newact = new Tproyectoactividade;
                $newact->cproyecto = $cproyecto;
                $newact->cactividad = $act['cactividad'];
                $newact->codigoactvidad = $act['codigoactividad'];
                $newact->descripcionactividad = $act['descripcionactividad'];
            }else{
                $newact = Tproyectoactividade::where('cproyectoactividades','=',$act['cproyectoactividades'])
                ->first();                
            }
            $newact->numerodocumentos=$request->input('txtnrodoc')[$act['cactividad']];
            $newact->numeroplanos = $request->input('txtnropla')[$act['cactividad']];
            $newact->cproyectosub = $act['cproyectosub'];
            $newact->save();
            
        }
        $delActividades=array();
        if(Session::get('delActividad')){
            $delActividades = Session::get('delActividad');
        }
        foreach($delActividades as $d){
            $delAct = Tproyectoactividade::find($d);
            $delAct->delete();
        }

        $participantes=array();
        if(Session::get('participantes')){
            $participantes = Session::get('participantes');
        }
        foreach($participantes as $part){
            if($part['cestructuraproyecto'] <= 0 ){
                $est = new Tproyectoestructuraparticipante();
                $est->cproyecto = $idProyecto;
                $est->croldespliegue =$part['croldespliegue'];
                $est->cpersona_rol= $part['cpersona_rol'];
                $est->tipoestructura='0';
                $est->rate= $part['rate'];
                $est->save();
            }
        }
        $delEstructura=array();
        if(Session::get('delEstructura')){
            $delEstructura = Session::get('delEstructura');
        }
        foreach($delEstructura as $d){
            $est = Tproyectoestructuraparticipante::find($d);
            $est->delete();
        }
        /* Agregar al Proyecto actividades Adm*/
        /*$tactAdm = DB::table('tactividad as act')
        ->where('act.tipoactividad','=','00002')
        ->get();
        foreach($tactAdm as $adm){
            $obj= Tproyectoactividade::where('cactividad','=',$adm->cactividad)
            ->where('cproyecto','=',$idProyecto)
            ->first();
            if(!$obj){
                $newact = new Tproyectoactividade;
                $newact->cproyecto = $idProyecto;
                $newact->cactividad = $adm->cactividad;
                $newact->codigoactvidad = $adm->codigo;
                $newact->descripcionactividad = $adm->descripcion;
                $newact->numerodocumentos=0;
                $newact->numeroplanos = 0;                
                $newact->save();

            }
        }*/

        /* Fin Agregar al Proyecto actividades Adm */

        DB::commit();
        return Redirect::route('editarEstructuraProyecto',$cproyecto);
    }     
    /*  Fin de Estructura de Proyecto*/

    /* Especificacion de Proyecto */
    public function especificacion(){
        return view('proyecto/especificacionProyectos');
    }
    public function editarEspecificacion(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->get();
        $proyecto = $proyecto[0];
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = DB::table('tservicio')->lists('descripcion','cservicio');
        Session::forget('listGastosPry');
        Session::forget('delGastosPry');
        Session::forget('listGastosLabPry');
        Session::forget('delGastosLabPry');

        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$proyecto->cproyecto)
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] ='';
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;
        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); 
        
        $paqpropuesta = DB::table('tproyectosub')->where('cproyecto','=',$proyecto->cproyecto)->lists('descripcionsub','cproyectosub');
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $revision=""; 
        $editar=true;    

        $objPro = new ProyectoSupport();
        $listEstr = $objPro->getProfesionalesSenior($proyecto->cproyecto);
        $listDis = $objPro->getDisciplinas($proyecto->cproyecto);
        $listEstr_dis = $objPro->getProfesionalesDisciplina($proyecto->cproyecto);
        $listEstr_apo = $objPro->getProfesionalesApoyo($proyecto->cproyecto);


        /* Estructura de Gastos*/
        $tipoGasto='00003';
        $gasParent= DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=',$tipoGasto)
        ->whereNull('cconcepto_parent')
        ->get();
        $estructura="";

        $objGas  = new GastosSupport();
        foreach ($gasParent as $gas) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$gas->cconcepto. "',";
            $estructura=$estructura ."label: '".$gas->descripcion ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$objGas->listarTreeview($gas,$tipoGasto);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }
        /* Fin de estructura de Gastos*/

        /* Estructura de Gastos Laboratorio*/
        $tipoGasto='00002';
        $gasParent= DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=',$tipoGasto)
        /*->whereNull('cconcepto_parent')*/
        ->get();
        $estructuraLab="";

        //$objGas  = new GastosSupport();
        foreach ($gasParent as $gas) {
            if(!empty($estructuraLab)){
                $estructuraLab = $estructuraLab .",";
            }
            //$actividades[count($actividades)]=$act;
            $estructuraLab=$estructuraLab ."{\n\r";
            $estructuraLab=$estructuraLab ."item: {\n\r";
            $estructuraLab=$estructuraLab ."id: '".$gas->cconcepto. "',";
            $estructuraLab=$estructuraLab ."label: '".$gas->descripcion ."',";
            
            $estructuraLab=$estructuraLab ."checked: false ";
            $estructuraLab=$estructuraLab ."}\n\r";

            
            /*
            
            $est=$objGas->listarTreeview($gas,$tipoGasto);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }*/

            $estructuraLab=$estructuraLab ."}\n\r";
        }
        /* Fin de estructura de Gastos de Laboratorio*/

        /* Obtener lista de Gastos Grabados*/
        $tgastos = DB::table('tproyectogastos as progas')
        ->join('tconceptogastos as congas','progas.concepto','=','congas.cconcepto')
        ->where('progas.cproyecto','=',$idProyecto)
        ->select('progas.*','congas.descripcion','congas.cconcepto_parent')
        ->orderBy('progas.item','ASC')
        ->get();
        $gastos = array();
        foreach($tgastos as $tgas){
            $g['cproyectogastos'] = $tgas->cproyectogastos;
            $g['cproyecto'] = $tgas->cproyecto;
            $g['item'] = $tgas->item;
            $g['concepto'] = $tgas->concepto;
            $g['descripcion'] = $tgas->descripcion;
            $g['unidad']=$tgas->unidad;
            $g['cantidad'] = $tgas->cantidad;
            $g['costou'] =  $tgas->costou;
            $g['subtotal'] = $tgas->cantidad * $tgas->costou;
            $g['cconcepto_parent'] = $tgas->cconcepto_parent;
            $g['comentario'] = $tgas->comentario;
            $g['cpersona_proveedor'] = $tgas->cpersona_proveedor;
            $g['cproyectosub'] = $tgas->cproyectosub;
            $gastos[count($gastos)]=$g;
        }
        /* Fin de Obtener lista de Gastos Grabados*/

        $objGas  = new GastosSupport();
        $gastos = $objGas->orderParent($gastos);
        Session::put('listGastosPry',$gastos);


        /* Obtener lista de Gastos Grabados laboratorio*/
        $tgastosLab = DB::table('tproyectogastoslaboratorio as progas')
        ->join('tconceptogastos as congas','progas.concepto','=','congas.cconcepto')
        ->where('progas.cproyecto','=',$idProyecto)
        ->select('progas.*','congas.descripcion','congas.cconcepto_parent')
        ->orderBy('progas.item','ASC')
        ->get();
        $gastosLab = array();

        foreach($tgastosLab as $tgasl){
            $g['cproyectogastoslab'] = $tgasl->cproyectogastoslab;
            $g['cproyecto'] = $tgasl->cproyecto;
            $g['item'] = $tgasl->item;
            $g['concepto'] = $tgasl->concepto;
            $g['descripcion'] = $tgasl->descripcion;
            $g['cmoneda']=$tgasl->cmoneda;
            $g['cantcimentacion'] = $tgasl->cantcimentacion;
            $g['cantcanteras'] = $tgasl->cantcanteras;
            $g['cantbotadero'] = $tgasl->cantbotadero;
            $g['preciou'] =  $tgasl->preciou;
            $g['subtotal'] =  $tgasl->preciou * ($tgasl->cantcimentacion + $tgasl->cantcanteras + $tgasl->cantbotadero);
            $g['cpersona_proveedor'] = $tgasl->cpersona_proveedor;
            $g['cproyectosub'] = $tgasl->cproyectosub;
            $g['cconcepto_parent'] = $tgasl->cconcepto_parent;
            $gastosLab[count($gastosLab)]=$g;
        }
        /* Fin de Obtener lista de Gastos Grabados Laboratorio*/   
        $gastosLab = $objGas->orderParent($gastosLab);
        Session::put('listGastosLabPry',$gastosLab);  

        return view('proyecto/especificacionProyectos',compact('proyecto','persona','editar','uminera','estadoproyecto','servicio','personal','monedas','medioentrega','revision','gastosLab','gastos','estructura','estructuraLab','listaAct','listEstr','listEstr_apo','listEstr_dis','listDis')); 
    }

    public function viewDetalleActividad(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();

        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$proyecto->cproyecto)
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] ='';
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);
        $objPro = new ProyectoSupport();
        $listEstr = $objPro->getProfesionalesSenior($proyecto->cproyecto);
        $listDis = $objPro->getDisciplinas($proyecto->cproyecto);
        $listEstr_dis = $objPro->getProfesionalesDisciplina($proyecto->cproyecto);
        $listEstr_apo = $objPro->getProfesionalesApoyo($proyecto->cproyecto);

        return view('partials.tableDetalleActividadPry',compact('listaAct','proyecto','listEstr','listDis','listEstr_dis','listEstr_apo'));
    }   

    public function editarHorasSenior(Request $request){
        $cestructuraproyecto = $request->input('cestructuraproyecto');
        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$request->input('cproyecto'))
        ->first();
        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$request->input('cproyecto'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] ='';
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $est = DB::table('tproyectohonorarios as hor')
            ->where('cestructuraproyecto','=',$cestructuraproyecto)
            ->where('cproyectoactividades','=',$act->cproyectoactividades)
            ->whereNull('cdisciplina')
            ->first();
            if($est){
                $a['hora_'.$act->cproyectoactividades."_".$cestructuraproyecto] = trim($est->horas);
            
            }
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);

        return view('partials.tableInputHorasActividadPry',compact('listaAct','cestructuraproyecto','proyecto'));
    }
    public function grabarHorasSenior(Request $request){
        DB::beginTransaction();
        $cestructuraproyecto = $request->input('cestructuraproyecto_h');
        $horas = $request->input('horas');
        //dd($horas);
        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$request->input('cproyecto_h'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            if(isset($horas[$act->cproyectoactividades])){
                if(!empty($horas[$act->cproyectoactividades] && $horas[$act->cproyectoactividades] > 0) ){
                    $obj= Tproyectohonorario::where('cproyectoactividades','=',$act->cproyectoactividades)
                    ->where('cestructuraproyecto','=',$cestructuraproyecto)
                    ->whereNull('cdisciplina')
                    ->first();
                    if (!$obj){
                        $obj = new Tproyectohonorario();
                        $obj->cestructuraproyecto = $cestructuraproyecto;
                        $obj->cproyectoactividades = $act->cproyectoactividades;
                    }
                    $obj->horas =trim($horas[$act->cproyectoactividades]);
                    $obj->save();
                }
            }
        }        
        DB::commit();
        return "";
    }

    public function listarInformacionProyecto(){
        
    }

    public function listadoInformacionProyecto(){

        return view('proyecto/listaProyecto');
    }

    public function viewInformacionProyecto(Request $request,$idProyecto){

        

        
        $proyecto = DB::table('tproyecto as pro')
        ->leftjoin('tpersona as tpe','pro.cpersonacliente','=','tpe.cpersona')
        ->leftjoin('tpersona as tpg','pro.cpersona_gerente','=','tpg.cpersona')
        ->leftjoin('tpersona as tgp','pro.cpersona_coordinador','=','tgp.cpersona')
        ->leftJoin('tservicio as tse','pro.cservicio','=','tse.cservicio')
        ->leftJoin('tsedes as tsd','pro.csede','=','tsd.csede')
        ->leftJoin('tunidadminera as tun','pro.cunidadminera','=','tun.cunidadminera')
        ->leftJoin('tmonedas as tm','pro.cmoneda','=','tm.cmoneda')
        ->leftJoin('testadoproyecto as tep','pro.cestadoproyecto','=','tep.cestadoproyecto')
        ->where('pro.cproyecto','=',$idProyecto)
        ->select('tpe.nombre as cliente','pro.codigo','tun.nombre','pro.nombre','tpg.nombre as gerente','pro.cproyecto','pro.descripcion','tgp.nombre as coordinador','tm.descripcion as moneda','pro.cmedioentrega','tsd.descripcion as sede','tse.descripcion as servicio','tep.descripcion as estado','pro.fproyecto','pro.finicio','pro.finicioreal','pro.fcierrereal','pro.fcierre','pro.fcierreadministrativo')
        ->orderBy('pro.cproyecto','ASC')
        ->first();
    

        return view('partials.panelInformacionProyecto',compact('proyecto'));
    }

    public function viewTransmital(Request $request,$idTransmital){

        $transmital = DB::table('ttransmittalejecucion as tte')
        ->leftjoin('tpersona as tps','tte.cpersona_cdoc','=','tps.cpersona')
        ->leftjoin('ttransmittalconfiguracion as ttc','tte.ctransconfiguracion','=','ttc.ctransconfiguracion')
        ->leftjoin('ttransmittalejecuciondetalle as ted','tte.ctransmittalejecucion','=','ted.ctransmittalejecucion')
        ->leftJoin('tproyectodocumentos as pyd','ted.revision','=','pyd.revisionactual')
        ->where('tte.ctransmittalejecucion','=',$idTransmital)
        ->where('tte.ctipo','=','1')
        ->select('tps.nombre as persona','tte.fecha','tte.ctipo','tte.tipoenvio','tte.nrotrasmittal','tte.ctransmittal_cliente','ttc.ctransconfiguracion','tte.ctransmittalejecucion','ted.ctransmittalejedetalle','ted.item','ted.codcliente','ted.codanddes','ted.descripcion','pyd.revisionactual','ted.cantidad')
        ->orderBy('tte.ctransmittalejecucion','ASC')
        ->first();

        return view('partials.panelTransmital',compact('transmital'));
    }


    public function viewListaEntregable(Request $request,$idEntregable){

        $listaentregable = DB::table('tproyecto as tpr')
        ->leftjoin('tproyectoentregables as pre','tpr.cproyecto','=','pre.cproyecto')
        ->leftjoin('tpersona as pes','tpr.cpersonacliente','=','pes.cpersona')
        ->leftjoin('tpersona as psn','tpr.cpersona_gerente','=','psn.cpersona')
        ->leftjoin('tpersona as tnp','tpr.cpersona_coordinador','=','tnp.cpersona')
        ->leftJoin('tservicio as svc','tpr.cservicio','=','svc.cservicio')
        ->leftJoin('tsedes as sed','tpr.csede','=','sed.csede')
        ->leftJoin('tunidadminera as umi','tpr.cunidadminera','=','umi.cunidadminera')
        ->leftJoin('tmonedas as tmo','tpr.cmoneda','=','tmo.cmoneda')
        ->leftJoin('testadoproyecto as epy','tpr.cestadoproyecto','=','epy.cestadoproyecto')
        ->leftJoin('tentregables as ten','pre.centregable','=','ten.centregables')
        ->where('tpr.cproyecto','=',$idEntregable)
        ->select('pes.nombre as cliente','tpr.codigo','umi.nombre','tpr.nombre','psn.nombre as gerente','tpr.cproyecto','tpr.descripcion','tnp.nombre as coordinador','tmo.descripcion as moneda','tpr.cmedioentrega','sed.descripcion as Sede','svc.descripcion as servicio','epy.descripcion as Estado','tpr.fproyecto','tpr.finicio','tpr.finicioreal','tpr.fcierrereal','tpr.fcierre','tpr.fcierreadministrativo','ten.descripcion as entregable','pre.cproyectoentregables')
        ->orderBy('tpr.cproyecto','ASC')
        ->first();


        return view('partials.panelListaEntregable',compact('listaentregable'));
    }

    public function viewEDT(Request $request,$idEdt){

        $edt = DB::table('tproyectoedt as ped')
        ->leftjoin('tproyecto as pyt','ped.cproyecto','=','pyt.cproyecto')
        ->where('ped.cproyecto','=',$idEdt)
        ->where('ped.tipo','=','0')
        ->select('ped.cproyectoedt','pyt.cproyecto','ped.codigo','ped.descripcion','ped.cproyectoedt_parent','ped.tipo')
        ->orderBy('ped.cproyectoedt','ASC')
        ->first();


        return view('partials.panelEDT',compact('edt'));
    }



    public function viewHojaResumen(Request $request,$idHRes){

        $hojaRes = DB::table('tproyecto as tpo')
        ->leftjoin('tpersona as pso','tpo.cpersonacliente','=','pso.cpersona')
        ->leftJoin('tunidadminera as unr','tpo.cunidadminera','=','unr.cunidadminera')
        ->leftJoin('tproyectodocumentos as tpd','tpo.cproyecto','=','tpd.cproyecto')
        ->leftJoin('testadoproyecto as esp','tpo.cestadoproyecto','=','esp.cestadoproyecto')
        ->where('tpo.cproyecto','=',$idHRes)
        ->select('pso.nombre as cliente','tpo.nombre','unr.nombre','tpd.codigodocumento','tpo.codigo','esp.descripcion','tpo.finicio','tpo.fcierre','tpd.revisionactual','tpd.fechadoc')
        ->orderBy('tpo.cproyecto','ASC')
        ->first();


        return view('partials.panelHojaResumen',compact('hojaRes'));
    }

/*
    public function viewEDT(Request $request,$idEdt){



        return view('partials.panelEDT',compact('edt'));
    }*/

    public function editarHorasDisciplina(Request $request){
        $cestructuraproyecto = $request->input('cestructuraproyecto');
        $cdisciplina = $request->input('cdisciplina');
        $propuesta = DB::table('tproyecto')
        ->where('cproyecto','=',$request->input('cproyecto'))
        ->first();
        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$request->input('cproyecto'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] ='';
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $est = DB::table('tproyectohonorarios as hor')
            ->where('cestructuraproyecto','=',$cestructuraproyecto)
            ->where('cproyectoactividades','=',$act->cproyectoactividades)
            ->where('cdisciplina','=',$cdisciplina)
            ->first();
            if($est){
                $a['hora_'.$act->cproyectoactividades."_".$cestructuraproyecto."_".$cdisciplina] = trim($est->horas);
            
            }
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);

        return view('partials.tableInputHorasActividadDisPry',compact('listaAct','cestructuraproyecto','proyecto','cdisciplina'));
    }
    public function grabarHorasDisciplina(Request $request){
        DB::beginTransaction();
        $cestructuraproyecto = $request->input('cestructuraproyecto_h');
        $cdisciplina = $request->input('cdisciplina');
        $horas = $request->input('horas');
        //dd($horas);
        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$request->input('cproyecto_hdis'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            if(isset($horas[$act->cproyectoactividades])){
                if(!empty($horas[$act->cproyectoactividades] && $horas[$act->cproyectoactividades] > 0) ){
                    $obj= Tproyectohonorario::where('cproyectoactividades','=',$act->cproyectoactividades)
                    ->where('cestructuraproyecto','=',$cestructuraproyecto)
                    ->where('cdisciplina','=',$cdisciplina)
                    ->first();
                    if (!$obj){
                        $obj = new Tproyectohonorario();
                        $obj->cestructuraproyecto = $cestructuraproyecto;
                        $obj->cproyectoactividades = $act->cproyectoactividades;
                        $obj->cdisciplina = $cdisciplina;
                    }
                    $obj->horas =trim($horas[$act->cproyectoactividades]);
                    $obj->save();
                }
            }
        }        
        DB::commit();
        return "";
    }

    public function editarHorasApoyo(Request $request){
        $cestructuraproyecto = $request->input('cestructuraproyecto');
        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$request->input('cproyecto'))
        ->first();
        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$request->input('cproyecto'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] ='';
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $est = DB::table('tproyectohonorarios as hor')
            ->where('cestructuraproyecto','=',$cestructuraproyecto)
            ->where('cproyectoactividades','=',$act->cproyectoactividades)
            ->whereNull('cdisciplina')
            ->first();
            if($est){
                $a['hora_'.$act->cproyectoactividades."_".$cestructuraproyecto] = trim($est->horas);
            
            }
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);

        return view('partials.tableInputHorasActividadPry',compact('listaAct','cestructuraproyecto','proyecto'));
    }
    public function grabarHorasApoyo(Request $request){
        DB::beginTransaction();
        $cestructuraproyecto = $request->input('cestructuraproyecto_h');
        $horas = $request->input('horas');
        //dd($horas);
        $lactividades = DB::table('tproyectoactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$request->input('cproyecto_hapo'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            if(isset($horas[$act->cproyectoactividades])){
                if(!empty($horas[$act->cproyectoactividades] && $horas[$act->cproyectoactividades] > 0) ){
                    $obj= Tproyectohonorario::where('cproyectoactividades','=',$act->cproyectoactividades)
                    ->where('cestructuraproyecto','=',$cestructuraproyecto)
                    ->whereNull('cdisciplina')
                    ->first();
                    if (!$obj){
                        $obj = new Tproyectohonorario();
                        $obj->cestructuraproyecto = $cestructuraproyecto;
                        $obj->cproyectoactividades = $act->cproyectoactividades;
                    }
                    $obj->horas =trim($horas[$act->cproyectoactividades]);
                    $obj->save();
                }
            }
        }        
        DB::commit();
        return "";
    }    
    public function addGastos(Request $request){
        $gastos = array();
        if (Session::get('listGastosPry')){
            $gastos = Session::get('listGastosPry');
        }
        $selected = $request->input('selected_gastos');
        $congastos = DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=','00003')
        ->whereIn('congas.cconcepto',$selected)
        ->get();
        foreach($congastos as $gas){
            $i= -1*(count($gastos)*3);
            $g['cproyectogastos'] = $i;
            $g['cproyecto'] = '';
            $g['item'] ='';
            $g['concepto'] = $gas->cconcepto;
            $g['descripcion'] = $gas->descripcion;
            $g['unidad']=$gas->unidad;
            $g['cantidad'] = '1';
            $g['costou'] =  $gas->costounitario;
            $g['subtotal'] = $g['cantidad'] * $g['costou'];
            $g['cconcepto_parent'] = $gas->cconcepto_parent;
            $g['comentario'] ='';
            $g['cpersona_proveedor'] ='';
            $g['cproyectosub'] ='';
            $gastos[count($gastos)]=$g;
            /* agregar parent */

            if(!(empty($gas->cconcepto_parent))){
                $res = array_search($gas->cconcepto_parent, array_map(function($e){return $e['concepto'];},$gastos));
                if($res===FALSE){
                    $parent = DB::table('tconceptogastos as gas')
                    ->where('gas.cconcepto','=',$gas->cconcepto_parent)
                    ->first();
                    if($parent){
                        $i= -1*(count($gastos)*3);
                        $p['cproyectogastos'] = $i;
                        $p['cproyecto'] = '';
                        $p['item'] ='';
                        $p['concepto'] = $parent->cconcepto;
                        $p['descripcion'] = $parent->descripcion;
                        $p['unidad']=$parent->unidad;
                        $p['cantidad'] = '1';
                        $p['costou'] =  $parent->costounitario;
                        $p['subtotal'] = $p['cantidad'] * $p['costou'];
                        $p['cconcepto_parent'] = $parent->cconcepto_parent;
                        $p['comentario'] ='';
                        $p['cpersona_proveedor'] ='';
                        $p['cproyectosub'] ='';
                        $gastos[count($gastos)]=$p;
                    }
                }
            }
            
            /* fin agregar parent */
            

        }

        $objGas  = new GastosSupport();
        $gastos = $objGas->orderParent($gastos);
        Session::put('listGastosPry',$gastos);

        //dd($congastos);
        //dd($request->input('selected_gastos'));
        return view('partials.tableGastosProyecto',compact('gastos'));

    }
    public function delGastos(Request $request){
        $selected = $request->input('chkselec');
        $gastos = Session::get('listGastosPry');
        $delGastos = Session::get('delGastosPry');
        $tmpGastos = array();
        if (count($selected) >0){
            
            foreach($gastos as $k => $gas){
                $res = array_search($gas['cproyectogastos'],$selected);
                if($res===FALSE){
                    $tmpGastos[count($tmpGastos)] = $gas;
                }
                foreach($selected as $v){
                    if($v > 0){
                        $delGastos[count($delGastos)] = $v;
                    }
                }
            }
        }
        $objGas  = new GastosSupport();
        $gastos = $tmpGastos;
        $gastos = $objGas->orderParent($gastos);
        Session::put('listGastosPry',$gastos);
        Session::put('delGastosPry',$delGastos);
        return view('partials.tableGastosProyecto',compact('gastos'));
    }

    public function addGastosLab(Request $request){
        $gastosLab = array();
        if (Session::get('listGastosLabPry')){
            $gastosLab = Session::get('listGastosLabPry');
        }
        $selected = $request->input('selected_gastos_lab');
        $congastos = DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=','00002')
        ->whereIn('congas.cconcepto',$selected)
        ->get();
        foreach($congastos as $gas){
            $i= -1*(count($gastosLab)*3);
            $g['cproyectogastoslab'] = $i;
            $g['cproyecto'] = '';
            $g['item'] ='';
            $g['concepto'] = $gas->cconcepto;
            $g['descripcion'] = $gas->descripcion;
            $g['cmoneda']=$gas->cmoneda;
            
            $g['preciou'] =  $gas->costounitario;
            $g['cantcimentacion'] = 0;
            $g['cantcanteras'] = 0;
            $g['cantbotadero'] = 0;
            $g['subtotal'] =0;
            $g['cpersona_proveedor'] ='';
            $g['cproyectosub'] ='';
            $gastosLab[count($gastosLab)]=$g;
            

        }

        $objGas  = new GastosSupport();
        $gastosLab = $objGas->orderParent($gastosLab);
        Session::put('listGastosLabPry',$gastosLab);

        //dd($congastos);
        //dd($request->input('selected_gastos'));
        return view('partials.tableGastosLabProyecto',compact('gastosLab'));

    }
    public function delGastosLab(Request $request){
        $selected = $request->input('chkselecLab');
        $gastosLab = Session::get('listGastosLabPry');
        $delGastosLab = Session::get('delGastosLabPry');
        $delGastos = Session::get('delGastos');
        $tmpGastos = array();
        if (count($selected) >0){
            
            foreach($gastosLab as $k => $gas){
                $res = array_search($gas['cproyectogastoslab'],$selected);
                if($res===FALSE){
                    $tmpGastos[count($tmpGastos)] = $gas;
                }
                foreach($selected as $v){
                    if($v > 0){
                        $delGastosLab[count($delGastos)] = $v;
                    }
                }
            }
        }
        $objGas  = new GastosSupport();
        $gastosLab = $tmpGastos;
        $gastosLab = $objGas->orderParent($gastosLab);
        Session::put('listGastosLabPry',$gastosLab);
        Session::put('delGastosLabPry',$delGastosLab);
        return view('partials.tableGastosLabProyecto',compact('gastosLab'));
    }    
    public function grabarEspecificacion(Request $request){
        DB::beginTransaction();
        $idProyecto = $request->input('cproyecto');
        $proyecto = Tproyecto::where('cproyecto','=',$idProyecto)->first();
        

        $proyecto->cestadoproyecto= '001';

        $proyecto->save();
        $cproyecto=$proyecto->cproyecto;

        /* Grabar Gastos */
        $igas = $request->input('gas');
        $icom = $request->input('txt');        
        if(Session::get('listGastosPry')){
            $gastos = Session::get('listGastosPry');
            //dd($gastos);
            foreach($gastos as $gas){
                if($gas['cproyectogastos'] <= 0){
                    $tgasto = new Tproyectogasto();
                    $tgasto->cproyecto = $cproyecto;
                }else{
                    $tgasto = Tproyectogasto::where('cproyectogastos','=',$gas['cproyectogastos'])->first();
                }
                $cantidad=0;
                if(isset($igas[$gas['cproyectogastos']]) ){
                    $cantidad = $igas[$gas['cproyectogastos']];
                }
                $comentario='';
                if(isset($icom[$gas['cproyectogastos']]) ){
                    $comentario = $icom[$gas['cproyectogastos']];
                }                 
                $tgasto->item = $gas['item'];
                $tgasto->concepto = $gas['concepto'];
                $tgasto->unidad = $gas['unidad'];
                $tgasto->cantidad = $cantidad;
                $tgasto->costou = $gas['costou'];
                $tgasto->comentario = $comentario;
                $tgasto->save();
            }
        }
        if(Session::get('delGastosPry')){
            $delGastos = Session::get('delGastosPry');
            foreach($delGastos as $k => $dg){
                //dd($dg);
                $obj = Tpropuestagasto::where('cproyectogastos','=',$dg)->delete();
                

            }
        }
        /* Fin Grabar Gastos */

        /* Grabar Gastos Lab*/
        if(Session::get('listGastosLabPry')){
            $gastos = Session::get('listGastosLabPry');
            $gaslcimen = $request->input('gaslcimen');
            $gaslbota = $request->input('gaslbota');
            $gaslcant = $request->input('gaslcant');            
            //dd($gastos);
            foreach($gastos as $gas){
                if($gas['cproyectogastoslab'] <= 0){
                    $tgasto = new Tproyectogastoslaboratorio();
                    $tgasto->cproyecto = $cproyecto;
                }else{
                    $tgasto = Tproyectogastoslaboratorio::where('cproyectogastoslab','=',$gas['cproyectogastoslab'])->first();
                }
                $tgasto->item = $gas['item'];
                $tgasto->concepto = $gas['concepto'];
                if(!empty($gas['cmoneda'])){
                    $tgasto->cmoneda = $gas['cmoneda'];    
                }
                
                $tgasto->cantbotadero = $gaslcimen[$gas['cproyectogastoslab']];
                $tgasto->cantcimentacion = $gaslbota[$gas['cproyectogastoslab']];
                $tgasto->cantcanteras = $gaslcant[$gas['cproyectogastoslab']];
                $tgasto->preciou = $gas['preciou'];
                $tgasto->save();
            }
        }
        if(Session::get('delGastosLabPry')){
            $delGastosLab = Session::get('delGastosLabPry');
            foreach($delGastosLab as $k => $dg){
                //dd($dg);
                $obj = Tproyectogastoslaboratorio::where('cproyectogastoslab','=',$dg)->delete();
                

            }
        }
        /* Fin Grabar Gastos Lab*/ 
        DB::commit();
        return Redirect::route('editarEspecificacionProyecto',$cproyecto);
    } 
    /*  Fin de Especificacion de Proyecto*/

    /* Registro de HR */
    public function registroHR($valor){       


         $estdproy=null;
         $editar=false;
         $valor=$valor;

         if ($valor == 'lec') {
            $readonly = 'readonly';
            $hidden = 'none';
            $disabled = 'disabled';
         }
         elseif ($valor == 'esc') {
            $readonly = '';
            $hidden = 'block';
            $disabled = '';
             
         }

        
          $arrayDatos=array();
        return view('proyecto/HR_Proyecto',compact('estdproy','editar','arrayDatos','valor','readonly','disabled','hidden'));
    }
    public function editarHR(Request $request,$valor,$idProyecto){

        // dd($valor);
         if ($valor == 'lec') {
            $readonly = 'readonly';
            $hidden = 'none';
            $disabled = 'disabled';
         }
         elseif ($valor == 'esc') {
            $readonly = '';
            $hidden = 'block';
            $disabled = '';
             
         }

         $correo = Tproyectoinformacionadicional::where('cproyecto','=',$idProyecto)->first();

        if (is_null($correo)) {
            $correo_inicio=null;
        }
        else {
            $correo_inicio = $correo->correoinicio;
        }

        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();



        if (is_null($proyecto->finicio)) {
            $feiniPla=null;            
        }

        else{
            $feiniPla=new Carbon($proyecto->finicio);
            $feiniPla=$feiniPla->format('d-m-Y');            
        }


        if (is_null($proyecto->fcierre)) {
            $fcierrePla=null;            
        }

        else{
            $fcierrePla=new Carbon($proyecto->fcierre);
            $fcierrePla=$fcierrePla->format('d-m-Y');          
        }


        if (is_null($proyecto->finicioreal)) {
            $feiniReal=null;            
        }

        else{
            $feiniReal=new Carbon($proyecto->finicioreal);
            $feiniReal=$feiniReal->format('d-m-Y');         
        }

        if (is_null($proyecto->fcierrereal)) {
            $fcierreReal=null;            
        }

        else{
            $fcierreReal=new Carbon($proyecto->fcierrereal);
            $fcierreReal=$fcierreReal->format('d-m-Y');
    
        }



        $tipogestionproyecto = DB::table('ttipogestionproyecto')->lists('descripcion','ctipogestionproyecto');
        $tipogestionproyecto = [''=>''] + $tipogestionproyecto; 

        //dd($proyecto,$tipogestionproyecto);
        //dd($fcierreReal,$proyecto->fcierrereal);
        
        //$proyecto = $proyecto[0];
        $aprobacion = false;
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
       


        $servicio = DB::table('tservicio')
        ->orderBy('orden','asc')
        ->lists('descripcion','cservicio');
        $servicio = ['' => ''] + $servicio;

  
        $tipoproyecto = DB::table('ttipoproyecto')
        ->where('cservicio','=',$proyecto->cservicio)
        ->lists('descripcion','ctipoproyecto');
        $tipoproyecto = [''=>''] + $tipoproyecto;



        //$estadoproyecto = Db::table('testadoproyecto')->lists('descripcion','cestadoproyecto');
        $personal = Tpersona::join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.abreviatura','tpersona.cpersona')->all(); 
        $personal = [''=>''] + $personal;

        $propuesta=DB::table('tpropuesta as per')
        ->where('per.cproyecto','=',$idProyecto)
        ->first();
        if($propuesta){
            
            $prop_infoad = DB::table('tpropuestainformacionadicional as prya')
            ->where('prya.cpropuesta','=',$propuesta->cpropuesta)
            ->first();


            if (is_null($propuesta->fadjucicacion)) {
                $fAdjud=null;            
            }

            else{
                $fAdjud=new Carbon($propuesta->fadjucicacion);
                $fAdjud=$fAdjud->format('d-m-Y');
        
            }
        }



        $tipoproy=Tcatalogo::where('codtab','=','00056')
        ->whereNotNull('digide')
        ->orderBy('digide','asc')
        ->lists('descripcion','digide')->all();
        $tipoproy = [''=>''] + $tipoproy;

        $direccion_cli=  DB::table('tpersonadirecciones as tp')
        ->where('tp.cpersona','=',$proyecto->cpersonacliente)      
        ->get();


        $direcli=null;

        foreach ($direccion_cli as $dc) {
            if($dc->ctipodireccion=='FIS'){
                $direcli=$dc->direccion;
            }
            elseif ($dc->ctipodireccion=='COM') {
               
                $direcli=$dc->direccion;
          
            }
            else{
                $direcli=null;
            }
        }

        $persona_cliente = DB::table('tpersona as tp')
        ->leftjoin('tpersonajuridicainformacionbasica as tpj','tp.cpersona','=','tpj.cpersona')
        ->where('tp.cpersona','=',$proyecto->cpersonacliente)       
        ->select('tp.*','tpj.telefono','tpj.razonsocial')
        ->first();

        //dd($direccion_cli,$direcli,$persona_cliente);
       /* $per_rdp = DB::table('tproyectodisciplina as prydis')
        ->join('tpersona as per','per.cpersona','=','prydis.cpersona_rdp')
        ->join('tdisciplina as dis','prydis.cdisciplina','=','dis.cdisciplina')
        ->where('prydis.cproyecto','=',$proyecto->cproyecto)
        ->select('prydis.*','dis.descripcion','per.nombre')
        ->get();*/

        $objEmp = new EmpleadoSupport();
        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp;


        $estdproy=$proyecto->cestadoproyecto;     


        if(!($proyecto->cestadoproyecto == '001')){
            //$personal_gp=$personal;
                   
        }  
       

        $personal_cp = $objEmp->listPersonaArea('56') ;
        $personal_cp = [''=>''] + $personal_cp;

        //dd($personal_cp);
        $personal_cd = $objEmp->listPersonaArea('57') ;
        $personal_cd = [''=>''] + $personal_cd;



        $per_rdp=DB::table('tproyectopersona as pryper')
        ->join('tpersona as per','per.cpersona','=','pryper.cpersona')
        ->join('tdisciplinaareas as aredis','pryper.cdisciplina','=','aredis.cdisciplina')
        ->join('tareas as tar','aredis.carea','=','tar.carea')
        ->leftjoin('tdisciplina as dis','aredis.cdisciplina','=','dis.cdisciplina')
        ->where('pryper.cproyecto','=',$idProyecto)
        ->where('pryper.eslider','=','1')
        ->select('pryper.cpersona','per.abreviatura','pryper.cdisciplina','pryper.cproyecto','dis.descripcion','tar.*')
        ->get();
        //dd($per_rdp);

        $listRDP = array();

        foreach($per_rdp as $rdp){

            if($rdp->esgeneral=='1' || $rdp->estecnica=='1' || $rdp->essoporte=='1' ){

            }
            else{
                $r['cproyecto'] = $rdp->cproyecto;
                $r['cdisciplina'] = $rdp->cdisciplina;
                $r['cpersona_rdp'] = $rdp->cpersona;
                //$r['escontratista'] = $rdp->escontratista;
                //$r['cpersona_proveedor'] = $rdp->cpersona_proveedor;
                //$r['eslp'] = $rdp->eslp;
                $r['descripcion'] = $rdp->descripcion;
                $r['nombre'] = $rdp->abreviatura;
                $listRDP[count($listRDP)]=$r;

            }         
          
        }

  

        $per_contproy=$objEmp->getPersonaProyectoXDisciplina($idProyecto,'10');

        $per_contdoc=$objEmp->getPersonaProyectoXDisciplina($idProyecto,'11');

        
        $paqpropuesta = DB::table('tproyectosub')->where('cproyecto','=',$proyecto->cproyecto)->lists('descripcionsub','cproyectosub');
        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();   

       

       
        $monedas = DB::table('tmonedas')->where('cmoneda','=',$proyecto->cmoneda)->first();

        $medioentrega = DB::table('tmedioentrega')->get();
        $revision=""; 


        $editar=true;      
        $documento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','HR')
        ->select('prydoc.cproyectodocumentos','prydoc.fechadoc','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first();     


       
        if($documento){
            if (is_null($documento->fechadoc)) {
                $fechadoc=null;            
            }

            else{
                $fechadoc=new Carbon($documento->fechadoc);
                $fechadoc=$fechadoc->format('d-m-Y');
        
            }     
            
        }


        $objProSup = new ProyectoSupport();

        $revisiones = $objProSup->getRevisionesDocumentos($proyecto->cproyecto,'HR','00005');

        $revisionesAnteriores = DB::table('tproyectodocumentos as pro')      
        ->where('pro.cdocumentoparapry','=','4')
        ->where('pro.cproyecto','=',$proyecto->cproyecto)
        ->whereNotNull('pro.revisionanterior')
        ->orderBy('pro.fechadoc','ASC')
        //->select('revisionanterior')
        ->get();
       // dd($revisionesAnteriores);
  


        $tcargos = Tcontactocargo::orderBy('descripcion','ASC')
        ->lists('descripcion','ccontactocargo');

        $tdocu = Tcatalogo::where('codtab','=','00012')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();
        $docu = [''=>''] + $tdocu;

        $clientefin=$persona_cliente->cpersona;
        $uminerac=null;
        $proy_adicional = DB::table('tproyectoinformacionadicional as prya')
        ->where('prya.cproyecto','=',$proyecto->cproyecto)
        ->first();
        if($proy_adicional){
            $uminerac = DB::table('tunidadmineracontactos as umc')
            ->where('umc.cunidadmineracontacto','=',$proy_adicional->cunidadmineracontacto)
            ->first();
            $totalHon=$proy_adicional->hon_and+$proy_adicional->hon_con1+$proy_adicional->hon_con2;
            $totalHon=number_format($totalHon,2,'.','');
            $totalGast=$proy_adicional->gast_and+$proy_adicional->gast_con1+$proy_adicional->gast_con2;
            $totalGast=number_format($totalGast,2,'.','');
            $totalLab=$proy_adicional->lab_and+$proy_adicional->lab_con1+$proy_adicional->lab_con2;
            $totalLab=number_format($totalLab,2,'.','');
            $totalDesc=$proy_adicional->desc_and+$proy_adicional->desc_con1+$proy_adicional->desc_con2;
            $totalDesc=number_format($totalDesc,2,'.','');
            $totalOtr=$proy_adicional->otr_and+$proy_adicional->otr_con1+$proy_adicional->otr_con2;
            $totalOtr=number_format($totalOtr,2,'.','');


            $totalAnddes=$proy_adicional->hon_and+$proy_adicional->gast_and+$proy_adicional->lab_and-$proy_adicional->desc_and+$proy_adicional->otr_and;
            $totalAnddes=number_format($totalAnddes,2,'.','');
            $totalSubCont1=$proy_adicional->hon_con1+$proy_adicional->gast_con1+$proy_adicional->lab_con1-$proy_adicional->desc_con1+$proy_adicional->otr_con1;
            $totalSubCont1=number_format($totalSubCont1,2,'.','');
            $totalSubCont2=$proy_adicional->hon_con2+$proy_adicional->gast_con2+$proy_adicional->lab_con2-$proy_adicional->desc_con2+$proy_adicional->otr_con2;
            $totalSubCont2=number_format($totalSubCont2,2,'.','');
            $totalPresupuesto=floatval($totalAnddes)+floatval($totalSubCont1)+floatval($totalSubCont2);
            $totalPresupuesto=number_format($totalPresupuesto,2,'.','');


             
            if(strlen($proy_adicional->cliente_final)>0){
                $clientefin=$proy_adicional->cliente_final;
            }


            /*if (is_null($proy_adicional->fechacorte)) {
            $fechacorte=null;            
            }

            else{
                $fechacorte=new Carbon($proy_adicional->fechacorte);
                $fechacorte=$fechacorte->format('d-m-Y');
        
            }*/
        }


        $tforma = Tformacotizacion::lists('descripcion','cformacotizacion')->all();

        $tcargo= DB::table('tcontactocargo')
        ->lists('descripcion','ccontactocargo');
        $tcargo = [''=>''] + $tcargo;

        $ttipo= DB::table('ttiposcontacto')
        ->lists('descripcion','ctipocontacto');
        $ttipo = [''=>''] + $ttipo;

        $tcliente_final= DB::table('tpersonajuridicainformacionbasica as jur') 
        ->where('jur.escliente','=','1')
        ->orderBy('jur.razonsocial','ASC')        
        ->lists('jur.razonsocial','jur.cpersona');
        $tcliente_final = [''=>''] + $tcliente_final;

        $lugartrabajo= DB::table('tlugartrabajo')        
        ->lists('descripcion','clugartrabajo');
        $lugartrabajo = [''=>''] + $lugartrabajo;



        $arrayDatos=array();
        $ad['proyecto']=$proyecto;
        $ad['cliente']=$persona;
        $arrayDatos=$ad;

        //dd($arrayDatos);    
       
        



        return view('proyecto/HR_Proyecto',compact('proyecto','persona','editar','uminera','servicio','personal','monedas','medioentrega','documento','estadoproyecto','persona_cliente','listRDP','revisiones','tcargos','tdocu','proy_adicional','uminerac','tforma','tipoproy','propuesta','per_contproy','per_contdoc','personal_gp','personal_cp','personal_cd','estdproy','tcargo','ttipo','tipoproyecto','totalHon','totalGast','totalLab','totalDesc','totalOtr','prop_infoad','feiniPla','fcierrePla','feiniReal','fcierreReal','fAdjud','tipogestionproyecto','direcli','totalAnddes','totalSubCont1','totalSubCont2','totalPresupuesto','arrayDatos','fechadoc','revisionesAnteriores','tcliente_final','clientefin','lugartrabajo','valor','readonly','hidden','disabled','correo_inicio')); 
    }
    public function grabarHR(Request $request){

        // dd($request->all());
       
        DB::beginTransaction();
        $idProyecto = $request->input('cproyecto');
        $valor = $request->input('valor');

         //si SI existe revision

        //dd(is_null($request->input('revisiondocumento')),strlen($request->input('revisiondocumento')));
       
        if (strlen($request->input('revisiondocumento'))>0) {

            // se ingresa una nueva revision por el sistema    
                 
            if (strlen($request->input('nuevarevision'))>0) {               
     
                $objTdoc= new Tproyectodocumento();
                $objTdoc->cproyecto=$idProyecto;
                $objTdoc->codigodocumento=$request->input('codigoproyecto').'-AND-25-HR-001';
                $objTdoc->revisionactual=$request->input('nuevarevision');
                $objTdoc->revisionanterior=$request->input('revisiondocumento');
                $objTdoc->fechadoc=Carbon::now(); 
                $objTdoc->cdocumentoparapry='4';
                $objTdoc->save();
           
            }
            //graba sobre la revision actual y actualiza la fecha

            else{

                $objTdoc = Tproyectodocumento::where('cproyecto','=',$idProyecto)->where('cdocumentoparapry','=','4')->where('revisionactual','=',$request->input('revisiondocumento'))->first();
                $objTdoc->fechadoc=Carbon::now(); 
                $objTdoc->save();

            }            
           

        }



        $proyecto = Tproyecto::where('cproyecto','=',$idProyecto)->first();

        $proyecto->nombre=$request->input('nombreproyecto');
        $proyecto->codigo=$request->input('codigoproyecto');
        $proyecto->descripcion=$request->input('descripcionproyecto');
        $proyecto->tipoproy=$request->input('tipoproy');
        $proyecto->cestadoproyecto= $request->input('estadoproyecto');

        if(!empty($request->input('cper_gerente'))){
            $proyecto->cpersona_gerente = $request->input('cper_gerente');
        }
        

        if(!empty($request->input('finicio'))){
            $proyecto->finicio = $request->input('finicio');
        }

        if(!empty($request->input('fcierre'))){
            $proyecto->fcierre = $request->input('fcierre');
        }

        if(!empty($request->input('finicioreal'))){
            $proyecto->finicioreal = $request->input('finicioreal');
        }

        else{
            $proyecto->finicioreal=null;
        }

        if(!empty($request->input('fcierrereal'))){
            $proyecto->fcierrereal = $request->input('fcierrereal');
        }

        else{
            $proyecto->fcierrereal=null;
        }

        if(!empty($request->input('tipgestionproy'))){
            $proyecto->ctipogestionproyecto= $request->input('tipgestionproy');
        }

        if(!empty($request->input('tipoproyecto'))){
            $proyecto->ctipoproyecto= $request->input('tipoproyecto');
        }

        if(!empty($request->input('servicio'))){
            $proyecto->cservicio= $request->input('servicio');
        }

     
        
        //si NO existe revision
       // dd($request->input('revisiondocumento'),strlen($request->input('revisiondocumento')));
        if (strlen($request->input('revisiondocumento'))<=0) {

            $objTdoc= new Tproyectodocumento();
            $objTdoc->cproyecto=$idProyecto;
            $objTdoc->codigodocumento=$request->input('codigoproyecto').'-AND-25-HR-001';
            $objTdoc->revisionactual='0';   
            $objTdoc->fechadoc=Carbon::now(); 
            $objTdoc->cdocumentoparapry='4';
            $objTdoc->save();

        }

        $proyecto->save();


        $cproyecto=$proyecto->cproyecto;
 

        $tproy_adicional = Tproyectoinformacionadicional::where('cproyecto','=',$idProyecto)
        ->first();
        if(!$tproy_adicional){
            $tproy_adicional = new Tproyectoinformacionadicional();
            $tproy_adicional->cproyecto = $idProyecto;
        }

        if(!empty($request->input('cunidadmineracontacto'))){
            $tproy_adicional->cunidadmineracontacto = $request->input('cunidadmineracontacto');
        }
        if(!empty($request->input('docaproba'))){
            $tproy_adicional->cdocaprobacion = $request->input('docaproba');
        }
        if(!empty($request->input('nrodocaproba'))){
            $tproy_adicional->nrodocaprobacion = $request->input('nrodocaproba');
        }
        if(!empty($request->input('comentarios'))){
            $tproy_adicional->comentarioshr = $request->input('comentarios');
        }        
        if(!empty($request->input('observaciones'))){
            $tproy_adicional->observacionhr = $request->input('observaciones');
        }        
        if(!empty($request->input('adelanto'))){
            $tproy_adicional->porcentajeadelanto = $request->input('adelanto');
        }        
        if(!empty($request->input('cformacotizacion'))){
            $tproy_adicional->cformacotizacion = $request->input('cformacotizacion');
        }     
        if(!empty($request->input('plazopagodias'))){
            $tproy_adicional->plazopagodias = $request->input('plazopagodias');
        }     
        if(!empty($request->input('diascorte'))){
            $tproy_adicional->diascorte = $request->input('diascorte');
        }     
        /*if(!empty($request->input('cp'))){
            $tproy_adicional->cpersona_cproyecto = $request->input('cp');
        }          
        if(!empty($request->input('cd'))){
            $tproy_adicional->cpersona_cdocumentario = $request->input('cd');
        }   */ 

        if(!empty($request->input('descweb'))){
            $tproy_adicional->descripcionweb = $request->input('descweb');
        } 


        /********************************************/
        if(!empty($request->input('hon_and'))){
            $tproy_adicional->hon_and = $request->input('hon_and');
        }
        if(!empty($request->input('hon_con1'))){
            $tproy_adicional->hon_con1 = $request->input('hon_con1');
        }
        if(!empty($request->input('hon_con2'))){
            $tproy_adicional->hon_con2 = $request->input('hon_con2');
        } 

        if(!empty($request->input('gast_and'))){
            $tproy_adicional->gast_and = $request->input('gast_and');
        }
        if(!empty($request->input('gast_con1'))){
            $tproy_adicional->gast_con1 = $request->input('gast_con1');
        }
        if(!empty($request->input('gast_con2'))){
            $tproy_adicional->gast_con2 = $request->input('gast_con2');
        } 

        if(!empty($request->input('lab_and'))){
            $tproy_adicional->lab_and = $request->input('lab_and');
        }
        if(!empty($request->input('lab_con1'))){
            $tproy_adicional->lab_con1 = $request->input('lab_con1');
        }
        if(!empty($request->input('lab_con2'))){
            $tproy_adicional->lab_con2 = $request->input('lab_con2');
        } 

        if(!empty($request->input('otr_and'))){
            $tproy_adicional->otr_and = $request->input('otr_and');
        }
        if(!empty($request->input('otr_con1'))){
            $tproy_adicional->otr_con1 = $request->input('otr_con1');
        }
        if(!empty($request->input('otr_con2'))){
            $tproy_adicional->otr_con2 = $request->input('otr_con2');
        } 

        if(!empty($request->input('desc_and'))){
            $tproy_adicional->desc_and = $request->input('desc_and');
        }
        if(!empty($request->input('desc_con1'))){
            $tproy_adicional->desc_con1 = $request->input('desc_con1');
        }
        if(!empty($request->input('desc_con2'))){
            $tproy_adicional->desc_con2 = $request->input('desc_con2');
        } 

        if(!empty($request->input('lider1'))){
            $tproy_adicional->lider1 = $request->input('lider1');
        } 

        if(!empty($request->input('lider2'))){
            $tproy_adicional->lider2 = $request->input('lider2');
        } 

        if(!empty($request->input('fotoproyhr'))){
            $tproy_adicional->fotoproyhr = $request->input('fotoproyhr');
        } 


        if(!empty($request->input('clientefinal'))){
            $tproy_adicional->cliente_final= $request->input('clientefinal');
        }

        if(!empty($request->input('lugartrabajo'))){
            $tproy_adicional->clugartrabajo= $request->input('lugartrabajo');
        }

        /********************************************/
                          
        $tproy_adicional->save();

        $tpropuesta=Tpropuestum::where('cproyecto','=',$idProyecto)
        ->first();

        if($tpropuesta){

            if(!empty($request->input('fadjudicacion'))){
                $tpropuesta->fadjucicacion=$request->input('fadjudicacion');
                $tpropuesta->save();
            } 
            
        }

      

        DB::commit();
        return Redirect::route('editarHR',[$valor,$cproyecto]);
    } 
    /* Fin de Registro de HR  */

    // inicio de envio correo de inicio
    
    public function enviarCorreoIni(Request $request){

        // dd($cproyecto);
        $cproyecto = $request->input('cproyecto');
        $valor = $request->input('valor');

        $correo = Tproyectoinformacionadicional::where('cproyecto','=',$cproyecto)->first();
        $correo->correoinicio = true;
        $correo->save();

        $gp = Tpersona::where('cpersona','=',$request->input('cper_gerente'))->first();
        $gerente = $gp->abreviatura;


        $titulo = $request->input('codigoproyecto')." - ".$request->input('nombreproyecto');
        $para_email = 'bruno.rosas@anddes.com';//cambiar correo
        $data = ["de_cliente"=> $request->input('personanombre'),"de_minera"=>$request->input('umineranombre'),"nombreproyecto"=>$request->input('nombreproyecto'),"codigoproyecto"=>$request->input('codigoproyecto'),"codigopropuesta"=>$request->input('codigopropuesta'),"gerente"=>$gerente];
        $result = EmailSend::correoinicio($data,$para_email,$titulo);

        return Redirect::route('editarHR',[$valor,$cproyecto]);
    }

    // fin de envio correo de inicio



    /* Cronograma de Proyecto*/
    public function cronogramaProyecto(){
        $editar=true; 
        return view('proyecto/cronogramaProyecto',compact('editar'));
    }
    public function editarCronogramaProyecto(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->get();
        $proyecto = $proyecto[0];
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = ['' => ''] + $servicio;
        
        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); 
        
        $paqpropuesta = DB::table('tproyectosub')->where('cproyecto','=',$proyecto->cproyecto)->lists('descripcionsub','cproyectosub');
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $revision=""; 
        $editar=true;        
        $listCro = array();
        $prycrono = DB::table('tproyectocronograma')->where('cproyecto','=',$idProyecto)->first(); 
        if($prycrono){
            $cronoDeta = DB::table('tproyectocronogramadetalle')->where('cproyectocronograma','=',$prycrono->cproyectocronograma)
            ->orderBy('item','asc')
            ->get();
            foreach($cronoDeta as $cdeta){
                $cro['cproyectocronogramadetalle'] = $cdeta->cproyectocronogramadetalle;
                $cro['cproyectocronograma'] = $cdeta->cproyectocronograma;
                $cro['item'] = $cdeta->item;
                $cro['cactividad'] = $cdeta->cactividad;
                $cro['descripcion'] = $cdeta->descripcion;
                $cro['idactproject'] = $cdeta->idactproject;
                $cro['duracion'] = $cdeta->duracion;
                $cro['idpredecesora'] = $cdeta->idpredecesora;
                $cro['idsucesora'] = $cdeta->idsucesora;
                $cro['idrecursoproject'] = $cdeta->idrecursoproject;
                $cro['monto'] =$cdeta->monto;
                $cro['cproyectoactividades'] = $cdeta->cproyectoactividades;
                $cro['cproyectoentregable'] = $cdeta->cproyectoentregable;
                $cro['cproyectosub'] = $cdeta->cproyectosub;
                $cro['finicio'] = $cdeta->finicio;
                $cro['ffin'] = $cdeta->ffin;
                $listCro[count($listCro)] = $cro;

            }
        }
        $documento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','CRO')
        ->select('prydoc.cproyectodocumentos','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first();

        $tactividades = DB::table('tproyectoactividades as pryact')
        ->join('tactividad as act','pryact.cactividad','=','act.cactividad')
        ->where('pryact.cproyecto','=',$idProyecto)
        ->select('pryact.cproyectoactividades','pryact.cactividad','pryact.descripcionactividad')
        ->get();
        $actividades = array(''=>'');
        $actividades_s = array();
        foreach($tactividades as $tact){
            $act['descripcion'] = $tact->descripcionactividad;
            $act['cactividad'] = $tact->cproyectoactividades;
            $actividades_s[count($actividades_s)] = $act;
            $actividades[$tact->cproyectoactividades] = $tact->descripcionactividad;
        }
        //$actividades = Collection::make($actividades);

        //dd($actividades);
        $tentregables = DB::table('tproyectoentregables as pryent')
        ->join('tentregables as ent','pryent.centregable','=','ent.centregables')
        ->where('pryent.cproyecto','=',$idProyecto)
        ->select('pryent.cproyectoentregables','pryent.centregable','ent.descripcion')
        ->orderBy('ent.descripcion','ASC')
        ->get();

        $entregables = array(''=>'');
        $entregables_s = array();
        foreach($tentregables as $tent){
            $ent['descripcion'] = $tent->descripcion;
            $ent['cproyectoentregables'] = $tent->cproyectoentregables;
            $entregables_s[count($entregables)] = $ent;
            $entregables[$tent->cproyectoentregables] = $tent->descripcion;
        }


        //Actividad checklist
        $tentregables = DB::table('tproyectoentregables as pryent')
        ->join('tentregables as ent','pryent.centregable','=','ent.centregables')
        ->where('pryent.cproyecto','=',$idProyecto)
        ->select('pryent.cproyectoentregables','ent.descripcion')
        ->orderBy('ent.descripcion','ASC')
        ->get();

        $entregables = array(''=>'');
        $entregables_s = array();
        foreach($tentregables as $tent){
            $ent['descripcion'] = $tent->descripcion;
            $ent['cproyectoentregables'] = $tent->cproyectoentregables;
            $entregables_s[count($entregables)] = $ent;
            $entregables[$tent->cproyectoentregables] = $tent->descripcion;
        }

        return view('proyecto/cronogramaProyecto',compact('proyecto','persona','editar','uminera','servicio','personal','monedas','medioentrega','revision','estadoproyecto','listCro','editar','actividades','entregables','documento','actividades_s','entregables_s')); 
    }
    public function grabarCronogramaProyecto(Request $request){
        DB::beginTransaction();
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$request->input('cproyecto'))->get();
        $proyecto = $proyecto[0];

        /*$proyecto->cestadoproyecto= '001';

        $proyecto->save();*/
        $cproyecto=$proyecto->cproyecto;

        $crono = Tproyectocronograma::where('cproyecto','=',$cproyecto)->first();
        $idcrono=0;
        if(count($crono) > 0){
            $idcrono = $crono->cproyectocronograma;
        }else{
            $crono = new Tproyectocronograma();
            $crono->cproyecto = $cproyecto;
            $crono->fregistro = Carbon::now();
            $crono->save();
            $idcrono = $crono->cproyectocronograma;
        }
        $cronograma = Input::get('cro');
        $item = 0;
        foreach($cronograma as $c){
            if ($c[0] < 0){
                $crod = new Tproyectocronogramadetalle();
                $crod->cproyectocronograma = $idcrono;
                $item++;
                $crod->item = $item;
                
            }else{
                $crod = Tproyectocronogramadetalle::where('cproyectocronogramadetalle','=',$c[0])->first();
                $item = $crod->item;
            }
            $tact = DB::table('tproyectoactividades')->where('cproyectoactividades','=',$c[2])->first();
            $crod->cactividad = $tact->cactividad;
            $crod->descripcion = $tact->descripcionactividad;
            if(!empty($c[3])){
                $crod->duracion = $c[3];
            }else{
                if($c[0] >0){
                    $crod->duracion = null;
                }
            }
            $crod->finicio = $c[4];
            $crod->ffin = $c[5];
            $crod->idpredecesora = $c[6];
            $crod->idsucesora = $c[7];
            $crod->idrecursoproject = $c[8];
            if(!empty($c[9])){
                $crod->monto = $c[9];
            }else{
                if($c[0]>0){
                    $crod->monto = null;
                }
            }
            $crod->cproyectoactividades = $c[2];
            if(!empty($c[10])){
                $crod->cproyectoentregable = $c[10];
            }else{
                if(empty($c[10]) || strlen($c[10])<=0 ){
                  $crod->cproyectoentregable=null;  
                }
            }
            $crod->save();
        }

        DB::commit();

        return Redirect::route('editarCronogramaProyecto',$cproyecto);
    }     

    public function UploadXml(Request $request){
        $cpropuesta= $request->input('cproyecto_file');
        $file = $request->file('fileXml');
        $extension = $file->getClientOriginalExtension();
        $id = Storage::disk('archivos')->put($file->getFilename().'.'.$extension,  File::get($file));
        $ruta = storage_path('archivos') . "/" . $file->getFilename().'.'.$extension;
        $xml = simplexml_load_file($ruta);
     
        dd($xml);        
        /*$objPHPExcel = PHPExcel_IOFactory::load($ruta);
        $sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
        $loadedSheetNames = $objPHPExcel->getSheetNames();
        dd($loadedSheetNames);*/
        /*if ($id){
            $dt = array();
            $data = Excel::load($ruta, function($reader)use($dt) {
                $reader->first();
                foreach($reader as $sheet) // loop through sheets
                {
                    $sheetTitle = $sheet;
                    $dt[count($dt)]=$sheetTitle;
                    dd($dt);
                }
            });
            
        }*/
    
         //return Redirect::route('editarDetallePropuesta',$cpropuesta);
    }    
    /*  Fin de Cronograma Proyecto */

    /* Entregables de Proyecto*/
    public function entregablesProyecto(){
        Session::forget('listEnt'); 
        $editar=false;  
        return view('proyecto/listaEntregables',compact('editar'));
    }
    public function editarEntregablesProyecto(Request $request,$idProyecto){

        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->get();
        $proyecto = $proyecto[0];

        Session::forget('listEnt');     
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = DB::table('tservicio')->lists('descripcion','cservicio');
        
        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); 
        
        $paqpropuesta = DB::table('tproyectosub')->where('cproyecto','=',$proyecto->cproyecto)->lists('descripcionsub','cproyectosub');
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $revision=""; 
        $editar=true;    

        $documento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','LE')
        ->select('prydoc.cproyectodocumentos','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first();
//////////////////////////////////////

///////////////////

        $fechasCab=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->where('tpe.cproyecto','=',$proyecto->cproyecto)
        ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec')
        ->orderBy('ttf.ctipofechasentregable','asc')
        ->distinct()
        ->get();

        $listEnt=$this->listarTodosEntregables($proyecto->cproyecto,'todos');


      
        Session::put('listEnt',$listEnt);

        
        $entregable = DB::table('tentregables as ent')    
        ->orderBy('ent.descripcion','ASC')
        ->lists('ent.descripcion','ent.centregables');

        $pryedt = DB::table('tproyectoedt as edt')
        ->where('edt.cproyecto','=',$proyecto->cproyecto)
        ->lists('descripcion','cproyectoedt');

        $catdoc = Tcategoriaentregable::lists('descripcion','ccategoriaentregable')
        ->all();        
        //$estructura = $estructura;
        //dd($estructura);
        $user=Auth::user();

        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->leftjoin('tareas as tap','ta.carea_parent','=','tap.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->select('e.cpersona','e.carea', 'e.ccargo','ta.carea','ta.descripcion','tap.carea as cpadre','tap.codigo as codigopadre')
        ->first();
        $carea=0;
        $carea_parent=0;
       // $codigoparent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->cpadre;
          //  $codigoparent=$tarea->codigopadre;
        }
        $personagte=DB::table('tareas as ta')
        ->where('ta.carea','=',$carea_parent)
        ->where('ta.codigo','ilike','%'.'GT'.'%')
        ->first();

        $codigoGT=0;

        if($personagte){
        $codigoGT=$personagte->codigo;
        }

        /*$responsable = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
      
        ->where('tnat.esempleado','=','1')
        ->where('emp.estado','=','ACT')
        ->where('emp.carea','=',$carea)
        ->orderBy('per.abreviatura','ASC');

           if(substr($codigoGT,0,2)=='GT'){
            $responsable=$responsable->orWhere('emp.carea','=',$carea_parent);
           }
                 
        $responsable=$responsable->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');
        $responsable = [''=>''] + $responsable;*/





        $colaboradores=DB::table('tproyectopersona as pryper')
        ->join('tpersona as per','per.cpersona','=','pryper.cpersona')
        ->join('tdisciplinaareas as aredis','pryper.cdisciplina','=','aredis.cdisciplina')
        ->join('tareas as tar','aredis.carea','=','tar.carea')
        ->join('tpersonadatosempleado as tpe','pryper.cpersona','=','tpe.cpersona')
        ->join('tcargos as tc','tpe.ccargo','=','tc.ccargo')

        //->leftjoin('tcategoriaprofesional as tcat','tpe.ccategoriaprofesional','=','tcat.ccategoriaprofesional')
        ->leftjoin('tdisciplina as dis','aredis.cdisciplina','=','dis.cdisciplina')
        ->where('pryper.cproyecto','=',$idProyecto)
       // ->where('pryper.eslider','=','1')
        ->select('pryper.cpersona','per.abreviatura','pryper.cdisciplina','pryper.cproyecto','dis.descripcion','tar.*','pryper.ccategoriaprofesional','pryper.eslider','tc.esjefaturaarea')
        ->orderBy('per.abreviatura','ASC')
        ->get();

        //$responsable = array();
        
        $responsable = [''=>''];
        $elaborador = [''=>''];
        $revisor = [''=>''];
        $aprobador = [''=>''];
        
        foreach($colaboradores as $col){

            if($col->esgeneral=='1' || $col->essoporte=='1' )
            {

            }
            
            else
            {
                
                if ($col->eslider=='1' ) {
                    $c = [$col->cpersona=>$col->abreviatura] ;
                    $responsable=$responsable+$c;
                }
                
                if ($col->esjefaturaarea=='1' || $col->ccategoriaprofesional=='13' || $col->ccategoriaprofesional=='14' || $col->ccategoriaprofesional=='15') {
                    $c = [$col->cpersona=>$col->abreviatura] ;
                    $revisor=$revisor+$c;
                }
                if ($col->ccategoriaprofesional=='13' || $col->ccategoriaprofesional=='14' || $col->ccategoriaprofesional=='15') {
                    $c = [$col->cpersona=>$col->abreviatura] ;
                    $aprobador=$aprobador+$c;
                }

                $c = [$col->cpersona=>$col->abreviatura] ;
                $elaborador=$elaborador+$c;

                //$responsable[count($responsable)]=$c;

            }         
          
        }

        $tipoProyEntregable = DB::table('ttipoproyectoentregable')
        ->lists('descripcion','ctipoproyectoentregable');

        $fechas = DB::table('ttipofechasentregable as ttf')
        ->lists('ttf.descripcion','ttf.ctipofechasentregable');

        return view('proyecto/listaEntregables',compact('proyecto','persona','editar','uminera','servicio','personal','monedas','medioentrega','revision','listEnt','estadoproyecto','documento',
                                                        'entregable','pryedt','catdoc','responsable','tipoProyEntregable','fechas','fechasCab','elaborador','revisor','aprobador')); 
    }


    public function verTablaEntregablesTodos($cproyecto,$estado){


        $listEnt=$this->listarTodosEntregables($cproyecto,$estado);
        //dd($cproyecto,$estado,$listEnt);

        return view('partials/tableListaEntregables',compact('listEnt'));
    }

    public function listarTodosEntregables($cproyecto,$estado){

        $documento = $this->obtenerRevisionEntregablesPryCli($cproyecto);
        $revisiones_internas=null;
        $crevision=null;
        if($documento){
            $revisiones_internas=$this->obtenerRevisionEntregablesPryInt($documento->cproyectodocumentos);
            $crevision=$revisiones_internas->cproyectoent_rev;
        }
        //dd($revisiones_internas->cproyectoent_rev);
        
        
        $listEnt=array();
        $pentrega = DB::table('tproyectoentregables as pryent')
        ->join('tentregables as ent','pryent.centregable','=','ent.centregables')
        ->leftjoin('ttipoproyectoentregable as tpe','pryent.ctipoproyectoentregable','=','tpe.ctipoproyectoentregable')
        ->join('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
        ->leftjoin('tdisciplinaareas as tda','dis.cdisciplina','=','tda.cdisciplina')
        ->leftjoin('tareas as tar','tda.carea','=','tar.carea')
        ->leftjoin('tproyecto as pry','pryent.cproyecto','=','pry.cproyecto')
        ->leftJoin('tactividad as act','pryent.cproyectoactividades','=','act.cactividad')
        ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
        ->leftJoin('tpersona as resp','pryent.cpersona_responsable','=','resp.cpersona')
        ->leftJoin('tpersona as ela','pryent.cper_elabora','=','ela.cpersona')
        ->leftJoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')

        ->leftJoin('tpersona as rev','pryent.cpersona_revisor','=','rev.cpersona')
        ->leftJoin('tpersona as apr','pryent.cpersona_aprobador','=','apr.cpersona')

        ->leftJoin('testadoentregable as est','pryent.cestadoentregable','=','est.cestadoentregable')
        ->leftJoin('tdocumentosparaproyecto as tdp','ent.cdocumentoparapry','=','tdp.cdocumentoparapry')
        ->leftJoin('ttiposentregables as tip','ent.ctipoentregable','=','tip.ctiposentregable')
        ->leftJoin('tproyectoentregables as tenp','pryent.cproyectoentregables_parent','=','tenp.cproyectoentregables')
        //->orderBy('pryent.descripcion_entregable','ASC')
        ->orderBy('edt.descripcion','ASC')
        ->orderBy('pryent.cproyectoentregables','ASC')
        ->where('pryent.cproyecto','=',$cproyecto)
        // ->where('tpe.ctipoproyectoentregable','=','2')
        //->whereIn('tpe.ctipoproyectoentregable',['2','3','4','5','6','7','8','9','10','11','12'])
        ->where('tpe.escontractual','=','c')
        ->where('edt.cproyectoent_rev','=',$crevision)
        ->where('edt.estado','=','1');
        

        if ($estado == 'todos') {
            $pentrega = $pentrega->where('est.descripcion','!=','Anulado');
        }

        else{
            $pentrega = $pentrega->where('est.descripcion','=','Anulado');
        }

        $pentrega = $pentrega->select('pryent.*','pryent.descripcion_entregable as descripcion','dis.descripcion as des_dis','act.descripcion as des_act','dis.cdisciplina')
        ->addSelect('edt.descripcion as des_edt','tpe.descripcion as desc_tipoproyentre','pryent.ctipoproyectoentregable','resp.abreviatura as responsable','resp.identificacion as dni_resp','ela.abreviatura as elaborador','ela.identificacion as dni_elab','est.descripcion as estado','est.activo as activo','tip.descripcion as des_tipoentre','tenp.descripcion_entregable as parent','rev.abreviatura as revisor','apr.abreviatura as aprobador','pryent.codigo as codEnt','pry.codigo as codproy','edt.codigo as cedt','tar.codigo as codarea','ent.ctipoentregable','tdp.codigo as coddoc','edt.ctipoedt','cli.abreviatura as cliente','pryent.codigocliente as codCli')
        ->get();            

        $fechasCab=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->where('tpe.cproyecto','=',$cproyecto)
        ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec')
        ->orderBy('ttf.ctipofechasentregable','asc')
        ->distinct()
        ->get();
    

        $i=0;
        foreach($pentrega as $pent){
           $i++;
           $listE['item'] = $i;
           $listE['cproyectoentregables'] = $pent->cproyectoentregables;
           $listE['cproyecto'] = $pent->cproyecto;
           $listE['codproy'] = $pent->codproy;
           $listE['centregable'] = $pent->centregable;
           $listE['cproyectoedt'] = $pent->cproyectoedt;
           $listE['cedt'] = $pent->cedt;
           $listE['observacion'] = $pent->observacion;
           $listE['cproyectoactividades'] = $pent->cproyectoactividades;
           $listE['cpersona_responsable'] = $pent->cpersona_responsable;
           $listE['crol_responsable'] = $pent->crol_responsable;
           $listE['codigo'] = $pent->codEnt;
           $listE['codigoCliente'] = $pent->codCli;
           $listE['descripcion']= $pent->descripcion;
           $listE['ccategoriaentregable']= $pent->ccategoriaentregable;
           $listE['cdisciplina'] = $pent->cdisciplina;
           $listE['codarea'] = $pent->codarea;
           $listE['ctipoentregable'] = $pent->ctipoentregable;
           $listE['coddoc'] = $pent->coddoc;
           $listE['ctipoedt'] = $pent->ctipoedt;
           $listE['cliente'] = $pent->cliente;
        
           $listE['des_dis'] = $pent->des_dis;
           $listE['des_act'] = $pent->des_act;
           $listE['des_edt'] = $pent->des_edt;

           $listE['responsable'] = $pent->responsable;
           $listE['elaborador'] = $pent->elaborador;
           $listE['estado'] = $pent->estado;
           $listE['activo'] = $pent->activo;
           $listE['tipoentregable'] = $pent->des_tipoentre;
           $listE['tipoproyentregable'] = $pent->desc_tipoproyentre; 
           $listE['ctipoproyentregable'] = $pent->ctipoproyectoentregable; 
           $listE['aprobador'] = $pent->aprobador; 
           $listE['revisor'] = $pent->revisor;
           $listE['crevision'] = $pent->revision;

           $imagen='images/'.$pent->dni_elab.'.jpg';

              if(file_exists($imagen)){
                $listE['imagen'] = $pent->dni_elab;
              }else{
                $listE['imagen'] = 'sinfoto';
              }

           $revisionEnt=Tcatalogo::where('codtab','=','00006')
           ->where('digide','=',$pent->revision)
           ->first();
            
           $listE['des_revision'] = $revisionEnt->valor; 

           $fase = DB::table('tfasesproyecto_gestion')
            ->where('codigo','=',trim($pent->cfaseproyecto))

            ->first();
            $cfase=null;
            $descfase=null;

            if($fase){

                $cfase=$fase->codigo;
                $descfase=$fase->descripcion;

            }

            $listE['fase'] = $descfase; 
            $listE['cfaseproyecto'] = $cfase; 

            $anexos=DB::table('tproyectoentregables as pryent')
            ->where('pryent.cproyectoentregables_parent','=',$pent->cproyectoentregables)
            ->count();

            $listE['anexos'] = $anexos; 


          
           $afechas=array();
           $cantFechas='';

           foreach ($fechasCab as $fc) {

                $fechas=DB::table('tproyectoentregablesfechas as pfe')             
                ->Join('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable') 
                ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec',DB::raw("to_char(pfe.fecha,'DD-MM-YY') as fecha"),DB::raw("to_char(pfe.fecha,'YYYY,MM,DD') as fecha_original"))
                ->where('pfe.cproyectoentregable','=',$pent->cproyectoentregables)
                ->orderBy('ttf.ctipofechasentregable','asc')
                ->get();  

                $cantFechas=count($fechas);

                $fec['fechaDesc']=$fc->tipofec;  
                $fec['fecha']=''; 
                $fec['ctipo']='';                    
                $fec['fecha_original']='';

                if($fechas){
               
                   foreach ($fechas as $f) {

                        if($f->ctipofechasentregable==$fc->ctipofechasentregable){
                            $fec['fecha']=$f->fecha;
                            $fec['fecha_original']=$f->fecha_original;
                            $fec['ctipo']=$f->ctipofechasentregable;  
                        }
                            
                    }
                        $afechas[count($afechas)]=$fec;
                }

                else{
                        $afechas='SinFechas';
                }
           }

           $listE['fechas']=$afechas;
           $listE['cantidadFechas']=$cantFechas;

          
           $listEnt[count($listEnt)]=$listE;
         
        }
// dd($listEnt);
        return $listEnt;
    }

    public function verActividadesProyecto(Request $request){


        $idProyecto=$request->input('cproyecto');

        /* Estructura de Actividades*/
        $objAct  = new ActividadSupport();     

        $estructura=$objAct->listarActividadesDelProyecto($idProyecto);
        //dd($estructura);
        /* Fin de estructura de Actividades*/

        return $estructura;


    }

       
    public function addEntregable(Request $request){
        //Sin uso 
        $objEnt = array();
        if ($request->input('add')==0){
            if(Session::get('listEnt')){
                $listEnt=Session::get('listEnt');

                $res = array_search($request->input('id'), array_map(function($e){return $e['cproyectoentregables'];},$listEnt));
                if(!($res===false)){
                    $objEnt=$listEnt[$res];
                }             

            }
        }
        $entregable = DB::table('tentregables as ent')
        ->lists('ent.descripcion','ent.centregables');
        $pryedt = DB::table('tproyectoedt as edt')
        ->where('edt.cproyecto','=',$request->input('cproyecto'))
        ->lists('descripcion','cproyectoedt');

        $cproyecto = $request->input('cproyecto');
        return view('partials.addEntregable',compact('objEnt','entregable','pryedt','cproyecto'));
    }

    public function actualizarEntregable(Request $request){
        //dd($request->all());

        DB::beginTransaction();
            $cproyectoentregable=$request->input('cproyectoentregables');

            $tentre = Tproyectoentregable::where('cproyectoentregables','=',$cproyectoentregable)->first();
            $tentre->cper_modifica=Auth::user()->cpersona;
            $tentre->fecha_modificacion=Carbon::now();
            $tentre->cestadoentregable = '003';

            if($request->input('actualizar')=="codigo"){
                $tentre->codigo =$request->input('codigo');
            }

            if($request->input('actualizar')=="codigoCli"){
                $tentre->codigocliente =$request->input('codigoCli');
            }

            if($request->input('actualizar')=="observacion"){
                $tentre->observacion =$request->input('observacion');
            }

            if($request->input('actualizar')=="descripcion"){
                $tentre->descripcion_entregable = $request->input('descripcion');    
            }

            if($request->input('actualizar')=="responsable"){
                $tentre->cpersona_responsable= $request->input('responsable');
            }

            if($request->input('actualizar')=="tipo"){
                $tentre->ctipoproyectoentregable= $request->input('tipo');
            }            
          
            if($request->input('actualizar')=="fase"){
                $tentre->cfaseproyecto = $this->valorNull($request->input('fase'),null);
            }
            $tentre->save();  

            if($request->input('actualizar')=="fecha"){
              
                $entfec = Tproyectoentregablesfecha::where('cproyectoentregable','=',$cproyectoentregable)
                ->where('ctipofechasentregable','=',$request->input('tipoFecha'))
                ->first();

                if (strlen($entfec)< 1) {
                    $entfec = new Tproyectoentregablesfecha();
                   // dd($entfec,'no existe');

                }
                else{    
                   // dd($entfec,'existe');

                    $tentre = Tproyectoentregable::where('cproyectoentregables','=',$cproyectoentregable)->first();
                    $tentre->cper_modifica=Auth::user()->cpersona;
                    $tentre->fecha_modificacion=Carbon::now();
                    $tentre->cestadoentregable = '003';
                    $tentre->save();

                    //dd($tentre,'existe');
                }
                    $entfec->ctipofechasentregable =$request->input('tipoFecha');
                    $entfec->cproyectoentregable = $cproyectoentregable;
                    $entfec->fecha =($request->input('fecha')=='')?null:$request->input('fecha');   

                    if ($request->input('fecha')=='') {
                        $entfec->delete();
                    }     
                    else{
                        $entfec->save();
                    }    

               
                    //dd($entfec);
            }

            $cproyecto=$tentre->cproyecto;
        DB::commit();

        //return Redirect::route('verTodosEntregables',$cproyecto);

        return Redirect::route('verTodosEntregablesPorEDT',$request->input('edt'));

    }

    public function saveEntregable(Request $request){

        $listEnt = array();
        if (Session::get('listEnt')){
            $listEnt = Session::get('listEnt');
        }
        $listE['item'] = 0;
        $cod = -1 * ((count($listE) + 10) + 33);
       $listE['cproyectoentregables'] = $cod;
       $listE['cproyecto'] = $request->input('cproyecto_ent');
       $listE['centregable'] = $request->input('centregable');
       $listE['cproyectoedt'] = $request->input('edt');
       $listE['observacion'] = $request->input('observacion');
       $listE['cproyectoactividades'] = $request->input('cproyectoactividades');
       $listE['cpersona_responsable'] = null;
       $listE['crol_responsable'] = null;
       $listE['codigo'] = $request->input('codigo');

       $entregable = DB::table('tentregables')->where('centregables','=',$request->input('centregable'))->first();
       $listE['descripcion']= $entregable->descripcion;
       $disciplina = DB::table('tdisciplina')->where('cdisciplina','=',$entregable->cdisciplina)->first();
       if($disciplina){
        $listE['des_dis'] = $disciplina->descripcion;
       }else{
           $listE['des_dis'] ='';
       }
       $listE['des_act'] = $request->input('des_act');
       $edts = DB::table('tproyectoedt')->where('cproyectoedt','=',$request->input('edt'))->first();
       $listE['des_edt'] = $edts->descripcion;
       $listE['id_fechaA'] = $cod - 30;
       $listE['fechaA'] = $request->input('fechaA');

       $listE['id_fechaB'] = $cod - 40;
       $listE['fechaB'] = $request->input('fechaB');

       $listE['id_fecha0'] = $cod - 50;
       $listE['fecha0'] = $request->input('fecha0');
       $listE['ccategoriaentregable'] = $request->input('catdoc');
       $tcatdoc = Db::table('tcategoriaentregable')->where('ccategoriaentregable','=',$request->input('catdoc'))
       ->first();
       $descat="";
       if($tcatdoc){
        $descat=$tcatdoc->descripcion;
       }
       $listE['des_categoriaentregable'] =$descat;
       $listEnt[count($listEnt)]=$listE;

       Session::put('listEnt',$listEnt);

       return view('partials.tableEntregablesProyecto',compact('listEnt'));

    }
    public function grabarEntregablesProyecto(Request $request){
        //dd($request->all());

        $cedt=$request->input('cproyectoedt_m');
        DB::beginTransaction();
        $tentre = new Tproyectoentregable();
        $tentre->cproyecto = $request->input('cproyecto'); 
        $tentre->cper_elabora=Auth::user()->cpersona;
        $tentre->fecha_elaboracion=Carbon::now();
        $tentre->cestadoentregable = '001';  

        $tentre->cproyectoedt = $cedt;
        $tentre->observacion = $request->input('observacionent');
        //$tentre->cpersona_responsable = $this->valorNull($request->input('responsable'),null);
        $tentre->ctipoproyectoentregable = $this->valorNull($request->input('tipoproyent'),null);
        //$tentre->ctipoproyectoentregable = $request->input('tipoproyent');
        //$tentre->cpersona_responsable = $request->input('responsable'); 
        $tentre->centregable = $request->input('centregable');

        $tentre->cfaseproyecto = $request->input('fase');

        $entregable = DB::table('tentregables as ent')
        ->where('centregables','=',$request->input('centregable'))
        ->first();

        $tentre->descripcion_entregable = $request->input('descripcion_entregable'); 
        $tentre->cproyectoentregables_parent = $request->input('cproyectoentregables_parent');   
        $tentre->save();  


        DB::commit();
        return Redirect::route('viewEntregables',$cedt);
    } 

    public function grabarEntregablesSeleccionados(Request $request){

        //dd($request->all());

        $cproyecto=$request->input('cproyecto_ent');
        $cedt=$request->input('cproyectoedt_m');

        $entregablesSeleccionados=$request->input('chkent');

        if(isset($entregablesSeleccionados)==true){            

            foreach ($entregablesSeleccionados as $ent) {

                $chkent=explode('_',$ent);
                $centregable=$chkent[0];
                $cproyectoentregable=$chkent[1];    

                $entregable = DB::table('tentregables as ent')
                ->where('centregables','=',$centregable)
                ->first();



                DB::beginTransaction();

                $tentre = new Tproyectoentregable();
                $tentre->cproyecto = $cproyecto; 
                $tentre->cper_elabora=Auth::user()->cpersona;
                $tentre->fecha_elaboracion=Carbon::now();
                $tentre->descripcion_entregable = $entregable->descripcion;   
                $tentre->cestadoentregable = '001'; 


                $tentre->cproyectoedt = $cedt;
                //$tentre->ctipoproyectoentregable = $request->input('tipoproyent');
                //$tentre->cpersona_responsable = $request->input('responsable');  
                //$tentre->cpersona_responsable = $this->valorNull($request->input('responsable'),null);
                $tentre->ctipoproyectoentregable = $this->valorNull($request->input('tipoproyent'),null);
                $tentre->observacion = $request->input('observacionent');
                $tentre->centregable = $centregable;

                $tentre->cfaseproyecto = $request->input('fasesEdt');

                $parent=$request->input('radio_ent');

                if (isset($parent)==true) {
                    $tentre->cproyectoentregables_parent = $parent;
                }

                $tentre->save();

                   
                 /*   if($cproyectoentregable =="sinAgregar")
                    {
                     
                        $tentre = new Tproyectoentregable();
                        $tentre->cproyecto = $cproyecto; 
                        $tentre->cper_elabora=Auth::user()->cpersona;
                        $tentre->fecha_elaboracion=Carbon::now();
                        $tentre->descripcion_entregable = $entregable->descripcion;   
                        $tentre->cestadoentregable = '001';   
                    }
                    else
                    {
                       
                        $tentre = Tproyectoentregable::where('cproyectoentregables','=',$cproyectoentregable)->first();
                        $tentre->cper_modifica=Auth::user()->cpersona;
                        $tentre->fecha_modificacion=Carbon::now();
                        $tentre->cestadoentregable = '003';
                        $tentre->save();
                    }
                   
                    $tentre->cproyectoedt = $cedt;
                    //$tentre->ctipoproyectoentregable = $request->input('tipoproyent');
                    //$tentre->cpersona_responsable = $request->input('responsable');  
                    //$tentre->cpersona_responsable = $this->valorNull($request->input('responsable'),null);
                    $tentre->ctipoproyectoentregable = $this->valorNull($request->input('tipoproyent'),null);
                    $tentre->observacion = $request->input('observacionent');
                    $tentre->centregable = $centregable;

                    $tentre->cfaseproyecto = $request->input('fasesEdt');

                    $parent=$request->input('radio_ent');

                    if (isset($parent)==true) {
                        $tentre->cproyectoentregables_parent = $parent;
                    }
 
                    $tentre->save();  */

                DB::commit();

            }


            
        }
        return Redirect::route('viewEntregables',$cedt);



    } 

    public function viewEntregables($edt){

        $listEnt=array();
        
        $pentrega = DB::table('tproyectoentregables as pryent')
        ->join('tentregables as ent','pryent.centregable','=','ent.centregables')
        ->leftJoin('ttipoproyectoentregable as tpe','pryent.ctipoproyectoentregable','=','tpe.ctipoproyectoentregable')
        ->leftJoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
        ->leftJoin('tactividad as act','pryent.cproyectoactividades','=','act.cactividad')
        ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
        ->leftJoin('tpersona as resp','pryent.cpersona_responsable','=','resp.cpersona')
        ->leftJoin('tpersona as ela','pryent.cper_elabora','=','ela.cpersona')
        ->leftJoin('testadoentregable as est','pryent.cestadoentregable','=','est.cestadoentregable')
        ->leftJoin('ttiposentregables as tip','ent.ctipoentregable','=','tip.ctiposentregable')
        // ->orderBy('dis.descripcion','ASC')
        // ->orderBy('pryent.descripcion_entregable','ASC')
        ->orderBy('tip.ctiposentregable','ASC')
        ->orderBy('pryent.cproyectoentregables','ASC')
        ->where('pryent.cproyectoedt','=',$edt)
        ->where('edt.estado','=','1')
        ->select('pryent.*','pryent.descripcion_entregable as descripcion','dis.descripcion as des_dis','act.descripcion as des_act')
        ->addSelect('edt.descripcion as des_edt','tpe.descripcion as desc_tipoproyentre','resp.abreviatura as responsable','ela.abreviatura as elaborador','est.descripcion as estado','est.activo as activo','tip.descripcion as des_tipoentre','ent.entregable_parent')
        ->get();        

       
        $i=0;
        foreach($pentrega as $pent){

            $fase = DB::table('tfasesproyecto_gestion')
            ->where('codigo','=',trim($pent->cfaseproyecto))

            ->first();
            $cfase=null;
            $descfase=null;

            if($fase){

                $cfase=$fase->codigo;
                $descfase=$fase->descripcion;

            }

            $instalacion=DB::table('tentregables as te')
                ->where('centregables','=', $pent->entregable_parent)
                ->first();

            if ($instalacion) {

                $componente=DB::table('tentregables as te')
                    ->where('centregables','=', $instalacion->entregable_parent)
                    ->first();
                
            }


           $i++;
           $listE['item'] = $i;
           $listE['cproyectoentregables'] = $pent->cproyectoentregables;
           $listE['cproyecto'] = $pent->cproyecto;
           $listE['centregable'] = $pent->centregable;
           $listE['cproyectoedt'] = $pent->cproyectoedt;
           $listE['observacion'] = $pent->observacion;
           $listE['cproyectoactividades'] = $pent->cproyectoactividades;
           $listE['cpersona_responsable'] = $pent->cpersona_responsable;
           $listE['crol_responsable'] = $pent->crol_responsable;
           $listE['codigo'] = $pent->codigo;
           $listE['descripcion']= $pent->descripcion;
           $listE['ccategoriaentregable']= $pent->ccategoriaentregable;
        
           $listE['des_dis'] = $pent->des_dis;
           $listE['des_act'] = $pent->des_act;
           $listE['des_edt'] = $pent->des_edt;
           $listE['responsable'] = $pent->responsable;
           $listE['elaborador'] = $pent->elaborador;
           $listE['estado'] = $pent->estado;
           $listE['activo'] = $pent->activo;
           $listE['tipoentregable'] = $pent->des_tipoentre;
           $listE['tipoproyentregable'] = $pent->desc_tipoproyentre;
           $listE['fase'] = $descfase;

           $listE['instalacion'] = $instalacion?$instalacion->descripcion:'';
           $listE['componente'] = $componente?$componente->descripcion:'';

           $listE['fase'] = $descfase;

           $listE['crevision'] = $pent->revision; 

           $revisionEnt=Tcatalogo::where('codtab','=','00006')
           ->where('digide','=',$pent->revision)
           ->first();
            
           $listE['des_revision'] = $revisionEnt->valor;

        
          
           $listEnt[count($listEnt)]=$listE;
           //dd($listEnt);
        }

        return view('partials.tableEntregablesProyecto',compact('listEnt'));

        

    }  

    public function obtenerEntregAgregadosXEDT($val,$descripcion,$idPryEDT){

        $listaEntregables=DB::table('tproyectoentregables as pryent')  
        ->leftjoin('tentregables as ent','pryent.centregable','=','ent.centregables')
        ->leftJoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
        ->leftJoin('ttiposentregables as tip','ent.ctipoentregable','=','tip.ctiposentregable')
        ->where('pryent.cproyectoedt','=',$idPryEDT)
        ->wherenotin('pryent.ctipoproyectoentregable',['1']);
        if ($val=='search') {
            $listaEntregables=$listaEntregables->where('pryent.descripcion_entregable','ilike','%'.$descripcion.'%');
        }

        $listaEntregables=$listaEntregables->select('pryent.*','dis.descripcion as disciplina','tip.descripcion as tipoentregable','dis.cdisciplina','tip.ctiposentregable')
        ->distinct()
        ->get();

        return $listaEntregables;

    }


    public function SelentregableHijo($idEntregable,$cproyecto,$edt){
   

        $lista=[];
        $entregable = DB::table('tentregables as te')      
        ->leftjoin('tdocumentosparaproyecto as td','te.cdocumentoparapry','=','td.cdocumentoparapry')
        ->where('te.entregable_parent','=',$idEntregable)
        ->orderBy('te.descripcion','ASC')
        //->lists('descripcion','centregables');
        ->select('te.*','td.codigo as tipodoc')
        ->get();
        //dd($entregable);

        foreach ($entregable as $e) {

            $ent['centregables']=$e->centregables;
            $ent['descripcion']=$e->descripcion;
            $ent['disciplina']=$e->cdisciplina;
            $ent['tipo_paquete']=$e->tipo_paquete;
            $ent['tipodoc']=$e->tipodoc;


            $proyectoEnt=DB::table('tproyectoentregables')  
            ->where('cproyecto','=',$cproyecto)
            ->where('centregable','=',$e->centregables)
            ->where('cproyectoedt','=',$edt)
            ->first();
            $ent['agregado']='no';
            $ent['cproyectoentregables']='sinAgregar';

            if($proyectoEnt){
                $ent['agregado']='si';
                $ent['cproyectoentregables']=$proyectoEnt->cproyectoentregables;
            
            }
            $lista[count($lista)]=$ent;          
        }
     
        return $lista;
    }     

    public function SearchEntregableHijo($idEntregable,$descripcion,$cproyecto,$edt){
   

        $lista=[];
        $entregable = DB::table('tentregables as te')  
        ->leftjoin('tdocumentosparaproyecto as td','te.cdocumentoparapry','=','td.cdocumentoparapry')    
        ->where('te.entregable_parent','=',$idEntregable)
        ->where('te.descripcion','ilike','%'.$descripcion.'%')
        ->orderBy('te.descripcion','ASC')
        //->lists('descripcion','centregables');
        ->select('te.*','td.codigo as tipodoc')
        ->get();

        //dd($idEntregable,$cproyecto);
        foreach ($entregable as $e) {

            $ent['centregables']=$e->centregables;
            $ent['descripcion']=$e->descripcion;
            $ent['tipo_paquete']=$e->tipo_paquete;
            $ent['tipodoc']=$e->tipodoc;

            $proyectoEnt=DB::table('tproyectoentregables')  
            ->where('cproyecto','=',$cproyecto)
            ->where('centregable','=',$e->centregables)
            ->where('cproyectoedt','=',$edt)
            ->first();
            $ent['agregado']='no';
            $ent['cproyectoentregables']='sinAgregar';

            if($proyectoEnt){
                $ent['agregado']='si';
                $ent['cproyectoentregables']=$proyectoEnt->cproyectoentregables;

                /*dd($proyectoEnt);
            $fechasEntPry=DB::table('tproyectoentregablesfechas as fep')
            ->join('ttipofechasentregable as tef','fep.ctipofechasentregable','=','tef.ctipofechasentregable')
            ->where('fep.cproyectoentregables','=',$proyectoEnt->cproyectoentregables)
            ->get();*/

            }

            $lista[count($lista)]=$ent;
           
        }
     
        return $lista;
    }    

    public function activarEntregable($idEnt,$act){

        DB::beginTransaction();
        //dd($idEnt);

        $tentre = Tproyectoentregable::where('cproyectoentregables','=',$idEnt)->first();
        $tentre->cper_modifica=Auth::user()->cpersona;
        $tentre->fecha_modificacion=Carbon::now();

        if($act=='0'){
            $tentre->cestadoentregable='003';
     
        }

        if($act=='1'){
            $tentre->cestadoentregable='002';

        }

        $tentre->save();
        $cedt=$tentre->cproyectoedt;
        $cproyecto=$tentre->cproyecto;

        DB::commit();   

        return Redirect::route('verTodosEntregablesPorEDT',$cedt); 


        //return Redirect::route('verTodosEntregables',$cproyecto);
    }

    public function obtenerCabeceraFechasXProyecto($idProyecto){

        $fechasCab=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->where('tpe.cproyecto','=',$idProyecto)
        ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec')
        ->orderBy('ttf.ctipofechasentregable','asc')
        ->distinct()
        ->get();

        return $fechasCab;

    }


    public function verTodosEntregables($cproyecto){



        $listEnt=$this->listarTodosEntregables($cproyecto,'todos');
       
        $user=Auth::user();

        $responsable=$this->getPersonalArea();

        $tipoProyEntregable = DB::table('ttipoproyectoentregable')
        ->lists('descripcion','ctipoproyectoentregable');

        $fechasCab=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->where('tpe.cproyecto','=',$cproyecto)
        ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec')
        ->orderBy('ttf.ctipofechasentregable','asc')
        ->distinct()
        ->get();


         return view('partials.tableTodosEntregablesProyecto',compact('listEnt','responsable','tipoProyEntregable','fechasCab'));

    }

    public function obtenerCabeceraFechasXEDT($edt){

        $fechasCab=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->where('tpe.cproyectoedt','=',$edt)
        ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec')
        ->orderBy('ttf.ctipofechasentregable','asc')
        ->distinct()
        ->get();

        //dd($fechasCab);

        return $fechasCab;

    }

    public function verTodosEntregablesPorEDT($edt){

        $cpersona = Auth::user()->cpersona;
        $tipos = ['2','3','4','5','6','7','8','9','10','11','12'];

        $listEnt=$this->listarTodosEntregablesPorEDT($edt,$tipos,'ninguno','EDT');

        $columnas_id = Tvistastablaspersona::columnas_id($cpersona,1);
        $columnas_nombre = Tvistastablaspersona::columnas_nombre($cpersona,1);
        $selecion_opcion = Tvistastablaspersona::selecion_opcion($cpersona,1,'selected');

        // dd($columnas_id,$columnas_nombre,$selecion_opcion);
       
        $user=Auth::user();

        $responsable=$this->getPersonalArea();

        $tipoProyEntregable = DB::table('ttipoproyectoentregable')
        ->lists('descripcion','ctipoproyectoentregable');

        $faseProy = DB::table('tfasesproyecto_gestion')
        ->lists('descripcion','codigo');
        $faseProy = [''=>''] + $faseProy;



        $fechasCab=$this->obtenerCabeceraFechasXEDT($edt);

              //return $listEnt;
         return view('partials.tableTodosEntregablesProyecto',compact('listEnt','responsable','tipoProyEntregable','fechasCab','faseProy','columnas_id','columnas_nombre','selecion_opcion'));

    }

    public function seleccionarcolumnas(Request $request)
    {   

        $cpersona = Auth::user()->cpersona;
        $opciones = $request->option;
        $tipo = $request->tipo;

        $sele = Tvistastablaspersona::guardarcustumizacion($cpersona,$tipo,$opciones);

        return $sele;
    }

    private function obtenerFechasXEntregable($idEntregable,$tipoFecha){

        $fechas=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->select('pfe.ctipofechasentregable','ttf.descripcion as tipofec','pfe.fecha',DB::raw("to_char(pfe.fecha,'DD/MM/YYYY') as fecha"),'tpe.cproyectoentregables')
        ->where('tpe.cproyectoentregables','=',$idEntregable);

        if ($tipoFecha=='fecha0') {
            $fechas=$fechas->where('ttf.ctipofechasentregable','=','9');
        }

        $fechas=$fechas->orderBy('ttf.ctipofechasentregable','desc')
        ->get();

        return $fechas;

    }

    public function getFechaEntAnteriores($idEntregable){

        $fechas=$this->obtenerFechasXEntregable($idEntregable,'todos');

        $fechasMenores=array();

        foreach ($fechas as $f) {
            $fechasMenores[count($fechasMenores)]=$f;
        }

        return $fechasMenores;
    }

    public function getFechasNoAgregadas($idEntregable){

        $fechas=$this->obtenerFechasXEntregable($idEntregable,'fecha0');

        $fecAgregadas=array();

        $listaAgregadas=$this->getFechaEntAnteriores($idEntregable);
        foreach ($listaAgregadas as $la) {
           $fecAgregadas[count($fecAgregadas)]=$la->ctipofechasentregable;
        }

        $listaFechas=DB::table('ttipofechasentregable as tf') 
        ->whereNotIn('tf.ctipofechasentregable',$fecAgregadas)
        ->orderBy('tf.ctipofechasentregable','asc');
        

        if($fechas){

            $listaFechas=$listaFechas->where('tf.ctipofechasentregable','>','9');

        }

        else{

            $listaFechas=$listaFechas->where('tf.ctipofechasentregable','<=','9');            
            
        }

        $listaFechas=$listaFechas->get();   

        return $listaFechas;

    }

    public function listarTodasLasFechas(){

        $listaFechas=DB::table('ttipofechasentregable as tf') 
        ->orderBy('tf.ctipofechasentregable','asc')
        ->get();

        return $listaFechas;

    }

    public function getFechaEntAnterioresTodos($edt){
        $proyectoEnt=DB::table('tproyectoentregables')  
        ->where('cproyectoedt','=',$edt)
        ->get();

        $fecAgregadasTodas=array();

        foreach ($proyectoEnt as $pe) {

            $listaAgregadas=$this->getFechaEntAnteriores($pe->cproyectoentregables);

            $ft[$pe->cproyectoentregables]=$listaAgregadas;

            array_push($fecAgregadasTodas,$ft);

            //$fecAgregadasTodas[count($fecAgregadasTodas)]=$ft;
        }

    }

    public function obtenerAnexos($idEntregable,$edt,$vista){

        $listEnt=$this->listarTodosEntregablesPorEDT($edt,'1',$idEntregable,$vista);

        return $listEnt;


    }

    private function listarTodosEntregablesPorEDT($edt,$tipo,$idEnt,$vista){

        $listEnt=array();
        $pentrega = DB::table('tproyectoentregables as pryent')
        ->join('tentregables as ent','pryent.centregable','=','ent.centregables')
        ->leftjoin('ttipoproyectoentregable as tpe','pryent.ctipoproyectoentregable','=','tpe.ctipoproyectoentregable')
        ->join('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
        ->leftJoin('tactividad as act','pryent.cproyectoactividades','=','act.cactividad')
        ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
        ->leftJoin('tpersona as resp','pryent.cpersona_responsable','=','resp.cpersona')
        ->leftJoin('tpersona as ela','pryent.cper_elabora','=','ela.cpersona')

        ->leftJoin('tpersona as rev','pryent.cpersona_revisor','=','rev.cpersona')
        ->leftJoin('tpersona as apr','pryent.cpersona_aprobador','=','apr.cpersona')

        ->leftJoin('testadoentregable as est','pryent.cestadoentregable','=','est.cestadoentregable')
        ->leftJoin('ttiposentregables as tip','ent.ctipoentregable','=','tip.ctiposentregable')
        ->leftJoin('tproyectoentregables as tenp','pryent.cproyectoentregables_parent','=','tenp.cproyectoentregables')
        ->orderBy('pryent.descripcion_entregable','ASC')
        ->where('pryent.cproyectoedt','=',$edt)
        ->where('edt.estado','=','1');

        if($tipo=='1'){
        $pentrega =$pentrega->where('tpe.ctipoproyectoentregable','=',$tipo);
        } else {
        $pentrega =$pentrega->whereIn('tpe.ctipoproyectoentregable',$tipo);
        }
        
        if($tipo=='1'){
            $pentrega =$pentrega->where('pryent.cproyectoentregables_parent','=',$idEnt);

        }

        
        $pentrega =$pentrega->select('pryent.*','pryent.descripcion_entregable as descripcion','dis.descripcion as des_dis','act.descripcion as des_act')
        ->addSelect('edt.descripcion as des_edt','tpe.descripcion as desc_tipoproyentre','pryent.ctipoproyectoentregable','resp.abreviatura as responsable','ela.abreviatura as elaborador','est.descripcion as estado','est.activo as activo','tip.descripcion as des_tipoentre','tenp.descripcion_entregable as parent','rev.abreviatura as revisor','apr.abreviatura as aprobador','pryent.codigocliente as codCli','edt.codigo as cedt')
        ->get();      

        if($tipo=='1'){
        //dd($pentrega,$idEnt);
        }


        $fechasCab=$this->obtenerCabeceraFechasXEDT($edt);


        if ($vista=='Proyecto') {
            $fechasCab=$this->obtenerCabeceraFechasXProyecto($pentrega[0]->cproyecto);
        }


        $i=0;
        foreach($pentrega as $pent){
           $i++;
            

            $fase = DB::table('tfasesproyecto_gestion')
            ->where('codigo','=',trim($pent->cfaseproyecto))

            ->first();
            $cfase=null;
            $descfase=null;

            if($fase){

                $cfase=$fase->codigo;
                $descfase=$fase->descripcion;

            }

           $listE['item'] = $i;
           $listE['cproyectoentregables'] = $pent->cproyectoentregables;
           $listE['cproyecto'] = $pent->cproyecto;
           $listE['centregable'] = $pent->centregable;
           $listE['cproyectoedt'] = $pent->cproyectoedt;
           $listE['observacion'] = $pent->observacion;
           $listE['cproyectoactividades'] = $pent->cproyectoactividades;
           $listE['cpersona_responsable'] = $pent->cpersona_responsable;
           $listE['crol_responsable'] = $pent->crol_responsable;
           $listE['codigo'] = $pent->codigo;
           $listE['codigoCliente'] = $pent->codCli;
           $listE['descripcion']= $pent->descripcion;
           $listE['ccategoriaentregable']= $pent->ccategoriaentregable;
           $listE['parent'] = $pent->parent;
        
           $listE['des_dis'] = $pent->des_dis;
           $listE['des_act'] = $pent->des_act;
           $listE['des_edt'] = $pent->des_edt;
           $listE['cedt'] = $pent->cedt;
           $listE['responsable'] = $pent->responsable;
           $listE['elaborador'] = $pent->elaborador;
           $listE['estado'] = $pent->estado;
           $listE['activo'] = $pent->activo;
           $listE['tipoentregable'] = $pent->des_tipoentre;
           $listE['tipoproyentregable'] = $pent->desc_tipoproyentre; 
           $listE['ctipoproyentregable'] = $pent->ctipoproyectoentregable; 
           $listE['fase'] = $descfase; 
           $listE['cfaseproyecto'] = $cfase; 

           $listE['revisor'] =$pent->revisor; 
           $listE['aprobador'] = $pent->aprobador; 

           $anexos=DB::table('tproyectoentregables as pryent')
            ->where('pryent.cproyectoentregables_parent','=',$pent->cproyectoentregables)
            ->count();

            $listE['anexos'] = $anexos; 

            $listE['crevision'] = $pent->revision; 

           $revisionEnt=Tcatalogo::where('codtab','=','00006')
           ->where('digide','=',$pent->revision)
           ->first();
            
           $listE['des_revision'] = $revisionEnt->valor;


          
           $afechas=array();

           $cantFechas='';

           foreach ($fechasCab as $fc) {

                $fechas=DB::table('tproyectoentregablesfechas as pfe')             
                ->Join('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable') 
                ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec',DB::raw("to_char(pfe.fecha,'DD-MM-YY') as fecha"))
                ->where('pfe.cproyectoentregable','=',$pent->cproyectoentregables)
                ->orderBy('ttf.ctipofechasentregable','asc')
                ->get(); 
                //dd(count($fechas)); 
                $cantFechas=count($fechas);

                $fec['fechaDesc']=$fc->tipofec;  
                $fec['fecha']='';   
                $fec['ctipo']=$fc->ctipofechasentregable;                       
            

                if($fechas){                    
               
                   foreach ($fechas as $f) {

                        if($f->ctipofechasentregable==$fc->ctipofechasentregable){
                            $fec['fecha']=$f->fecha;
                        }
                            
                    }
                        $afechas[count($afechas)]=$fec;
                }

                else{
                        $afechas='SinFechas';
                }
           }

           $listE['fechas']=$afechas;
           $listE['cantidadFechas']=$cantFechas;

          
           $listEnt[count($listEnt)]=$listE;
         
        }

        return $listEnt;
    }

    private function getPersonalArea(){
        $user=Auth::user();

        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->leftjoin('tareas as tap','ta.carea_parent','=','tap.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->select('e.cpersona','e.carea', 'e.ccargo','ta.carea','ta.descripcion','tap.carea as cpadre','tap.codigo as codigopadre')
        ->first();
        $carea=0;
        $carea_parent=0;
       // $codigoparent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->cpadre;
          //  $codigoparent=$tarea->codigopadre;
        }
        $personagte=DB::table('tareas as ta')
        ->where('ta.carea','=',$carea_parent)
        ->where('ta.codigo','ilike','%'.'GT'.'%')
        ->first();

        $codigoGT=0;

        if($personagte){
        $codigoGT=$personagte->codigo;
        }

        $personal = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
      
        ->where('tnat.esempleado','=','1')
        ->where('emp.estado','=','ACT')
        ->where('emp.carea','=',$carea)
        ->orderBy('per.abreviatura','ASC');

           if(substr($codigoGT,0,2)=='GT'){
            $personal=$personal->orWhere('emp.carea','=',$carea_parent);
           }
                 
        $personal=$personal->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');
        $personal = [''=>''] + $personal;

        return $personal;
    }

    public function getEntregableProyecto($idEnt){

        $proyectoEntregable=DB::table('tproyectoentregables as tpe')
        ->leftjoin('tentregables as te','tpe.centregable','=','te.centregables')
        ->where('tpe.cproyectoentregables','=',$idEnt)
        ->select('tpe.*','te.cdisciplina as disciplina','te.ctipoentregable as tipoent')
        ->first();

        //dd($proyectoEntregable);


        return Response::json($proyectoEntregable);

    }


    public function grabarFechasEntregablePry(Request $request){

        DB::beginTransaction();
        $cproyectoentregable= $request->input('centregableproy_fecha');
        $cproyectoentfecha=$request->input('cproyectoentfecha');      
     
        /*if (strlen($cproyectoentfecha)<=0) {
            $entfec = new Tproyectoentregablesfecha();
            
        }
        else{
            $est = Tproyectoentregablesfecha::where('cproyectoentregablesfechas','=',$cproyectoentfecha)
            ->where('ctipofechasentregable','=',$request->input('tipofecha'))
            ->first();

            $tentre = Tproyectoentregable::where('cproyectoentregables','=',$request->input('centregableproy_fecha'))->first();
            $tentre->cper_modifica=Auth::user()->cpersona;
            $tentre->fecha_modificacion=Carbon::now();
            $tentre->cestadoentregable = '003';
            $tentre->save();
        }
            $entfec->cproyectoentregable = $request->input('centregableproy_fecha');
            $entfec->ctipofechasentregable =$request->input('tipofecha');
            $entfec->fecha =$request->input('fecha');            
            $entfec->save();*/
            $tipofech = DB::table('ttipofechasentregable')->where('ctipofechasentregable','=',$request->input('tipofecha'))->first();

            $entfec = Tproyectoentregablesfecha::where('cproyectoentregable','=',$cproyectoentregable)
            ->where('ctipofechasentregable','=',$request->input('tipofecha'))
            ->first();

            if (strlen($entfec)< 1) {
                $entfec = new Tproyectoentregablesfecha();

            }
            else{    

                $tentre = Tproyectoentregable::where('cproyectoentregables','=',$cproyectoentregable)->first();
                $tentre->cper_modifica=Auth::user()->cpersona;
                $tentre->fecha_modificacion=Carbon::now();
                $tentre->cestadoentregable = '003';
                $tentre->save();

            }
                $entfec->ctipofechasentregable =$request->input('tipofecha');
                $entfec->cproyectoentregable = $cproyectoentregable;
                $entfec->fecha =$request->input('fecha');
                $entfec->revision = $tipofech->revision;  
                $entfec->save();
        
        DB::commit();   

        return Redirect::route('viewFechasEntregablesPry',$cproyectoentregable); 


    }

    public function grabarFechasEntregablePrySelec(Request $request){


        $cproyecto=$request->input('cproyecto');
        $entregablesSeleccionados=$request->input('checkbox_ent');

        if(isset($entregablesSeleccionados)==true){

            foreach ($entregablesSeleccionados as $ent) {

                DB::beginTransaction();

                $tipofech = DB::table('ttipofechasentregable')->where('ctipofechasentregable','=',$request->input('tipofechaform'))->first();

                $entfec = Tproyectoentregablesfecha::where('cproyectoentregable','=',$ent)
                ->where('ctipofechasentregable','=',$request->input('tipofechaform'))
                ->first();

                if (strlen($entfec)< 1) {
                    $entfec = new Tproyectoentregablesfecha();

                }
                else{    

                    $tentre = Tproyectoentregable::where('cproyectoentregables','=',$ent)->first();
                    $tentre->cper_modifica=Auth::user()->cpersona;
                    $tentre->fecha_modificacion=Carbon::now();
                    $tentre->cestadoentregable = '003';
                    $tentre->save();

                }
                    $entfec->ctipofechasentregable =$request->input('tipofechaform');
                    $entfec->cproyectoentregable = $ent;
                    $entfec->fecha =$request->input('fechaform');
                    $entfec->revision = $tipofech->revision;             
                    $entfec->save();

                DB::commit();  

            }

        }

        //return Redirect::route('verTodosEntregablesPorEDT',$request->input('cpryedt'));  
 
        //return Redirect::route('verTodosEntregables',$cproyecto);   

    }

    public function viewFechasEntregablesPry($idEntregable){
       
        $fechasEntPry=DB::table('tproyectoentregablesfechas as fep')
        ->leftjoin('ttipofechasentregable as tef','fep.ctipofechasentregable','=','tef.ctipofechasentregable')
        ->where('fep.cproyectoentregable','=',$idEntregable)
        ->select('fep.*',DB::raw("to_char(fep.fecha,'DD-MM-YY') as fecha"),'tef.descripcion as tipofecha')
        ->orderBy('tef.ctipofechasentregable')
        ->get();
        $i=0;

        $listFecEnt=array();
        foreach ($fechasEntPry as $fe) {
            $i++;
            $fec['item'] = $i;
            $fec['cproyectoentregablesfechas'] = $fe->cproyectoentregablesfechas;
            $fec['cproyectoentregable'] = $fe->cproyectoentregable;
            $fec['ctipofecha'] = $fe->ctipofechasentregable;
            $fec['tipofecha'] = $fe->tipofecha;
            $fec['revision'] = $fe->revision;
            $fec['fecha'] = $fe->fecha;
            $listFecEnt[count($listFecEnt)]=$fec;
        }


        return view('partials.tableFechasEntregablesProyecto',compact('listFecEnt'));       


    }

    public function obtenerDisciplinasEntregables($tipoEntregable){
        $disciplina = DB::table('tdisciplina as td')
        ->join('tentregables as te','td.cdisciplina','=','te.cdisciplina')
        ->where('te.ctipoentregable','=',$tipoEntregable)
        ->select('td.descripcion','td.cdisciplina')
        ->distinct()
        ->get();
        //->lists('td.descripcion','td.cdisciplina');


        //return Response::json($disciplina);

        return $disciplina;

    }

    public function eliminarEntregablePry($idEntPry){       

        $cproyectoentregables = Tproyectoentregable::where('cproyectoentregables','=',$idEntPry)->first();
       
            DB::beginTransaction();
                $cedt=$cproyectoentregables->cproyectoedt;
                try
                {
                    $cproyectoentregables->delete();
                }catch(\Illuminate\Database\QueryException $e){
                    
                }

            DB::commit();
            
        //return Redirect::route('editarLeccionesAprendidas',$cproyecto);

        return Redirect::route('viewEntregables',$cedt);
    }


    public function eliminarEntregablePrySeleccionados(Request $request){

        $cproyecto=$request->input('cproyecto_ent');
        $cedt=$request->input('cproyectoedt_m');

        $entregablesSeleccionados=$request->input('checkbox_ent_tabla');

        //dd($request->all());

        if(isset($entregablesSeleccionados)==true){            

            foreach ($entregablesSeleccionados as $ent) {

                $chkent=explode('_',$ent);
                $centregable=$chkent[0];
                $cproyectoentregable=$chkent[1]; 


                if($cproyectoentregable != 'sinAgregar'){

                    $cproyectoentregables = Tproyectoentregable::where('cproyectoentregables','=',$cproyectoentregable)->first();

                    DB::beginTransaction();
                        $cedt=$cproyectoentregables->cproyectoedt;
                        try
                        {
                            $cproyectoentregables->delete();
                        }catch(\Illuminate\Database\QueryException $e){
                            
                        }

                    DB::commit();
                }
                else{

                }
            }
        }
        return Redirect::route('viewEntregables',$cedt);



    } 


    public function asignarActividadesEntregables(Request $request){       
     
        dd('guardado con exito',$request->all());
    }


    /*  Fin de Entregable de Proyecto*/

    /* EDT de Proyecto */
    public function edtProyecto(){
        Session::forget('edts');
        Session::forget('delEDT');
        return view('proyecto/registroEDT');
    }
    public function editarEdtProyecto(Request $request,$idProyecto){
        Session::forget('edts');
        Session::forget('delEDT');

        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = [''=>''] + $servicio;
        
        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); 
        
        $proyectosub = DB::table('tproyectosub')->where('cproyecto','=',$proyecto->cproyecto)->lists('descripcionsub','cproyectosub');

        $tipoedt = DB::table('ttipoedt')->lists('descripcion','ctipoedt');
        // dd($tipoedt);

        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $revision=""; 
        $editar=true;     

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();

       

        /*$disciplina = DB::table('tdisciplina as td')
        //->join('tdisciplinaareas as tda','tda.cdisciplina','=','td.cdisciplina')
        //->join('tareas as ta','ta.carea','=','tda.carea')
        //->whereNull('essoporte')
        ->orderBy('td.descripcion','ASC')
        ->lists('td.descripcion','td.cdisciplina');
        $disciplina = [''=>''] + $disciplina;*/


        // dd($disciplina);

        $disciplinaTodos = DB::table('tdisciplina as td')
        ->orderBy('td.descripcion')
        ->lists('td.descripcion','td.cdisciplina');
        $disciplinaTodos = [''=>''] + $disciplinaTodos;
        
        $tipoentregables = DB::table('ttiposentregables as tte')    
        ->orderBy('tte.descripcion','ASC')
        ->lists('tte.descripcion','tte.ctiposentregable');
        $tipoentregables = [''=>''] + $tipoentregables;

        $entregable = DB::table('tentregables as ent')    
        ->orderBy('ent.descripcion','ASC')
        ->lists('ent.descripcion','ent.centregables');
        $entregable = [''=>''] + $entregable;
        

        $tipoProyEntregable = DB::table('ttipoproyectoentregable')
        ->lists('descripcion','ctipoproyectoentregable');

        /* Fin Detalle EDT */
        $user=Auth::user();

        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->leftjoin('tareas as tap','ta.carea_parent','=','tap.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->select('e.cpersona','e.carea', 'e.ccargo','ta.carea','ta.descripcion','tap.carea as cpadre','tap.codigo as codigopadre')
        ->first();
        $carea=0;
        $carea_parent=0;
       // $codigoparent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->cpadre;
          //  $codigoparent=$tarea->codigopadre;
        }
        $personagte=DB::table('tareas as ta')
        ->where('ta.carea','=',$carea_parent)
        ->where('ta.codigo','ilike','%'.'GT'.'%')
        ->first();

        $codigoGT=0;

        if($personagte){
        $codigoGT=$personagte->codigo;
        }
        $objProSup = new ProyectoSupport();
        $fechas = DB::table('ttipofechasentregable as ttf')
        ->lists('ttf.descripcion','ttf.ctipofechasentregable');

        //dd($fechas);

        $responsable = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
      
        ->where('tnat.esempleado','=','1')
        ->where('emp.estado','=','ACT')
        ->where('emp.carea','=',$carea)
        ->orderBy('per.abreviatura','ASC');

           if(substr($codigoGT,0,2)=='GT'){
            $responsable=$responsable->orWhere('emp.carea','=',$carea_parent);
           }
                 
        $responsable=$responsable->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');
        $responsable = [''=>''] + $responsable;


        $faseProy = DB::table('tfasesproyecto_gestion')
        ->lists('descripcion','codigo');
        //$faseProy = [''=>''] + $faseProy;


        $documento = $this->obtenerRevisionEntregablesPryCli($proyecto->cproyecto);
        $revisiones_internas=null;
        $crevision=null;
        if($documento){
            $revisiones_internas=$this->obtenerRevisionEntregablesPryInt($documento->cproyectodocumentos);
            $crevision=$revisiones_internas->cproyectoent_rev;
        }

        $revisiones = $objProSup->getRevisionesDocumentos($proyecto->cproyecto,'LE','00058');

        $revisiones_anteriores =DB::table('tproyectodocumentos as pd')
            ->join('tproyectoentregablesrevisiones as pr','pd.cproyectodocumentos','=','pr.cproyectodocumentos')
            ->where('pd.cproyecto','=',$idProyecto)
            ->orderBy('desc','desc')
            ->lists(DB::raw('CONCAT(revision_cliente,\' - \',revision_interna) as desc'),'cproyectoent_rev');
            //->get();

        //dd($revisiones_anteriores);

        //dd($revisiones_internas->cproyectoent_rev);

        /* Detalle de EDT*/    
        
        $listaEDT= $this->listaDetalleEDT($idProyecto,$crevision);

        //dd($listaEDT);

        $tiposDocumento= DB::table('tdocumentosparaproyecto')
        //->select(DB::raw('CONCAT(codigo,\'    - \',descripcion) as desc'),'cdocumentoparapry')
        ->orderBy('codigo','asc')
        //->lists('desc','cdocumentoparapry');
        ->lists('codigo','cdocumentoparapry');
        $tiposDocumento = [''=>''] + $tiposDocumento;

         //->select(DB::RAW('CONCAT(tproductos_codigo," - ",tproductos_nombre," - ",tproductos_descripcion) as "prd"'),'idtproductos')
        
        $objflujoEmp = new FlujoLE();
        $flujo = $objflujoEmp->flujoEmp();

        Session::put('edts',$listaEDT);
       
        return view('proyecto/registroEDT',compact('proyecto','persona','editar','uminera','servicio','responsable','monedas','medioentrega','revision','proyectosub','listaEDT','estadoproyecto','documento','tipoedt','disciplinaTodos',/*'tipoentregables',*/'tipoProyEntregable','fechas','faseProy','revisiones_internas','revisiones','tiposDocumento','flujo','revisiones_anteriores','crevision')); 
    }


    public function verDetalleEDT($idProyecto,$revision){

        $listaEDT= $this->listaDetalleEDT($idProyecto,$revision);
        // return($listaEDT);

        return view('partials.tableEDTProyecto',compact('listaEDT'));  

    }

    private function listaDetalleEDT($idProyecto,$revision){
        $listaEDT= array();
        $edt=array();
        $tEDT = DB::table('tproyectoedt as tedt')
        ->leftjoin('ttipoedt as te','te.ctipoedt','=','tedt.ctipoedt')
        ->select('tedt.*','te.descripcion as tipoedt')      
        ->where('tedt.cproyecto','=',$idProyecto)
        ->where('tedt.cproyectoent_rev','=',$revision)
        ->where('tedt.estado','!=',0)
        ->orderBy('tedt.ctipoedt','ASC')
        ->orderBy('tedt.orden','ASC')
        ->get();
        // dd($tEDT);


        foreach ($tEDT as $t) {
            $edt[count($edt)]=$t;
        }
        //dd($edt);

        $listaEDTEntregable= array();

        foreach($tEDT as $edt){

            $fase = DB::table('tfasesproyecto_gestion')
            ->where('codigo','=',trim($edt->cfaseproyecto))

            ->first();
            $cfase=null;
            $descfase=null;

            if($fase){

                $cfase=$fase->codigo;
                $descfase=$fase->descripcion;

            }

            $tEDTLEPlanos = DB::table('tproyectoentregables as tle')
            ->leftjoin('tentregables as te','tle.centregable','=','te.centregables')
            ->join('tproyectoedt as tedt','tle.cproyectoedt','=','tedt.cproyectoedt')
            ->where('te.ctipoentregable','=','002')
            ->where('tle.cproyecto','=',$idProyecto)
            ->where('tle.cproyectoedt','=',$edt->cproyectoedt)
            ->where('tedt.cproyectoent_rev','=',$revision)
            ->orderBy('tle.codigo','ASC')
            ->get();

            $tEDTLEDoc = DB::table('tproyectoentregables as tle')
            ->leftjoin('tentregables as te','tle.centregable','=','te.centregables')
            ->join('tproyectoedt as tedt','tle.cproyectoedt','=','tedt.cproyectoedt')
            ->where('te.ctipoentregable','=','001')
            ->where('tle.cproyecto','=',$idProyecto)
            ->where('tle.cproyectoedt','=',$edt->cproyectoedt)
            ->where('tedt.cproyectoent_rev','=',$revision)
            ->orderBy('tle.codigo','ASC')
            ->get();

            $tEDTLEPFiguras = DB::table('tproyectoentregables as tle')
            ->leftjoin('tentregables as te','tle.centregable','=','te.centregables')
            ->join('tproyectoedt as tedt','tle.cproyectoedt','=','tedt.cproyectoedt')
            ->where('te.ctipoentregable','=','003')
            ->where('tle.cproyecto','=',$idProyecto)
            ->where('tle.cproyectoedt','=',$edt->cproyectoedt)
            ->where('tedt.cproyectoent_rev','=',$revision)
            ->orderBy('tle.codigo','ASC')
            ->get();

            $tEDTLEMapas = DB::table('tproyectoentregables as tle')
            ->leftjoin('tentregables as te','tle.centregable','=','te.centregables')
            ->join('tproyectoedt as tedt','tle.cproyectoedt','=','tedt.cproyectoedt')
            ->where('te.ctipoentregable','=','004')
            ->where('tle.cproyecto','=',$idProyecto)
            ->where('tle.cproyectoedt','=',$edt->cproyectoedt)
            ->where('tedt.cproyectoent_rev','=',$revision)
            ->orderBy('tle.codigo','ASC')
            ->get();

            $tEDTLE = DB::table('tproyectoentregables as tle')
            ->join('tproyectoedt as tedt','tle.cproyectoedt','=','tedt.cproyectoedt')
            ->where('tle.cproyecto','=',$idProyecto)
            ->where('tle.cproyectoedt','=',$edt->cproyectoedt)
            ->where('tedt.cproyectoent_rev','=',$revision)
            ->orderBy('tle.codigo','ASC')
            ->get();

            //dd($tEDTLEPlanos,$tEDTLEDoc,$tEDTLE,$revision);

            $listaEDT[count($listaEDT)] = array(
                'item' => count($listaEDT) +1 ,
                'cproyectoedt' => $edt->cproyectoedt,
                'cproyectoedt_parent' => $edt->cproyectoedt_parent,
                'cproyecto' => $edt->cproyecto,
                'nivel'=> $edt->nivel,
                'codigo' => $edt->codigo,
                'descripcion' => $edt->descripcion,
                'nroent' => count($tEDTLE),
                'nroentPla' => count($tEDTLEPlanos),
                'nroentDoc' => count($tEDTLEDoc),
                'nroentFig' => count($tEDTLEPFiguras),
                'nroentMap' => count($tEDTLEMapas),
                'ctipoedt' => $edt->ctipoedt,
                'tipoedt' => $edt->tipoedt,
                'cfaseproyecto' => $cfase,
                'faseproyecto' => $descfase
            );
        }

        return $listaEDT;

    }

    public function getFasesProy(){

         $faseProy = DB::table('tfasesproyecto_gestion')         
        ->get();

        return $faseProy;

    }

    public function addEDT(Request $request){
        $cod= $request->input('cod');
        $nom= $request->input('nom');

        $listaEDT=array();
        if(Session::get('edts')){
            $listaEDT = Session::get('edts',array());
        }
        $i= -1*(count($listaEDT)+1)*10;


        $listaEDT[count($listaEDT)] = array(
            'item' => count($listaEDT) + 1,
            'cproyectoedt' => $i ,
            'codigo' => $cod,
            'descripcion' => $nom,
            'nroent' => 0,
            'tipo' => "0"
            );

        Session::put('edts',$listaEDT);
        $listaEDT = Session::get('edts',array());
        // dd($listaEDT);
        return view('partials.tableEDTProyecto',compact('listaEDT'));
    }

    public function delEDT($idEDT){
        // $listaEDT=array();
        // if(Session::get('edts')){
        //     $listaEDT = Session::get('edts');
        // }
        // if($idEDT > 0){
        //     $delEDT=array();
        //     if(Session::get('delEDT')){
        //         $delEDT = Session::get('delEDT');
        //     }
        //     $delEDT[count($delEDT)]=$idEDT;
        //     Session::put('delEDT',$delEDT);
        // }
        // $newestr=array();
        // foreach($listaEDT as $par){
        //     if($par['cproyectoedt']!=$idEDT){
        //         $newestr[count($newestr)]=$par;
        //     }
        // }
        // $listaEDT=$newestr;
        // Session::put('edts',$listaEDT);

        // return view('partials.tableEDTProyecto',compact('listaEDT'));
        // 
        // 
        $existe_edt = Tproyectoedt::where('cproyectoedt','=',$idEDT)->first();
        $existe_entregables = Tproyectoentregable::where('cproyectoedt','=',$existe_edt->cproyectoedt)->count();
        
        return $existe_entregables;

    }

    public function updateestadoedt($idEDT)
    {
        $cantidad = $this->delEDT($idEDT);

        $edt = Tproyectoedt::where('cproyectoedt','=',$idEDT)->first();
        $edt->estado = 0;
        $edt->save();

        return $cantidad;

    }

    public function delete_edt($idEDT)
    {
        $cantidad = $this->delEDT($idEDT);

        $edt = Tproyectoedt::where('cproyectoedt','=',$idEDT)->delete();


        return $cantidad;

    }

    public function grabarEdtProyecto(Request $request){

        DB::beginTransaction();

        $cproyecto = $request->input('cproyecto');

        $revisionesCli = $this->obtenerRevisionEntregablesPryCli($cproyecto);

        // se ingresa una nueva revision por el sistema   

        $crevision=0; 

             
        if (!(is_null($revisionesCli))==false) {               
 
            $objTdoc= new Tproyectodocumento();
            $objTdoc->cproyecto=$cproyecto;
            $objTdoc->codigodocumento=$request->input('codigoproyecto').'-AND-25-LE-001';
            $objTdoc->revisionactual='A';
            $objTdoc->fechadoc=Carbon::now(); 
            $objTdoc->cdocumentoparapry='1';
            $objTdoc->save();

            $objEntRev= new Tproyectoentregablesrevisiones();
            $objEntRev->cproyectodocumentos=$objTdoc->cproyectodocumentos;
            $objEntRev->revision_cliente=$objTdoc->revisionactual;
            $objEntRev->revision_interna='0';
            $objEntRev->fecha_rev=Carbon::now(); 
            $objEntRev->save();

            $crevision=$objEntRev->cproyectoent_rev; 
        }

        else{

            $crevision=$request->input('cod_revisionInt'); 


        }

        $cproyectoedt = $request->input('cproyectoedt');
        if (strlen($cproyectoedt)<=0) {
            $est = new Tproyectoedt();
            //$est->nivel=$request->input('nivel');
            $est->nivel=0;

            $est->orden='a'.$request->input('codigo');

            $tipoEDT=DB::table('ttipoedt')
            ->where('ctipoedt','=',$request->input('tipo'))
            ->first();

            $est->cproyecto = $cproyecto;

            $est->descripcion= $tipoEDT->descripcion;
            $est->ctipoedt=$request->input('tipo');  
        }
        else{
            $est = Tproyectoedt::where('cproyectoedt','=',$cproyectoedt)->first();
            $est->descripcion= $request->input('nombre');
            $est->codigo =$request->input('codigo');
        }
            $est->cproyectoent_rev =$crevision;


            $est->save();
        
        DB::commit();

        return Redirect::route('editarEdtProyecto',$cproyecto);
    }

    public function editEDTProyecto($idEdt){

        $proyectoEdt=DB::table('tproyectoedt')
        ->where('cproyectoedt','=',$idEdt)
        ->first();


        return Response::json($proyectoEdt);

    }

    public function Seltipoentregable($disciplina){
        $tipoentregable = Tentregable::where('centregables','=',$disciplina)
        ->orderBy('cdisciplina','ASC')
        ->lists('descripcion','centregables')->all();
        
        //$tdpto = [''=>''] + $tdpto;
        return Response::json($tipoentregable);

    }
    public function Selentregable($disciplina,$tipoentregable){
   

        $lista=[];
        $entregable = DB::table('tentregables')
        ->where('cdisciplina','=',$disciplina)
        ->where('ctipoentregable','=',$tipoentregable)
        ->where('nivel','=',0)
        ->orderBy('descripcion','ASC')
        ->lists('descripcion','centregables');
        //->get();

        /*foreach ($entregable as $key => $value) {
           array_push($lista, $value->descripcion);
            //dd($lista,$key,$value);
           
        }*/
        
        // dd($entregable);
        //$tdpto = [''=>''] + $tdpto;
        return Response::json($entregable);
        //return $lista;
    }     

    

    /* Fin de EDT de Proyecto*/ 


    /* Planificacion Area de Proyecto*/
    public function planificacionareaProyecto(){

        $semana_act=date("W");
        $semana_ini=date("W");
        $semana_fin=date("W");        
        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array();
        $nofacturables[' '] = '--';
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        /*  Actividades Por proyecto */
       
        $es_jefe=false;
        $disciplinas="0";
        $cpersona = Auth::user()->cpersona;
        $tdatosemp = DB::table('tpersonadatosempleado as emp')
        ->join('tcargos as car','car.ccargo','=','emp.ccargo')
        ->where('emp.cpersona','=',$cpersona)
        ->whereNull('emp.ftermino')
        ->where('car.esjefaturaarea','=','1')
        ->orderBy('emp.fingreso','DESC')
        ->first();
        if($tdatosemp){
            $es_jefe=true;
            if(strlen($tdatosemp->carea)>0){
                $tdis = DB::table('tdisciplinaareas as da')
                ->where('da.carea','=',$tdatosemp->carea)
                ->where('da.activo','=','1')
                ->first();
                if($tdis){
                    $disciplinas=$tdis->cdisciplina;
                }
            }
        }

        $tdatosemp = DB::table('tpersonadatosempleado as emp')
        ->leftjoin('tareas as ta','emp.carea','=','ta.carea')
        ->where('emp.cpersona','=',$cpersona)
        ->whereNull('emp.ftermino')
        ->orderBy('emp.fingreso','DESC')
        ->first();

         $carea=0;         
         $carea_parent=0;
        if($tdatosemp){
            $carea = $tdatosemp->carea;
            $carea_parent=$tdatosemp->carea_parent;
        }
        $Acpersonas=array();
        $tpersonas = DB::table('tpersonadatosempleado as emp')
        ->where('emp.carea','=',$carea)
        ->orWhere('emp.carea','=',$carea_parent)
        ->whereNull('emp.ftermino')
        ->get();
        foreach($tpersonas as $per){
            $Acpersonas[count($Acpersonas)]=$per->cpersona;
        }



        $tproyectos = DB::table('tproyecto as pry')
        ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')
        ->join('tproyectodisciplina as dis','pry.cproyecto','=','dis.cproyecto');
        if(!$es_jefe){
            $tproyectos=$tproyectos
            ->join('tproyectopersona as pp',function($join){
                $join->on('pp.cdisciplina','=','dis.cdisciplina')
                ->on('pp.cproyecto','=','dis.cproyecto');
            })
            
            ->where('pp.cpersona','=',$cpersona)
            ->where('pp.eslider','=','1');
        }else{
            $tproyectos=$tproyectos
            ->where('dis.cdisciplina','=',$disciplinas);
        }
        $anio=date("Y");
        $tproyectos = $tproyectos
        ->where('pry.cestadoproyecto','=','001')
        ->where(DB::raw('substr(pry.codigo,1,2)'),'=',substr($anio,2,2) );
        $tproyectos=$tproyectos->orderBy('pry.codigo','ASC')
        ->select('pry.cproyecto','pry.nombre','pry.codigo','tu.nombre as uminera')
        ->distinct()
        ->get();

         //dd($tproyectos);
       

        $actividadPadre=DB::table('tproyectoactividades as a')
            ->join('tproyectoactividades as ap','a.cproyectoactividades','=','ap.cproyectoactividades_parent')
            ->leftJoin('tproyecto as pry','a.cproyecto','=','pry.cproyecto')
            //->where('a.cproyecto','=',$cproyecto) 
            ->where(DB::raw('substr(pry.codigo,1,2)'),'=',substr($anio,2,2) ) 
            ->where('pry.cestadoproyecto','=','001') 
            ->distinct('a.cproyectoactividades') 
            ->orderBy('pry.codigo')
            ->select('a.cproyectoactividades','a.cproyecto','pry.codigo')       
            ->get();

        $actPadre=array();
        foreach ($actividadPadre as $ap) {
            $p['cproyectoactividades']=$ap->cproyectoactividades;
            $p['cproyecto']=$ap->cproyecto;
            $p['codigo']=$ap->codigo;
            $actPadre[count($actPadre)] = $p;
        }

        //dd($actividadPadre);


        $proyectos = array();
        
        foreach($tproyectos as $pry){

            $p['cproyecto'] = $pry->cproyecto;
            $p['nombre'] = $pry->nombre;
            $p['codigo'] = $pry->codigo;
            $p['uminera'] = $pry->uminera;
            $p['planificaciones'] = array();
            $aActividades = array();
            $tprohab = DB::table('tproyectoactividadeshabilitacion as hab')
            ->where('hab.cproyecto','=',$p['cproyecto'])
            ->whereIn('hab.cpersona',$Acpersonas)
            ->where('hab.activo','=','1')
            ->select('cproyectoactividades')
            ->distinct()
            ->get();
            foreach($tprohab as $act){

                $aActividades[count($aActividades)]=$act->cproyectoactividades;
            }
            $tactividades= DB::table('tproyectoactividades as pryact')
            ->where('pryact.cproyecto','=',$p['cproyecto'])
            ->whereIn('pryact.cproyectoactividades',$aActividades)
            //->orderBy('codigoactvidad','ASC')
             ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
            ->get();
            foreach($tactividades as $pact){
                $ac=array();
                $ac['cproyectoactividades']=$pact->cproyectoactividades;
                $ac['cproyecto']=$pry->cproyecto;
                $ac['cproyectoejecucioncab']='';
                $ac['cactividad']='';
                $ac['tipo']='';
                $ac['personanombre']='';
                $ac['unidadminera']='';
                $ac['nombreproy']=$pry->nombre;
                $ac['des_act'] =$pact->codigoactvidad." ".$pact->descripcionactividad;     
                $ac['ctiponofacturable']='';               
                $ac['cpersona_ejecuta']='';
                $ac['cpersona_ejecuta_nom'] = '';                
                $ac['indice']=$this->getStamp();
               
                //------------------------------------------------------------//
                 
                    $horaspresupuest=DB::table('tproyectohonorarios as ph')         
                    ->join('tproyectoactividades as pa','ph.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')       
                    ->where('ph.cproyectoactividades','=',$pact->cproyectoactividades)
                    ->where('pep.cproyecto','=',$pry->cproyecto)                            
                    ->distinct('ph.cproyectoactividades')
                    ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pep.rate as ratehonora','ph.cdisciplina','ph.cproyectohonorarios')
                    ->get();

                    $totpresupuest=0;
                    $horaspres=0;

                    foreach ($horaspresupuest as $hp) {
                       $totpresupuest=$totpresupuest+($hp->hpresup*$hp->ratehonora);
                       $horaspres=$horaspres+$hp->hpresup;
                    }

                     $horasejecut=DB::table('tproyectoejecucion as pe')
                    ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
                    ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
                    ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')                       
                    ->where('pec.cproyectoactividades','=',$pact->cproyectoactividades)
                    ->where('pe.cproyecto','=',$pry->cproyecto)
                   // ->where('pec.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('pe.estado',[2,3,4,5])                   
                    ->distinct('pec.cproyectoactividades')
                    ->select('pec.cproyectoactividades','pec.cproyectoejecucioncab','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo')
                    ->get();

                      //dd($horasejecut,$horaspresupuest,$)

                    $totejectufact=0;
                    $totejectunofact=0;
                    $horaseje=0;

                    foreach ($horasejecut as $hp) {

                        if($hp->tipo=='1'){
                            $totejectufact=$totejectufact+($hp->horasejecutadas*$hp->ratecatego);
                        }

                        if($hp->tipo=='2'){
                            $totejectunofact=$totejectunofact+($hp->horasejecutadas*$hp->ratecatego);
                        }

                        $horaseje=$horaseje+$hp->horasejecutadas;
                       
                    }
                   
                   $ac['montejecu']=floatval($totejectufact)+floatval($totejectunofact);

                   /* if($k->tipo=='1'){
                        $pla['montejecu']=$totejectufact;
                    }

                    if($k->tipo=='2'){
                        $pla['montejecu']=$totejectunofact;
                    }
                   */
                    $ac['montpresup']=floatval($totpresupuest);                    
                    $ac['saldo']=floatval($totpresupuest)-(floatval($totejectufact)+floatval($totejectunofact));

                    $ac['horaspresup']=$horaspres;
                    $ac['horasejecu']=$horaseje;
                    $ac['saldohoras']=$horaspres-$horaseje;
                   // dd($horaspresupuest);



                    $p['planificaciones'][count($p['planificaciones'])]=$ac;

                    //------------------------------------------------------------//

                  ;


            }

            $proyectos[count($proyectos)]=$p;

        }
      
        /* Fin  Actividades Por proyecto */
  
        return view('proyecto/planificacionPorAreas',compact('proyectos','nofacturables','semana_act','semana_ini','semana_fin','actPadre'));
    }

    public function viewPlanificacionArea(Request $request){  
        $semana_act=date("W");
        $semana_ini=date("W");
        $semana_fin=date("W");
        $dia_act = date("Ymd");
        $user = Auth::user();
        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->carea_parent;
        }
        $fechaDesde=$request->input('fdesde');
        $fechaHasta=$request->input('fhasta');

        $discip= DB::table('tdisciplinaareas')
        ->where('carea','=',$carea)
        ->first();
        $cdisciplina=0;
        if($discip){
            $cdisciplina=$discip->cdisciplina;
        }
        
        
        if(substr($fechaDesde,2,1)=='/' || substr($fechaDesde,2,1)=='-'){    
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2));
            $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2));

        }else{
            $fechaDesde = Carbon::create(substr($fechaDesde,0,4),substr($fechaDesde,5,2),substr($fechaDesde,8,2));
            $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2));
        }
        $fechaDesde = $fechaDesde->startOfWeek();
        $fechaHasta = $fechaHasta->endOfWeek();
        $semana_ini = $fechaDesde->weekOfYear;
        $semana_fin = $fechaHasta->weekOfYear;
        $fecha1 = Carbon::create($fechaDesde->year,$fechaDesde->month,$fechaDesde->day,0,0,0);
        $fecha2 = Carbon::create($fechaHasta->year,$fechaHasta->month,$fechaHasta->day,0,0,0);


        $nroFila=0;

        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array();
        $nofacturables[' '] = '--';        
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        /* Personal por area */
        $personal = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
        ->where('tnat.esempleado','=','1')
        ->where('emp.carea','=',$carea)
        //->orwhere('emp.carea','=',$carea_parent)
        ->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');

        //dd($personal);
        /*  Fin Personal por area*/

        /*  Actividades Por proyecto */
       // $carea=0;
        //$carea_parent=0;
        $es_jefe=false;
        $disciplinas="0";
        $cpersona = Auth::user()->cpersona;
        $tdatosemp = DB::table('tpersonadatosempleado as emp')
        ->join('tcargos as car','car.ccargo','=','emp.ccargo')
        ->where('emp.cpersona','=',$cpersona)
        ->whereNull('emp.ftermino')
        ->where('car.esjefaturaarea','=','1')
        ->orderBy('emp.fingreso','DESC')
        ->first();
        if($tdatosemp){
            $es_jefe=true;
            if(strlen($tdatosemp->carea)>0){
                $tdis = DB::table('tdisciplinaareas as da')
                ->where('da.carea','=',$tdatosemp->carea)
                ->where('da.activo','=','1')
                ->first();
                if($tdis){
                    $disciplinas=$tdis->cdisciplina;
                }
            }
        }

       /* $tdatosemp = DB::table('tpersonadatosempleado as emp')
        ->where('emp.cpersona','=',$cpersona)
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->whereNull('emp.ftermino')
        ->orderBy('emp.fingreso','DESC')
        ->first();
        if($tdatosemp){
            $carea = $tdatosemp->carea;
            
        }*/
        $Acpersonas=array();
        $tpersonas = DB::table('tpersonadatosempleado as emp')
        ->where('emp.carea','=',$carea)
        ->orWhere('emp.carea','=',$carea_parent)
        ->whereNull('emp.ftermino')
        ->get();


        foreach($tpersonas as $per){
            $Acpersonas[count($Acpersonas)]=$per->cpersona;
        }

        

        $tproyectos = DB::table('tproyecto as pry')
        ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')
        ->join('tproyectodisciplina as dis','pry.cproyecto','=','dis.cproyecto');
        if(!$es_jefe){
            $tproyectos=$tproyectos
            ->join('tproyectopersona as pp',function($join){
                $join->on('pp.cdisciplina','=','dis.cdisciplina')
                ->on('pp.cproyecto','=','dis.cproyecto');
            })
            ->where('pp.cpersona','=',$cpersona)
            ->where('pp.eslider','=','1');
        }else{
            $tproyectos=$tproyectos;//->where('dis.cdisciplina','=',$disciplinas);
        }
        $anio=$request->input("anio");
        $tproyectos = $tproyectos
        ->where('pry.cestadoproyecto','=','001')
        ->where(DB::raw('substr(pry.codigo,1,2)'),'=',substr($anio,2,2) );        
        $tproyectos=$tproyectos->orderBy('pry.codigo','ASC')
        ->select('pry.cproyecto','pry.nombre','pry.codigo','tu.nombre as uminera')
        ->distinct()
        ->get();
        //dd($tproyectos);
        $proyectos = array();
        foreach($tproyectos as $pry){
            
            $p['cproyecto'] = $pry->cproyecto;
            $p['nombre'] = $pry->nombre;
            $p['codigo'] = $pry->codigo;
            $p['uminera'] = $pry->uminera;
            $p['planificaciones'] = array();
            $planificaciones = array();


            $aActividades = array();
            $tprohab = DB::table('tproyectoactividadeshabilitacion as hab')
            ->where('hab.cproyecto','=',$p['cproyecto'])
            ->whereIn('hab.cpersona',$Acpersonas)
            ->where('hab.activo','=','1')
            ->select('hab.cpersona','cproyectoactividades')
            ->distinct()
            ->get();
            foreach($tprohab as $act){

                $aActividades[count($aActividades)]=$act->cproyectoactividades;
            }
           // dd($tprohab);
            $tactividades= DB::table('tproyectoactividades as pryact')
            ->where('pryact.cproyecto','=',$p['cproyecto'])
            ->whereIn('pryact.cproyectoactividades',$aActividades)
            ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
            //->orderBy('codigoactvidad','ASC')
            ->get();
            foreach($tactividades as $pact){
                $ac=array();
                $ac['cproyectoactividades']=$pact->cproyectoactividades;
                $ac['cproyecto']=$pry->cproyecto;
                $ac['cproyectoejecucioncab']='';
                $ac['cactividad']='';
                $ac['tipo']='';
                $ac['personanombre']='';
                $ac['unidadminera']='';
                $ac['nombreproy']=$pry->nombre;
                $ac['des_act'] =$pact->codigoactvidad. " ".$pact->descripcionactividad;     
                $ac['ctiponofacturable']='';
                $ac['montpresup']='';
                $ac['montejecu']='';
                $ac['saldo']='';
                
                $ac['cpersona_ejecuta']='';
                $ac['cpersona_ejecuta_nom'] = '';                    
                $ac['indice']=$this->getStamp();

                /*******************************************************************************************************/

                $horaspresup=DB::table('tproyectohonorarios as ph')         
                    ->join('tproyectoactividades as pa','ph.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')       
                    ->where('ph.cproyectoactividades','=',$pact->cproyectoactividades)
                    ->where('pep.cproyecto','=',$pry->cproyecto)                            
                    ->distinct('ph.cproyectoactividades')
                    ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pep.rate as ratehonora','ph.cdisciplina','ph.cproyectohonorarios')
                    ->get();

                    $totpresup=0;
                    $horaspre=0;

                    foreach ($horaspresup as $hp) {
                       $totpresup=$totpresup+($hp->hpresup*$hp->ratehonora);
                       $horaspre=$horaspre+$hp->hpresup;
                    }

                     $horasejec=DB::table('tproyectoejecucion as pe')
                    ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
                    ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
                    ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')                       
                    ->where('pec.cproyectoactividades','=',$pact->cproyectoactividades)
                    ->where('pe.cproyecto','=',$pry->cproyecto)
                   // ->where('pec.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('pe.estado',[2,3,4,5])                   
                    ->distinct('pec.cproyectoactividades')
                    ->select('pec.cproyectoactividades','pec.cproyectoejecucioncab','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo')
                    ->get();

                      //dd($horasejecut,$horaspresupuest,$)

                    $totejectufa=0;
                    $totejectunofa=0;
                    $horasejfac=0;
                    $horasejnofac=0;


                    foreach ($horasejec as $hp) {

                        if($hp->tipo=='1'){
                            $totejectufa=floatval($totejectufa)+(floatval($hp->horasejecutadas)*floatval($hp->ratecatego));
                            $horasejfac=$horasejfac+$hp->horasejecutadas;
                        }

                        if($hp->tipo=='2'){
                            $totejectunofa=floatval($totejectunofa)+(floatval($hp->horasejecutadas)*floatval($hp->ratecatego));
                            $horasejnofac=$horasejnofac+$hp->horasejecutadas;
                        }
                       
                    }
                   
                   $ac['montejecu']=floatval($totejectufa+$totejectunofa);

                   /* if($k->tipo=='1'){
                        $pla['montejecu']=$totejectufa;
                    }

                    if($k->tipo=='2'){
                        $pla['montejecu']=$totejectunofa;
                    }
                   */
                    $ac['montpresup']=floatval($totpresup);                    
                    $ac['saldo']=floatval($totpresup)-(floatval($totejectufa)+floatval($totejectunofa));
                    $ac['horaspresup']=$horaspre;
                    $ac['horasejecu']=$horasejfac+$horasejnofac;
                    $ac['saldohoras']=$horaspre-($horasejfac+$horasejnofac);



                /*******************************************************************************************************/
                $planificaciones[count($planificaciones)]=$ac;

                /* actividades programadas Gerente de Area */
                $tactividadesk = DB::table('tproyectoejecucioncab as cab')
                ->join('tpersonadatosempleado as emp','emp.cpersona','=','cab.cpersona_ejecuta')
                ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
                ->leftJoin('tproyectoactividades as pryact',function($join){
                    $join->on('eje.cproyectoactividades','=','pryact.cproyectoactividades');
                    $join->on('eje.cproyecto','=','pryact.cproyecto');
                })        
                ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
                ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')
                ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')
                ->where('pryact.cproyecto','=',$p['cproyecto'])
                ->where('pryact.cproyectoactividades','=',$pact->cproyectoactividades)
                ->where('pry.cestadoproyecto','=','001')
                ->where('emp.carea','=',$carea_parent);
               // ->orwhere('emp.carea','=',$carea_parent);
        
                $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
                ->whereIn('cab.tipo',['1','2'])
                ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
                $tactividadesk=$tactividadesk->select('cab.cproyectoejecucioncab','pry.nombre as nombreproy','cab.cpersona_ejecuta','cab.tipo','cab.cproyectoactividades','pry.cproyecto','cab.cactividad','pryact.descripcionactividad')
                ->addSelect('tpc.nombre as cliente','tum.nombre as uminera','cab.ctiponofacturable','pryact.codigoactvidad')
                ->distinct()
                ->orderBy('cab.cproyectoejecucioncab','ASC')
                ->get();

                //dd($tactividadesk);

                $w=0;
                foreach($tactividadesk as $k){
                    $nroFila++;
                    $w++;
                    $pla= array();
                    $pla['item'] = $w;
                    $pla['cproyectoejecucioncab']=$k->cproyectoejecucioncab;
                    $pla['personanombre']= $k->cliente;
                    $pla['cpersona_ejecuta'] = $k->cpersona_ejecuta;
                    $tpernom = DB::table('tpersona as per')
                    ->where('per.cpersona','=',$k->cpersona_ejecuta)
                    ->first();
                    $nom="";
                    if($tpernom){
                        $nom = $tpernom->abreviatura;
                    }
                    $pla['cpersona_ejecuta_nom'] = $nom;                    
                    $pla['unidadminera']= $k->uminera;
                    $pla['nombreproy']=$k->nombreproy;
                    $pla['cproyectoactividades'] = $k->cproyectoactividades;
                    $pla['cactividad']=$k->cactividad;
                    $pla['cproyecto'] = $k->cproyecto;
                    $pla['cestructuraproyecto']="";    
                    $pla['entregable'] ="";
                    $pla['responsable'] ='';     
                    $pla['des_act'] = $k->codigoactvidad . " ". $k->descripcionactividad; 
                    $pla['ctiponofacturable'] = $k->ctiponofacturable;
                    $pla['tipo']  = $k->tipo;  

                    //------------------------------------------------------------//

                    $horaspresupuest=DB::table('tproyectohonorarios as ph')
                    
                    ->join('tproyectoactividades as pa','ph.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')       
                    ->where('ph.cproyectoactividades','=',$pla['cproyectoactividades'])
                    ->where('pep.cproyecto','=',$pla['cproyecto'])                            
                    ->distinct('ph.cproyectoactividades')
                    ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pep.rate as ratehonora','ph.cdisciplina')
                    ->get();

                    $totpresupuest=0;
                   


                    foreach ($horaspresupuest as $hp) {
                       $totpresupuest=$totpresupuest+($hp->hpresup*$hp->ratehonora);
     
                    }

                     $horasejecut=DB::table('tproyectoejecucion as pe')
                    ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
                    ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
                    ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')                       
                    ->where('pec.cproyectoactividades','=',$pla['cproyectoactividades'])
                    ->where('pe.cproyecto','=',$pla['cproyecto'])
                   // ->where('pec.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('pe.estado',[2,3,4,5])                   
                    ->distinct('pec.cproyectoactividades')
                    ->select('pec.cproyectoactividades','pec.cproyectoejecucioncab','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo')
                    ->get();

                    $totejectufact=0;
                    $totejectunofact=0;

                    foreach ($horasejecut as $hp) {

                        if($hp->tipo=='1'){
                            $totejectufact=floatval($totejectufact)+(floatval($hp->horasejecutadas)*floatval($hp->ratecatego));
                        }

                        if($hp->tipo=='2'){
                            $totejectunofact=floatval($totejectunofact)+(floatval($hp->horasejecutadas)*floatval($hp->ratecatego));
                        }
                       
                    }
                   
                   $pla['montejecu']='';

                    if($k->tipo=='1'){
                        $pla['montejecu']=floatval($totejectufact);
                    }

                    if($k->tipo=='2'){
                        $pla['montejecu']=floatval($totejectunofact);
                    }
                   
                    $pla['montpresup']=floatval($totpresupuest);                    
                    $pla['saldo']=floatval($totpresupuest)-(floatval($totejectufact)+floatval($totejectunofact));


                   /* $horasprescategoria=DB::table('tproyectohonorarios as ph')                   
                    ->join('tproyectoactividades as pa','ph.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')
                    ->join('tproyectopersona as pp','pep.cproyecto','=','pp.cproyecto')
                    ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')  
                    ->join('troldespliegue as tr','cp.croldespliegue','=','tr.croldespliegue')

                    ->where('ph.cproyectoactividades','=',$pla['cproyectoactividades'])
                    ->where('pep.cproyecto','=',$pla['cproyecto'])  
                    ->where('pp.cpersona','=',$k->cpersona_ejecuta)              
                    ->distinct('tr.croldespliegue')
                    ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pp.cproyectopersona','pp.cpersona','pp.ccategoriaprofesional','tr.croldespliegue','tr.descripcionrol')
                    ->get();
                    dd($horasprescategoria);
                    /*$totpresupuest=0;
                    $horaspres=0;


                    foreach ($horaspresupuest as $hp) {
                       $totpresupuest=$totpresupuest+($hp->hpresup*$hp->ratehonora);
                       $horaspres=$horaspres+$hp->hpresup;
                    }
                    */


                 /*   $horasejecutpersona=DB::table('tproyectoejecucion as pe')
                    ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
                    ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
                    ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')                       
                    ->where('pec.cproyectoactividades','=',$pla['cproyectoactividades'])
                    ->where('pe.cproyecto','=',$pla['cproyecto'])
                    ->where('pe.cpersona_ejecuta','=',$k->cpersona_ejecuta)
                   // ->where('pec.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('pe.estado',[2,3,4,5])                   
                    ->distinct('pec.cproyectoactividades')
                    ->select('pec.cproyectoactividades','pec.cproyectoejecucioncab','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo','pe.cpersona_ejecuta')
                    ->get();

                    //dd($horasejecutpersona);

                    $hrs_ejecff_pers=0;
                    $hrs_ejecnf_pers=0;
                    $totalhrs_ejec_ff=0;

                    foreach ($horasejecutpersona as $hp) {

                        if($hp->tipo=='1'){
                            $hrs_ejecff_pers=$hrs_ejecff_pers+$hp->horasejecutadas;
                        }

                        if($hp->tipo=='2'){
                            $hrs_ejecnf_pers=$hrs_ejecnf_pers+$hp->horasejecutadas;
                        }
                       
                    }

                    $pla['horaspresup']=$horaspres;

                     if($k->tipo=='1'){
                        $pla['horasejecu']=$hrs_ejecff_pers;
                    }

                    if($k->tipo=='2'){
                        $pla['horasejecu']=$hrs_ejecnf_pers;
                    }
                    //$pla['horasejecu']=$hrs_ejecff_pers+$hrs_ejecnf_pers;
                    $pla['saldohoras']='';*/

                    $pla['horaspresup']='';
                    $pla['horasejecu']='';
                    $pla['saldohoras']='';

                    //------------------------------------------------------------//
        
                    $actividadesp = DB::table('tproyectoejecucion as eje')
                    ->where('eje.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('eje.tipo',['1','2'])
                    ->whereIn('eje.estado',[1,2,3,4,5])
                    ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
                    $actividadesp=$actividadesp->select('eje.cproyectoejecucion','eje.horasplanificadas','eje.horasejecutadas',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'),DB::raw('to_char(eje.fplanificado,\'YYYY-MM-DD\' ) as fec'))
                    ->addSelect('eje.obsplanificada','eje.estado')
                    ->orderBy('fpla','ASC')
                    ->get();
        
                    foreach($actividadesp as $act){
        
                        $pla[$act->fpla]['cproyectoejecucion']= $act->cproyectoejecucion;
                        $pla[$act->fpla]['cproyectohonorarios']='';
                        $pla[$act->fpla]['estado']=$act->estado;
                        $pla[$act->fpla]['observacion']=$act->obsplanificada;
                        $pla[$act->fpla]['horas'] =$act->horasplanificadas;
                        $pla[$act->fpla]['horas_eje'] =$act->horasejecutadas;
                        
                        
                        
                    }
                    $pla['indice']=$this->getStamp();
                    $planificaciones[count($planificaciones)] = $pla;                   
                }

                /*  Finactividades programadas participantes Gerente de Area*/


                /* actividades programadas participantes area*/
                $tactividadesk = DB::table('tproyectoejecucioncab as cab')
                ->join('tpersonadatosempleado as emp','emp.cpersona','=','cab.cpersona_ejecuta')
                ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
                ->leftJoin('tproyectoactividades as pryact',function($join){
                    $join->on('eje.cproyectoactividades','=','pryact.cproyectoactividades');
                    $join->on('eje.cproyecto','=','pryact.cproyecto');
                })        
                ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
                ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')
                ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')
                ->where('pryact.cproyecto','=',$p['cproyecto'])
                ->where('pryact.cproyectoactividades','=',$pact->cproyectoactividades)
                ->where('pry.cestadoproyecto','=','001')
                ->where('emp.carea','=',$carea);
               // ->orwhere('emp.carea','=',$carea_parent);
        
                $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
                ->whereIn('cab.tipo',['1','2'])
                ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
                $tactividadesk=$tactividadesk->select('cab.cproyectoejecucioncab','pry.nombre as nombreproy','cab.cpersona_ejecuta','cab.tipo','cab.cproyectoactividades','pry.cproyecto','cab.cactividad','pryact.descripcionactividad')
                ->addSelect('tpc.nombre as cliente','tum.nombre as uminera','cab.ctiponofacturable','pryact.codigoactvidad')
                ->distinct()
                ->orderBy('cab.cproyectoejecucioncab','ASC')
                ->get();

                //dd($tactividadesk);

                $w=0;
                foreach($tactividadesk as $k){
                    $nroFila++;
                    $w++;
                    $pla= array();
                    $pla['item'] = $w;
                    $pla['cproyectoejecucioncab']=$k->cproyectoejecucioncab;
                    $pla['personanombre']= $k->cliente;
                    $pla['cpersona_ejecuta'] = $k->cpersona_ejecuta;
                    $tpernom = DB::table('tpersona as per')
                    ->where('per.cpersona','=',$k->cpersona_ejecuta)
                    ->first();
                    $nom="";
                    if($tpernom){
                        $nom = $tpernom->abreviatura;
                    }
                    $pla['cpersona_ejecuta_nom'] = $nom;                    
                    $pla['unidadminera']= $k->uminera;
                    $pla['nombreproy']=$k->nombreproy;
                    $pla['cproyectoactividades'] = $k->cproyectoactividades;
                    $pla['cactividad']=$k->cactividad;
                    $pla['cproyecto'] = $k->cproyecto;
                    $pla['cestructuraproyecto']="";    
                    $pla['entregable'] ="";
                    $pla['responsable'] ='';     
                    $pla['des_act'] = $k->codigoactvidad . " ". $k->descripcionactividad; 
                    $pla['ctiponofacturable'] = $k->ctiponofacturable;
                    $pla['tipo']  = $k->tipo;  

                    //------------------------------------------------------------//

                    $horaspresupuest=DB::table('tproyectohonorarios as ph')
                    
                    ->join('tproyectoactividades as pa','ph.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')       
                    ->where('ph.cproyectoactividades','=',$pla['cproyectoactividades'])
                    ->where('pep.cproyecto','=',$pla['cproyecto'])                            
                    ->distinct('ph.cproyectoactividades')
                    ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pep.rate as ratehonora','ph.cdisciplina')
                    ->get();

                    $totpresupuest=0;
                    $horaspres=0;

                    foreach ($horaspresupuest as $hp) {
                       $totpresupuest=$totpresupuest+($hp->hpresup*$hp->ratehonora);
                       $horaspres=$horaspres+$hp->hpresup;
                    }

                     $horasejecut=DB::table('tproyectoejecucion as pe')
                    ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
                    ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
                    ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')                       
                    ->where('pec.cproyectoactividades','=',$pla['cproyectoactividades'])
                    ->where('pe.cproyecto','=',$pla['cproyecto'])
                   // ->where('pec.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('pe.estado',[2,3,4,5])                   
                    ->distinct('pec.cproyectoactividades')
                    ->select('pec.cproyectoactividades','pec.cproyectoejecucioncab','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo')
                    ->get();

                    $totejectufact=0;
                    $totejectunofact=0;

                    foreach ($horasejecut as $hp) {

                        if($hp->tipo=='1'){
                            $totejectufact=floatval($totejectufact)+(floatval($hp->horasejecutadas)*floatval($hp->ratecatego));
                        }

                        if($hp->tipo=='2'){
                            $totejectunofact=floatval($totejectunofact)+(floatval($hp->horasejecutadas)*floatval($hp->ratecatego));
                        }
                       
                    }
                   
                   $pla['montejecu']='';

                    if($k->tipo=='1'){
                        $pla['montejecu']=floatval($totejectufact);
                    }

                    if($k->tipo=='2'){
                        $pla['montejecu']=floatval($totejectunofact);
                    }
                   
                    $pla['montpresup']=floatval($totpresupuest);                    
                    $pla['saldo']=floatval($totpresupuest)-(floatval($totejectufact)+floatval($totejectunofact));



                 /*   $horasejecutpersona=DB::table('tproyectoejecucion as pe')
                    ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
                    ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
                    ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
                    ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')                       
                    ->where('pec.cproyectoactividades','=',$pla['cproyectoactividades'])
                    ->where('pe.cproyecto','=',$pla['cproyecto'])
                    ->where('pe.cpersona_ejecuta','=',$k->cpersona_ejecuta)
                   // ->where('pec.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('pe.estado',[2,3,4,5])                   
                    ->distinct('pec.cproyectoactividades')
                    ->select('pec.cproyectoactividades','pec.cproyectoejecucioncab','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo','pe.cpersona_ejecuta')
                    ->get();

                    //dd($horasejecutpersona);

                    $hrs_ejecff_pers=0;
                    $hrs_ejecnf_pers=0;
                    $totalhrs_ejec_ff=0;

                    foreach ($horasejecutpersona as $hp) {

                        if($hp->tipo=='1'){
                            $hrs_ejecff_pers=$hrs_ejecff_pers+$hp->horasejecutadas;
                        }

                        if($hp->tipo=='2'){
                            $hrs_ejecnf_pers=$hrs_ejecnf_pers+$hp->horasejecutadas;
                        }
                       
                    }

                    $pla['horaspresup']=$horaspres;

                     if($k->tipo=='1'){
                        $pla['horasejecu']=$hrs_ejecff_pers;
                    }

                    if($k->tipo=='2'){
                        $pla['horasejecu']=$hrs_ejecnf_pers;
                    }
                    //$pla['horasejecu']=$hrs_ejecff_pers+$hrs_ejecnf_pers;
                    $pla['saldohoras']='';

                    */

                    $pla['horaspresup']='';
                    $pla['horasejecu']='';
                    $pla['saldohoras']='';

                    //------------------------------------------------------------//
        
                    $actividadesp = DB::table('tproyectoejecucion as eje')
                    ->where('eje.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
                    ->whereIn('eje.tipo',['1','2'])
                    ->whereIn('eje.estado',[1,2,3,4,5])
                    ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
                    $actividadesp=$actividadesp->select('eje.cproyectoejecucion','eje.horasplanificadas','eje.horasejecutadas',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'),DB::raw('to_char(eje.fplanificado,\'YYYY-MM-DD\' ) as fec'))
                    ->addSelect('eje.obsplanificada','eje.estado')
                    ->orderBy('fpla','ASC')
                    ->get();
        
                    foreach($actividadesp as $act){
        
                        $pla[$act->fpla]['cproyectoejecucion']= $act->cproyectoejecucion;
                        $pla[$act->fpla]['cproyectohonorarios']='';
                        $pla[$act->fpla]['estado']=$act->estado;
                        $pla[$act->fpla]['observacion']=$act->obsplanificada;
                        $pla[$act->fpla]['horas'] =$act->horasplanificadas;
                        $pla[$act->fpla]['horas_eje'] =$act->horasejecutadas;
                        
                        
                        
                    }
                    $pla['indice']=$this->getStamp();
                    $planificaciones[count($planificaciones)] = $pla;                   
                }


                $p['planificaciones']=$planificaciones;

                /* Fin actividades programadas particpiantes area*/


            }


            $proyectos[count($proyectos)]=$p;
        
        }
        /* Fin  Actividades Por proyecto */

        $actividadPadre=DB::table('tproyectoactividades as a')
            ->join('tproyectoactividades as ap','a.cproyectoactividades','=','ap.cproyectoactividades_parent')
            ->leftJoin('tproyecto as pry','a.cproyecto','=','pry.cproyecto')
            //->where('a.cproyecto','=',$cproyecto) 
            ->where(DB::raw('substr(pry.codigo,1,2)'),'=',substr($anio,2,2) ) 
            ->where('pry.cestadoproyecto','=','001') 
            ->distinct('a.cproyectoactividades') 
            ->orderBy('pry.codigo')
            ->select('a.cproyectoactividades','a.cproyecto','pry.codigo')       
            ->get();

        $actPadre=array();
        foreach ($actividadPadre as $ap) {
            $p['cproyectoactividades']=$ap->cproyectoactividades;
            $p['cproyecto']=$ap->cproyecto;
            $p['codigo']=$ap->codigo;
            $actPadre[count($actPadre)] = $p;
        }

        //dd($planificaciones);
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        $fdesde=$fechaDesde; //$request->input('fdesde');
        $fhasta=$fechaHasta; //$request->input('fhasta');
        /*$fdesde = Carbon::createFromDate(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2));
        $fhasta = Carbon::createFromDate(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2));*/
        //$fdesde = $fdesde->startOfWeek(); // la fecha de inicio de semana
        //$fhasta = $fhasta->endOfWeek();     // Fecha fin de la semana.        
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek]);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT)."". str_pad($dia->day,2,'0',STR_PAD_LEFT) ) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT)."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT)) ) ) {
                $seguir=false;    
            }

            
        } 
        
         return view('partials.tablePlanificacionPorArea',compact('proyectos','personal','dias','nroFila','nofacturables','semana_act','semana_ini','semana_fin','dia_act','actPadre'));


    }

    public function verHorasAcumuladasParticipante(Request $request){
        $user = Auth::user();

        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->carea_parent;

        }            


        $fechaDesde=$request->input('fdesde');
        $fechaHasta=$request->input('fhasta');
        
        //$fec=$fdesde;
        $fecha = $fechaDesde;
        if(substr($fecha,2,1)=='/' || substr($fecha,2,1)=='-'){    
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2),0,0,0);
            $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2),0,0,0);
        }else{
            $fechaDesde = Carbon::create(substr($fechaDesde,0,4),substr($fechaDesde,5,2),substr($fechaDesde,8,2),0,0,0);
            $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2),0,0,0);
        }
        $fecha1 = Carbon::create($fechaDesde->year,$fechaDesde->month,$fechaDesde->day,0,0,0);
        $fecha2 = Carbon::create($fechaHasta->year,$fechaHasta->month,$fechaHasta->day,0,0,0);
        //$semana = $fecha->weekOfYear; // se obtiene la semana del aÃ±o que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.

        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','cab.cpersona_ejecuta')
        ->join('tpersona as p','cab.cpersona_ejecuta','=','p.cpersona')
        ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('emp.estado','=','ACT')
        ->where('emp.carea','=',$carea)
        ->orWhere('emp.carea','=',$carea_parent);

        $planiAcu = array();
        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2','3'])
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
        $tactividadesk=$tactividadesk->select('cab.cpersona_ejecuta','p.nombre','p.abreviatura')
        ->distinct()
        ->orderBy('p.abreviatura')
        ->get();
        foreach($tactividadesk as $tab){
            $pla=array();
            $pla['cpersona_ejecuta']=$tab->cpersona_ejecuta;
            $pla['nombre']=$tab->abreviatura;
            $actividadesp = DB::table('tproyectoejecucioncab as cab')
            ->join('tpersonadatosempleado as emp','emp.cpersona','=','cab.cpersona_ejecuta')        
            ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
           /* ->where('emp.carea','=',$carea)*/
            ->where('cab.cpersona_ejecuta','=',$pla['cpersona_ejecuta']);

            $actividadesp = $actividadesp->whereIn('eje.estado',[1,2,3,4,5])
            ->whereIn('cab.tipo',['1','2','3'])
            ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));

            $actividadesp=$actividadesp->select(DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'),DB::raw('SUM(eje.horasplanificadas) as horas'))
            ->groupBy('fpla')
            ->get();
            foreach($actividadesp as $act){

                $pla[$act->fpla]['horas'] =$act->horas;
                
                
            }

            $planiAcu[count($planiAcu)]=$pla;
        }


        
       

        //dd($planificaciones);
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        /*$fdesde=$fec; //$request->input('fdesde');
        $fhasta=$fec; //$request->input('fhasta');
        $fdesde = Carbon::createFromDate(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2));
        $fhasta = Carbon::createFromDate(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2));*/
        $fdesde = $fecha1; 
        $fhasta = $fecha2;    
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek],$dia->weekOfYear,$dia->dayOfWeek);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT)."". str_pad($dia->day,2,'0',STR_PAD_LEFT) ) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT)."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT)) ) ) {
                $seguir=false;    
            }      

        } 
       // $participantes= $request->

        return view('partials.planificacionPorAreasParticipantes',compact('dias','planiAcu'));

    }

    public function verTareaProyecto(Request $request){
        $cproyecto = $request->input('pry');
        $objAct  = new ActividadSupport();
        
        $actParent= DB::table('tproyectoactividades as pa')
        ->where('pa.cproyecto','=',$cproyecto)
        ->whereNull('cproyectoactividades_parent')
        ->get();
        $estructura="";

        
        foreach ($actParent as $act) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$act->cproyectoactividades. "',";
            $estructura=$estructura ."label: '".$act->descripcionactividad ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$objAct->listarTreeviewProyPla($act,$cproyecto);
            //dd($est);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }                
        return $estructura;
    }
    public function grabarPlanificacionArea(Request $request){



        $cproyecto=0;
        DB::beginTransaction();
        $deta = $request->input('plani');
        //dd($deta);
        foreach($deta as $pla ){
            if($pla[9]=='N'){
                if(strlen($pla[3])>0){
                    $cambiar_persona=true;
                    if(strlen($pla[5])<=0){
                        $objCab = new Tproyectoejecucioncab();
                        //$objCab->semana='';
                        if(strlen($pla[0])>0){
                            $objCab->cproyectoactividades=$pla[0];
                        }
                        if(strlen($pla[6])>0){
                            $objCab->cactividad=$pla[6];                    
                        }
                        $objCab->estado='1';
                    
                    }else{
                        $objCab = Tproyectoejecucioncab::where('cproyectoejecucioncab','=',$pla[5])->first();
                        $nroMeje = DB::table('tproyectoejecucion as e')
                        ->where('cproyectoejecucioncab','=',$pla[5])
                        ->where('e.estado','<>','1')
                        ->count();
                        if($nroMeje>0){
                            $cambiar_persona=false;
                        }

                    }
                    if(strlen(trim($pla[8]))>0){
                        $objCab->ctiponofacturable=$pla[8];
                    }else{
                        $objCab->ctiponofacturable=null;
                    }  
                    //$objCab->estado='1';
                    $objCab->tipo=$pla[7];
                    $objCab->fregistro=date('Y-m-d H:i:s');
                    if($cambiar_persona){
                        $objCab->cpersona_ejecuta=$pla[3];
                    }
                    $objCab->save();
                    $cproyectoejecucioncab = $objCab->cproyectoejecucioncab;
                    $horas = $pla[4];
                    foreach($horas as $key => $value){ //key es YYYYMMDD
                        if(strlen($value[0]) >0  ){
                            if(strlen($value[1])>0){
                                // tiene cproyectoejecucion
                                $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$value[1])->first();


                            }else{
                                $obj = new Tproyectoejecucion();    
                                $obj->ccondicionoperativa='PLA';
                                $obj->estado='1';
                         
                                if(strlen($pla[1])>0){
                                    $obj->cproyecto=$pla[1];
                                }
                                if(strlen($pla[0])>0){
                                    $obj->cproyectoactividades=$pla[0];
                                }                            
                                if(strlen($pla[6])>0){
                                    $obj->cactividad=$pla[6];
                                }
                                
                                $obj->cproyectoejecucioncab=$cproyectoejecucioncab;
                                
                            }
                            if(strlen(trim($pla[8]))>0){
                                $obj->ctiponofacturable=$pla[8];
                            }else{
                                $obj->ctiponofacturable=null;
                            }                            
                            if($obj->estado==1){
                                $obj->cpersona_ejecuta=$pla[3];
                                $obj->horasplanificadas=$value[0];
                                $time = Carbon::now();
                                $fec = substr($key,0,4)."-".substr($key,4,2)."-".substr($key,6,2);
                                $obj->obsplanificada=$value[4];
                                $obj->fplanificado=$fec . " " . $time->hour . ":" . $time->minute .":". $time->second ;
                                $obj->fejecuta=$fec;

                                $obj->fregistro= Carbon::now();
                                $obj->tipo=$pla[7];
                                
                                $obj->csubsistema='004';
                              
                                $obj->save();       
                            }             
                        }
                    }
                }
            }else{
                // Si se va eliminar
                //5 cproyectoejecucioncab
                if(strlen($pla[5])>0){
                    $objCab = Tproyectoejecucioncab::where('cproyectoejecucioncab','=',$pla[5])->first();
                    $teje = DB::table('tproyectoejecucion')
                    ->where('cproyectoejecucioncab','=',$pla[5])
                    ->get();
                    $eliminar=true;
                    foreach($teje as $ej){
                        if($ej->estado!=1){
                            $eliminar = false;
                            break;
                        }
                    }
                    if($eliminar){
                        $deletedRows = Tproyectoejecucion::where('cproyectoejecucioncab', $pla[5])->delete();                        
                        $objCab->delete();
                    }
                }
            }
            
        }
        DB::commit();
        return "OK";
    }     
    /* Fin de Planificación de Area de Planificacion de Proyecto*/


    /*  Consulta Planificacion Proyecto*/
    public function consultaplanificacionProyecto(){
        $disciplinas = Tdisciplina::lists('descripcion','cdisciplina')->all();
        $disciplinas = [''=>'Generales'] + $disciplinas;
        return view('proyecto/consultaPlanificacion',compact('disciplinas'));
    }


     public function viewConsultaProyecto(Request $request){  

        $fdes=$request->input('fdesde');
        $fhas=$request->input('fhasta');

        $fdes = Carbon::createFromDate(substr($fdes,6,4),substr($fdes,3,2),substr($fdes,0,2));
        $fhas = Carbon::createFromDate(substr($fhas,6,4),substr($fhas,3,2),substr($fhas,0,2));

        $fdesde=$request->input('fdesde');
        $fhasta=$request->input('fhasta');
        $fdesde = Carbon::createFromDate(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2));
        $fhasta = Carbon::createFromDate(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2));
        $seguir = true;
        $dias= array();
        $dia = $fdesde;
        while($seguir){
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT)."". str_pad($dia->day,2,'0',STR_PAD_LEFT) ) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT)."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT)) ) ) {
                $seguir=false;    
            }

            
        } 

        $actividadesp = DB:: table('tproyectoejecucion as eje')
        ->join('tproyectocronogramadetalle as crodet','eje.cproyectocronogramadetalle','=','crodet.cproyectocronogramadetalle')
        ->join('tpersona as per_eje','eje.cpersona_ejecuta','=','per_eje.cpersona')
        ->leftJoin('tproyectoactividades as pryact','crodet.cproyectoactividades','=','pryact.cproyectoactividades')
        /*->leftJoin('tproyectohonorarios as hono',function($join){
            $join->on('crodet.cproyectoactividades','=','hono.cproyectoactividades');
            $join->on('hono.cestructuraproyecto','=','eje.cestructuraproyecto');
        })*/
        ->leftJoin('tproyecto as tp','pryact.cproyecto','=','tp.cproyecto')
        ->leftJoin('tpersona as tpc','tp.cpersonacliente','=','tpc.cpersona')
        ->leftJoin('tunidadminera as tum','tp.cunidadminera','=','tum.cunidadminera');
        /*if(strlen($request->input('cdisciplina'))>0){
            $actividadesp=$actividadesp->where('hono.cdisciplina','=',$request->input('cdisciplina'));
        }else{
            $actividadesp=$actividadesp->whereNull('hono.cdisciplina');
        }*/    
        $actividadesp = $actividadesp->where(DB::raw('date(eje.fplanificado)'),'>=',$fdes->toDateString());
        $actividadesp = $actividadesp->where(DB::raw('date(eje.fplanificado)'),'<=',$fhas->toDateString());
        $actividadesp=$actividadesp->select('crodet.*',DB::raw('date(eje.fplanificado) as fplanificado'),'eje.horasplanificadas','per_eje.nombre as colaborador','pryact.codigoactvidad as codact','pryact.descripcionactividad as desact'/*,'hono.horas'*/,'tum.nombre as umin','tpc.nombre as per_cli','tp.nombre as nom_proy','eje.cestructuraproyecto')
        ->orderBy('tp.nombre','ASC')
        ->orderBy('per_eje.nombre','ASC')
        ->get();        
        
        $planificaciones = array();
        foreach($actividadesp as $act){
            $honora = DB::table('tproyectohonorarios as hono')
            ->where('hono.cproyectoactividades','=',$act->cproyectoactividades)
            ->where('hono.cestructuraproyecto','=',$act->cestructuraproyecto);
            if(strlen($request->input('cdisciplina'))>0){
                $honora=$honora->where('hono.cdisciplina','=',$request->input('cdisciplina'));
            }else{
                $honora=$honora->whereNull('hono.cdisciplina');
            }             
            $honora=$honora->first();
            if($honora){
                $pla = array();
                $pla['item'] = $act->item;
                $pla['personanombre']=$act->per_cli;
                $pla['unidadminera']=$act->umin;
                $pla['nombreproy']=$act->nom_proy;
                $pla['cproyectocronogramadetalle'] = $act->cproyectocronogramadetalle;
                $pla['cproyectoejecucion']='';
                $pla['des_act'] = $act->desact;
                $pla['horasPla'] = $honora->horas;
                $pla['horaseje'] = 0;
                $pla['porav'] = 0;
                $pla['fplanificado'] = $act->fplanificado;
                $pla[$pla['fplanificado'] ] = $act->horasplanificadas;
                $entrega = DB::table('tproyectoentregables as pryent')
                ->join('tentregables as ent','pryent.centregable','=','ent.centregables')
                ->where('pryent.cproyectoentregables','=',$act->cproyectoentregable)
                ->select('pryent.*','ent.codigo','ent.descripcion')
                ->first();
                $e ="";
                if(!empty($entrega)){
                    $e= $entrega->descripcion;
                }
                $par="";
                if(!is_null($act->cestructuraproyecto)){
                    $tparticipa = DB::table('tproyectoestructuraparticipantes as par')
                    ->join('troldespliegue as rol','par.croldespliegue','=','rol.croldespliegue')
                    ->where('par.cestructuraproyecto','=',$act->cestructuraproyecto)
                    ->select('rol.descripcionrol')
                    ->first();
                    $par = $tparticipa->descripcionrol;
                }
                $pla['participante']=$par ;
                $pla['cestructuraproyecto']=$act->cestructuraproyecto;             
                $pla['entregable'] =$e;
                $pla['colaborador'] = $act->colaborador;
                $pla['responsable'] ='';

                $pla['h1'] ='';
                $planificaciones[count($planificaciones)] = $pla;
            }
        }



         return view('partials.tableConsultaProyecto',compact('planificaciones','dias'));


    }
     public function insertarTareaProyecto(Request $request){
        $user = Auth::user();
        $dia_act=date('Ymd');
        $tarea = DB::table('tpersonadatosempleado as e')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        if($tarea){
            $carea = $tarea->carea;
        }

        $fechaDesde=$request->input('fdesde');
        $fechaHasta=$request->input('fhasta');

        if(substr($fechaDesde,2,1)=='/' || substr($fechaDesde,2,1)=='-'){    
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2),0,0,0);
            $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2),23,59,59);
        }else{
            $fechaDesde = Carbon::create(substr($fechaDesde,0,4),substr($fechaDesde,5,2),substr($fechaDesde,8,2),0,0,0);
            $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2),23,59,59);
        }        
        $fdesde = Carbon::createFromDate($fechaDesde->year,$fechaDesde->month,$fechaDesde->day);
        $fhasta = Carbon::createFromDate($fechaHasta->year,$fechaHasta->month,$fechaHasta->day);
        //$semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fdesde = $fdesde->startOfWeek(); // la fecha de inicio de semana
        $fhasta = $fhasta->endOfWeek();     // Fecha fin de la semana.
        
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek]);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT)."". str_pad($dia->day,2,'0',STR_PAD_LEFT) ) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT)."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT)) ) ) {
                $seguir=false;    
            }

            
        }          
        $nroFila = $request->input('nroFila');
        $nroFilaI = $nroFila;
        $cproyecto = $request->input('cproyecto');
        $act = $request->input('cproyectoactividades');
        $tipo_fac = $request->input('chk_fact');
        if(!$tipo_fac){
            $tipo_fac=array();
        }
        $tipo_nfac = $request->input('chk_nfact');
        if(!$tipo_nfac){
            $tipo_nfac=array();
        }        
        $sel_persona =array();
        foreach($tipo_fac as $fac){
            $pr=array();
            $pr['cpersona']=$fac;
            $pr['tipo']='1';
            $sel_persona[count($sel_persona)]=$pr;
        }
        foreach($tipo_nfac as $fac){
            $pr=array();
            $pr['cpersona']=$fac;
            $pr['tipo']='2';
            $sel_persona[count($sel_persona)]=$pr;
        }

        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array();
        $nofacturables[''] = '--';
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        $personal = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
        ->where('tnat.esempleado','=','1')
        ->where('emp.carea','=',$carea)
        ->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');
        $personal = [''=>''] + $personal;


        /* Actividades a  agregar*/


        $tactividadesk = DB::table('tproyectoactividades as pryact')
        ->join('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
        ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')
        ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')
        ->where('pry.cproyecto','=',$cproyecto)
        ->where('pryact.cproyectoactividades','=',$act);
        //->whereNotNull('pryact.cproyectoactividades_parent');
        $tactividadesk=$tactividadesk->select('pry.cproyecto','pryact.cproyectoactividades','tpc.nombre as per_cli')
        ->addSelect('tum.nombre as umin','pry.nombre as nom_proy','pryact.codigoactvidad','pryact.descripcionactividad as desact')
        ->get();
        $planificaciones = array();

        $w=0;


        foreach($tactividadesk as $k){
            foreach($sel_persona as $per){
                $nroFila++;
                $w++;
                $pla = array();
                $pla['item'] = $w;
                $pla['cproyectoejecucioncab']='';
                $pla['personanombre']=$k->per_cli;
                $pla['cpersona_ejecuta'] = $per['cpersona'];
                $tpernom = DB::table('tpersona as per')
                ->where('per.cpersona','=',$per['cpersona'])
                ->first();
                $nom="";
                if($tpernom){
                    $nom = $tpernom->abreviatura;
                }
                $pla['cpersona_ejecuta_nom'] = $nom;
                $pla['unidadminera']=$k->umin;
                $pla['nombreproy']= $k->nom_proy;
                $pla['cproyectoactividades'] = $k->cproyectoactividades;
                $pla['cactividad']='';
                $pla['cproyecto'] = $k->cproyecto;
                $pla['cestructuraproyecto']="";    
                $pla['entregable'] ="";
                $pla['responsable'] ='';     
                $pla['des_act'] =$k->codigoactvidad . " ". $k->desact;      
                $pla['horasPla'] = 0;
                $pla['ctiponofacturable']='';            
                
                $pla['horaseje'] = 0;
                $pla['porav'] = 0;
                $pla['tipo']  = $per['tipo'];                  

                foreach($dias as $d){
                    $pla[$d[0]]['cproyectoejecucion']= "";
                    $pla[$d[0]]['cproyectohonorarios']="";
                    $pla[$d[0]]['horas'] = "";
                    $pla[$d[0]]['estado']="1";
                    $pla[$d[0]]['observacion']="";                
                }
                    
                $pla['indice']=$this->getStamp();
                $planificaciones[count($planificaciones)] = $pla;
            }
        }
        
        /*  FIn  Actividades a  agregar*/
        return view('partials.lineaPlanificacionPorArea',compact('planificaciones','personal','dias','nroFila','nroFilaI','nofacturables','dia_act'));
     }
     public function insertarTareaClo(Request $request){
        $user = Auth::user();
        $dia_act=date('Ymd');
        $tarea = DB::table('tpersonadatosempleado as e')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        if($tarea){
            $carea = $tarea->carea;
        }         
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        $fechaDesde=$request->input('fdesde');
        $fechaHasta=$request->input('fhasta');
        $tipo = $request->input("tipo");
        if(substr($fechaDesde,2,1)=='/' || substr($fechaDesde,2,1)=='-'){    
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2),0,0,0);
            $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2),0,0,0);
        }else{
            $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2),0,0,0);
            $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2),0,0,0);
            
        }        
        $fdesde = Carbon::create($fechaDesde->year,$fechaDesde->month,$fechaDesde->day,0,0,0);
        $fhasta = Carbon::create($fechaHasta->year,$fechaHasta->month,$fechaHasta->day,0,0,0);
        //$semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fdesde = $fdesde->startOfWeek(); // la fecha de inicio de semana
        $fhasta = $fhasta->endOfWeek();     // Fecha fin de la semana.
                
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek]);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT)."". str_pad($dia->day,2,'0',STR_PAD_LEFT) ) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT)."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT)) ) ) {
                $seguir=false;    
            }

            
        }          
        $nroFila = $request->input('nroFila');
        $nroFilaI = $nroFila;
        $act_proy = $request->input('act_proy');
        $act_act = $request->input('act_act');
        $cpersona_s = $request->input('cpersona');
        $tipo = $request->input('tipo');
        //$tipo='2';
        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array();
        $nofacturables[''] = '--';
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        $personal = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
        ->where('tnat.esempleado','=','1')
        ->where('emp.carea','=',$carea)
        ->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');


        /* Actividades a  agregar*/

        if($tipo==3){
            $tactividadesk = DB::table('tactividad as a');
            // ->whereIn('a.cactividad',$act);
            $tactividadesk=$tactividadesk->select(DB::raw(' \'\' as cproyecto'),DB::raw(' \'\' as cproyectoactividades'),DB::raw(' \'\' as per_cli'))
            ->addSelect(DB::raw(' \'\' as umin'),DB::raw(' \'\' as nom_proy'),'a.descripcion as desact','a.cactividad')
            ->get();
        }else{
            $tactividadesk = DB::table('tproyectoactividades as pryact')
            ->join('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
            //->leftJoin('tactividad as a','pryact.cactividad','=','a.cactividad')
            ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')
            ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')
            ->where('pryact.cproyectoactividades','=',$act_proy);
            $tactividadesk=$tactividadesk->select('pry.cproyecto','pryact.cproyectoactividades','tpc.nombre as per_cli')
            ->addSelect('tum.nombre as umin','pry.nombre as nom_proy','pryact.descripcionactividad as desact')
            ->addSelect(DB::raw(' \'\' as cactividad'),'pryact.codigoactvidad')
            ->get();
        }

        $planificaciones = array();

        $w=0;
        foreach($tactividadesk as $k){
               
            $nroFila++;
            $w++;
            $pla['item'] = $w;
            $pla['cproyectoejecucioncab']='';
            $pla['personanombre']=$k->per_cli;
            $pla['cpersona_ejecuta'] = $cpersona_s;
            $tpernom = DB::table('tpersona as per')
            ->where('per.cpersona','=',$cpersona_s)
            ->first();
            $nom="";
            if($tpernom){
                $nom = $tpernom->abreviatura;
            }
            $pla['cpersona_ejecuta_nom'] = $nom;            
            $pla['unidadminera']=$k->umin;
            $pla['nombreproy']= $k->nom_proy;
            $pla['cproyectoactividades'] = $k->cproyectoactividades;
            $pla['cactividad']= $k->cactividad;
            $pla['cproyecto'] = $k->cproyecto;
            $pla['cestructuraproyecto']="";    
            $pla['entregable'] ="";
            $pla['responsable'] ='';     
            $pla['des_act'] = $k->codigoactvidad ." ".$k->desact;      
            $pla['horasPla'] = 0;
            $pla['ctiponofacturable']='';            
            
            $pla['horaseje'] = 0;
            $pla['porav'] = 0;
            $pla['tipo']  = $tipo;                  

            foreach($dias as $d){
                $pla[$d[0]]['cproyectoejecucion']= "";
                $pla[$d[0]]['cproyectohonorarios']="";
                $pla[$d[0]]['horas'] = "";
                $pla[$d[0]]['estado']="1";
                $pla[$d[0]]['observacion']="";                
            }
            $pla['indice']=$this->getStamp();

            $planificaciones[count($planificaciones)] = $pla;
        }
        
        /*  FIn  Actividades a  agregar*/
        return view('partials.lineaPlanificacionPorArea',compact('planificaciones','personal','dias','nroFila','nroFilaI','nofacturables','dia_act'));
     }
     public function asignaPlaPersonal(Request $request){
        $user = Auth::user();
        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->carea_parent;
        }         
        $selec = $request->input('selec');
        //dd($selec);
        if(count($selec)<=0){
            $selec=array();
        }

        /*$nroFila = $request->input('nroFila');
        $nroFilaI = $nroFila;*/
        $descripcion="";
        $cproyectoactividades = $request->input('act_proy');       
        $tproyact = DB::table('tproyectoactividades as pryact')
        ->where('cproyectoactividades','=',$cproyectoactividades)
        ->first();
        $cproy='';
        if($tproyact){
            $descripcion = $tproyact->descripcionactividad;
            $cproy=$tproyact->cproyecto;
        }

        
        $personal=array();
        $tpersonal = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
        ->join('tproyectoactividadeshabilitacion as hab','per.cpersona','=','hab.cpersona')
        ->where('tnat.esempleado','=','1')
        ->where('hab.cproyectoactividades','=',$cproyectoactividades)
        ->where('emp.carea','=',$carea)        
        ->orderBy('per.nombre','ASC');
        //->get();

        $proyectopersona=DB::table('tproyectopersona as p')
        ->join('tpersonadatosempleado as e','p.cpersona','=','e.cpersona')    
        ->get();
        foreach ($proyectopersona as $pp) {
            if($pp->carea==$carea_parent){
                 $tpersonal=$tpersonal->orWhere('emp.carea','=',$carea_parent)
                  ->where('hab.cproyecto','=',$cproy)
                  ->where('hab.cproyectoactividades','=',$cproyectoactividades);
            }
        }
        $tpersonal=$tpersonal->get();


        foreach($tpersonal as $per){
            $p=array();
            $p['cpersona']=$per->cpersona;
            $p['nombre'] =$per->abreviatura;
            $p['sele_fact']='0';
            $p['sele_nfact']='0';      
                
                foreach($selec as $se){
                    if($cproyectoactividades==$se[0] && $per->cpersona==$se[1] && $se[2]=='1'){
                        $p['sele_fact']='1';
                    }
                    /*if($cproyectoactividades==$se[0] && $per->cpersona==$se[1] && $se[2]=='2'){
                        $p['sele_nfact']='1';
                    }*/
                }    
                   


            $personal[count($personal)]=$p;
        }
        /* Fin No Facturables*/

        /* Actividades a  agregar*/

        
        

        return view('partials.AsignaPlaParticipaTarea',compact('personal','cproyectoactividades','descripcion'));
     }     

    /* Fin de Planificacion de Proyecto*/



    /*
     * Indicadores
     */

    public function curvaS(){
        return view('proyecto/curvaSIndicadores');
    }

    public function editarcurvaS(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();

        Session::forget('listcurvaS');       

        $uminera = DB::table('tunidadminera')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();

         $persona  = DB::table('tpersona')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();

        $testado = DB::table('testadoproyecto')
        ->lists('descripcion','cestadoproyecto');
        $testado = [''=>'']+ $testado;

        $tperiodo = DB::table('tcatalogo')
        ->lists('descripcion','catalogoid');
        $tperiodo = [''=>'']+ $tperiodo;

        $curvaind=array();

        Session::put('listcurvaS',$curvaind);
        
        return view('proyecto/curvaSIndicadores',compact('proyecto','uminera','persona','testado','tperiodo'));
    }

    public function controlEjecucion(){
        return view('proyecto/controlEjecucion');
    } 

    public function editarControlEjecucion(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();

        Session::forget('listcont');

        $uminera = DB::table('tunidadminera')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();        

        $persona  = DB::table('tpersona')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();

        $testado = DB::table('testadoproyecto')
        ->lists('descripcion','cestadoproyecto');
        $testado = [''=>'']+ $testado; 

        $tarea = DB::table('tdisciplina')
        ->lists('descripcion','cdisciplina');
        $tarea = [''=>'']+ $tarea; 

        $contejec=array();

        Session::put('listcont',$contejec);
        
        return view('proyecto/controlEjecucion',compact('proyecto','uminera','persona','testado','tarea'));
    }

    public function grabarcontrolEjecucion(){
        return view('proyecto/controlEjecucion');
    }

    public function registroDeviaciones(){
        return view('proyecto/registroDesviaciones');
    }

    public function editarregistroDesviacion(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();

        Session::forget('listreg');

        $uminera = DB::table('tunidadminera')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();        

        $persona  = DB::table('tpersona')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();

        $testado = DB::table('testadoproyecto')
        ->lists('descripcion','cestadoproyecto');
        $testado = [''=>'']+ $testado;

        $regdesv=array();

        Session::put('listreg',$regdesv);
        
        return view('proyecto/registroDesviaciones',compact('proyecto','uminera','persona','testado'));
    }
    public function matrizpfull(){
        return view('proyecto.matrizfull');
    }
    public function matrizp(Request $request){
        $proyectos=array();
        $gteproyectos = DB::table('tproyecto as pry')
        ->join('tpersona as ger','pry.cpersona_gerente','=','ger.cpersona')
        ->select('pry.cpersona_gerente','ger.nombre','ger.identificacion','ger.abreviatura')
        ->distinct()
        ->where('pry.cestadoproyecto','=','001')
        ->orderBy("ger.nombre","ASC")
        ->get();
        $i=0;
        $p=0;
        $pi=0;
        foreach($gteproyectos as $gte){
            $tproyecto= DB::table('tproyecto as pry')
            ->join('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')
            ->select('cli.nombre as cliente','pry.codigo','pry.nombre as nombrepry','pry.finicio','pry.fcierre','pry.cpersona_gerente','pry.cproyecto',DB::raw('(CASE WHEN extract(days from (now() - finicio)) <=7  THEN 1 ELSE 0 END) as nuevo'),DB::raw('(CASE WHEN extract(days from (fcierre - now() )) > 7 AND extract(days from (fcierre - now() )) <= 14  THEN 1 ELSE 0 END) as medio'),DB::raw('(CASE WHEN extract(days from (fcierre - now())) <=7  THEN 1 ELSE 0 END) as ultimo'))
            ->where('pry.cpersona_gerente','=',$gte->cpersona_gerente)
            ->where('pry.cestadoproyecto','=','001')
            ->orderBy("pry.codigo","ASC")
            ->get();
            $pry['cpersona_gerente']=$gte->cpersona_gerente;
            //$pry['gp']=$gte->nombre;   
            $pry['gp']=$gte->abreviatura;
            $pry['identificacion_gp'] = $gte->identificacion;
            $pry['deta']=array();
            $flag=0;
            $p++;
            $pi=0;
            foreach($tproyecto as $tpry){
                $flag=1;
                $i++;
                $pi++;
                $pryd['codigopry']=$tpry->codigo;
                $pryd['nombrepry']=$tpry->nombrepry;
                $pryd['cliente'] = $tpry->cliente;
                $pryd['cproyecto'] = $tpry->cproyecto;
                $pryd['nuevo'] = $tpry->nuevo;
                $pryd['medio'] = $tpry->medio;
                $pryd['ultimo'] = $tpry->ultimo;
                /* tpryecto Disciplina*/
                // $tdis = DB::table('tproyectodisciplina as pdis')
                $tdis = DB::table('tproyectopersona as pdis')
                ->join('tdisciplina as dis','pdis.cdisciplina','=','dis.cdisciplina')
                // ->leftJoin('tpersona as p','pdis.cpersona_rdp','=','p.cpersona')
                ->leftJoin('tpersona as p','pdis.cpersona','=','p.cpersona')
                ->leftJoin('tpersonanaturalinformacionbasica as nat','nat.cpersona','=','p.cpersona')
                ->where('pdis.cproyecto','=',$tpry->cproyecto)
                // ->select('pdis.cdisciplina','dis.descripcion','pdis.eslp','nat.nombres','nat.apaterno','p.abreviatura')
                ->select('pdis.cdisciplina','dis.descripcion','pdis.eslider','nat.nombres','nat.apaterno','p.abreviatura')
                ->get();
                $pryd['lider']="";
                $pryd['civil']="";
                $pryd['geotec']="";
                $pryd['hidrau']="";
                foreach($tdis as $d){
                    //$nombres = $d->apaterno ." ". $d->nombres;
                    $nombres = $d->abreviatura;
                    // if($d->eslp=='1'){
                        // $pryd['lider']= $nombres;
                    // }
                    if($d->cdisciplina==1 && $d->eslider=1){
                        //diseno civil
                        $pryd['civil']= $nombres;
                    }else if($d->cdisciplina==3  && $d->eslider=1){
                        //hidraulico
                        $pryd['hidrau']= $nombres;
                    }else if($d->cdisciplina==4  && $d->eslider=1){
                        //geotecnia
                        $pryd['geotec']= $nombres;

                    }
                }


                $fec_ini= substr($tpry->finicio,8,2).'-'.substr($tpry->finicio,5,2).'-'.substr($tpry->finicio,0,4);
                $fec_fin= substr($tpry->fcierre,8,2).'-'.substr($tpry->fcierre,5,2).'-'.substr($tpry->fcierre,0,4);


                $pryd['finicio'] = $fec_ini;
                $pryd['fcierre'] = $fec_fin;
                $pry['deta'][count($pry['deta'])]=$pryd;
                if($i>=10 || $p>=3 || ($p>=3 && $pi>=4)){
                    $proyectos[count($proyectos)]=$pry;
                    $pry['cpersona_gerente']=$gte->cpersona_gerente;
                    //$pry['gp']=$gte->nombre;
                    $pry['gp']=$gte->abreviatura;      
                    $pry['identificacion_gp'] = $gte->identificacion;      
                    $pry['deta']=array(); 
                    $flag=0;
                    $i=0;     
                    $p=0;              
                }
            }
            if($flag>0){
                $proyectos[count($proyectos)]=$pry;
            }
        }

        return view('proyecto/matrizp',compact('proyectos'));
    }
    public function images($image = null)
    {
        $path = storage_path().'/images/'.$image .'.jpg';
        dd($path);
        if (file_exists($path)) { 
            return Response::download($path);
        }
    }
    public function cambiosCronogramaRooster(){

        return view('proyecto/cambiosCronogramaRooster');
    }

    public function verCambiosCronogramaRooster(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();


        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = ['' => ''] + $servicio;

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();        
        $revision=""; 


        $editar=true;      
        $documento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','ROS')
        ->select('prydoc.cproyectodocumentos','prydoc.fechadoc','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first();          
        $fechaI=Carbon::today();
        $fechaF=Carbon::today();

        $tfechas = DB::table('tproyectocronograma as crono')
        ->join('tproyectocronogramaconstrucciondetalle as deta','crono.cproyectocronograma','=','deta.cproyectocronograma')
        ->join('tconstrucciondetafecha as fec','deta.cproyectocronogramacons','=','fec.cproyectocronogramacons')
        ->where('crono.cproyecto','=',$proyecto->cproyecto)
        ->select(DB::raw('min(fecha) as minimo, max(fecha) as maximo' ))
        ->first();
        //dd($tfechas);
        if($tfechas){
            if(!is_null($tfechas->minimo)){
                $fechaI= Carbon::createFromDate(substr($tfechas->minimo,0,4),substr($tfechas->minimo,5,2),substr($tfechas->minimo,8,2));
            }
            if(!is_null($tfechas->maximo)){
                $fechaF=Carbon::createFromDate(substr($tfechas->maximo,0,4),substr($tfechas->maximo,5,2),substr($tfechas->maximo,8,2));
            }
        }

        $objProSup = new ProyectoSupport();

        $revisiones = $objProSup->getRevisionesDocumentos($proyecto->cproyecto,'ROS','00013'); 

        $tpersonal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->take(15)
        ->get();
        $personal = array();
        foreach($tpersonal as $per){
            $p['nombre'] = $per->nombre;
            $personal[count($personal)]=$p;
        } 

        /* * Detalle de Cronograma Rooster * */
        $crono = array();
        $tcronodeta = DB::table('tproyectocronograma as crono')
        ->join('tproyectocronogramaconstrucciondetalle as deta','crono.cproyectocronograma','=','deta.cproyectocronograma')
        ->join('tpersona as per','deta.cpersonaempleado','=','per.cpersona')
        ->leftJoin('tpersonadatosempleado as dat','deta.cpersonaempleado','=','dat.cpersona')
        ->leftJoin('tcargos as car','dat.ccargo','=','car.ccargo')
        ->where('crono.cproyecto','=',$proyecto->cproyecto)

        ->select('deta.*','per.nombre as empleado','dat.ccargo','car.descripcion as cargo')
        ->get();
        foreach($tcronodeta as $deta){
            $c['cproyectocronogramacons'] = $deta->cproyectocronogramacons;
            $c['cproyectocronograma'] = $deta->cproyectocronograma;
            $c['cpersonaempleado'] = $deta->cpersonaempleado;
            $c['croldespliegue'] = $deta->croldespliegue;
            $c['cproyectosub'] = $deta->cproyectosub;
            $c['empleado'] = $deta->empleado;
            $c['cargo'] = $deta->cargo;

            $tfechas = DB::table('tconstrucciondetafecha as cfec')
            ->where('cfec.cproyectocronogramacons','=',$deta->cproyectocronogramacons)
            ->select('cfec.*',DB::raw('to_char(cfec.fecha,\'YYYYMMDD\' ) as feje'))
            ->get();
            $c['deta'] = array();
            foreach($tfechas as $fec){
                $d['cconstrucciondetafecha']=$fec->cconstrucciondetafecha;
                $d['fecha'] = $fec->fecha;
                $d['codactividad'] = $fec->codactividad;
                $d['nrohoras'] = $fec->nrohoras;
                $d['observacion'] = $fec->observacion;
                $c['deta'][$fec->feje]=$d;
            }

            $crono[count($crono)]= $c;

        }
        //dd($crono);
        /* Fin Detalle de Cronograma Rooster */
        $tleyendarooster = DB::table('tcatalogo')
        ->where('codtab','=','00014')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->get();     
        $leyenda = array();  
        foreach ($tleyendarooster as $tley){
            $leye['cod'] = $tley->digide;
            $leye['des'] = $tley->descripcion;
            $leye['color'] = $tley->valor;
            $leyenda[count($leyenda)] = $leye;
        }

        $fdesde = Carbon::createFromDate($fechaI->year,$fechaI->month,$fechaI->day);
        $fhasta = Carbon::createFromDate($fechaF->year,$fechaF->month,$fechaF->day);
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".$dia->month."". $dia->day) <= ($fhasta->year ."".$fhasta->month."". $fhasta->day) ) ) {
                $seguir=false;    
            }

            
        } 
        
        
        return view('proyecto/cambiosCronogramaRooster',compact('proyecto','fechaI','fechaF','crono','persona','uminera','servicio','editar','documento','revisiones','estadoproyecto','leyenda','dias','personal'));
    }
    public function visualizarCambiosCronogramaRooster(Request $request){
        $fdes = $request->input('fdesde');
        $fhas = $request->input('fhasta');
        //dd(substr($fhas,6,4)."-".substr($fhas,3,2)."-".substr($fhas,0,2));
        if(substr($fdes,2,1)=='-' || substr($fdes,2,1)=='/'){
            $fdes = substr($fdes,6,4)."-".substr($fdes,3,2)."-".substr($fdes,0,2);
        }else{
            $fdes = substr($fdes,0,4)."-".substr($fdes,5,2)."-".substr($fdes,8,2);
        }
        if(substr($fhas,2,1)=='-' || substr($fhas,2,1)=='/'){
            $fhas = substr($fhas,6,4)."-".substr($fhas,3,2)."-".substr($fhas,0,2);
        }else{
            $fhas = substr($fhas,0,4)."-".substr($fhas,5,2)."-".substr($fhas,8,2);
        }
        
        $fdes = date('d-m-Y',strtotime($fdes));
        $fhas = date('d-m-Y',strtotime($fhas));
        
        //dd($request->input('fhasta'));
        $cproyecto = $request->input('cproyecto');
        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$cproyecto)
        ->first();
        $proyectocronograma = DB::table('tproyectocronograma')
        ->where('cproyecto','=',$cproyecto)
        ->first();

        /* * Detalle de Cronograma Rooster * */
        $crono = array();
        $tcronodeta = DB::table('tproyectocronograma as crono')
        ->join('tproyectocronogramaconstrucciondetalle as deta','crono.cproyectocronograma','=','deta.cproyectocronograma')
        ->join('tpersona as per','deta.cpersonaempleado','=','per.cpersona')
        ->leftJoin('tpersonadatosempleado as dat','deta.cpersonaempleado','=','dat.cpersona')
        ->leftJoin('tcargos as car','dat.ccargo','=','car.ccargo')
        ->where('crono.cproyecto','=',$proyecto->cproyecto)
        ->select('deta.*','per.nombre as empleado','dat.ccargo','car.descripcion as cargo')
        ->get();
        foreach($tcronodeta as $deta){
            $c['cproyectocronogramacons'] = $deta->cproyectocronogramacons;
            $c['cproyectocronograma'] = $deta->cproyectocronograma;
            $c['cpersonaempleado'] = $deta->cpersonaempleado;
            $c['croldespliegue'] = $deta->croldespliegue;
            $c['cproyectosub'] = $deta->cproyectosub;
            $c['empleado'] = $deta->empleado;
            $c['cargo'] = $deta->cargo;

            $tfechas = DB::table('tconstrucciondetafecha as cfec')
            ->where('cfec.cproyectocronogramacons','=',$deta->cproyectocronogramacons)
            ->select('cfec.*',DB::raw('to_char(cfec.fecha,\'YYYYMMDD\' ) as feje'))
            ->get();
            $c['deta'] = array();
            foreach($tfechas as $fec){
                $d['cconstrucciondetafecha']=$fec->cconstrucciondetafecha;
                $d['fecha'] = $fec->fecha;
                $d['codactividad'] = $fec->codactividad;
                $d['nrohoras'] = $fec->nrohoras;
                $d['observacion'] = $fec->observacion;
                $c['deta'][$fec->feje]=$d;
            }

            $crono[count($crono)]= $c;

        }
        /* Fin Detalle de Cronograma Rooster */


        $tleyendarooster = DB::table('tcatalogo')
        ->where('codtab','=','00014')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->get();     
        $leyenda = array();  
        foreach ($tleyendarooster as $tley){
            $leye['cod'] = $tley->digide;
            $leye['des'] = $tley->descripcion;
            $leye['color'] = $tley->valor;
            $leyenda[count($leyenda)] = $leye;
        }


        /*$fdesde = Carbon::createFromDate(2016,12,01);
        $fhasta = Carbon::createFromDate(2016,12,15);*/
        $fdesde = Carbon::parse($fdes);
        $fhasta = Carbon::parse($fhas);
        
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        $sfhasta = ($fhasta->year ."". str_pad($fhasta->month,2,'0',STR_PAD_LEFT) ."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT));
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);
            $sdia = ($dia->year ."". str_pad($dia->month,2,'0',STR_PAD_LEFT) ."". str_pad($dia->day,2,'0',STR_PAD_LEFT));
            if ( !( $sdia <= $sfhasta ) ) {
                
                $seguir=false;    
            }

            
        } 
        
        return view('partials.tableCronogramaRooster',compact('dias','personal','crono','leyenda'));

    }
    public function grabarCambiosCronogramaRooster(Request $request){
        $cproyecto = $request->input('cproyecto');
        $rooster = $request->input('rooster');
        foreach($rooster as $roos){
            
            foreach($roos[3] as $dKey => $dValor){ // dKey => 20170102 
                if(strlen($dValor[3])>0){
                    if(strlen($dValor[0]) >0 ){
                        $obj = Tconstrucciondetafecha::where('cconstrucciondetafecha','=',$dValor[0])->first();
                    }else{
                        $obj= new Tconstrucciondetafecha();
                        $obj->cproyectocronogramacons= $roos[1];
                        $obj->fecha = substr($dKey,0,4). "-". substr($dKey,4,2)."-".substr($dKey,6,2);

                    }
                    $obj->codactividad = $dValor[2];
                    $obj->nrohoras = $dValor[3];
                    $obj->observacion = $dValor[4];
                    $obj->save();
                }else{
                    // sino hay horas
                    if(strlen($dValor[0]) > 0 ){
                        // si existe objeto guardado anteriormente
                        $obj = Tconstrucciondetafecha::where('cconstrucciondetafecha','=',$dValor[0])->first();
                        $obj->codactividad = '';
                        $obj->nrohoras = 0;
                        $obj->observacion = '';
                        $obj->save();                        
                    }

                }
            }
        }

        return Redirect::route('verCambiosCronogramaRooster',compact('cproyecto'));
    }
    

    public function horasEjecutadasRooster(){

        return view('proyecto/controlHorasEjecutadas');
    }

    public function verHorasEjecutadasRooster(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();


        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = ['' => ''] + $servicio;

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();        
        $revision=""; 

        $fechaI=date('d-m-Y');
        $fechaF=date('d-m-Y');
        
        $editar=true;      
        $documento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','ROS')
        ->select('prydoc.cproyectodocumentos','prydoc.fechadoc','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first();          

        $objProSup = new ProyectoSupport();

        $revisiones = $objProSup->getRevisionesDocumentos($proyecto->cproyecto,'ROS','00013');    

        /*$tpersonal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->take(15)
        ->get();
        $personal = array();
        foreach($tpersonal as $per){
            $p['nombre'] = $per->nombre;
            $personal[count($personal)]=$p;
        } */

        $tleyendarooster = DB::table('tcatalogo')
        ->where('codtab','=','00014')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->get();     
        $leyenda = array();  
        foreach ($tleyendarooster as $tley){
            $leye['cod'] = $tley->digide;
            $leye['des'] = $tley->descripcion;
            $leye['color'] = $tley->valor;
            $leyenda[count($leyenda)] = $leye;
        }
        /*$fdesde = Carbon::createFromDate(2016,12,01);
        $fhasta = Carbon::createFromDate(2016,12,15);
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".$dia->month."". $dia->day) <= ($fhasta->year ."".$fhasta->month."". $fhasta->day) ) ) {
                $seguir=false;    
            }

            
        } */

        return view('proyecto/controlHorasEjecutadas',compact('proyecto','uminera','servicio','editar','documento','revisiones','estadoproyecto','leyenda'));
    }

    public function verControlHorasConstruccion(Request $request){
        $cproyecto = $request->input('cproyecto');
        $fdes = $request->input('fdesde');
        $fhas = $request->input('fhasta');
        //dd(substr($fhas,6,4)."-".substr($fhas,3,2)."-".substr($fhas,0,2));
        if(substr($fdes,2,1)=='-' || substr($fdes,2,1)=='/'){
            $fdes = substr($fdes,6,4)."-".substr($fdes,3,2)."-".substr($fdes,0,2);
        }else{
            $fdes = substr($fdes,0,4)."-".substr($fdes,5,2)."-".substr($fdes,8,2);
        }
        if(substr($fhas,2,1)=='-' || substr($fhas,2,1)=='/'){
            $fhas = substr($fhas,6,4)."-".substr($fhas,3,2)."-".substr($fhas,0,2);
        }else{
            $fhas = substr($fhas,0,4)."-".substr($fhas,5,2)."-".substr($fhas,8,2);
        }
        
        $fdes = date('d-m-Y',strtotime($fdes));
        $fhas = date('d-m-Y',strtotime($fhas));

        $fdesC = date('Ymd',strtotime($fdes));
        $fhasC = date('Ymd',strtotime($fhas));
        //dd($fdesC);
        $crono=array();

        /* Informacion a Mostrar */
        $crono = array();
        $tcronodeta = DB::table('tproyectocronograma as crono')
        ->join('tproyectocronogramaconstrucciondetalle as deta','crono.cproyectocronograma','=','deta.cproyectocronograma')
        ->join('tpersona as per','deta.cpersonaempleado','=','per.cpersona')
        ->leftJoin('tpersonadatosempleado as dat','deta.cpersonaempleado','=','dat.cpersona')
        ->leftJoin('tcargos as car','dat.ccargo','=','car.ccargo')
        ->where('crono.cproyecto','=',$cproyecto)

        ->select('deta.*','per.nombre as empleado','dat.ccargo','car.descripcion as cargo')
        ->get();
        foreach($tcronodeta as $deta){
            $c['cproyectocronogramacons'] = $deta->cproyectocronogramacons;
            $c['cproyectocronograma'] = $deta->cproyectocronograma;
            $c['cpersonaempleado'] = $deta->cpersonaempleado;
            $c['croldespliegue'] = $deta->croldespliegue;
            $c['cproyectosub'] = $deta->cproyectosub;
            $c['empleado'] = $deta->empleado;
            $c['cargo'] = $deta->cargo;

            $tfechas = DB::table('tconstrucciondetafecha as cfec')
            ->where('cfec.cproyectocronogramacons','=',$deta->cproyectocronogramacons)
            ->whereBetween(DB::raw('to_char(cfec.fecha,\'YYYYMMDD\' )'),[$fdesC,$fhasC])
            ->select('cfec.*',DB::raw('to_char(cfec.fecha,\'YYYYMMDD\' ) as feje'))
            ->get();
            $c['deta'] = array();
            foreach($tfechas as $fec){
                $d['cconstrucciondetafecha']=$fec->cconstrucciondetafecha;
                $d['fecha'] = $fec->fecha;
                $d['codactividad'] = $fec->codactividad;
                $d['nrohoras'] = $fec->nrohoras;
                $d['observacion'] = $fec->observacion;
                /* Datos de ejecucion */
                $d['cproyectoejecucionconstruccion']='';
                $d['cproyectocronoconstrdetalle']='';
                $d['cpersona_empleado']='';
                $d['fejecucion']='';
                $d['estado']='';
                $d['horasejecutadas']='';
                $d['horasaprobadas']='';
                $d['faprobacion']='';
                $d['observacion']='';
                $d['tareaconstruccion']='';
                $d['fplanificado']='';
                $d['eje']='0';   
                $d['feje'] = $fec->feje;
                /* Fin Datos ejecucion */             
                $c['deta'][$fec->feje]=$d;                
                
            }

            $ejecucion = DB::table('tproyectoejecucionconstruccion as eje')
            ->where('cproyectocronoconstrdetalle','=',$deta->cproyectocronogramacons)
            ->where('cpersona_empleado','=',$deta->cpersonaempleado)
            ->whereBetween(DB::raw('to_char(eje.fejecucion,\'YYYYMMDD\' )'),[$fdesC,$fhasC])                
            /*->where('fejecucion','=',$fec->fecha)*/
            ->select('eje.*',DB::raw('to_char(eje.fejecucion,\'YYYYMMDD\' ) as feje'))
            ->get();

            foreach($ejecucion as $eje){
                /* Datos Planificado*/
                $res = array_search($eje->feje , array_map(function($e){ return $e['feje']; },$c['deta']));
                
                if($res===false){
                    $d['cconstrucciondetafecha']='';
                    $d['fecha'] = '';
                    $d['codactividad'] = '';
                    $d['nrohoras'] = '';
                    $d['observacion'] = '';                
                }
                /* Fin datos plainificado */
                /*  Datos Ejecucion*/
                $d['eje']='1';
                $d['cproyectoejecucionconstruccion']=$eje->cproyectoejecucionconstruccion;
                $d['cproyectocronoconstrdetalle']=$eje->cproyectocronoconstrdetalle;
                $d['cpersona_empleado']=$eje->cpersona_empleado;
                $d['fejecucion']=$eje->fejecucion;
                $d['estado']=$eje->estado;
                $d['horasejecutadas']=$eje->horasejecutadas;
                $d['nrohoras'] = $eje->horasejecutadas;;
                $d['horasaprobadas']=$eje->horasaprobadas;
                $d['faprobacion']=$eje->faprobacion;
                $d['observacion']=$eje->observacion;
                $d['tareaconstruccion']=$eje->tareaconstruccion;
                $d['fplanificado']=$eje->fplanificado;
                $d['feje'] = $eje->feje;
                /* Fin Datos ejecucion */
                $c['deta'][$eje->feje]=$d;
            }

            $crono[count($crono)]= $c;

        }
        //dd($crono);
        /* Fin de informacion a mostrar */ 

        $tleyendarooster = DB::table('tcatalogo')
        ->where('codtab','=','00014')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->get();     
        $leyenda = array();  
        foreach ($tleyendarooster as $tley){
            $leye['cod'] = $tley->digide;
            $leye['des'] = $tley->descripcion;
            $leye['color'] = $tley->valor;
            $leyenda[count($leyenda)] = $leye;
        }

        $fdesde = Carbon::parse($fdes);
        $fhasta = Carbon::parse($fhas);
        
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        $sfhasta = ($fhasta->year ."". str_pad($fhasta->month,2,'0',STR_PAD_LEFT) ."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT));
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);
            $sdia = ($dia->year ."". str_pad($dia->month,2,'0',STR_PAD_LEFT) ."". str_pad($dia->day,2,'0',STR_PAD_LEFT));
            if ( !( $sdia <= $sfhasta ) ) {
                
                $seguir=false;    
            }

            
        }         

        return view('partials.tableControlHorasRooster',compact('crono','dias','leyenda'));
    }
    public function grabarHorasEjecutadasRooster(Request $request){
        $cproyecto = $request->input('cproyecto');
        $rooster = $request->input('rooster');
        //dd($rooster);
        foreach($rooster as $roos){
            
            foreach($roos[3] as $dKey => $dValor){ // dKey => 20170102 
                $chk = isset($dValor[5])==1?$dValor[5]:'';
                if(strlen($dValor[3])>0 && $chk=='1'){
                    if(strlen($dValor[6]) >0 ){
                        $obj = Tproyectoejecucionconstruccion::where('cproyectoejecucionconstruccion','=',$dValor[6])->first();
                    }else{
                        $obj= new Tproyectoejecucionconstruccion();
                        $obj->cproyectocronoconstrdetalle= $roos[1];
                        $obj->cpersona_empleado = $roos[0];
                        $obj->fejecucion = substr($dKey,0,4). "-". substr($dKey,4,2)."-".substr($dKey,6,2);
                        $obj->estado='1';

                    }
                    $obj->tareaconstruccion = $dValor[2];
                    $obj->horasejecutadas = $dValor[3];
                    $obj->observacion = $dValor[4];
                    $obj->save();
                }else{
                    // sino hay horas
                    if(strlen($dValor[6]) > 0 ){
                        // si existe objeto guardado anteriormente
                       $obj = Tproyectoejecucionconstruccion::where('cproyectoejecucionconstruccion','=',$dValor[6])->delete();
                    }

                }
            }
        }
        return view('proyecto/controlHorasEjecutadas');
    }

    public function validahorasRooster(){

        return view('proyecto/validacionHorasEjecutadas');
    }

    public function verValidahorasRooster(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();


        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = ['' => ''] + $servicio;

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();        
        $revision=""; 


        $editar=true;      
        $documento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','ROS')
        ->select('prydoc.cproyectodocumentos','prydoc.fechadoc','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first();          

        $objProSup = new ProyectoSupport();

        $documento = $objProSup->getRevisionesDocumentos($proyecto->cproyecto,'ROS','00013');  

        /*$tpersonal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->take(15)
        ->get();
        $personal = array();
        foreach($tpersonal as $per){
            $p['nombre'] = $per->nombre;
            $personal[count($personal)]=$p;
        } */

        $tleyendarooster = DB::table('tcatalogo')
        ->where('codtab','=','00014')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->get();     
        $leyenda = array();  
        foreach ($tleyendarooster as $tley){
            $leye['cod'] = $tley->digide;
            $leye['des'] = $tley->descripcion;
            $leye['color'] = $tley->valor;
            $leyenda[count($leyenda)] = $leye;
        }
        /*
        $fdesde = Carbon::createFromDate(2016,12,01);
        $fhasta = Carbon::createFromDate(2016,12,15);
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".$dia->month."". $dia->day) <= ($fhasta->year ."".$fhasta->month."". $fhasta->day) ) ) {
                $seguir=false;    
            }

            
        } */        
        return view('proyecto/validacionHorasEjecutadas',compact('proyecto','persona','uminera','servicio','editar','documento','revisiones','estadoproyecto','leyenda'));
    }
    public function verValidarHorasConstruccion(Request $request){
        //boton Visualizar de Validar Control de Hopras
        $cproyecto = $request->input('cproyecto');
        $fdes = $request->input('fdesde');
        $fhas = $request->input('fhasta');
        //dd(substr($fhas,6,4)."-".substr($fhas,3,2)."-".substr($fhas,0,2));
        if(substr($fdes,2,1)=='-' || substr($fdes,2,1)=='/'){
            $fdes = substr($fdes,6,4)."-".substr($fdes,3,2)."-".substr($fdes,0,2);
        }else{
            $fdes = substr($fdes,0,4)."-".substr($fdes,5,2)."-".substr($fdes,8,2);
        }
        if(substr($fhas,2,1)=='-' || substr($fhas,2,1)=='/'){
            $fhas = substr($fhas,6,4)."-".substr($fhas,3,2)."-".substr($fhas,0,2);
        }else{
            $fhas = substr($fhas,0,4)."-".substr($fhas,5,2)."-".substr($fhas,8,2);
        }
        
        $fdes = date('d-m-Y',strtotime($fdes));
        $fhas = date('d-m-Y',strtotime($fhas));

        $fdesC = date('Ymd',strtotime($fdes));
        $fhasC = date('Ymd',strtotime($fhas));
        $crono=array();

        /* Informacion a Mostrar */
        $crono = array();
        $tcronodeta = DB::table('tproyectocronograma as crono')
        ->join('tproyectocronogramaconstrucciondetalle as deta','crono.cproyectocronograma','=','deta.cproyectocronograma')
        ->join('tpersona as per','deta.cpersonaempleado','=','per.cpersona')
        ->leftJoin('tpersonadatosempleado as dat','deta.cpersonaempleado','=','dat.cpersona')
        ->leftJoin('tcargos as car','dat.ccargo','=','car.ccargo')
        ->where('crono.cproyecto','=',$cproyecto)
        ->select('deta.*','per.nombre as empleado','dat.ccargo','car.descripcion as cargo')
        ->get();
        foreach($tcronodeta as $deta){
            $c['cproyectocronogramacons'] = $deta->cproyectocronogramacons;
            $c['cproyectocronograma'] = $deta->cproyectocronograma;
            $c['cpersonaempleado'] = $deta->cpersonaempleado;
            $c['croldespliegue'] = $deta->croldespliegue;
            $c['cproyectosub'] = $deta->cproyectosub;
            $c['empleado'] = $deta->empleado;
            $c['cargo'] = $deta->cargo;

            $c['deta'] = array();


            $ejecucion = DB::table('tproyectoejecucionconstruccion as eje')
            ->where('cproyectocronoconstrdetalle','=',$deta->cproyectocronogramacons)
            ->where('cpersona_empleado','=',$deta->cpersonaempleado)
            ->whereBetween(DB::raw('to_char(eje.fejecucion,\'YYYYMMDD\' )'),[$fdesC,$fhasC])                
            /*->where('fejecucion','=',$fec->fecha)*/
            ->select('eje.*',DB::raw('to_char(eje.fejecucion,\'YYYYMMDD\' ) as feje'))
            ->get();

            foreach($ejecucion as $eje){
                /* Datos Planificado*/
                $res = array_search($eje->feje , array_map(function($e){ return $e['feje']; },$c['deta']));
                
                if($res===false){
                    $d['cconstrucciondetafecha']='';
                    $d['fecha'] = '';
                    $d['codactividad'] = '';
                    $d['nrohoras'] = '';
                    $d['observacion'] = '';                
                }
                /* Fin datos plainificado */
                /*  Datos Ejecucion*/
                $d['eje']='1';
                $d['cproyectoejecucionconstruccion']=$eje->cproyectoejecucionconstruccion;
                $d['cproyectocronoconstrdetalle']=$eje->cproyectocronoconstrdetalle;
                $d['cpersona_empleado']=$eje->cpersona_empleado;
                $d['fejecucion']=$eje->fejecucion;
                $d['estado']=$eje->estado;
                $d['horasejecutadas']=$eje->horasejecutadas;
                $d['nrohoras'] = $eje->horasejecutadas;;
                $d['horasaprobadas']=$eje->horasaprobadas;
                $d['faprobacion']=$eje->faprobacion;
                $d['observacion']=$eje->observacion;
                $d['tareaconstruccion']=$eje->tareaconstruccion;
                $d['fplanificado']=$eje->fplanificado;
                $d['feje'] = $eje->feje;
                /* Fin Datos ejecucion */
                $c['deta'][$eje->feje]=$d;
            }

            $crono[count($crono)]= $c;

        }

        /* Fin de informacion a mostrar */ 

        $tleyendarooster = DB::table('tcatalogo')
        ->where('codtab','=','00014')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->get();     
        $leyenda = array();  
        foreach ($tleyendarooster as $tley){
            $leye['cod'] = $tley->digide;
            $leye['des'] = $tley->descripcion;
            $leye['color'] = $tley->valor;
            $leyenda[count($leyenda)] = $leye;
        }

        $fdesde = Carbon::parse($fdes);
        $fhasta = Carbon::parse($fhas);
        
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        $sfhasta = ($fhasta->year ."". str_pad($fhasta->month,2,'0',STR_PAD_LEFT) ."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT));
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);
            $sdia = ($dia->year ."". str_pad($dia->month,2,'0',STR_PAD_LEFT) ."". str_pad($dia->day,2,'0',STR_PAD_LEFT));
            if ( !( $sdia <= $sfhasta ) ) {
                
                $seguir=false;    
            }

            
        }               
        return view('partials.tableValidacionHorasRooster',compact('crono','dias','leyenda'));
    }
    public function grabarValidahorasRooster(Request $request){
        $cproyecto = $request->input('cproyecto');
        $rooster = $request->input('rooster');
        //dd($rooster);
        foreach($rooster as $roos){
            
            foreach($roos[3] as $dKey => $dValor){ // dKey => 20170102 
                $chk = isset($dValor[5])==1?$dValor[5]:'';
                if(strlen($dValor[3])>0 && $chk=='1'){
                    if(strlen($dValor[6]) >0 ){
                        $obj = Tproyectoejecucionconstruccion::where('cproyectoejecucionconstruccion','=',$dValor[6])->first();
                    }else{
                        $obj= new Tproyectoejecucionconstruccion();
                        $obj->cproyectocronoconstrdetalle= $roos[1];
                        $obj->cpersona_empleado = $roos[0];
                        $obj->fejecucion = substr($dKey,0,4). "-". substr($dKey,4,2)."-".substr($dKey,6,2);
                        

                    }
                    $obj->estado='2';
                    $obj->tareaconstruccion = $dValor[2];
                    $obj->horasaprobadas = $dValor[3];
                    $obj->observacion = $dValor[4];
                    $obj->save();
                }else{
                    // sino hay horas
                    if(strlen($dValor[6]) > 0 ){
                        // si existe objeto guardado anteriormente
                       $obj = Tproyectoejecucionconstruccion::where('cproyectoejecucionconstruccion','=',$dValor[6])->delete();
                    }

                }
            }
        }
        return view('proyecto/validacionHorasEjecutadas');
    } 

    public function btnAddcheckEnt(Request $request){
            $checkEnt = array();
            if(Session::get('listproy')){
                $checkEnt = Session::get('listproy',array());
            }
            $i= -1*(count($checkEnt)+1)*10;
            $cproyectochecklist=$i;            
            $actividad = $request->input('cchecklist');
            $resultado = $request->input('resultado');
            $check = $request->input('CHECK');
            $pavance = $request->input('porcentajeavance');
            $observacion = $request->input('observacion');
            
            $tactividad = DB::table('tchecklist')  
            ->where('cchecklist','=',$actividad)
            ->first();
            
            $cen['cproyectochecklist'] = $cproyectochecklist;
            
            $cen['cchecklist'] = $actividad;
            $cen['des_check'] = $tactividad->descripcionactvividad;
            $cen['resultado'] = $resultado;
            $cen['CHECK'] = $check;
            $cen['porcentajeavance'] = $pavance;
            $cen['observacion'] = $observacion;


            $checkEnt[count($checkEnt)]=$cen;

            Session::put('listproy',$checkEnt);

            return view('partials.modalcheckListProyecto',compact('checkEnt'));
        
    }

    public function verCheckListProy($checkEnt){
        //Proyecto CheckList
        $estadoproyecto = testadoproyecto::lists('descripcion','cestadoproyecto')
        ->all();
        $faseproyecto = tfasesproyecto::lists('descripcion','cfaseproyecto')
        ->all();
        

        /* Fin CheckList */

        
    

        return view('partials.modalcheckListProyecto',compact('tproyectochecklist'));
    }

    public function editarChecklistProy(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();

        //$listproy = array();
        Session::forget('listproy');

        $testado = DB::table('testadoproyecto')
        ->lists('descripcion','cestadoproyecto');
        $testado = [''=>'']+ $testado; 

        $tfase = DB::table('tfasesproyecto')
        ->lists('descripcion','cfaseproyecto');
        $tfase = [''=>'']+ $tfase;

        //Proceso
        $tproceso = DB::table('tproyectochecklist as pcl')        
        ->leftJoin('tchecklist as cl','pcl.cchecklist','=','cl.cchecklist')
        ->leftJoin('tfasesproyecto as tf','cl.cfaseproyecto','=','tf.cfaseproyecto')
        ->where('cproyecto','=',$idProyecto)
        ->lists('tf.descripcion','tf.cfaseproyecto');
        /*->select('pcl.*','tf.descripcion')
        ->get();*/


        $tactividad = DB::table('tchecklist')
        ->lists('descripcionactvividad','cchecklist');
        $tactividad = [''=>'']+ $tactividad; 

         //combos
         
        $trol = DB::table('troldespliegue')
        ->lists('descripcionrol','croldespliegue');
        $trol = [''=>'']+ $trol; 
        //Fin combos
        
        $persona  = DB::table('tpersona')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();
        
        $uminera = DB::table('tunidadminera')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->first();
        
        $checkEnt=array();



        
        /* Consulta Check List */
        $tproyectochecklist = DB::table('tproyectochecklist as pcl')        
        ->leftJoin('tchecklist as tcl','pcl.cchecklist','=','tcl.cchecklist')
        ->where('cproyecto','=',$idProyecto)
        ->orderBy('tcl.descripcionactvividad','ASC')
        ->select('pcl.*','pcl.cproyectochecklist','pcl.cchecklist','tcl.descripcionactvividad as des_check')
        ->get();     
        
        foreach($tproyectochecklist as $pcl){
            
            $pc['cproyectochecklist']=$pcl->cproyectochecklist;
            // $pc['cfaseproyecto']=$pcl->cproyecto;
            $pc['des_check']=$pcl->des_check;
            $pc['cchecklist']=$pcl->cchecklist;
            $pc['resultado']=$pcl->resultado;
            $pc['CHECK']=$pcl->CHECK;
            $pc['porcentajeavance']=$pcl->porcentajeavance;
            $pc['observacion']=$pcl->observacion;            
            $checkEnt[count($checkEnt)]=$pc;
        }
        
        Session::put('listproy',$checkEnt);   
        
         
        return view('proyecto/checkListProyecto',compact('proyecto','persona','uminera','checkEnt','testado','tfase','tproceso','trol','tproyectochecklist','tactividad'));
    }
    public function grabarChecklistProy(Request $request){
            DB::beginTransaction();
            $cproyecto=$request->input('cproyecto');
            $listproy=Session::get('listproy');

            foreach($listproy as $pcl){
                if($pcl['cproyectochecklist'] <= 0){
                    $newpcl = new Tproyectochecklist();
                    $newpcl->cproyecto=$cproyecto;
                    
                }else{
                    $newpcl = Tproyectochecklist::where('cproyectochecklist','=',$pcl['cproyectochecklist'])->first();
                }
                    //$newpcl->cproyecto = $cproyecto;
                    $newpcl->cchecklist=$pcl['cchecklist'];
                    $newpcl->resultado=$pcl['resultado'];
                    $newpcl->CHECK=$pcl['CHECK'];
                    $newpcl->porcentajeavance=$pcl['porcentajeavance'];
                    $newpcl->observacion=$pcl['observacion'];
                    $newpcl->save();   
            }   

            $checkligrabar=array();
            if(Session::get('saveproy')){
                $checkligrabar = Session::get('saveproy');
            }
            foreach($checkligrabar as $c){
                $delCheck = Tproyectochecklist::find($c);
                $delCheck->delete();
            }


            DB::commit();
            return Redirect::route('editarChecklistProy',$cproyecto);
    }  

    

       

    public function checklistProy(){
        return view('proyecto/checkListProyecto');
    }


    public function listarleccionesAprendidas(){
        $listaLaCab=array();
        $listaLaPry=DB::table('tleccionesaprendidas as tle')
        ->Join('tproyecto as pry','pry.cproyecto','=','tle.cproyecto')
        ->distinct('pry.codigo')
        ->orderBy('pry.codigo','ASC')
        ->select('pry.codigo','pry.nombre','pry.cproyecto')
        ->get();

        $listaLa=DB::table('tleccionesaprendidas as tle')
        ->Join('tareas as ta','tle.carea','=','ta.carea')
        ->Join('tfasesproyecto as tfp','tle.cfaseproyecto','=','tfp.cfaseproyecto')
        // ->Join('tareasconocimiento as tac','tle.careaconocimiento','=','tac.careaconocimiento')
        ->Join('tpersona as tp','tle.cpersona_elaborado','=','tp.cpersona')
        ->Join('tproyecto as pry','pry.cproyecto','=','tle.cproyecto')
        ->Join('tpersona as gp','pry.cpersona_gerente','=','gp.cpersona')
        ->Join('tunidadminera as um','pry.cunidadminera','=','um.cunidadminera')
        // ->Join('tproyectopersona as pyper','tp.cpersona','=','pyper.cpersona')
        // ->Join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        // ->leftJoin('tpersonanaturalinformacionbasica as tpni','tp.cpersona','=','tpni.cpersona')
        // ->leftJoin('troldespliegue as tr','tle.crol_elaborado','=','tr.croldespliegue')
        // ->where('tle.cproyecto','=',$idProyecto)
        // ->where('pry.cproyecto','=',$value->cproyecto)
        ->orderBy('tle.cleccionaprendida','ASC')
        ->select('tle.cleccionaprendida','tle.item','tle.descripcion','tle.tag','tle.fecha','tle.carea','ta.descripcion as area','tle.cproyecto','tle.cfaseproyecto','tfp.descripcion as fase','tle.careaconocimiento','tle.suceso','tle.hizo','tle.debiohacer','tle.cpersona_elaborado','tp.nombre as elaborado','tle.crol_elaborado','tle.cpersona_aprobado','tp.nombre as aprobado','tp.cpersona','tp.abreviatura','pry.codigo','pry.nombre','gp.abreviatura as gerente','um.nombre as uindadminera',DB::raw("to_char(tle.fecha,'DD-MM-YY') as fec"))
        ->get();

        // foreach ($listaLaPry as $value) {

        // $cabLA['codigo']=$value->codigo;
        // $cabLA['nombre']=$value->nombre;
            
        // $listaLa=DB::table('tleccionesaprendidas as tle')
        // ->Join('tareas as ta','tle.carea','=','ta.carea')
        // ->Join('tfasesproyecto as tfp','tle.cfaseproyecto','=','tfp.cfaseproyecto')
        // ->Join('tareasconocimiento as tac','tle.careaconocimiento','=','tac.careaconocimiento')
        // ->Join('tpersona as tp','tle.cpersona_elaborado','=','tp.cpersona')
        // ->Join('tproyecto as pry','pry.cproyecto','=','tle.cproyecto')
        // // ->Join('tproyectopersona as pyper','tp.cpersona','=','pyper.cpersona')
        // // ->Join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        // // ->leftJoin('tpersonanaturalinformacionbasica as tpni','tp.cpersona','=','tpni.cpersona')
        // // ->leftJoin('troldespliegue as tr','tle.crol_elaborado','=','tr.croldespliegue')
        // // ->where('tle.cproyecto','=',$idProyecto)
        // ->where('pry.cproyecto','=',$value->cproyecto)
        // ->orderBy('tle.cleccionaprendida','ASC')
        // ->select('tle.cleccionaprendida','tle.item','tle.descripcion','tle.tag','tle.fecha','tle.carea','ta.descripcion as area','tle.cproyecto','tle.cfaseproyecto','tfp.descripcion as fase','tle.careaconocimiento','tac.descripcion as areaconocimiento','tle.suceso','tle.hizo','tle.debiohacer','tle.cpersona_elaborado','tp.nombre as elaborado','tle.crol_elaborado','tle.cpersona_aprobado','tp.nombre as aprobado','tp.cpersona','tp.abreviatura','pry.codigo','pry.nombre')
        // ->get();

        //     $detalleLA=array();
        //     foreach ($listaLa as $ladetalle) {
        //         $d['cleccionaprendida']=$ladetalle->cleccionaprendida;
        //         $d['descripcion']=$ladetalle->descripcion;
        //         $d['fecha']=$ladetalle->fecha;
        //         $d['area']=$ladetalle->area;
        //         $d['fase']=$ladetalle->fase;
        //         $d['areaconocimiento']=$ladetalle->areaconocimiento;
        //         $d['suceso']=$ladetalle->suceso;
        //         $d['hizo']=$ladetalle->hizo;
        //         $d['debiohacer']=$ladetalle->debiohacer;
        //         $d['abreviatura']=$ladetalle->abreviatura;
        //         $detalleLA[count($detalleLA)]=$d;
        //     }

        //     $cabLA['detalle']=$detalleLA;
        //     $listaLaCab[count($listaLaCab)]=$cabLA;
        // }


        // dd($listaLa);
        return view('proyecto/listarleccionesAprendidas',compact('listaLa','listaLaPry','listaLaCab'));
        
    }

    // public function listarLA(){
        
    //     return Datatables::queryBuilder(DB::table('tleccionesaprendidas as tle')
    //     ->Join('tareas as ta','tle.carea','=','ta.carea')
    //     ->Join('tfasesproyecto as tfp','tle.cfaseproyecto','=','tfp.cfaseproyecto')
    //     ->Join('tareasconocimiento as tac','tle.careaconocimiento','=','tac.careaconocimiento')
    //     ->Join('tpersona as tp','tle.cpersona_elaborado','=','tp.cpersona')
    //     ->Join('tproyecto as pry','pry.cproyecto','=','tle.cproyecto')
    //     // ->Join('tproyectopersona as pyper','tp.cpersona','=','pyper.cpersona')
    //     // ->Join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
    //     // ->leftJoin('tpersonanaturalinformacionbasica as tpni','tp.cpersona','=','tpni.cpersona')
    //     // ->leftJoin('troldespliegue as tr','tle.crol_elaborado','=','tr.croldespliegue')
    //     // ->where('tle.cproyecto','=',$idProyecto)
    //     // ->where('tpni.esempleado','=','1')
    //     ->orderBy('tle.cleccionaprendida','ASC')
    //     ->select('tle.cleccionaprendida','tle.item','tle.descripcion','tle.tag','tle.fecha','tle.carea','ta.descripcion as area','tle.cproyecto','tle.cfaseproyecto','tfp.descripcion as fase','tle.careaconocimiento','tac.descripcion as areaconocimiento','tle.suceso','tle.hizo','tle.debiohacer','tle.cpersona_elaborado','tp.nombre as elaborado','tle.crol_elaborado','tle.cpersona_aprobado','tp.nombre as aprobado','tp.cpersona','tp.abreviatura'
    //         ,DB::raw('CONCAT(\'row_\',tle.cleccionaprendida)  as "DT_RowId"')))->make(true); 
    //     // ->get();
    // }

    public function leccionesAprendidas(){
        $bsave=false;

        return view('proyecto/leccionesAprendidas',compact('bsave'));
    }
    public function editarLeccionesAprendidas(Request $request,$idProyecto){


        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$idProyecto)->first();

        $bsave=true;
        //$listLecApre=array();
        Session::forget('listLecApre');  
        Session::forget('delLecAprend');            

        $persona  = DB::table('tpersona')
        ->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $uminera = DB::table('tunidadminera')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first(); 


        $tdocumento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','LE')
        ->select('prydoc.cproyectodocumentos','prydoc.fechadoc','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first();               
             
        // $rol_elaborado = DB::table('troldespliegue')
        // ->orderBy('descripcionrol','ASC')
        // ->lists('descripcionrol','croldespliegue');        
        // $rol_elaborado = [''=>''] + $rol_elaborado;

        // $tarea = DB::table('tareas as a')
        // ->lists('descripcion','carea');
        // $tarea = [''=>''] + $tarea;
        // dd($personala);
        // $tarea = $personala->area;
        // $rol_elaborado = $personala->categoria;
        // $persona_elaborado = $personala->nombre;

        // $persona_elaborado =DB::table('tpersona as tp')
        // ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        // ->where('tpi.esempleado','=','1')
        // ->orderBy('tp.nombre','ASC')
        // ->lists('tp.nombre','tp.cpersona');
        // $persona_elaborado = [''=>''] + $persona_elaborado;

        // $persona_aprobado =DB::table('tpersona as tp')
        // ->leftJoin('tpersonanaturalinformacionbasica as tpi','tp.cpersona','=','tpi.cpersona')
        // ->where('tpi.esempleado','=','1')
        // ->orderBy('tp.nombre','ASC')
        // ->lists('tp.nombre','tp.cpersona');
        // $persona_aprobado = [''=>''] + $persona_aprobado;  

        $tareaconocimiento = DB::table('tareasconocimiento')
        ->lists('descripcion','careaconocimiento');
        $tareaconocimiento = [''=>''] + $tareaconocimiento;

        $tfaseproyecto = DB::table('tfasesproyecto')
        ->orderBy('codigo','ASC')
        ->lists('descripcion','cfaseproyecto');
        $tfaseproyecto = [''=>''] + $tfaseproyecto;

        $testadoproyecto = DB::table('testadoproyecto')
        ->lists('descripcion','cestadoproyecto');
       
        $lecaprend=array();

        $tleccionesaprendidas=DB::table('tleccionesaprendidas as tle')
        ->leftJoin('tareas as ta','tle.carea','=','ta.carea')
        ->leftJoin('tfasesproyecto as tfp','tle.cfaseproyecto','=','tfp.cfaseproyecto')
        ->leftJoin('tareasconocimiento as tac','tle.careaconocimiento','=','tac.careaconocimiento')
        ->leftJoin('tpersona as tp','tle.cpersona_elaborado','=','tp.cpersona')
        // ->Join('tproyectopersona as pyper','tp.cpersona','=','pyper.cpersona')
        // ->Join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        ->leftJoin('tpersonanaturalinformacionbasica as tpni','tp.cpersona','=','tpni.cpersona')
        ->leftJoin('troldespliegue as tr','tle.crol_elaborado','=','tr.croldespliegue')
        ->where('tle.cproyecto','=',$idProyecto)
        ->where('tpni.esempleado','=','1')
        ->orderBy('tle.cleccionaprendida','ASC')
        ->select('tle.cleccionaprendida','tle.item','tle.descripcion','tle.tag','tle.fecha','tle.carea','ta.descripcion as area','tle.cproyecto','tle.cfaseproyecto','tfp.descripcion as fase','tle.careaconocimiento','tac.descripcion as areaconocimiento','tle.suceso','tle.hizo','tle.debiohacer','tle.cpersona_elaborado','tp.nombre as elaborado','tle.crol_elaborado','tle.cpersona_aprobado','tp.nombre as aprobado','tr.descripcionrol','tp.cpersona','tp.abreviatura',DB::raw("to_char(tle.fecha,'DD-MM-YY') as fec"))
        ->get();

        $i=0;

        foreach($tleccionesaprendidas as $tlec){
            $i++;

            $personala = DB::table('tpersona as per ')
        ->join('tpersonadatosempleado as tper','per.cpersona','=','tper.cpersona')
        ->join('tareas as a','tper.carea','=','a.carea')
        ->join('tproyectopersona as pyper','per.cpersona','=','pyper.cpersona')
        ->join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        // ->select('a.descripcion as area','per.abreviatura as nombre','cat.descripcion as categoria')
        ->where('tper.cpersona','=',$tlec->cpersona)
        ->first();

            $lea['cleccionaprendida']= $tlec->cleccionaprendida;
            $lea['item']= $i;
            $lea['tag']= $tlec->tag;
            $lea['descripcion']= $tlec->descripcion;
            $lea['fecha']= $tlec->fec;
            $lea['area']= $tlec->area;
            $lea['carea']= $tlec->carea;
            //$lea['cproyecto']=$tlec->cproyecto;
            $lea['cfaseproyecto']= $tlec->cfaseproyecto;
            $lea['fase']= $tlec->fase;
            $lea['careaconocimiento']= $tlec->careaconocimiento;
            $lea['areaconocimiento']= $tlec->areaconocimiento;
            $lea['suceso']= $tlec->suceso;
            $lea['hizo']= $tlec->hizo;
            $lea['debiohacer']= $tlec->debiohacer;
            $lea['cpersona_elaborado']= $tlec->cpersona_elaborado;
            $lea['elaborado']= $tlec->abreviatura;
            $lea['crol_elaborado']= $tlec->crol_elaborado;
            // $lea['descripcionrol']= $tlec->descripcionrol;
            $lea['descripcionrol']= $personala->descripcion;
            $lea['cpersona_aprobado']= $tlec->cpersona_aprobado;
            $lea['aprobado']= $tlec->aprobado;
            $lecaprend[count($lecaprend)]=$lea;

        }
        Session::put('listLecApre',$lecaprend);


        return view('proyecto/leccionesAprendidas',compact('proyecto','persona','uminera','tarea','tareaconocimiento','tfaseproyecto','testadoproyecto','lecaprend','tdocumento','revision','rol_elaborado','persona_elaborado','persona_aprobado','tleccionesaprendidas','bsave'));
    }
    public function grabarLeccionesAprendidas(Request $request){
        DB::beginTransaction();

        $cproyecto = $request->input('cproyecto');
        $listLecApre=Session::get('listLecApre');

        $user = Auth::user()->cpersona;
        
        $personala = DB::table('tpersona as per ')
        ->join('tpersonadatosempleado as tper','per.cpersona','=','tper.cpersona')
        ->join('tareas as a','tper.carea','=','a.carea')
        ->join('tproyectopersona as pyper','per.cpersona','=','pyper.cpersona')
        ->join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        // ->select('a.descripcion as area','per.abreviatura as nombre','cat.descripcion as categoria')
        ->where('tper.cpersona','=',$user)
        ->first();

        foreach($listLecApre as $leap){
            if($leap['cleccionaprendida']<=0){
                $newleap = new Tleccionesaprendida();
                $newleap->cproyecto=$cproyecto;
                $newleap->carea=$personala->carea;
                //$newleap->cleccionaprendida=$leap['cleccionaprendida'];
                $newleap->cpersona_elaborado=$personala->cpersona;
                $newleap->crol_elaborado= $personala->ccategoriaprofesional;
                
            }
            else{
                $newleap=Tleccionesaprendida::where('cleccionaprendida','=',$leap['cleccionaprendida'])
                ->first();
            }

                $newleap->item=$leap['item'];
                $newleap->tag=$leap['tag'];
                $newleap->descripcion=$leap['descripcion'];
                $newleap->fecha=$leap['fecha'];
                $newleap->cfaseproyecto=$leap['cfaseproyecto'];
                $newleap->careaconocimiento=$leap['careaconocimiento'];
                $newleap->suceso=$leap['suceso'];
                $newleap->hizo=$leap['hizo'];
                $newleap->debiohacer=$leap['debiohacer'];
                // $newleap->cpersona_aprobado=$leap['cpersona_aprobado'];
                $newleap->save();

        }

        $delLeccionesAprendidas=array();
        if(Session::get('delLecAprend')){
            $delLeccionesAprendidas = Session::get('delLecAprend');
        }
        foreach($delLeccionesAprendidas as $d){
            $delLeA= Tleccionesaprendida::find($d);
            $delLeA->delete();
        }   

        DB::commit();
        return Redirect::route('editarLeccionesAprendidas',$cproyecto);
    }  

   

    public function addLeccApren(Request $request){
      

        $cproyecto = $request->input('cproyecto');
        $cleccionaprendida=$request->input('cleccionaprendida');
        
        DB::beginTransaction();



        $user = Auth::user()->cpersona;
        
        $personala = DB::table('tpersona as per ')
        ->join('tpersonadatosempleado as tper','per.cpersona','=','tper.cpersona')
        ->join('tareas as a','tper.carea','=','a.carea')
        ->join('tproyectopersona as pyper','per.cpersona','=','pyper.cpersona')
        ->join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        // ->select('a.descripcion as area','per.abreviatura as nombre','cat.descripcion as categoria')
        ->where('tper.cpersona','=',$user)
        ->where('pyper.cproyecto','=',$cproyecto)
        ->first();

        if (strlen($request->input('cleccionaprendida'))<=0) {

            $newleap = new Tleccionesaprendida();
            $newleap->cproyecto=$cproyecto;
            $newleap->carea=$personala->carea;
            $newleap->cpersona_elaborado=$personala->cpersona;
            $newleap->crol_elaborado= $personala->ccategoriaprofesional;       
            
        }

        else
        {
            $newleap=Tleccionesaprendida::where('cleccionaprendida','=',$cleccionaprendida)->first();
        }
       
        //$newleap->item=$request->input('cleccionaprendida');
        //$newleap->tag=$request->input('cleccionaprendida');
        $newleap->descripcion=$request->input('descripcion');
        $newleap->fecha=$request->input('fecha');
        $newleap->cfaseproyecto=$request->input('cfaseproyecto');
        //$newleap->careaconocimiento=$request->input('cleccionaprendida');
        $newleap->suceso=$request->input('suceso');
        $newleap->hizo=$request->input('hizo');
        $newleap->debiohacer=$request->input('debiohacer');
        $newleap->save();         

        DB::commit();

        return Redirect::route('editarLeccionesAprendidas',$cproyecto);
    }



    public function editLecAprend($idLeccionesAprendidas){
        // dd($idLeccionesAprendidas);

        $tfaseproyecto = DB::table('tfasesproyecto')
        ->orderBy('codigo','ASC')
        ->lists('descripcion','cfaseproyecto');
        $tfaseproyecto = [''=>''] + $tfaseproyecto;

        $ediLA = DB::table('tleccionesaprendidas')
        ->where('cleccionaprendida','=',$idLeccionesAprendidas)
        ->select('*',DB::raw("to_char(fecha,'DD-MM-YY') as fecha"))
        ->first();

        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$ediLA->cproyecto)
        ->first();

        return view('partials.addLeccionesAprendidas',compact('ediLA','tfaseproyecto','proyecto'));

    }

    public function delLecAprend($idLeccionesAprendidas){       

         DB::beginTransaction();
        $lecaprend = Tleccionesaprendida::where('cleccionaprendida','=',$idLeccionesAprendidas)->first();
        $cproyecto = $lecaprend->cproyecto;
        try
        {
            $lecaprend->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('editarLeccionesAprendidas',$cproyecto);
    }

    public function editarContactosUmineraProyecto(Request $request,$idProyecto){

        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$idProyecto)
        ->first();
  
        $persona  = DB::table('tpersona')
        ->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $uminera = DB::table('tunidadminera')
        ->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first(); 

        $testadoproyecto = DB::table('testadoproyecto')
        ->lists('descripcion','cestadoproyecto');  

        $tunidadmineracontactos=DB::table('tunidadmineracontactos as tu')
       ->leftJoin('tunidadminera as um','tu.cunidadminera','=','um.cunidadminera')
       ->leftjoin('tproyecto as tp','um.cunidadminera','=','tp.cunidadminera')
       ->leftJoin('ttiposcontacto as tc','tu.ctipocontacto','=','tc.ctipocontacto')
       ->leftJoin('tcontactocargo as cc','tu.ccontactocargo','=','cc.ccontactocargo')
       ->where('tp.cproyecto','=',$idProyecto)
       ->select('tu.*','um.nombre as uminera','tc.descripcion as tipo','cc.descripcion as cargo')
       ->get();

       $tumcon=array();
        
        foreach($tunidadmineracontactos as $tuc){
             $tc['cunidadmineracontacto']= $tuc->cunidadmineracontacto;
             $tc['uminera']= $tuc->uminera;
             $tc['cunidadminera']= $tuc->cunidadminera;
             $tc['apaterno']= $tuc->apaterno;
             $tc['amaterno']= $tuc->amaterno;
             $tc['nombres']= $tuc->nombres;
             $tc['ctipocontacto']= $tuc->ctipocontacto;
             $tc['tipo']= $tuc->tipo;
             $tc['ccontactocargo']= $tuc->ccontactocargo;
             $tc['cargo']= $tuc->cargo;
             $tc['email']= $tuc->email;
             $tc['telefono']= $tuc->telefono;
             $tc['anexo']= $tuc->anexo;
             $tc['cel1']= $tuc->cel1;
             $tc['cel2']= $tuc->cel2;
             $tumcon[count($tumcon)]=$tc;
        }               
 
        return view('proyecto/registroContactosProyecto',compact('proyecto','persona','uminera','testadoproyecto','tunidadmineracontactos'));
    }
  
    /*  Fin Planificación area de tareas administrativas*/
    public function planificacionTareaAdm(){
        $semana_act = date("W");
        $semana_ini = date("W");
        $semana_fin = date("W");
        return view('proyecto/planificacionAreasTareasAdm',compact('semana_act','semana_ini','semana_fin'));
    }

    public function viewPlanificacionAreaAdm(Request $request){
        $semana_act = date("W");
        $semana_ini = date("W");
        $semana_fin = date("W");
        $tactividadesAdm=DB::table('tactividad as act')
        ->where('act.tipoactividad','=','00002')
        ->whereNotNull('cactividad_parent')
        ->orderBy('codigo','ASC')
        ->get();
        
        $actividadesAdm=array();
        foreach($tactividadesAdm as $t){
            $p=array();
            $p['cactividad']=$t->cactividad;
            $p['codigo']=$t->codigo;
            $p['descripcion']=$t->descripcion;
            $actividadesAdm[count($actividadesAdm)]=$p;
        }
        //dd($actividadesAdm);
        return view('partials.tableActividadesAdministrativa',compact('actividadesAdm','semana_act','semana_ini','semana_fin'));
    }
    public function verHorasAcumuladasParticipanteAdm(Request $request){
        $user = Auth::user();
        $semana_act = date("W");
        $semana_ini = date("W");
        $semana_fin = date("W");        
        $tarea = DB::table('tpersonadatosempleado as e')
        ->join('tareas as a','a.carea','=','e.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent =$tarea->carea_parent;
        }
        $fdesde=$request->input('fdesde');
        
        $fec=$fdesde;
        $fecha = $fec;
        if(substr($fec,2,1)=='/' || substr($fec,2,1)=='-'){    
            $fecha = Carbon::create(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2),0,0,0);

        }else{
            $fecha = Carbon::create(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2),0,0,0);
        }
        $fecha1 = Carbon::createFromDate($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $fecha2 = Carbon::createFromDate($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $semana = $fecha->weekOfYear; // se obtiene la semana del aÃ±o que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.
        $semana_ini = $fecha1->weekOfYear;
        $semana_fin = $fecha2->weekOfYear;
        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','cab.cpersona_ejecuta')
        ->join('tpersona as p','cab.cpersona_ejecuta','=','p.cpersona')
        ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('emp.estado','=','ACT')
        ->where('emp.carea','=',$carea)
        ->orWhere('emp.carea','=',$carea_parent);

        $planiAcu = array();
        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2','3'])
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
        $tactividadesk=$tactividadesk->select('cab.cpersona_ejecuta','p.nombre','p.abreviatura')
        ->distinct()
        ->orderBy('p.abreviatura')
        ->get();
        foreach($tactividadesk as $tab){
            $pla=array();
            $pla['cpersona_ejecuta']=$tab->cpersona_ejecuta;
            $pla['nombre']=$tab->abreviatura;
            $actividadesp = DB::table('tproyectoejecucioncab as cab')
            ->join('tpersonadatosempleado as emp','emp.cpersona','=','cab.cpersona_ejecuta')        
            ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
            /*->where('emp.carea','=',$carea)*/
            ->where('cab.cpersona_ejecuta','=',$pla['cpersona_ejecuta']);

            $actividadesp = $actividadesp->whereIn('eje.estado',[1,2,3,4,5])
            ->whereIn('cab.tipo',['1','2','3'])
            ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));

            $actividadesp=$actividadesp->select(DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'),DB::raw('SUM(eje.horasplanificadas) as horas'))
            ->groupBy('fpla')
            ->get();
            foreach($actividadesp as $act){

                $pla[$act->fpla]['horas'] =$act->horas;
                
                
            }

            $planiAcu[count($planiAcu)]=$pla;
        }


        
       

        //dd($planificaciones);
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        $fdesde=$fec; //$request->input('fdesde');
        $fhasta=$fec; //$request->input('fhasta');
        $fdesde = Carbon::createFromDate(substr($fdesde,6,4),substr($fdesde,3,2),substr($fdesde,0,2));
        $fhasta = Carbon::createFromDate(substr($fhasta,6,4),substr($fhasta,3,2),substr($fhasta,0,2));
        $fdesde = $fdesde->startOfWeek(); // la fecha de inicio de semana
        $fhasta = $fhasta->endOfWeek();     // Fecha fin de la semana.        
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek],$dia->weekOfYear,$dia->dayOfWeek);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT)."". str_pad($dia->day,2,'0',STR_PAD_LEFT) ) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT)."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT)) ) ) {
                $seguir=false;    
            }      

        } 
       // $participantes= $request->

        return view('partials.planificacionPorAreasParticipantes',compact('dias','planiAcu','semana_ini','semana_fin'));

    }

    public function viewSemanaAdm(Request $request){
        $colaborador=array();
        $cactividad=$request->input('cactividad');
        $fecha = $request->input('fdesde');
        $dia_act=date('Ymd');
        if(substr($fecha,2,1)=='/'){    
            $fecha = Carbon::create(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2),0,0,0);

        }else{
            $fecha = Carbon::create(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2),0,0,0);
        }
        $fecha1 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $fecha2 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.
        $semana_ini = $fecha1->weekOfYear;
        $semana_fin = $fecha2->weekOfYear;   

        $cpersona = Auth::user()->cpersona;

        $tdatosemp = DB::table('tpersonadatosempleado as emp')
        ->join('tcargos as car','car.ccargo','=','emp.ccargo')
        ->leftJoin('tareas as ta','emp.carea','=','ta.carea')
        ->where('emp.cpersona','=',$cpersona)
        ->whereNull('emp.ftermino')
        ->where('car.esjefaturaarea','=','1')
        ->first(); 


        //Inicio consultas de area, area padre y codigo de area padre 
        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->leftjoin('tareas as tap','ta.carea_parent','=','tap.carea')
        ->where('cpersona','=',$cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->select('e.cpersona','e.carea', 'e.ccargo','ta.carea','ta.descripcion','tap.carea as cpadre','tap.codigo as codigopadre')
        ->first();
      
        $carea_parent=0;         
        if($tarea){          
            $carea_parent=$tarea->cpadre;        
        }  

        $personagte=DB::table('tareas as ta')
        ->where('ta.carea','=',$carea_parent)
        ->where('ta.codigo','ilike','%'.'GT'.'%')
        ->first();

        $codigoGT=0;

        if($personagte){
        $codigoGT=$personagte->codigo;
        }
        //Fin de area, area padre y codigo de area padre 

        if($tdatosemp){
            if($tdatosemp->carea > 0){
                $tcola = DB::table('tpersona as per')
                ->join('tpersonadatosempleado as emp','per.cpersona','=','emp.cpersona')
                ->leftjoin('tareas as ta','emp.carea','=','ta.carea')
                ->leftJoin('tcategoriaprofesional as tcp','emp.ccategoriaprofesional','=','tcp.ccategoriaprofesional')
                ->whereNull('emp.ftermino')
                ->where('emp.carea','=',$tdatosemp->carea)
                ->where('emp.estado','=','ACT');
                if(substr($codigoGT,0,2)=='GT'){
                    $tcola=$tcola->orWhere('emp.carea','=',$carea_parent);
                }
                $tcola=$tcola->orderBy('tcp.orden','ASC')
                ->select('per.*')
                ->get();
             
                foreach($tcola as $col){
                    $c=array();
                    $c['cpersona']=$col->cpersona;
                    $c['nombre']= $col->abreviatura;
                    $c['cproyectoejecucioncab']='';
                    $tejecucion =DB::table('tproyectoejecucioncab as cab')
                    ->join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
                    ->where('cab.cactividad','=',$cactividad)
                    ->where('cab.cpersona_ejecuta','=',$col->cpersona)
                    ->whereIn('eje.estado',[1,2,3,4,5])
                    ->whereIn('cab.tipo',['3'])
                    ->whereBetween('eje.fplanificado',array($fecha1,$fecha2))
                    ->select('eje.cproyectoejecucion','eje.horasplanificadas','eje.horasejecutadas',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'),DB::raw('to_char(eje.fplanificado,\'YYYY-MM-DD\' ) as fec'))
                    ->addSelect('eje.obsplanificada','eje.estado','eje.cproyectoejecucioncab')
                    ->orderBy('fpla','ASC')
                    ->get();
        
                    foreach($tejecucion as $act){
                        $c['cproyectoejecucioncab']=$act->cproyectoejecucioncab;
                        $c[$act->fpla]['cproyectoejecucion']= $act->cproyectoejecucion;
                        $c[$act->fpla]['estado']=$act->estado;
                        $c[$act->fpla]['observacion']=$act->obsplanificada;        
                        $c[$act->fpla]['horas'] =$act->horasplanificadas;
                        $c[$act->fpla]['horas_eje']= $act->horasejecutadas;
                        
                    }


                    $colaborador[count($colaborador)]=$c;
                }
            }
        }

        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];

        $fdesde = Carbon::createFromDate($fecha->year,$fecha->month,$fecha->day);
        $fhasta = Carbon::createFromDate($fecha->year,$fecha->month,$fecha->day);
        $fdesde = $fdesde->startOfWeek(); // la fecha de inicio de semana
        $fhasta = $fhasta->endOfWeek();     // Fecha fin de la semana.        
        $seguir = true;
        $dias= array();
        $dia = $fdesde; 
        while($seguir){
            
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek]);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT)."". str_pad($dia->day,2,'0',STR_PAD_LEFT) ) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT)."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT)) ) ) {
                $seguir=false;    
            }

            
        }         
        return view('partials.tableSemanaAdministrativa',compact('colaborador','dias','cactividad','semana_ini','semana_fin','dia_act'));
    }
    public function grabarPlanificacionAreaAadm(Request $request){
        $cactividad = $request->input('view_cactividad');
        $plaadm = $request->input('plaadm');
        foreach($plaadm as $adm){
            $grabar=false;
            /* validar linea*/
            foreach($adm as $k => $v){
                if(strlen($k)>=6){
                    if(strlen($v[2]) > 0){
                        $grabar=true;
                        break;
                    }
                }
            }
            if(!$grabar){
                continue;
            }
            /* fin validar linea */
            if(strlen($adm[0])>0){
                $cab = Tproyectoejecucioncab::where('cproyectoejecucioncab','=',$adm[0])->first();
            }else{
                $cab = new  Tproyectoejecucioncab();
                $cab->cactividad= $cactividad;
                $cab->estado = '1';
                $cab->tipo='3';
                $cab->fregistro=date('Y-m-d');
                $cab->cpersona_ejecuta=$adm[1];
                $cab->save();
            }
            foreach($adm as $k => $v){
                if(strlen($k)>=6){
                    if( strlen($v[1])>0 ){ //cproyectoejecucion
                        $obj = Tproyectoejecucion::where('cproyectoejecucion','=',$v[1])->first();
                        if(strlen($v[2]) <= 0){
                            $obj->delete();
                            continue;
                        }                        
                    }else{
                        if(strlen($v[2]) <= 0){
                            continue;
                        }
                        $obj = new Tproyectoejecucion();
                        $obj->cproyectoejecucioncab=$cab->cproyectoejecucioncab;                            
                    }
                    
                    $obj->ccondicionoperativa='PLA';
                    $obj->estado='1';
                
                    
                    $obj->cactividad=$cactividad;
                    $obj->cpersona_ejecuta=$adm[1];
                    if(strlen($v[2]) > 0){
                        $obj->horasplanificadas=$v[2];    
                    }else{
                        $obj->horasplanificadas=null;    
                    }
                    
                    $fec = substr($k,0,4)."-".substr($k,4,2)."-".substr($k,6,2);
                    $obj->obsplanificada='';
                    $obj->fplanificado=$fec;
                    $obj->fejecuta=$fec;

                    $obj->fregistro= Carbon::now();
                    $obj->tipo='3';
                    
                    $obj->csubsistema='004';
                    $obj->save();  

                }
            }
        }
        return "OK";
    }
    /*  Fin Planificación area de tareas administrativas*/
    public function asignacionParticipantesProyecto(Request $request){

         return view('proyecto/asignacionParticipantes');

    }

    public function editarAsignacionParticipantes(Request $request,$idProyecto){

        Session::forget('proyectoParticipantes');
        Session::forget('delDisciplinas');

        $user = Auth::user();
      /*  $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->carea_parent;

        }*/

        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->leftjoin('tareas as tap','ta.carea_parent','=','tap.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->select('e.cpersona','e.carea', 'e.ccargo','ta.carea','ta.descripcion','tap.carea as cpadre','tap.codigo as codigopadre')
        ->first();
        $carea=0;
        $carea_parent=0;
       // $codigoparent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->cpadre;
          //  $codigoparent=$tarea->codigopadre;
        }


        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = [''=>''] + $servicio;

       /* $personal = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')      
        ->where('tnat.esempleado','=','1')
        ->where('emp.carea','=',$carea)    
        ->orWhere('emp.carea','=',$carea_parent)       
        ->orderBy('per.nombre','ASC')
        ->lists('per.nombre','per.cpersona');
        $personal = [''=>''] + $personal; */

        $personagte=DB::table('tareas as ta')
        ->where('ta.carea','=',$carea_parent)
        ->where('ta.codigo','ilike','%'.'GT'.'%')
        ->first();

        $codigoGT=0;

        if($personagte){
        $codigoGT=$personagte->codigo;
        }

        

        $personal = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')
      
        ->where('tnat.esempleado','=','1')
        ->where('emp.estado','=','ACT')
        ->where('emp.carea','=',$carea)
        ->orderBy('per.abreviatura','ASC');

           if(substr($codigoGT,0,2)=='GT'){
            $personal=$personal->orWhere('emp.carea','=',$carea_parent);
           }
                 
        $personal=$personal->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');
        $personal = [''=>''] + $personal;

        //dd($personal);
 
        $categoria=DB::table('tcategoriaprofesional')
        ->orderBy('descripcion','ASC')
        ->lists('descripcion','ccategoriaprofesional');
        $categoria = [''=>''] + $categoria;
        
        $proyectosub = DB::table('tproyectosub')->where('cproyecto','=',$proyecto->cproyecto)->lists('descripcionsub','cproyectosub');
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $revision=""; 
        $editar=true;     

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();

        $documento = DB::table('tproyectodocumentos as prydoc')
        ->join('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','EDT')
        ->select('prydoc.cproyectodocumentos','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first(); 

        $tdisciplinas = DB::table('tdisciplina as d')
        ->join('tdisciplinaareas as da','da.cdisciplina','=','d.cdisciplina')
        ->select('d.*')
        ->distinct()
        ->get();         

        $disciplinas = array();
        foreach($tdisciplinas as $tdis){
            $d['cdisciplina'] = $tdis->cdisciplina;
            $d['descripcion'] = $tdis->descripcion;
            $d['tipodisciplina'] = $tdis->tipodisciplina;
            $da = array();
            $dis_area = DB::table('tdisciplinaareas as da')
            ->join('tareas as a','da.carea','=','a.carea')
            ->join('tpersonadatosempleado as pd','a.carea','=','pd.carea')
            ->join('tpersona as p','pd.cpersona','=','p.cpersona')
            ->where('da.cdisciplina','=',$d['cdisciplina'])
            ->select('p.*','pd.carea','da.cdisciplina')
            ->get();
            $d['da']= $dis_area;
            $disciplinas[count($disciplinas)] = $d;
        }

        $tproyectodisciplina = DB::table('tproyectopersona as tpp')
        ->leftJoin('tdisciplina as dis','tpp.cdisciplina','=','dis.cdisciplina')
        ->leftJoin('tdisciplinaareas as tda','tpp.cdisciplina','=','tda.cdisciplina')
        ->leftJoin('tareas as ta','tda.carea','=','ta.carea')
        ->leftJoin('tpersona as p','tpp.cpersona','=','p.cpersona')        
        ->leftJoin('tcategoriaprofesional as tcp','tpp.ccategoriaprofesional','=','tcp.ccategoriaprofesional')
        ->join('tpersonadatosempleado as emp','tpp.cpersona','=','emp.cpersona')
        ->where('tpp.cproyecto','=',$idProyecto)
        ->where('emp.carea','=',$carea)        
        ->orderBy('tcp.orden','ASC')
        ->select('dis.descripcion as disciplina','p.abreviatura as participante','tcp.descripcion as categoria','tpp.*','tcp.orden as orden');

        $proyectopersona=DB::table('tproyectopersona as tpp')
        ->join('tpersonadatosempleado as e','tpp.cpersona','=','e.cpersona')    
        ->get();
        foreach ($proyectopersona as $p) {
            if($p->carea==$carea_parent){
                 $tproyectodisciplina=$tproyectodisciplina->orWhere('emp.carea','=',$carea_parent)
                  ->where('tpp.cproyecto','=',$idProyecto);
            }
        }

        $tproyectodisciplina=$tproyectodisciplina->get();      

        $proyectodisciplina=array();
        foreach($tproyectodisciplina as $td){
            $d['cproyectopersona']=$td->cproyectopersona;
            $d['cpersona_rdp']=$td->cpersona;
            $d['cdisciplina']=$td->cdisciplina;           
            $d['disciplina']=$td->disciplina;
            $d['participante']=$td->participante;
            $d['orden']=$td->orden;
            $d['ccategoriaprofesional']=$td->ccategoriaprofesional;
            $d['categoria']=$td->categoria;
            if($td->eslider=='1'){
                $d['eslider']='Si';
            }
            else{
                $d['eslider']='';               
            }               

            $proyectodisciplina[count($proyectodisciplina)]=$d; 
                
        }       
        
      
        $bsave=true;
        Session::put('proyectoParticipantes',$proyectodisciplina);



        //dd($proyectodisciplina,$rlider,$request);
         
        return view('proyecto/asignacionParticipantes',compact('proyecto','persona','editar','uminera','servicio','personal','monedas','medioentrega','revision','proyectosub','estadoproyecto','documento','proyectodisciplina','bsave','disciplinas','categoria'));
    }


    public function verParticipantes(Request $request){
        $proyectodisciplina=array();
        if(Session::get('proyectoParticipantes')){
            $proyectodisciplina = Session::get('proyectoParticipantes');
        }    
      
        return view('proyecto.tableParticipantes',compact('proyectodisciplina'));
    }

    public function agregarParticipantes(Request $request){

            //dd($request->all());
              
            $user = Auth::user();

            $tarea = DB::table('tpersonadatosempleado as e')
            ->leftjoin('tareas as ta','e.carea','=','ta.carea')
            ->where('cpersona','=',$user->cpersona)
            //->select('e.cpersona','ta.carea')
            ->first();


            $carea=0;
            $carea_parent=0;
            
            if($tarea){
                $carea = $tarea->carea;
                $carea_parent=$tarea->carea_parent;
            }

            $tdisciplina = DB::table('tdisciplinaareas as ta')
            ->leftjoin('tdisciplina as d','ta.cdisciplina','=','d.cdisciplina')
            ->where('ta.carea','=',$carea)
            ->first();

            $proyectodisciplina=array();
            if(Session::get('proyectoParticipantes')){
                $proyectodisciplina = Session::get('proyectoParticipantes');
            }    
     
            $participanteseleccionado='';

           
            $nombre='';


        foreach ($proyectodisciplina as $k) {
            //dd($proyectodisciplina);
            $personal = DB::table('tpersona as per')
            ->where('per.cpersona','=',$request->input('personal'))         
            ->first();

            if($personal){
                $nombre=$personal->abreviatura;
            }           
        

            if( $k['participante']!=$nombre){

                $i = -1 * (count($proyectodisciplina) + 3)*10*date('s');
                $pd['cproyectopersona']= $i;                   
                $pd['cpersona_rdp'] = $request->input('personal');
                $pd['cdisciplina']=$tdisciplina->cdisciplina; 
                $pd['disciplina'] = $tdisciplina->descripcion;
                $tpersona = DB::table('tpersona')
                ->where('cpersona','=',$pd['cpersona_rdp'])
                ->first();
                $pd['participante'] = $tpersona->abreviatura;
                $categoria=DB::table('tcategoriaprofesional')
                ->where('ccategoriaprofesional','=',$request->input('categoria'))
                ->first();
                $pd['ccategoriaprofesional'] =$request->input('categoria'); 
                $pd['categoria'] =$categoria->descripcion; 
                $pd['orden'] =$categoria->orden;

                if($request->input('chklider')=='1_'.$pd['cpersona_rdp']){
                    $pd['eslider']='Si';
                }
                else{
                     $pd['eslider']='';
                }                           
                                
            }
            else{
                echo '<script language="javascript">alert("Este colaborador ya fue agregado");</script>'; 

                return view('proyecto.tableParticipanteSeleccionado',compact('proyectodisciplina'));
            }
        }

        if(empty($proyectodisciplina)){
            $i = -1 * (count($proyectodisciplina) + 3)*10*date('s');
                $pd['cproyectopersona']= $i;                   
                $pd['cpersona_rdp'] = $request->input('personal');
                $pd['cdisciplina']=$tdisciplina->cdisciplina; 
                $pd['disciplina'] = $tdisciplina->descripcion;                      
                $tpersona = DB::table('tpersona')
                ->where('cpersona','=',$pd['cpersona_rdp'])
                ->first();
                $pd['participante'] = $tpersona->abreviatura;
                $categoria=DB::table('tcategoriaprofesional')
                ->where('ccategoriaprofesional','=',$request->input('categoria'))
                ->first();
                $pd['ccategoriaprofesional'] =$request->input('categoria'); 
                $pd['categoria'] =$categoria->descripcion; 
                $pd['orden'] =$categoria->orden;

                if($request->input('chklider')=='1_'.$pd['cpersona_rdp']){
                    $pd['eslider']='Si';
                }
                else{
                     $pd['eslider']='';
                }                
        }

        $proyectodisciplina[count($proyectodisciplina)] = $pd;              


                foreach ($proyectodisciplina as $key => $orden) {
                    
                    $aux[$key]=$orden['orden'];
                    
                }

                $array=array_multisort($aux,SORT_ASC,$proyectodisciplina);
        Session::put('proyectoParticipantes',$proyectodisciplina);

             

         return view('proyecto.tableParticipanteSeleccionado',compact('proyectodisciplina'));
            
    }

  

    public function agregarLiderEquipo(Request $request){

        if(Session::get('proyectoParticipantes')){
            $proyectodisciplina = Session::get('proyectoParticipantes');
        }  
        

       
        $radioLider=$request->input('chklider');

        $perlider=explode('_', $radioLider);

        if (!(is_null($radioLider)||strlen($radioLider)<0)){
             $proyectodisciplinanew=array();

            foreach ($proyectodisciplina as $pd) {

                if($pd['cpersona_rdp']==$perlider[1]){

                    $pd['eslider']='Si';

                                
                   }
                else{

                    $pd['eslider']='';
                  
                }
                    $proyectodisciplinanew[count($proyectodisciplinanew)] = $pd;     
            }

            Session::put('proyectoParticipantes',$proyectodisciplinanew); 
             
        }       

                 

        return Redirect::route('verEquipo');
     
    }

    function verEquipo(){





        $proyectodisciplina=array();
        if(Session::get('proyectoParticipantes')){
            $proyectodisciplina = Session::get('proyectoParticipantes');
        }   
        
             
        return view('proyecto.tableParticipanteSeleccionado',compact('proyectodisciplina'));

    }

    public function borrarParticipante(Request $request){
        $proyectodisciplina=array();
        $delDisciplinas = array();

        if(Session::get('delDisciplinas')){
            $delDisciplinas = Session::get('delDisciplinas');
        }

        $aDel = $request->input('chk');



        foreach($aDel as $del){ 

            if($del > 0){ 
                $delDisciplinas[count($delDisciplinas)] = $del;                
            }

        }
        Session::put('delDisciplinas',$delDisciplinas);  



        if(Session::get('proyectoParticipantes')){
            $proyectodisciplina = Session::get('proyectoParticipantes');
        }

        $new = array();
        $temp = $proyectodisciplina;
        foreach($aDel as $del){
            $new = array();
            foreach($temp as $pd){
             
                    if ($pd['cproyectopersona']!=$del){

                        $new[count($new)]=$pd;  

                                
                }
            }
            $temp=$new;

        }      
        $proyectodisciplina = $temp;

        Session::put('proyectoParticipantes',$proyectodisciplina);        

        return view('proyecto.tableParticipanteSeleccionado',compact('proyectodisciplina'));
    }    

     public function grabarParticipante(Request $request){


         DB::beginTransaction();
         $cproyecto= $request->input('cproyecto');
         //dd($request->all());


         $tproyecto=DB::table('tproyecto')
        ->where('cproyecto','=',$cproyecto)
        ->first();

        
        $proyectodisciplina=array();

        $delDisciplinas = array();
        if(Session::get('delDisciplinas')){
            $delDisciplinas = Session::get('delDisciplinas');
        }
        if(Session::get('proyectoParticipantes')){
            $proyectodisciplina = Session::get('proyectoParticipantes');
        }

        foreach($delDisciplinas as $d){
            Tproyectopersona::where('cdisciplina','=',$d)->delete();
        }

         $persona=null;
         foreach($proyectodisciplina as $p){
             //dd($proyectodisciplina);

                // asi era esta cosa obtener lider y mostrar el lider real
             $disciplina_actual = DB::table('tpersonadatosempleado as em')
                                    ->leftjoin('tdisciplinaareas as tdispc','em.carea','=','tdispc.carea')
                                    ->where('em.cpersona','=',$p['cpersona_rdp'])
                                    ->select('em.cpersona','tdispc.cdisciplina')
                                    ->first();



            if($p['cproyectopersona']<=0){
                $prod = new Tproyectopersona();

            }else{
                $prod = Tproyectopersona::where('cproyectopersona','=',$p['cproyectopersona'])->first();
            }      
            $prod->cproyecto=$cproyecto;
            //$prod->cdisciplina = $p['cdisciplina'];
            $prod->cdisciplina =$disciplina_actual->cdisciplina;
            $prod->cpersona = $p['cpersona_rdp'];
            $prod->ccategoriaprofesional = $p['ccategoriaprofesional'];

            //$prod->eslider=$p['eslider'];
                     
           if( $p['eslider']=='Si'){
                $prod->eslider='1';
                $prod->seniors=null;//seniors es igual vacio y es lider

                $persona=$p['cpersona_rdp'];               

            }  
            else{
                $prod->eslider='0';
            }               
            $prod->save();         
            
        }  

       $tarea = DB::table('tpersonadatosempleado as e')
                ->leftjoin('tareas as ta','e.carea','=','ta.carea')
                ->where('cpersona','=',$persona)
                ->whereNull('ftermino')
                ->first();
                $carea=0;              
        $codigo_area="";
                if($tarea){
                    $carea = $tarea->carea; 
                    $codigo_area = $tarea->codigo;
                }

       /* $tproyectoinformacionadicional=DB::table('tproyectoinformacionadicional')
        ->where('cproyecto','=',$cproyecto)
        ->first();*/

                $proyinfad=Tproyectoinformacionadicional::where('cproyecto','=',$cproyecto)->first();

      
                if(is_null($proyinfad)){

                    $proyinfad=new Tproyectoinformacionadicional();
                    $proyinfad->cproyecto=$cproyecto;
                    //$proyinfad->cunidadmineracontacto=null; 

                 }

                /*else{
                    $proyinfad=Tproyectoinformacionadicional::where('cproyecto','=' ,$cproyecto)->first();
               
                 }*/

                    if($codigo_area=='26'){
                        $proyinfad->cpersona_cproyecto=$persona;
                    }

                    if($codigo_area=='27'){
                        $proyinfad->cpersona_cdocumentario=$persona;
                    }
                    //dd($proyinfad);
                    $proyinfad->save();          

     
        DB::commit();
        
        return Redirect::route('editarAsignacionParticipantes',$cproyecto);
    }

    public function eliminarParticipanteProyecto($participante){
        DB::beginTransaction();
        $proyectopersona = Tproyectopersona::where('cproyectopersona','=',$participante)->first();
        $cproyecto = $proyectopersona->cproyecto;
        try
        {
            $proyectopersona->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('verParticipProyec',$cproyecto);
    }


    public function verParticipProyec(Request $request,$idProyecto){

       $user = Auth::user();
            $tarea = DB::table('tpersonadatosempleado as e')
            ->leftjoin('tareas as ta','e.carea','=','ta.carea')
            ->where('cpersona','=',$user->cpersona)
            ->whereNull('ftermino')
            ->orderBy('fingreso','DESC')
            ->first();
            $carea=0;
            $carea_parent=0;
            
            if($tarea){
                $carea = $tarea->carea;
                $carea_parent=$tarea->carea_parent;
            }

       $tproyectodisciplina = DB::table('tproyectopersona as tpp')
        ->leftJoin('tdisciplina as dis','tpp.cdisciplina','=','dis.cdisciplina')
        ->leftJoin('tdisciplinaareas as tda','tpp.cdisciplina','=','tda.cdisciplina')
        ->leftJoin('tareas as ta','tda.carea','=','ta.carea')
        ->leftJoin('tpersona as p','tpp.cpersona','=','p.cpersona')        
        ->leftJoin('tcategoriaprofesional as tcp','tpp.ccategoriaprofesional','=','tcp.ccategoriaprofesional')
        ->join('tpersonadatosempleado as emp','tpp.cpersona','=','emp.cpersona')
        ->where('tpp.cproyecto','=',$idProyecto)
        ->where('emp.carea','=',$carea)        
        ->orderBy('tcp.orden','ASC')
        ->select('dis.descripcion as disciplina','p.abreviatura as participante','tcp.descripcion as categoria','tpp.*','tcp.orden as orden');

        $proyectopersona=DB::table('tproyectopersona as tpp')
        ->join('tpersonadatosempleado as e','tpp.cpersona','=','e.cpersona')    
        ->get();
        foreach ($proyectopersona as $p) {
            if($p->carea==$carea_parent){
                 $tproyectodisciplina=$tproyectodisciplina->orWhere('emp.carea','=',$carea_parent)
                  ->where('tpp.cproyecto','=',$idProyecto);
            }
        }

       $tproyectodisciplina=$tproyectodisciplina->get();      

        $proyectodisciplina=array();
        foreach($tproyectodisciplina as $td){
            $d['cproyectopersona']=$td->cproyectopersona;
            $d['cpersona_rdp']=$td->cpersona;
            $d['cdisciplina']=$td->cdisciplina;
            $d['disciplina']=$td->disciplina;
            $d['participante']=$td->participante;
            $d['orden']=$td->orden;
            $d['ccategoriaprofesional']=$td->ccategoriaprofesional;
            $d['categoria']=$td->categoria;
            if($td->eslider=='1'){
                $d['eslider']='Si';
            }
            else{
                $d['eslider']='';
            }               

            $proyectodisciplina[count($proyectodisciplina)]=$d; 
                
        }   
        return view('proyecto.tableParticipantes',compact('proyectodisciplina'));
    }


    public function habilitarTareasProyecto(Request $request){

         return view('proyecto/habilitacionActividades');

    }

   public function listarProyLider(){       
        //DB::raw('fn_getcondicionopgastos(tge.cgastosejecucion) as condicion'),}

        $cpersona = Auth::user()->cpersona;

        $esjefe=DB::table('tpersonadatosempleado as e')
        ->leftjoin('tcargos as c','e.ccargo','=','c.ccargo')      
        ->where('e.cpersona','=',$cpersona)
        ->select('e.cpersona','c.esjefaturaarea')
        ->first();

        $objflujoEmp = new FlujoLE();
        $flujo = $objflujoEmp->ruteoGP($cpersona);

// dd($flujo);
        if($esjefe){


            if($esjefe->esjefaturaarea=='1'){

                return Datatables::queryBuilder(DB::table('tproyecto')
                ->join('testadoproyecto as te','tproyecto.cestadoproyecto','=','te.cestadoproyecto')
                ->leftjoin('tpersona as tper','tproyecto.cpersonacliente','=','tper.cpersona')
                ->leftJoin('tpersona as p','tproyecto.cpersona_gerente','=','p.cpersona')
                ->leftjoin('tunidadminera as tu','tproyecto.cunidadminera','=','tu.cunidadminera')
                // ->where('tproyecto.cestadoproyecto','=','001')
                ->whereIn('tproyecto.cestadoproyecto',['001','004','005'])
                ->select('tproyecto.cproyecto','tproyecto.codigo','tper.nombre as cliente','tu.nombre as uminera','tproyecto.nombre', 'p.abreviatura as GteProyecto','te.descripcion',DB::raw('CONCAT(\'row_\',tproyecto.cproyecto)  as "DT_RowId"'))
                )->make(true);
            }elseif ($flujo == 'GP') {
            return Datatables::queryBuilder(DB::table('tproyecto')
                ->join('testadoproyecto as te','tproyecto.cestadoproyecto','=','te.cestadoproyecto')
                ->leftjoin('tpersona as tper','tproyecto.cpersonacliente','=','tper.cpersona')
                ->leftJoin('tpersona as p','tproyecto.cpersona_gerente','=','p.cpersona')
                ->leftjoin('tunidadminera as tu','tproyecto.cunidadminera','=','tu.cunidadminera')
                // ->where('tproyecto.cestadoproyecto','=','001')
                ->whereIn('tproyecto.cestadoproyecto',['001','004','005'])
                ->where('tproyecto.cpersona_gerente','=',$cpersona)
                ->select('tproyecto.cproyecto','tproyecto.codigo','tper.nombre as cliente','tu.nombre as uminera','tproyecto.nombre', 'p.abreviatura as GteProyecto','te.descripcion',DB::raw('CONCAT(\'row_\',tproyecto.cproyecto)  as "DT_RowId"'))
                )->make(true);
        }
            else{

                return Datatables::queryBuilder(DB::table('tproyecto')
                ->join('testadoproyecto as te','tproyecto.cestadoproyecto','=','te.cestadoproyecto')
                ->leftjoin('tpersona as tper','tproyecto.cpersonacliente','=','tper.cpersona')
                ->leftJoin('tpersona as p','tproyecto.cpersona_gerente','=','p.cpersona')
                ->leftjoin('tunidadminera as tu','tproyecto.cunidadminera','=','tu.cunidadminera')
                ->join('tproyectopersona as tp','tp.cproyecto','=','tproyecto.cproyecto')
                ->where('tp.cpersona','=',$cpersona)
                ->where('tp.eslider','=','1')
                // ->where('tproyecto.cestadoproyecto','=','001')
                ->whereIn('tproyecto.cestadoproyecto',['001','004','005'])
                ->select('tproyecto.cproyecto','tproyecto.codigo','tper.nombre as cliente','tu.nombre as uminera','tproyecto.nombre', 'p.abreviatura as GteProyecto','te.descripcion',DB::raw('CONCAT(\'row_\',tproyecto.cproyecto)  as "DT_RowId"'))
                )->make(true);

            }
        }
       

    }

    public function editarHabilitacionActividades(Request $request,$idProyecto){

        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = [''=>''] + $servicio;

        $user = Auth::user();
        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->carea_parent;

        }

        $tproyectoactividadeshabilitacion=DB::table('tproyectoactividadeshabilitacion')
        ->where('cproyecto','=',$idProyecto)
        ->get();

        $activhabil=array();
      

        foreach ($tproyectoactividadeshabilitacion as $ph) {

          if($ph->activo=='1'){
            $chkhabilitado='1_'.$ph->cproyectoactividades.'_'.$ph->cpersona;
            $ah['activo']=$chkhabilitado;
            $activhabil[count($activhabil)] = $ah;
          }
          else{
            $ah['activo']='0';
          }
        }     

        $lparticipantes=DB::table('tproyectopersona as p')
        ->leftjoin('tproyecto as pr','p.cproyecto','=','pr.cproyecto')
        ->leftjoin('tpersona as per','p.cpersona','=','per.cpersona')
        ->leftjoin('tcategoriaprofesional as c','p.ccategoriaprofesional','=','c.ccategoriaprofesional')       
        ->join('tpersonadatosempleado as emp','p.cpersona','=','emp.cpersona')
        ->where('p.cproyecto','=',$idProyecto)
        ->where('emp.carea','=',$carea)  
        ->orderBy('c.orden','ASC')
        ->select('p.*','per.nombre','c.descripcion','per.abreviatura as usuario');
      
        $proyectopersona=DB::table('tproyectopersona as p')
        ->join('tpersonadatosempleado as e','p.cpersona','=','e.cpersona')    
        ->get();
        foreach ($proyectopersona as $pp) {
            if($pp->carea==$carea_parent){
                 $lparticipantes=$lparticipantes->orWhere('emp.carea','=',$carea_parent)
                  ->where('p.cproyecto','=',$idProyecto);
            }
        }
        $lparticipantes=$lparticipantes->get();   

        $participantes=array();
        foreach($lparticipantes as $par){

            $p['cproyectopersona'] = $par->cproyectopersona;
            $p['cproyecto'] =$par->cproyecto;
            $p['cpersona'] = $par->cpersona;
            $p['ccategoriaprofesional'] = $par->ccategoriaprofesional;
            $p['cdisciplina'] = $par->cdisciplina;
            $p['eslider'] = $par->eslider;
            $p['nombre'] = $par->nombre;
            $p['usuario'] =$par->usuario;
            $p['descripcion'] = $par->descripcion;           
            $participantes[count($participantes)] = $p;
        }         

        $lactividades = DB::table('tproyectoactividades as pact')
        ->leftjoin('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$proyecto->cproyecto)
        //->orderBy('pact.codigoactvidad','ASC')
        ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
        ->select('pact.*')
        ->get();
        $listaAct = array();   


        $actividadPadre=DB::table('tproyectoactividades as a')
            ->join('tproyectoactividades as ap','a.cproyectoactividades','=','ap.cproyectoactividades_parent')
            ->where('a.cproyecto','=',$proyecto->cproyecto)   
            ->distinct('a.cproyectoactividades') 
            ->select('a.cproyectoactividades')       
            ->get();

        $actPadre=array();
        foreach ($actividadPadre as $ap) {
            $p['cproyectoactividades']=$ap->cproyectoactividades;
            $actPadre[count($actPadre)] = $p;
        }
        //dd($actPadre);
        //dd($actPadre);

        foreach($lactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] =$act->codigoactvidad;
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cproyectoactividades_parent;

            
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        //$listaAct = $objAct->orderParentActividadEjecucion($listaAct);
       

        return view('proyecto/habilitacionActividades',compact('proyecto','persona','uminera','servicio','listaAct','participantes','chkhabilitado','activhabil','actPadre'));
    }

    public function verCategoria(Request $request){
        $cpersona=$request->input('cpersona');
        $tpersona=DB::table('tpersonadatosempleado as tpe')
        ->where('tpe.cpersona','=',$cpersona)
        ->first();
        $categoria='';
        if($tpersona){
            $categoria=$tpersona->ccategoriaprofesional;
        }
        return $categoria;

    }

    public function grabarHabilitacion(Request $request){
        $proyecto = Tproyecto::where('cproyecto','=',$request->input('cproyecto'))->first();
        $cproyecto=$proyecto->cproyecto;
        
        DB::beginTransaction();

        $user = Auth::user();
        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->carea_parent;

        }       
        $activihabilarea= DB::table('tproyectoactividadeshabilitacion as p')       
        ->join('tpersonadatosempleado as pd','p.cpersona','=','pd.cpersona')  
        ->where('pd.carea','=',$carea)  
        ->orWhere('pd.carea','=',$carea_parent)
        ->where('p.cproyecto','=',$cproyecto)
        ->select('p.cproyacthab')      
        ->get();

        $activihabilareaarray=array();
        foreach ($activihabilarea as $ac) {
           $a['cproyacthab'] = $ac->cproyacthab;
           $activihabilareaarray[count($activihabilareaarray)] = $a;
        }

          

            $res=$request->input('chkacthabi');  
            $res2 = Tproyectoactividadeshabilitacion::where('cproyecto','=',$cproyecto)
            ->whereIn('cproyacthab',$activihabilareaarray)
            ->update(['activo' => '0']);

     
            /*Tproyectodisciplina::where('cproyecto','=',$idProyecto)
            ->update(['eslp' => '0']);*/
            /*foreach ($res as $r ){
                $eValor=explode( '_', $r);  

                $tproyharegistrados=DB::table('tproyectoactividadeshabilitacion')
                    ->whereNotIn('cproyectoactividades',[$eValor[1]])
                    ->whereNotIn('cproyectoactividades',[$eValor[2]])
                    ->get();

                    //dd($tproyharegistrados);
                  if($tproyharegistrados){
                        foreach ($tproyharegistrados as $tpr) {
                        $tpahr=Tproyectoactividadeshabilitacion::where('cproyacthab','=',$tpr->cproyacthab)->first();
                       
                        $tpahr->activo=0;
                        $tpahr->save();
                    }

                    }

            }*/

            if(!is_null($request->input('chkacthabi'))){

                foreach ($res as $valor) {

                    $aValor=explode( '_', $valor);  
                   
                    $tproyectoactividadeshabilitacion=Tproyectoactividadeshabilitacion::where('cproyectoactividades','=',$aValor[1])
                         ->where('cpersona','=',$aValor[2])
                         ->first();
 
                    if(is_null($tproyectoactividadeshabilitacion)){
                            $tpah=new Tproyectoactividadeshabilitacion();
                            $tpah->cproyectoactividades=$aValor[1];
                            $tpah->cpersona=$aValor[2];
                            $tpah->cproyecto=$cproyecto;          
                           
                    } 

                    else{
                        $tpah=Tproyectoactividadeshabilitacion::where('cproyacthab','=',$tproyectoactividadeshabilitacion->cproyacthab)->first();
                    }

                            $tpah->activo=1;
                            $tpah->save();
                }
            }

        DB::commit();       
       return Redirect::route('editarHabilitacionActividades',$cproyecto);
    }

    public function verReporteHorasParticipante(Request $request){
        $idParticipante = $request->input('cpersona');

        $cpersona = Auth::user()->cpersona;
        $tarea = DB::table('tpersonadatosempleado as e')           
        ->where('cpersona','=',$cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')      
        ->first();
        $carea=0;
        
        
        if($tarea){
            $carea = $tarea->carea;
            
        }

        $tpersona = DB::table('tpersona as per')
        ->where('per.cpersona','=',$idParticipante)
        ->first();
        $per_nombre="";
        if($tpersona){
            $per_nombre = $tpersona->abreviatura;
        }

        $nroFila=0;
        $objAct  = new ActividadSupport();
        /* Obtener la Fecha para sacar la semana de trabajo*/
        
        $fec=$request->input('fecha');
        // dd($fec);
        $fecha = $fec;
        if(substr($fec,2,1)=='/' || substr($fec,2,1)=='-'){    
            $fecha = Carbon::create(substr($fecha,6,4),substr($fecha,3,2),substr($fecha,0,2),0,0,0);

        }else{
            $fecha = Carbon::create(substr($fecha,0,4),substr($fecha,5,2),substr($fecha,8,2),0,0,0);
        }
        $fecha1 = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $fecha2 = Carbon::create($fecha->year,$fecha->month,$fecha->day,23,59,59);
        $semana = $fecha->weekOfYear; // se obtiene la semana del año que corresponde la fecha
        $fecha1 = $fecha1->startOfWeek(); // la fecha de inicio de semana
        $fecp = Carbon::create($fecha1->year,$fecha1->month,$fecha1->day,0,0,0); // Fecha de Proceso
        $fecha_eje = Carbon::create($fecha1->year,$fecha1->month,$fecha1->day,0,0,0); // Fecha para realizar consulta en tproyectoejecucion
        $fecha2 = $fecha2->endOfWeek();     // Fecha fin de la semana.
        
        $anio=$fecha->year;

        $fdesde = Carbon::create($fecha->year,$fecha->month,$fecha->day,0,0,0);
        $fhasta = Carbon::create($fecha->year,$fecha->month,$fecha->day,23,59,59);    
        $fdesde = $fdesde->startOfWeek(); 
        $fhasta = $fhasta->endOfWeek(); 
        $seguir = true;
        $dias= array();
        $dia = $fdesde;
        $aDia = ['Do','Lu','Ma','Mi','Ju','Vi','Sa'];
        while($seguir){
            $dias[count($dias)] = array($dia->toDateString(),$dia->day,$aDia[$dia->dayOfWeek]);
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".str_pad($dia->month,2,'0',STR_PAD_LEFT) ."". str_pad($dia->day,2,'0',STR_PAD_LEFT)) <= ($fhasta->year ."".str_pad($fhasta->month,2,'0',STR_PAD_LEFT) ."". str_pad($fhasta->day,2,'0',STR_PAD_LEFT) ) ) ) {
                $seguir=false;    
            }            
        } 

        $historialApro=array(); 
        $historialCom=array();
        $cpersona = $idParticipante;
        $tproyectopersona = DB::table('tproyectopersona as pp')
        ->where('pp.cpersona','=',$cpersona)
        ->select('pp.cproyecto')
        ->get();
        $aProyectos = array();
        foreach($tproyectopersona as $o){
            $aProyectos[count($aProyectos)]=$o->cproyecto;
        }


        $tclientes = DB::table('tproyecto as pry')
        ->join('tpersona as per','pry.cpersonacliente','=','per.cpersona')
        ->join('tpersonajuridicainformacionbasica as jur','jur.cpersona','=','per.cpersona')
        
        ->where('jur.escliente','=','1')
        ->orderBy('per.nombre','ASC')
        ->whereIn('pry.cproyecto',$aProyectos)
        ->select('per.*')
        ->distinct()
        ->get();
        $clientes = array();
        foreach($tclientes as $cli){
            $clientes[$cli->cpersona]=$cli->nombre;
        }
                    

        /* No Facturables */
        $tnofacturables = DB::table('ttiposnofacturables')
        ->orderBy('descripcion','ASC')
        ->get();
        $nofacturables = array(""=>"");
        foreach($tnofacturables as $nf){
            $nofacturables[$nf->ctiponofacturable] = $nf->descripcion;
        }
        /* Fin No Facturables*/

        /* mostrar programacion de la Fecha*/
        $alinea = array();
        $sumFF=0;
        $sumNF=0;
        $sumAN=0; 
        $w=0;

        //$filas=0;
        

        /* Tareas Administrativas*/
        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->leftJoin('tactividad as a ','cab.cactividad','=','a.cactividad');
        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->where('cab.cpersona_ejecuta','=',$idParticipante)                 
        ->where('cab.tipo','=','3')
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
        $tactividadesk=$tactividadesk->select('cab.cproyectoejecucioncab','cab.cpersona_ejecuta','cab.tipo')
        ->addSelect('cab.cactividad','cab.cactividad','a.codigo','a.descripcion','cab.ctiponofacturable')
       ->orderBy('a.codigo','ASC')
        ->distinct()
        ->get();
        //dd($tactividadesk);

        foreach($tactividadesk as $k){
            $nroFila++;
            $w++;
            $pla= array();    
            
            

            $pla['item'] = $w;
            
            $pla['codigopry']= "";
            $pla['codigoactividad']='';            
            $pla['cproyectoejecucioncab']=$k->cproyectoejecucioncab;
            $pla['personanombre']= '';
            $pla['cpersona_ejecuta'] = $k->cpersona_ejecuta;
            $pla['unidadminera']= '';
            $pla['nombreproy']='';
            $pla['cproyectoactividades'] = '';
            $pla['cactividad']=$k->cactividad;
            $pla['cproyecto'] = '';
            $pla['cestructuraproyecto']="";    
            $pla['entregable'] ="";
            $pla['responsable'] ='';     
            $pla['descripcionactividad'] = $k->descripcion;  
            $pla['ctiponofacturable']= $k->ctiponofacturable;
            $pla['horasPla'] = 0;
            $pla['ctiponofacturable'] = $k->ctiponofacturable;
            
            $pla['horaseje'] = 0;
            $pla['porav'] = 0;
            $pla['porav_costo']=0;
            $pla['tipo']  = $k->tipo;                  
            $pla['gp']  = '';  
            $pla['tot']=0;
            $pla['mensajehoras']='';  
            $pla['mensajecosto']='';
            


            $actividadesp = DB::table('tproyectoejecucion as eje')
            ->where('eje.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
            ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
            $actividadesp=$actividadesp->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'),DB::raw('to_char(eje.fplanificado,\'YYYY-MM-DD\' ) as fec'),DB::raw('to_char(eje.fplanificado,\'D\' ) as dia'))
            ->orderBy('fpla','ASC')
            ->get();
            //dd($actividadesp);
            foreach($actividadesp as $ej){
                /*
                Obtener Historial de Aprobaciones
                */
                $taprob = DB::table('tflujoaprobacion as apro')
                ->leftJoin('tpersona as per','apro.cpersona_aprobado','=','per.cpersona')
                ->where('apro.cproyectoejecucion','=',$ej->cproyectoejecucion)
                ->select('apro.*','per.nombre as emp')
                ->get();
                foreach($taprob as $aprob){
                    $his['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $his['fecha']=$aprob->fregistro;
                    $his['ccondicionoperativa']=$aprob->ccondicionoperativa_aprobado;
                    $his['ccondicionoperativa_sig']=$aprob->ccondicionoperativa_siguiente;
                    $his['aprobado']=$aprob->emp;
                    $his['actividad']=$k->descripcion;
                    $his['horas']=$ej->horasejecutadas;                    
                    $historialApro[count($historialApro)] = $his;
                }
                /*  Fin Historial de aprobaciones*/

                /*
                Obtener Historial de Comentarios
                */
                if( strlen($ej->obsplanificada)>0 || strlen($ej->obsejecutadas)>0 || strlen($ej->obsobservadas)>0 ){
                    $hic['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $hic['actividad']=$k->descripcion;
                    $hic['fecha']=$ej->fplanificado;
                    $hic['comentariosPla']=$ej->obsplanificada;
                    $hic['comentariosEje']=$ej->obsejecutadas;
                    $hic['comentariosObs']=$ej->obsobservadas;
                    $hic['horas']=$ej->horasejecutadas;
                    $historialCom[count($historialCom)] = $hic;
                }
                /*  Fin Historial de comentarios*/ 
                $pla[$ej->fpla]['cproyectoejecucion']=$ej->cproyectoejecucion;
                $pla[$ej->fpla]['estado'] = $ej->estado;
                $pla[$ej->fpla]['horasplanificadas'] = (is_null($ej->horasplanificadas)?'':$ej->horasplanificadas);
                $pla[$ej->fpla]['obsplanificada'] = $ej->obsplanificada;
                $pla[$ej->fpla]['horasejecutadas'] = (is_null($ej->horasejecutadas)?'':$ej->horasejecutadas);
                $pla[$ej->fpla]['obsejecutadas'] = $ej->obsejecutadas;
                $pla[$ej->fpla]['horasaprobadas'] = (is_null($ej->horasaprobadas)?'':$ej->horasaprobadas);
                $pla[$ej->fpla]['obsaprobadas'] = $ej->obsaprobadas;
                $pla[$ej->fpla]['horasobservadas'] = (is_null($ej->horasobservadas)?'':$ej->horasobservadas);
                $pla[$ej->fpla]['obsobservadas'] = $ej->obsobservadas;
                $pla[$ej->fpla]['cpersona_ejecuta'] = $ej->cpersona_ejecuta;
                $pla[$ej->fpla]['fejecuta'] = $ej->fejecuta;
                $pla[$ej->fpla]['fregistro'] =  $ej->fregistro;
                $pla[$ej->fpla]['faprobacion'] = $ej->faprobacion;
                $pla[$ej->fpla]['ordenaprobacion'] = $ej->ordenaprobacion;
                $pla[$ej->fpla]['crol_poraprobar'] = $ej->crol_poraprobar;
                $pla[$ej->fpla]['cpersona'] = $ej->cpersona;
                $pla[$ej->fpla]['fplanificado'] = $ej->fplanificado;
                $pla[$ej->fpla]['cestructuraproyecto'] = $ej->cestructuraproyecto;
                $pla[$ej->fpla]['diasemana'] = $ej->dia;

                

                /*$resReab = array_search($ej->ccondicionoperativa,$aNivelReapertura);
                
                if($resReab===FALSE){
                }else{
                    
                    $reabrir=true;
                }*/
                switch ($ej->estado) {
                            case '1':
                                # code...
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                            
                                }
                                break;
                            case '2':
                            case '3':
                            case '4':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;
                            
                            
                            case '5':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;                      
                        
                }    
          
            }               

            $alinea[count($alinea)]=$pla;

        }
        /* Fin Tareas Administrativas*/
        /* tareas de proyecto */
        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->leftJoin('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->leftJoin('tproyectoactividades as pryact',function($join){
            $join->on('eje.cproyectoactividades','=','pryact.cproyectoactividades');
            $join->on('eje.cproyecto','=','pryact.cproyecto');
        })        
        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto')
        ->leftJoin('tpersona as tpc','pry.cpersonacliente','=','tpc.cpersona')
        ->leftJoin('tpersona as gp','pry.cpersona_gerente','=','gp.cpersona')
        ->leftJoin('tunidadminera as tum','pry.cunidadminera','=','tum.cunidadminera')
        ->where('cab.cpersona_ejecuta','=',$idParticipante)         
        ->where('pry.cestadoproyecto','=','001');

        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2'])
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
        $tactividadesk=$tactividadesk->select('cab.cproyectoejecucioncab','pry.nombre as nombreproy','cab.cpersona_ejecuta')
        ->addSelect('cab.tipo','cab.cproyectoactividades','pry.cproyecto','cab.cactividad','pryact.descripcionactividad')
        ->addSelect('tpc.abreviatura as cliente','tum.nombre as uminera','cab.ctiponofacturable')
        ->addSelect('gp.cpersona as codgp','gp.abreviatura as gp','pry.codigo as codigopry','pryact.codigoactvidad')
        ->distinct()
        ->orderBy('pry.codigo','ASC')
        ->orderBy('pryact.codigoactvidad','ASC')
        ->get();      
        foreach($tactividadesk as $k){
            $nroFila++;
            $w++;
            $pla= array();

            $sumhoras2=0;
            $pla['item'] = $w;
            $pla['cproyectoejecucioncab']=$k->cproyectoejecucioncab;
            $pla['codigoactividad']=$k->codigoactvidad;
            $pla['personanombre']= $k->cliente;
            $pla['codigopry']= $k->codigopry;
            $pla['cpersona_ejecuta'] = $k->cpersona_ejecuta;
            $pla['unidadminera']= $k->uminera;
            $pla['nombreproy']=$k->nombreproy;
            $pla['cproyectoactividades'] = $k->cproyectoactividades;
            $pla['cactividad']=$k->cactividad;
            $pla['cproyecto'] = $k->cproyecto;
            $pla['cestructuraproyecto']="";    
            $pla['entregable'] ="";
            $pla['responsable'] ='';     
            $pla['descripcionactividad'] = $k->descripcionactividad;      
            $pla['horasPla'] = 0;
            $pla['ctiponofacturable'] = $k->ctiponofacturable;
            $pla['des_ctiponofacturable'] = '';
            $pla['gp'] = $k->gp;
            $pla['mensajehoras']='';  
            $pla['mensajecosto']='';

            if(!is_null($k->ctiponofacturable)){
                $nf = DB::table('ttiposnofacturables as nf')
                ->where('ctiponofacturable','=',$k->ctiponofacturable)
                ->first();
                if($nf){
                    $pla['des_ctiponofacturable'] = $nf->descripcion;
                }
            }


            $pla['horaseje'] = $objAct->sumHorasEjecutadasProyArea($k->cproyectoactividades,$carea);
                $tot = $objAct->sumHorasPresupuestadasProyArea($k->cproyectoactividades,$carea);
                $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100) );

                $tot_costo_presup=$objAct->sumCostoPresupuestadoProyArea($k->cproyectoactividades,$k->cproyecto,$carea);
                $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProyArea($k->cproyectoactividades,$k->cproyecto,$carea);

               // dd($tot_costo_ejecutado,$tot_costo_presup, $pla['porav']);

                if($tot_costo_presup==0|| is_null($tot_costo_presup)){

                    $pla['horaseje'] = $objAct->sumHorasEjecutadasProy($k->cproyectoactividades);
                    $tot = $objAct->sumHorasPresupuestadasProy($k->cproyectoactividades);
                    $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100) );

                    $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($k->cproyectoactividades,$k->cproyecto);
                    $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($k->cproyectoactividades,$k->cproyecto);

                   /* Inicio Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal) */

                    if($tot_costo_presup==0|| is_null($tot_costo_presup)){
                       // dd($k->cproyecto,$k->cproyectoactividades,$k->cproyectoejecucioncab);

                         $porcentajeCosto=-1;
                    }

                    else{
                        $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    }

                    /* Fin Si no tiene monto presupuestado en la tabla thonorarios (codigo temporal)*/
                    $pla['porav_costo']=round($porcentajeCosto);

                

                    if($porcentajeCosto==-1){
                        $pla['mensajehoras']='Sin horas';
                        $pla['mensajecosto']='Sin monto';

                    }
                    else{
                        $pla['mensajecosto']=$pla['porav_costo'].'%';
                        $pla['mensajehoras']=$pla['porav'].'%';
                    }

                    //$porcentajeCosto=100;

                }
                else{

                    $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    $pla['porav_costo']=round($porcentajeCosto);
                    

                    $pla['mensajehoras']=$pla['porav'].'%';  
                    $pla['mensajecosto']=$pla['porav_costo'].'%';

                  

                }


            
          /*  if($cpersona==$k->codgp){

                $pla['horaseje'] = $objAct->sumHorasEjecutadasProy($k->cproyectoactividades);
                $tot = $objAct->sumHorasPresupuestadasProy($k->cproyectoactividades);
                $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100,2) );

                $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($k->cproyectoactividades,$k->cproyecto);
                $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($k->cproyectoactividades,$k->cproyecto);

                $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;

                $pla['porav_costo']=round($porcentajeCosto,2);

                $pla['mensajehoras']=$pla['porav'].'%';  
                $pla['mensajecosto']=$pla['porav_costo'].'%';

                 

            }

            else{


                $pla['horaseje'] = $objAct->sumHorasEjecutadasProyArea($k->cproyectoactividades,$carea);
                $tot = $objAct->sumHorasPresupuestadasProyArea($k->cproyectoactividades,$carea);
                $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100,2) );

                $tot_costo_presup=$objAct->sumCostoPresupuestadoProyArea($k->cproyectoactividades,$k->cproyecto,$carea);
                $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProyArea($k->cproyectoactividades,$k->cproyecto,$carea);

               // dd($tot_costo_ejecutado,$tot_costo_presup, $pla['porav']);

                if($tot_costo_presup==0|| is_null($tot_costo_presup)){

                    $pla['horaseje'] = $objAct->sumHorasEjecutadasProy($k->cproyectoactividades);
                    $tot = $objAct->sumHorasPresupuestadasProy($k->cproyectoactividades);
                    $pla['porav'] =   ($tot->suma==0 || is_null($tot->suma)?0:round(($pla['horaseje']/$tot->suma)*100,2) );

                    $tot_costo_presup=$objAct->sumCostoPresupuestadoProy($k->cproyectoactividades,$k->cproyecto);
                    $tot_costo_ejecutado=$objAct->sumCostoEjecutadoProy($k->cproyectoactividades,$k->cproyecto);

                    $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                     $pla['porav_costo']=round($porcentajeCosto,2);
                   

                    $pla['mensajehoras']='Horas general';  
                    $pla['mensajecosto']='No tiene monto Presupuestado';

                    //$porcentajeCosto=100;

                }
                else{

                    $porcentajeCosto=($tot_costo_ejecutado/$tot_costo_presup)*100;
                    $pla['porav_costo']=round($porcentajeCosto,2);
                    

                    $pla['mensajehoras']=$pla['porav'].'%';  
                    $pla['mensajecosto']=$pla['porav_costo'].'%';

                  

                }

               
                
            }*/

            $pla['tot']= $tot->suma;
            $pla['tipo']  = $k->tipo; 
            
            
            
            $actividadesp = DB::table('tproyectoejecucion as eje')
            ->where('eje.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
            ->whereBetween('eje.fplanificado',array($fecha1,$fecha2));
            $actividadesp=$actividadesp->select('eje.*',DB::raw('to_char(eje.fplanificado,\'YYYYMMDD\' ) as fpla'))
            ->addSelect(DB::raw('to_char(eje.fplanificado,\'YYYY-MM-DD\' ) as fec'),DB::raw('to_char(eje.fplanificado,\'D\' ) as dia'))
            ->orderBy('fpla','ASC')
            ->get();

            foreach($actividadesp as $ej){
                /*
                Obtener Historial de Aprobaciones
                */
                $taprob = DB::table('tflujoaprobacion as apro')
                ->leftJoin('tpersona as per','apro.cpersona_aprobado','=','per.cpersona')
                ->where('apro.cproyectoejecucion','=',$ej->cproyectoejecucion)
                ->select('apro.*','per.nombre as emp')
                ->get();
                foreach($taprob as $aprob){
                    $his['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $his['fecha']=$aprob->fregistro;
                    $his['ccondicionoperativa']=$aprob->ccondicionoperativa_aprobado;
                    $his['ccondicionoperativa_sig']=$aprob->ccondicionoperativa_siguiente;
                    $his['aprobado']=$aprob->emp;
                    $his['actividad']=$k->descripcionactividad;
                    $his['horas']=$ej->horasejecutadas;                    
                    $historialApro[count($historialApro)] = $his;
                }
                /*  Fin Historial de aprobaciones*/

                /*
                Obtener Historial de Comentarios
                */
                if( strlen($ej->obsplanificada)>0 || strlen($ej->obsejecutadas)>0 || strlen($ej->obsobservadas)>0 ){
                    $hic['cproyectoejecucion']=$ej->cproyectoejecucion;
                    $hic['actividad']=$pla['nombreproy'] . "/" .$pla['unidadminera'] ."/" .$k->descripcionactividad;
                    $hic['horas']=$ej->horasejecutadas;
                    $hic['fecha']=$ej->fplanificado;
                    $hic['comentariosPla']=$ej->obsplanificada;
                    $hic['comentariosEje']=$ej->obsejecutadas;
                    $hic['comentariosObs']=$ej->obsobservadas;
                    $historialCom[count($historialCom)] = $hic;
                }

                /*  Fin Historial de comentarios*/                  

                $pla[$ej->fpla]['cproyectoejecucion']=$ej->cproyectoejecucion;
                $pla[$ej->fpla]['estado'] = $ej->estado;
                $pla[$ej->fpla]['horasplanificadas'] = (is_null($ej->horasplanificadas)?'':$ej->horasplanificadas);
                $pla[$ej->fpla]['obsplanificada'] = $ej->obsplanificada;
                $pla[$ej->fpla]['horasejecutadas'] = (is_null($ej->horasejecutadas)?'':$ej->horasejecutadas);
                $pla[$ej->fpla]['obsejecutadas'] = $ej->obsejecutadas;
                $pla[$ej->fpla]['horasaprobadas'] = (is_null($ej->horasaprobadas)?'':$ej->horasaprobadas);
                $pla[$ej->fpla]['obsaprobadas'] = $ej->obsaprobadas;
                $pla[$ej->fpla]['horasobservadas'] = (is_null($ej->horasobservadas)?'':$ej->horasobservadas);
                $pla[$ej->fpla]['obsobservadas'] = $ej->obsobservadas;
                $pla[$ej->fpla]['cpersona_ejecuta'] = $ej->cpersona_ejecuta;
                $pla[$ej->fpla]['fejecuta'] = $ej->fejecuta;
                $pla[$ej->fpla]['fregistro'] =  $ej->fregistro;
                $pla[$ej->fpla]['faprobacion'] = $ej->faprobacion;
                $pla[$ej->fpla]['ordenaprobacion'] = $ej->ordenaprobacion;
                $pla[$ej->fpla]['crol_poraprobar'] = $ej->crol_poraprobar;
                $pla[$ej->fpla]['cpersona'] = $ej->cpersona;
                $pla[$ej->fpla]['fplanificado'] = $ej->fplanificado;
                $pla[$ej->fpla]['cestructuraproyecto'] = $ej->cestructuraproyecto;
                $pla[$ej->fpla]['diasemana'] = $ej->dia; 

               

                /*$resReab = array_search($ej->ccondicionoperativa,$aNivelReapertura);
                
                if($resReab===FALSE){
                }else{
                    
                    $reabrir=true;
                }*/
                switch ($ej->estado) {
                            case '1':
                                # code...
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasplanificadas)==1?$ej->horasplanificadas:0;
                                        break;                            
                                }
                                break;
                            case '2':
                            case '3':
                            case '4':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;
                            
                            
                            case '5':
                                switch ($k->tipo) {
                                    case '1':
                                        # code...
                                        $sumFF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;
                                    case '2':
                                        # code...
                                        $sumNF+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                        
                                    case '3':
                                        # code...
                                        $sumAN+= isset($ej->horasejecutadas)==1?$ej->horasejecutadas:0;
                                        break;                            
                                }
                                break;                      
                        
                }   
                              
                
            }  
      
            $alinea[count($alinea)]=$pla;



        }

       // dd($alinea);
        /* fin tareas de proyecto */
        $objEmp = new EmpleadoSupport();
        $nroHoras = $objEmp->getCargabilidadPlani($idParticipante,$fecha1,$fecha2);
        $nroHorasCar = $objEmp->getCargabilidadEje($idParticipante,$fecha1,$fecha2);
        //return $idParticipante. "-".$fecha;
        //return $alinea;
        //dd($dias);
        return view('partials/modalHorasParticipante',compact('alinea','dias','per_nombre','semana','fecha1','fecha2','nroHoras','nroHorasCar','anio'));
    }


    
    public function printHRProy($almacenar,$idProyecto){


        $proyecto = DB::table('tproyecto as tp')
        ->leftjoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
        ->leftjoin('ttipoproyecto as ttp','tp.ctipoproyecto','=','ttp.ctipoproyecto')
        ->leftjoin('tpersona as tpgt','tp.cpersona_gerente','=','tpgt.cpersona')
        ->leftjoin('ttipogestionproyecto as tpg','tp.ctipogestionproyecto','=','tpg.ctipogestionproyecto')
        ->where('cproyecto','=',$idProyecto)
        ->select('tp.*','ts.descripcion as servicio','ttp.descripcion as tipoproyecto','tpgt.abreviatura as gerente','tpg.descripcion as tipgestion')
        ->first();

        if (is_null($proyecto->finicio)) {
            $feiniPla=null;            
        }

        else{
            $feiniPla=new Carbon($proyecto->finicio);
            $feiniPla=$feiniPla->format('d-m-Y');            
        }


        if (is_null($proyecto->fcierre)) {
            $fcierrePla=null;            
        }

        else{
            $fcierrePla=new Carbon($proyecto->fcierre);
            $fcierrePla=$fcierrePla->format('d-m-Y');          
        }


        if (is_null($proyecto->finicioreal)) {
            $feiniReal=null;            
        }

        else{
            $feiniReal=new Carbon($proyecto->finicioreal);
            $feiniReal=$feiniReal->format('d-m-Y');         
        }

        if (is_null($proyecto->fcierrereal)) {
            $fcierreReal=null;            
        }

        else{
            $fcierreReal=new Carbon($proyecto->fcierrereal);
            $fcierreReal=$fcierreReal->format('d-m-Y');
    
        }
      
        $direccion_cli=  DB::table('tpersonadirecciones as tp')
        ->where('tp.cpersona','=',$proyecto->cpersonacliente)      
        ->get();


        $direcli=null;

        foreach ($direccion_cli as $dc) {
            if($dc->ctipodireccion=='FIS'){
                $direcli=$dc->direccion;
            }
            elseif ($dc->ctipodireccion=='COM') {
               
                $direcli=$dc->direccion;
          
            }
            else{
                $direcli=null;
            }
        }

        $persona_cliente = DB::table('tpersona as tp')
        ->leftjoin('tpersonajuridicainformacionbasica as tpj','tp.cpersona','=','tpj.cpersona')
        ->where('tp.cpersona','=',$proyecto->cpersonacliente)       
        ->select('tp.*','tpj.telefono','tpj.razonsocial')
        ->first();
        //dd($persona_cli);
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();            

        $propuesta=DB::table('tpropuesta as pro')
        ->where('pro.cproyecto','=',$idProyecto)
        ->select('pro.*',DB::raw("to_char(pro.fadjucicacion,'DD-MM-YY') as fadjucicacion"))
        ->first();      


        if(!($proyecto->cestadoproyecto == '001')){
            //$personal_gp=$personal;                   
        }  
        
        $objEmp = new EmpleadoSupport();
        $per_contproy=$objEmp->getPersonaProyectoXDisciplina($idProyecto,'10');

        $per_contdoc=$objEmp->getPersonaProyectoXDisciplina($idProyecto,'11');

     

        $monedas = DB::table('tmonedas')->where('cmoneda','=',$proyecto->cmoneda)->first();


        $proy_adicional = DB::table('tproyectoinformacionadicional as prya')      
        ->leftjoin('tformacotizacion as tf','prya.cformacotizacion','=','tf.cformacotizacion')  
        ->leftjoin('tpersona as l1','prya.lider1','=','l1.cpersona')
        ->leftjoin('tpersona as l2','prya.lider2','=','l2.cpersona')
        ->where('prya.cproyecto','=',$idProyecto)
        ->select('prya.*','l1.abreviatura as li1','l2.abreviatura as li2','tf.descripcion as formacotiza')
        ->first();
        if($proy_adicional){
            $uminerac = DB::table('tunidadmineracontactos as umc')
            ->leftjoin('tcontactocargo as tc','umc.ccontactocargo','=','tc.ccontactocargo')
            ->leftjoin('ttiposcontacto as ttc','umc.ctipocontacto','=','ttc.ctipocontacto')
            ->where('umc.cunidadmineracontacto','=',$proy_adicional->cunidadmineracontacto)
            ->select('umc.*','tc.descripcion as contcargo','ttc.descripcion as tipocont')
            ->first();

           //dd($uminerac);


            $totalHon=$proy_adicional->hon_and+$proy_adicional->hon_con1+$proy_adicional->hon_con2;
            $totalHon=number_format($totalHon,2,'.','');
            $totalGast=$proy_adicional->gast_and+$proy_adicional->gast_con1+$proy_adicional->gast_con2;
            $totalGast=number_format($totalGast,2,'.','');
            $totalLab=$proy_adicional->lab_and+$proy_adicional->lab_con1+$proy_adicional->lab_con2;
            $totalLab=number_format($totalLab,2,'.','');
            $totalDesc=$proy_adicional->desc_and+$proy_adicional->desc_con1+$proy_adicional->desc_con2;
            $totalDesc=number_format($totalDesc,2,'.','');
            $totalOtr=$proy_adicional->otr_and+$proy_adicional->otr_con1+$proy_adicional->otr_con2;
            $totalOtr=number_format($totalOtr,2,'.','');


            $totalAnddes=$proy_adicional->hon_and+$proy_adicional->gast_and+$proy_adicional->lab_and+$proy_adicional->desc_and+$proy_adicional->otr_and;
            $totalAnddes=number_format($totalAnddes,2,'.','');
            $totalSubCont1=$proy_adicional->hon_con1+$proy_adicional->gast_con1+$proy_adicional->lab_con1+$proy_adicional->desc_con1+$proy_adicional->otr_con1;
            $totalSubCont1=number_format($totalSubCont1,2,'.','');
            $totalSubCont2=$proy_adicional->hon_con2+$proy_adicional->gast_con2+$proy_adicional->lab_con2+$proy_adicional->desc_con2+$proy_adicional->otr_con2;
            $totalSubCont2=number_format($totalSubCont2,2,'.','');
            $totalPresupuesto=floatval($totalAnddes)+floatval($totalSubCont1)+floatval($totalSubCont2);
            $totalPresupuesto=number_format($totalPresupuesto,2,'.','');

            /*if (is_null($proy_adicional->fechacorte)) {
            $fechacorte=null;            
            }

            else{
                $fechacorte=new Carbon($proy_adicional->fechacorte);
                $fechacorte=$fechacorte->format('d-m-Y');
        
            }*/

            $tcliente_final= DB::table('tpersonajuridicainformacionbasica as jur') 
            ->where('jur.cpersona','=',$proy_adicional->cliente_final)
            ->first();
            

            $lugartrabajo= DB::table('tlugartrabajo')        
            ->where('clugartrabajo','=',$proy_adicional->clugartrabajo)
            ->first();

            
        }

        $documento = DB::table('tproyectodocumentos as prydoc')
        ->leftjoin('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','HR')
        ->select('prydoc.cproyectodocumentos','prydoc.fechadoc','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first(); 

        if($documento){
            if (is_null($documento->fechadoc)) {
                $fechadoc=null;            
            }
            else{
                $fechadoc=new Carbon($documento->fechadoc);
                $fechadoc=$fechadoc->format('d-m-Y');
                //$fechadoc = Right($fechadoc, 2);
            }     
        }

        //dd(substr($fechadoc, 0,6).substr($fechadoc, -2));
      

        //-------------------------------------------------------------------------------//
       

        $view =  \View::make('proyecto/printHRProyecto',compact('proyecto','persona_cli','uminera','propuesta','per_contproy','per_contdoc','nombreLider','proy_adicional','feiniPla','fcierrePla','feiniReal','fcierreReal','uminerac','direcli','totalHon','totalGast','totalLab','totalDesc','totalOtr','totalAnddes','totalSubCont1','totalSubCont2','totalPresupuesto','monedas','documento','fechadoc','persona_cliente','tcliente_final','lugartrabajo'))->render();
        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','landscape');

        $ruta = public_path('archivos/proyectos/');
        $nombreArchivo=$proyecto->cproyecto.'_R'.$documento->revision.'.pdf';       
        $output = $pdf->output(); // Obtener el PDF generado

        if($almacenar=='1'){
            if (!is_dir($ruta.$proyecto->cproyecto)) {

                mkdir($ruta.$proyecto->cproyecto, 0777,true);
                chmod($ruta.$proyecto->cproyecto, 0777);               
                
            }

            file_put_contents( $ruta.$proyecto->cproyecto.'/'.$nombreArchivo, $output);
        }


        return $pdf->stream($nombreArchivo);

    }


     public function printHRandSaveProy($almacenar,$idProyecto){


        $proyecto = DB::table('tproyecto as tp')
        ->leftjoin('tservicio as ts','tp.cservicio','=','ts.cservicio')
        ->leftjoin('ttipoproyecto as ttp','tp.ctipoproyecto','=','ttp.ctipoproyecto')
        ->leftjoin('tpersona as tpgt','tp.cpersona_gerente','=','tpgt.cpersona')
        ->leftjoin('ttipogestionproyecto as tpg','tp.ctipogestionproyecto','=','tpg.ctipogestionproyecto')
        ->where('cproyecto','=',$idProyecto)
        ->select('tp.*','ts.descripcion as servicio','ttp.descripcion as tipoproyecto','tpgt.abreviatura as gerente','tpg.descripcion as tipgestion')
        ->first();

        if (is_null($proyecto->finicio)) {
            $feiniPla=null;            
        }

        else{
            $feiniPla=new Carbon($proyecto->finicio);
            $feiniPla=$feiniPla->format('d-m-Y');            
        }


        if (is_null($proyecto->fcierre)) {
            $fcierrePla=null;            
        }

        else{
            $fcierrePla=new Carbon($proyecto->fcierre);
            $fcierrePla=$fcierrePla->format('d-m-Y');          
        }


        if (is_null($proyecto->finicioreal)) {
            $feiniReal=null;            
        }

        else{
            $feiniReal=new Carbon($proyecto->finicioreal);
            $feiniReal=$feiniReal->format('d-m-Y');         
        }

        if (is_null($proyecto->fcierrereal)) {
            $fcierreReal=null;            
        }

        else{
            $fcierreReal=new Carbon($proyecto->fcierrereal);
            $fcierreReal=$fcierreReal->format('d-m-Y');
    
        }
      
        $direccion_cli=  DB::table('tpersonadirecciones as tp')
        ->where('tp.cpersona','=',$proyecto->cpersonacliente)      
        ->get();


        $direcli=null;

        foreach ($direccion_cli as $dc) {
            if($dc->ctipodireccion=='FIS'){
                $direcli=$dc->direccion;
            }
            elseif ($dc->ctipodireccion=='COM') {
               
                $direcli=$dc->direccion;
          
            }
            else{
                $direcli=null;
            }
        }

        $persona_cliente = DB::table('tpersona as tp')
        ->leftjoin('tpersonajuridicainformacionbasica as tpj','tp.cpersona','=','tpj.cpersona')
        ->where('tp.cpersona','=',$proyecto->cpersonacliente)       
        ->select('tp.*','tpj.telefono','tpj.razonsocial')
        ->first();
        //dd($persona_cli);
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();            

        $propuesta=DB::table('tpropuesta as pro')
        ->where('pro.cproyecto','=',$idProyecto)
        ->select('pro.*',DB::raw("to_char(pro.fadjucicacion,'DD-MM-YY') as fadjucicacion"))
        ->first();       


        if(!($proyecto->cestadoproyecto == '001')){
            //$personal_gp=$personal;                   
        }  
        
        $objEmp = new EmpleadoSupport();
        $per_contproy=$objEmp->getPersonaProyectoXDisciplina($idProyecto,'10');

        $per_contdoc=$objEmp->getPersonaProyectoXDisciplina($idProyecto,'11');

     

        $monedas = DB::table('tmonedas')->where('cmoneda','=',$proyecto->cmoneda)->first();


        $proy_adicional = DB::table('tproyectoinformacionadicional as prya')      
        ->leftjoin('tformacotizacion as tf','prya.cformacotizacion','=','tf.cformacotizacion')  
        ->leftjoin('tpersona as l1','prya.lider1','=','l1.cpersona')
        ->leftjoin('tpersona as l2','prya.lider2','=','l2.cpersona')
        ->where('prya.cproyecto','=',$idProyecto)
        ->select('prya.*','l1.abreviatura as li1','l2.abreviatura as li2','tf.descripcion as formacotiza')
        ->first();
        if($proy_adicional){
            $uminerac = DB::table('tunidadmineracontactos as umc')
            ->leftjoin('tcontactocargo as tc','umc.ccontactocargo','=','tc.ccontactocargo')
            ->leftjoin('ttiposcontacto as ttc','umc.ctipocontacto','=','ttc.ctipocontacto')
            ->where('umc.cunidadmineracontacto','=',$proy_adicional->cunidadmineracontacto)
            ->select('umc.*','tc.descripcion as contcargo','ttc.descripcion as tipocont')
            ->first();

           //dd($uminerac);


            $totalHon=$proy_adicional->hon_and+$proy_adicional->hon_con1+$proy_adicional->hon_con2;
            $totalHon=number_format($totalHon,2,'.','');
            $totalGast=$proy_adicional->gast_and+$proy_adicional->gast_con1+$proy_adicional->gast_con2;
            $totalGast=number_format($totalGast,2,'.','');
            $totalLab=$proy_adicional->lab_and+$proy_adicional->lab_con1+$proy_adicional->lab_con2;
            $totalLab=number_format($totalLab,2,'.','');
            $totalDesc=$proy_adicional->desc_and+$proy_adicional->desc_con1+$proy_adicional->desc_con2;
            $totalDesc=number_format($totalDesc,2,'.','');
            $totalOtr=$proy_adicional->otr_and+$proy_adicional->otr_con1+$proy_adicional->otr_con2;
            $totalOtr=number_format($totalOtr,2,'.','');


            $totalAnddes=$proy_adicional->hon_and+$proy_adicional->gast_and+$proy_adicional->lab_and+$proy_adicional->desc_and+$proy_adicional->otr_and;
            $totalAnddes=number_format($totalAnddes,2,'.','');
            $totalSubCont1=$proy_adicional->hon_con1+$proy_adicional->gast_con1+$proy_adicional->lab_con1+$proy_adicional->desc_con1+$proy_adicional->otr_con1;
            $totalSubCont1=number_format($totalSubCont1,2,'.','');
            $totalSubCont2=$proy_adicional->hon_con2+$proy_adicional->gast_con2+$proy_adicional->lab_con2+$proy_adicional->desc_con2+$proy_adicional->otr_con2;
            $totalSubCont2=number_format($totalSubCont2,2,'.','');
            $totalPresupuesto=floatval($totalAnddes)+floatval($totalSubCont1)+floatval($totalSubCont2);
            $totalPresupuesto=number_format($totalPresupuesto,2,'.','');

            /*if (is_null($proy_adicional->fechacorte)) {
            $fechacorte=null;            
            }

            else{
                $fechacorte=new Carbon($proy_adicional->fechacorte);
                $fechacorte=$fechacorte->format('d-m-Y');
        
            }*/

            $tcliente_final= DB::table('tpersonajuridicainformacionbasica as jur') 
            ->where('jur.cpersona','=',$proy_adicional->cliente_final)
            ->first();
            

            $lugartrabajo= DB::table('tlugartrabajo')        
            ->where('clugartrabajo','=',$proy_adicional->clugartrabajo)
            ->first();

            
        }

        $documento = DB::table('tproyectodocumentos as prydoc')
        ->leftjoin('tdocumentosparaproyecto as tdoc','prydoc.cdocumentoparapry','=','tdoc.cdocumentoparapry')
        /*->join('ttiposdocproyecto as tipo','tdoc.ctipodocproy','=','tdoc.ctipodocproy')*/
        ->where('prydoc.cproyecto','=',$proyecto->cproyecto)
        ->where('tdoc.codigo','=','HR')
        ->select('prydoc.cproyectodocumentos','prydoc.fechadoc','prydoc.codigodocumento as codigo','prydoc.revisionactual as revision')
        ->orderBy('prydoc.fechadoc','desc')
        ->first(); 

        if($documento){
            if (is_null($documento->fechadoc)) {
                $fechadoc=null;            
            }
            else{
                $fechadoc=new Carbon($documento->fechadoc);
                $fechadoc=$fechadoc->format('d-m-Y');
            }     
        }

        //dd($documento);
      

        //-------------------------------------------------------------------------------//
       

        $view =  \View::make('proyecto/printHRProyecto',compact('proyecto','persona_cli','uminera','propuesta','per_contproy','per_contdoc','nombreLider','proy_adicional','feiniPla','fcierrePla','feiniReal','fcierreReal','uminerac','direcli','totalHon','totalGast','totalLab','totalDesc','totalOtr','totalAnddes','totalSubCont1','totalSubCont2','totalPresupuesto','monedas','documento','fechadoc','persona_cliente','tcliente_final','lugartrabajo'))->render();
        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','landscape');

        $ruta = public_path('archivos/proyectos/');
        $nombreArchivo=$proyecto->cproyecto.'_R'.$documento->revision.'.pdf';       
        $output = $pdf->output(); // Obtener el PDF generado

        if($almacenar=='1'){
            if (!is_dir($ruta.$proyecto->cproyecto)) {

                mkdir($ruta.$proyecto->cproyecto, 0777,true);
                chmod($ruta.$proyecto->cproyecto, 0777);               
                
            }

            file_put_contents( $ruta.$proyecto->cproyecto.'/'.$nombreArchivo, $output);
        }


        return $documento->revision;
    }
    

    function getStamp(){
        $now = (string)microtime();
        $now = explode(' ', $now);
        $mm = explode('.', $now[0]);
        $mm = $mm[1];
        $now = $now[1];
        $segundos = $now % 60;
        $segundos = $segundos < 10 ? "$segundos" : $segundos;
        return strval(date("YmdHi",mktime(date("H"),date("i"),date("s"),date("m"),date("d"),date("Y"))) . "$segundos$mm");
    }



public function printLAProy($idProyecto){
    $proyecto = DB::table('tproyecto as tp')
    ->leftjoin('tpersona as tpgt','tp.cpersona_gerente','=','tpgt.cpersona')
    ->select('tp.*','tpgt.abreviatura as gerente')
    ->where('cproyecto','=',$idProyecto)
    ->first();

        //$listLecApre=array();
        // Session::forget('listLecApre');  
        // Session::forget('delLecAprend');            

        $persona  = DB::table('tpersona')
        ->where('cpersona','=',$proyecto->cpersonacliente)->first();
        // dd($proyecto);

        $uminera = DB::table('tunidadminera as um')
        ->leftjoin('tpais as pa','pa.cpais','=','um.cpais')
        ->where('um.cpersona','=',$proyecto->cpersonacliente)
        ->where('um.cunidadminera','=',$proyecto->cunidadminera)
        ->select('um.*','pa.descripcion as pais')
        ->first();
        // dd($uminera);
        $ubicacion='';

                $tdepa='';
                $tprovi='';
                $tdist='';
        if(strlen($uminera->cubigeo)>0 && $uminera->cpais=='PER' ){           
                $dpto=substr($uminera->cubigeo,0,2).'0000';
                $prov=substr($uminera->cubigeo,0,4).'00';

                $tdpto = Tubigeo::where('cpais','=',$uminera->cpais)
                ->where('cubigeo','=',$dpto)
                ->first();


                if($tdpto){
                    $tdepa = ' - '.$tdpto->descripcion;
                }

                $tprov= Tubigeo::where('cpais','=',$uminera->cpais)
                ->where('cubigeo','=',$prov)
                ->first();

                if($tprov){
                    $tprovi = ' - '.$tprov->descripcion;
                }

                $tdis= Tubigeo::where('cpais','=',$uminera->cpais)
                ->where('cubigeo','=',$uminera->cubigeo)
                ->first();

                if($tdis){
                    $tdist =' - '.$tdis->descripcion;
                }

            }
            $ubicacion=$uminera->pais.$tdepa.$tprovi.$tdist;


            // dd($ubicacion);

            

        $fase = DB::table('tfasesproyecto')->get();

        $lecaprend=array();

        $tleccionesaprendidas=DB::table('tleccionesaprendidas as tle')
        ->leftJoin('tareas as ta','tle.carea','=','ta.carea')
        ->leftJoin('tfasesproyecto as tfp','tle.cfaseproyecto','=','tfp.cfaseproyecto')
        ->leftJoin('tareasconocimiento as tac','tle.careaconocimiento','=','tac.careaconocimiento')
        ->leftJoin('tpersona as tp','tle.cpersona_elaborado','=','tp.cpersona')
        // ->Join('tproyectopersona as pyper','tp.cpersona','=','pyper.cpersona')
        // ->Join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        ->leftJoin('tpersonanaturalinformacionbasica as tpni','tp.cpersona','=','tpni.cpersona')
        ->leftJoin('troldespliegue as tr','tle.crol_elaborado','=','tr.croldespliegue')
        ->where('tle.cproyecto','=',$idProyecto)
        ->where('tpni.esempleado','=','1')
        ->orderBy('tle.cleccionaprendida','ASC')
        ->select('tle.cleccionaprendida','tle.item','tle.descripcion','tle.tag','tle.fecha','tle.carea','ta.descripcion as area','tle.cproyecto','tle.cfaseproyecto','tfp.descripcion as fase','tle.careaconocimiento','tac.descripcion as areaconocimiento','tle.suceso','tle.hizo','tle.debiohacer','tle.cpersona_elaborado','tp.nombre as elaborado','tle.crol_elaborado','tle.cpersona_aprobado','tp.nombre as aprobado','tr.descripcionrol','tp.cpersona','tp.abreviatura',DB::raw("to_char(tle.fecha,'DD-MM-YY') as fec"))
        ->get();

        $i=0;

        foreach($tleccionesaprendidas as $tlec){

            $i++;

            $personala = DB::table('tpersona as per ')
        ->join('tpersonadatosempleado as tper','per.cpersona','=','tper.cpersona')
        ->join('tareas as a','tper.carea','=','a.carea')
        ->join('tproyectopersona as pyper','per.cpersona','=','pyper.cpersona')
        ->join('tcategoriaprofesional as cat','cat.ccategoriaprofesional','=','pyper.ccategoriaprofesional')
        // ->select('a.descripcion as area','per.abreviatura as nombre','cat.descripcion as categoria')
        ->where('tper.cpersona','=',$tlec->cpersona)
        ->first();

            $lea['cleccionaprendida']= $tlec->cleccionaprendida;
            $lea['item']= $i;
            $lea['tag']= $tlec->tag;
            $lea['descripcion']= $tlec->descripcion;
            $lea['fecha']= $tlec->fec;
            $lea['area']= $tlec->area;
            $lea['carea']= $tlec->carea;
            //$lea['cproyecto']=$tlec->cproyecto;
            $lea['cfaseproyecto']= $tlec->cfaseproyecto;
            $lea['fase']= $tlec->fase;
            $lea['careaconocimiento']= $tlec->careaconocimiento;
            $lea['areaconocimiento']= $tlec->areaconocimiento;
            $lea['suceso']= $tlec->suceso;
            $lea['hizo']= $tlec->hizo;
            $lea['debiohacer']= $tlec->debiohacer;
            $lea['cpersona_elaborado']= $tlec->cpersona_elaborado;
            $lea['elaborado']= $tlec->abreviatura;
            $lea['crol_elaborado']= $tlec->crol_elaborado;
            // $lea['descripcionrol']= $tlec->descripcionrol;
            // $lea['concepto']= $personala->concepto;
            $lea['cpersona_aprobado']= $tlec->cpersona_aprobado;
            $lea['aprobado']= $tlec->aprobado;
            $lecaprend[count($lecaprend)]=$lea;

        }
        // Session::put('listLecApre',$lecaprend);

        $view =  \View::make('proyecto/printLAProyecto',compact('proyecto','persona','uminera','lecaprend','fase','ubicacion'))->render();
        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','landscape');

         return $pdf->stream($proyecto->codigo.'-AND-25-LA-001.pdf');
}


public function planificacionProy(Request $request){

    return view('proyecto/planificacionProyectos');

}

public function visualizarPlanificacion(Request $request){

    $semana_act=date("W");
    $semana_ini=date("W");
    $semana_fin=date("W");
    $dia_act = date("Ymd");
    
    $fechaDesde=$request->input('fdesde');
    $fechaHasta=$request->input('fhasta');

    if(substr($fechaDesde,2,1)=='/' || substr($fechaDesde,2,1)=='-'){    
        $fechaDesde = Carbon::create(substr($fechaDesde,6,4),substr($fechaDesde,3,2),substr($fechaDesde,0,2));
        $fechaHasta = Carbon::create(substr($fechaHasta,6,4),substr($fechaHasta,3,2),substr($fechaHasta,0,2));

    }else{
        $fechaDesde = Carbon::create(substr($fechaDesde,0,4),substr($fechaDesde,5,2),substr($fechaDesde,8,2));
        $fechaHasta = Carbon::create(substr($fechaHasta,0,4),substr($fechaHasta,5,2),substr($fechaHasta,8,2));
    }
    $fechaDesde = $fechaDesde->startOfWeek();
    $fechaHasta = $fechaHasta->endOfWeek();
    $semana_ini = $fechaDesde->weekOfYear;
    $semana_fin = $fechaHasta->weekOfYear;

    $anio=$fechaDesde->year;

    $cantSemanas=0;
    $cantSemanas=intval($semana_fin-$semana_ini);  

    $semanasPlani=array();
   
    $sem_inicio=intval($semana_ini);

    $total_semanas=$sem_inicio+$cantSemanas;

    for ($i=$sem_inicio; $i <=$total_semanas ; $i++) { 
        $semanasPlani[count($semanasPlani)]=$i;
    }
    //dd($semanasPlani);

    $user = Auth::user();
    
        //dd($prueba);

    $tarea = DB::table('tpersonadatosempleado as e')
    ->leftjoin('tareas as ta','e.carea','=','ta.carea')
    ->where('cpersona','=',$user->cpersona)
    ->whereNull('ftermino')
    ->orderBy('fingreso','DESC')
    ->first();
    $carea=null;
    $carea_parent=null;
    
    if($tarea){
        $carea = $tarea->carea;
        $carea_parent=$tarea->carea_parent;
    }
    
    $todColaboradorGer=DB::table('tpersona as per')
    ->leftjoin('tpersonadatosempleado as emp','per.cpersona','=','emp.cpersona')
    ->leftjoin('tareas as ta','emp.carea','=','ta.carea')       
    ->leftjoin('tcategoriaprofesional as tcp','emp.ccategoriaprofesional','=','tcp.ccategoriaprofesional')
    ->where('emp.estado','=','ACT')
    ->Where('ta.carea','=',$carea_parent)
    ->orderBy('tcp.orden','asc') 
    ->orderBy('per.abreviatura','asc') 
    ->select('per.*','ta.carea','ta.codigo as codarea','ta.descripcion as area','tcp.orden','tcp.descripcion as categoria')        
    ->get();

    $aTodcolger=array();
    foreach ($todColaboradorGer as $c) {
        //dd($todColaboradorGer);
        if(substr($c->codarea,0,2)=='GT'){
            $aTodcolger[count($aTodcolger)]=$c;      
        }  
           
    }

    $todColaboradores=DB::table('tpersona as per')
    ->leftjoin('tpersonadatosempleado as emp','per.cpersona','=','emp.cpersona')
    ->leftjoin('tareas as ta','emp.carea','=','ta.carea')
    ->leftjoin('tcategoriaprofesional as tcp','emp.ccategoriaprofesional','=','tcp.ccategoriaprofesional')
    ->where('emp.estado','=','ACT')
    ->where('ta.carea','=',$carea)     
    ->orderBy('tcp.orden','asc')         
    ->select('per.*','ta.carea','ta.descripcion as area','tcp.orden','tcp.descripcion as categoria')
    ->get();

    $aTodcol=array();
        foreach ($todColaboradores as $c) {
            $aTodcol[count($aTodcol)]=$c;            
    }  

        //Juntando los 2 arrays de colaboradores

    $listTodosColaboradores=array_merge($aTodcolger,$aTodcol);

        //dd($listTodosColaboradores);

    $acolCargSem=array();

    foreach ($listTodosColaboradores as $ltc) {

        foreach ($semanasPlani as $s) {

            $objEmp = new EmpleadoSupport();
            $cargabilidadeje = $objEmp->getCargabilidadEjeXSemana($ltc->cpersona,$s);
            $cargabilidadplani = $objEmp->getCargabilidadPlaniXSem($ltc->cpersona,$s);
            //var_dump($cargabilidadplani);
            //var_dump($ltc->cpersona);            
            //var_dump($s);            return;

            $ccs[$ltc->cpersona.'_'.$s]['pla']=$cargabilidadplani;
            $ccs[$ltc->cpersona.'_'.$s]['eje']=$cargabilidadeje;

            $tipocontrato=DB::table('tpersonadatosempleado as per')
            ->leftjoin('ttipocontrato as tc','per.ctipocontrato','=','tc.ctipocontrato')
            ->select('tc.horas')
            ->where('cpersona','=',$ltc->cpersona)
            ->first();

            $tipocontrato=$this->valorNull($tipocontrato->horas,'0.00');
            $tothoraseje = $this->valorNull($objEmp->totalCargaEjeXSem($ltc->cpersona,$s),'0.00');
            $tothoraspla = $this->valorNull($objEmp->totalCargaPlaniXSem($ltc->cpersona,$s),'0.00');

            $ccs[$ltc->cpersona.'_'.$s]['totdis']=number_format($tipocontrato,2,'.','')-number_format($tothoraseje,2,'.','');
            $ccs[$ltc->cpersona.'_'.$s]['totpla']=$tothoraspla;
            $ccs[$ltc->cpersona.'_'.$s]['toteje']=$tothoraseje;





            $acolCargSem=$ccs;

          
            $acolCargSem[count($acolCargSem)]=$ccs;
         
        }
        
    }

      //dd($acolCargSem);

    
    $tproyectos = DB::table('tproyecto as pry')
    ->join('tproyectopersona as pp','pp.cproyecto','=','pry.cproyecto')
    ->leftjoin('tunidadminera as tu','pry.cunidadminera','=','tu.cunidadminera')
    ->join('tproyectodisciplina as dis','pry.cproyecto','=','dis.cproyecto')
    ->where('pry.cestadoproyecto','=','001')
    ->where('pp.eslider','=','1')
    ->where('pp.cpersona','=',$user->cpersona)
    ->select('pry.cproyecto','pry.nombre','pry.codigo','tu.nombre as uminera')
    ->orderBy('pry.codigo','asc')
    ->distinct()
    ->get();

    $cantproy=null;

    $listaProy=array();

    foreach ($tproyectos as $tp) {
       $cantproy++;

        $pr['cproyecto']=$tp->cproyecto;
        $pr['nombreproy']=$tp->nombre;
       $pr['codproy']=$tp->codigo;
       $pr['uminera']=$tp->uminera;
      
        $colaboradorGer=DB::table('tproyectopersona as pp')
        ->join('tpersona as per','pp.cpersona','=','per.cpersona')
        ->join('tpersonadatosempleado as emp','pp.cpersona','=','emp.cpersona')
        ->join('tareas as ta','emp.carea','=','ta.carea')       
        ->join('tcategoriaprofesional as tcp','emp.ccategoriaprofesional','=','tcp.ccategoriaprofesional')
        ->where('emp.estado','=','ACT')
        ->where('pp.cproyecto','=',$tp->cproyecto)      
        ->Where('ta.carea','=',$carea_parent)
        ->orderBy('tcp.orden','asc') 
        ->orderBy('per.abreviatura','asc') 
        ->select('per.*','ta.codigo as codarea','ta.descripcion as area','tcp.orden')        
        ->get();

        $acolger=array();
        foreach ($colaboradorGer as $c) {
           // dd(substr($c->codarea,0,2));           
            if(substr($c->codarea,0,2)=='GT'){
            $acolger[count($acolger)]=$c;      
            } 
        }

        $colaboradores=DB::table('tproyectopersona as pp')
        ->join('tpersona as per','pp.cpersona','=','per.cpersona')
        ->join('tpersonadatosempleado as emp','pp.cpersona','=','emp.cpersona')
        ->join('tareas as ta','emp.carea','=','ta.carea')
        ->join('tareas as tap','ta.carea_parent','=','tap.carea')
        ->join('tcategoriaprofesional as tcp','emp.ccategoriaprofesional','=','tcp.ccategoriaprofesional')
        ->where('emp.estado','=','ACT')
        ->where('pp.cproyecto','=',$tp->cproyecto)
        ->where('ta.carea','=',$carea)
        ->orderBy('tcp.orden','asc')         
        ->select('per.*','ta.descripcion as area','tcp.orden')
        ->get();

        $acol=array();
        foreach ($colaboradores as $c) {
            $acol[count($acol)]=$c;            
        }  
        //Juntando los 2 arrays de colaboradores

        //dd($acol, $acolger);

        $listColaboradores=array_merge($acolger,$acol);
        $pr['colaboradores']=$listColaboradores;
        $listaProy[count($listaProy)]=$pr;
    }

    //dd($listaProy);
        

    //dd($semana_ini,$semana_fin,$tproyectos);

    return view('proyecto/planificacionProyectos',compact('tproyectos','cantproy','listaProy','dias','semanasPlani','anio','listTodosColaboradores','acolCargSem'));

}

public function getCargabilidadColaborador(Request $request){

    $objEmp = new EmpleadoSupport();
    $cargabilidadeje = $objEmp->getCargabilidadEjeXSemana($request->input('cpersona'),$request->input('semana'));
    $cargabilidadplani = $objEmp->getCargabilidadPlaniXSem($request->input('cpersona'),$request->input('semana'));

    return array($cargabilidadeje,$cargabilidadplani);

}

public function savePlanificacion(Request $request){

    dd($request);
    //$iniSem=date('Y-m-d', strtotime('01/01 +' . ($j - 1) . ' weeks first day +' .'1' . ' day'));


}

private function valorNull($valor, $defecto=null){
        if($valor!=null or $valor!="" or strlen($valor)<1)
            return $valor;
        return $defecto;
}

public function grabarEDTFila(Request $request){
    
    $edtParent=DB::table('tproyectoedt')
    ->where('cproyectoedt','=',$request->input('cproyectoedt_parent'))
    ->first();
    $nivelPadre=0;
    $tipoEdt=null;

    if ($edtParent) {
        $nivelPadre=$edtParent->nivel;
        $tipoEdt=$edtParent->ctipoedt;
    }

    $nivelPadre=intval($nivelPadre);

    DB::beginTransaction();

    $edt = new Tproyectoedt();
    $edt->cproyecto = $request->input('cproyecto');
    $edt->cproyectoedt_parent =$request->input('cproyectoedt_parent');
    $edt->nivel =$nivelPadre+1;
    $edt->ctipoedt =$tipoEdt;
    $edt->cproyectoent_rev =$request->input('cproyectoent_rev');
    $edt->save();

    DB::commit();   

    return $edt;
}

public function actualizarEDT(Request $request){

    $edt = Tproyectoedt::where('cproyectoedt','=',$request->input('cproyectoedt'))->first();

    if($request->input('actualizar')=="codigo"){
        $edt->codigo =$request->input('codigo');

        $edt->orden ='b'.$request->input('codigo');
    }
    if($request->input('actualizar')=="descripcion"){
        $edt->descripcion= $request->input('descripcion');
    }

    if($request->input('actualizar')=="fase"){
        $edt->cfaseproyecto = $this->valorNull($request->input('fase'),null);
    }
  
    $edt->save();

    return $edt;


}

public function grabarRevisionEntregable(Request $request){

    //dd($request->all());
    $edtProy=Tproyectoedt::where('cproyecto','=',$request->input('cproyecto'))
    ->where('cproyectoent_rev','=',$request->input('cod_revisionInt'))
    ->where('estado','=',1)
    ->get();


    $edt_clonados=[];
    foreach ($edtProy as $edt) {

        // Clonando EDT

        $edtNuevo = new Tproyectoedt(); 
        $edtNuevo->cproyecto =$edt->cproyecto;
        $edtNuevo->codigo =$edt->codigo;
        $edtNuevo->descripcion=$edt->descripcion;
        $edtNuevo->tipo =$edt->tipo;
        $edtNuevo->ctipoedt=$edt->ctipoedt;
        $edtNuevo->nivel=$edt->nivel;
        $edtNuevo->orden=$edt->orden;
        $edtNuevo->cfaseproyecto=$edt->cfaseproyecto;
        $edtNuevo->cproyectoent_rev =$edt->cproyectoent_rev;
        $edtNuevo->referencia_edt =$edt->cproyectoedt;
        $edtNuevo->save();

        array_push($edt_clonados,$edtNuevo->cproyectoedt);

        // Inicio actualizar padre de EDT

        if(strlen($edt->cproyectoedt_parent)>0){

            // Buscando padre de edt original en la referencia de edt clonado

            $edtPadre=Tproyectoedt::where('referencia_edt','=',$edt->cproyectoedt_parent) 
            ->whereIn('cproyectoedt',$edt_clonados)         
            ->first();

            // Extrae codigo de padre clonado

            $cedtpadre=null;
            if($edtPadre){
                $cedtpadre=$edtPadre->cproyectoedt;
            }
            // Actualiza el codigo padre de edt 
            Tproyectoedt::where('cproyectoedt','=',$edtNuevo->cproyectoedt)->update(['cproyectoedt_parent'=>$cedtpadre]);
            
        }

        // Fin actualizar padre de EDT


        $entregables=Tproyectoentregable::where('cproyectoedt','=',$edt->cproyectoedt)->get();

        $ent_clonados=[];

        foreach ($entregables as $ent) {

            $entNuevo = new Tproyectoentregable();
            $entNuevo->cproyecto = $ent->cproyecto;
            $entNuevo->centregable = $ent->centregable;
            $entNuevo->cproyectoedt = $edtNuevo->cproyectoedt;
            $entNuevo->observacion = $ent->observacion;
            $entNuevo->cpersona_responsable = $ent->cpersona_responsable;
            $entNuevo->crol_responsable = $ent->crol_responsable;
            $entNuevo->codigo = $ent->codigo;
            $entNuevo->cestadoentregable = $ent->cestadoentregable;
            $entNuevo->ccategoriaentregable = $ent->ccategoriaentregable;
            $entNuevo->revision= $ent->revision;
            $entNuevo->fecha_elaboracion= $ent->fecha_elaboracion;
            $entNuevo->cper_elabora= $ent->cper_elabora;
            $entNuevo->fecha_modificacion= $ent->fecha_modificacion;
            $entNuevo->cper_modifica= $ent->cper_modifica;
            $entNuevo->ctipoproyectoentregable = $ent->ctipoproyectoentregable;
            $entNuevo->cfaseproyecto = $ent->cfaseproyecto;
            $entNuevo->descripcion_entregable = $ent->descripcion_entregable;
            $entNuevo->cpersona_revisor = $ent->cpersona_revisor;
            $entNuevo->cpersona_aprobador = $ent->cpersona_aprobador;
            $entNuevo->codigocliente = $ent->codigocliente;
            $entNuevo->referencia_entregable = $ent->cproyectoentregables;
            $entNuevo->ruteo = $ent->ruteo;
            $entNuevo->ruta = $ent->ruta;
            $entNuevo->cestadoentregableruteo = $ent->cestadoentregableruteo;
            $entNuevo->save(); 

            array_push($ent_clonados,$entNuevo->cproyectoentregables);

            if(strlen($ent->cproyectoentregables_parent)>0){

                $entregablePadre=Tproyectoentregable::where('referencia_entregable','=',$ent->cproyectoentregables_parent)
                ->whereIn('cproyectoentregables',$ent_clonados)  
                ->first();


                $centpadre=null;
                if($entregablePadre){
                    $centpadre=$entregablePadre->cproyectoentregables;
                }

                Tproyectoentregable::where('cproyectoentregables','=',$entNuevo->cproyectoentregables)->update(['cproyectoentregables_parent'=>$centpadre]);

            }

            Tproyectoentregable::where('cproyectoentregables','=',$ent->cproyectoentregables)->update(['cestadoentregable'=>'001']);

            $entfec = Tproyectoentregablesfecha::where('cproyectoentregable','=',$ent->cproyectoentregables)->get();

            foreach ($entfec as $fec) {

                $fecNueva = new Tproyectoentregablesfecha();
                $fecNueva->cproyectoentregable =$entNuevo->cproyectoentregables;
                $fecNueva->revision =$fec->revision;
                $fecNueva->fecha =$fec->fecha;
                $fecNueva->ctipofechasentregable =$fec->ctipofechasentregable;
                $fecNueva->referencia_fechas =$fec->cproyectoentregablesfechas;
                $fecNueva->save();

            }
        }
    }

    if($request->input('tipoRevision')=="cliente"){

        DB::beginTransaction();

        $objTdoc= new Tproyectodocumento();
        $objTdoc->cproyecto=$request->input('cproyecto');
        $objTdoc->codigodocumento=$request->input('codigoproyecto').'-AND-25-LE-001';
        $objTdoc->revisionactual=$request->input('nuevaRevision');
        $objTdoc->revisionanterior=$request->input('revisionCli');
        $objTdoc->fechadoc=Carbon::now(); 
        $objTdoc->cdocumentoparapry='1';
        $objTdoc->save();

        $objEntRev= new Tproyectoentregablesrevisiones();
        $objEntRev->cproyectodocumentos=$objTdoc->cproyectodocumentos;
        $objEntRev->revision_cliente=$objTdoc->revisionactual;
        $objEntRev->revision_interna='0';
        $objEntRev->fecha_rev=Carbon::now(); 
        $objEntRev->save();


        DB::commit();   

    };

    if($request->input('tipoRevision')=="interna"){
        
        DB::beginTransaction();

        $objEntRev= new Tproyectoentregablesrevisiones();
        $objEntRev->cproyectodocumentos=$request->input('cod_revisioncli');
        $objEntRev->revision_cliente=$request->input('revisionCli');
        $objEntRev->revision_interna=$request->input('revisionInt')+1;
        $objEntRev->fecha_rev=Carbon::now(); 
        $objEntRev->save();

      
        DB::commit();   
    };

    foreach ($edtProy as $edt) {

        $edt->cproyectoent_rev=$objEntRev->cproyectoent_rev;
        //$edt->orden=$objEntRev->orden;
        $edt->save();

    }



    return Redirect::route('editarEdtProyecto',$request->input('cproyecto'));
}

private function obtenerRevisionEntregablesPryCli($cproyecto){

    $revisiones = DB::table('tproyectodocumentos as pro')
    ->join('tdocumentosparaproyecto as prydoc','pro.cdocumentoparapry','=','prydoc.cdocumentoparapry')
    ->where('prydoc.codigo','=','LE')
    ->where('pro.cproyecto','=',$cproyecto)
    ->orderBy('fechadoc','desc')
    ->select('pro.cproyectodocumentos','pro.fechadoc','pro.codigodocumento as codigo','pro.revisionactual as revision',DB::raw("to_char(pro.fechadoc,'dd-mm-yy') as fecharev"))
    ->first();

    return $revisiones;



}

private function obtenerRevisionEntregablesPryInt($cproyectodoc){

    $revisiones = DB::table('tproyectoentregablesrevisiones as pro')
    ->where('pro.cproyectodocumentos','=',$cproyectodoc)
    ->orderBy('fecha_rev','desc')
    ->first();

    return $revisiones;

}

public function grabarInfoAdicionalEntregable(Request $request){

   

    $cproyecto=$request->input('cproyecto');
    $entregablesSeleccionados=$request->input('checkbox_ent');

    if(isset($entregablesSeleccionados)==true){

        foreach ($entregablesSeleccionados as $ent) {

            DB::beginTransaction();

                $tentre = Tproyectoentregable::where('cproyectoentregables','=',$ent)->first();
                $tentre->cper_modifica=Auth::user()->cpersona;
                $tentre->fecha_modificacion=Carbon::now();
                $tentre->cestadoentregable = '003';

                   
                if (strlen($request->input('revisor'))>0) {
                    $tentre->cpersona_revisor =$request->input('revisor');
                    
                }
                if (strlen($request->input('aprobador'))>0) {
                    $tentre->cpersona_aprobador =$request->input('aprobador');
                    
                }

                if (strlen($request->input('responsableInt'))>0) {
                    $tentre->cpersona_responsable =$request->input('responsableInt');
                    
                }

                if (strlen($request->input('elaborador'))>0) {
                    $tentre->cper_elabora =$request->input('elaborador');
                    
                }
                $tentre->save();
            DB::commit();  


        }

    }

    return Redirect::route('editarEntregablesProyecto',$cproyecto);

}

public function codificarEntregablesSeleccionados(Request $request){

    $cantTotalEnt=intval($request->input('cantTotalEnt'));

    $cproyecto=$request->input('cproyecto');
    $entregablesSeleccionados=$request->input('checkbox_ent');

   


    if(isset($entregablesSeleccionados)==true){

        asort($entregablesSeleccionados);

        foreach ($entregablesSeleccionados as $ent) {

            $entregable=DB::table('tproyectoentregables as pryent')
            ->join('tentregables as ent','pryent.centregable','=','ent.centregables')
            ->leftjoin('ttipoproyectoentregable as tpe','pryent.ctipoproyectoentregable','=','tpe.ctipoproyectoentregable')
            ->join('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
            ->leftjoin('tdisciplinaareas as tda','dis.cdisciplina','=','tda.cdisciplina')
            ->leftjoin('tareas as tar','tda.carea','=','tar.carea')
            ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
            ->leftJoin('tproyecto as pry','edt.cproyecto','=','pry.cproyecto')
            ->leftJoin('tdocumentosparaproyecto as tdp','ent.cdocumentoparapry','=','tdp.cdocumentoparapry')
            ->leftJoin('ttipoedt as tte','edt.ctipoedt','=','tte.ctipoedt')
            ->leftJoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')


            ->leftJoin('tactividad as act','pryent.cproyectoactividades','=','act.cactividad')

            ->leftJoin('testadoentregable as est','pryent.cestadoentregable','=','est.cestadoentregable')
            ->leftJoin('ttiposentregables as tip','ent.ctipoentregable','=','tip.ctiposentregable')
            ->leftJoin('tproyectoentregables as tenp','pryent.cproyectoentregables_parent','=','tenp.cproyectoentregables')
            ->where('pryent.cproyectoentregables','=',$ent)
            ->where('pryent.ctipoproyectoentregable','=','2')
            ->where('edt.estado','=','1')
            ->select('pry.codigo as codigoproy','pryent.cfaseproyecto as fase','edt.codigo as cedt','tar.codigo as codarea','tdp.codigo as coddoc','ent.paquete','edt.ctipoedt','tte.descripcion as tipoedt','cli.abreviatura as cliente','pryent.ctipoproyectoentregable','ent.ctipoentregable')
            ->first();
            
            $codigo=$entregable->codigoproy;

            //ctipoproyectoentregable: contractual=2 ; anexo=1
                //ctipoentregable: Documentos=001 ; Planos=002 ; Figuras=003 ; Mapas=004
                    //ctipoedt: Gestión=1 ; Técnicos=2 ; Comunicación=3

            if ($entregable->ctipoproyectoentregable=='2') {
                if ($entregable->ctipoentregable=='001') {
                    if($entregable->ctipoedt=='1'){
                        $codigo = $codigo.'-'.'AND'.'-'.$entregable->codarea.'-'.$entregable->coddoc.'-'.$entregable->paquete;
                    }
                    if($entregable->ctipoedt=='2'){

                        $codigo = $codigo.'-'.$entregable->fase.'-'.$entregable->cedt.'-'.$entregable->codarea.'-'.$entregable->coddoc.'-'.$entregable->paquete;
                    }
                    if($entregable->ctipoedt=='3'){
                        $codigo = $codigo.'-'.$entregable->coddoc.'-'.'AND'.'-'.$entregable->cliente.'-'.$entregable->paquete;
                    }
                }
                if ($entregable->ctipoentregable=='002') {
                    $codigo = $codigo.'-'.$entregable->fase.'-'.$entregable->cedt.'-'.$entregable->codarea.'-'.$entregable->coddoc.'-'.$entregable->paquete;
                }
                if ($entregable->ctipoentregable=='004') {
                    $codigo = $codigo.'-'.$entregable->fase.'-'.$entregable->cedt.'-'.$entregable->codarea.'-'.$entregable->coddoc.'-'.$entregable->paquete;
                }
                $tentre = Tproyectoentregable::where('cproyectoentregables','=',$ent)->update(['codigo'=>$codigo]);
            }
        }
    }

    return Redirect::route('editarEntregablesProyecto',$cproyecto);
}

public function agregarEntregablesGrupos(Request $request){

    $cedt=$request->input('cproyectoedt_m');

    $cantidadEnt=intval($request->input('cantidad_entregable'));

    for ($i=1; $i <= $cantidadEnt ; $i++) { 


        DB::beginTransaction();
        $tentre = new Tproyectoentregable();
        $tentre->cproyecto = $request->input('cproyecto'); 
        $tentre->cper_elabora=Auth::user()->cpersona;
        $tentre->fecha_elaboracion=Carbon::now();
        $tentre->cestadoentregable = '001';  

        $tentre->cproyectoedt = $cedt;
        $tentre->observacion = $request->input('observacionent');
        //$tentre->cpersona_responsable = $this->valorNull($request->input('responsable'),null);
        $tentre->ctipoproyectoentregable = $this->valorNull($request->input('tipoproyent'),null);
        //$tentre->ctipoproyectoentregable = $request->input('tipoproyent');
        //$tentre->cpersona_responsable = $request->input('responsable'); 
        $tentre->centregable = $request->input('centregable');
        $tentre->cfaseproyecto = $request->input('fase');
        $entregable = DB::table('tentregables as ent')
        ->where('centregables','=',$request->input('centregable'))
        ->first();

        $tentre->descripcion_entregable = $request->input('descripcion_entregable').' '.$i.' de '.$cantidadEnt; 
        $tentre->cproyectoentregables_parent = $request->input('cproyectoentregables_parent');   
        $tentre->save();  


        DB::commit();
    }

    return Redirect::route('viewEntregables',$cedt);
} 

public function grabarEntregablesNuevo(Request $request){

    DB::beginTransaction(); 

     //dd(strlen($request->input('entregable_parent')),$request->input('entregable_parent')); 
    $entregable = new Tentregable();    
    $entregable->descripcion= $request->input('descripcion');
    $entregable->ctipoentregable= $request->input('ctipoentregable');   
    $entregable->cdisciplina= $request->input('cdisciplina');   
    $entregable->nivel= $request->input('nivel');  

    if ($request->input('nivel')!='0') {

         $entregable->entregable_parent=$request->input('entregable_parent');
    } 
    //$entregable->cdocumentoparapry= $request->input('cdocumentoparapry');  

    if (strlen($request->input('cdocumentoparapry'))>0) {        
        $entregable->cdocumentoparapry= $request->input('cdocumentoparapry');
    }

    if (strlen($request->input('paquete'))>0) {        
        $entregable->paquete= $request->input('paquete');
    } 
    $entregable->tipo_paquete= $request->input('tipo_paquete');  
    $entregable->save();    

    DB::commit();

}

public function listarComboEntregablesXTipo($tipo){

    $entregable = DB::table('ttiposentregables');
    if ($tipo=='1') {
       $entregable=$entregable->whereIn('ctiposentregable',['001','002','003','004']);
    }
    if ($tipo=='2') {
        $entregable=$entregable->whereIn('ctiposentregable',['001','002','004']);
    }
    $entregable=$entregable->get();
    return $entregable;
}


public function codificarEntregablesTodos($idProyecto){

    $listEnt=$this->listarTodosEntregables($idProyecto,'todos');

    $arrayEnt=array();

    foreach ($listEnt as $ent) {

        $e['tipo_ent']=$ent['ctipoentregable'];
        $e['tipo_edt']=$ent['ctipoedt'];

        /*if ($ent['ctipoentregable']=='001') {

            if ($ent['ctipoedt']=='1') {

                $e['codigo']=$ent['cdisciplina'].'_'.$ent['centregable'].'_'.$ent['cproyectoentregables'].'_'.$ent['codproy'].'_'.$ent['cfaseproyecto'].'_'.$ent['cedt'].'_'.$ent['codarea'].'_'.$ent['coddoc'].'-gestion-Documento';
            }
           
            if ($ent['ctipoedt']=='2') {
                
                $e['codigo']=$ent['cdisciplina'].'_'.$ent['centregable'].'_'.$ent['cproyectoentregables'].'_'.$ent['codproy'].'_'.$ent['codarea'].'_'.$ent['coddoc'].'-tecnicos-Documento';
            }

            if ($ent['ctipoedt']=='3') {
                
                $e['codigo']=$ent['cdisciplina'].'_'.$ent['centregable'].'_'.$ent['cproyectoentregables'].'_'.$ent['codproy'].'_'.$ent['cliente'].'_'.$ent['coddoc'].'comunicacion-Documento';
            }
        }

        if ($ent['ctipoentregable']=='002') {

            $e['codigo']=$ent['cdisciplina'].'_'.$ent['centregable'].'_'.$ent['cproyectoentregables'].'_'.$ent['codproy'].'_'.$ent['cfaseproyecto'].'_'.$ent['cedt'].'_'.$ent['codarea'].'_'.$ent['coddoc'].'-Plano';
        }

        if ($ent['ctipoentregable']=='004') {

            $e['codigo']=$ent['cdisciplina'].'_'.$ent['centregable'].'_'.$ent['cproyectoentregables'].'_'.$ent['codproy'].'_'.$ent['cfaseproyecto'].'_'.$ent['cedt'].'_'.$ent['codarea'].'_'.$ent['coddoc'].'-Plano';
        }*/


        $e['codigo']=$ent['cdisciplina'].'_'.$ent['centregable'].'_'.$ent['cproyectoentregables'].'_'.$ent['codproy'].'_'.$ent['coddoc'].'_'.$ent['codarea'].'_'.$ent['cfaseproyecto'].'_'.$ent['cedt'].'_'.$ent['cliente'];

        $arrayEnt[count($arrayEnt)]=$e;
    }

    sort($arrayEnt);

    //dd($arrayEnt);
    $primer_ent=explode('_',$arrayEnt[0]['codigo']);

    $disciplina=$primer_ent[0];
    $centregable=$primer_ent[1];
    $centregableproy=$primer_ent[2];
    //dd($arrayEnt);
    $cor_final=0;
    $cor_disc=0;
    $cor_ent=0;

    foreach ($arrayEnt as $ae) {
       // dd($primer_ent[5]);
        $ent=explode('_',$ae['codigo']);

        $entr=$ent[0];
        $dis=$ent[1];
        $cproyent=$ent[2];
        $codproy=$ent[3];
        $doc=$ent[4];
        $area=$ent[5];
        $fase=$ent[6];
        $edt=$ent[7];
        $cliente=$ent[8];


       /* echo $ent[0].' | '.$ent[1].' | '.$ent[2].' | '.$ent[3].' | '.$ent[4].' | '.$ent[5].' | '.$ent[6].' | '.$ent[7].' | '.$ent[8]."<br>";*/
        $codigo_ini='';

        if ($ae['tipo_ent']=='001') {

            if ($ae['tipo_edt']=='1') { 

                $codigo_ini=$codproy.'-AND-'.$area.'-'.$doc.'-';

            }
           
            if ($ae['tipo_edt']=='2') { 

                $codigo_ini=$codproy.'-'.$fase.'-'.$edt.'-'.$area.'-'.$doc.'-';
                
            }

            if ($ae['tipo_edt']=='3') { 

                $codigo_ini=$codproy.'-'.$doc.'-AND-'.$cliente.'-';
              
            }
        }

        if ($ae['tipo_ent']=='002') {
            $codigo_ini=$codproy.'-'.$fase.'-'.$edt.'-'.$area.'-'.$doc.'-';

           
        }

        if ($ae['tipo_ent']=='004') { 
            $codigo_ini=$codproy.'-'.$fase.'-'.$edt.'-'.$area.'-'.$doc.'-';

            
        }

// ----------------------------------------------------------------------------- //

        if ($ent[0]==$disciplina) {
           
            if ($ent[1]==$centregable) {
                $cor_ent+=1;
            }
            else{
                if ($cor_ent>10) {
                    $cor_disc=floor($cor_final/10)*10;
                    
                }
                $cor_disc+=10;
  
                $cor_ent=0;
            }
        }

        else{
           $cor_disc=0;
           $cor_ent=1;
           $cor_final=0;
   
        }

        $cor_final=$cor_disc+$cor_ent;
        $disciplina=$ent[0];
        $centregable=$ent[1];
        $centregableproy=$ent[2];

        $correlativo=str_pad($cor_final, 3, "0", STR_PAD_LEFT);

        $codigo_completo=$codigo_ini.$correlativo;

        Tproyectoentregable::where('cproyectoentregables','=',$cproyent)->update(['codigo'=>$codigo_completo]);

        //echo $codigo_completo.' <br> ';

        //echo $codigo_ini.'_'. str_pad($cor_final, 3, "0", STR_PAD_LEFT).'_'.$ent[1].'_'.$ent[0].' <br> ';

// ----------------------------------------------------------------------------- //

    }

    return Redirect::route('editarEntregablesProyecto',$idProyecto);

}


public function printEDTProy($idProyecto){

    /*$proyecto = DB::table('tproyecto as tp')
    ->leftjoin('tpersona as tpgt','tp.cpersona_gerente','=','tpgt.cpersona')
    ->select('tp.*','tpgt.abreviatura as gerente')
    ->where('cproyecto','=',$idProyecto)
    ->first();

    $listaEntregables=$this->listarTodosEntregables($idProyecto,'todos');

    dd($listaEntregables);*/

         //Session::put('listLecApre',$lecaprend);

        $view =  \View::make('proyecto/printEDTProyecto',compact(''))->render();
        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','D');

         return $pdf->stream($idProyecto.'-AND-25-LA-001.pdf');
}
    

    public function ActualizaListaProyecto(Request $request)
    {   
        /** inicio de actualizacion de senior  por diciplina */
        //dd($request->all());
        if ($request->cpersona) {
            $encuentrocategoriaporusuario = Tpersonadatosempleado::where('cpersona','=', $request->cpersona)->first();

            $buscodiciplina = DB::table('tdisciplinaareas as tda')
                           // ->leftjoin('')
                            ->select('tda.cdisciplina')
                            ->where('tda.carea','=',$encuentrocategoriaporusuario->carea)
                            ->first();
            //dd($buscodiciplina);


            $filtrocategoria = DB::table('tproyectopersona as tpp')
                ->join('tpersonadatosempleado as tpe','tpp.cpersona','=', 'tpe.cpersona')
                ->join('tdisciplinaareas as tda', 'tpe.carea','=', 'tda.carea')
                ->select('tpp.cproyectopersona','tpe.ccategoriaprofesional', 'tpe.carea','tda.cdisciplina','tpe.cpersona')
                ->where('tpe.cpersona','=',$request->cpersona)
                ->where('tda.cdisciplina','=',$buscodiciplina->cdisciplina)
                ->where('tpp.cproyecto','=', $request->cproyecto)->first();

            $categoria = $filtrocategoria?$filtrocategoria->ccategoriaprofesional:$encuentrocategoriaporusuario->ccategoriaprofesional;

            $buscamosper = DB::table('tproyectopersona as tpp')
                ->join('tpersonadatosempleado as tpe','tpp.cpersona','=', 'tpe.cpersona')
                //->where('ccategoriaprofesional','=',$categoria)
                // ->where('tpe.ccargo','=',$encuentrocategoriaporusuario->ccargo)
                ->where('cproyecto','=',$request->cproyecto)
                // ->where('cdisciplina','=',$buscodiciplina->cdisciplina)
                ->where('tpp.seniors','=',$request->seniors)
                ->get();

            foreach ($buscamosper as $key => $value) {



                $actualizocproyecto = Tproyectopersona::where('cproyectopersona','=', $value->cproyectopersona)->first();
                $actualizocproyecto->eslider = '0';
                $actualizocproyecto->seniors = null;
                $actualizocproyecto->update();
                
            }
            
            if(!is_null($filtrocategoria)){
                
                $creoproyectopersona = Tproyectopersona::where('cproyectopersona','=', $filtrocategoria->cproyectopersona)->first();
                $creoproyectopersona->eslider = '0';//s eca
                $creoproyectopersona->seniors = $request->seniors;
                $creoproyectopersona->save();   
                
            }
            else{
               
                $creoproyectopersona = new Tproyectopersona;//Creo nuevo registro que no tiene categoria o persona en el proyecto
                $creoproyectopersona->cproyecto = $request->cproyecto;
                $creoproyectopersona->cpersona = $request->cpersona;
                $creoproyectopersona->ccategoriaprofesional = $categoria;
                $creoproyectopersona->cdisciplina = $buscodiciplina->cdisciplina;
                $creoproyectopersona->eslider = '0';//se cambio
                $creoproyectopersona->seniors = $request->seniors;
                $creoproyectopersona->save();

            } 
        }else{ 
           // dd($request->cselect,$request->cproyecto);

            $selectanterior = Tproyectopersona::where('cpersona','=',$request->cselect)
            ->where('cproyecto','=',$request->cproyecto)
            ->first();
            $selectanterior->eslider = '0';
            $selectanterior->seniors = null;
            $selectanterior->update();
        }
        
        $Proyectos=$this->datosProyectos($request->cestado,$request->valor,$request->tipoproyecto);

        
        
        
        $buscocargos2 = $this->obtenercabecera();
        $lidersis= $this->obtenerliderescabecera();
        //$estadoproyecto=$vista['estadoproyecto'];
                
        return view('proyecto/listatable',compact('Proyectos', 'buscocargos2','lidersis'));
    }

    /** fin de listado proyecto por senior si seniroi enrique es selccioanado pasa ser 1 pero si senioir enrique es remplazado
     * pasa a ser 0 y el nuevo entra como 1
     */


    public function guardar_ascocidos(Request $request){

        $actividades = $request->actividades;
        $cpropuesta = $request->cpropuesta;
        $cproyecto = $request->cproyecto;

        foreach ($actividades as $key => $act) {
            $a = explode('_', $act);
            $cpropuestaactividades = $a[0];
            $cproyectoactividades = $a[1];
            $pro_act_del = Tproyectohonorario::where('cproyectoactividades', '=', $cproyectoactividades)->delete();

            $actividad_propuesta = Tpropuestaactividade::where('cpropuestaactividades','=',$cpropuestaactividades)->first();

            $actividad_proyecto = Tproyectoactividade::where('cproyectoactividades','=',$cproyectoactividades)->update(['numeroplanos'=>$actividad_propuesta->numeroplanos,'numerodocumentos'=>$actividad_propuesta->numerodocumentos]);
        }

        $pro_estr_del = DB::table('tproyectoestructuraparticipantes')->where('cproyecto','=',$cproyecto)->delete();

        $prop_estr = DB::table('testructuraparticipante')->where('cpropuesta','=',$cpropuesta)->get();       

        foreach ($prop_estr as $key => $estr_ppa) {
            
            $proy_act = Tproyectoestructuraparticipante::where('cestructurapropuesta','=',$estr_ppa->cestructurapropuesta)->first();

            if (!$proy_act) {
                $proy_act = new Tproyectoestructuraparticipante;
                $proy_act->cestructurapropuesta = $estr_ppa->cestructurapropuesta;
            }

            $proy_act->cproyecto = $cproyecto;
            $proy_act->croldespliegue = $estr_ppa->croldespliegue;
            $proy_act->tipoestructura = null;
            $proy_act->rate = $estr_ppa->rate;
            $proy_act->cdisciplina = $estr_ppa->cdisciplina;
            $proy_act->cestructurapropuesta = $estr_ppa->cestructurapropuesta;
            $proy_act->save();
        }
            // dd($request->all());

        foreach ($actividades as $key => $act) {

            $a = explode('_', $act);
            $cpropuestaactividades = $a[0];
            $cproyectoactividades = $a[1];

            $prop_hon = DB::table('tpropuestahonorarios')->where('cpropuestaactividades','=',$cpropuestaactividades)->get();


            foreach ($prop_hon as $key => $hon) {
                
                $proy_act = DB::table('tproyectoestructuraparticipantes')->where('cestructurapropuesta','=',$hon->cestructurapropuesta)->first();

                // foreach ($proy_act as $key => $est) {
                    
                    $proy_hon = Tproyectohonorario::where('cestructuraproyecto','=',$proy_act->cestructuraproyecto)->where('cproyectoactividades','=',$cproyectoactividades)->where('cdisciplina','=',$hon->cdisciplina)->first();

                    if (!$proy_hon) {
                        $proy_hon = new Tproyectohonorario;
                    } 
                    $proy_hon->cproyectoactividades = $cproyectoactividades;
                    $proy_hon->horas = $hon->horas;
                    $proy_hon->cestructuraproyecto = $proy_act->cestructuraproyecto;
                    $proy_hon->cdisciplina = $hon->cdisciplina;
                    $proy_hon->save();

                // }

            }

        }
            // dd($actividades,$prop_hon,$proy_act,$proy_hon);
        return 'ok';

    }

    private function obtener_datos_proyecto($cproyecto){

        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$cproyecto)->first();
        $gerente  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersona_gerente)->first();
        $cliente  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $estadoproyecto = Testadoproyecto::where('cestadoproyecto','=',$proyecto->cestadoproyecto)->first();
        $uminera = DB::table('tunidadminera')->where('cunidadminera','=',$proyecto->cunidadminera)->first(); 
        $propuesta = DB::table('tpropuesta')->where('cproyecto','=',$cproyecto)->first();  
        $documento_rev = $this->obtenerRevisionEntregablesPryCli($cproyecto);

        $revisiones_internas=null;
        $crevision=null;
        if($documento_rev){
            $revisiones_internas=$this->obtenerRevisionEntregablesPryInt($documento_rev->cproyectodocumentos);
            $crevision=$revisiones_internas->cproyectoent_rev;
        }

        //dd($revisiones_internas,$crevision);



        return array([  'cproyecto'=>$proyecto->cproyecto,
                        'cliente'=>$cliente->nombre,
                        'codproyecto'=>$proyecto->codigo,
                        'nombreproyecto'=>$proyecto->nombre,
                        'estadoproyecto'=>$estadoproyecto->descripcion,
                        'uminera'=>$uminera->nombre,
                        'cunidadminera'=>$uminera->cunidadminera,
                        'gerente'=>$gerente->abreviatura,
                        'propuesta'=>$propuesta?$propuesta->ccodigo:'',
                        'documento_rev'=>$documento_rev,
                        'documento_rev_interna'=>$crevision,
                ]);

    }

    private function obtener_entregables_print($edt,$tipo,$con_anexo,$estados_ent){

        $listaEntregables=DB::table('tproyectoentregables as pryent')  
            ->leftjoin('tentregables as ent','pryent.centregable','=','ent.centregables')
            ->leftJoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
            ->leftJoin('ttiposentregables as tip','ent.ctipoentregable','=','tip.ctiposentregable')
            ->leftJoin('ttipoproyectoentregable as tpe','pryent.ctipoproyectoentregable','=','tpe.ctipoproyectoentregable')
            ->where('pryent.cproyectoedt','=',$edt);

            if ($tipo != 'todos') {
               $listaEntregables=$listaEntregables->where('ent.ctipoentregable','=',$tipo);
            }
            if ($con_anexo == 'contractual') {
                $listaEntregables=$listaEntregables->where('tpe.escontractual','=','c');
            }
            else {
                $listaEntregables=$listaEntregables->where('tpe.escontractual','=','a')
                                                   ->where('pryent.cproyectoentregables_parent','=',$con_anexo)
                                                   ->orderBy('pryent.codigo','asc');
            }

            if ($estados_ent != 'todos') {
                $listaEntregables=$listaEntregables->where('pryent.cestadoentregable','!=','002');
            }
            //->where('pryent.ctipoproyectoentregable','=','2');
        $listaEntregables=$listaEntregables->select('pryent.*','tip.descripcion as tipoentregable','dis.cdisciplina','tip.ctiposentregable',DB::raw("initcap(dis.descripcion) as disciplina"))
            ->orderBy('pryent.codigo','desc')
            ->get();

        foreach ($listaEntregables as $key => $le) {

            $fechaA=DB::table('tproyectoentregablesfechas')->where('cproyectoentregable','=',$le->cproyectoentregables)->where('ctipofechasentregable','=','4')->select(DB::raw("to_char(fecha,'dd-mm-yy') as fecha"))->first();

            $fechaB=DB::table('tproyectoentregablesfechas')->where('cproyectoentregable','=',$le->cproyectoentregables)->where('ctipofechasentregable','=','5')->select(DB::raw("to_char(fecha,'dd-mm-yy') as fecha"))->first();

            $fecha0=DB::table('tproyectoentregablesfechas')->where('cproyectoentregable','=',$le->cproyectoentregables)->where('ctipofechasentregable','=','9')->select(DB::raw("to_char(fecha,'dd-mm-yy') as fecha"))->first();

            $le->fechaA = $fechaA?$fechaA->fecha:'';
            $le->fechaB = $fechaB?$fechaB->fecha:'';
            $le->fecha0 = $fecha0?$fecha0->fecha:'';
        }

        return $listaEntregables;
    }

    private function agregar_anexos_print($arrayContractuales,$tipo,$estados_ent){


        foreach ($arrayContractuales as $key => $ac) {

            $ac->anexos_mapas=$this->obtener_entregables_print($ac->cproyectoedt,'003',$ac->cproyectoentregables,$estados_ent);
            $ac->anexos_figuras=$this->obtener_entregables_print($ac->cproyectoedt,'004',$ac->cproyectoentregables,$estados_ent);
        }
        return $arrayContractuales;
    }

    public function imprimirEntregables($listaDetalleEDT,$cproyecto){

        $estados_ent= ($listaDetalleEDT == 'lecli')?'002':'todos';
        
        $proyecto = $this->obtener_datos_proyecto($cproyecto);
        $edt = $this->listaDetalleEDT($cproyecto,$proyecto[0]['documento_rev_interna']);

        foreach ($edt as $key => $e) {

            $doc=$this->obtener_entregables_print($e['cproyectoedt'],'001','contractual',$estados_ent);
            $pla=$this->obtener_entregables_print($e['cproyectoedt'],'002','contractual',$estados_ent);
            $map=$this->obtener_entregables_print($e['cproyectoedt'],'003','contractual',$estados_ent);

            $edt[$key]['ent_documentos']=$this->agregar_anexos_print($doc,'001',$estados_ent);
            $edt[$key]['ent_planos']=$this->agregar_anexos_print($pla,'001',$estados_ent);
            $edt[$key]['ent_mapas']=$this->agregar_anexos_print($map,'001',$estados_ent);

        }

        // dd($edt);

        if ($listaDetalleEDT == 'edt') {
            $view =  \View::make('proyecto/printEDTProyecto',compact('edt','proyecto'))->render();
        }

        if ($listaDetalleEDT == 'leand') {
            $view =  \View::make('proyecto/printLEanddes',compact('edt','proyecto'))->render();
        }

        if ($listaDetalleEDT == 'lecli') {
            $view =  \View::make('proyecto/printLEcliente',compact('edt','proyecto'))->render();
        }

        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','D');

        return $pdf->stream($cproyecto.'-AND-25-LA-001.pdf');


    }

    //lardin funcion obtener senior para la vista del modal
    public function obtenerSenior($id){


        $projects =DB::table('tproyecto as tpro')
                   ->where('tpro.cproyecto','=',$id)->first();


       $seniors = DB::table('tcargos as tcar')
                    ->wherenotnull('tcar.seniors')
                    ->leftjoin('tdisciplina as tdisp','tcar.seniors','=','tdisp.cdisciplina')
                    ->select('tdisp.descripcion','tdisp.cdisciplina')->distinct()
                    //->groupBy('tcar.seniors','tdisp.descripcion')
                    ->orderby('tdisp.descripcion', 'asc')
                    ->get();


        //dd($seniors);

        foreach ($seniors as $key => $senior){
            /*$personas=DB::table('tpersonadatosempleado as tpere')
                        ->join('tpersona as tper', 'tper.cpersona','=', 'tpere.cpersona' )
                        ->leftjoin('tdisciplinaareas as tdispc','tpere.carea','=','tdispc.carea')
                        ->where('tdispc.cdisciplina','=',$senior->cdisciplina)
                        ->where('tpere.estado','=','ACT')
                        ->select('tper.abreviatura','tper.cpersona','tpere.ccargo','tdispc.cdisciplina')->distinct()
                        ->get();*/

            $cargos = DB::table('tcargos as tc')
                ->where('tc.seniors','=',$senior->cdisciplina)
                ->lists('tc.ccargo');

            $personas = DB::table('tpersonadatosempleado as tpde')
                ->join('tpersona as tper', 'tper.cpersona','=', 'tpde.cpersona' )
                ->leftjoin('tdisciplinaareas as tdispc','tpde.carea','=','tdispc.carea')
                ->select('tper.abreviatura','tper.cpersona','tdispc.cdisciplina')
                ->where('tpde.estado','=','ACT')
                ->wherein('tpde.ccargo',$cargos)
                ->get();


            $senior->senior_person=$personas;
            $seniors[$key]->cabecera = "Senior ".$senior->descripcion;

            foreach ($personas as $key =>$person ){

                $asingPerson=DB::table('tproyectopersona as tperson')
                    ->where('cpersona', '=', $person->cpersona)
                    ->where('cproyecto', '=', $id)
                    //->where('cdisciplina','=',$personas->cdisciplina)
                    ->where('eslider', '=', '0')
                    ->wherenotnull('seniors')
                    ->first();
                $person->asingPerson =$asingPerson? 'selected': '';

            }
        }

        return view('proyecto.seniorsproyectos',[
            //'proyecto'=>$proyecto,
            'seniors'=>$seniors,
            'projects'=>$projects
        ]);
    }
    //fin de controller
}

