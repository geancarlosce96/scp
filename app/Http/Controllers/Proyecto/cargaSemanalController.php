<?php

namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;

use App\Http\Requests;

use Auth;
use Carbon\Carbon;
use DB;
use Response;

use App\Http\Controllers\Controller;

class cargaSemanalController extends Controller
{

    public function verAreas()
    {
        $careas = $this->areas();


         return view('proyecto.CargabilidadSemanal.opcionescargabilidad', compact('careas'));
    }
    public function verAreasGrafico()
    {
        $careas = $this->areas();
        return view('proyecto.CargabilidadSemanal.graficopcionescargabilidad', compact('careas'));
    }
    public function verAreasIndicadores()
    {
        $careas = $this->areas();

        return view('proyecto.CargabilidadSemanal.indicadoropcionesCS', compact('careas'));
    }
    public function verAreasPersonalIndicadores()
    {
        $careas = $this->areas();

        return view('proyecto.CargabilidadSemanal.indicadoropcionesPersonalCS', compact('careas'));
    }

    public function getproyectos(Request $request)
    {
        //dd($request->all());

        $tipo = $request->tipo;
        $careas = $request->careas;
        $semana = $this->numsemana($request->fecha_ini);
        $semana_fin = $this->endweek($request->fecha_ini);
        $anio_inicial = $this->aniofecha($semana_fin);
        $fecha_inicial_semana = $this->startweek($request->fecha_ini);
        $fecha_final_semana = $this->endweek($request->fecha_ini);



        $proyectos =  $this->listaproyectosfacnofacadmin($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$tipo);


        $empleados = $this->empleados($careas,$fecha_inicial_semana,$fecha_final_semana);
        $empleadosejecutados = $this->empleadosejecutados($careas,$fecha_inicial_semana,$fecha_final_semana);
        // dd($empleados,$empleadosejecutados);

        $emple = array_merge($empleados,$empleadosejecutados);

        $empledos_list = array_unique($emple);

        $empleados_lista = [];

        foreach ($empledos_list as $key => $value) {
            $list = explode('-',$value);
            $e['cpersona'] =  $cpersona = $list[0];
            $e['abreviatura'] =  $abreviatura = $list[1];
            $e['estado'] =  $estado = $list[2];

            array_push($empleados_lista, $e);
            
        }

        $arrayproyectos = [];

        foreach ( $proyectos as $py)
        {
            $col['cunidadminera'] = $py->cunidadmin;
            $col['nombrecliente'] = $py->nombrecliente;
            $col['nombreunidadminera'] = $py->nombreunidadminera;
            $col['codigo'] = $py->codigo;
            $col['cproyecto'] = $py->cproyecto;
            $col['descripcion'] = $py->description;
            $col['estado'] = $py->estadoo;
            $col['carea'] = $py->carea;
            $col['sum'] = floatval($py->sum);
            $col['semana'] =$semana;

            $arrayhe = [];
            foreach ($empledos_list as $em){

                $list = explode('-',$em);
                $cpersona = $list[0];
                $abreviatura = $list[1];
                $estado = $list[2];

                $horas_empleado =  $this->listaproyectosfacnofacadminuser($py->cproyecto,$careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$tipo,$cpersona);

                $colum['sumempleado'] = 0;
                if($horas_empleado != null)
                {
                    $colum['sumempleado'] = floatval($horas_empleado);
                }
                array_push($arrayhe,$colum);
            }

            $col['sumatotal_empleado'] =  $arrayhe;
            array_push($arrayproyectos,$col);

        }

        // dd($arrayproyectos);

        return view('proyecto.CargabilidadSemanal.detallefacturable', compact('arrayproyectos','empleados_lista'));

    }
    private function horas_($careas,$semana,$fecha_inicial,$fecha_final,$tipo,$valor){


        $horas = DB::table('tproyectoejecucion as tpe')
            ->where('tpe.carea','=',$careas)
            // ->where(DB::raw('extract(week from fplanificado)'),'=',$semana)
            // ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            ->where('tipo','=',$tipo);
            if ($tipo=='3') {
                
                if ($valor == 'interna') {
                $horas = $horas->wherenull('tpe.cactividad');

                }
                else {
                $horas = $horas->wherenull('tpe.cproyectoactividades');
                }
            }

            $horas =$horas->sum('tpe.horasejecutadas');
       // dd($horas);


    return $horas;
}

    private function horas_semanas2($careas,$fecha_ini,$fecha_fin,$semana,$tipo,$valor){
        // dd($careas,$fecha_ini,$fecha_fin,$semana,$tipo,$valor);
        $horas = DB::table('tproyectoejecucion as tpe')
            ->where('tpe.carea','=',$careas)
            ->where('tipo','=',$tipo)
            ->where('fplanificado','>=',$fecha_ini)
            ->where('fplanificado','<=',$fecha_fin)
            ->where(DB::raw('extract(week from fplanificado)'),'=',$semana);
            if ($tipo=='3') {
                
                if ($valor == 'interna') {
                $horas = $horas->wherenull('tpe.cactividad');

                }
                else {
                $horas = $horas->wherenull('tpe.cproyectoactividades');
                }
            }
            $horas = $horas->sum('tpe.horasejecutadas');

        return $horas;
    }

public function graficosemana(Request $request){

        $semana = $this->numsemana($request->fecha_ini);
        $fecha_inicial_semana = $this->startweek($request->fecha_ini);
        $fecha_final_semana = $this->endweek($request->fecha_ini);
        // $anio = $this->aniofecha($request->fecha_ini);
        $careas = $request->careas;


        //dd($semana);


        $horasFF = $this->horas_($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,1,'externa');
        $horasFN = $this->horas_($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,2,'externa');
        $horasAND = $this->horas_($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'externa');
        $horasANDint = $this->horas_($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'interna');

        $horasportipo=[floatval($semana),floatval($horasFF),floatval($horasFN),floatval($horasAND),floatval($horasANDint)];


    return $horasportipo;
}

public function grafico3semanas(Request $request)

    {   

        $fecha_ini = $this->startweek($request->fecha_ini);
        $semana_ini = $fecha_ini->weekOfYear;
        $anio_inicial = $this->aniofecha($fecha_ini);

        $fecha_final = $this->endweek($request->fecha_final);
        $semana_fin =$fecha_final->weekOfYear;
        $anio_final = $this->aniofecha($fecha_final);

        $fi=$fecha_ini->toDateString();
        $ff=$fecha_final->toDateString();

        // dd($fecha_ini,$fecha_final,$anio_final);

        $careas = $request->careas;



        $num_sema = [];

        if ($anio_final == $anio_inicial) {

            for ($i = $semana_ini; $i <= $semana_fin; $i++) {

                //array_push($num_sema, $i);

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }

        } else if ($anio_final > $anio_inicial) {

            $num_sema1 = [];
            $num_sema2 = [];
            if ($semana_fin > $semana_ini )
            {
                for ($i = $semana_ini; $i <= $semana_fin; $i++) {

                    //array_push($num_sema, $i);

                    array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
                }

            }
                else{
                    //dd($fechaDesde,$fechaHasta,$semana_ini,$semana_fin,$anio_inicial,$anio_final,'OK');
                    for ($i = $semana_ini; $i <= 52; $i++) {

                        array_push($num_sema1, ['semana' => $i, 'anio' => $anio_inicial]);
                    }

                    // dd($num_sema);

                    for ($i = 1; $i <= $semana_fin; $i++) {

                        array_push($num_sema2, ['semana' => $i, 'anio' => $anio_final]);
                    }

                    $num_sema = array_merge($num_sema1,$num_sema2);
                }



        }
        else {

        }

       // dd($num_sema);

        $arraysemana =[];
        foreach($num_sema as $ns)

        {
            $col['horasFF'] = floatval($this->horas_semanas2($careas,$fi,$ff,$ns['semana'],1,'externa'));
            $col['horasFN'] =  floatval($this->horas_semanas2($careas,$fi,$ff,$ns['semana'],2,'externa'));
            $col['horasAND'] =  floatval($this->horas_semanas2($careas,$fi,$ff,$ns['semana'],3,'externa'));
            $col['horasINT'] =  floatval($this->horas_semanas2($careas,$fi,$ff,$ns['semana'],3,'interna'));
            $col['semana'] = floatval( $ns['semana']);

            array_push($arraysemana,$col);
        }

        // dd($arraysemana);

         return $arraysemana;

  }


    public function grafico_porusuarios(Request $request)
{
    //dd($request->all());
    $semana = $this->numsemana($request->fecha_ini);
    $fecha_inicial_semana = $this->startweek($request->fecha_ini);
    $fecha_final_semana = $this->endweek($request->fecha_ini);
    // $anio = $this->aniofecha($anio_inicial);
    $careas = $request->careas;
    $emple = [];

    $empledos_list = [];


    $empleados = $this->empleados($careas,$fecha_inicial_semana,$fecha_final_semana);
    $empleadosejecutados = $this->empleadosejecutados($careas,$fecha_inicial_semana,$fecha_final_semana);

    $emple = array_merge($empleados,$empleadosejecutados);

    $empledos_list = array_unique($emple);

    $array_fac_nofac_adm = [];
    foreach ($empledos_list as $emp)
    {
        $list = explode('-',$emp);
        $cpersona = $list[0];
        $abreviatura = $list[1];
        $estado = $list[2];

        $sumtotal_fac =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,1,'externa');
        $sumtotal_nofac =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,2,'externa');
        $sumtotal_admin =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'externa');
        $sumtotal_admin_interna =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'interna');

        $suma_total_horas = $sumtotal_fac+$sumtotal_nofac+$sumtotal_admin+$sumtotal_admin_interna;

        if ($suma_total_horas > 0) {
            array_push($array_fac_nofac_adm,[
                'user' => $cpersona,
                'abreviatura' => $abreviatura,
                'sumuser_fac' => $sumtotal_fac,
                'sumuser_nofac' => $sumtotal_nofac,
                'sumuser_admin' => $sumtotal_admin,
                'sumuser_admin_interna' => $sumtotal_admin_interna,
                'semana'=>$semana,
            ]);
        }

   // dd($suma_total_horas);
   
    }


    return $array_fac_nofac_adm;



}
    public function grafico_nofacturable (Request $request)
    {
        $semana = $this->numsemana($request->fecha_ini);
        // $anio_inicial = $this->aniofecha($request->fecha_ini);
        $fecha_inicial_semana = $this->startweek($request->fecha_ini);
        $fecha_final_semana = $this->endweek($request->fecha_ini);
        $careas = $request->careas;


        // GRAFICA NO FACTURABLES

        $actividades = $this->listaactividadesnofacturables();
        // dd($actividades);

        $arrayhorasnofact_nuevo=[];

        foreach ($actividades as $act)
        {
            $ac['cactividad']=$act['pk'];
            $ac['descripcion']=$act['descripcion'];
            $ac['semana']=$semana;
            $acumulativo_nuevo =  $this->obtener_horasnofacturables($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$act['pk']);
            $ac['sumatodos']=floatval($acumulativo_nuevo);
            
            if ($ac['sumatodos'] > 0) {
            array_push($arrayhorasnofact_nuevo,$ac);
            }

        }

        // dd($arrayhorasnofact_nuevo);
        return $arrayhorasnofact_nuevo;

    }
    public function grafico_admin(Request $request)
    {

        $semana = $this->numsemana($request->fecha_ini);
        // $anio_inicial = $this->aniofecha($request->fecha_ini);
        $fecha_inicial_semana = $this->startweek($request->fecha_ini);
        $fecha_final_semana = $this->endweek($request->fecha_ini);
        $careas = $request->careas;

        // GRAFICO ADMINISTRATIVOS INTERNOS

        $actividadesinternas = $this->listaactividadesadministrativas();

        //dd($actividadesinternas);

        $arrayinternasactividades  = [];

        //dd("hoa",$actividadesinternas);
        foreach ($actividadesinternas  as $act)
        {

            $acti['cactividad']=$act['pk'];
            $acti['descripcion']=$act['descripcion'];
            $acti['semana']=$semana;
            $acumulativo_nuevo = $this->obtener_horasnofacturablesadmin($careas, $semana,$fecha_inicial_semana,$fecha_final_semana, $act['pk']);
            $acti['sumatodos']=floatval($acumulativo_nuevo);

            if ($acti['sumatodos'] > 0) {
            array_push($arrayinternasactividades,$acti);
            }


        }

      // dd($arrayinternasactividades);

        return $arrayinternasactividades;

    }
    public function grafico_internos(Request $request)
    {

        $semana = $this->numsemana($request->fecha_ini);
        // $anio = $this->aniofecha($request->fecha_ini);
        $fecha_inicial_semana = $this->startweek($request->fecha_ini);
        $fecha_final_semana = $this->endweek($request->fecha_ini);
        $careas = $request->careas;

        $internos = [];

        $proyectos = DB::table('tproyectoejecucion as tpe')
            ->join('tproyecto as tp','tp.cproyecto','=','tpe.cproyecto')
            ->select('tp.codigo','tp.descripcion as nombrepy','tpe.cproyecto','tp.nombre')
            ->where('tpe.carea','=',$careas)
            // ->where(DB::raw('extract(week from fplanificado)'),'=',$semana)
            // ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->where('tpe.fplanificado','>=',$fecha_inicial_semana)
            ->where('tpe.fplanificado','<=',$fecha_final_semana)
            ->where('tipo','=','3')
            ->wherenull('tpe.cactividad')
            ->distinct('tpe.cproyecto')
            ->get();

            // dd($proyectos);

        foreach ($proyectos as $key => $pry) {

            $horas = DB::table('tproyectoejecucion as tpe')
            ->where('tpe.carea','=',$careas)
            // ->where(DB::raw('extract(week from fplanificado)'),'=',$semana)
            // ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->where('tpe.fplanificado','>=',$fecha_inicial_semana)
            ->where('tpe.fplanificado','<=',$fecha_final_semana)
            ->where('tipo','=','3')
            ->where('cproyecto','=',$pry->cproyecto)
            ->wherenull('tpe.cactividad')
            ->sum('tpe.horasejecutadas');

            $py['codigo'] = $pry->codigo;
            $py['nombre'] = $pry->nombre;
            $py['suma_horas'] = floatval($horas);
            $py['semana']=$semana;

            array_push($internos,$py);
        }


        return $internos;

    }
    public function grafico_productividadCargabilidad(Request $request)
    {

        $semana = $this->numsemana($request->fecha_final);
        $semana_fin = $this->endweek($request->fecha_final);
        $fecha_inicial_semana = $this->startweek($request->fecha_final);
        $fecha_final_semana = $this->endweek($request->fecha_final);
        $anio_inicial = $this->aniofecha($semana_fin);
        $careas = $request->careas;
        //dd($semana,$fecha_inicial_semana,$fecha_final_semana);


        $empleados = $this->empleados($careas,$fecha_inicial_semana,$fecha_final_semana);
        $empleadosejecutados = $this->empleadosejecutados($careas,$fecha_inicial_semana,$fecha_final_semana);

        $emple = array_merge($empleados,$empleadosejecutados);

        $empledos_list = array_unique($emple);

        //dd($empledos_list);

        $array_fac_nofac_adm = [];



        foreach ($empledos_list as $emp)
        {
            $list = explode('-',$emp);
            $cpersona = $list[0];
            $abreviatura = $list[1];

            $sumtotal_fac =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,1,'externa');
            $sumtotal_nofac =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,2,'externa');
            $sumtotal_admin =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'externa');
            $sumtotal_internas =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'interna');




            //Real
            $real = $sumtotal_fac + $sumtotal_nofac + $sumtotal_admin + $sumtotal_internas ;
            //acumulado


            // Planeado

            // Cargabilidad

            // No Laborable

            $actividadesnofacturablesnolaborables =  $this->listaactividadesnofacturablesnolaborables();

            $array_sum_nofacturables_nolaborables = [];

            foreach ($actividadesnofacturablesnolaborables as $anol)
            {

                $horas_por_empleado_actividad_descanso_proyecto_2 = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, $anol['pk'],$cpersona);
                array_push($array_sum_nofacturables_nolaborables,$horas_por_empleado_actividad_descanso_proyecto_2);
            }

            $horas_por_empleado_actividad_descanso_proyecto = array_sum($array_sum_nofacturables_nolaborables);

            $actividadesnolaborables = $this->listaactividadesadministrativasnolaborables();
            $array_sum_nofacturables = [];
            foreach ($actividadesnolaborables as $acnolabo)
            {
                $horas_por_empleado_actividad_admin_nolaborables =  $this->obtener_horasnofacturablesadminuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$acnolabo['pk'],$cpersona);
                array_push($array_sum_nofacturables,$horas_por_empleado_actividad_admin_nolaborables);
            }

            $nolaborables_user =  array_sum($array_sum_nofacturables) + $horas_por_empleado_actividad_descanso_proyecto;
            // $acumulado



            // No productivo

            $actividadesnofacturablesnoproductivas =  $this->listaactividadesnofacturablesnoproductivas();

            $array_sum_nofacturables_noproductivas = [];
            foreach ($actividadesnofacturablesnoproductivas as $anfnp)
            {
                $horas_por_empleado_actividad_noproductivo_descanso_proyecto_retrabajos = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, $anfnp['pk'],$cpersona);
                array_push($array_sum_nofacturables_noproductivas,$horas_por_empleado_actividad_noproductivo_descanso_proyecto_retrabajos);
            }

            $nopro_user = array_sum($array_sum_nofacturables_noproductivas);

            $horas_por_empleado_actividad_noproductivo_descanso_proyecto_ = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, 4,$cpersona);
            //$horas_por_empleado_actividad_noproductivo_retrabajos_ = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, 11,$cpersona);



            $actividadesnoproductivos = $this->listaactividadesadministrativasnoproductivos();

            $array_sum_noproductivas = [];
            foreach ($actividadesnoproductivos as $acnoprod)
            {
                $horas_por_empleado_actividad_admin_noproductiva =  $this->obtener_horasnofacturablesadminuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$acnoprod['pk'],$cpersona);
                array_push($array_sum_noproductivas,$horas_por_empleado_actividad_admin_noproductiva);
            }

            $noproductiva_user =  array_sum($array_sum_noproductivas) + $nopro_user;

            // Productividad

            $sumfacnofacadmin_porusuario = $sumtotal_fac + $sumtotal_nofac + $sumtotal_admin + $sumtotal_internas ;
            $numerador_produ = $sumfacnofacadmin_porusuario - $noproductiva_user;
            $denominador_prod = $sumfacnofacadmin_porusuario - $nolaborables_user;

            $productividad_user = 0;
            if($denominador_prod  != 0)
            {
                $productividad_user =  round(($numerador_produ/$denominador_prod)*100);
            }

            // Cargabilidad

            $numerador_cargabilidad = $sumtotal_fac + $sumtotal_nofac - $horas_por_empleado_actividad_noproductivo_descanso_proyecto_;
            $denominador_cargabilidad = $real - $nolaborables_user;

            $cargabilidad_user = 0;
            if($denominador_cargabilidad != 0)
            {
                $cargabilidad_user = round(($numerador_cargabilidad/$denominador_cargabilidad)*100);
            }

            if ($productividad_user > 0 || $cargabilidad_user > 0) {
                
                array_push($array_fac_nofac_adm,[
                    'abreviatura' => $abreviatura,
                    'semana' => $semana,
                    'productividad_user' =>$productividad_user,
                    'cargabilidad_user' =>$cargabilidad_user,
                ]);

            }

        }

        return $array_fac_nofac_adm;

    }
 

public function actividadesadministrativas(Request $request)
{
    $semana = $this->numsemana($request->fecha_ini);
    $anio_inicial = $this->aniofecha($request->fecha_ini);
    $careas = $request->careas;
    $fecha_inicial_semana = $this->startweek($request->fecha_ini);
    $fecha_final_semana = $this->endweek($request->fecha_ini);

    // GRAFICA NO FACTURABLES

    $empleados = $this->empleados($careas,$fecha_inicial_semana,$fecha_final_semana);
    $empleadosejecutados = $this->empleadosejecutados($careas,$fecha_inicial_semana,$fecha_final_semana);

    $emple = array_merge($empleados,$empleadosejecutados);

    $empledos_list = array_unique($emple);

    $actividadesinternas = $this->listaactividadesadministrativasinternas();

    $arrayinternasactividades  = [];

    //dd("hoa",$actividadesinternas);
    foreach ($actividadesinternas  as $act)
    {

        $acti['cactividad']=$act['pk'];
        $acti['descripcion']=$act['descripcion'];
        $acumulativo_nuevo_admin_interno = 0;

        foreach ($empledos_list as $em) {

            $list = explode('-',$em);
            $cpersona = $list[0];
            $abreviatura = $list[1];
            $estado = $list[2];

            $horas_por_empleado_actividad_admin_internas = $this->obtener_horasnofacturablesadmininternas($careas, $semana,$fecha_inicial_semana,$fecha_final_semana, $act['pk'], $cpersona);
            $acumulativo_nuevo_admin_interno += floatval($horas_por_empleado_actividad_admin_internas);
        }
        $acti['sumatodos']=$acumulativo_nuevo_admin_interno;

        array_push($arrayinternasactividades,$acti);

    }


    //dd($arrayhorasnofact_nuevo);
}


    public function getFACNOFACADMIN(Request $request){



        $semana = $this->numsemana($request->fecha_ini);
        $semana_fin = $this->endweek($request->fecha_ini);
        $fecha_inicial_semana = $this->startweek($request->fecha_ini);
        $fecha_final_semana = $this->endweek($request->fecha_ini);
        $anio_inicial = $this->aniofecha($semana_fin);
        $careas = $request->careas;

        $empleados = $this->empleados($careas,$fecha_inicial_semana,$fecha_final_semana);
        $empleadosejecutados = $this->empleadosejecutados($careas,$fecha_inicial_semana,$fecha_final_semana);

        $emple = array_merge($empleados,$empleadosejecutados);

        $empledos_list = array_unique($emple);

        //dd($empledos_list);

        $array_fac_nofac_adm = [];

        $acumulativofac = 0;
        $acumulativonofac = 0;
        $acumulativoadmin = 0;
        $acumulativointernas = 0;
        $acumuladoreal = 0;
        $acumuladoplaneado = 0;
        $acumuladonolaborables = 0;
        $acumuladonoproductivo = 0;

        foreach ($empledos_list as $emp)
        {
            $list = explode('-',$emp);
            $cpersona = $list[0];
            $abreviatura = $list[1];
            $horas = $list[3];

            $sumtotal_fac =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,1,'externa');
            $sumtotal_nofac =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,2,'externa');
            $sumtotal_admin =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'externa');
            $sumtotal_internas =  $this->horasxempleado($careas,$cpersona,$semana,$fecha_inicial_semana,$fecha_final_semana,3,'interna');



            // acumulado

            $acumulativofac += $sumtotal_fac;
            $acumulativonofac += $sumtotal_nofac;
            $acumulativoadmin += $sumtotal_admin;
            $acumulativointernas += $sumtotal_internas;


            //Real
            $real = $sumtotal_fac + $sumtotal_nofac + $sumtotal_admin + $sumtotal_internas ;
                //acumulado
            $acumuladoreal += $real;

            // Planeado
             $acumuladoplaneado +=  $horas;
            // Cargabilidad

            // No Laborable

            $actividadesnofacturablesnolaborables =  $this->listaactividadesnofacturablesnolaborables();

            $array_sum_nofacturables_nolaborables = [];

            foreach ($actividadesnofacturablesnolaborables as $anol)
            {

                $horas_por_empleado_actividad_descanso_proyecto_2 = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, $anol['pk'],$cpersona);
                array_push($array_sum_nofacturables_nolaborables,$horas_por_empleado_actividad_descanso_proyecto_2);
            }

            $horas_por_empleado_actividad_descanso_proyecto = array_sum($array_sum_nofacturables_nolaborables);

            $actividadesnolaborables = $this->listaactividadesadministrativasnolaborables();
            $array_sum_nofacturables = [];
            foreach ($actividadesnolaborables as $acnolabo)
            {
                $horas_por_empleado_actividad_admin_nolaborables =  $this->obtener_horasnofacturablesadminuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$acnolabo['pk'],$cpersona);
                array_push($array_sum_nofacturables,$horas_por_empleado_actividad_admin_nolaborables);
            }

            $nolaborables_user =  array_sum($array_sum_nofacturables) + $horas_por_empleado_actividad_descanso_proyecto;
            // $acumulado

            $acumuladonolaborables +=$nolaborables_user;


            // No productivo

            $actividadesnofacturablesnoproductivas =  $this->listaactividadesnofacturablesnoproductivas();

            $array_sum_nofacturables_noproductivas = [];
            foreach ($actividadesnofacturablesnoproductivas as $anfnp)
            {
                $horas_por_empleado_actividad_noproductivo_descanso_proyecto_retrabajos = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, $anfnp['pk'],$cpersona);
                array_push($array_sum_nofacturables_noproductivas,$horas_por_empleado_actividad_noproductivo_descanso_proyecto_retrabajos);
            }

            $nopro_user = array_sum($array_sum_nofacturables_noproductivas);

            $horas_por_empleado_actividad_noproductivo_descanso_proyecto_ = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, 4,$cpersona);
            //$horas_por_empleado_actividad_noproductivo_retrabajos_ = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, 11,$cpersona);



            $actividadesnoproductivos = $this->listaactividadesadministrativasnoproductivos();

            $array_sum_noproductivas = [];
            foreach ($actividadesnoproductivos as $acnoprod)
            {
                $horas_por_empleado_actividad_admin_noproductiva =  $this->obtener_horasnofacturablesadminuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$acnoprod['pk'],$cpersona);
                array_push($array_sum_noproductivas,$horas_por_empleado_actividad_admin_noproductiva);
            }

            $noproductiva_user =  array_sum($array_sum_noproductivas) + $nopro_user;
            $acumuladonoproductivo += $noproductiva_user;

            // Productividad

            $sumfacnofacadmin_porusuario = $sumtotal_fac + $sumtotal_nofac + $sumtotal_admin + $sumtotal_internas ;
            $numerador_produ = $sumfacnofacadmin_porusuario - $noproductiva_user;
            $denominador_prod = $sumfacnofacadmin_porusuario - $nolaborables_user;

            $productividad_user = 0;
            if($denominador_prod  != 0)
            {
                $productividad_user =  round(($numerador_produ/$denominador_prod)*100);
            }

            // Cargabilidad

            $numerador_cargabilidad = $sumtotal_fac + $sumtotal_nofac - $horas_por_empleado_actividad_noproductivo_descanso_proyecto_;
            $denominador_cargabilidad = $real - $nolaborables_user;

            $cargabilidad_user = 0;
            if($denominador_cargabilidad != 0)
            {
                $cargabilidad_user = round(($numerador_cargabilidad/$denominador_cargabilidad)*100);
            }




            array_push($array_fac_nofac_adm,[
                'user' =>$cpersona,
                'abreviatura' => $abreviatura,
                'semana' => $semana,
                'sumuser_fac' => $sumtotal_fac,
                'sumuser_nofac' => $sumtotal_nofac,
                'sumuser_admin' => $sumtotal_admin,
                'sumuser_interno' => $sumtotal_internas,
                'real' => $real,
                'horasplaneado' => $horas,
                'nolaborables_user' =>$nolaborables_user,
                'noproductiva_user' =>$noproductiva_user,
                'productividad_user' =>$productividad_user,
                'cargabilidad_user' =>$cargabilidad_user,
            ]);

        }

        // ŕoductividad total

        $sumfacnofacadmin_total = $acumulativofac + $acumulativonofac + $acumulativoadmin + $acumulativointernas;
        $numeradortotal_produ = $sumfacnofacadmin_total - $acumuladonoproductivo;
        $denominadortotal_prod = $sumfacnofacadmin_total - $acumuladonolaborables;

        $productividad_total = 0;
        if($denominadortotal_prod != 0)
        {
            $productividad_total = round(($numeradortotal_produ/$denominadortotal_prod)*100);
        }

        // Cargabilidad Total

        $descansototalarea = $this-> obtener_horasnofacturables($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,4);
        $numerador_cargabilidad_total = $acumulativofac + $acumulativonofac - $descansototalarea;
        $denominador_cargabilidad_total =  $acumuladoreal - $acumuladonolaborables;

        $cargabilidad_total = 0;
        if($denominador_cargabilidad_total != 0)
        {
            $cargabilidad_total = round(($numerador_cargabilidad_total/$denominador_cargabilidad_total)*100);
        }


        return view('proyecto.CargabilidadSemanal.FACNOFACADMINporpersonal', compact('array_fac_nofac_adm',
            'semana',
            'acumulativofac',
            'acumulativonofac',
            'acumulativoadmin',
            'acumulativointernas',
            'acumuladoreal',
            'acumuladoplaneado',
            'acumuladonolaborables',
            'acumuladonoproductivo',
            'productividad_total',
            'cargabilidad_total'));
    }
    public function getActividadesNOFAC(Request $request)
    {

        $semana = $this->numsemana($request->fecha_ini);
        $semana_fin = $this->endweek($request->fecha_ini);
        $anio_inicial = $this->aniofecha($semana_fin);
        $fecha_inicial_semana = $this->startweek($request->fecha_ini);
        $fecha_final_semana = $this->endweek($request->fecha_ini);
        $careas = $request->careas;
        $tipo = $request->tipo;

        $empleados = $this->empleados($careas,$fecha_inicial_semana,$fecha_final_semana);
        $empleadosejecutados = $this->empleadosejecutados($careas,$fecha_inicial_semana,$fecha_final_semana);

        $emple = array_merge($empleados,$empleadosejecutados);

        $empledos_list = array_unique($emple);

        $empleados_lista = [];

        foreach ($empledos_list as $key => $value) {
            $list = explode('-',$value);
            $e['cpersona'] =  $cpersona = $list[0];
            $e['abreviatura'] =  $abreviatura = $list[1];
            $e['estado'] =  $estado = $list[2];

            array_push($empleados_lista, $e);
            
        }   

        if($request->tipo == '1')
        {
            $actividades = $this->listaactividadesnofacturables();


             $arrayhorasnofact_nuevo=[];
            $acumuladoporarea = 0;

            foreach ($actividades as $act)
            {
                $ac['cactividad']=$act['pk'];
                $ac['descripcion']=$act['descripcion'];
                $arrayhorasnofact = [];
                $acumulativo_nuevo = 0;
                foreach ($empledos_list as $em)
                {

                    $list = explode('-',$em);
                    $cpersona = $list[0];
                    $abreviatura = $list[1];
                    $estado = $list[2];

                        $horas_por_empleado_actividad = $this->obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana, $act['pk'],$cpersona);
                        // $careas,$semana,$anio,$actnofact,$user
                        $emp['cpersona']=$abreviatura;
                        $emp['cpersonaid']=$cpersona;
                        $emp['sumempleadoactivity'] = floatval($horas_por_empleado_actividad);
                        $acumulativo_nuevo +=  $emp['sumempleadoactivity'];
                        array_push($arrayhorasnofact,$emp);
                }
                $ac['empleados']=$arrayhorasnofact;
                $ac['sumatodos']=$acumulativo_nuevo;
                $acumuladoporarea +=$ac['sumatodos'];

                array_push($arrayhorasnofact_nuevo,$ac);

            }


        }
       elseif($request->tipo == '2') {

            $actividades = $this->listaactividadesadministrativas();
            $arrayhorasnofact_nuevo=[];
            $acumuladoporarea = 0;
            //$acumuladoporareaemplado = 0;

            foreach ($actividades as $act)
            {
                $ac['cactividad']=$act['pk'];
                $ac['descripcion']=$act['descripcion'];
                $arrayhorasnofact = [];
                $acumulativo_nuevo = 0;

                foreach ($empledos_list as $em)
                {

                    $list = explode('-',$em);
                    $cpersona = $list[0];
                    $abreviatura = $list[1];
                    $estado = $list[2];

                    $horas_por_empleado_actividad_admin = $this->obtener_horasnofacturablesadminuser($careas,$semana,$fecha_inicial_semana,$fecha_final_semana,$act['pk'],$cpersona);
                    $emp['cpersona']=$abreviatura;
                    $emp['cpersonaid']=$cpersona;
                    $emp['sumempleadoactivity'] =  floatval($horas_por_empleado_actividad_admin);
                    $acumulativo_nuevo +=  $emp['sumempleadoactivity'];
                    array_push($arrayhorasnofact,$emp);

                }

                $ac['empleados']=$arrayhorasnofact;
                $ac['sumatodos']=$acumulativo_nuevo;
                $acumuladoporarea +=$ac['sumatodos'];
                //$acumuladoporareaemplado +=$acumulativo_nuevo;
                array_push($arrayhorasnofact_nuevo,$ac);
            }
        }

       // dd($arrayhorasnofact_nuevo);
        $arrayempleadosactivity = [];
        foreach ($arrayhorasnofact_nuevo as $ahnf){

            foreach ($ahnf['empleados'] as $empled){
                $repeat=false;
                for($i=0;$i<count($arrayempleadosactivity);$i++)
                {
                    if($arrayempleadosactivity[$i]['cpersonaid']==$empled['cpersonaid'])
                    {
                        $arrayempleadosactivity[$i]['sumempleadoactivity']+=$empled['sumempleadoactivity'];
                        $repeat=true;
                        break;
                    }
                }
                if($repeat==false)
                    $arrayempleadosactivity[] = array('cpersonaid' => $empled['cpersonaid'], 'sumempleadoactivity' => $empled['sumempleadoactivity']);
            }
        }

        return view('proyecto.CargabilidadSemanal.activividadesNOFACT', compact('actividades','semana','empleados_lista','arrayhorasnofact_nuevo','acumuladoporarea','acumuladoporareaemplado','arrayempleadosactivity','tipo'));
    }
    public function sumtotalhorasproyectos($proyecto)
    {
        $sumtotal = DB::table('tproyecto as tp')
                  ->leftjoin('tproyectoejecucion as tpe', 'tp.cproyecto', '=', 'tpe.cproyecto')
                 ->leftjoin('tpersonadatosempleado as tpers', 'tpe.cpersona_ejecuta', '=', 'tpers.cpersona')
                 ->where('tpe.cproyecto','=',$proyecto)
                 //->where('tpe.cpersona_ejecuta','=',$cpersona)
                ->select('tpe.cproyecto',DB::raw('sum(tpe.horasejecutadas) as sum'))
                ->groupBy('tpe.cproyecto')
                ->first();

        return $sumtotal->sum;
    }
    public function horasxempleado($careas,$userarea,$semana,$fecha_inicial,$fecha_final,$tipo,$valor)
    {
        $horasxempleado  = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tpersonadatosempleado as tpers', 'tpe.cpersona_ejecuta', '=', 'tpers.cpersona')
            ->leftjoin('tpersona as tpde', 'tpe.cpersona_ejecuta', '=', 'tpde.cpersona')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpers.carea')
            //->select(DB::raw('sum(tpe.horasejecutadas) as sum'))
            ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            // ->where(DB::raw('extract(year from fplanificado)'),'=',$anio)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.tipo','=',$tipo)
            ->where('tpe.cpersona_ejecuta', '=', $userarea);

            if ($tipo=='3') {
                
                if ($valor == 'interna') {
                $horasxempleado = $horasxempleado->wherenull('tpe.cactividad');

                }
                else {
                $horasxempleado = $horasxempleado->wherenull('tpe.cproyectoactividades');
                }
            }


            $horasxempleado = $horasxempleado->sum('tpe.horasejecutadas');


        $sumatoria_horas = 0;
            if($horasxempleado != null)
            {
                $sumatoria_horas = floatval($horasxempleado);
            }


         return $sumatoria_horas;
    }

    public function empleados($careas,$fecha_inicial,$fecha_final)
    {
        $empleados =  DB::table('tpersonadatosempleado as tpers')
            ->join('tpersona as tpe', 'tpe.cpersona', '=', 'tpers.cpersona')
            ->join('tusuarios as tu','tu.cpersona','=','tpers.cpersona')
            ->join('tproyectoejecucion as tpry','tpry.cpersona_ejecuta','=','tpe.cpersona')
            ->leftjoin('ttipocontrato as tpc', 'tpc.ctipocontrato', '=', 'tpers.ctipocontrato')
            // ->select('tpers.cpersona','tpe.abreviatura','tpers.estado')
            // ->addselect(DB::raw('sum(tpry.horasejecutadas) as suma'),DB::raw("extract(week from tpry.fplanificado) as semana"))
            ->where('tpry.carea','=',$careas)
            ->where('tpers.estado','=','ACT')
            // ->where('tpry.fplanificado','>=',$fecha_inicial)
            // ->where('tpry.fplanificado','<=',$fecha_final)
            // ->groupBy('tpe.abreviatura','tpers.cpersona','tpers.estado',DB::raw("extract(week from tpry.fplanificado)"))
            // ->orderBy('tpe.abreviatura','ASC')
            ->distinct()
            ->lists(DB::raw('CONCAT(tpers.cpersona,\'-\',tpe.abreviatura,\'-\',tpers.estado,\'-\',tpc.horas) as nombre'));

           // dd( $empleados );

        return $empleados;

    }

    public function empleadosejecutados($careas,$fecha_inicial,$fecha_final)
    {
        $empleadosejecutados =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
            ->leftjoin('tpersonadatosempleado as tpde','tpde.cpersona','=','tp.cpersona')
            ->leftjoin('ttipocontrato as tpc', 'tpc.ctipocontrato', '=', 'tpde.ctipocontrato')
            // ->select('tp.cpersona','tp.abreviatura','tpde.estado')
            // ->addselect(DB::raw('sum(tpe.horasejecutadas) as suma'),DB::raw("extract(week from tpe.fplanificado) as semana"))
            ->where('tpe.carea','=',$careas)
            // ->where('tpe.cpersona_ejecuta','=',$cpersona)
            // ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            // ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            // ->where(DB::raw('sum(tpe.horasejecutadas)'),'>',0)
            // ->groupBy('tp.abreviatura','tp.cpersona','tpde.estado',DB::raw("extract(week from tpe.fplanificado)"))
            // ->sum('tpe.horasejecutadas');
            // ->orderBy('tp.abreviatura','ASC')
            ->distinct()
            ->lists(DB::raw('CONCAT(tp.cpersona,\'-\',tp.abreviatura,\'-\',tpde.estado,\'-\',tpc.horas) as nombre'));
            //dd($empleadosejecutados);

        return $empleadosejecutados;

    }


    public function empleadoshorasplaneado($careas)
    {
        $empleados =  DB::table('tpersonadatosempleado as tpers')
            ->leftjoin('tpersona as tpe', 'tpe.cpersona', '=', 'tpers.cpersona')
            ->leftjoin('ttipocontrato as tpc', 'tpc.ctipocontrato', '=', 'tpers.ctipocontrato')
            ->select('tpe.abreviatura','tpers.cpersona','tpc.horas')
            ->where('tpers.carea','=',$careas)
            ->where('tpers.estado','=','ACT')
            ->orderBy('tpe.abreviatura','ASC')
            ->get();

        return $empleados;

    }
    public function listaactividadesnofacturables()
    {
        $actividadesNOFacturable = DB::table('ttiposnofacturables as ttnf')
                                ->select('ttnf.ctiponofacturable','ttnf.descripcion')
                                ->orderBy('ttnf.descripcion' ,'ASC')
                                ->get();
         $arrayNOFACT = [];
        foreach ($actividadesNOFacturable as $act)
        {
            $col['pk'] = $act->ctiponofacturable;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayNOFACT,$col);
        }

        return $arrayNOFACT;
    }
    public function listaactividadesnofacturablesdescansoproyecto()
    {
        $actividadesNOFacturable = DB::table('ttiposnofacturables as ttnf')
            ->select('ttnf.ctiponofacturable','ttnf.descripcion')
            ->where('ttnf.ctiponofacturable','=','4')
            ->first();

        return $actividadesNOFacturable->ctiponofacturable;
    }
    public function obtener_horasnofacturables($careas,$semana,$fecha_inicial,$fecha_final,$actnofact)
    {

        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
                                    ->leftjoin('ttiposnofacturables as ttnf', 'ttnf.ctiponofacturable', '=', 'tpe.ctiponofacturable')
                                    // ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
                                    // ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
                                    ->where('tpe.fplanificado','>=',$fecha_inicial)
                                    ->where('tpe.fplanificado','<=',$fecha_final)
                                    ->where('tpe.carea','=',$careas)
                                    ->where('tpe.tipo','=',2)
                                    ->where('tpe.ctiponofacturable','=',$actnofact)
                                    ->sum('tpe.horasejecutadas');

        return $horasxempleadoofacturables;

    }

    public function obtener_horasnofacturablesuser($careas,$semana,$fecha_inicial,$fecha_final,$actnofact,$user)
    {

        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('ttiposnofacturables as ttnf', 'ttnf.ctiponofacturable', '=', 'tpe.ctiponofacturable')
            ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            // ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cpersona_ejecuta','=',$user)
            ->where('tpe.tipo','=',2)
            ->where('tpe.ctiponofacturable','=',$actnofact)
            ->sum('tpe.horasejecutadas');

        return $horasxempleadoofacturables;

    }

    public function listaactividadesadministrativas()
    {
        $actividadesadministrativas = DB::table('tactividad as ta')
            ->select('ta.cactividad','ta.descripcion')
            ->wherenotNull('ta.cactividad_parent')
            ->where('tipoactividad','=','00002')
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->cactividad;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }
    public function listaactividadesadministrativasnolaborables()
    {
        $actividadesadministrativas = DB::table('tactividad as ta')
            ->select('ta.cactividad','ta.descripcion')
            ->wherenotNull('ta.cactividad_parent')
            ->wherenotNull('ta.cactividad_nolaborable')
            ->orderBy('cactividad','ASC')
            ->where('tipoactividad','=','00002')
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->cactividad;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }
    public function listaactividadesadministrativasnoproductivos()
    {
        $actividadesadministrativas = DB::table('tactividad as ta')
            ->select('ta.cactividad','ta.descripcion')
            ->wherenotNull('ta.cactividad_parent')
            ->wherenotNull('ta.cactividad_noproductiva')
            ->orderBy('cactividad','ASC')
            ->where('tipoactividad','=','00002')
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->cactividad;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }

    public function listaactividadesadministrativasinternas()
    {
        $actividadesadministrativasADMIN = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyecto as tp', 'tp.cproyecto', '=', 'tpa.cproyecto')
            ->select('tpa.cproyectoactividades','tpa.descripcionactividad')
            //->where('tp.tipoproy','=','00001')
            ->get();

        $arrayADMIN_INTERNOS = [];
        foreach ($actividadesadministrativasADMIN as $act)
        {
            $col['pk'] = $act->cproyectoactividades;
            $col['descripcion'] = $act->descripcionactividad;
            array_push($arrayADMIN_INTERNOS,$col);
        }

        return $arrayADMIN_INTERNOS;
    }
    public function obtener_horasnofacturablesadmininternas($careas,$semana,$fecha_inicial,$fecha_final,$tiponofac,$userarea)
    {
        //dd($careas,$semana,$tiponofac,$userarea);
        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tpersonadatosempleado as tpers', 'tpe.cpersona_ejecuta', '=', 'tpers.cpersona')
            ->leftjoin('tpersona as tpde', 'tpe.cpersona_ejecuta', '=', 'tpde.cpersona')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpers.carea')
            ->leftjoin('tactividad as tact', 'tact.cactividad', '=', 'tpe.cactividad')
            ->select('tpde.abreviatura','tpe.cpersona_ejecuta',DB::raw('sum(tpe.horasejecutadas) as sum'),'tpe.cactividad','tact.descripcion')
            ->groupBy('tpde.abreviatura','tpe.cpersona_ejecuta','tpe.cactividad','tact.descripcion')
            ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            ->where('tpe.cactividad','=',$tiponofac)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cpersona_ejecuta','=',$userarea);


            if($tiponofac == '3' )
            {

                $horasxempleadoofacturables =  $horasxempleadoofacturables->whereNull('tpe.cproyectoactividades');

            }


        $horasxempleadoofacturables = $horasxempleadoofacturables->distinct()->get();

        $sumatoria_horas=0;
        foreach ($horasxempleadoofacturables as $he) {
            if ($he->sum != null) {
                $sumatoria_horas= $he->sum;
            }
        }

        return $sumatoria_horas;
    }
    public function obtener_horasnofacturablesadmin($careas,$semana,$fecha_inicial,$fecha_final,$cactividad)
    {
        //dd($careas,$semana,$tiponofac,$userarea);
        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tactividad as tact', 'tact.cactividad', '=', 'tpe.cactividad')
            // ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            // ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)

            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cactividad','=',$cactividad)
            ->where('tpe.tipo','=',3)
            ->whereNotNull('tpe.cactividad')
            ->sum('tpe.horasejecutadas');

        $sumatoria_horas = 0;
        if($horasxempleadoofacturables != null)
        {
            $sumatoria_horas = $horasxempleadoofacturables;
        }

        return $sumatoria_horas;
    }
    public function obtener_horasnofacturablesadminuser($careas,$semana,$fecha_inicial,$fecha_final,$cactividad,$user)
    {
        //dd($careas,$semana,$tiponofac,$userarea);
        $horasxempleadoofacturables = DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tactividad as tact', 'tact.cactividad', '=', 'tpe.cactividad')
            // ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            // ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cactividad','=',$cactividad)
            ->where('tpe.cpersona_ejecuta','=',$user)
            ->where('tpe.tipo','=',3)
            ->whereNotNull('tpe.cactividad')
            ->sum('tpe.horasejecutadas');

        $sumatoria_horas = 0;
        if($horasxempleadoofacturables != null)
        {
            $sumatoria_horas = $horasxempleadoofacturables;
        }

        return $sumatoria_horas;
    }

    public function areas(){

        $user = Auth::user();

        $cargoid =  DB::table('tpersonadatosempleado as tpde')
            ->select('tpde.ccargo')
            ->where('tpde.cpersona','=',$user->cpersona)
            ->lists('tpde.ccargo');

        //Obtiene los cargos hijos
        $cargos_hijos=$this->obtener_cargos_hijos($cargoid);

        $array_cargos_hijos_todos=[];

        array_push($array_cargos_hijos_todos, $cargos_hijos);

        //obtiene los cargos nietos
        while ( $cargos_hijos != null) {

            $cargos_hijos=$this->obtener_cargos_hijos($cargos_hijos);
            array_push($array_cargos_hijos_todos,$cargos_hijos);
        }

        $careas = [''=>''];

        $cargos_hijos_todos=[];


        if ($array_cargos_hijos_todos) {

            //Coloca en un array unidimensional todos los cargos nietos e hijos encontrados
            foreach ($array_cargos_hijos_todos as $ach) {

                foreach ($ach as $ch) {
                    array_push($cargos_hijos_todos, $ch);
                }

            }

            //Obtiene todos los colaboradores con los cargos encontrados y lista sus areas
            $careas_colaboradores =DB::table('tpersonadatosempleado')   
                ->where('estado','=','ACT')
                ->whereIn('ccargo',$cargos_hijos_todos)   
                ->select('carea')   
                ->distinct()  
                ->lists('carea');

            //Obtiene las areas de los colaboradores encontrados en $careas_colaboradores
            $careas = DB::table('tareas')
                ->whereIn('carea',$careas_colaboradores)
                ->orderBy('codigo_sig','ASC')
                ->select('carea','descripcion','codigo_sig','careaparentdespliegue')
                ->get();
           
        }

         return $careas;
    }

    private function obtener_cargos_hijos($ccargo=[]){

        $cargos_hijos =  DB::table('tcargos as tc')
            ->select('tc.ccargo','tc.cargoparentdespliegue')
            ->whereIn('tc.cargoparentdespliegue',$ccargo)
            ->lists('tc.ccargo');
   
        return $cargos_hijos;

    }
    public function areas2()
    {
        $user = Auth::user();

        $cargoid =  DB::table('tpersonadatosempleado as tpde')
            ->select('tpde.ccargo')
            ->where('tpde.cpersona','=',$user->cpersona)
            ->first();

     /*   $cargogerenciageneral =  DB::table('tpersonadatosempleado as tpde')
            ->select('tpde.ccargo')
            ->where('tpde.cpersona','=',3829)
            ->get();*/


        $cargo_parent =  DB::table('tcargos as tc')
            ->select('tc.ccargo','tc.cargoparentdespliegue')
            ->where('tc.cargoparentdespliegue','=',$cargoid->ccargo)
            ->get();

        $cargo_array = [];
        foreach ($cargo_parent as $ccar)
        {
            $c['ccargo'] = $ccar->ccargo;
            array_push($cargo_array,$c);
        }

        //dd($cargo_parent,$cargo_array);
        //

        $careasdata =DB::table('tpersonadatosempleado as tpde')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpde.carea')
            ->select('tpde.carea','ta.descripcion','ta.codigo_sig','ta.careaparentdespliegue')
            ->where('tpde.estado','=','ACT')
            ->whereIn('tpde.ccargo',$cargo_array)
           // ->where('ta.codigo_sig', 'NOT LIKE', '%G%')
            ->distinct('tpde.carea','ta.descripcion','ta.codigo_sig','ta.careaparentdespliegue')
            ->orderBy('ta.codigo_sig','ASC')
            ->get();

        $careas_gerencia =DB::table('tpersonadatosempleado as tpde')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpde.carea')
            ->select('tpde.carea','ta.descripcion','ta.codigo_sig')
            ->where('tpde.estado','=','ACT')
            ->whereIn('tpde.ccargo',$cargo_array)
            //->where('ta.codigo_sig', 'LIKE', '%G%')
            ->distinct('tpde.carea','ta.descripcion','ta.codigo_sig')
            ->orderBy('ta.codigo_sig','ASC')
            ->get();

        $area_gerencia_array = [];
        foreach ($careas_gerencia as $ccar)
        {
            $d['carea'] = $ccar->carea;
            array_push($area_gerencia_array,$d);
        }

        $careas_real =DB::table('tpersonadatosempleado as tpde')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpde.carea')
            ->select('tpde.carea','ta.descripcion','ta.codigo_sig','ta.careaparentdespliegue')
            ->where('tpde.estado','=','ACT')
            ->whereIn('ta.careaparentdespliegue',$area_gerencia_array)
            ->distinct('tpde.carea','ta.descripcion','ta.codigo_sig','ta.careaparentdespliegue')
            ->orderBy('ta.codigo_sig','ASC')
            ->get();

       // dd($careasdata, $careas_real);

        $careas = array_merge($careasdata, $careas_real);
       /* if ($cargogerenciageneral == true)
        {
            $careas =  DB::table('tproyectoejecucion as tpe')
                ->leftjoin('tpersonadatosempleado as tpde', 'tpe.cpersona_ejecuta', '=', 'tpde.cpersona')
                ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpde.carea')
                ->groupBy('tpde.carea','ta.descripcion','ta.codigo_sig')
                ->select('tpde.carea','ta.descripcion','ta.codigo_sig')
                ->orderBy('ta.codigo_sig','ASC')
                ->get();

        }*/

        //$orderarea  =asort($careas);
        //dd($orderarea);
        dd($careas);
        return $careas;

    }
    public function numsemana($fecha)
    {
        $numero_semana = $fecha;
        if (substr($numero_semana, 2, 1) == '/' || substr($numero_semana, 2, 1) == '-') {
            $numero_semana = Carbon::create(substr($numero_semana, 6, 4), substr($numero_semana, 3, 2), substr($numero_semana, 0, 2));

        } else {
            $numero_semana = Carbon::create(substr($numero_semana, 0, 4), substr($numero_semana, 5, 2), substr($numero_semana, 8, 2));
        }


        $semana = $numero_semana->weekOfYear;

        return $semana;

    }
    public function startweek($fecha)
    {
        $numero_semana = $fecha;
        if (substr($numero_semana, 2, 1) == '/' || substr($numero_semana, 2, 1) == '-') {
            $numero_semana = Carbon::create(substr($numero_semana, 6, 4), substr($numero_semana, 3, 2), substr($numero_semana, 0, 2));

        } else {
            $numero_semana = Carbon::create(substr($numero_semana, 0, 4), substr($numero_semana, 5, 2), substr($numero_semana, 8, 2));
        }


        $start = $numero_semana->startOfWeek();

        return $start;
    }
    public function endweek($fecha)
    {
        $numero_semana = $fecha;
        if (substr($numero_semana, 2, 1) == '/' || substr($numero_semana, 2, 1) == '-') {
            $numero_semana = Carbon::create(substr($numero_semana, 6, 4), substr($numero_semana, 3, 2), substr($numero_semana, 0, 2));

        } else {
            $numero_semana = Carbon::create(substr($numero_semana, 0, 4), substr($numero_semana, 5, 2), substr($numero_semana, 8, 2));
        }


        $end = $numero_semana->endOfWeek();

        return $end;
    }

    public  function aniofecha($fecha)
    {
        $aniofecha = $fecha;
        if (substr($aniofecha, 2, 1) == '/' || substr($aniofecha, 2, 1) == '-') {
            $aniofecha = Carbon::create(substr($aniofecha, 6, 4), substr($aniofecha, 3, 2), substr($aniofecha, 0, 2));

        } else {
            $aniofecha = Carbon::create(substr($aniofecha, 0, 4), substr($aniofecha, 5, 2), substr($aniofecha, 8, 2));
        }


        $anio = $aniofecha->format('Y');;

        return $anio;

    }

    public function listaproyectosfacnofacadmin($careas,$semana,$fecha_inicial,$fecha_final,$tipo)
    {
        $proyectos =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tproyecto as tp', 'tp.cproyecto', '=', 'tpe.cproyecto')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->leftjoin('testadoproyecto as tep', 'tep.cestadoproyecto', '=', 'tp.cestadoproyecto')
            ->leftjoin('tpersona as tper', 'tp.cpersonacliente', '=', 'tper.cpersona')
            ->select(DB::raw("SUM(tpe.horasejecutadas) as sum"),'tp.codigo','tpe.cproyecto','tpe.carea','tpe.tipo','tum.codigo as cunidadmin','tum.nombre as nombreunidadminera','tep.descripcion as estadoo','tp.nombre as description','tper.nombre as nombrecliente')
            ->groupBy('tp.codigo','tpe.cproyecto','tpe.carea','tpe.tipo','cunidadmin','nombreunidadminera','estadoo','description','nombrecliente')
            ->where('tpe.carea','=',$careas)
           // ->where('tpe.cproyecto','=',5537)
            // ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)
            // ->where(DB::raw('extract(year from fplanificado)'),'=',$anio_inicial)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)
            ->where('tpe.tipo','=',$tipo)
            // if ($tipo=='3') {
                
                // if ($valor == 'interna') {
                ->wherenull('tpe.cactividad')

                // }
                // else {
                // $horas = $horas->wherenull('tpe.cproyectoactividades');
                // }
            // }
            ->orderBy('tum.codigo','ASC')
            ->get();

        return $proyectos;

    }

    public function listaproyectosfacnofacadminuser($cproyecto,$careas,$semana,$fecha_inicial,$fecha_final,$tipo,$user)
    {
        $proyectos =  DB::table('tproyectoejecucion as tpe')
            ->leftjoin('tproyecto as tp', 'tp.cproyecto', '=', 'tpe.cproyecto')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->leftjoin('testadoproyecto as tep', 'tep.cestadoproyecto', '=', 'tp.cestadoproyecto')
            ->leftjoin('tpersona as tper', 'tp.cpersonacliente', '=', 'tper.cpersona')
            ->where('tpe.carea','=',$careas)
            ->where('tpe.cproyecto','=',$cproyecto)
            ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$semana)

            // ->where(DB::raw("extract(year from tpe.fplanificado)"),'=',$anio)
            ->where('tpe.fplanificado','>=',$fecha_inicial)
            ->where('tpe.fplanificado','<=',$fecha_final)

            ->where('tpe.tipo','=',$tipo)
            ->where('tpe.cpersona_ejecuta','=',$user)
            ->sum('tpe.horasejecutadas');

        return $proyectos;

    }

    public function listaactividadesnofacturablesnolaborables()
    {
        $actividadesadministrativas = DB::table('ttiposnofacturables as ta')
            ->select('ta.ctiponofacturable','ta.descripcion')
            ->where('ta.cactividadnofacturable_nolaborable','=',1)
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->ctiponofacturable;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }

    public function listaactividadesnofacturablesnoproductivas()
    {
        $actividadesadministrativas = DB::table('ttiposnofacturables as ta')
            ->select('ta.ctiponofacturable','ta.descripcion')
            ->where('ta.cactividadnofacturable_noproductiva','=', 2)
            ->get();

        $arrayADMIN = [];
        foreach ($actividadesadministrativas as $act)
        {
            $col['pk'] = $act->ctiponofacturable;
            $col['descripcion'] = $act->descripcion;
            array_push($arrayADMIN,$col);
        }

        return $arrayADMIN;
    }






}
