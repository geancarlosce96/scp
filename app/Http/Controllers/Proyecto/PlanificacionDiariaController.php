<?php

namespace App\Http\Controllers\Proyecto;

use  App\Http\Controllers\Controller;
use App\Models\Planificacion;
use App\Models\Planificacion_log;
use App\Models\Tpersona;
use App\Models\Tpersonadatosempleado;
use App\Models\Tproyecto;
use App\Models\Tproyectoejecucion;
use App\Models\Tproyectohonorario;
use App\Models\Tproyectopersona;
use App\Models\Tusuario;
use App\Models\Planificacion_diaria;
use Auth;
use Date;
use DB;
use Illuminate\Http\Request;

class PlanificacionDiariaController extends Controller
{
	//public 
	public function diarias()
	{
		$usuario = Auth::user()->nombre;
		$fecha = Date("Y-m-d");
		$proyectos = [];
		return view("proyecto.planificacion.diaria", 
			["usuario" => $usuario, "fecha" => $fecha, "proyectos" => $proyectos]);
	}

	//ajax

	public function resumen(Request $request)
	{
		$fecha = $request->input("fecha");
		$cproyecto = $request->input("cproyecto");
		$tipo = $request->input("tipo");

		$data = [];
		$matriz = [];
		$cab = [];
		$total = [];		
		$cpersona = Auth::user()->cpersona;
		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();

		if($tipo == "ResumenProVSPer")
			$this->_generarResumenProVSPer();
		else if ($tipo == "ResumenActVsPer")
			$this->_generarResumenActVsPer($cproyecto, $fecha, $persona->carea, $matriz, $cab, $total);


		$data["matriz"] = $matriz;
				
		if($tipo == "ResumenProVSPer")
			return view("text");
		else if ($tipo == "ResumenActVsPer")
			return view("proyecto.planificacion.resumen_diario_act_vs_personas", $data);

		return $matriz;		
	}

	public function obtenerUsuariosdeProyecto(Request $request, $cproyecto)
	{
		$obj = [];
		$cpersona = Auth::user()->cpersona;
		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();		

		//obtener periodo, semana
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");
		$fecha = $request->input("fecha");
		$cproyectoactividades =  $request->input("cproyectoactividades");
		$mostrarUsuariosConHoras = $request->input("mostrarUsuariosConHoras");

		//usuarios		
		$lista = Tproyecto::obtenerUsuariosDeProyectoConActividadHabilitada($cproyecto, $persona->carea, $cproyectoactividades);

		foreach ($lista as $key => $item)
			//$item->horas = Tproyecto::obtenerHoraPlanificadaSegunProyectoActividadFecha($cproyectoactividades, $item->cpersona, $fecha);
			$item->horas = Tproyecto::obtenerHorasDeSemana($periodo, $semana, $cproyectoactividades, $item->user);

		if($mostrarUsuariosConHoras == "true")
			$lista = $this->filtrarUsuariosConHorasPlanificadas($lista);

		$obj["lista"] = $lista;
		return $obj;
	}

	public function obtenerUsuariosDeArea(Request $request, $cproyecto)
	{
		$obj = [];
		$cpersona = Auth::user()->cpersona;
		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();
		$carea = $persona->carea;

		//obtener periodo, semana
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");
		$fecha = $request->input("fecha");
		$mostrarUsuariosConHoras = $request->input("mostrarUsuariosConHoras");
		$cproyectoactividades =  $request->input("cproyectoactividades");

		//usuarios
		$lista = Tproyecto::obtenerUsuariosDeArea($carea);

		//obtener usuarios de proyecto para comparar
		$usuariosDeProyecto = Tproyecto::obtenerUsuariosDeProyectoConActividadHabilitada($cproyecto, $persona->carea, $cproyectoactividades);

		foreach ($lista as $key => $item){
			//$item->horas = Tproyecto::obtenerHoraPlanificadaSegunProyectoActividadFecha($cproyectoactividades, $item->cpersona, $fecha);
			$item->usuarioProyecto = Tproyecto::esUsuarioDeProyecto($usuariosDeProyecto, $item->cpersona);
			$item->horas = Tproyecto::obtenerHorasDeSemana($periodo, $semana, $cproyectoactividades, $item->user, $item->usuarioProyecto);
		}

		$obj["lista"] = $lista;
		return $obj;
	}

	public function guardarHoras(Request $request)
	{
		//$comentarios = $request->input("comentario"); verificar*
		$comentarios = "";
		$cpersona = $request->input("cpersona");
		$cproyecto = $request->input("cproyecto");
		$cproyectoactividades = $request->input("cproyectoactividades");
		$fecha = $request->input("fecha");
		$horas = $request->input("horas");
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");
		$usuario = $request->input("usuario");
		$dia = $request->input("dia");

		$actividad = Tproyecto::guardarHoraPlanificadaSegunProyectoActividadFecha($cproyectoactividades, $cpersona, $fecha, $horas, $comentarios, $cproyecto);
		$actividad->horas = Tproyecto::guardarHoraEnSemana($periodo, $semana, $cproyectoactividades, $usuario, $dia, $horas, $comentarios);
		$actividad->cab = Tproyecto::guardarCabProyectoActividadSemanaPersona($cproyectoactividades, $semana, $cpersona);

		return $actividad;
	}

	public function obtenerActividades($cproyecto, Request $request)
	{
		$fecha = $request->input("fecha");
		$mostrarActividadesConHoras = $request->input("mostrarActividadesConHoras");

		$actividades = DB::table('tproyectoactividades as t')
                ->select('t.*')                
				->where("cproyecto", "=", $cproyecto)
				//->orderBy("codigoactvidad", "asc")
				->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
                ->get();

        if($mostrarActividadesConHoras == "true")
			$actividades = $this->filtrarActividadesConHorasRegistradas($actividades, $fecha);

       	//procesar 
        $this->_procesarActividades($actividades);
		
		$data["lista"] = $actividades;
        return $data;
	}

	//private

	private function _procesarActividades(&$actividades)
	{
		$posTemp = 0;

		for ($i=0; $i <count($actividades); $i++) { 
			if($i==0)
				$posTemp = $i;

			if($actividades[$posTemp]->cproyectoactividades == $actividades[$i]->cproyectoactividades_parent)
				$actividades[$posTemp]->permitirRegistro = false;
			
			$actividades[$i]->permitirRegistro = true;
			$posTemp = $i;
		}
	}

	private function filtrarUsuariosConHorasPlanificadas($lista){
		$listaTemp = [];

		foreach ($lista as $key => $usuario){
			if($usuario->horas != null){
				//if($usuario->horas["planificadas"] > 0)
				if($usuario->horas->planificadas > 0)
					array_push($listaTemp, $usuario);
			}
		}

		return $listaTemp;
	}

	private function filtrarActividadesConHorasRegistradas($lista, $fecha)
	{
		$listaTemp = [];

		foreach ($lista as $key => $item){
			$horas = Tproyectoejecucion::where("cproyectoactividades", "=", $item->cproyectoactividades)
				->where("ccondicionoperativa", "=", "PLA")
				->where("fplanificado", "=", $fecha)
				->sum('horasplanificadas');

			if($horas > 0)
				array_push($listaTemp, $item);
		}

		return $listaTemp;
	}

	private function _obtenerArrayPersonas($personas, &$arrayPersonas, &$arrayPersonasCodigo)
	{
		$arrayPersonas = [];
		$arrayPersonasCodigo = [];
		foreach ($personas as $key => $persona){
			array_push($arrayPersonas, $persona->nombre);
			array_push($arrayPersonasCodigo, $persona->cpersona);
		}
		return $arrayPersonas;
	}

	private function _obtenerArrayActividades($actividades, &$arrayActividades, &$arrayActividadesCodigo)
	{
		$arrayActividades = [];
		foreach ($actividades as $key => $actividad){
			array_push($arrayActividades, $actividad->descripcionactividad);
			array_push($arrayActividadesCodigo, $actividad->cproyectoactividades);
		}
		return $arrayActividades;
	}

	private function _inicializarMatrizActVsPer($arrayActividades, $arrayPersonas, &$matriz)
	{
		$cantidadColumnas = 1 + count($arrayPersonas) + 1; // columnas 1:actividades 2..n-1:personas n:total
		
		//#1 columna actividades
		//fila total por persona, que visualmente estara debajo de las actividades
		array_push($arrayActividades, "TOTAL");
		array_push($matriz, array_merge(["Actividades\Personas"], $arrayActividades));
		
		//#2 columna personas
		for ($i=0; $i < count($arrayPersonas); $i++) { 
			$temp = [];
			array_push($temp, $arrayPersonas[$i]); //fila con el nombre
			for ($j=0; $j < count($arrayActividades); $j++)
				array_push($temp, 0); //fila por cada actividad
			array_push($temp, 0); //fila total 
			array_push($matriz, $temp);
		}
		
		//#3 columna total
		$temp = [];
		array_push($temp, "TOTAL"); //fila total
		for ($j=0; $j < count($arrayActividades); $j++)
			array_push($temp, 0); //fila por cada actividad
		array_push($temp, 0); //fila total 
		array_push($matriz, $temp);
	}

	private function _completarDataMatrizActVsPer(&$matriz, $personasHoras, $arrayPersonasCodigo, $arrayActividadesCodigo)
	{
		//obtener personas horas tproyectoejecucion segun cproyecto, actividad, fecha
		foreach ($personasHoras as $key => $persona) {
			$cpersonaPos = $cproyectoactividades = -1;
			//obtener cpersona posicion en arrayPersonasCodigo			
			for ($i=0; $i < count($arrayPersonasCodigo); $i++) { 
				//echo "$arrayPersonasCodigo[$i] == $persona->cpersona_ejecuta";
				if($arrayPersonasCodigo[$i] == $persona->cpersona_ejecuta)
					$cpersonaPos = $i;
			}
			//obtener cproyectoactividades posicion en array arrayActividadesCodigo			
			for ($i=0; $i < count($arrayActividadesCodigo) ; $i++) { 
				if($arrayActividadesCodigo[$i] == $persona->cproyectoactividades)
					$cproyectoactividades = $i;
			}
			//agregar hora
			$matriz[$cpersonaPos+1][$cproyectoactividades+1] = $persona->horasplanificadas;
		}

		//completar totales, desde las posiciones donde se encuentra la informacion de horas 1..n-1
		for ($i=1; $i < count($matriz)-1; $i++) { 
			$totalVertical = 0;
			for ($j=1; $j <= count($arrayActividadesCodigo); $j++) { 
				$totalVertical += $matriz[$i][$j];
				$matriz[count($matriz)-1][$j] += $matriz[$i][$j];
			}
			$matriz[$i][$j] = $totalVertical; //totales verticales en la ultima fila 
		}
	}
	
	private function _generarResumenActVsPer($cproyecto, $fecha, $carea, &$matriz, &$arrayPersonas, &$total)
	{
		$matriz = [];		
		//obtener proyecto
		$proyecto = Tproyecto::where("cproyecto", "=", $cproyecto)->first();
		//obtener actividades
		$actividades = DB::table('tproyectoactividades as t')
                ->select('t.*')                
				->where("cproyecto", "=", $cproyecto)
				->orderBy("codigoactvidad", "asc")
                ->get();
        //arrayActividades
		$arrayActividades = $arrayActividadesCodigo = [];
        $this->_obtenerArrayActividades($actividades, $arrayActividades, $arrayActividadesCodigo);
		//obtener personas
		$personas = Tproyecto::_obtenerUsuariosDeProyecto($cproyecto, $carea);
		//arrayPersonas
		$arrayPersonas = $arrayPersonasCodigo = [];
		$this->_obtenerArrayPersonas($personas, $arrayPersonas, $arrayPersonasCodigo);
		//obtener personas y horas
		$personasHoras = Tproyectoejecucion::where("cproyecto", "=", $cproyecto)->where("ccondicionoperativa", "=", "PLA")
			->where("fplanificado", "=", $fecha)->get();		
		//inicializar matriz, 
		$this->_inicializarMatrizActVsPer($arrayActividades, $arrayPersonas, $matriz);
		//completar con data
		$this->_completarDataMatrizActVsPer($matriz, $personasHoras, $arrayPersonasCodigo, $arrayActividadesCodigo);
		//dd($matriz);
	}

	private function _generarResumenProVSPer()
	{
		return [];
	}


}
