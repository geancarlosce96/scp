<?php

namespace App\Http\Controllers\Proyecto;

use App\Http\Controllers\Controller;
use App\Models\Planificacion;
use App\Models\Planificacion_log;
use App\Models\Tpersona;
use App\Models\Tpersonadatosempleado;
use App\Models\Tproyecto;
use App\Models\Tproyectopersona;
use App\Models\Tusuario;
use App\Models\Tarea;
use Auth;
use DB;
use Illuminate\Http\Request;
use Date;
use App\Models\Tcategoriaprofesional;

class PlanificacionResumenController extends Controller
{
	//public 
	public function agregarPersona(Request $request){
		$ccategoriaprofesional = $request->input("ccategoriaprofesional");
		$cpersona = $request->input("cpersona");
		$cproyecto = $request->input("cproyecto");

		$detalle = new Tproyectopersona();
		$detalle->cpersona = $cpersona;
		$detalle->cpersona = $cpersona;
		$detalle->cproyecto = $cproyecto;
		$detalle->eslider = '0';

		$detalle->save();

		return $detalle;
	}

	public function resumen_panoramico($periodo=null, $semana=null)
	{
		if($periodo==null or $semana==null)
			return redirect("planificacion/horas");		

		$cpersona = Auth::user()->cpersona;
		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();
		$grupo_areas = Tarea::where('carea','=',$persona->carea)->first();
		//listado responsables (cpersona, nombre, cproyecto, codigo, proyecto)
		// $lideres = $this->_obtenerLideresDeArea($persona->carea);
		$lideres = $this->_obtenerLideresDeArea($grupo_areas->grupo_areas);
		//array de proyectos (cproyecto)
		$arrayCodProyectos = $arrayProyectos = $arrayCodigoProyectos = [];
		$this->_obtenerCodProyectos_toArray($lideres, $arrayCodProyectos, $arrayProyectos, $arrayCodigoProyectos);
		//listado de personas (cproyecto, cpersona, persona)
		$personas = $this->_obtenerPersonasDeProyectos($arrayCodProyectos,$persona->carea);		
		//array de personas (nombres) (categorias)
		$arrayPersonas = $arrayCategorias = $arrayCodPersonas = [];
		$this->_obtenerPersonas_toArray($personas, $arrayPersonas, $arrayCategorias, $arrayCodPersonas);
		//arrayCategoriasObjetos {categoria, numero_columnas} para mostrar en la tabla resumen panoramico
		$arrayCategoriasContador = $this->_obtenerCategorias_toArrayContador($arrayCategorias);
		//obtener horas planificadas
		$horasPlanificadas = $this->_obtenerPlanificacionDePersonas($periodo, $semana, $arrayCodProyectos);		
		//asignar horas a personas		
		$this->_actualizarHorasPlanificadas($personas, $horasPlanificadas);		
		//inicializar matriz		
		$matriz = [];
		$this->_inicializarMatriz($matriz, $lideres, count($arrayPersonas));
		//actualizar data matriz		
		$this->_actualizarDataMatriz($matriz, $personas, $arrayPersonas, $arrayCodProyectos);
		//obtener rowspan lideres
		$arrayRowspanLideres = $this->_obtenerRowspanLideres($matriz);
		$totalProfesional = [];
		$totalProyecto = [];
		//total por proyecto
		$this->_obtenerTotales($matriz, $totalProfesional, $totalProyecto, count($arrayPersonas));
		//horas disponibles
		$horasDiponibles = [];
		$this->_obtenerHorasDisponibles_toArray($arrayCodPersonas, $personas, $horasDiponibles, $periodo, $semana);
		//categorias 
		$listaCategorias = Tcategoriaprofesional::all();

		$fecha = date("Y-m-d h:i:s");		
		
		return view("proyecto.planificacion.panoramico", 
			["personas" => $arrayPersonas, "categorias" => $arrayCategorias, "matriz" => $matriz, 
			"periodo" => $periodo, "semana" => $semana, "arrayCodPersonas" => $arrayCodPersonas,
			"fecha" => $fecha, "arrayRowspanLideres" => $arrayRowspanLideres,
			"listaCategorias" => $listaCategorias, "arrayCodigoProyectos" => $arrayCodigoProyectos,
			"totalProfesional" => $totalProfesional, "totalProyecto" => $totalProyecto,
			"arrayProyectos" => $arrayProyectos, "arrayCodProyectos" => $arrayCodProyectos,
			"horasDiponibles" => $horasDiponibles, "arrayCategoriasContador" => $arrayCategoriasContador]);
	}

	public function guardar(Request $request)
	{
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");
		$cpersona = $request->input("cpersona");
		$codigoproyecto = $request->input("codigoproyecto");
		$disponibles = $request->input("horas");

		$cproyecto = Tproyecto::where("codigo", "=", $codigoproyecto)->first()->cproyecto;
		$usuario = Tusuario::where("cpersona", "=", $cpersona)->first()->nombre;

		$horas = Planificacion::where("periodo", "=", $periodo)->where("semana", "=", $semana)->where("cproyecto", "=", $cproyecto)->where("usuario", "=", $usuario)->first();

		if($horas==null)
			$horas = new Planificacion();

		$horas->cproyecto = $cproyecto;
		$horas->disponibles = 40 - $disponibles;
		$horas->periodo = $periodo;
		$horas->planificadas = $disponibles;
		$horas->semana = $semana;
		$horas->total = 40;
		$horas->usuario = $usuario;
		$horas->save();

		$disponibles = $horas->disponibles;

		//calcular disponibilidad
		$suma_planificadas = DB::table('planificacion')
                ->select(DB::raw('SUM(planificadas) as planificadas, SUM(planificadas_adm) as planificadas_adm'))                
				->where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)
                ->first();

		$disponibles = $horas->total - $suma_planificadas->planificadas;

		$this->guardar_log($cproyecto, $periodo, $semana, $usuario, $horas->planificadas, Auth::user()->nombre);

		Planificacion::where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)
          		->update(['disponibles' => $disponibles]);

		return ["msj" => "Ud ha registrado $horas->planificadas horas planificadas al usuario $usuario en la semana $semana del $periodo."];
	}

	//private

	private function guardar_log($cproyecto, $periodo, $semana, $usuario, $horas, $usuario_registro)
	{
		$log = new Planificacion_log();
		$log->periodo = $periodo;			
		$log->cproyecto = $cproyecto;
		$log->semana = $semana;
		$log->mensaje = "Usuario $usuario_registro planifico $horas horas al usuario $usuario en la semana $semana del $periodo.";
		$log->save();
	}

	private function _obtenerLideresDeArea($carea)
	{
		$lista = DB::table("tproyectopersona as pro")
			->join("tpersonadatosempleado as de", "de.cpersona", "=", "pro.cpersona")
			->join("tpersona as p", "p.cpersona", "=", "pro.cpersona")
			->join("tproyecto as proy", "proy.cproyecto", "=", "pro.cproyecto")
			->join("tusuarios as u", "u.cpersona", "=", "p.cpersona")
			->join('tareas as ta','ta.carea','=','de.carea')
			->join('tcategoriaprofesional as tcp','tcp.ccategoriaprofesional','=','de.ccategoriaprofesional')
			->select("pro.cpersona", "p.abreviatura as persona_nombre", "pro.cproyecto", "proy.codigo", "proy.nombre as proyecto_descripcion")
			->where('u.estado','=','ACT')
			->where("pro.eslider", "=", "1")
			// ->where("de.carea", "=", $carea)
			->where('ta.grupo_areas','=',$carea)
			->orderBy("tcp.orden")
			->orderBy("p.abreviatura", "ASC")
			->orderBy("proy.nombre", "ASC")
			//->groupby('pro.cpersona')->distinct()
			->get();

		return $lista;
	}

	private function _obtenerCodProyectos_toArray($lideres, &$arrayCodProyectos, &$arrayProyectos, &$arrayCodigoProyectos)
	{
		$arrayCodProyectos = [];
		
		foreach ($lideres as $key => $lider) {
			if(!in_array($lider->cproyecto, $arrayCodProyectos)){
				array_push($arrayCodProyectos, $lider->cproyecto);
				array_push($arrayProyectos, $lider->proyecto_descripcion);
				array_push($arrayCodigoProyectos, $lider->codigo);
			}				
		}

		return $arrayCodProyectos;
	}

	private function _obtenerPersonasDeProyectos($arrayCodProyectos,$carea)
	{
		// $carea_parent = Tarea::where('carea','=',$carea)->first();
		$grupo_areas = Tarea::where('carea','=',$carea)->first();

		$personas = DB::table("tproyectopersona as pro")
			->join("tpersona as p", "p.cpersona", "=", "pro.cpersona")
			->join("tusuarios as u", "u.cpersona", "=", "pro.cpersona")
			->join("tpersonadatosempleado as tpe","tpe.cpersona","=","p.cpersona")
			->join("tcategoriaprofesional as tc","tc.ccategoriaprofesional","=","tpe.ccategoriaprofesional")
			->join('tareas as ta','ta.carea','=','tpe.carea')
			->select("pro.cproyecto", "pro.cpersona", "p.abreviatura as nombre", "u.nombre as usuario","tc.descripcion as categoria")
			->where('u.estado','=','ACT')
			// ->where("tpe.carea", "=", $carea)
			// ->whereIn('tpe.carea',[$carea,$carea_parent->carea_parent])
			->where('ta.grupo_areas','=',$grupo_areas->grupo_areas)
			->whereIn("pro.cproyecto", $arrayCodProyectos)
			->orderBy("tc.orden")
			->orderBy("p.abreviatura")
			->get();
		// dd($carea);
		return $personas;
	}

	private function _obtenerPersonas_toArray($personas, &$arrayPersonas, &$arrayCategorias, &$arrayCodPersonas)
	{

		foreach ($personas as $key => $persona) {
			if(!in_array($persona->nombre, $arrayPersonas)){
				array_push($arrayPersonas,$persona->nombre);
				array_push($arrayCategorias,$persona->categoria);
				array_push($arrayCodPersonas,$persona->cpersona);
			}
		}		
	}

	private function _obtenerPlanificacionDePersonas($periodo, $semana, $arrayCodProyectos)
	{
		$horasPlanificadas = DB::table("planificacion as p")
			->join("tusuarios as u", "u.nombre", "=", "p.usuario")
			->select("u.cpersona", "p.cproyecto", "p.planificadas", "p.planificadas_adm", "p.disponibles")
			->where("p.periodo", "=", $periodo)
			->where("p.semana", "=", $semana)
			->whereIn("p.cproyecto", $arrayCodProyectos)
			->get();

		return $horasPlanificadas;
	}

	private function _inicializarMatriz(&$matriz, $lideres, $cantidad)
	{
		foreach ($lideres as $key => $lider) {
			$fila = [$lider->persona_nombre, $lider->codigo, $lider->proyecto_descripcion];
			for ($i=0; $i < $cantidad; $i++)
				array_push($fila, "");
			
			array_push($matriz, $fila);
		}
	}

	private function _actualizarDataMatriz(&$matriz, $personas, $arrayPersonas, $arrayCodProyectos)
	{
		foreach ($personas as $key => $persona) {
			$posP = $this->_obtenerPosArrayPersonas($persona->nombre, $arrayPersonas);
			$posPro = $this->_obtenerPosArrayCodProyectos($persona->cproyecto, $arrayCodProyectos);

			if($posP != -1 && $posPro != -1)
			{
				if(isset($persona->planificadas))
					$matriz[$posPro][$posP+3] = $persona->planificadas;
				else
					$matriz[$posPro][$posP+3] = "0";
			}

		}
	}

	private function _obtenerPosArrayPersonas($nombre, $arrayPersonas)
	{
		$pos = array_search($nombre, $arrayPersonas);
		if($pos===false)
			return -1;

		return $pos;
	}

	private function _obtenerPosArrayCodProyectos($cproyecto, $arrayCodProyectos)
	{
		$pos = array_search($cproyecto, $arrayCodProyectos);
		if($pos===false)
			return -1;

		return $pos;
	}

	private function _actualizarHorasPlanificadas(&$personas, $horasPlanificadas)
	{
		foreach ($horasPlanificadas as $key => $horas) {
			foreach ($personas as $key => $persona) {
				if($horas->cproyecto == $persona->cproyecto && $horas->cpersona == $persona->cpersona)
					$persona->planificadas = $horas->planificadas;				
			}
		}
	}

	private function _obtenerRowspanLideres(&$matriz)
	{
		$arrayRowspanLideres = [];
		$lider_nombre_temp = "";
		for ($i=0; $i < count($matriz); $i++) { 			
			if($lider_nombre_temp != $matriz[$i][0]){				
				$lider_nombre_temp = $matriz[$i][0];
				$arrayRowspanLideres[$lider_nombre_temp] = 1;
			}
			else
				$arrayRowspanLideres[$lider_nombre_temp] += 1;
		}

		return $arrayRowspanLideres;
	}

	private function _obtenerTotales($matriz, &$totalProfesional, &$totalProyecto, $cantidadPersonas)
	{
		$posValores = count($matriz[0]);

		//inicializar totalProfesional
		for ($i=0; $i < $cantidadPersonas; $i++)
			array_push($totalProfesional, 0);
		//inicializar totalProyecto
		for ($i=0; $i < count($matriz); $i++)
			array_push($totalProyecto, 0);
		//sumar valores
		for ($i=0; $i < count($matriz) ; $i++) { 			
			for ($j=3; $j < $posValores; $j++) {
				$totalProyecto[$i] += floatval($matriz[$i][$j]);
				$totalProfesional[$j-3] += floatval($matriz[$i][$j]);
			}	
		}
	}

	private function _obtenerHorasDisponibles_toArray($arrayCodPersonas, $personas, &$horasDiponibles, $periodo, $semana)
	{
		$cantidadPersonas = count($arrayCodPersonas);
		//inicializar
		for ($i=0; $i < $cantidadPersonas; $i++)
			array_push($horasDiponibles, 0);
		//completar
		for ($i=0; $i < $cantidadPersonas; $i++) { 
			//obtener usuario
			$usuario = $this->_obtenerUsuarioDePersona($personas, $arrayCodPersonas[$i]);
			$horasDiponibles[$i] = Planificacion::obtenerDisponibilidadPorPeriodoSemanaProyecto($periodo, $semana, $usuario);
		}

	}

	private function _obtenerUsuarioDePersona($personas, $cpersona)
	{
		foreach ($personas as $key => $persona) {
			//echo "$cpersona <br>";
			if($persona->cpersona == $cpersona)
				return $persona->usuario;
		}
		return "";
	}

	private function _obtenerCategorias_toArrayContador($arrayCategorias)
	{
		$arrayCategoriasObjetos = [];
		$categoriaNombreTemp = "";		
		$posActual = -1;
		
		foreach ($arrayCategorias as $key => $categoriaNombre) {
			if($categoriaNombreTemp != $categoriaNombre){
				$arrayCategoria = [];
				$arrayCategoria["categoriaNombre"] = $categoriaNombre;
				$arrayCategoria["cantidadColumnas"] = 1;
				array_push($arrayCategoriasObjetos, $arrayCategoria);
				$categoriaNombreTemp = $categoriaNombre;
				$posActual++;
			}
			else
				$arrayCategoriasObjetos[$posActual]["cantidadColumnas"] += 1;
		}

		return $arrayCategoriasObjetos;
	}

}
