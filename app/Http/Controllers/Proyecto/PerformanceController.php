<?php

namespace App\Http\Controllers\Proyecto;

use App\Http\Controllers\Controller;
use App\Models\Tperformance;
use Auth;
use Carbon\Carbon;
use DB;
use Response;
use Illuminate\Http\Request;

class PerformanceController extends Controller
{
    //public
    public function listarprojectgeneral()
    {

        $tproyectos = $this->CapturarProyectosPorCargo();
        $lider_or_gerente = $this->lider_or_gerente();

        return view('proyecto.performance', compact('tproyectos','lider_or_gerente'));

    }

    public function CapturarProyectosPorCargo()
    {
        $user = Auth::user();

        $lider_or_gerente = $this->lider_or_gerente();
        //dd($lider_or_gerente);

        $tproyectos = DB::table('tproyecto as tp')
            ->leftjoin('tproyectopersona as tpyp', 'tpyp.cproyecto', '=', 'tp.cproyecto')
            ->leftjoin('tproyectodisciplina as tda', 'tp.cproyecto', '=', 'tda.cproyecto');

        if ($lider_or_gerente == 'lider') {

            $tproyectos = $tproyectos->where('tpyp.cpersona','=', $user->cpersona)
                ->where('tpyp.eslider', '=', '1');

        }
        if($lider_or_gerente == 'gerente')
        {
            $tproyectos = $tproyectos->where('tp.cpersona_gerente', '=', $user->cpersona);
        }
        if($lider_or_gerente == 'jefe') {

        }

        $tproyectos = $tproyectos->select('tp.cproyecto', 'tp.codigo', 'tp.nombre')
            ->distinct()
            ->where('tp.cestadoproyecto','=','001')
            ->orderBy('tp.codigo','ASC')
            ->get();
       // dd($tproyectos);

        return $tproyectos;
    }

    public function getAreas($cproyecto)
    {
            $user = Auth::user();

            $lider_or_gerente = $this->lider_or_gerente();

            $tareas = DB::table('tproyectopersona as td')
                ->leftjoin('tproyecto as tpry', 'tpry.cproyecto', '=', 'td.cproyecto')
                ->leftjoin('tdisciplina as tidsc', 'tidsc.cdisciplina', '=', 'td.cdisciplina')
                ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'tidsc.cdisciplina')
                ->leftjoin('tareas as ta', 'ta.carea', '=', 'tda.carea');

            if($lider_or_gerente == 'gerente'){

                 $tareas =  $tareas->where('td.cproyecto', '=', $cproyecto)
                ->select('td.cproyecto', 'td.cdisciplina as disciplina', 'tidsc.descripcion as area', 'ta.carea','tpry.finicio', 'tpry.fcierre')
                ->distinct()
                ->get();

            }
            elseif($lider_or_gerente == 'jefe' || $lider_or_gerente == 'lider')
            {
                   $tareas =  $tareas->where('td.cproyecto', '=', $cproyecto)
                                         ->where('td.eslider', '=',  1)
                                         ->where('td.cpersona', '=',  $user->cpersona)
                                        ->select('td.cproyecto', 'td.cdisciplina as disciplina', 'tidsc.descripcion as area', 'ta.carea','tpry.finicio','tpry.fcierre')->get();
            }

          //  dd($tareas);
       
        return $tareas;

    }

    public function getActividades(Request $request, $cproyecto, $disciplina)
    {
        $cproyecto  = $request->input('cproyecto');
        $disciplina = $request->input('disciplina');

        $cabecera_participante = $this->getCabezera($cproyecto, $disciplina);

        $arraycount_Cabezera = [];
        for ($w=4;$w<count($cabecera_participante);$w++)
        {
            array_push($arraycount_Cabezera,$w);

        }
        //dd($arraycount_Cabezera);
        //  dd($disciplina);
        $project_ingresado = DB::table('tproyecto as tp')
            ->where('tp.cproyecto', '=', $cproyecto)
            ->first();

        //dd($project_ingresado);
        $codproject = explode('-', $project_ingresado->codigo);

        $listaproyect = DB::table('tproyecto as tp')
            ->where('tp.codigo', 'ilike', $codproject[0] . '%')
            ->orderBy('tp.codigo', 'ASC')
            ->get();

        $fechaDesde = $request->input('fdesde');

        $fechaHasta = $request->input('fhasta');

        if (substr($fechaDesde, 2, 1) == '/' || substr($fechaDesde, 2, 1) == '-') {
            $fechaDesde = Carbon::create(substr($fechaDesde, 6, 4), substr($fechaDesde, 3, 2), substr($fechaDesde, 0, 2));
            $fechaHasta = Carbon::create(substr($fechaHasta, 6, 4), substr($fechaHasta, 3, 2), substr($fechaHasta, 0, 2));

        } else {
            $fechaDesde = Carbon::create(substr($fechaDesde, 0, 4), substr($fechaDesde, 5, 2), substr($fechaDesde, 8, 2));
            $fechaHasta = Carbon::create(substr($fechaHasta, 0, 4), substr($fechaHasta, 5, 2), substr($fechaHasta, 8, 2));
        }


        $fec_fin_full = $fechaHasta->toDateString();

       // dd($fechaDesde,$fechaHasta);
        $fecha_ini_project =  DB::table('tproyecto as tp')
              ->where('tp.cproyecto', '=', $cproyecto)
              ->select(DB::raw("extract(week from tp.finicio) as finicioweek"))
              ->first();

        $fecha_fin_project =  DB::table('tproyecto as tp')
        ->where('tp.cproyecto', '=', $cproyecto)
        ->select(DB::raw("extract(week from tp.fcierre) as fcierreweek"))
        ->first();

        $semana_ini_Proy = $fechaDesde->weekOfYear;
        $semana_fin_Proy = $fechaHasta->weekOfYear;

        $anio_inicial = $fechaDesde->format('Y');
        $anio_final   = $fechaHasta->format('Y');

        $cant_semana_anio_inicial = intval(date("W",strtotime('28th December '.$anio_inicial)));
        $cant_semana_anio_final = intval(date("W",strtotime('28th December '.$anio_final)));

        $semana_fin_anio_del_proyecto = $cant_semana_anio_inicial - $semana_ini_Proy + 1;

        if ($anio_inicial == $anio_final) {

            $semana_ini = $fechaDesde->weekOfYear -  intval($fecha_ini_project->finicioweek) + 1;
            $dif = intval($fecha_ini_project->finicioweek) - 1 ;
            $semana_fin =   $fechaHasta->weekOfYear - $dif;

            }
        else
            {
                $semana_ini = $fechaDesde->weekOfYear -  intval($fecha_ini_project->finicioweek) + 1;
                $dif = intval($fecha_ini_project->finicioweek) - 1 ;
                $semana_fin =   $cant_semana_anio_inicial - $dif + $fechaHasta->weekOfYear;

            }

         //dd($semana_ini,$semana_fin);
       //  dd($semana_ini_Proy,$semana_fin_Proy,$semana_ini,$semana_fin,$cant_semana_anio_inicial,$cant_semana_anio_final);


        //dd($semana_ini,$semana_fin);

        $anio_inicial = $fechaDesde->format('Y');
        $anio_final   = $fechaHasta->format('Y');

        $anio_inicial = intval($anio_inicial);
        $anio_final   = intval($anio_final);

        $fechaDesde1 = $fechaDesde->toDateString();
        $fechaHasta2 = $fechaHasta->toDateString();



        $num_sema = [];

        if ($anio_final == $anio_inicial) {

            for ($i = $semana_ini; $i <= $semana_fin; $i++) {

                //array_push($num_sema, $i);

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }

        } elseif ($anio_final > $anio_inicial) {

            $num_sema1 = [];
            $num_sema2 = [];
            if ($semana_fin > $semana_ini )
            {
                for ($i = $semana_ini; $i <= $semana_fin; $i++) {

                    //array_push($num_sema, $i);

                    array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
                }

            }
            else{
                //dd($fechaDesde,$fechaHasta,$semana_ini,$semana_fin,$anio_inicial,$anio_final,'OK');
                for ($i = $semana_ini; $i <= $semana_fin_anio_del_proyecto; $i++) {

                    array_push($num_sema1, ['semana' => $i, 'anio' => $anio_inicial]);
                }

                // dd($num_sema);

                for ($i = 1; $i <= $semana_fin; $i++) {

                    array_push($num_sema2, ['semana' => $i, 'anio' => $anio_final]);
                }

                $num_sema = array_merge($num_sema1,$num_sema2);
            }



        }




        // dd($sem_date);

        function devuelveArrayFechasEntreOtrasDos($fechaInicio, $fechaFin,$num_semana)
        {
            $arrayFechas=array();
            $fechaMostrar = $fechaInicio;

            for ($i=0; $i < $num_semana ; $i++) {
                $arrayFechas[]=$fechaMostrar;
                // $fechaMostrar = date("d-m-Y", strtotime($fechaMostrar . " + 1 week"));
                $fechaMostrar = date("Y-m-d", strtotime($fechaMostrar . " + 1 week"));
              }

            /*while(strtotime($fechaMostrar) <= strtotime($fechaFin)) {
                $arrayFechas[]=$fechaMostrar;
                $fechaMostrar = date("Y-m-d", strtotime($fechaMostrar . " + 1 week"));

            }*/
         //   dd($arrayFechas);
            return $arrayFechas;
        }

        $arrayFechas=devuelveArrayFechasEntreOtrasDos($fechaDesde1,$fechaHasta2,count($num_sema));
        // dd($arrayFechas);

        $abf = [];
        foreach ($arrayFechas as $aF)
        {
            $fechaMostrarstart =  Carbon::parse($aF)->startOfWeek()->format("d-m-Y");
            $fechaMostrarend =  Carbon::parse($aF)->endOfWeek()->format("d-m-Y");
            $conv = $fechaMostrarstart .'   '. $fechaMostrarend;
            array_push($abf,$conv);

        }

       // dd($arrayFechas,$abf,$num_sema);
        //dd($num_sema,$abf);
         $array_FinalCabecera = [];
         foreach ($num_sema as $key => $value)
         {
             $col['semana'] = $value['semana'];
             $col['fecha_inicio_final'] = $abf[$key];
             array_push($array_FinalCabecera,$col);
         }



        $arrayProyectos=$this->getActividadescurva($cproyecto, $disciplina,$fechaDesde1,$fechaHasta2);
       $rateshorasejecutadascabezera = $this->horasxrateplanificadastotalescabecera($cproyecto,$disciplina);
       //dd($rateshorasejecutadascabezera);

        $user = Auth::user();

             // Si es gerente o no
        $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user->cpersona)
            ->get();

         $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.cpersona', '=', $user->cpersona)
            ->where('tpyp.eslider','=','1')
            ->get();



            $lider_or_gerente = '';

            if($esgerente)
            {
                 $lider_or_gerente = 'gerente';
            }
            elseif($eslider)
            {
                  $lider_or_gerente = 'lider';
            }


       //dd($num_sema);


      //  return view('proyecto.performance.listaactivity', compact('arrayProyectos', 'cabecera_participante', 'num_sema','rateshorasejecutadascabezera','lider_or_gerente'));
        return view('proyecto.performance.listaactividadesPerformance', compact('arrayProyectos', 'cabecera_participante', 'array_FinalCabecera','rateshorasejecutadascabezera','lider_or_gerente','arraycount_Cabezera'));
        //return $arrayProyectos ;
    }


    public function getArrayPerformanceCurvas(Request $request){

       // dd($request->all());

        $cproyecto=$request->cproyecto;
        $disciplina=$request->disciplina;
        $fechadesde=$request->fdesde;
        $fechahasta=$request->fhasta;


        if (substr($fechadesde, 2, 1) == '/' || substr($fechadesde, 2, 1) == '-') {
            $fechadesde = Carbon::create(substr($fechadesde, 6, 4), substr($fechadesde, 3, 2), substr($fechadesde, 0, 2));
            $fechahasta = Carbon::create(substr($fechahasta, 6, 4), substr($fechahasta, 3, 2), substr($fechahasta, 0, 2));

        } else {
            $fechadesde = Carbon::create(substr($fechadesde, 0, 4), substr($fechadesde, 5, 2), substr($fechadesde, 8, 2));
            $fechahasta = Carbon::create(substr($fechahasta, 0, 4), substr($fechahasta, 5, 2), substr($fechahasta, 8, 2));
        }


        $semana_ini = $fechadesde->weekOfYear;
        $semana_fin =   $fechahasta->weekOfYear;


        $anio_inicial_string = $fechadesde->format('Y');
        $anio_final_string   = $fechahasta->format('Y');

        $anio_inicial = intval($anio_inicial_string);
        $anio_final   = intval($anio_final_string);



        $num_sema = [];
        if ($anio_final == $anio_inicial) {

            for ($i = $semana_ini; $i <= $semana_fin; $i++) {

                //array_push($num_sema, $i);

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }

        } else if ($anio_final > $anio_inicial) {
            //dd($fechaDesde,$fechaHasta,$semana_ini,$semana_fin,$anio_inicial,$anio_final,'OK');
            for ($i = $semana_ini; $i <= 52; $i++) {

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_inicial]);
            }

            // dd($num_sema);

            for ($i = 1; $i <= $semana_fin; $i++) {

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }

        } else {

        }
      //  dd($num_sema);


        // dd($cproyecto,$fechadesde,$fechahasta,$disciplina);

       // dd($fec_ini_full,$fec_fin_full);


                  /*====================================
                        =            Filtro hijos            =
                        ====================================*/

                        // $sumatoria_rate_hora_detalle_parent = $this->suma_rate_activities_with_son('51658', 'Nuevo', '02');

                        $array_porcentaje_padre = [];
                       

                        //$porcentaje_parent_8 = $this->visualizar_porcentaje_for_semana_parent('51658','39', '2018');
                        // dd($porcentaje_parent_8);

                        //dd($actividades);
                    foreach ($num_sema as $ns) {
                           
                        $user = Auth::user();

                          $activity_parent_proyecto = $this->actividadespadreproyecto($cproyecto);


                          foreach ($activity_parent_proyecto as  $actpp) {

                            $porcentaje_parent_8 = $this->versolohijosdepadres($actpp->cproyectoactividades_parent);


                                $datosum  = 0;
                                $datosum2 = 0;

                                if ($porcentaje_parent_8) {
                                   // dd('es padre');
                                        $arraysumadetalleactivity_performance = [];
                                        foreach ($porcentaje_parent_8 as $pp8) {


                                            $suma_rate_detalle_activity = 0;
                                            $suma_por_activity          = 0;


                                                $sumatoria_rate_hora_detalle = $this->obtenerhorasrateperformance($pp8->cproyectoactividades, $disciplina);
                                               //dd($sumatoria_rate_hora_detalle);

                                                if ($sumatoria_rate_hora_detalle) {

                                                    foreach ($sumatoria_rate_hora_detalle as $sr) {
                                                        //dd($sr);
                                                        $suma_actividad = $sr->horas * $sr->rate;

                                                        $suma_rate_detalle_activity = $suma_rate_detalle_activity + $suma_actividad;

                                                        //$suma_por_activity =  $suma_por_activity + $suma_rate_detalle_activity;

                                                    }

                                                   $p['subtotal']                  = $suma_rate_detalle_activity;
                                                   $datosum += $p['subtotal'];
                                                }

                                               
                                            

                                          // dd($datosum);
                                           


                                            $porcentaje_parent_perfomance = $this->visualizar_porcentaje_for_semana_parent($pp8->cproyectoactividades,$ns['semana'], $ns['anio'], $user->cpersona,$disciplina);
                                            // $horasdepadreactivity =  $this->curvaperformanceavance($actpp->cproyectoactividades_parent ,$disciplina);
                                             //dd($horasdepadreactivity);
                                            //$porcentaje_parent_perfomance = $this->visualizar_porcentaje_for_semana_parent('51667','47','2018','3892');
                                           // $porcentaje_parent_perfomance = $this->visualizar_porcentaje_for_semana_parent($pp8->cproyectoactividades,$ns['semana'], $ns['anio'], $user->cpersona);
                                            //dd($porcentaje_parent_perfomance);

                                            if($porcentaje_parent_perfomance){
                                                foreach($porcentaje_parent_perfomance as $ppp)
                                                {
                                                    $p['multi_subtotal_porcentaje'] = $suma_rate_detalle_activity * ($ppp->porcentaje);
                                                }

                                                $datosum2 += $p['multi_subtotal_porcentaje'];
                                            }

                                                else{
                                            }


                                            $horaspadre = 0;

                                            // capturar horas presupuestadas de cada actividad padre

                                            $horasdepadreactivity =  $this->curvaperformanceavance($actpp->cproyectoactividades_parent ,$disciplina);
                                            $horaspadre =  $horasdepadreactivity;
                                            $horaspadre_integer =  floatval(number_format($horaspadre, 2));

                                             // capturar rate presupuestadas de cada actividad padre
                                            $ratepadrepresupuestados = 0;

                                            $ratepadreactivitypadre = $this->sumarateparentpresupuestadas($actpp->cproyectoactividades_parent ,$disciplina);
                                            if($ratepadreactivitypadre)
                                            {
                                                foreach ($ratepadreactivitypadre as $ratepadre) {

                                                     $multiplicacion_h_r = $ratepadre->horas * $ratepadre->rate;
                                                     $ratepadrepresupuestados += $multiplicacion_h_r;
                                                }
                                              //  $ratepadrepresupuestados_integer = floatval(number_format($ratepadrepresupuestados, 2));
                                            }


                                             


                                        }
                                           

                                        array_push($array_porcentaje_padre, [
                                            //'porcentaje'  => $porcentaje_parent_8['porcentaje'],
                                            'semana'                 => $ns['semana'],
                                            'performance'            =>  $datosum2 == 0 ? 0  : floatval(number_format(($datosum2 / $datosum), 2)),
                                            'hora'                   =>  $horaspadre_integer,
                                            'rate'                   =>  $ratepadrepresupuestados,
                                            'performance_hora'       =>  $datosum2 == 0 ? 0  : floatval(number_format(($datosum2 / $datosum)/100 * $horaspadre_integer, 2)),
                                            'performance_hora_rate'  =>  $datosum2 == 0 ? 0  : floatval(($datosum2 / $datosum)/100 * $ratepadrepresupuestados),   

                                            ]);
                                        }
                                else
                                {
                                    array_push($array_porcentaje_padre, [
                                            //'porcentaje'  => $porcentaje_parent_8['porcentaje'],
                                           
                                            'semana'                   => $ns['semana'],
                                           /* 'cactividad'               => '',
                                            'pk_actividadpadre'        =>$pp8->cproyectoactividades,
                                            'anio'                     => $ns['anio'],
                                            'suma_por_activity'        => 0,
                                            'subtotal_porcentaje_rate' => 0,*/
                                            'performance'              => 0,
                                            'hora'                     => 0,
                                            'rate'        => 0,
                                            'performance_hora_rate' => 0,
                                            ]);
                                }

                              }
                            } // num_semana

        $avanceperformance = $this->avanceperformance($semana_ini,$array_porcentaje_padre);
        //dd($avanceperformance);



        // $avanceperformancearray =  (array)$avanceperformance;
        $resumenProyectosPorcentaje= $this->getActividadescurva($cproyecto, $disciplina,$fechadesde,$fechahasta);
        $resumenProFinal = $resumenProyectosPorcentaje[0]['resumenTotal'];
        //dd($resumenProFinal);


        $conver = [];
        $contador1 = 0;
        $contadorate = 0;
        $indice = 0;
        foreach ($avanceperformance as $cv => $value) {
            $ol['semana'] = $value['semana'];
            $ol['performance_hora'] = $value['performance_hora'];
            $ol['performance_hora_rate'] = $value['performance_hora_rate'];

            //Horas

            $arrayProyectos=$this->obtenercurvas_horas($cproyecto,$fechadesde,$fechahasta, $disciplina, $value['semana']);
            $ol['suma_horas_ejecutadas'] =  $arrayProyectos ?  floatval($arrayProyectos->sum)  : 0 ;
            $contador1 +=  $arrayProyectos ? floatval($arrayProyectos->sum)  : 0 ;
            $ol['acumulado'] =   $contador1;

            //Rate
            $arrayProyectosrate=$this->obtenercurvas_horas_rates($cproyecto,$fechadesde,$fechahasta, $disciplina, $value['semana']);
            $sum = 0;
            foreach ($arrayProyectosrate as $apr) {
                $ratehora = $apr->rate * $apr->horasejecutadas;
                $sum += $ratehora;
            }

            $ol['rate'] =  $arrayProyectosrate ?  floatval($sum)  : 0 ;
            $contadorate +=  $arrayProyectosrate ? floatval($sum)  : 0 ;
            $ol['acumulativo'] =   $contadorate;

            $ol['cpi_hora'] =   $contador1 == 0 ? 0 : floatval(number_format(($value['performance_hora']/ $contador1),2)) ;
            $ol['cpi_hora_rate'] =  $contadorate == 0 ? 0 : floatval(number_format($value['performance_hora_rate']/ $contadorate,2));
            $ol['resumenPorcentajeFinal'] = $resumenProFinal[$indice]['resumenFinal'];


            array_push( $conver,$ol);

            $indice++;
        }


       // dd($conver);
       // dd($conver.$resumenProFinal);

        return $conver;

      //  return $array_porcentaje_padre;

        // dd($array_porcentaje_padre);

         // return view('proyecto.performance.curva-s')->with('arraygethorascurva', $arraygethorascurva);


    }

    public function avanceperformance($semana_ini,$array_porcentaje_padre)
    {
              //dd($array_porcentaje_padre,$num_sema);
                        $arrary_semana = [];
                        $key =  0;
                       // foreach ($num_sema as $ns) {
                        
                             foreach ($array_porcentaje_padre as $aporcen ) {

                                        $key =  $aporcen['semana'];
                                            if (!array_key_exists($key, $arrary_semana))
                                           {
                                            $arrary_semana[$key] = array(
                                                'semana' => $aporcen['semana'],
                                                'performance_hora' => $aporcen['performance_hora'],
                                                'performance_hora_rate' => $aporcen['performance_hora_rate'],
                                            );
                                            } else {
                                                //$arrary_semana[$key]['performance_hora'] = $arrary_semana[$key]['performance_hora'] + $aporcen['performance_hora'];
                                               $arrary_semana[$key]['performance_hora'] = $arrary_semana[$key]['performance_hora'] + $aporcen['performance_hora'];
                                                $arrary_semana[$key]['performance_hora_rate'] = $arrary_semana[$key]['performance_hora_rate'] + $aporcen['performance_hora_rate'];
                                                //$arrary_semana[$key]['itemMaxPoint'] = $arrary_semana[$key]['itemMaxPoint'] +$item['itemMaxPoint'];
                                            }
                                            $key++;
                                            
                                        }

                                 return   $arrary_semana ;
                          //  }
    }





     public function getActividadescurva($cproyecto, $disciplina,$fechaDesde,$fechaHasta)
    {


        $cproyecto  = $cproyecto;
        $disciplina = $disciplina;

        $fechaDesde =  $fechaDesde;
        $fechaHasta = $fechaHasta;
        //dd($fechaHasta->date);



        $arrayProyectos = [];

        $proyUnico = $this->identificarProyecto($cproyecto);


        $proy['cproyecto'] = $proyUnico->cproyecto;
        $proy['codigo']    = $proyUnico->codigo;
        $proy['nombre']    = $proyUnico->nombre;

        $hijosdirectos = $this->listadeActividadesHijosDirectosporProyecto($cproyecto);
       // dd($hijosdirectos);

        $actividadesArray    = [];

        $sumahorasPresupuestadasNivelProyecto  = 0;
        $sumaMontoPresupuestadasNivelProyecto  = 0;
        $sumahorasConsumidosNivelProyecto  = 0;
        $sumaMontoConsumidosNivelProyecto  = 0;

        $sumahoras_rate_hora = 0;

        $result_multiplicacion_monto_porcentjae_Hijos =[];
        $result_multiplicacion_monto_porcentjae_Nietos =[];
        $result_multiplicacion_monto_porcentjae_Bisnietos =[];
      //  $result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos =[];
        foreach ($hijosdirectos as $hd)
        {
             $result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos =[];

            $actPadre['codigoactividad'] = $hd->codigoactvidad;
            $actPadre['nombreAct'] = $hd->descripcionactividad;
            $actPadre['cproyectoactividadesPadre'] = $hd->cproyectoactividades;
            $listadeactividadesHijos = $this->actividad_padre_or_hijo($hd->cproyectoactividades);

            $actividadesHijoArray    = [];

            $acumulativohoraspresupuestadasHijos = 0;
            $acumulativohoraspresupuestadasNietos = 0;
            $acumulativohoraspresupuestadasBisnieto = 0;

            $acumulativomontopresupuestadasHijos = 0;
            $acumulativomontopresupuestadasNietos = 0;
            $acumulativomontopresupuestadasBisnieto = 0;

            $acumulativohorasConsumidasHijos = 0;
            $acumulativohorasConsumidasNietos = 0;
            $acumulativohorasConsumidasBisnieto = 0;

            $acumulativomontoConsumidasHijos = 0;
            $acumulativomontoConsumidasNietos = 0;
            $acumulativomontoConsumidasBisnieto = 0;

            foreach ($listadeactividadesHijos as $hdH)
            {
               // $result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos =[];
                $actHijo['codigoactividad'] = $hdH->codigoactvidad;
                $actHijo['nombreAct'] = $hdH->descripcionactividad;
                $actHijo['cproyectoactividades'] = $hdH->cproyectoactividades;
                $actHijo['cproyecto'] = $cproyecto;
                $actHijo['cproyectoactividades_parent'] = $hdH->cproyectoactividades_parent;
                $actHijo['horapresupuestadasperformance'] = $this->obtenerhoraspresuuestadasPerformance($hdH->cproyectoactividades,$disciplina);
                $actHijo['monto_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdH->cproyectoactividades,$disciplina,$cproyecto)['sumtotalPresupuestadas'];
                $actHijo['array_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdH->cproyectoactividades,$disciplina,$cproyecto)['estructuraParticipantes'];
                $actHijo['horaejecutadosperformance'] = $this->horas_ejecutadas($fechaHasta,$hdH->cproyectoactividades, $disciplina, $cproyecto);
                $actHijo['monto_Consumidos'] = $this->obtenerateconsumidas_condetalle($fechaHasta,$hdH->cproyectoactividades,$disciplina,$cproyecto)['sumtotalConsumidos'];
                $actHijo['array_Consumidos'] = $this->obtenerateconsumidas_condetalle($fechaHasta,$hdH->cproyectoactividades,$disciplina,$cproyecto)['arrayConsumidosCategoria'];
                $actHijo['array_performance'] = $this->numerosemana($fechaDesde,$fechaHasta,$hdH->cproyectoactividades,$disciplina, $actHijo['monto_Presupuestados'],$hdH->cproyectoactividades_parent);


                foreach($actHijo['array_performance'] as $t) {
                    $repeat=false;
                    for($x=0;$x<count($result_multiplicacion_monto_porcentjae_Hijos);$x++)
                    {
                        if($result_multiplicacion_monto_porcentjae_Hijos[$x]['semana']== $t['semana'] && $result_multiplicacion_monto_porcentjae_Hijos[$x]['anio']== $t['anio'] && $result_multiplicacion_monto_porcentjae_Hijos[$x]['pk_actividadPadre']== $t['pk_actividadPadre'])
                        {
                            $result_multiplicacion_monto_porcentjae_Hijos[$x]['porcentaje_por_montoPre']+=$t['porcentaje_por_montoPre'];
                            $repeat=true;
                            break;
                        }
                    }
                    if($repeat==false)
                        $result_multiplicacion_monto_porcentjae_Hijos[] = array(    'anio'                      => $t['anio'],
                                                                                    'semana'                    => $t['semana'],
                                                                                    'pk_actividadPadre'         => $t['pk_actividadPadre'],
                                                                                    'porcentaje_por_montoPre'   => $t['porcentaje_por_montoPre']
                                                                                );
                }
                //dd($result_multiplicacion_monto_porcentjae_Hijos);



                $acumulativohoraspresupuestadasHijos += $actHijo['horapresupuestadasperformance'];
                $acumulativomontopresupuestadasHijos +=  $actHijo['monto_Presupuestados'];
                $acumulativohorasConsumidasHijos +=  $actHijo['horaejecutadosperformance'];
                $acumulativomontoConsumidasHijos +=  $actHijo['monto_Consumidos'];

                $listadeactividadesNietos = $this->actividad_padre_or_hijo($hdH->cproyectoactividades);


                $actividadesNietosArray    = [];
                foreach ($listadeactividadesNietos as $hdN)
                {
                    $actNieto['codigoactividad'] = $hdN->codigoactvidad;
                    $actNieto['nombreAct'] = $hdN->descripcionactividad;
                    $actNieto['cproyectoactividades'] = $hdN->cproyectoactividades;
                    $actNieto['cproyecto'] = $cproyecto;
                    $actNieto['cproyectoactividades_parent'] = $hdH->cproyectoactividades_parent;
                    $actNieto['horapresupuestadasperformance'] = $this->obtenerhoraspresuuestadasPerformance($hdN->cproyectoactividades,$disciplina);
                    $actNieto['monto_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdN->cproyectoactividades,$disciplina,$cproyecto)['sumtotalPresupuestadas'];
                    $actNieto['array_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdN->cproyectoactividades,$disciplina,$cproyecto)['estructuraParticipantes'];
                    $actNieto['horaejecutadosperformance'] = $this->horas_ejecutadas($fechaHasta,$hdN->cproyectoactividades, $disciplina, $cproyecto);
                    $actNieto['monto_Consumidos'] = $this->obtenerateconsumidas_condetalle($fechaHasta,$hdN->cproyectoactividades,$disciplina,$cproyecto)['sumtotalConsumidos'];
                    $actNieto['array_Consumidos'] = $this->obtenerateconsumidas_condetalle($fechaHasta,$hdN->cproyectoactividades,$disciplina,$cproyecto)['arrayConsumidosCategoria'];
                    $actNieto['array_performance'] = $this->numerosemana($fechaDesde,$fechaHasta,$hdN->cproyectoactividades,$disciplina, $actNieto['monto_Presupuestados'],$hdH->cproyectoactividades_parent);

                    foreach($actNieto['array_performance'] as $n) {
                        $repeat=false;
                        for($i=0;$i<count($result_multiplicacion_monto_porcentjae_Nietos);$i++)
                        {
                            if($result_multiplicacion_monto_porcentjae_Nietos[$i]['semana']== $n['semana'] && $result_multiplicacion_monto_porcentjae_Nietos[$i]['anio']== $n['anio'] &&  $result_multiplicacion_monto_porcentjae_Nietos[$i]['pk_actividadPadre']== $n['pk_actividadPadre'])
                            {
                                $result_multiplicacion_monto_porcentjae_Nietos[$i]['porcentaje_por_montoPre']+=$n['porcentaje_por_montoPre'];
                                $repeat=true;
                                break;
                            }
                        }
                        if($repeat==false)
                            $result_multiplicacion_monto_porcentjae_Nietos[] = array( 'anio'                    => $n['anio'],
                                                                                      'semana'                  => $n['semana'],
                                                                                      'pk_actividadPadre'       => $n['pk_actividadPadre'],
                                                                                      'porcentaje_por_montoPre' => $n['porcentaje_por_montoPre']);
                    }


                    $listadeactividadesBisnietos = $this->actividad_padre_or_hijo($hdN->cproyectoactividades);

                    $acumulativohoraspresupuestadasNietos += $actNieto['horapresupuestadasperformance'];
                    $acumulativomontopresupuestadasNietos +=  $actNieto['monto_Presupuestados'];
                    $acumulativohorasConsumidasNietos += $actNieto['horaejecutadosperformance'];
                    $acumulativomontoConsumidasNietos +=  $actNieto['monto_Consumidos'];


                    $actividadesBisnietosArray    = [];
                    foreach ($listadeactividadesBisnietos as $hdB)
                    {
                        $actBisnieto['codigoactividad'] = $hdB->codigoactvidad;
                        $actBisnieto['nombreAct'] = $hdB->descripcionactividad;
                        $actBisnieto['cproyectoactividades'] = $hdB->cproyectoactividades;
                        $actBisnieto['cproyecto'] = $cproyecto;
                        $actBisnieto['cproyectoactividades_parent'] = $hdH->cproyectoactividades_parent;
                        $actBisnieto['horapresupuestadasperformance'] = $this->obtenerhoraspresuuestadasPerformance($hdB->cproyectoactividades,$disciplina);
                        $actBisnieto['monto_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdB->cproyectoactividades,$disciplina,$cproyecto)['sumtotalPresupuestadas'];
                        $actBisnieto['array_Presupuestados'] = $this->obteneratepresuestadas_condetalle($hdB->cproyectoactividades,$disciplina,$cproyecto)['estructuraParticipantes'];
                        $actBisnieto['horaejecutadosperformance'] = $this->horas_ejecutadas($fechaHasta,$hdB->cproyectoactividades, $disciplina, $cproyecto);
                        $actBisnieto['monto_Consumidos'] = $this->obtenerateconsumidas_condetalle($fechaHasta,$hdB->cproyectoactividades,$disciplina,$cproyecto)['sumtotalConsumidos'];
                        $actBisnieto['array_Consumidos'] = $this->obtenerateconsumidas_condetalle($fechaHasta,$hdB->cproyectoactividades,$disciplina,$cproyecto)['arrayConsumidosCategoria'];
                        $actBisnieto['array_performance'] = $this->numerosemana($fechaDesde,$fechaHasta,$hdB->cproyectoactividades,$disciplina,$actBisnieto['monto_Presupuestados'],$hdH->cproyectoactividades_parent);

                        foreach($actBisnieto['array_performance'] as $b) {
                            $repeat=false;
                            for($y=0;$y<count($result_multiplicacion_monto_porcentjae_Bisnietos);$y++)
                            {
                                if($result_multiplicacion_monto_porcentjae_Bisnietos[$y]['semana']== $b['semana'] && $result_multiplicacion_monto_porcentjae_Bisnietos[$y]['anio']== $b['anio'] && $result_multiplicacion_monto_porcentjae_Bisnietos[$y]['pk_actividadPadre']== $b['pk_actividadPadre'] )
                                {
                                    $result_multiplicacion_monto_porcentjae_Bisnietos[$y]['porcentaje_por_montoPre']+=$b['porcentaje_por_montoPre'];
                                    $repeat=true;
                                    break;
                                }
                            }
                            if($repeat==false)
                                $result_multiplicacion_monto_porcentjae_Bisnietos[] = array( 'anio'   => $b['anio'],
                                                                                             'semana' => $b['semana'],
                                                                                            'pk_actividadPadre'       => $b['pk_actividadPadre'],
                                                                                            'porcentaje_por_montoPre' => $b['porcentaje_por_montoPre']);
                        }

                        $acumulativohoraspresupuestadasBisnieto += $actBisnieto['horapresupuestadasperformance'];
                        $acumulativomontopresupuestadasBisnieto +=  $actBisnieto['monto_Presupuestados'];
                        $acumulativohorasConsumidasBisnieto += $actBisnieto['horaejecutadosperformance'];
                        $acumulativomontoConsumidasBisnieto +=  $actBisnieto['monto_Consumidos'];


                        array_push($actividadesBisnietosArray,$actBisnieto);
                    }

                    $actNieto['actBisnieto'] = $actividadesBisnietosArray;
                    array_push($actividadesNietosArray,$actNieto);
                }

                $actHijo['actNieto'] = $actividadesNietosArray;
                array_push($actividadesHijoArray,$actHijo);
            }

            //dd($result_multiplicacion_monto_porcentjae_Hijos);

            $actPadre['sumtotalhoraspresupuestadasPadre'] =$actividadesHijoArray;
            $actPadre['actHijo'] =$actividadesHijoArray;
            $actPadre['Suma__totalHorasPresupuestadas'] =$acumulativohoraspresupuestadasHijos + $acumulativohoraspresupuestadasNietos + $acumulativohoraspresupuestadasBisnieto;
            $sumahorasPresupuestadasNivelProyecto +=  $actPadre['Suma__totalHorasPresupuestadas'];
            $actPadre['Suma__totalMontoPresupuestadas'] =$acumulativomontopresupuestadasHijos + $acumulativomontopresupuestadasNietos + $acumulativomontopresupuestadasBisnieto;
            $sumaMontoPresupuestadasNivelProyecto += $actPadre['Suma__totalMontoPresupuestadas'];
            $actPadre['Suma__totalHorasCargadas'] =$acumulativohorasConsumidasHijos + $acumulativohorasConsumidasNietos + $acumulativohorasConsumidasBisnieto;
            $sumahorasConsumidosNivelProyecto  +=  $actPadre['Suma__totalHorasCargadas'];
            $actPadre['Suma__totalHoraConsumidasCargadas'] = $acumulativomontoConsumidasHijos + $acumulativomontoConsumidasNietos + $acumulativomontoConsumidasBisnieto ;
            $sumaMontoConsumidosNivelProyecto  +=  $actPadre['Suma__totalHoraConsumidasCargadas'];

            $arrraymerge_hijos_nietos_bisnietos = array_merge($result_multiplicacion_monto_porcentjae_Hijos,$result_multiplicacion_monto_porcentjae_Nietos,$result_multiplicacion_monto_porcentjae_Bisnietos);

           // dd($arrraymerge_hijos_nietos_bisnietos,$result_multiplicacion_monto_porcentjae_Hijos,$result_multiplicacion_monto_porcentjae_Nietos,$result_multiplicacion_monto_porcentjae_Bisnietos);
            $result_multiplicacion_monto_porcentjae_Bisnietos_subtotal = [];
            foreach($arrraymerge_hijos_nietos_bisnietos as $ahnb) {
                $repeat=false;
                for($yz=0;$yz<count($result_multiplicacion_monto_porcentjae_Bisnietos_subtotal);$yz++)
                {
                    if($result_multiplicacion_monto_porcentjae_Bisnietos_subtotal[$yz]['semana']== $ahnb['semana'] && $result_multiplicacion_monto_porcentjae_Bisnietos_subtotal[$yz]['anio']== $ahnb['anio'] && $result_multiplicacion_monto_porcentjae_Bisnietos_subtotal[$yz]['pk_actividadPadre']== $ahnb['pk_actividadPadre'] )
                    {
                        $result_multiplicacion_monto_porcentjae_Bisnietos_subtotal[$yz]['porcentaje_por_montoPre']+=$ahnb['porcentaje_por_montoPre'];
                        $repeat=true;
                        break;
                    }
                }
                if($repeat==false)
                    $result_multiplicacion_monto_porcentjae_Bisnietos_subtotal[] = array( 'anio'   => $ahnb['anio'],
                        'semana' => $ahnb['semana'],
                        'pk_actividadPadre'       => $ahnb['pk_actividadPadre'],
                        'porcentaje_por_montoPre' => $ahnb['porcentaje_por_montoPre']);
            }
           // dd($result_multiplicacion_monto_porcentjae_Bisnietos_subtotal);

            $result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos =[];

            foreach($result_multiplicacion_monto_porcentjae_Bisnietos_subtotal as $w) {
                   // dd($hd->cproyectoactividades);

                   //if ($w['pk_actividadPadre'] ==  $hd->cproyectoactividades ) {
                   if ($w['pk_actividadPadre'] ==  $hd->cproyectoactividades ) {
                    	 $pkpadreperformance['pkpadre'] = $hd->cproyectoactividades;
                    	 $pkpadreperformance['semana'] = $w['semana'];
                    	 $pkpadreperformance['anio'] = $w['anio'];
                    	 $pkpadreperformance['porcentaje_por_montoPre'] = $actPadre['Suma__totalMontoPresupuestadas']== 0 ? 0:round(($w['porcentaje_por_montoPre'] /$actPadre['Suma__totalMontoPresupuestadas']),2) ;
                       array_push($result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos,$pkpadreperformance);
                   }

            }
            //dd($result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos);


            $actPadre['porcentaje_por_montoPre__semana'] = $result_multiplicacion_monto_porcentjae_Hijos_Nietos_Bisnietos;
           // $actPadre['arrraymerge_hijos_nietos_bisnietos'] = $arrraymerge_hijos_nietos_bisnietos;

            array_push($actividadesArray,$actPadre);
        }
        //dd($actividadesArray);




        $proy['sumahorasPresupuestadasNivelProyecto']    = $sumahorasPresupuestadasNivelProyecto;
        $proy['sumaMontoPresupuestadasNivelProyecto']    = $sumaMontoPresupuestadasNivelProyecto;
        $proy['sumahorasConsumidosNivelProyecto']        = $sumahorasConsumidosNivelProyecto;
        $proy['sumaMontoConsumidosNivelProyecto']        = $sumaMontoConsumidosNivelProyecto;


        $proy['sum_act_total']                           = $sumahoras_rate_hora;
        $proy['actPadre']                                = $actividadesArray;

        $arr2 = [];
        foreach ($actividadesArray as $posicion => $valor) {
            $arr=[];
            $montoPadre = $valor['Suma__totalMontoPresupuestadas'];
            $sub['nombreAct']  = $valor['nombreAct'];
            foreach ($valor['porcentaje_por_montoPre__semana'] as $key => $val) {
                $sub['sub'] = $val['porcentaje_por_montoPre'] * $montoPadre;
                $sub['semana'] = $val['semana'];
                $sub['anio'] = $val['anio'];

                array_push($arr,$sub);
            }
            array_push($arr2,$arr);

        }
        $semana_anio=$actividadesArray[0]['porcentaje_por_montoPre__semana'];
       // dd($semana_anio,$arr2);



        $sumatotal=[];

        foreach ($semana_anio as $sa){

            $acumulado=0;

            for($a=0;$a<count($arr2);$a++) {

                foreach ($arr2[$a] as $abd){
                    //dd($abd);

                    if($sa['semana']==$abd['semana'] && $sa['anio']==$abd['anio']) {

                        $acumulado    +=$abd['sub'];
                        $resumenFinal = ($sumaMontoPresupuestadasNivelProyecto == 0 ? 0 :$acumulado / $sumaMontoPresupuestadasNivelProyecto);

                    }
                }
            }

            array_push($sumatotal,[
                'resumenFinal' => round($resumenFinal,2),
                'semana' => $sa['semana'],
                'anio' =>  $sa['anio'],
            ]);

        }
       // dd($sumatotal);
        $proy['resumenTotal']                            = $sumatotal;
        $arrayProyectos[count($arrayProyectos)]          = $proy;

       // dd($arrayProyectos);
        return $arrayProyectos;

    }


    public function getCabezera($cproyecto, $cdisciplina)
    {
        /*----------  Lista de cabezera  ----------*/

        $project_ingresado = DB::table('tproyecto as tp')
            ->leftjoin('tproyectoestructuraparticipantes as tpe', 'tp.cproyecto', '=', 'tpe.cproyecto')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cestructuraproyecto', '=', 'tpe.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpe.croldespliegue')
            ->select('tpe.croldespliegue', 'trd.descripcionrol', 'trd.orden');

        if ($cdisciplina == 'Nuevo') {

            $project_ingresado = $project_ingresado->where('tpe.cproyecto', '=', $cproyecto);
        } else {

            $project_ingresado = $project_ingresado->where('tpe.cproyecto', '=', $cproyecto)
                ->where('tph.cdisciplina', '=', $cdisciplina);
        }

        $project_ingresado = $project_ingresado->orderBy('trd.orden', 'ASC')
            ->distinct()
            ->get();

        // dd($project_ingresado);

        return $project_ingresado;

        /*----------  End Lista de cabezera  ----------*/
    }

    private function obtenerhorasrate($cactividad, $disciplina, $roldespliegue)
    {
        //dd($disciplina);
        $sumatoria_rate_hora = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpep.croldespliegue')
            ->select('tpa.cproyectoactividades', 'trd.descripcionrol', DB::raw("to_number(tph.horas, '99G999D9S') as horas"), 'tpep.rate', 'trd.croldespliegue', 'tpa.cproyectoactividades_parent');

        if ($roldespliegue != 'Todos') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('trd.croldespliegue', '=', $roldespliegue);
        }

        if ($disciplina == 'Nuevo') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tpa.cproyectoactividades', '=', $cactividad);
        } else {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tph.cdisciplina', '=', $disciplina)
                ->where('tph.cproyectoactividades', '=', $cactividad);
        }

        $sumatoria_rate_hora = $sumatoria_rate_hora->orderBy('trd.orden', 'ASC')->get();
        //dd($sumatoria_rate_hora);

        return $sumatoria_rate_hora;
    }

    private function obtenerhorasrateperformance($cactividad, $disciplina)
    {
        //dd($disciplina);
        $sumatoria_rate_hora = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpep.croldespliegue')
            ->select('tpa.cproyectoactividades', 'trd.descripcionrol', DB::raw("to_number(tph.horas, '99G999D9S') as horas"), 'tpep.rate', 'trd.croldespliegue', 'tpa.cproyectoactividades_parent');


        if ($disciplina == 'Nuevo') {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tpa.cproyectoactividades', '=', $cactividad);
        } else {

            $sumatoria_rate_hora = $sumatoria_rate_hora->where('tph.cdisciplina', '=', $disciplina)
                ->where('tph.cproyectoactividades', '=', $cactividad);
        }

        $sumatoria_rate_hora = $sumatoria_rate_hora->orderBy('trd.orden', 'ASC')->get();
        //dd($sumatoria_rate_hora);

        return $sumatoria_rate_hora;
    }



    private function activityfather_or_son($cactividad)
    {
        $activityfather_or_son = DB::table('tproyectoactividades as tpa')
            ->where('tpa.cproyectoactividades_parent', '=', $cactividad)
            ->get();

        return $activityfather_or_son;

    }

        public function liderorgerente($user)
    {
        
         $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.eslider','=','1')
            ->where('tpyp.cpersona', '=', $user)   
            ->get();

        $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user)
            ->get();


            if($esgerente){

                $lidergerente = 'gerente';
            }
            else{
                 $lidergerente = 'lider';

            }

            return  $lidergerente;
    }


    private function visualizar_porcentaje_for_semana($cactividad, $num_sem, $periodo,$user,$disciplina)
    {


         $lidergerente = $this->liderorgerente($user);

        $porcentaje = DB::table('tperformance as tper')
            ->leftjoin('tproyectoactividades as tpa', 'tper.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')

            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tper.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->where('tper.cproyectoactividades', '=', $cactividad)
            ->where('tper.semana', '=', $num_sem)
            ->where('tper.periodo', '=', $periodo);

           if($lidergerente == 'gerente' && $disciplina == 'Nuevo')
        {
            //dd($esgerente,'GP','Nuevo');
             $porcentaje = $porcentaje
                                      ->select('tper.cperformance','tper.porcentaje','tper.semana','tpa.cproyectoactividades','tda.cdisciplina')
                                     // ->where('tda.cdisciplina','=', $disciplina)
                                      ->orderBy('tper.semana', 'asc')
                                      ->distinct()
                                     ->get(); 

        }

         elseif($lidergerente == 'gerente'   && $disciplina != 'Nuevo')
        { //dd($esgerente,'GP','Disc');
                         $porcentaje = $porcentaje->select('tper.cperformance','tper.porcentaje','tper.semana','tpa.cproyectoactividades','tda.cdisciplina')
                        ->where('tda.cdisciplina','=', $disciplina)
                        ->orderBy('tper.semana', 'asc')
                        ->distinct()
                        ->first(); 
        }
       elseif($lidergerente == 'lider'){
        //dd($eslider,'lider');
             $porcentaje = $porcentaje->where('tper.created_user', '=', $user)
                                      ->select('tper.porcentaje', 'tper.semana','tpa.cproyectoactividades','tpa.cproyectoactividades_parent')
                                        ->orderBy('tper.semana', 'asc')
                                        ->first();
       }

        //dd($porcentaje);

        return $porcentaje;
    }


    private function visualizar_porcentaje_for_semana_parent($cactividad,$num_sem,$periodo,$user,$disciplina)
    {
         $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.eslider','=','1')
            ->where('tpyp.cpersona', '=', $user)   
            ->get();

         $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user)
            ->get();



        $porcentaje = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tperformance as tper', 'tper.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tareas as ta', 'ta.carea', '=', 'tper.carea')
            ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
            ->where('tpa.cproyectoactividades', '=', $cactividad)
            ->where('tper.semana', $num_sem )
            ->where('tper.periodo', $periodo );

            if( $esgerente && $disciplina != 'Nuevo')
            {
                 $porcentaje = $porcentaje->where('tda.cdisciplina',$disciplina )
                                         ->select('tpa.cproyectoactividades','tper.porcentaje','tper.semana','tper.periodo','tpa.descripcionactividad','tda.cdisciplina')->get();
            }

            elseif($esgerente && $disciplina == 'Nuevo')
            {
                /*$porcentaje = $porcentaje->groupBy('tpa.cproyectoactividades','tper.semana','tper.periodo','tpa.descripcionactividad')
                                         ->select('tpa.cproyectoactividades',DB::raw('ROUND(AVG(tper.porcentaje),2) as porcentaje'),'tper.semana','tper.periodo','tpa.descripcionactividad')->get();*/
                             $porcentaje = $porcentaje
                                      ->select('tper.cperformance','tper.porcentaje','tper.semana','tpa.cproyectoactividades','tda.cdisciplina')
                                     // ->where('tda.cdisciplina','=', $disciplina)
                                      ->orderBy('tper.semana', 'asc')
                                      ->distinct()
                                     ->get();



            }
           /* elseif($eslider && $disciplina != 'Nuevo'){

                $porcentaje = $porcentaje->select('tpa.cproyectoactividades','tper.porcentaje','tper.semana','tper.periodo','tpa.descripcionactividad')->get();
            }*/

             elseif($eslider){
        //dd($eslider,'lider');
             $porcentaje = $porcentaje->where('tper.created_user', '=', $user)
                                      ->select('tper.porcentaje', 'tper.semana','tpa.cproyectoactividades','tpa.cproyectoactividades_parent','tda.cdisciplina')
                                        ->orderBy('tper.semana', 'asc')
                                        ->get();
//dd($user);          
       }

            
            //->distinct()
        //dd($porcentaje);

        return $porcentaje;
    }


    private function versolohijosdepadres ($cactividad)
    {
         $onlyson = DB::table('tproyectoactividades as tpa')
                        ->where('tpa.cproyectoactividades_parent', '=', $cactividad)
                        ->select('tpa.cproyectoactividades')
                        ->get();

        return $onlyson;
    }


    private function suma_rate_activities_with_son($cactividad, $disciplina, $roldespliegue)
    {
        //dd($cactividad);

        $suma_rate_activities_with_son = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpep.croldespliegue')
            ->select('tpa.cproyectoactividades', 'trd.descripcionrol', DB::raw("to_number(tph.horas, '99G999D9S') as horas"), 'tpep.rate', 'trd.croldespliegue', 'tpa.cproyectoactividades_parent');

        if ($roldespliegue != 'Todos') {

            $suma_rate_activities_with_son = $suma_rate_activities_with_son->where('trd.croldespliegue', '=', $roldespliegue);
        }

        if ($disciplina == 'Nuevo') {

            $suma_rate_activities_with_son = $suma_rate_activities_with_son->where('tpa.cproyectoactividades_parent', '=', $cactividad);
        } else {

            $suma_rate_activities_with_son = $suma_rate_activities_with_son->where('tph.cdisciplina', '=', $disciplina)
                ->where('tpa.cproyectoactividades_parent', '=', $cactividad);
        }

        $suma_rate_activities_with_son = $suma_rate_activities_with_son->orderBy('trd.orden', 'ASC')->get();

        //dd($disciplina,$suma_rate_activities_with_son);

        return $suma_rate_activities_with_son;

    }

    public function guardar_porcentaje(Request $request)
    {
      //  dd($request->all());

        $user = Auth::user();

        $carea = DB::table('tproyectopersona as typ')
            ->leftjoin('tdisciplina as tdisc', 'tdisc.cdisciplina', '=', 'typ.cdisciplina')
            ->leftjoin('tdisciplinaareas as tda', 'tda.cdisciplina', '=', 'typ.cdisciplina')
            ->select('tda.carea')
            ->where('typ.cpersona' , '=', $user->cpersona)
            ->where('typ.cproyecto' , '=', $request->cproyecto)
            ->first();

        $tperformance = Tperformance::where('cproyectoactividades', '=', $request->get('cproyectoactividades'))
            ->where('semana', '=', $request->get('semana'))
            ->where('periodo', '=', $request->get('periodo'))
            ->where('carea', '=', $carea->carea)
            ->first();

        if (!$tperformance) {
            $tperformance = new Tperformance();
           
        }

       
        $tperformance->cproyectoactividades = $request->get('cproyectoactividades');
        $tperformance->porcentaje           = $request->get('porcentaje');
        $tperformance->semana               = $request->get('semana');
        $tperformance->periodo              = $request->get('periodo');
        $tperformance->cproyecto              = $request->get('cproyecto');
        $tperformance->created_user         = $user->cpersona;
        $tperformance->carea                = $carea == null ? 0: $carea->carea;
        $tperformance->save();


        return $tperformance;
    }

    private function horas_ejecutadas($fec_fin_full,$cactividad,$disciplina,$cproyecto)
    {
          $horas_ejecutadas_sum = DB::table('tproyectoejecucion as tpe')
                    ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
                    ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
                    ->select(DB::raw("sum(tpe.horasejecutadas) as sum"));
                if ($disciplina == 'Nuevo') {

                    $horas_ejecutadas_sum = $horas_ejecutadas_sum->where('tpe.cproyectoactividades', '=', $cactividad);
                } else {

                    $horas_ejecutadas_sum = $horas_ejecutadas_sum->where('tda.cdisciplina', '=', $disciplina)
                                                                ->where('tpe.cproyectoactividades', '=',$cactividad );
                }


               $horas_ejecutadas_sum = $horas_ejecutadas_sum->where('tpe.cproyecto', '=',$cproyecto )
                                                            ->where('tpe.fplanificado','<=',$fec_fin_full)    
                                                             ->first();
                $horas_ejecutadas = 0;

                if ($horas_ejecutadas_sum->sum != null) {
                    $horas_ejecutadas = round($horas_ejecutadas_sum->sum,2);
                }

               return  $horas_ejecutadas;
    }


    private function horasxrateplanificadastotales ($fec_fin_full,$cactividad,$disciplina,$cproyecto,$categoriaprofesional)
    {
       

        $rateshorasejecutadas = DB::table('tproyectoejecucion as tpe')
                    ->leftjoin('tproyectoactividades as tpa', 'tpe.cproyectoactividades', '=', 'tpa.cproyectoactividades')
                    ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
                    ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
                    ->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
                    ->leftjoin('tproyectopersona as tpp', 'tp.cpersona', '=', 'tpp.cpersona')
                    ->leftjoin('tcategoriaprofesional as tcp', 'tpp.ccategoriaprofesional', '=', 'tcp.ccategoriaprofesional')
                    //->select('tpa.cproyectoactividades','sum(tpe.horasejecutadas) as horasejecutadas)','ta.descripcion as area','tpa.descripcionactividad','tcp.ccategoriaprofesional','tcp.descripcion as categoria','tp.nombre','tcp.rate','tpe.carea');
                     ->select('tpe.cproyectoactividades',DB::raw("sum(tpe.horasejecutadas) as horasejecutadas"),'tcp.rate','tpe.carea','tpp.cdisciplina','tcp.ccategoriaprofesional');

                       if ($categoriaprofesional != 'Todos') {

                        $rateshorasejecutadas = $rateshorasejecutadas->where('tpp.ccategoriaprofesional', '=', $categoriaprofesional);

                    }
                    if ($disciplina == 'Nuevo') {

                        $rateshorasejecutadas = $rateshorasejecutadas->where('tpe.cproyectoactividades', '=',$cactividad);

                    } else {

                        $rateshorasejecutadas = $rateshorasejecutadas->where('tpe.cproyectoactividades', '=',$cactividad)
                            ->where('tda.cdisciplina', '=',$disciplina);
                    }


                   $rateshorasejecutadas = $rateshorasejecutadas->where('tpp.cproyecto', '=',$cproyecto)
                                                                ->where('tpe.cproyecto', '=',$cproyecto )
                                                                ->where('tpe.fplanificado','<=',$fec_fin_full)
                                                                ->whereNotNull('tpe.carea')
                                                                ->groupBy('tpe.cproyectoactividades','tcp.rate','tpe.carea','tpp.cdisciplina','tcp.ccategoriaprofesional')
                                                                ->get();
       // dd($rateshorasejecutadas);
        return   $rateshorasejecutadas ;         
                    
    }
    public function obtenerateconsumidas_condetalle($fec_fin_full,$cactividad,$disciplina,$cproyecto){
        $cproyecto = $cproyecto;
        $cabecera_participante_montoConsumidos = $this->horasxrateplanificadastotalescabecera($cproyecto,$disciplina);
        //dd($cabecera_participante_montoConsumidos);

        $arraysumadetalleactivity__consumidas   = [];
        $suma_rate_detalle_activity__consumidas= 0;
        foreach ($cabecera_participante_montoConsumidos as $ctp) {
            // dd($cab);
            $uniquecategoriaProfesional = [];
            $sumatoria_rate_hora_detalle__consumidas = $this->horasxrateplanificadastotales($fec_fin_full,$cactividad,$disciplina,$cproyecto, $ctp->ccategoriaprofesional);
            foreach ($sumatoria_rate_hora_detalle__consumidas as $ucatep)
            {
                $uct['multi'] = $ucatep->horasejecutadas * $ucatep->rate;
                $uct['ccategoriaaprofesional'] = $ucatep->ccategoriaprofesional;
                array_push($uniquecategoriaProfesional,$uct);
            }

            $arrayempleadosactivity = [];
            foreach ($uniquecategoriaProfesional as $ahnf){

                $repeat=false;
                for($i=0;$i<count($arrayempleadosactivity);$i++)
                {
                    if($arrayempleadosactivity[$i]['ccategoriaaprofesional']==$ahnf['ccategoriaaprofesional'])
                    {
                        $arrayempleadosactivity[$i]['multi']+=$ahnf['multi'];
                        $repeat=true;
                        break;
                    }
                }
                if($repeat==false)
                    $arrayempleadosactivity[] = array('ccategoriaaprofesional' => $ahnf['ccategoriaaprofesional'], 'multi' => $ahnf['multi']);

            }



            $var2 = 0;
            foreach ($arrayempleadosactivity as $ccateg)
            {
                if ($ctp->ccategoriaprofesional == $ccateg['ccategoriaaprofesional'])
                {
                    $var2 = $ccateg['multi'];
                    $suma_rate_detalle_activity__consumidas += $var2;
                }
            }
            array_push($arraysumadetalleactivity__consumidas,['rateporCategoria' => $var2]);

        }
        //dd($arraysumadetalleactivity__consumidas,$suma_rate_detalle_activity__consumidas);
        return array("arrayConsumidosCategoria" => $arraysumadetalleactivity__consumidas, "sumtotalConsumidos" => $suma_rate_detalle_activity__consumidas);



    }


        private function horasxrateplanificadastotalescabecera ($cproyecto,$disciplina)
    {
        //dd($cactividad,$disciplina,$cproyecto,$ccategoriaprofesional);


        $horasxrateplanificadastotalescabecera = DB::table('tproyectoejecucion as tpe')
                    
                    ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
                    ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')

                    ->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
                    ->leftjoin('tproyectopersona as tpp', 'tp.cpersona', '=', 'tpp.cpersona')
                   
                    ->leftjoin('tcategoriaprofesional as tcp', 'tpp.ccategoriaprofesional', '=', 'tcp.ccategoriaprofesional')

                    ->select('tcp.descripcion as categoria','tcp.orden','tcp.ccategoriaprofesional');

                    if ($disciplina != 'Nuevo') {

                            $horasxrateplanificadastotalescabecera = $horasxrateplanificadastotalescabecera->where('tpp.cdisciplina', '=',$disciplina);
                                                                         
                     }

                   $horasxrateplanificadastotalescabecera = $horasxrateplanificadastotalescabecera
                                                                ->where('tpp.cproyecto', '=',$cproyecto)
                                                                ->where('tpe.cproyecto', '=',$cproyecto )
                                                                ->whereNotNUll('tcp.ccategoriaprofesional')
                                                                ->orderBy('tcp.orden', 'ASC')->distinct()->get();
                 //  dd($rateshorasejecutadas);    
                 

        return   $horasxrateplanificadastotalescabecera ;         
                    
    }

      private function horasxrateplanificadastotales_por_activity($fec_fin_full,$cactividad,$cproyecto,$disciplina,$categoriaprofesional)
    {
       
      // dd($semana_fin,$anio_final,$cactividad,$cproyecto,$disciplina,$categoriaprofesional);
        $horasxrateplanificadastotales_por_activity = DB::table('tproyectoejecucion as tpe')
                    ->leftjoin('tproyectoactividades as tpa', 'tpe.cproyectoactividades', '=', 'tpa.cproyectoactividades')

                    ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
                    ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')

                    ->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
                    ->leftjoin('tproyectopersona as tpp', 'tp.cpersona', '=', 'tpp.cpersona')
                   
                    ->leftjoin('tcategoriaprofesional as tcp', 'tpp.ccategoriaprofesional', '=', 'tcp.ccategoriaprofesional')

                    //->select('tpe.cproyectoejecucion','tpa.cproyectoactividades','tpe.horasejecutadas','ta.descripcion as area','tpa.descripcionactividad','tcp.ccategoriaprofesional','tcp.descripcion as categoria','tcp.ccategoriaprofesional','tp.nombre','tcp.rate','tpe.carea');
                    ->select('tpe.cproyectoejecucion','tpe.horasejecutadas','tpa.cproyectoactividades','tcp.descripcion','tpp.cdisciplina','tpe.fplanificado');


                     if ($categoriaprofesional != 'Todos') {

                        $horasxrateplanificadastotales_por_activity = $horasxrateplanificadastotales_por_activity->where('tpp.ccategoriaprofesional', '=', $categoriaprofesional);
                    }   

                    
                    if ($disciplina == 'Nuevo') {

                            $horasxrateplanificadastotales_por_activity = $horasxrateplanificadastotales_por_activity->where('tpa.cproyectoactividades', '=',$cactividad)
                                                                                                            ->where('tpp.ccategoriaprofesional', '=', $categoriaprofesional);
                                                                         
                     } else {

                            $horasxrateplanificadastotales_por_activity = $horasxrateplanificadastotales_por_activity->where('tpa.cproyectoactividades', '=',$cactividad)
                                                                         ->where('tpp.ccategoriaprofesional', '=', $categoriaprofesional)
                                                                        ->where('tda.cdisciplina', '=',$disciplina);
                     }


                   $horasxrateplanificadastotales_por_activity = $horasxrateplanificadastotales_por_activity->where('tpp.cproyecto', '=',$cproyecto)
                                                                ->where('tpe.cproyecto', '=',$cproyecto )
                                                                ->where('tpe.fplanificado','<=',$fec_fin_full)            
                                                                ->orderBy('tpe.cproyectoejecucion','asc')
                                                                ->get();
                 //  dd($horasxrateplanificadastotales_por_activity);    
                 

                    
        return   $horasxrateplanificadastotales_por_activity ;         
                    
    }


      private function horasxrateplanificadastotales_por_activity_parent($fec_fin_full,$cactividad,$cproyecto,$disciplina,$categoriaprofesional)
    {
       
      // dd($semana_fin,$anio_final,$cactividad,$cproyecto,$disciplina,$categoriaprofesional);
        $horasxrateplanificadastotales_por_activity = DB::table('tproyectoejecucion as tpe')
                    ->leftjoin('tproyectoactividades as tpa', 'tpe.cproyectoactividades', '=', 'tpa.cproyectoactividades')

                    ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
                    ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')

                    ->leftjoin('tpersona as tp', 'tp.cpersona', '=', 'tpe.cpersona_ejecuta')
                    ->leftjoin('tproyectopersona as tpp', 'tp.cpersona', '=', 'tpp.cpersona')
                   
                    ->leftjoin('tcategoriaprofesional as tcp', 'tpp.ccategoriaprofesional', '=', 'tcp.ccategoriaprofesional')


                    ->select('tpe.cproyectoejecucion','tpe.horasejecutadas','tpa.cproyectoactividades','tcp.rate','tpe.carea','tpp.cdisciplina','tpe.fplanificado');
 


                    
                    if ($disciplina != 'Nuevo') {

                            $horasxrateplanificadastotales_por_activity = $horasxrateplanificadastotales_por_activity->where('tda.cdisciplina', '=',$disciplina);
                                                                         
                     } 


                   $horasxrateplanificadastotales_por_activity = $horasxrateplanificadastotales_por_activity->where('tpp.cproyecto', '=',$cproyecto)
                                                                ->where('tpe.cproyecto', '=',$cproyecto )
                                                                ->where('tpe.fplanificado','<=',$fec_fin_full) 
                                                                ->where('tpa.cproyectoactividades_parent', '=',$cactividad)         
                                                                ->orderBy('tpe.cproyectoejecucion','asc')
                                                                ->distinct()
                                                                ->get();
                   //dd($horasxrateplanificadastotales_por_activity);    
                 

                    
        return   $horasxrateplanificadastotales_por_activity ;         
                    
    }

        public function getfecha($cproyecto)
    {
        
         $fecha = DB::table('tproyecto as tpy')
            ->where('tpy.cproyecto', '=', $cproyecto)
            ->select('tpy.cproyecto', 'tpy.finicio as finicio', 'tpy.fcierre as fcierre')
            ->get();

        $fecha_array = [];
        foreach ($fecha as $fch)
        {
            $fec['finicio'] = $fch->finicio;
            $fec['fcierre'] = $fch->fcierre;
            array_push($fecha_array,$fec);
        }

       // dd($fecha_array);
        return $fecha_array;

   }



public function listarprojectgeneralcurva()
    {

        $user = Auth::user();

        // conocemos el area de la persona
        $tarea = DB::table('tpersonadatosempleado as e')
            ->leftjoin('tareas as ta', 'e.carea', '=', 'ta.carea')
            ->leftjoin('tdisciplinaareas as tda', 'ta.carea', '=', 'tda.carea')
            ->where('cpersona', '=', $user->cpersona)
            ->whereNull('ftermino')
            ->orderBy('fingreso', 'DESC')
            ->select('e.cpersona', 'e.carea', 'e.ccargo', 'ta.carea', 'ta.descripcion', 'tda.cdisciplina')
            ->first();

           // dd($tarea);


        $cdisciplina = null;
        if ($tarea) {
            $cdisciplina = $tarea->cdisciplina;
        }

       // dd($cdisciplina);
        // Si es gerente o no
        $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user->cpersona)
            ->get();

         $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.cpersona', '=', $user->cpersona)   
            ->get();

            $lider_or_gerente = '';

            if($esgerente)
            {
                 $lider_or_gerente = 'gerente';
            }
            elseif($eslider)
            {
                  $lider_or_gerente = 'lider';
            }

           // dd($lider_or_gerente);  

        //dd($cdisciplina);
        $tproyectos = DB::table('tproyecto as tp')
            ->leftjoin('tproyectopersona as tpyp', 'tpyp.cproyecto', '=', 'tp.cproyecto')
            ->leftjoin('tproyectodisciplina as tda', 'tp.cproyecto', '=', 'tda.cproyecto');
        //->where('tp.cestadoproyecto','=','001');

        if ($esgerente) {
            $tproyectos = $tproyectos->where('tp.cpersona_gerente', '=', $user->cpersona);

        }
        elseif($eslider)
        {
            $tproyectos = $tproyectos->where('tpyp.cpersona','=', $user->cpersona)
                                      ->where('tpyp.eslider', '=', '1')          ;
                                     

        }
         else {
            $tproyectos = $tproyectos->where('tda.cdisciplina', '=', $cdisciplina);
        }

        $tproyectos = $tproyectos->select('tp.cproyecto', 'tp.codigo', 'tp.nombre',DB::raw("to_char(tp.finicio,'DD-MM-YYYY') as finicio"),DB::raw("to_char(tp.fcierre,'DD-MM-YYYY') as fcierre"))
            ->distinct()
            ->orderBy('tp.codigo', 'ASC')
            ->get();


        return view('proyecto.curvas', compact('tproyectos','lider_or_gerente'));

    }





    public function obtenercurvas_horas($cproyecto,$fdesde,$fhasta,$disciplina,$num_sem)
    //public function obtenercurvas_horas($cproyecto,$fdesde,$fhasta,$disciplina,$num_sem)

    {

     /*   $semana_ini = $fdesde->weekOfYear;
        $periodo_ini = $fdesde->year;

        $semana_fin = $fhasta->weekOfYear;
        $periodo_fin = $fhasta->year;*/

        //dd($cproyecto,$fdesde,$fhasta,$disciplina,$semana_ini, $periodo_ini,  $semana_fin , $periodo_fin);
        //dd($cproyecto,$fdesde,$fhasta,$disciplina);


        $curvahoras = DB::table('tproyecto as tp')
                        ->leftjoin('tproyectoejecucion as tpe', 'tpe.cproyecto', '=', 'tp.cproyecto')
                        ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
                        ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')
                        ->where('tpe.cproyecto','=',$cproyecto)

                        /*->where('tpe.fplanificado','>=',$fdesde)
                        ->where('tpe.fplanificado','<=',$fhasta)*/
                        ->whereBetween('tpe.fplanificado', array($fdesde,$fhasta))
                        ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$num_sem)

                        ->select(DB::raw('sum(tpe.horasejecutadas) as sum'),
                          // DB::raw('sum(sum(tpe.horasejecutadas)) over (PARTITION BY extract(week from tpe.fplanificado) ORDER BY tpe.fplanificado) as acumulado'),
                            'tpe.cproyecto')
                        ->groupBy('tpe.cproyecto');
                        /*->select('tpe.cproyecto','tpe.cproyectoejecucion','tpe.fplanificado')
                         ->orderBy('tpe.fplanificado', 'ASC');*/

                         if ($disciplina != 'Nuevo') {

                            $curvahoras =  $curvahoras->where('tda.cdisciplina', '=',$disciplina);                                                 
                          }

                          $curvahoras =  $curvahoras->first();

        // dd($curvahoras);                                                                  
                          
         return  $curvahoras;             

    }


    public function  obtenercurvas_horas_rates ($cproyecto,$fdesde,$fhasta,$disciplina,$num_sem)
    {

        $curvahorasrate = DB::table('tproyecto as tp')
                        ->leftjoin('tproyectoejecucion as tpe', 'tpe.cproyecto', '=', 'tp.cproyecto')
                        //->leftjoin('tproyectoactividades as tpe', 'tpe.cproyecto', '=', 'tp.cproyecto')
                        ->leftjoin('tareas as ta', 'ta.carea', '=', 'tpe.carea')
                        ->leftjoin('tdisciplinaareas as tda', 'tda.carea', '=', 'ta.carea')

                        ->leftjoin('tpersona as tpers', 'tpers.cpersona', '=', 'tpe.cpersona_ejecuta')
                        ->leftjoin('tproyectopersona as tpp', 'tpers.cpersona', '=', 'tpp.cpersona')
                        ->leftjoin('tcategoriaprofesional as tcp', 'tpp.ccategoriaprofesional', '=', 'tcp.ccategoriaprofesional')


                        ->where('tpe.cproyecto','=',$cproyecto)

                        /*->where('tpe.fplanificado','>=',$fdesde)
                        ->where('tpe.fplanificado','<=',$fhasta)*/
                        ->whereBetween('tpe.fplanificado', array($fdesde,$fhasta))
                        ->where(DB::raw("extract(week from tpe.fplanificado)"),'=',$num_sem)


                       /* ->where(DB::raw("extract(week from tpe.fplanificado)"),'>=',$semana_ini)
                        ->where(DB::raw("extract(year from tpe.fplanificado)"),'>=',$periodo_ini)

                         ->where(DB::raw("extract(week from tpe.fplanificado)"),'<=',$semana_fin)
                        ->where(DB::raw("extract(year from tpe.fplanificado)"),'<=',$periodo_fin)*/

                        //->select(DB::raw("ROUND('sum(tpe.horasejecutadas) as sum')"),'tpe.cproyecto')
                        // ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"), 'ASC')
                        ->select('tpe.cproyectoejecucion','tpe.fplanificado','tpe.horasejecutadas','tcp.rate',
                           /* DB::raw('sum(sum(tpe.sum)) over as acumulado'),*/
                            'tpe.cproyecto')
                        ->distinct()
                        ->orderBy('tpe.cproyectoejecucion', 'ASC');
                       // ->groupBy('tpe.cproyecto');
                        /*->select('tpe.cproyecto','tpe.cproyectoejecucion','tpe.fplanificado')
                         ->orderBy('tpe.fplanificado', 'ASC');*/

                         if ($disciplina != 'Nuevo') {

                            $curvahorasrate =  $curvahorasrate->where('tda.cdisciplina', '=',$disciplina);                                                 
                          }

                          $curvahorasrate =  $curvahorasrate->get();

        // dd($curvahorasrate);                                                                  
                          
         return  $curvahorasrate; 

    }

    public function curvaperformanceavance($cactividad,$disciplina)

    {

         $activity_sum_padre = DB::table('tproyectohonorarios as tph')
                         ->leftjoin('tproyectoactividades as tpa', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
                           ->select(DB::raw("sum(to_number(horas, '99G999D9S')) as sum"))
                           ->where('tpa.cproyectoactividades_parent', '=', $cactividad); 
                                             

                if ($disciplina != 'Nuevo') {
                   
                        $activity_sum_padre =  $activity_sum_padre ->where('tph.cdisciplina', '=', $disciplina); 
                }

                $activity_sum_padre = $activity_sum_padre->first();

                // dd($activity_sum);

                $var = 0;

                if ($activity_sum_padre->sum != null) {
                    $var = $activity_sum_padre->sum;
                }

               

          return   $var ;        

    }


     private function actividadespadreproyecto($cproyecto)
    {
        $activityfather_or_son = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyecto as tp', 'tp.cproyecto', '=', 'tpa.cproyecto')
            ->select('tpa.cproyectoactividades_parent')
            ->whereNotNull('cproyectoactividades_parent')
            ->where('tp.cproyecto', '=', $cproyecto)
            ->orderBy('cproyectoactividades_parent','ASC')
            ->distinct()
            ->get();
            //dd($activityfather_or_son);

        return $activityfather_or_son;
    }

     private function sumarateparentpresupuestadas($cactividad, $disciplina)
    {
        //dd($cactividad);

        $suma_rate_activities_with_son = DB::table('tproyectoactividades as tpa')
            ->leftjoin('tproyectohonorarios as tph', 'tph.cproyectoactividades', '=', 'tpa.cproyectoactividades')
            ->leftjoin('tproyectoestructuraparticipantes as tpep', 'tpep.cestructuraproyecto', '=', 'tph.cestructuraproyecto')
            ->leftjoin('troldespliegue as trd', 'trd.croldespliegue', '=', 'tpep.croldespliegue')
            ->select('tpa.cproyectoactividades', DB::raw("to_number(tph.horas, '99G999D9S') as horas"), 'tpep.rate', 'tpa.cproyectoactividades_parent');


        if ($disciplina == 'Nuevo') {

            $suma_rate_activities_with_son = $suma_rate_activities_with_son->where('tpa.cproyectoactividades_parent', '=', $cactividad);
        } else {

            $suma_rate_activities_with_son = $suma_rate_activities_with_son->where('tph.cdisciplina', '=', $disciplina)
                ->where('tpa.cproyectoactividades_parent', '=', $cactividad);
        }

        $suma_rate_activities_with_son = $suma_rate_activities_with_son->orderBy('trd.orden', 'ASC')->get();

        //dd($disciplina,$suma_rate_activities_with_son);

        return $suma_rate_activities_with_son;

    }

    public function lider_or_gerente()
    {
        $user = Auth::user();

        $esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $user->cpersona)
            ->get();
        //dd($esgerente);

        $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.cpersona', '=', $user->cpersona)
            ->where('tpyp.eslider','=','1')
            ->get();

        $cargoId = DB::table('tpersonadatosempleado as e')
            ->where('cpersona', '=', $user->cpersona)
            ->whereNull('ftermino')
            ->orderBy('fingreso', 'DESC')
            ->select('cpersona','ccargo')
            ->first();

        $esjefe = DB::table('tcargos as tcar')
            ->where('tcar.cargoparentdespliegue', '=', $cargoId->ccargo)
            ->get();


        $lider_or_gerente = '';

        if($eslider)
        {
            $lider_or_gerente = 'lider';
        }
        if($esgerente)
        {
            $lider_or_gerente = 'gerente';
        }
        if($esjefe)
        {
            $lider_or_gerente = 'jefe';
        }

        return $lider_or_gerente;

    }

    public function identificarProyecto($cproyecto)
    {
        $project_ingresado = DB::table('tproyecto as tp')
            ->leftjoin('tunidadminera as tum', 'tum.cunidadminera', '=', 'tp.cunidadminera')
            ->select('tp.codigo', 'tp.cproyecto', 'tp.nombre', 'tum.nombre as nombreunidadMinera')
            ->where('tp.cproyecto', '=', $cproyecto)
            ->first();

        return $project_ingresado;
    }

    public function listadeActividadesHijosDirectosporProyecto($cproyecto)
    {
        $actividades = DB::table('tproyectoactividades')
            ->select('cproyectoactividades','codigoactvidad','descripcionactividad','cproyectoactividades_parent')
            ->where('cproyecto', '=',$cproyecto)
            ->where('cproyectoactividades_parent', '=',null)
            ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
            ->get();
        return $actividades;
    }

    private function actividad_padre_or_hijo($cactividad)
    {
        $activityfather_or_son = DB::table('tproyectoactividades as tpa')
            ->select('tpa.cproyectoactividades','tpa.codigoactvidad','tpa.descripcionactividad','tpa.cproyecto','tpa.cproyectoactividades_parent')
            ->orderBy('tpa.codigoactvidad','ASC')
            ->where('tpa.cproyectoactividades_parent', '=', $cactividad)
            ->get();

        return $activityfather_or_son;

    }
    private function obtenerhoraspresuuestadasPerformance($cactividad,$disciplina)
    {
        $activity_sum = DB::table('tproyectohonorarios as tph')
            ->select(DB::raw("sum(to_number(horas, '99G999D9S')) as sum"));

        if ($disciplina == 'Nuevo') {

            $activity_sum = $activity_sum->where('tph.cproyectoactividades', '=', $cactividad);
        } else {

            $activity_sum = $activity_sum->where('tph.cdisciplina', '=', $disciplina)
                ->where('tph.cproyectoactividades', '=',$cactividad);
        }

        $activity_sum = $activity_sum->first();

        // dd($activity_sum);

        $var = 0;

        if ($activity_sum->sum != null) {
            $var = $activity_sum->sum;
        }
        return $var;

    }

    private function obteneratepresuestadas_condetalle($cactividad,$disciplina,$cproyecto)
    {
        $cproyecto = $cproyecto;

        $cabecera_participante = $this->getCabezera($cproyecto, $disciplina);
        $suma_rate_detalle_activity = 0;
        $arraysumadetalleactivity   = [];

        foreach ($cabecera_participante as $cab) {
            // dd($cab);
            $sumatoria_rate_hora_detalle = $this->obtenerhorasrate($cactividad, $disciplina, $cab->croldespliegue);

            $var1 = 0;
            if ($sumatoria_rate_hora_detalle) {

                foreach ($sumatoria_rate_hora_detalle as $sr) {
                    //dd($sr);
                    $suma_actividad = $sr->horas * $sr->rate;
                    $suma_rate_detalle_activity += $suma_actividad;


                    if ($suma_actividad != null) {
                        $var1 = $suma_actividad;
                    }
                }
            }
            array_push($arraysumadetalleactivity, ['suma' => $var1]);
        }


        return array("estructuraParticipantes" => $arraysumadetalleactivity, "sumtotalPresupuestadas" => $suma_rate_detalle_activity);
    }

    public function numerosemana($fDesde,$fHasta,$cactividad,$disciplina,$montoPresupuestad,$cactividadPadre)
    {
        $fechaDesde = $fDesde;
        $fechaHasta = $fHasta;
        $cactividad = $cactividad;
        $montoPresupuestad = $montoPresupuestad;
        $cactividadPadre = $cactividadPadre;
        //$montoTotalPrespuestosPadre = $montoTotalPrespuestosPadre;

        if (substr($fechaDesde, 2, 1) == '/' || substr($fechaDesde, 2, 1) == '-') {
            $fechaDesde = Carbon::create(substr($fechaDesde, 6, 4), substr($fechaDesde, 3, 2), substr($fechaDesde, 0, 2));
            $fechaHasta = Carbon::create(substr($fechaHasta, 6, 4), substr($fechaHasta, 3, 2), substr($fechaHasta, 0, 2));

        } else {
            $fechaDesde = Carbon::create(substr($fechaDesde, 0, 4), substr($fechaDesde, 5, 2), substr($fechaDesde, 8, 2));
            $fechaHasta = Carbon::create(substr($fechaHasta, 0, 4), substr($fechaHasta, 5, 2), substr($fechaHasta, 8, 2));
        }




        $semana_ini = $fechaDesde->weekOfYear;
        $semana_fin =   $fechaHasta->weekOfYear;

        //dd($semana_ini,$semana_fin);

        $anio_inicial = $fechaDesde->format('Y');
        $anio_final   = $fechaHasta->format('Y');

        $anio_inicial = intval($anio_inicial);
        $anio_final   = intval($anio_final);


        //dd($fechaDesde,$fechaHasta,$semana_ini,$semana_fin,$anio_inicial,$anio_final,'OK');
        $num_sema = [];
        if ($anio_final == $anio_inicial) {

            for ($i = $semana_ini; $i <= $semana_fin; $i++) {

                //array_push($num_sema, $i);

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }

        } else if ($anio_final > $anio_inicial) {

            for ($i = $semana_ini; $i <= 52; $i++) {

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_inicial]);
            }

            // dd($num_sema);

            for ($i = 1; $i <= $semana_fin; $i++) {

                array_push($num_sema, ['semana' => $i, 'anio' => $anio_final]);
            }
        }

        $array_porcentaje          = [];
        $semana_start              = Carbon::now()->weekOfYear;


        foreach ($num_sema as $ns) {
            $user = Auth::user();
            $porcentaje = $this->visualizar_porcentaje_for_semana($cactividad, $ns['semana'], $ns['anio'],$user->cpersona,$disciplina);

            $liderorgerente =  $this->liderorgerente($user->cpersona);

            $arrayporcentaje = [];
            $pesototal = 0;
            $sumaareas = 0;
            $var2 = 0;
            if($liderorgerente == 'gerente' && $disciplina == 'Nuevo'){
                if($porcentaje){
                    foreach ($porcentaje as $kpp) {
                        $c['cdisciplina'] = $kpp->cdisciplina;
                        $c['cperformance'] = $kpp->cperformance;
                        $c['cproyectoactividades'] = $kpp->cproyectoactividades;
                        $c['semana'] = $kpp->semana;
                        $c['porcentaje'] = $kpp->porcentaje;
                        $acum = 0;
                        $data = $this->obtenerhorasrateperformance($kpp->cproyectoactividades,$kpp->cdisciplina);

                        foreach ($data as $dt) {
                            $producto =  $dt->horas *  $dt->rate;
                            $acum  += $producto;
                        }
                        //dd($data);

                        $c['montoarea'] =  $acum ? $acum : 0;
                        $c['multiplicacion'] =  $c['montoarea'] *  $c['porcentaje'] ;
                        $sumaareas +=  $c['montoarea'];
                        $pesototal +=  $c['multiplicacion'];
                        $var2 =   $sumaareas != 0 ?  $pesototal/$sumaareas : 0 ;
                        array_push($arrayporcentaje,$c);
                    }
                }
            }

            if ($liderorgerente == 'lider' || ($liderorgerente == 'gerente' && $disciplina != 'Nuevo')) {
                if ($porcentaje != null) {
                    $var2 = $porcentaje->porcentaje;
                }

            }

            $habilitado = true;
            if ($anio_inicial == $anio_final) {
                if ($ns['semana'] < $semana_start &&  $ns['semana'] < $semana_start - 2 ) {
                    $habilitado = false;
                }
                else if ($ns['semana'] > $semana_start)
                {
                    $habilitado = false;
                }

            } else if ($anio_final > $anio_inicial) {
                if ($ns['semana'] < $semana_start ) {
                    $habilitado = false;
                }

                else if ($ns['semana'] > $semana_start) {
                    $habilitado = false;
                }
            }

            array_push($array_porcentaje, ['porcentaje' => round($var2,2),
                'habilitado'                                => $habilitado,
                'semana'                                    => $ns['semana'],
                'anio'                                      => $ns['anio'],
                'porcentaje_por_montoPre'                   => round($var2,2) * $montoPresupuestad ,
                'pk_actividadPadre'                         => $cactividadPadre
               /* 'pk_parent'                                 => $act->cproyectoactividades_parent,
                'pk_actividad'                                 => $act->cproyectoactividades*/

            ]);
        }

        return $array_porcentaje;


    }




}
