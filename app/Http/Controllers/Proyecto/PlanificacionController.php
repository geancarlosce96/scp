<?php

namespace App\Http\Controllers\Proyecto;

use  App\Http\Controllers\Controller;
use App\Models\Planificacion;
use App\Models\Planificacion_log;
use App\Models\Tpersona;
use App\Models\Tpersonadatosempleado;
use App\Models\Tproyecto;
use App\Models\Tproyectopersona;
use App\Models\Tusuario;
use App\Models\Tproyectoejecucion;
use App\Models\Tproyectohonorario;
use App\Models\Tarea;
use Auth;
use DB;
use Illuminate\Http\Request;
use Date;
use App\Erp\PanelSupport;

class PlanificacionController extends Controller
{
	//public 
	public function index()
	{
		$usuario = Auth::user()->nombre;
		$fecha = Date("Y-m-d");

		$jefe = $this->isjefe(Auth::user()->cpersona);

		return view("proyecto.planificacion.horas", ["usuario" => $usuario, "fecha" => $fecha, 'jefe'=>$jefe]);
	}

	private function isjefe($cpersona){

		$tpersona=DB::table('tpersona as per')        
		  ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
		  ->where('per.cpersona','=',$cpersona)
		  ->first();

		 $ccargo=null;
		  if ($tpersona) {
		    $ccargo=$tpersona->ccargo;
		  }

		  $tcargos=DB::table('tcargos as tc') 
		  ->where('tc.ccargo','=',$ccargo)
		  ->first();

		  $jefe = 0;

		  if ($tcargos) {
			  if ($tcargos->esjefaturaarea == '1') {
			  	$jefe = 1;
			  }
			  else {
			  	$jefe = 0;
			  }
		  }

		  return $jefe;
	}

	public function obtenerProyectos(Request $request)
	{
		$obj = [];
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");
		$mostrarProyectosConHoras = $request->input("mostrarProyectosConHoras");

		$jefecito = $this->isjefe(Auth::user()->cpersona);

		// dd($jefecito);

		if ($jefecito == 1) {
			$lista = Tproyecto::where("cestadoproyecto", "=", "001")->get();
		}
		else {
			$lista = Tproyectopersona::where("eslider", "=", "1")->where("cpersona", "=", Auth::user()->cpersona)->get();
		}
			
		// $lista = Tproyectopersona::where("eslider", "=", "1")->where("cpersona", "=", Auth::user()->cpersona)->get();

		if($mostrarProyectosConHoras == "true")
			$lista = $this->filtrarProyectosConHorasRegistradas($lista, $periodo, $semana);

		foreach ($lista as $key => $item) {
			$proyecto = Tproyecto::find($item->cproyecto);
			$suma_eje = Tproyectoejecucion::where('cproyecto','=',$item->cproyecto)->sum('horasejecutadas');
			$item->nombre = $proyecto->codigo ." ". $proyecto->nombre;
			$item->suma_eje = $suma_eje;
		}


		$obj["lista"] = $lista;
		return $obj;
	}

	public function obtenerLogsdeProyecto($cproyecto)
	{
		$obj = [];
		$logs = Planificacion_log::where("cproyecto", "=", $cproyecto)->orderBy("created_at", "desc")->take(5)->get();
		$obj["lista"] = $logs;
		return $obj;
	}

	public function obtenerUsuariosdeProyecto(Request $request, $cproyecto)
	{
		$obj = [];
		$cpersona = Auth::user()->cpersona;
		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();
		$lista = [];

		//obtener periodo, semana
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");
		$mostrarUsuariosConHoras = $request->input("mostrarUsuariosConHoras");
		
		//usuarios;
		$lista = $this->_obtenerUsuariosDeProyecto($cproyecto, $persona->carea);

		foreach ($lista as $key => $item)
			$item->horas = $this->obtenerPlanificacionPorPeriodoSemanaProyecto($cproyecto, $periodo, $semana, $item->user);		

		if($mostrarUsuariosConHoras == "true")
			$lista = $this->obtenerFiltrarUsuariosConHorasPlanificadas($lista);
		
		$obj["lista"] = $lista;

		// dd("jajajajajajaja",$lista);
		return $obj;
	}

	public function obtenerUsuariosDeArea(Request $request, $cproyecto)
	{
		$obj = [];	
		$cpersona = Auth::user()->cpersona;
		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();
		$carea = $persona->carea;

		//obtener periodo, semana
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");

		$grupo_areas = Tarea::where('carea','=',$carea)->first();

		// $tecnicas = [];

		// $areatecnicas = Tarea::where('carea_parent','=',$carea_parent->carea_parent)->get();

		// foreach ($areatecnicas as $value) {
		// 	array_push($tecnicas, $value->carea);
		// }
		// 	array_push($tecnicas,$carea_parent->carea_parent);
		
		// dd($carea_parent->grupo_areas);
		//usuarios;
		$lista = DB::table('tpersonadatosempleado')
			->join('tpersona', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->join('tusuarios', 'tusuarios.cpersona', '=', 'tpersona.cpersona')
			->join('tcategoriaprofesional','tcategoriaprofesional.ccategoriaprofesional','=','tpersonadatosempleado.ccategoriaprofesional')
			->join('tareas as ta','ta.carea','=','tpersonadatosempleado.carea')
			->select('tpersona.cpersona', 'tpersona.abreviatura as nombre', 'tusuarios.cusuario', 'tusuarios.nombre as user','tcategoriaprofesional.descripcion as profesion')
			->where('tusuarios.estado', "=", "ACT")			
			// ->whereIn('tpersonadatosempleado.carea',[$carea,$carea_parent->carea_parent])
			// ->whereIn('tpersonadatosempleado.carea',$tecnicas)
			// ->where('tpersonadatosempleado.carea', "=", $carea)
			->where('ta.grupo_areas','=',$grupo_areas->grupo_areas)
			->where('tpersona.cpersona', "!=", '0')
			->orderBy('tcategoriaprofesional.orden','ASC')
			->orderBy('tpersona.abreviatura','ASC')
			->get();

		// dd($lista);
		//obtener usuarios de proyecto para comparar
		$usuariosDeProyecto = $this->_obtenerUsuariosDeProyecto($cproyecto, $carea);

		foreach ($lista as $key => $item){
			$item->horas = $this->obtenerPlanificacionPorPeriodoSemanaProyecto($cproyecto, $periodo, $semana, $item->user);
			$item->usuarioProyecto = $this->_esUsuarioDeProyecto($usuariosDeProyecto, $item->cpersona);
		}

		$obj["lista"] = $lista;	
		return $obj;
	}

	// public function obtenerUsuariosDeGerencia(Request $request, $cproyecto)
	// {
	// 	$obj = [];	
	// 	$cpersona = Auth::user()->cpersona;
	// 	$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();
	// 	$carea = $persona->carea;

	// 	//obtener periodo, semana
	// 	$periodo = $request->input("periodo");
	// 	$semana = $request->input("semana");

	// 	$carea_parent = Tarea::where('carea','=',$carea)->first();
	// 	$tecnicas = [];
	// 	$areatecnicas = Tarea::where('carea_parent','=',$carea_parent->carea_parent)->get();
	// 	foreach ($areatecnicas as $value) {
	// 		array_push($tecnicas, $value->carea);
	// 	}
	// 		array_push($tecnicas,$carea_parent->carea_parent);
		
	// 	// dd($areasxgerencia);
	// 	//usuarios;
	// 	$lista = DB::table('tpersonadatosempleado')
	// 		->join('tpersona', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
	// 		->join('tusuarios', 'tusuarios.cpersona', '=', 'tpersona.cpersona')
	// 		->join('tcategoriaprofesional','tcategoriaprofesional.ccategoriaprofesional','=','tpersonadatosempleado.ccategoriaprofesional')
	// 		->select('tpersona.cpersona', 'tpersona.abreviatura as nombre', 'tusuarios.cusuario', 'tusuarios.nombre as user','tcategoriaprofesional.descripcion as profesion')
	// 		->where('tusuarios.estado', "=", "ACT")			
	// 		// ->whereIn('tpersonadatosempleado.carea',[54,53,52,82,51])
	// 		->whereIn('tpersonadatosempleado.carea',$tecnicas)
	// 		// ->where('tpersonadatosempleado.carea', "=", $carea)
	// 		->where('tpersona.cpersona', "!=", '0')
	// 		->orderBy('tcategoriaprofesional.orden','ASC')
	// 		->orderBy('tpersona.abreviatura','ASC')
	// 		->get();

	// 	// dd($lista);
	// 	//obtener usuarios de proyecto para comparar
	// 	$usuariosDeProyecto = $this->_obtenerUsuariosDeProyecto($cproyecto, $carea);

	// 	foreach ($lista as $key => $item){
	// 		$item->horas = $this->obtenerPlanificacionPorPeriodoSemanaProyecto($cproyecto, $periodo, $semana, $item->user);
	// 		$item->usuarioProyecto = $this->_esUsuarioDeProyecto($usuariosDeProyecto, $item->cpersona);
	// 	}

	// 	$obj["lista"] = $lista;	
	// 	return $obj;
	// }

	public function obtenerUltimasActualizaciones($cproyecto)
	{
		$lista = [];
		return $lista;
	}

	public function guardarHoras(Request $request)
	{
		// dd($request->all());
		$periodo = $request->input("periodo");
		$planificadas = $request->input("planificadas");
		$planificadas_anterior = $request->input("planificadas_anterior");
		$cproyecto = $request->input("proyecto");
		$semana = $request->input("semana");
		$usuario = $request->input("usuario");
		$total = $request->input("total");

		$horas = Planificacion::where("cproyecto", "=", $cproyecto)
				->where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)->first();
		// dd($horas);


		// dd($ejecutadas);

		$disponibles = 0;

		$dis = DB::table('tusuarios as us')
		->join('tpersonadatosempleado as tpe','tpe.cpersona','=','us.cpersona')
		->join('ttipocontrato as tc','tc.ctipocontrato','=','tpe.ctipocontrato')
		->where('us.nombre','=',$usuario)->first();

		if($horas == null){
			$horas = new Planificacion();
			$horas->periodo = $periodo;			
			$horas->cproyecto = $cproyecto;
			$horas->semana = $semana;
			$horas->usuario = $usuario;
			$horas->total = $dis->horas;			
		}		
		
		$horas->planificadas = $planificadas;
		$horas->save();

		//calcular disponibilidad
		$suma_planificadas = DB::table('planificacion')
                ->select(DB::raw('SUM(planificadas) as planificadas, SUM(planificadas_adm) as planificadas_adm'))                
				->where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)
                ->first();
		
		$horas_eje = Planificacion::where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)->get();
		$suma_eje = 0;
		foreach ($horas_eje as $eje) {
			$suma_eje += $eje->ejecutadas;
		}

		$ejecutadas = $suma_eje;

		$suma_plani = $suma_planificadas->planificadas+$suma_planificadas->planificadas_adm;
		
		$suma_horas=0;

		if ($suma_eje > $suma_plani) {
			$suma_horas = $suma_eje;
		}
		else {
			$suma_horas = $suma_plani;
		}
		

		$horas->disponibles = $disponibles = $horas->total - $suma_horas;


		$this->guardar_log($cproyecto, $periodo, $semana, $usuario, $horas->planificadas, Auth::user()->nombre);

		Planificacion::where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)
          		->update(['disponibles' => $disponibles]);

		// dd($suma_horas,$suma_eje,$suma_plani,$disponibles);

          		// dd($horas->disponibles);
		return $horas;
	}

	public function resumen(Request $request, $cpersona = null)
	{
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");

		$cpersona = Auth::user()->cpersona;
		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();
		$matriz = [];
		$proyectos_nombres = [];
		//obtener proyectos**
		$proyectos = $this->_obtenerListadoProyectos($proyectos_nombres);
		//obtener personas**
		$personas = $this->_obtenerUsuariosDeProyectos($proyectos, $persona->carea);		
		$this->_filtrarPersonasConHorasRegistradasEnPeriodoSemana($personas, $periodo, $semana);		
		//obtener horas planificadas **
		$arrayPersonaUsuario = [];
		$cab = $this->_obtenerCabeceras($personas, $arrayPersonaUsuario);				
		$arrayHorasDisponibles = [];
		$this->_obtenerHorasDisponibles_toArray($arrayPersonaUsuario, $arrayHorasDisponibles, $periodo, $semana);
		$matriz = $this->_inicializarMatriz($proyectos, $cab, $proyectos_nombres);
		$this->_agregarValorPersona($matriz, $personas, $proyectos, $cab);
		$total = $this->_agregarTotal($matriz);
		$totalV = $this->_agregarTotalV($matriz, count($cab));		
		
		return view("proyecto.planificacion.resumen", ["matriz" => $matriz, "cab" => $cab, "total" => $total, "totalV" => $totalV,
			"arrayHorasDisponibles" => $arrayHorasDisponibles ]);
	}

	//private 

	private function guardar_log($cproyecto, $periodo, $semana, $usuario, $horas, $usuario_registro)
	{
		$log = new Planificacion_log();
		$log->periodo = $periodo;			
		$log->cproyecto = $cproyecto;
		$log->semana = $semana;
		$log->mensaje = "Usuario $usuario_registro planifico $horas horas al usuario $usuario en la semana $semana del $periodo.";
		$log->save();
	}

	private function _obtenerUsuariosDeProyecto($cproyecto, $carea)
	{
		$grupo_areas = Tarea::where('carea','=',$carea)->first();

		// $tecnicas = [];

		// $areatecnicas = Tarea::where('carea_parent','=',$carea_parent->carea_parent)->get();

		// foreach ($areatecnicas as $value) {
		// 	array_push($tecnicas, $value->carea);
		// }
		// 	array_push($tecnicas,$carea_parent->carea_parent);

		$lista = DB::table('tproyectopersona')
			->join('tpersona', 'tproyectopersona.cpersona', '=', 'tpersona.cpersona')            
			->join('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->join('tusuarios', 'tusuarios.cpersona', '=', 'tpersona.cpersona')
			->join('tcategoriaprofesional', 'tcategoriaprofesional.ccategoriaprofesional', '=', 'tproyectopersona.ccategoriaprofesional')
			->join('tareas as ta','ta.carea','=','tpersonadatosempleado.carea')
			->select('tpersona.cpersona', 'tpersona.abreviatura as nombre', 'tusuarios.cusuario', 'tusuarios.nombre as user', 'tcategoriaprofesional.descripcion as profesion')
			//->where('tproyectopersona.eslider', "=", "0")
			->where('tusuarios.estado', "=", "ACT")	
			->where("tproyectopersona.cproyecto", "=", $cproyecto)
			// ->where('tpersonadatosempleado.carea', "=", $carea)
			// ->whereIn('tpersonadatosempleado.carea',[$carea,$carea_parent->carea_parent])
			// ->whereIn('tpersonadatosempleado.carea',$tecnicas)
			->where('ta.grupo_areas','=',$grupo_areas->grupo_areas)
			// ->orderBy('tproyectopersona.eslider','ASC')
			->orderBy('tcategoriaprofesional.orden','ASC')
			->orderBy('tpersona.abreviatura','ASC')
			->get();

		return $lista;
	}

	private function _obtenerUsuariosDeProyectos($proyectos, $carea)
	{
		$grupo_areas = Tarea::where('carea','=',$carea)->first();

		// $tecnicas = [];

		// $areatecnicas = Tarea::where('carea_parent','=',$carea_parent->carea_parent)->get();

		// foreach ($areatecnicas as $value) {
		// 	array_push($tecnicas, $value->carea);
		// }
		// 	array_push($tecnicas,$carea_parent->carea_parent);

		$lista = DB::table('tproyectopersona')
			->join('tpersona', 'tproyectopersona.cpersona', '=', 'tpersona.cpersona')            
			->join('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->join('tusuarios', 'tusuarios.cpersona', '=', 'tpersona.cpersona')
			->join('tareas as ta','ta.carea','=','tpersonadatosempleado.carea')
			->join('tcategoriaprofesional', 'tcategoriaprofesional.ccategoriaprofesional', '=', 'tpersonadatosempleado.ccategoriaprofesional')
			->select('tproyectopersona.cproyecto', 'tpersona.cpersona', 'tpersona.abreviatura as nombre', 'tusuarios.nombre as usuario')
			->whereIn("tproyectopersona.cproyecto", $proyectos)
			// ->where('tpersonadatosempleado.carea', "=", $carea)		
			// ->whereIn('tpersonadatosempleado.carea',[$carea,$carea_parent->carea_parent])	
			// ->whereIn('tpersonadatosempleado.carea',$tecnicas)
			->where('ta.grupo_areas','=',$grupo_areas->grupo_areas)
			->orderBy('tcategoriaprofesional.orden','ASC')
			->orderBy('tpersona.abreviatura','ASC')
			// ->orderBy('tusuarios.nombre','ASC')
			->get();

		foreach ($lista as $key => $obj) {
			$obj->cantidad = $this->_obtenerTotalHorasDisponibles($obj->cproyecto, $obj->usuario);
		}

		return $lista;
	}

	private function _esUsuarioDeProyecto($usuarios, $cpersona)
	{
		foreach ($usuarios as $key => $usuario) {
			if($usuario->cpersona == $cpersona)
				return true;
		}
		return false;
	}

	private function _obtenerTotalHorasDisponibles($cproyecto, $usuario)
	{
		$horas = DB::table('planificacion')
			->select(DB::raw('SUM(planificadas + planificadas_adm) as total'))
			->groupBy('usuario')
			->where("cproyecto", "=", $cproyecto)
			->where("usuario", "=", $usuario)
			->first();

		if($horas==null)
			return 0;

		return intval($horas->total);
	}

	private function _obtenerListadoProyectos(&$proyectos_nombres)
	{
		$proyectos_lista = [];
		$proyectos = DB::table('tproyectopersona')
			->join('tproyecto', 'tproyecto.cproyecto', '=', 'tproyectopersona.cproyecto')            
			->where("eslider", "=", "1")
			->where("cpersona", "=", Auth::user()->cpersona)
			->select('tproyectopersona.cproyecto', 'tproyecto.nombre')
			->orderBy('tproyecto.codigo','ASC')
			->get();

		foreach ($proyectos as $key => $proyecto){
			array_push($proyectos_lista, $proyecto->cproyecto);
			array_push($proyectos_nombres, $proyecto->nombre);
		}

		return $proyectos_lista;
	}

	private function _inicializarMatriz($proyectos, $personas, $proyectos_nombres)
	{
		$matriz = [];
		$i=0;
		foreach ($proyectos as $key => $proyecto) {
			$obj = [];
			//$obj["cproyecto"] = $proyecto;
			array_push($obj, $proyectos_nombres[$i]);
			foreach ($personas as $key => $persona) {
				//$obj[$persona] = 0;
				array_push($obj, 0);
			}
			array_push($matriz, $obj);
			$i++;
		}
		return $matriz;
	}

	private function _obtenerCabeceras($personas, &$arrayPersonaUsuario)
	{
		$cab = [];

		foreach ($personas as $key => $persona) {			
			if(!in_array($persona->nombre, $cab)){
				array_push($cab, $persona->nombre);
				array_push($arrayPersonaUsuario, $persona->usuario);
			}
		}

		return $cab;
	}

	private function _agregarValorPersona(&$matriz, &$personas, $proyectos, $cab)
	{
		foreach ($personas as $key => $persona) {
			$i = array_search($persona->cproyecto, $proyectos);
			$j = array_search($persona->nombre, $cab)+1;
			$matriz[$i][$j] = $persona->cantidad;
		}
	}

	private function _agregarTotal(&$matriz)
	{
		$total = [];

		for ($i=0; $i < count($matriz); $i++) { 
			
			$total_horizontal = 0;
			for ($j=1; $j < count($matriz[$i]); $j++)
				$total_horizontal += $matriz[$i][$j];

			array_push($total, $total_horizontal);
		}

		return $total;
	}

	private function _agregarTotalV(&$matriz, $cantidad)
	{
		$total_lista = [];		

		for ($i=0; $i < $cantidad; $i++)
			array_push($total_lista, 0);

		for ($i=0; $i < count($matriz); $i++) { 
			
			$total = 0;
			for ($j=1; $j < count($matriz[$i]); $j++)
				$total_lista[$j-1] += $matriz[$i][$j];
		}

		return $total_lista;
	}

	private function obtenerFiltrarUsuariosConHorasPlanificadas($lista){
		$listaTemp = [];

		foreach ($lista as $key => $usuario){
			if($usuario->horas != null){
				if($usuario->horas["planificadas"] > 0)
					array_push($listaTemp, $usuario);
			}
		}

		return $listaTemp;
	}

	private function filtrarProyectosConHorasRegistradas($lista, $periodo, $semana)
	{
		$listaTemp = [];

		foreach ($lista as $key => $item){
			$horas = planificacion::where('cproyecto','=', $item->cproyecto)
				->where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->sum('planificadas');

			if($horas > 0)
				array_push($listaTemp, $item);
		}

		return $listaTemp;
	}

	public function obtenerPlanificacionPorPeriodoSemanaProyecto($cproyecto, $periodo, $semana, $usuario)
	{
		//planificacion propia
		$horas = Planificacion::where("cproyecto", "=", $cproyecto)
				->where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)->first();

		if($horas != null){				
				//conversion necesaria*
				$horas->planificadas = floatval($horas->planificadas);
				$horas->planificadas_adm = floatval($horas->planificadas_adm);
				$horas->disponibles = floatval($horas->disponibles);
				$horas->total = floatval($horas->total);
				$horas->ejecutadas = floatval($horas->ejecutadas);
				$horas->ejecutadas_adm = floatval($horas->ejecutadas_adm);			
			return $horas;
		}

		//planificacion relacionada
		$horas = Planificacion::where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)->first();

		if($horas != null){
				//conversion necesaria*
				$horas->planificadas = floatval($horas->planificadas);
				$horas->planificadas_adm = floatval($horas->planificadas_adm);
				$horas->disponibles = floatval($horas->disponibles);
				$horas->total = floatval($horas->total);
				$horas->ejecutadas = floatval($horas->ejecutadas);
				$horas->ejecutadas_adm = floatval($horas->ejecutadas_adm);

			$horas->planificadas = 0;			
		// dd($horas);
			return $horas;
		}

		$dis = DB::table('tusuarios as us')
		->join('tpersonadatosempleado as tpe','tpe.cpersona','=','us.cpersona')
		->join('ttipocontrato as tc','tc.ctipocontrato','=','tpe.ctipocontrato')
		->where('us.nombre','=',$usuario)->first();

		if($dis != null){
			$dis->horas = floatval($dis->horas);
		}


		//buscar disponibles, ejecutadas*
		$objP = ["planificadas" => 0, "disponibles" => $dis->horas, "ejecutadas" => 0];
		return $objP;
	}

	private function _obtenerUsuarioDePersona($personas, $cpersona)
	{
		foreach ($personas as $key => $persona) {			
			if($persona->cpersona == $cpersona)
				return $persona->usuario;
		}
		return "";
	}

	private function _obtenerHorasDisponibles_toArray($arrayPersonaUsuario, &$arrayHorasDisponibles, $periodo, $semana)
	{
		//inicializar
		for ($i=0; $i < count($arrayPersonaUsuario); $i++)
			array_push($arrayHorasDisponibles, 0);		
		//completar
		for ($i=0; $i < count($arrayPersonaUsuario); $i++) { 
			$arrayHorasDisponibles[$i] = Planificacion::obtenerDisponibilidadPorPeriodoSemanaProyecto($periodo, $semana, $arrayPersonaUsuario[$i]);
		}
	}

	private function _filtrarPersonasConHorasRegistradasEnPeriodoSemana(&$personas, $periodo, $semana)
	{
		$personasTemp = [];
		$personasUsuarios = [];
		$usuariosPermitidos = [];
		$usuariosExcluir = [];
		
		for ($i=0; $i < count($personas) ; $i++) {
			
			if(in_array($personas[$i]->usuario, $usuariosExcluir) == true){
				$a;
			}
			else if(in_array($personas[$i]->usuario, $usuariosPermitidos) == true){
				array_push($personasTemp, $personas[$i]);
			}
			else {
				$usuario = $personas[$i]->usuario;
				$horas = Planificacion::where("periodo", "=", $periodo)->where("semana", "=", $semana)
					->where("usuario", "=", $usuario)->sum('planificadas');
				if($horas > 0){
					array_push($personasTemp, $personas[$i]);
					array_push($usuariosPermitidos, $personas[$i]->usuario);
				}
				else
					array_push($usuariosExcluir, $personas[$i]->usuario);
			}

		}

		$personas = $personasTemp;
	}


	public function horas_totales(Request $request, $usuario=null)
	{
		$periodo = $request->input("periodo");
		$semana = $request->input("semana");
		$cpersona = Auth::user()->cpersona;

		$persona = tpersonadatosempleado::where("cpersona", "=", $cpersona)->first();
		$carea = $persona->carea;

		// $carea_parent = Tarea::where('carea','=',$carea)->first();
		$grupo_areas = Tarea::where('carea','=',$carea)->first();

		$profesionales = DB::table('tpersonadatosempleado')
		->join('tusuarios', 'tusuarios.cpersona', '=', 'tpersonadatosempleado.cpersona')
		->join('tareas as ta','ta.carea','=','tpersonadatosempleado.carea')
		// ->whereIn('tpersonadatosempleado.carea',[$carea,$carea_parent->carea_parent])
		->where('ta.grupo_areas','=',$grupo_areas->grupo_areas)
		->get();

		// dd($profesionales);
		$hora_profesionales = [];
		
		foreach ($profesionales as $pro) {
		$horas_admin = 0;
		$horas_pro = 0;
		$horas_plani = 0;
		$horas_dis = 0;
		$horas_totales = [];
			
		$horas_tot =  Planificacion::
		//whereNull('cproyecto')
		where('usuario','=',$pro->nombre)
		->where('periodo','=',$periodo)
		->where('semana','=',$semana)
		// ->where('usuario','=','waldo.huallanca')
		->get();

		// dd($horas_tot);

		foreach ($horas_tot as $value) {
			if ($value->cproyecto == null) {
				$horas_admin += $value->ejecutadas;
			}else {
				$horas_pro += $value->ejecutadas;
			}
			$horas_plani = 0 + $value->planificadas;
			$horas_dis = 0 + $value->disponibles;
		}
		array_push($horas_totales, ['horas_proyecto'=>$horas_pro,'horas_admin'=>$horas_admin,'horas_plani'=>$horas_plani,'horas_dis'=>$horas_dis]);

		array_push($hora_profesionales,['persona'=>$pro->nombre, 'horas'=>$horas_totales]);
		}

		return $hora_profesionales;

	}

}
