<?php
namespace App\Http\Controllers\Proyecto;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Erp\HelperSIGSupport;
use Yajra\Datatables\Facades\Datatables;
use App\Erp\ActividadSupport;
use DB;
use Auth;
use App\Models\Tservicio;
use Session;
use Redirect;
use App\Models\Tproyectoactividade;
use App\Models\Tproyectoejecucion;
use App\Models\Tproyectoactividadeshabilitacion;
use App\Models\Tpersona;
use App\Models\Tpersonadatosempleado;
use App\Models\Tarea;


class ProyectoActividadesController extends Controller
{


     /* Estructura de Proyecto*/
    public function ActividadesProyecto(){
        Session::forget('pactividades');
        $actiParent= DB::table('tactividad')->whereNull('cactividad_parent')->get();
        $actividades = array();
        $objAct  = new ActividadSupport();
        foreach ($actiParent as $act) {
            $actividades[count($actividades)]=$act;
            $actividades=$objAct->listarActividad($actividades,$act);
        }
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;  

        return view('proyecto/proyectoactividades/proyectoactividades',compact('servicios','actividades'));
    }



    public function listarActividades(Request $request,$idProyecto){
        if (Session::get('pactividades')){
            $actividadesp= Session::get('pactividades');
            
        }else{
            if(strlen($idProyecto)> 0 && $idProyecto!='0'){
                $actividadesp=DB::table('tproyectoactividades')->where('cproyecto','=',$idProyecto)->get();
            }else{   
                $actividadesp=array();

            }
        }
        return view('partials.tableActividadPropuesta',compact('actividadesp'));        
    }


    public function editarActividadesProyecto(Request $request,$idProyecto){

        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();
        $servicio = Tservicio::lists('descripcion','cservicio')->all();
        $servicio = [''=>''] + $servicio;

        $user = Auth::user();
        $tarea = DB::table('tpersonadatosempleado as e')
        ->leftjoin('tareas as ta','e.carea','=','ta.carea')
        ->where('cpersona','=',$user->cpersona)
        ->whereNull('ftermino')
        ->orderBy('fingreso','DESC')
        ->first();
        $carea=0;
        $carea_parent=0;
        
        if($tarea){
            $carea = $tarea->carea;
            $carea_parent=$tarea->carea_parent;

        }

        $tproyectoactividadeshabilitacion=DB::table('tproyectoactividadeshabilitacion')
        ->where('cproyecto','=',$idProyecto)
        ->get();

        $activhabil=array();
      

        foreach ($tproyectoactividadeshabilitacion as $ph) {

          if($ph->activo=='1'){
            $chkhabilitado='1_'.$ph->cproyectoactividades.'_'.$ph->cpersona;
            $ah['activo']=$chkhabilitado;
            $activhabil[count($activhabil)] = $ah;
          }
          else{
            $ah['activo']='0';
          }
        }     

        $lparticipantes=DB::table('tproyectopersona as p')
        ->leftjoin('tproyecto as pr','p.cproyecto','=','pr.cproyecto')
        ->leftjoin('tpersona as per','p.cpersona','=','per.cpersona')
        ->leftjoin('tcategoriaprofesional as c','p.ccategoriaprofesional','=','c.ccategoriaprofesional')       
        ->join('tpersonadatosempleado as emp','p.cpersona','=','emp.cpersona')
        ->where('p.cproyecto','=',$idProyecto)
        ->where('emp.carea','=',$carea)  
        ->orderBy('c.orden','ASC')
        ->select('p.*','per.nombre','c.descripcion','per.abreviatura as usuario');
      
        $proyectopersona=DB::table('tproyectopersona as p')
        ->join('tpersonadatosempleado as e','p.cpersona','=','e.cpersona')    
        ->get();
        foreach ($proyectopersona as $pp) {
            if($pp->carea==$carea_parent){
                 $lparticipantes=$lparticipantes->orWhere('emp.carea','=',$carea_parent)
                  ->where('p.cproyecto','=',$idProyecto);
            }
        }
        $lparticipantes=$lparticipantes->get();   

        $participantes=array();
        foreach($lparticipantes as $par){

            $p['cproyectopersona'] = $par->cproyectopersona;
            $p['cproyecto'] =$par->cproyecto;
            $p['cpersona'] = $par->cpersona;
            $p['ccategoriaprofesional'] = $par->ccategoriaprofesional;
            $p['cdisciplina'] = $par->cdisciplina;
            $p['eslider'] = $par->eslider;
            $p['nombre'] = $par->nombre;
            $p['usuario'] =$par->usuario;
            $p['descripcion'] = $par->descripcion;           
            $participantes[count($participantes)] = $p;
        }         

        $lactividades = DB::table('tproyectoactividades as pact')
        ->leftjoin('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cproyecto','=',$proyecto->cproyecto)
        //->orderBy('pact.codigoactvidad','ASC')
        ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
        ->select('pact.*')
        ->get();
        $listaAct = array();

        //listado de tproyecto ejecucion

        $tproyectoejecucion = DB::table('tproyectoejecucion')
            ->where('cproyecto','=',$proyecto->cproyecto)   
            ->distinct('cproyectoactividades') 
            ->select('cproyectoactividades');

        //Tproyectoejecucion::where('cproyecto','=',$proyecto->cproyecto)->distinct('a.cproyectoactividades')->select('cproyectoactividades')->all();
        //dd($tproyectoejecucion);

        //Busco los padres por disting
        //$actividadPadre=DB::table('tproyectoactividades as a')
        //    ->join('tproyectoactividades as ap','a.cproyectoactividades','=','ap.cproyectoactividades_parent')
        //    ->where('a.cproyecto','=',$proyecto->cproyecto)   
        //    ->distinct('a.cproyectoactividades') 
        //    ->select('a.cproyectoactividades');      
        //    ->get();
        // realizo un union manteniendo el disting para no duplicar los padres en null parent    
        $actividadPadreTotal=DB::table('tproyectoactividades as a')
            ->where('a.cproyecto','=',$proyecto->cproyecto)    
            //->whereNull('a.cproyectoactividades_parent')
            ->select('a.cproyectoactividades')
            ->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
            //->union($actividadPadre)
            ->whereNotIn('cproyectoactividades', $tproyectoejecucion)       
            ->get();

        //Agrego los demas datos de titulo y descripcion    
        for ($i=0; $i < count($actividadPadreTotal); $i++) { 
            $actividadpadredatos = Tproyectoactividade::where('cproyectoactividades','=', $actividadPadreTotal[$i]->cproyectoactividades)->first();
            $habilitadoestado = Tproyectoactividadeshabilitacion::where('cproyectoactividades','=',$actividadpadredatos->cproyectoactividades)->count();
            //dd($habilitadoestado);
            $actividadPadreTotal[$i]->habilitadoestado = $habilitadoestado;
            $actividadPadreTotal[$i]->codigoactvidad = $actividadpadredatos->codigoactvidad;
            $actividadPadreTotal[$i]->codigopadre = $actividadpadredatos->cproyectoactividades_parent;
            $actividadPadreTotal[$i]->descripcionactividad = $actividadpadredatos->descripcionactividad;
            // $actividadPadreTotal[$i]->nombrehijo = substr($actividadpadredatos->codigoactvidad, -2)+1;
            // $actividadPadreTotal[$i]->nombreoriginal = $actividadpadredatos->codigoactvidad;
            // $actividadPadreTotal[$i]->ultimosdosvalores = substr($actividadpadredatos->codigoactvidad, -2);
            // $actividadPadreTotal[$i]->valorsiguiente = substr($actividadpadredatos->codigoactvidad, -2)+1;
            // $actividadPadreTotal[$i]->longitudpalabra = strlen($actividadpadredatos->codigoactvidad);
            // $actividadPadreTotal[$i]->restolongitud = strlen($actividadpadredatos->codigoactvidad)-2; 
            $nombreoriginal = $actividadpadredatos->codigoactvidad;
            $longitudpalabra = strlen($nombreoriginal);
            $sumandonbombredelhijo = Tproyectoactividade::where('cproyectoactividades_parent','=', $actividadpadredatos->cproyectoactividades)->count();
            $cantidadhijos = $sumandonbombredelhijo;
            $cantidadhijosmasuno = $sumandonbombredelhijo + 1;
            $actividadPadreTotal[$i]->cantidadhijos = $sumandonbombredelhijo;
            //Configuro el set del ultimo rellenado y planto el nuevo
            if($cantidadhijos == 0){
                if($longitudpalabra == 1){
                    $actividadPadreTotal[$i]->nombrehijonuevo = $actividadpadredatos->codigoactvidad.'.01';
                }elseif($longitudpalabra > 1){
                    if(substr($actividadpadredatos->codigoactvidad, -1) ==='.'){
                      $actividadPadreTotal[$i]->nombrehijonuevo = $actividadpadredatos->codigoactvidad.'01';  
                    }elseif(substr($actividadpadredatos->codigoactvidad, -2) ==='00'){
                      $actividadPadreTotal[$i]->nombrehijonuevo =  str_replace('.00', '.01', $actividadpadredatos->codigoactvidad);
                    }else{
                      $actividadPadreTotal[$i]->nombrehijonuevo = $actividadpadredatos->codigoactvidad.'.01';  
                    }
                }
            }else{
                if($longitudpalabra == 1){
                    $actividadPadreTotal[$i]->nombrehijonuevo = $actividadpadredatos->codigoactvidad.'.'.str_pad($cantidadhijosmasuno,2,'00', STR_PAD_LEFT);
                }elseif($longitudpalabra > 1){
                    if(substr($actividadpadredatos->codigoactvidad, -1) ==='.'){
                      $actividadPadreTotal[$i]->nombrehijonuevo = $actividadpadredatos->codigoactvidad.str_pad($cantidadhijosmasuno,2,'00', STR_PAD_LEFT);  
                    }elseif(substr($actividadpadredatos->codigoactvidad, -2) ==='00'){
                        $actividadPadreTotal[$i]->nombrehijonuevo = substr($actividadpadredatos->codigoactvidad, 0, -2).str_pad($cantidadhijosmasuno,2,'00', STR_PAD_LEFT);
                    }else{
                      $actividadPadreTotal[$i]->nombrehijonuevo = $actividadpadredatos->codigoactvidad.'.'.str_pad($cantidadhijosmasuno,2,'00', STR_PAD_LEFT);
                    }
                }
            }
            
        }
        //dd($actividadPadreTotal[12]->habilitaequipo);

        foreach($lactividades as $act){
            $a['cproyectoactividades'] = $act->cproyectoactividades;
            $a['item'] =$act->codigoactvidad;
            $a['cproyecto'] = $act->cproyecto;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactvidad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cproyectosub'] = $act->cproyectosub;
            $a['cactividad_parent'] = $act->cproyectoactividades_parent;
            $existoenhabilitado = Tproyectoactividadeshabilitacion::where('cproyectoactividades','=',$act->cproyectoactividades)->count();
            $estadohabilitado = Tproyectoactividadeshabilitacion::where('cproyectoactividades','=',$act->cproyectoactividades)->where('activo','=','1')->count();
            $a['existoenhabilitados'] = $existoenhabilitado;
            $a['estadohabilitado'] = $estadohabilitado;

            //Agrego equipo de trabajo por actividad solo para ver
            $habilitaequipo = Tproyectoactividadeshabilitacion::where('cproyectoactividades','=',$act->cproyectoactividades)->get();
            $a['equipotrabajo'] = $habilitaequipo;
            //dd($a['equipotrabajo']);
             
             for ($e=0; $e < count($habilitaequipo); $e++) { 
                if(count($a['equipotrabajo'][$e]) > 0){
                    if (!is_null($a['equipotrabajo'][$e]->cpersona)) {
                        $cpersonaequipo = Tpersona::where('cpersona','=',$a['equipotrabajo'][$e]->cpersona)->first();
                        $tdatosempleado = Tpersonadatosempleado::where('cpersona','=',$habilitaequipo[$e]->cpersona)->first();
                        $tarea = Tarea::where('carea','=',$tdatosempleado->carea)->first();
                        $a['equipotrabajo'][$e]['nombreemple']= $cpersonaequipo->abreviatura; 
                        $a['equipotrabajo'][$e]['areaemple'] = $tarea->descripcion;
                    }    
                }
             }
            
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        //$listaAct = $objAct->orderParentActividadEjecucion($listaAct);
        //dd($listaAct[6]->equipotrabajo);
        //dd($listaAct[6]['equipotrabajo']);

        return view('proyecto/proyectoactividades/proyectoactividades',compact('proyecto','persona','editar','uminera','servicio','listaAct','participantes','chkacthabi','chkhabilitado','activhabil','actividadPadreTotal'));
    }


    public function CreoActividadProyecto(Request $request)
    {
        //dd(!empty($request->padreactividad));
        //Creo una nueva actividad para un proyecto 
        $tactividadproyecto = new Tproyectoactividade;
        $tactividadproyecto->cproyecto = $request->cproyecto;
        $tactividadproyecto->codigoactvidad = $request->itemactividadcreate;
        $tactividadproyecto->descripcionactividad = $request->nombreactividad;
        //valido si viene el padre si viene lo creo
        if(!empty($request->padreactividad)){
            $tactividadproyecto->cproyectoactividades_parent = $request->padreactividad;    
        };
        $tactividadproyecto->save();
        //return redirect()->route('editarActividadesProyecto',$request->cproyecto);
        //return view('editarActividadesProyecto',$request->cproyecto);
        return Redirect::route('editarActividadesProyecto',$request->cproyecto);
    }    
    
    public function ModificoActividadProyecto(Request $request)
    {
        //dd($request->all());
        //Edito contenido 
        $idProyecto = $request->cproyecto;
        $editoactividad = Tproyectoactividade::where('cproyectoactividades','=',$request->cproyectoactividades)->first();
        $editoactividad->codigoactvidad = $request->item;
        $editoactividad->descripcionactividad = $request->decripcionactividad;
        $editoactividad->update();
        //$data = 'hello world';
        return Redirect::route('editarActividadesProyecto',$request->cproyecto);
        //return response()->json($request->cproyecto);

        //$data = $this->editarActividadesProyecto($request->cproyecto);
        //die();

        //return redirect('int?url=ActividadesProyecto');
        //return $data;// Redirect::route('editarActividadesProyecto',$request->cproyecto);
        //return Redirect::route('ActividadesProyecto', $request->cproyecto);


    }

    // public function editarActProyecto($cproyectoactividades)
    // {
    //     $proyecto = Tproyectoactividade::where('cproyectoactividades','=',$request->cproyectoactividades)->first();

    //     return view('partials/editActProyecto',compact('$proyecto'));
    // }

    public function estadotproyectoactividad($id)
    {
        $tproactividades = Tproyectoactividade::where('cproyectoactividades','=',$id)->first();
        $thijosactividadeshabilitado = Tproyectoactividadeshabilitacion::where('cproyectoactividades','=',$id)->get();
        $estadohabilitado = Tproyectoactividadeshabilitacion::where('cproyectoactividades','=',$id)->where('activo','=','1')->count();

        if ($estadohabilitado >= 1 ) {
             
            for ($i=0; $i < count($thijosactividadeshabilitado); $i++) { 
               $actthabilitado = Tproyectoactividadeshabilitacion::where('cproyacthab','=', $thijosactividadeshabilitado[$i]->cproyacthab)->first();
               $actthabilitado->activo = '0';
               $actthabilitado->update();
            }
        }else{
            for ($i=0; $i < count($thijosactividadeshabilitado); $i++) { 
               $actthabilitado = Tproyectoactividadeshabilitacion::where('cproyacthab','=', $thijosactividadeshabilitado[$i]->cproyacthab)->first();
               $actthabilitado->activo = '1';
               $actthabilitado->update();
            }   
        }

        return Redirect::route('editarActividadesProyecto',$tproactividades->cproyecto);
    }
}