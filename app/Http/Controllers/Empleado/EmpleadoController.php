<?php

namespace App\Http\Controllers\Empleado;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tpersonadatosempleado;
use App\Models\Tpersonaotrosdatosempleado;
use App\Models\Tpersonatelefono;
use App\Models\Tpersonadatosfamiliare;
use App\Models\Tpersonaestudio;
use App\Models\Tpersonadatosmedico;
use App\Models\Tpersonadatosmedicosalergium;
use App\Models\Tpersonadatosmedicosenfermedad;
use App\Models\Tpai;
use App\Models\Tubigeo;
use App\Models\Tpersonanaturalinformacionbasica;
use App\Models\Tcatalogo;
use App\Models\Tespecialidad;
use App\Models\Tinstitucion;
use App\Models\Tenfermedad;
use App\Models\Talergia;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;

class EmpleadoController extends Controller
{
	//
	public function listarEmpleadosRDP(){
		return Datatables::queryBuilder(
			DB::table('tpersona')
			->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
			->join('tpersonadatosempleado as emp','tpersona.cpersona','=','emp.cpersona')
			->select('tpersona.cpersona','emp.codigosig as codigo','tpersona.nombre as nombre',DB::raw('CONCAT(\'rowrdp_\',tpersona.cpersona)   as "DT_RowId"'))
			->where('tpersona.ctipopersona','=','NAT')
			->where('tnat.esempleado','=','1')
			)->make(true); 
	}

	public function listadoEmpleado(){

		return view('ghumana.listadoEmpleados');

	}
	public function listarEmpleado(){
		return Datatables::queryBuilder(DB::table('tpersona as tper')
			->join('tpersonanaturalinformacionbasica as tnat','tper.cpersona','=','tnat.cpersona')
			->leftJoin('tpersonadatosempleado as tot','tper.cpersona','=','tot.cpersona')
			->leftJoin('tareas as are','tot.carea','=','are.carea')
			->where('tnat.esempleado','=','1')
			// ->orderBy('tnat.apaterno','asc')
			// ->orderBy('tnat.amaterno','asc')
			// ->orderBy('tnat.nombres','asc')
			->select('tper.cpersona','tper.identificacion','tnat.apaterno','tnat.amaterno','tnat.nombres','are.descripcion','tot.estado'
				,DB::raw('CONCAT(\'row_\',tper.cpersona)  as "DT_RowId"'))
			)->make(true);        
	}
	public function registroEmpleado(){
		$tpais = Tpai::lists('descripcion','cpais')->all();
		$tpais = [''=>''] + $tpais; 

		$tpais_nac = Tpai::lists('descripcion','cpais')->all();
		$tpais_nac = [''=>''] + $tpais_nac;    

		$testadocivil = Tcatalogo::where('codtab','=','00015')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$testadocivil = [''=>''] + $testadocivil;   

		$tgenero = Tcatalogo::where('codtab','=','00016')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tgenero = [''=>''] + $tgenero; 

		$tparentesco = Tcatalogo::where('codtab','=','00018')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tparentesco = [''=>''] + $tparentesco;

		$tnivel = Tcatalogo::where('codtab','=','00019')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tnivel = [''=>''] + $tnivel;

		$tcarrera = DB::table('tespecialidad')
		->lists('descripcion','cespecialidad');
		$tcarrera = [''=>''] + $tcarrera;

		$tinstitucion = DB::table('tinstitucion')
		->lists('descripcion','cinstitucion');
		$tinstitucion = [''=>''] + $tinstitucion;

		$testado= Tcatalogo::where('codtab','=','00022')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$testado = [''=>''] + $testado;

		$tgrupo_san = Tcatalogo::where('codtab','=','00023')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tgrupo_san = [''=>''] + $tgrupo_san;  

		$tenfermedad = DB::table('tenfermedad')
		->lists('descripcion','cenfermedad');
		$tenfermedad = [''=>''] + $tenfermedad;  

		$talergia = DB::table('talergias')
		->lists('descripcion','calergias');
		$talergia = [''=>''] + $talergia;    
		

		return view('ghumana.empleado',compact('tpais','tpais_nac','testadocivil','tgenero','tparentesco','tnivel','tcarrera','tinstitucion','testado','tgrupo_san','tenfermedad','talergia'));
	}
	public function editarEmpleado(Request $request,$idEmpleado){
		$persona = DB::table('tpersona')
		->where('cpersona','=',$idEmpleado)
		->first();
		$personanat = DB::table('tpersonanaturalinformacionbasica')
		->where('cpersona','=',$idEmpleado)
		->first();

		$personaemp = DB::table('tpersonadatosempleado')
		->where('cpersona','=',$idEmpleado)
		->first();
		$personafam = DB::table('tpersonadatosfamiliares')
		->where('cpersona','=',$idEmpleado)
		->get();

		$personamed = DB::table('tpersonadatosmedicos')
		->where('cpersona','=',$idEmpleado)
		->get();

		$personamedaler = DB::table('tpersonadatosmedicosalergia')
		->where('cpersona','=',$idEmpleado)
		->get();

		$personamedenf = DB::table('tpersonadatosmedicosenfermedad')
		->where('cpersona','=',$idEmpleado)
		->get();

		$personamedest = DB::table('tpersonaestudios')
		->where('cpersona','=',$idEmpleado)
		->get();    

		$personaotremp = DB::table('tpersonaotrosdatosempleados')
		->where('cpersona','=',$idEmpleado)
		->get();                

		$personatel_do = DB::table('tpersonatelefonos')
		->where('ctipotelefono','=','DOM')
		->where('cpersona','=',$idEmpleado)
		->first();   

		$personatel_eme = DB::table('tpersonatelefonos')
		->where('ctipotelefono','=','EME')
		->where('cpersona','=',$idEmpleado)
		->first();   

		$personatel_cel = DB::table('tpersonatelefonos')
		->where('ctipotelefono','=','CEL')
		->where('cpersona','=',$idEmpleado)
		->first();   

		$personatel_con = DB::table('tpersonatelefonos')
		->where('ctipotelefono','=','CON')
		->where('cpersona','=',$idEmpleado)
		->first();   

		$personadir = DB::table('tpersonadirecciones')
		->where('cpersona','=',$idEmpleado)
		->where('ctipodireccion','=','DO')
		->first();

		$personadir_ema = DB::table('tpersonadirecciones')
		->where('cpersona','=',$idEmpleado)
		->where('ctipodireccion','=','MA')
		->first();

		$personadir_nac = DB::table('tpersonadirecciones')
		->where('cpersona','=',$idEmpleado)
		->where('ctipodireccion','=','NA')
		->first();                

		$tparentesco = Tcatalogo::where('codtab','=','00018')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tparentesco = [''=>''] + $tparentesco;

		$testadocivil = Tcatalogo::where('codtab','=','00015')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$testadocivil = [''=>''] + $testadocivil;

		$tgenero = Tcatalogo::where('codtab','=','00016')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tgenero = [''=>''] + $tgenero;

		$tnivel = Tcatalogo::where('codtab','=','00019')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tnivel = [''=>''] + $tnivel;

		$tcarrera = DB::table('tespecialidad')
		->lists('descripcion','cespecialidad');
		$tcarrera = [''=>''] + $tcarrera;

		$tinstitucion = DB::table('tinstitucion')
		->lists('descripcion','cinstitucion');
		$tinstitucion = [''=>''] + $tinstitucion;

		$testado= Tcatalogo::where('codtab','=','00022')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$testado = [''=>''] + $testado;

		$tgrupo_san = Tcatalogo::where('codtab','=','00023')
		->whereNotNull('digide')
		->lists('descripcion','digide')->all();  
		$tgrupo_san = [''=>''] + $tgrupo_san;  

		$tenfermedad = DB::table('tenfermedad')
		->lists('descripcion','cenfermedad');
		$tenfermedad = [''=>''] + $tenfermedad;  

		$talergia = DB::table('talergias')
		->lists('descripcion','calergias');
		$talergia = [''=>''] + $talergia;     

		$cubigeo="000000";
		$cpais=0;
		$cdpto="000000";
		$cprov="000000";

		$cubigeo_nac="000000";
		$cpais_nac=0;
		$cdpto_nac="000000";
		$cprov_nac="000000";

		if($personadir){
			$cpais = $personadir->cpais;
			$cubigeo = $personadir->cubigeo;
			if(strlen($cubigeo)==6){
				$cdpto=substr($cubigeo,0,2)."0000";
				$cprov=substr($cubigeo,0,4)."00";

			}
		}

		if($personadir_nac){
			$cpais_nac = $personadir_nac->cpais;
			$cubigeo_nac = $personadir_nac->cubigeo;
			if(strlen($cubigeo_nac)==6){
				$cdpto_nac=substr($cubigeo_nac,0,2)."0000";
				$cprov_nac=substr($cubigeo_nac,0,4)."00";

			}
		}

		$tpais = Tpai::lists('descripcion','cpais')->all();
		$tpais = [''=>''] + $tpais;        
		$tdpto = Tubigeo::where('cpais','=',$cpais)
		->where(DB::raw('substr(cubigeo,3,4)'),'=','0000')
		->lists('descripcion','cubigeo')->all();
		$tdpto = [''=>''] + $tdpto;

		$tpais_nac = Tpai::lists('descripcion','cpais')->all();
		$tpais_nac = [''=>''] + $tpais_nac;        
		$tdpto_nac = Tubigeo::where('cpais','=',$cpais_nac)
		->where(DB::raw('substr(cubigeo,3,4)'),'=','0000')
		->lists('descripcion','cubigeo')->all();

		$tdpto_nac = [''=>''] + $tdpto_nac;

		$tprov = Tubigeo::where('cpais','=',$cpais)
		->where(DB::raw('substr(cubigeo,5,2)'),'=','00')
		->where(DB::raw('substr(cubigeo,1,2)'),'=',substr($cdpto,0,2))
		->where(DB::raw('substr(cubigeo,3,2)'),'!=','00')        
		->lists('descripcion','cubigeo')->all();
		$tprov = [''=>''] + $tprov;

		$tprov_nac = Tubigeo::where('cpais','=',$cpais_nac)
		->where(DB::raw('substr(cubigeo,5,2)'),'=','00')
		->where(DB::raw('substr(cubigeo,1,2)'),'=',substr($cdpto_nac,0,2))
		->where(DB::raw('substr(cubigeo,3,2)'),'!=','00')        
		->lists('descripcion','cubigeo')->all();
		$tprov_nac = [''=>''] + $tprov_nac;        



		$tdis = Tubigeo::where('cpais','=',$cpais)
		->where(DB::raw('substr(cubigeo,1,4)'),'=',substr($cprov,0,4))
		->where(DB::raw('substr(cubigeo,5,2)'),'!=','00')        
		->lists('descripcion','cubigeo')->all();                
		$tdis = [''=>''] + $tdis;

		$tdis_nac = Tubigeo::where('cpais','=',$cpais_nac)
		->where(DB::raw('substr(cubigeo,1,4)'),'=',substr($cprov_nac,0,4))
		->where(DB::raw('substr(cubigeo,5,2)'),'!=','00')        
		->lists('descripcion','cubigeo')->all();                
		$tdis_nac = [''=>''] + $tdis_nac;

		/* Datos De Familiares */
		/*$tpersonadatosfamiliare = DB::table('tpersonadatosfamiliares')
		->where('cpersona','=',$idEmpleado)
		->orderBy('apaterno','ASC')
		->orderBy('amaterno','ASC')
		->orderBy('nombres','ASC')
		->get();*/
		 Session::forget('listFamEmp');  
		$tpersonadatosfamiliare=array();

		$tpersonadatosfamiliares = DB::table('tpersonadatosfamiliares as tf')
		->leftJoin('tcatalogo as tc','tf.parentesco','=','tc.digide')
		->leftJoin('tcatalogo as tc1','tf.genero','=','tc1.digide')
		->where('tc.codtab','=','00018')
		->where('tc1.codtab','=','00016')
		->where('cpersona','=',$idEmpleado)	
		->orderBy('tf.apaterno','ASC')
		->orderBy('tf.amaterno','ASC')
		->orderBy('tf.nombres','ASC')		
		->select('tf.cpersonafamiliar','tf.parentesco','tf.apaterno','tf.amaterno','tf.nombres','tf.genero','tf.identificacion','tc.descripcion as desparentesco','tc1.descripcion as desgenero')
		->get();

		foreach($tpersonadatosfamiliares as $tpf){
			$fae['cpersonafamiliar']= $tpf->cpersonafamiliar;
	        $fae['identificacion']= $tpf->identificacion;
	        $fae['apaterno']= $tpf->apaterno;
	        $fae['amaterno']= $tpf->amaterno;
	        $fae['nombres']= $tpf->nombres;
	        $fae['parentesco_fam']= $tpf->parentesco;
	        $fae['desparentesco']= $tpf->desparentesco;
	        $fae['genero_fam'] = $tpf->genero; 
	        $fae['desgenero'] = $tpf->desgenero;     
	        $tpersonadatosfamiliare[count($tpersonadatosfamiliare)]=$fae;

		}
		Session::put('listFamEmp',$tpersonadatosfamiliare);	


		/* Fin Datos De Familiares */

		Session::forget('listEstEmp');  
		$tpersonadatosestudio=array();

		$tpersonadatosestudios = DB::table('tpersonaestudios as p')
		->leftJoin('tcatalogo as tc','p.nivel','=','tc.digide')
		->leftJoin('tcatalogo as tc1','p.estado','=','tc1.digide')	
		->leftJoin('tespecialidad as e','p.cespecialidad','=','p.cespecialidad')
		->leftJoin('tinstitucion as i','p.cinstitucion','=','i.cinstitucion')
		->where('tc.codtab','=','00019')
		->where('tc1.codtab','=','00022')	
		->where('p.cpersona','=',$idEmpleado)
		->orderBy('p.nivel','ASC')
		->orderBy('p.cespecialidad','ASC')
		->orderBy('p.cinstitucion','ASC')
		->orderBy('p.estado','ASC')
		->select('p.*','tc.descripcion as descnivel','e.descripcion as descespecialidad','i.descripcion as descinstitucion','tc1.descripcion as descestado')
		->get();

		foreach($tpersonadatosestudios as $tpe){
			$est['cpersonaestudios']= $tpe->cpersonaestudios;
	        $est['nivel']= $tpe->nivel;
	        $est['descnivel']= $tpe->descnivel;
	        $est['cespecialidad']= $tpe->cespecialidad;
	        $est['descespecialidad']= $tpe->descespecialidad;
	        $est['cinstitucion']= $tpe->cinstitucion;
	        $est['descinstitucion']= $tpe->descinstitucion;
	        $est['estado'] = $tpe->estado; 
	        $est['descestado'] = $tpe->descestado;   
	        $est['colegiatura']= $tpe->colegiatura;
	        $est['fingreso']= $tpe->fingreso;
	        $est['ftermino'] = $tpe->ftermino; 
	        $tpersonadatosestudio[count($tpersonadatosestudio)]=$est;
		}

		Session::put('listEstEmp',$tpersonadatosestudio);


		Session::forget('listEnfEmp');  
		$tpersonadatosmedicosenfermedad=array();

		$tpersonadatosmedicosenfermedades = DB::table('tpersonadatosmedicosenfermedad as tpe')
		->leftJoin('tenfermedad as te','te.cenfermedad','=','tpe.cenfermedad')	
		->where('cpersona','=',$idEmpleado)
		->orderBy('cenfermedad','ASC')
		->select('te.descripcion as descenfermedad','tpe.observacion','tpe.cpersonaenfermedad','tpe.cenfermedad')
		->get();

		foreach($tpersonadatosmedicosenfermedades as $tenf){
			$enf['cpersonaenfermedad']= $tenf->cpersonaenfermedad;
			$enf['descenfermedad']= $tenf->descenfermedad;
	        $enf['observacion']= $tenf->observacion;	        
	        $enf['cenfermedad']= $tenf->cenfermedad;	        
	        $tpersonadatosmedicosenfermedad[count($tpersonadatosmedicosenfermedad)]=$enf;
		}

		Session::forget('listAlerEmp');  
		$tpersonadatosmedicosalergia=array();

		$tpersonadatosmedicosalergias = DB::table('tpersonadatosmedicosalergia as tpa')
		->leftJoin('talergias as ta','tpa.calergias','=','ta.calergias')
		->where('cpersona','=',$idEmpleado)
		->orderBy('calergias','ASC')
		->select('ta.descripcion as descalergia','tpa.observacion','tpa.calergias','tpa.cpersonaalergia')
		->get();

		foreach($tpersonadatosmedicosalergias as $tpa){
			$enf['cpersonaalergia']= $tpa->cpersonaalergia;
			$enf['descalergia']= $tpa->descalergia;
	        $enf['observacion']= $tpa->observacion;	        
	        $enf['calergias']= $tpa->calergias;	        
	        $tpersonadatosmedicosalergia[count($tpersonadatosmedicosalergia)]=$enf;
		}

		return view('ghumana.empleado',compact('persona','personanat','personaemp','personafam','personamed','personamedaler','personamedenf','personamedest','personaotremp','personatel_do','personatel_eme','personatel_con','personatel_cel','personadir','personadir_ema','personadir_nac','tpais','tdpto','tprov','tdis','testadocivil','tgenero','tpais_nac','tdpto_nac','tprov_nac','tdis_nac','cpais_nac','cubigeo_nac','cdpto_nac','cprov_nac','cpais','cdpto','cprov','cubigeo','tparentesco','tnivel','tcarrera','tinstitucion','testado','tgrupo_san','tenfermedad','talergia','tpersonadatosfamiliares','tpersonadatosestudios','tpersonadatosmedicosenfermedades','tpersonadatosmedicosalergias'));
	}
	public function grabarEmpleado(Request $request){
		
		$idEmpleado = $request->input('cpersona');
		if(strlen($idEmpleado)<=0){
			$persona = new Tpersona();
			$persona->ctipopersona='NAT';
			$persona->ctipoidentificacion ='2';
			$persona->identificacion =$request->input('identificacion');			
			$persona->abreviatura =$request->input('abreviatura');			
			$persona->ctipoidentificacion ='2';
			$persona->save();
			$idEmpleado = $persona->cpersona;
		}else{
			$persona = Tpersona::where('cpersona','=',$idEmpleado)->first();
		}
		$persona->nombre = $request->input('apaterno') . ' ' .$request->input('amaterno') . ' '. $request->input('nombres');

		$persona->save();
		/* Persona Direcciones*/

		$personadir = Tpersonadireccione::where('cpersona','=',$idEmpleado)
			->where('ctipodireccion','=','DO')
			->first();
		$cnt=0;
		if(!$personadir){
			$cnt = Tpersonadireccione::where('cpersona','=',$idEmpleado)->count();
			$personadir = new Tpersonadireccione();
			$personadir->numerodireccion=++$cnt;
			$personadir->ctipodireccion='DO';
			$personadir->cpersona = $idEmpleado;
		}

		$personadir->cpais=$request->input("pais");
		if($request->input("dis") && $request->input("dis")!=''){
			$personadir->cubigeo = $request->input("dis");
		}
		$personadir->direccion = $request->input('direccion');
		$personadir->referencia = $request->input('referencia');
		$personadir->save();
		// Nacimiento
		$personadir_nac = Tpersonadireccione::where('cpersona','=',$idEmpleado)
			->where('ctipodireccion','=','NA')
			->first();
		if(!$personadir_nac){
			$cnt = Tpersonadireccione::where('cpersona','=',$idEmpleado)->count();
			$personadir_nac = new Tpersonadireccione();
			$personadir_nac->numerodireccion=++$cnt;
			$personadir_nac->ctipodireccion='NA';
			$personadir_nac->cpersona = $idEmpleado;
		}

		$personadir_nac->cpais=$request->input("pais_nac");

		if($request->input("dis_nac") && $request->input("dis_nac")!=''){
			$personadir_nac->cubigeo = $request->input("dis_nac");
		}
		$personadir_nac->direccion = '';
		$personadir_nac->referencia = '';
		$personadir_nac->save();


		// Email
		$personadir_ema = Tpersonadireccione::where('cpersona','=',$idEmpleado)
			->where('ctipodireccion','=','MA')
			->first();
		if(!$personadir_ema){
			$cnt = Tpersonadireccione::where('cpersona','=',$idEmpleado)->count();
			$personadir_ema = new Tpersonadireccione();
			$personadir_ema->numerodireccion=++$cnt;
			$personadir_ema->ctipodireccion='MA';
			$personadir_ema->cpersona = $idEmpleado;
		}

		$personadir_ema->direccion = $request->input('personadir_ema');
		$personadir_ema->referencia = '';
		$personadir_ema->save();

		/* Fin Persona Direcciones*/

		/* Persona natural*/
		$personanat = Tpersonanaturalinformacionbasica::where('cpersona','=',$idEmpleado)->first();
		if(!$personanat){
			$personanat = new Tpersonanaturalinformacionbasica();
			$personanat->cpersona = $idEmpleado;    

		}

		$personanat->apaterno = $request->input('apaterno');
		$personanat->amaterno = $request->input('amaterno');
		$personanat->nombres = $request->input('nombres');
		$personanat->esempleado='1';
		if( strlen($request->input('estadocivil')) > 0){
			$personanat->estadocivil = $request->input('estadocivil');
		}
		if( strlen($request->input('genero')) > 0){
			$personanat->genero = $request->input('genero');
		}
		if( strlen($request->input('fnacimiento')) > 0){
			$personanat->fnacimiento = $request->input('fnacimiento');
		}        
		$personanat->ctipoidentificacion=$persona->ctipoidentificacion;
		$personanat->identificacion=$persona->identificacion;
		if( strlen($request->input('pais_nac')) > 0){
			$personanat->cnacionalidad = $request->input('pais_nac');
		}
		$personanat->save();

		/* Fin Persona natural*/

		/* Persona Telefonos*/
		//domicilio
		$personatel_do = Tpersonatelefono::where('cpersona','=',$idEmpleado)
		->where('ctipotelefono','=','DOM')
		->first();
		$cnt=0;
		if(!$personatel_do){
			$cnt = Tpersonatelefono::where('cpersona','=',$idEmpleado)->count();
			$personatel_do = new Tpersonatelefono();
			$personatel_do->cpersona = $idEmpleado;
			$personatel_do->snumerotelefono = ++$cnt;
			$personatel_do->ctipotelefono = 'DOM';

		}
		$personatel_do->telefono = $request->input('personatel_do');
		$personatel_do->save();

		//celular
		$personatel_cel = Tpersonatelefono::where('cpersona','=',$idEmpleado)
		->where('ctipotelefono','=','CEL')
		->first();
		$cnt=0;
		if(!$personatel_cel){
			$cnt = Tpersonatelefono::where('cpersona','=',$idEmpleado)->count();
			$personatel_cel = new Tpersonatelefono();
			$personatel_cel->snumerotelefono = ++$cnt;
			$personatel_cel->ctipotelefono = 'CEL';
			$personatel_cel->cpersona = $idEmpleado;

		}
		$personatel_cel->telefono = $request->input('personatel_cel');
		$personatel_cel->save();        

		//emergencia
		$personatel_eme = Tpersonatelefono::where('cpersona','=',$idEmpleado)
		->where('ctipotelefono','=','EME')
		->first();
		$cnt=0;
		if(!$personatel_eme){
			$cnt = Tpersonatelefono::where('cpersona','=',$idEmpleado)->count();
			$personatel_eme = new Tpersonatelefono();
			$personatel_eme->snumerotelefono = ++$cnt;
			$personatel_eme->ctipotelefono = 'EME';
			$personatel_eme->cpersona = $idEmpleado;
		}
		$personatel_eme->telefono = $request->input('personatel_eme');
		$personatel_eme->save();  

		//contacto
		$personatel_con = Tpersonatelefono::where('cpersona','=',$idEmpleado)
		->where('ctipotelefono','=','CON')
		->first();
		$cnt=0;
		if(!$personatel_con){
			$cnt = Tpersonatelefono::where('cpersona','=',$idEmpleado)->count();
			$personatel_con = new Tpersonatelefono();
			$personatel_con->snumerotelefono = ++$cnt;
			$personatel_con->ctipotelefono = 'CON';
			$personatel_con->cpersona = $idEmpleado;
		}
		$personatel_con->telefono = $request->input('personatel_con');
		$personatel_con->save();                  
		/* Persona Telefonos*/

		/*Inicio Persona Familiar*/
		$listaFam=Session::get('listFamEmp');
		

		foreach ($listaFam as $lf ) {
			if($lf['cpersonafamiliar']<=0){
				$famemp = new Tpersonadatosfamiliare();
				$famemp->cpersona=$idEmpleado;
				$famemp->ctipoidentificacion= '2';
			}else{
				$famemp = Tpersonadatosfamiliare::where('cpersonafamiliar','=',$lf['cpersonafamiliar'])
				->first();
			}
		
dd($listaFam,$famemp);
			$famemp->identificacion= $lf['identificacion'];
			$famemp->apaterno= $lf['apaterno'];
			$famemp->amaterno= $lf['amaterno'];
			$famemp->nombres= $lf['nombres'];
			$famemp->parentesco= $lf['parentesco_fam'];
			$famemp->genero= $lf['genero_fam'];

			dd($listaFam,$famemp);
			$famemp->save();
		}



		/*Fin Persona Familiar*/


		/*Inicio Persona Estudios*/
		/*Fin Persona Estudios*/

		/*Inicio Persona Medicos*/
		/*Fin Persona Medicos*/


		DB::commit();

		return Redirect::route('editarEmpleado',$idEmpleado);
	}
	public function saveFamEmpleado(Request $request){
		
		$tpersonadatosfamiliare= array();

		if(Session::get('listFamEmp')){
            $tpersonadatosfamiliare = Session::get('listFamEmp',array());

        } 

        $i= -1*(count($tpersonadatosfamiliare)+1)*10;
        $cfamemp = $i;
        $identificacion = $request->input('identificacion');
        $apaterno = $request->input('apaterno');
        $amaterno = $request->input('amaterno');
        $nombres = $request->input('nombres');
        $parentesco = $request->input('parentesco_fam');
        $genero = $request->input('genero_fam');

        /* Parentesco Familiar Empleado */
		$parentes = Tcatalogo::where('codtab','=','00018')
		->where('digide','=',$parentesco)
		->first();
		/* Fin Parentesco Familiar Empleado */

		/* Género Familiar Empleado */
		$gener = Tcatalogo::where('codtab','=','00016')
		->where('digide','=',$genero)
		->first();
		/* Fin Género Familiar Empleado */       


        $fae['cpersonafamiliar']= $cfamemp;
        $fae['identificacion']= $identificacion;
        $fae['apaterno']= $apaterno;
        $fae['amaterno']= $amaterno;
        $fae['nombres']= $nombres;
        $fae['parentesco_fam']= $parentesco;
        $fae['desparentesco']= $parentes->descripcion;
        $fae['genero_fam'] = $genero; 
        $fae['desgenero'] = $gener->descripcion;     
        $tpersonadatosfamiliare[count($tpersonadatosfamiliare)]=$fae;   

        
        Session::put('listFamEmp',$tpersonadatosfamiliare);		
  
		return view('partials.tableModalFamiliaresEmpleado',compact('tpersonadatosfamiliare')); 

	}

	public function verFamEmpleado($cpersona){        

		/* Parentesco Familiar Empleado */
		$parentesco = Tcatalogo::where('codtab','=','00018')
		->whereNotNull('digide')
		->lists('descripcion','catalogoid')->all();
		/* Fin Parentesco Familiar Empleado */

		/* Género Familiar Empleado */
		$genero = Tcatalogo::where('codtab','=','00016')
		->lists('descripcion','catalogoid')->all();
		/* Fin Género Familiar Empleado */       
	
			
		$tpersonadatosfamiliare = DB::table('tpersonadatosfamiliares as tf')
		->leftJoin('tcatalogo as tc','tf.parentesco','=','tc.digide')
		->leftJoin('tcatalogo as tc1','tf.genero','=','tc1.digide')
		->where('tc.codtab','=','00018')
		->where('tc1.codtab','=','00016')
		->where('cpersona','=',$cpersona)	
		->orderBy('tf.apaterno','ASC')
		->orderBy('tf.amaterno','ASC')
		->orderBy('tf.nombres','ASC')		
		->select('tf.cpersonafamiliar','tf.parentesco','tf.apaterno','tf.amaterno','tf.nombres','tf.genero','tf.identificacion','tc.descripcion as parentesco','tc1.descripcion as genero')
		->get();
		/* Fin Datos De Familiares */

		return view('partials.tableModalFamiliaresEmpleado',compact('tpersonadatosfamiliare'));

	}

	public function saveEstudEmpleado(Request $request){

		$tpersonadatosestudio=array();

		if(Session::get('listEstEmp')){
            $tpersonadatosestudio = Session::get('listEstEmp',array());

        } 

        $i= -1*(count($tpersonadatosestudio)+1)*10;
        $cpersonaestudios = $i;
        $nivel= $request->input('nivel_est');
		$cespecialidad= $request->input('carrera_est');
		$cinstitucion= $request->input('instituto_est');
		$colegiatura= $request->input('Colegiatura');
		$estado= $request->input('estado_est');
		$fingreso= $request->input('finicio');
		$ftermino= $request->input('ffin');

		/* Nivel de Estudio Empleado */
		$dnivel = Tcatalogo::where('codtab','=','00019')
		->where('digide','=',$nivel)
		->first();
		/* Fin Nivel de Estudio Empleado */

		/* Carrera Empleado */
		$dcarrera = Tespecialidad::where('cespecialidad','=',$cespecialidad)
        ->first();
		/* Fin Carrera Empleado */

		/* Institución Empleado */
		$dinstitucion = Tinstitucion::where('cinstitucion','=',$cinstitucion)
        ->first();
		/* Fin Institución Empleado */

		/* Estado Estudio Empleado */
		$destado = Tcatalogo::where('codtab','=','00022')
		->where('digide','=',$estado)
		->first();
		/* Fin Estadoo Empleado */  

		$est['cpersonaestudios']= $cpersonaestudios;
	    $est['nivel']= $nivel;
	    $est['descnivel']= $dnivel->descripcion;
	    $est['cespecialidad']= $cespecialidad;
	    $est['descespecialidad']= $dcarrera->descripcion;
	    $est['cinstitucion']= $cinstitucion;
	    $est['descinstitucion']= $dinstitucion->descripcion;
	    $est['estado'] = $estado; 
	    $est['descestado'] = $destado->descripcion;   
	    $est['colegiatura']= $colegiatura;
	    $est['fingreso']= $fingreso;
	    $est['ftermino'] = $ftermino;
	    $tpersonadatosestudio[count($tpersonadatosestudio)]=$est; 

	    Session::put('listEstEmp',$tpersonadatosestudio);		


	    return view('partials.tableModalEstudiosEmpleado',compact('tpersonadatosestudio'));

		}

	public function verEstudEmpleado($cpersona){

		/* Nivel de Estudio Empleado */
		$nivel = Tcatalogo::where('codtab','=','00019')
		->whereNotNull('digide')
		->lists('tcatalogo','catalogoid')->all();
		/* Fin Nivel de Estudio Empleado */

		/* Carrera Empleado */
		 $carrera = Tespecialidad::lists('descripcion','cespecialidad')
        ->all();
		/* Fin Carrera Empleado */

		/* Institución Empleado */
		$institucion = Tinstitucion::lists('descripcion','cinstitucion')
        ->all();
		/* Fin Institución Empleado */

		/* Estado Estudio Empleado */
		$estado = Tcatalogo::where('codtab','=','00022')
		->whereNotNull('digide')
		->lists('tcatalogo','catalogoid')->all();
		/* Fin Estadoo Empleado */  
	
		/* Datos De Estudio Empleado */
		$tpersonadatosestudio = DB::table('tpersonaestudios as p')
		->leftJoin('tcatalogo as tc','p.nivel','=','tc.digide')
		->leftJoin('tcatalogo as tc1','p.estado','=','tc1.digide')	
		->leftJoin('tespecialidad as e','p.cespecialidad','=','p.cespecialidad')
		->leftJoin('tinstitucion as i','p.cinstitucion','=','i.cinstitucion')
		->where('tc.codtab','=','00019')
		->where('tc1.codtab','=','00022')	
		->where('p.cpersona','=',$cpersona)
		->orderBy('p.nivel','ASC')
		->orderBy('p.cespecialidad','ASC')
		->orderBy('p.cinstitucion','ASC')
		->orderBy('p.estado','ASC')
		->select('p.*','tc.descripcion as nivel','e.descripcion as cespecialidad','i.descripcion as cinstitucion','tc1.descripcion as estado')
		->get();
		/* Fin Datos De Estudio Empleado */
		return view('partials.tableModalEstudiosEmpleado',compact('tpersonadatosestudio'));
	}

	public function saveEnfEmpleado(Request $request){

		$tpersonadatosmedicosenfermedad=array();

		if(Session::get('listEnfEmp')){
            $tpersonadatosmedicosenfermedad = Session::get('listEnfEmp',array());
        } 

        $i= -1*(count($tpersonadatosmedicosenfermedad)+1)*10;
        $cpersonaenfermedad = $i;
        $cenfermedad= $request->input('descrip_enf');
		$observacion= $request->input('observacion_enf');

		/* Enfermedad Empleado */
		 $enfermedad = Tenfermedad::where('cenfermedad','=',$cenfermedad)		
        ->first();
		/* Fin Enfermedad Empleado */

			$enf['cpersonaenfermedad']= $cpersonaenfermedad;
			$enf['descenfermedad']= $enfermedad->descripcion;
	        $enf['observacion']= $observacion;	        
	        $enf['cenfermedad']= $cenfermedad;	        
	        $tpersonadatosmedicosenfermedad[count($tpersonadatosmedicosenfermedad)]=$enf;
		
	        Session::put('listEnfEmp',$tpersonadatosmedicosenfermedad);


		
		return view('partials.tableModalEnfermedadesEmpleado',compact('tpersonadatosmedicosenfermedad'));

		}

	public function verEnfEmpleado(Request $request,$cpersona){

		/* Enfermedad Empleado */
		 $enfermedad = Tenfermedad::lists('descripcion','cenfermedad')
        ->all();
		/* Fin Enfermedad Empleado */

		/* Datos De Estudio Empleado */
		$tpersonadatosmedicosenfermedad = DB::table('tpersonadatosmedicosenfermedad as tpe')
		->leftJoin('tenfermedad as te','te.cenfermedad','=','tpe.cenfermedad')		
		->where('cpersona','=',$cpersona)
		->orderBy('tpe.cenfermedad','ASC')
		->select('te.descripcion','tpe.observacion','tpe.cpersonaenfermedad','tpe.cenfermedad')
		->get();
		/* Fin Datos De Estudio Empleado */
		return view('partials.tableModalEnfermedadesEmpleado',compact('tpersonadatosmedicosenfermedad'));
	}

	public function saveAlergEmpleado(Request $request){

		$tpersonadatosmedicosalergia=array();

		if(Session::get('listAlerEmp')){
            $tpersonadatosmedicosalergia = Session::get('listAlerEmp',array());
        } 

        $i= -1*(count($tpersonadatosmedicosalergia)+1)*10;
        $cpersonaalergia = $i;
        $calergias= $request->input('descrip_aler');
		$observacion= $request->input('observacion_aler');

		$alergia = Talergia::where('calergias','=',$calergias)		
        ->first();

		$ale['cpersonaalergia']= $cpersonaalergia;
		$ale['descalergia']= $alergia->descripcion;
	    $ale['observacion']= $observacion;	        
	    $ale['calergias']= $calergias;	        
	    $tpersonadatosmedicosalergia[count($tpersonadatosmedicosalergia)]=$ale;

		Session::put('listAlerEmp',$tpersonadatosmedicosalergia);

		return view('partials.tableModalAlergiasEmpleado',compact('tpersonadatosmedicosalergia'));		

		}

	public function verAlergEmpleado(Request $request,$cpersona){
		/* Alergias Empleado */
		 $alergia = Talergia::lists('descripcion','calergias')
        ->all();
		/* Fin Alergias Empleado */

		/* Datos De Alergias Empleado */
		$tpersonadatosmedicosalergia = DB::table('tpersonadatosmedicosalergia as tpa')
		->leftJoin('talergias as ta','tpa.calergias','=','ta.calergias')
		->where('cpersona','=',$cpersona)
		->orderBy('tpa.calergias','ASC')
		->select('ta.descripcion','tpa.observacion','tpa.calergias','tpa.cpersonaalergia')
		->get();
		/* Fin Datos De Estudio Empleado */
		return view('partials.tableModalAlergiasEmpleado',compact('tpersonadatosmedicosalergia'));	
	}

    public function eliminarFamiliarEmpleado($cpersona){
    	
    }

    public function eliminarEstudioEmpleado($cpersona){
        DB::beginTransaction();
        $estudio = Tpersonaestudio::where('cpersonaestudios','=',$cpersona)->first();
        $cpersona_est = $estudio->cpersona;
        try
        {
            $estudio->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('verEstudEmpleado',$cpersona_est);
    }

    public function eliminarEnfEmpleado($cpersona){
        DB::beginTransaction();
        $enfermedad = Tpersonadatosmedicosenfermedad::where('cpersonaenfermedad','=',$cpersona)->first();
        $cpersona_enf = $enfermedad->cpersona;
        try
        {
            $enfermedad->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('verEnfEmpleado',$cpersona_enf);
    }

    public function eliminarAlergEmpleado($cpersona){
        DB::beginTransaction();
        $alergia = Tpersonadatosmedicosalergium::where('cpersonaalergia','=',$cpersona)->first();
        $cpersona_alerg = $alergia->cpersona;
        try
        {
            $alergia->delete();
        }catch(\Illuminate\Database\QueryException $e){
            
        }
        DB::commit();
        return Redirect::route('verAlergEmpleado',$cpersona_alerg);
    }
}