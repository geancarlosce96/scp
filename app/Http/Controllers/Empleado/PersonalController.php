<?php

namespace App\Http\Controllers\Empleado;

use Illuminate\Http\Request;
use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tpersonadatosempleado;
use App\Models\Tpersonaotrosdatosempleado;
use App\Models\Tpersonatelefono;
use App\Models\Tpersonadatosfamiliare;
use App\Models\Tpersonaestudio;
use App\Models\Tpersonadatosmedico;
use App\Models\Tpersonadatosmedicosalergium;
use App\Models\Tpersonadatosmedicosenfermedad;
use App\Models\Tpai;
use App\Models\Tubigeo;
use App\Models\Tpersonanaturalinformacionbasica;
use App\Models\Tcatalogo;
use App\Models\Tespecialidad;
use App\Models\Tinstitucion;
use App\Models\Tenfermedad;
use App\Models\Talergia;
use App\Models\Tarea;
use App\Models\Tcargo;
use App\Models\Tcategoriaprofesional;
use App\Models\Ttipocontacto;
use App\Models\Tusuario;
use App\Models\Tusuariopassword;
use App\Models\Tmenupersona;
use Carbon\Carbon;
use Redirect;
use Session;
use DB;

class PersonalController extends Controller
{
	public function listarpersonal()
	{
		$listarpersonal = DB::table('tpersona as p')
			->leftjoin('tpersonanaturalinformacionbasica as tnat','p.cpersona','=','tnat.cpersona')
			->leftJoin('tpersonadatosempleado as pe','p.cpersona','=','pe.cpersona')
			->leftJoin('tcargos as tc','pe.ccargo','=','tc.ccargo')
			->leftJoin('tusuarios as tu','pe.cpersona','=','tu.cpersona')
			->leftJoin('tusuariopassword as tup','tu.cusuario','=','tup.cusuario')
			->leftJoin('tareas as ta','pe.carea','=','ta.carea')
			->leftJoin('tpersonadatosempleado as peap','tc.cargoparentdespliegue','=','peap.ccargo')
			->leftJoin('tpersona as pea','pea.cpersona','=','peap.cpersona')
			->where('tnat.esempleado','=','1')
			->select('p.cpersona','p.nombre','tu.nombre as usuario','p.identificacion','tc.ccargo','tc.descripcion as cargo','ta.carea','ta.descripcion as area','pe.ccategoriaprofesional','pea.abreviatura as aprobador','tu.estado as estadousuario','pe.estado as estadopersona','p.abreviatura','pe.fingreso','tnat.apaterno','tnat.amaterno','tnat.nombres')
			->get();

		// dd($listarpersonal);
		$testadocivil = Tcatalogo::where('codtab','=','00015')
		->whereNotNull('digide')
		->get();  

		$tgenero = Tcatalogo::where('codtab','=','00016')
		->whereNotNull('digide')
		->get();  

		$testado= Tcatalogo::where('codtab','=','00024')
		->whereNotNull('digide')
		->get();  

		$tarea = DB::table('tareas')
		->get();

		$tcargo = DB::table('tcargos')
		->get();

		$tcategoria = DB::table('tcategoriaprofesional')
		->get();

		$tcontrato = DB::table('ttipocontrato')
		->get();

		$valor = 'nuevo';

			return view('ghumana.listadoEmpleados',compact('listarpersonal','testadocivil','tgenero','testado','tarea','tcargo','tcategoria','tcontrato','valor'));

	}
	public function personal($valor)
	{	
		
			
		$listarpersonal = DB::table('tpersona as p')
			->leftjoin('tpersonanaturalinformacionbasica as tnat','p.cpersona','=','tnat.cpersona')
			->leftJoin('tpersonadatosempleado as pe','p.cpersona','=','pe.cpersona')
			->leftJoin('tcargos as tc','pe.ccargo','=','tc.ccargo')
			->leftJoin('tusuarios as tu','pe.cpersona','=','tu.cpersona')
			->leftJoin('tusuariopassword as tup','tu.cusuario','=','tup.cusuario')
			->leftJoin('tareas as ta','pe.carea','=','ta.carea')
			->leftJoin('tpersonadatosempleado as peap','tc.cargoparentdespliegue','=','peap.ccargo')
			->leftJoin('tpersona as pea','pea.cpersona','=','peap.cpersona')
			->select('p.cpersona','p.nombre','tu.nombre as usuario','p.identificacion','tc.ccargo','tc.descripcion as cargo','ta.carea','ta.descripcion as area','pe.ccategoriaprofesional','pea.abreviatura as aprobador','tu.estado as estadousuario','pe.estado as estadopersona','p.abreviatura','pe.fingreso','tnat.apaterno','tnat.amaterno','tnat.nombres','pe.ctipocontrato','tnat.genero','tnat.estadocivil')
			->where('tnat.esempleado','=','1')
			->where('p.cpersona','=',$valor)
			->get();



	return $listarpersonal;

		
	}

	public function grabarpersonal(Request $request)
	{	
		// dd($request->all());
		
		if ($request->input('cpersona')=='nuevo') {
			$persona = new Tpersona;
			$personabasica = new Tpersonanaturalinformacionbasica;
			$personaempleado = new Tpersonadatosempleado;
			
		} else {
			$persona = Tpersona::where('cpersona','=',$request->input('cpersona'))->first();
			$personabasica = Tpersonanaturalinformacionbasica::where('cpersona','=',$request->input('cpersona'))->first();
			$personaempleado = Tpersonadatosempleado::where('cpersona','=',$request->input('cpersona'))->first();
		}

		$persona->ctipopersona='NAT';
		$persona->ctipoidentificacion='2';
		$persona->identificacion=$request->input('dni');
		$persona->nombre=$request->input('apaterno')." ".$request->input('amaterno')." ".$request->input('nombres');
		$persona->abreviatura=$request->input('alias');
		$persona->save();

		$personabasica->cpersona = $persona->cpersona;
		$personabasica->apaterno = $request->input('apaterno');
		$personabasica->amaterno = $request->input('amaterno');
		$personabasica->nombres = $request->input('nombres');
		$personabasica->esempleado = '1';
		$personabasica->ctipoidentificacion = '2';
		$personabasica->identificacion = $request->input('dni');
		$personabasica->genero = $request->input('genero');
		$personabasica->estadocivil = $request->input('estadocivil');
		$personabasica->save();

		$personaempleado->cpersona = $persona->cpersona;
		$personaempleado->carea = $request->input('area');
		$personaempleado->ccargo = $request->input('cargo');
		$personaempleado->fingreso = $request->input('fecha');
		$personaempleado->estado = $request->input('estadopersona');
		$personaempleado->ccategoriaprofesional = $request->input('categoria');
		$personaempleado->ctipocontrato = $request->input('contrato');
		$personaempleado->save();

		if ($request->input('usuario') && $request->input('clave')) {
			
		// dd($request->input('usuario'));
		$usuario = new Tusuario();
		$usuario->nombre= $request->input('usuario');
		$usuario->estado= $request->input('estadopersona');
		$usuario->cpersona= $persona->cpersona;
		$usuario->save();

		$usuariopassword=new Tusuariopassword();
		$usuariopassword->cusuario= $usuario->cusuario;
		$usuariopassword->fhasta= $request->input('fecha');
		$usuariopassword->fdesde= $request->input('fecha');
		$usuariopassword->password= md5($request->input('clave'));
		$usuariopassword->caduca= $request->input('fecha');
		$usuariopassword->save();

        $tmenupersona= new Tmenupersona();
        $tmenupersona->menuid='04';
        $tmenupersona->cpersona=$persona->cpersona;
        $tmenupersona->permiso='01';
        $tmenupersona->save();

        $tmenupersona1= new Tmenupersona();
        $tmenupersona1->menuid='0401';
        $tmenupersona1->cpersona=$persona->cpersona;
        $tmenupersona1->permiso='01';
        $tmenupersona1->save();

		$tmenupersona= new Tmenupersona();
        $tmenupersona->menuid='05';
        $tmenupersona->cpersona=$persona->cpersona;
        $tmenupersona->permiso='01';
        $tmenupersona->save();

        $tmenupersona1= new Tmenupersona();
        $tmenupersona1->menuid='0501';
        $tmenupersona1->cpersona=$persona->cpersona;
        $tmenupersona1->permiso='01';
        $tmenupersona1->save();

		}

		return Redirect::route('listarpersonal');

	}

	public function estadoempleadousuario($id)
	{
		$valor='';
		// $cpersona = explode("_", $id);

		$personaempleado = Tpersonadatosempleado::where('cpersona','=',$id)->first();
		$usuario = Tusuario::where('cpersona','=',$id)->first();

		if ($personaempleado->estado=='ACT' && $usuario->estado=='ACT' ) {
			$personaempleado->estado = 'INA';
			$usuario->estado = 'INA';
			$valor = 'Inactivo';
		}
		else if ($personaempleado->estado=='INA' && $usuario->estado=='INA' ) {
			$personaempleado->estado = 'ACT';
			$usuario->estado = 'ACT';
			$valor = 'Activo';
		}

		else {
			$personaempleado->estado = 'ACT';
			$usuario->estado = 'ACT';
			$valor = 'Activado';	
		}


		$personaempleado->save();
		$usuario->save();

		// return $valor;

		return Redirect::route('listarpersonal');
	}

}

