<?php

namespace App\Http\Controllers\Cdocumentario;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Facades\Datatables;
use DB;
use Redirect;
use Carbon\Carbon;
use App\Models\Ttransmittalconfiguracion;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tcatalogo;
use App\Models\Ttransmittalejecucion;
use App\Models\Testadoproyecto;
use App\Models\Tproyecto;
use App\Models\Tproyectoedt;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesfecha;
use App\Models\Tproyectoinformacionadicional;
use App\Erp\ProyectoSupport;
use App\Erp\EmpleadoSupport;
use App\Erp\HelperSIGSupport;
use App\Erp\GastosPdf;
use App\Models\Ttransmittalejecuciondetalle;
use Session;
use App\Models\Ttransmittalconfiguracioncontacto;
use App\Models\Tcategoriaentregable;
use Response;


class DocumentarioController extends Controller
{
    public function configuracionTransmittalPropuesta(){

        return view('cdocumentario/configTransmittal');
    }
    public function editarConfiguracionTransmittalPropuesta(Request $request,$idPropuesta){
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->first();
        $persona = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$propuesta->cpersona)->first();

        $configuracion = DB::table('ttransmittalconfiguracion as tt')
        ->where('tt.ctipo','=','1')
        ->where('tt.cpropuesta','=',$idPropuesta)
        ->first();
        $logo = $personajur->logo;
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();

        $tformato = Tcatalogo::where('codtab','=','00000')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();

        $ttipoenvio = Tcatalogo::where('codtab','=','00000')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();        
        Session::forget('contactosPropuesta');
        $transconfigcontac=array();
        if($configuracion){
            $ttransconfigcontac= DB::table('ttransmittalconfiguracioncontacto as con')
            ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','con.ctipocontacto')
            ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','con.ccontactocargo')
            /*->leftJoin('tunidadmineracontactos as ucon','con.cunidadmineracontacto','=','ucon.cunidadmineracontacto')*/
            ->where('ctransconfiguracion','=',$configuracion->ctransconfiguracion)
            ->whereNull('cpersona_contacroanddes')
            ->select('con.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
            ->get();
            foreach($ttransconfigcontac as $con){
                $c['ctransmittalconfiguracioncontacto']=$con->ctransmittalconfiguracioncontacto;
                $c['cunidadmineracontacto'] = $con->cunidadmineracontacto;
                if(!is_null($c['cunidadmineracontacto'])){
                    $umincon =DB::table('tunidadmineracontactos as ucon')
                    ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','ucon.ctipocontacto')
                    ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','ucon.ccontactocargo')
                    ->where('ucon.cunidadmineracontacto','=',$c['cunidadmineracontacto'])
                    ->select('ucon.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
                    ->first();
                    if($umicon){
                        $c['apaterno']=$umicon->apaterno;
                        $c['amaterno']=$umicon->amaterno;
                        $c['apellidos'] = $umincon->apaterno . " ".$umincon->amaterno;
                        $c['nombres'] = $umicon->nombres;
                        $c['contacto_cargo'] = $umicon->contacto_cargo;
                        $c['ccontactocargo']=$umicon->ccontactocargo;
                        $c['contacto']=$umincon->contacto;
                        $c['ctipocontacto']= $umincon->ctipocontacto;
                        $c['ctipoinformacioncontacto']="";
                        $c['email'] = $umincon->email;
                    }
                }else{
                    $c['apellidos']=$con->apellidos;
                    $c['apaterno']='';
                    $c['amaterno']='';
                    $c['nombres'] = $con->nombres;
                    $c['contacto_cargo'] = $con->contacto_cargo;
                    $c['ccontactocargo']=$con->ccontactocargo;
                    $c['contacto']=$con->contacto;
                    $c['ctipocontacto']= $con->ctipocontacto;
                    $c['ctipoinformacioncontacto']="";
                    $c['email'] = $con->email;
                }

                $transconfigcontac[count($transconfigcontac)] = $c;
            }
        }
        $ttiposcontacto = DB::table('ttiposcontacto')->lists('descripcion','ctipocontacto');
        $ttiposcontacto = [''=>''] + $ttiposcontacto;
        $tcontactocargo = DB::table('tcontactocargo')->lists('descripcion','ccontactocargo');
        $tcontactocargo = [''=>''] + $tcontactocargo;
        Session::put('contactosPropuesta',$transconfigcontac);

        return view('cdocumentario/configTransmittal',compact('propuesta','persona','uminera','personajur','logo','configuracion','transconfigcontac','tformato','ttipoenvio','ttiposcontacto','tcontactocargo'));
    }
    public function modalContactoTransmittalPropuesta(Request $request,$id){
            $ttransconfigcontac= DB::table('ttransmittalconfiguracioncontacto as con')
            ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','con.ctipocontacto')
            ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','con.ccontactocargo')
            /*->leftJoin('tunidadmineracontactos as ucon','con.cunidadmineracontacto','=','ucon.cunidadmineracontacto')*/
            ->where('ctransmittalconfiguracioncontacto','=',$id)
            ->select('con.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
            ->first();
            $tconf = array();
            if($ttransconfigcontac){
                $tconf['ctransmittalconfiguracioncontacto']=$ttransconfigcontac->ctransmittalconfiguracioncontacto;
                $tconf['cunidadmineracontacto'] = $ttransconfigcontac->cunidadmineracontacto;
                if(!(is_null($tconf['cunidadmineracontacto']))){
                    $umincon =DB::table('tunidadmineracontactos as ucon')
                    ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','ucon.ctipocontacto')
                    ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','ucon.ccontactocargo')
                    ->where('ucon.cunidadmineracontacto','=',$tconf['cunidadmineracontacto'])
                    ->select('ucon.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
                    ->first();
                    if($umicon){
                        $c['apaterno']=$umicon->apaterno;
                        $c['amaterno']=$umicon->amaterno;
                        $c['apellidos'] = $umincon->apaterno . " ".$umincon->amaterno;
                        $c['nombres'] = $umicon->nombres;
                        $c['contacto_cargo'] = $umicon->contacto_cargo;
                        $c['ccontactocargo']=$umicon->ccontactocargo;
                        $c['contacto']=$umincon->contacto;
                        $c['ctipocontacto']= $umincon->ctipocontacto;
                        $c['email'] = $umincon->email;
                        $c['ctipoinformacioncontacto']="";
                    }
                }else{
                    $tconf['apellidos']=$ttransconfigcontac->apellidos;
                    $tconfc['apaterno']='';
                    $tconf['amaterno']='';
                    $tconfc['nombres'] = $ttransconfigcontac->nombres;
                    $tconf['contacto_cargo'] = $ttransconfigcontac->contacto_cargo;
                    $tconf['ccontactocargo']=$ttransconfigcontac->ccontactocargo;
                    $tconf['contacto']=$ttransconfigcontac->contacto;
                    $tconf['ctipocontacto']= $ttransconfigcontac->ctipocontacto;
                    $tconf['email'] = $ttransconfigcontac->email;
                    $tconf['ctipoinformacioncontacto']="";
                }
            }
            return Response::json($tconf);


    }
    public function addContactoTransmittalPropuesta(Request $request){
        $transconfigcontac = array();
        if(Session::get('contactosPropuesta')){
            $transconfigcontac=Session::get('contactosPropuesta');

        }
        $des_cargo="";
        $des_contacto="";
        if(strlen($request->input('tipocontacto') )>0){
            $tcontacto = DB::table('ttiposcontacto')
            ->where('ctipocontacto','=',$request->input('tipocontacto'))
            ->first();
            if($tcontacto){
                $des_contacto = $tcontacto->descripcion;
            }
        }
        if(strlen($request->input('contactocargo') )>0){
            $tcargo = DB::table('tcontactocargo')
            ->where('ccontactocargo','=',$request->input('contactocargo'))
            ->first();
            if($tcargo){
                $des_cargo = $tcargo->descripcion;
            }
        }        
        $i=-1*(count($transconfigcontac)+1)*10;
        $c['ctransmittalconfiguracioncontacto']=$i;
        $c['cunidadmineracontacto']=null;
        $c['apaterno']=$request->input('apaterno');
        $c['amaterno']=$request->input('amaterno');
        $c['apellidos'] = $c['apaterno']. " ".$c['amaterno'];
        $c['nombres']=$request->input('nombres');
        $c['contacto_cargo'] = $des_cargo;
        $c['ccontactocargo']=$request->input('contactocargo');
        $c['contacto']=$des_contacto;
        $c['ctipocontacto']= $request->input('tipocontacto');
        $c['ctipoinformacioncontacto']="";
        $c['email'] = "";        
        /* agregar campos */

        $transconfigcontac[count($transconfigcontac)] = $c;

        Session::put('contactosPropuesta',$transconfigcontac);
        return view('partials.tableConfiguracionTransmittalContactoPropuesta',compact('transconfigcontac'));
    }
    public function delContactoTransmittalPropuesta(Request $request,$idContacto){
        $transconfigcontac = array();
        if(Session::get('contactosPropuesta')){
            $transconfigcontac = Session::get('contactosPropuesta');

        }
        $temp = array();
        foreach($transconfigcontac as $con){
            if(!($con['ctransmittalconfiguracioncontacto']==$idContacto)){
                $temp[count($temp)]=$con;
            }                

        }
        if($idContacto>0){
            $del = Ttransmittalconfiguracioncontacto::where('ctransmittalconfiguracioncontacto','=',$idContacto);
            $del->delete();
        }        
        $transconfigcontac = $temp;        
        Session::put('contactosPropuesta',$transconfigcontac);
        return view('partials.tableConfiguracionTransmittalContactoPropuesta',compact('transconfigcontac'));
    }
    public function UploadLogoEmpresa(Request $request){
        $cpropuesta= $request->input('cpropuesta_file');
        $propuesta = DB::table('tpropuesta')
        ->where('cpropuesta','=',$cpropuesta)
        ->first();
        $personajur = Tpersonajuridicainformacionbasica::where('cpersona','=',$propuesta->cpersona)
        ->first();

        $file = $request->file('logo');
        $extension = $file->getClientOriginalExtension();
        $id = Storage::disk('archivos')->put($file->getFilename().'.'.$extension,  File::get($file));
        $ruta = storage_path('archivos') . "/" . $file->getFilename().'.'.$extension;
        $data = file_get_contents($ruta);
        $imagen = pg_escape_bytea($data);
        if($personajur){
            $personajur->logo=$imagen;
            $personajur->save();
        }

    }
    public function UploadLogoEmpresaTRProy(Request $request){
        $cproyecto= $request->input('cproyecto_file');
        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$cproyecto)
        ->first();
        $personajur = Tpersonajuridicainformacionbasica::where('cpersona','=',$proyecto->cpersonacliente)
        ->first();

        $file = $request->file('logo');
        $extension = $file->getClientOriginalExtension();
        $id = Storage::disk('archivos')->put($file->getFilename().'.'.$extension,  File::get($file));
        $ruta = storage_path('archivos') . "/" . $file->getFilename().'.'.$extension;
        $data = file_get_contents($ruta);
        $imagen = pg_escape_bytea($data);
        if($personajur){
            $personajur->logo=$imagen;
            $personajur->save();
        }

    }
    public function viewLogoEmpresa($cpersona){
        $personajur = Tpersonajuridicainformacionbasica::where('cpersona','=',$cpersona)
        ->first();
        $img = Image::make($personajur->logo);
        return $img->response();

    }
    public function grabarConfTransmittalPropuesta(Request $request){
        $cpropuesta=$request->input('cpropuesta');
        $ctransconfiguracion = $request->input('ctransconfiguracion');
        DB::beginTransaction();
        if(strlen($ctransconfiguracion)<=0){
            $ttransconfig = new Ttransmittalconfiguracion();
            $ttransconfig->cpropuesta=$cpropuesta;
            $ttransconfig->ctipo='1';
        }else{
            $ttransconfig = Ttransmittalconfiguracion::where('ctransconfiguracion','=',$ctransconfiguracion)->first();
        }


        

        //tformato
        if($request->input('formato')!=''){
            $ttransconfig->formato = $request->input('formato');
        }
        //tipo de emvio
        if($request->input('tipoenvio')!=''){
            $ttransconfig->tipoenvio = $request->input('tipoenvio');
        }        
        $ttransconfig->numero=$request->input('codtransmittal');
        $ttransconfig->save();

        //configuracion de contactos
        $contact = array();
        if(Session::get('contactosPropuesta')){
            $contact = Session::get('contactosPropuesta');
        }
        foreach($contact as $con){
            if($con['ctransmittalconfiguracioncontacto']<=0){
                $contacto = new Ttransmittalconfiguracioncontacto();
                $contacto->ctransconfiguracion=$ttransconfig->ctransconfiguracion;
            }else{
                $contacto = Ttransmittalconfiguracioncontacto::where('ctransmittalconfiguracioncontacto','=',$con['ctransmittalconfiguracioncontacto'])->first();
            }
            $contacto->apellidos=$con['apellidos'];
            $contacto->nombres= $con['nombres'];
            $contacto->ctipocontacto=$con['ctipocontacto'];
            $contacto->ccontactocargo=$con['ccontactocargo'];
            
            $contacto->ctipoinformacioncontacto=$con['ctipoinformacioncontacto'];
            $contacto->email=$con['email'];
            $contacto->save();

        }
        DB::commit();
        return Redirect::route('editarConfTransmittal',$cpropuesta);
    }    

    public function genTransmittalPropuesta(){

        return view('cdocumentario/generarTransmittal');
    }

    public function editarGenTransmittalPropuesta(Request $request,$idPropuesta){
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->first();
        $persona = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$propuesta->cpersona)->first();
        $configuracion = DB::table('ttransmittalconfiguracion as tt')
        ->where('tt.ctipo','=','1')
        ->where('tt.cpropuesta','=',$idPropuesta)
        ->first();

        /*$ejecucion = DB::table('ttransmittalejecucion eje')
        ->where('eje ')
        ->first();*/

        
        return view('cdocumentario/generarTransmittal',compact('propuesta','persona','personajur','configuracion'));
    }

    public function grabarGenTransmittalPropuesta(Request $request){
        $cpropuesta=$request->input('cpropuesta');
        //$ctransmittalejecucion =  $request->input('ctransmittalejecucion');
        
        $configuracion = DB::table('ttransmittalconfiguracion as tt')
        ->where('tt.ctipo','=','1')
        ->where('tt.cpropuesta','=',$cpropuesta)
        ->first();        

        DB::beginTransaction();
        if(strlen($ctransmittalejecucion)<=0){
            $ttranseje = new Ttransmittalejecucion();
            $ttranseje->cpropuesta=$cpropuesta;
            $ttranseje->ctipo='1';
        }else{
            $ttranseje = Ttransmittalejecucion::where('ctransmittalejecucion','=',$ctransmittalejecucion)->first();
        }

        $ttranseje->save();

        DB::commit();
        return Redirect::route('editarGenTransmittal',$cpropuesta);
    }
    public function panelJefe(){
        $datos=array();
        //nro de proyectos
        $datos['nroproy'] =DB::table('tproyecto')->count();


        //proyectos por estado
        $tdatos_proy_est =DB::table('tproyecto')
        ->select(DB::raw('cestadoproyecto,count(*) as nro'))
        ->groupBy('cestadoproyecto')
        ->get();
        $datos['proy_estado']=array();
        foreach($tdatos_proy_est as $est){
            $datos['proy_estado'][$est->cestadoproyecto]=$est->nro;
        }

        //nro de entregables
        $datos['nroent'] = DB::table('tproyectoentregables')->count();

        //entregables por estado
        $tdatos_ent_est = DB::table('tproyectoentregables as ep')
        ->join('testadoentregable as es_e','ep.cestadoentregable','=','es_e.cestadoentregable') 
        ->select(DB::raw('ep.cestadoentregable,count(*) as nro'))
        ->groupBy('ep.cestadoentregable')
        ->get();
        foreach($tdatos_ent_est as $est){
            $datos['ent_estado'][$est->cestadoentregable]=$est->nro;

        }
        $tpersonal_cd = DB::table('tpersona as p')
        ->join('tpersonanaturalinformacionbasica as nat','nat.cpersona','=','p.cpersona')
        ->where('nat.esempleado','=','1')
        ->select('p.cpersona','p.nombre')
        ->get();
        $datos['personal']=array();
        foreach($tpersonal_cd as $per){
            $tlaboral = DB::table('tpersonadatosempleado as dat')
            ->join('tareas as are','are.carea','=','dat.carea')
            ->where('cpersona','=',$per->cpersona)
            ->where('are.codigo','=','27') /* especificar el codigo correcto de Control Documentario*/
            ->orderBy('dat.fingreso','DESC')
            ->first();
            if($tlaboral){
                $per_cd['cpersona']=$per->cpersona;
                $per_cd['nombre'] = $per->nombre;
                
                $tdatos_proy_estado =DB::table('tproyecto as p')
                ->join('tproyectoinformacionadicional as pia','p.cproyecto','=','pia.cproyecto')
                ->where('pia.cpersona_cdocumentario','=',$per->cpersona)
                ->select(DB::raw('p.cestadoproyecto,count(*) as nro'))
                ->groupBy('p.cestadoproyecto')
                ->get();
                foreach($tdatos_proy_estado as $esta){
                    $per_cd['proy_'.$esta->cestadoproyecto] = $esta->nro;

                    
                }
                $tdatos_ent_estado = DB::table('tproyectoentregables as ep')
                ->join('tproyectoinformacionadicional as pia','ep.cproyecto','=','pia.cproyecto')
                ->join('testadoentregable as es_e','ep.cestadoentregable','=','es_e.cestadoentregable') 
                ->where('pia.cpersona_cdocumentario','=',$per->cpersona)
                ->select(DB::raw('ep.cestadoentregable,count(*) as nro'))
                ->groupBy('ep.cestadoentregable')
                ->get();
                foreach($tdatos_ent_estado as $esta){
                    $per_cd['ent_'.$esta->cestadoentregable] = $esta->nro;

                    
                }

                $datos['personal'][count($datos['personal'])]=$per_cd;


            }
        }


        return view('cdocumentario.panelControlDocumentario',compact('datos'));
    }

    public function listaJefe(){
        $objEmp = new EmpleadoSupport(); 
        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp;   
        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = ['' =>''] + $estadoproyecto;

        $proyecto = DB::table('tproyecto')->lists('nombre','cproyecto');
        $proyecto = [''=>''] + $proyecto;
        
        return view('cdocumentario.listaProyectosCD',compact('estadoproyecto','personal_gp','proyecto'));
    }
    public function buscarListaJefe(Request $request){
        $objEmp = new EmpleadoSupport(); 
        $personal_cd = $objEmp->getPersonalArea_cbx('27'); //$objEmp->getPersonalRol('CD','001','2') ;
        $personal_cd = [''=>''] + $personal_cd;  
        $query=DB::table('tproyecto as p')
        ->join('tpersona as per','per.cpersona','=','p.cpersonacliente')
        ->join('testadoproyecto as est','p.cestadoproyecto','=','est.cestadoproyecto')
        ->leftJoin('tpersona as gte','gte.cpersona','=','p.cpersona_gerente')
        ->leftJoin('tproyectoinformacionadicional as adi','adi.cproyecto','=','p.cproyecto')
        ->leftJoin('tpersona as controlproy','adi.cpersona_cproyecto','=','controlproy.cpersona')
        ->leftJoin('tpersona as controldoc','adi.cpersona_cdocumentario','=','controldoc.cpersona')
        ->select('p.codigo','p.nombre','p.cproyecto','per.nombre as cliente','gte.nombre as gerente','controlproy.nombre as controlproy')
        ->addSelect('controldoc.nombre as controldoc','est.descripcion','adi.carpeta','adi.cpersona_cdocumentario');
        if (strlen($request->input('cproyecto'))>0){
            $query= $query->where('p.cproyecto','=',$request->input('cproyecto'));
        }
        if (strlen($request->input('estadoproyecto'))>0){
            $query= $query->where('p.cestadoproyecto','=',$request->input('estadoproyecto'));
        }
        if (strlen($request->input('gteproyecto'))>0){
            $query= $query->where('p.cpersona_gerente','=',$request->input('gteproyecto'));
        }                
        $tproyectos=$query->get();
        $listaProy=array();
        foreach($tproyectos as $p){
            $pry['cproyecto']= $p->cproyecto;
            $pry['codigo'] = $p->codigo;
            $pry['nombre'] = $p->nombre;
            $pry['cliente'] = $p->cliente;
            $pry['gerente'] = $p->gerente;
            $pry['controlproy'] = $p->controlproy;
            $pry['controldoc'] = $p->controldoc;
            $pry['id_controldoc'] = $p->cpersona_cdocumentario;
            $pry['estado'] = $p->descripcion;
            $pry['carpeta'] = $p->carpeta;
            $listaProy[count($listaProy)]=$pry;

        }


        

        return view('partials.tableListaProyectosCD',compact('listaProy','personal_cd'));
    }
    public function grabarListaJefe(Request $request){
        DB::beginTransaction();
        $lista=$request->input('lista');
        foreach($lista as $li){
            if(strlen($li[1])>0){
                $obj = Tproyectoinformacionadicional::where('cproyecto','=',$li[0])->first();
                if(!$obj){
                    $obj = new Tproyectoinformacionadicional();
                }
                $obj->cpersona_cdocumentario= $li[1];
                $obj->save();
            }
        }
        DB::commit();
        return $this->buscarListaJefe($request);
    }
    public function viewUbicacion(Request $request){
        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$request->cproyecto)
        ->first();
        $adicional = DB::table('tproyectoinformacionadicional')
        ->where('cproyecto','=',$proyecto->cproyecto)
        ->first();

        $objHelper = new HelperSIGSupport();
        $tubicacion = $objHelper->getListCatalogo('00051'); //ubicacion de proyecto
        $tubicacion = [''=>''] + $tubicacion;
        
        return view('cdocumentario.modalUbicacion',compact('proyecto','tubicacion','adicional'));
    }
    public function saveUbicacion(Request $request){
        $adicional = Tproyectoinformacionadicional::where('cproyecto','=',$request->input('cproyecto'))->first();
        if(!$adicional){
            $adicional = new Tproyectoinformacionadicional();
            $adicional->cproyecto = $request->input('cproyecto');

        }
        $adicional->ubicacion = $request->input('ubicacion');
        $adicional->save();
        return "OK";
    }    

    public function programacionJefe(){
        $objEmp = new EmpleadoSupport(); 
        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp;         
        $personal_cd = $objEmp->getPersonalArea_cbx('27'); //$objEmp->getPersonalRol('CD','001','2') ;
        $personal_cd = [''=>''] + $personal_cd;              
        return view('cdocumentario.programacionCD',compact('personal_gp','personal_cd'));
    }

    public function buscarProgramacionJefe(){
        $listaProg=array();
        //proyecto con entregables y programacion
        return view('partials.tableProgramacionJefe',compact('listaProg'));
    }
    public function ubicacionJefe(){
        $listUbi=array();
        $objHelper = new HelperSIGSupport();
        $tubicacion = $objHelper->getListCatalogo('00051'); //ubicacion de proyecto
        foreach($tubicacion as $k => $v){
            $u=array();
            $u['cubi']=$k;
            $u['ubi']=$v;
            
            $listUbi[count($listUbi)]=$u;
        }
        return view('cdocumentario.ubicacionProyectos',compact('listUbi'));
    }

    public function buscarUbicacionJefe(){
        return view('cdocumentario.ubicacionProyectos');
    }

    public function panelUsuario(){
        $objEmp = new EmpleadoSupport(); 
        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp;   
        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = ['' =>''] + $estadoproyecto;

        $proyecto = DB::table('tproyecto')->lists('nombre','cproyecto');
        $proyecto = [''=>''] + $proyecto;
        return view('cdocumentario.panelUsuarioCD',compact('personal_gp','estadoproyecto','proyecto'));
    }

    public function buscarPanelUsuario(){
        $query=DB::table('tproyecto as p')
        ->join('tpersona as per','per.cpersona','=','p.cpersonacliente')
        ->join('testadoproyecto as est','p.cestadoproyecto','=','est.cestadoproyecto')
        ->leftJoin('tpersona as gte','gte.cpersona','=','p.cpersona_gerente')
        ->leftJoin('tproyectoinformacionadicional as adi','adi.cproyecto','=','p.cproyecto')
        ->leftJoin('tpersona as controlproy','adi.cpersona_cproyecto','=','controlproy.cpersona')
        ->leftJoin('tpersona as controldoc','adi.cpersona_cdocumentario','=','controldoc.cpersona')
        ->select('p.codigo','p.nombre','per.nombre as cliente','gte.nombre as gerente','controlproy.nombre as controlproy','controldoc.nombre as controldoc','est.descripcion','adi.carpeta');
        if (strlen($request->input('cproyecto'))>0){
            $query= $query->where('p.cproyecto','=',$request->input('cproyecto'));
        }
        if (strlen($request->input('estadoproyecto'))>0){
            $query= $query->where('p.cestadoproyecto','=',$request->input('estadoproyecto'));
        }
        if (strlen($request->input('gteproyecto'))>0){
            $query= $query->where('p.cpersona_gerente','=',$request->input('gteproyecto'));
        }                
        $tproyectos=$query->get();
        $listaProy=array();
        foreach($tproyectos as $p){
            $pry['codigo'] = $p->codigo;
            $pry['nombre'] = $p->nombre;
            $pry['cliente'] = $p->cliente;
            $pry['gerente'] = $p->gerente;
            $pry['controlproy'] = $p->controlproy;
            $pry['controldoc'] = $p->controldoc;
            $pry['estado'] = $p->descripcion;
            $pry['carpeta'] = $p->carpeta;
            $listaProy[count($listaProy)]=$pry;

        }        
        return view('cdocumentario.panelUsuarioCD',compact('listaProy'));
    }

    public function configuracionTRUser(){
        return view('cdocumentario.configTransmittalUsuario');
    }
    public function editarConfiguracionTRUser(Request $request,$idProyecto){
        $nroFila=0;
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        //Session::forget('configContactos');
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;

        $configuracion = DB::table('ttransmittalconfiguracion as tt')
        ->where('tt.ctipo','=','2')
        ->where('tt.cproyecto','=',$idProyecto)
        ->first();
        $logo = $personajur->logo;

        $tformato = Tcatalogo::where('codtab','=','00032')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();

        $ttipoenvio = Tcatalogo::where('codtab','=','00037')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();  

        $ttipoinformacion = Tcatalogo::where('codtab','=','00052')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();  

        $ttipodestino = Tcatalogo::where('codtab','=','00053')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();  

        $transconfigcontac=array();
        if($configuracion){
            $ttransconfigcontac= DB::table('ttransmittalconfiguracioncontacto as con')
            ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','con.ctipocontacto')
            ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','con.ccontactocargo')
            ->leftJoin('tunidadmineracontactos as ucon','con.cunidadmineracontacto','=','ucon.cunidadmineracontacto')
            ->where('ctransconfiguracion','=',$configuracion->ctransconfiguracion)
            ->whereNull('cpersona_contacroanddes')
            ->select('con.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
            ->get();
            foreach($ttransconfigcontac as $con){
                $nroFila++;
                $c['ctransmittalconfiguracioncontacto']=$con->ctransmittalconfiguracioncontacto;
                $c['cunidadmineracontacto'] = $con->cunidadmineracontacto;
                $c['cpersona_contacroanddes'] = $con->cpersona_contacroanddes;
                $c['ttipoinformacion']=explode(',',$con->ctipoinformacioncontacto);
                $c['ttipodestino']=explode(',',$con->ctipodestino);
                if(!is_null($c['cunidadmineracontacto'])){
                    $umicon =DB::table('tunidadmineracontactos as ucon')
                    ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','ucon.ctipocontacto')
                    ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','ucon.ccontactocargo')
                    ->where('ucon.cunidadmineracontacto','=',$c['cunidadmineracontacto'])
                    ->select('ucon.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
                    ->first();
                    if($umicon){
                        $c['apaterno']=$umicon->apaterno;
                        $c['amaterno']=$umicon->amaterno;
                        $c['apellidos'] = $umicon->apaterno . " ".$umicon->amaterno;
                        $c['nombres'] = $umicon->nombres;
                        $c['contacto_cargo'] = $umicon->contacto_cargo;
                        $c['ccontactocargo']=$umicon->ccontactocargo;
                        $c['contacto']=$umicon->contacto;
                        $c['ctipocontacto']= $umicon->ctipocontacto;
                        //$c['ctipoinformacioncontacto']="";
                        $c['email'] = $umicon->email;
                    }
                }else{
                    $c['apellidos']=$con->apellidos;
                    $c['apaterno']='';
                    $c['amaterno']='';
                    $c['nombres'] = $con->nombres;
                    $c['contacto_cargo'] = $con->contacto_cargo;
                    $c['ccontactocargo']=$con->ccontactocargo;
                    $c['contacto']=$con->contacto;
                    $c['ctipocontacto']= $con->ctipocontacto;
                    //$c['ctipoinformacioncontacto']="";
                    $c['email'] = $con->email;
                }

                $transconfigcontac[count($transconfigcontac)] = $c;
            }
        }
        $ttiposcontacto = DB::table('ttiposcontacto')->lists('descripcion','ctipocontacto');
        $ttiposcontacto = [''=>''] + $ttiposcontacto;
        $tcontactocargo = DB::table('tcontactocargo')->lists('descripcion','ccontactocargo');
        $tcontactocargo = [''=>''] + $tcontactocargo;        
        //Session::put('configContactos',$transconfigcontac);
        /* Contactos registrados de Unidad Minera*/
        $contactUM = array();
        $tcontactUM = DB::table('tunidadmineracontactos as con')
        ->where('cunidadminera','=',$proyecto->cunidadminera)
        ->get();
        foreach($tcontactUM as $con){
                $cn['cunidadmineracontacto']=$con->cunidadmineracontacto;
                $cn['cunidadminera']=$con->cunidadminera;
                $cn['apaterno']=$con->apaterno;
                $cn['amaterno']=$con->amaterno;
                $cn['nombres']=$con->nombres;
                $cn['ctipocontacto']=$con->ctipocontacto;
                $cn['ccontactocargo']=$con->ccontactocargo;
                $cn['email']=$con->email;
                $cn['telefono']=$con->telefono;
                $cn['anexo']=$con->anexo;
                $cn['cel1']=$con->cel1;
                $cn['cel2']=$con->cel2;
                $contactUM[count($contactUM)]=$cn;
        }
        /* Fin de Contactos registrados de Unidad Minera*/
        return view('cdocumentario.configTransmittalUsuario',compact('proyecto','contactUM','persona','uminera','personajur','configuracion','logo','tformato','ttipoenvio','estadoproyecto','transconfigcontac','ttiposcontacto','tcontactocargo','ttipoinformacion','ttipodestino','nroFila'));
        
    }

    public function addContactoTransmittalProyectoUM(Request $request){
        $transconfigcontac = array();
        /*if(Session::get('configContactos')){
            $transconfigcontac=Session::get('configContactos');

        }*/
        $nroFila = $request->input('nroFilaUM');
        $nroFilaI = $nroFila;
        $sele = $request->input('conum');
        $tcontactoUM = DB::table('tunidadmineracontactos as con')
        ->whereIn('cunidadmineracontacto',$sele)
        ->get();
        foreach($tcontactoUM as $con){
            $des_cargo="";
            $des_contacto="";
            if(strlen($con->ctipocontacto )>0){
                $tcontacto = DB::table('ttiposcontacto')
                ->where('ctipocontacto','=',$con->ctipocontacto)
                ->first();
                if($tcontacto){
                    $des_contacto = $tcontacto->descripcion;
                }
            }
            if(strlen($con->ccontactocargo )>0){
                $tcargo = DB::table('tcontactocargo')
                ->where('ccontactocargo','=',$con->ccontactocargo)
                ->first();
                if($tcargo){
                    $des_cargo = $tcargo->descripcion;
                }
            }        
            $i=-1*(count($transconfigcontac)+1)*10;
            $c['ctransmittalconfiguracioncontacto']=$i;
            $c['cunidadmineracontacto']= $con->cunidadmineracontacto;
            $c['cpersona_contacroanddes']= null;
            $c['apaterno']=$con->apaterno;
            $c['amaterno']=$con->amaterno;
            $c['apellidos'] = $c['apaterno']. " ".$c['amaterno'];
            $c['nombres']=$con->nombres;
            $c['contacto_cargo'] = $des_cargo;
            $c['ccontactocargo']=$con->ccontactocargo;
            $c['contacto']=$des_contacto;
            $c['ctipocontacto']= $con->ctipocontacto;
            $c['ttipoinformacion']="";
            $c['ttipodestino']="";
            $c['email'] = $con->email;   
            $nroFila++;     
            /* agregar campos */

            $transconfigcontac[count($transconfigcontac)] = $c;
        }
        $ttipoinformacion = Tcatalogo::where('codtab','=','00052')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();  

        $ttipodestino = Tcatalogo::where('codtab','=','00053')
        ->whereNotNull('digide')
        ->lists('descripcion','digide')->all();  
        //Session::put('configContactos',$transconfigcontac);

        return view('partials.lineaConfiguracionTransmittalContactoProyecto',compact('nroFila','nroFilaI','transconfigcontac','ttipoinformacion','ttipodestino'));
    }

    public function modalContactoTransmittalProyecto(Request $request,$id){
            $ttransconfigcontac= DB::table('ttransmittalconfiguracioncontacto as con')
            ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','con.ctipocontacto')
            ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','con.ccontactocargo')
            /*->leftJoin('tunidadmineracontactos as ucon','con.cunidadmineracontacto','=','ucon.cunidadmineracontacto')*/
            ->where('ctransmittalconfiguracioncontacto','=',$id)
            ->select('con.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
            ->first();
            $tconf = array();
            if($ttransconfigcontac){
                $tconf['ctransmittalconfiguracioncontacto']=$ttransconfigcontac->ctransmittalconfiguracioncontacto;
                $tconf['cunidadmineracontacto'] = $ttransconfigcontac->cunidadmineracontacto;
                if(!(is_null($tconf['cunidadmineracontacto']))){
                    $umincon =DB::table('tunidadmineracontactos as ucon')
                    ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','ucon.ctipocontacto')
                    ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','ucon.ccontactocargo')
                    ->where('ucon.cunidadmineracontacto','=',$tconf['cunidadmineracontacto'])
                    ->select('ucon.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
                    ->first();
                    if($umicon){
                        $c['apaterno']=$umicon->apaterno;
                        $c['amaterno']=$umicon->amaterno;
                        $c['apellidos'] = $umincon->apaterno . " ".$umincon->amaterno;
                        $c['nombres'] = $umicon->nombres;
                        $c['contacto_cargo'] = $umicon->contacto_cargo;
                        $c['ccontactocargo']=$umicon->ccontactocargo;
                        $c['contacto']=$umincon->contacto;
                        $c['ctipocontacto']= $umincon->ctipocontacto;
                        $c['email'] = $umincon->email;
                        $c['ctipoinformacioncontacto']="";
                    }
                }else{
                    $tconf['apellidos']=$ttransconfigcontac->apellidos;
                    $tconfc['apaterno']='';
                    $tconf['amaterno']='';
                    $tconfc['nombres'] = $ttransconfigcontac->nombres;
                    $tconf['contacto_cargo'] = $ttransconfigcontac->contacto_cargo;
                    $tconf['ccontactocargo']=$ttransconfigcontac->ccontactocargo;
                    $tconf['contacto']=$ttransconfigcontac->contacto;
                    $tconf['ctipocontacto']= $ttransconfigcontac->ctipocontacto;
                    $tconf['email'] = $ttransconfigcontac->email;
                    $tconf['ctipoinformacioncontacto']="";
                }
            }
            return Response::json($tconf);


    }
    public function addContactoTransmittalProyecto(Request $request){
        $transconfigcontac = array();
        if(Session::get('configContactos')){
            $transconfigcontac=Session::get('configContactos');

        }
        $des_cargo="";
        $des_contacto="";
        if(strlen($request->input('tipocontacto') )>0){
            $tcontacto = DB::table('ttiposcontacto')
            ->where('ctipocontacto','=',$request->input('tipocontacto'))
            ->first();
            if($tcontacto){
                $des_contacto = $tcontacto->descripcion;
            }
        }
        if(strlen($request->input('contactocargo') )>0){
            $tcargo = DB::table('tcontactocargo')
            ->where('ccontactocargo','=',$request->input('contactocargo'))
            ->first();
            if($tcargo){
                $des_cargo = $tcargo->descripcion;
            }
        }        
        $i=-1*(count($transconfigcontac)+1)*10;
        $c['ctransmittalconfiguracioncontacto']=$i;
        $c['cunidadmineracontacto']=null;
        $c['apaterno']=$request->input('apaterno');
        $c['amaterno']=$request->input('amaterno');
        $c['apellidos'] = $c['apaterno']. " ".$c['amaterno'];
        $c['nombres']=$request->input('nombres');
        $c['contacto_cargo'] = $des_cargo;
        $c['ccontactocargo']=$request->input('contactocargo');
        $c['contacto']=$des_contacto;
        $c['ctipocontacto']= $request->input('tipocontacto');
        $c['ctipoinformacioncontacto']="";
        $c['email'] = $request->input('email');          
        /* agregar campos */

        $transconfigcontac[count($transconfigcontac)] = $c;

        Session::put('configContactos',$transconfigcontac);
        return view('partials.tableConfiguracionTransmittalContactoProyecto',compact('transconfigcontac',''));
    }
    public function delContactoTransmittalProyecto(Request $request,$idContacto){
        // SIN USO
        $transconfigcontac = array();
        if(Session::get('configContactos')){
            $transconfigcontac = Session::get('configContactos');

        }
        $temp = array();
        foreach($transconfigcontac as $con){
            if(!($con['ctransmittalconfiguracioncontacto']==$idContacto)){
                $temp[count($temp)]=$con;
            }                

        }
        if($idContacto>0){
            $del = Ttransmittalconfiguracioncontacto::where('ctransmittalconfiguracioncontacto','=',$idContacto);
            $del->delete();
        }        
        $transconfigcontac = $temp;        
        Session::put('configContactos',$transconfigcontac);
        return view('partials.tableConfiguracionTransmittalContactoProyecto',compact('transconfigcontac'));
    }    
    public function grabarConfiguracionTRUser(Request $request){

        $cproyecto=$request->input('cproyecto');
        $ctransconfiguracion = $request->input('ctransconfiguracion');
        DB::beginTransaction();
        if(strlen($ctransconfiguracion)<=0){
            $ttransconfig = new Ttransmittalconfiguracion();
            $ttransconfig->cproyecto=$cproyecto;
            $ttransconfig->ctipo='2';
        }else{
            $ttransconfig = Ttransmittalconfiguracion::where('ctransconfiguracion','=',$ctransconfiguracion)->first();
        }
        

        //tformato
        if($request->input('formato')!=''){
            $ttransconfig->formato = $request->input('formato');
        }
        //tipo de emvio
        if($request->input('tipoenvio')!=''){
            $ttransconfig->tipoenvio = $request->input('tipoenvio');
        }        
        $ttransconfig->numero=$request->input('codtransmittal');
        $ttransconfig->save();

        //configuracion de contactos
        $contact = array();
        $contact = $request->input("con");
        //dd($contact);
        /*if(Session::get('configContactos')){
            $contact = Session::get('configContactos');
        }*/
        foreach($contact as $con){
            if($con[10]=='N'){
                if($con[2]<=0){ //ctransmittalconfiguracioncontacto
                    $contacto = new Ttransmittalconfiguracioncontacto();
                    $contacto->ctransconfiguracion=$ttransconfig->ctransconfiguracion;
                }else{
                    $contacto = Ttransmittalconfiguracioncontacto::where('ctransmittalconfiguracioncontacto','=',$con[2])->first();
                }
                if(!is_null($con[4])){ //cunidadmineracontacto
                    $contacto->cunidadmineracontacto=$con[4];
                }
                $contacto->apellidos=$con[5];
                $contacto->nombres= $con[6];
                $contacto->ctipocontacto=$con[7];
                $contacto->ccontactocargo=$con[8];
                $ctipo="";
                $destino="";
                if(isset($con[0])==1 ){
                    if (is_array($con[0])){
                        $ctipo= implode(",",$con[0]);
                    }else{
                        $ctipo= $con[0];
                    }
                }
                if (isset($con[1])==1){
                    if ( is_array($con[1])){
                        $destino= implode(",",$con[1]);
                    }else{
                        $destino= $con[1];
                    }         
                }
                $contacto->ctipoinformacioncontacto=$ctipo; //'ctipoinformacioncontacto'
                $contacto->ctipodestino = $destino; //ctipodestino
                $contacto->email=$con[9];
                $contacto->save();
            }else{
                if($con[2]>0){
                    $obj = Ttransmittalconfiguracioncontacto::where('ctransmittalconfiguracioncontacto','=',$con[2])->first();
                    $obj->delete();
                }
            }

        }        
        DB::commit();        
        return Redirect::route('editarConfiguracionTRUser',$cproyecto);
        
    }    

    public function consultaEDT(){
        return view('cdocumentario.consultaUsuarioEDT');
    }

    public function editarConsultaEDT(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        Session::forget('proyEdt');
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;   

        $tproyectoedt = array();
        $tedt= DB::table('tproyectoedt')
        ->where('cproyecto','=',$idProyecto)
        ->get();     
        foreach($tedt as $e){
            $edt['cproyectoedt'] = $e->cproyectoedt;
            $edt['cproyecto'] = $e->cproyecto;
            $edt['codigo'] = $e->codigo;
            $edt['descripcion'] = $e->descripcion;
            $edt['cproyectoedt_parent'] = $e->cproyectoedt_parent;
            $edt['tipo'] = $e->tipo;

            $tproyectoedt[count($tproyectoedt)] = $edt;
        }

        Session::put('proyEdt',$tproyectoedt);
        return view('cdocumentario.consultaUsuarioEDT',compact('proyecto','persona','uminera','personajur','estadoproyecto','tproyectoedt'));
    }
    public function addItemEDT(Request $request){
        $tproyectoedt = array();
        if(Session::get('proyEdt')){
            $tproyectoedt=Session::get('proyEdt');
        }
        $i = -1 * (count($tproyectoedt)+1)*10;
        $edt['cproyectoedt'] = $i;
        $edt['codigo'] = $request->input('codigo');
        $edt['descripcion'] = $request->input('descripcion');
        $edt['cproyectoedt_parent'] = null;
        $edt['tipo'] = "0";

        $tproyectoedt[count($tproyectoedt)] = $edt;
        Session::put('proyEdt',$tproyectoedt);
        return view('partials.tableEDTDocumentario',compact('tproyectoedt'));
    }
    public function delItemEDT(Request $request,$id){
        $tproyectoedt = array();
        if(Session::get('proyEdt')){
            $tproyectoedt = Session::get('proyEdt');

        }
        $temp = array();
        foreach($tproyectoedt as $con){
            if(!($con['cproyectoedt']==$id)){
                $temp[count($temp)]=$con;
            }                

        }
        if($id>0){
            $del = Tproyectoedt::where('cproyectoedt','=',$id);
            $del->delete();
        }        
        $tproyectoedt = $temp;        
        Session::put('proyEdt',$tproyectoedt);
        return view('partials.tableEDTDocumentario',compact('tproyectoedt'));
    } 
    public function grabarConsultaEDT(Request $request){
        DB::beginTransaction();
        $cproyecto = $request->input('cproyecto');
        if(Session::get('proyEdt')){
            $tproyedt = Session::get('proyEdt');
        }
        foreach($tproyedt as $edt){
            if($edt['cproyectoedt']<0){
                $proyedt  = new Tproyectoedt();
                $proyedt->cproyecto= $cproyecto;
            }else{
                $proyedt  = Tproyectoedt::where('cproyectoedt','=',$edt['cproyectoedt'])->first();
            }
            $proyedt->codigo = $edt['codigo'];
            $proyedt->descripcion = $edt['descripcion'];
            if(!is_null($edt['cproyectoedt_parent'])){
                $proyedt->cproyectoedt_parent= $edt['cproyectoedt_parent'];
            }
            $proyedt->tipo = $edt['tipo'];
            $proyedt->save();
        }
        
        DB::commit();
        return Redirect::route('editarConsultaEDT',$cproyecto);
        
    }

    public function listaEntregableCD(){
        return view('cdocumentario.listaEntregablesUsuarioEDT');
    }

    public function editarListaEntregableCD(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto; 

        $proyectoentregables = array();
        Session::forget('proyent');
        $tproyectoentregables= DB::table('tproyectoentregables as pe')
        ->leftJoin('tproyectoedt as edt','pe.cproyectoedt','=','edt.cproyectoedt')
        ->leftJoin('testadoentregable as est_ent','pe.cestadoentregable','=','est_ent.cestadoentregable')
        ->leftJoin('tentregables as ent','pe.centregable','=','ent.centregables')
        ->leftJoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')        
        ->leftJoin('ttiposentregables as tent','ent.ctipoentregable','=','tent.ctiposentregable')
        ->where('pe.cproyecto','=',$idProyecto)
        ->select('pe.*','ent.codigo as cod_entre','ent.descripcion','ent.ctipoentregable')
        ->addSelect('ent.cdisciplina','tent.descripcion as tipo_entregable','est_ent.descripcion as estado_ent')
        ->addSelect('edt.codigo as cod_edt','edt.descripcion as des_edt','dis.descripcion as disciplina')
        ->get();        
        foreach($tproyectoentregables as $pent){
            $e['cproyectoentregables'] = $pent->cproyectoentregables;
            $e['cproyecto']=$pent->cproyecto;
            $e['centregable'] = $pent->centregable;
            $e['cproyectoedt'] = $pent->cproyectoedt;
            $e['observacion'] = $pent->observacion;
            $e['cproyectoactividades'] = $pent->cproyectoactividades;
            $e['cpersona_responsable'] = $pent->cpersona_responsable;
            $e['crol_responsable'] = $pent->crol_responsable;
            $e['codigo'] = $pent->codigo;
            $e['cestadoentregable'] = $pent->cestadoentregable;
            $e['estado_ent'] = $pent->estado_ent;

            $e['cod_entre'] = $pent->cod_entre;
            $e['descripcion'] = $pent->descripcion;
            $e['ctipoentregable'] = $pent->ctipoentregable;
            $e['cdisciplina'] =$pent->cdisciplina;
            $e['disciplina'] =$pent->disciplina;
            $e['revision'] = $pent->revision;

            $e['cod_edt'] = $pent->cod_edt;
            $e['des_edt'] = $pent->des_edt;

            $e['tipo_entregable']=$pent->tipo_entregable;

            $tentfechas= DB::table('tproyectoentregablesfechas')
            ->where('cproyectoentregable','=',$pent->cproyectoentregables)
            ->where('tipofecha','=','1')
            ->get();
            foreach($tentfechas as $fent){
                $e['fecha'.trim($fent->revision)]=$fent->fecha;
            }
            $proyectoentregables[count($proyectoentregables)]=$e;
        }
        Session::put('proyent',$proyectoentregables);
        
        return view('cdocumentario.listaEntregablesUsuarioEDT',compact('proyecto','persona','uminera','personajur','estadoproyecto','proyectoentregables'));
    }
    public function viewResumenTR(Request $request){
        $proyecto = DB::table('tproyecto')
        ->where('cproyecto','=',$request->input('cproyecto'))->first();

        $listTr=array();
        $documentos = array();
        $sel = $request->input("sel");
        
        $tentregables = DB::table('tproyectoentregables as pe')
        ->join('tentregables as ent','pe.centregable','=','ent.centregables')
        ->whereIn('pe.cproyectoentregables',$sel)
        ->select('pe.*','ent.descripcion')
        ->get();
        foreach($tentregables as $t){
            $doc=array();
            $doc['cproyectoentregables'] = $t->cproyectoentregables;
            $doc['descripcion'] = $t->descripcion;
            $doc['codigo'] = $t->codigo;
            $ttransmittal = DB::table('ttransmittalejecuciondetalle as det')
            ->join('ttransmittalejecucion as cab','cab.ctransmittalejecucion','=','det.ctransmittalejecucion')
            ->join('ttransmittalconfiguracion as conf','conf.ctransconfiguracion','=','cab.ctransconfiguracion')
            ->where('det.cproyectoentregables','=',$t->cproyectoentregables)
            ->select('conf.numero','cab.nrotrasmittal','cab.fecha','det.revision')
            ->get();
            $doc['trs']=array();
            $w=0;
            foreach($ttransmittal as $tr){
                $w++;
                $det=array();
                $det['item']=$w;
                $det['nrotrasmittal']=$tr->nrotrasmittal;
                $det['fecha']=$tr->fecha;
                $det['revision']=$tr->revision;
                $doc['trs'][count($doc['trs'])]=$det;
            }
            $documentos[count($documentos)] = $doc;
        }

        return view('cdocumentario.modalResumenTR',compact('documentos','proyecto'));
    }

    public function getConfiguracionContacto($idProyecto,$configuracion){

        $transconfigcontac= array();
        if($configuracion){


            $ttransconfigcontac= DB::table('ttransmittalconfiguracioncontacto as con')
            ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','con.ctipocontacto')
            ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','con.ccontactocargo')
            /*->leftJoin('tunidadmineracontactos as ucon','con.cunidadmineracontacto','=','ucon.cunidadmineracontacto')*/
            ->where('ctransconfiguracion','=',$configuracion->ctransconfiguracion)
            ->whereNull('cpersona_contacroanddes')
            ->select('con.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
            ->get();
            foreach($ttransconfigcontac as $con){
                $c['ctransmittalconfiguracioncontacto']=$con->ctransmittalconfiguracioncontacto;
                $c['cunidadmineracontacto'] = $con->cunidadmineracontacto;
                if(!is_null($c['cunidadmineracontacto'])){
                    $umicon =DB::table('tunidadmineracontactos as ucon')
                    ->leftJoin('ttiposcontacto as tc','tc.ctipocontacto','=','ucon.ctipocontacto')
                    ->leftJoin('tcontactocargo as tca','tca.ccontactocargo','=','ucon.ccontactocargo')
                    ->where('ucon.cunidadmineracontacto','=',$c['cunidadmineracontacto'])
                    ->select('ucon.*','tc.descripcion as contacto','tca.descripcion as contacto_cargo')
                    ->first();
                    if($umicon){
                        $c['apaterno']=$umicon->apaterno;
                        $c['amaterno']=$umicon->amaterno;
                        $c['apellidos'] = $umicon->apaterno . " ".$umicon->amaterno;
                        $c['nombres'] = $umicon->nombres;
                        $c['contacto_cargo'] = $umicon->contacto_cargo;
                        $c['ccontactocargo']=$umicon->ccontactocargo;
                        $c['contacto']=$umicon->contacto;
                        $c['ctipocontacto']= $umicon->ctipocontacto;
                        $c['ctipoinformacioncontacto']="";
                        $c['email'] = $umicon->email;
                    }
                }else{
                    $c['apellidos']=$con->apellidos;
                    $c['apaterno']='';
                    $c['amaterno']='';
                    $c['nombres'] = $con->nombres;
                    $c['contacto_cargo'] = $con->contacto_cargo;
                    $c['ccontactocargo']=$con->ccontactocargo;
                    $c['contacto']=$con->contacto;
                    $c['ctipocontacto']= $con->ctipocontacto;
                    $c['ctipoinformacioncontacto']="";
                    $c['email'] = $con->email;
                }

                $transconfigcontac[count($transconfigcontac)] = $c;
            }
        }
        return $transconfigcontac;
    }
    public function agregaraTR(Request $request){
        $idProyecto = $request->input('cproyecto');
        $sel = $request->input('sel');
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        Session::forget('ejecuDeta');
        Session::forget('DelEjecuDeta');
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;   

        $numero_sec=1;
        $tproyecto_adi= DB::table('tproyectoinformacionadicional')
        ->where('cproyecto','=',$idProyecto)
        ->first();     
        $per_cdocu = "";
        if($tproyecto_adi){
            $per_cdocu = $tproyecto_adi->cpersona_cdocumentario;
        }

        /* configuración y contacto */
        $configuracion = DB::table('ttransmittalconfiguracion')
        ->where('cproyecto','=',$idProyecto)
        ->first();       
        $ttrans = DB::table('ttransmittalejecucion as cab')
        ->where('ctransconfiguracion','=',$configuracion->ctransconfiguracion)
        ->select(DB::raw('max(right(nrotrasmittal,3)) as maximo'))
        ->first();
        if($ttrans){
            $numero_sec = intVal($ttrans->maximo)+1;
        }         
        $transconfigcontac = $this->getConfiguracionContacto($idProyecto,$configuracion);

        $tipoenvio=DB::table('tcatalogo')
        ->where('codtab','=','00037')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('descripcion','digide'); 
        $ejecu_deta=array();


        /* fin de configuracion y contacto */

        /* Entregables del Proyecto*/
        $tentregables=DB::table('tproyectoentregables as ent')
        ->join('tentregables as t','ent.centregable','=','t.centregables')
        ->where('ent.cproyecto','=',$idProyecto)
        ->lists('t.descripcion','ent.cproyectoentregables');
        $tentregables = [''=>'']+ $tentregables ;
        
        /* Fin de proyectos entregables*/
        $objEmp = new EmpleadoSupport();
        $personal_cd = $objEmp->getPersonalArea_cbx('27');
        $numero_sec=str_pad($numero_sec,3,'0',STR_PAD_LEFT);

        /*Detalle de Ejecucion*/
        $tdetalleejecucion = DB::table('tproyectoentregables as pryent')
        ->leftJoin('tentregables as ent','ent.centregables','=','pryent.centregable')
        ->whereIn('pryent.cproyectoentregables',$sel)
        ->select('pryent.*','ent.descripcion')
        ->get();
        $w=0;
        foreach($tdetalleejecucion as $deta){
            $w++;
            $d=array();
            $d['ctransmittalejedetalle']='';
            $d['cproyectoentregables']=$deta->cproyectoentregables;
            $d['ctransmittalejecucion'] = '';
            $d['item'] = $w;
            $d['codigo'] = $deta->codigo;
            $d['descripcion'] = $deta->descripcion;
            $d['revision'] = "A";
            $d['cantidad'] = 1;

            $ejecu_deta[count($ejecu_deta)]=$d;
        }
        /* fin Detalle de ejecucion */
        Session::put('ejecuDeta',$ejecu_deta);


        return view('cdocumentario.generarTransmittalUsuario',compact('proyecto','persona','uminera','personajur','estadoproyecto','configuracion','transconfigcontac','tipoenvio','personal_cd','per_cdocu','ejecu_deta','tentregables','numero_sec'));
    }

    public function grabarListaEntregableCD(Request $request){
        DB::beginTransaction();
        $cproyecto = $request->input('cproyecto');
        $ent = $request->input('ent');
        foreach($ent as $e){
            if(strlen($e[0])>0){
                $obj = Tproyectoentregable::where('cproyectoentregables','=',$e[0])->first();
                $obj->codigo=$e[1];
                $obj->revision=$e[2];
                $obj->save();
            }
        }
        DB::commit();
        
        return Redirect::route('editarListaEntregableCD',$cproyecto);
    }

    public function revisionDocumentos(){
        return view('cdocumentario.revisionDocumentosUsuario');
    }
    public function editarRevisionDocumentos(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;        

        $categoriadoc = Tcategoriaentregable::lists('descripcion','ccategoriaentregable')
        ->all();
        
       
        return view('cdocumentario.revisionDocumentosUsuario',compact('proyecto','persona','uminera','personajur','estadoproyecto','categoriadoc'));
    }
    public function verRevisionesDoc(Request $request){
        $categoriadoc = $request->input('categoriadoc');
        $idProyecto = $request->input('cproyecto');
        $tproyectoentregables= DB::table('tproyectoentregables as pe')
        ->leftJoin('tproyectoedt as edt','pe.cproyectoedt','=','edt.cproyectoedt')
        ->leftJoin('testadoentregable as est_ent','pe.cestadoentregable','=','est_ent.cestadoentregable')
        ->leftJoin('tentregables as ent','pe.centregable','=','ent.centregables')
        ->leftJoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')        
        ->leftJoin('ttiposentregables as tent','ent.ctipoentregable','=','tent.ctiposentregable')
        ->where('pe.cproyecto','=',$idProyecto)
        ->where('pe.ccategoriaentregable','=',$categoriadoc)
        ->select('pe.*','ent.codigo as cod_entre','ent.descripcion','ent.ctipoentregable')
        ->addSelect('ent.cdisciplina','tent.descripcion as tipo_entregable','est_ent.descripcion as estado_ent')
        ->addSelect('edt.codigo as cod_edt','edt.descripcion as des_edt','dis.descripcion as disciplina')
        ->get(); 
        $listRevDoc=array();
        foreach($tproyectoentregables as $pent){
            $e['cproyectoentregables'] = $pent->cproyectoentregables;
            $e['cproyecto']=$pent->cproyecto;
            $e['centregable'] = $pent->centregable;
            $e['cproyectoedt'] = $pent->cproyectoedt;
            $e['observacion'] = $pent->observacion;
            $e['cproyectoactividades'] = $pent->cproyectoactividades;
            $e['cpersona_responsable'] = $pent->cpersona_responsable;
            $e['crol_responsable'] = $pent->crol_responsable;
            $e['codigo'] = $pent->codigo;
            $e['cestadoentregable'] = $pent->cestadoentregable;
            $e['estado_ent'] = $pent->estado_ent;

            $e['cod_entre'] = $pent->cod_entre;
            $e['descripcion'] = $pent->descripcion;
            $e['ctipoentregable'] = $pent->ctipoentregable;
            $e['cdisciplina'] =$pent->cdisciplina;
            $e['disciplina'] =$pent->disciplina;

            $e['cod_edt'] = $pent->cod_edt;
            $e['des_edt'] = $pent->des_edt;
            $e['revision'] = $pent->revision;

            $e['tipo_entregable']=$pent->tipo_entregable;

            $tentfechas= DB::table('tproyectoentregablesfechas')
            ->where('cproyectoentregable','=',$pent->cproyectoentregables)
            ->where('tipofecha','=','1')
            ->get();
            foreach($tentfechas as $fent){
                $e['fecha'.trim($fent->revision)]=$fent->fecha;
            }
            $listRevDoc[count($listRevDoc)]=$e;
        }        

        
        return view('partials.tableRevisionesDocumentos',compact('listRevDoc'));
    }
    public function grabarRevisionDocumentos(){
        return view('cdocumentario.revisionDocumentosUsuario');
    }
    public function generacionTR(){
        return view('cdocumentario.generarTransmittalUsuario');
    }
    public function editarGeneracionTR(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        Session::forget('ejecuDeta');
        Session::forget('DelEjecuDeta');
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;   

        $numero_sec=1;
        $tproyecto_adi= DB::table('tproyectoinformacionadicional')
        ->where('cproyecto','=',$idProyecto)
        ->first();     
        $per_cdocu = "";
        if($tproyecto_adi){
            $per_cdocu = $tproyecto_adi->cpersona_cdocumentario;
        }

        /* configuración y contacto */
        $configuracion = DB::table('ttransmittalconfiguracion')
        ->where('cproyecto','=',$idProyecto)
        ->first();                
        $ttrans = DB::table('ttransmittalejecucion as cab')
        ->where('ctransconfiguracion','=',$configuracion->ctransconfiguracion)
        ->where('ctipo','=','1')
        ->select(DB::raw('max(right(nrotrasmittal,3)) as maximo'))
        ->first();
        if($ttrans){
            $numero_sec = intVal($ttrans->maximo)+1;
        }        
        $transconfigcontac = $this->getConfiguracionContacto($idProyecto,$configuracion);
        $tipoenvio=DB::table('tcatalogo')
        ->where('codtab','=','00037')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('descripcion','digide'); 
        $ejecu_deta=array();
        Session::put('ejecuDeta',$ejecu_deta);

        /* fin de configuracion y contacto */

        /* Entregables del Proyecto*/
        $tentregables=DB::table('tproyectoentregables as ent')
        ->join('tentregables as t','ent.centregable','=','t.centregables')
        ->where('ent.cproyecto','=',$idProyecto)
        ->lists('t.descripcion','ent.cproyectoentregables');
        $tentregables = [''=>'']+ $tentregables ;
        
        /* Fin de proyectos entregables*/
        $objEmp = new EmpleadoSupport();
        $personal_cd = $objEmp->getPersonalArea_cbx('27');
        $numero_sec=str_pad($numero_sec,3,'0',STR_PAD_LEFT);
        return view('cdocumentario.generarTransmittalUsuario',compact('proyecto','persona','uminera','personajur','estadoproyecto','configuracion','transconfigcontac','tipoenvio','personal_cd','per_cdocu','ejecu_deta','tentregables','numero_sec'));
    }
    public function obtenerTransmittal($idTransmittal){

        $transmittalejecucion =DB::table('ttransmittalejecucion as eje')
        ->where('eje.ctransmittalejecucion','=',$idTransmittal)
        ->first();
        $transmittalconf = DB::table('ttransmittalconfiguracion as conf')
        ->where('conf.ctransconfiguracion','=',$transmittalejecucion->ctransconfiguracion)
        ->first();

        $idProyecto= $transmittalconf->cproyecto;
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$transmittalconf->cproyecto)
        ->first();
        Session::forget('ejecuDeta');
        Session::forget('DelEjecuDeta');

        $numero_sec = substr($transmittalejecucion->nrotrasmittal,-3);

        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;   
  
        $per_cdocu = "";
        if($transmittalejecucion){
            $per_cdocu = $transmittalejecucion->cpersona_cdoc;
        }

        /* configuración y contacto */
        $configuracion = DB::table('ttransmittalconfiguracion')
        ->where('cproyecto','=',$idProyecto)
        ->first();                
        $transconfigcontac = $this->getConfiguracionContacto($idProyecto,$configuracion);

        $tipoenvio=DB::table('tcatalogo')
        ->where('codtab','=','00037')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('descripcion','digide');  
        $ejecu_deta=array();
        


        /* fin de configuracion y contacto */

        /*Detalle de Ejecucion*/
        $tdetalleejecucion = DB::table('ttransmittalejecuciondetalle as deta')
        ->where('deta.ctransmittalejecucion','=',$transmittalejecucion->ctransmittalejecucion)
        ->get();
        foreach($tdetalleejecucion as $deta){
            $d['ctransmittalejedetalle']=$deta->ctransmittalejedetalle;
            $d['cproyectoentregables']=$deta->cproyectoentregables;
            $d['ctransmittalejecucion'] = $deta->ctransmittalejecucion;
            $d['item'] = $deta->item;
            $d['codigo'] = $deta->codcliente;
            $d['descripcion'] = $deta->descripcion;
            $d['revision'] = $deta->revision;
            $d['cantidad'] = $deta->cantidad;

            $ejecu_deta[count($ejecu_deta)]=$d;
        }
        /* fin Detalle de ejecucion */


        /* Entregables del Proyecto*/
        $tentregables=DB::table('tproyectoentregables as ent')
        ->join('tentregables as t','ent.centregable','=','t.centregables')
        ->where('ent.cproyecto','=',$idProyecto)
        ->lists('t.descripcion','ent.cproyectoentregables');
        $tentregables = [''=>'']+ $tentregables ;
        
        /* Fin de proyectos entregables*/
        $objEmp = new EmpleadoSupport();
        $personal_cd = $objEmp->getPersonalArea_cbx('27');
        Session::put('ejecuDeta',$ejecu_deta);

        return view('cdocumentario.generarTransmittalUsuario',compact('numero_sec','proyecto','persona','uminera','personajur','estadoproyecto','configuracion','transconfigcontac','transmittalejecucion','tipoenvio','personal_cd','per_cdocu','tentregables','ejecu_deta'));


        

    }
    
    public function grabarGeneracionTR(Request $request){

        DB::beginTransaction();
        $ctransmittalejecucion = $request->input('ctransmittalejecucion');
        $cproyecto = $request->input('cproyecto');
        $configuracion = DB::table('ttransmittalconfiguracion')
        ->where('cproyecto','=',$cproyecto)
        ->first();
        if(strlen($ctransmittalejecucion)<=0){
            $ejecu = new Ttransmittalejecucion();
            $ejecu->ctipo='1';
            $ejecu->fecha=Carbon::now();

        }else{
            $ejecu = Ttransmittalejecucion::where('ctransmittalejecucion','=',$ctransmittalejecucion)->first();
        }
        $ejecu->tipoenvio=$request->input('tipoenvio'); //ver tipo envio
        $ejecu->nrotrasmittal =$configuracion->numero."-". $request->input('secuencia');
        $ejecu->cpersona_cdoc = $request->input('personal_cd');
        $ejecu->ctransconfiguracion = $configuracion->ctransconfiguracion;
        $ejecu->save();
        $ctransmittalejecucion = $ejecu->ctransmittalejecucion;
        $ejecu_detalle  = array();
        if(Session::get('ejecuDeta')){
            $ejecu_detalle = Session::get('ejecuDeta');
        }
        foreach($ejecu_detalle as $det){
            if($det['ctransmittalejedetalle']<=0){
                $deta = new Ttransmittalejecuciondetalle();
                $deta->ctransmittalejecucion = $ejecu->ctransmittalejecucion;

            }else{
                $deta = Ttransmittalejecuciondetalle::where('ctransmittalejedetalle','=',$det['ctransmittalejedetalle'])->first();
            }
            $deta->item = $det['item'];
            $deta->codcliente = $det['codigo'];
            $deta->codanddes = $det['codigo'];
            $deta->descripcion = $det['descripcion'];
            $deta->revision = $det['revision'];
            $deta->cantidad = $det['cantidad'];
            if(is_null($deta->cproyectoentregables)){
                if(strlen($det['cproyectoentregables'])>0){
                    $deta->cproyectoentregables = $det['cproyectoentregables'];
                }
            }else{
                if(strlen($det['cproyectoentregables'])>0){
                    $deta->cproyectoentregables = $det['cproyectoentregables'];
                }else{
                    $deta->cproyectoentregables=null;
                }
            }
            
            $deta->save(); 
            
        }



        /* Eliminar Items de Detalle 
        */
        if(Session::get('DelEjecuDeta')){
            $eliminar = Session::get('DelEjecuDeta');
            foreach($eliminar as $e){
                if($e>0){
                    $obj = Ttransmittalejecuciondetalle::where('ctransmittalejedetalle','=',$e)->first();
                    $obj->delete();
                }


            }
        }
  
        /*Fin Eliminar Items de Detalle
        */

        DB::commit();
        return Redirect::route('obtenerTransmittal',$ctransmittalejecucion);
    }
    public function emitirGeneracionTR(Request $request){
        return "";
    }

    public function printedGeneracionTR(Request $request){
        $ctran = $request->input('ctransmittalejecucion');
        if(strlen($ctran)>0){

            $trancab = DB::table('ttransmittalejecucion as cab')
            ->where('ctransmittalejecucion','=',$ctran)
            ->first();
            $obj= new GastosPdf('P');   
            $obj->AddPage();     
            $obj->SetDrawColor(179,179,179);
            $logo = storage_path('archivos')."/images/logorpt.jpg";
            $obj->Image($logo,15,10,50,19);

            $obj->SetFont('Arial','B',10); 
            $obj->SetXY(15,10);
            $obj->Cell(50,19,'',1,0,'C');
            
            $obj->SetXY(75,10);
            $obj->MultiCell(60,5,"\n"."CONTROL DOCUMENTARIO\n Transmittal",0,'C');
            $obj->Rect(65,10,80,19);

            $obj->SetXY(145,10);
            $obj->Cell(50,9.5,'',1,0,'C');

            $fec=date('d-m-Y');
            $obj->SetXY(145,19.5);
            $obj->MultiCell(50,5,"10-AND-27-FOR-0401 / R1 / 04-10-16",0,'C');
            $obj->Rect(145,19.5,50,9.5);  
            $obj->setX(15);
            $obj->Cell(40,5,"Cliente:",1,0,'L');
            $obj->Cell(50,5,"",1,0,'L');
            $obj->Cell(40,5,"Transmittal:",1,0,'L');
            $obj->Cell(50,5,$trancab->nrotrasmittal,1,0,'L');
            $obj->Ln();

            $obj->setX(15);
            $obj->Cell(40,5,utf8_decode("Atención:"),1,0,'L');
            $obj->Cell(50,5,"",1,0,'L');
            $obj->Cell(40,5,"Proyecto:",1,0,'L');
            $obj->Cell(50,5,"",1,0,'L');
            $obj->Ln();


            $obj->setX(15);
            $obj->Cell(40,5,"C.C.:",1,0,'L');
            $obj->Cell(50,5,"",1,0,'L');
            $obj->Cell(40,5,"Referencia:",1,0,'L');
            $obj->Cell(50,5,"",1,0,'L');
            $obj->Ln();

            $obj->setX(15);
            $obj->Cell(40,5,"Envio:",1,0,'L');
            $obj->Cell(50,5,"",1,0,'L');
            $obj->Cell(20,5,"Codigo:",1,0,'L');
            $obj->Cell(35,5,"",1,0,'L');
            $obj->Cell(15,5,"Fecha:",1,0,'L');
            $obj->Cell(20,5,date('d-m-Y'),1,0,'L');            
            $obj->Ln();   
            $obj->setX(15);
            $obj->SetFillColor(0 , 105, 170);
            $obj->SetTextColor(255, 255, 255);
            $obj->Cell(20,5,"Nro",1,0,'L',true);
            $obj->Cell(50,5,"Documento Nro",1,0,'L',true);
            $obj->Cell(10,5,"Rev.",1,0,'L',true);
            $obj->Cell(70,5,utf8_decode("Titulo o Descripción"),1,0,'L',true);
            $obj->Cell(10,5,"Tipo",1,0,'L',true);
            $obj->Cell(20,5,"Cantidad",1,0,'L',true);


			$obj->Output('D');
			exit;                        

        }
    }
    public function modalItemGeneraTR(Request $request,$id){
        $datos=array();
        if(Session::get('ejecuDeta')){
            $datos = Session::get('ejecuDeta');
        }
        $item = array();
        if(strlen($id) > 0 ){
            foreach($datos as $d){
                if($d['ctransmittalejedetalle']==$id){
                    $item['cproyectoentregables'] = $d['cproyectoentregables'];
                    $item['ctransmittalejedetalle'] = $d['ctransmittalejedetalle'];
                    $item['item']=$d['item'];
                    $item['codigo']=$d['codigo'];
                    $item['descripcion']=$d['descripcion'];
                    $item['revision']=$d['revision'];
                    $item['cantidad']=$d['cantidad'];
                }
            }

        }
        return Response::json($item);
    }
    public function eliminarItemGenerarTR(Request $request,$id){
        $ejecu_deta=array();
        $delEjecuDeta=array();
        if(Session::get('ejecuDeta')){
            $ejecu_deta = Session::get('ejecuDeta');
        }
        if(Session::get('DelEjecuDeta')){
            $delEjecuDeta = Session::get('DelEjecuDeta');
        }
        $temp = array();
        foreach($delEjecuDeta as $det){
            if(!($det['ctransmittalejedetalle']==$id)){
                $temp[count($temp)] = $det;
            }
        }
        if($id>0){
            $delEjecuDeta[count($delEjecuDeta)]=$id;
        }
        $ejecu_deta = $temp;
        Session::put('DelEjecuDeta',$delEjecuDeta);
        Session::put('ejecuDeta',$ejecu_deta);
        return view('partials.tableDetalleGenTransmittal',compact('ejecu_deta'));
    }
    public function saveItemGeneraTR(Request $request){
        $ejecu_deta=array();
        if(Session::get('ejecuDeta')){
            $ejecu_deta = Session::get('ejecuDeta');
        }
        
        if(strlen($request->input('ctransmittalejedetalle'))>0){
            $res = array_search($request->input('ctransmittalejedetalle'), array_map(function($e){return $e['ctransmittalejedetalle'];},$ejecu_deta));
            if($res===false){
                $i= -1 *(count($ejecu_deta)+1)*10;
                $item['ctransmittalejedetalle']= $i;            

                $item['cproyectoentregables']=$request->input('cproyectoentregables');
                $item['item']=0;

                $item['codigo']=$request->input('codigo');
                $item['descripcion']=$request->input('descripcion');
                $item['revision']=$request->input('revision');
                $item['cantidad']=$request->input('cantidad');
                $ejecu_deta[count($ejecu_deta)] =  $item;

            }else{

                $item =$ejecu_deta[$res];

                if(strlen($item['cproyectoentregables'])>0){
                    if(strlen($request->input('cproyectoentregables')>0)){
                        $item['cproyectoentregables']= $request->input('cproyectoentregables');
                    }else{
                        $item['cproyectoentregables']=null;
                    }

                }else{
                    if(strlen($request->input('cproyectoentregables')>0)){
                        $item['cproyectoentregables']= $request->input('cproyectoentregables');
                    }

                }
                $item['item']=$request->input('item');
                $item['codigo']=$request->input('codigo');
                $item['descripcion']=$request->input('descripcion');
                $item['revision']=$request->input('revision');
                $item['cantidad']=$request->input('cantidad');
                
                $ejecu_deta[$res] = $item;

            }
        }else{
                $i= -1 *(count($ejecu_deta)+1)*10;
                $item['ctransmittalejedetalle']= $i;            
                $item['cproyectoentregables']= $request->input('cproyectoentregables');
                $item['item']=0;
                $item['codigo']=$request->input('codigo');
                $item['descripcion']=$request->input('descripcion');
                $item['revision']=$request->input('revision');
                $item['cantidad']=$request->input('cantidad');
                $ejecu_deta[count($ejecu_deta)] =  $item;
        }
        $j=1;
        
        /*foreach($ejecu_deta as $k => $det){
            $det['item'] = $j;
            $ejecu_deta[$res] =  $det;
            $i++;
        }*/
        
        Session::put('ejecuDeta',$ejecu_deta);
        return view('partials.tableDetalleGenTransmittal',compact('ejecu_deta'));
    }
    public function listarTR(){
            return Datatables::queryBuilder(DB::table('tproyecto as pry')
            ->join('ttransmittalconfiguracion as conf','pry.cproyecto','=','conf.cproyecto')
            ->join('ttransmittalejecucion as eje','eje.ctransconfiguracion','=','conf.ctransconfiguracion')
            ->where('eje.ctipo','=','1')
            ->select('pry.nombre as proyecto','pry.codigo','conf.numero','eje.nrotrasmittal as secuencia',DB::raw('CONCAT(\'row_\',eje.ctransmittalejecucion)  as "DT_RowId"'))
            )->make(true);  
    }
    public function listarRecTR(){
            return Datatables::queryBuilder(DB::table('tproyecto as pry')
            ->join('ttransmittalconfiguracion as conf','pry.cproyecto','=','conf.cproyecto')
            ->join('ttransmittalejecucion as eje','eje.ctransconfiguracion','=','conf.ctransconfiguracion')
            ->where('eje.ctipo','=','2')
            ->select('pry.nombre as proyecto','pry.codigo','conf.numero','eje.nrotrasmittal as secuencia',DB::raw('CONCAT(\'row_\',eje.ctransmittalejecucion)  as "DT_RowId"'))
            )->make(true);  
    }
    public function recepcionTR(){
        return view('cdocumentario.generarDocumento');
    }

    public function editarRecepcionTR(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        Session::forget('ejecuDetaRec');
        Session::forget('DelEjecuDetaRec');        
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;  
        $tproyecto_adi= DB::table('tproyectoinformacionadicional')
        ->where('cproyecto','=',$idProyecto)
        ->first();     
        $per_cdocu = "";
        if($tproyecto_adi){
            $per_cdocu = $tproyecto_adi->cpersona_cdocumentario;
        }

        /* configuración y contacto */
        $configuracion = DB::table('ttransmittalconfiguracion')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        
        $tipoenvio=DB::table('tcatalogo')
        ->where('codtab','=','00037')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('descripcion','digide'); 

        $tresultado=DB::table('tcatalogo')
        ->where('codtab','=','00036')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('descripcion','digide');  
        $tresultado = [''=>''] + $tresultado;

        $trevision=DB::table('tcatalogo')
        ->where('codtab','=','00009')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('valor','digide');    
        $trevision = [''=>''] + $trevision;

        $ejecu_deta=array();
        Session::put('ejecuDeta',$ejecu_deta);

        /* fin de configuracion y contacto */

        /* Entregables del Proyecto*/
        $tentregables=DB::table('tproyectoentregables as ent')
        ->join('tentregables as t','ent.centregable','=','t.centregables')
        ->where('ent.cproyecto','=',$idProyecto)
        ->lists('t.descripcion','ent.cproyectoentregables');
        $tentregables = [''=>'']+ $tentregables ;
        
        /* Fin de proyectos entregables*/
        $objEmp = new EmpleadoSupport();
        $personal_cd = $objEmp->getPersonalArea_cbx('27');              
        return view('cdocumentario.generarDocumento',compact('proyecto','persona','uminera','personajur','estadoproyecto','configuracion','tipoenvio','personal_cd','per_cdocu','ejecu_deta','tentregables','trevision','tresultado'));
    }
    public function obtenerTransmittalRecepcion($idTransmittal){

        $transmittalejecucion =DB::table('ttransmittalejecucion as eje')
        ->where('eje.ctransmittalejecucion','=',$idTransmittal)
        ->first();
        $transmittalconf = DB::table('ttransmittalconfiguracion as conf')
        ->where('conf.ctransconfiguracion','=',$transmittalejecucion->ctransconfiguracion)
        ->first();

        $idProyecto= $transmittalconf->cproyecto;
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$transmittalconf->cproyecto)
        ->first();
        Session::forget('ejecuDetaRec');
        Session::forget('DelEjecuDetaRec');



        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;   
  
        $per_cdocu = "";
        if($transmittalejecucion){
            $per_cdocu = $transmittalejecucion->cpersona_cdoc;
        }

        /* configuración y contacto */
        $configuracion = DB::table('ttransmittalconfiguracion')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        
        $tipoenvio=DB::table('tcatalogo')
        ->where('codtab','=','00037')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('descripcion','digide'); 
        $ejecu_deta=array();

        $tresultado=DB::table('tcatalogo')
        ->where('codtab','=','00036')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('descripcion','digide');  
        $tresultado = [''=>''] + $tresultado;

        $trevision=DB::table('tcatalogo')
        ->where('codtab','=','00009')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->lists('valor','digide');    
        $trevision = [''=>''] + $trevision;        
        


   
        /*Detalle de Ejecucion*/
        $tdetalleejecucion = DB::table('ttransmittalejecuciondetalle as deta')
        ->where('deta.ctransmittalejecucion','=',$transmittalejecucion->ctransmittalejecucion)
        ->get();
        foreach($tdetalleejecucion as $deta){
            $d['ctransmittalejedetalle']=$deta->ctransmittalejedetalle;
            $d['cproyectoentregables']=$deta->cproyectoentregables;
            $d['ctransmittalejecucion'] = $deta->ctransmittalejecucion;
            $d['item'] = $deta->item;
            $d['codigocliente'] = $deta->codcliente;
            $d['codigoanddes'] = $deta->codanddes;
            $d['descripcion'] = $deta->descripcion;
            $d['revision'] = $deta->revision;
            $des = DB::table('tcatalogo')
            ->where('codtab','=','00009')
            ->where('digide','=',trim($deta->revision))
            ->whereNotNull('digide')
            ->first();
            $d['revision_des'] = $des->descripcion;

            $d['resultado'] = trim($deta->cresultado);
            $des = DB::table('tcatalogo')
            ->where('codtab','=','00036')
            ->where('digide','=',trim($deta->cresultado))
            ->whereNotNull('digide')
            ->first();            
            $d['resultado_des'] = $des->descripcion;
            $d['descripcion'] = $deta->descripcion;
            $d['comentarios'] = $deta->comentarios;
            $d['cantidad'] = $deta->cantidad;

            $ejecu_deta[count($ejecu_deta)]=$d;
        }
        /* fin Detalle de ejecucion */


        /* Entregables del Proyecto*/
        $tentregables=DB::table('tproyectoentregables as ent')
        ->join('tentregables as t','ent.centregable','=','t.centregables')
        ->where('ent.cproyecto','=',$idProyecto)
        ->lists('t.descripcion','ent.cproyectoentregables');
        $tentregables = [''=>'']+ $tentregables ;
        
        /* Fin de proyectos entregables*/
        $objEmp = new EmpleadoSupport();
        $personal_cd = $objEmp->getPersonalArea_cbx('27');
        Session::put('ejecuDetaRec',$ejecu_deta);

        return view('cdocumentario.generarDocumento',compact('proyecto','persona','uminera','personajur','estadoproyecto','configuracion','transmittalejecucion','tipoenvio','personal_cd','per_cdocu','tentregables','ejecu_deta','tresultado','trevision'));


        

    }

    public function obtenerTransmittalEnvidado($idTransmittal){

        $transmittalejecucion =DB::table('ttransmittalejecucion as eje')
        ->where('eje.ctransmittalejecucion','=',$idTransmittal)
        ->first();
        $transmittalconf = DB::table('ttransmittalconfiguracion as conf')
        ->where('conf.ctransconfiguracion','=',$transmittalejecucion->ctransconfiguracion)
        ->first();

        $idProyecto= $transmittalconf->cproyecto;
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$transmittalconf->cproyecto)
        ->first();


        
        $ejecu_deta=array();
        if(Session::get('ejecuDetaRec')){
            $ejecu_deta = Session::get('ejecuDetaRec');
        }
        /*Detalle de Ejecucion*/
        $tdetalleejecucion = DB::table('ttransmittalejecuciondetalle as deta')
        ->where('deta.ctransmittalejecucion','=',$transmittalejecucion->ctransmittalejecucion)
        ->get();
        foreach($tdetalleejecucion as $deta){
            $d['ctransmittalejedetalle']=$deta->ctransmittalejedetalle;
            $d['cproyectoentregables']=$deta->cproyectoentregables;
            $d['ctransmittalejecucion'] = $deta->ctransmittalejecucion;
            $d['item'] = $deta->item;

            $d['codigocliente'] = $deta->codcliente;
            $d['codigoanddes'] = $deta->codanddes;
            $d['descripcion'] = $deta->descripcion;
            $d['revision'] = $deta->revision;
            $des = DB::table('tcatalogo')
            ->where('codtab','=','00009')
            ->where('digide','=',trim($deta->revision))
            ->whereNotNull('digide')
            ->first();
            $d['revision_des'] = $des->descripcion;

        
            $d['resultado_des'] = '';

            $d['cantidad'] = $deta->cantidad;

            $ejecu_deta[count($ejecu_deta)]=$d;
        }
        /* fin Detalle de ejecucion */



        return view('partials.tableDetalleRecepTransmittal',compact('ejecu_deta'));


        

    }
    public function grabarRecepcionTR(Request $request){
        DB::beginTransaction();
        $ctransmittalejecucion = $request->input('ctransmittalejecucion');
        $cproyecto = $request->input('cproyecto');
        $configuracion = DB::table('ttransmittalconfiguracion')
        ->where('cproyecto','=',$cproyecto)
        ->first();
        if(strlen($ctransmittalejecucion)<=0){
            $ejecu = new Ttransmittalejecucion();
            $ejecu->ctipo='2';
            $ejecu->fecha=Carbon::now();

        }else{
            $ejecu = Ttransmittalejecucion::where('ctransmittalejecucion','=',$ctransmittalejecucion)->first();
        }
        $ejecu->tipoenvio='1'; //ver tipo envio
        $ejecu->nrotrasmittal = $request->input('secuencia');
        $ejecu->cpersona_cdoc = $request->input('personal_cd');
        $ejecu->ctransconfiguracion = $configuracion->ctransconfiguracion;
        $ejecu->save();
        $ctransmittalejecucion = $ejecu->ctransmittalejecucion;
        $ejecu_detalle  = array();
        if(Session::get('ejecuDetaRec')){
            $ejecu_detalle = Session::get('ejecuDetaRec');
        }
        foreach($ejecu_detalle as $det){
            if($det['ctransmittalejedetalle']<=0){
                $deta = new Ttransmittalejecuciondetalle();
                $deta->ctransmittalejecucion = $ejecu->ctransmittalejecucion;

            }else{
                $deta = Ttransmittalejecuciondetalle::where('ctransmittalejedetalle','=',$det['ctransmittalejedetalle'])->first();
            }
            $deta->item = $det['item'];
            $deta->codcliente = $det['codigocliente'];
            $deta->codanddes = $det['codigoanddes'];
            $deta->descripcion = $det['descripcion'];
            $deta->revision = $det['revision'];
            $deta->cresultado = $det['resultado'];
            $deta->comentarios = $det['comentarios'];
            $deta->cantidad = $det['cantidad'];
            if(is_null($deta->cproyectoentregables)){
                if(strlen($det['cproyectoentregables'])>0){
                    $deta->cproyectoentregables = $det['cproyectoentregables'];
                }
            }else{
                if(strlen($det['cproyectoentregables'])>0){
                    $deta->cproyectoentregables = $det['cproyectoentregables'];
                }else{
                    $deta->cproyectoentregables=null;
                }
            }

            
            $deta->save(); 
            
        }



        /* Eliminar Items de Detalle 
        */
        if(Session::get('DelEjecuDetaRec')){
            $eliminar = Session::get('DelEjecuDetaRec');
            foreach($eliminar as $e){
                if($e>0){
                    $obj = Ttransmittalejecuciondetalle::where('ctransmittalejedetalle','=',$e)->first();
                    $obj->delete();
                }


            }
        }
  
        /*Fin Eliminar Items de Detalle
        */

        DB::commit();
        
        return Redirect::route('obtenerTransmittalRecepcion',$ctransmittalejecucion);
    }    
    public function modalItemRecepcionTR(Request $request,$id){
        $datos=array();
        if(Session::get('ejecuDetaRec')){
            $datos = Session::get('ejecuDetaRec');
        }
        $item = array();
        if(strlen($id) > 0 ){
            foreach($datos as $d){
                if($d['ctransmittalejedetalle']==$id){
                    $item['cproyectoentregables'] = $d['cproyectoentregables'];
                    $item['ctransmittalejedetalle'] = $d['ctransmittalejedetalle'];
                    $item['item']=$d['item'];
                    $item['codigocliente']=$d['codigocliente'];
                    $item['codigoanddes']=$d['codigoanddes'];
                    $item['descripcion']=$d['descripcion'];
                    $item['revision']=$d['revision'];
                    $item['revision_des']=$d['revision_des'];
                    $item['resultado_des']=$d['resultado_des'];                    
                    $item['resultado']=$d['resultado'];
                    $item['comentarios']=$d['comentarios'];
                    $item['cantidad']=$d['cantidad'];
                }
            }

        }
        return Response::json($item);
    }
    public function eliminarItemRecepcionTR(Request $request,$id){
        $ejecu_deta=array();
        $delEjecuDeta=array();
        if(Session::get('ejecuDetaRec')){
            $ejecu_deta = Session::get('ejecuDetaRec');
        }
        if(Session::get('DelEjecuDetaRec')){
            $delEjecuDeta = Session::get('DelEjecuDetaRec');
        }
        $temp = array();
        foreach($delEjecuDeta as $det){
            if(!($det['ctransmittalejedetalle']==$id)){
                $temp[count($temp)] = $det;
            }
        }
        if($id>0){
            $delEjecuDeta[count($delEjecuDeta)]=$id;
        }
        $ejecu_deta = $temp;
        Session::put('DelEjecuDeta',$delEjecuDeta);
        Session::put('ejecuDeta',$ejecu_deta);
        return view('partials.tableDetalleRecepTransmittal',compact('ejecu_deta'));
    }
    public function saveItemRecepcionTR(Request $request){
        $ejecu_deta=array();
        if(Session::get('ejecuDetaRec')){
            $ejecu_deta = Session::get('ejecuDetaRec');
        }
        
        if(strlen($request->input('ctransmittalejedetalle'))>0){
            $res = array_search($request->input('ctransmittalejedetalle'), array_map(function($e){return $e['ctransmittalejedetalle'];},$ejecu_deta));
            if($res===false){
                $i= -1 *(count($ejecu_deta)+1)*10;
                $item['ctransmittalejedetalle']= $i;            

                $item['cproyectoentregables']=$request->input('cproyectoentregables');
                $item['item']=0;

                $item['codigoanddes']=$request->input('codigoanddes');
                $item['codigocliente']=$request->input('codigocliente');
                $item['descripcion']=$request->input('descripcion');
                $item['revision']=$request->input('revision');
                $des = DB::table('tcatalogo')
                ->where('codtab','=','00009')
                ->where('digide','=',$request->input('revision'))
                ->whereNotNull('digide')
                ->first();
                $item['revision_des'] = $des->descripcion;

                $des = DB::table('tcatalogo')
                ->where('codtab','=','00036')
                ->where('digide','=',$request->input('resultado'))
                ->whereNotNull('digide')
                ->first();            
                $d['resultado_des'] = $des->descripcion;

                $item['resultado']=$request->input('resultado');
                $item['comentarios']=$request->input('comentarios');
                $item['cantidad']=$request->input('cantidad');
                $ejecu_deta[count($ejecu_deta)] =  $item;

            }else{

                $item =$ejecu_deta[$res];

                if(strlen($item['cproyectoentregables'])>0){
                    if(strlen($request->input('cproyectoentregables')>0)){
                        $item['cproyectoentregables']= $request->input('cproyectoentregables');
                    }else{
                        $item['cproyectoentregables']=null;
                    }

                }else{
                    if(strlen($request->input('cproyectoentregables')>0)){
                        $item['cproyectoentregables']= $request->input('cproyectoentregables');
                    }

                }
                $item['item']=$request->input('item');
                $item['codigoanddes']=$request->input('codigoanddes');
                $item['codigocliente']=$request->input('codigocliente');
                $item['descripcion']=$request->input('descripcion');
                $item['revision']=$request->input('revision');
                $des = DB::table('tcatalogo')
                ->where('codtab','=','00009')
                ->where('digide','=',$request->input('revision'))
                ->whereNotNull('digide')
                ->first();
                $item['revision_des'] = $des->descripcion;

                $des = DB::table('tcatalogo')
                ->where('codtab','=','00036')
                ->where('digide','=',$request->input('resultado'))
                ->whereNotNull('digide')
                ->first();            
                $item['resultado_des'] = $des->descripcion;     
                $item['resultado']=$request->input('resultado');
                $item['comentarios']=$request->input('comentarios');                           
                $item['cantidad']=$request->input('cantidad');
                
                $ejecu_deta[$res] = $item;

            }
        }else{
                $i= -1 *(count($ejecu_deta)+1)*10;
                $item['ctransmittalejedetalle']= $i;            
                $item['cproyectoentregables']= $request->input('cproyectoentregables');
                $item['item']=0;
                $item['codigoanddes']=$request->input('codigoanddes');
                $item['codigocliente']=$request->input('codigocliente');
                $item['descripcion']=$request->input('descripcion');
                $item['revision']=$request->input('revision');
                $des = DB::table('tcatalogo')
                ->where('codtab','=','00009')
                ->where('digide','=',$request->input('revision'))
                ->whereNotNull('digide')
                ->first();
                $item['revision_des'] = $des->descripcion;

                $des = DB::table('tcatalogo')
                ->where('codtab','=','00036')
                ->where('digide','=',$request->input('resultado'))
                ->whereNotNull('digide')
                ->first();            
                $item['resultado_des'] = $des->descripcion;     
                $item['resultado']=$request->input('resultado');
                $item['comentarios']=$request->input('comentarios');                
                $item['cantidad']=$request->input('cantidad');
                $ejecu_deta[count($ejecu_deta)] =  $item;
        }
        $j=1;
        
        /*foreach($ejecu_deta as $k => $det){
            $det['item'] = $j;
            $ejecu_deta[$res] =  $det;
            $i++;
        }*/
        
        Session::put('ejecuDetaRec',$ejecu_deta);
        return view('partials.tableDetalleRecepTransmittal',compact('ejecu_deta'));
    }
    public function RegistroDOCsnTR(){
        return view('errors.enConstruccion');
    }

    public function editarRegistroDOCsnTR(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;        
        return view('errors.enConstruccion',compact('proyecto','persona','uminera','personajur','estadoproyecto'));
    }

    public function grabarRegistroDOCsnTR(){
        return view('errors.enConstruccion');
    }        
    public function planificacionCD(){
        return view('cdocumentario.planificacionCDUsuario');
    }
    public function editarPlanificacionCD(Request $request,$idProyecto){
        $proyecto = DB::table('tproyecto as p')
        ->where('cproyecto','=',$idProyecto)
        ->first();
        $persona  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$proyecto->cpersonacliente)
        ->where('cunidadminera','=',$proyecto->cunidadminera)->first();

        $personajur = DB::table('tpersonajuridicainformacionbasica')->where('cpersona','=',$proyecto->cpersonacliente)->first();

        $estadoproyecto = Testadoproyecto::lists('descripcion','cestadoproyecto')->all();
        $estadoproyecto = [''=>''] + $estadoproyecto;        
        return view('cdocumentario.planificacionCDUsuario',compact('proyecto','persona','uminera','personajur','estadoproyecto'));
    }
    public function grabarPlanificacionCD(){
        return view('cdocumentario.planificacionCDUsuario');
    }        


    public function entregablexcd(Request $request){

        $fecha = $request->fecha;

        $cds = DB::table('tproyectoentregablesfechas as pfe')
        ->join('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        // ->join('tproyecto as tp','tpe.cproyecto','=','tp.cproyecto')
        // ->join('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->leftjoin('tproyectoinformacionadicional as tpia','tpia.cproyecto','=','tpe.cproyecto')
        ->leftjoin('tpersona as cd','cd.cpersona','=','tpia.cpersona_cdocumentario')
        ->select('cd.cpersona','cd.abreviatura','pfe.fecha')
        ->where(DB::raw("to_char(pfe.fecha,'YYYY-MM-DD')"),'=',$fecha)
        ->distinct('cd.cpersona','cd.abreviatura','pfe.fecha')
        ->get();

        $cd = [];
        $si = 0;
        $no = 0;
        $si_suma = 0;
        $no_suma = 0;
// dd($cds);
        foreach ($cds as $key => $value) {
            $cant = DB::table('tproyectoentregablesfechas as pef')
            ->join('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pef.cproyectoentregable')  
            ->leftjoin('tproyectoinformacionadicional as tpia','tpia.cproyecto','=','tpe.cproyecto')
            ->where('tpia.cpersona_cdocumentario','=',$value->cpersona)
            ->where('fecha','=',$value->fecha)
            ->lists('cproyectoentregable');
            
            $canti_emi = DB::table('tproyectoentregables as pe')
            ->leftjoin('tproyectoinformacionadicional as tpia','tpia.cproyecto','=','pe.cproyecto')
            // ->leftjoin('tpersona as cd','cd.cpersona','=','tpia.cpersona_cdocumentario')
            ->where('tpia.cpersona_cdocumentario','=',$value->cpersona)
            ->where('ruteo','=','CD')
            ->where('cestadoentregableruteo','=','APR')
            ->whereIn('cproyectoentregables',$cant)->count();

            $canti_tra = DB::table('tproyectoentregables as pe')
            ->leftjoin('tproyectoinformacionadicional as tpia','tpia.cproyecto','=','pe.cproyecto')
            // ->leftjoin('tpersona as cd','cd.cpersona','=','tpia.cpersona_cdocumentario')
            ->where('tpia.cpersona_cdocumentario','=',$value->cpersona)
            ->where('ruteo','=','CD')
            ->where('cestadoentregableruteo','=','REC')
            ->whereIn('cproyectoentregables',$cant)->count();

            foreach ($cant as $id => $val) {
                $tra = DB::table('ttransmittal as tr')
                ->join('ttransmittaldetalle as trd','trd.ctransmittal','=','tr.ctransmittal')
                ->where('trd.cproyectoentregables','=',$val)
                ->first();
                // dd($tra);
                if ($tra) {
                    if ($tra->fechaenvio == $value->fecha) {
                        $si += 1;
                    }else {
                        $no +=1;
                    }
                }else {
                    $no +=1;
                }
            $six = $si*100/count($cant);
            $nox = $no*100/count($cant);
            }

        $si_suma += $six*100/count($cds);
        $no_suma += $nox*100/count($cds);

        $c['abreviatura'] = $value->abreviatura;
        $c['fecha'] = $value->fecha;
        $c['cantidad_plani'] = count($cant);
        $c['cantidad_entre_apr'] = $canti_emi;
        $c['cantidad_entre_rec'] = $canti_tra;
        $c['cumplio'] = $six;
        $c['nocumplio'] = $nox;
        $c['suma_cumplio'] = $si_suma;
        $c['suma_nocumplio'] = $no_suma;

        array_push($cd,$c);
        }
        // dd($cant);

        return $cd;

    }
    
}
