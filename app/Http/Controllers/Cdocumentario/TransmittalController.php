<?php

namespace App\Http\Controllers\Cdocumentario;

use Illuminate\Http\Request;
use App\Erp\FlujoLE;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Yajra\Datatables\Facades\Datatables;
use App\Models\Ttransmittalconfiguracion;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tcatalogo;
use App\Models\Ttransmittalejecucion;
use App\Models\Testadoproyecto;
use App\Models\Tproyecto;
use App\Models\Tproyectoedt;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesfecha;
use App\Models\Tproyectoinformacionadicional;
use App\Models\Ttransmittalejecuciondetalle;
use App\Models\Ttransmittalconfiguracioncontacto;
use App\Models\Tcategoriaentregable;
use App\Models\Tmotivoenvioentregable;
use App\Models\Transmittal;
use App\Models\Transmittaldetalle;
use App\Models\Transmittalcontacto;
use App\Models\Tunidadmineracontacto;
use App\Models\Tproyectoentregablesruteo;
use App\Erp\ProyectoSupport;
use App\Erp\EmpleadoSupport;
use App\Erp\HelperSIGSupport;
use App\Erp\GastosPdf;
use Carbon\Carbon;
use Session;
use Response;
use DB;
use Redirect;
use Auth;


class TransmittalController extends Controller
{
    //*****  Obtiene el listado de todos los proyectos  *****//
    public function listarProyectosTr(){

        $proyectos=DB::table('tproyecto')
            ->join('testadoproyecto as te','tproyecto.cestadoproyecto','=','te.cestadoproyecto')
            ->join('tpersona as tper','tproyecto.cpersonacliente','=','tper.cpersona')
            ->join('tunidadminera as tu','tproyecto.cunidadminera','=','tu.cunidadminera')
            ->leftJoin('tpersona as p','tproyecto.cpersona_gerente','=','p.cpersona')
            ->leftJoin('tpersona as c','tproyecto.cpersona_coordinador','=','c.cpersona')
            ->where('tproyecto.cestadoproyecto','=','001')
            ->select('tproyecto.cproyecto','tproyecto.codigo','tper.nombre as cliente','tu.nombre as uminera','tproyecto.nombre', 
            'p.abreviatura as GteProyecto','te.descripcion')->get();

        return view('transmittal/tablaListaProyectos',compact('proyectos'));

    }

    private function valorNull($valor, $defecto=null)
    {
        if($valor!=null or $valor!="")
            return $valor;
        return $defecto;
    }

    //*****  Muestra la vita de Listado de Transmittals  *****//
    public function listado_transmittal_documento(){

        return view('transmittal/listaTRxDocumento');
    }


    //*****  Muestra la vista de Listado de Transmittals de un Proyecto seleccionado  *****//
    public function cargar_transmittal_proyecto_seleccionado(Request $request,$idProyecto){

    	$proyecto=$this->obtener_datos_proyecto($idProyecto);
        $listaEntregables=$this->listarTodosEntregables($idProyecto,'todos','sin_grupo');
        //dd($listaEntregables);
        $revisiones = Tcatalogo::where('codtab','=','00006')
            ->whereNotNull('digide')
            ->orderBy(DB::raw("to_number(digide,'999.99999999')"),'ASC')
            ->lists('valor','digide')->all();

        $motivo = Tmotivoenvioentregable::lists('descripcion','cmotivoenvio')->all();

    	return view('transmittal/listaTRxDocumento',
            ['proyecto'=>$proyecto[0],
             'entTodosTR'=>$listaEntregables,
             'revisiones'=>$revisiones, 
             'motivo'=>$motivo
            ]);
    	
    }


    //*****  Obtiene datos del Proyecto seleccionado  *****//
    private function obtener_datos_proyecto($idProyecto){

    	$proyecto = DB::table('tproyecto')->where('cproyecto','=',$idProyecto)->first();
        $cliente  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $estadoproyecto = Testadoproyecto::where('cestadoproyecto','=',$proyecto->cestadoproyecto)->first();
        $uminera = DB::table('tunidadminera')->where('cunidadminera','=',$proyecto->cunidadminera)->first();    	

        return array([ 	'cproyecto'=>$proyecto->cproyecto,
        				'cliente'=>$cliente->nombre,
        				'codproyecto'=>$proyecto->codigo,
        				'nombreproyecto'=>$proyecto->nombre,
        				'estadoproyecto'=>$estadoproyecto->descripcion,
        				'uminera'=>$uminera->nombre,
                        'cunidadminera'=>$uminera->cunidadminera,
                        'logouminera'=>$uminera->logo_unidadminera
        		]);

    }




    //*****  Devuelve Array todos los entregables del Proyecto seleccionado  *****//
    public function listarTodosEntregables($cproyecto,$vista,$ent_grupo){

        $documento = $this->obtenerRevisionEntregablesPryCli($cproyecto);
        $revisiones_internas=null;
        $crevision=null;
        if($documento){
            $revisiones_internas=$this->obtenerRevisionEntregablesPryInt($documento->cproyectodocumentos);
            $crevision=$revisiones_internas->cproyectoent_rev;
        }
        
        //$vista='modal_recepcion_ent';
        $listEnt=array();
        $pentrega = DB::table('tproyectoentregables as pryent')
            ->leftjoin('tentregables as ent','pryent.centregable','=','ent.centregables')
            ->leftjoin('ttipoproyectoentregable as tpe','pryent.ctipoproyectoentregable','=','tpe.ctipoproyectoentregable')
            ->leftjoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
            ->leftjoin('tdisciplinaareas as tda','dis.cdisciplina','=','tda.cdisciplina')
            ->leftjoin('tareas as tar','tda.carea','=','tar.carea')
            ->leftjoin('tproyecto as pry','pryent.cproyecto','=','pry.cproyecto')
            ->leftJoin('tactividad as act','pryent.cproyectoactividades','=','act.cactividad')
            ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
            ->leftJoin('tpersona as resp','pryent.cpersona_responsable','=','resp.cpersona')
            ->leftJoin('tpersona as ela','pryent.cper_elabora','=','ela.cpersona')
            ->leftJoin('tpersona as cli','pry.cpersonacliente','=','cli.cpersona')

            ->leftJoin('tpersona as rev','pryent.cpersona_revisor','=','rev.cpersona')
            ->leftJoin('tpersona as apr','pryent.cpersona_aprobador','=','apr.cpersona')

            ->leftJoin('testadoentregable as est','pryent.cestadoentregable','=','est.cestadoentregable')
            ->leftJoin('tdocumentosparaproyecto as tdp','ent.cdocumentoparapry','=','tdp.cdocumentoparapry')
            ->leftJoin('ttiposentregables as tip','ent.ctipoentregable','=','tip.ctiposentregable')
            ->leftJoin('tproyectoentregables as tenp','pryent.cproyectoentregables_parent','=','tenp.cproyectoentregables')
            //->orderBy('pryent.descripcion_entregable','ASC')
            //->orderBy('edt.descripcion','ASC')
            //->orderBy('pryent.cproyectoentregables','ASC')
            ->wherenotIn('pryent.revision',['0','1'])
            ->where('edt.estado','=','1')
            ->where('pryent.cproyecto','=',$cproyecto);

            if ($vista=='modal_agregar_ent') {
                $pentrega=$pentrega->where('pryent.cestadoentregableruteo','=','APR')
                                    ->where('pryent.ruteo','=','CD');

                                    //dd($cproyecto,$vista,$ent_grupo);

            if ($ent_grupo != 'sin_grupo' ) {

                $ent_grupo= explode(',', $ent_grupo);

                $pentrega=$pentrega->whereIn('pryent.cproyectoentregables',$ent_grupo);
               
            }


            }

            elseif ($vista=='modal_recepcion_ent') {
                $pentrega=$pentrega->where('pryent.cestadoentregableruteo','=','REC')
                                    ->where('pryent.ruteo','=','CD');
            }
            //
            $pentrega=$pentrega->where('tpe.ctipoproyectoentregable','=','2')
            ->where('edt.cproyectoent_rev','=',$crevision)
            ->select('pryent.*','pryent.descripcion_entregable as descripcion','dis.descripcion as des_dis','act.descripcion as des_act','dis.cdisciplina')
            ->addSelect('edt.descripcion as des_edt','tpe.descripcion as desc_tipoproyentre','pryent.ctipoproyectoentregable','resp.abreviatura as responsable','ela.abreviatura as elaborador','est.descripcion as estado','est.activo as activo','tip.descripcion as des_tipoentre','tenp.descripcion_entregable as parent','rev.abreviatura as revisor','apr.abreviatura as aprobador','pryent.codigo as codEnt','pry.codigo as codproy','edt.codigo as cedt','tar.codigo as codarea','ent.ctipoentregable','tdp.codigo as coddoc','edt.ctipoedt','cli.abreviatura as cliente','pryent.codigocliente as codCli','pryent.fecha_modificacion as fmodificacion_ent')
            ->get();   

            //dd($pentrega,$vista);         

        $fechasCab=DB::table('tproyectoentregablesfechas as pfe')  
            ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
            ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
            ->where('tpe.cproyecto','=',$cproyecto)
            ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec')
            ->orderBy('ttf.ctipofechasentregable','asc')
            ->distinct()
            ->get();

            

        $i=0;
        foreach($pentrega as $pent){
           $i++;
           $listE['item'] = $i;
           $listE['cproyectoentregables'] = $pent->cproyectoentregables;
           $listE['cproyecto'] = $pent->cproyecto;
           $listE['codproy'] = $pent->codproy;
           $listE['centregable'] = $pent->centregable;
           $listE['cproyectoedt'] = $pent->cproyectoedt;
           $listE['cedt'] = $pent->cedt;
           $listE['observacion'] = $pent->observacion;
           $listE['cproyectoactividades'] = $pent->cproyectoactividades;
           $listE['cpersona_responsable'] = $pent->cpersona_responsable;
           $listE['crol_responsable'] = $pent->crol_responsable;
           $listE['codigo'] = $pent->codEnt;
           $listE['codigoCliente'] = $pent->codCli;
           $listE['descripcion']= $pent->descripcion;
           $listE['ccategoriaentregable']= $pent->ccategoriaentregable;
           $listE['cdisciplina'] = $pent->cdisciplina;
           $listE['codarea'] = $pent->codarea;
           $listE['ctipoentregable'] = $pent->ctipoentregable;
           $listE['coddoc'] = $pent->coddoc;
           $listE['ctipoedt'] = $pent->ctipoedt;
           $listE['cliente'] = $pent->cliente;
        
           $listE['des_dis'] = $pent->des_dis;
           $listE['des_act'] = $pent->des_act;
           $listE['des_edt'] = $pent->des_edt;

           $listE['responsable'] = $pent->responsable;
           $listE['elaborador'] = $pent->elaborador;
           $listE['estado'] = $pent->estado;
           $listE['activo'] = $pent->activo;
           $listE['tipoentregable'] = $pent->des_tipoentre;
           $listE['tipoproyentregable'] = $pent->desc_tipoproyentre; 
           $listE['ctipoproyentregable'] = $pent->ctipoproyectoentregable; 
           $listE['aprobador'] = $pent->aprobador; 
           $listE['revisor'] = $pent->revisor; 
           $listE['fmodificacion_ent'] = $pent->fmodificacion_ent; 

           $listE['crevision'] = $pent->revision; 

           $revisionEnt=Tcatalogo::where('codtab','=','00006')
           ->where('digide','=',$pent->revision)
           ->first();
            
           $listE['des_revision'] = $revisionEnt->valor;           
           
           $fase = DB::table('tfasesproyecto_gestion')
            ->where('codigo','=',trim($pent->cfaseproyecto))

            ->first();
            $cfase=null;
            $descfase=null;

            if($fase){

                $cfase=$fase->codigo;
                $descfase=$fase->descripcion;

            }

            $listE['fase'] = $descfase; 
            $listE['cfaseproyecto'] = $cfase; 

            $anexos=DB::table('tproyectoentregables as pryent')
            ->where('pryent.cproyectoentregables_parent','=',$pent->cproyectoentregables)
            ->count();

            $listE['anexos'] = $anexos; 


            $transmittal_ultimo=DB::table('ttransmittal as tr')
            ->leftjoin('ttransmittaldetalle as trd','tr.ctransmittal','=','trd.ctransmittal')
            ->select('tr.*',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaemision"))
            ->where('trd.cproyectoentregables','=',$pent->cproyectoentregables)
            ->orderBy('tr.fechaenvio','desc')
            ->first();

            $listE['fechaemisionTR'] = $transmittal_ultimo? $transmittal_ultimo->fechaemision:'';

            //dd($transmittal_ultimo,$pent->cproyectoentregables);

          
           $afechas=array();
           $cantFechas='';

           foreach ($fechasCab as $fc) {

                $fechas=DB::table('tproyectoentregablesfechas as pfe')             
                    ->Join('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable') 
                    ->select('ttf.ctipofechasentregable','ttf.descripcion as tipofec',DB::raw("to_char(pfe.fecha,'DD-MM-YY') as fecha"))
                    ->where('pfe.cproyectoentregable','=',$pent->cproyectoentregables)
                    ->orderBy('ttf.ctipofechasentregable','asc')
                    ->get();  

                $cantFechas=count($fechas);

                $fec['fechaDesc']=$fc->tipofec;  
                $fec['fecha']=''; 
                $fec['ctipo']='';                    
            

                if($fechas){
               
                   foreach ($fechas as $f) {

                        if($f->ctipofechasentregable==$fc->ctipofechasentregable){
                            $fec['fecha']=$f->fecha;
                            $fec['ctipo']=$f->ctipofechasentregable;  
                        }
                            
                    }
                        $afechas[count($afechas)]=$fec;
                }

                else{
                        $afechas='SinFechas';
                }
           }

           $listE['fechas']=$afechas;
           $listE['cantidadFechas']=$cantFechas;

          
           $listEnt[count($listEnt)]=$listE;
         
        }
        //dd($listEnt);

        return $listEnt;

        
    }

    //*****  Devuelve Ultima revision- cliente del Proyecto seleccionado  *****//
    private function obtenerRevisionEntregablesPryCli($cproyecto){

        $revisiones = DB::table('tproyectodocumentos as pro')
            ->join('tdocumentosparaproyecto as prydoc','pro.cdocumentoparapry','=','prydoc.cdocumentoparapry')
            ->where('prydoc.codigo','=','LE')
            ->where('pro.cproyecto','=',$cproyecto)
            ->orderBy('fechadoc','desc')
            ->select('pro.cproyectodocumentos','pro.fechadoc','pro.codigodocumento as codigo','pro.revisionactual as revision')
            ->first();

        return $revisiones;

    }

    //*****  Devuelve Ultima revision- interna del Proyecto seleccionado  *****//

    private function obtenerRevisionEntregablesPryInt($cproyectodoc){

        $revisiones = DB::table('tproyectoentregablesrevisiones as pro')
            ->where('pro.cproyectodocumentos','=',$cproyectodoc)
            ->orderBy('fecha_rev','desc')
            ->first();

        return $revisiones;

    }

    //*****  Muestra la vista para registrar un nuevo TR  *****//

    public function generar_transmittal_nuevo($idProyecto,Request $request){
        // dd('entregablessssss',$request->all());

        $entregables_pendientes=$_GET['ent'];
        // $entregables_pendientes=$request->entregables;

        //dd($entregables_pendientes);

        $proyecto=$this->obtener_datos_proyecto($idProyecto);

        $listaEntregables=$this->listarTodosEntregables($idProyecto,'modal_agregar_ent',$entregables_pendientes);

        $revisiones = Tcatalogo::where('codtab','=','00006')
            ->whereNotNull('digide')
            ->orderBy(DB::raw("to_number(digide,'999.99999999')"),'ASC')
            ->lists('valor','digide')->all();

        $motivo = Tmotivoenvioentregable::lists('descripcion','cmotivoenvio')->all();

        $tipoenvio=Tcatalogo::where('codtab','=','00059')
            ->whereNotNull('digide')
            ->lists('descripcion','digide')->all();

        $tipoenvio = ['N'=>'Seleccione tipo de envío'] + $tipoenvio;

        $transmittal = Transmittal::get()->all();
       
        $personaCD= DB::table('tproyectopersona as per')      
        ->leftjoin('tpersona as tp','per.cpersona','=','tp.cpersona')  
        ->where('per.cproyecto','=',$idProyecto)
        ->where('per.cdisciplina','=','11')
        ->where('eslider','=','1')
        ->first();

        $fechaemision=Carbon::now()->format('d-m-Y');

        $numeroTR=$this->generarNumeroTR($idProyecto,'A-C','ACT');

        $transmittalsXProyecto = Transmittal::where('cproyecto','=',$idProyecto)
        ->where('tipotransmittal','=','A-C')
        ->lists('ctransmittal')->all();

        $ultimoTRconContacto = Transmittalcontacto::whereIn('ctransmittal',$transmittalsXProyecto)->orderBy('ctransmittal','desc')->lists('ctransmittal')->first();

        $contactosAtencion = $this->obtenerNombreContactos($ultimoTRconContacto,'001');
  
        $arrayCA = $contactosAtencion['codigoContacto'];
        $arrayCA_nom = $contactosAtencion['nombreContacto'];


        $contactosCC = $this->obtenerNombreContactos($ultimoTRconContacto,'002');
        $arrayCC=$contactosCC['codigoContacto'];
        $arrayCC_nom=$contactosCC['nombreContacto'];

        $estado_enviado='';


        return view('transmittal/generarTransmittal',
             ['proyecto'=>$proyecto[0],
             'listEnt'=>$listaEntregables,
             'revisiones'=>$revisiones, 
             'motivo'=>$motivo,
             'tipoenvio'=>$tipoenvio,
             'personaCD'=>$personaCD,
             'fechaemision'=>$fechaemision,
             'numeroTR'=>$numeroTR,
             'arrayCA'=>$arrayCA,
             'arrayCA_nom'=>$arrayCA_nom,
             'arrayCC'=>$arrayCC,
             'arrayCC_nom'=>$arrayCC_nom,
             'estado_enviado'=>$estado_enviado,
            ]);
    }

    //*****  Grabar TR  *****//

    public function transmittal_guardar(Request $request){

        // dd($request->all());

        $objflujoEmp = new FlujoLE();
        $flujo = $objflujoEmp->flujo();
        $flujo = explode(',',$flujo);

      
        DB::beginTransaction();

        if ($request->ctransmittal=='nuevo') {
            $transmittal= new Transmittal();
            $transmittal->created_user=Auth::user()->cpersona;
        }

        else{
            $transmittal=Transmittal::where('ctransmittal','=',$request->ctransmittal)->first();
        }


        $transmittal->cproyecto=$request->cproyecto;
        $transmittal->numero=$request->numeroTransmittal;
        $transmittal->tipotransmittal=$request->tipoTransmittal;
        $transmittal->tipoenvio=$this->valorNull($request->tipoenvio,null);;
        $transmittal->fechaenvio=$request->fechaenvio;
        $transmittal->updated_user=Auth::user()->cpersona;
        $transmittal->estado='ACT';
        $transmittal->estado_enviado='GEN';
        $transmittal->save();

        if ($request->cont_atencion != '') {
            $contactoum=explode(',', $request->cont_atencion);

            foreach ($contactoum as $c) {

                $contacto_CA = Transmittalcontacto::where('ctransmittalcontacto','=',$c)->first();

                if (!$contacto_CA) {

                    $contacto_CA = new Transmittalcontacto();
                    $contacto_CA->cunidadmineracontacto =$c;
                    $contacto_CA->ctransmittal =$transmittal->ctransmittal;
                    $contacto_CA->tipocontacto ='001';
                    $contacto_CA->save();

                }

            }
        }

        if ($request->cont_cc != '') {
            $contactoum=explode(',', $request->cont_cc);

            foreach ($contactoum as $c) {

                $contacto_CC = Transmittalcontacto::where('ctransmittalcontacto','=',$c)->first();
                if (!$contacto_CC) {

                    $contacto_CC= new Transmittalcontacto();
                    $contacto_CC->cunidadmineracontacto =$c;
                    $contacto_CC->ctransmittal =$transmittal->ctransmittal;
                    $contacto_CC->tipocontacto ='002';
                    $contacto_CC->save();

                }
            }
        }


        if (!is_null($request['detalleTransmittal'])) {
            
            for ($i = 0; $i < count(   $request['detalleTransmittal']); $i++) {

                if ($request['detalleTransmittal'][$i]['ctransmittaldetalle']=='nuevo'){
                    
                    $transmittal_detalle = new Transmittaldetalle;
                    $transmittal_detalle->updated_user=Auth::user()->cpersona;
                    $transmittal_detalle->created_user=Auth::user()->cpersona;
                    $transmittal_detalle->ctransmittal=$transmittal->ctransmittal;
                    if ($request['detalleTransmittal'][$i]['cproyectoentregables']!='nuevoEntCli') {
                        $transmittal_detalle->cproyectoentregables=$this->valorNull($request['detalleTransmittal'][$i]['cproyectoentregables'],null);
                    }
                    $transmittal_detalle->codigo_entregable=$this->valorNull($request['detalleTransmittal'][$i]['codigo'],null);
                    $transmittal_detalle->revision_entregable=$this->valorNull($request['detalleTransmittal'][$i]['revision'],null);
                    $transmittal_detalle->descripcion_entregable=$request['detalleTransmittal'][$i]['descripcion'];
                    $transmittal_detalle->cmotivoenvio=$this->valorNull($request['detalleTransmittal'][$i]['motivo'],null);//$request['detalleTransmittal'][$i]['motivo'];
                    $transmittal_detalle->save();
                }

                else{

                    $transmittal_detalle = Transmittaldetalle::where('ctransmittaldetalle','=',$request['detalleTransmittal'][$i]['ctransmittaldetalle'])->first();
                    $transmittal_detalle->cmotivoenvio=$this->valorNull($request['detalleTransmittal'][$i]['motivo'],null);
                    $transmittal_detalle->created_user=Auth::user()->cpersona;

                    if ($request->tipoTransmittal == 'C-A') {
                        $transmittal_detalle->revision_entregable=$this->valorNull($request['detalleTransmittal'][$i]['revision'],null);
                    }
                    $transmittal_detalle->save();
                }

            }
        }

        DB::commit();

        //dd($transmittal->ctransmittal);
        return $transmittal->ctransmittal;

    }

    public function transmittal_enviar(Request $request){

        //dd($request->all());

        $objflujoEmp = new FlujoLE();
        $flujo = $objflujoEmp->flujo();
        $flujo = explode(',',$flujo);

        $transmittal=Transmittal::where('ctransmittal','=',$request->ctransmittal)->first();
        DB::beginTransaction();


        if (!is_null($request['detalleTransmittal'])) {
            
            for ($i = 0; $i < count(   $request['detalleTransmittal']); $i++) {

                if ($request['detalleTransmittal'][$i]['cproyectoentregables']!='nuevoEntCli'){


                    $proyecto_entregable=$tentre = Tproyectoentregable::where('cproyectoentregables','=',$request['detalleTransmittal'][$i]['cproyectoentregables'])->first();

                    $cestadoentregableruteo='';
                    $ruteoActual='';

                    if ($request->tipoTransmittal=='C-A') {


                        $proyecto_entregable->cestadoentregableruteo = 'REV';
                        $ruteoActual=$flujo[0];

                        if ($request['detalleTransmittal'][$i]['motivo']=='10') {

                            $ruteoActual=end($flujo);
                            $proyecto_entregable->cestadoentregableruteo = 'FIN';

                        }

                        $proyecto_entregable->ruteo=$ruteoActual;


                    }
                    elseif ($request->tipoTransmittal=='A-C') {
                        $proyecto_entregable->ruteo = 'CD';
                        $proyecto_entregable->cestadoentregableruteo = 'REC';
                        $ruteoActual=end($flujo);

                        if ($request['detalleTransmittal'][$i]['motivo']=='10') {
                            $proyecto_entregable->cestadoentregableruteo = 'FIN';
                        }

                    }

                    $proyecto_entregable->save();
                    $ruteo_traza = new Tproyectoentregablesruteo;
                    $ruteo_traza->cproyectoentregable = $request['detalleTransmittal'][$i]['cproyectoentregables'];
                    $ruteo_traza->ruteo_actual = $ruteoActual;
                    $ruteo_traza->estado = $proyecto_entregable->cestadoentregableruteo;
                    $ruteo_traza->revision = $request['detalleTransmittal'][$i]['revision'];
                    $ruteo_traza->fecha = Carbon::now();
                    $ruteo_traza->cpersona = Auth::user()->cpersona;
                    $ruteo_traza->save();   

                }
            }

            $transmittal->estado_enviado='ENV';
            $transmittal->save();
        }




        DB::commit();

        return $transmittal->ctransmittal;



    }

    //*****  Ver datos y detalle de  TR  *****//

    public function ver_transmittal_proyecto($idProyecto,$tipo,$ctransmittal){

        $proyecto=$this->obtener_datos_proyecto($idProyecto);
        $listaEntregablesTR=$this->obtenerEntregablesEnTR($ctransmittal);
        //$transmittal= Transmittal::where('ctransmittal','=',$ctransmittal)->first();

        $transmittal=DB::table('ttransmittal as tu')
            ->select('tu.*',DB::raw("to_char(tu.fechaenvio,'DD-MM-YYYY') as fechaemision"))
            ->where('ctransmittal','=',$ctransmittal)
            ->first();

        $estado_enviado=$transmittal->estado_enviado;

        //dd($transmittal);

        $fechaemision=$transmittal->fechaemision;
        $numeroTR=$transmittal->numero;

        if ($tipo=='A-C') {
           
            $listaEntregables=$this->listarTodosEntregables($idProyecto,'modal_agregar_ent','sin_grupo');
        }

        elseif ($tipo=='C-A') {
            $listaEntregables=$this->listarTodosEntregables($idProyecto,'modal_recepcion_ent','sin_grupo');
        }
        //dd($listaEntregables);

        $revisiones = Tcatalogo::where('codtab','=','00006')
            ->whereNotNull('digide')
            ->orderBy(DB::raw("to_number(digide,'999.99999999')"),'ASC')
            ->lists('valor','digide')->all();

        $motivo = Tmotivoenvioentregable::lists('descripcion','cmotivoenvio')->all();

        $tipoenvio=Tcatalogo::where('codtab','=','00059')
            ->whereNotNull('digide')
            ->lists('descripcion','digide')->all();

        $tipoenvio = ['N'=>'Seleccione tipo de envío'] + $tipoenvio;

        $contactosAtencion = $this->obtenerNombreContactos($ctransmittal,'001');
  
        $arrayCA = $contactosAtencion['codigoContacto'];
        $arrayCA_nom = $contactosAtencion['nombreContacto'];


        $contactosCC = $this->obtenerNombreContactos($ctransmittal,'002');
        $arrayCC=$contactosCC['codigoContacto'];
        $arrayCC_nom=$contactosCC['nombreContacto'];


        $personaCD= DB::table('tproyectopersona as per')  
        ->leftjoin('tpersona as tp','per.cpersona','=','tp.cpersona')     
        ->where('per.cproyecto','=',$idProyecto)
        ->where('per.cdisciplina','=','11')
        ->where('eslider','=','1')
        ->first();

        //dd($arrayCA,$contactosAtencion);
        //dd($contactosAtencion);

        if ($tipo=='A-C') {
                return view('transmittal/generarTransmittal',
                 ['proyecto'=>$proyecto[0],
                 'listEnt'=>$listaEntregables,
                 'revisiones'=>$revisiones, 
                 'motivo'=>$motivo,
                 'entregablesTR'=>$listaEntregablesTR,
                 'transmittal'=>$transmittal,
                 'tipoenvio'=>$tipoenvio,
                 'arrayCA'=>$arrayCA,
                 'arrayCA_nom'=>$arrayCA_nom,
                 'arrayCC'=>$arrayCC,
                 'arrayCC_nom'=>$arrayCC_nom,
                 'personaCD'=>$personaCD,
                 'fechaemision'=>$fechaemision,
                 'numeroTR'=>$numeroTR,
                 'estado_enviado'=>$estado_enviado,
                ]);
            
        }

        elseif ($tipo=='C-A') {
            
                return view('transmittal/generarRecepcionTR',
                 ['proyecto'=>$proyecto[0],
                 'listEnt'=>$listaEntregables,
                 'revisiones'=>$revisiones, 
                 'motivo'=>$motivo,
                 'entregablesTR'=>$listaEntregablesTR,
                 'transmittal'=>$transmittal,
                 'tipoenvio'=>$tipoenvio,
                 'arrayCA'=>$arrayCA,
                 'arrayCA_nom'=>$arrayCA_nom,
                 'arrayCC'=>$arrayCC,
                 'arrayCC_nom'=>$arrayCC_nom,
                 'personaCD'=>$personaCD,
                 'fechaemision'=>$fechaemision,
                 'numeroTR'=>$numeroTR,
                 'estado_enviado'=>$estado_enviado,
                ]);
        }

    }

    private function obtenerNombreContactos($ctransmittal,$tipocontacto){

        $contactosAtencion=$this->obtenerContactosTR($ctransmittal,$tipocontacto);

        $arrayContacto=[];
        $arrayContacto_nom=[];

        if ($contactosAtencion) {
            foreach ($contactosAtencion as $ca) {
                array_push($arrayContacto,$ca['cunidadmineracontacto']);
                array_push($arrayContacto_nom,$ca['nombres'].' '.$ca['apaterno']);
            }
        }

        $arrayContacto = implode(',', $arrayContacto);
        $arrayContacto_nom = implode(',', $arrayContacto_nom);

        return ['codigoContacto'=>$arrayContacto,'nombreContacto'=>$arrayContacto_nom];
    }

    //*****  Obtiene detalle de TR  *****//

    private function obtenerEntregablesEnTR($ctransmittal){

        $entregablesTR = DB::table('ttransmittaldetalle as trd')
            ->leftjoin('tmotivoenvioentregable as m','trd.cmotivoenvio','=','m.cmotivoenvio')
            ->where('trd.ctransmittal','=',$ctransmittal)
            ->select('trd.*','m.cmotivoenvio','m.descripcion as motivo','m.abreviatura')
            ->get();

            foreach ($entregablesTR as $key => $item) {
                $revision = $this->catalogodescripcion('00006',$item->revision_entregable);
                $item->revision=$revision ? $revision->valor : '';
            }

        return $entregablesTR;

    }

    public function obtenerRevisionesCombo(){
        $revisiones = Tcatalogo::where('codtab','=','00006')
            ->whereNotNull('digide')
            ->orderBy(DB::raw("to_number(digide,'999.99999999')"),'ASC')
            ->get();
        return $revisiones;
    }

    public function obtenerMotivoCombo(){
        $motivo = DB::table('tmotivoenvioentregable')
            ->orderBy('descripcion','asc')
            ->get();
        return $motivo;
    }

    public function obtenerListaClientes($cproyecto,$transmittal,$tipocontacto){
        //public function obtenerListaClientes($cproyecto,$tipocontacto){

         $array_um=$this->obtenerContactosUM($cproyecto);

        return view('transmittal/tablaContactosTR',['listaContactos'=>$array_um]);

    }


    //*****  Obtiene todos los contactos de la UM *****//
    private function obtenerContactosUM($cproyecto){

       $tunidadmineracontactos=DB::table('tunidadmineracontactos as tu')
           ->leftJoin('tunidadminera as um','tu.cunidadminera','=','um.cunidadminera')
           ->leftjoin('tproyecto as tp','um.cunidadminera','=','tp.cunidadminera')
           ->leftJoin('ttiposcontacto as tc','tu.ctipocontacto','=','tc.ctipocontacto')
           ->leftJoin('tcontactocargo as cc','tu.ccontactocargo','=','cc.ccontactocargo')
           ->select('tu.*','um.nombre as uminera','tc.descripcion as tipo','cc.descripcion as cargo')
           ->where('tp.cproyecto','=',$cproyecto)
           ->orderBy('tu.apaterno','asc')
           ->distinct()
           ->get();


       $array_um=[];

       foreach ($tunidadmineracontactos as $tum) {

            $um["cunidadmineracontacto"]=$tum->cunidadmineracontacto;
            $um["apaterno"]=$tum->apaterno;
            $um["cunidadminera"]=$tum->cunidadminera;
            $um["amaterno"]=$tum->amaterno;
            $um["nombres"]=$tum->nombres;
            $um["ctipocontacto"]=$tum->ctipocontacto;
            $um["ccontactocargo"]=$tum->ccontactocargo;
            $um["email"]=$tum->email;
            $um["telefono"]=$tum->telefono;
            $um["anexo"]=$tum->anexo;
            $um["cel1"]=$tum->cel1;
            $um["cel2"]=$tum->cel2;
            $um["direccion"]=$tum->direccion;
            $um["uminera"]=$tum->uminera;
            $um["tipo"]=$tum->tipo;
            $um["cargo"]=$tum->cargo;
            array_push($array_um, $um);
            //dd($array_um,$um);
       }

       return $array_um;
    }

    //*****  Obtiene todos los contactos agregados a un TR segun el tipo *****//
    private function obtenerContactosTR($transmittal,$tipocontacto){

        $array_um=[];
        if ($transmittal!='nuevo') {

            $contactosTR=DB::table('tunidadmineracontactos as tu')
                ->leftjoin('ttransmittalcontacto as ttc','ttc.cunidadmineracontacto','=','tu.cunidadmineracontacto')
                ->where('ttc.ctransmittal','=',$transmittal)
                ->where('ttc.tipocontacto','=',$tipocontacto)
                ->select('tu.*','ttc.ctransmittalcontacto','ttc.tipocontacto','ttc.ctransmittal')
                ->get();

            foreach ($contactosTR as $tum) {

                    $um["cunidadmineracontacto"]=$tum->cunidadmineracontacto;
                    $um["apaterno"]=$tum->apaterno;
                    $um["amaterno"]=$tum->amaterno;
                    $um["nombres"]=$tum->nombres;
                    $um["ctransmittal"]=$tum->ctransmittal;
                    $um["ctransmittalcontacto"]=$tum->ctransmittalcontacto;
                    $um["tipocontacto"]=$tum->tipocontacto;

                    array_push($array_um, $um);
            }
        }
       return $array_um;
    }

    //*****  Guarda nuevo el contacto en Tunidadmineracontacto *****//
    public function guardar_contacto(Request $request){

        DB::beginTransaction();
    
            $contact = new Tunidadmineracontacto();
            $contact->apaterno= $request->input('apaterno');
            $contact->amaterno= $request->input('amaterno');
            $contact->nombres= $request->input('nombres');
            $contact->email= $request->input('email');
            $contact->telefono= $request->input('telefono');
            $contact->cunidadminera=$request->input('cunidadminera');
            $contact->ctipocontacto= 'TR';
            $contact->save();

        DB::commit();

    }


    //*****  Obtiene todos los TR de un proyecto *****//
    public function obtenerListaTRProyecto($cproyecto){

        $transmittals=DB::table('ttransmittal as tr')
            ->leftjoin('tcatalogo as tp','tr.tipoenvio','=','tp.digide')
            ->where('tp.codtab','=','00059')
            ->whereNotNull('digide')
            ->where('tr.cproyecto','=',$cproyecto)
            ->where('tr.estado','=','ACT')
            ->select('tr.*','tp.descripcion as envio')
            ->distinct()
            ->get();
           // dd($transmittals);

            return view('transmittal/tablaListaTR',
            ['transmittals'=>$transmittals]);
        
        //return $transmittal;

    }


    //*****  Muestra la vista para registrar un nuevo TR  *****//
    public function generar_recepcion_transmittal($idProyecto){

        $proyecto=$this->obtener_datos_proyecto($idProyecto);

        $listaEntregables=$this->listarTodosEntregables($idProyecto,'modal_recepcion_ent','sin_grupo');

        $revisiones = Tcatalogo::where('codtab','=','00006')
            ->whereNotNull('digide')
            ->orderBy(DB::raw("to_number(digide,'999.99999999')"),'ASC')
            ->lists('valor','digide')->all();

        $motivo = Tmotivoenvioentregable::lists('descripcion','cmotivoenvio')->all();

        $tipoenvio=Tcatalogo::where('codtab','=','00059')
            ->whereNotNull('digide')
            ->lists('descripcion','digide')->all();

        $tipoenvio = ['N'=>'Seleccione tipo de envío'] + $tipoenvio;
        //dd($transmittal);

        $transmittal = Transmittal::get()->all();

        /*$personaCD= DB::table('tpersona as per')       
        ->where('per.cpersona','=',Auth::user()->cpersona)
        ->first();*/

        $personaCD= DB::table('tproyectopersona as per') 
        ->leftjoin('tpersona as tp','per.cpersona','=','tp.cpersona')       
        ->where('per.cproyecto','=',$idProyecto)
        ->where('per.cdisciplina','=','11')
        ->where('eslider','=','1')
        ->first();


        //dd($personaCD);

        $fechaemision=Carbon::now()->format('d-m-Y');

        $numeroTR=$this->generarNumeroTR($idProyecto,'C-A','ACT');

        $estado_enviado='';

      
        return view('transmittal/generarRecepcionTR',
             ['proyecto'=>$proyecto[0],
             'listEnt'=>$listaEntregables,
             'revisiones'=>$revisiones, 
             'motivo'=>$motivo,
             'tipoenvio'=>$tipoenvio,
             'personaCD'=>$personaCD,
             'fechaemision'=>$fechaemision,
             'numeroTR'=>$numeroTR,
             'estado_enviado'=>$estado_enviado,
        ]);
          
    }


    //*****  Obtiene todos los TR que contengan al entregable seleccionado *****//
    public function listarTRporEntregables($cPryEnt){

        $TRdetalle= Transmittaldetalle::where('cproyectoentregables','=',$cPryEnt)
                ->select('ctransmittal')
                ->distinct()
                ->get();

        $transmittals=[];

        foreach ($TRdetalle as $trd) {

            $tr=DB::table('ttransmittal as tr')
            ->leftjoin('tcatalogo as tp','tr.tipoenvio','=','tp.digide')
            ->where('codtab','=','00059')
            ->whereNotNull('digide')
            ->where('tr.ctransmittal','=',$trd->ctransmittal)
            ->where('tr.estado','=','ACT')
            ->select('tr.*',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaenvio"),'tp.descripcion as envio')
            ->orderBy('digide','ASC')
            ->first();

            if ($tr) {
                array_push($transmittals,$tr);
                
            }

        }

        return view('transmittal/tablaTransmittals',
             ['transmittals'=>$transmittals]);


    }


    //*****  buscar TR por numero *****//
    public function buscaTRporNumero($numeroTR){
        $transmittals=DB::table('ttransmittal as tr')
            ->where('tr.numero','=',$numeroTR)
            ->first();
        $mensaje='false';
        if ($transmittals) {
            $mensaje='true';
        }
        return $mensaje;
    }

    //***** generar numero de TR nuevo *****//
    public function generarNumeroTR($cproyecto,$tipo,$estado){

        $proyecto = DB::table('tproyecto')->where('cproyecto','=',$cproyecto)->first();
        $cliente  = DB::table('tpersona')->where('cpersona','=',$proyecto->cpersonacliente)->first();
        $fechaemision=Carbon::now()->format('Ymd');
        $fechaemisionCodigo=substr($fechaemision,2,6);

        $transmittal=DB::table('ttransmittal')
            ->where('cproyecto','=',$cproyecto)
            ->where('tipotransmittal','=',$tipo);

        if ($tipo=='C-A') {
            $transmittal=$transmittal->where('numero','ilike','%'.$fechaemisionCodigo);
        }

        $transmittal=$transmittal->orderBy('numero','desc')
            ->get();

        $inicial=0;

        if ($transmittal) {
            $correlativo=$inicial+1+count($transmittal); 
        }
        else{
            $correlativo=$inicial+1;
        }
        $codigo='sin codigo';
        if ($tipo=='A-C') {
            $prefijo=$proyecto->codigo.'-TR-AND-'.$cliente->abreviatura.'-';
            $codigo=$prefijo.str_pad($correlativo,3,'000', STR_PAD_LEFT);
        }
        elseif ($tipo=='C-A') {
            $prefijo='ST-';
            $codigo=$prefijo.str_pad($correlativo,3,'000', STR_PAD_LEFT).'-'.$fechaemisionCodigo;
        }

       return $codigo;
    }

    //***** Imprimir Transmittal *****//
    public function imprimirTransmittal(Request $request,$ctransmittal){

        $transmittal=DB::table('ttransmittal as tu')
            ->select('tu.*',DB::raw("to_char(tu.fechaenvio,'DD-MM-YYYY') as fechaemision"))
            ->where('ctransmittal','=',$ctransmittal)
            ->first();

        $idProyecto=$transmittal->cproyecto;


        $proyecto=$this->obtener_datos_proyecto($idProyecto);
        $listaEntregablesTR=$this->obtenerEntregablesEnTR($ctransmittal);
        //$transmittal= Transmittal::where('ctransmittal','=',$ctransmittal)->first();

        //dd($listaEntregablesTR);

        $tipoenvio=$this->catalogodescripcion('00059',$transmittal->tipoenvio);
       

        //dd($proyecto,$transmittal,$tipoenvio);


        $fechaemision=$transmittal->fechaemision;
        $numeroTR=$transmittal->numero;


        $contactosAtencion=$this->obtenerContactosTR($ctransmittal,'001');

            $arrayCA_nom=[];

            if ($contactosAtencion) {
                foreach ($contactosAtencion as $ca) {
                    array_push($arrayCA_nom,$ca['nombres'].' '.$ca['apaterno']);
                }
            }

            $arrayCA_nom = implode(',', $arrayCA_nom);


        $contactosCC=$this->obtenerContactosTR($ctransmittal,'002');
            $arrayCC_nom=[];

            if ($contactosCC) {
                foreach ($contactosCC as $ca) {
                    array_push($arrayCC_nom,$ca['nombres'].' '.$ca['apaterno']);
                }
            }

            $arrayCC_nom = implode(',', $arrayCC_nom);

        /*$personaCD= DB::table('tpersona as per')       
        ->where('per.cpersona','=',$transmittal->created_user)
        ->first();*/

        $personaCD= DB::table('tproyectopersona as per')  
        ->leftjoin('tpersona as tp','per.cpersona','=','tp.cpersona')     
        ->where('per.cproyecto','=',$idProyecto)
        ->where('per.cdisciplina','=','11')
        ->where('eslider','=','1')
        ->first();

        //dd($personaCD);

        $view =  \View::make('transmittal.printTR',
                ['proyecto'=>$proyecto[0],
                 'entregablesTR'=>$listaEntregablesTR,
                 'transmittal'=>$transmittal,
                 'arrayCA_nom'=>$arrayCA_nom,
                 'arrayCC_nom'=>$arrayCC_nom,
                 'personaCD'=>$personaCD,
                 'fechaemision'=>$fechaemision,
                 'tipoenvio'=>$tipoenvio,]

            )->render();


        $pdf=\App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4','D');

      
    //dd($ctransmittal,'dsafsdf');
        return $pdf->stream($transmittal->numero+'.pdf');

    }

    //*****  Muestra la vita de Logs de Transmittals  *****//
    public function listado_transmittal_logs(){

        return view('transmittal/listaTRLogs');
    }

    public function cargar_logs_proyecto_seleccionado(Request $request,$idProyecto){

        $proyecto=$this->obtener_datos_proyecto($idProyecto);
      

        return view('transmittal/listaTRLogs',
            [
                'proyecto'=>$proyecto[0],  

            ]);
        
    }

    ////////*********************************************************************////////

    public function verReportesLogs(Request $request,$tipo){

        $tabla='';

        if ($tipo=='andcli') {
           $tabla=$this->obtenerlog_andcli($request->cproyecto);
        }

        else if ($tipo=='cliand') {
           $tabla=$this->obtenerlog_cliand($request->cproyecto);
        }

        else if ($tipo=='letec') {
           $tabla=$this->obtenerlogs_le($request->cproyecto,'2');
        }

        else if ($tipo=='leges') {
           $tabla=$this->obtenerlogs_le($request->cproyecto,'1');
        }

        else if ($tipo=='lecom') {
           $tabla=$this->obtenerlogs_le($request->cproyecto,'3');
        }

           return $tabla;

    }

    private function obtenerlog_andcli($cproyecto)
    {

        $listaEntregables=$this->obtenerLE_enviados_recibidos($cproyecto,'A-C');

        return view('transmittal/tablaLogANDCLI',
            [
             'entTodosTR'=>$listaEntregables,
             
            ]);

    }

    private function obtenerlog_cliand($cproyecto)
    {

        $listaEntregables=$this->obtenerLE_enviados_recibidos($cproyecto,'C-A');

        return view('transmittal/tablaLogCLIAND',
            [
             'entTodosTR'=>$listaEntregables,
             
            ]);

    }

    private function obtenerlogs_le($cproyecto,$tipo)
    {

        if ($tipo=='2') 
        {
            $listaEntregables=$this->obtenerLE_tec_todos($cproyecto,$tipo);
        }

        else
        {
            $listaEntregables=$this->obtenerLE_tec_ges_com($cproyecto,$tipo);
        }

        return view('transmittal/tablaLog_ges_tec_com',
            [
             'entTodosTR'=>$listaEntregables,
             
            ]);

    }

   

    //*****  Obtiene detalle de LE  *****//
    private function obtenerLE_enviados_recibidos($cproyecto,$tipo)
    {
       
        $crevision=$this->obtener_ultima_revision_proyecto($cproyecto);

        $lpentrega = DB::table('tproyectoentregables as pryent')
            ->leftjoin('tproyecto as pry','pryent.cproyecto','=','pry.cproyecto')
            ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
            ->leftjoin('ttransmittaldetalle as trd','pryent.cproyectoentregables','=','trd.cproyectoentregables')
            ->leftjoin('ttransmittal as tr','trd.ctransmittal','=','tr.ctransmittal')
            ->leftjoin('tmotivoenvioentregable as tm','trd.cmotivoenvio','=','tm.cmotivoenvio')
            ->where('edt.cproyectoent_rev','=',$crevision)
            ->where('pryent.cproyecto','=',$cproyecto)
            ->where('tr.tipotransmittal','=',$tipo)
            ->where('edt.estado','=','1')
            ->where('tr.estado_enviado','=','ENV')
            ->select('pryent.cproyectoentregables','tr.numero as numero_tr','trd.revision_entregable as rev','trd.revision_entregable as revision','pryent.codigo','pryent.descripcion_entregable','tm.descripcion as motivo',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaemision"),'tr.tipoenvio as envio')
            ->orderBy('pryent.codigo','asc')
            ->orderBy('revision','asc')
            ->get();

        $aentregables=[];
        foreach ($lpentrega as $lpe) {

            $revision=$this->catalogodescripcion('00006',$lpe->revision);
            $envio=$this->catalogodescripcion('00059',$lpe->envio);

            $lp['numero_tr']=$lpe->numero_tr;
            $lp['revision']= $revision? $revision->valor:'';
            $lp['codigo']=$lpe->codigo;
            $lp['descripcion_entregable']=$lpe->descripcion_entregable;
            $lp['motivo']=$lpe->motivo;
            $lp['fechaemision']=$lpe->fechaemision;
            $lp['envio']=$envio? $envio->descripcion:'';

            $tr=DB::table('ttransmittal as tr')
                ->leftjoin('ttransmittaldetalle as trd','tr.ctransmittal','=','trd.ctransmittal')
                ->select('tr.*',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaemision"))
                ->where('trd.cproyectoentregables','=',$lpe->cproyectoentregables)
                ->where('trd.revision_entregable','=',$lpe->revision);

            if ($tipo == 'A-C') {

                $tr=$tr->where('tr.tipotransmittal','=','C-A')->first();
            }

            else{

                $tr=$tr->where('tr.tipotransmittal','=','A-C')->first();
                    
            }

            $lp['TR']= $tr ? $tr->numero :'';

            $lp['TR_fecha']= $tr ? $tr->fechaemision :'';

            array_push($aentregables, $lp);
        }

        return $aentregables;

    }

    private function obtenerLE_tec_todos($cproyecto,$tipo){

        $crevision=$this->obtener_ultima_revision_proyecto($cproyecto);

        $lpentrega_en_TR = DB::table('tproyectoentregables as pryent')
            ->leftjoin('tproyecto as pry','pryent.cproyecto','=','pry.cproyecto')
            ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
            ->leftjoin('ttransmittaldetalle as trd','pryent.cproyectoentregables','=','trd.cproyectoentregables')
            ->leftjoin('tmotivoenvioentregable as tm','trd.cmotivoenvio','=','tm.cmotivoenvio')
            ->leftjoin('ttransmittal as tr','trd.ctransmittal','=','tr.ctransmittal')
            ->where('edt.cproyectoent_rev','=',$crevision)
            ->where('pryent.cproyecto','=',$cproyecto)
            ->where('edt.ctipoedt','=',$tipo)
            ->where('tr.tipotransmittal','=','A-C')
            ->where('edt.estado','=','1')
            ->where('tr.estado_enviado','=','ENV')
            ->select('pryent.cproyectoentregables','tr.numero as numero_tr','trd.revision_entregable as revision','pryent.codigo','pryent.descripcion_entregable','tm.descripcion as motivo',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaemision"),'tr.tipoenvio as envio','edt.ctipoedt','edt.codigo as edt_cod','pryent.cestadoentregable','pryent.centregable')
            ->orderBy('pryent.codigo','asc')
            ->orderBy('pryent.codigo','asc')
            ->orderBy('revision','asc')
            ->get();

        $array_lpentrega_en_TR=[];

        foreach ($lpentrega_en_TR as $key => $lpen) {

            array_push($array_lpentrega_en_TR, $lpen->cproyectoentregables);
        }

        $array_lpentrega_en_TR=array_unique($array_lpentrega_en_TR);

        $lpentrega_sin_TR = DB::table('tproyectoentregables as pryent')
            ->leftjoin('tproyecto as pry','pryent.cproyecto','=','pry.cproyecto')
            ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
            ->where('edt.cproyectoent_rev','=',$crevision)
            ->where('pryent.cproyecto','=',$cproyecto)
            ->where('edt.ctipoedt','=',$tipo)
            ->where('edt.estado','=','1')
            ->select('pryent.cproyectoentregables','pryent.codigo','pryent.descripcion_entregable','edt.ctipoedt','edt.codigo as edt_cod','pryent.cestadoentregable','pryent.centregable','pryent.revision');

            if (!is_null($array_lpentrega_en_TR)) {

                $lpentrega_sin_TR=$lpentrega_sin_TR->wherenotIn('pryent.cproyectoentregables',$array_lpentrega_en_TR);

            }

        $lpentrega_sin_TR=$lpentrega_sin_TR
            ->orderBy('pryent.codigo','asc')
            ->get();

            foreach ($lpentrega_sin_TR as $key => $str) {
                $str->numero_tr='';
                $str->revision=$str->revision;
                $str->motivo='';
                $str->fechaemision='';
                $str->envio='';
            }

        $lpentrega=array_merge($lpentrega_sin_TR,$lpentrega_en_TR);

            foreach ($lpentrega as $key => $orden) {

                $aux[$key]=$orden->cproyectoentregables;

            }

        if ($lpentrega) {

            $array=array_multisort($aux,SORT_ASC,$lpentrega);

        }

        $aentregables=[];

        foreach ($lpentrega as $lpe) {

            $fechaEnvio = Carbon::now();
            $fechaRespuesta = Carbon::now();

            if ($lpe->fechaemision != '') {
                $fechaEnvio = new Carbon($lpe->fechaemision);
            }


            $revision=$this->catalogodescripcion('00006',$lpe->revision);
            $envio=$this->catalogodescripcion('00059',$lpe->envio);
            $estado=DB::table('testadoentregable')
                ->where('cestadoentregable','=',$lpe->cestadoentregable)
                ->first();

            $entregable=DB::table('tentregables as ent')
                ->leftjoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
                ->leftjoin('tdisciplinaareas as tda','dis.cdisciplina','=','tda.cdisciplina')
                ->leftjoin('tareas as tar','tda.carea','=','tar.carea')
                ->select('tar.codigo_sig','ent.cdocumentoparapry')
                ->where('ent.centregables','=',$lpe->centregable)
                ->first();

            $tipodoc=DB::table('tdocumentosparaproyecto')
                ->where('cdocumentoparapry','=',$entregable->cdocumentoparapry)
                ->first();
               // dd($lpe->numero_tr);
                $lp['numero_tr']=$lpe->numero_tr;
                $lp['revision']= $revision? $revision->valor:'';
                $lp['codigo']=$lpe->codigo;
                $lp['descripcion_entregable']=$lpe->descripcion_entregable;
                $lp['motivo']=$lpe->motivo;
                $lp['fechaemision']=$lpe->fechaemision;
                $lp['envio']=$envio? $envio->descripcion:'';
                $lp['edt_cod']=$lpe->edt_cod;
                $lp['area']=$entregable? $entregable->codigo_sig:'';
                $lp['tipo_ent']=$tipodoc? $tipodoc->codigo:'';


            $tr=DB::table('ttransmittal as tr')
                ->leftjoin('ttransmittaldetalle as trd','tr.ctransmittal','=','trd.ctransmittal')
                ->select('tr.*',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaemision"))
                ->where('trd.cproyectoentregables','=',$lpe->cproyectoentregables)
                ->where('trd.revision_entregable','=',$lpe->revision)
                ->where('tr.tipotransmittal','=','C-A')
                ->first();

                if ($tr) {
                    if ($tr->fechaemision != '') {
                        $fechaRespuesta = new Carbon($tr->fechaemision);
                    }
                }
                       
            $dias_respuesta = $fechaRespuesta->diff($fechaEnvio);

            $dias_respuesta = $dias_respuesta->days;

            if ($lpe->fechaemision == '') {
                $dias_respuesta = '';
            }

                $lp['dias'] = $dias_respuesta;
                $lp['TR']= $tr ? $tr->numero :'';
                $lp['TR_fecha']= $tr ? $tr->fechaemision :'';

            array_push($aentregables, $lp);
        }

        return $aentregables;

    }

    private function obtenerLE_tec_ges_com($cproyecto,$tipo){

        $crevision=$this->obtener_ultima_revision_proyecto($cproyecto);

        $lpentrega = DB::table('tproyectoentregables as pryent')
            ->leftjoin('tproyecto as pry','pryent.cproyecto','=','pry.cproyecto')
            ->leftJoin('tproyectoedt as edt','pryent.cproyectoedt','=','edt.cproyectoedt')
            ->join('ttransmittaldetalle as trd','pryent.cproyectoentregables','=','trd.cproyectoentregables')
            ->leftjoin('tmotivoenvioentregable as tm','trd.cmotivoenvio','=','tm.cmotivoenvio')
            ->join('ttransmittal as tr','trd.ctransmittal','=','tr.ctransmittal')
            ->where('edt.cproyectoent_rev','=',$crevision)
            ->where('pryent.cproyecto','=',$cproyecto)
            ->where('edt.ctipoedt','=',$tipo)
            ->where('edt.estado','=','1')
            ->where('tr.tipotransmittal','=','A-C')
            ->select('pryent.cproyectoentregables','tr.numero as numero_tr','trd.revision_entregable as revision','pryent.codigo','pryent.descripcion_entregable','tm.descripcion as motivo',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaemision"),'tr.tipoenvio as envio','edt.ctipoedt','edt.codigo as edt_cod','pryent.cestadoentregable','pryent.centregable')
            ->orderBy('pryent.codigo','asc')
            ->orderBy('revision','asc')
            ->get();


        $aentregables=[];
        foreach ($lpentrega as $lpe) {

            $fechaEnvio = Carbon::now();
            $fechaRespuesta = Carbon::now();

            if ($lpe->fechaemision != '') {
                $fechaEnvio = new Carbon($lpe->fechaemision);
            }

            $revision=$this->catalogodescripcion('00006',$lpe->revision);
            $envio=$this->catalogodescripcion('00059',$lpe->envio);
            $estado=DB::table('testadoentregable')
                ->where('cestadoentregable','=',$lpe->cestadoentregable)
                ->first();

            $entregable=DB::table('tentregables as ent')
                ->leftjoin('tdisciplina as dis','ent.cdisciplina','=','dis.cdisciplina')
                ->leftjoin('tdisciplinaareas as tda','dis.cdisciplina','=','tda.cdisciplina')
                ->leftjoin('tareas as tar','tda.carea','=','tar.carea')
                ->select('tar.codigo_sig','ent.cdocumentoparapry')
                ->where('ent.centregables','=',$lpe->centregable)
                ->first();

            $tipodoc=DB::table('tdocumentosparaproyecto')
                ->where('cdocumentoparapry','=',$entregable->cdocumentoparapry)
                ->first();
               
                $lp['numero_tr']=$lpe->numero_tr;
                $lp['revision']= $revision? $revision->valor:'';
                $lp['codigo']=$lpe->codigo;
                $lp['descripcion_entregable']=$lpe->descripcion_entregable;
                $lp['motivo']=$lpe->motivo;
                $lp['fechaemision']=$lpe->fechaemision;
                $lp['envio']=$envio? $envio->descripcion:'';
                $lp['edt_cod']=$lpe->edt_cod;
                $lp['area']=$entregable? $entregable->codigo_sig:'';
                $lp['tipo_ent']=$tipodoc? $tipodoc->codigo:'';


            $tr=DB::table('ttransmittal as tr')
                ->leftjoin('ttransmittaldetalle as trd','tr.ctransmittal','=','trd.ctransmittal')
                ->select('tr.*',DB::raw("to_char(tr.fechaenvio,'DD-MM-YYYY') as fechaemision"))
                ->where('trd.cproyectoentregables','=',$lpe->cproyectoentregables)
                ->where('trd.revision_entregable','=',$lpe->revision)
                ->where('tr.tipotransmittal','=','C-A')
                ->first();

                if ($tr) {
                    if ($tr->fechaemision != '') {
                        $fechaRespuesta = new Carbon($tr->fechaemision);
                    }
                }
                       
            $dias_respuesta = $fechaRespuesta->diff($fechaEnvio);

            $dias_respuesta = $dias_respuesta->days;

            if ($lpe->fechaemision == '') {
                $dias_respuesta = '';
            }

                $lp['dias'] = $dias_respuesta;
                $lp['TR']= $tr ? $tr->numero :'';
                $lp['TR_fecha']= $tr ? $tr->fechaemision :'';

            array_push($aentregables, $lp);
        }

        return $aentregables;

    }

    private function catalogodescripcion($codtab,$valor){

        $tipoenvio=DB::table('tcatalogo')
            ->where('codtab','=',$codtab)
            ->whereNotNull('digide')
            ->where('digide','=',$valor)
            ->first();

        return $tipoenvio;

    }

    private function obtener_ultima_revision_proyecto($cproyecto){

        $documento = $this->obtenerRevisionEntregablesPryCli($cproyecto);
        $revisiones_internas=null;
        $crevision=null;

        if($documento){
            $revisiones_internas=$this->obtenerRevisionEntregablesPryInt($documento->cproyectodocumentos);
            $crevision=$revisiones_internas->cproyectoent_rev;
        }

        return $crevision;

    }

    public function eliminarDetalleTR(Request $request){

        $ctrdetalle = $request->input("ctrdetalle");

        $deletedRows = Transmittaldetalle::where('ctransmittaldetalle', "=", $ctrdetalle)->first();

        $cproyectoentregable = Tproyectoentregable::where('cproyectoentregables',$deletedRows->cproyectoentregables)->first();
        $cproyectoentregable->cestadoentregableruteo ='APR';
        $cproyectoentregable->save(); 

        $deletedRows->delete();

        return $deletedRows;
    }

    public function actualizarTablaDetalleTR($ctransmittal){

        $transmittal=DB::table('ttransmittal as tu')
            ->select('tu.*',DB::raw("to_char(tu.fechaenvio,'DD-MM-YYYY') as fechaemision"))
            ->where('ctransmittal','=',$ctransmittal)
            ->first();
        $fechaemision=$transmittal->fechaemision;
        $entregablesTR=$this->obtenerEntregablesEnTR($ctransmittal);

        return view('transmittal/tablaEntregablesTransmittal',compact('entregablesTR','transmittal'));

    }

}
