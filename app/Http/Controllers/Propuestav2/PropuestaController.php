<?php

namespace App\Http\Controllers\Propuestav2;

use App\Http\Controllers\Controller;
use App\Models\Tpersona;
use App\Models\Contacto;
use App\Models\Invitacion;
use App\Models\Prepropuesta;
use App\Models\Propuesta;
use Auth;
use DB;
use Illuminate\Http\Request;

class PropuestaController extends Controller
{
	//public 

	public function index()
	{
		$lista = Propuesta::orderBy("propuesta", "asc")->get();
		$n = 1;
		foreach ($lista as $key => $item) {
			$item->n = $n;
			$item->cliente = Tpersona::find($item->cliente_id);
			$item->contacto = Contacto::find($item->contacto_id);
			$n++;
		}

		$data["lista"] = $lista;

		return view("propuestav2.propuesta.listado", $data);
	}

	public function form()
	{
		$data["clientes"] = Tpersona::where("ctipopersona", "=", "JUR")->orderBy("nombre", "ASC")->get();		
		$data["prepropuestas"] = Prepropuesta::all();
		
		return view("propuestav2.propuesta.form", $data);
	}

	public function formPost(Request $request)
	{
		//contacto
		$contacto = new Contacto();
		$contacto->nombres = $request->input("nombres");
		$contacto->apellidos = $request->input("apellidos");
		$contacto->cargo = $request->input("cargo");
		$contacto->numero = $request->input("numero");
		$contacto->correo = $request->input("correo");
		$contacto->save();

		//propuesta
		$pre = new Propuesta();
		$pre->contacto_id = $contacto->id;
		$pre->prepropuesta_id = $request->input("prepropuesta_id");
		$pre->cliente_id = $request->input("cliente_id");

		$pre->codigo = $request->input("codigo");		
		$pre->disciplinas = $request->input("disciplinas");
		$pre->emision = $request->input("emision");
		$pre->gerencia = $request->input("gerencia");
		$pre->motivo = $request->input("motivo");
		$pre->propuesta = $request->input("propuesta");
		$pre->revision = $request->input("revision");
		$pre->tipo_servicio = $request->input("tipo_servicio");
		$pre->unidad = $request->input("unidad");
		
		$pre->save();

		return redirect("propuestas");
	}

	//private 

	private function _obtener()
	{

	}
	
}
