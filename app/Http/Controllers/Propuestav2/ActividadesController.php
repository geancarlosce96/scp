<?php

namespace App\Http\Controllers\Propuestav2;

use App\Http\Controllers\Controller;
use App\Models\Cliente;
use App\Models\Contacto;
use App\Models\Invitacion;
use App\Models\Prepropuesta;
use App\Models\Propuesta;
use App\Models\Actividad;
use Auth;
use DB;
use Illuminate\Http\Request;

class ActividadesController extends Controller
{
	//public 

	public function form()
	{
		$clientes = Cliente::all();
		$data["clientes"] = $clientes;
		
		return view("propuestav2.propuesta.actividades", $data);
	}

	public function formPost(Request $request)
	{
		//actividades
		$invitacion = new Actividad();		
		$invitacion->save();

		return redirect("propuestas");
	}

	//private 

	private function _obtener()
	{

	}
	
}
