<?php

namespace App\Http\Controllers\Propuestav2;

use App\Http\Controllers\Controller;
use App\Models\Tpersona;
use App\Models\Contacto;
use App\Models\Invitacion;
use App\Models\Prepropuesta;
use App\Models\Propuesta;
use Auth;
use DB;
use Illuminate\Http\Request;

class InvitacionController extends Controller
{
	//public 

	public function index()
	{
		$lista = Invitacion::orderBy("propuesta", "asc")->get();
		$n = 1;
		foreach ($lista as $key => $item) {
			$item->n = $n;
			$item->cliente = Tpersona::find($item->cliente_id);
			//$item->cliente = Tpersona::where("cpersona", "=", $item->cliente_id)->first();
			$n++;
		}

		$data["lista"] = $lista;

		return view("propuestav2.invitacion.listado", $data);
	}

	public function form()
	{
		$clientes = Tpersona::where("ctipopersona", "=", "JUR")->orderBy("nombre", "ASC")->get();
		$data["clientes"] = $clientes;
		
		return view("propuestav2.invitacion.form",	$data);
	}

	public function formPost(Request $request)
	{
		//contacto
		$contacto = new Contacto();
		$contacto->nombres = $request->input("nombres");
		$contacto->apellidos = $request->input("apellidos");
		$contacto->cargo = $request->input("cargo");
		$contacto->numero = $request->input("numero");
		$contacto->correo = $request->input("correo");
		$contacto->save();

		//invitacion
		$invitacion = new Invitacion();
		$invitacion->contacto_id = $contacto->id;
		$invitacion->cliente_id = $request->input("cliente_id");
		$invitacion->propuesta = $request->input("propuesta");
		$invitacion->save();

		return redirect("invitaciones");
	}

	public function obtenerInvitacion($id)
	{
		$invitacion = Invitacion::find($id);
		$invitacion->contacto = Contacto::find($invitacion->contacto_id);
		return $invitacion;
	}

	//private 

	private function _obtener()
	{

	}
	
}
