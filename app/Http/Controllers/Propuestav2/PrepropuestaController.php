<?php

namespace App\Http\Controllers\Propuestav2;

use App\Http\Controllers\Controller;
use App\Models\Tpersona;
use App\Models\Contacto;
use App\Models\Invitacion;
use App\Models\Prepropuesta;
use App\Models\Propuesta;
use Auth;
use DB;
use Illuminate\Http\Request;

class PrePropuestaController extends Controller
{
	//public 

	public function index()
	{
		$lista = Prepropuesta::orderBy("propuesta", "asc")->get();
		$n = 1;
		foreach ($lista as $key => $item) {
			$item->n = $n;
			$item->cliente = Tpersona::find($item->cliente_id);
			$item->contacto = Contacto::find($item->contacto_id);
			$n++;
		}

		$data["lista"] = $lista;

		return view("propuestav2.prepropuesta.listado", $data);
	}

	public function form()
	{
		$data["clientes"] = Tpersona::where("ctipopersona", "=", "JUR")->orderBy("nombre", "ASC")->get();
		$data["invitaciones"] = Invitacion::all();
		
		return view("propuestav2.prepropuesta.form", $data);
	}

	public function formPost(Request $request)
	{
		//contacto
		$contacto = new Contacto();
		$contacto->nombres = $request->input("nombres");
		$contacto->apellidos = $request->input("apellidos");
		$contacto->cargo = $request->input("cargo");
		$contacto->numero = $request->input("numero");
		$contacto->correo = $request->input("correo");
		$contacto->save();

		//prepropuesta
		$pre = new Prepropuesta();
		$pre->cliente_id = $request->input("cliente_id");
		$pre->codigo = $request->input("codigo");
		$pre->contacto_id = $contacto->id;
		$pre->propuesta = $request->input("propuesta");
		$pre->unidad = $request->input("unidad");
		$pre->invitacion_id = $request->input("invitacion_id");
		$pre->save();

		return redirect("prepropuestas");
	}

	public function obtenerPrepropuesta($id)
	{
		$propuesta = Prepropuesta::find($id);
		$propuesta->contacto = Contacto::find($propuesta->contacto_id);
		return $propuesta;
	}

	//private 

	private function _obtener()
	{

	}
	
}
