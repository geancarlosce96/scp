<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tpersona;
use App\Models\Tpersonadatosempleado;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tusuariopassword;
use App\Lib\EmailSend;
use App\Erp\FlujoLE;
use App\Models\Tcatalogo;
use App\Models\Ttipofechasentregable;
use App\Models\Tproyecto;
use App\Models\Tvistastablaspersona;
use App\Http\Controllers\Proyecto\ProyectoController;
use App\Http\Controllers\Proyecto\cargaSemanalController;
use Auth;
use DB;
use Carbon\Carbon;
use Redirect;
use DateTime;

class WelcomeController extends Controller
{
	
	public function welcome(){
        // dd(Auth::user());
        $usuario = Auth::user()->cpersona;

        $cusuario = DB::table('tusuarios')
        ->where('cpersona','=',$usuario)
        ->first();

		$datosempleado = DB::table('tpersonadatosempleado')
        ->where('cpersona','=',$usuario)
        // ->select('carea')
        ->first();

        $disciplina = DB::table('tdisciplinaareas')
        ->where('carea','=',$datosempleado->carea)
        // ->select('carea')
        ->first();

        $cargo = DB::table('tcargos')
        ->where('ccargo','=',$datosempleado->ccargo)
        ->first();

		$jefeinmediato = $this->esjefeinmediato($usuario);

        $objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujoEmp();

		$proyectos = $this->ruteoedtproyecto();

		$fechastodas = $this->sarTodasLasFechas();

		$tproyectos = $this->satproyecto();
        // dd($tproyectos);

		$lider_or_gerente = $this->liderOgp($usuario);

		$lecaprendidas = $this->lecaprendidas();

		// $gantt = $this->gantentregables();
		// dd($gantt);

        /*Dirige la time sheet luego de iniciar sesion o actualizar la pagina*/
        if(strlen($cusuario->remember_token)!=null){
            // dd('token');
         // if ($cargo->esjefaturaarea == 1) {
	         return view('welcome',compact('proyectos','flujo','fechastodas','tproyectos','lider_or_gerente','datosempleado','disciplina','lecaprendidas','cargo','jefeinmediato'));
         // } else {
	         // return view('timesheet/welcome');
         // }
         
        }
        else {
            // dd('notoken'); 
            $nomusuario=$cusuario->nombre;           
            $nuevo='si';
            $usuariopassword=Tusuariopassword::where('cusuario','=',$cusuario->cusuario)
            ->first();

            //$usuario=$cusuario->cusuario;

            $passwordActual=$usuariopassword->password;
         //return Redirect::route('passwordUsuario');
            return view('sistema.registroPassword',compact('usuario','nuevo','passwordActual','nomusuario'));
        }
        /*Dirige al panel principal luego de iniciar sesion o actualizar la pagina*/
        
        //return view('welcome');

    }

    public function sarTodasLasFechas(){

        $saFechas=DB::table('ttipofechasentregable as tf') 
        ->orderBy('tf.orden','asc')
        // ->orderBy(DB::raw("to_number(orden,'999.99999999')"),'ASC')
        ->get();

        return $saFechas;

    }

    private function ruteoedtproyecto()
	{
		$objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujoEmp();

		$cestadoentregableruteo = "'APR','REV','OBS','REG'";
		// $cestadoentregableruteo = ['APR','REV','OBS','REG'];
		// dd($cestadoentregableruteo);

		$proyectos = DB::table('tproyecto as tp')
		->join('tproyectoentregables as tpe','tpe.cproyecto','=','tp.cproyecto')
		->join('tpersona as cli','tp.cpersonacliente','=','cli.cpersona')
		->join('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
		->join('tproyectopersona as tpp','tpp.cproyecto','=','tp.cproyecto') 
		->select('tpe.cproyecto','tp.codigo','tp.nombre','cli.nombre as cliente','tu.nombre as uminera','tp.cpersona_gerente','tpe.fecha_modificacion');
		
		if ($flujo=='GP') {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where('tp.cpersona_gerente','=',Auth::user()->cpersona)
			->where('tpe.ruteo','=',$flujo);
		}elseif ($flujo=='LD') {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"),'>',0)
			->where('tpp.cpersona','=',Auth::user()->cpersona)->where('tpp.eslider','=',1)
			->where('tpe.cestadoentregableruteo','!=','REG');	
		}elseif ($flujo=='JA') {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"),'>',0);	
		}
		else {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"),'>',0)
			->where('tpp.cpersona','=',Auth::user()->cpersona);
		}

		$proyectos = $proyectos->distinct('tp.cproyecto')
		// ->whereNotin('tpe.cestadoentregableruteo',['FIN'])
		->orderBy('tpe.fecha_modificacion');
		//->get();

		//almacena $proyectos en $proyectosEntregables
		$proyectosEntregables=$proyectos;

		//Termina de obtener los datos de la consulta
		$proyectos = $proyectos->get();

		//Agrega el codigo de entregable al select
		$proyectosEntregables = $proyectosEntregables->addSelect('tpe.cproyectoentregables')->get();

		foreach ($proyectos as $key => $pry) {
			$array_entregables=[];
			foreach ($proyectosEntregables as $key => $en) {

			//Agrega a $array_entregables solo si el proyecto disgregadoo es igual al de $proyectos y si las fechas coinciden
				
				if ($en->cproyecto==$pry->cproyecto) {
					if ($en->fecha_modificacion==$pry->fecha_modificacion) {
						array_push($array_entregables, $en->cproyectoentregables);					
					}
				}
			}

			//Une todos los entregables encontrados por proyecto
			$pry->centregables_todos=implode(',', $array_entregables);

		}

		// dd($proyectos);

		return $proyectos;
	}

	private function esjefeinmediato($cpersona){
		$esjefe=DB::table('tpersonadatosempleado as e')
        ->leftjoin('tcargos as c','e.ccargo','=','c.ccargo')      
        ->where('e.cpersona','=',$cpersona)
        // ->select('e.cpersona','c.esjefaturaarea')
        ->first();

        // dd($esjefe->ccargo);

        $jefeinmediato = DB::table('tcargos')
        ->where('cargoparentdespliegue','=',$esjefe->ccargo)
        ->first();

        if ($jefeinmediato) {
        	$ji = true;
        }else {
        	$ji = false;
        }

        return $ji;
	}

	private function satproyecto()
	{
		$cpersona = Auth::user()->cpersona;
		$lider_or_gerente = $this->liderOgp($cpersona);
// dd($cpersona);

		$esjefe = $this->esjefeinmediato($cpersona);

		$proyectos = DB::table('tproyecto as tp')
		->leftjoin('tproyectopersona as tpp','tpp.cproyecto','=','tp.cproyecto')
		->leftjoin('tpersona as cli','tp.cpersonacliente','=','cli.cpersona')
		->leftjoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
		->select('tpp.cproyecto','tp.codigo','tp.nombre','cli.nombre as cliente','tu.nombre as uminera','tp.finicio','tp.fcierre')
		->distinct('tpp.cproyecto')
		->where('tp.cestadoproyecto','=','001');
		
		if($esjefe == true){

		} elseif ($lider_or_gerente == 'gerente') {
			$proyectos = $proyectos->where('tp.cpersona_gerente','=',$cpersona);
			
		} elseif ($lider_or_gerente == 'lider') {
			$proyectos = $proyectos->where('tpp.cpersona','=',$cpersona)->where('tpp.eslider','=','1');
		}else{
			$proyectos = $proyectos
			->where('tpp.cpersona','=',$cpersona);
		}
		$proyectos = $proyectos->orderBy('tp.codigo')->get();
	
		return $proyectos;
	}

	public function verequipoproyecto(Request $request)
	{
		$usuario = Auth::user()->cpersona;

		$cproyecto = $request->cproyecto;

		$lider_or_gerente = $this->liderOgp($usuario);
		// dd($lider_or_gerente);

		$area = Tpersonadatosempleado::where('cpersona','=',Auth::user()->cpersona)->first();

		$equipo = DB::table('tproyectopersona as tpp')
		->join('tpersona as per','per.cpersona','=','tpp.cpersona')
		->join('tpersonadatosempleado as tpde','tpde.cpersona','=','tpp.cpersona')
		->join('tareas as ta','ta.carea','=','tpde.carea')
		->join('tcategoriaprofesional as tcp','tcp.ccategoriaprofesional','=','tpp.ccategoriaprofesional')
		->join('tproyecto as tp','tpp.cproyecto','=','tp.cproyecto')
		->leftjoin('tdisciplinaareas as tda','tda.cdisciplina','=','tpp.cdisciplina')
		->select('per.cpersona','per.abreviatura','tp.codigo','tp.nombre','ta.descripcion as area','tcp.descripcion as categoria','tpp.eslider')
		->distinct('per.cpersona')
		->where('tpde.estado','=','ACT')
		->where('tpp.cproyecto','=',$cproyecto);
		if ($lider_or_gerente != "gerente") {
		$equipo=$equipo->where('tda.carea','=',$area['carea']);
		}
		$equipo=$equipo->get();

		foreach ($equipo as $key => $value) {
			$sumahoras = DB::table('tproyectoejecucion')->where('cpersona_ejecuta','=',$value->cpersona)->where('cproyecto','=',$cproyecto)->sum('horasejecutadas');
			$value->suma=$sumahoras;
		}

		// dd($equipo);

		// $verequipoproyecto =  array_merge($proyecto,$equipo);

		return view('partials.equipoproyecto',compact('equipo'));
		// return $equipo;
	}

	public function entregables(Request $request)
	{
		$objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujoEmp();
		
		$emp = $objflujoEmp->ruteoEmp(Auth::user()->cpersona);
		$ld = $objflujoEmp->ruteoCat(Auth::user()->cpersona);
		$gp = $objflujoEmp->ruteoGP(Auth::user()->cpersona);
		$otros = $objflujoEmp->ruteoOtros(Auth::user()->cpersona);
		// dd($emp,$ld,$gp,$otros);
		$areas_jerarquia = new cargaSemanalController();
		$areas = $areas_jerarquia->areas();

		$careas = [];

		foreach ($areas as $key => $value) {
			array_push($careas,$value->carea);
		}

		$disciplinas = DB::table('tdisciplinaareas')->wherein('carea',$careas)->lists('cdisciplina');
		//dd($careas,$disciplinas);
		$cproyecto = $request->cproyecto;

		$proyecto_edt=DB::table('tproyectoedt as edt')
            ->orderBy('edt.cproyecto','desc')
            ->orderBy('edt.cproyectoent_rev','desc')
            ->select('edt.cproyecto','edt.cproyectoent_rev');

            if ($cproyecto != 'todos') {
            	$proyecto_edt=$proyecto_edt->where('edt.cproyecto','=',$cproyecto);
            }

        $proyecto_edt=$proyecto_edt->distinct()->get();

        $proy='';
        $ultima_revision= [];

        foreach ($proyecto_edt as $key => $e) {

            if ($proy != $e->cproyecto) {
                array_push($ultima_revision, $e->cproyectoent_rev);
            }

            $proy=$e->cproyecto;
        }

		$entregables = [];
		$color = 'blue';
		$letra = 'black';

		$proyectos = DB::table('tproyectoentregablesfechas as pfe')
        ->leftjoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
		->leftjoin('tproyecto as tp','tpe.cproyecto','=','tp.cproyecto')
        ->leftjoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->leftjoin('tpersona as cli','tp.cpersonacliente','=','cli.cpersona')
        ->leftjoin('tpersona as resp','tpe.crol_responsable','=','resp.cpersona')
        ->leftjoin('tpersona as elab','tpe.cper_elabora','=','elab.cpersona')
        ->leftjoin('tpersona as revi','tpe.cpersona_revisor','=','revi.cpersona')
        ->leftjoin('tpersona as apr','tpe.cpersona_aprobador','=','apr.cpersona')
        ->leftjoin('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
        ->leftJoin('tproyectoedt as edt','tpe.cproyectoedt','=','edt.cproyectoedt')
		->whereIn('edt.cproyectoent_rev',$ultima_revision);
        if ($cproyecto != 'todos') {
        	$proyectos = $proyectos->where('tpe.cproyecto','=',$cproyecto);
        }

        $proyectos = $proyectos
        ->select('pfe.ctipofechasentregable','ttf.ctipofechasentregable as tipofec','pfe.fecha','pfe.fecha as fec',DB::raw("to_char(pfe.fecha,'YYYY/MM/DD') as fecha"),'tpe.cproyectoentregables','tpe.cproyecto','tp.nombre','cli.nombre as cliente','tu.nombre as uminera','tp.cpersona_gerente','tpe.codigo','tpe.codigocliente','tpe.descripcion_entregable','pfe.cproyectoentregablesfechas','ttf.descripcion as fechastodas','tp.codigo as codpy','ttf.revision as rev_fec','ttf.orden','resp.abreviatura as resp','elab.abreviatura as elab','revi.abreviatura as revi','apr.abreviatura as apr');
		if ($emp->esjefaturaarea=='1') {
			$proyectos = $proyectos;
			// $proyectos = $proyectos->join('tproyectopersona as tpp','tpp.cproyecto','=','tp.cproyecto')->wherein('tpp.cdisciplina',$disciplinas);
			$editable = ['false'];
		}elseif ($ld=='LD') {
			$proyectos = $proyectos->join('tproyectopersona as tpp','tpp.cproyecto','=','tp.cproyecto')->where('tpp.cpersona','=',Auth::user()->cpersona)->where('tpp.eslider','=',1);
			$editable = ['false'];
		}elseif ($gp=='GP') {
			$proyectos = $proyectos->where('tp.cpersona_gerente','=',Auth::user()->cpersona);		
			$editable = ['true'];
		}
		else {
			$proyectos = $proyectos->join('tproyectopersona as tpp','tpp.cproyecto','=','tp.cproyecto')->where('tpp.cpersona','=',Auth::user()->cpersona);
			$editable = ['false'];
		}
		
		$proyectos = $proyectos->get();
//dd($proyectos);
		if ($proyectos) {
			$cproyecto = ['sihay'];
		}
		else {
			$cproyecto = ['nohay'];
		}

		foreach ($proyectos as $key => $value) {

			$rev = DB::table('tcatalogo')->where('digide','=',$value->rev_fec)->where('codtab','=','00006')->first();

		$fecha = Carbon::now();
		$f = $fecha->toDateString();
		$hoy = Carbon::parse($f);

			$entre['id'] = $value->cproyectoentregablesfechas;
			$entre['title'] = $value->descripcion_entregable."-".$value->codigo."_R".$rev->valor;
			$entre['start'] = $value->fec;
			$entre['end'] = $value->fec;
			$entre['nombre'] = $value->fechastodas;
			$entre['entregable'] = $value->cproyectoentregables;
			$entre['orden'] = $value->orden;
			$entre['allDay'] = true;

			$entre['cproyecto'] = $value->cproyecto;

			$entre['codpy'] = $value->codpy ? $value->codpy : 'Sin código' ;
			$entre['codigo'] = $value->codigo ? $value->codigo : 'Sin codificación' ;
			$entre['codigocliente'] = $value->codigocliente ? $value->codigocliente : 'Sin codificación' ;
			$entre['descripcion'] = $value->descripcion_entregable ? $value->descripcion_entregable : 'Sin descripción' ;
			$entre['resp'] = $value->resp ? $value->resp : 'No asignado' ;
			$entre['elab'] = $value->elab ? $value->elab : 'No asignado' ;
			$entre['revi'] = $value->revi ? $value->revi : 'No asignado' ;
			$entre['apr'] = $value->apr ? $value->apr : 'No asignado';

		if ($value->fec < $hoy ){
			$color = 'red';
			$letra = 'white';
		} 
		if ($hoy <= $value->fec && $hoy->addDay(3) >= $value->fec ) {
			$color = 'yellow';
			$letra = 'black';
		}
		if ($value->fec > $hoy->addDay(3) ) {
			$color = 'green';
			$letra = 'white';
		}

			$entre['textColor'] = $letra;
			$entre['color'] = $color;
			// $entre['editable'] = $editable;

			array_push($entregables, $entre);
			
		}

		$horario = [];

		$hor = DB::table('thorasextrascontrol')->get();

		foreach ($hor as $key => $hrs) {
			
			$h['dow'] = $hrs->id;
			$h['dia'] = $hrs->dia;
			$h['start'] = $hrs->horarioentrada;
			$h['end'] = $hrs->horariosalida;

			array_push($horario,$h);
		}

		$resultado = array_merge($cproyecto, $editable, $entregables);

		return $resultado;

	}

	private function horario()
	{
		$horario = [];

		$hor = DB::table('thorasextrascontrol')->get();

		foreach ($hor as $key => $hrs) {
			
			$h['dow'] = $hrs->id;
			$h['start'] = $hrs->horarioentrada;
			$h['end'] = $hrs->horariosalida;

			array_push($horario,$h);
		}

		return $horario;
	}

	private function lecaprendidas()
	{
		$cantle = DB::table('tleccionesaprendidas')->count();

		return $cantle;
	}

	public function gantentregables(Request $request){
		// dd($request->cproyecto);
		$pro = new ProyectoController();
		$gantt = $pro->listarTodosEntregables($request->cproyecto,'todos');

		return $gantt;
	}

    private function liderOgp($cpersona){

    	$esgerente = DB::table('tpersona as tp')
            ->leftjoin('tpersonarol as tpr', 'tp.cpersona', '=', 'tpr.cpersona')
            ->where('tpr.croldespliegue', '=', '19')
            ->where('tp.cpersona', '=', $cpersona)
            ->get();

        $eslider  =  DB::table('tproyectopersona as tpyp')
            ->leftjoin('tpersona as tpr', 'tpr.cpersona', '=', 'tpyp.cpersona')
            ->where('tpyp.cpersona', '=', $cpersona)
            ->where('tpyp.eslider','=','1')
            ->get();

    	$lider_or_gerente = '';

        if($esgerente)
        {
             $lider_or_gerente = 'gerente';
        }
        elseif($eslider)
        {
              $lider_or_gerente = 'lider';
        }

        return $lider_or_gerente;

    }

}


