<?php

namespace App\Http\Controllers\Edt;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Tproyectoedt;
use App\Models\Tproyecto;
use App\Models\Tproyectoinformacionadicional;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesruteo;
use App\Models\Tcatalogo;
use App\Models\Transmittal;
use App\Models\Transmittaldetalle;
use App\Models\Tproyectopersona;
use App\Models\Treglascondicion;
use App\Models\Tproyectoentregablesfecha;
use App\Models\Tpersona;
use App\Erp\FlujoLE;
use Carbon\Carbon;
use Auth;
use DB;

class EdtController extends Controller
{
	//public 
	public function grafico(){
		return view("edt.grafico");
	}	

	//ajax

	public function obtener($cproyecto){

		$documento = $this->obtenerRevisionEntregablesPryCli($cproyecto);
        $revisiones_internas=null;
        $crevision=null;
        if($documento){
            $revisiones_internas=$this->obtenerRevisionEntregablesPryInt($documento->cproyectodocumentos);
            $crevision=$revisiones_internas->cproyectoent_rev;
        }

		// $proyecto = Tproyecto::where("codigo","=",$cproyecto)->first();
		// $codigo = intval($proyecto->cproyecto);
		$lista = tproyectoedt::where("cproyecto", "=", $cproyecto)->orderBy("codigo", "asc")
		->where('cproyectoent_rev','=',$crevision)->get();
		// dd($lista);
		return $lista;
	}

	public function guardarHijo(Request $request){

		// dd($request->all());
		
		// $cproyecto = $request->input("cproyecto");
		
		// $proyecto = Tproyecto::where("codigo","=",$cproyecto)->first();
		// 
		$edtParent=DB::table('tproyectoedt')
	    ->where('cproyectoedt','=',$request->input('cproyectoedt_parent'))
	    ->first();

	    $nivelPadre=0;
	    $tipoEdt=null;
	    $revi=null;

	    if ($edtParent) {
	        $nivelPadre=$edtParent->nivel;
	        $tipoEdt=$edtParent->ctipoedt;
	        $revi=$edtParent->cproyectoent_rev;
	    }

	    $nivelPadre=intval($nivelPadre)+1;

		$codigo = $request->input("codigo");
		$cproyecto = $request->input("cproyecto");
		$cproyectoedt_parent = $request->input("cproyectoedt_parent");
		$descripcion = $request->input("descripcion");	

		$padre = 

		//guardar elemento hijo
		$hijo = new tproyectoedt();
		$hijo->codigo = $codigo;
		$hijo->cproyecto = $cproyecto;
		$hijo->cproyectoedt_parent = $cproyectoedt_parent;
		$hijo->descripcion = $descripcion;
		$hijo->ctipoedt = $tipoEdt;
		$hijo->orden = "b".$codigo;
		$hijo->nivel = $nivelPadre;
		$hijo->cproyectoent_rev = $revi;
		$hijo->save();

		//actualizar indicando que ahora tiene elementos hijos
		$padre = tproyectoedt::where("cproyectoedt", "=", $cproyectoedt_parent)->first();
		$padre->tipo = 1;
		$padre->save();

		return $hijo;
	}

	public function eliminarElemento(Request $request){
		$cproyectoedt = $request->input("cproyectoedt");
		$deletedRows = tproyectoedt::where('cproyectoedt', "=", $cproyectoedt)->delete();
		return $deletedRows;
	}

	//private

	private function obtenerRevisionEntregablesPryCli($cproyecto){

	    $revisiones = DB::table('tproyectodocumentos as pro')
	    ->join('tdocumentosparaproyecto as prydoc','pro.cdocumentoparapry','=','prydoc.cdocumentoparapry')
	    ->where('prydoc.codigo','=','LE')
	    ->where('pro.cproyecto','=',$cproyecto)
	    ->orderBy('fechadoc','desc')
	    ->select('pro.cproyectodocumentos','pro.fechadoc','pro.codigodocumento as codigo','pro.revisionactual as revision')
	    ->first();

	    return $revisiones;

	}

	private function obtenerRevisionEntregablesPryInt($cproyectodoc){

	    $revisiones = DB::table('tproyectoentregablesrevisiones as pro')
	    ->where('pro.cproyectodocumentos','=',$cproyectodoc)
	    ->orderBy('fecha_rev','desc')
	    ->first();

	    return $revisiones;

	}

// Inicio de funciones para ruteo de entregable
	public function grabarEntregablePrySelecRuteo(Request $request)
	{

		$entregables = $request->check_entre;
		$valor = $request->valor;

		$objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujo();
		
		$flujo = explode(',',$flujo);

		$primerflujo = reset($flujo);
		// dd($request->all(),$flujo);

		// dd($entregables);
		// 
		$cantFechas = $this->fechasentregables($entregables);
		$codigo_ent = $this->codigoentregables($entregables);

		// dd($cantFechas,$codigo_ent);
	if ($cantFechas == 'false' or $codigo_ent == 'false') {
		$condicion = 'false';
		return $condicion;
	}else {
		
		if ($valor == 'sele') {
			$ent = $entregables;
			$cproyecto = Tproyectoentregable::where('cproyectoentregables','=',$entregables)->first();
			$rev_siguiente = 0;
	        $this->grabaruteo($ent,$primerflujo,'REG',$rev_siguiente);
	        $cant = $this->cantidadentregablesruteo($cproyecto->cproyecto,$primerflujo,'REG');
	        return $cant;
		}
		if ($valor == 'check') {
			foreach ($entregables as $key => $ent) {
		        $cproyecto = Tproyectoentregable::where('cproyectoentregables','=',$ent)->first();
				$rev_siguiente = 0;
		        $this->grabaruteo($ent,$primerflujo,'REG',$rev_siguiente);
		    }
		        $cant = $this->cantidadentregablesruteo($cproyecto->cproyecto,$primerflujo,'REG');
		        return $cant;
		}

		if ($valor == 'ruteo') {
			foreach ($entregables as $key => $value) {

				$entre = explode('_',$value);
				$ent = $entre[0];
				$estado = $entre[1]; // etado de entregable ruteo

		        $cproyecto = Tproyectoentregable::where('cproyectoentregables','=',$ent)->first();
		        $flujonext = $objflujoEmp->flujonext($cproyecto->ruteo);

	        	$tr = $this->transmittal($ent,$cproyecto->revision);
	        		if ($tr) {
				        if ($tr->cmotivoenvio == 4 || $tr->cmotivoenvio == 5) {
				        	$rev = 50;	//cambia a revision 0
				        }else {
							$rev = 1 + intval($cproyecto->revision); //cambia a la siguiente revisión
				        }
	        		}
	        		else {
	        	// dd($cproyecto->revision);
	        			if ($cproyecto->cestadoentregableruteo == 'OBS') {
	        				$rev = $cproyecto->revision;	
	        			}else {
	        				$rev = 1 + $cproyecto->revision; //cambia a la siguiente revisión
	        			}
	        		}
		        $this->grabaruteo($ent,$flujonext,$estado,$rev);
		    }
		        // $cant = $this->cantidadentregablesruteo($cproyecto->cproyecto,$flujonext);
		        // return redirect()->route('ruteoedtproyecto');
		}

		if ($valor == 'todos') {
				// dd($entregables);
			foreach ($entregables as $key => $value) {

				$entre = explode('_',$value);
				$ent = $entre[0];
				$estado = $entre[1]; // etado de entregable ruteo
				$cond = $entre[2];

				$cproyecto = Tproyectoentregable::where('cproyectoentregables','=',$ent)->first();

				$flujoprev = $objflujoEmp->flujoprev($cproyecto->ruteo);
		        $flujonext = $objflujoEmp->flujonext($cproyecto->ruteo);

		        if ($estado == 'REV') {
		        	$tr = $this->transmittal($ent,$cproyecto->revision);
	        		if ($tr) {
				        if ($tr->cmotivoenvio == 4 || $tr->cmotivoenvio == 5) {
				        	$rev = '50';	//cambia a revision 0
				        }else {
							$rev = 1 + intval($cproyecto->revision); //cambia a la siguiente revisión
				        }
				        $this->grabaruteo($ent,$flujonext,$estado,$rev);
	        		}
	        		else {
	        			$rev = $cproyecto->revision; //cambia a la siguiente revisión
				        $this->grabaruteo($ent,$flujoprev,$estado,$rev);
	        		}
		        }
		        elseif ($estado == 'APR' and $cproyecto->revision == 1) {
		        	$rev = 1 + intval($cproyecto->revision); //cambia a la siguiente revisión
		        	$this->grabaruteo($ent,$flujonext,$estado,$rev);
		        }
		        else {
		        	$rev = $cproyecto->revision; //cambia a la siguiente revisión
		        	if ($estado == 'OBS') {
			        	$this->grabaruteo($ent,$flujoprev,$estado,$rev);
		        	}else {
		        		$this->grabaruteo($ent,$flujonext,$estado,$rev);
		        	}
		        }


			}
		}

        return redirect()->route('ruteoedtproyecto');
    }

	}

	private function cantidadentregablesruteo($cproyecto,$ruteo,$estado)
	{
		$cant = Tproyectoentregable::where('cproyecto','=',$cproyecto)->where('ruteo','=',$ruteo)->where('cestadoentregableruteo','=',$estado)->count();

		return $cant;
	}

	private function revisiones($rev)
	{
		$revisionEnt=Tcatalogo::where('codtab','=','00006')
           ->where('digide','=',$rev)
           ->first();

        return $revisionEnt;

	}

	private function transmittal($cproyectoentregables, $rev)
	{
		$tr = DB::table('ttransmittal as tt')
		->leftjoin('ttransmittaldetalle as ttd','ttd.ctransmittal','=','tt.ctransmittal')
		->where('tt.tipotransmittal','=','C-A')
		->where('ttd.cproyectoentregables','=',$cproyectoentregables)
		->where('ttd.revision_entregable','=',$rev)
		->first();

		return $tr;
	}

	private function fechasentregables($cproyectoentregables)
	{	

		if (count($cproyectoentregables) <= 1) {
			
			$entrega = is_array($cproyectoentregables);

			if ($entrega == true) {
				$entre = explode('_',$cproyectoentregables[0]);
				$ent = $entre[0];
			} else {
				$ent = $cproyectoentregables;
			}
			// $estado = $entre[1]; // etado de entregable ruteo
		// dd($ent);
			
			$fechas=DB::table('tproyectoentregablesfechas as pfe')  
	        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
	        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
	        ->select('pfe.ctipofechasentregable','ttf.descripcion as tipofec','pfe.fecha',DB::raw("to_char(pfe.fecha,'DD/MM/YYYY') as fecha"),'tpe.cproyectoentregables')
	        ->where('tpe.cproyectoentregables','=',$ent)
	        ->count();
	        
		        if ($fechas == 0) {
		        	$cantFechas = 'false';
					return $cantFechas;
				} 

		} else {
			
			foreach ($cproyectoentregables as $key => $value) {

			$entre = explode('_',$value);
			$ent = $entre[0];
			// $estado = $entre[1]; // etado de entregable ruteo
			
			$fechas=DB::table('tproyectoentregablesfechas as pfe')  
	        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
	        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
	        ->select('pfe.ctipofechasentregable','ttf.descripcion as tipofec','pfe.fecha',DB::raw("to_char(pfe.fecha,'DD/MM/YYYY') as fecha"),'tpe.cproyectoentregables')
	        ->where('tpe.cproyectoentregables','=',$ent)
	        ->count();
				
				if ($fechas == 0) {
					$cantFechas = 'false';
					return $cantFechas;
				} 
			}
		}

			$cantFechas = 'true';

        return $cantFechas;
	}

	private function codigoentregables($cproyectoentregables)
	{

		if (count($cproyectoentregables) <= 1) {

			$entrega = is_array($cproyectoentregables);

			if ($entrega == true) {
				$entre = explode('_',$cproyectoentregables[0]);
				$ent = $entre[0];
			} else {
				$ent = $cproyectoentregables;
			}
			// $estado = $entre[1]; // etado de entregable ruteo
			
			$codigo=DB::table('tproyectoentregables')  
	        ->select('codigo')
	        ->where('cproyectoentregables','=',$ent)
	        ->first();
	        
		        if ($codigo == '') {
		        	$codigo_ent = 'false';
					return $codigo_ent;
				} 

		} else {
			
			foreach ($cproyectoentregables as $key => $value) {

			$entre = explode('_',$value);
			$ent = $entre[0];
			// $estado = $entre[1]; // etado de entregable ruteo
			
			$codigo=DB::table('tproyectoentregables')  
	        ->select('codigo')
	        ->where('cproyectoentregables','=',$ent)
	        ->first();

				if ($codigo == '') {
					$codigo_ent = 'false';
					return $codigo_ent;
				} 
			}
		}

			$codigo_ent = 'true';

        return $codigo_ent;
	}

	private function grabaruteo($le,$ruteo,$estado,$rev){

		$objflujoEmp = new FlujoLE();
		// $flujonext = $objflujoEmp->flujonext($ruteo);
		// dd($le,$ruteo,$estado,$rev,$flujonext);

		$tentre = Tproyectoentregable::where('cproyectoentregables','=',$le)->first();
        $tentre->cper_modifica=Auth::user()->cpersona;
        $tentre->fecha_modificacion=Carbon::now();
        $tentre->ruteo = $ruteo;
        $tentre->revision=$rev;
        $tentre->cestadoentregableruteo = $estado;
        $tentre->save();
        

        $ruteo_traza = new Tproyectoentregablesruteo;
        $ruteo_traza->cproyectoentregable = $le;
        $ruteo_traza->ruteo_actual = $ruteo;
        $ruteo_traza->estado = $estado;
        $ruteo_traza->revision = $rev;
        $ruteo_traza->fecha = Carbon::now();
        $ruteo_traza->cpersona = Auth::user()->cpersona;
        $ruteo_traza->comentario = $tentre->comentario;
        $ruteo_traza->save();

        $tentre->comentario = null;
        $tentre->save();

        return $tentre;

	}

	public function verentregableruteo(Request $request){

		$cproyecto = $request->cproyecto;
		$ruteo = $request->ruteo;
		$estado = $request->estado;
		$vista = $request->vista;
		$fecha = $request->fecha;

		$entregables = [];

		// dd($request->all());

		if ($vista) {

			$entre = DB::table('tproyectoentregables as tpe')
			->join('tcatalogo as tc','tc.digide','=','tpe.revision')
			// ->leftjoin('tproyectoentregablesruteo as tper')
			// ->select('tpe.*','tc.*')
			// ->addSelect(DB::raw('select count(*) from tproyectoentregablesruteo as tper where tper.cproyectoentregable ='.$entregables->cproyectoentregables))
			->where('tpe.cproyecto','=',$cproyecto)
			->where('tpe.ruteo','=',$ruteo)
			->whereIn('tpe.cestadoentregableruteo',$estado);
			if($fecha){
			$entre = $entre->where('tpe.fecha_modificacion','=',$fecha);
			}

			$entre = $entre->where('tc.codtab','=','00006')
			->orderBy('tpe.centregable')
			->get();

			// dd($entre);

			foreach ($entre as $key => $value) {
				
			$contador = DB::table('tproyectoentregablesruteo as tper')
			->join('tpersona as tp','tp.cpersona','=','tper.cpersona')
			->where('tper.cproyectoentregable','=',$value->cproyectoentregables)
			->whereNotNull('tper.comentario')
			->count();

			$ent['cproyectoentregables'] = $value->cproyectoentregables ;
			$ent['codigo'] = $value->codigo ;
			$ent['revision'] = $value->revision ;
			$ent['descripcion_entregable'] = $value->descripcion_entregable ;
			$ent['ruteo'] = $value->ruteo ;
			$ent['ruta'] = $value->ruta ;
			$ent['cestadoentregableruteo'] = $value->cestadoentregableruteo ;
			$ent['comentario'] = $value->comentario ;
			$ent['descripcion'] = $value->descripcion ;
			$ent['valor'] = $value->valor ;
			$ent['cantidad'] = $contador ;
			$ent['cproyecto'] = $value->cproyecto ;

			array_push($entregables,$ent);

			}
			


			return view('partials.'.$vista,compact('entregables'));

		}else{

			$cant = $this->cantidadentregablesruteo($cproyecto,$ruteo,$estado);
			return $cant;

		}

	}

	public function verentregableruteocomentario(Request $request)
	{
		// dd($request->all());
		$cproyectoentregable = $request->cproyectoentregable;
		$ruteo = $request->ruteo;
		$revision = $request->rev;
		$vista = $request->vista;

		$trazabilidad = [];

		$comentario = DB::table('tproyectoentregablesruteo as tper')
		->join('tpersona as tp','tp.cpersona','=','tper.cpersona')
		->where('tper.cproyectoentregable','=',$cproyectoentregable)
		->where('tper.comentario','!=','')
		->orderBy('tper.fecha','ASC')
		->get();

		$contador = DB::table('tproyectoentregablesruteo as tper')
		->join('tpersona as tp','tp.cpersona','=','tper.cpersona')
		->where('tper.cproyectoentregable','=',$cproyectoentregable)
		->whereNotNull('tper.comentario')
		->count();

		$comen = DB::table('tproyectoentregables as tpe')
		// ->join('tpersona as tp','tp.cpersona','=','tpe.cpersona')
		->select('tpe.comentario','tpe.codigo','tpe.descripcion_entregable')
		->where('tpe.cproyectoentregables','=',$cproyectoentregable)
		->where('tpe.cper_modifica','=',Auth::user()->cpersona)
		// ->where('tpe.revision','=',$revision)
		->whereNotNull('tpe.comentario')
		// ->orderBy('tpe.fecha','ASC')
		->first();
		if ($comen) {
			$comenta = $comen->comentario;
			$codigo_ent = $comen->codigo;
			$nombre_ente = $comen->descripcion_entregable;
		}
		else {
			$comenta = null;
			$codigo_ent = '';
			$nombre_ente = '';
		}

		// dd($comentario);

		foreach ($comentario as $key => $value) {
			if ($value->cpersona == Auth::user()->cpersona) {
				$user = true;
			}
			else {
				$user = false;
			}

			$ver['cproyectoentregable'] = $value->cproyectoentregable;
			$ver['abreviatura'] = $value->abreviatura;
			$date = date_create($value->fecha);
			$ver['fecha'] = date_format($date, 'd-m-Y H:i:s');
			$ver['comentario'] = $value->comentario;
			$ver['cpersona'] = $user;
			$ver['cantidad'] = $contador;
            $imagen='images/'.$value->identificacion.'.jpg';

	          if(file_exists($imagen)){
				$ver['imagen'] = $value->identificacion;
	          }else{
	            $ver['imagen'] = 'sinfoto';
	          }

			array_push($trazabilidad, $ver);

		}


		// dd($trazabilidad,$comen);
		// $traza = $trazabilidad;


		return view('partials.'.$vista,compact('trazabilidad','contador','comenta','cproyectoentregable','codigo_ent','nombre_ente'));
	}

	public function actualizarruteo(Request $request)
	{
	// dd($request->all());
		$cproyectoentregables = $request->cproyectoentregables;
		$ruta = $request->ruta;
		$comentario = $request->comentario;
		$entregables = $request->entregables;
		$rutatodos = $request->rutatodos;

		if (isset($ruta)) {
			$tentre = $this->grabaruta($cproyectoentregables,$ruta);
		}
		if (isset($comentario)) {
			$tentre = $this->grabacomentario($cproyectoentregables,$comentario);
		}
		if (isset($entregables)) {
			foreach ($entregables as $key => $ent) {
				if ($ent != "") {
			$tentre = $this->grabacomentario($ent,$comentario);
				}
			}
		}
		if (isset($rutatodos)) {
			foreach ($entregables as $key => $ent) {
				if ($ent != "") {
			$tentre = $this->grabaruta($ent,$rutatodos);
				}
			}

			return $request->all();
		}


		return $tentre;

	}

	private function grabaruta($le,$ruta){

		$tentre = Tproyectoentregable::where('cproyectoentregables','=',$le)->first();
        $tentre->cper_modifica=Auth::user()->cpersona;
        $tentre->fecha_modificacion=Carbon::now();
        $tentre->ruta = $ruta;
        $tentre->save();

        return $tentre;

	}

	private function grabacomentario($le,$comentario){
		// dd($le,$comentario);
		$tentre = Tproyectoentregable::where('cproyectoentregables','=',$le)->first();
        $tentre->cper_modifica=Auth::user()->cpersona;
        $tentre->fecha_modificacion=Carbon::now();
        $tentre->comentario = $comentario;
        $tentre->save();

        return $tentre;

	}

	private function obtenerFechasXEntregableMenor($idEntregable,$orden){

        $fechas=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->select('pfe.ctipofechasentregable','ttf.descripcion as tipofec','pfe.fecha',DB::raw("to_char(pfe.fecha,'YYYY-MM-DD') as fecha"),'tpe.cproyectoentregables')
        ->where('tpe.cproyectoentregables','=',$idEntregable)
        ->where('ttf.orden','<',$orden)
		->orderBy('ttf.orden','desc')
        ->first();

        if ($fechas) {
	        $fecha = $fechas;
        }
        else {
        	$fecha = 'nohay';
        }

        return $fecha;

    }

    private function obtenerFechasXEntregableMayor($idEntregable,$orden){

        $fechas=DB::table('tproyectoentregablesfechas as pfe')  
        ->leftJoin('tproyectoentregables as tpe','tpe.cproyectoentregables','=','pfe.cproyectoentregable')    
        ->leftJoin('ttipofechasentregable as ttf','pfe.ctipofechasentregable','=','ttf.ctipofechasentregable')  
        ->select('pfe.ctipofechasentregable','ttf.descripcion as tipofec','pfe.fecha',DB::raw("to_char(pfe.fecha,'YYYY-MM-DD') as fecha"),'tpe.cproyectoentregables')
        ->where('tpe.cproyectoentregables','=',$idEntregable)
        ->where('ttf.orden','>',$orden)
		->orderBy('ttf.orden','asc')
        ->first();

        if ($fechas) {
	        $fecha = $fechas;
        }
        else {
        	$fecha = 'nohay';
        }

        return $fecha;

    }

	public function grabarfechaentregablecalendario(Request $request){

		$id = $request->id;
		$fecha_origen = $request->fecha_origen;
		$fecha_destino = $hoy = Carbon::parse($request->fecha_destino);
		$entregable = $request->entregable;
		$orden = $request->orden;

		$fecha_menor = $this->obtenerFechasXEntregableMenor($entregable,$orden);
		$fecha_mayor = $this->obtenerFechasXEntregableMayor($entregable,$orden);

		if ($fecha_menor == 'nohay') {
			$fecha_anterior = $fecha_menor;
		} else {
			$fecha_anterior = Carbon::parse($fecha_menor->fecha);
		}

		if ($fecha_mayor == 'nohay') {
			$fecha_posterio = $fecha_mayor;
		} else {
			$fecha_posterio = Carbon::parse($fecha_mayor->fecha);
		}

		// validaciones de fechas
		if ($fecha_anterior == 'nohay') {
			if ($fecha_destino > $fecha_posterio) {
				$resultado = "false";
			} else {
				$resultado = $this->savefechasentregables($id,$fecha_destino);
			}
		} else {
			if ($fecha_destino < $fecha_anterior) {
				$resultado = "false";
			}else {
				if ($fecha_posterio == 'nohay') {
					$resultado = $this->savefechasentregables($id,$fecha_destino);
				}else {
					if ($fecha_destino > $fecha_posterio) {
						$resultado = "false";
					} else {
						$resultado = $this->savefechasentregables($id,$fecha_destino);
					}
				}
			}
		}

	return $resultado;
	}

	private function savefechasentregables($id,$fecha)
	{
		$actualizado = Tproyectoentregablesfecha::where('cproyectoentregablesfechas','=',$id)->first();
		$actualizado->fecha = $fecha;
		$actualizado->save();

		return "true";
	}

	public function ruteoedtproyecto()
	{
		$objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujoEmp();

		$cestadoentregableruteo = "'APR','REV','OBS','REG'";
		// $cestadoentregableruteo = ['APR','REV','OBS','REG'];
		// dd($cestadoentregableruteo);

		$proyectos = DB::table('tproyecto as tp')
		->join('tproyectoentregables as tpe','tpe.cproyecto','=','tp.cproyecto')
		->join('tpersona as cli','tp.cpersonacliente','=','cli.cpersona')
		->join('tunidadminera as tu','tp.cunidadminera','=','tu.cunidadminera')
		->join('tproyectopersona as tpp','tpp.cproyecto','=','tp.cproyecto') 
		->select('tpe.cproyecto','tp.codigo','tp.nombre','cli.nombre as cliente','tu.nombre as uminera','tp.cpersona_gerente','tpe.fecha_modificacion');
		
		if ($flujo=='GP') {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where('tp.cpersona_gerente','=',Auth::user()->cpersona)
			->where('tpe.ruteo','=',$flujo);
		}elseif ($flujo=='LD') {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"),'>',0)
			->where('tpp.cpersona','=',Auth::user()->cpersona)->where('tpp.eslider','=',1)
			->where('tpe.cestadoentregableruteo','!=','REG');	
		}elseif ($flujo=='JA') {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"),'>',0);	
		}
		else {
		$proyectos = $proyectos->addSelect(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"))
			->where(DB::raw("(select count(*) from tproyectoentregables as ent where ruteo='".$flujo."' and ent.cproyecto=tpe.cproyecto and ent.fecha_modificacion=tpe.fecha_modificacion and ent.cestadoentregableruteo in(".$cestadoentregableruteo."))"),'>',0)
			->where('tpp.cpersona','=',Auth::user()->cpersona);
		}

		$proyectos = $proyectos->distinct('tp.cproyecto')
		// ->whereNotin('tpe.cestadoentregableruteo',['FIN'])
		->orderBy('tpe.fecha_modificacion');
		//->get();

		//almacena $proyectos en $proyectosEntregables
		$proyectosEntregables=$proyectos;

		//Termina de obtener los datos de la consulta
		$proyectos = $proyectos->get();

		//Agrega el codigo de entregable al select
		$proyectosEntregables = $proyectosEntregables->addSelect('tpe.cproyectoentregables')->get();

		foreach ($proyectos as $key => $pry) {
			$array_entregables=[];
			foreach ($proyectosEntregables as $key => $en) {

			//Agrega a $array_entregables solo si el proyecto disgregadoo es igual al de $proyectos y si las fechas coinciden
				
				if ($en->cproyecto==$pry->cproyecto) {
					if ($en->fecha_modificacion==$pry->fecha_modificacion) {
						array_push($array_entregables, $en->cproyectoentregables);					
					}
				}
			}

			//Une todos los entregables encontrados por proyecto
			$pry->centregables_todos=implode(',', $array_entregables);

		}

		return view('proyecto.ruteoedtproyecto',compact('proyectos','flujo'));
	}

	public function colaentregable(Request $request)
	{
		$entregables = $request->le;

		$canti_cd = [];

		$objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujo();

		$entre = explode('_',$entregables[0]);
		$ent = $entre[0];
		$estado = $entre[1]; // etado de entregable ruteo
		$cond = $entre[2];

		$cproyecto = Tproyectoentregable::where('cproyectoentregables','=',$ent)->first();
		$cd = Tproyectoinformacionadicional::where('cproyecto','=',$cproyecto->cproyecto)->first();
		$per = tpersona::where('cpersona','=',$cd->cpersona_cdocumentario)->first();
		$proyectos = DB::table('tproyectoinformacionadicional')->where('cpersona_cdocumentario','=',$cd->cpersona_cdocumentario)->lists('cproyecto');
		
		$flujoprev = $objflujoEmp->flujoprev($cproyecto->ruteo);
        $flujonext = $objflujoEmp->flujonext($cproyecto->ruteo);

        $cantidad = Tproyectoentregable::where('ruteo','=',$flujonext)->where('cestadoentregableruteo','=',$estado)->wherein('cproyecto',$proyectos)->count();

        array_push($canti_cd,$per->abreviatura,$cantidad);

		// dd($proyectos);
        
        return $canti_cd;
	}

	
}



