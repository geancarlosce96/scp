<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Alternativa;
use App\Models\Capacitacion;
use App\Models\Capacitacion_aprobacion;
use App\Models\Capacitacion_persona;
use App\Models\Evaluacion;
use App\Models\Evaluacion_persona;
use App\Models\Pregunta;
use App\Models\Tusuario;
use Auth;
use DB;

class TestController extends Controller
{
	public function index()
	{
		return view("test");
	}

}
