<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use App\Models\Alternativa;
use App\Models\Capacitacion;
use App\Models\Capacitacion_aprobacion;
use App\Models\Capacitacion_persona;
use App\Models\Evaluacion;
use App\Models\Evaluacion_persona;
use App\Models\Pregunta;
use App\Models\Tusuario;
use App\Models\Tpersona;
use App\Lib\EmailSend;
use Auth;
use DB;

class CapacitacionController extends Controller
{
	public function index()
	{
		return view("capacitacion.form", ["opcion" => "registrar"]);    	
	}

	private function valorNull($valor, $defecto=null)
	{
		if($valor!=null or $valor!="")
			return $valor;
		return $defecto;
	}

	public function verCapacitacion($id)
	{
		return view("capacitacion.form", ["id" => $id, "opcion" => "ver"]);
	}

	public function obtenerInfoCapacitacion($id)
	{
		$capacitacion = Capacitacion::find($id);
		$capacitacion->evaluacion = Evaluacion::where("capacitacion_id", "=", $id)->first();

		return $capacitacion;
	}

	private function procesar_aprobadores($lista_cadena, $capacitacion_id, $proyecto_titulo = "")
	{
		$aprobadores = explode(",", $lista_cadena);

		foreach ($aprobadores as $key => $nombre) {
			//buscar en la bd
			$usuario = Tusuario::where("nombre", "=", trim($nombre) )->first();
			
			if($usuario!=null){
				//registrar
				$detalle = new Capacitacion_aprobacion();
				$detalle->capacitacion_id = $capacitacion_id;
				$detalle->usuario_id = $usuario->cusuario;
				$detalle->save();

				//obtener persona
				$persona = tpersona::where("cpersona", "=", $usuario->cpersona)->first();

				//mandar email
				/*$titulo = "Solicitud para Aprobar Capacitación";
				$para_correo = $nombre."@anddes.com";
				$para_nombre = $persona->abreviatura;				
				$data = ["de_usuario" => Auth::user()->nombre, "proyecto" => $proyecto_titulo, "titulo" => $titulo];
				$result = EmailSend::enviar_individual($data, $para_correo, $para_nombre, $titulo);*/
			}            
		}

		return 0;
	}

	private function procesar_personas($lista_cadena, $capacitacion_id)
	{
		$personas = explode(",", $lista_cadena);        

		foreach ($personas as $key => $nombre) {
			//buscar en la bd
			$usuario = Tusuario::where("nombre", "like", trim($nombre) )->first();
			
			if($usuario!=null){                
				//registrar
				$detalle = new Capacitacion_persona();
				$detalle->capacitacion_id = $capacitacion_id;
				$detalle->usuario_id = $usuario->cusuario;
				$detalle->save();    
			}else if(strpos($nombre, "(AREA)") != false){
				$this->procesar_area($capacitacion_id, $nombre);
			}
		}

		return 0;
	}

	private function procesar_area($capacitacion_id, $area_cadena)
	{
		$area = str_replace(" (AREA)", "", $area_cadena);         
		$delimitador_pos = strpos($area, "-");        
		$area_id = substr($area, $delimitador_pos+1);
		
		$usuarios = DB::table('tpersonadatosempleado')
			->join('tusuarios', 'tpersonadatosempleado.cpersona', '=', 'tusuarios.cpersona')            
			->select('tusuarios.cusuario')
			->where('tpersonadatosempleado.carea', "=", $area_id)
			->get();

		foreach ($usuarios as $key => $usuario) {
			$detalle = new Capacitacion_persona();
			$detalle->capacitacion_id = $capacitacion_id;
			$detalle->usuario_id = $usuario->cusuario;
			$detalle->save();  
		}
		
		return;
	}

	private function procesar_area_evaluacion($evaluacion_id, $area_cadena)
	{
		$area = str_replace(" (AREA)", "", $area_cadena);         
		$delimitador_pos = strpos($area, "-");        
		$area_id = substr($area, $delimitador_pos+1);
		
		$usuarios = DB::table('tpersonadatosempleado')
			->join('tusuarios', 'tpersonadatosempleado.cpersona', '=', 'tusuarios.cpersona')            
			->select('tusuarios.cusuario')
			->where('tpersonadatosempleado.carea', "=", $area_id)
			->get();

		foreach ($usuarios as $key => $usuario) {
		   $detalle = new Evaluacion_persona();
			$detalle->evaluacion_id = $evaluacion_id;
			$detalle->usuario_id = $usuario->cusuario;
			$detalle->save(); 
		}
		
		return;
	}

	private function procesar_personas_evaluacion($lista_cadena, $evaluacion_id)
	{
		$personas = explode(",", $lista_cadena);

		foreach ($personas as $key => $nombre) {            
			//buscar en la bd
			$usuario = Tusuario::where("nombre", "like", trim($nombre) )->first();
			
			if($usuario!=null){ 
				//registrar
				$detalle = new Evaluacion_persona();
				$detalle->evaluacion_id = $evaluacion_id;
				$detalle->usuario_id = $usuario->cusuario;
				$detalle->save();    
			}else if(strpos($nombre, "(AREA)") != false){
				$this->procesar_area_evaluacion($evaluacion_id, $nombre);
			}            
		}

		return 0;
	}

	public function procesar(Request $request)
	{    	
		
		$motivo = $request->input("txtmotivo");
		$videos = $request->input("txtvideos");
		$aprobadores = $request->input("aprobadores");
		$personas = $request->input("personas");

		$chkevaluacion = $request->input("chkevaluacion");

		$capacitacion = new Capacitacion();
		$capacitacion->aprobadores = $aprobadores;
		$capacitacion->cantidad_evaluacion = 1;
		$capacitacion->fecha_inicio = $this->valorNull($request->input("txtfechadisponible"), date("Y-m-d H:i:s"));
		$capacitacion->motivo = $motivo;
		$capacitacion->personas = $personas;
		$capacitacion->titulo = $request->input("txttitulo");
		$capacitacion->usuario_id = Auth::user()->cusuario;
		$capacitacion->videos = $videos;

		$capacitacion->save();
		$capacitacion->generarCodigo();

		$this->procesar_aprobadores($aprobadores, $capacitacion->id, $capacitacion->titulo);
		$this->procesar_personas($personas, $capacitacion->id);

		if(isset($chkevaluacion)){
			//si tiene check, mandar evaluacion
			$evaluacion = new Evaluacion();
			$evaluacion->cantidad_preguntas = $this->valorNull($request->input("txtcantidadpreguntas"), 0);            
			$evaluacion->capacitacion_id = $capacitacion->id;
			$evaluacion->duracion = $this->valorNull($request->input("txtduracion"), 0);
			$evaluacion->fecha_fin = $this->valorNull($request->input("txtfechafin"));
			$evaluacion->fecha_inicio = $this->valorNull($request->input("txtfechainicio"), null);
			$evaluacion->objetivo = $request->input("txtobjetivo");

			$evaluacion->calcular_valores();

			if($evaluacion->fecha_inicio == null)
				$evaluacion->fecha_inicio = $capacitacion->fecha_inicio;

			$evaluacion->save();

			//registrar relacion con personas
			$this->procesar_personas_evaluacion($personas, $evaluacion->id);            
			
			return redirect("evaluacion/$evaluacion->id/registrar?_redirect=capacitacionMsjConfirmacion");
		}		
		
		//grabar, mensaje de completado
		return view("capacitacion.completado");
	}

	public function evaluacion_form($id)
	{        
		$evaluacion = Evaluacion::find($id);
		$usuario_id = Auth::user()->cusuario;  

		if($evaluacion==null)
			return redirect("capacitaciones/propuestas");

		$capacitacion = Capacitacion::find($evaluacion->capacitacion_id);

		return view ("capacitacion.evaluacion", ["id" => $id, "evaluacion" => $evaluacion, "capacitacion" => $capacitacion,
			"usuario_id" => $usuario_id ]);
	}

	private function eliminar_preguntas($evaluacion_id)
	{
		$preguntas = Pregunta::where("evaluacion_id", "=", $evaluacion_id)->get();

		foreach ($preguntas as $key => $pregunta)
			DB::table('alternativa')->where('pregunta_id', '=', $pregunta->id)->delete();


		DB::table('pregunta')->where('evaluacion_id', '=', $evaluacion_id)->delete();
	}

	public function evaluacion_registrar(Request $request, $id)
	{
		$preguntas = $request->input("preguntas");
		$_redirect = $request->input("_redirect");
		$msjRespuesta = "";

		if(count($preguntas)<=0)
			return ["estado" => 1];

		$evaluacion = Evaluacion::find($id);

		//eliminar lo que ya existe
		$this->eliminar_preguntas($evaluacion->id);		

		if($evaluacion->cantidad_preguntas!=count($preguntas)){
			$evaluacion->cantidad_preguntas = count($preguntas);
			$evaluacion->save();
		}			

		foreach ($preguntas as $key => $objPregunta) {
			$pregunta = new Pregunta();
			$pregunta->descripcion = $objPregunta['descripcion'];
			$pregunta->evaluacion_id = $id;
			$pregunta->save();

			$opcion_unica = 0;
			
			foreach ($objPregunta['alternativas'] as $key => $objAlternativa) {
				$alternativa = new Alternativa();
				$alternativa->descripcion = $objAlternativa['descripcion'];
				$alternativa->pregunta_id = $pregunta->id;
				$alternativa->correcta = ($objAlternativa['checked']=="true") ? 1 : 0;
				$alternativa->save();

				$opcion_unica += $alternativa->correcta;
			}

			$pregunta->opcion_unica = ($opcion_unica<=1) ? 1 : 0;
			$pregunta->save();
		}

		if(trim($_redirect) == "capacitacionMsjConfirmacion")
			$msjRespuesta = "Se ha guardado la evaluación. Se notificará a los aprobadores. Luego de ser aprobada, los usuarios tendrán disponible la evaluación y la capacitación.";
		else
			$msjRespuesta = "Se ha guardado la evaluación.";

		return ["estado" => 0, "msj" => $msjRespuesta];

		/*if( isset($_redirect)!= null && $_redirect == "capacitacionMsjConfirmacion" )
			return view("capacitacion.completado");

		return redirect("capacitaciones/propuestas");*/
	}

	public function capacitacionesRegistradas()
	{
		$usuario_id = Auth::user()->cusuario;        
		
		//ordernar por estado, titulo
		//obtener capacitaciones del usuario que registro y de los aprobadores
		$lista = [];
		$capacitaciones = Capacitacion::where("usuario_id", "=", $usuario_id)->get();
		$aprobaciones = DB::table('capacitacion')
			->join('capacitacion_aprobacion', 'capacitacion_aprobacion.capacitacion_id', '=', 'capacitacion.id')            
			->select('capacitacion.*')
			->where("capacitacion_aprobacion.estado", "=", 1)
			->where("capacitacion_aprobacion.usuario_id", "=", $usuario_id)
			->get();

		$aprobaciones_cantidad = count($aprobaciones);

		$i=1;
		
		foreach ($aprobaciones as $key => $aprobacion){
			$aprobacion->n = $i;
			$aprobacion->estado_descripcion = $this->obtenerEstadoCapacitacion($aprobacion->estado);
			$aprobacion->aprobar = true;
			array_push($lista, $aprobacion);
			$i++;
		}

		foreach ($capacitaciones as $key => $item) {
			$item->n = $i;
			$item->estado_descripcion = $this->obtenerEstadoCapacitacion($item->estado);
			$item->aprobar = false;            
			array_push($lista, $item);
			$i++;
		}

		return view("capacitacion.list", ["capacitaciones" => $lista, 
			"aprobaciones_cantidad" => $aprobaciones_cantidad]);
	}

	private function obtenerEstadoEvaluacionDescripcion($estado)
	{
		switch ($estado) {
			case '1':
				return "En proceso";
				break;
			case '2':
				return "Realizado";
				break;
		}

		return "Pendiente";
	}

	private function obtenerEstadoCapacitacion($estado)
	{
		$descripcion = "";
		
		switch ($estado) {
			case '0':
				$descripcion = "Pendiente";
				break;
			case '1':
				$descripcion = "Aprobado";
				break;
			case '2':
				$descripcion = "Finalizado";
				break;
			case '3':
				$descripcion = "Cancelado";
				break;
			case '4':
				$descripcion = "Observado";
				break;            
		}

		return $descripcion;
	}

	public function capacitaciones()
	{
		$usuario_id = Auth::user()->cusuario;
		// dd($usuario_id);
		$fecha = date("Y-m-d H:i:s");        
		$capacitaciones = DB::table('capacitacion_persona')
			->join('capacitacion', 'capacitacion_persona.capacitacion_id', '=', 'capacitacion.id')            
			// ->select('capacitacion.*')
			->where("capacitacion.estado", "=", 1)
			->where("capacitacion_persona.usuario_id", "=", $usuario_id)
			->where("capacitacion.fecha_inicio", "<=", $fecha)
			->get();        
		$i=1;

		foreach ($capacitaciones as $key => $item) {            
			$item->n = $i;

			$item->evaluacion = $this->obtenerEvaluacion($item->id);  
			$item->evaluacionPersona = $this->obtenerEvaluacionPersona($item->evaluacion != null  ? $item->evaluacion->id : 0);            
			$item->estado_descripcion = $this->obtenerEstadoCapacitacion($item->estado);
			$item->estadoEvaluacionDescripcion = $this->obtenerEstadoEvaluacionDescripcion($item->evaluacionPersona != null ? $item->evaluacionPersona->estado : 0);

			$i++;
		}
// dd($capacitaciones);
		return view("evaluacion.list", [ "capacitaciones" => $capacitaciones ]);
	}

	private function obtenerEvaluacion($capacitacion_id)
	{
		$evaluacion = Evaluacion::where("capacitacion_id", "=", $capacitacion_id)->first();
		return $evaluacion;
	}

	private function obtenerEvaluacionPersona($evaluacion_id)
	{
		$usuario_id = Auth::user()->cusuario;
		$evaluacion = Evaluacion_persona::where("evaluacion_id", "=", $evaluacion_id)
			->where("usuario_id", "=", $usuario_id)
			->first();
		return $evaluacion;
	}

	public function rendir($id)
	{
		$usuario_id = Auth::user()->cusuario;

		$evaluacion = Evaluacion::find($id);        
		$evaluacion->preguntas = $this->obtenerPreguntas($id, true);

		$evaluacion_persona = Evaluacion_persona::where("evaluacion_id", "=", $evaluacion->id)
			->where("usuario_id", "=", $usuario_id)->where("estado", "=", 0)->first();

		if($evaluacion_persona==null)
			return redirect("capacitaciones");

		$evaluacion_persona->estado = 1;
		$evaluacion_persona->save();

		return view("evaluacion.form", ["evaluacion" => $evaluacion]);
	}

	private function aleatorioPreguntas(&$preguntas)
	{
		$cantidad_mezclas =  rand(1, 10);
		$cantidad = count($preguntas);

		for ($i=0; $i <$cantidad_mezclas; $i++) { 
			$numOrigen = rand(1, $cantidad)-1;
			$numDestino = rand(1, $cantidad)-1;

			$objTemp = $preguntas[$numOrigen];
			$preguntas[$numOrigen] = $preguntas[$numDestino];
			$preguntas[$numDestino] = $objTemp;
		}
		
	}

	private function obtenerPreguntas($id, $aleatorio = false)
	{
		$preguntas = Pregunta::where("evaluacion_id", "=", $id)->get();        
		$this->aleatorioPreguntas($preguntas);

		$i = 1;

		foreach ($preguntas as $key => $pregunta) {
			$pregunta->n = $i;
			$i++;
			$pregunta->alternativas = $this->obtenerAlternativas($pregunta->id, true);
		}

		return $preguntas;
	}

	private function obtenerAlternativas($id, $aleatorio = false)
	{
		$alternativas = Alternativa::where("pregunta_id", "=", $id)->get();
		$this->aleatorioPreguntas($alternativas);

		$i = 1;

		foreach ($alternativas as $key => $alternativa) {
			$alternativa->n = $i;
			$i++;            
		}

		return $alternativas;
	}

	public function rendir_verificar(Request $request, $id)
	{
		$puntos = 0;        
		$usuario_id = Auth::user()->cusuario;
		$preguntas = $request->input("preguntas");

		//obtener evaluacion
		$e = Evaluacion::find($id);

		//registrar la puntuacion        
		$evaluacion = Evaluacion_persona::where("evaluacion_id", "=", $id)
			->where("usuario_id", "=", $usuario_id)->where("estado", "=", 1)->first();

		if($evaluacion == null)
			return redirect("capacitaciones");
		
		//obtener preguntas
		$e->preguntas = $this->obtenerPreguntas($id);

		//recorrer segun la cantidad de preguntas mostradas
		foreach ($preguntas as $key => $pregunta) {
			if($this->verificar($pregunta, $request->input("opc_".$pregunta)))
				$puntos++;
		}

		$msg = "";

		if($evaluacion == null)
			return "no hay evaluacion relacionada";
		   
		$evaluacion->puntaje = $puntos;        
		
		if($puntos >= $e->cantidad_preguntas_min){
			$msg = "Ud ha aprobado";
			$evaluacion->estado = 2;
		}            
		else{
			$evaluacion->estado = 3;
			$msg = "Ud ha desaprobado";
		}

		$evaluacion->save();

		//obtener capacitacion, actualizar capacitacion

		$capacitacion = Capacitacion::find($e->capacitacion_id);
		$capacitacion_persona = Capacitacion_persona::where("capacitacion_id", "=", $capacitacion->id)
			->where("usuario_id", "=", $usuario_id)->first();

		$capacitacion_persona->estado = 2;
		$capacitacion_persona->save();

		return view("evaluacion.mensaje", ["msg" => $msg]);
	}

	private function obtenerEstadoEvaluacion($id)
	{
		$evaluacion = Evaluacion::where("capacitacion_id", "=", $id)->where("usuario_id", "=", 1)->first();
		if($evaluacion != null)
			return $evaluacion->estado;

		return 0;
	}

	private function verificar($pregunta_respondida_id, $alternativas_respondidas_id)
	{
		if($alternativas_respondidas_id == null)
			return false;

		//verificar preguntas mostradas con su respuesta, con la lista de preguntas y respuestas
		$alternativas = Alternativa::where("pregunta_id", "=", $pregunta_respondida_id)->where("correcta","=",1)->get();
		$puntos = 0;
		$puntos_total = count($alternativas);        

		if(count($alternativas_respondidas_id) > $puntos_total)
			return false;

		foreach ($alternativas_respondidas_id as $key => $alternativa_respondida) {
			foreach ($alternativas as $key => $alternativa) {                

				if($alternativa_respondida == $alternativa->id){                    
					$puntos++;
				}
			}
		}

		if($puntos_total == $puntos)
			return true;

		return false;
	}

	public function ver($id, $video_numero = 1)
	{
		$embeber = true;
		$capacitacion = Capacitacion::find($id);
		$link_video = $this->obtenerVideoSegunNumero($capacitacion->videos, $video_numero);
		$cantidad_videos = count( explode("\n", $capacitacion->videos) );
		$video_siguiente_numero = $video_numero;
		$link_video = $this->convertirLinkEmbebido($link_video, $embeber);          

		if($video_numero < $cantidad_videos)
			$video_siguiente_numero++;
		else
			$video_siguiente_numero = 0;      
		
		return view("evaluacion.ver",
			["capacitacion" => $capacitacion, 
			"link_video" => $link_video,
			"cantidad_videos" => $cantidad_videos,
			"video_siguiente_numero" => $video_siguiente_numero,
			"video_numero_actual" => $video_numero,
			"embeber" => $embeber]
		);
	}

	private function convertirLinkEmbebido($link, &$embeber=true)
	{
		if(strpos( $link, "youtube")!=false)
			return $this->convertirLinkYoutubeAEmbebido($link);

		if(strpos($link, "drive.google.com")!=false)
			return $link;

		$embeber = false;
		return $link;
	}

	private function convertirLinkYoutubeAEmbebido($link)
	{
		//convertir de https://www.youtube.com/watch?v=AtGDsy6AJW4 a https://www.youtube.com/embed/vWlToJhTHIY        
		$cantidad_total = strlen($link);
		$pos_video = strpos($link, "v=") + 2;
		//echo $link;
		//echo $pos_video;
		$link = "https://www.youtube.com/embed/". substr( $link , $pos_video);
		return $link;      
		
	}

	private function obtenerVideoSegunNumero($videos_origen, $num_video)
	{
		$videos = explode("\n", $videos_origen);
		return $videos[$num_video-1];
	}

	public function obtenerPersonas(Request $request)
	{        
		$lista = [];

		//areas 
		$areasDB = DB::table('tareas as a')
			->select('a.carea', 'a.descripcion')
			->orderBy('a.carea', 'ASC')
			->orderBy('a.descripcion', 'ASC')
			->get();

		foreach ($areasDB as $key => $area) {
			array_push($lista, $area->descripcion." -$area->carea (AREA)");
		}

		//personas 
		$personasDB = DB::table('tusuarios as u')
			->select('u.nombre')
			->orderBy('u.nombre', 'ASC')
			->where('u.estado','=','ACT')
			->get();        

		foreach ($personasDB as $key => $persona) {            
			array_push($lista, $persona->nombre);
		}        

		return $lista;
	}

	public function obtenerAprobadores(Request $request)
	{
		$personasDB = DB::table('tusuarios as u')
			->select('u.nombre')
			->orderBy('u.nombre', 'ASC')
			->where('u.estado','=','ACT')
			->get();

		$personas = [];

		foreach ($personasDB as $key => $persona) {            
			array_push($personas, $persona->nombre);
		}

		return $personas;
	}

	public function capacitacion_aprobar($id)
	{
		$usuario_id = Auth::user()->cusuario;
		
		$capacitacion_aprobacion = Capacitacion_aprobacion::where("capacitacion_id", "=", $id)->
			where("usuario_id", "=", $usuario_id)->where("estado", "=", 1)->first();

		if($capacitacion_aprobacion!=null){
			$capacitacion_aprobacion->estado = 2;
			$capacitacion_aprobacion->save();
		}

		$this->capacitacion_comprobar_aprobaciones($id);        
		
		return redirect("capacitaciones/propuestas");
	}

	private function capacitacion_comprobar_aprobaciones($id)
	{
		//si tiene más personas por aprobar la capacitacion

		$pendientes = Capacitacion_aprobacion::where("capacitacion_id", "=", $id)
			->where("estado", "=", 1)->get();        

		if(count($pendientes) == 0){
			$capacitacion = Capacitacion::find($id);
			$capacitacion->estado = 1;
			$capacitacion->save();    
		}
		
	}

	public function capacitaciones_usuarios($id){        

		$capacitacion = DB::table('capacitacion')
			->join('tusuarios', 'tusuarios.cusuario', '=', 'capacitacion.usuario_id')
			->select('tusuarios.nombre', 'capacitacion.*')
			->where("capacitacion.id", "=", $id)->first();

		$usuarios = DB::table('capacitacion_persona')
			->join('tusuarios', 'tusuarios.cusuario', '=', 'capacitacion_persona.usuario_id')
			->select('tusuarios.*')
			->where("capacitacion_persona.capacitacion_id", "=", $id)->get();

		$i=1;
		foreach ($usuarios as $key => $e)
			$e->n = $i++;

		return view("capacitacion.usuarios", [
			"usuarios" => $usuarios, "capacitacion" => $capacitacion, 
		]);
	}

	public function capacitaciones_usuarios_evaluados($id)
	{
		$capacitacion = DB::table('capacitacion')
			->join('tusuarios', 'tusuarios.cusuario', '=', 'capacitacion.usuario_id')
			->select('tusuarios.nombre', 'capacitacion.*')
			->where("capacitacion.id", "=", $id)->first();

		$evaluacion = Evaluacion::where("capacitacion_id", "=", $id)->first();

		$usuarios = DB::table('evaluacion_persona')
			->join('tusuarios', 'tusuarios.cusuario', '=', 'evaluacion_persona.usuario_id')
			->select('tusuarios.nombre', 'evaluacion_persona.*')
			->where("evaluacion_persona.evaluacion_id", "=", $evaluacion->id)->get();

		$i=1;
		foreach ($usuarios as $key => $e){
			$e->n = $i++;
			$e->estado_descripcion = $this->estado_descripcion($e->estado);
		}

		return view("evaluacion.usuarios", [
			"usuarios" => $usuarios, "capacitacion" => $capacitacion, "evaluacion" => $evaluacion
		]);   
	}

	public function estado_descripcion($estado)
    {
    	
    	if($estado == 1)
            return "En proceso";
        else if($estado == 2)
            return "Aprobado";
        else if($estado == 3)
            return "Desaprobado";
        
        return "Pendiente";
    }

	public function evaluacion_cancelar($id)
	{
		$usuario_id = Auth::user()->cusuario;
		$evaluacion = Evaluacion_persona::where("evaluacion_id", "=", $id)->where("usuario_id", "=", $usuario_id)->first();
		$evaluacion->estado = 3;
		$evaluacion->save();

		return redirect("capacitaciones");
	}

	public function editarEvaluacion($capacitacion_id)
	{
		$evaluacion = Evaluacion::where("capacitacion_id", "=", $capacitacion_id)->first();

		if($evaluacion == null)
			return redirect("capacitaciones/propuestas");

		return redirect("evaluacion/$evaluacion->id/registrar");
	}

	public function obtenerPreguntasEvaluacion($evaluacion_id)
	{
		$preguntas = Pregunta::where("evaluacion_id", "=", $evaluacion_id)->get();

		foreach ($preguntas as $key => $pregunta)
			$pregunta->alternativas = Alternativa::where("pregunta_id", "=", $pregunta->id)->get();

		return $preguntas;
	}

	public function retirarUsuario($id, $usuario_id)
	{
		$capacitacion = Capacitacion::find($id);
		
		if($capacitacion == null)
			return redirect("capacitaciones/propuestas");

		DB::table('capacitacion_persona')->where('capacitacion_id', '=', $capacitacion->id)
			->where('usuario_id', '=', $usuario_id)->delete();

		$evaluacion = Evaluacion::where("capacitacion_id", "=", $capacitacion->id)->first();

		if($evaluacion != null)
			DB::table('evaluacion_persona')->where('evaluacion_id', '=', $evaluacion->id)
			->where('usuario_id', '=', $usuario_id)->delete();		
		
		return redirect("capacitaciones/$capacitacion->id/usuarios") ;
	}

	public function obtenerPlantilla($codigo)
	{
		$capacitacion = Capacitacion::where("codigo", "=", $codigo)->first();
		$data["capacitacion"] = $capacitacion;

		if($capacitacion == null)
			$data["msj"] = "No se ha encontrado planitilla con ese código indicado.";
		else
			$data["msj"] = "Plantilla con código $codigo ha sido cargada";

		return $data;
	}
	
}
