<?php

namespace App\Http\Controllers\Propuesta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tpai;
use App\Models\Tubigeo;
use App\Models\Tunidadmineraestado;
use App\Models\Tunidadminera;
use App\Models\Tunidadmineracontacto;
use App\Models\Ttiposcontacto;
use App\Models\Tcontactocargo;
use App\Models\Invitacion;
use App\Models\Preliminar;
use App\Lib\EmailSend;
use Auth;
use DB;
use Carbon\Carbon;
use Redirect;

class PreliminarController extends Controller
{
	private function valorNull($valor, $defecto=null){
        if($valor!=null or $valor!="")
            return $valor;
        return $defecto;
	}

	public function preliminar(){

		$cliente = $this->verCliente();
		$tsedes = $this->sedes();

		$codigopreliminar = $this->existepreliminar();

		$listarpreliminar = DB::table('preliminar as pre')
		->leftjoin('tunidadminera as tum','tum.cunidadminera','=','pre.unidadminera_id')
		->leftjoin('tpersona as cli','cli.cpersona','=','tum.cpersona')
		->select('pre.*','tum.nombre as uminera','cli.nombre')
		->get();

		$tpais = Tpai::lists('descripcion','cpais')->all();
        $tpais = [''=>''] + $tpais;

        $tpais_um = Tpai::lists('descripcion','cpais')->all();
        $tpais_um = [''=>''] + $tpais;

        $umineraestado = Tunidadmineraestado::lists('descripcion','cumineraestado')
        ->all();
        // $umineraestado = [''=>''] + $umineraestado;

        /* Unidad Minera Contactos */
        $tcargo= DB::table('tcontactocargo')
        ->lists('descripcion','ccontactocargo');
        $tcargo = [''=>''] + $tcargo;

        $ttipo= DB::table('ttiposcontacto')
        ->lists('descripcion','ctipocontacto');
        $ttipo = [''=>''] + $ttipo;

        $aniocod= Carbon::now();
        $aniocod = $aniocod->year;
        $aniocod=substr($aniocod, 2,2);

		// dd($listarpreliminar);
		$vista = 'preliminar';
		$cod = 'hidden';

		return view('propuesta.v2.preliminar', compact('cliente','listarpreliminar','vista','tpais','tpais_um','umineraestado','tcargo','ttipo','tsedes','aniocod','codigopreliminar','cod'));
	}

	public function verCliente(){

		$cliente = Tpersona::where('ctipopersona','=','JUR')->orderBy("nombre","ASC")->get();

		return $cliente;

	}

	private function existepreliminar(){

		$cant='';
		$anio = date('y');
		
		$codigo = DB::table('preliminar')
		->orderBy('codigo_preliminar','desc')
		->first();

		$cod = explode('.', $codigo->codigo_preliminar);

		if ($cod[0]==$anio) {
			// dd($anio);
			$cant= $codigo->codigo_preliminar;
			$corre = explode('.', $cant);
			$newcod = str_pad($corre[1]+1,2,'0',STR_PAD_LEFT);
			$codigonuevo = $newcod;
		}
		else {
		// dd($cod[0],$anio);
			$codigonuevo = $cant;	
		}

		return $codigonuevo;
	}

	private function sedes(){
		$tsedes=DB::table('tsedes')
        ->lists('descripcion','codigosede');

        return $tsedes;
	}

	public function grabarpreliminar(Request $request){
		// dd($request->all());

		$preliminar = new Preliminar();
		$preliminar->codigo_preliminar = $request->input('codigopre');
		$preliminar->invitacion_id = $this->valorNull($request->input('invitacion_id'));
		$preliminar->nombre_preliminar = $request->input('nombre_preliminar');
		$preliminar->contacto_id = $request->input('select_contacto');
		$preliminar->unidadminera_id = $request->input('select_minera');
		$preliminar->fecha_preliminar = $request->input('fecha_preliminar');
		$preliminar->comentario_preliminar = $request->input('comentario_preliminar');
		$preliminar->comunicar_preliminar = $request->input('comunicar_preliminar');
		$preliminar->fecha_visita = $this->valorNull($request->input('fecha_visita'));
		$preliminar->fecha_consulta = $this->valorNull($request->input('fecha_consulta'));
		$preliminar->fecha_absolucion = $this->valorNull($request->input('fecha_absolucion'));
		$preliminar->fecha_presentacion = $this->valorNull($request->input('fecha_presentacion'));
		$preliminar->user_create = Auth::user()->cpersona;
		$preliminar->save();

		return redirect("preliminar");
	}

	public function generarPre($id){

		$invitacion = DB::table('invitacion as in')
		->join('tunidadminera as tum','tum.cunidadminera','=','in.unidadminera_id')
		->join('tpersona as cli','cli.cpersona','=','tum.cpersona')
		->join('tunidadmineracontactos as tumc','tumc.cunidadminera','=','tum.cunidadminera')
		->join('tcontactocargo as tcc','tcc.ccontactocargo','=','tumc.ccontactocargo')
		->select('in.*','tum.nombre as uminera','cli.*','tumc.*','tcc.*')
		->where('in.id','=',$id)
		->get();

		// dd($invitacion);

		return $invitacion;
	}
}