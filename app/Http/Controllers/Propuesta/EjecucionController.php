<?php

namespace App\Http\Controllers\Propuesta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tpai;
use App\Models\Tubigeo;
use App\Models\Tunidadmineraestado;
use App\Models\Tunidadminera;
use App\Models\Tunidadmineracontacto;
use App\Models\Ttiposcontacto;
use App\Models\Tcontactocargo;
use App\Models\Invitacion;
use App\Models\Preliminar;
use App\Models\Tpropuestum;
use App\Models\Tarea;
use App\Models\Tdisciplina;
use App\Models\Tdisciplinaarea;
use App\Models\Propuestapersona;
use App\Lib\EmailSend;
use Auth;
use DB;
use Carbon\Carbon;
use Redirect;

class EjecucionController extends Controller
{
	
	public function equipo($cpropuesta){

		$propuesta = $this->propuesta($cpropuesta);

		// dd($propuesta);

		$gerentes = $this->listapersonal('GP');
		$coordinador = $this->listapersonal('CP');
		$responsable = $this->listapersonal('RP');
		$revisor = $this->listapersonal('RFP');

		// dd($gerentes,$coordinador,$responsable,$revisor);

		return view('propuesta.v2.equipo',compact('gerentes','coordinador','responsable','revisor','propuesta'));
	}

	private function propuesta($cpropuesta)
	{
		$propuesta = DB::table('tpropuesta as tpro')
        ->join('tpersona as tper','tpro.cpersona','=','tper.cpersona')
        ->join('tunidadminera as tu','tpro.cunidadminera','=','tu.cunidadminera')
        ->where('tpro.cpropuesta','=',$cpropuesta)
        ->select('tpro.*','tper.nombre as cliente','tu.nombre as uminera')
		->first();

		return $propuesta;		
	}

	private function listapersonal($rol)
	{
		$personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->join('tpersonarol as pr','tpersona.cpersona','=','pr.cpersona')
        ->join('tusuarios as tu','tpersona.cpersona','=','tu.cpersona')
        ->join('troldespliegue as rol','pr.croldespliegue','=','rol.croldespliegue')
        ->where('tnat.esempleado','=','1')
        ->where('rol.codigorol','=',$rol)
        ->where('pr.estado','=','001')
        ->where('tu.estado','=','ACT')
        ->select('tpersona.abreviatura','tpersona.cpersona')
        ->orderBy('tpersona.abreviatura')
        ->get();

        return $personal;
	}

	public function grabarpropuestaequipo(Request $request)
	{

		$cpropuesta = $request->input('cpropuesta');
		$tecnicos = implode(',',$request->input('responsables'));
		
		// dd($request->all());
		
		$equipo = $this->propuesta($cpropuesta);
		$equipo->cpersona_gteproyecto = $request->input('gerentes');
		$equipo->cpersona_resppropuesta = $request->input('responsable');
		$equipo->cpersona_coordinadorpropuesta = $request->input('coordinador');
		$equipo->cpersona_revprincipal = $request->input('revisor');
		$equipo->cpersona_resptecnicos = $tecnicos;
		$equipo->save();
		
		return redirect('disciplinas/$cpropuesta');
		// return "OK";
	}
	
	public function disciplinas($cpropuesta){

	// $this->grabarpropuestaequipo();

	$propuesta = $this->propuesta($cpropuesta);	

	$disciplina = DB::table('tdisciplinaareas as tda')
	->join('tareas as ta','ta.carea','=','tda.carea')
	->join('tdisciplina as td','td.cdisciplina','=','tda.cdisciplina')
	->orderBy('ta.codigo')
	// ->whereNotNull('estecnica')
	->get();



	// dd($disciplina);

		return view('propuesta.v2.disciplinas', compact('disciplina','propuesta'));
	}

	public function tag(){

	$disciplina = DB::table('tdisciplinaareas as tda')
	->join('tareas as ta','ta.carea','=','tda.carea')
	->join('tdisciplina as td','td.cdisciplina','=','tda.cdisciplina')
	->orderBy('ta.codigo')
	// ->whereNotNull('estecnica')
	->get();

	// dd($personas);

		return view('propuesta.v2.tag',compact('disciplina'));
	}

	public function estructura(){


		return view('propuesta.v2.estructura');
	}

}

