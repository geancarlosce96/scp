<?php

namespace App\Http\Controllers\Propuesta;

use App\Http\Controllers\Controller;
use App\Models\Propuesta_opciones;
use App\Models\Propuesta_tag_estructura;
use App\Models\Propuesta_tag_servicio;
use App\Models\TablaHorasColumna;
use App\Models\TablaHorasCelda;
use App\Models\Tpropuesta;
use App\Models\Tpropuestaactividade;
use App\Models\Tpropuestadisciplina;
use App\Models\Tpropuestagasto;
use App\Models\Tpropuestagastoslaboratorio;

class RevisionController extends Controller
{
	public function generar($cpropuesta)
	{
		$propuesta = Tpropuesta::where("cpropuesta", "=", $cpropuesta)->first();
		$nuevo = $this->_copiarInformacion($propuesta);
		$this->_cambiarVisibilidad($propuesta);

		return $nuevo;
		return $propuesta;
	}

	//private 

	private function _cambiarVisibilidad($propuesta)
	{
		$propuesta->revision_visible = 0;
		$propuesta->save();
	}

	private function _copiarInformacion($propuesta)
	{
		// Aquí se tiene que copiar toda la información de la propuesta
		// de forma separada, tabla por tabla en funciones separadas
		// y también campos por campo para tener bien indicados
		// la mayoría de campos que se crea necesarios
		$nuevo = $this->_copiarTbPropuesta($propuesta);
		$actividades_referencias = [];	//referencias de id actuales con la copia
		
		$this->_copiarTbOpciones($propuesta->cpropuesta, $nuevo->cpropuesta);
		$this->_copiarTbTagServicio($propuesta->cpropuesta, $nuevo->cpropuesta);
		$this->_copiarTbTagEstructura($propuesta->cpropuesta, $nuevo->cpropuesta);		
		$this->_copiarTbActividades($propuesta->cpropuesta, $nuevo->cpropuesta, $actividades_referencias);	//* referencias
  		$this->_copiarTbDisciplinas($propuesta->cpropuesta, $nuevo->cpropuesta);
  		$this->_copiarTbGastos($propuesta->cpropuesta, $nuevo->cpropuesta);
  		$this->_copiarTbGastosLaboratorios($propuesta->cpropuesta, $nuevo->cpropuesta);
  		$this->_copiarTbHoras($propuesta->cpropuesta, $nuevo->cpropuesta, $actividades_referencias);	//* celdas, referencias
		return $nuevo;
	}

	private function _copiarTbPropuesta($propuesta)
	{
		// campos que se copiaran sus valores
		$campos = [
			"cpersona", 
			"cunidadminera", 
			"ccodigo", 
			"nombre",
		  "descripcion",
		  "tag",
		  "cformacotizacion",
		  "revision",
		  "ctipopropuesta",
		  "cservicio",
		  "cestadopropuesta",
		  "cpersona_resppropuesta",
		  "cpersona_revprincipal",
		  "cpersona_gteproyecto",
		  "cpersona_coordinadorpropuesta",
		  "cmoneda",
		  "cmedioentrega",
		  "observaciones",
		  "fentregabases",
		  "fconsulta",
		  "fabsolucionconsultas",
		  "fpresentacion",
		  "frespuesta",
		  "fcharla",
		  "fvtecnica",
		  "ftipo",
		  "ccondicionoperativa",
		  "csubsistema",
		  "cproyecto",
		  "fpropuesta",
		  "csede",
		  "fadjucicacion",
		  "ctipoproyecto",
		  "ctipogestionproyecto",
		  "cpersona_revisor_propuesta",
		  "cpersona_revisor_proyecto",
		  "revision_num"];

		$nuevo = new Tpropuesta();
		
		foreach($campos as $key => $value)
			$nuevo->$value = $propuesta->$value;

		$nuevo->revision_num = $propuesta->revision_num + 1;
		$nuevo->save();
		return $nuevo;
	}

	private function _copiarTbOpciones($cpropuesta, $cpropuesta_nuevo)
	{
		$campos = ["actividades", "disciplinas","equipo","gastos","gastoslaboratorios","horas","tags"];
		$opciones = Propuesta_opciones::where("cpropuesta", "=", $cpropuesta)->first();

		if($opciones == null)
			return;
		
		$opcion_nuevo = new Propuesta_opciones();
		$opcion_nuevo->cpropuesta = $cpropuesta_nuevo;

		foreach($campos as $key => $value)
			$opcion_nuevo->$value = $opciones->$value;

		$opcion_nuevo->save();
	}

	private function _copiarTbTagServicio($cpropuesta, $cpropuesta_nuevo)
	{
		$tags = Propuesta_tag_servicio::where("propuesta_id", "=", $cpropuesta)->get();

		foreach ($tags as $tag) {
			$tag_nuevo = new Propuesta_tag_servicio();
			$tag_nuevo->propuesta_id = $cpropuesta_nuevo;
			$tag_nuevo->propuesta_tag_servicio_id = $tag->propuesta_tag_servicio_id;
			$tag_nuevo->save();
		}
	}

	private function _copiarTbTagEstructura($cpropuesta, $cpropuesta_nuevo)
	{
		$tags = Propuesta_tag_estructura::where("propuesta_id", "=", $cpropuesta)->get();

		foreach ($tags as $tag) {
			$tag_nuevo = new Propuesta_tag_estructura();
			$tag_nuevo->propuesta_id = $cpropuesta_nuevo;
			$tag_nuevo->propuesta_tag_estructura_id = $tag->propuesta_tag_estructura_id;
			$tag_nuevo->save();
		}
	}

	private function _copiarTbDisciplinas($cpropuesta, $cpropuesta_nuevo)
	{
		$disciplinas = Tpropuestadisciplina::where("cpropuesta", "=", $cpropuesta)->get();

        foreach ($disciplinas as $key => $disciplina) {
        	$disciplina_nuevo = new Tpropuestadisciplina();
        	$disciplina_nuevo->cpropuesta = $cpropuesta_nuevo;
  			$disciplina_nuevo->cdisciplina = $disciplina->cdisciplina;
        	$disciplina_nuevo->save();
        }
	}

	private function _copiarTbGastos($cpropuesta, $cpropuesta_nuevo)
	{
		$campos = ["concepto", "unidad", "cantidad", "costou", "comentario"];
		$gastos = Tpropuestagasto::where("cpropuesta", "=", $cpropuesta)->get();

		foreach ($gastos as $key => $gasto) {
			$gasto_nuevo = new Tpropuestagasto();
			$gasto_nuevo->cpropuesta = $cpropuesta_nuevo;
			
			foreach($campos as $key => $value)
				$gasto_nuevo->$value = $gasto->$value;
			
			$gasto_nuevo->save();
		}
	}

	private function _copiarTbGastosLaboratorios($cpropuesta, $cpropuesta_nuevo)
	{
		$campos = ["concepto", "preciou", "cmoneda", "cantcimentacion", "cantcanteras", "cantbotadero"];
		$gastos = Tpropuestagastoslaboratorio::where("cpropuesta", "=", $cpropuesta)->get();

		foreach ($gastos as $key => $gasto) {
			$gasto_nuevo = new Tpropuestagastoslaboratorio();
			$gasto_nuevo->cpropuesta = $cpropuesta_nuevo;
			
			foreach($campos as $key => $value)
				$gasto_nuevo->$value = $gasto->$value;
			
			$gasto_nuevo->save();
		}
	}

	private function _copiarTbActividades($cpropuesta, $cpropuesta_nuevo, &$actividades_referencias)
	{
		$campos = ["cactividad", "codigoactividad", "descripcionactividad", 
			"cpropuestaactividades_parent", "hijos"];			
		$actividades = Tpropuestaactividade::where("cpropuesta", "=", $cpropuesta)
			->orderBy("cpropuestaactividades") ->get();

		foreach ($actividades as $key => $actividad) {
			$actividad_nuevo = new Tpropuestaactividade();
			$actividad_nuevo->cpropuesta = $cpropuesta_nuevo;
			
			foreach($campos as $key => $value)
				$actividad_nuevo->$value = $actividad->$value;
			
			if( $actividad->cpropuestaactividades_parent != null)
				$actividad_nuevo->cpropuestaactividades_parent=$actividades_referencias[$actividad->cpropuestaactividades_parent];
			$actividad_nuevo->save();

			$actividades_referencias[$actividad->cpropuestaactividades] = $actividad_nuevo->cpropuestaactividades;
		}
	}

	private function _copiarTbHoras($cpropuesta, $cpropuesta_nuevo, $actividades_referencias)
	{
		// #tabla_horas_columna
		$columnas = TablaHorasColumna::where("cpropuesta", "=", $cpropuesta)->get();		
		// #tabla_horas_celda
		$celdas = TablaHorasCelda::where("cpropuesta", "=", $cpropuesta)->get();
		//referencias de id actuales con la copia
		$columnas_referencias = [];

		//copiar tabla_horas_columna
		foreach ($columnas as $key => $columna) {
			$columna_nueva = new TablaHorasColumna();
			$columna_nueva->cpropuesta = $cpropuesta_nuevo;
			$columna_nueva->descripcion = $columna->descripcion;
			$columna_nueva->num_orden = $columna->num_orden;
			$columna_nueva->editable = $columna->editable;
			$columna_nueva->disciplina_id = $columna->disciplina_id;
			$columna_nueva->profesion_id = $columna->profesion_id;
			$columna_nueva->save();
			$columnas_referencias[$columna->id] = $columna_nueva->id;
		}

		//copiar tabla_horas_celda
		foreach ($celdas as $key => $celda) {
			$celda_nueva = new TablaHorasCelda();
			$celda_nueva->cpropuesta = $cpropuesta_nuevo;
			$celda_nueva->fila_codigo = $actividades_referencias[$celda->fila_codigo];	//ref
			$celda_nueva->columna_codigo = $columnas_referencias[$celda->columna_codigo];	//ref
			$celda_nueva->valor = $celda->valor;
			$celda_nueva->save();
		}
	}

}
