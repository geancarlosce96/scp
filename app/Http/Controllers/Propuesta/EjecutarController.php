<?php

namespace App\Http\Controllers\Propuesta;

use App\Http\Controllers\Controller;
use App\Models\Propuesta;
use App\Models\Propuesta_disciplina;
use App\Models\Propuesta_opciones;
use App\Models\Propuesta_tecnico;
use App\Models\Tactividad;
use App\Models\TagEstructura;
use App\Models\Tconceptogasto;
use App\Models\Tdisciplina;
use App\Models\Tdisciplinaarea;
use App\Models\Tpersona;
use App\Models\Tpropuesta;
use App\Models\Tpropuestaactividade;
use App\Models\Tpropuestagasto;
use App\Models\Tpropuestagastoslaboratorio;
use App\Models\TablaHoras;
use App\Models\Profesiones;
use Auth;
use DB;
use Illuminate\Http\Request;

class EjecutarController extends Controller
{
	//public
	public function ejecutar($cpropuesta){
		//buscar opcion proxima que no está guardada para redireccionar
		$opcion = Propuesta_opciones::obtenerProximaOpcion($cpropuesta);
		return redirect("propuesta/$cpropuesta/ejecutar/$opcion");
	}

	// 9 opciones actualmente
	public function equipo($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);
		
		if($propuesta == null) return redirect("propuesta");

		$propuesta_tecnicos = Propuesta::obtenerTecnicosToArray($cpropuesta);

		$gerentes = Tpersona::listapersonal('GP');
		$coordinador = Tpersona::listapersonal('CP');
		$responsable = Tpersona::listapersonal('RP');
		$revisor = Tpersona::listapersonal('RFP');

		return view("propuesta.ejecutar.opcion.equipo", 
			compact('gerentes','coordinador','responsable','revisor','propuesta', 'propuesta_tecnicos'));
	}

	public function disciplinas($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);
		$disciplinasSeleccionadas = Tdisciplinaarea::obtenerSegunPropuesta($cpropuesta);
		$disciplinas = Tdisciplinaarea::obtenerListado();

		return view("propuesta.ejecutar.opcion.disciplinas", 
			compact('propuesta', 'disciplinas', 'disciplinasSeleccionadas'));
	}

	public function tags($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);
		$tag_estructuras = TagEstructura::all();
		$tag_servicios = Tdisciplina::allWithServicios();
		$tag_estructuras_seleccionados = TagEstructura::obtenerSegunPropuesta($cpropuesta);
		$tag_servicios_seleccionados = Tdisciplina::obtenerSegunPropuesta($cpropuesta);

		return view("propuesta.ejecutar.opcion.tags", 
			compact('propuesta', 'tag_estructuras', 'tag_servicios', 
				'tag_estructuras_seleccionados',
				'tag_servicios_seleccionados'));
	}

	public function actividades($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);

		return view("propuesta.ejecutar.opcion.actividades", 
			compact('propuesta'));
	}

	public function horas($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);

		return view("propuesta.ejecutar.opcion.horas", 
			compact('propuesta'));
	}

	public function gastos($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);
		$gastos = Tconceptogasto::allWithOrder("00001");

		return view("propuesta.ejecutar.opcion.gastos", 
			compact('propuesta'),
			compact('gastos'));
	}

	public function gastosLaboratorios($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);

		return view("propuesta.ejecutar.opcion.gastosLaboratorios", 
			compact('propuesta'));
	}

	public function resumenGlobal($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);
		$actividades = Tpropuestaactividade::where("cpropuesta", "=", $cpropuesta)
						->orderBy("codigoactividad", "asc")
						->orderBy("descripcionactividad", "asc")
						->get();

		$tabla = new TablaHoras();

		foreach ($actividades as $key => $actividad)
			$actividad->total = $tabla->obtenerTotalHorasSegunActividad($actividad->cpropuestaactividades);

		$total = 0;
		$actividadPadreTemporalPos = -1;
		$actividadPadres = [];
		
		for ($i=0; $i < count($actividades); $i++) 
		{ 
			if($actividades[$i]->cpropuestaactividades_parent != null) {
				$total += $actividad->total;
			} 
			else {
				if($actividadPadreTemporalPos != -1){
					$actividades[$actividadPadreTemporalPos]->total = $total;					
					array_push($actividadPadres, $actividades[$actividadPadreTemporalPos]);
				}

				$total = 0;
				$actividadPadreTemporalPos = $i;
			}
		}

		if($actividadPadreTemporalPos != -1){
			$actividades[$actividadPadreTemporalPos]->total = $total;
			array_push($actividadPadres, $actividades[$actividadPadreTemporalPos]);
		}

		$actividades = $actividadPadres;

		return view("propuesta.ejecutar.opcion.resumenGlobal", 
			compact('propuesta', 'actividades'));
	}

	public function resumenHH($cpropuesta){
		$propuesta = Propuesta::info($cpropuesta);
		$disciplinas = Tdisciplinaarea::obtenerSegunPropuestaConNombre($cpropuesta);
		$profesiones = Profesiones::obtener();

		return view("propuesta.ejecutar.opcion.resumenHH", 
			compact('propuesta','disciplinas', 'profesiones'));
	}

	//ajax

	public function obtenerHorasHH($cpropuesta)
	{
		$tabla = new TablaHoras();
		//obtienen todas las celdas
		$tablaCompleta = $tabla->obtener($cpropuesta);
		$celdas = $tablaCompleta['celdas'];
		//se agrupan segun columna en un objeto y se obtienen totales, se obtiene disciplina				
		$totalesColumna = $this->agruparCeldasResumenHH($celdas);
		//obtener columnas
		$columnas = $tablaCompleta['columnas'];
		//agregar total de horas
		$totales = $this->agregarTotalResumenHH($columnas, $totalesColumna);

		return $columnas;
	}

	public function grabarCelda(Request $request, $cpropuesta)
	{
		$celda = $request->input("celda");
		$tabla = new TablaHoras();
		$nuevaCelda = $tabla->guardarCelda($cpropuesta, $celda);
		
		Propuesta_opciones::actualizar($cpropuesta, "horas", Auth::user()->cpersona);	//log
		return $nuevaCelda;
	}

	public function horasGuardar(Request $request, $cpropuesta)
	{
		$celdas = $request->input("celdas");
		
		$tabla = new TablaHoras();
		$tabla->guardarCeldas($cpropuesta, $celdas);

		Propuesta_opciones::actualizar($cpropuesta, "horas", Auth::user()->cpersona);	//log
		return $celdas;
	}

	public function obtenerTablaHoras($cpropuesta)
	{
		$opciones_registradas = Propuesta_opciones::where("cpropuesta", "=", $cpropuesta)->first();
		$tabla = new TablaHoras();

		if($opciones_registradas->horas == 0)
			$tabla->generar($cpropuesta);		

		$data = [];
		$data["tabla"] = $tabla->obtener($cpropuesta);

		return $data;
	}

	public function obtenerInformacionMatrizHoras($cpropuesta)
	{
		$data = [];
		$data["actividades"] = Tpropuestaactividade::where("cpropuesta", "=", $cpropuesta)
									->orderBy("cpropuestaactividades", "asc")->get();
		$data["disciplinas"] = Tdisciplinaarea::obtenerSegunPropuestaConNombre($cpropuesta);
		$data["matrizNueva"] = "true";

		return $data;
	}

	public function equipoGuardar(Request $request){
		$cpropuesta = $request->input("cpropuesta");
		$tecnicos = $request->input('responsables');
		if($tecnicos == null) $tecnicos = [];
		$propuesta = tpropuesta::where("cpropuesta", "=",  $cpropuesta)->first();		
		$propuesta->cpersona_gteproyecto = $request->input('gerentes');		
		$propuesta->cpersona_resppropuesta = $request->input('responsable');
		$propuesta->cpersona_coordinadorpropuesta = $request->input('coordinador');
		$propuesta->cpersona_revprincipal = $request->input('revisor');		
		$propuesta->cpersona_revisor_propuesta = $request->input('revisor_propuesta');
		$propuesta->cpersona_revisor_proyecto = $request->input('revisor_proyecto');
		$propuesta->save();

		//responsables

		DB::table('propuesta_tecnico')->where('cpropuesta', '=', $cpropuesta)->delete();
		
		for ($i=0; $i < count($tecnicos); $i++) { 
			$tecnico = new Propuesta_tecnico();
			$tecnico->cpropuesta = $cpropuesta;
			$tecnico->persona_id = $tecnicos[$i];
			$tecnico->save();
		}

		Propuesta_opciones::actualizar($cpropuesta, "equipo", Auth::user()->cpersona);	//log
		return $propuesta;
	}

	public function disciplinasGuardar(Request $request){
		$cpropuesta = $request->input("cpropuesta");
		$propuesta = tpropuesta::where("cpropuesta", "=",  $cpropuesta)->first();
		$disciplinas = $request->input("disciplinas");
		if($disciplinas == null) $disciplinas = [];

		DB::table('tpropuestadisciplina')->where('cpropuesta', '=', $cpropuesta)->delete();

		for ($i=0; $i < count($disciplinas); $i++) {
			$tecnico = new Propuesta_disciplina();
			$tecnico->cpropuesta = $cpropuesta;
			$tecnico->cdisciplina = $disciplinas[$i];
			$tecnico->cpersona_rdp = Auth::user()->cpersona;
			$tecnico->save();
		}

		$propuesta->disciplinas = $disciplinas;

		Propuesta_opciones::actualizar($cpropuesta, "disciplinas", Auth::user()->cpersona);	//log
		return $propuesta;
	}

	public function tagsGuardar(Request $request)
	{
		$cpropuesta = $request->input("cpropuesta");
		$chk_tag_servicios = $request->input("chk_tag_servicios");
		$chk_tag_estructuras = $request->input("chk_tag_estructuras");

		$propuesta = tpropuesta::where("cpropuesta", "=",  $cpropuesta)->first();
		$propuesta->guardarTag($chk_tag_estructuras, $chk_tag_servicios, $cpropuesta);

		Propuesta_opciones::actualizar($cpropuesta, "tags", Auth::user()->cpersona);	//log
		return $propuesta;
	}

	public function actividadesGuardar(Request $request, $cpropuesta)
	{
		$elementos = $request->input("elementos", []);
		//DB::table('tpropuestaactividades')->where('cpropuesta', '=', $cpropuesta)->delete(); //limpiar ya registrados
		Tpropuestaactividade::where('cpropuesta', '=', $cpropuesta)->update(['estado' => 0]);
		
		foreach ($elementos as $key => $elemento)
			$this->registrarActividad($cpropuesta, $elemento);
		
		DB::table('tpropuestaactividades')->where('cpropuesta', '=', $cpropuesta)
			->where("estado", "=", 0)->delete(); //limpiar ya registrados
		Propuesta_opciones::actualizar($cpropuesta, "actividades", Auth::user()->cpersona);	//log
		return $elementos;
	}

	public function gastosGuardar(Request $request, $cpropuesta)
	{
		$elementos = $request->input("elementos", []);
		DB::table('tpropuestagastos')->where('cpropuesta', '=', $cpropuesta)->delete();

		foreach ($elementos as $key => $elemento) {
			$this->registrarGasto($cpropuesta, $elemento);
		}

		Propuesta_opciones::actualizar($cpropuesta, "gastos", Auth::user()->cpersona);	//log
		return $elementos;
	}

	public function gastosLabGuardar(Request $request, $cpropuesta)
	{
		$elementos = $request->input("elementos", []);
		DB::table('tpropuestagastoslaboratorio')->where('cpropuesta', '=', $cpropuesta)->delete();

		foreach ($elementos as $key => $elemento) {
			$this->registrarGastoLab($cpropuesta, $elemento);
		}

		Propuesta_opciones::actualizar($cpropuesta, "gastoslaboratorios", Auth::user()->cpersona);	//log
		return $elementos;
	}

	public function obtenerActividades()
	{
		$actividades = $this->obtenerActividadesTreeArray();
		
		foreach ($actividades as $key => $a)
			$a->icon = "glyphicon glyphicon-pushpin";
		
		return $actividades;
	}

	public function obtenerActividadesActuales($cpropuesta)
	{
		$actividades = $this->obtenerActividadesActualesTreeArray($cpropuesta);
		
		foreach ($actividades as $key => $a)
			$a->icon = "glyphicon glyphicon-pushpin";

		return $actividades;
	}

	public function obtenerGastos($cpropuesta)
	{
		$gastos = [];
		$opciones_registradas = Propuesta_opciones::where("cpropuesta", "=", $cpropuesta)->first();
		
		if($opciones_registradas!= null && $opciones_registradas->gastos == 1){
			$gastos["estado"] = 1;
			$gastos["lista"] = Tpropuestagasto::obtenerSegunPropuesta($cpropuesta);
		}
		else{
			$gastos["estado"] = 0;
			$gastos["lista"] = Tconceptogasto::allWithOrder("00001");
		}

		foreach ($gastos["lista"] as $key => $gasto) {
			$gasto->subtotal = $gasto->cantidad * $gasto->costou;
		}

		return $gastos;
	}

	public function obtenerGastosLab($cpropuesta)
	{
		$gastos = [];
		$opciones_registradas = Propuesta_opciones::where("cpropuesta", "=", $cpropuesta)->first();
		
		if($opciones_registradas!= null && $opciones_registradas->gastoslaboratorios == 1){
			$gastos["estado"] = 1;
			$gastos["lista"] = Tpropuestagasto::obtenerLabSegunPropuesta($cpropuesta);
		}
		else{
			$gastos["estado"] = 0;
			$gastos["lista"] = Tconceptogasto::allWithOrder("00002");
		}

		foreach ($gastos["lista"] as $key => $gasto) {
			$gasto->cantcimentacion = ($gasto->cantcimentacion == null) ? 0 : floatval($gasto->cantcimentacion);
			$gasto->cantcanteras = ($gasto->cantcanteras == null) ? 0 : floatval($gasto->cantcanteras);
			$gasto->subtotal = $gasto->preciou * floatval($gasto->cantcimentacion + $gasto->cantcanteras);
		}

		return $gastos;
	}

	//private

	private function obtenerActividadesTreeArray()
	{
		//format to plufin tree
		$actividades = Tactividad::where("tipoactividad", "=", "00001")->orderBy("cactividad", "asc")->get();

		foreach ($actividades as $key => $actividad) {
			$actividad->id = $actividad->cactividad;
			$actividad->text = $actividad->codigo . " " . $actividad->descripcion;
			$actividad->parent = ($actividad->cactividad_parent == null) ? "#" : $actividad->cactividad_parent;
		}
		return $actividades;
	}

	private function obtenerActividadesActualesTreeArray($cpropuesta)
	{
		//format to plufin tree

		$actividades = Tpropuestaactividade::where("cpropuesta", "=", $cpropuesta)->orderBy("cpropuestaactividades", "asc")->get();

		foreach ($actividades as $key => $actividad) {
			$actividad->id = $actividad->cpropuestaactividades;
			$actividad->text = $actividad->codigoactividad . " " . $actividad->descripcionactividad;
			$actividad->parent = ($actividad->cpropuestaactividades_parent == null) ? "#" : $actividad->cpropuestaactividades_parent;
		}
		return $actividades;
	}

	private function registrarActividad($cpropuesta, $elemento, $parent_id = null)
	{
		$actividad = null;

		if(is_numeric($elemento["id"]))
			$actividad = Tpropuestaactividade::where("cpropuestaactividades", "=", $elemento["id"])->first();
		
		if($actividad == null){
			$actividad = new Tpropuestaactividade();			
			$actividad->cpropuesta = $cpropuesta;
		}

		$actividad->codigoactividad = "";
		$actividad->cpropuestaactividades_parent = $parent_id;
		$actividad->descripcionactividad = trim($elemento["text"]);
		$actividad->estado = 1;
		$actividad->save();

		if(isset($elemento["children"]) && count($elemento["children"])>0){
			$this->registrarActividadesHijos($cpropuesta, $elemento["children"], $actividad->cpropuestaactividades);
			$actividad->hijos = 1;
			$actividad->save();
		}

		return $actividad;
	}

	private function registrarActividadesHijos($cpropuesta, $elementos, $parent_id)
	{
		foreach ($elementos as $key => $elemento)
			$this->registrarActividad($cpropuesta, $elemento, $parent_id);
	}

	private function registrarGasto($cpropuesta, $elemento)
	{
		$gasto = new Tpropuestagasto();		
		$gasto->cantidad = (isset($elemento['cantidad']) && $elemento['cantidad']!="") ? $elemento['cantidad'] : 0;
		$gasto->comentario = (isset($elemento['comentario']) && $elemento['comentario']!="") ? $elemento['comentario'] : '';
		$gasto->concepto = (isset($elemento['concepto']) && $elemento['concepto']!="") ? $elemento['concepto'] : null;
		$gasto->costou = (isset($elemento['costounitario']) && $elemento['costounitario']!="") ? $elemento['costounitario'] : null;
		$gasto->unidad = (isset($elemento['unidad']) && $elemento['unidad']!="") ? $elemento['unidad'] : 1;
		$gasto->cpropuesta = $cpropuesta;
		$gasto->save();

		return $gasto;
	}

	private function registrarGastoLab($cpropuesta, $elemento)
	{
		$gasto = new Tpropuestagastoslaboratorio();		
		$gasto->cantcimentacion = (isset($elemento['cantcimentacion']) && $elemento['cantcimentacion']!="") ? 
			$elemento['cantcimentacion'] : 0;		
		$gasto->cantcanteras = (isset($elemento['cantcanteras']) && $elemento['cantcanteras']!="") ? $elemento['cantcanteras'] : 0;
		$gasto->cantcimentacion = (isset($elemento['cantcimentacion']) && $elemento['cantcimentacion']!="") ?
			$elemento['cantcimentacion'] : 0;
		$gasto->concepto = (isset($elemento['concepto']) && $elemento['concepto']!="") ? $elemento['concepto'] : null;
		$gasto->preciou = (isset($elemento['costounitario']) && $elemento['costounitario']!="") ? $elemento['costounitario'] : null;
		$gasto->cpropuesta = $cpropuesta;
		$gasto->save();

		return $gasto;
	}

	private function agruparCeldasResumenHH($celdas)
	{
		$grupos = [];
		foreach ($celdas as $key => $obj) {
			$this->agregarCeldaAgrupo($grupos, $obj);
		}
		return $grupos;
	}

	private function agregarCeldaAgrupo(&$grupos, $obj)
	{
		$agregado = false;

		for ($i=0; $i < count($grupos); $i++) { 
			if($grupos[$i]->columna_codigo == $obj->columna_codigo){
				$grupos[$i]->valor += intval($obj->valor);
				$agregado = true;
			}
		}

		if(!$agregado){
			$grupo = new \stdClass();
			$grupo->columna_codigo = $obj->columna_codigo; 
			$grupo->valor = intval($obj->valor);
			array_push($grupos, $grupo);
		}

	}

	private function agregarTotalResumenHH(&$columnas, $total)
	{
		foreach ($columnas as $key => $col) {
			foreach ($total as $key => $t) {
				if($col->id == $t->columna_codigo)
					$col->valor = $t->valor;
			}
		}
	}

}
