<?php

namespace App\Http\Controllers\Propuesta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tpai;
use App\Models\Tubigeo;
use App\Models\Tunidadmineraestado;
use App\Models\Tunidadminera;
use App\Models\Tunidadmineracontacto;
use App\Models\Ttiposcontacto;
use App\Models\Tcontactocargo;
use App\Models\Invitacion;
use App\Models\Preliminar;
use App\Models\Tpropuestum;
use App\Models\Tarea;
use App\Models\Tdisciplina;
use App\Models\tdisciplinaarea;
use App\Lib\EmailSend;
use Auth;
use DB;
use Carbon\Carbon;
use Redirect;

class PropuestasController extends Controller
{
	public function propuesta(){
		// dd("InvitacionController");

		
		// $cli["clientes"] = $cliente;
		$cliente = $this->verCliente();
		$tsedes = $this->sedes();
		$codigopropuesta = $this->existepropuesta();
        $portafolio = $this->portafolio();

        // dd($codigopropuesta);

		$listarpropuestas = (DB::table('tpropuesta')
            ->leftjoin('tpersona as tper','tpropuesta.cpersona','=','tper.cpersona')
            ->leftjoin('tunidadminera as tu','tpropuesta.cunidadminera','=','tu.cunidadminera')
            ->leftjoin('testadopropuesta as ep','tpropuesta.cestadopropuesta','=','ep.cestadopropuesta')
            ->leftJoin('tpersona as p','tpropuesta.cpersona_gteproyecto','=','p.cpersona')
            ->select('tpropuesta.cpropuesta','tpropuesta.ccodigo','tpropuesta.fpropuesta'
                ,'tper.nombre as cliente','tu.nombre as uminera','tpropuesta.nombre',DB::raw('fn_getdisciplinaspropuesta(tpropuesta.cpropuesta) as disciplina'), 
            'ep.descripcion','tpropuesta.revision','p.abreviatura as gteProyecto', 'tpropuesta.revision_num')
            )
		->where("tpropuesta.revision_visible", "=", 1)
		->orderBy('cpropuesta','desc')
		->get();

		$tpais = Tpai::lists('descripcion','cpais')->all();
        $tpais = [''=>''] + $tpais;

        $tpais_um = Tpai::lists('descripcion','cpais')->all();
        $tpais_um = [''=>''] + $tpais;

        $umineraestado = Tunidadmineraestado::lists('descripcion','cumineraestado')
        ->all();
        // $umineraestado = [''=>''] + $umineraestado;

        /* Unidad Minera Contactos */
        $tcargo= DB::table('tcontactocargo')
        ->lists('descripcion','ccontactocargo');
        $tcargo = [''=>''] + $tcargo;

        $ttipo= DB::table('ttiposcontacto')
        ->lists('descripcion','ctipocontacto');
        $ttipo = [''=>''] + $ttipo;

        $aniocod= Carbon::now();
        $aniocod = $aniocod->year;
        $aniocod=substr($aniocod, 2,2);

		// dd($listarpropuestas);
		$vista = 'propuesta';

		if ($codigopropuesta=='') {
		$cod = 'text';
		}else {
		$cod = 'hidden';
		}


		return view('propuesta.v2.propuesta', compact('cliente','listarpropuestas','vista','tpais','tpais_um','umineraestado','tcargo','ttipo','tsedes','aniocod','codigopropuesta','cod','portafolio'));
	}

	private function existepropuesta(){

		$cant='';
		$anio = date('y');
		
		$codigo = DB::table('tpropuesta')
		->orderBy('cpropuesta','desc')
		->first();

		// dd($codigo);

		$cod = explode('.', $codigo->ccodigo);

		if ($cod[0]==$anio) {
			// dd($anio);
			$cant= $codigo->ccodigo;
			$corre = explode('.', $cant);
			$newcod = str_pad($corre[2]+1,3,'0',STR_PAD_LEFT);
			$codigonuevo = $newcod;
		}
		else {
		// dd($cod[0],$anio);
			$codigonuevo = $cant;	
		}

		// dd($codigonuevo);

		return $codigonuevo;
	}


	public function minera($idcliente){

		$minera = Tunidadminera::where('cpersona','=',$idcliente)->get();

		return $minera;
	}

	public function contacto($idminera){

		$contacto = DB::table('tunidadmineracontactos as tumc')
		->join('tcontactocargo as tcc','tcc.ccontactocargo','=','tumc.ccontactocargo')
		->where('cunidadminera','=',$idminera)
		->get();

		return $contacto;
	}

	private function sedes(){
		$tsedes=DB::table('tsedes')
        ->lists('descripcion','codigosede');

        return $tsedes;
	}

	private function portafolio(){
		$servicio = DB::table('tservicio')
		->orderBy('orden','ASC')
		->get();

        return $servicio;
	}

	public function tipoproyecto($idPortafolio){

		$tipoproyecto = DB::table('ttipoproyecto')
        ->where('cservicio','=',$idPortafolio)
        ->lists('descripcion','ctipoproyecto');

		return $tipoproyecto;
	}

	public function generarPro($id){

		$invitacion = DB::table('invitacion as in')
		->join('tunidadminera as tum','tum.cunidadminera','=','in.unidadminera_id')
		->join('tpersona as cli','cli.cpersona','=','tum.cpersona')
		->join('tunidadmineracontactos as tumc','tumc.cunidadminera','=','tum.cunidadminera')
		->join('tcontactocargo as tcc','tcc.ccontactocargo','=','tumc.ccontactocargo')
		->select('in.*','tum.nombre as uminera','cli.*','tumc.*','tcc.*')
		->where('in.id','=',$id)
		->get();

		// dd($invitacion);

		return $invitacion;
	}


	public function grabarpropuesta(Request $request){

		// dd($request->all());

		$propuesta = new Tpropuestum();
		$propuesta->invitacion_id = $request->input('invitacion_id_pro');
		$propuesta->ccodigo = $request->input('codigopro');
		$propuesta->cpersona = $request->input('select_cliente');
		$propuesta->cunidadminera = $request->input('select_minera_pro');
		$propuesta->nombre = $request->input('nombre_propuesta');
		$propuesta->contactoid = $request->input('select_contacto_pro');
		$propuesta->fpropuesta = $request->input('fecha_propuesta');
		$propuesta->fvtecnica = $request->input('fecha_visita');
		$propuesta->fconsulta = $request->input('fecha_consulta');
		$propuesta->fabsolucionconsultas = $request->input('fecha_absolucion');
		$propuesta->fpresentacion = $request->input('fecha_presentacion_pro');
		$propuesta->finterna = $request->input('fecha_interna');
		$propuesta->comunicar_propuesta = $request->input('comunicar_propuesta');
		$propuesta->cservicio = $request->input('select_portafolio');
		$propuesta->ctipoproyecto = $request->input('select_tipoproyecto');
		$propuesta->observaciones = $request->input('comentario_propuesta');
		$propuesta->cpersona_coordinadorpropuesta = Auth::user()->cpersona;
		$propuesta->cestadopropuesta = '007';
		$propuesta->revision = 'A';
		$propuesta->save();


		return redirect("propuesta");
	}

	public function verCliente(){

		$cliente = Tpersona::where('ctipopersona','=','JUR')->orderBy("nombre","ASC")->get();

		return $cliente;

	}

	public function getEdit($id)
	{
		dd($id);
		$invitacion = DB::table('invitacion as in')
		->join('tunidadminera as tum','tum.cunidadminera','=','in.unidadminera_id')
		->join('tpersona as cli','cli.cpersona','=','tum.cpersona')
		->join('tunidadmineracontactos as tumc','tumc.cunidadminera','=','tum.cunidadminera')
		->join('tcontactocargo as tcc','tcc.ccontactocargo','=','tumc.ccontactocargo')
		->select('in.*','tum.nombre as uminera','cli.*','tumc.*','tcc.*')
		->where('in.id','=',$id)
		->get();


		return $invitacion;
	}

public function disciplinas(){

	$disciplina = DB::table('tdisciplinaareas as tda')
	->join('tareas as ta','ta.carea','=','tda.carea')
	->join('tdisciplina as td','td.cdisciplina','=','tda.cdisciplina')
	->orderBy('ta.codigo')
	// ->whereNotNull('estecnica')
	->get();



	// dd($disciplina);

		return view('propuesta.v2.disciplinas', compact('disciplina'));
	}

	public function equipo(){

		$gerentes = $this->listapersonal('GP');
		$coordinador = $this->listapersonal('CP');
		$responsable = $this->listapersonal('RP');
		$revisor = $this->listapersonal('RFP');

		// dd($gerentes,$coordinador,$responsable,$revisor);

		return view('propuesta.v2.equipo',compact('gerentes','coordinador','responsable','revisor'));
	}

	private function listapersonal($rol)
	{
		$personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->join('tpersonarol as pr','tpersona.cpersona','=','pr.cpersona')
        ->join('tusuarios as tu','tpersona.cpersona','=','tu.cpersona')
        ->join('troldespliegue as rol','pr.croldespliegue','=','rol.croldespliegue')
        ->where('tnat.esempleado','=','1')
        ->where('rol.codigorol','=',$rol)
        ->where('pr.estado','=','001')
        ->where('tu.estado','=','ACT')
        ->select('tpersona.abreviatura','tpersona.cpersona')
        ->orderBy('tpersona.abreviatura')
        ->get();

        return $personal;
	}

	public function tag(){

	$disciplina = DB::table('tdisciplinaareas as tda')
	->join('tareas as ta','ta.carea','=','tda.carea')
	->join('tdisciplina as td','td.cdisciplina','=','tda.cdisciplina')
	->orderBy('ta.codigo')
	// ->whereNotNull('estecnica')
	->get();

	// dd($personas);

		return view('propuesta.v2.tag',compact('disciplina'));
	}

	public function estructura(){


		return view('propuesta.v2.estructura');
	}

}

