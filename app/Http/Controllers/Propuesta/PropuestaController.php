<?php

namespace App\Http\Controllers\Propuesta;

use Illuminate\Http\Request;
use App\Http\Requests\PropuestaRequest;

use Yajra\Datatables\Facades\Datatables;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Tpropuestum;
use App\Models\Tpropuestapresentacion;
use App\Models\Tpropuestadisciplina;
use App\Models\Tpropuestapaquete;
use App\Models\Tservicio;
use App\Models\Tactividad;
use App\Models\Tconceptogasto;
use App\Models\Tpropuestagasto;
use App\Models\Tpropuestagastoslaboratorio;
use App\Models\Tpropuestahonorario;
use App\Models\Testadopropuestum;
use App\Models\Tpropuestacronogramaconstruccion;
use App\Models\Tpropuestaconstrucciondetafecha;
use App\Models\Tpropuestainformacionadicional;

use App\Erp\GastosSupport;
use App\Erp\ExcelSupport;
use App\Erp\PropuestaSupport;
use DB;
use Auth;
use Redirect;
use App\Erp\HelperSIGSupport;
use App\Erp\ActividadSupport;
use App\Erp\EmpleadoSupport;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Maatwebsite\Excel\Facades\Excel;
use Session;
use Carbon\Carbon;
use App\Models\Tpropuestaactividade;
use App\Models\Troldespliegue;
use App\Models\Testructuraparticipante;
use App\Models\Tusuariopassword;
use PHPExcel;
use PHPExcel_IOFactory; 
use PHPExcel_Cell;
use PHPExcel_Cell_DataType;
use PHPExcel_Worksheet;
use App\Erp\EnumSubsistema;

use App\Erp\FlowSupport;
use App\Erp\ChunkReadFilter;


use App\Models\Tproyectoestructuraparticipante;

class PropuestaController extends Controller
{
    //
    // public function welcome(){
    //     // dd(Auth::user());
    //     $usuario = Auth::user()->cpersona;

    //     $cusuario = DB::table('tusuarios')
    //     ->where('cpersona','=',$usuario)
    //     ->first();

    //     // dd($cusuario);
    //     /*Dirige la time sheet luego de iniciar sesion o actualizar la pagina*/
    //     if(strlen($cusuario->remember_token)!=null){
    //         // dd('token');
    //      return view('timesheet/welcome');
    //      // return view('welcome');
    //     }
    //     else {
    //         // dd('notoken'); 
    //         $nomusuario=$cusuario->nombre;           
    //         $nuevo='si';
    //         $usuariopassword=Tusuariopassword::where('cusuario','=',$cusuario->cusuario)
    //         ->first();

    //         //$usuario=$cusuario->cusuario;

    //         $passwordActual=$usuariopassword->password;
    //      //return Redirect::route('passwordUsuario');
    //         return view('sistema.registroPassword',compact('usuario','nuevo','passwordActual','nomusuario'));
    //     }
    //     /*Dirige al panel principal luego de iniciar sesion o actualizar la pagina*/

        
    //     return view('welcome');

    // }
    public function listar(){
    	return view('propuesta/listadoPropuestas');
    }
    public function listarGrid(){
        return Datatables::queryBuilder(DB::table('tpropuesta')
            ->join('tpersona as tper','tpropuesta.cpersona','=','tper.cpersona')
            ->join('tunidadminera as tu','tpropuesta.cunidadminera','=','tu.cunidadminera')
            ->join('testadopropuesta as ep','tpropuesta.cestadopropuesta','=','ep.cestadopropuesta')
            ->leftJoin('tpersona as p','tpropuesta.cpersona_gteproyecto','=','p.cpersona')
            ->select('tpropuesta.cpropuesta','tpropuesta.ccodigo'
                ,'tper.nombre as cliente','tu.nombre as uminera','tpropuesta.nombre',DB::raw('fn_getdisciplinaspropuesta(tpropuesta.cpropuesta) as disciplina'), 
            'ep.descripcion','tpropuesta.revision','p.nombre as gteProyecto',DB::raw('CONCAT(\'row_\',tpropuesta.cpropuesta)  as "DT_RowId"'))
            )->make(true);
    }

    public function listarSoloPropuestas(){
        return Datatables::queryBuilder(DB::table('tpropuesta')
            ->join('tpersona as tper','tpropuesta.cpersona','=','tper.cpersona')
            ->join('tunidadminera as tu','tpropuesta.cunidadminera','=','tu.cunidadminera')
            ->join('testadopropuesta as ep','tpropuesta.cestadopropuesta','=','ep.cestadopropuesta')
            ->leftJoin('tpersona as p','tpropuesta.cpersona_gteproyecto','=','p.cpersona')
            ->select('tpropuesta.cpropuesta','tpropuesta.ccodigo'
                ,'tper.nombre as cliente','tu.nombre as uminera','tpropuesta.nombre',DB::raw('fn_getdisciplinaspropuesta(tpropuesta.cpropuesta) as disciplina'), 
            'ep.descripcion','tpropuesta.revision','p.nombre as gteProyecto',DB::raw('CONCAT(\'row_\',tpropuesta.cpropuesta)  as "DT_RowId"'))
            ->whereNotIn('tpropuesta.cestadopropuesta',['007'])
            )->make(true);
    }



    public function listaPropuestaEstados(Request $request){
        return Datatables::queryBuilder(DB::table('tpropuesta')
            ->join('tpersona as tper','tpropuesta.cpersona','=','tper.cpersona')
            ->join('tunidadminera as tu','tpropuesta.cunidadminera','=','tu.cunidadminera')
            ->join('testadopropuesta as ep','tpropuesta.cestadopropuesta','=','ep.cestadopropuesta')
            ->leftJoin('tpersona as p','tpropuesta.cpersona_gteproyecto','=','p.cpersona')
            ->where('ep.cestadopropuesta','=',$request->input('estado'))
            ->select('tpropuesta.cpropuesta','tpropuesta.ccodigo'
                ,'tper.nombre as cliente','tu.nombre as uminera','tpropuesta.nombre',DB::raw('fn_getdisciplinaspropuesta(tpropuesta.cpropuesta) as disciplina'), 
            'ep.descripcion','tpropuesta.revision','p.nombre as gteProyecto',DB::raw('CONCAT(\'row_\',tpropuesta.cpropuesta)  as "DT_RowId"'))
            )->make(true);        
    }    

    public function buscar(Request $request){
        $listaps = DB::table('tpropuesta')
            ->join('tpersona as tper','tpropuesta.cpersona','=','tper.cpersona')
            ->join('tunidadminera as tu','tpropuesta.cunidadminera','=','tu.cunidadminera')
            ->join('testadopropuesta as ep','tpropuesta.cestadopropuesta','=','ep.cestadopropuesta')
            ->leftJoin('tpersona as p','tpropuesta.cpersona_gteproyecto','=','p.cpersona')
            ->select('tpropuesta.cpropuesta','tpropuesta.ccodigo',
                'tper.nombre as cliente','tu.nombre as uminera','tpropuesta.nombre','tpropuesta.revision as Disciplina', 
            'ep.descripcion','tpropuesta.revision','p.nombre as gteProyecto')
            ->where('tpropuesta.nombre','like', '%'.$request->input('cliente').'%')        
            ->get();        
        return view('propuesta/listadoPropuestas',compact('listaps'));
    }

    public function registro(){

        Session::forget('paqpropuesta');
        //Session::flush();    
        $objEmp = new EmpleadoSupport();    
        $obj = new Tpropuestapaquete();
        $obj->cpropuestapaquete=0;
        $obj->cpropuesta=0;
        $obj->codigopaquete='000';
        $obj->nombrepaquete='Generales';
        $paqpropuesta = array(0=>$obj);
        Session::put('paqpropuesta',$paqpropuesta);

        $propuestaSupport = new HelperSIGSupport();
        $servicio = DB::table('tservicio')->lists('descripcion','cservicio');
        $formacotiza = DB::table('tformacotizacion')->get();

        $estadopropuesta = DB::table('testadopropuesta')
        ->orderBy('cestadopropuesta','ASC')
        ->lists('descripcion','cestadopropuesta');
        $estadopropuesta = [''=>''] + $estadopropuesta;
        $personal_rp = $objEmp->getPersonalRol('RP','001','2') ;
        $personal_rp = [''=>''] + $personal_rp;
        $personal_rfp = $objEmp->getPersonalRol('RFP','001','2') ;
        $personal_rfp = [''=>''] + $personal_rfp;
        $personal_cp = $objEmp->getPersonalRol('CP','001','2') ;
        $personal_cp = [''=>''] + $personal_cp;
        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp;


        $tipopropresentacion = DB::table('ttipospropuestapresentacion')->get();
        $objProSup = new PropuestaSupport();
        $revs = $objProSup->getRevisiones('');
        reset($revs);
        $revision="A";  
        if(count($revs)>0){
            $revision = current($revs);
        }
        //$codigoProp =  $propuestaSupport->getCodigoPropuesta(0);
        $codigoProp=null;
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();

        $editar=false;
        $bsave=false;

        $aprobacion = false;

    	return view('propuesta/registroPropuesta',compact('editar','servicio','formacotiza','estadopropuesta','personal_rp','personal_rfp','personal_cp','personal_gp','tipopropresentacion','monedas','medioentrega','revision','codigoProp','bsave','aprobacion'));
    }

    public function editar(Request $request,$id){
        Session::forget('paqpropuesta');
        Session::forget('delPaqpropuesta');
        //Session::flush(); 

        $objEmp = new EmpleadoSupport(); 
        $aprobacion=false;
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$id)->get();
        $propuesta = $propuesta[0];
        $persona  = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();
        $servicio = DB::table('tservicio')->lists('descripcion','cservicio');
        $formacotiza = DB::table('tformacotizacion')->get();
        $estadopropuesta = DB::table('testadopropuesta')
        ->orderBy('cestadopropuesta','ASC')
        ->lists('descripcion','cestadopropuesta');
        $estadopropuesta = [''=>''] + $estadopropuesta;
        $personal_rp = $objEmp->getPersonalRol('RP','001','2') ;
        $personal_rp = [''=>''] + $personal_rp;

        $personal_rfp = $objEmp->getPersonalRol('RFP','001','2') ;
        $personal_rfp = [''=>''] + $personal_rfp;
        $personal_cp = $objEmp->getPersonalRol('CP','001','2') ;
        $personal_cp = [''=>''] + $personal_cp;
        $personal_gp = $objEmp->getPersonalRol('GP','001','2') ;
        $personal_gp = [''=>''] + $personal_gp;        

        $tipopropresentacion = DB::table('ttipospropuestapresentacion')->get();
        //pendiente devolver los valores de tpropuestapresentacion
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $propresentacion = DB::table('tpropuestapresentacion')->where('cpropuesta','=',$propuesta->cpropuesta)->get();
        $revision=""; // se utiliza en la vista si no existe propuesta->revision 
        $editar=true;
        $bsave=true;

        /* Aprobaciones*/
        $objFlow = new FlowSupport();
        $aflujo = $objFlow->obtenerSiguienteCondicion('001','2',$propuesta->ccondicionoperativa);
        $hflujo = $objFlow->obtenerHistorialPropuesta($propuesta->cpropuesta);
        $aprobacion = $objFlow->isApruebaPropuesta($propuesta->cpropuesta);
        //dd($aflujo);
        /* Fin Aprobaciones*/

        $paquetesp=array();
        $tpaquetesp=DB::table('tpropuestapaquetes')->where('cpropuesta','=',$propuesta->cpropuesta)->get();
        foreach($tpaquetesp as $tpaq){
            $p=array();
            $p['cpropuestapaquete'] = $tpaq->cpropuestapaquete;
            $p['cpropuesta'] = $tpaq->cpropuesta;
            $p['codigopaquete'] = $tpaq->codigopaquete;
            $p['nombrepaquete'] = $tpaq->nombrepaquete;
            $paquetesp[count($paquetesp)]=$p;
        }
                
            
        
        Session::put('paqpropuesta',$paquetesp);        

        return view('propuesta/registroPropuesta',compact('propuesta','persona','editar','uminera','servicio','formacotiza','estadopropuesta','personal_rp','personal_rfp','personal_cp','personal_gp','tipopropresentacion','monedas','medioentrega','revision','propresentacion','bsave','aflujo','hflujo','aprobacion'));
    }

    public function grabar(PropuestaRequest $request){
        DB::beginTransaction();
        $cpropuesta= $request->input('cpropuesta');
        if(strlen($cpropuesta)>0){
            $propuesta = Tpropuestum::where('cpropuesta','=',$cpropuesta)->first();
            $propresentacion = Tpropuestapresentacion::where('cpropuesta','=',$cpropuesta)->delete();
            if(empty($propuesta->ccondicionoperativa)){
                $propuesta->ccondicionoperativa='ELA';
            }
            if(empty($propuesta->revision)){
                $propuesta->revision="A";
            }
        }else{
            $propuesta = new Tpropuestum();
            $propuesta->ccondicionoperativa='ELA';
            $propuesta->revision="A";
        }

        if(strlen(trim($propuesta->ccodigo)) <= 0 ){
            $prop = new HelperSIGSupport();
            $propuesta->ccodigo =  $prop->getCodigoPropuesta();
        }

        /** Asignar Valores de los Campos
        **/


        $propuesta->cpersona = $request->input('cpersona');
        $propuesta->cunidadminera = $request->input('cunidadminera');
        $propuesta->ccodigo = $request->input('codigopropuesta');
        $propuesta->nombre = $request->input('propuestanombre');
        $propuesta->descripcion = $request->input('propuestadescripcion');

        $propuesta->cestadopropuesta = $request->input('estadopropuesta');
        $propuesta->tag =$request->input('propuestatag');
        $propuesta->revision = $request->input('propuestarevision');
        $propuesta->ftipo = $request->input('tipopropuesta');
        $propuesta->cformacotizacion = $request->input('formacotiza');
        $propuesta->cservicio = $request->input('servicio');
        $propuesta->cpersona_revprincipal = $request->input('revprincipal');
        $propuesta->cpersona_coordinadorpropuesta = $request->input('coorpropuesta');
        $propuesta->cpersona_resppropuesta = $request->input('respropuesta');
        $propuesta->cpersona_gteproyecto = $request->input('gtepropuesta');
        $propuesta->observaciones = $request->input('propuestaobservaciones');


        $propuesta->cmoneda = $request->input('moneda');
        $propuesta->cmedioentrega = $request->input('mentrega');

        if(strlen($request->input('propuestafbases')>0)){
            $propuesta->fentregabases= $request->input('propuestafbases');
        }else{
            $propuesta->fentregabases=null;
        }
        if(strlen($request->input('fpresentacion')>0)){
            $propuesta->fpresentacion= $request->input('fpresentacion');
        }else{
            $propuesta->fpresentacion=null;
        }

        if(strlen($request->input('fvtecnica')>0)){
            $propuesta->fvtecnica= $request->input('fvtecnica');
        }else{
            $propuesta->fvtecnica=null;
        }

        if(strlen($request->input('fconsulta')>0)){
            $propuesta->fconsulta= $request->input('fconsulta');
        }else{
            $propuesta->fconsulta=null;
        }

        if(strlen($request->input('frespuesta')>0)){
            $propuesta->frespuesta= $request->input('frespuesta');
        }else{
            $propuesta->frespuesta=null;
        }

        if(strlen($request->input('fabsolucionconsultas')>0)){
            $propuesta->fabsolucionconsultas= $request->input('fabsolucionconsultas');
        }else{
            $propuesta->fabsolucionconsultas=null;
        }

        if(strlen($request->input('fcharla')>0)){
            $propuesta->fcharla= $request->input('fcharla');
        }else{
            $propuesta->fcharla=null;
        }

        $propuesta->fpropuesta = Carbon::now();
        /* Fin Asignación de Campos ----
        */
        $propuesta->save();
        $cpropuesta = $propuesta->cpropuesta;
        
        /*$objProSup = new PropuestaSupport();
        $rev = $objProSup->generarRevision($propuesta->cpropuesta);
        $propuesta->revision=$rev;
        $propuesta->save();*/

        // Array presentacion de Propuesta;
        $presentacion = Input::get('presentacion');
        $i=0;
        foreach($presentacion as $p){
            $i++;
            DB::table('tpropuestapresentacion')->insert(
                ['cpropuesta' => $propuesta->cpropuesta, 'cpresentacion' => $p,'item' => $i]
            );
        }

        //Grabar Paquetes de Propuesta
        $paqpropuesta = Session::get('paqpropuesta');
        if($paqpropuesta){
            foreach($paqpropuesta as $paq){
                if($paq['cpropuestapaquete']<=0){
                    $paquetespropuesta = new Tpropuestapaquete();
                    $paquetespropuesta->cpropuesta=$cpropuesta;
                }else{
                    $paquetespropuesta = Tpropuestapaquete::where('cpropuesta','=',$cpropuesta)
                    ->where('cpropuestapaquete','=',$paq['cpropuestapaquete'])->first(); 
                }
                    
                $paquetespropuesta->codigopaquete=$paq['codigopaquete'];
                $paquetespropuesta->nombrepaquete=$paq['nombrepaquete'];
                $paquetespropuesta->save();
            }
        }
        $delPaqpropuesta = Session::get('delPaqpropuesta');
        if($delPaqpropuesta){
            foreach($delPaqpropuesta as $del){
                Tpropuestapaquete::where('cpropuestapaquete','=',$del)->delete();
            }
        }
        

        DB::commit();        
        
        return Redirect::route('editarPropuesta',$cpropuesta);
    }
    public function sgteFlujo(Request $request){
        DB::beginTransaction();
        $objFlow = new FlowSupport();
        $cpropuesta = $request->input('cpropuesta_aprob');
        $next = $request->input('flujo');


        $res = $objFlow->siguienteFlujoPropuesta($cpropuesta,$next);
        DB::commit();
        return Redirect::route('editarPropuesta',$cpropuesta);
    }
    public function mostrarPaquetes(Request $request,$idPropuesta){
        $paquetesp=array();
        if (Session::get('paqpropuesta')){
            $paquetesp= Session::get('paqpropuesta');
            
        }
        //dd($paquetesp);
        return View('partials.tablePaquetes',compact('paquetesp'));
    }
    public function addPaquete(Request $request){
        $cpropuesta = $request->input('cpropuesta');
        if(strlen(trim($request->input('codpaq'))) <=0 && strlen(trim($request->input('nompaq'))) <=0  ){
            return Redirect::route('listarPaquetes',0);
        }
        $paquetesp=array();
        $obj = array();
        $obj['cpropuestapaquete']=-1*(count($paquetesp)+10);
        $obj['cpropuesta']=$cpropuesta;
        $obj['codigopaquete']=$request->input('codpaq');
        $obj['nombrepaquete']=$request->input('nompaq');
        if(Session::get('paqpropuesta')){
            $paquetesp= Session::get('paqpropuesta');
        }
        $paquetesp[count($paquetesp)]=$obj;
        Session::put('paqpropuesta',$paquetesp);
        return Redirect::route('listarPaquetes',$cpropuesta);
    }
    public function delPaquete(Request $request){
        $cpropuestapaquete = $request->input('cpropuestapaquete');
        $cpropuesta = $request->input('cpropuesta');
        $delPaquetesp=array();
        if(Session::get('delPaqpropuesta')){
            $delPaquetesp= Session::get('delPaqpropuesta');
        }        

        if($cpropuestapaquete > 0){
            $delPaquetesp[count($delPaquetesp)] = $cpropuestapaquete;
        }else{

        }
        $paquetesp = array();
        if(Session::get('paqpropuesta')){
            $paquetesp= Session::get('paqpropuesta');
        }

        $temp = array();

        foreach($paquetesp as $paq){
            if($paq['cpropuestapaquete']!=$cpropuestapaquete){
                $temp[count($temp)] = $paq;
            }
        }
        $paquetesp=$temp;

        Session::put('paqpropuesta',$paquetesp);
        Session::put('delPaqpropuesta',$delPaquetesp);        
        //dd($paquetesp);
        return View('partials.tablePaquetes',compact('paquetesp'));
        //return Redirect::route('listarPaquetes',$cpropuesta);
    }

    public function disciplina(){
        //$disciplinas = DB::table('tdisciplina')->get();
        
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;
        $bsave=false;
    	return view('propuesta/disciplina',compact('servicios','bsave'));
    }   

    public function editarDisciplina(Request $request,$idPropuesta){
        Session::forget('propuestadis');
        Session::forget('delDisciplinas');
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->first();
        $persona = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $personaresp='';
        if(strlen($propuesta->cpersona_resppropuesta)>0){
            $personaresp = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona_resppropuesta)->first();
        }
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();



        $tdisciplinas = DB::table('tdisciplina as d')
        ->join('tdisciplinaareas as da','da.cdisciplina','=','d.cdisciplina')
        ->select('d.*')
        ->distinct()
        ->get();
        $disciplinas = array();
        foreach($tdisciplinas as $tdis){
            $d['cdisciplina'] = $tdis->cdisciplina;
            $d['descripcion'] = $tdis->descripcion;
            $d['tipodisciplina'] = $tdis->tipodisciplina;
            $da = array();
            $dis_area = DB::table('tdisciplinaareas as da')
            ->join('tareas as a','da.carea','=','a.carea')
            ->join('tpersonadatosempleado as pd','a.carea','=','pd.carea')
            ->join('tpersona as p','pd.cpersona','=','p.cpersona')
            ->where('da.cdisciplina','=',$d['cdisciplina'])
            ->orderBy('p.nombre','ASC')
            ->select('p.*','pd.carea','da.cdisciplina')
            ->get();
            $d['da']= $dis_area;
            $disciplinas[count($disciplinas)] = $d;
        }
        
        $tpropuestadisciplina = DB::table('tpropuestadisciplina as p')
        ->join('tdisciplina as d','p.cdisciplina','=','d.cdisciplina')
        ->leftJoin('tpersona as rdp','p.cpersona_rdp','=','rdp.cpersona')
        ->where('cpropuesta','=',$propuesta->cpropuesta)
        ->select("p.*",'d.descripcion','rdp.nombre as name_rdp')
        ->get();

        $propuestadisciplina = array();
        foreach($tpropuestadisciplina as $dis){
            $pd['cpropuestadisciplina']=$dis->cpropuestadisciplina;
            $pd['cdisciplina']=$dis->cdisciplina;
            $pd['cpersona_rdp']=$dis->cpersona_rdp;
            $pd['descripcion'] = $dis->descripcion;
            $pd['name_rdp'] = $dis->name_rdp;
            $propuestadisciplina[count($propuestadisciplina)]=$pd;
        }
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;        

        $bsave=true;
        Session::put('propuestadis',$propuestadisciplina);
        return view('propuesta/disciplina',compact('servicios','propuesta','persona','uminera','personaresp','propuestadisciplina','bsave','disciplinas'));
    }
    public function verDisciplinas(Request $request){
        $propuestadisciplina=array();
        if(Session::get('propuestadis')){
            $propuestadisciplina = Session::get('propuestadis');
        }        
        return view('propuesta.tableDisciplina',compact('propuestadisciplina'));
    }
    public function agregarDisciplinas(Request $request){
        $propuestadisciplina=array();
        if(Session::get('propuestadis')){
            $propuestadisciplina = Session::get('propuestadis');
        }
        $tdisciplinas = DB::table('tdisciplina')->get();


        foreach($tdisciplinas as $dis){
            if(!is_null($request->input('rad_'.$dis->cdisciplina)) ){
             
                $i = -1 * (count($propuestadisciplina) + 10)*2;
                $pd['cpropuestadisciplina']= $i;
                $pd['cdisciplina']=$dis->cdisciplina;
                $valor = $request->input('rad_'.$dis->cdisciplina);
                $aValor  = explode('_',$valor);
                $pd['cpersona_rdp'] = $aValor[1];
                $tpersona = DB::table('tpersona')
                ->where('cpersona','=',$pd['cpersona_rdp'])
                ->first();
                $pd['descripcion'] = $dis->descripcion;
                $pd['name_rdp'] = $tpersona->nombre;
                $propuestadisciplina[count($propuestadisciplina)] = $pd;
            }
        }

        Session::put('propuestadis',$propuestadisciplina);
        
        return view('propuesta.tableDisciplinaSeleccionado',compact('propuestadisciplina'));
    }
    public function borrarDisciplinas(Request $request){
        $propuestadisciplina=array();
        $delDisciplinas = array();
        if(Session::get('delDisciplinas')){
            $delDisciplinas = Session::get('delDisciplinas');
        }
        $aDel = $request->input('chk');
        foreach($aDel as $del){
            if($del > 0){
                $delDisciplinas[count($delDisciplinas)] = $del;
            }

        }
        Session::put('delDisciplinas',$delDisciplinas);  

        if(Session::get('propuestadis')){
            $propuestadisciplina = Session::get('propuestadis');
        }

        $new = array();
   
        foreach($propuestadisciplina as $pd){
            foreach($aDel as $del){
                        $new[count($new)]=$pd;

                
            }

        }        
        $propuestadisciplina = $new;

        Session::put('propuestadis',$propuestadisciplina);                
        return view('propuesta.tableDisciplinaSeleccionado',compact('propuestadisciplina'));
    }    
    public function grabarDisciplina(Request $request){
        DB::beginTransaction();
        $cpropuesta= $request->input('cpropuesta');
        /*$disciplina = Input::get('disciplina');
        Tpropuestadisciplina::where('cpropuesta','=',$cpropuesta)->delete();

        foreach($disciplina as $d){

                if(isset($d[0])){
                    $disciplinas = new Tpropuestadisciplina();
                    $disciplinas->cpropuesta=$cpropuesta;
                    $disciplinas->cdisciplina=$d[0];
                    if(!empty($d[1])){
                        $disciplinas->cpersona_rdp=$d[1];
                    }
                    if(isset($d[2])){
                        $disciplinas->escontratista = '1';
                    }
                    if(!empty($d[3])){
                        $disciplinas->cpersona_proveedor=$d[3];
                    }
                    $disciplinas->save();
                }

        }  */ 
        //dd($disciplina);
        $propuestadisciplina=array();
        $delDisciplinas = array();
        if(Session::get('delDisciplinas')){
            $delDisciplinas = Session::get('delDisciplinas');
        }
        if(Session::get('propuestadis')){
            $propuestadisciplina = Session::get('propuestadis');
        }
        
        foreach($delDisciplinas as $d){
            Tpropuestadisciplina::where('cpropuestadisciplina','=',$d)->delete();
        }     
        foreach($propuestadisciplina as $p){
            if($p['cpropuestadisciplina']<=0){
                $prod = new Tpropuestadisciplina();
            }else{
                $prod = Tpropuestadisciplina::where('cpropuestadisciplina','=',$p['cpropuestadisciplina'])->first();
            }
            $prod->cpropuesta=$cpropuesta;
            $prod->cdisciplina = $p['cdisciplina'];
            $prod->cpersona_rdp = $p['cpersona_rdp'];
            $prod->save();
        }           
        DB::commit();
        
        return Redirect::route('editarDisciplinaPropuesta',$cpropuesta);
    }

    public function estructura(){
        Session::forget('pactividades');
        $actiParent= DB::table('tactividad')->whereNull('cactividad_parent')->get();
        $actividades = array();
        $objAct  = new ActividadSupport();
        foreach ($actiParent as $act) {
            $actividades[count($actividades)]=$act;
            $actividades=$objAct->listarActividad($actividades,$act);
        }
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;        
    	return view('propuesta/estructuraPropuesta',compact('servicios','actividades'));
    }

    public function editarEstructura(Request $request,$idPropuesta){
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->get();
        $propuesta = $propuesta[0];
        Session::forget('pactividades');
        Session::forget('participantes');
        Session::forget('delActividad');
        Session::forget('delEstructura');
        $actiParent= DB::table('tactividad')->whereNull('cactividad_parent')->get();
        $actividades = array();
        $objAct  = new ActividadSupport();
        foreach ($actiParent as $act) {
            $actividades[count($actividades)]=$act;
            $actividades=$objAct->listarActividad($actividades,$act);
        }
        
        $persona = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $personacoor='';
        if(strlen($propuesta->cpersona_coordinadorpropuesta)>0){
            $personacoor = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona_coordinadorpropuesta)->first();
        }
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();

        $paqpropuesta = DB::table('tpropuestapaquetes')->where('cpropuesta','=',$propuesta->cpropuesta)->lists('nombrepaquete','cpropuestapaquete');

        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;  

        $tactividades = DB::table('tpropuestaactividades as proact')
        ->leftJoin('tactividad as act','proact.cactividad','=','act.cactividad')
        ->where('cpropuesta','=',$idPropuesta)
        ->select('proact.*','act.cactividad_parent')
        ->orderBY("codigoactividad",'ASC')
        ->get();
        
        $actividadesp = array();
        foreach($tactividades as $act){
            $a['cpropuestaactividades'] = $act->cpropuestaactividades;
            $a['item'] ='';
            $a['cpropuesta'] = $act->cpropuesta;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactividad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cpropuestapaquete'] = $act->cpropuestapaquete;
            $a['cpropuestaactividades_parent'] = $act->cpropuestaactividades_parent;
            $actividadesp[count($actividadesp)] = $a;
        }  
        // dd($actividadesp);
        $actividadesp = $objAct->orderParentActividadEjecucion($actividadesp);

        Session::put('pactividades',$actividadesp);

        /* Estructura Treeview de Actividades*/
        $actParent= DB::table('tactividad as act')
        ->where('tipoactividad','=','00001')
        ->whereNull('cactividad_parent')
        ->get();
        $estructura="";

        
        foreach ($actParent as $act) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$act->cactividad. "',";
            $estructura=$estructura ."label: '".$act->descripcion ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$objAct->listarTreeview($act);
            //dd($est);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }
        /* Fin de estructura de Treeview Actividades*/

        $categorias = Troldespliegue::select('croldespliegue',DB::raw('CONCAT(codigorol,\'-\',descripcionrol) as descripcion'))
        ->where('csubsistema','=','001' /*new EnumSubsistema(EnumSubsistema::PROPUESTA)*/)
        ->where('tiporol','=','1')
        ->lists('descripcion','croldespliegue')->all();
        $categorias = [''=>'']+ $categorias;

        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); 
        $personal = [''=>''] + $personal;

        $personal_rate = DB::table('tpersona as per')
        ->join('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->get();

        $participantes= array();
        $tparticipantes = DB::table('testructuraparticipante as testru')
        ->join('troldespliegue as rol','rol.croldespliegue','=','testru.croldespliegue')
        ->leftJoin('tpersona as tper','tper.cpersona','=','testru.cpersona_rol')
        ->select('testru.cestructurapropuesta','testru.cpropuesta','testru.croldespliegue','testru.cpersona_rol','testru.rate','tper.nombre','rol.descripcionrol')
        ->where('rol.csubsistema','=','001'/*new EnumSubsistema(EnumSubsistema::PROPUESTA)*/)
        ->where('rol.tiporol','=','1')
        ->where('testru.cpropuesta','=',$idPropuesta)
        ->where('testru.tipoestructura','=','0')
        ->orderBy('rol.codigorol','ASC')
        ->get();

        foreach($tparticipantes as $tpar){
            $participantes[count($participantes)] = array(
                'cestructurapropuesta' => $tpar->cestructurapropuesta,
                'cpropuesta' => $tpar->cpropuesta,
                'croldespliegue' => $tpar->croldespliegue,
                'cpersona_rol' => $tpar->cpersona_rol,
                'rate' => $tpar->rate,
                'nombre' => $tpar->nombre,
                'descripcionrol' => $tpar->descripcionrol
                );
        }

        Session::put('participantes',$participantes);
       

        return view('propuesta/estructuraPropuesta',compact('servicios','propuesta','uminera','persona','personacoor','paqpropuesta','actividades','actividadesp','estructura','categorias','personal','participantes','personal_rate'));
    } 

    public function listarActividades(Request $request,$idPropuesta){
        if (Session::get('pactividades')){
            $actividadesp= Session::get('pactividades');
            
        }else{
            if(strlen($idPropuesta)> 0 && $idPropuesta!='0'){
                $actividadesp=DB::table('tpropuestaactividades')->where('cpropuesta','=',$idPropuesta)->get();
                
            }else{
                
                $actividadesp=array();

            }
        }
        return view('partials.tableActividadPropuesta',compact('actividadesp'));        
    }
    public function addActividad(Request $request){
        $acti = $request->input('selected_actividad');
        
        $tactividad = DB::table('tactividad')->whereIn('cactividad',$acti)->get();
        $actividadesp = array();
        if(Session::get('pactividades')){
            $actividadesp = Session::get('pactividades');
        }
        $obj = new ActividadSupport();
        foreach($tactividad as $a){
            $res = array_search($a->cactividad, array_map(function($e){return $e['cactividad'];},$actividadesp));
            //dd(array_map(function($e){return $e->cactividad;},$actividadesp));
            if( $res===false){
                //$wact = new Tpropuestaactividade();
                $i= -1*(count($actividadesp)+1)*10;
                $wact['item']='';
                $wact['cpropuestaactividades'] =$i;
                $wact['cactividad'] = $a->cactividad;
                $wact['codigoactividad'] = $a->codigo;
                $wact['descripcionactividad'] = $a->descripcion;
                $wact['numerodocumentos'] = 0;
                $wact['numeroplanos'] = 0;
                $wact['cpropuestapaquete'] = $request->input('paqpropuesta');
                $wact['cactividad_parent'] = $a->cactividad_parent;
                $wact['cpropuestaactividades_parent'] = null;
                $actividadesp[count($actividadesp)] = $wact;
                if(strlen($a->cactividad_parent) >0){
                    $actividadesp = $obj->addParent($actividadesp,$a->cactividad_parent,$request->input('paqpropuesta'));
                }
                $actividadesp=$obj->orderParentActividadEjecucion($actividadesp);
                Session::put('pactividades',$actividadesp); 
            }
        } 
        $actividadesp = Session::get('pactividades',array());
        return view('partials.tableActividadPropuesta',compact('actividadesp'));  
    }  
    public function delActividad($idActividad){
        $actividadesp = array();
        if(Session::get('pactividades')){
            $actividadesp = Session::get('pactividades');
        }
        if($idActividad > 0){
            $delActividad=array();
            if(Session::get('delActividad')){
                $delActividad = Session::get('delActividad');
            }
            $delActividad[count($delActividad)]=$idActividad;
            Session::put('delActividad',$delActividad);
        }        
        $newactiv = array();
        //dd($actividadesp);
        foreach($actividadesp as $act){
            if ($act['cpropuestaactividades']!=$idActividad){
                $newactiv[count($newactiv)]=$act;
            }
            

        }        
        $actividadesp = $newactiv;

        Session::put('pactividades',$actividadesp);
        return view('partials.tableActividadPropuesta',compact('actividadesp'));  
    }
    public function addEstructura(Request $request){
        $categ= $request->input('categorias');
        $profe= $request->input('profesional');
        $rate = $request->input('rate');
        if(empty($rate)){
            $rate=0;
        }
        if(!empty($profe)){
            $personal = DB::table('tpersona')->where('cpersona','=',$profe)->first();
        
        }
        $rol= DB::table('troldespliegue')->where('croldespliegue','=',$categ)->first();

        $participantes=array();
        if(Session::get('participantes')){
            $participantes = Session::get('participantes',array());
        }
        $i= -1*(count($participantes)+1)*10;


        $participantes[count($participantes)] = array(
            'cestructurapropuesta' => $i ,
            'croldespliegue' => $categ,
            'cpersona_rol' => $profe,
            'rate' => $rate,
            'nombre' => isset($personal->nombre)==1?$personal->nombre:'',
            'descripcionrol' => $rol->descripcionrol
            );

        Session::put('participantes',$participantes);
        $participantes = Session::get('participantes',array());
        //dd($participantes);
        return view('partials.tableEstructura',compact('participantes'));
    }

    public function delEstructura($idestructura){
        $participantes=array();
        if(Session::get('participantes')){
            $participantes = Session::get('participantes');
        }
        if($idestructura > 0){
            $delEstructura=array();
            if(Session::get('delEstructura')){
                $delEstructura = Session::get('delEstructura');
            }
            $delEstructura[count($delEstructura)]=$idestructura;
            Session::put('delEstructura',$delEstructura);
        }
        $newestr=array();
        foreach($participantes as $par){
            if($par['cestructurapropuesta']!=$idestructura){
                $newestr[count($newestr)]=$par;
            }
        }
        $participantes=$newestr;
        Session::put('participantes',$participantes);

        return view('partials.tableEstructura',compact('participantes'));
    }

    public function grabarEstructura(Request $request){
        DB::beginTransaction();
        $cpropuesta= $request->input('cpropuesta');
        $actividadesp = Session::get('pactividades');
        //dd($request->input('txtnrodoc'));
        foreach($actividadesp as $act){
            if ($act['cpropuestaactividades'] <=0){
                $newact = new Tpropuestaactividade();
                $newact->cpropuesta = $cpropuesta;
                $newact->cactividad = $act['cactividad'];
                $newact->codigoactividad = $act['codigoactividad'];
                $newact->descripcionactividad = $act['descripcionactividad'];
                if(!is_null($act['cactividad_parent']) && strlen($act['cactividad_parent'])>0){
                    $res = array_search($act['cactividad_parent'],array_map(function($e){ return $e['cactividad'];},$actividadesp));
                    if($res===FALSE){

                    }else{
                        $newact->cpropuestaactividades_parent = $actividadesp[$res]['cpropuestaactividades'];
                    }
                }
            }else{
                $newact= Tpropuestaactividade::where('cpropuestaactividades','=',$act['cpropuestaactividades'])->first();
            }
            $newact->numerodocumentos=$request->input('txtnrodoc')[$act['cpropuestaactividades']];
            $newact->numeroplanos = $request->input('txtnropla')[$act['cpropuestaactividades']];
            $newact->cpropuestapaquete = $act['cpropuestapaquete'];
            $newact->save();
        }
        $delActividades=array();
        if(Session::get('delActividad')){
            $delActividades = Session::get('delActividad');
        }
        foreach($delActividades as $d){
            $delAct = Tpropuestaactividade::find($d);
            $delAct->delete();
        }

        $participantes=array();
        if(Session::get('participantes')){
            $participantes = Session::get('participantes');
        }
        foreach($participantes as $part){
            if($part['cestructurapropuesta'] <= 0 ){
                $est = new Testructuraparticipante();
                $est->cpropuesta = $cpropuesta;
                $est->croldespliegue =$part['croldespliegue'];
                if(isset($part['cpersona_rol'])){
                    if($part['cpersona_rol'] >0){
                        $est->cpersona_rol= $part['cpersona_rol'];
                    }
                }
                $est->tipoestructura='0';
                $est->rate= $part['rate'];
                $est->save();
            }
        }
        $delEstructura=array();
        if(Session::get('delEstructura')){
            $delEstructura = Session::get('delEstructura');
        }
        foreach($delEstructura as $d){
            $est = Testructuraparticipante::find($d);
            $est->delete();
        }
        DB::commit();
        return Redirect::route('editarEstructuraPropuesta',$cpropuesta);
    }

    public function detalle(){
        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;          
        return view('propuesta/detallePropuesta',compact('servicios'));
    }  
    public function editarDetalle(Request $request,$idPropuesta){
        $totGastos=0;
        $totGastosLabCan=0;
        $totGastosLabCim=0;
        $totGastosLabBot=0;
        $totGastosLab=0;

        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->first();
        $persona = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $personaresp='';
        Session::forget('listGastos');
        Session::forget('delGastos');
        Session::forget('listGastosLab');
        Session::forget('delGastosLab');        
        if(strlen($propuesta->cpersona_resppropuesta)>0){
            $personaresp = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona_resppropuesta)->first();
        }
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();

        $paqpropuesta = DB::table('tpropuestapaquetes')->where('cpropuesta','=',$propuesta->cpropuesta)->lists('nombrepaquete','cpropuestapaquete');
        $paq = "";
        foreach($paqpropuesta as $k => $v){
            $paq = $k;
            break;
        }

        $servicios = Tservicio::lists('descripcion','cservicio')->all();
        $servicios = [''=>''] + $servicios;     



        $lactividades = DB::table('tpropuestaactividades as pact')
        ->leftJoin('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$propuesta->cpropuesta)
        ->where('pact.cpropuestapaquete','=',$paq)
        ->select('pact.*','act.cactividad_parent')
        ->orderBy('pact.codigoactividad','ASC')
        ->get();
        $listaAct = array();
        $cont_doc = 0;
        $cont_pla = 0;
        foreach($lactividades as $act){
            $a['cpropuestaactividades'] = $act->cpropuestaactividades;
            $a['item'] ='';
            $a['cpropuesta'] = $act->cpropuesta;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactividad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cpropuestapaquete'] = $act->cpropuestapaquete;
            $a['cpropuestaactividades_parent'] = $act->cpropuestaactividades_parent;
            $a['comentario'] = $act->comentario;
            $a['flag_sugerencia'] = $act->flag_sugerencia;
            $a['sugerencia_descripcion'] = $act->sugerencia_descripcion;
            $listaAct[count($listaAct)] = $a;
            $cont_doc+=$act->numerodocumentos;
            $cont_pla+=$act->numeroplanos;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEje($listaAct);

        
        
        $objPro = new PropuestaSupport();
        $listEstr = $objPro->getProfesionalesSenior($propuesta->cpropuesta);
        $listDis = $objPro->getDisciplinas($propuesta->cpropuesta);
        $listEstr_dis = $objPro->getProfesionalesDisciplina($propuesta->cpropuesta);
        $listEstr_apo = $objPro->getProfesionalesApoyo($propuesta->cpropuesta);

        /* Estructura de Gastos*/
        $tipoGasto='00003';
        $gasParent= DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=',$tipoGasto)
        ->whereNull('cconcepto_parent')
        ->get();
        $estructura="";

        $objGas  = new GastosSupport();
        foreach ($gasParent as $gas) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$gas->cconcepto. "',";
            $estructura=$estructura ."label: '".$gas->descripcion ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$objGas->listarTreeview($gas,$tipoGasto);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }
        /* Fin de estructura de Gastos*/

        /* Estructura de Gastos Laboratorio*/
        $tipoGasto='00002';
        $gasParent= DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=',$tipoGasto)
        /*->whereNull('cconcepto_parent')*/
        ->get();
        $estructuraLab="";

        //$objGas  = new GastosSupport();
        foreach ($gasParent as $gas) {
            if(!empty($estructuraLab)){
                $estructuraLab = $estructuraLab .",";
            }
            //$actividades[count($actividades)]=$act;
            $estructuraLab=$estructuraLab ."{\n\r";
            $estructuraLab=$estructuraLab ."item: {\n\r";
            $estructuraLab=$estructuraLab ."id: '".$gas->cconcepto. "',";
            $estructuraLab=$estructuraLab ."label: '".$gas->descripcion ."',";
            
            $estructuraLab=$estructuraLab ."checked: false ";
            $estructuraLab=$estructuraLab ."}\n\r";

            
            /*
            
            $est=$objGas->listarTreeview($gas,$tipoGasto);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }*/

            $estructuraLab=$estructuraLab ."}\n\r";
        }
        /* Fin de estructura de Gastos de Laboratorio*/

        /* Obtener lista de Gastos Grabados*/
        $tgastos = DB::table('tpropuestagastos as progas')
        ->join('tconceptogastos as congas','progas.concepto','=','congas.cconcepto')
        ->where('progas.cpropuesta','=',$idPropuesta)
        ->select('progas.*','congas.descripcion','congas.cconcepto_parent')
        ->orderBy('progas.item','ASC')
        ->get();

        $gastos = array();
        foreach($tgastos as $tgas){
            $g['cpropuestaproyectogastos'] = $tgas->cpropuestaproyectogastos;
            $g['cpropuesta'] = $tgas->cpropuesta;
            $g['item'] = $tgas->item;
            $g['concepto'] = $tgas->concepto;
            $g['descripcion'] = $tgas->descripcion;
            $g['unidad']=$tgas->unidad;
            $g['cantidad'] = $tgas->cantidad;
            $g['costou'] =  $tgas->costou;
            $g['subtotal'] = $tgas->cantidad * $tgas->costou;
            $g['cconcepto_parent'] = $tgas->cconcepto_parent;
            $g['comentario'] = $tgas->comentario;
            $g['cpersona_proveedor'] = $tgas->cpersona_proveedor;
            $g['cpropuestapaquete'] = $tgas->cpropuestapaquete;
            $totGastos+= $g['subtotal'];
            $gastos[count($gastos)]=$g;
        }
        /* Fin de Obtener lista de Gastos Grabados*/

        $objGas  = new GastosSupport();
        $gastos = $objGas->orderParent($gastos);
        Session::put('listGastos',$gastos);


        /* Obtener lista de Gastos Grabados laboratorio*/
        $tgastosLab = DB::table('tpropuestagastoslaboratorio as progas')
        ->join('tconceptogastos as congas','progas.concepto','=','congas.cconcepto')
        ->where('progas.cpropuesta','=',$idPropuesta)
        ->select('progas.*','congas.descripcion','congas.cconcepto_parent')
        ->orderBy('progas.item','ASC')
        ->get();
        $gastosLab = array();

        foreach($tgastosLab as $tgasl){
            $g['cpropuestagastoslab'] = $tgasl->cpropuestagastoslab;
            $g['cpropuesta'] = $tgasl->cpropuesta;
            $g['item'] = $tgasl->item;
            $g['concepto'] = $tgasl->concepto;
            $g['descripcion'] = $tgasl->descripcion;
            $g['cmoneda']=$tgasl->cmoneda;
            $g['cantcimentacion'] = $tgasl->cantcimentacion;
            $g['cantcanteras'] = $tgasl->cantcanteras;
            $g['cantbotadero'] = $tgasl->cantbotadero;
            $g['preciou'] =  $tgasl->preciou;
            $g['subtotal'] =  $tgasl->preciou * ($tgasl->cantcimentacion + $tgasl->cantcanteras + $tgasl->cantbotadero);
            $g['cpersona_proveedor'] = $tgasl->cpersona_proveedor;
            $g['cpropuestapaquete'] = $tgasl->cpropuestapaquete;
            $g['cconcepto_parent'] = $tgasl->cconcepto_parent;
            $totGastosLabCim+=$tgasl->cantcimentacion;
            $totGastosLabBot+=$tgasl->cantbotadero;
            $totGastosLabCan+=$tgasl->cantcanteras;
            $totGastosLab+= $g['subtotal'];
            $gastosLab[count($gastosLab)]=$g;
        }
        /* Fin de Obtener lista de Gastos Grabados Laboratorio*/   
        $gastosLab = $objGas->orderParent($gastosLab);
        Session::put('listGastosLab',$gastosLab);     

    	/*return view('propuesta/detallePropuesta',compact('servicios','propuesta','uminera','persona','personaresp','paqpropuesta','estructura','gastos','gastosLab','estructuraLab','listaAct','listEstr','listEstr_apo','listEstr_dis','listDis','totGastos','totGastosLab','totGastosLabCim','totGastosLabBot','totGastosLabCan'));*/

        /* Cronograma Rooster */
        $fechaI=Carbon::today();
        $fechaF=Carbon::today();

        $tfechas = DB::table('tpropuestacronogramaconstruccion as crono')
        ->join('tpropuestaconstrucciondetafecha as fec','crono.tcronogramaconstruccionid','=','fec.tcronogramaconstruccionid')
        ->where('crono.cpropuesta','=',$propuesta->cpropuesta)
        ->select(DB::raw('min(fecha) as minimo, max(fecha) as maximo' ))
        ->first();
        //dd($tfechas);
        if($tfechas){
            if(!is_null($tfechas->minimo)){
                $fechaI= Carbon::createFromDate(substr($tfechas->minimo,0,4),substr($tfechas->minimo,5,2),substr($tfechas->minimo,8,2));
            }
            if(!is_null($tfechas->maximo)){
                $fechaF=Carbon::createFromDate(substr($tfechas->maximo,0,4),substr($tfechas->maximo,5,2),substr($tfechas->maximo,8,2));
            }
        }


        /* * Detalle de Cronograma Rooster * */
        $crono = array();
        $tcronodeta = DB::table('tpropuestacronogramaconstruccion as crono')
        ->join('tpersona as per','crono.cpersonaempleado','=','per.cpersona')
        ->leftJoin('tpersonadatosempleado as dat','crono.cpersonaempleado','=','dat.cpersona')
        ->leftJoin('tcargos as car','dat.ccargo','=','car.ccargo')
        ->where('crono.cpropuesta','=',$propuesta->cpropuesta)

        ->select('crono.*','per.nombre as empleado','dat.ccargo','car.descripcion as cargo')
        ->get();
        
        foreach($tcronodeta as $deta){
            $c['tcronogramaconstruccionid'] = $deta->tcronogramaconstruccionid;
            $c['cpersonaempleado'] = $deta->cpersonaempleado;
            $c['croldespliegue'] = $deta->croldespliegue;
            $c['empleado'] = $deta->empleado;
            $c['cargo'] = $deta->cargo;

            $tfechas = DB::table('tpropuestaconstrucciondetafecha as cfec')
            ->where('cfec.tcronogramaconstruccionid','=',$deta->tcronogramaconstruccionid)
            ->select('cfec.*',DB::raw('to_char(cfec.fecha,\'YYYYMMDD\' ) as feje'))
            ->get();
            $c['deta'] = array();
            foreach($tfechas as $fec){
                $d['cpropuestaconstrucciondetafecha']=$fec->cpropuestaconstrucciondetafecha;
                $d['fecha'] = $fec->fecha;
                $d['codactividad'] = $fec->codactividad;
                $d['nrohoras'] = $fec->horas;
                $d['observacion'] = $fec->observacion;
                $c['deta'][$fec->feje]=$d;
            }

            $crono[count($crono)]= $c;

        }
        //dd($crono);
        /* Fin Detalle de Cronograma Rooster */
        $tleyendarooster = DB::table('tcatalogo')
        ->where('codtab','=','00014')
        ->whereNotNull('digide')
        ->orderBy('digide','ASC')
        ->get();     
        $leyenda = array();  
        foreach ($tleyendarooster as $tley){
            $leye['cod'] = $tley->digide;
            $leye['des'] = $tley->descripcion;
            $leye['color'] = $tley->valor;
            $leyenda[count($leyenda)] = $leye;
        }
        $fdesde = Carbon::createFromDate($fechaI->year,$fechaI->month,$fechaI->day);
        $fhasta = Carbon::createFromDate($fechaF->year,$fechaF->month,$fechaF->day);
        $seguir = true;
        $dias= array();
        $dia = $fdesde; //->day."_".$fdesde->month;
        while($seguir){
            //$dia = $fdesde->day."_".$fdesde->month;
            $dias[count($dias)] = $dia->toDateString();
            $dia = $fdesde->addDay(1);

            if ( !( ($dia->year ."".$dia->month."". $dia->day) <= ($fhasta->year ."".$fhasta->month."". $fhasta->day) ) ) {
                $seguir=false;    
            }

            
        }         
        // Creando Array de totales por actividad horizontal solo hijos
        $lactividades1 = DB::table('tpropuestaactividades as pact')
        ->leftJoin('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$propuesta->cpropuesta)
        ->where('pact.cpropuestapaquete','=',$paq)
        ->select('pact.cpropuestaactividades','pact.cpropuestaactividades_parent')
        ->orderBy('pact.codigoactividad','ASC')
        ->get();
        //dd($lactividades1);
        for ($i=0; $i < count($lactividades1); $i++) { 
            $sumatotal =  Tpropuestahonorario::where('cpropuestaactividades','=', $lactividades1[$i]->cpropuestaactividades)->get();
            if(!is_null($sumatotal)){
                $total = 0;
                for ($e=0; $e < count($sumatotal); $e++) { 
                   $total += $sumatotal[$e]->horas;
                }
                $lactividades1[$i]->total = $total;
            }else{
                $lactividades1[$i]->total = 0;
            }
            
        }
        //dd($lactividades1);
        /* Cronograma Rooster */
        //dd($listaAct);
    	return view('propuesta/detallePropuesta',compact('servicios','crono','dias','leyenda','personal','propuesta','uminera','persona','personaresp','paqpropuesta','estructura','gastos','gastosLab','estructuraLab','listaAct','listEstr','listEstr_apo','listEstr_dis','listDis','totGastos','totGastosLab','totGastosLabCim','totGastosLabBot','totGastosLabCan','cont_doc','cont_pla','lactividades1'));
    }     
    public function viewDetalleActividad(Request $request){
        $idPropuesta = $request->input('cpropuesta');
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->first();
        $paq = $request->input('paqpropuesta');
        $lactividades = DB::table('tpropuestaactividades as pact')
        ->leftJoin('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$propuesta->cpropuesta)
        ->where('pact.cpropuestapaquete','=',$paq)
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cpropuestaactividades'] = $act->cpropuestaactividades;
            $a['item'] ='';
            $a['cpropuesta'] = $act->cpropuesta;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactividad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cpropuestapaquete'] = $act->cpropuestapaquete;
            $a['cpropuestaactividades_parent'] = $act->cpropuestaactividades_parent;
            $a['comentario'] = $act->comentario;
            $a['flag_sugerencia'] = $act->flag_sugerencia;
            $a['sugerencia_descripcion'] = $act->sugerencia_descripcion;            
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);
        $objPro = new PropuestaSupport();
        $listEstr = $objPro->getProfesionalesSenior($propuesta->cpropuesta);
        $listDis = $objPro->getDisciplinas($propuesta->cpropuesta);
        $listEstr_dis = $objPro->getProfesionalesDisciplina($propuesta->cpropuesta);
        $listEstr_apo = $objPro->getProfesionalesApoyo($propuesta->cpropuesta);



        return view('partials.tableDetalleActividad',compact('listaAct','propuesta','listEstr','listDis','listEstr_dis','listEstr_apo'));
    }

    public function editarHorasSenior(Request $request){
        $cestructurapropuesta = $request->input('cestructurapropuesta');
        $propuesta = DB::table('tpropuesta')
        ->where('cpropuesta','=',$request->input('cpropuesta'))
        ->first();
        $lactividades = DB::table('tpropuestaactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$request->input('cpropuesta'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cpropuestaactividades'] = $act->cpropuestaactividades;
            $a['item'] ='';
            $a['cpropuesta'] = $act->cpropuesta;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactividad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cpropuestapaquete'] = $act->cpropuestapaquete;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $est = DB::table('tpropuestahonorarios as hor')
            ->where('cestructurapropuesta','=',$cestructurapropuesta)
            ->where('cpropuestaactividades','=',$act->cpropuestaactividades)
            ->whereNull('cdisciplina')
            ->first();
            if($est){
                $a['hora_'.$act->cpropuestaactividades."_".$cestructurapropuesta] = $est->horas;
            
            }
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);

        return view('partials.tableInputHorasActividad',compact('listaAct','cestructurapropuesta','propuesta'));
    }
    public function grabarHorasSenior(Request $request){
        DB::beginTransaction();
        $cestructurapropuesta = $request->input('cestructurapropuesta_h');
        $horas = $request->input('horas');
        //dd($horas);
        $lactividades = DB::table('tpropuestaactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$request->input('cpropuesta_h'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            if(isset($horas[$act->cpropuestaactividades])){
                if(!empty($horas[$act->cpropuestaactividades] && $horas[$act->cpropuestaactividades] > 0) ){
                    $obj= Tpropuestahonorario::where('cpropuestaactividades','=',$act->cpropuestaactividades)
                    ->where('cestructurapropuesta','=',$cestructurapropuesta)
                    ->whereNull('cdisciplina')
                    ->first();
                    if (!$obj){
                        $obj = new Tpropuestahonorario();
                        $obj->cestructurapropuesta = $cestructurapropuesta;
                        $obj->cpropuestaactividades = $act->cpropuestaactividades;
                    }
                    $obj->horas =$horas[$act->cpropuestaactividades];
                    $obj->save();
                }
            }
        }        
        DB::commit();
        return "";
    }

    public function editarHorasDisciplina(Request $request){
        $cestructurapropuesta = $request->input('cestructurapropuesta');
        $cdisciplina = $request->input('cdisciplina');
        $propuesta = DB::table('tpropuesta')
        ->where('cpropuesta','=',$request->input('cpropuesta'))
        ->first();
        $lactividades = DB::table('tpropuestaactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$request->input('cpropuesta'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cpropuestaactividades'] = $act->cpropuestaactividades;
            $a['item'] ='';
            $a['cpropuesta'] = $act->cpropuesta;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactividad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cpropuestapaquete'] = $act->cpropuestapaquete;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $est = DB::table('tpropuestahonorarios as hor')
            ->where('cestructurapropuesta','=',$cestructurapropuesta)
            ->where('cpropuestaactividades','=',$act->cpropuestaactividades)
            ->where('cdisciplina','=',$cdisciplina)
            ->first();
            if($est){
                $a['hora_'.$act->cpropuestaactividades."_".$cestructurapropuesta."_".$cdisciplina] = $est->horas;
            
            }
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);

        return view('partials.tableInputHorasActividadDis',compact('listaAct','cestructurapropuesta','propuesta','cdisciplina'));
    }
    public function grabarHorasDisciplina(Request $request){
        DB::beginTransaction();
        $cestructurapropuesta = $request->input('cestructurapropuesta_h');
        $cdisciplina = $request->input('cdisciplina');
        $horas = $request->input('horas');
        //dd($horas);
        $lactividades = DB::table('tpropuestaactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$request->input('cpropuesta_hdis'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            if(isset($horas[$act->cpropuestaactividades])){
                if(!empty($horas[$act->cpropuestaactividades] && $horas[$act->cpropuestaactividades] > 0) ){
                    $obj= Tpropuestahonorario::where('cpropuestaactividades','=',$act->cpropuestaactividades)
                    ->where('cestructurapropuesta','=',$cestructurapropuesta)
                    ->where('cdisciplina','=',$cdisciplina)
                    ->first();
                    if (!$obj){
                        $obj = new Tpropuestahonorario();
                        $obj->cestructurapropuesta = $cestructurapropuesta;
                        $obj->cpropuestaactividades = $act->cpropuestaactividades;
                        $obj->cdisciplina = $cdisciplina;
                    }
                    $obj->horas =$horas[$act->cpropuestaactividades];
                    $obj->save();
                }
            }
        }        
        DB::commit();
        return "";
    }

    public function editarHorasApoyo(Request $request){
        $cestructurapropuesta = $request->input('cestructurapropuesta');
        $propuesta = DB::table('tpropuesta')
        ->where('cpropuesta','=',$request->input('cpropuesta'))
        ->first();
        $lactividades = DB::table('tpropuestaactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$request->input('cpropuesta'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            $a['cpropuestaactividades'] = $act->cpropuestaactividades;
            $a['item'] ='';
            $a['cpropuesta'] = $act->cpropuesta;
            $a['cactividad'] = $act->cactividad;
            $a['codigoactividad'] = $act->codigoactividad;
            $a['descripcionactividad'] = $act->descripcionactividad;
            $a['numerodocumentos'] = $act->numerodocumentos;
            $a['numeroplanos'] = $act->numeroplanos;
            $a['cpersona_proveedor'] = $act->cpersona_proveedor;
            $a['cpropuestapaquete'] = $act->cpropuestapaquete;
            $a['cactividad_parent'] = $act->cactividad_parent;
            $est = DB::table('tpropuestahonorarios as hor')
            ->where('cestructurapropuesta','=',$cestructurapropuesta)
            ->where('cpropuestaactividades','=',$act->cpropuestaactividades)
            ->whereNull('cdisciplina')
            ->first();
            if($est){
                $a['hora_'.$act->cpropuestaactividades."_".$cestructurapropuesta] = $est->horas;
            
            }
            $listaAct[count($listaAct)] = $a;
        }
        $objAct = new ActividadSupport();
        $listaAct = $objAct->orderParentActividadEjecucion($listaAct);

        return view('partials.tableInputHorasActividad',compact('listaAct','cestructurapropuesta','propuesta'));
    }
    public function grabarHorasApoyo(Request $request){
        DB::beginTransaction();
        $cestructurapropuesta = $request->input('cestructurapropuesta_h');
        $horas = $request->input('horas');
        //dd($horas);
        $lactividades = DB::table('tpropuestaactividades as pact')
        ->join('tactividad as act','pact.cactividad','=','act.cactividad')
        ->where('pact.cpropuesta','=',$request->input('cpropuesta_hapo'))
        ->select('pact.*','act.cactividad_parent')
        ->get();
        $listaAct = array();
        foreach($lactividades as $act){
            if(isset($horas[$act->cpropuestaactividades])){
                if(!empty($horas[$act->cpropuestaactividades] && $horas[$act->cpropuestaactividades] > 0) ){
                    $obj= Tpropuestahonorario::where('cpropuestaactividades','=',$act->cpropuestaactividades)
                    ->where('cestructurapropuesta','=',$cestructurapropuesta)
                    ->whereNull('cdisciplina')
                    ->first();
                    if (!$obj){
                        $obj = new Tpropuestahonorario();
                        $obj->cestructurapropuesta = $cestructurapropuesta;
                        $obj->cpropuestaactividades = $act->cpropuestaactividades;
                    }
                    $obj->horas =$horas[$act->cpropuestaactividades];
                    $obj->save();
                }
            }
        }        
        DB::commit();
        return "";
    }    

    public function addGastos(Request $request){
        $gastos = array();
        $totGastos=0;
        if (Session::get('listGastos')){
            $gastos = Session::get('listGastos');
        }
        $selected = $request->input('selected_gastos');
        $congastos = DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=','00003')
        ->whereIn('congas.cconcepto',$selected)
        ->get();
        foreach($congastos as $gas){
            $i= -1*(count($gastos)*3);
            $g['cpropuestaproyectogastos'] = $i;
            $g['cpropuesta'] = '';
            $g['item'] ='';
            $g['concepto'] = $gas->cconcepto;
            $g['descripcion'] = $gas->descripcion;
            $g['unidad']=$gas->unidad;
            $g['cantidad'] = '1';
            $g['costou'] =  $gas->costounitario;
            $g['subtotal'] = $g['cantidad'] * $gas->costounitario;
            $g['cconcepto_parent'] = $gas->cconcepto_parent;
            $g['comentario'] ='';
            $g['cpersona_proveedor'] ='';
            $g['cpropuestapaquete'] ='';
          
            $gastos[count($gastos)]=$g;
            /* agregar parent */

            if(!(empty($gas->cconcepto_parent))){
                $res = array_search($gas->cconcepto_parent, array_map(function($e){return $e['concepto'];},$gastos));
                if($res===FALSE){
                    $parent = DB::table('tconceptogastos as gas')
                    ->where('gas.cconcepto','=',$gas->cconcepto_parent)
                    ->first();
                    if($parent){
                        $i= -1*(count($gastos)*3);
                        $p['cpropuestaproyectogastos'] = $i;
                        $p['cpropuesta'] = '';
                        $p['item'] ='';
                        $p['concepto'] = $parent->cconcepto;
                        $p['descripcion'] = $parent->descripcion;
                        $p['unidad']=$parent->unidad;
                        $p['cantidad'] = '1';
                        $p['costou'] =  $parent->costounitario;
                        $p['subtotal'] = $p['cantidad'] * $parent->costounitario;
                        $p['cconcepto_parent'] = $parent->cconcepto_parent;
                        $p['comentario'] ='';
                        $p['cpersona_proveedor'] ='';
                        $p['cpropuestapaquete'] ='';
                        $gastos[count($gastos)]=$p;
                    }
                }
            }
            
            /* fin agregar parent */
            

        }

        $totGastos = array_sum(array_map(function($i){
            return  $i['subtotal'];
        },$gastos));
        $objGas  = new GastosSupport();
        $gastos = $objGas->orderParent($gastos);
        Session::put('listGastos',$gastos);

        //dd($congastos);
        //dd($request->input('selected_gastos'));
        return view('partials.tableGastosPropuesta',compact('gastos','totGastos'));

    }
    public function delGastos(Request $request){
        $totGastos= 0;
        $selected = $request->input('chkselec');
        $gastos = Session::get('listGastos');
        $delGastos = Session::get('delGastos');
        $tmpGastos = array();
        if (count($selected) >0){
            
            foreach($gastos as $k => $gas){
                $res = array_search($gas['cpropuestaproyectogastos'],$selected);
                if($res===FALSE){
                    $tmpGastos[count($tmpGastos)] = $gas;
                    $totGastos+= $gas['subtotal'];
                }
                foreach($selected as $v){
                    if($v > 0){
                        $delGastos[count($delGastos)] = $v;
                    }
                }
            }
        }
        $objGas  = new GastosSupport();
        $gastos = $tmpGastos;
        $gastos = $objGas->orderParent($gastos);
        Session::put('listGastos',$gastos);
        Session::put('delGastos',$delGastos);
        return view('partials.tableGastosPropuesta',compact('gastos','totGastos'));
    }

    public function addGastosLab(Request $request){
        $totGastosLabCan=0;
        $totGastosLabCim=0;
        $totGastosLabBot=0;
        $totGastosLab=0;        
        $gastosLab = array();
        if (Session::get('listGastosLab')){
            $gastosLab = Session::get('listGastosLab');
        }
        $selected = $request->input('selected_gastos_lab');
        $congastos = DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=','00002')
        ->whereIn('congas.cconcepto',$selected)
        ->get();
        foreach($congastos as $gas){
            $i= -1*(count($gastosLab)*3);
            $g['cpropuestagastoslab'] = $i;
            $g['cpropuesta'] = '';
            $g['item'] ='';
            $g['concepto'] = $gas->cconcepto;
            $g['descripcion'] = $gas->descripcion;
            $g['cmoneda']=$gas->cmoneda;
            $g['cconcepto_parent'] = $gas->cconcepto_parent;            
            $g['preciou'] =  $gas->costounitario;
            $g['cantcimentacion'] = 0;
            $g['cantcanteras'] = 0;
            $g['cantbotadero'] = 0;
            $g['subtotal'] =  $gas->costounitario * ($g['cantcimentacion'] + $g['cantcanteras'] + $g['cantbotadero']);            
            $g['cpersona_proveedor'] ='';
            $g['cpropuestapaquete'] ='';
            $gastosLab[count($gastosLab)]=$g;

            if(!(empty($gas->cconcepto_parent))){
                $res = array_search($gas->cconcepto_parent, array_map(function($e){return $e['concepto'];},$gastosLab));
                if($res===FALSE){
                    $parent = DB::table('tconceptogastos as gas')
                    ->where('gas.cconcepto','=',$gas->cconcepto_parent)
                    ->first();
                    if($parent){
                        $i= -1*(count($gastosLab)*3);
                        $p['cpropuestagastoslab'] = $i;
                        $p['cpropuesta'] = '';
                        $p['item'] ='';
                        $p['concepto'] = $parent->cconcepto;
                        $p['descripcion'] = $parent->descripcion;
                        $p['cmoneda']=$parent->cmoneda;
                        $p['cantcimentacion'] = 0;
                        $p['cantcanteras'] = 0;
                        $p['cantbotadero'] = 0;
                        $p['preciou'] =  $parent->costounitario;
                        $p['subtotal'] =  $parent->costounitario * ($p['cantcimentacion'] + $p['cantcanteras'] + $p['cantbotadero']);  
                        $p['cconcepto_parent'] = $parent->cconcepto_parent;
                        $p['cpersona_proveedor'] ='';
                        $p['cpropuestapaquete'] ='';
                        $gastosLab[count($gastosLab)]=$p;
                    }
                }
            }            

        }
        $totGastosLab = array_sum(array_map(function($i){
            return  $i['subtotal'];
        },$gastosLab));
        $totGastosLabCim = array_sum(array_map(function($i){
            return  $i['preciou']*$i['cantcimentacion'];
        },$gastosLab));
        $totGastosLabBot = array_sum(array_map(function($i){
            return  $i['preciou']*$i['cantbotadero'];
        },$gastosLab));
        $totGastosLabCan = array_sum(array_map(function($i){
            return  $i['preciou']*$i['cantcanteras'];
        },$gastosLab));                        
        $objGas  = new GastosSupport();
        $gastosLab = $objGas->orderParent($gastosLab);
        Session::put('listGastosLab',$gastosLab);

        //dd($congastos);
        //dd($request->input('selected_gastos'));
        return view('partials.tableGastosLabPropuesta',compact('gastosLab','totGastosLab','totGastosLabCim','totGastosLabBot','totGastosLabCan'));

    }
    public function delGastosLab(Request $request){
        $selected = $request->input('chkselecLab');
        $gastosLab = Session::get('listGastosLab');
        $delGastosLab = Session::get('delGastosLab');
        $delGastos = Session::get('delGastos');
        $totGastosLabCim = 0;
        $totGastosLabBot = 0;
        $totGastosLabCan = 0;
        $totGastosLab = 0;
        $tmpGastos = array();
        if (count($selected) >0){
            
            foreach($gastosLab as $k => $gas){
                $res = array_search($gas['cpropuestagastoslab'],$selected);
                if($res===FALSE){
                    $tmpGastos[count($tmpGastos)] = $gas;
                    $totGastosLabCim+=$gas['preciou'] * ($gas['cantcimentacion']);
                    $totGastosLabBot+=$gas['preciou'] * ($gas['cantbotadero']);
                    $totGastosLabCan+=$gas['preciou'] * ($gas['cantcanteras']);
                    $totGastosLab+= $gas['subtotal'];                       
                }
                foreach($selected as $v){
                    if($v > 0){
                        $delGastosLab[count($delGastos)] = $v;
                    }
                }
            }
        }
        $objGas  = new GastosSupport();
        $gastosLab = $tmpGastos;
        $gastosLab = $objGas->orderParent($gastosLab);
        Session::put('listGastosLab',$gastosLab);
        Session::put('delGastosLab',$delGastosLab);
        return view('partials.tableGastosLabPropuesta',compact('gastosLab','totGastosLab','totGastosLabCim','totGastosLabBot','totGastosLabCan'));
    }

    public function grabarDetalle(Request $request){
        //dd($request->all());
        DB::beginTransaction();
        $cpropuesta= $request->input('cpropuesta');
        $objProp = new PropuestaSupport();
        /* Grabar Horas Detalle */
            /* Grabar horas Senior */
            $objProp->saveEstructuraActividadSenior($cpropuesta,$request);
            /* Fin Grabar Hora Senior*/

            /* Grabar horas Disciplina */
            $objProp->saveEstructuraActividadDisciplina($cpropuesta,$request);
            /* Fin Grabar Hora Disciplina*/

            /* Grabar horas Apoyo */
            $objProp->saveEstructuraActividadApoyo($cpropuesta,$request);
            /* Fin Grabar Hora Apoyo*/

        /* Fin Grabar horas Detalle */
        /* Eliminar Registros Seleccionados*/
        $horas = $request->input("hora");
        foreach($horas as $ho){
           
            if(strlen($ho[0])>0){
                $obj = Tpropuestaactividade::where('cpropuestaactividades','=',$ho[0])->first();
                if($obj){
                //Comentario
                    if(strlen($ho[4])>0){
                        $obj->comentario = $ho[4];
                    }
                //Sugrenecia
                    $obj->sugerencia_descripcion = $ho[2];
                    
                    $obj->flag_sugerencia = $ho[3];

                    $obj->save();
                }
                

                //Eliminar
                if($ho[1]=='S'){
                    $obj = Tpropuestaactividade::where('cpropuestaactividades','=',$ho[0])->first();
                    if($obj){
                        $objAux=Tpropuestahonorario::where('cpropuestaactividades','=',$ho[0])->delete();
                        $obj->delete();
                    }
                }
            }

        }

        /* Fin Eliminar registros Seleccionados */

        /* Grabar Gastos */
        $igas = $request->input('gas');
        $icom = $request->input('txt');
        //dd($igas);
        if(Session::get('listGastos')){
            $gastos = Session::get('listGastos');
            //dd($gastos);
            foreach($gastos as $gas){
                if($gas['cpropuestaproyectogastos'] <= 0){
                    $tgasto = new Tpropuestagasto();
                    $tgasto->cpropuesta = $cpropuesta;
                }else{
                    $tgasto = Tpropuestagasto::where('cpropuestaproyectogastos','=',$gas['cpropuestaproyectogastos'])->first();
                }
                $tgasto->item = $gas['item'];
                $tgasto->concepto = $gas['concepto'];
                $tgasto->unidad = $gas['unidad'];
                $cantidad=0;
                if(isset($igas[$gas['cpropuestaproyectogastos']]) ){
                    $cantidad = $igas[$gas['cpropuestaproyectogastos']];
                }
                $comentario='';
                if(isset($icom[$gas['cpropuestaproyectogastos']]) ){
                    $comentario = $icom[$gas['cpropuestaproyectogastos']];
                }                
                $tgasto->cantidad = $cantidad;
                $tgasto->costou = $gas['costou'];
                $tgasto->comentario = $comentario;
                $tgasto->save();
            }
        }
        if(Session::get('delGastos')){
            $delGastos = Session::get('delGastos');
            foreach($delGastos as $k => $dg){
                //dd($dg);
                $obj = Tpropuestagasto::where('cpropuestaproyectogastos','=',$dg)->delete();
                

            }
        }
        /* Fin Grabar Gastos */

        /* Grabar Gastos Lab*/
        if(Session::get('listGastosLab')){
            $gastos = Session::get('listGastosLab');
            $gaslcimen = $request->input('gaslcimen');
            $gaslbota = $request->input('gaslbota');
            $gaslcant = $request->input('gaslcant');
            //dd($gastos);
            foreach($gastos as $gas){
                if($gas['cpropuestagastoslab'] <= 0){
                    $tgasto = new Tpropuestagastoslaboratorio();
                    $tgasto->cpropuesta = $cpropuesta;
                }else{
                    $tgasto = Tpropuestagastoslaboratorio::where('cpropuestagastoslab','=',$gas['cpropuestagastoslab'])->first();
                }
                $tgasto->item = $gas['item'];
                $tgasto->concepto = $gas['concepto'];
                if(!empty($gas['cmoneda'])){
                    $tgasto->cmoneda = $gas['cmoneda'];    
                }
                if(isset( $gaslcimen[$gas['cpropuestagastoslab']])==1){
                    $tgasto->cantcimentacion= $gaslcimen[$gas['cpropuestagastoslab']];
                }
                if(isset( $gaslbota[$gas['cpropuestagastoslab']])==1){
                    $tgasto->cantbotadero  = $gaslbota[$gas['cpropuestagastoslab']];
                }
                if(isset( $gaslcant[$gas['cpropuestagastoslab']])==1){
                    $tgasto->cantcanteras = $gaslcant[$gas['cpropuestagastoslab']];
                }
                $tgasto->preciou = $gas['preciou'];
                $tgasto->save();
                $tt=DB::table('tconceptogastos')
                ->where('cconcepto','=',$gas['concepto'])
                ->first();
                if($tt){
                    if(!is_null($tt->cconcepto_parent)){
                        $tab=DB::table('tconceptogastos')
                        ->where('cconcepto','=',$tt->cconcepto_parent)
                        ->first();                        
                        if(!$tab){
                            $tgastoAux = new Tpropuestagastoslaboratorio();
                            $tgastoAux->cpropuesta = $cpropuesta;                            
                            $tgastoAux->concepto = $tab->cconcepto;                            
                            $tgastoAux->save();
                        }
                    }
                }

            }
        }
        if(Session::get('delGastosLab')){
            $delGastosLab = Session::get('delGastosLab');
            foreach($delGastosLab as $k => $dg){
                //dd($dg);
                $obj = Tpropuestagastoslaboratorio::where('cpropuestagastoslab','=',$dg)->delete();
                

            }
        }
        /* Fin Grabar Gastos Lab*/        
        DB::commit();
        return Redirect::route('editarDetallePropuesta',$cpropuesta);
    }  

    public function Upload(Request $request){
        $cpropuesta= $request->input('cpropuesta_file');
        $file = $request->file('fileXls');//extraenombre archivo
        $extension = $file->getClientOriginalExtension();//extension
        $id = Storage::disk('archivos')->put($file->getFilename().'.'.$extension,  File::get($file));
        $ruta = storage_path('archivos') . "/" . $file->getFilename().'.'.$extension;//trae el achivo con la ruta

        $inputFileType = PHPExcel_IOFactory::identify($ruta);//obtiene archivo
        $objReader = PHPExcel_IOFactory::createReader($inputFileType);//lee las hojas del archivo
        //$worksheetNames = $objReader->getSheetNames($ruta);//obtiene nombre de hojas
        
        $tipohoja = $request->input('tipohoja');
        $nombrehoja = $request->input('nombrehoja');
        
        switch($tipohoja){
            case 'ROO':
                $objExcel = $objReader->load($ruta);
                $objWorksheet = $objExcel->getActiveSheet();

                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'

                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5            
                $this->importarDatosRooster($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request);
                break;
            case 'HH':
                try{
                    if(strlen($nombrehoja)<=0){
                        $nombrehoja="Honorarios";
                    }
                    $objReader->setLoadSheetsOnly($nombrehoja);
                    $objReader->setReadDataOnly(true);
                    $objExcel = $objReader->load($ruta);
                
                } catch(PHPExcel_Reader_Exception $e) {
                    die('Error loading file: '.$e->getMessage());
                }
                
                $objWorksheet = $objExcel->getActiveSheet();
                
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'

                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5            
                $this->importarDatosHH($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request);
                break;
            case 'GAS':
                if(strlen($nombrehoja)<=0){
                    $nombrehoja="Gastos";
                }            
                $objReader->setLoadSheetsOnly($nombrehoja);
                $objExcel = $objReader->load($ruta);
                
                $objWorksheet = $objExcel->getActiveSheet();
                
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'

                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5            
                $this->importarDatosGastos($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request);
                break;
            case 'LAB':
                if(strlen($nombrehoja)<=0){
                    $nombrehoja="Lab";
                }                        
                $objReader->setLoadSheetsOnly($nombrehoja);
                $objExcel = $objReader->load($ruta);
                
                $objWorksheet = $objExcel->getActiveSheet();
                
                $highestRow = $objWorksheet->getHighestRow(); // e.g. 10
                $highestColumn = $objWorksheet->getHighestColumn(); // e.g 'F'

                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn); // e.g. 5            
                $this->importarDatosLaboratorio($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request);
                break;                                

        }
        //dd($highestRow);
        //dd($highestColumn);
        //dd($highestColumnIndex);


       return Redirect::route('editarDetallePropuesta',$cpropuesta); 
    }

    public function importarDatosGastos($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request){    
        $hoja= array();
        $iniFilaDatos=$request->input('ga_inicioDatos'); //7;
        $finFilaDatos=$request->input('ga_finDatos'); //25;

        $colItem=$request->input('ga_colItem'); //"A";
        $colDescripcion=$request->input('ga_colDes'); //"B";
        $colUnidad=$request->input('ga_colUni'); //"C";
        $colCant =$request->input('ga_colCant'); //'D';
        $colCosto=$request->input('ga_colCos'); //"E";
        $colSubTotal=$request->input('ga_colSub'); //"F";
        $colComentarios=$request->input('ga_colCom'); //"G";


        $indexColItem=PHPExcel_Cell::columnIndexFromString($colItem)-1;
        $indexColDescripcion=PHPExcel_Cell::columnIndexFromString($colDescripcion)-1;
        $indexColUnidad=PHPExcel_Cell::columnIndexFromString($colUnidad)-1;
        $indexColCant =PHPExcel_Cell::columnIndexFromString($colCant)-1;
        $indexColCosto=PHPExcel_Cell::columnIndexFromString($colCosto)-1;
        $indexColSubTotal=PHPExcel_Cell::columnIndexFromString($colSubTotal)-1;
        $indexColComentarios=PHPExcel_Cell::columnIndexFromString($colComentarios)-1;

        for($row=1;$row <= $highestRow; ++$row){
            $linea=array();
            if($row>=$iniFilaDatos){
                $item = trim($objWorksheet->getCellByColumnAndRow($indexColItem,$row)->getCalculatedValue());
                if($item==''){
                    $leerDatos=false;
                }else{
                    $leerDatos=true;
                }
                if($leerDatos){
                    $linea['item']=$item;
                    $linea['cconcepto']='';
                    $linea['descripcion']=trim($objWorksheet->getCellByColumnAndRow($indexColDescripcion,$row)->getCalculatedValue());
                    $tcon = DB::table('tconceptogastos')->where('descripcion','like',$linea['descripcion'])
                    ->first();
                    if($tcon){
                        $linea['cconcepto']=$tcon->cconcepto;

                    }

                    $linea['unidad']=trim($objWorksheet->getCellByColumnAndRow($indexColUnidad,$row)->getCalculatedValue());
                    $linea['cantidad']=trim($objWorksheet->getCellByColumnAndRow($indexColCant,$row)->getCalculatedValue());
                    $linea['costo']=trim($objWorksheet->getCellByColumnAndRow($indexColCosto,$row)->getCalculatedValue());
                    $linea['subtotal']=trim($objWorksheet->getCellByColumnAndRow($indexColSubTotal,$row)->getCalculatedValue());
                    $linea['comentarios']=trim($objWorksheet->getCellByColumnAndRow($indexColComentarios,$row)->getCalculatedValue());
                    $hoja[count($hoja)]=$linea;
                }          
                
            }
        }
        //dd($hoja);
        //use App\Models\Tpropuestagasto;
        //use App\Models\Tpropuestagastoslaboratorio;
        DB::beginTransaction();
        foreach($hoja as $ho){
            if(strlen($ho['cconcepto'])>0){
                $obj = Tpropuestagasto::where('cpropuesta','=',$cpropuesta)
                ->where('concepto','=',$ho['cconcepto'])
                ->first();
                if(!$obj){
                    $obj = new Tpropuestagasto();
                    $obj->cpropuesta= $cpropuesta;
                    $obj->concepto=$ho['cconcepto'];

                }
                if(strlen($ho['unidad'])>0){
                    if(strlen($ho['unidad'])>5){
                        $obj->unidad=substr($ho['unidad'],0,5);
                    }else{
                        $obj->unidad=$ho['unidad'];
                    }
                }
          
                if(strlen($ho['cantidad'])>0){
                    $obj->cantidad=$ho['cantidad'];
                }
                    
                if(strlen(trim($ho['costo']))>0){
                    $obj->costou=$ho['costo'];
                }
                $obj->comentario=$ho['comentarios'];
                $obj->item=$ho['item'];
                $obj->save();

            }

        }
        DB::commit();
    }

    public function importarDatosLaboratorio($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request){
        $hoja= array();
        $iniFilaDatos=$request->input('gal_inicioDatos'); //8;
        $finFilaDatos=$request->input('gal_finDatos'); //36;

        $colItem=$request->input('gal_colItem'); //"A";
        $colDescripcion=$request->input('gal_colDes'); //"B";
        $colCantCant =$request->input('gal_colCantCant'); //'D';
        $colCantCimen =$request->input('gal_colCantCimen'); //'E';
        $colCantBot =$request->input('gal_colCantBot'); //'';        
        $colCosto=$request->input('gal_colCos'); //"C";
        $colSubTotal=$request->input('gal_colSub'); //"F";


        $indexColItem=PHPExcel_Cell::columnIndexFromString($colItem)-1;
        $indexColDescripcion=PHPExcel_Cell::columnIndexFromString($colDescripcion)-1;
        $indexColCantCimen=strlen($colCantCimen)>0?PHPExcel_Cell::columnIndexFromString($colCantCimen)-1:-1;
        $indexColCantCant =strlen($colCantCant)>0?PHPExcel_Cell::columnIndexFromString($colCantCant)-1:-1;
        $indexColCantBot = strlen($colCantBot)>0?PHPExcel_Cell::columnIndexFromString($colCantBot)-1:-1;
        $indexColCosto=PHPExcel_Cell::columnIndexFromString($colCosto)-1;
        $indexColSubTotal=PHPExcel_Cell::columnIndexFromString($colSubTotal)-1;


        for($row=1;$row <= $finFilaDatos; ++$row){
            $linea=array();
            if($row>=$iniFilaDatos){
                $item = trim($objWorksheet->getCellByColumnAndRow($indexColItem,$row)->getCalculatedValue());
                if($item==''){
                    $leerDatos=false;
                }else{
                    $leerDatos=true;
                }
                if($leerDatos){
                    $linea['item']=$item;
                    $linea['cconcepto']='';
                    $linea['cconcepto_parent']='';                    
                    $linea['descripcion']=trim($objWorksheet->getCellByColumnAndRow($indexColDescripcion,$row)->getCalculatedValue());
                    $tcon = DB::table('tconceptogastos')->where('descripcion','like',$linea['descripcion'])
                    ->first();
                    if($tcon){
                        $linea['cconcepto']=$tcon->cconcepto;
                        $linea['cconcepto_parent']=$tcon->cconcepto_parent;                        
                    }

                    $linea['cantidadCant']=($indexColCantCant>=0)?trim($objWorksheet->getCellByColumnAndRow($indexColCantCant,$row)->getCalculatedValue()):'';
                    $linea['cantidadCimen']=($indexColCantCimen>=0)?trim($objWorksheet->getCellByColumnAndRow($indexColCantCimen,$row)->getCalculatedValue()):'';
                    $linea['cantidadBot']=($indexColCantBot>=0)?trim($objWorksheet->getCellByColumnAndRow($indexColCantBot,$row)->getCalculatedValue()):'';
                    $linea['preciou']=trim($objWorksheet->getCellByColumnAndRow($indexColCosto,$row)->getCalculatedValue());
                    $linea['subtotal']=trim($objWorksheet->getCellByColumnAndRow($indexColSubTotal,$row)->getCalculatedValue());
                    $hoja[count($hoja)]=$linea;
                }          
                
            }
        }
        //dd($hoja);
        //use App\Models\Tpropuestagasto;
        //use App\Models\Tpropuestagastoslaboratorio;
        DB::beginTransaction();
        foreach($hoja as $ho){
            if(strlen($ho['cconcepto'])>0){
                $obj = Tpropuestagastoslaboratorio::where('cpropuesta','=',$cpropuesta)
                ->where('concepto','=',$ho['cconcepto'])
                ->first();
                if(!$obj){
                    $obj = new Tpropuestagastoslaboratorio();
                    $obj->cpropuesta= $cpropuesta;
                    $obj->concepto=$ho['cconcepto'];

                }
          
                if(strlen($ho['cantidadCant'])>0){
                    $obj->cantcanteras=$ho['cantidadCant'];
                }
                if(strlen($ho['cantidadCimen'])>0){
                    $obj->cantcimentacion=$ho['cantidadCimen'];
                }
                if(strlen($ho['cantidadBot'])>0){
                    $obj->cantbotadero=$ho['cantidadBot'];
                }                
                if(strlen($ho['preciou'])>0){
                    $obj->preciou=$ho['preciou'];
                }                     
                $obj->item=$ho['item'];
                $obj->save();
                if(strlen($ho['cconcepto_parent'])>0){
                    $tt = DB::table('tpropuestagastoslaboratorio')
                    ->where('cpropuesta','=',$cpropuesta)
                    ->where('concepto','=',$ho['cconcepto_parent'])
                    ->first();
                    if(!$tt){
                        $objAux = new Tpropuestagastoslaboratorio();
                        $objAux->cpropuesta= $cpropuesta;
                        $objAux->concepto=$ho['cconcepto_parent'];                        

                        $objAux->save();

                    }
                }                
            }

        }
        DB::commit();
    }

    public function importarDatosHH($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request){
        
        $hoja= array();
        $leerDatos =false;
        $inicioDatos=$request->input('ho_inicioDatos'); //9;
        $finDatos=$request->input('ho_finDatos'); //21;
        $iniSenior=$request->input('ho_inicioSenior'); //"E";
        $finSenior=$request->input('ho_finSenior'); //"K";
        $iniDis =$request->input('ho_inicioDis'); //"L";
        $finDis =$request->input('ho_finDis'); //"AY";
        $iniApoyo=$request->input('ho_inicioApoyo'); //"AZ";
        $finApoyo=$request->input('ho_finApoyo'); //"BA";
        //indices
        $indexIniSenior=PHPExcel_Cell::columnIndexFromString($iniSenior)-1;
        $indexFinSenior=PHPExcel_Cell::columnIndexFromString($finSenior)-1;
        $indexIniDis =PHPExcel_Cell::columnIndexFromString($iniDis)-1;
        $indexFinDis =PHPExcel_Cell::columnIndexFromString($finDis)-1;
        $indexIniApoyo=PHPExcel_Cell::columnIndexFromString($iniApoyo)-1;
        $indexFinApoyo=PHPExcel_Cell::columnIndexFromString($finApoyo)-1;

        $xls = new ExcelSupport();
        $seniors=array();
        $disciplinas=array();
        $apoyos=array();

        $paqpropuesta=$request->input('paquete');


        $rolDis='';
        $cdisDis='';
        $rateDis='';
        for($col=1;$col<=$highestColumnIndex; ++$col){
            if($col>=$indexIniSenior && $col<=$indexFinSenior){
                //F Identificar persona del Rol Senior
                $s=array();
                $s['croldespliegue'] = '';
                $s['descripcion'] = trim($objWorksheet->getCellByColumnAndRow($col,6)->getCalculatedValue());
                $s['descripcion'] = $xls->normaliza($s['descripcion']);
                $s['cpersona'] = '';
                $s['nombre'] = trim($objWorksheet->getCellByColumnAndRow($col,8)->getCalculatedValue());


                $s['rate'] = trim($objWorksheet->getCellByColumnAndRow($col,7)->getCalculatedValue());
                $s['cestructurapropuesta']='';
                $trol= Db::table('troldespliegue')
                ->where(DB::raw('lower(descripcionrol)'),'like','%'.$s['descripcion'].'%')
                ->where('tiporol','=','1')
                ->first();
                if($trol){
                    $s['croldespliegue'] = $trol->croldespliegue;
                }
                
                $seniors['s_'.$col]=$s;
            }
            if($col>=$indexIniDis && $col<=$indexFinDis){
                //Identificar Roles de Disiciplinas
                $d=array();
                $d['croldespliegue'] = '';
                $d['deta']='S'; // Si es S es detalle por disciplina si es N es total por la categhoria
                $d['descripcion'] = trim($objWorksheet->getCellByColumnAndRow($col,6)->getCalculatedValue());
                $d['descripcion'] = $xls->normaliza($d['descripcion']);
                $d['cdisciplina'] = '';
                $d['des_disciplina'] = trim($objWorksheet->getCellByColumnAndRow($col,8)->getCalculatedValue());
                $d['rate'] = trim($objWorksheet->getCellByColumnAndRow($col,7)->getCalculatedValue());
                $d['cestructurapropuesta']='';
                $d['cpropuestadisciplina']='';
                if(strlen($d['rate'])>0){
                    $rateDis=$d['rate'];
                }else{
                    if(strlen($rateDis)>0){
                        $d['rate']=$rateDis;
                    }
                }
                if(strlen(trim($d['descripcion']))>1){
                    $trol= Db::table('troldespliegue')
                    ->where(DB::raw('levenshtein(lower(descripcionrol),\''.$d['descripcion'].'\')'),'<','1')
                    ->where('tiporol','=','1')
                    ->first();
                    if($trol){
                        $d['croldespliegue'] = $trol->croldespliegue;
                        $rolDis = $trol->croldespliegue;
                        $d['deta']='N';
                    }                
                }else{
                    if(strlen($d['croldespliegue'])<=0){
                        $d['croldespliegue'] = $rolDis;
                    }
                }
                if(strpos($d['des_disciplina'],"/")===FALSE){
                    $tdis=DB::table('tdisciplina')
                    // ->where(DB::raw('trim(lower(descripcion))'),'=',trim(strtolower($d['des_disciplina'])))
                    ->where(DB::raw('trim(lower(descripcion))'),'like','%'.trim(strtolower($d['des_disciplina'])).'%')
                    ->first();
                   // dd($tdis,$d['des_disciplina'],strpos($d['des_disciplina'],"/"));
                    if($tdis){
                        $d['cdisciplina'] = $tdis->cdisciplina;
                    }
                }
                $disciplinas['d_'.$col]=$d;
            }      
            if($col>=$indexIniApoyo && $col<=$indexFinApoyo){
                //IDentificar Roles de Apoyo
                $a=array();
                $a['croldespliegue'] = '';
                $a['descripcion'] = trim($objWorksheet->getCellByColumnAndRow($col,6)->getCalculatedValue());
                $a['descripcion'] = $xls->normaliza($a['descripcion']);
                $a['rate'] = trim($objWorksheet->getCellByColumnAndRow($col,7)->getCalculatedValue()); 
                $a['cestructurapropuesta']='';
                if(strlen($a['descripcion'])>0){
                    $trol= Db::table('troldespliegue')
                    ->where(DB::raw('levenshtein(lower(descripcionrol),\''.$a['descripcion'].'\')'),'<=','5')
                    ->where('tiporol','=','1')
                    ->first();
                    if($trol){
                        $a['croldespliegue'] = $trol->croldespliegue;
                    }                       
                }    
                $apoyos['a_'.$col]=$a;
                
            }                    
        }         
        //dd($seniors,$disciplinas,$apoyos);
        $objAct = new ActividadSupport();
        for($row=1;$row <= $highestRow; ++$row){
            $linea = array();

            if($row>=$inicioDatos && $row<=$finDatos){
                $item = trim($objWorksheet->getCellByColumnAndRow(0,$row)->getFormattedValue());
                if($item==''){
                    $leerDatos=true;
                }else{
                    $leerDatos=false;
                }
                if(!$leerDatos){
                    $linea['cpropuestaactividades']='';
                    $linea['actividad'] = trim($objWorksheet->getCellByColumnAndRow(1,$row)->getCalculatedValue());
                    if(strlen(trim($item))<=1){
                        $item.=".00";
                    }
                    $linea['item'] = $item;
                    $linea['cactividad'] = '';
                    //$linea['cpropuestaactividades_parent'] = '';
                    $linea['estructura']=explode('.',$item);
                    $linea['isparent']=$objAct->isPatern($linea['estructura']);
                    $linea['nivel']=$objAct->getNivel($linea['estructura']);
                    /*$act = DB::table('tactividad as act')
                    //->where('descripcion','like','%'.$linea['actividad'].'%')
                    ->where(DB::raw('levenshtein(lower(descripcion),\''.$linea['actividad'].'\')'),'<=','5')
                    ->first();
                    if($act){
                        $linea['cactividad'] = $act->cactividad;
                    }*/
                    $docs='';
                    $planos='';
                    $linea['docs'] = '';
                    $linea['planos'] = '';
                    $docs = trim($objWorksheet->getCellByColumnAndRow(3,$row)->getCalculatedValue());
                    $planos = trim($objWorksheet->getCellByColumnAndRow(4,$row)->getCalculatedValue());
                    if($docs!=''){
                        $linea['docs'] = $docs;
                    }
                    if($planos!=''){
                        $linea['planos'] = $planos;
                    }
                    for($col=1;$col<=$highestColumnIndex; ++$col){
                        $dato=trim($objWorksheet->getCellByColumnAndRow($col,$row)->getCalculatedValue());
                        
                        //
                        if($col>=$indexIniSenior && $col<=$indexFinSenior){
                            $linea['s_'.$col]=(strlen($dato)>0?$dato:'');
                            
                        }

                        if($col>=$indexIniDis && $col<=$indexFinDis){
                            $linea['d_'.$col]=(strlen($dato)>0?$dato:'');
                        }

                        if($col>=$indexIniApoyo && $col<=$indexFinApoyo){
                            $linea['a_'.$col]=(strlen($dato)>0?$dato:'');
                        }
                        
                    }                                        
                    $hoja[count($hoja)]=$linea;                
                }
            }

            

        }        
        //dd($hoja);//,$seniors,$disciplinas,$apoyos);

        //Guardar los cambios de la Hoja
        DB::beginTransaction();

        //dd($seniors);
        //Save Estrucutura Seniors
        foreach($seniors as $k => $se){
            if(strlen($se['croldespliegue'])>0){
                $obj=Testructuraparticipante::where('cpropuesta','=',$cpropuesta)
                ->where('croldespliegue','=',$se['croldespliegue'])
                ->first();
                if(!$obj){
                    $obj = new Testructuraparticipante();
                    $obj->cpropuesta= $cpropuesta;
                    $obj->croldespliegue = $se['croldespliegue'];
                }
                if(strlen($se['cpersona'])>0){
                    $obj->cpersona_rol= $se['cpersona'];
                }
                $obj->tipoestructura='1';
                $obj->rate= $se['rate'];
                $obj->save();
                $se['cestructurapropuesta'] = $obj->cestructurapropuesta;
                
                $seniors[$k]=$se;
            }


        }
        //dd($seniors);
        // Fin Save Estructura Seniors
        //Save Estrucutura Disicplinas
        foreach($disciplinas as $k => $di){
            if(strlen($di['croldespliegue'])>0){
                $obj=Testructuraparticipante::where('cpropuesta','=',$cpropuesta)
                ->where('croldespliegue','=',$di['croldespliegue'])
                ->first();
                if(!$obj){
                    $obj = new Testructuraparticipante();
                    $obj->cpropuesta= $cpropuesta;
                    $obj->croldespliegue = $di['croldespliegue'];
                }

                $obj->tipoestructura='2';
                $obj->rate= $di['rate'];
                $obj->save();
                $di['cestructurapropuesta'] = $obj->cestructurapropuesta;
                
                if(strlen($di['cdisciplina'])>0){
                    $objD = Tpropuestadisciplina::where('cpropuesta','=',$cpropuesta)
                    ->where('cdisciplina','=',$di['cdisciplina'])
                    ->first();
                    if($objD){
                        $di['cpropuestadisciplina'] =$objD->cpropuestadisciplina ;

                    }
                }
                $disciplinas[$k]=$di;
            }

        }
        //dd($disciplinas);
        // Fin Save Estructura Disicplinas

        //dd($apoyos);
        //Save Estrucutura Apoyos
        foreach($apoyos as $k => $ap){
            if(strlen($ap['croldespliegue'])>0){
                $obj=Testructuraparticipante::where('cpropuesta','=',$cpropuesta)
                ->where('croldespliegue','=',$ap['croldespliegue'])
                ->first();
                if(!$obj){
                    $obj = new Testructuraparticipante();
                    $obj->cpropuesta= $cpropuesta;
                    $obj->croldespliegue = $ap['croldespliegue'];
                }

                $obj->tipoestructura='3';
                $obj->rate= $ap['rate'];
                $obj->save();
                $ap['cestructurapropuesta'] = $obj->cestructurapropuesta;
                
                $apoyos[$k]=$ap;
            }


        }
        
        // Fin Save Estructura Apoyos

        
        foreach($hoja as $k => $ho){
            //Save tpropuestaactividades
            if(strlen($ho['actividad'])>0){
                $obj=null;
                if(strlen($ho['cpropuestaactividades'])>0){
                    $obj = Tpropuestaactividade::where('cpropuesta','=',$cpropuesta)
                    ->where('cpropuestaactividades','=',$ho['cpropuestaactividades'])
                    ->first();
                }
                if(!$obj){
                    $obj = new Tpropuestaactividade();
                    $obj->cpropuesta = $cpropuesta;
                    //$obj->cactividad = $ho['cactividad'];

                }
                $obj->descripcionactividad= $ho['actividad'];                
                $obj->cpropuestapaquete=$paqpropuesta;
                $obj->codigoactividad=$ho['item'];
                if(!$ho['isparent']){
                    $niv = $ho['nivel']-1;
                    $bus = "";//$ho['estructura'][$niv-1];
                    for($r=0;$r < $niv;$r++){
                        if(strlen(trim($bus))>0){
                            $bus .=".";
                        }
                        if($r==0){
                            $bus.=$ho['estructura'][$r];
                        }else{
                            $bus.=str_pad($ho['estructura'][$r],2,'0',STR_PAD_LEFT);
                        }

                    }
                    $res = array_search($bus,array_map(function ($e){
                        return $e['item'];
                    },$hoja));
                    if($res===FALSE){
                    }else{
                        if(isset($hoja[$res]['cpropuestaactividades'])==1 && strlen($hoja[$res]['cpropuestaactividades'])>0){
                            $obj->cpropuestaactividades_parent = $hoja[$res]['cpropuestaactividades'];
                        }
                    }
                }
                /*$tact = DB::table('tactividad')
                ->where('cactividad','=',$ho['cactividad'])
                ->first();
                if($tact){
                    $obj->descripcionactividad=$tact->descripcion;
                    if(!is_null($tact->cactividad_parent)){
                        $objAux = Tpropuestaactividade::where('cpropuesta','=',$cpropuesta)
                        ->where('cactividad','=',$tact->cactividad_parent)
                        ->first();                        
                        if(!$objAux){
                            $objAux = new Tpropuestaactividade();
                            $objAux->cpropuesta = $cpropuesta;
                            $objAux->cactividad = $tact->cactividad_parent;                            
                            $tt = DB::table('tactividad')
                            ->where('cactividad','=',$tact->cactividad_parent)
                            ->first();
                            if($tt){
                                $objAux->descripcionactividad=$tt->descripcion;
                            }
                            $objAux->save();
                        }
                    }
                }else{
                    $obj->descripcionactividad=$ho['descripcion'];
                }*/
                if(strlen($ho['docs'])>0){
                    $obj->numerodocumentos = $ho['docs'];
                }
                if(strlen($ho['planos'])>0){
                    $obj->numeroplanos = $ho['planos'];
                }
                
                $obj->save();
                $ho['cpropuestaactividades']= $obj->cpropuestaactividades;
                $hoja[$k]=$ho;
            }
            //Fin Save tpropuestaactividades

            //save tpropuestahonorarios
            if(strlen($ho['cpropuestaactividades'])>0){
                for($col=1;$col<=$highestColumnIndex; ++$col){
                    
                    //
                    if($col>=$indexIniSenior && $col<=$indexFinSenior){
                        if(strlen($ho['cpropuestaactividades'])>0 && strlen($seniors['s_'.$col]['cestructurapropuesta'])>0){
                            if(strlen($ho['s_'.$col])>0){
                                $objS = Tpropuestahonorario::where('cpropuestaactividades','=',$ho['cpropuestaactividades'])
                                ->where('cestructurapropuesta','=',$seniors['s_'.$col]['cestructurapropuesta'])
                                ->first();
                                if(!$objS){
                                    $objS= new Tpropuestahonorario();
                                    $objS->cpropuestaactividades=$ho['cpropuestaactividades'];
                                    $objS->cestructurapropuesta= $seniors['s_'.$col]['cestructurapropuesta'];
                                    
                                }
                                $objS->horas=$ho['s_'.$col];
                                $objS->save();
                            }
                        }
                        
                    }

                    if($col>=$indexIniDis && $col<=$indexFinDis){
                        if(strlen($ho['cpropuestaactividades'])>0 && strlen($disciplinas['d_'.$col]['cestructurapropuesta'])>0){
                            if(strlen($ho['d_'.$col])>0){
                                if($disciplinas['d_'.$col]['deta']=='S'){
                                    $objD = Tpropuestahonorario::where('cpropuestaactividades','=',$ho['cpropuestaactividades'])
                                    ->where('cestructurapropuesta','=',$disciplinas['d_'.$col]['cestructurapropuesta'])
                                    ->first();
                                    if(!$objD){
                                        $objD= new Tpropuestahonorario();
                                        $objD->cpropuestaactividades=$ho['cpropuestaactividades'];
                                        $objD->cestructurapropuesta= $disciplinas['d_'.$col]['cestructurapropuesta'];
                                        
                                    }
                                    if(strlen($disciplinas['d_'.$col]['cdisciplina'])>0){
                                        $objD->cdisciplina = $disciplinas['d_'.$col]['cdisciplina'];

                                    }
                                    $objD->horas=$ho['d_'.$col];
                                    $objD->save();                        
                                }
                            }
                        }
                    }

                    if($col>=$indexIniApoyo && $col<=$indexFinApoyo){
                        if(strlen($ho['cpropuestaactividades']) > 0 && strlen($apoyos['a_'.$col]['cestructurapropuesta'])>0){
                            if(strlen($ho['a_'.$col])>0){
                                $objA = Tpropuestahonorario::where('cpropuestaactividades','=',$ho['cpropuestaactividades'])
                                ->where('cestructurapropuesta','=',$apoyos['a_'.$col]['cestructurapropuesta'])
                                ->first();
                                if(!$objA){
                                    $objA= new Tpropuestahonorario();
                                    $objA->cpropuestaactividades=$ho['cpropuestaactividades'];
                                    $objA->cestructurapropuesta= $apoyos['a_'.$col]['cestructurapropuesta'];
                                    
                                }
                                $objA->horas=$ho['a_'.$col];
                                $objA->save();           
                            }             
                        }
                    }
                    
                }                  
            }
            

        }
        

        Db::commit();

        //Fin Guardar los cambios de la Hoja
    }

    public function importarDatosRooster($cpropuesta,$highestRow,$objWorksheet,$highestColumnIndex,$request){
        /*Tpropuestacronogramaconstruccion;
        Tpropuestaconstrucciondetafecha*/
        
        $hoja= array();
        for($row=1;$row <= $highestRow; ++$row){
            //obtener Participantes E6
            $dato = $objWorksheet->getCellByColumnAndRow(4,$row)->getValue();
            if(!is_null($dato) && $row>=6){
                
                $aPersona =  explode(' ',$dato);
                $tper = DB::table('tpersona as per');
                //dd($aPersona);
                foreach($aPersona as $p){
                    $tper=$tper->where('nombre','like','%'.$p.'%');
                }

                $tper=$tper->first();
                $r['cpersona'] = 0;
                if($tper){
                    $r['cpersona'] = $tper->cpersona;
                    $r['dias'] = array();
                    for($col=1;$col<=$highestColumnIndex; ++$col){
                        //200
                        if($col>=11 && $col<=200){
                            $cod = $objWorksheet->getCellByColumnAndRow($col,$row)->getValue();
                            $fecha='';
                            $horas ='';
                            $dias=0;
                            if(!is_null($cod)){
                                $fecha = $objWorksheet->getCellByColumnAndRow($col,4)->getFormattedValue();
                                $fecha = '20'.substr($fecha,6,2).'-'.substr($fecha,3,2).'-'.substr($fecha,0,2);
                                $horas = 8;
                                
                            }else{
                                $cod='';
                            }
                            /*$fecha = Carbon::createFromDate(1900,1,1);*/
                            $d['fecha']= $fecha;
                            $d['horas'] = $horas;
                            $d['cod'] = $cod;
                            $r['dias'][count($r['dias'])] = $d;
                        }
                        //$hoja[$row][$col]= $objWorksheet->getCellByColumnAndRow($col,$row)->getValue();
                        
                    }                    
                    $hoja[count($hoja)]=$r;
                }
                
            }
            //
            //for($col=1;$col<=$highestColumnIndex; ++$col){
                //$hoja[$row][$col]= $objWorksheet->getCellByColumnAndRow($col,$row)->getValue();

            //}
            
        }
        //dd($hoja);
        DB::beginTransaction();
        /*$obj = Tpropuestacronogramaconstruccion::where('cpropuesta','=',$cpropuesta)->first();
        $objDeta = Tpropuestaconstrucciondetafecha::where()*/
        foreach($hoja as $h){
            $obj = new Tpropuestacronogramaconstruccion();
            $obj->cpropuesta=$cpropuesta;
            $obj->cpersonaempleado = $h['cpersona'];
            $obj->save();
            /*foreach($h['dias'] as $dd){
                $objD = new Tpropuestaconstrucciondetafecha();
                $objD->tcronogramaconstruccionid=$obj->tcronogramaconstruccionid;

                $objD->save();
            }*/

        }
        DB::commit();
    }  

    public function revision(){

    	return view('propuesta/generacionPropuesta');
    } 

    public function editarRevision(Request $request,$idPropuesta){
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$idPropuesta)->get();
        $propuesta = $propuesta[0];
        $persona  = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)
        ->where('cunidadminera','=',$propuesta->cunidadminera)->first();
        $servicio = DB::table('tservicio')->lists('descripcion','cservicio');
        $formacotiza = DB::table('tformacotizacion')->get();
        $estadopropuesta = Db::table('testadopropuesta')->lists('descripcion','cestadopropuesta');
        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->where('tnat.esempleado','=','1')
        ->lists('tpersona.nombre','tpersona.cpersona'); 
        $tipopropresentacion = DB::table('ttipospropuestapresentacion')->get();
        //pendiente devolver los valores de tpropuestapresentacion
        $monedas = DB::table('tmonedas')->get();
        $medioentrega = DB::table('tmedioentrega')->get();
        $propresentacion = DB::table('tpropuestapresentacion')->where('cpropuesta','=',$propuesta->cpropuesta)->get();
        $revision=$propuesta->revision; // se utiliza en la vista si no existe propuesta->revision 
        $objProSup = new PropuestaSupport();

        $revisiones = $objProSup->getRevisiones($propuesta->cpropuesta);
        $editar=true;


        return view('propuesta/generacionPropuesta',compact('propuesta','persona','editar','uminera','servicio','formacotiza','estadopropuesta','personal','tipopropresentacion','monedas','medioentrega','revision','propresentacion','revisiones'));
    }  

    public function grabarRevision(Request $request){
        DB::beginTransaction();
        $cpropuesta= $request->input('cpropuesta');
        $propuesta = Tpropuestum::where('cpropuesta','=',$cpropuesta)->first();

        /* Generar Nueva Revision*/
        $objProSup = new PropuestaSupport();
        $rev = $objProSup->generarRevision($propuesta->cpropuesta);
        $propuesta->revision =$rev;
        $propresentacion = Tpropuestapresentacion::where('cpropuesta','=',$cpropuesta)->delete();   
        



        /* Fin Asignación de Campos ----
        */
        $propuesta->save();

        // Array presentacion de Propuesta;
        $presentacion = Input::get('presentacion');
        $i=0;
        foreach($presentacion as $p){
            $i++;
            DB::table('tpropuestapresentacion')->insert(
                ['cpropuesta' => $propuesta->cpropuesta, 'cpresentacion' => $p,'item' => $i]
            );
        }

        DB::commit();
        return Redirect::route('editarRevisionPropuesta',$cpropuesta);
    }

    public function importarppavista(){

        $propuestas = DB::table('tpropuesta')->orderBy('ccodigo','ASC')->get();

        return view('propuesta.importarppa',compact('propuestas'));

    }

    public function verimportarppavista(Request $request){

        $cpropuesta = $request->cpropuesta;
        $ppa = [];
        $propuesta = DB::table('tpropuesta')->where('cpropuesta','=',$cpropuesta)->first();
        $cliente = DB::table('tpersona')->where('cpersona','=',$propuesta->cpersona)->first();
        $uminera = DB::table('tunidadminera')->where('cpersona','=',$propuesta->cpersona)->where('cunidadminera','=',$propuesta->cunidadminera)->first();

        array_push($ppa,[$cliente,$uminera]);

        return $ppa;
        
    }

    public function leyendappa(){
        $roldespliegue = DB::table('troldespliegue')->orderBy('orden','DESC')->whereNotNull('abreviatura')->get();
        return $roldespliegue;
    }

    public function leyendasig(){
        $areas = DB::table('tdisciplinaareas as tda')
        ->leftjoin('tareas as ta','ta.carea','=','tda.carea')
        ->orderBy('ta.codigo_sig','DESC')->get();
         return $areas;
     }

    public function importarppa(Request $request)
    {

        $hojaseleccionada = $request->nombrehoja;
        $datos = $request->datos;
        $separador = $request->separador;
        $cpropuesta = $request->cpropuesta;

        $archivo = $request->file('archivo');
        $nombre_original=$archivo->getClientOriginalName();
        $extension=$archivo->getClientOriginalExtension();
        $r1=Storage::disk('archivos')->put($nombre_original,  \File::get($archivo) );
        $ruta  =  storage_path('archivos') ."/". $nombre_original;
        // if($r1){

        $objExcel = Excel::load($ruta);
        $hojas = $objExcel->getSheetNames(); //Obtener los nombres de las hojas que exiten el excel

        if ($datos == 'sindatos') {
            
            if ($hojaseleccionada == '0') {
                return ['hojas',$hojas];
            }else if (isset($hojaseleccionada) ) {
                $hoja_activa = $objExcel->setActiveSheetIndexByName($hojaseleccionada);//Activar hoja por nombre
                $title = $hoja_activa->getTitle();//Obtener el nombre de la hoaa activa
                $highestRow = $hoja_activa->getHighestRow(); // Cantidad de filas
                $highestColumn = $hoja_activa->getHighestColumn(); // Cantidad de columnas en letras
                $dimensionsheet = $hoja_activa->calculateWorksheetDimension(); // Dimensión de la matriz
                $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);// Cantidad de columnas

                $fila = [];
                $columna = [];

                for ($row = 1; $row <= $highestRow; $row++) 
                { 
                    for($col = 0; $col < $highestColumnIndex; $col++) 
                    { 
                        $cell = $hoja_activa->getCellByColumnAndRow($col, $row); //posicion de las celda
                        $val = $cell->getCalculatedValue();//obtener el valor de la celda

                        if ($val == '' || $val == null) {
                            $val = 0;
                            array_push($fila, $val);
                         } else {
                            array_push($fila, $val);
                         } 
                    } 
                }

            $valores = array_chunk($fila, $highestColumnIndex);//particionar el arrary en N partes

            return ['matriz',$hojas,$hojaseleccionada,$valores];
            }
            else {
                return ['hojas',$hojas];
            }

        }

        if ($datos == 'condatos') {
                
            $cabecera = $request->cab;
            $detalles = array_chunk($request->det, count($request->cab));//particionar el arrary en N partes

             //eliminando tablas
            $ppa_act = Tpropuestaactividade::where('cpropuesta','=',$cpropuesta)->get();

            foreach ($ppa_act as $key => $value) {
            $ppa_act = Tpropuestaactividade::where('cpropuesta','=',$value->cpropuesta)->update(['cpropuestaactividades_parent'=>null]);

            $ppa_hon = Tpropuestahonorario::where('cpropuestaactividades','=',$value->cpropuestaactividades)->delete();

            }
            $ppa_act = Tpropuestaactividade::where('cpropuesta','=',$cpropuesta)->delete();
            $ppa_estpar = Testructuraparticipante::where('cpropuesta','=',$cpropuesta)->delete();

            //fin de la eliminación
            foreach ($cabecera as $key => $cab) {
                $pos = strpos($cab, $separador);
                // dd($pos);
                if ($pos === false) {
                    $codigo_sig = '';
                    $cat = '';
                    $rate = '';
                }else {
                    $cabe = explode('_', $cab);
                    $codigo_sig = $cabe[0];
                    $cat = $cabe[1];
                    $rate = $cabe[2];
                }
                    

                    $rol = DB::table('troldespliegue')->where('abreviatura','=',$cat)->first();
                    $area = DB::table('tareas')->where('codigo_sig','=',$codigo_sig)->first();

                    if ($area) {
                        $dis = DB::table('tdisciplinaareas')->where('carea','=',$area->carea)->first();
                        
                // tpropuestadisciplina
                        $propdis = Tpropuestadisciplina::where('cpropuesta','=',$cpropuesta)->where('cdisciplina','=',$dis->cdisciplina)->first();

                    // dd($ppa_dis);
                        if (!$propdis) {
                        //nuevo
                        $propdis = new Tpropuestadisciplina;
                        }
                        $propdis->cpropuesta = $cpropuesta;
                        $propdis->cdisciplina = $dis->cdisciplina;
                        $propdis->estado = 1;
                        $propdis->save();
                        
                    }
                // testructuraparticipante
                    if ($rol) {
                        $despl = Testructuraparticipante::where('cpropuesta','=',$cpropuesta)->where('croldespliegue','=',$rol->croldespliegue)->where('rate','=', $rate)->where('cdisciplina','=',$dis->cdisciplina)->first();

                        if (!$despl) {
                        //nuevo
                        $despl = new Testructuraparticipante;
                        }
                        $despl->cpropuesta = $cpropuesta;
                        $despl->croldespliegue = $rol->croldespliegue;
                        $despl->tipoestructura = null;
                        $despl->rate = $rate;
                        $despl->cdisciplina = $dis->cdisciplina; 
                        $despl->save();
                    }
            }
                   $sumatotal = 0;
                    foreach ($detalles as $d => $det) {
                        // dd($det);
                        $cod = explode('.', $det[0]);
                        
                // tpropuestaactividades
                        $act = new Tpropuestaactividade;
                        $act->cpropuesta =$cpropuesta;
                        $act->codigoactividad = $det[0];
                        $act->descripcionactividad = $det[1];
                        $act->numerodocumentos = $det[2];
                        $act->numeroplanos = $det[3];

                        if ($cod[1] === '00') {
                            $act->cpropuestaactividades_parent = null;
                        }else {

                            if (count($cod) == 2) {
                                $padre = substr($det[0], 0, -3).'.00';
                            }
                            else {
                                $padre = substr($det[0], 0, -3);
                            }

                            $act_padre = DB::table('tpropuestaactividades')
                            ->where('codigoactividad','=',$padre)
                            ->where('cpropuesta','=',$cpropuesta)->first();

                            $act->cpropuestaactividades_parent = $act_padre->cpropuestaactividades;
                        }
                       
                        $act->estado = '1';
                        $act->save();

                // tpropuestahonorarios
                        
                        foreach ($cabecera as $key => $cab) {
                            $pos = strpos($cab, $separador);
                            // dd($pos);
                            if ($pos === false) {
                                $codigo_sig = '';
                                $cat = '';
                            }else {
                                $cabe = explode('_', $cab);
                                $codigo_sig = $cabe[0];
                                $cat = $cabe[1];
                            }

                            $area = DB::table('tareas')->where('codigo_sig','=',$codigo_sig)->first();
                            $rol = DB::table('troldespliegue')->where('abreviatura','=',$cat)->first();

                            if ($area) {
                                $dis = DB::table('tdisciplinaareas')->where('carea','=',$area->carea)->first();
                                $despl = Testructuraparticipante::where('cpropuesta','=',$cpropuesta)->where('croldespliegue','=',$rol->croldespliegue)->where('cdisciplina','=',$dis->cdisciplina)->first();
                                if ($det[$key]!=0) {
                                    
                                    $hrs = new Tpropuestahonorario;
                                    $hrs->cpropuestaactividades = $act->cpropuestaactividades;
                                    $hrs->cestructurapropuesta = $despl->cestructurapropuesta;
                                    $hrs->horas = $det[$key];
                                    $hrs->cdisciplina = $dis->cdisciplina;
                                    $hrs->save();
                                
                                }
                                    $sumatotal+=floatval($det[$key]);

                            }

                        }

                    }

                return ['cargado',$hojas,$hojaseleccionada,$sumatotal];
            

        }

                    // return view("mensajes.msj_correcto")->with("msj"," Usuarios Cargados Correctamente");

        // }
        // else
        // {
        //     return view("mensajes.msj_rechazado")->with("msj","Error al subir el archivo");
        // }

    }

    public function actividades_ppa_proy(Request $request) {

        $cpropuesta = $request->cpropuesta;

        $cproyecto = Tpropuestum::where('cpropuesta','=',$cpropuesta)->first();

        $act_ppa = DB::table('tpropuestaactividades')
        ->where('cpropuesta','=',$cpropuesta)
        ->orderBy(DB::raw("to_number(codigoactividad,'999.99999999')"),'ASC')
        ->get();

        foreach ($act_ppa as $key => $value) {
            $padre = DB::table('tpropuestaactividades')->where('cpropuestaactividades_parent','=',$value->cpropuestaactividades)->first();
            $value->padre = $padre? 'true' : 'false';
        }

       
        $act_pry = DB::table('tproyectoactividades')->where('cproyecto','=',$cproyecto->cproyecto)->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')->get();

        foreach ($act_pry as $key => $value) {
            $padre = DB::table('tproyectoactividades')->where('cproyectoactividades_parent','=',$value->cproyectoactividades)->first();
            $value->padre = $padre? 'disabled' : '';
        }

        return view('propuesta.modalasociaractividades',compact('act_ppa','act_pry','cproyecto','cpropuesta'));
    }

}
