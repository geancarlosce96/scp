<?php

namespace App\Http\Controllers\Propuesta;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Tpersona;
use App\Models\Tpersonajuridicainformacionbasica;
use App\Models\Tpersonadireccione;
use App\Models\Tpai;
use App\Models\Tubigeo;
use App\Models\Tunidadmineraestado;
use App\Models\Tunidadminera;
use App\Models\Tunidadmineracontacto;
use App\Models\Ttiposcontacto;
use App\Models\Tcontactocargo;
use App\Models\Invitacion;
use App\Models\Preliminar;
use App\Lib\EmailSend;
use Auth;
use DB;
use Carbon\Carbon;
use Redirect;

class InvitacionController extends Controller
{
	public function invitacion(){
		// dd("InvitacionController.php");

		
		// $cli["clientes"] = $cliente;
		$cliente = $this->verCliente();
		$tsedes = $this->sedes();
		$codigopreliminar = $this->existepreliminar();
		$codigopropuesta = $this->existepropuesta();
		$portafolio = $this->portafolio();

		$listainvitaciones = DB::table('invitacion as in')
		->leftjoin('tunidadminera as tum','tum.cunidadminera','=','in.unidadminera_id')
		->leftjoin('tpersona as cli','cli.cpersona','=','tum.cpersona')
		->select('in.*','tum.nombre as uminera','cli.nombre')
		->get();

		$tpais = Tpai::lists('descripcion','cpais')->all();
        $tpais = [''=>''] + $tpais;

        $tpais_um = Tpai::lists('descripcion','cpais')->all();
        $tpais_um = [''=>''] + $tpais;

        $umineraestado = Tunidadmineraestado::lists('descripcion','cumineraestado')
        ->all();
        // $umineraestado = [''=>''] + $umineraestado;

        /* Unidad Minera Contactos */
        $tcargo= DB::table('tcontactocargo')
        ->lists('descripcion','ccontactocargo');
        $tcargo = [''=>''] + $tcargo;

        $ttipo= DB::table('ttiposcontacto')
        ->lists('descripcion','ctipocontacto');
        $ttipo = [''=>''] + $ttipo;

        $aniocod= Carbon::now();
        $aniocod = $aniocod->year;
        $aniocod=substr($aniocod, 2,2);

		// dd($listainvitaciones);
		$vista = 'invitacion';

		if ($codigopreliminar=='') {
		$cod = 'text';
		}else {
		$cod = 'hidden';
		}


		return view('propuesta.v2.invitacion', compact('cliente','listainvitaciones','vista','tpais','tpais_um','umineraestado','tcargo','ttipo','tsedes','aniocod','codigopreliminar','cod','portafolio','codigopropuesta'));
	}

	private function existepreliminar(){

		$cant='';
		$anio = date('y');
		
		$codigo = DB::table('preliminar')
		->orderBy('codigo_preliminar','desc')
		->first();

		// dd($codigo);
		if ($codigo) {
		$cod = explode('.', $codigo->codigo_preliminar);

		if ($cod[0]==$anio) {
			// dd($anio);
			$cant= $codigo->codigo_preliminar;
			$corre = explode('.', $cant);
			$newcod = str_pad($corre[1]+1,2,'0',STR_PAD_LEFT);
			$codigonuevo = $newcod;
		}
		else {
		// dd($cod[0],$anio);
			$codigonuevo = $cant;	
		}

		return $codigonuevo;
		}
	}

	private function existepropuesta(){

		$cant='';
		$anio = date('y');
		
		$codigo = DB::table('tpropuesta')
		->orderBy('cpropuesta','desc')
		->first();

		// dd($codigo);

		$cod = explode('.', $codigo->ccodigo);

		if ($cod[0]==$anio) {
			// dd($anio);
			$cant= $codigo->ccodigo;
			$corre = explode('.', $cant);
			$newcod = str_pad($corre[2]+1,3,'0',STR_PAD_LEFT);
			$codigonuevo = $newcod;
		}
		else {
		// dd($cod[0],$anio);
			$codigonuevo = $cant;	
		}

		// dd($codigonuevo);

		return $codigonuevo;
	}


	public function minera($idcliente){

		$minera = Tunidadminera::where('cpersona','=',$idcliente)->get();

		return $minera;
	}

	public function contacto($idminera){

		$contacto = DB::table('tunidadmineracontactos as tumc')
		->join('tcontactocargo as tcc','tcc.ccontactocargo','=','tumc.ccontactocargo')
		->where('cunidadminera','=',$idminera)
		->get();

		return $contacto;
	}

	private function sedes(){
		$tsedes=DB::table('tsedes')
        ->lists('descripcion','codigosede');

        return $tsedes;
	}

	private function portafolio(){
		$servicio = DB::table('tservicio')
		->orderBy('orden','ASC')
		->get();

        return $servicio;
	}

	public function tipoproyecto($idPortafolio){

		$tipoproyecto = DB::table('ttipoproyecto')
        ->where('cservicio','=',$idPortafolio)
        ->lists('descripcion','ctipoproyecto');

		return $tipoproyecto;
	}

	public function grabarinvitacion(Request $request){

		// dd($request->all());

		$invitación = new Invitacion();
		$invitación->unidadminera_id = $request->input('minera');
		$invitación->unidadminera_id = $request->input('mineraid');
		$invitación->contacto_id = $request->input('contacto');
		$invitación->nombre_invitacion = $request->input('invitacion');
		$invitación->comentario_invitacion = $request->input('comentario_invitacion');
		$invitación->comunicar_invitacion = $request->input('comunicar_invitacion');
		$invitación->fecha_invitacion = $request->input('fecha');
		$invitación->user_create = Auth::user()->cpersona;
		$invitación->save();

		if ($request->input('comunicar_invitacion')=='on') {
			
		// dd('correo');
		$titulo = "Invitacion";
		$para_correo = $request->input('email_invitacion');
		$para_nombre = "CEP";
		$data = ["de_cliente"=> $request->input('cliente_nombre'),"de_minera"=>$request->input('minera_nombre'),"invitacion"=>$request->input('invitacion')];
		$result = EmailSend::enviar_invitacion($data,$para_correo,$para_nombre,$titulo);

		}
		return redirect("invitacion");
	}

	public function verCliente(){

		$cliente = Tpersona::where('ctipopersona','=','JUR')->orderBy("nombre","ASC")->get();

		return $cliente;

	}

	public function getEdit($id)
	{
		$invitacion = DB::table('invitacion as in')
		->join('tunidadminera as tum','tum.cunidadminera','=','in.unidadminera_id')
		->join('tpersona as cli','cli.cpersona','=','tum.cpersona')
		->join('tunidadmineracontactos as tumc','tumc.cunidadminera','=','tum.cunidadminera')
		->join('tcontactocargo as tcc','tcc.ccontactocargo','=','tumc.ccontactocargo')
		->select('in.*','tum.nombre as uminera','cli.*','tumc.*','tcc.*')
		->where('in.id','=',$id)
		->get();

		// dd($invitacion);

		return $invitacion;
	}
}


