<?php

namespace App\Http\Controllers\Logistica;

use Illuminate\Http\Request;
use Redirect;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Erp_centro_costo;
use App\Models\Erp_centro_costo_sub;
use App\Models\Erp_herramienta;
use App\Models\Erp_producto;
use App\Models\Erp_solicitud_cab;
use App\Models\Erp_solicitud_det;
use App\Models\Tproyecto;
use App\Models\Tpersonadatosempleado;
use App\Models\Tusuario;
use App\Models\Tpersona;
use Auth;
use DB;
use Response;

class LogisticaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id = null)
    {
        //datos del usuario logueado
        $datoslogueado = Tpersona::where('cpersona','=', Auth::user()->cpersona)->first();
        //Verifico los cabecera
        //dd(!isset(Auth::user()->cpersona));
        if (isset(Auth::user()->cpersona)) {
            $tengocabecera = Erp_solicitud_cab::where('id_usuario','=', $datoslogueado->identificacion)->where('estado','=',0)->whereNull('id_empresa')->whereNull('referencia')->whereNull('id_usuario_solicitante')->whereNull('id_usuario_aprobacion')->whereNull('id_empleado_entrega')->first();
        } else {
            return Redirect::route('login');
        }

        //dd($tengocabecera);

        if ($id) {
            $idregistrado = $id;
        }elseif($tengocabecera){
            $idregistrado = $tengocabecera->id;
        }else {
            //Inserto registro 
            $idsolicitud = new Erp_solicitud_cab;
            $idsolicitud->id_usuario = $datoslogueado->identificacion;
            $idsolicitud->estado = 0;
            $idsolicitud->save();
            $idregistrado = $idsolicitud->id;
        }
        //dd($idregistrado);

        //busco componentes de la vista
        $ccosto = Erp_centro_costo::all();
        $ccostosub = Erp_centro_costo_sub::all();
        $herramientas = Erp_herramienta::all();
        $productos = Erp_producto::all();
        $proyectos = Tproyecto::all();
        $personas = Tpersonadatosempleado::all();
        //anexo los nombres
        foreach ($personas as $key => $value) {
            $personadatos = Tpersona::where('cpersona','=', $value->cpersona)->first();
            $personas[$key]->nombre = $personadatos->nombre;
            $personas[$key]->abreviatura = $personadatos->abreviatura;
        }

        foreach ($ccostosub as $key => $value) {
            $ccostodatos = Erp_centro_costo::where('id','=', $value->id_centrocosto)->first();
            $ccostosub[$key]->ccostonombre = $ccostodatos->descripcion;
        }

        //detalle de la solicitud creo la tabla
        $tablaarticulos = Erp_solicitud_det::where('id_solicitud_cab','=', $idregistrado)->get();
        for ($i=0; $i < count($tablaarticulos); $i++) {
            if ($tablaarticulos[$i]->es_producto == 1) { 
                $determinocodigo =  Erp_producto::where('id','=', $tablaarticulos[$i]->id_producto_herramienta)->first();
                $tablaarticulos[$i]->unidad_metrica_producto = $determinocodigo->desc_unidadmedida;
            } else {
                $determinocodigo =  Erp_herramienta::where('id','=', $tablaarticulos[$i]->id_producto_herramienta)->first();
            } 
            $ccostroname = Erp_centro_costo::where('id','=', $tablaarticulos[$i]->id_centro_costo)->first();
            $subccostoname = Erp_centro_costo_sub::where('id','=', $tablaarticulos[$i]->id_sub_centrocosto)->first();
            $tablaarticulos[$i]->proherrcodigo = $determinocodigo->codigo;
            $tablaarticulos[$i]->nombreccosto = $ccostroname->descripcion;
            $tablaarticulos[$i]->nombresubcosto = $subccostoname->descripcion;
        }
        //dd($tablaarticulos);
        //creo datos select
        $selectdatos = Erp_solicitud_cab::where('id','=', $idregistrado)->first();
        $selectdatosdet = Erp_solicitud_det::where('id_solicitud_cab','=',$idregistrado)->first();
        if ($selectdatosdet) {
            $codigoproyectovalido = $selectdatosdet->codigo_proyecto;
        } else {
            $codigoproyectovalido = 0;
        }
        if ($selectdatos->id_usuario_solicitante) {
            //homologo datos para traer dni
            $elsolicitante = Tpersona::where('identificacion','=', $selectdatos->id_usuario_solicitante)->first();
            $elentrega= Tpersona::where('identificacion','=', $selectdatos->id_empleado_entrega)->first();
            $elaprobado = Tpersona::where('identificacion','=', $selectdatos->id_usuario_aprobacion)->first();
            $itemempresa = ($selectdatos->id)==1?'Anddes':'Geolab';
            $itemproyecto = $codigoproyectovalido;
            $itemsolicitante = $elsolicitante->cpersona;
            $itementregado = $elentrega->cpersona;
            $itemaprobado = $elaprobado->cpersona;
            $itemreferencia = $selectdatos->referencia;
        } else {
            $itemempresa = 'Anddes';
            $itemproyecto = 'A';
            $itemsolicitante = 3;
            $itementregado = 3;
            $itemaprobado = 3;
            $itemreferencia = '';
        }
        
        //Traigo solicitudes solicitadas por DNI usuario logueado

        $tengolassolicitudes = Erp_solicitud_cab::where('id_usuario','=', $datoslogueado->identificacion)->where('id_usuario_solicitante','<>','""')->get();

        for ($i=0; $i < count($tengolassolicitudes); $i++) { 
            $tengolassolicitudes[$i]->empresa = ($tengolassolicitudes[$i]->id_empresa)==1?'Anddes':'Geolab';
            $solicitudet = Erp_solicitud_det::where('id_solicitud_cab','=', $tengolassolicitudes[$i]->id)->first();
            $todaslassolicitudes = Erp_solicitud_det::where('id_solicitud_cab','=', $tengolassolicitudes[$i]->id)->get();
            $cuentoarticuls = Erp_solicitud_det::where('id_solicitud_cab','=', $tengolassolicitudes[$i]->id)->count();
            $tengolassolicitudes[$i]->proyecto = $solicitudet->codigo_proyecto;
            $elsolicitante = Tpersona::where('identificacion','=', $tengolassolicitudes[$i]->id_usuario_solicitante)->first();
            $elentreg = Tpersona::where('identificacion','=', $tengolassolicitudes[$i]->id_empleado_entrega)->first();
            if ($elentreg) {
                $entregadosolicitud = $elentreg->abreviatura;
            } else {
                $entregadosolicitud = '';
            }
            $tengolassolicitudes[$i]->solicitante = $elsolicitante->abreviatura;
            $tengolassolicitudes[$i]->entregadoa = $entregadosolicitud;
            $tengolassolicitudes[$i]->totalarticulos = $cuentoarticuls;
            $tengolassolicitudes[$i]->detarticulo = $todaslassolicitudes;
            //dd()
            for ($a=0; $a < count($tengolassolicitudes[$i]->detarticulo); $a++) {
                if ($tengolassolicitudes[$i]->detarticulo[$a]->es_producto == 1) { 
                    $determinocodigo =  Erp_producto::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_producto_herramienta)->first();
                    $tengolassolicitudes[$i]->detarticulo[$a]->unidad_metrica_producto = $determinocodigo->desc_unidadmedida;

                } else {
                    $determinocodigo =  Erp_herramienta::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_producto_herramienta)->first();
                } 
                $ccostroname = Erp_centro_costo::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_centro_costo)->first();
                $subccostoname = Erp_centro_costo_sub::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_sub_centrocosto)->first();
                $nameproyect = Tproyecto::where('codigo','=',$tengolassolicitudes[$i]->proyecto)->first();
                //dd($nameproyect);
                $tengolassolicitudes[$i]->detarticulo[$a]->nombreproyecto = $nameproyect->nombre;
                $tengolassolicitudes[$i]->detarticulo[$a]->proherrcodigo = $determinocodigo->codigo;
                $tengolassolicitudes[$i]->detarticulo[$a]->nombreccosto = $ccostroname->descripcion;
                $tengolassolicitudes[$i]->detarticulo[$a]->nombresubcosto = $subccostoname->descripcion;
                $tengolassolicitudes[$i]->detarticulo[$a]->fechacreacion = $tengolassolicitudes[$i]->created_at->format('d/m/y');
            }
        }
        
        //dd($tengolassolicitudes[0]);
        //dd($proyectos[0]);
        return view('logistica.index',[
            'ccosto' => $ccosto,
            'ccostosub' => $ccostosub,
            'herramientas' => $herramientas,
            'productos' => $productos,
            'proyectos' => $proyectos,
            'personas' => $personas,
            'idsolicitud' => $idregistrado,
            'tablaarticulos' => $tablaarticulos,
            'itemempresa' => $itemempresa,
            'itemproyecto' => $itemproyecto,
            'itemsolicitante' => $itemsolicitante,
            'itementregado' => $itementregado,
            'itemaprobado' => $itemaprobado,
            'itemreferencia' => $itemreferencia,
            'tablasolicitudes' => $tengolassolicitudes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function CreoSolicitud(Request $request)
    {
        //dd($request->all());
        //$id_um = '';
        if($request->herramienta){
            $producto_herramienta = $request->herramienta;
            $datosproducto = Erp_herramienta::where('id','=', $request->herramienta)->first();
            $descripciondetalle = $datosproducto->descripcion;
            $cantidad = 1;
            $observacion = $request->observaherramienta;
            //dd($id_um);
            //centro de costo
            $centrodecosto = Erp_centro_costo_sub::where('id','=',$request->ccostroherramienta)->first();
            $ccostropadre = $centrodecosto->id_centrocosto;
            $subcentrodecosto = $centrodecosto->id;
            $es_producto = 0;
            $es_herramienta = 1;
            $unidad_medida = 0;
        }else{
            $producto_herramienta = $request->producto;
            //$cantidad = $request->cantidadproducto;
            $datosproducto = Erp_producto::where('id','=', $request->producto)->first();
            //dd($datosproducto);
            //determino descripcioneditable
            if($request->descripcionproducto){
                $descripciondetalle = $request->descripcionproducto;
            }else{
                $descripciondetalle = $datosproducto->desc_producto;
            }
            
            //determino decimales o enteros para la cantidad
            if($datosproducto->permitedecimales == 0){
                $cantidad = number_format($request->cantidadtotal, 2, '.', ',');
            }else{
                $cantidad = intval($request->cantidadtotal);
            }
            //dd($cantidad);
            $observacion = $request->observaproducto;
            //centro de costo
            $centrodecosto = Erp_centro_costo_sub::where('id','=',$request->ccostroproducto)->first();
            $ccostropadre = $centrodecosto->id_centrocosto;
            $subcentrodecosto =  $centrodecosto->id;
            $es_producto = 1;
            $es_herramienta = 0;
            //unidad de medida
            $unidad_medida = $datosproducto->id_um;
            //dd($id_num);
        }
        //codigo proyecto
        $codigoproyecto = Tproyecto::where('cproyecto','=',$request->proyecto)->first();
        //homologo datos para traer dni
        $cantidadherramienta = 1;
        $dnisolicitante = Tpersona::where('cpersona','=', $request->id_solicitante)->first();
        $dnientrega= Tpersona::where('cpersona','=', $request->id_entregado)->first();
        $dniaprobador = Tpersona::where('cpersona','=', $request->id_aprobador)->first();
        
        //actualizo cabecera
        $actsolicitud = Erp_solicitud_cab::where('id','=', $request->idsolicitud)->first();
        $actsolicitud->id_empresa = $request->id_empresa;
        $actsolicitud->referencia = $request->referencia;
        $actsolicitud->id_usuario_solicitante = $dnisolicitante->identificacion;
        $actsolicitud->id_usuario_aprobacion = $dniaprobador->identificacion;
        //$actsolicitud->fecha_aprobacion = '20180117'; lleno la fecha de la aprobacion 
        $actsolicitud->id_empleado_entrega = $dnientrega->identificacion;
        $actsolicitud->update();
        //dd($descripciondetalle);
        //entro al detalle y creo datos
        $creosolicitudet = new Erp_solicitud_det;
        $creosolicitudet->id_solicitud_cab = $request->idsolicitud;
        $creosolicitudet->id_producto_herramienta = $producto_herramienta;
        $creosolicitudet->descripcion = $descripciondetalle;
        $creosolicitudet->id_um = !is_null($unidad_medida)?$unidad_medida:0;
        $creosolicitudet->cantidad = $cantidad;
        $creosolicitudet->observaciones = $observacion;
        $creosolicitudet->codigo_proyecto = $codigoproyecto->codigo;
        $creosolicitudet->fecha = date("Ymd");
        $creosolicitudet->id_centro_costo = $ccostropadre;
        $creosolicitudet->id_sub_centrocosto = $subcentrodecosto;
        $creosolicitudet->es_producto = $es_producto;
        $creosolicitudet->es_herramienta = $es_herramienta;
        $creosolicitudet->save();

        //detalle de la solicitud creo la tabla
        $tablaarticulos = Erp_solicitud_det::where('id_solicitud_cab','=', $request->idsolicitud)->get();
        for ($i=0; $i < count($tablaarticulos); $i++) {
            if ($tablaarticulos[$i]->es_producto == 1) { 
                $determinocodigo =  Erp_producto::where('id','=', $tablaarticulos[$i]->id_producto_herramienta)->first();
                $tablaarticulos[$i]->unidad_metrica_producto = $determinocodigo->desc_unidadmedida;
            } else {
                $determinocodigo =  Erp_herramienta::where('id','=', $tablaarticulos[$i]->id_producto_herramienta)->first();
            } 
            $ccostroname = Erp_centro_costo::where('id','=', $tablaarticulos[$i]->id_centro_costo)->first();
            $subccostoname = Erp_centro_costo_sub::where('id','=', $tablaarticulos[$i]->id_sub_centrocosto)->first();
            $tablaarticulos[$i]->proherrcodigo = $determinocodigo->codigo;
            $tablaarticulos[$i]->nombreccosto = $ccostroname->descripcion;
            $tablaarticulos[$i]->nombresubcosto = $subccostoname->descripcion;
        }

        return view('logistica.tablaarticulos',[
            'tablaarticulos' => $tablaarticulos
        ]);

        //dd('terminen update y create');
        
    }

    public function deleteitemtablaarticulo(Request $request)
    {
        $idcabecera = Erp_solicitud_det::where('id','=',$request->id)->first();
        $eliminorow = Erp_solicitud_det::find($request->id);
        $eliminorow->delete();
        //return 'Nose que retornar';
        //detalle de la solicitud creo la tabla
        $tablaarticulos = Erp_solicitud_det::where('id_solicitud_cab','=', $idcabecera->id_solicitud_cab)->get();
        for ($i=0; $i < count($tablaarticulos); $i++) {
            if ($tablaarticulos[$i]->es_producto == 1) { 
                $determinocodigo =  Erp_producto::where('id','=', $tablaarticulos[$i]->id_producto_herramienta)->first();
                $tablaarticulos[$i]->unidad_metrica_producto = $determinocodigo->desc_unidadmedida;
            } else {
                $determinocodigo =  Erp_herramienta::where('id','=', $tablaarticulos[$i]->id_producto_herramienta)->first();
            } 
            $ccostroname = Erp_centro_costo::where('id','=', $tablaarticulos[$i]->id_centro_costo)->first();
            $subccostoname = Erp_centro_costo_sub::where('id','=', $tablaarticulos[$i]->id_sub_centrocosto)->first();
            $tablaarticulos[$i]->proherrcodigo = $determinocodigo->codigo;
            $tablaarticulos[$i]->nombreccosto = $ccostroname->descripcion;
            $tablaarticulos[$i]->nombresubcosto = $subccostoname->descripcion;
        }

        return view('logistica.tablaarticulos',[
            'tablaarticulos' => $tablaarticulos
        ]);
    }

    public function logisticambiostatus($id)
    {
        $idcabecera = Erp_solicitud_cab::where('id','=',$id)->first();
        $idcabecera->estado = 1;
        $idcabecera->update();
        //dd($idcabecera);
        //return Redirect::route('logistica.index');
    }

    public function listaindex()
    {
        //Verifico los cabecera
        //dd(!isset(Auth::user()->cpersona));
        $elaprobador= Tpersona::where('cpersona','=', Auth::user()->cpersona)->first();
        $tengolassolicitudes = Erp_solicitud_cab::where('id_usuario_aprobacion','=', $elaprobador->identificacion)->where('estado','=',1)->get();

        for ($i=0; $i < count($tengolassolicitudes); $i++) { 
            $tengolassolicitudes[$i]->empresa = ($tengolassolicitudes[$i]->id_empresa)==1?'Anddes':'Geolab';
            $solicitudet = Erp_solicitud_det::where('id_solicitud_cab','=', $tengolassolicitudes[$i]->id)->first();
            $todaslassolicitudes = Erp_solicitud_det::where('id_solicitud_cab','=', $tengolassolicitudes[$i]->id)->get();
            $cuentoarticuls = Erp_solicitud_det::where('id_solicitud_cab','=', $tengolassolicitudes[$i]->id)->count();
            $tengolassolicitudes[$i]->proyecto = $solicitudet->codigo_proyecto;
            $elsolicitante = Tpersona::where('identificacion','=', $tengolassolicitudes[$i]->id_usuario_solicitante)->first();
            $elentrega= Tpersona::where('identificacion','=', $tengolassolicitudes[$i]->id_empleado_entrega)->first();
            $tengolassolicitudes[$i]->solicitante = $elsolicitante->abreviatura;
            $tengolassolicitudes[$i]->entregadoa = $elentrega->abreviatura;
            $tengolassolicitudes[$i]->totalarticulos = $cuentoarticuls;
            $tengolassolicitudes[$i]->detarticulo = $todaslassolicitudes;
            //dd()
            for ($a=0; $a < count($tengolassolicitudes[$i]->detarticulo); $a++) {
                if ($tengolassolicitudes[$i]->detarticulo[$a]->es_producto == 1) { 
                    $determinocodigo =  Erp_producto::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_producto_herramienta)->first();
                    $tengolassolicitudes[$i]->detarticulo[$a]->unidad_metrica_producto = $determinocodigo->desc_unidadmedida;
                } else {
                    $determinocodigo =  Erp_herramienta::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_producto_herramienta)->first();
                } 
                $ccostroname = Erp_centro_costo::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_centro_costo)->first();
                $subccostoname = Erp_centro_costo_sub::where('id','=', $tengolassolicitudes[$i]->detarticulo[$a]->id_sub_centrocosto)->first();
                $tengolassolicitudes[$i]->detarticulo[$a]->proherrcodigo = $determinocodigo->codigo;
                $tengolassolicitudes[$i]->detarticulo[$a]->nombreccosto = $ccostroname->descripcion;
                $tengolassolicitudes[$i]->detarticulo[$a]->nombresubcosto = $subccostoname->descripcion;
            }
        }

        $listadostatus = Erp_solicitud_cab::where('id_usuario_aprobacion','=', $elaprobador->identificacion)->where('estado','<>',0)->get();

        for ($i=0; $i < count($listadostatus); $i++) { 
            $listadostatus[$i]->empresa = ($listadostatus[$i]->id_empresa)==1?'Anddes':'Geolab';
            $solicitudet = Erp_solicitud_det::where('id_solicitud_cab','=', $listadostatus[$i]->id)->first();
            $todaslassolicitudes = Erp_solicitud_det::where('id_solicitud_cab','=', $listadostatus[$i]->id)->get();
            $cuentoarticuls = Erp_solicitud_det::where('id_solicitud_cab','=', $listadostatus[$i]->id)->count();
            $listadostatus[$i]->proyecto = $solicitudet->codigo_proyecto;
            $elsolicitante = Tpersona::where('identificacion','=', $listadostatus[$i]->id_usuario_solicitante)->first();
            $elentrega= Tpersona::where('identificacion','=', $listadostatus[$i]->id_empleado_entrega)->first();
            $listadostatus[$i]->solicitante = $elsolicitante->abreviatura;
            $listadostatus[$i]->entregadoa = $elentrega->abreviatura;
            $listadostatus[$i]->totalarticulos = $cuentoarticuls;
            $listadostatus[$i]->detarticulo = $todaslassolicitudes;
            //dd()
            for ($a=0; $a < count($listadostatus[$i]->detarticulo); $a++) {
                if ($listadostatus[$i]->detarticulo[$a]->es_producto == 1) { 
                    $determinocodigo =  Erp_producto::where('id','=', $listadostatus[$i]->detarticulo[$a]->id_producto_herramienta)->first();
                    $listadostatus[$i]->detarticulo[$a]->unidad_metrica_producto = $determinocodigo->desc_unidadmedida;
                } else {
                    $determinocodigo =  Erp_herramienta::where('id','=', $listadostatus[$i]->detarticulo[$a]->id_producto_herramienta)->first();
                } 
                $ccostroname = Erp_centro_costo::where('id','=', $listadostatus[$i]->detarticulo[$a]->id_centro_costo)->first();
                $subccostoname = Erp_centro_costo_sub::where('id','=', $listadostatus[$i]->detarticulo[$a]->id_sub_centrocosto)->first();
                $listadostatus[$i]->detarticulo[$a]->proherrcodigo = $determinocodigo->codigo;
                $listadostatus[$i]->detarticulo[$a]->nombreccosto = $ccostroname->descripcion;
                $listadostatus[$i]->detarticulo[$a]->nombresubcosto = $subccostoname->descripcion;
            }
        }

        //dd($tengolassolicitudes[0]);
        return view('logistica.listadodesolicitudes',[
            'tablaarticulos' => $tengolassolicitudes,
            'listadostatus' => $listadostatus
        ]);

    }

    public function AprobacionesSolicitud(Request $request)
    {
        //dd($request->all());
        $solicitudapruebo = Erp_solicitud_cab::where('id','=', $request->id_solicitud)->first();
        if ($request->estado == 'aprobar') {
            $solicitudapruebo->estado = 2;
            $solicitudapruebo->fecha_aprobacion = date("Ymd");
        } else {
            $solicitudapruebo->estado = 3;
        }
        $solicitudapruebo->update();
        
        return Redirect::route('listadologistica.index');
        
        

    }


    
}
