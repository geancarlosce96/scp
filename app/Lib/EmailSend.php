<?php

namespace App\Lib;

use Mail;

class EmailSend
{
	public static function enviar_individual($data = [], $para_email = "eswinxd@gmail.com", $para_nombre="Eswin", $titulo="test")
    {
        $data["para_email"] = $para_email;
        $data["para_nombre"] = $para_nombre;
        $data["titulo"] = $titulo;
		
		$result = Mail::send('email.test', $data, function($message) use ($data) {
	    	$message->to($data['para_email'], $data['para_nombre'])
	    		->subject($data['titulo']);
	    	$message->from('info@anddes.com','Sistema SCP');
		});	

    	return $result;
    }

    public static function enviar_invitacion($data = [], $para_email = "fabio.colan@anddes.com", $para_nombre="Fabio", $titulo="Invitación")
    {
        $data["para_email"] = $para_email;
        $data["para_nombre"] = $para_nombre;
        $data["titulo"] = $titulo;
		
		$result = Mail::send('email.invitacion', $data, function($message) use ($data) {
	    	$message->to($data['para_email'], $data['para_nombre'])
		    		->subject($data['titulo']);
	    	$message->from('info@anddes.com','Sistema SCP');
		});	

    	return $result;
    }

    public static function correoinicio($data = [], $para_email = "fabio.colan@anddes.com", $titulo="Inicio Proyectos")
    {
        $data["para_email"] = $para_email;
        $data["titulo"] = $titulo;
		
		$result = Mail::send('email.correoinicio', $data, function($message) use ($data) {
	    	$message->to($data['para_email'])
		    		->subject($data['titulo']);
	    	$message->from('info@anddes.com','Sistema SCP');
		});	

    	return $result;
    }

    public static function horasextras($data)
    {
       
		
		$result = Mail::send('email.hextrasemail', $data, function($message) use ($data) {
	    	$message->to($data['correojefeinmediato'])->to($data['correojefesuperior'])
		    		->subject($data['titulocorreo']);
	    	$message->from('info@anddes.com','Sistema SCP');
		});	

    	return $result;
    }
}