<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_solicitud_det extends model
{
    protected $table = "erp_solicitud_det";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_solicitud_cab',
        'id_producto_herramienta',
        'descripcion',
        'id_um',
        'cantidad',
        'observaciones',
        'codigo_proyecto',
        'estado',
        'fecha',
        'id_centro_costo',
        'id_sub_centrocosto',
        'es_producto',
        'es_herramienta',
    ];

}