<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestarevision extends Model {

    /**
     * Generated
     */
    protected $primaryKey = "cpropuestarevision";
    protected $table = 'tpropuestarevision';
    protected $fillable = ['cpropuestarevision', 'cpropuesta', 'revisionanterior', 'revisionactual', 'xmlrevisionanterior', 'xmlrevisionactual', 'frevision'];

    public $timestamps = false;

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }


}
