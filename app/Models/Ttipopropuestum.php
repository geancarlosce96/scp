<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttipopropuestum extends Model {

    /**
     * Generated
     */

    protected $table = 'ttipopropuesta';
    protected $fillable = ['ctipopropuesta', 'descripcion'];


    public function tpropuesta() {
        return $this->hasMany(\App\Models\Tpropuestum::class, 'ctipopropuesta', 'ctipopropuesta');
    }


}
