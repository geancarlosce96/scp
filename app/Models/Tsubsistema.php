<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tsubsistema extends Model {

    /**
     * Generated
     */

    protected $table = 'tsubsistema';
    protected $fillable = ['csubsistema', 'descripcion'];


    public function tcondicionesoperativas() {
        return $this->hasMany(\App\Models\Tcondicionesoperativa::class, 'csubsistema', 'csubsistema');
    }


}
