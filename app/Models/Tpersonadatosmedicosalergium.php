<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonadatosmedicosalergium extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cpersonaalergia';
    protected $table = 'tpersonadatosmedicosalergia';
    protected $fillable = ['cpersonaalergia', 'cpersona', 'calergias', 'observacion'];

    public $timestamps = false;

    public function talergium() {
        return $this->belongsTo(\App\Models\Talergia::class, 'calergias', 'calergias');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
