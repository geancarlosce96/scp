<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttipoidentificacion extends Model {

    /**
     * Generated
     */

    protected $table = 'ttipoidentificacion';
    protected $fillable = ['ctipoidentificacion', 'descripcion'];


    public function tpersonas() {
        return $this->hasMany(\App\Models\Tpersona::class, 'ctipoidentificacion', 'ctipoidentificacion');
    }


}
