<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoactividadeshabilitacion extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cproyacthab';
    protected $table = 'tproyectoactividadeshabilitacion';
    protected $fillable = ['cproyacthab', 'cproyectoactividades', 'cpersona', 'cproyecto', 'activo', 'finicio', 'ftermino'];

    public $timestamps = false;

    public function tproyectoactividades() {
        return $this->belongsTo(\App\Models\Tproyectoactividade::class, 'cproyectoactividades', 'cproyectoactividades');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

  
    public function tpersona() {
        return $this->hasMany(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

  

}
