<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tleccionesaprendida extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cleccionaprendida';
    protected $table = 'tleccionesaprendidas';
    protected $fillable = ['cleccionaprendida', 'cproyecto', 'item', 'descripcion', 'tag', 'fecha', 'carea', 'cfaseproyecto', 'careaconocimiento', 'suceso', 'cpersona_elaborado', 'crol_elaborado', 'cpersona_aprobado'];
    public $timestamps = false;


    public function tarea() {
        return $this->belongsTo(\App\Models\Tarea::class, 'carea', 'carea');
    }

    public function tareasconocimiento() {
        return $this->belongsTo(\App\Models\Tareasconocimiento::class, 'careaconocimiento', 'careaconocimiento');
    }

    public function tfasesproyecto() {
        return $this->belongsTo(\App\Models\Tfasesproyecto::class, 'cfaseproyecto', 'cfaseproyecto');
    }

    public function tpersona1() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_elaborado', 'cpersona');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_aprobado', 'cpersona');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'crol_elaborado', 'croldespliegue');
    }


}
