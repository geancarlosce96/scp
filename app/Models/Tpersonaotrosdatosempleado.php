<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonaotrosdatosempleado extends Model {

    /**
     * Generated
     */

    protected $table = 'tpersonaotrosdatosempleados';
    protected $fillable = ['cpersonaotrosdatosempleados', 'cpersona', 'licconducir', 'afp', 'cusp', 'codbanco', 'ctabanco', 'visapais', 'pasaporte', 'fvenicmientopasaporte'];



}
