<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Date;

class Tproyecto extends Model {

    /**
     * Generated
     */
    protected $primaryKey = "cproyecto";
    protected $table = 'tproyecto';
    protected $fillable = ['cproyecto', 'cpersonacliente', 'codigo', 'cunidadminera', 'nombre', 'descripcion', 'cpersona_coordinador', 'cpersona_gerente', 'cmoneda', 'cmedioentrega', 'cservicio','fproyecto','csede',
    'finicio','finicioreal','fcierre','fcierrereal','fcierreadministrativo','tag','tipoproy','ctipoproyecto','ctipogestionproyecto'];

    public $timestamps = false;

    public function tmedioentrega() {
        return $this->belongsTo(\App\Models\Tmedioentrega::class, 'cmedioentrega', 'cmedioentrega');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersonacliente', 'cpersona');
    }

    public function tpersona_coordinador() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_coordinador', 'cpersona');
    }

    public function tpersona_gerente() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_gerente', 'cpersona');
    }

    public function tservicio() {
        return $this->belongsTo(\App\Models\Tservicio::class, 'cservicio', 'cservicio');
    }

    public function tunidadminera() {
        return $this->belongsTo(\App\Models\Tunidadminera::class, 'cunidadminera', 'cunidadminera');
    }

    public function tchecklists() {
        return $this->belongsToMany(\App\Models\Tchecklist::class, 'tproyectochecklist', 'cproyecto', 'cchecklist');
    }

    public function tdocumentosparaproyectos() {
        return $this->belongsToMany(\App\Models\Tdocumentosparaproyecto::class, 'tproyectodocumentos', 'cproyecto', 'cdocumentoparapry');
    }

    public function tleccionesaprendidas() {
        return $this->hasMany(\App\Models\Tleccionesaprendida::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectochecklists() {
        return $this->hasMany(\App\Models\Tproyectochecklist::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectocronogramas() {
        return $this->hasMany(\App\Models\Tproyectocronograma::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectodisciplinas() {
        return $this->hasMany(\App\Models\Tproyectodisciplina::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectodocumentos() {
        return $this->hasMany(\App\Models\Tproyectodocumento::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectoedts() {
        return $this->hasMany(\App\Models\Tproyectoedt::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectoentregables() {
        return $this->hasMany(\App\Models\Tproyectoentregable::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectogastos() {
        return $this->hasMany(\App\Models\Tproyectogasto::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectogastoslaboratorios() {
        return $this->hasMany(\App\Models\Tproyectogastoslaboratorio::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectoactividades() {
        return $this->hasMany(\App\Models\Tproyectoactividade::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectoestructuraparticipantes() {
        return $this->hasMany(\App\Models\Tproyectoestructuraparticipante::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectosubs() {
        return $this->hasMany(\App\Models\Tproyectosub::class, 'cproyecto', 'cproyecto');
    }

    public function ttransmittalconfiguracions() {
        return $this->hasMany(\App\Models\Ttransmittalconfiguracion::class, 'cproyecto', 'cproyecto');
    }

    public static function _obtenerUsuariosDeProyecto($cproyecto, $carea)
    {
        $lista = DB::table('tproyectopersona')
            ->join('tpersona', 'tproyectopersona.cpersona', '=', 'tpersona.cpersona')            
            ->join('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
            ->join('tusuarios', 'tusuarios.cpersona', '=', 'tpersona.cpersona')
            ->join('tcategoriaprofesional', 'tcategoriaprofesional.ccategoriaprofesional', '=', 'tproyectopersona.ccategoriaprofesional')
            ->select('tpersona.cpersona', 'tpersona.abreviatura as nombre', 'tusuarios.cusuario', 'tusuarios.nombre as user', 'tcategoriaprofesional.descripcion as profesion')
            //->where('tproyectopersona.eslider', "=", "0")
            ->where('tusuarios.estado', "=", "ACT") 
            ->where("tproyectopersona.cproyecto", "=", $cproyecto)
            ->where('tpersonadatosempleado.carea', "=", $carea)
            ->orderBy('tproyectopersona.eslider','DESC')
            ->orderBy('tcategoriaprofesional.orden','ASC')
            ->orderBy('tpersona.abreviatura','ASC')
            ->get();

        return $lista;
    }

    public static function obtenerUsuariosDeProyectoConActividadHabilitada($cproyecto, $carea, $cproyectoactividades)
    {
        $lista = Tproyecto::_obtenerUsuariosDeProyecto($cproyecto, $carea);
        $listaTemp = [];

        //filtrar segun actividad habilitada
        foreach ($lista as $key => $item) {
            $actividad = Tproyectoactividadeshabilitacion::where("cproyectoactividades", "=", $cproyectoactividades)
                ->where("activo", "=", "1")
                ->where("cproyecto", "=", $cproyecto)
                ->count();
            if($actividad > 0)
                array_push($listaTemp, $item);
        }

        return $listaTemp;
    }

    public static function obtenerUsuariosDeArea($carea)
    {
        $lista = DB::table('tpersonadatosempleado')
            ->join('tpersona', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
            ->join('tusuarios', 'tusuarios.cpersona', '=', 'tpersona.cpersona')
            ->join('tcategoriaprofesional','tcategoriaprofesional.ccategoriaprofesional','=','tpersonadatosempleado.ccategoriaprofesional')         
            ->select('tpersona.cpersona', 'tpersona.abreviatura as nombre', 'tusuarios.cusuario', 'tusuarios.nombre as user','tcategoriaprofesional.descripcion as profesion')
            ->where('tusuarios.estado', "=", "ACT")         
            ->where('tpersonadatosempleado.carea', "=", $carea)
            ->where('tpersona.cpersona', "!=", '0')
            ->orderBy('tcategoriaprofesional.orden','ASC')
            ->orderBy('tpersona.abreviatura','ASC')
            ->get();

        return $lista;
    }

    public static function obtenerHorasDeSemana($periodo, $semana, $cproyectoactividades, $user, $esUsuarioDeProyecto = true)
    {
        $horas = Planificacion_diaria::where("periodo", "=", $periodo)
            ->where("semana", "=", $semana)
            ->where("cproyectoactividades", "=", $cproyectoactividades)
            ->where("usuario", "=", $user)
            ->first();

        if($horas == null){
            $horas = new \stdClass;        

            if($esUsuarioDeProyecto)
                $horas->lun =$horas->mar = $horas->mie = $horas->jue = $horas->vie = $horas->sab = $horas->dom = $horas->comentarios = "";
        }

        return $horas;
    }

    public static function obtenerHoraPlanificadaSegunProyectoActividadFecha($cproyectoactividades, $cpersona_ejecuta, $fplanificado)
    {
        $horas = DB::table('tproyectoejecucion as t')
            ->select("t.*")
            ->where("cproyectoactividades", "=", $cproyectoactividades)
            ->where("cpersona_ejecuta", "=", $cpersona_ejecuta)
            ->where("fplanificado", "=", $fplanificado)
            ->first();

        if($horas != null){
            $horas->planificadas = $horas->horasplanificadas;
            $horas->comentarios = $horas->obsplanificada;
        }
        else{
            $horas = new \stdClass;
            $horas->planificadas = 0;
            $horas->comentarios = "";
        }

        return $horas;
    }

    public static function guardarHoraPlanificadaSegunProyectoActividadFecha($cproyectoactividades, $cpersona_ejecuta, $fplanificado, $horasplanificadas, $comentarios, $cproyecto = 0)
    {
        //verificar si existe
        $actividad = Tproyectoejecucion::where("cproyectoactividades", "=", $cproyectoactividades)->where("cpersona_ejecuta", "=", $cpersona_ejecuta)
            ->where("fplanificado", "=", $fplanificado)->first();
        //guardar
        
        //buscar carea de persona
        $persona = Tpersonadatosempleado::where("cpersona", "=", $cpersona_ejecuta)->first();
        
        if($persona==null)
            return null;

        $carea = $persona->carea;
        //crear cabecera
        //crear registro
        if($actividad == null){
            $actividad = new Tproyectoejecucion();
            $actividad->estado = '1';        
            $actividad->tipo = '1';    
            $actividad->ccondicionoperativa = 'PLA';
            $actividad->csubsistema = '004';
            $actividad->carea = $carea;
            $actividad->cproyecto = $cproyecto;
        }
        
        $actividad->horasplanificadas = $horasplanificadas;
        //$actividad->obsplanificada = $comentarios; verificar*
        $actividad->fplanificado = $fplanificado;        
        $actividad->cpersona_ejecuta = $cpersona_ejecuta;
        $actividad->cproyectoactividades = $cproyectoactividades;
        $actividad->cproyecto = $cproyecto;

        $actividad->save();

        return $actividad;
    }

    public static function guardarCabProyectoActividadSemanaPersona($cproyectoactividades, $semana, $cpersona_ejecuta)
    {

        $cab = Tproyectoejecucioncab::where("cproyectoactividades", "=", $cproyectoactividades)
            ->where("semana", "=", $semana)
            ->where("cpersona_ejecuta", "=", $cpersona_ejecuta)
            ->where("tipo", "=", 1)
            ->first();

        if($cab == null)
        {
            $cab = new Tproyectoejecucioncab();
            $cab->semana = $semana;
            $cab->estado = "1";
            $cab->tipo = "1"; // 1- FACTURABLES FF 2- NO FACTURABKES 3- ADMINISTRATIVAS ANDDES verificar*
            $cab->fregistro = date("y-m-d");
            $cab->cpersona_ejecuta = $cpersona_ejecuta;
            $cab->cproyectoactividades = $cproyectoactividades;
        }

        return $cab;
    }

    public static function guardarHoraEnSemana($periodo, $semana, $cproyectoactividades, $usuario, $dia, $horas, $comentarios)
    {
        $planificacion = Planificacion_diaria::where("periodo", "=", $periodo)
            ->where("semana", "=", $semana)
            ->where("cproyectoactividades", "=", $cproyectoactividades)
            ->where("usuario", "=", $usuario)
            ->first();

        if($planificacion == null)
            $planificacion = new Planificacion_diaria();
        
        $planificacion->cproyectoactividades = $cproyectoactividades;
        $planificacion->periodo = $periodo;
        $planificacion->semana = $semana;
        $planificacion->usuario = $usuario;

        switch ($dia) {
            case 'lun': $planificacion->lun = $horas; break;
            case 'mar': $planificacion->mar = $horas; break;
            case 'mie': $planificacion->mie = $horas; break;
            case 'jue': $planificacion->jue = $horas; break;
            case 'vie': $planificacion->vie = $horas; break;
            case 'sab': $planificacion->sab = $horas; break;
            case 'dom': $planificacion->dom = $horas; break;
        }

        $planificacion->save();

        return $planificacion;
    }

    public static function esUsuarioDeProyecto($usuarios, $cpersona)
    {
        foreach ($usuarios as $key => $usuario) {
            if($usuario->cpersona == $cpersona)
                return true;
        }
        return false;
    }

}
