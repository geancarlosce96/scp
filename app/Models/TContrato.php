<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TContrato extends Model
{
    protected $table = 'tcontrato';
    protected $primaryKey = 'ccontrato';
    protected $fillable = ['conos','fechafirma', 'fechainicio', 'fechatermino','updated_user','created_user','cproyecto'
    ];
}
