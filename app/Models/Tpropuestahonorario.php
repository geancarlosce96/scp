<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestahonorario extends Model {

    /**
     * Generated
     */

    protected $primaryKey = "cpropuestahonorarios";
    protected $table = 'tpropuestahonorarios';
    protected $fillable = ['cpropuestahonorarios', 'cpropuestaactividades', 'cestructurapropuesta', 'horas', 'xmlareadetalle', 'cpropuesta_proveedor', 'cpropuestapaquete'];

    public $timestamps = false;

    public function testructuraparticipante() {
        return $this->belongsTo(\App\Models\Testructuraparticipante::class, 'cestructurapropuesta', 'cestructurapropuesta');
    }

    public function tpropuestaactividade() {
        return $this->belongsTo(\App\Models\Tpropuestaactividade::class, 'cpropuestaactividades', 'cpropuestaactividades');
    }

    public function tpropuestapaquete() {
        return $this->belongsTo(\App\Models\Tpropuestapaquete::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }


}
