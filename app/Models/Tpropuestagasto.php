<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tpropuestagasto extends Model {

    /**
     * Generated
     */

    protected $primaryKey = "cpropuestaproyectogastos";
    protected $table = 'tpropuestagastos';
    protected $fillable = ['cpropuestaproyectogastos', 'cpropuesta', 'item', 'concepto', 'unidad', 'cantidad', 'costou', 'comentario', 'cpersona_proveedor', 'cpropuestapaquete'];    

    public function tconceptogasto() {
        return $this->belongsTo(\App\Models\Tconceptogasto::class, 'concepto', 'cconcepto');
    }

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestapaquete() {
        return $this->belongsTo(\App\Models\Tpropuestapaquete::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }

    public static function obtenerSegunPropuesta($cpropuesta)
    {
        $gastos = DB::table('tpropuestagastos as p')
            ->join("tconceptogastos as g", "p.concepto", "=", "g.cconcepto")
            ->where('cpropuesta', '=', $cpropuesta)
            ->orderBy("p.concepto", "asc")
            ->orderBy("cpropuestaproyectogastos", "asc")
            ->get();        
        return $gastos;
    }

    public static function obtenerLabSegunPropuesta($cpropuesta)
    {
        $gastos = DB::table('tpropuestagastoslaboratorio as p')
            ->join("tconceptogastos as g", "p.concepto", "=", "g.cconcepto")
            ->where('cpropuesta', '=', $cpropuesta)
            ->orderBy("p.concepto", "asc")
            ->orderBy("cpropuestagastoslab", "asc")
            ->get();        
        return $gastos;
    }

}
