<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tareasconocimiento extends Model {

    /**
     * Generated
     */
    protected $primaryKey='careaconocimiento';
    protected $table = 'tareasconocimiento';
    protected $fillable = ['careaconocimiento', 'descripcion'];
    public $timestamps=false;

    public function tleccionesaprendidas() {
        return $this->hasMany(\App\Models\Tleccionesaprendida::class, 'careaconocimiento', 'careaconocimiento');
    }


}
