<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tunidadmineracontacto extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cunidadmineracontacto';
    protected $table = 'tunidadmineracontactos';
    protected $fillable = ['cunidadmineracontacto', 'cunidadminera', 'cpersona_contactoanddes', 'apaterno', 'amaterno', 'nombres', 'ctipocontacto', 'ctipoinformacioncontacto', 'ccontactocargo', 'email', 'telefono','anexo','cel1','cel2'];

    public $timestamps = false;


    public function tcontactocargo() {
        return $this->belongsTo(\App\Models\Tcontactocargo::class, 'ccontactocargo', 'ccontactocargo');
    }

    public function ttiposcontacto() {
        return $this->belongsTo(\App\Models\Ttiposcontacto::class, 'ctipocontacto', 'ctipocontacto');
    }

    public function ttiposinformacioncontacto() {
        return $this->belongsTo(\App\Models\Ttiposinformacioncontacto::class, 'ctipoinformacioncontacto', 'ctipoinformacioncontacto');
    }

    public function ttransmittalconfiguracions() {
        return $this->belongsToMany(\App\Models\Ttransmittalconfiguracion::class, 'ttransmittalconfiguracioncontacto', 'cunidadmineracontacto', 'ctransconfiguracion');
    }

    public function ttransmittalconfiguracioncontactos() {
        return $this->hasMany(\App\Models\Ttransmittalconfiguracioncontacto::class, 'cunidadmineracontacto', 'cunidadmineracontacto');
    }

    public function tunidadminera() {
        return $this->hasMany(\App\Models\Tunidadminera::class, 'cunidadminera', 'cunidadminera');
    }


}
