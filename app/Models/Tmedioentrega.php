<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tmedioentrega extends Model {

    /**
     * Generated
     */

    protected $table = 'tmedioentrega';
    protected $fillable = ['cmedioentrega', 'descripcion'];


    public function tpropuesta() {
        return $this->hasMany(\App\Models\Tpropuestum::class, 'cmedioentrega', 'cmedioentrega');
    }

    public function tproyectos() {
        return $this->hasMany(\App\Models\Tproyecto::class, 'cmedioentrega', 'cmedioentrega');
    }


}
