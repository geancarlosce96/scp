<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestapresentacion extends Model {

    /**
     * Generated
     */

    protected $table = 'tpropuestapresentacion';
    protected $fillable = ['cpropuesta', 'cpresentacion', 'item'];


    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function ttipospropuestapresentacion() {
        return $this->belongsTo(\App\Models\Ttipospropuestapresentacion::class, 'cpresentacion', 'cpresentacion');
    }


}
