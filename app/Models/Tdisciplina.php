<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tdisciplina extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cdisciplina';
    protected $table = 'tdisciplina';
    protected $fillable = ['cdisciplina', 'descripcion','abrevia'];
    public $timestamps=false;

    public function ttiposentregables() {
        return $this->belongsToMany(\App\Models\Ttiposentregable::class, 'tentregables', 'cdisciplina', 'ctipoentregable');
    }

    public function tpropuesta() {
        return $this->belongsToMany(\App\Models\Tpropuestum::class, 'tpropuestadisciplina', 'cdisciplina', 'cpropuesta');
    }

    public function tentregables() {
        return $this->hasMany(\App\Models\Tentregable::class, 'cdisciplina', 'cdisciplina');
    }

    public function tpropuestadisciplinas() {
        return $this->hasMany(\App\Models\Tpropuestadisciplina::class, 'cdisciplina', 'cdisciplina');
    }

    public function tproyectodisciplinas() {
        return $this->hasMany(\App\Models\Tproyectodisciplina::class, 'cdisciplina', 'cdisciplina');
    }

    static public function allWithServicios()
    {
        $tags = Tdisciplina::all();

        foreach ($tags as $key => $tag)
            $tag->items = TagServicio::where("servicio_grupal_id", "=", $tag->cdisciplina)->get();

        return $tags;
    }

    static public function obtenerSegunPropuesta($cpropuesta)
    {
        $tags = Propuesta_tag_servicio::where("propuesta_id", "=", $cpropuesta)
            ->select("propuesta_tag_servicio_id")
            ->get()->toArray();

        for ($i=0; $i < count($tags); $i++)
            $tags[$i] = $tags[$i]["propuesta_tag_servicio_id"];

        return $tags;
    }


}
