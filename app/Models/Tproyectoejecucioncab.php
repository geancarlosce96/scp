<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoejecucioncab extends Model {

    /**
     * Generated
     */
     protected $primaryKey="cproyectoejecucioncab";
    protected $table = 'tproyectoejecucioncab';
    protected $fillable = ['cproyectoejecucioncab', 'semana', 'cactividad', 'estado', 'tipo', 'fregistro', 'cpersona_ejecuta', 'cproyectoactividades'];

    public $timestamps = false;


    public function tproyectoactividade() {
        return $this->belongsTo(\App\Models\Tproyectoactividade::class, 'cproyectoactividades', 'cproyectoactividades');
    }

    public function tactividad() {
        return $this->belongsTo(\App\Models\Tactividad::class, 'cactividad', 'cactividad');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_ejecuta', 'cpersona');
    }

    public function tproyectoejecucions() {
        return $this->hasMany(\App\Models\Tproyectoejecucion::class, 'cproyectoejecucioncab', 'cproyectoejecucioncab');
    }


}
