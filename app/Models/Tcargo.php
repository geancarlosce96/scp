<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tcargo extends Model {

    /**
     * Generated
     */
    protected $primaryKey = "ccargo";
    protected $table = 'tcargos';
    protected $fillable = ['ccargo', 'codigo', 'descripcion', 'siglas', 'ccargo_parent', 'nivel_aprobacion_vacaciones'];

   public $timestamps = false;
    public function tcargo() {
        return $this->belongsTo(\App\Models\Tcargo::class, 'ccargo_parent', 'ccargo');
    }

    public function tcargos() {
        return $this->hasMany(\App\Models\Tcargo::class, 'ccargo_parent', 'ccargo');
    }

    public function tpersonadatosempleados() {
        return $this->hasMany(\App\Models\Tpersonadatosempleado::class, 'ccargo', 'ccargo');
    }


}
