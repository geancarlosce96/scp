<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluacion_persona extends model
{
    protected $table = "evaluacion_persona";

    public function estado_descripcion()
    {
    	$estado = $this->estado;
    	
    	if($estado == 1)
            return "En proceso";
        else if($estado == 2)
            return "Aprobado";
        else if($estado == 3)
            return "Desaprobado";
        
        return "Pendiente";
    }
    
}
