<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoedt extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cproyectoedt';
    protected $table = 'tproyectoedt';
    protected $fillable = ['cproyectoedt', 'cproyecto', 'codigo', 'descripcion', 'cproyectoedt_parent', 'tipo','nivel','cproyectoent_rev'];

    public $timestamps = false;


    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectoedt() {
        return $this->belongsTo(\App\Models\Tproyectoedt::class, 'cproyectoedt_parent', 'cproyectoedt');
    }

    public function tproyectoedts() {
        return $this->hasMany(\App\Models\Tproyectoedt::class, 'cproyectoedt_parent', 'cproyectoedt');
    }

    public function tproyectoentregables() {
        return $this->hasMany(\App\Models\Tproyectoentregable::class, 'cproyectoedt', 'cproyectoedt');
    }


}
