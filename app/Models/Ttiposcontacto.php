<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttiposcontacto extends Model {

    /**
     * Generated
     */
    protected $primaryKey='ctipocontacto';
    protected $table = 'ttiposcontacto';
    protected $fillable = ['ctipocontacto', 'descripcion'];
 	public $timestamps=false;


    public function tunidadmineracontactos() {
        return $this->hasMany(\App\Models\Tunidadmineracontacto::class, 'ctipocontacto', 'ctipocontacto');
    }


}
