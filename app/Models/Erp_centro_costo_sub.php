<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_centro_costo_sub extends model
{
    protected $table = "erp_centrocostosub";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_centrocosto',
        'codigo',
        'descripcion',
    ];

}