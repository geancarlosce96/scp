<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tcategoriaprofesional extends Model {

    /**
     * Generated
     */
    
    protected $primaryKey ="ccategoriaprofesional";
    protected $table = 'tcategoriaprofesional';
    protected $fillable = ['ccategoriaprofesional', 'descripcion', 'abreviatura'];

    public $timestamps = false;

    public function tproyectopersonas() {
        return $this->hasMany(\App\Models\Tproyectopersona::class, 'ccategoriaprofesional', 'ccategoriaprofesional');
    }


}
