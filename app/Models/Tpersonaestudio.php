<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonaestudio extends Model {

    /**
     * Generated
     */
    protected $primaryKey="cpersonaestudios";
    protected $table = 'tpersonaestudios';
    protected $fillable = ['cpersonaestudios', 'cpersona', 'spersonaestudios', 'nivel', 'cespecialidad', 'cinstitucion', 'colegiatura', 'estado', 'fingreso', 'ftermino'];

    public $timestamps = false;
    public function tespecialidad() {
        return $this->belongsTo(\App\Models\Tespecialidad::class, 'cespecialidad', 'cespecialidad');
    }

    public function tinstitucion() {
        return $this->belongsTo(\App\Models\Tinstitucion::class, 'cinstitucion', 'cinstitucion');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
