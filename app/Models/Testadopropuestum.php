<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testadopropuestum extends Model {

    /**
     * Generated
     */

    protected $table = 'testadopropuesta';
    protected $fillable = ['cestadopropuesta', 'descripcion'];


    public function tpropuesta() {
        return $this->hasMany(\App\Models\Tpropuestum::class, 'cestadopropuesta', 'cestadopropuesta');
    }


}
