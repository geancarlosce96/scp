<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Talergia extends Model {

    /**
     * Generated
     */
   
    protected $primaryKey='calergias';
    protected $table = 'talergias';
    protected $fillable = ['calergias', 'descripcion'];
    public $timestamps = false;

    public function tpersonas() {
        return $this->belongsToMany(\App\Models\Tpersona::class, 'tpersonadatosmedicosalergia', 'calergias', 'cpersona');
    }

    public function tpersonadatosmedicosalergia() {
        return $this->hasMany(\App\Models\Tpersonadatosmedicosalergium::class, 'calergias', 'calergias');
    }


}
