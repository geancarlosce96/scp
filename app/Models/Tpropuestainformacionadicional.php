<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestainformacionadicional extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cpropuesta';
    protected $table = 'tpropuestainformacionadicional';
    protected $fillable = ['cpropuesta', 'cdocaprobacion', 'nrodocaprobacion'];
    
    public $timestamps=false;
   

}
