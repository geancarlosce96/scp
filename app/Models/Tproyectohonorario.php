<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectohonorario extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cproyectohonorarios';
    protected $table = 'tproyectohonorarios';
    protected $fillable = ['cproyectohonorarios', 'cproyectoactividades', 'horas', 'xmlareadetalle', 'cproyecto_proveedor', 'cproyectosub', 'cestructuraproyecto'];

    public $timestamps = false;

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cproyecto_proveedor', 'cpersona');
    }

    public function tproyectoactividade() {
        return $this->belongsTo(\App\Models\Tproyectoactividade::class, 'cproyectoactividades', 'cproyectoactividades');
    }

    public function tproyectoestructuraparticipante() {
        return $this->belongsTo(\App\Models\Tproyectoestructuraparticipante::class, 'cestructuraproyecto', 'cestructuraproyecto');
    }

    public function tproyectosub() {
        return $this->belongsTo(\App\Models\Tproyectosub::class, 'cproyectosub', 'cproyectosub');
    }


}
