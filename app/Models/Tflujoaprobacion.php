<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tflujoaprobacion extends Model {

    /**
     * Generated
     */
    protected $primaryKey="cflujoaprobacion";

    protected $table = 'tflujoaprobacion';
    protected $fillable = ['cflujoaprobacion', 'csubsistema', 'tipoaprobacion', 'cproyecto', 'cpropuesta', 'cdocumentoparapry', 'ccondicionoperativa_aprobado', 'cpersona_aprobado', 'croldespliegue_aprobado', 'orden_aprobado', 'fregistro'];

    public $timestamps = false;


    public function tdocumentosparaproyecto() {
        return $this->belongsTo(\App\Models\Tdocumentosparaproyecto::class, 'cdocumentoparapry', 'cdocumentoparapry');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_aprobado', 'cpersona');
    }

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'croldespliegue_aprobado', 'croldespliegue');
    }

    public function tsubsistema() {
        return $this->belongsTo(\App\Models\Tsubsistema::class, 'csubsistema', 'csubsistema');
    }


}
