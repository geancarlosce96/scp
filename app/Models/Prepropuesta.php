<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Prepropuesta extends model
{
    protected $table = "prepropuesta";

    public function estado_toString()
    {
    	$estados = ["0" => "inactivo", "1" => "activo"];
    	return $estados[$this->estado];
    }
    
}
