<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoinformacionadicional extends Model {

    /**
     * Generated
     */

    protected $primaryKey ='cproyecto';
    protected $table = 'tproyectoinformacionadicional';

    protected $fillable = ['cproyecto', 'cunidadmineracontacto', 'cdocaprobacion', 'nrodocaprobacion', 'comentarioshr', 'cimagen', 'observacionhr', 'porcentajeadelanto', 'cformacotizacion', 'plazopagodias', 'diascorte','cpersona_cdocumentario','cpersona_cproyecto','lider1','lider2','fotoproyhr','fechacorte'];


    public $timestamps =false;
    public function tformacotizacion() {
        return $this->belongsTo(\App\Models\Tformacotizacion::class, 'cformacotizacion', 'cformacotizacion');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tunidadmineracontacto() {
        return $this->belongsTo(\App\Models\Tunidadmineracontacto::class, 'cunidadmineracontacto', 'cunidadmineracontacto');
    }


}
