<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttiposdocproyecto extends Model {

    /**
     * Generated
     */

    protected $table = 'ttiposdocproyecto';
    protected $fillable = ['ctipodocproy', 'descripcion'];


    public function tdocumentosparaproyectos() {
        return $this->hasMany(\App\Models\Tdocumentosparaproyecto::class, 'ctipodocproy', 'ctipodocproy');
    }


}
