<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TPlanificacion extends Model
{
    protected $table = 'tplanificacion';
    protected $primaryKey = 'cplanificacion';
    protected $fillable = ['komcliente', 'kominterno', 'lc', 'alc',
        'croproyecto','ram','listariesgos','updated_user','created_user','cproyecto'
    ];
}
