<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testadoproyecto extends Model {

    /**
     * Generated
     */
    protected $primaryKey ="cestadoproyecto";
    protected $table = 'testadoproyecto';
    protected $fillable = ['cestadoproyecto', 'descripcion'];

    public $timestamps= false;

    public function tproyectos() {
        return $this->hasMany(\App\Models\Tproyecto::class, 'cestadoproyecto', 'cestadoproyecto');
    }


}
