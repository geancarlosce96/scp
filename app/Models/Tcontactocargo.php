<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tcontactocargo extends Model {

    /**
     * Generated
     */
    protected $primaryKey='ccontactocargo';
    protected $table = 'tcontactocargo';
    protected $fillable = ['ccontactocargo', 'descripcion'];
    public $timestamps=false;

    public function tunidadmineracontactos() {
        return $this->hasMany(\App\Models\Tunidadmineracontacto::class, 'ccontactocargo', 'ccontactocargo');
    }


}
