<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TMonitoreo extends Model
{
    protected $table = 'tmonitoreo';
    protected $primaryKey = 'cmonitoreo';
    protected $fillable = ['encuesta', 'encuesta1', 'encuesta2', 'encuestafecha',
        'encuestafecha1','encuestafecha2','updated_user','created_user','cproyecto'
    ];
}
