<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;
use Carbon\Carbon;

class Tvistastablaspersona extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'tvistastablaspersona';

    protected $table = 'tvistastablaspersona';
    protected $fillable = ['tvistastablaspersona', 'cpersona', 'columnas', 'ctvistastablas', 'created_at','updated_at'];

    // public $timestamps = false;

    public static function columnas_id($cpersona,$tipo){

		$tabla = DB::table('tvistastablas')->where('ctvistastablas','=',$tipo)->get();

        $array_col = [];

        $col = Tvistastablaspersona::select('columnas')->where('cpersona','=',$cpersona)->where('ctvistastablas','=',$tipo)->first();
        if ($col) {
            $array_col = explode(',', $col->columnas);
        }
        // dd($array_col);
        $columnas_id = [];
        
        foreach ($tabla as $key => $cabecera) {
            $cab = explode(',', $cabecera->columnas);
            foreach ($cab as $key => $value) {
                $col = explode('-', $value);
                array_push($columnas_id,$col[0]);
            }
        };

        return $columnas_id;
	}

	public static function columnas_nombre($cpersona,$tipo){

		$tabla = DB::table('tvistastablas')->where('ctvistastablas','=',$tipo)->get();

        $array_col = [];

        $col = Tvistastablaspersona::select('columnas')->where('cpersona','=',$cpersona)->where('ctvistastablas','=',$tipo)->first();
        if ($col) {
            $array_col = explode(',', $col->columnas);
        }
        // dd($array_col);
        $columnas_nombre = [];

        foreach ($tabla as $key => $cabecera) {
            $cab = explode(',', $cabecera->columnas);
            foreach ($cab as $key => $value) {
                $col = explode('-', $value);
                array_push($columnas_nombre,$col[1]);
            }
        };

        // natcasesort($columnas_nombre);//Ordenar un array usando un algoritmo de "orden natural" insensible a mayúsculas-minúsculas

        return $columnas_nombre;
	}

	public static function selecion_opcion($cpersona,$tipo,$valor){

		$tabla = DB::table('tvistastablas')->where('ctvistastablas','=',$tipo)->get();

        $array_col = [];

        $col = Tvistastablaspersona::select('columnas')->where('cpersona','=',$cpersona)->where('ctvistastablas','=',$tipo)->first();
        if ($col) {
            $array_col = explode(',', $col->columnas);
        }
        // dd($array_col);
        $selecion_opcion = [];

        $columnas_id = Tvistastablaspersona::columnas_id($cpersona,$tipo);


        foreach ($columnas_id as $key => $val) {
            if (in_array($val, $array_col)) {
                $seleccionado=$valor;
            }else {
                $seleccionado='';
            }
            array_push($selecion_opcion,$seleccionado);
        };

        

        return $selecion_opcion;
	}

 	public static function guardarcustumizacion($cpersona,$tipo,$opciones){

        $columnas = Tvistastablaspersona::where('cpersona','=',$cpersona)->where('ctvistastablas','=',$tipo)->first();
       
        if (!$columnas) {
            $columnas = new Tvistastablaspersona();
        }

        if ($opciones != null) {
            $sele = implode(',', $opciones);
            $columnas->columnas = $sele;
        }else {
        	$sele = 0;
            $columnas->columnas = null;
        }

        $columnas->cpersona = $cpersona;
        $columnas->ctvistastablas = $tipo;
        $columnas->save();
        

        return $sele;
    }
}
