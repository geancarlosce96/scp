<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tperformance extends Model
{
    protected $table = 'tperformance';
    protected $primaryKey = 'cperformance';
    protected $fillable = ['cperformance', 'semana', 'porcentaje', 'cproyectoactividades','created_at','updated_at','carea','cproyecto'];

    
}
