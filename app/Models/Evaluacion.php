<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Evaluacion extends model
{
    protected $table = "evaluacion";

    public function calcular_duracion()
    {
    	if($this->duracion == 0)
    		$this->duracion = round($this->cantidad_preguntas_mostrar*2.5);
    }

    public function estado_descripcion()
    {
    	$estado = $this->estado;
    	
    	if($estado == 1)
            return "En proceso";
        else if($estado == 2)
            return "Aprobado";
        else if($estado == 3)
            return "Desaprobado";
        
        return "Pendiente";
    }

    public function calcular_minimo_aprobar()
    {
        $this->cantidad_preguntas_min = round($this->cantidad_preguntas_mostrar*0.7);
    }
    public function calcular_preguntas_mostrar()
    {
        $this->cantidad_preguntas_mostrar = ($this->cantidad_preguntas <= 20) ? $this->cantidad_preguntas : 20;
    }

    public function calcular_valores()
    {
        $this->calcular_preguntas_mostrar();
        $this->calcular_minimo_aprobar();
        $this->calcular_duracion();
    }

}
