<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestadisciplina extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cpropuesta';
    
    protected $table = 'tpropuestadisciplina';
    protected $fillable = ['cpropuestadisciplina', 'cpropuesta', 'cdisciplina', 'cpersona_rdp', 'escontratista', 'cpersona_proveedor'];

    public function tdisciplina() {
        return $this->belongsTo(\App\Models\Tdisciplina::class, 'cdisciplina', 'cdisciplina');
    }

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }


}
