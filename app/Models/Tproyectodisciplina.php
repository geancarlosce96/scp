<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectodisciplina extends Model {

    /**
     * Generated
     */
    protected $primaryKey ='cproyectodisciplina';
    protected $table = 'tproyectodisciplina';
    protected $fillable = ['cproyectodisciplina', 'cproyecto', 'cdisciplina', 'cpersona_rdp', 'escontratista', 'cpersona_proveedor'];


    public $timestamps = false;
    public function tdisciplina() {
        return $this->belongsTo(\App\Models\Tdisciplina::class, 'cdisciplina', 'cdisciplina');
    }

    public function tpersona_rdp() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_rdp', 'cpersona');
    }

    public function tpersona_prov() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_proveedor', 'cpersona');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }


}
