<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tfasesproyecto extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cfaseproyecto';
    protected $table = 'tfasesproyecto';
    protected $fillable = ['cfaseproyecto', 'codigo', 'descripcion'];
    public $timestamps=false;

    public function tleccionesaprendidas() {
        return $this->hasMany(\App\Models\Tleccionesaprendida::class, 'cfaseproyecto', 'cfaseproyecto');
    }

    public function tchecklists() {
        return $this->hasMany(\App\Models\Tchecklist::class, 'cfaseproyecto', 'cfaseproyecto');
    }


}
