<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoactividade extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cproyectoactividades';
    protected $table = 'tproyectoactividades';
    protected $fillable = ['cproyectoactividades', 'cproyecto', 'cactividad', 'codigoactvidad', 'descripcionactividad', 'numerodocumentos', 'numeroplanos', 'cpersona_proveedor', 'cproyectosub'];

    public $timestamps = false;

    public function tactividad() {
        return $this->belongsTo(\App\Models\Tactividad::class, 'cactividad', 'cactividad');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectosub() {
        return $this->belongsTo(\App\Models\Tproyectosub::class, 'cproyectosub', 'cproyectosub');
    }

    public function tproyectocronogramadetalles() {
        return $this->hasMany(\App\Models\Tproyectocronogramadetalle::class, 'cproyectoactividades', 'cproyectoactividades');
    }

    public function tproyectohonorarios() {
        return $this->hasMany(\App\Models\Tproyectohonorario::class, 'cproyectoactividades', 'cproyectoactividades');
    }


}
