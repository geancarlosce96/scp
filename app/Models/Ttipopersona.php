<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttipopersona extends Model {

    /**
     * Generated
     */

    protected $table = 'ttipopersona';
    protected $fillable = ['ctipopersona', 'descripcion'];


    public function tpersonas() {
        return $this->hasMany(\App\Models\Tpersona::class, 'ctipopersona', 'ctipopersona');
    }


}
