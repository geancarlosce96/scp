<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectodocumento extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cproyectodocumentos';
    protected $table = 'tproyectodocumentos';
    protected $fillable = ['cproyectodocumentos', 'cproyecto', 'codigodocumento', 'revisionactual', 'revisionanterior', 'xmldocanterior', 'fechadoc', 'cdocumentoparapry'];
    
    public $timestamps = false;

    public function tdocumentosparaproyecto() {
        return $this->belongsTo(\App\Models\Tdocumentosparaproyecto::class, 'cdocumentoparapry', 'cdocumentoparapry');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }


}
