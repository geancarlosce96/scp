<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tusuariopassword extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cusuario';
    protected $table = 'tusuariopassword';
    protected $fillable = ['cusuario', 'fhasta', 'fdesde', 'password', 'caduca'];
	public $timestamps = false;

    public function tusuario() {
        return $this->belongsTo(\App\Models\Tusuario::class, 'cusuario', 'cusuario');
    }


}
