<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Propuesta_opciones extends model
{
    protected $table = "propuesta_opciones";

    static public function actualizar($cpropuesta, $opcion, $cpersona = null){
        $opciones = Propuesta_opciones::where("cpropuesta", "=", $cpropuesta)->first();
        
        if($opciones == null)
            $opciones = new Propuesta_opciones();    

        switch ($opcion) {
            case "actividades": $opciones->actividades = 1;  break;
            case "disciplinas": $opciones->disciplinas = 1;  break;
            case "equipo": $opciones->equipo = 1;  break;
            case "gastos": $opciones->gastos = 1;  break;
            case "gastoslaboratorios": $opciones->gastoslaboratorios = 1;  break;
            case "horas": $opciones->horas = 1;  break;
            case "resumenGlobal": $opciones->resumenglobal = 1;  break;
            case "resumenHH": $opciones->resumenhh = 1;  break;
            case "tags": $opciones->tags = 1;  break;
        }

        $opciones->cpropuesta = $cpropuesta;
        $opciones->save();

        $log = new Propuesta_opciones_log();
        $log->cpropuesta = $cpropuesta;
        $log->opcion = $opcion;
        $log->cpersona = $cpersona;
        $log->save();
    }

    static public function obtenerProximaOpcion($cpropuesta){
        $opciones = Propuesta_opciones::where("cpropuesta", "=", $cpropuesta)->first();

        if($opciones == null){
            $opciones = new Propuesta_opciones();
            $opciones->cpropuesta = $cpropuesta;
            $opciones->save();
        }

        if($opciones->equipo == 0) return "equipo";
        if($opciones->disciplinas == 0) return "disciplinas";
        if($opciones->tags == 0) return "tags";
        if($opciones->actividades == 0) return "actividades";
        if($opciones->horas == 0) return "horas";
        if($opciones->gastos == 0) return "gastos";
        if($opciones->gastoslaboratorios == 0) return "gastoslaboratorios";

        return "equipo";
    }

}
