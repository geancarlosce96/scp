<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectopersona extends Model {

    /**
     * Generated
     */

    protected $primaryKey = "cproyectopersona";
    protected $table = 'tproyectopersona';
    protected $fillable = ['cproyectopersona', 'cproyecto', 'cpersona', 'ccategoriaprofesional', 'cdisciplina'];

    public $timestamps = false;

    public function tcategoriaprofesional() {
        return $this->belongsTo(\App\Models\Tcategoriaprofesional::class, 'ccategoriaprofesional', 'ccategoriaprofesional');
    }

    public function tdisciplina() {
        return $this->belongsTo(\App\Models\Tdisciplina::class, 'cdisciplina', 'cdisciplina');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }


}
