<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tmenu extends Model {

    /**
     * Generated
     */

    protected $table = 'tmenu';
    protected $fillable = ['menuid', 'descripcion', 'ruta', 'estado', 'modulo'];



}
