<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tinstitucion extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cinstitucion';
    protected $table = 'tinstitucion';
    protected $fillable = ['cinstitucion', 'abrevia', 'descripcion', 'tipoinstitucion'];
    public $timestamps = false;

    public function tpersonaestudios() {
        return $this->hasMany(\App\Models\Tpersonaestudio::class, 'cinstitucion', 'cinstitucion');
    }


}
