<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tgastosejecucion extends Model {

    /**
     * Generated
     */
    protected $primaryKey ="cgastosejecucion";
    protected $table = 'tgastosejecucion';
    protected $fillable = ['cgastosejecucion', 'numerorendicion', 'descripcion', 'valorcambio', 'total', 'totaldolares', 'tipo_rendicion'];

    public $timestamps = false;

    public function tcentrocostos() {
        return $this->belongsToMany(\App\Models\Tcentrocosto::class, 'tgastosejecuciondetalle', 'cgastosejecucion', 'ccentrocosto');
    }

    public function tgastosejecuciondetalles() {
        return $this->hasMany(\App\Models\Tgastosejecuciondetalle::class, 'cgastosejecucion', 'cgastosejecucion');
    }


}
