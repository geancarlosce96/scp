<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestapaquete extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cpropuestapaquete';
    protected $table = 'tpropuestapaquetes';
    protected $fillable = ['cpropuestapaquete', 'cpropuesta', 'codigopaquete', 'nombrepaquete'];
    public $timestamps = false;

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestagastos() {
        return $this->hasMany(\App\Models\Tpropuestagasto::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }

    public function tpropuestagastoslaboratorios() {
        return $this->hasMany(\App\Models\Tpropuestagastoslaboratorio::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }

    public function tpropuestaactividades() {
        return $this->hasMany(\App\Models\Tpropuestaactividade::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }

    public function tpropuestahonorarios() {
        return $this->hasMany(\App\Models\Tpropuestahonorario::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }


}
