<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectocronograma extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cproyectocronograma';
    protected $table = 'tproyectocronograma';
    protected $fillable = ['cproyectocronograma', 'cproyecto', 'fregistro'];

    public $timestamps = false;


    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectocronogramadetalles() {
        return $this->hasMany(\App\Models\Tproyectocronogramadetalle::class, 'cproyectocronograma', 'cproyectocronograma');
    }

    public function tproyectocronogramaconstrucciondetalles() {
        return $this->hasMany(\App\Models\Tproyectocronogramaconstrucciondetalle::class, 'cproyectocronograma', 'cproyectocronograma');
    }


}
