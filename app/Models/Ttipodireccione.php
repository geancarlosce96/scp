<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttipodireccione extends Model {

    /**
     * Generated
     */

    protected $table = 'ttipodirecciones';
    protected $fillable = ['ctipodireccion', 'descripcion'];


    public function tpersonadirecciones() {
        return $this->hasMany(\App\Models\Tpersonadireccione::class, 'ctipodireccion', 'ctipodireccion');
    }


}
