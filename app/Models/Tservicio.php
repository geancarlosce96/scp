<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tservicio extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cservicio';
    protected $table = 'tservicio';
    protected $fillable = ['cservicio', 'descripcion','orden'];
    public $timestamps=false;


    public function tpropuesta() {
        return $this->hasMany(\App\Models\Tpropuestum::class, 'cservicio', 'cservicio');
    }

    public function tproyectos() {
        return $this->hasMany(\App\Models\Tproyecto::class, 'cservicio', 'cservicio');
    }


}
