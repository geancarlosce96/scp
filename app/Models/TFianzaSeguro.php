<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TFianzaSeguro extends Model
{
    protected $table = 'tfianzaseguros';
    //protected $dates = ['cf_inicio','cf_vigencia','srg_inicio','srg_vigencia','srp_inicio','srp_vigencia'];
    protected $primaryKey = 'cfianzaseguro';
    protected $fillable = ['cfianzaseguro', 'cf', 'cf_inicio','cf_vigencia',
                            'srg','srg_inicio','srg_vigencia',
                            'srp','srp_inicio','srp_vigencia','cproyecto'
                            ];
    public $timestamps = false;
}
