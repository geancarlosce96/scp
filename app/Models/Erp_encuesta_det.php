<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_encuesta_det extends model
{
    protected $table = "erp_encuesta_det";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_encuesta',
        'tipo_respuesta',
        'grupo',
        'tipo',
        'detalle',
        'orden',
        'id_respuesta',
        'texto_respuesta',
        'detalle',
        'detalle',
        'detalle',
    ];

}