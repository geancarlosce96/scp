<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonadatosfamiliare extends Model {

    /**
     * Generated
     */

    protected $primaryKey="cpersonafamiliar";
    protected $table = 'tpersonadatosfamiliares';
    protected $fillable = ['cpersonafamiliar', 'cpersona', 'parentesco', 'genero', 'ctipoidentificacion', 'identificacion', 'apaterno', 'amaterno', 'nombres'];

    public $timestamps = false;
    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
