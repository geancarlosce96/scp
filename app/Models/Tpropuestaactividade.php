<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestaactividade extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cpropuestaactividades';
    protected $table = 'tpropuestaactividades';
    protected $fillable = ['cpropuestaactividades', 'cpropuesta', 'cactividad', 'codigoactividad', 'descripcionactividad', 'numerodocumentos', 'numeroplanos', 'cpersona_proveedor', 'cpropuestapaquete'];

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestapaquete() {
        return $this->belongsTo(\App\Models\Tpropuestapaquete::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }

    public function tpropuestahonorarios() {
        return $this->hasMany(\App\Models\Tpropuestahonorario::class, 'cpropuestaactividades', 'cpropuestaactividades');
    }


}
