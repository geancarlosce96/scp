<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonajuridicainformacionbasica extends Model {

    /**
     * Generated
     */
    protected $primaryKey ='cpersona';
    protected $table = 'tpersonajuridicainformacionbasica';
    protected $fillable = ['cpersona', 'razonsocial', 'nombrecomercial', 'escliente', 'esproveedor', 'tag', 'web', 'logo', 'codigosig'];
	public $timestamps = false;

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
