<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Troldespliegue extends Model {

    /**
     * Generated
     */
    protected $primaryKey ='croldespliegue';
    protected $table = 'troldespliegue';
    protected $fillable = ['croldespliegue', 'codigorol', 'descripcionrol', 'tiporol','csubsistema'];
    public $timestamps = false;


    public function taprobcionesflujos() {
        return $this->hasMany(\App\Models\Taprobcionesflujo::class, 'crol_aprueba', 'croldespliegue');
    }

    public function tleccionesaprendidas() {
        return $this->hasMany(\App\Models\Tleccionesaprendida::class, 'crol_elaborado', 'croldespliegue');
    }

    public function tpropuestacronogramaconstruccions() {
        return $this->hasMany(\App\Models\Tpropuestacronogramaconstruccion::class, 'croldespliegue', 'croldespliegue');
    }

    public function testructuraparticipantes() {
        return $this->hasMany(\App\Models\Testructuraparticipante::class, 'croldespliegue', 'croldespliegue');
    }

    public function tproyectoejecucions() {
        return $this->hasMany(\App\Models\Tproyectoejecucion::class, 'crol_poraprobar', 'croldespliegue');
    }

    public function tproyectocronogramaconstrucciondetalles() {
        return $this->hasMany(\App\Models\Tproyectocronogramaconstrucciondetalle::class, 'croldespliegue', 'croldespliegue');
    }

    public function tproyectoestructuraparticipantes() {
        return $this->hasMany(\App\Models\Tproyectoestructuraparticipante::class, 'croldespliegue', 'croldespliegue');
    }


}
