<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thorasextrasaprobadas extends model
{
    protected $table = "thorasextrasaprobadas";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'thorasextras',
        'useraccion',
        'accionrealizada',
        'observacion',
        'idhorasextrassolicitudint',
    ];

}