<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttransmittalejecucion extends Model {

    /**
     * Generated
     */
    protected $primaryKey ='ctransmittalejecucion';
    protected $table = 'ttransmittalejecucion';
    protected $fillable = ['ctransmittalejecucion', 'ctipo', 'fecha', 'tipoenvio', 'nrotrasmittal', 'ctransmittal_cliente', 'cpersona_cdoc', 'ctransconfiguracion'];

    public $timestamps = false;

    public function ttransmittalconfiguracion() {
        return $this->belongsTo(\App\Models\Ttransmittalconfiguracion::class, 'ctransconfiguracion', 'ctransconfiguracion');
    }

    public function ttransmittalejecuciondetalles() {
        return $this->hasMany(\App\Models\Ttransmittalejecuciondetalle::class, 'ctransmittalejecucion', 'ctransmittalejecucion');
    }


}
