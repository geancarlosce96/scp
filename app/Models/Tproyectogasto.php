<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectogasto extends Model {

    /**
     * Generated
     */
    protected $primaryKey ='cproyectogastos';
    protected $table = 'tproyectogastos';
    protected $fillable = ['cproyectogastos', 'cproyecto', 'item', 'concepto', 'unidad', 'cantidad', 'costou', 'comentario', 'cpersona_proveedor', 'cproyectosub'];

    public $timestamps = false;

    public function tconceptogasto() {
        return $this->belongsTo(\App\Models\Tconceptogasto::class, 'concepto', 'cconcepto');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_proveedor', 'cpersona');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectosub() {
        return $this->belongsTo(\App\Models\Tproyectosub::class, 'cproyectosub', 'cproyectosub');
    }


}
