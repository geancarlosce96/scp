<?php

namespace App\Models;
use DB;

class TablaHoras
{
	const TIENE_HIJOS = 1;
	const NO_TIENE_HIJOS = 1;
	const NO_PERMITIR_EDICION = 0;
	const PERMITIR_EDICION = 1;

	//public	

	public function generar($cpropuesta)
	{
		$this->eliminarRegistrosActuales($cpropuesta);

		//generar
		$this->generarColumnas($cpropuesta);
	}

	public function obtener($cpropuesta)
	{
		$tabla = [];

		$tabla["columnas"] = TablaHorasColumna::where("cpropuesta", "=", $cpropuesta)
			->where("estado", "=", 1)->orderBy("num_orden", "asc")->get();		
		$tabla["filas"] = $this->obtenerFilas($cpropuesta);
		$tabla["celdas"] = TablaHorasCelda::where("cpropuesta", "=", $cpropuesta)
			->where("estado", "=", 1)->orderBy("id")->get();

		return $tabla;
	}

	public function guardarCeldas($cpropuesta, $celdas)
	{
		
		foreach ($celdas as $key => $celda) {
			if($celda['valor'] != ""){
				$cel = new TablaHorasCelda();
				$cel->valor = $celda['valor'];
				$cel->fila_codigo = $celda['fila'];
				$cel->columna_codigo = $celda['columna'];
				$cel->cpropuesta = $cpropuesta;
				$cel->save();	
			}			
		}

	}

	public function guardarCelda($cpropuesta, $celda)
	{
		$cel = TablaHorasCelda::where("columna_codigo", "=", $celda["columna"])
			->where("fila_codigo", "=", $celda["fila"])->first();

		if($cel == null){
			$cel = new TablaHorasCelda();
			$cel->fila_codigo = $celda['fila'];
			$cel->columna_codigo = $celda['columna'];
			$cel->cpropuesta = $cpropuesta;
		}

		$cel->valor = $celda['valor'];
		$cel->save();

		return $cel;
	}

	public function actualizarCeldas($cpropuesta, $celda)
	{
		$cel = TablaHorasCelda::where("id", "=", $celda["id"])->first();
		$cel->valor = $celda["valor"];
		$cel->save();

		return $cel;
	}

	public function obtenerTotalHorasSegunActividad($codigo)
	{
		$valores = DB::table('tabla_horas_celda')->select("valor")->where('fila_codigo', $codigo)->get();
		$total = 0;
		
		foreach ($valores as $key => $val)
			$total += intval($val->valor);

		return $total;
	}
	
	//private 

	private function obtenerFilas($cpropuesta)
	{
		$lista = Tpropuestaactividade::where("cpropuesta", "=", $cpropuesta)
			->select("cpropuestaactividades as id", "descripcionactividad as descripcion", 
				"codigoactividad as codigo", "hijos")
			->orderBy("codigoactividad", "asc")
			->orderBy("descripcionactividad", "asc")
			->get();

		foreach ($lista as $key => $elemento)
			$elemento->editable = ($elemento->hijos == 1) ? $this::NO_PERMITIR_EDICION : $this::PERMITIR_EDICION;

		return $lista;
	}

	private function eliminarRegistrosActuales($cpropuesta)
	{
		DB::table('tabla_horas_columna')->where('cpropuesta', '=', $cpropuesta)->delete();
		DB::table('tabla_horas_fila')->where('cpropuesta', '=', $cpropuesta)->delete();
		DB::table('tabla_horas_celda')->where('cpropuesta', '=', $cpropuesta)->delete();
	}

	private function generarColumnas($cpropuesta)
	{
		$col = null;
		$orden = 1; //numero de orden visualmente

		$orden = $this->generarColumnasPrevias($cpropuesta, $orden);
		$orden = $this->generarColumnasDisciplinas($cpropuesta, $orden);
		$orden = $this->generarColumnasPosteriores($cpropuesta, $orden);

		return $orden;
	}

	private function generarColumnasPrevias($cpropuesta, $orden = 1)
	{
		$plantilla = ["Actividad", "Revisor del Proyecto", "Gerente de Proyecto"];

		foreach ($plantilla as $key => $p) {
			$col = new TablaHorasColumna();
        	$col->cpropuesta = $cpropuesta;
        	$col->descripcion = $p;
        	$col->num_orden = $orden;
        	$col->editable = $this::NO_PERMITIR_EDICION;
        	$col->save();

        	$orden++;
		}

		return $orden;
	}

	private function generarColumnasDisciplinas($cpropuesta, $orden)
	{
		$disciplinas = DB::table("tpropuestadisciplina as p")
            ->join('tdisciplina as d','d.cdisciplina','=','p.cdisciplina')
            ->whereNotNull("created_at")            
            ->where("cpropuesta", "=", $cpropuesta)
            ->select("d.descripcion", "d.abrevia", "d.cdisciplina")
            ->orderBy("d.descripcion", "asc")
            ->get();

        foreach ($disciplinas as $key => $d) {
        	$col = new TablaHorasColumna();        	
        	$col->cpropuesta = $cpropuesta;
        	$col->descripcion = $d->descripcion;
        	$col->disciplina_id = $d->cdisciplina;
        	$col->editable = $this::NO_PERMITIR_EDICION;
        	$col->num_orden = $orden;
        	$col->save();

        	$orden++;
        	$orden = $this->generarColumnasDisciplinasProfesiones($cpropuesta, $orden, $d->cdisciplina);
        	$orden = $this->generarColumnasDisciplinasAdicionales($cpropuesta, $orden, $d->cdisciplina);
        }

        return $orden;
	}

	private function generarColumnasDisciplinasProfesiones($cpropuesta, $orden = 1, $disciplina_id)
	{
		$plantilla = Profesiones::obtener();

		foreach ($plantilla as $key => $p) {
			$col = new TablaHorasColumna();
        	$col->cpropuesta = $cpropuesta;
        	$col->descripcion = $p->nombre;
        	$col->profesion_id = $p->id;
        	$col->disciplina_id = $disciplina_id;
        	$col->num_orden = $orden;
        	$col->save();

        	$orden++;
		}

		return $orden;	
	}

	private function generarColumnasDisciplinasAdicionales($cpropuesta, $orden = 1, $disciplina_id)
	{
		$plantilla = ["Planos / Figuras"];

		foreach ($plantilla as $key => $p) {
			$col = new TablaHorasColumna();
        	$col->cpropuesta = $cpropuesta;
        	$col->descripcion = $p;
        	$col->disciplina_id = $disciplina_id;
        	$col->num_orden = $orden;
        	$col->save();

        	$orden++;
		}

		return $orden;
	}

	private function generarColumnasPosteriores($cpropuesta, $orden = 1)
	{
		$plantilla = ["Total"];

		foreach ($plantilla as $key => $p) {
			$col = new TablaHorasColumna();
        	$col->cpropuesta = $cpropuesta;
        	$col->descripcion = $p;
        	$col->num_orden = $orden;
        	$col->editable = $this::NO_PERMITIR_EDICION;
        	$col->save();
		}

		return $orden;
	}

	private function generarFilas($cpropuesta)
	{
		$orden = 1;
		$orden = $this->generarFilasActividades($cpropuesta);
	}

	private function generarFilasActividades($cpropuesta, $orden = 1)
	{
		$actividades = Tpropuestaactividade::where("cpropuesta", "=", $cpropuesta)
			->orderBy("codigoactividad", "asc")
			->orderBy("descripcionactividad", "asc")
			->get();

		foreach ($actividades as $key => $actividad) {
			$fila = new TablaHorasFila();
        	$fila->cpropuesta = $cpropuesta;
        	$fila->descripcion = $actividad->descripcionactividad;
        	$fila->num_orden = $orden;
        	
        	if($actividad->hijos == $this::TIENE_HIJOS)
        		$fila->editable = $this::NO_PERMITIR_EDICION;

        	$fila->save();

        	$orden++;
		}

		return $orden;
	}

}
