<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Toficina extends Model {

    /**
     * Generated
     */
    protected $primaryKey = "coficinas";
    protected $table = 'toficinas';
    protected $fillable = ['coficinas', 'csede', 'codigoofi', 'descripcion'];
    public $timestamps = false;

    public function tsede() {
        return $this->belongsTo(\App\Models\Tsede::class, 'csede', 'csede');
    }

    public function tpersonadatosempleados() {
        return $this->hasMany(\App\Models\Tpersonadatosempleado::class, 'coficinas', 'coficinas');
    }


}
