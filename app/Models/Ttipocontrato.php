<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttipocontrato extends Model {

    /**
     * Generated
     */
   
    protected $primaryKey='ctipocontrato';
    protected $table = 'ttipocontrato';
    protected $fillable = ['ctipocontrato', 'descripcion','horas'];
    public $timestamps = false;

  

}
