<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tactividad extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cactividad';
    protected $table = 'tactividad';
    protected $fillable = ['cactividad', 'codigo', 'descripcion', 'cactividad_parent'];
    public $timestamps=false;

    public function tactividad() {
        return $this->belongsTo(\App\Models\Tactividad::class, 'cactividad_parent', 'cactividad');
    }

    public function tactividads() {
        return $this->hasMany(\App\Models\Tactividad::class, 'cactividad_parent', 'cactividad');
    }

    public function tproyectoactividades() {
        return $this->hasMany(\App\Models\Tproyectoactividade::class, 'cactividad', 'cactividad');
    }

}
