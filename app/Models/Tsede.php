<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tsede extends Model {

    /**
     * Generated
     */
    protected $primaryKey='csede';
    protected $table = 'tsedes';
    protected $fillable = ['csede', 'codigosede', 'descripcion'];
    public $timestamps=false;

    public function toficinas() {
        return $this->hasMany(\App\Models\Toficina::class, 'csede', 'csede');
    }


}
