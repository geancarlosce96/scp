<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\TagServicio;

class TagServicioGrupo extends Model {
	protected $table = 'tag_servicio_grupo';

	static public function allWithServicios()
	{
		$tags = TagServicioGrupo::all();

		foreach ($tags as $key => $tag)
			$tag->items = TagServicio::where("servicio_grupal_id", "=", $tag->id)->get();			
		
		return $tags;
	}
}
