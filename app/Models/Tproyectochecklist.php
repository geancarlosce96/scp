<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectochecklist extends Model {

    /**
     * Generated
     */
    protected $primaryKey="cproyectochecklist";
    protected $table = 'tproyectochecklist';
    protected $fillable = ['cproyectochecklist', 'cproyecto', 'cchecklist', 'resultado', 'CHECK', 'porcentajeavance', 'observacion'];


    public $timestamps=false;

    public function tchecklist() {
        return $this->belongsTo(\App\Models\Tchecklist::class, 'cchecklist', 'cchecklist');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }


}
