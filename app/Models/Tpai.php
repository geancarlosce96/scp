<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpai extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cpais';
    protected $table = 'tpais';
    protected $fillable = ['cpais', 'descripcion'];
    public $timestamps=false;


}
