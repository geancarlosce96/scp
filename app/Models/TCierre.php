<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TCierre extends Model
{
    protected $table = 'tcierre';
    protected $primaryKey = 'ccierre';
    protected $fillable = ['acs', 'acscodigo', 'acsfechaenvio', 'acsfecharecepcion',
        'reunioncierre','correofin','listariesgos','updated_user','created_user','cproyecto'
    ];
}
