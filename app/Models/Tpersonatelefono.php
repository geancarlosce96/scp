<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonatelefono extends Model {
	use Traits\HasCompositePrimaryKey; 

    /**
     * Generated
     */

    protected $primaryKey = ['cpersona','snumerotelefono'];
    protected $table = 'tpersonatelefonos';
    protected $fillable = ['cpersona', 'snumerotelefono', 'ctipotelefono', 'telefono'];
	public $incrementing = false;
    public $timestamps = false;
    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
