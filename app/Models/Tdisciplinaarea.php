<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tdisciplinaarea extends Model {

    /**
     * Generated
     */

    protected $table = 'tdisciplinaareas';
    protected $fillable = ['cdisciplina', 'carea', 'activo'];


    public function tarea() {
        return $this->belongsTo(\App\Models\Tarea::class, 'carea', 'carea');
    }

    public function tdisciplina() {
        return $this->belongsTo(\App\Models\Tdisciplina::class, 'cdisciplina', 'cdisciplina');
    }

    public static function obtenerListado()
    {
        $disciplinas = DB::table('tdisciplinaareas as tda')
            ->join('tareas as ta','ta.carea','=','tda.carea')
            ->join('tdisciplina as td','td.cdisciplina','=','tda.cdisciplina')
            ->orderBy('ta.codigo')
            ->get();

        return $disciplinas;
    }

    public static function obtenerSegunPropuesta($cpropuesta)
    {
        $disciplinas = Tpropuestadisciplina::where("cpropuesta", "=", $cpropuesta)
            ->select("cdisciplina")
            ->get()
            ->toArray();

        for ($i=0; $i < count($disciplinas) ; $i++)
            $disciplinas[$i] = $disciplinas[$i]["cdisciplina"];

        return $disciplinas;
    }

    public static function obtenerSegunPropuestaConNombre($cpropuesta)
    {
        $disciplinas = DB::table("tpropuestadisciplina as p")
            ->join('tdisciplina as d','d.cdisciplina','=','p.cdisciplina')
            ->whereNotNull("created_at")            
            ->where("cpropuesta", "=", $cpropuesta)
            ->get();

        return $disciplinas;
    }


}
