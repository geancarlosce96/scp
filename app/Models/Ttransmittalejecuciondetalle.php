<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttransmittalejecuciondetalle extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'ctransmittalejedetalle';
    protected $table = 'ttransmittalejecuciondetalle';
    protected $fillable = ['ctransmittalejedetalle', 'item', 'codcliente', 'codanddes', 'descripcion', 'revision', 'cantidad', 'ctransmittalejecucion','comentarios','resultado'];

    public $timestamps = false;

    public function ttransmittalejecucion() {
        return $this->belongsTo(\App\Models\Ttransmittalejecucion::class, 'ctransmittalejecucion', 'ctransmittalejecucion');
    }


}
