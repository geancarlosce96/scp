<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_herramienta extends model
{
    protected $table = "erp_herramienta";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'codigo',
        'descripcion',
    ];

}