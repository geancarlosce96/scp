<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_encuesta_cab extends model
{
    protected $table = "erp_encuesta_cab";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dni_evaluado',
        'dni_evaluador',
        'tipo_evaluador',
        'perfil',
        'id_estado',
    ];

}