<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Planificacion extends model
{
    protected $table = "planificacion";

    public static function obtenerDisponibilidadPorPeriodoSemanaProyecto($periodo, $semana, $usuario)
	{
		//planificacion propia
		$horas = Planificacion::where("periodo", "=", $periodo)
				->where("semana", "=", $semana)
				->where("usuario", "=", $usuario)->first();

		if($horas != null)
			return $horas->disponibles;

		$horas = new Planificacion();
		$horas->disponibles = 40;

		return $horas->disponibles;
	}
    
}
