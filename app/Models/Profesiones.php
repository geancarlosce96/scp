<?php

namespace App\Models;
use DB;

class Profesiones
{
	public static function obtener(){
		$plantilla = ["Senior", "A", "B", "C", "E"];
		$lista = [];

		for ($i = 0; $i < count($plantilla); $i++) { 
			$obj = new \stdClass();
			$obj->id = $i + 1;
			$obj->nombre = $plantilla[$i];
			array_push($lista, $obj);			
		}

		return $lista;
	}

}
