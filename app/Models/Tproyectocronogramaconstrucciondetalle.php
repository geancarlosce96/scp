<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectocronogramaconstrucciondetalle extends Model {

    /**
     * Generated
     */

    protected $table = 'tproyectocronogramaconstrucciondetalle';
    protected $fillable = ['cproyectocronogramacons', 'cproyectocronograma', 'cpersonaempleado', 'xmldetalletareas', 'croldespliegue', 'cproyectosub'];


    public function tproyectosub() {
        return $this->belongsTo(\App\Models\Tproyectosub::class, 'cproyectosub', 'cproyectosub');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'croldespliegue', 'croldespliegue');
    }

    public function tproyectocronograma() {
        return $this->belongsTo(\App\Models\Tproyectocronograma::class, 'cproyectocronograma', 'cproyectocronograma');
    }

    public function tpersonas() {
        return $this->belongsToMany(\App\Models\Tpersona::class, 'tproyectoejecucionconstruccion', 'cproyectocronoconstrdetalle', 'cpersona_empleado');
    }

    public function tproyectoejecucionconstruccions() {
        return $this->hasMany(\App\Models\Tproyectoejecucionconstruccion::class, 'cproyectocronoconstrdetalle', 'cproyectocronogramacons');
    }


}
