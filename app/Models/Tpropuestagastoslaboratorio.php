<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestagastoslaboratorio extends Model {

    /**
     * Generated
     */

    protected $primaryKey = "cpropuestagastoslab";
    protected $table = 'tpropuestagastoslaboratorio';
    protected $fillable = ['cpropuestagastoslab', 'cpropuesta', 'item', 'concepto', 'preciou', 'cmoneda', 'cantcimentacion', 'cantcanteras', 'cantbotadero', 'cpersona_proveedor', 'cpropuestapaquete'];

    public function tconceptogasto() {
        return $this->belongsTo(\App\Models\Tconceptogasto::class, 'concepto', 'cconcepto');
    }

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestapaquete() {
        return $this->belongsTo(\App\Models\Tpropuestapaquete::class, 'cpropuestapaquete', 'cpropuestapaquete');
    }


}
