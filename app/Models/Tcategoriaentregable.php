<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tcategoriaentregable extends Model {

    /**
     * Generated
     */

    protected $primaryKey ="ccategoriaentregable";
    protected $table = 'tcategoriaentregable';
    protected $fillable = ['ccategoriaentregable', 'descripcion', 'activo'];

    public $timestamps = false;


    public function tproyectoentregables() {
        return $this->hasMany(\App\Models\Tproyectoentregable::class, 'ccategoriaentregable', 'ccategoriaentregable');
    }


}
