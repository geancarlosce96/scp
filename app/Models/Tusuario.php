<?php namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class Tusuario extends Model implements AuthenticatableContract{

    use Authenticatable;
    /**
     * Generated
     */
    protected $primaryKey ='cusuario';
    protected $table = 'tusuarios';
    protected $fillable = ['cusuario', 'nombre', 'estado', 'cpersona'];
    public $timestamps = false;

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function tusuariopasswords() {
        return $this->hasMany(\App\Models\Tusuariopassword::class, 'cusuario', 'cusuario');
    }

    public function tusuariosesiones() {
        return $this->hasMany(\App\Models\Tusuariosesione::class, 'cusuario', 'cusuario');
    }


}
