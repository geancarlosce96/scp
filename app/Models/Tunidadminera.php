<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tunidadminera extends Model {

    /**
     * Generated
     */
    
    protected $primaryKey ='cunidadminera';
    protected $table = 'tunidadminera';
    protected $fillable = ['cunidadminera', 'cpersona', 'cumineraestado', 'cpais', 'cubigeo', 'direccion', 'telefono', 'nombre'];

    public $timestamps = false;


    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function tunidadmineraestado() {
        return $this->belongsTo(\App\Models\Tunidadmineraestado::class, 'cumineraestado', 'cumineraestado');
    }

    public function tproyectos() {
        return $this->hasMany(\App\Models\Tproyecto::class, 'cunidadminera', 'cunidadminera');
    }

    static public function AllWithOrder()
    {
        return Tunidadminera::orderBy("nombre", "ASC")->get();
    }


}
