<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testadoentregable extends Model {

    /**
     * Generated
     */

    protected $table = 'testadoentregable';
    protected $fillable = ['cestadoentregable', 'descripcion', 'activo'];


    public function tproyectoentregables() {
        return $this->hasMany(\App\Models\Tproyectoentregable::class, 'cestadoentregable', 'cestadoentregable');
    }


}
