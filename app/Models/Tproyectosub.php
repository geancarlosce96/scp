<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectosub extends Model {

    /**
     * Generated
     */
    protected $primaryKey ='cproyectosub';
    protected $table = 'tproyectosub';
    protected $fillable = ['cproyectosub', 'cproyecto', 'codigosub', 'descripcionsub'];

    public $timestamps = false;

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectocronogramadetalles() {
        return $this->hasMany(\App\Models\Tproyectocronogramadetalle::class, 'cproyectosub', 'cproyectosub');
    }

    public function tproyectocronogramaconstrucciondetalles() {
        return $this->hasMany(\App\Models\Tproyectocronogramaconstrucciondetalle::class, 'cproyectosub', 'cproyectosub');
    }

    public function tproyectogastos() {
        return $this->hasMany(\App\Models\Tproyectogasto::class, 'cproyectosub', 'cproyectosub');
    }

    public function tproyectogastoslaboratorios() {
        return $this->hasMany(\App\Models\Tproyectogastoslaboratorio::class, 'cproyectosub', 'cproyectosub');
    }

    public function tproyectoactividades() {
        return $this->hasMany(\App\Models\Tproyectoactividade::class, 'cproyectosub', 'cproyectosub');
    }

    public function tproyectohonorarios() {
        return $this->hasMany(\App\Models\Tproyectohonorario::class, 'cproyectosub', 'cproyectosub');
    }


}
