<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tvacacionaprobada extends model
{
    protected $table = "tvacacionaprobada";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'idsolicitud',
        'useraccion',
        'accionrealizada',
        'observacion',
    ];

}