<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treglascondicion extends Model {

    /**
     * Generated
     */

    protected $table = 'treglascondicion';
    protected $fillable = ['creglacondicion', 'csubsistema', 'tipoaprobacion', 'ccondicionoperativa', 'proceso'];


    public function tsubsistema() {
        return $this->belongsTo(\App\Models\Tsubsistema::class, 'csubsistema', 'csubsistema');
    }


}
