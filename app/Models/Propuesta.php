<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Tpersona;
use App\Models\Contacto;
use DB;

class Propuesta extends model
{
    protected $table = "propuesta";

    public function generarCodigo()
    {
        //formado yy-mm-secuencial
        $cantidad = DB::table('propuesta')->count()+1;
        $this->codigo = date("y.m.").$cantidad;
    }
    
    public function estado_toString()
    {
    	$estados = ["0" => "inactivo", "1" => "activo"];
    	return $estados[$this->estado];
    }

    public function guardarTag($tag_estructuras, $tag_servicios)
    {
    	$detalles = [];

    	foreach ($tag_estructuras as $key => $item) {
    		array_push($detalles, 
    			['propuesta_id' => $this->id, 'propuesta_tag_estructura_id' => $item,
    			'created_at' => $this->created_at, 'updated_at' => $this->updated_at
    			]);
    	}

    	DB::table('propuesta_tag_estructura')->insert($detalles);
    	
    	$detalles = [];

    	foreach ($tag_servicios as $key => $item) {
    		array_push($detalles, 
    			['propuesta_id' => $this->id, 'propuesta_tag_servicio_id' => $item,
    			'created_at' => $this->created_at, 'updated_at' => $this->updated_at
    			]);
    	}

    	DB::table('propuesta_tag_servicio')->insert($detalles);
    }

    static public function allFields()
    {
        $lista = Propuesta::orderBy("propuesta", "asc")->get();
        $n = 1;
        foreach ($lista as $key => $item) {
            $item->n = $n;
            $item->cliente = Tpersona::find($item->cliente_id);
            $item->contacto = Contacto::find($item->contacto_id);
            $item->estado_toString = $item->estado_toString();
            $n++;
        }

        return $lista;
    }

    static public function info($cpropuesta)
    {
        $propuesta = DB::table('tpropuesta as tpro')
        ->join('tpersona as tper','tpro.cpersona','=','tper.cpersona')
        ->join('tunidadminera as tu','tpro.cunidadminera','=','tu.cunidadminera')
        ->where('tpro.cpropuesta','=', $cpropuesta)
        ->select('tpro.*','tper.nombre as cliente','tu.nombre as uminera')
        ->first();

        if($propuesta == null)
            return null;

        $opciones = Propuesta_opciones::where("cpropuesta", "=", $cpropuesta)->first();
        $propuesta->opciones = ($opciones == null) ? [] : $opciones;

        return $propuesta;
    }

    static public function obtenerTecnicos($cpropuesta)
    {
        $tecnicos = Propuesta_tecnico::where("cpropuesta", "=", $cpropuesta)->get();
        return $tecnicos;
    }

    static public function obtenerTecnicosToArray($cpropuesta)
    {
        $tecnicos = Propuesta::obtenerTecnicos($cpropuesta);
        $tecnicos = $tecnicos->toArray();

        for ($i=0; $i < count($tecnicos) ; $i++)
            $tecnicos[$i] = $tecnicos[$i]["persona_id"];

        return $tecnicos;
    }
    
}
