<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_encuesta_cat extends model
{
    protected $table = "erp_encuesta_cat";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'descripcion',
        'desde',
        'hasta',
    ];

}