<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tvacacionessolicitud extends model
{
    protected $table = "tvacacionessolicitud";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'totalvacionesacumuladas',
        'diassolicitados',
        'estadova',
        'tipovaca',
        'fdesde',
        'fhasta',
        'idempleado',
        'idjefeinmediato',
        'idjefesuperior',
        'aprobado',
    ];

}