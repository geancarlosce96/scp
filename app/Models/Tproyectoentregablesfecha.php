<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoentregablesfecha extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cproyectoentregablesfechas';

    protected $table = 'tproyectoentregablesfechas';
    protected $fillable = ['cproyectoentregablesfechas', 'cproyectoentregable', 'tipofecha', 'revision', 'fecha','ctipofechasentregable'];

    public $timestamps = false;

 
}
