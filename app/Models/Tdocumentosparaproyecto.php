<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tdocumentosparaproyecto extends Model {

    /**
     * Generated
     */
    protected $primaryKey = "cdocumentoparapry";
    protected $table = 'tdocumentosparaproyecto';
    protected $fillable = ['cdocumentoparapry', 'codigo', 'descripcion', 'ctipodocproy'];

    public $timestamps = false;

    public function ttiposdocproyecto() {
        return $this->belongsTo(\App\Models\Ttiposdocproyecto::class, 'ctipodocproy', 'ctipodocproy');
    }

    public function tproyectos() {
        return $this->belongsToMany(\App\Models\Tproyecto::class, 'tproyectodocumentos', 'cdocumentoparapry', 'cproyecto');
    }

    public function tproyectodocumentos() {
        return $this->hasMany(\App\Models\Tproyectodocumento::class, 'cdocumentoparapry', 'cdocumentoparapry');
    }


}
