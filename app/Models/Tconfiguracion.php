<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tconfiguracion extends Model {

    /**
     * Generated
     */   
    protected $primaryKey='cconfiguracion';
    protected $table = 'tconfiguracion';
    protected $fillable = ['cconfiguracion', 'sevidorsmtp','correo_remi_ale','correo_dest_not','tiemposesion','tiempocaducidad','tiempoper'];
    public $timestamps = false;

}