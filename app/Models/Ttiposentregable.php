<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttiposentregable extends Model {

    /**
     * Generated
     */
    protected $primaryKey = "ctiposentregable";
    protected $table = 'ttiposentregables';
    protected $fillable = ['ctiposentregable', 'descripcion'];
    public $timestamps = false;

    public function tdisciplinas() {
        return $this->belongsToMany(\App\Models\Tdisciplina::class, 'tentregables', 'ctipoentregable', 'cdisciplina');
    }

    public function tentregables() {
        return $this->hasMany(\App\Models\Tentregable::class, 'ctipoentregable', 'ctiposentregable');
    }


}
