<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttransmittalconfiguracion extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'ctransconfiguracion';
    protected $table = 'ttransmittalconfiguracion';
    protected $fillable = ['ctransconfiguracion', 'ctipo', 'cproyecto', 'cpropuesta', 'formato', 'nombreformato', 'archivoformato', 'tipoenvio'];

    public $timestamps = false;

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tunidadmineracontactos() {
        return $this->belongsToMany(\App\Models\Tunidadmineracontacto::class, 'ttransmittalconfiguracioncontacto', 'ctransconfiguracion', 'cunidadmineracontacto');
    }

    public function ttransmittalconfiguracioncontactos() {
        return $this->hasMany(\App\Models\Ttransmittalconfiguracioncontacto::class, 'ctransconfiguracion', 'ctransconfiguracion');
    }

    public function ttransmittalejecucions() {
        return $this->hasMany(\App\Models\Ttransmittalejecucion::class, 'ctransconfiguracion', 'ctransconfiguracion');
    }


}
