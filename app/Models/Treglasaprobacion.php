<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Treglasaprobacion extends Model {

    /**
     * Generated
     */

    protected $table = 'treglasaprobacion';
    protected $fillable = ['creglaaprobacion', 'csubsistema', 'tipoaprobacion', 'orden', 'ccondicionoperativa', 'roles_a', 'orden_a'];


    public function tsubsistema() {
        return $this->belongsTo(\App\Models\Tsubsistema::class, 'csubsistema', 'csubsistema');
    }


}
