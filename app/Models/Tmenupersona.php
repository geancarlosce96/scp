<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tmenupersona extends Model {

    /**
     * Generated
     */
	protected $primaryKey='cmenupersona';
    protected $table = 'tmenupersona';
    protected $fillable = ['cmenupersona', 'menuid', 'cpersona', 'permiso'];
	public $timestamps = false;


}
