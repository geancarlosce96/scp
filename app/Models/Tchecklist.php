<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tchecklist extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cchecklist';
    protected $table = 'tchecklist';
    protected $fillable = ['cchecklist', 'crol_despliegue', 'descripcionactvividad', 'cfaseproyecto'];
    public $timestamps=false;

    public function tfasesproyecto() {
        return $this->belongsTo(\App\Models\Tfasesproyecto::class, 'cfaseproyecto', 'cfaseproyecto');
    }

    public function tproyectos() {
        return $this->belongsToMany(\App\Models\Tproyecto::class, 'tproyectochecklist', 'cchecklist', 'cproyecto');
    }

    public function tproyectochecklists() {
        return $this->hasMany(\App\Models\Tproyectochecklist::class, 'cchecklist', 'cchecklist');
    }


}
