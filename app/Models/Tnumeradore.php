<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tnumeradore extends Model {

    /**
     * Generated
     */
     protected $primaryKey= 'cnumerador';
    protected $table = 'tnumeradores';
    protected $fillable = ['cnumerador', 'csubsistema', 'entidad', 'numero'];

    public $timestamps = false;



}
