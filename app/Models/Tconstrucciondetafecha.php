<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tconstrucciondetafecha extends Model {

    /**
     * Generated
     */
     protected $primaryKey = "cconstrucciondetafecha";
    protected $table = 'tconstrucciondetafecha';
    protected $fillable = ['cconstrucciondetafecha', 'cproyectocronogramacons', 'fecha', 'codactividad', 'nrohoras', 'observacion'];

    public $timestamps = false;


    public function tproyectocronogramaconstrucciondetalle() {
        return $this->belongsTo(\App\Models\Tproyectocronogramaconstrucciondetalle::class, 'cproyectocronogramacons', 'cproyectocronogramacons');
    }


}
