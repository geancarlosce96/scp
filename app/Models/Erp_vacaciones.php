<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_vacaciones extends model
{
    protected $table = "erp_vacaciones";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dni',
        'tiposaldo',
        'saldo',
        'saldo_temp',
    ];

}