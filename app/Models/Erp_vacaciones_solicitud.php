<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_vacaciones_solicitud extends model
{
    protected $table = "erp_vacaciones_solicitud";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'totalvacionesacumuladas',
        'diassolicitados',
        'estadova',
        'tipovaca',
        'fdesde',
        'fhasta',
    ];

}