<?php 
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Date;

class Capacitacion extends Model {    
    
    protected $table = 'capacitacion';

    public function generarCodigo(){
    	$this->codigo = "C".date("Y-m-").$this->id;
    	$this->save();
    }

}
