<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttransmittalconfiguracioncontacto extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'ctransmittalconfiguracioncontacto';
    protected $table = 'ttransmittalconfiguracioncontacto';
    protected $fillable = ['ctransmittalconfiguracioncontacto', 'cpersona_contacroanddes', 'cunidadmineracontacto', 'apellidos', 'ctransconfiguracion', 'nombres', 'ctipocontacto', 'ctipoinformacioncontacto', 'ccontactocargo', 'email', 'telefono'];

    public $timestamps = false;

    public function ttransmittalconfiguracion() {
        return $this->belongsTo(\App\Models\Ttransmittalconfiguracion::class, 'ctransconfiguracion', 'ctransconfiguracion');
    }

    public function tunidadmineracontacto() {
        return $this->belongsTo(\App\Models\Tunidadmineracontacto::class, 'cunidadmineracontacto', 'cunidadmineracontacto');
    }


}
