<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoejecucionconstruccion extends Model {

    /**
     * Generated
     */

    protected $primaryKey ='cproyectoejecucionconstruccion';
    protected $table = 'tproyectoejecucionconstruccion';
    protected $fillable = ['cproyectoejecucionconstruccion', 'cproyectocronoconstrdetalle', 'cpersona_empleado', 'fejecucion', 'estado', 'horasejecutadas', 'horasaprobadas', 'faprobacion', 'observacion', 'tareaconstruccion','fplanificado'];

    public $timestamps = false;


    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_empleado', 'cpersona');
    }

    public function tproyectocronogramaconstrucciondetalle() {
        return $this->belongsTo(\App\Models\Tproyectocronogramaconstrucciondetalle::class, 'cproyectocronoconstrdetalle', 'cproyectocronogramacons');
    }


}
