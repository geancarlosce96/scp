<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tenfermedad extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cenfermedad';
    protected $table = 'tenfermedad';
    protected $fillable = ['cenfermedad', 'descripcion'];
    public $timestamps=false;

    public function tpersonas() {
        return $this->belongsToMany(\App\Models\Tpersona::class, 'tpersonadatosmedicosenfermedad', 'cenfermedad', 'cpersona');
    }

    public function tpersonadatosmedicosenfermedads() {
        return $this->hasMany(\App\Models\Tpersonadatosmedicosenfermedad::class, 'cenfermedad', 'cenfermedad');
    }


}
