<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectocronogramadetalle extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cproyectocronogramadetalle';
    protected $table = 'tproyectocronogramadetalle';
    protected $fillable = ['cproyectocronogramadetalle', 'cproyectocronograma', 'item', 'cactividad', 'descripcion', 'idactproject', 'duracion', 'idpredecesora', 'idsucesora', 'idrecursoproject', 'monto', 'cproyectoactividades', 'cproyectoentregable', 'cproyectosub','finicio','ffin'];


    public $timestamps = false;
    public function tproyectoentregable() {
        return $this->belongsTo(\App\Models\Tproyectoentregable::class, 'cproyectoentregable', 'cproyectoentregables');
    }

    public function tproyectoactividade() {
        return $this->belongsTo(\App\Models\Tproyectoactividade::class, 'cproyectoactividades', 'cproyectoactividades');
    }

    public function tproyectocronograma() {
        return $this->belongsTo(\App\Models\Tproyectocronograma::class, 'cproyectocronograma', 'cproyectocronograma');
    }

    public function tproyectosub() {
        return $this->belongsTo(\App\Models\Tproyectosub::class, 'cproyectosub', 'cproyectosub');
    }

    public function tproyectoejecucions() {
        return $this->hasMany(\App\Models\Tproyectoejecucion::class, 'cproyectocronogramadetalle', 'cproyectocronogramadetalle');
    }


}
