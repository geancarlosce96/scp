<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tcentrocosto extends Model {

    /**
     * Generated
     */

    protected $primaryKey ="ccentrocosto";
    protected $table = 'tcentrocostos';
    protected $fillable = ['ccentrocosto', 'codigo', 'descripcion', 'tipocentrocosto'];

    public $timestamps = false;

    public function tgastosejecucions() {
        return $this->belongsToMany(\App\Models\Tgastosejecucion::class, 'tgastosejecuciondetalle', 'ccentrocosto', 'cgastosejecucion');
    }

    public function tgastosejecuciondetalles() {
        return $this->hasMany(\App\Models\Tgastosejecuciondetalle::class, 'ccentrocosto', 'ccentrocosto');
    }


}
