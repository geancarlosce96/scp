<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttiposnofacturable extends Model {

    /**
     * Generated
     */

    protected $primaryKey ="ctiponofacturable";
    protected $table = 'ttiposnofacturables';
    protected $fillable = ['ctiponofacturable', 'descripcion'];

    public $timestamps = false;


}
