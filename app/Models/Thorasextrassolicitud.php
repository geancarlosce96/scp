<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thorasextrassolicitud extends model
{
    protected $table = "thorasextrassolicitud";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fechahorasextras',
        'horassolicitadas',
        'estadohe',
        'cproyecto',
        'motivo',
        'idfechaiso',
        'idempleado',
        'idjefeinmediato',
        'idgerenteproyecto',
        'aprobado',
    ];

}