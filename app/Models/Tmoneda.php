<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tmoneda extends Model {

    /**
     * Generated
     */

    protected $table = 'tmonedas';
    protected $fillable = ['cmoneda', 'descripcion'];


    public function tconceptogastos() {
        return $this->hasMany(\App\Models\Tconceptogasto::class, 'cmoneda', 'cmoneda');
    }

    public function tpropuesta() {
        return $this->hasMany(\App\Models\Tpropuestum::class, 'cmoneda', 'cmoneda');
    }


}
