<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testructuraparticipante extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cestructurapropuesta';
    protected $table = 'testructuraparticipante';
    protected $fillable = ['cestructurapropuesta', 'cpropuesta', 'croldespliegue', 'cpersona_rol', 'tipoestructura', 'rate'];
    public $timestamps = false;

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_rol', 'cpersona');
    }

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'croldespliegue', 'croldespliegue');
    }

    public function tpropuestahonorarios() {
        return $this->hasMany(\App\Models\Tpropuestahonorario::class, 'cestructurapropuesta', 'cestructurapropuesta');
    }


}
