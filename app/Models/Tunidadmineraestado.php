<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tunidadmineraestado extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cumineraestado';
    protected $table = 'tunidadmineraestado';
    protected $fillable = ['cumineraestado', 'descripcion'];

    public $timestamps = false;

    public function tunidadmineras() {
        return $this->hasMany(\App\Models\Tunidadminera::class, 'cumineraestado', 'cumineraestado');
    }


}
