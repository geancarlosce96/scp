<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Propuesta_disciplina extends model
{
    protected $primaryKey = 'cpropuestadisciplina';
    protected $table = "tpropuestadisciplina";
}
