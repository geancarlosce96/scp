<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttipospropuestapresentacion extends Model {

    /**
     * Generated
     */

    protected $table = 'ttipospropuestapresentacion';
    protected $fillable = ['cpresentacion', 'descripcion'];


    public function tpropuesta() {
        return $this->belongsToMany(\App\Models\Tpropuestum::class, 'tpropuestapresentacion', 'cpresentacion', 'cpropuesta');
    }

    public function tpropuestapresentacions() {
        return $this->hasMany(\App\Models\Tpropuestapresentacion::class, 'cpresentacion', 'cpresentacion');
    }


}
