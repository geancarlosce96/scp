<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class XX_Migracion extends model
{
    protected $table = "xx_migracion";

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'proyectoid',
        'gasto_padre',
        'gastoid',
        'nombre_gasto',
        'asignado',
        'venta',
        'monto_total',
        'monto_igv',
        'igv',
        'otro_impuesto',
        'monto_impuesto',
        'submonto',
        'moneda',
        'fecha_rendicion',
        'fecha_factura',
        'estado_rendicion',
        'categoriaid',
        'areaid',
        'bill_cliente',
        'reembolsable',
        'num_factura',
        'proveedor',
        'laboratorio_cantidad',
        'laboratorio_PU',
        'descripcion',
        'numero_rendicion',
        'soles_monto_total',
        'dolar_monto_total',
        'nombre_rendicion',
        'monto_total_rendicion',
        'comentario',
        'monto_reembolso',
        'monto_cliente',
        'numero_completo',
        'dolar_monto_total',
        'dolar_monto_cliente',
        'dolar_monto_reembolso',
        'gasto_persona_id',
        'tipo_proyecto',

    ];

}
