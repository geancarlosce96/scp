<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestaconstrucciondetafecha extends Model {

    /**
     * Generated
     */
    protected $primaryKey ="cpropuestaconstrucciondetafecha";
    protected $table = 'tpropuestaconstrucciondetafecha';
    protected $fillable = ['cpropuestaconstrucciondetafecha', 'tcronogramaconstruccionid', 'fecha', 'codactividad', 'horas', 'observacion'];

    public $timestamps = false;
    


    public function tpropuestacronogramaconstruccion() {
        return $this->belongsTo(\App\Models\Tpropuestacronogramaconstruccion::class, 'tcronogramaconstruccionid', 'tcronogramaconstruccionid');
    }


}
