<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tusuariosesione extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cusuario';
    protected $table = 'tusuariosesiones';
    protected $fillable = ['cusuario', 'fhasta', 'fdesde', 'sesionid'];
    public $timestamps = false;

    public function tusuario() {
        return $this->belongsTo(\App\Models\Tusuario::class, 'cusuario', 'cusuario');
    }


}
