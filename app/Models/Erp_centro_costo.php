<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_centro_costo extends model
{
    protected $table = "erp_centrocosto";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'descripcion',
    ];

}