<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestacronogramaconstruccion extends Model {

    /**
     * Generated
     */
     protected $primaryKey ="tcronogramaconstruccionid";
    protected $table = 'tpropuestacronogramaconstruccion';
    protected $fillable = ['tcronogramaconstruccionid', 'cpropuesta', 'cpersonaempleado', 'croldespliegue', 'xmldetalletareas'];

    public $timestamps = false;

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersonaempleado', 'cpersona');
    }

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'croldespliegue', 'croldespliegue');
    }


}
