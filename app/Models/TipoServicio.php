<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoServicio extends model
{
    protected $table = "tipo_servicio";

    static public function AllWithOrder()
    {
    	return TipoServicio::orderBy("descripcion", "ASC")->get();
    }
    
}
