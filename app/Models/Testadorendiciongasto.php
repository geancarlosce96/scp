<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testadorendiciongasto extends Model {

    /**
     * Generated
     */

    protected $table = 'testadorendiciongasto';
    protected $fillable = ['cestadorendiciongasto', 'codigo', 'descripcion'];


    public function tgastosejecucions() {
        return $this->hasMany(\App\Models\Tgastosejecucion::class, 'cestadorendiciongasto', 'cestadorendiciongasto');
    }


}
