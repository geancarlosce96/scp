<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tmonedacambio extends Model {

    /**
     * Generated
     */

    protected $primaryKey ='cmonedacambio';
    protected $table = 'tmonedacambio';
    protected $fillable = ['cmonedacambio','cmoneda', 'cmonedavalor', 'fecha', 'valorcompra', 'valorventa'];

    public $timestamps=false;


    public function tmoneda() {
        return $this->belongsTo(\App\Models\Tmoneda::class, 'cmoneda', 'cmoneda');
    }


}
