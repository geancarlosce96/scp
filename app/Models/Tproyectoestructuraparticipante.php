<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoestructuraparticipante extends Model {

    /**
     * Generated
     */

    protected $primaryKey = "cestructuraproyecto";
    protected $table = 'tproyectoestructuraparticipantes';
    protected $fillable = ['cestructuraproyecto', 'cproyecto', 'croldespliegue', 'cpersona_rol', 'tipoestructura', 'rate'];

    public $timestamps = false;


    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona_rol', 'cpersona');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'croldespliegue', 'croldespliegue');
    }

    public function tproyectohonorarios() {
        return $this->hasMany(\App\Models\Tproyectohonorario::class, 'cestructuraproyecto', 'cestructuraproyecto');
    }


}
