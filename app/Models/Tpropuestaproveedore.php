<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestaproveedore extends Model {

    /**
     * Generated
     */

    protected $table = 'tpropuestaproveedores';
    protected $fillable = ['cpropuesta', 'cpersona', 'activo'];


    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function tpropuestum() {
        return $this->belongsTo(\App\Models\Tpropuestum::class, 'cpropuesta', 'cpropuesta');
    }


}
