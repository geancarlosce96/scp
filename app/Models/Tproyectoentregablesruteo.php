<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoentregablesruteo extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cproyectoentregablesruteo';

    protected $table = 'tproyectoentregablesruteo';
    protected $fillable = ['cproyectoentregablesruteo', 'cproyectoentregable', 'ruteo_actual', 'ruteo_siguiente', 'revision','fecha','cpersona'];

    public $timestamps = false;

 
}
