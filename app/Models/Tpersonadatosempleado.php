<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonadatosempleado extends Model {

    /**
     * Generated
     */
     protected $primaryKey='cpersona';

    protected $table = 'tpersonadatosempleado';
    protected $fillable = ['cpersona', 'sdatoslaborales', 'codigosig', 'carea', 'ccargo', 'fingreso', 'ftermino', 'cmoneda1', 'rate1', 'cmoneda2', 'rate2', 'cmoneda3', 'rate3', 'cmoneda4', 'rate4', 'cmoneda5', 'rate5', 'estado', 'cmotivo', 'observacion', 'coficinas','ctipocontrato'];

    public $timestamps = false;



    public function tarea() {
        return $this->belongsTo(\App\Models\Tarea::class, 'carea', 'carea');
    }

    public function tcargo() {
        return $this->belongsTo(\App\Models\Tcargo::class, 'ccargo', 'ccargo');
    }

    public function toficina() {
        return $this->belongsTo(\App\Models\Toficina::class, 'coficinas', 'coficinas');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function ttipocontacto() {
        return $this->belongsTo(\App\Models\Ttipocontrato::class, 'ctipocontrato', 'ctipocontrato');
    }


}
