<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tformacotizacion extends Model {

    /**
     * Generated
     */

    protected $table = 'tformacotizacion';
    protected $fillable = ['cformacotizacion', 'descripcion'];


    public function tpropuesta() {
        return $this->hasMany(\App\Models\Tpropuestum::class, 'cformacotizacion', 'cformacotizacion');
    }


}
