<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonarol extends Model {

    /**
     * Generated
     */

    $primaryKey="cpersona_rol";
    protected $table = 'tpersonarol';
    protected $fillable = ['cpersona_rol', 'cpersona', 'croldespliegue', 'estado'];


    $timestamps = false;
    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'croldespliegue', 'croldespliegue');
    }


}
