<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tarea extends Model {

    /**
     * Generated
     */
    protected $primaryKey='carea';
    protected $table = 'tareas';
    protected $fillable = ['carea', 'codigo', 'descripcion', 'siglas', 'carea_parent','esgeneral','estecnica','esarea'];
    
    public $timestamps=false;

    public function tarea() {
        return $this->belongsTo(\App\Models\Tarea::class, 'carea_parent', 'carea');
    }

    public function tleccionesaprendidas() {
        return $this->hasMany(\App\Models\Tleccionesaprendida::class, 'carea', 'carea');
    }

    public function tareas() {
        return $this->hasMany(\App\Models\Tarea::class, 'carea_parent', 'carea');
    }

    public function tpersonadatosempleados() {
        return $this->hasMany(\App\Models\Tpersonadatosempleado::class, 'carea', 'carea');
    }


}
