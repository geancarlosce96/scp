<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tconceptogasto extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cconcepto';
    protected $table = 'tconceptogastos';
    protected $fillable = ['cconcepto', 'descripcion', 'unidad', 'cmoneda', 'costounitario', 'tipogasto','cconcepto_parent','categoria_gasto','estado'];

    public $timestamps = false;
    public function tmoneda() {
        return $this->belongsTo(\App\Models\Tmoneda::class, 'cmoneda', 'cmoneda');
    }

    public function tpropuestagastos() {
        return $this->hasMany(\App\Models\Tpropuestagasto::class, 'concepto', 'cconcepto');
    }

    public function tpropuestagastoslaboratorios() {
        return $this->hasMany(\App\Models\Tpropuestagastoslaboratorio::class, 'concepto', 'cconcepto');
    }

    public function tproyectogastos_pry() {
        return $this->hasMany(\App\Models\Tproyectogasto::class, 'concepto', 'cconcepto');
    }

    public function tproyectogastoslaboratorios_pry() {
        return $this->hasMany(\App\Models\Tproyectogastoslaboratorio::class, 'concepto', 'cconcepto');
    }

    public static function allWithOrder($tipogasto = "00001")
    {
        $gastos = Tconceptogasto::where("tipogasto", "=", $tipogasto)->orderBy("cconcepto")->get();

        foreach ($gastos as $key => $gasto) {
            $gasto->cantidad = 0; 
            $gasto->costou = 0;
            $gasto->comentario = '';
            $gasto->cantcimentacion = 0;
            $gasto->preciou = 0;
        }

        return $gastos;
    }

}
