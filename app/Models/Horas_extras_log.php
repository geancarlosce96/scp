<?php
/**
 * Created by PhpStorm.
 * User: lardinarmas
 * Date: 12/03/19
 * Time: 11:57 AM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Horas_extras_log extends model
{

    protected  $table= "hora_extras_log";

    protected $fillable = [
        'id',
        'id_solicitud_horas_extras',
        'estado_anterior',
        'estado_actual',
        'persona_aprobada',
    ];

}