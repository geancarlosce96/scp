<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ttiposinformacioncontacto extends Model {

    /**
     * Generated
     */
	protected $primaryKey='ctipoinformacioncontacto';
    protected $table = 'ttiposinformacioncontacto';
    protected $fillable = ['ctipoinformacioncontacto', 'descripcion'];
    public $timestamps=false;



    public function tunidadmineracontactos() {
        return $this->hasMany(\App\Models\Tunidadmineracontacto::class, 'ctipoinformacioncontacto', 'ctipoinformacioncontacto');
    }


}
