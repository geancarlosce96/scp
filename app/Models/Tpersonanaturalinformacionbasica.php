<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonanaturalinformacionbasica extends Model {

    /**
     * Generated
     */

    protected $primaryKey='cpersona';
    protected $table = 'tpersonanaturalinformacionbasica';
    protected $fillable = ['cpersona', 'apaterno', 'amaterno', 'nombres', 'fnacimiento', 'esempleado', 'ctipoidentificacion', 'identificacion', 'genero', 'estadocivil', 'cnacionalidad'];

    public $timestamps = false;

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
