<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Thorasextrascontrol extends model
{
    protected $table = "thorasextrascontrol";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'dia',
        'horarioentrada',
        'horariosalida',
        'maxhoras',
        'idiso',
        'aprobado',
    ];

}