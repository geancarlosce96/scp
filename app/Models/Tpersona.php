<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tpersona extends Model {

    /**
     * Generated
     */
    protected $primaryKey ='cpersona';
    protected $table = 'tpersona';
    protected $fillable = ['cpersona', 'ctipopersona', 'ctipoidentificacion', 'identificacion', 'nombre'];
    public $timestamps = false;

    public function ttipoidentificacion() {
        return $this->belongsTo(\App\Models\Ttipoidentificacion::class, 'ctipoidentificacion', 'ctipoidentificacion');
    }

    public function ttipopersona() {
        return $this->belongsTo(\App\Models\Ttipopersona::class, 'ctipopersona', 'ctipopersona');
    }

    public function talergias() {
        return $this->belongsToMany(\App\Models\Talergia::class, 'tpersonadatosmedicosalergia', 'cpersona', 'calergias');
    }

    public function tenfermedads() {
        return $this->belongsToMany(\App\Models\Tenfermedad::class, 'tpersonadatosmedicosenfermedad', 'cpersona', 'cenfermedad');
    }

    public function tproyectocronogramaconstrucciondetalles() {
        return $this->belongsToMany(\App\Models\Tproyectocronogramaconstrucciondetalle::class, 'tproyectoejecucionconstruccion', 'cpersona_empleado', 'cproyectocronoconstrdetalle');
    }

    public function taprobcionesflujos() {
        return $this->hasMany(\App\Models\Taprobcionesflujo::class, 'cpersona_aprobada', 'cpersona');
    }

    public function tleccionesaprendidas() {
        return $this->hasMany(\App\Models\Tleccionesaprendida::class, 'cpersona_elaborado', 'cpersona');
    }

    public function tleccionesaprendidas_aprobado() {
        return $this->hasMany(\App\Models\Tleccionesaprendida::class, 'cpersona_aprobado', 'cpersona');
    }

    public function tpersonadatosfamiliares() {
        return $this->hasMany(\App\Models\Tpersonadatosfamiliare::class, 'cpersona', 'cpersona');
    }

    public function tpersonadatosmedicos() {
        return $this->hasMany(\App\Models\Tpersonadatosmedico::class, 'cpersona', 'cpersona');
    }

    public function tpersonadatosmedicosalergia() {
        return $this->hasMany(\App\Models\Tpersonadatosmedicosalergium::class, 'cpersona', 'cpersona');
    }

    public function tpersonadatosmedicosenfermedads() {
        return $this->hasMany(\App\Models\Tpersonadatosmedicosenfermedad::class, 'cpersona', 'cpersona');
    }

    public function tpersonaestudios() {
        return $this->hasMany(\App\Models\Tpersonaestudio::class, 'cpersona', 'cpersona');
    }

    public function tpersonatelefonos() {
        return $this->hasMany(\App\Models\Tpersonatelefono::class, 'cpersona', 'cpersona');
    }

    public function tpropuestacronogramaconstruccions() {
        return $this->hasMany(\App\Models\Tpropuestacronogramaconstruccion::class, 'cpersonaempleado', 'cpersona');
    }

    public function testructuraparticipantes() {
        return $this->hasMany(\App\Models\Testructuraparticipante::class, 'cpersona_rol', 'cpersona');
    }

    public function tproyectodisciplinas_rdp() {
        return $this->hasMany(\App\Models\Tproyectodisciplina::class, 'cpersona_rdp', 'cpersona');
    }

    public function tproyectodisciplinas() {
        return $this->hasMany(\App\Models\Tproyectodisciplina::class, 'cpersona_proveedor', 'cpersona');
    }

    public function tproyectoejecucions() {
        return $this->hasMany(\App\Models\Tproyectoejecucion::class, 'cpersona', 'cpersona');
    }

    public function tproyectoejecucionconstruccions() {
        return $this->hasMany(\App\Models\Tproyectoejecucionconstruccion::class, 'cpersona_empleado', 'cpersona');
    }

    public function tproyectogastos() {
        return $this->hasMany(\App\Models\Tproyectogasto::class, 'cpersona_proveedor', 'cpersona');
    }

    public function tproyectogastoslaboratorios() {
        return $this->hasMany(\App\Models\Tproyectogastoslaboratorio::class, 'cpersona_proveedor', 'cpersona');
    }

    public function tproyectoestructuraparticipantes() {
        return $this->hasMany(\App\Models\Tproyectoestructuraparticipante::class, 'cpersona_rol', 'cpersona');
    }

    public function tproyectohonorarios() {
        return $this->hasMany(\App\Models\Tproyectohonorario::class, 'cproyecto_proveedor', 'cpersona');
    }

    public function tproyectos_cliente() {
        return $this->hasMany(\App\Models\Tproyecto::class, 'cpersonacliente', 'cpersona');
    }

    public function tproyectos() {
        return $this->hasMany(\App\Models\Tproyecto::class, 'cpersona_coordinador', 'cpersona');
    }

    public function tproyectos_gte() {
        return $this->hasMany(\App\Models\Tproyecto::class, 'cpersona_gerente', 'cpersona');
    }

    public function tunidadmineras() {
        return $this->hasMany(\App\Models\Tunidadminera::class, 'cpersona', 'cpersona');
    }

    public function tusuarios() {
        return $this->hasMany(\App\Models\Tusuario::class, 'cpersona', 'cpersona');
    }

    public function tpersonadatosempleado() {
        return $this->hasOne(\App\Models\Tpersonadatosempleado::class, 'cpersona', 'cpersona');
    }

    public function tpersonadireccione() {
        return $this->hasOne(\App\Models\Tpersonadireccione::class, 'cpersona', 'cpersona');
    }

    public function tpersonajuridicainformacionbasica() {
        return $this->hasOne(\App\Models\Tpersonajuridicainformacionbasica::class, 'cpersona', 'cpersona');
    }

    public function tpersonanaturalinformacionbasica() {
        return $this->hasOne(\App\Models\Tpersonanaturalinformacionbasica::class, 'cpersona', 'cpersona');
    }

    static public function AllClientsWithOrder()
    {
        return Tpersona::where("ctipopersona", "=", "JUR")->orderBy("nombre", "ASC")->get();
    }

    public static function listapersonal($rol)
    {
        $personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->join('tpersonarol as pr','tpersona.cpersona','=','pr.cpersona')
        ->join('tusuarios as tu','tpersona.cpersona','=','tu.cpersona')
        ->join('troldespliegue as rol','pr.croldespliegue','=','rol.croldespliegue')
        ->where('tnat.esempleado','=','1')
        ->where('rol.codigorol','=', $rol)
        ->where('pr.estado','=','001')
        ->where('tu.estado','=','ACT')
        ->select('tpersona.abreviatura','tpersona.cpersona')
        ->orderBy('tpersona.abreviatura')
        ->get();

        return $personal;
    }


}
