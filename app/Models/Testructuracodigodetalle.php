<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testructuracodigodetalle extends Model {

    /**
     * Generated
     */

    protected $table = 'testructuracodigodetalle';
    protected $fillable = ['cestructuracodigodetalle', 'sorden', 'metodo', 'cestructuracodigo'];


    public function testructuracodigo() {
        return $this->belongsTo(\App\Models\Testructuracodigo::class, 'cestructuracodigo', 'cestructuracodigo');
    }


}
