<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Testructuracodigo extends Model {

    /**
     * Generated
     */

    protected $table = 'testructuracodigo';
    protected $fillable = ['cestructuracodigo', 'descripcion', 'observaciones'];


    public function testructuracodigodetalles() {
        return $this->hasMany(\App\Models\Testructuracodigodetalle::class, 'cestructuracodigo', 'cestructuracodigo');
    }


}
