<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tgastosejecuciondetalle extends Model {

    /**
     * Generated
     */
    protected $primaryKey ="cgastosejecuciondet";
    protected $table = 'tgastosejecuciondetalle';
    protected $fillable = ['cgastosejecuciondet', 'cgastosejecucion', 'ccentrocosto', 'tipoactividad', 'cproyecto', 'cpropuesta', 'carea', 'tipogasto', 'descripcion', 'reembolsable', 'tipodocumento', 'numerodocumento', 'cpersona_cliente', 'ruc_cliente', 'razon_cliente', 'valorcambio', 'cmoneda', 'cantidad', 'preciounitario', 'subtotal', 'impuesto', 'total', 'cpersona_empleado', 'ccondicionoperativa','fdocumento'];

    public $timestamps = false;

    public function tcentrocosto() {
        return $this->belongsTo(\App\Models\Tcentrocosto::class, 'ccentrocosto', 'ccentrocosto');
    }

    public function tgastosejecucion() {
        return $this->belongsTo(\App\Models\Tgastosejecucion::class, 'cgastosejecucion', 'cgastosejecucion');
    }


}
