<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Tpropuesta extends model
{
    protected $primaryKey = "cpropuesta";
    protected $table = "tpropuesta";

    public function guardarTag($tag_estructuras, $tag_servicios, $cpropuesta = null)
    {
    	$detalles = [];

        DB::table('propuesta_tag_estructura')->where("propuesta_id", "=", $cpropuesta)->delete();
        DB::table('propuesta_tag_servicio')->where("propuesta_id", "=", $cpropuesta)->delete();

    	foreach ($tag_estructuras as $key => $item) {
    		array_push($detalles, 
    			['propuesta_id' => $cpropuesta, 'propuesta_tag_estructura_id' => $item,
    			'created_at' => $this->created_at, 'updated_at' => $this->updated_at
    			]);
    	}

    	DB::table('propuesta_tag_estructura')->insert($detalles);
    	
    	$detalles = [];

    	foreach ($tag_servicios as $key => $item) {
    		array_push($detalles, 
    			['propuesta_id' => $cpropuesta, 'propuesta_tag_servicio_id' => $item,
    			'created_at' => $this->created_at, 'updated_at' => $this->updated_at
    			]);
    	}

    	DB::table('propuesta_tag_servicio')->insert($detalles);
    }
}
