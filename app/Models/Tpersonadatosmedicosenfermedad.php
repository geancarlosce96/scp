<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonadatosmedicosenfermedad extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cpersonaenfermedad';
    protected $table = 'tpersonadatosmedicosenfermedad';
    protected $fillable = ['cpersonaenfermedad', 'cpersona', 'cenfermedad', 'observacion'];

    public $timestamps = false;


    public function tenfermedad() {
        return $this->belongsTo(\App\Models\Tenfermedad::class, 'cenfermedad', 'cenfermedad');
    }

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
