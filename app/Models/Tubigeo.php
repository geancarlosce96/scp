<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tubigeo extends Model {

    /**
     * Generated
     */
    protected $primaryKey=['cpais','cubigeo'];
    protected $table = 'tubigeo';
    protected $fillable = ['cpais', 'cubigeo', 'descripcion'];

    public $timestamps = false;

    public function tpersonadirecciones() {
        return $this->hasMany(\App\Models\Tpersonadireccione::class, 'cubigeo', 'cubigeo');
    }


}
