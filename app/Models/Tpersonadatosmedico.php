<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonadatosmedico extends Model {

    /**
     * Generated
     */

    protected $table = 'tpersonadatosmedicos';
    protected $fillable = ['cpersonadatosmedicos', 'cpersona', 'gruposanguineo', 'essalud', 'eps', 'epscompania', 'segonco', 'segoncocompania', 'segvida'];


    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }


}
