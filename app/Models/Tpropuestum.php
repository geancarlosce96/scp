<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpropuestum extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'cpropuesta';
    protected $table = 'tpropuesta';
    protected $fillable = ['cpropuesta', 'cpersona', 'cunidadminera', 'ccodigo', 'nombre', 'descripcion', 'tag', 'cformacotizacion', 'revision', 'ctipopropuesta', 'cservicio', 'cestadopropuesta', 'cpersona_resppropuesta', 'cpersona_revprincipal', 'cpersona_gteproyecto', 'cpersona_coordinadorpropuesta', 'cmoneda', 'cmedioentrega', 'observaciones', 'fentregabases', 'fconsulta', 'fabsolucionconsultas', 'fpresentacion', 'frespuesta', 'fcharla', 'fvtecnica', 'ftipo', 'ccondicionoperativa', 'csubsistema', 'cproyecto','fpropuesta','csede'];
    public $timestamps = false;

    public function tcondicionesoperativa() {
        return $this->belongsTo(\App\Models\Tcondicionesoperativa::class, 'ccondicionoperativa', 'ccondicionoperativa');
    }

    public function testadopropuestum() {
        return $this->belongsTo(\App\Models\Testadopropuestum::class, 'cestadopropuesta', 'cestadopropuesta');
    }

    public function tformacotizacion() {
        return $this->belongsTo(\App\Models\Tformacotizacion::class, 'cformacotizacion', 'cformacotizacion');
    }

    public function tmedioentrega() {
        return $this->belongsTo(\App\Models\Tmedioentrega::class, 'cmedioentrega', 'cmedioentrega');
    }

    public function tmoneda() {
        return $this->belongsTo(\App\Models\Tmoneda::class, 'cmoneda', 'cmoneda');
    }

    public function tservicio() {
        return $this->belongsTo(\App\Models\Tservicio::class, 'cservicio', 'cservicio');
    }

    public function ttipopropuestum() {
        return $this->belongsTo(\App\Models\Ttipopropuestum::class, 'ctipopropuesta', 'ctipopropuesta');
    }

    public function tdisciplinas() {
        return $this->belongsToMany(\App\Models\Tdisciplina::class, 'tpropuestadisciplina', 'cpropuesta', 'cdisciplina');
    }

    public function ttipospropuestapresentacions() {
        return $this->belongsToMany(\App\Models\Ttipospropuestapresentacion::class, 'tpropuestapresentacion', 'cpropuesta', 'cpresentacion');
    }

    public function tpropuestacronogramaconstruccions() {
        return $this->hasMany(\App\Models\Tpropuestacronogramaconstruccion::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestadisciplinas() {
        return $this->hasMany(\App\Models\Tpropuestadisciplina::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestagastos() {
        return $this->hasMany(\App\Models\Tpropuestagasto::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestagastoslaboratorios() {
        return $this->hasMany(\App\Models\Tpropuestagastoslaboratorio::class, 'cpropuesta', 'cpropuesta');
    }

    public function testructuraparticipantes() {
        return $this->hasMany(\App\Models\Testructuraparticipante::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestaactividades() {
        return $this->hasMany(\App\Models\Tpropuestaactividade::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestapaquetes() {
        return $this->hasMany(\App\Models\Tpropuestapaquete::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestapresentacions() {
        return $this->hasMany(\App\Models\Tpropuestapresentacion::class, 'cpropuesta', 'cpropuesta');
    }

    public function tpropuestarevisions() {
        return $this->hasMany(\App\Models\Tpropuestarevision::class, 'cpropuesta', 'cpropuesta');
    }

    public function ttransmittalconfiguracions() {
        return $this->hasMany(\App\Models\Ttransmittalconfiguracion::class, 'cpropuesta', 'cpropuesta');
    }


}
