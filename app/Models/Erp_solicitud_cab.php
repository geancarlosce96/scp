<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_solicitud_cab extends model
{
    protected $table = "erp_solicitud_cab";

    public $timestamps = true;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_empresa',
        'id_usuario',
        'referencia',
        'id_usuario_solicitante',
        'id_usuario_aprobacion',
        'fecha_aprobacion',
        'estado',
        'id_empleado_entrega',
    ];

}