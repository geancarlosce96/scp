<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Erp_producto extends model
{
    protected $table = "erp_producto";

    public $timestamps = false;

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'id_um',
        'codigo',
        'desctipoproducto',
        'ind_texto_variable',
        'desc_producto',
        'desc_unidadmedida',
        'permitedecimales',
    ];

}