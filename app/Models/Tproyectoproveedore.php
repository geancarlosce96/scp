<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoproveedore extends Model {

    /**
     * Generated
     */

    protected $table = 'tproyectoproveedores';
    protected $fillable = ['cproyecto', 'cpersona', 'activo'];


    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }


}
