<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tpersonadireccione extends Model {
use Traits\HasCompositePrimaryKey; 
    /**
     * Generated
     */
    protected $primaryKey =['cpersona','numerodireccion'];
    protected $table = 'tpersonadirecciones';
    protected $fillable = ['cpersona', 'numerodireccion', 'ctipodireccion', 'direccion', 'referencia', 'numero', 'cpais', 'cubigeo'];
    public $incrementing = false; 
    public $timestamps = false;

    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function ttipodireccione() {
        return $this->belongsTo(\App\Models\Ttipodireccione::class, 'ctipodireccion', 'ctipodireccion');
    }

    public function tubigeo() {
        return $this->belongsTo(\App\Models\Tubigeo::class, 'cubigeo', 'cubigeo');
    }


}
