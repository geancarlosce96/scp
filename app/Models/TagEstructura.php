<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class TagEstructura extends Model {
	protected $table = 'tag_estructura';

	static public function obtenerSegunPropuesta($cpropuesta)
	{
		$tags = Propuesta_tag_estructura::where("propuesta_id", "=", $cpropuesta)
			->select("propuesta_tag_estructura_id")
			->get()->toArray();

		for ($i=0; $i < count($tags); $i++)
			$tags[$i] = $tags[$i]["propuesta_tag_estructura_id"];

		return $tags;
	}
}
