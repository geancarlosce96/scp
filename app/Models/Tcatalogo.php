<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tcatalogo extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'catalogoid';
    protected $table = 'tcatalogo';
    protected $fillable = ['catalogoid', 'codtab', 'digide', 'descripcion', 'valor'];

    public $timestamps = false;


}
