<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoentregablesrevisiones extends model
{
	protected $primaryKey='cproyectoent_rev';
    protected $table = "tproyectoentregablesrevisiones";
    public $timestamps=false;
    
}
