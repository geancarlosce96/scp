<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tcondicionesoperativa extends Model {

    /**
     * Generated
     */

    protected $table = 'tcondicionesoperativas';
    protected $fillable = ['ccondicionoperativa', 'csubsistema', 'descripcion'];


    public function tsubsistema() {
        return $this->belongsTo(\App\Models\Tsubsistema::class, 'csubsistema', 'csubsistema');
    }

    public function tpropuesta() {
        return $this->hasMany(\App\Models\Tpropuestum::class, 'ccondicionoperativa', 'ccondicionoperativa');
    }


}
