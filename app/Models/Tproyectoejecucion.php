<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoejecucion extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cproyectoejecucion';
    protected $table = 'tproyectoejecucion';
    protected $fillable = ['cproyectoejecucion', 'cproyectocronogramadetalle', 'estado', 'horasplanificadas', 'obsplanificada', 'horasejecutadas', 'obsejecutadas', 'horasaprobadas', 'obsaprobadas', 'horasobservadas', 'obsobservadas', 'cpersona_ejecuta', 'fejecuta', 'fregistro', 'faprobacion', 'ordenaprobacion', 'crol_poraprobar', 'cpersona','fplanificado','tipo','ctiponofacturable','ccondicionoperativa','csubsistema','carea'];

    public $timestamps = false;


    public function tpersona() {
        return $this->belongsTo(\App\Models\Tpersona::class, 'cpersona', 'cpersona');
    }

    public function tproyectocronogramadetalle() {
        return $this->belongsTo(\App\Models\Tproyectocronogramadetalle::class, 'cproyectocronogramadetalle', 'cproyectocronogramadetalle');
    }

    public function troldespliegue() {
        return $this->belongsTo(\App\Models\Troldespliegue::class, 'crol_poraprobar', 'croldespliegue');
    }

    public function taprobcionesflujos() {
        return $this->hasMany(\App\Models\Taprobcionesflujo::class, 'cproyectoejecucion', 'cproyectoejecucion');
    }


}
