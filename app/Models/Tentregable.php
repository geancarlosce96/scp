<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tentregable extends Model {

    /**
     * Generated
     */
    protected $primaryKey = 'centregables';
    protected $table = 'tentregables';
    protected $fillable = ['centregables', 'codigo', 'descripcion', 'ctipoentregable', 'cdisciplina'];

    public $timestamps = false;

    public function tdisciplina() {
        return $this->belongsTo(\App\Models\Tdisciplina::class, 'cdisciplina', 'cdisciplina');
    }

    public function ttiposentregable() {
        return $this->belongsTo(\App\Models\Ttiposentregable::class, 'ctipoentregable', 'ctiposentregable');
    }


}
