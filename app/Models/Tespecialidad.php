<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tespecialidad extends Model {

    /**
     * Generated
     */
    protected $primaryKey='cespecialidad';
    protected $table = 'tespecialidad';
    protected $fillable = ['cespecialidad', 'abrevia', 'descripcion'];
    public $timestamps=false;

    public function tpersonaestudios() {
        return $this->hasMany(\App\Models\Tpersonaestudio::class, 'cespecialidad', 'cespecialidad');
    }


}
