<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tproyectoentregable extends Model {

    /**
     * Generated
     */

    protected $primaryKey = 'cproyectoentregables';
    protected $table = 'tproyectoentregables';
    protected $fillable = ['cproyectoentregables', 'cproyecto', 'centregable', 'cproyectoedt', 'observacion', 'cproyectoactividades', 'cpersona_responsable', 'crol_responsable', 'cpersona_revisor', 'cpersona_aprobador'];

    public $timestamps = false;

    public function tproyecto() {
        return $this->belongsTo(\App\Models\Tproyecto::class, 'cproyecto', 'cproyecto');
    }

    public function tproyectoedt() {
        return $this->belongsTo(\App\Models\Tproyectoedt::class, 'cproyectoedt', 'cproyectoedt');
    }

    public function tproyectocronogramadetalles() {
        return $this->hasMany(\App\Models\Tproyectocronogramadetalle::class, 'cproyectoentregable', 'cproyectoentregables');
    }

    public function tproyectoentregablesfechas() {
        return $this->hasMany(\App\Models\Tproyectoentregablesfecha::class, 'cproyectoentregable', 'cproyectoentregables');
    }


}
