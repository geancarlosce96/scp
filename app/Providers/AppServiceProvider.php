<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Tvistastablaspersona;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        view()->composer('layouts.master', function($view) {
            $view
            ->with('atajo_id', Tvistastablaspersona::columnas_id(Auth::user()->cpersona,2))
            ->with('atajo_nombre',Tvistastablaspersona::columnas_nombre(Auth::user()->cpersona,2))
            ->with('atajo_opcion',Tvistastablaspersona::selecion_opcion(Auth::user()->cpersona,2,'checked'))
            ->with('widgets_id', Tvistastablaspersona::columnas_id(Auth::user()->cpersona,3))
            ->with('widgets_nombre',Tvistastablaspersona::columnas_nombre(Auth::user()->cpersona,3))
            ->with('widgets_opcion',Tvistastablaspersona::selecion_opcion(Auth::user()->cpersona,3,'checked'))
            ;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
