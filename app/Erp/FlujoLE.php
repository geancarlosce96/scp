<?php
namespace App\Erp;

use App\Models\Tproyectoedt;
use App\Models\Tproyecto;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesruteo;
use App\Models\Tcatalogo;
use App\Models\Transmittal;
use App\Models\Transmittaldetalle;
use App\Models\Tproyectopersona;
use App\Models\Treglascondicion;

use Carbon\Carbon;
use Auth;
use DB;

class FlujoLE{

	public function flujo(){

		$flujo = Treglascondicion::where('csubsistema','=','007')->first();

		return $flujo->proceso;
	}

	public function flujonext($flujoact)
	{
		$objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujo();
		$flujo = explode(',',$flujo);
		$flujo_actual = array_search($flujoact,$flujo);
		$flujo_siguiente = $flujo[$flujo_actual+1];

		return $flujo_siguiente;
	}

	public function flujoprev($flujoact)
	{
		$objflujoEmp = new FlujoLE();
		$flujo = $objflujoEmp->flujo();
		$flujo = explode(',',$flujo);
		$flujo_actual = array_search($flujoact,$flujo);
		$flujo_anterior = $flujo[$flujo_actual-1];

		return $flujo_anterior;
	}

	public function ruteoGP($cpersona)//si la persona es Gerente
	{
		$cargoruteoGP=DB::table('tproyecto as tp')
		->select('*')
		// ->addSelect(DB::raw("(select case when tp.cpersona_gerente =".$cpersona." then 'GP' else '' end) as gerente"))
		->where('tp.cpersona_gerente', '=' ,$cpersona)
		->first();

		if ($cargoruteoGP) {
			$gp = 'GP';
		} else {
			$gp = 'No';
		}

		return $gp;
	}

	public function ruteoCat($cpersona)//si la persona es lider dentro del proyecto
	{
		$cargoruteoCat = DB::table('tproyectopersona as tpp')
		->join('tcategoriaprofesional as tc','tc.ccategoriaprofesional','=','tpp.ccategoriaprofesional')
		// ->select('*')
		// ->addSelect(DB::raw("(select case when tpp.eslider = '1' then 'LD' else '' end) as lider"))
		->where('cpersona','=',$cpersona)
		->where('tpp.eslider','=',1)
		->first();

		if ($cargoruteoCat) {
			$ld = 'LD';
		} else {
			$ld = 'No';
		}

		return $ld;
	}

	public function ruteoOtros($cpersona)//si la persona es Senio u otro cargo dentro del proyecto
	{
		$cargoruteoCat = DB::table('tproyectopersona as tpp')
		->join('tcategoriaprofesional as tc','tc.ccategoriaprofesional','=','tpp.ccategoriaprofesional')
		// ->select('*')
		// ->addSelect(DB::raw("(select case when tpp.eslider = '1' then 'LD' else '' end) as lider"))
		->where('cpersona','=',$cpersona)
		->where('tpp.eslider','!=',1)
		->first();

		if ($cargoruteoCat) {
			$otro = $cargoruteoCat->ruteole;
		} else {
			$otro = 'No';
		}

		return $otro;
	}

	public function ruteoEmp($cpersona)// si la persona es jefatura
	{
		$cargoruteoEmp = DB::table('tpersonadatosempleado as temp')
		->join('tcargos as tc','tc.ccargo','=','temp.ccargo')
		->join('tareas as ta','ta.carea','=','temp.carea')
		->where('cpersona','=',$cpersona)
		->first();

		return $cargoruteoEmp;
	}

	public function ruteoJefeInmediato($cpersona){

		$cargo= DB::table('tpersonadatosempleado as temp')
			->where('cpersona','=',$cpersona)
			->first();

		$cargoruteoEmp = DB::table('tcargos')		
			->where('cargoparentdespliegue','=',$cargo->ccargo)
			->first();

		return $cargoruteoEmp ? 'JI': 'No';

	}

	public function flujoEmp()
	{
		$flujo = $this->flujo();
		$flujo = explode(',',$flujo);
		

		$emp = $this->ruteoEmp(Auth::user()->cpersona);
		$ld = $this->ruteoCat(Auth::user()->cpersona);
		$gp = $this->ruteoGP(Auth::user()->cpersona);
		$otros = $this->ruteoOtros(Auth::user()->cpersona);

		$ruteo = '';

		if (in_array($emp->siglas,$flujo)) {//SI ES DE AREA
			$ruteo = $emp->siglas;
		}else {
			if (in_array($gp,$flujo)) {//SI ES GERENTE
				$ruteo = $gp;
			}else {
				if (in_array($ld,$flujo)) {//SI ES LIDER
					$ruteo = $ld;
				}else {
					if (in_array($emp->ruteole,$flujo)) {//SI ES JEFATURA
						$ruteo = $emp->ruteole;
					}else {
						if (in_array($otros,$flujo)) {//SI ES OTRO CATEGORIA DENTRO DEL PROYECTO
							$ruteo = $otros;
						}else {
							$ruteo = '';
						}
					}
				}
			}
		}

		return $ruteo;
	}


}


