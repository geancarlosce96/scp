<?php
namespace App\Erp;

use App\Models\Tpropuestaactividade;
use App\Models\Troldespliegue;
use App\Models\Testructuraparticipante;
use DB;
use Auth;
use App\Models\Tpropuestum;
use App\Models\Tpropuestapresentacion;
use App\Models\Tpropuestadisciplina;
use App\Models\Tpropuestapaquete;
use App\Models\Tservicio;
use App\Models\Tactividad;
use App\Models\Tconceptogasto;
use App\Models\Tpropuestagasto;
use App\Models\Tpropuestagastoslaboratorio;
use App\Models\Tpropuestahonorario;
use App\Models\Tpropuestarevision;

class PropuestaSupport{
	public function getProfesionalesSenior($cpropuesta){
		/* Profesionales Roles */
        $trol_prof_senior = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00002')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_prof='';
        foreach($trol_prof_senior as $prof){
            if(!empty($roles_prof)){
                $roles_prof.=',';
            }
            $roles_prof.= $prof->valor;

        }
        $aroles_prof = explode(',',$roles_prof);


        $estrp = DB::table('testructuraparticipante as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cpropuesta','=',$cpropuesta)
        ->whereIn('rol.codigorol',$aroles_prof)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        $listEstr = array();
        foreach($estrp as $est){
            $e['cestructurapropuesta'] = $est->cestructurapropuesta;
            $e['croldespliegue'] =$est->croldespliegue;
            $e['codigorol'] = $est->codigorol;
            $e['descripcionrol'] = $est->descripcionrol;
            $e['tiporol'] = $est->tiporol;
            $e['cestructurapropuesta'] = $est->cestructurapropuesta;
            $e['cpropuesta'] = $est->cpropuesta;
            $e['cpersona_rol'] = $est->cpersona_rol;
            $e['rate'] = isset($est->rate)==1?$est->rate:0;
            $lact = DB::table('tpropuestaactividades as pact')
            ->leftJoin('tactividad as act','pact.cactividad','=','act.cactividad')
            ->join('tpropuestahonorarios as hor','pact.cpropuestaactividades','=','hor.cpropuestaactividades')
            ->where('pact.cpropuesta','=',$cpropuesta)
            ->where('hor.cestructurapropuesta','=',$est->cestructurapropuesta)
            ->select('pact.*','act.cactividad_parent','hor.horas')
            ->get();
            $suma_total = 0;
            foreach($lact as $act){
                $e['hora_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades] = $act->horas;
                $suma_total+=$e['hora_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades];
            }
            $e['suma_total_'.$e['croldespliegue']]=$suma_total;

            $listEstr[count($listEstr)]=$e;
            //$e['totallinea'] = 
        }
        return $listEstr;
        /* Fin Profesionales roles */ 
	}
    public function saveEstructuraActividadSenior($cpropuesta,$request){
        $trol_prof_senior = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00002')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_prof='';
        foreach($trol_prof_senior as $prof){
            if(!empty($roles_prof)){
                $roles_prof.=',';
            }
            $roles_prof.= $prof->valor;

        }
        $aroles_prof = explode(',',$roles_prof);

        $estrp = DB::table('testructuraparticipante as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cpropuesta','=',$cpropuesta)
        //->whereIn('rol.codigorol',$aroles_prof)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        
        foreach($estrp as $est){

            $lact = DB::table('tpropuestaactividades as pact')
            ->where('pact.cpropuesta','=',$cpropuesta)
            ->select('pact.*')
            ->get();

            foreach($lact as $act){
                //$arraypedro= $est->cestructurapropuesta.'_'.$act->cpropuestaactividades;
                //dd($est->cestructurapropuesta.'_'.$act->cpropuestaactividades );
                if(!is_null($request->input('hora_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades)) && strlen($request->input('hora_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades)) >0 ){

                    $obj= Tpropuestahonorario::where('cpropuestaactividades','=',$act->cpropuestaactividades)
                    ->where('cestructurapropuesta','=',$est->cestructurapropuesta)
                    ->whereNull('cdisciplina')
                    ->first();
                    if (!$obj){
                        $obj = new Tpropuestahonorario();
                        $obj->cestructurapropuesta = $est->cestructurapropuesta;
                        $obj->cpropuestaactividades = $act->cpropuestaactividades;
                    }
                    $obj->horas =$request->input('hora_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades);
                    $obj->save();


                }
                //$e['hora_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades] = $act->horas;
            }


        }
        //dd($arraypedro);
        return ;        
    }
    public function saveEstructuraActividadDisciplina($cpropuesta,$request){
        $trol_prof_dis = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00001')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_prof_dis='';
        foreach($trol_prof_dis as $prof){
            if(!empty($roles_prof)){
                $roles_prof_dis.=',';
            }
            $roles_prof_dis.= $prof->valor;

        }
        $aroles_prof_dis = explode(',',$roles_prof_dis);
        $listDis = $this->getDisciplinas($cpropuesta);

        $estrp = DB::table('testructuraparticipante as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cpropuesta','=',$cpropuesta)
        ->whereIn('rol.codigorol',$aroles_prof_dis)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();;
        
        foreach($estrp as $est){
            $lact = DB::table('tpropuestaactividades as pact')
            ->where('pact.cpropuesta','=',$cpropuesta)
            ->select('pact.*')
            ->get();;
            foreach($lact as $act){
                foreach($listDis as $dis){
                    if(!is_null($request->input('horad_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades.'_'.$dis['cdisciplina'])) && strlen($request->input('horad_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades.'_'.$dis['cdisciplina'])) >0 ){

                        $obj= Tpropuestahonorario::where('cpropuestaactividades','=',$act->cpropuestaactividades)
                        ->where('cestructurapropuesta','=',$est->cestructurapropuesta)
                        ->where('cdisciplina','=',$dis['cdisciplina'])
                        ->first();
                        if (!$obj){
                            $obj = new Tpropuestahonorario();
                            $obj->cestructurapropuesta = $est->cestructurapropuesta;
                            $obj->cpropuestaactividades = $act->cpropuestaactividades;
                            $obj->cdisciplina = $dis['cdisciplina'];
                        }
                        $obj->horas =$request->input('horad_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades.'_'.$dis['cdisciplina']);
                        $obj->save();


                    }  
                }                  
            }            

        }
        return;
    }
    public function saveEstructuraActividadApoyo($cpropuesta,$request){
        $trol_prof_apo = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00003')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_apo='';
        foreach($trol_prof_apo as $prof){
            if(!empty($roles_apo)){
                $roles_apo.=',';
            }
            $roles_apo.= $prof->valor;

        }
        $aroles_apo = explode(',',$roles_apo);
        $estrp = DB::table('testructuraparticipante as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cpropuesta','=',$cpropuesta)
        ->whereIn('rol.codigorol',$aroles_apo)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        //dd($estrp);
        foreach($estrp as $est){
            $lact = DB::table('tpropuestaactividades as pact')
            ->where('pact.cpropuesta','=',$cpropuesta)
            ->select('pact.*')
            ->get();
            //dd($lact);
            foreach($lact as $act){
                if(!is_null($request->input('horaa_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades)) && strlen($request->input('horaa_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades)) >0 ){

                    $obj= Tpropuestahonorario::where('cpropuestaactividades','=',$act->cpropuestaactividades)
                    ->where('cestructurapropuesta','=',$est->cestructurapropuesta)
                    ->whereNull('cdisciplina')
                    ->first();
                    if (!$obj){
                        $obj = new Tpropuestahonorario();
                        $obj->cestructurapropuesta = $est->cestructurapropuesta;
                        $obj->cpropuestaactividades = $act->cpropuestaactividades;
                    }
                    $obj->horas =$request->input('horaa_'.$est->cestructurapropuesta.'_'.$act->cpropuestaactividades);
                    $obj->save();
                }                
                //$e['hora_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades] = $act->horas;
            }               

        }

        return ;
    }
	public function getDisciplinas($cpropuesta){
        $tprop_disciplina = DB::table('tpropuestadisciplina as prodis')
        ->join('tdisciplina as dis','prodis.cdisciplina','=','dis.cdisciplina')
        ->where('prodis.cpropuesta','=',$cpropuesta)
        ->select('prodis.*','dis.descripcion','dis.abrevia')
        ->get();
        $listDis=array();
        foreach($tprop_disciplina as $dis){
            $d['cdisciplina'] = $dis->cdisciplina;
            $d['descripcion'] = $dis->descripcion;
            $d['abrevia'] = $dis->abrevia;
            $listDis[count($listDis)]=$d;
        }	
        return $listDis;	
	}
	public function getProfesionalesApoyo($cpropuesta){
        $trol_prof_apo = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00003')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_apo='';
        foreach($trol_prof_apo as $prof){
            if(!empty($roles_apo)){
                $roles_apo.=',';
            }
            $roles_apo.= $prof->valor;

        }
        $aroles_apo = explode(',',$roles_apo);
        $estrp = DB::table('testructuraparticipante as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cpropuesta','=',$cpropuesta)
        ->whereIn('rol.codigorol',$aroles_apo)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        $listEstr_apo = array();
        foreach($estrp as $est){
            $e['cestructurapropuesta'] = $est->cestructurapropuesta;
            $e['croldespliegue'] =$est->croldespliegue;
            $e['codigorol'] = $est->codigorol;
            $e['descripcionrol'] = $est->descripcionrol;
            $e['tiporol'] = $est->tiporol;
            $e['cestructurapropuesta'] = $est->cestructurapropuesta;
            $e['cpropuesta'] = $est->cpropuesta;
            $e['cpersona_rol'] = $est->cpersona_rol;
            $e['rate'] = isset($est->rate)==1?$est->rate:0;
            $lact = DB::table('tpropuestaactividades as pact')
            ->leftJoin('tactividad as act','pact.cactividad','=','act.cactividad')
            ->join('tpropuestahonorarios as hor','pact.cpropuestaactividades','=','hor.cpropuestaactividades')
            ->where('pact.cpropuesta','=',$cpropuesta)
            ->where('hor.cestructurapropuesta','=',$est->cestructurapropuesta)
            ->select('pact.*','act.cactividad_parent','hor.horas')
            ->get();
            $suma_total = 0;
            foreach($lact as $act){
                $e['hora_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades] = $act->horas;
                $suma_total+=$e['hora_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades];
            }
            $e['suma_total_'.$e['croldespliegue']]=$suma_total;
            $listEstr_apo[count($listEstr_apo)]=$e;
        }
        //dd($listEstr_apo);
        return $listEstr_apo;
	}

	public function getProfesionalesDisciplina($cpropuesta){
        $trol_prof_dis = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00001')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_prof_dis='';
        foreach($trol_prof_dis as $prof){
            if(!empty($roles_prof)){
                $roles_prof_dis.=',';
            }
            $roles_prof_dis.= $prof->valor;

        }
        $aroles_prof_dis = explode(',',$roles_prof_dis);


        $estrp = DB::table('testructuraparticipante as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cpropuesta','=',$cpropuesta)
        ->whereIn('rol.codigorol',$aroles_prof_dis)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        $listEstr_dis = array();
        foreach($estrp as $est){
            $e['cestructurapropuesta'] = $est->cestructurapropuesta;
            $e['croldespliegue'] =$est->croldespliegue;
            $e['codigorol'] = $est->codigorol;
            $e['descripcionrol'] = $est->descripcionrol;
            $e['tiporol'] = $est->tiporol;
            $e['cestructurapropuesta'] = $est->cestructurapropuesta;
            $e['cpropuesta'] = $est->cpropuesta;
            $e['cpersona_rol'] = $est->cpersona_rol;
            $e['rate'] = isset($est->rate)==1?$est->rate:0;
            $e['cdisciplina']='0';
            $lact = DB::table('tpropuestaactividades as pact')
            ->leftJoin('tactividad as act','pact.cactividad','=','act.cactividad')
            ->join('tpropuestahonorarios as hor','pact.cpropuestaactividades','=','hor.cpropuestaactividades')
            ->where('pact.cpropuesta','=',$cpropuesta)
            ->where('hor.cestructurapropuesta','=',$est->cestructurapropuesta)
            ->select('pact.*','act.cactividad_parent','hor.horas','hor.cdisciplina')
            ->get();
            $suma_total = 0;
            foreach($lact as $act){
                $e['cdisciplina']=$act->cdisciplina;
                $e['hora_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades.'_'.$act->cdisciplina] = $act->horas;
                if(!isset($e['suma_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades])){
                    $e['suma_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades]=0;
                }
                $e['suma_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades]+=$act->horas;

                $suma_total+=$e['suma_'.$e['cestructurapropuesta'].'_'.$act->cpropuestaactividades];
            }            
            $e['suma_total_'.$e['croldespliegue']]=$suma_total;
            $listEstr_dis[count($listEstr_dis)]=$e;
        }
        // dd($listEstr_dis);
        return $listEstr_dis;
	}

    public function getRevisiones($cpropuesta){
        $arevisiones= array();
        $revisiones =array();
        if(!empty($cpropuesta)){
            $revisiones = DB::table('tpropuestarevision as pro')
            ->where('pro.cpropuesta','=',$cpropuesta)
            ->orderBy('frevision','ASC')
            ->lists('revisionactual');
        }
        
        //00004 - codigo catalogo de revisiones aceptadas por Propuesta.
        $next_rev = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00004')
        ->whereNotNull('cat.digide')
        ->whereNotIn('cat.valor',$revisiones)
        ->get();
        foreach($next_rev as $rev){
            $arevisiones[$rev->valor]=$rev->valor;
        }

        return $arevisiones;

    }

    public function generarRevision($cpropuesta){
        $propuesta = DB::table('tpropuesta')
        ->where('cpropuesta','=',$cpropuesta)
        ->first();
        $revisionanterior='';
        if($propuesta){
            $revisionanterior = $propuesta->revision;
        }
        $rev = $this->getRevisiones($cpropuesta);
        reset($rev);
        $nuevaRevision = current($rev);
        $proprev = new Tpropuestarevision();
        $proprev->revisionanterior = $revisionanterior;
        $proprev->revisionactual=$nuevaRevision;
        $proprev->cpropuesta = $cpropuesta;
        $proprev->save();
        return $nuevaRevision;
    }

}
?>