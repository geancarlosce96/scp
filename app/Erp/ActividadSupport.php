<?php
namespace App\Erp;

use DB;
use Auth;
use App\Models\Tactividad;

class ActividadSupport{
	public function listarActividad($lista,$actividad){
		$actividades = DB::table('tactividad')->where('cactividad_parent','=',$actividad->cactividad)
		->where('tipoactividad','=','00001')
		->get();
		if(count($actividades) >0){
			foreach($actividades as $act){
				$lista[count($lista)]= $act;
				$lista=$this->listarActividad($lista,$act);
			}
			return $lista;
		}else{
			return $lista;
		}

	}

	public function listarActividadPropuesta($lista,$actividad,$cpropuesta){
		$actividades = DB::table('tactividad')
		->where('cactividad_parent','=',$actividad->cactividad)
		->where('tipoactividad','=','00001')
		->get();
		if(count($actividades) >0){
			foreach($actividades as $act){
				$lista[count($lista)]= $act;
				$lista=$this->listarActividad($lista,$act);
			}
			return $lista;
		}else{
			return $lista;
		}

	}
	public function listarActividadProyecto($actividad,$cproyecto){
		$actividades = DB::table('tproyectoactividades as pryact')
        ->join('tactividad as act','pryact.cactividad','=','act.cactividad')
		->where('act.tipoactividad','=','00001')
        ->where('pryact.cproyecto','=',$cproyecto)
		->where('cactividad_parent','=',$actividad->cactividad)->get();
		$estructura="";
		if(count($actividades) >0){
			foreach($actividades as $act){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{";
	            $estructura=$estructura ."text: '".$act->descripcion ."',";
	            $estructura=$estructura ."tags: '".$act->cproyectoactividades. "'";
	            
				$est=$this->listarActividadProyecto($act,$cproyecto);
				if(!empty($est)){
					$estructura=$estructura .",nodes: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}

	}

	public function orderParentActividadEjecucion($actividades){
		$parent=array();
		$childs = array();
		$tmpAct = array();
		foreach($actividades as $act){
			if(empty($act['cpropuestaactividades_parent'])){
				$parent[count($parent)] = $act;
			}else{
				$childs[count($childs)] = $act;
			}

		}
		$i=1;
		$j=1;
		foreach($parent as $p){
			if(isset($p['item'])){
				$p['item'] = $i.'.'.'00';
			}
			$tmpAct[count($tmpAct)] = $p;
			$j=1;
			foreach($childs as $c){
				if($c['cpropuestaactividades_parent']==$p['cpropuestaactividades']){
					if(isset($p['item'])){
						$c['item'] = $i.'.'.str_pad($j,2,'00',STR_PAD_LEFT);
					}
					$tmpAct[count($tmpAct)] = $c;
					$j++;
				}

			}
			$i++;
				
		}

		return $tmpAct;		
	}

	public function orderParentActividadEje($actividades){
		$parent=array();
		$childs = array();
		$tmpAct = array();
		foreach($actividades as $act){
			if(empty($act['cpropuestaactividades_parent'])){
				$parent[count($parent)] = $act;
			}else{
				$childs[count($childs)] = $act;
			}

		}
		//$i=1;
		//$j=1;
		foreach($parent as $p){
			$tmpAct[count($tmpAct)] = $p;
			$tmpAct=$this->getChild($p,$childs,$tmpAct);
			//$j=1;

			//$i++;
				
		}

		return $tmpAct;		
	}
	public function getChild($parent,$childs,$tmpAct){
			foreach($childs as $c){
				if($c['cpropuestaactividades_parent']==$parent['cpropuestaactividades']){

					$tmpAct[count($tmpAct)] = $c;
					$tmpAct=$this->getChild($c,$childs,$tmpAct);
					//$j++;
				}

			}
			return $tmpAct;
	}


	public function addParent($aAct, $a,$paq){
		$tab = DB::table('tactividad as a')
		->where('a.cactividad','=',$a)
		->first();
		if($tab){
			$res = array_search($tab->cactividad, array_map(function($e){return $e['cactividad'];},$aAct));
			if( $res===false){
                $i= -1*(count($aAct)+1)*10;
                $wact['item']='';
                $wact['cpropuestaactividades'] =$i;
                $wact['cactividad'] = $tab->cactividad;
                $wact['codigoactividad'] = $tab->codigo;
                $wact['descripcionactividad'] = $tab->descripcion;
                $wact['numerodocumentos'] = 0;
                $wact['numeroplanos'] = 0;
                $wact['cpropuestapaquete'] = $paq;
                $wact['cactividad_parent'] = $tab->cactividad_parent;	
				$wact['cpropuestaactividadesparent'] = null;	
				$aAct[count($aAct)] = $wact;
				if(strlen($tab->cactividad_parent) > 0){
					$aAct = $this->addParent($aAct,$tab->cactividad_parent,$paq);
				}
			}
		}
		return $aAct;
	}
	public function listarTreeview($actividadl){
		$actividad = DB::table('tactividad as act')
		->where('cactividad_parent','=',$actividadl->cactividad)
		->where('tipoactividad','=','00001')
		->get();
		//dd($actividad);
		$estructura="";
		if(count($actividad) >0){
			foreach($actividad as $act){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{\n\r";
            	$estructura=$estructura ."item: {\n\r";
            	$estructura=$estructura ."id: '".$act->cactividad. "',";
	            $estructura=$estructura ."label: '".$act->descripcion ."',";
	            $estructura=$estructura ."checked: false ";
	            $estructura=$estructura ."}\n\r";
	            
				$est=$this->listarTreeview($act);
				if(!empty($est)){
					$estructura=$estructura .",children: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}
	public function listarTreeviewTipo($actividadl,$tipo){
		$actividad = DB::table('tactividad as act')
		->where('cactividad_parent','=',$actividadl->cactividad)
		->where('tipoactividad','=',$tipo)
		->orderby('act.descripcion')
		->get();
		//dd($actividad);
		$estructura="";
		if(count($actividad) >0){
			foreach($actividad as $act){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{\n\r";
            	$estructura=$estructura ."item: {\n\r";
            	$estructura=$estructura ."id: '".$act->cactividad. "',";
	            $estructura=$estructura ."label: '".$act->descripcion ."',";
	            $estructura=$estructura ."checked: false ";
	            $estructura=$estructura ."}\n\r";
	            
				$est=$this->listarTreeviewTipo($act,$tipo);
				if(!empty($est)){
					$estructura=$estructura .",children: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}

	public	function adtAdmin($actividadl,$tipo){
		$actividad = DB::table('tactividad as act')
			->where('cactividad_parent', '=', $actividadl->cactividad)
			->where('tipoactividad', '=', $tipo)
			->orderby('act.descripcion')
			->get();

		$estructura = "";

		if (count($actividad) > 0) {
			foreach ($actividad as $act) {
				if (!empty($estructura)) {
					$estructura = $estructura . ",";
				}
				//$lista[count($lista)]= $act;
				$estructura = $estructura . "\n\r";
				$estructura = $estructura . "{\n\r";
				$estructura = $estructura . "id: '" . $act->cactividad . "',";
				$estructura = $estructura . "text: '" . $act->descripcion . "',";
				$estructura = $estructura . "checked: false ,";
				$estructura = $estructura . "icon : 'glyphicon glyphicon-pushpin'";
				// $estructura = $estructura . "}\n\r";

				$est = $this->adtAdmin($act, $tipo);
				if (!empty($est)) {
					$estructura = $estructura . ",children: [ ";
					$estructura = $estructura . $est;
					$estructura = $estructura . "]";
				}
				$estructura = $estructura . "}";
			}
			return $estructura;
		} else {
			return $estructura;
		}
	}

	public function listarTreeviewProy($actividadl,$idProyecto,$actHijos){
		$aActividades = array();
		$objAct  = new ActividadSupport();
		$cpersona = Auth::user()->cpersona;
		$flgAct=false;
		/*
		$tproyectopersona = DB::table('tproyectoactividadeshabilitacion as ho')
		->join('tproyectopersona as pp','ho.cpersona','=','pp.cpersona')
		->where('pp.cproyecto','=',$idProyecto)
		->where('pp.cpersona','=',$cpersona)
        ->where('ho.cproyecto','=',$idProyecto)
        ->where('ho.activo','=','1')		
		->select('ho.cproyectoactividades')
		->get();
		foreach($tproyectopersona as $pho){
			$aActividades[count($aActividades)]=$pho->cproyectoactividades;
		}
		//dd($aActividades);
        if(count($aActividades)<=0){
            $flgAct=true;
        }
        if($flgAct){
            $aActividades=$objAct->getTareasProyecto($cproyecto,
            $cpersona
            );
		}*/
		$actividad = DB::table('tproyectoactividades as pa')
		->where('pa.cproyecto','=',$idProyecto)	
		//->whereIn('pa.cproyectoactividades',$aActividades)
		->where('cproyectoactividades_parent','=',$actividadl->cproyectoactividades)
		->orderBy(DB::raw("to_number(codigoactvidad,'999.99999999')"),'ASC')
		->get();
		//dd($actividad);
		$estructura="";
		if(count($actividad) >0){
			foreach($actividad as $act){
				if($this->isChild($act->cproyectoactividades)){
					$res = array_search($act->cproyectoactividades,$actHijos);
					if($res===FALSE){
						continue;
					}
				}else{
					$res = $this->isParentValid($act->cproyectoactividades,$actHijos);
					if(!$res){
						continue;
					}
				}				
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{\n\r";
            	$estructura=$estructura ."item: {\n\r";
            	$estructura=$estructura ."id: '".$act->cproyectoactividades. "',";
	            $estructura=$estructura ."label: '".$act->descripcionactividad ."',";
	            $estructura=$estructura ."checked: false ";
	            $estructura=$estructura ."}\n\r";
	            
				$est=$this->listarTreeviewProy($act,$idProyecto,$actHijos);
				if(!empty($est)){
					$estructura=$estructura .",children: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}	
	/*
	* Lista las actividades registradas para un proyecto
	*/
	public function listarTreeviewProyPla($actividadl,$idProyecto){
		$aActividades = array();

		$actividad = DB::table('tproyectoactividades as pa')
		->where('pa.cproyecto','=',$idProyecto)	
		//->whereIn('pa.cproyectoactividades',$aActividades)
		->where('cproyectoactividades_parent','=',$actividadl->cproyectoactividades)
		->get();
		//dd($actividad);
		$estructura="";
		if(count($actividad) >0){
			foreach($actividad as $act){

	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{\n\r";
            	$estructura=$estructura ."item: {\n\r";
            	$estructura=$estructura ."id: '".$act->cproyectoactividades. "',";
	            $estructura=$estructura ."label: '".$act->descripcionactividad ."',";
	            $estructura=$estructura ."checked: false ";
	            $estructura=$estructura ."}\n\r";
	            
				$est=$this->listarTreeviewProyPla($act,$idProyecto);
				if(!empty($est)){
					$estructura=$estructura .",children: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}	
	public function listarTreeviewAdm($actividadl){ //Antes Proy
		$actividad = DB::table('tactividad as act')
		->where('cactividad_parent','=',$actividadl->cactividad)
		->get();
		//dd($actividad);
		$estructura="";
		if(count($actividad) >0){
			foreach($actividad as $act){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{\n\r";
            	$estructura=$estructura ."item: {\n\r";
            	$estructura=$estructura ."id: '".$act->cactividad. "',";
	            $estructura=$estructura ."label: '".$act->descripcion ."',";
	            $estructura=$estructura ."checked: false ";
	            $estructura=$estructura ."}\n\r";
	            
				$est=$this->listarTreeviewAdm($act);
				if(!empty($est)){
					$estructura=$estructura .",children: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}
	public function sumHorasEjecutadasProy($cproyectoactividades){
		$sum =0;
		$sum = DB::table('tproyectoejecucioncab as cab')
		->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
		->where('cab.cproyectoactividades','=',$cproyectoactividades)
		->sum('horasejecutadas');
		return $sum;
	}
	public function sumHorasPresupuestadasProy($cproyectoactividades){
		$sum =0;
		$sum = DB::table('tproyectohonorarios as hono')
		->where('hono.cproyectoactividades','=',$cproyectoactividades)
		->select(DB::raw('sum(cast(horas as float)) as suma'))->first();
		
		return $sum;
	}

	public function sumCostoPresupuestadoProy($cproyectoactividades,$cproyecto){
		$horaspresupuest=DB::table('tproyectohonorarios as ph')       
        ->join('tproyectoactividades as pa','ph.cproyectoactividades','=','pa.cproyectoactividades')
        ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')       
        ->where('ph.cproyectoactividades','=',$cproyectoactividades)
        ->where('pep.cproyecto','=',$cproyecto)                            
        ->distinct('ph.cproyectoactividades')
        ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pep.rate as ratehonora','ph.cdisciplina','ph.cproyectohonorarios')
        ->get();

        $sum=0;
        $horaspres=0;

        foreach ($horaspresupuest as $hp) {
            $sum=$sum+($hp->hpresup*$hp->ratehonora);
                      
        }

                   // $sum=$horaspresupuest;
		return $sum;
	}


	public function sumCostoEjecutadoProy($cproyectoactividades,$cproyecto/*,$tipo*/){
		$horasejecut=DB::table('tproyectoejecucion as pe')
        ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
        ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
        ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
        ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional')                       
        ->where('pec.cproyectoactividades','=',$cproyectoactividades)
        ->where('pp.cproyecto','=',$cproyecto)
                   // ->where('pec.cproyectoejecucioncab','=',$pla['cproyectoejecucioncab'])
        ->whereIn('pe.estado',[2,3,4,5])                   
        ->distinct('pec.cproyectoactividades')
        ->select('pec.cproyectoactividades','pec.cproyectoejecucioncab','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo')
        ->get();

        $totejectufact=0;
        $totejectunofact=0;
        $horaseje=0;

        foreach ($horasejecut as $hp) {

           if($hp->tipo=='1'){
               $totejectufact=$totejectufact+($hp->horasejecutadas*$hp->ratecatego);
           }

           if($hp->tipo=='2'){
               $totejectunofact=$totejectunofact+($hp->horasejecutadas*$hp->ratecatego);
           }       
        }

     /*   if($tipo==1){
        	$sum=$totejectufact;
        }
        if($tipo==2){
        	$sum=$totejectunofact;
        }*/
      
        $sum=$totejectunofact+$totejectufact;
        // $sum=$horasejecut;
		
		return $sum;
	}

	/****************   INICIO HORAS/COSTOS PRESUPUESTADOS Y EJECUTADOS POR AREA   ****************/
	/**********************************************************************************************/


		
	public function sumHorasEjecutadasProyArea($cproyectoactividades,$carea){
		$sum =0;
		$sum = DB::table('tproyectoejecucioncab as cab')
		->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
		->leftjoin('tpersonadatosempleado as p','eje.cpersona_ejecuta','=','p.cpersona')
		->where('p.carea','=',$carea)
		->where('cab.cproyectoactividades','=',$cproyectoactividades)		
		->sum('horasejecutadas');
		return $sum;
	}
	public function sumHorasPresupuestadasProyArea($cproyectoactividades,$carea){
		$sum =0;
	
		$sum = DB::table('tproyectohonorarios as hono')
		->join('tdisciplinaareas as da','hono.cdisciplina','=','da.cdisciplina')
		->where('da.carea','=',$carea)
		->where('hono.cproyectoactividades','=',$cproyectoactividades)
		->select(DB::raw('sum(cast(horas as float)) as suma'))->first();

		return $sum;
	}

	public function sumCostoPresupuestadoProyArea($cproyectoactividades,$cproyecto,$carea){
		$horaspresupuest=DB::table('tproyectohonorarios as ph')       
        ->join('tproyectoactividades as pa','ph.cproyectoactividades','=','pa.cproyectoactividades')
        ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')    
        ->join('tdisciplinaareas as da','ph.cdisciplina','=','da.cdisciplina')
        ->where('ph.cproyectoactividades','=',$cproyectoactividades)
        ->where('pep.cproyecto','=',$cproyecto)  
        ->where('da.carea','=',$carea)                         
        ->distinct('ph.cproyectoactividades')
        ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pep.rate as ratehonora','ph.cdisciplina','ph.cproyectohonorarios')
        ->get();

        $sum=0;
        $horaspres=0;

        foreach ($horaspresupuest as $hp) {
                       $sum=$sum+($hp->hpresup*$hp->ratehonora);
                      
                    }
		return $sum;
	}


	public function sumCostoEjecutadoProyArea($cproyectoactividades,$cproyecto,$carea){
		$horasejecut=DB::table('tproyectoejecucion as pe')
        ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
        ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
        ->join('tproyectoactividades as pa','pe.cproyectoactividades','=','pa.cproyectoactividades')
        ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional') 
        ->leftjoin('tpersonadatosempleado as p','pe.cpersona_ejecuta','=','p.cpersona')
		->where('p.carea','=',$carea)                      
        ->where('pec.cproyectoactividades','=',$cproyectoactividades)
        ->where('pp.cproyecto','=',$cproyecto)
        ->whereIn('pe.estado',[2,3,4,5])                   
        ->distinct('pec.cproyectoactividades')
        ->select('p.carea','pec.cproyectoactividades','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo')
        ->orderby('pe.cproyectoejecucion')
        ->get();

        $totejectufact=0;
        $totejectunofact=0;
        $horaseje=0;

        foreach ($horasejecut as $hp) {

           if($hp->tipo=='1'){
               $totejectufact=$totejectufact+($hp->horasejecutadas*$hp->ratecatego);
           }

           if($hp->tipo=='2'){
               $totejectunofact=$totejectunofact+($hp->horasejecutadas*$hp->ratecatego);
           }       
        }

      /*if($tipo==1){
        	$sum=$totejectufact;
        }
        if($tipo==2){
        	$sum=$totejectunofact;
        }*/
      
        $sum=$totejectunofact+$totejectufact;
        
		
		return $sum;
	}

	public function sumHorasEjecutadasProyectoArea($cproyecto,$carea){
		$sum =0;
		$sum = DB::table('tproyectoejecucioncab as cab')
		->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
		->leftjoin('tpersonadatosempleado as p','eje.cpersona_ejecuta','=','p.cpersona')
		->where('p.carea','=',$carea)
		->where('cab.cproyectoactividades','=',$cproyectoactividades)		
		->sum('horasejecutadas');
		return $sum;
	}
	public function sumHorasPresupuestadasProyectoArea($cproyecto,$carea){
		$sum =0;
	
		$sum = DB::table('tproyectohonorarios as hono')
		->join('tdisciplinaareas as da','hono.cdisciplina','=','da.cdisciplina')
		->where('da.carea','=',$carea)
		->where('hono.cproyectoactividades','=',$cproyectoactividades)
		->select(DB::raw('sum(cast(horas as float)) as suma'))->first();

		return $sum;
	}

	public function sumCostoPresupuestadoProyectoArea($cproyecto,$carea){
		$horaspresupuest=DB::table('tproyectohonorarios as ph')       
        ->join('tproyectoestructuraparticipantes as pep','ph.cestructuraproyecto','=','pep.cestructuraproyecto')    
        ->join('tdisciplinaareas as da','ph.cdisciplina','=','da.cdisciplina')
        ->where('ph.cproyectoactividades','=',$cproyectoactividades)
        ->where('pep.cproyecto','=',$cproyecto)  
        ->where('da.carea','=',$carea)                         
        ->distinct('ph.cproyectoactividades')
        ->select('ph.cproyectoactividades','pa.descripcionactividad','ph.horas as hpresup','pep.rate as ratehonora','ph.cdisciplina','ph.cproyectohonorarios')
        ->get();

        $sum=0;
        $horaspres=0;

        foreach ($horaspresupuest as $hp) {
                       $sum=$sum+($hp->hpresup*$hp->ratehonora);
                      
                    }
		return $sum;
	}

	public function sumCostoEjecutadoProyectoArea($cproyecto,$carea){
		$horasejecut=DB::table('tproyectoejecucion as pe')
        ->join('tproyectoejecucioncab as pec','pe.cproyectoejecucioncab','=','pec.cproyectoejecucioncab') 
        ->join('tproyectopersona as pp','pe.cpersona_ejecuta','=','pp.cpersona')
        
        ->join('tcategoriaprofesional as cp','pp.ccategoriaprofesional','=','cp.ccategoriaprofesional') 
        ->leftjoin('tpersonadatosempleado as p','pe.cpersona_ejecuta','=','p.cpersona')
		->where('p.carea','=',$carea)                      
        
        ->where('pp.cproyecto','=',$cproyecto)
        ->whereIn('pe.estado',[2,3,4,5])                   
        ->distinct('pec.cproyectoactividades')
        ->select('p.carea','pec.cproyectoactividades','pe.estado','pa.descripcionactividad','pe.cpersona_ejecuta','cp.ccategoriaprofesional','cp.rate as ratecatego','pe.horasejecutadas','pe.cproyectoejecucion','pe.tipo')
        ->orderby('pe.cproyectoejecucion')
        ->get();

        $totejectufact=0;
        $totejectunofact=0;
        $horaseje=0;

        foreach ($horasejecut as $hp) {

           if($hp->tipo=='1'){
               $totejectufact=$totejectufact+($hp->horasejecutadas*$hp->ratecatego);
           }

           if($hp->tipo=='2'){
               $totejectunofact=$totejectunofact+($hp->horasejecutadas*$hp->ratecatego);
           }       
        }

      /*if($tipo==1){
        	$sum=$totejectufact;
        }
        if($tipo==2){
        	$sum=$totejectunofact;
        }*/
      
        $sum=$totejectunofact+$totejectufact;
        
		
		return $sum;
	}
	
	/*********************************************************************************************/
	/****************    FIN HORAS/COSTOS PRESUPUESTADOS Y EJECUTADOS POR AREA    ****************/



	public function isPatern($est){
		//dd($est);
		if(count($est)<=1){
			return true;
		}
		if($est[count($est)-1]==0){
			return true;
		}else{
			return false;
		}
	}

	public function getNivel($est){
		$i=0;
		foreach($est as $e){
			$i++;
			if(intval($e)==0){
				return ($i-1);
			}
		}
		return $i;
	}
	public function isChild($act){
		$res = true;
		$tt = DB::table('tproyectoactividades as act')
		->where('act.cproyectoactividades_parent','=',$act)
		->first();
		if($tt){
			$res = false;
		}
		return $res;
	}
	/* Verificar si es padre de algún hijo en algun nivel
	*/
	public function isParentValid($act,$actHijos){
		$res = false;
		$tt = DB::table('tproyectoactividades as act')
		->where('cproyectoactividades_parent','=',$act)
		->get();
		foreach($tt as $t){
			$par = DB::table('tproyectoactividades as act')
			->where('cproyectoactividades','=',$t->cproyectoactividades)
			->whereNotNull('cproyectoactividades_parent')
			->first();
			if($par){
				$res_p = $this->isParent($t->cproyectoactividades,$actHijos);
				if ($res_p){
					$res = $this->isParentValid($t->cproyectoactividades,$actHijos);
					if($res){
						break;
					}
				}else{
					$w = array_search($t->cproyectoactividades,$actHijos);
					if($w===FALSE){
						$res=false;
					}else{
						$res = true;
						break;
					}
				}
			}/*else{
				$res = $this->isParentValid($t->cproyectoactividades,$actHijos);
				if($res){
					break;
				}
			}*/

		}


		return $res;

	}
	public function isParent($act){
		$res = false;
		$tt = DB::table('tproyectoactividades as act')
		->where('cproyectoactividades_parent','=',$act)
		->count();
		if($tt>0){
			$res=true;
		}
		return $res;
	}
	public function getTareasProyecto($idProyecto,$user){
		$act=array();
		$tt = DB::table('tproyectopersona as pp')
		->where('cproyecto','=',$idProyecto)
		->where('cpersona','=',$user)
		->first();
		if($tt){
			$tab = DB::table('tproyectoactividades as pryact')
			->where('cproyecto','=',$idProyecto)
			->get();
			foreach($tab as $t){
				$pp= DB::table('tproyectoactividades as act')
				->where('cproyecto','=',$idProyecto)
				->where('cproyectoactividades_parent','=',$t->cproyectoactividades)
				->get();
				if($pp){

				}else{
					$act[count($act)]=$t->cproyectoactividades;
				}
			}		
		}

		return $act;
	}

	public function getTareasProyectoGP($idProyecto){
		$act=array();
			$tab = DB::table('tproyectoactividades as pryact')
			->where('cproyecto','=',$idProyecto)
			->get();
			foreach($tab as $t){
				$pp= DB::table('tproyectoactividades as act')
				->where('cproyecto','=',$idProyecto)
				->where('cproyectoactividades_parent','=',$t->cproyectoactividades)
				->get();
				if($pp){

				}else{
					$act[count($act)]=$t->cproyectoactividades;
				}
			}		

		return $act;
	}


	public function listarActividadesDelProyecto($cproyecto){

		$aActHijos=$this->getTareasProyectoGP($cproyecto);
		$tactividades = DB::table('tproyectoactividades as pryact')
        //->join('tproyectoactividades as pryact','act.cactividad','=','pryact.cactividad')
        ->whereIn('pryact.cproyectoactividades',$aActHijos)
        ->select('pryact.cproyectoactividades_parent')
        ->distinct()
        ->get();

        $aActividades = array();
        $aActPadres= array();
        foreach($tactividades as $act){
            $aActividades[count($aActividades)] = $act->cproyectoactividades_parent;
        }
        
        foreach($aActividades as $t){
            $tt = DB::table('tproyectoactividades as pryact')
            ->where('cproyectoactividades','=',$t)
            ->whereNotNull('cproyectoactividades_parent')
            ->select('cproyectoactividades_parent')
            ->first();
            if($tt){
                $tx = $this->buscarPadre($tt->cproyectoactividades_parent);
            }else{
                $tx=$t;
            }
            $res = array_search($tx,$aActPadres,false);
            if($res===FALSE){
                $aActPadres[count($aActPadres)]=$tx;
            }
        }
        /* complementar hijos sin padres */
        $tpadres = DB::table('tproyectoactividades as pryact')
        ->whereIn('pryact.cproyectoactividades',$aActHijos)
        ->whereNull('pryact.cproyectoactividades_parent')
        ->select('pryact.cproyectoactividades')
        ->distinct()
        ->get();
        foreach($tpadres as $act){
            $aActPadres[count($aActPadres)] = $act->cproyectoactividades;
        }
        /* fin complementar hijos sin padres */

        $actParent= DB::table('tproyectoactividades as pa')
        //->join('tproyectoactividades as pa','act.cactividad','=','pa.cactividad')
        ->where('pa.cproyecto','=',$cproyecto)
        ->whereIn('pa.cproyectoactividades',$aActPadres)
        ->whereNull('cproyectoactividades_parent')
        ->orderBy('codigoactvidad','ASC')
        ->get();
        
        $estructura="";
        //dd($aActPadres);
        //dd($actParent);
        foreach ($actParent as $act) {
            if(!empty($estructura)){
                $estructura= $estructura .",";
            }
            //$actividades[count($actividades)]=$act;
             $estructura=$estructura ."{\n\r";
            $estructura=$estructura ."item: {\n\r";
            $estructura=$estructura ."id: '".$act->cproyectoactividades. "',";
            $estructura=$estructura ."label: '".$act->descripcionactividad ."',";
            
            $estructura=$estructura ."checked: false ";
            $estructura=$estructura ."}\n\r";

            

            
            $est=$this->listarTreeviewProy($act,$cproyecto,$aActHijos);
            //dd($est);
            if(!empty($est)){
                $estructura=$estructura .",children: [ ";
                $estructura = $estructura . $est;
                $estructura=$estructura ."]";
            }

            $estructura=$estructura ."}\n\r";
        }       
        //dd($estructura);
        return $estructura;

	}

}
?>
