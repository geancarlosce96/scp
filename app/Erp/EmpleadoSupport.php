<?php
namespace App\Erp;

use DB;
use App\Models\Tactividad;

class EmpleadoSupport{
	public function getPersonalRol($rol,$csubsistema,$tiporol){
		$personal = DB::table('tpersona')
        ->join('tpersonanaturalinformacionbasica as tnat','tpersona.cpersona','=','tnat.cpersona')
        ->join('tpersonarol as pr','tpersona.cpersona','=','pr.cpersona')
        ->join('troldespliegue as rol','pr.croldespliegue','=','rol.croldespliegue')
        ->where('tnat.esempleado','=','1')
        ->where('rol.tiporol','=',$tiporol)
        ->where('rol.csubsistema','=',$csubsistema)
        ->where('rol.codigorol','=',$rol)
        ->where('pr.estado','=','001')
        ->orderBy('tpersona.nombre')
        ->lists('tpersona.abreviatura','tpersona.cpersona');

        return $personal;
	}
    public function getPersonalArea_cbx($cod_area){
        $tpersonal_cd = DB::table('tpersona as p')
        ->join('tpersonanaturalinformacionbasica as nat','nat.cpersona','=','p.cpersona')
        ->where('nat.esempleado','=','1')
        ->select('p.cpersona','p.nombre')
        ->get();
        $personal_cd['']='';
        foreach($tpersonal_cd as $per){
            $tlaboral = DB::table('tpersonadatosempleado as dat')
            ->join('tareas as are','are.carea','=','dat.carea')
            ->where('dat.cpersona','=',$per->cpersona)
            ->where('are.codigo','=',$cod_area) /* especificar el codigo correcto de Control Documentario*/
            ->orderBy('dat.fingreso','DESC')
            ->first();
            if($tlaboral){
                $personal_cd[$per->cpersona]=$per->nombre;                
            }
        }  
        return $personal_cd;
    }
    public function getCargabilidadPlani($cpersona,$fecha1,$fecha2){
        $nroHoras=0;
        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);

        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2'])
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2))
        ->where('eje.horasplanificadas','>',0);
        $tactividadesk=$tactividadesk->select('eje.*')
        ->get(); 
        foreach($tactividadesk as $v){
            $nroHoras += $v->horasplanificadas;
        } 
        $totHoras=$this->totalCargaPlani($cpersona,$fecha1,$fecha2);
        if($totHoras<=0){
            $totHoras=1;
        }
        $nroHoras = round(($nroHoras/$totHoras)*100);
        return $nroHoras;
    }
    public function getCargabilidadEje($cpersona,$fecha1,$fecha2){
        $nroHoras=0;
        $nroHoras = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);

        $nroHoras = $nroHoras->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2'])
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2))
        ->where('eje.horasejecutadas','>',0)
        ->sum('horasejecutadas'); 
        $totHoras=$this->totalCargaEje($cpersona,$fecha1,$fecha2);
        if($totHoras<=0){
            $totHoras=1;
        }
        $nroHoras = round(($nroHoras/$totHoras)*100);        
        return $nroHoras;
    }   
    public function totalCargaPlani($cpersona,$fecha1,$fecha2){
        //FF NF y AN
        $nroHoras=0;
        $nroHoras = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);        

        $nroHoras = $nroHoras->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2','3'])
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2))
        ->where('eje.horasplanificadas','>',0)
        ->sum('horasplanificadas'); 
        
        return $nroHoras;
    } 
    public function totalCargaEje($cpersona,$fecha1,$fecha2){
        //FF NF y AN
        $nroHoras=0;
        $nroHoras = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);

        $nroHoras = $nroHoras->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2','3'])
        ->whereBetween('eje.fplanificado',array($fecha1,$fecha2))
        ->where('eje.horasejecutadas','>',0)
        ->sum('horasejecutadas'); 

        return $nroHoras;
    }   

    public function listPersonaArea($carea){

        $personal = DB::table('tpersona as per')
        ->leftjoin('tpersonanaturalinformacionbasica as tnat','per.cpersona','=','tnat.cpersona')
        ->leftjoin('tpersonadatosempleado as emp','emp.cpersona','=','per.cpersona')  
        ->leftjoin('tareas as ta','emp.carea','=','ta.carea')
        ->leftJoin('tusuarios as tu','emp.cpersona','=','tu.cpersona')
        ->where('tnat.esempleado','=','1')
        ->where('tu.estado','=','ACT')
        ->where('emp.carea','=',$carea)
        ->orderBy('per.abreviatura','ASC')
        ->orderBy('per.nombre','ASC')
        ->lists('per.abreviatura','per.cpersona');
        return $personal;
    }  

    public function getPersonaProyectoXDisciplina($idProyecto,$idDiscplina){

        $personalProy=DB::table('tproyectopersona as pryper')
        ->leftjoin('tpersona as per','per.cpersona','=','pryper.cpersona')
        ->leftjoin('tproyecto as pry','pryper.cproyecto','=','pry.cproyecto')
        ->leftjoin('tdisciplina as dis','pryper.cdisciplina','=','dis.cdisciplina')
        ->where('pry.cproyecto','=',$idProyecto)
        ->where('pryper.cdisciplina','=',$idDiscplina)
        ->where('pryper.eslider','=','1')
        ->select('pryper.cdisciplina','pryper.cproyecto','pryper.cpersona','per.abreviatura','dis.descripcion')
        ->first();

        return $personalProy;        
    }

    public function totalCargaPlaniXSem($cpersona,$semana){
        //FF NF y AN
        $nroHoras=0;
        $nroHoras = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);        

        $nroHoras = $nroHoras->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2','3'])
        ->where(DB::raw('EXTRACT(week FROM eje.fplanificado)'),'=',$semana)
        ->where('eje.horasplanificadas','>',0)
        ->sum('horasplanificadas'); 
        
        return $nroHoras;
    } 
    public function totalCargaEjeXSem($cpersona,$semana){
        //FF NF y AN
        $nroHoras=0;
        $nroHoras = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);

        $nroHoras = $nroHoras->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2','3'])
        ->where(DB::raw('EXTRACT(week FROM eje.fplanificado)'),'=',$semana)
        ->where('eje.horasejecutadas','>',0)
        ->sum('horasejecutadas'); 

        return $nroHoras;
    }   


    public function getCargabilidadEjeXSemana($cpersona,$semana){
        $nroHoras=0;
        $nroHoras = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);

        $nroHoras = $nroHoras->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2'])
        ->where(DB::raw('EXTRACT(week FROM eje.fplanificado)'),'=',$semana)
        ->where('eje.horasejecutadas','>',0)
        ->sum('horasejecutadas'); 
        $totHoras=$this->totalCargaEjeXSem($cpersona,$semana);
        if($totHoras<=0){
            $totHoras=1;
        }
        $nroHoras = round(($nroHoras/$totHoras)*100);        
        return $nroHoras;
    }   
    public function getCargabilidadPlaniXSem($cpersona,$semana){
        $nroHoras=0;
        $tactividadesk = DB::table('tproyectoejecucioncab as cab')
        ->Join('tproyectoejecucion as eje','cab.cproyectoejecucioncab','=','eje.cproyectoejecucioncab')
        ->where('cab.cpersona_ejecuta','=',$cpersona);

        $tactividadesk = $tactividadesk->whereIn('eje.estado',[1,2,3,4,5])
        ->whereIn('cab.tipo',['1','2'])
        ->where(DB::raw('EXTRACT(week FROM eje.fplanificado)'),'=',$semana)
        ->where('eje.horasplanificadas','>',0);
        $tactividadesk=$tactividadesk->select('eje.*')
        ->get(); 

        //var_dump( count($tactividadesk) );
        foreach($tactividadesk as $v){
            $nroHoras += $v->horasplanificadas;
        } 
        $totHoras=$this->totalCargaPlaniXSem($cpersona,$semana);
        //var_dump($totHoras);
        if($totHoras<=0){
            $totHoras=1;
        }
        $nroHoras = round(($nroHoras/$totHoras)*100);
        return $nroHoras;
    }


}
?>