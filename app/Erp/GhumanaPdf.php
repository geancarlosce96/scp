<?php
namespace App\Erp;

use Fpdf;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class GhumanaPdf extends \Codedge\Fpdf\Fpdf\FPDF{
 //Cabecera de página
   public $titulo="";
   public $fecha="";
   public $hora="";
   public $codProy="";
   public $codUmin="";
   public $umin="";
   public $nombre="";
   public $servicio="";
   public $GP="";
   public $medio="";
   public $estado="";
   public $finicio="";  
   public $fcierre="";

function Header()
   {
    $logo = storage_path('archivos')."/images/logorpt.jpg";
       $this->Image($logo,10,8,33);

        /* Inicio Titulo */
       $this->SetFont('Times','B',15);   

       $this->SetXY(133.5,20);

       $this->Cell(37,10,$this->titulo,0,0,'C');
        /* Fin Titulo */

        /* Inicio Fecha */
       $this->SetFont('Times','B',11);

       $this->SetXY(220,7);

       $this->Cell(0,7,$this->fecha,0,0,'L');
       /* Fin Fecha */   
       $this->SetXY(220,14);

       $this->Cell(0,7,$this->hora,0,0,'L');
       /* Fin Hora */   

       /* Inicio de Cabecera Tabla */
       $this->SetFont('Times','B',9);
       
       $this->SetXY(10,30);
       $this->Cell(18,8,'Cod.Proy',1,0,'C');

       $this->SetXY(28,30);
       $this->Cell(18,8,'Cod.UMin',1,0,'C');

       $this->SetXY(46,30);
       $this->Cell(26,8,'Unidad Minera',1,0,'C');

       $this->SetXY(72,30);
       $this->Cell(58,8,'Nombre de Proyecto',1,0,'C');

       $this->SetXY(130,30);
       $this->Cell(30,8,'Servicio',1,0,'C');

       $this->SetXY(160,30);
       $this->Cell(40,8,'Gte. Propuesta',1,0,'C');

       $this->SetXY(200,30);
       $this->Cell(20,8,'Medio',1,0,'C');

       $this->SetXY(220,30);
       $this->Cell(19,8,'Estado',1,0,'C');

       $this->SetXY(239,30);
       $this->Cell(24,8,'Fecha Inicio',1,0,'C');   

       $this->SetXY(263,30);
       $this->Cell(24,8,'Fecha Cierre',1,0,'C');   

       $this->Ln();
       /* Fin de Cabecera Tabla */

       /* Cuerpo Tabla */

       $this->SetFont('Times','B',9);
       
       $this->SetXY(10,38);
       $this->Cell(18,8,$this->codProy,0,0,'C');

       $this->SetXY(28,38);
       $this->Cell(18,8,$this->codUmin,0,0,'C');

       $this->SetXY(46,38)
       $this->Cell(26,8,$this->umin,0,0,'C');

       $this->SetXY(72,38);
       $this->Cell(58,8,$this->nombre,0,0,'C');

       $this->SetXY(130,38);
       $this->Cell(30,8,$this->servicio,0,0,'C');

       $this->SetXY(160,38);
       $this->Cell(40,8,$this->GP,0,0,'C');

       $this->SetXY(200,38);
       $this->Cell(20,8,$this->medio,0,0,'C');

       $this->SetXY(220,38);
       $this->Cell(19,8,$this->estado,0,0,'C');

       $this->SetXY(239,38);
       $this->Cell(24,8,$this->finicio,0,0,'C');   

       $this->SetXY(263,30);
       $this->Cell(24,8,$this->fcierre,1,0,'C');

       $this->Ln();
       /* Fin de Cabecera Tabla */

   } 

}

?>