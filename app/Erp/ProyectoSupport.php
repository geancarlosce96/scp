<?php
namespace App\Erp;

use App\Models\Tpropuestaactividade;
use App\Models\Testructuraparticipante;
use DB;
use Auth;
use App\Models\Tpropuestum;
use App\Models\Tpropuestapresentacion;
use App\Models\Tpropuestadisciplina;
use App\Models\Tpropuestapaquete;
use App\Models\Tservicio;
use App\Models\Tactividad;
use App\Models\Tpropuestagasto;
use App\Models\Tpropuestagastoslaboratorio;
use App\Models\Tpropuestahonorario;
use App\Models\Tproyecto;
use App\Models\Tproyectoestructuraparticipante;
use App\Models\Testadopropuestum;
use App\Models\Testadoproyecto;
use App\Models\Tproyectoactividade;
use App\Models\Tproyectoedt;
use App\Models\Tproyectodisciplina;
use App\Models\Tproyectoentregable;
use App\Models\Tproyectoentregablesfecha;
use App\Models\Tproyectodocumento;
use App\Models\Tconceptogasto;
use App\Models\Tproyectogasto;
use App\Models\Tproyectogastoslaboratorio;
use App\Models\Tproyectohonorario;
use App\Models\Tentregable;
use App\Models\Ttiposentregable;
use App\Models\Troldespliegue;
use App\Models\Tproyectocronogramadetalle;
use App\Models\Tproyectocronograma;
use App\Models\Tproyectosub;
use App\Models\Tdisciplina;

class ProyectoSupport{
	public function getProfesionalesSenior($cproyecto){
		/* Profesionales Roles */
        $trol_prof_senior = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00002')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_prof='';
        foreach($trol_prof_senior as $prof){
            if(!empty($roles_prof)){
                $roles_prof.=',';
            }
            $roles_prof.= $prof->valor;

        }
        $aroles_prof = explode(',',$roles_prof);


        $estrp = DB::table('tproyectoestructuraparticipantes as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cproyecto','=',$cproyecto)
        ->whereIn('rol.codigorol',$aroles_prof)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        $listEstr = array();
        foreach($estrp as $est){
            $e['cestructuraproyecto'] = $est->cestructuraproyecto;
            $e['croldespliegue'] =$est->croldespliegue;
            $e['codigorol'] = $est->codigorol;
            $e['descripcionrol'] = $est->descripcionrol;
            $e['tiporol'] = $est->tiporol;
            
            $e['cproyecto'] = $est->cproyecto;
            $e['cpersona_rol'] = $est->cpersona_rol;
            $e['rate'] = isset($est->rate)==1?$est->rate:0;
            $lact = DB::table('tproyectoactividades as pact')
            ->join('tactividad as act','pact.cactividad','=','act.cactividad')
            ->join('tproyectohonorarios as hor','pact.cproyectoactividades','=','hor.cproyectoactividades')
            ->where('pact.cproyecto','=',$cproyecto)
            ->where('hor.cestructuraproyecto','=',$est->cestructuraproyecto)
            ->select('pact.*','act.cactividad_parent','hor.horas')
            ->get();
            foreach($lact as $act){
                $e['hora_'.$e['cestructuraproyecto'].'_'.$act->cproyectoactividades] = $act->horas;
            }

            $listEstr[count($listEstr)]=$e;
        }
        //dd($listEstr);
        return $listEstr;
        /* Fin Profesionales roles */ 
	}

	public function getDisciplinas($cproyecto){
        $tprop_disciplina = DB::table('tproyectodisciplina as prodis')
        ->join('tdisciplina as dis','prodis.cdisciplina','=','dis.cdisciplina')
        ->where('prodis.cproyecto','=',$cproyecto)
        ->select('prodis.*','dis.descripcion','dis.abrevia')
        ->get();
        $listDis=array();
        foreach($tprop_disciplina as $dis){
            $d['cdisciplina'] = $dis->cdisciplina;
            $d['descripcion'] = $dis->descripcion;
            $d['abrevia'] = $dis->abrevia;
            $listDis[count($listDis)]=$d;
        }	
        return $listDis;	
	}
	public function getProfesionalesApoyo($cproyecto){
        $trol_prof_apo = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00003')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_apo='';
        foreach($trol_prof_apo as $prof){
            if(!empty($roles_apo)){
                $roles_apo.=',';
            }
            $roles_apo.= $prof->valor;

        }
        $aroles_apo = explode(',',$roles_apo);
        $estrp = DB::table('tproyectoestructuraparticipantes as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cproyecto','=',$cproyecto)
        ->whereIn('rol.codigorol',$aroles_apo)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        $listEstr_apo = array();
        foreach($estrp as $est){
            $e['cestructuraproyecto'] = $est->cestructuraproyecto;
            $e['croldespliegue'] =$est->croldespliegue;
            $e['codigorol'] = $est->codigorol;
            $e['descripcionrol'] = $est->descripcionrol;
            $e['tiporol'] = $est->tiporol;
            $e['cproyecto'] = $est->cproyecto;
            $e['cpersona_rol'] = $est->cpersona_rol;
            $e['rate'] = isset($est->rate)==1?$est->rate:0;
            $lact = DB::table('tproyectoactividades as pact')
            ->join('tactividad as act','pact.cactividad','=','act.cactividad')
            ->join('tproyectohonorarios as hor','pact.cproyectoactividades','=','hor.cproyectoactividades')
            ->where('pact.cproyecto','=',$cproyecto)
            ->where('hor.cestructuraproyecto','=',$est->cestructuraproyecto)
            ->select('pact.*','act.cactividad_parent','hor.horas')
            ->get();
            foreach($lact as $act){
                $e['hora_'.$e['cestructuraproyecto'].'_'.$act->cproyectoactividades] = $act->horas;
            }               
            $listEstr_apo[count($listEstr_apo)]=$e;
        }

        return $listEstr_apo;
	}

	public function getProfesionalesDisciplina($cproyecto){
        $trol_prof_dis = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=','00001')
        ->whereNotNull('cat.digide')
        ->orderBy('cat.digide')
        ->get();
        $roles_prof_dis='';
        foreach($trol_prof_dis as $prof){
            if(!empty($roles_prof)){
                $roles_prof_dis.=',';
            }
            $roles_prof_dis.= $prof->valor;

        }
        $aroles_prof_dis = explode(',',$roles_prof_dis);


        $estrp = DB::table('tproyectoestructuraparticipantes as part')
        ->join('troldespliegue as rol','part.croldespliegue','=','rol.croldespliegue')
        ->where('part.cproyecto','=',$cproyecto)
        ->whereIn('rol.codigorol',$aroles_prof_dis)
        ->select('part.*','rol.codigorol','rol.descripcionrol','rol.tiporol')
        ->get();
        $listEstr_dis = array();
        foreach($estrp as $est){
            $e['cestructuraproyecto'] = $est->cestructuraproyecto;
            $e['croldespliegue'] =$est->croldespliegue;
            $e['codigorol'] = $est->codigorol;
            $e['descripcionrol'] = $est->descripcionrol;
            $e['tiporol'] = $est->tiporol;
            $e['cproyecto'] = $est->cproyecto;
            $e['cpersona_rol'] = $est->cpersona_rol;
            $e['rate'] = isset($est->rate)==1?$est->rate:0;
            $e['cdisciplina']='0';
            $lact = DB::table('tproyectoactividades as pact')
            ->join('tactividad as act','pact.cactividad','=','act.cactividad')
            ->join('tproyectohonorarios as hor','pact.cproyectoactividades','=','hor.cproyectoactividades')
            ->where('pact.cproyecto','=',$cproyecto)
            ->where('hor.cestructuraproyecto','=',$est->cestructuraproyecto)
            ->select('pact.*','act.cactividad_parent','hor.horas','hor.cdisciplina')
            ->get();
            foreach($lact as $act){
                $e['cdisciplina']=$act->cdisciplina;
                $e['hora_'.$e['cestructuraproyecto'].'_'.$act->cproyectoactividades.'_'.$act->cdisciplina] = $act->horas;
            }            
            $listEstr_dis[count($listEstr_dis)]=$e;
        }
        return $listEstr_dis;
	}

    public function getRevisionesDocumentos($cproyecto,$coddoc,$codcatalogo){
        $arevisiones= array(''=>'');
        $revisiones =array();
        if(!empty($cproyecto)){
            $revisiones = DB::table('tproyectodocumentos as pro')
            ->join('tdocumentosparaproyecto as prydoc','pro.cdocumentoparapry','=','prydoc.cdocumentoparapry')
            ->where('prydoc.codigo','=',$coddoc)
            ->where('pro.cproyecto','=',$cproyecto)
            ->orderBy('fechadoc','ASC')
            ->lists('revisionactual');
            //dd($revisiones);
        }
        
        //00005 - codigo catalogo de revisiones aceptadas por Propuesta.

        $next_rev = DB::table('tcatalogo as cat')
        ->where('cat.codtab','=',$codcatalogo)
        ->whereNotNull('cat.digide')
        ->whereNotIn('cat.valor',$revisiones)
        ->orderBy(DB::raw("to_number(cat.digide,'999.99999999')"),'ASC')
        //->orderBy('digide','asc')
        ->get();
        foreach($next_rev as $rev){
            $arevisiones[$rev->valor]=$rev->valor;
        }

        return $arevisiones;

    }

}
?>