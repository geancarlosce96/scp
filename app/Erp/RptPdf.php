<?php
namespace App\Erp;

use Fpdf;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class RptPdf extends \Codedge\Fpdf\Fpdf\FPDF{
   //Cabecera de página
   function Header()
   {
    $logo = storage_path('archivos')."/images/logorpt.jpg";
       $this->Image($logo,10,8,33);

      $this->SetFont('Arial','B',12);

      $this->Cell(30,10,'Title',1,0,'C');

   }    
}
?>