<?php
namespace App\Erp;

use DB;
class GastosSupport{
	public function listarTreeview_bk($gasto,$tipogasto){
		$gastos = DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=',$tipogasto)
		->where('cconcepto_parent','=',$gasto->cconcepto)->get();
		$estructura="";
		if(count($gastos) >0){
			foreach($gastos as $gas){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{";
	            $estructura=$estructura ."text: '".$gas->descripcion ."',";
	            $estructura=$estructura ."tags: '".$gas->cconcepto. "',";
	            $estructura=$estructura ."state: {checked: true}";
	            
				$est=$this->listarTreeview($gas,$tipogasto);
				if(!empty($est)){
					$estructura=$estructura .",nodes: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}

	public function listarTreeview($gasto,$tipogasto){
		$gastos = DB::table('tconceptogastos as congas')
        ->where('congas.tipogasto','=',$tipogasto)
		->where('cconcepto_parent','=',$gasto->cconcepto)
		->get();
		$estructura="";
		if(count($gastos) >0){
			foreach($gastos as $gas){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{\n\r";
            	$estructura=$estructura ."item: {\n\r";
            	$estructura=$estructura ."id: '".$gas->cconcepto. "',";
	            $estructura=$estructura ."label: '".$gas->descripcion ."',";
	            $estructura=$estructura ."checked: false ";
	            $estructura=$estructura ."}\n\r";
	            
				$est=$this->listarTreeview($gas,$tipogasto);
				if(!empty($est)){
					$estructura=$estructura .",children: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}

	public function orderParent($gastos){
		$parent=array();
		$childs = array();
		$tmpGastos = array();
		foreach($gastos as $gas){
			if(empty($gas['cconcepto_parent'])){
				$parent[count($parent)] = $gas;
			}else{
				$childs[count($childs)] = $gas;
			}

		}
		$i=1;
		$j=1;
		foreach($parent as $p){
			$p['item'] = $i.'.'.'00';
			$tmpGastos[count($tmpGastos)] = $p;
			$j=1;
			foreach($childs as $c){
				if($c['cconcepto_parent']==$p['concepto']){
					$c['item'] = $i.'.'.str_pad($j,2,'00',STR_PAD_LEFT);
					$tmpGastos[count($tmpGastos)] = $c;
					$j++;
				}

			}
			$i++;
				
		}

		return $tmpGastos;

	}

}

?>