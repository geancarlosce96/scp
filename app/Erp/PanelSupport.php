<?php
namespace App\Erp;

use DB;
use Auth;
use Carbon\Carbon;
use App\Models\Tusuario;
use App\Models\Tpersona;
use App\Models\Tvistastablaspersona;


class PanelSupport{

	public function alertasObs($idPersona){

		$anotificacion=array();

		$obs=DB::table('tproyectoejecucion as pe')
	
		->select(DB::raw("extract(week from pe.fplanificado::date)as semana,sum(pe.horasobservadas)"),'pe.cpersona_ejecuta','pe.estado')
		->where('pe.cpersona_ejecuta','=',$idPersona)
		->whereNotNull('pe.horasobservadas')
		->whereNotNull('pe.horasejecutadas')
		->where('pe.estado','=','2')
		->where('pe.ccondicionoperativa','=','EJE')
		->groupby('semana','pe.cpersona_ejecuta','pe.estado')
		->orderby('semana')
		->get();
		$codigo=0;	
		$ccondopsig='';

		foreach ($obs as $o) {

			$fecha=DB::table('tproyectoejecucion as pe')	
			->select(DB::raw("extract(week from pe.fplanificado::date)as semana,sum(pe.horasobservadas)"),'pe.cpersona_ejecuta','pe.estado','pe.fplanificado')
			->where('pe.cpersona_ejecuta','=',$idPersona)
			->whereNotNull('pe.horasobservadas')
			->where('pe.estado','=','2')
			->where('pe.ccondicionoperativa','=','EJE')
			->groupby('semana','pe.cpersona_ejecuta','pe.estado','pe.fplanificado')
			->orderby('semana')
			->where(DB::raw("extract(week from pe.fplanificado::date)"),'=',$o->semana)
			->first();
	
	
			$not['semana']=$o->semana;		       
	        $not['cantidad']=$o->sum;
	        $not['fecha']=$fecha->fplanificado;
	        $not['tipo']=1;
	        $anotificacion[count($anotificacion)] = $not;				
		}

		return $anotificacion;
	}


	public function alertasPlani($idPersona){

	$anotificacion=array();

		
	    $planificadas=DB::table('tproyectoejecucion as pe')
	
		->select(DB::raw("extract(week from pe.fplanificado::date)as semana,sum(pe.horasplanificadas)"),'pe.cpersona_ejecuta','pe.estado')
		->where('pe.cpersona_ejecuta','=',$idPersona)
		->whereNotNull('pe.horasplanificadas')
		->where('pe.estado','=','1')
		->groupby('semana','pe.cpersona_ejecuta','pe.estado')
		->orderby('semana')
		->get();

		foreach ($planificadas as $p) {

				$fecha=DB::table('tproyectoejecucion as pe')	
					->select(DB::raw("extract(week from pe.fplanificado::date)as semana,sum(pe.horasobservadas)"),'pe.cpersona_ejecuta','pe.estado','pe.fplanificado','pe.fplanificado')
					->where('pe.cpersona_ejecuta','=',$idPersona)
					->whereNotNull('pe.horasplanificadas')
					->where('pe.estado','=','1')
					->groupby('semana','pe.cpersona_ejecuta','pe.estado','pe.fplanificado','pe.fplanificado')
					->orderby('semana')
					->where(DB::raw("extract(week from pe.fplanificado::date)"),'=',$p->semana)
					->first();


				$not['semana']=$p->semana;		       
		        $not['cantidad']=$p->sum;
		        $not['fecha']=$fecha->fplanificado;
		        $not['tipo']=2;
		        $anotificacion[count($anotificacion)] = $not;				
		}

		return $anotificacion;
	}

	public function alertasEje($idPersona){

	$anotificacion=array();
	$sem_act = date('W');

		
	    $eje=DB::table('tproyectoejecucion as pe')
	
		->select(DB::raw("extract(week from pe.fplanificado::date)as semana,sum(pe.horasejecutadas)"),'pe.cpersona_ejecuta','pe.estado')
		->where('pe.cpersona_ejecuta','=',$idPersona)
		->whereNotNull('pe.horasejecutadas')
		->whereNull('pe.horasobservadas')
		->where('pe.estado','=','2')
		->where('pe.ccondicionoperativa','=','EJE')
		->where(DB::raw("extract(week from pe.fplanificado::date)"),'!=',$sem_act)
		->groupby('semana','pe.cpersona_ejecuta','pe.estado')
		->orderby('semana')
		->get();
		$codigo=0;	
		$ccondopsig='';

		foreach ($eje as $e) {

				$fecha=DB::table('tproyectoejecucion as pe')	
					->select(DB::raw("extract(week from pe.fplanificado::date)as semana,sum(pe.horasejecutadas)"),'pe.cpersona_ejecuta','pe.estado','pe.fplanificado')
					->where('pe.cpersona_ejecuta','=',$idPersona)
					->whereNotNull('pe.horasejecutadas')
					->where('pe.estado','=','2')
					->where('pe.ccondicionoperativa','=','EJE')
					->groupby('semana','pe.cpersona_ejecuta','pe.estado','pe.fplanificado')
					->orderby('semana')
					->where(DB::raw("extract(week from pe.fplanificado::date)"),'=',$e->semana)
					->first();
		
				$not['semana']=$e->semana;		       
		        $not['cantidad']=$e->sum;
		        $not['fecha']=$fecha->fplanificado;
		        $not['tipo']=3;
		        $anotificacion[count($anotificacion)] = $not;				
		}

		return $anotificacion;
	}


	public function alertasHTPend($idPersona){

	$anotificacion=array();

	$tpersona=DB::table('tpersona as per')        
    ->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
	->where('per.cpersona','=',$idPersona)
	->first();

	$fingreso=$tpersona->fingreso;
	$diaing = substr($fingreso,8,2);
    $mesing = substr($fingreso,5,2);
    $anioing = substr($fingreso,0,4);
    $semanaing = date('W',  mktime(0,0,0,$mesing,$diaing,$anioing));

    $fechaactual=Carbon::now();

    $anioact=$fechaactual->year;
    $semanaact=$fechaactual->weekOfYear;

    for ($i='2018'; $i <= $anioact ; $i++) { 

    	$ultsem=Carbon::create($i,12,31,0,0,0);

    	$ultsem =$ultsem->startOfWeek();

            if ($ultsem->weekOfYear=='1') {
                    $ultsem->addDays(-1);

            }
        //$ultsem=$ultsem->weekOfYear;

    	$semanainicio="";
        $semanafin="";

        if($i==$anioing && $i==$anioact){
            $semanainicio= $semanaing; 
            $semanafin=$semanaact-1;                
        }    

        if($i == $anioing && $i < $anioact){
            $semanainicio= $semanaing; 
            $semanafin=$ultsem->weekOfYear;             
        }  
        
        if($i > $anioing && $i == $anioact){
            $semanainicio= 1; 
            $semanafin=$semanaact-1;                  
        } 

        if($i > $anioing && $i < $anioact){
            $semanainicio= 1; 
            $semanafin=$ultsem->weekOfYear;
        } 

        if($i < $anioing && $i < $anioact){
            $semanainicio= $semanaing; 
            $semanafin=$ultsem->weekOfYear;             
        } 


        if($i >= $anioing) {

	        for ($j=$semanainicio; $j <= $semanafin ; $j++) {

	        	$pendientes=DB::table('tproyectoejecucioncab as cab')
	            ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')     
	            ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona') 
	            ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio'),'eje.estado','eje.ccondicionoperativa','per.cpersona','per.abreviatura')
	            ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado),per.abreviatura,per.cpersona,eje.estado,eje.ccondicionoperativa'))
	            ->orderBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'ASC')
	            ->orderBy(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'ASC')
	            ->orderBy(DB::raw('per.cpersona'),'ASC')
	            ->whereIn('eje.estado',['1','2','3','4','5'])     
	            ->where('per.cpersona','=',$idPersona)  
	            ->whereNotNull('eje.horasejecutadas')
	            ->where(DB::raw('EXTRACT(WEEK FROM eje.fplanificado)'),'=',$j)
	            ->where(DB::raw('EXTRACT(YEAR FROM eje.fplanificado)'),'=',$i)
	            ->get(); 

	            	$iniSem=date('Y-m-d', strtotime('01/01/'.$i.' +' . ($j - 1) . ' weeks first day +' .'1' . ' day'));

	            if(!$pendientes || is_null($pendientes)){

	            //dd($i,$anioing,$anioact,$semanainicio,$semanafin,$pendientes,$iniSem,$j);
	            $pend['semana']=$j;		       
		        $pend['cantidad']=1;
		        $pend['fecha']=$iniSem;

		        $anotificacion[count($anotificacion)] = $pend;

	            }        	
	        }
        }
    }	
    return $anotificacion; 
		
}

public function alertasJIPend($idPersona){

			$anotificacion=array();


			$JIpend=DB::table('tproyectoejecucioncab as cab')
	        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
	        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
	        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
	        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
	        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto') 
	        ->whereIn('eje.estado',['3','4'])
	        ->where('ccondicionoperativa','=','VJI')
	        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$idPersona.' as bigint))'),'=',DB::raw('true'))
	        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio,sum(eje.horasejecutadas)'))
	        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado)'))
	      	->orderby('anio','desc')
	        ->orderby('semana','desc')
	        ->get();

	        foreach ($JIpend as $jip) {

	        	$fecha=DB::table('tproyectoejecucioncab as cab')
		        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
		        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
		        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
		        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
		        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto') 
		        ->whereIn('eje.estado',['3','4'])
		        ->where('ccondicionoperativa','=','VJI')
		        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$idPersona.' as bigint))'),'=',DB::raw('true'))
		        ->where(DB::raw("extract(week from eje.fplanificado::date)"),'=',$jip->semana)
		        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio,sum(eje.horasejecutadas)'),'eje.fplanificado')
		        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado)'),'eje.fplanificado')
		        ->orderby('anio','desc')
	       		->orderby('semana','desc')
		        ->first();     


		        $not['semana']=$jip->semana;		       
		        $not['cantidad']=$jip->sum;
		        $not['fecha']=$fecha->fplanificado;
		        $not['tipo']=1;
		        $anotificacion[count($anotificacion)] = $not;
	        	
	        }	

			return $anotificacion;
	}

	public function alertasGPPend($idPersona){

			$anotificacion=array();


			$GPpend=DB::table('tproyectoejecucioncab as cab')
	        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
	        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
	        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
	        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
	        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto') 
	        ->whereIn('eje.estado',['3','4'])
	        ->where('ccondicionoperativa','=','RGP')
	        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$idPersona.' as bigint))'),'=',DB::raw('true'))
	        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio,sum(eje.horasejecutadas)'))
	        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado)'))
	        ->orderby('anio','desc')
	        ->orderby('semana','desc')
	        ->get();

	        foreach ($GPpend as $gpp) {

	        	$fecha=DB::table('tproyectoejecucioncab as cab')
		        ->join('tproyectoejecucion as eje','eje.cproyectoejecucioncab','=','cab.cproyectoejecucioncab')
		        ->leftJoin('tproyectoactividades as pryact','cab.cproyectoactividades','=','pryact.cproyectoactividades')
		        ->leftJoin('tactividad as act','cab.cactividad','=','act.cactividad')
		        ->join('tpersona as per','eje.cpersona_ejecuta','=','per.cpersona')
		        ->leftJoin('tproyecto as pry','pryact.cproyecto','=','pry.cproyecto') 
		        ->whereIn('eje.estado',['3','4'])
		        ->where('ccondicionoperativa','=','RGP')
		        ->where(DB::raw('"fn_IsEvaluador"(cproyectoejecucion,CAST('.$idPersona.' as bigint))'),'=',DB::raw('true'))
		        ->where(DB::raw("extract(week from eje.fplanificado::date)"),'=',$gpp->semana)
		        ->select(DB::raw('EXTRACT(WEEK FROM eje.fplanificado) as semana'),DB::raw('EXTRACT(YEAR FROM eje.fplanificado) as anio,sum(eje.horasejecutadas)'),'eje.fplanificado')
		        ->groupBy(DB::raw('EXTRACT(YEAR FROM eje.fplanificado),EXTRACT(WEEK FROM eje.fplanificado)'),'eje.fplanificado')
		        ->orderby('anio','desc')
	        	->orderby('semana','desc')
		        ->first();     


		        $not['semana']=$gpp->semana;		       
		        $not['cantidad']=$gpp->sum;
		        $not['fecha']=$fecha->fplanificado;
		        $not['tipo']=1;
		        $anotificacion[count($anotificacion)] = $not;
	        	
	        }	

			return $anotificacion;
	}

	public function	JIpersonalogin($cpersona){

		$tpersona=DB::table('tpersona as per')        
		->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
		->where('per.cpersona','=',$cpersona)
		->first();

		$ccargo=null;
		if ($tpersona) {
		  $ccargo=$tpersona->ccargo;
		}

		$tcargosJI=DB::table('tcargos as tc') 
		->where('tc.ccargo','=',$ccargo)
		->first();

		$ccargoparent=null;
		if ($tcargosJI) {
		  $ccargoparent=$tcargosJI->cargoparentdespliegue;
		}

		$tpersonaJI=DB::table('tpersona as per')        
		->join('tusuarios as tu','per.cpersona','=','tu.cpersona')
		->join('tpersonadatosempleado as dat','per.cpersona','=','dat.cpersona')
		->where('tu.estado','=','ACT')
		->where('dat.ccargo','=',$ccargoparent)
		->first();

		$JI=null;

		if ($tpersonaJI) {		
			$JI=$tpersonaJI->abreviatura;
		}

		return $JI;

	}

	public function alertevaluacion()
	{
		$usuario = Auth::user()->cusuario;
		$tusuario = Tusuario::find($usuario);
		$tpersona = Tpersona::where('cpersona','=', $tusuario->cpersona)->first();

		$evaluacionescat= DB::table('erp_encuesta_cab')
			->leftjoin('erp_encuesta_cat', 'erp_encuesta_cat.id', '=', 'erp_encuesta_cab.id_categoria')
			->leftjoin('tpersona', 'erp_encuesta_cab.dni_evaluado', '=', 'tpersona.identificacion')
			->leftjoin('tusuarios','tusuarios.cpersona','=','tpersona.cpersona')
			->leftjoin('tpersonadatosempleado', 'tpersonadatosempleado.cpersona', '=', 'tpersona.cpersona')
			->leftjoin('tcargos', 'tcargos.ccargo', '=', 'tpersonadatosempleado.ccargo')
			->select('erp_encuesta_cat.descripcion','erp_encuesta_cat.desde','erp_encuesta_cat.hasta','erp_encuesta_cat.id as idcat','erp_encuesta_cab.id_estado as estado_cab')
			->where('erp_encuesta_cab.dni_evaluador','=', $tpersona->identificacion)
			->where('tpersonadatosempleado.estado','=','ACT')
			->where('tusuarios.estado','=','ACT')
			->where('erp_encuesta_cab.id_estado','=',1)
			->groupBy('erp_encuesta_cat.descripcion','erp_encuesta_cat.desde','erp_encuesta_cat.hasta','erp_encuesta_cat.id','erp_encuesta_cab.id_estado')
			->get();
		$cuentoevaluacionescat	 = count($evaluacionescat);
		return $cuentoevaluacionescat;

	}
	
}
?>