<?php
namespace App\Erp;

use DB;
use App\Erp\EnumSubsistema;
use App\Erp\EnumTipoAprobacion;
use App\Models\Tpropuestum;
use App\Models\Tflujoaprobacion;
use Carbon\Carbon;
use Auth;

class FlowSupport{
	public function obtenerSiguienteCondicion($csubsistema,$tipoaprobacion,$condicion_actual){
		$aProceso = array(''=>'');
		$tnext = DB::table('treglascondicion as rc')
		->where('rc.csubsistema','=',$csubsistema)
		->where(DB::raw('trim(rc.tipoaprobacion)'),'=',$tipoaprobacion)
		->where(DB::raw('trim(rc.ccondicionoperativa)'),'=',$condicion_actual)
		->first();
		//dd($tnext);
		if($tnext){
			$atemp= explode(';',$tnext->proceso);
			//dd($atemp);
			foreach ($atemp as $value) {
				$at = explode(',',$value);
				//dd($at);
				$tcond = DB::table('tcondicionesoperativas')
				->where('ccondicionoperativa','=', $at[1])
				->first();
				$aProceso[$at[1]] = $at[1] . "-". $tcond->descripcion;
				
			}
		}
		return $aProceso;
	}
	public function obtenerSiguienteCondicionAut($csubsistema,$tipoaprobacion,$condicion_actual){
		$aProceso = array();
		$tnext = DB::table('treglascondicion as rc')
		->where('rc.csubsistema','=',$csubsistema)
		->where(DB::raw('trim(rc.tipoaprobacion)'),'=',$tipoaprobacion)
		->where(DB::raw('trim(rc.ccondicionoperativa)'),'=',$condicion_actual)
		->first();
		//dd($tnext);
		if($tnext){
			$atemp= explode(';',$tnext->proceso);
			//dd($atemp);
			foreach ($atemp as $value) {
				$at = explode(',',$value);
				//dd($at);
				$aProceso[count($aProceso)] = array($at[1],$at[0] );
				
			}
		}
		return $aProceso;
	}	
	public function obtenerPasoFlujo($csubsistema,$tipoaprobacion,$condicion/*,$orden*/){
		$tflow = DB::table('treglasaprobacion as ra')
		->where('ra.csubsistema','=',$csubsistema)
		->where(DB::raw('trim(ra.tipoaprobacion)'),'=',$tipoaprobacion)
		/*->where('ra.orden','=',$orden)*/
		->where(DB::raw('trim(ra.ccondicionoperativa)'),'=',$condicion)
		->first();
		//dd($condicion);
		$aFlow=array();
		if($tflow){
			$f['roles_a']=$tflow->roles_a;
			$f['orden_a']=$tflow->orden_a;
			$aFlow[count($aFlow)]=$f;
		}
		return $aFlow;
	}
	public function obtenerHistorialPropuesta($cpropuesta){
		$tflujo = DB::table('tflujoaprobacion as f')
		->leftJoin('tcondicionesoperativas as ante',function($join){
			$join->on('f.ccondicionoperativa_aprobado','=','ante.ccondicionoperativa')
			->where('ante.csubsistema','=','001');
		})
		->leftJoin('tcondicionesoperativas as sgte',function($join){
			$join->on('f.ccondicionoperativa_siguiente','=','sgte.ccondicionoperativa')
			->where('sgte.csubsistema','=','001');
		})		
		->where('f.csubsistema','=','001')
		->where(DB::raw('trim(tipoaprobacion)'),'=','2')
		->where('f.cpropuesta','=',$cpropuesta)
		->select('f.*','ante.descripcion as desante','sgte.descripcion as dessgte')		
		->orderBy('orden_aprobado','ASC')
		->orderBy('fregistro','ASC')
		->get();
		$aFlujo=array();
		$i=1;
		foreach($tflujo as $tf){
			$f['item'] = $i;
			$f['cflujoaprobacion'] = $tf->cflujoaprobacion;
			$f['ccondicionoperativa_aprobado'] = $tf->ccondicionoperativa_aprobado;
			$f['desante'] = $tf->desante;
			$f['cpersona_aprobado'] = $tf->cpersona_aprobado;
			$tper= DB::table('tpersona as per')
			->where('per.cpersona','=',$tf->cpersona_aprobado)
			->first();
			$f['aprobador'] = $tper->nombre;
			$f['ccondicionoperativa_siguiente'] = $tf->ccondicionoperativa_siguiente;
			$f['dessgte'] = $tf->dessgte;
			$aFlujo[count($aFlujo)] = $f;
			$i++;
		}
		return $aFlujo;
	}
	public function obtenerHistorialHoras($cproyectoejecucion){
		$tflujo = DB::table('tflujoaprobacion as f')
		->leftJoin('tcondicionesoperativas as ante',function($join){
			$join->on('f.ccondicionoperativa_aprobado','=','ante.ccondicionoperativa')
			->where('ante.csubsistema','=','004');
		})
		->leftJoin('tcondicionesoperativas as sgte',function($join){
			$join->on('f.ccondicionoperativa_siguiente','=','sgte.ccondicionoperativa')
			->where('sgte.csubsistema','=','004');
		})		
		->where('f.csubsistema','=','004')
		/*->where(DB::raw('trim(tipoaprobacion)'),'=','3')*/
		->where('f.cproyectoejecucion','=',$cproyectoejecucion)
		->select('f.*','ante.descripcion as desante','sgte.descripcion as dessgte')		
		->orderBy('orden_aprobado','ASC')
		->orderBy('fregistro','ASC')
		->get();
		$aFlujo=array();
		$i=1;
		foreach($tflujo as $tf){
			$f['item'] = $i;
			$f['cflujoaprobacion'] = $tf->cflujoaprobacion;
			$f['ccondicionoperativa_aprobado'] = $tf->ccondicionoperativa_aprobado;
			$f['desante'] = $tf->desante;
			$f['cpersona_aprobado'] = $tf->cpersona_aprobado;
			$tper= DB::table('tpersona as per')
			->where('per.cpersona','=',$tf->cpersona_aprobado)
			->first();
			$f['aprobador'] = $tper->nombre;
			$f['ccondicionoperativa_siguiente'] = $tf->ccondicionoperativa_siguiente;
			$f['dessgte'] = $tf->dessgte;
			$aFlujo[count($aFlujo)] = $f;
			$i++;
		}
		return $aFlujo;
	}	
	public function guardarAprobacionTimeSheet($cproyectoejecucion,$conAnterior,$conActual,$tipoaprobacion,$cpersonaaprobado,$orden){
		$obj = new Tflujoaprobacion();
		$obj->csubsistema='004';
		$obj->tipoaprobacion=$tipoaprobacion;



		$obj->ccondicionoperativa_aprobado=$conAnterior;
		$obj->cpersona_aprobado=$cpersonaaprobado;
		$obj->orden_aprobado=$orden;
		$obj->fregistro=date('Y-M-d H:i:s');
		$obj->ccondicionoperativa_siguiente=$conActual;
		$obj->cproyectoejecucion=$cproyectoejecucion;
		$obj->save();

	}
	public function guardarAprobacionGastos($cgastosejecuciondet,$conAnterior,$conActual,$tipoaprobacion,$cpersonaaprobado,$orden){
		$obj = new Tflujoaprobacion();
		$obj->csubsistema='006';
		$obj->tipoaprobacion=$tipoaprobacion;



		$obj->ccondicionoperativa_aprobado=$conAnterior;
		$obj->cpersona_aprobado=$cpersonaaprobado;
		$obj->orden_aprobado=$orden;
		$obj->fregistro=date('Y-M-d  H:i:s');
		$obj->ccondicionoperativa_siguiente=$conActual;
		$obj->cgastosejecuciondet=$cgastosejecuciondet;
		$obj->save();

	}	
	public function siguienteFlujoPropuesta($cpropuesta,$nextCondicion){
		$res=0;
		$cpersona = Auth::user()->cpersona;
		$objProp = Tpropuestum::where('cpropuesta','=',$cpropuesta)->first();

		$nextFlujo = $this->obtenerPasoFlujo('001','2',$objProp->ccondicionoperativa);
		$roles_a='';
		$orden_a='';

		foreach($nextFlujo as $n){
			$roles_a = $n['roles_a'];
			$orden_a = $n['orden_a'];
		}
		//dd($roles_a);
		$trol = DB::table('troldespliegue as rol')
		->whereIn(DB::raw('trim(rol.codigorol)'),explode(',',$roles_a))
		->first();

		$objFlujo = new Tflujoaprobacion();
		$objFlujo->csubsistema = '001';
		$objFlujo->tipoaprobacion ='2';
		$objFlujo->cpropuesta = $cpropuesta;
		$objFlujo->ccondicionoperativa_aprobado = $objProp->ccondicionoperativa;
		$objFlujo->cpersona_aprobado = $cpersona;
		//$objFlujo->croldespliegue_aprobado = $trol->croldespliegue;
		$objFlujo->orden_aprobado = $orden_a;
		$objFlujo->fregistro = Carbon::now();
		$objFlujo->save();


		$objProp->ccondicionoperativa = $nextCondicion;
		$objProp->save();
		return $res;
	}

	public function isApruebaPropuesta($cpropuesta){
		$result = false;
		$tprop = DB::table('tpropuesta')
		->where('cpropuesta','=',$cpropuesta)
		->first();
		$condicion='';
		if($tprop){
			$condicion = $tprop->ccondicionoperativa;

		}
		$ares = $this->obtenerPasoFlujo('001','2',$condicion);
		foreach($ares as $r){
			$trol = DB::table('troldespliegue as rol')
			->whereIN('codigorol',explode(',',$r['roles_a']))
			->first();
			if($trol){
				$per_rol = DB::table('tpersonarol as prol')
				->where('prol.cpersona','=',Auth::user()->cpersona)
				->where('prol.croldespliegue','=',$trol->croldespliegue)
				->where('prol.estado','=','001')
				->first();
				if($per_rol){
					$result = true;
				}
			}
		}
		return $result;
	}

	public function isApruebaDocumentos($cpropuesta,$coddoc){
		$result = false;

		return $result;
	}	

	/*public function getCondicionOperativasPersonaTimeSheet($pDetail){
		$stringCO="";
		$csubsistema="004";
		$treglas = DB::table('treglasaprobacion as reg')
		->where('csubsistema','=','004')
		->get();

		foreach($treglas as $rol){
			$con = $rol->ccondicionoperativa;
			$funcion = "get".$csubsistema."".$con;
			$res = method_exists($this,$funcion);
			if($res){
				if(strlen($stringCO)>0){
					$stringCO.=",";
				}
				$stringCO.=$con;
			}

		//del organigrama

		//de roles de Proyecto -- GP, RDP, 
		}
		
		return $stringCO;

	}
	public function get004VJI($pDetail){
		return true;
	}
	public function get004RGP($pDetail){
		//se busca en tproyecto
		$tproy = DB::table('tproyecto as p')
		->first();
		return true;
	}*/

	
}
?>