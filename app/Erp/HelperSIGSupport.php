<?php
namespace App\Erp;
use DB;
use App\Models\Tnumeradore;

class HelperSIGSupport
{
	public function __construct(){

	}
	/*
	* Obtener Codigo de Cliente
	* @param,
	* @result, Codigo de cliente
	*/
	public function getCodigoCliente($idCliente){
		$result="";
		$result = $this->getYY();

		return $result;
	}	
	public function getCodigoPropuesta($cpersona=0){
		$codigo = $this->getYY();
		return $codigo;
	}
	public function getCodigoProyecto($cpersona=0){
		$codigo = $this->getYY();
		return $codigo;
	}	
	public  function getYY(){
		return date('y');
	}
	public  function getSede($csede=0){
		$codigosede='10';
		if ($csede!=0){
			$sede = DB::table('tsedes')->where('csede','=',$csede)->first();
			if(count($sede) > 0){
				$codigosede = $sede->codigosede;
			}
		}
		return $codigosede;
	}
	public  function getIncrement(){
		return 100;
	}

	private function ultimoGastos()
	{
		$ultimo = DB::table('tgastosejecucion')
		->orderBy('cgastosejecucion','DESC')
		->get();

		return $ultimo;
	}

	public function getNumeroRendicion($csubsistema,$entidad){

		$periodo = $this->ultimoGastos();
		$anio = substr($periodo[0]->numerorendicion,0,4);
		// dd($anio);
		if (date('Y')!=$anio) {
			$rest = Tnumeradore::where('csubsistema','=',$csubsistema)->where('entidad','=',$entidad)->first();
			$rest->numero = 0;
			$rest->save();
		}
		$tnum = DB::table('tnumeradores as num')
		->where('csubsistema','=',$csubsistema)
		->where('entidad','=',$entidad)
		->first();

		$num=$tnum->numero;
		$num++;
		$cnum="";
		$cnum = date('Y') . str_pad($num,7,'0',STR_PAD_LEFT);

		return $cnum;

	}
	public function genNumeroRendicion($csubsistema,$entidad){
		$tnum = Tnumeradore::where('csubsistema','=',$csubsistema)
		->where('entidad','=',$entidad)
		->first();
		$num=0;
		$cnum="";
		if($tnum){
			$num= $tnum->numero;
			$num++;
			$cnum = date('Y') . str_pad($num,7,'0',STR_PAD_LEFT);
			$tnum->numero = $num;
			$tnum->save();

		}
		return $cnum;
	}
	public function getListCatalogo($codtab){
        $tipoactividad = DB::table('tcatalogo')
        ->where('codtab','=',$codtab)
        ->whereNotNull('digide')
        ->orderBy('descripcion','ASC')
        ->lists('descripcion','digide');		

		return $tipoactividad;
	}



	public function listarTreeview_bk($menu){
		$menu = DB::table('tmenu')
        ->where('menuid','=',$menu)
		->get();
		$estructura="";
		if(count($menu) >0){
			foreach($menu as $m){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				
	            $estructura=$estructura ."{";
	            $estructura=$estructura ."text: '".$m->descripcion ."',";
	            $estructura=$estructura ."tags: '".$m->menuid. "',";
	            $estructura=$estructura ."state: {checked: true}";
	            
				$est=$this->listarTreeview($menu);
				if(!empty($est)){
					$estructura=$estructura .",nodes: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}

	public function listarTreeview($m){
		$menu = DB::table('tmenu')   
		->where(DB::raw('LENGTH(menuid)'),'>',DB::raw('2'))
		->where(DB::raw('substr(menuid,1,2)'),'=',$m->menuid)
		->orderBy('descripcion','ASC')	
		->get();
		$estructura="";
		if(count($menu) >0){
			foreach($menu as $m){
	            if(!empty($estructura)){
	                $estructura= $estructura .",";
	            }
				//$lista[count($lista)]= $act;
	            $estructura=$estructura ."{\n\r";
            	$estructura=$estructura ."item: {\n\r";
            	$estructura=$estructura ."id: '".$m->menuid ."',";
	            $estructura=$estructura ."label: '".$m->descripcion. "',";
	            $estructura=$estructura ."checked: false ";
	            $estructura=$estructura ."}\n\r";
	            
				/*$est=$this->listarTreeview($menu);
				if(!empty($est)){
					$estructura=$estructura .",children: [ ";				
		            $estructura = $estructura . $est;
		            $estructura=$estructura ."]";
				}*/
				$estructura=$estructura ."}";
			}
			return $estructura;
		}else{
			return $estructura;
		}
	}



}



?>
