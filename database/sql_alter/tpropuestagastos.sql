ALTER TABLE tpropuestagastos ADD COLUMN estado integer DEFAULT 1;
ALTER TABLE tpropuestagastos ADD COLUMN created_at timestamp without time zone;
ALTER TABLE tpropuestagastos ADD COLUMN updated_at timestamp without time zone;
