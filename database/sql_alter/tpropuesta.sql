ALTER TABLE tpropuesta ADD COLUMN created_at timestamp without time zone;
ALTER TABLE tpropuesta ADD COLUMN updated_at timestamp without time zone;
ALTER TABLE tpropuesta ADD COLUMN cpersona_revisor_propuesta integer;
ALTER TABLE tpropuesta ADD COLUMN cpersona_revisor_proyecto integer;
ALTER TABLE tpropuesta ADD COLUMN revision_num integer DEFAULT 1;
ALTER TABLE tpropuesta ADD COLUMN revision_visible integer DEFAULT 1;
