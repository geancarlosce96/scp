ALTER TABLE tpropuestaactividades ADD COLUMN estado integer DEFAULT 1;
ALTER TABLE tpropuestaactividades ADD COLUMN hijos integer DEFAULT 0;
ALTER TABLE tpropuestaactividades ADD COLUMN created_at timestamp without time zone;
ALTER TABLE tpropuestaactividades ADD COLUMN updated_at timestamp without time zone;
