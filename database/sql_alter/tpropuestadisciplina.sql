ALTER TABLE tpropuestadisciplina ADD COLUMN estado integer DEFAULT 1;
ALTER TABLE tpropuestadisciplina ADD COLUMN created_at timestamp without time zone;
ALTER TABLE tpropuestadisciplina ADD COLUMN updated_at timestamp without time zone;
