ALTER TABLE tpropuestagastoslaboratorio ADD COLUMN comentario character varying(200) DEFAULT '';
ALTER TABLE tpropuestagastoslaboratorio ADD COLUMN estado integer DEFAULT 1;
ALTER TABLE tpropuestagastoslaboratorio ADD COLUMN created_at timestamp without time zone;
ALTER TABLE tpropuestagastoslaboratorio ADD COLUMN updated_at timestamp without time zone;
