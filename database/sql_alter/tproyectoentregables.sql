ALTER TABLE tproyectoentregables ADD COLUMN cfaseproyecto integer;
ALTER TABLE tproyectoentregables ADD COLUMN descripcion_entregable character varying(500);
ALTER TABLE tproyectoentregables ADD COLUMN cproyectoentregables_parent bigint;
ALTER TABLE tproyectoentregables ADD COLUMN cpersona_revisor bigint;
ALTER TABLE tproyectoentregables ADD COLUMN ruteo character varying(5);
ALTER TABLE tproyectoentregables ADD COLUMN ruta text;
ALTER TABLE tproyectoentregables ADD COLUMN cestadoentregableruteo character varying(5);
ALTER TABLE tproyectoentregables ADD COLUMN comentario character varying(150);
