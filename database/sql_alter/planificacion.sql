ALTER TABLE planificacion ALTER COLUMN cproyecto TYPE integer USING (trim(cproyecto)::integer);
ALTER TABLE planificacion_log ALTER COLUMN cproyecto TYPE integer USING (trim(cproyecto)::integer);