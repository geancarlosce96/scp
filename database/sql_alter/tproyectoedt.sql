ALTER TABLE tproyectoedt ALTER descripcion TYPE character varying(200);
ALTER TABLE tproyectoedt ADD COLUMN cproyectoent_rev integer;
ALTER TABLE tproyectoedt ADD COLUMN referencia_edt bigint;
ALTER TABLE tproyectoedt ADD COLUMN estado bigint default 1;