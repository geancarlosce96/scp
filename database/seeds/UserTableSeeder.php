<?php

use App\User;
use Illuminate\Database\Seeder;
//use Faker\Factory as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        //$faker = Faker::create();
        DB::table('users')->delete();
        User::create(array(
        		'name' => 'pruebas',
        		'email' => 'pruebas@anddes.com',
        		'password'=> Hash::make('1234')
        	)
        	);

    }
}
