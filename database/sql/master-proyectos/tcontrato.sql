CREATE TABLE tcontrato
(
  ccontrato serial PRIMARY KEY,
  conos varchar(255),
  fechafirma timestamp without time zone,
  fechainicio timestamp without time zone,
  fechatermino timestamp without time zone,
  updated_user bigint,
  created_user bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  cproyecto bigint,

  CONSTRAINT fk_cproyecto_contrato FOREIGN KEY (cproyecto)
      REFERENCES tproyecto (cproyecto) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)

