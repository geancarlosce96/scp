CREATE TABLE tmonitoreo
(
  cmonitoreo serial PRIMARY KEY,
  encuesta text,
  encuesta1 text,
  encuesta2 text,
  encuestafecha timestamp without time zone,
  encuestafecha1 timestamp without time zone,
  encuestafecha2 timestamp without time zone,
  updated_user bigint,
  created_user bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  cproyecto bigint,

  CONSTRAINT fk_cproyecto_tfianzaseguros FOREIGN KEY (cproyecto)
      REFERENCES tproyecto (cproyecto) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)