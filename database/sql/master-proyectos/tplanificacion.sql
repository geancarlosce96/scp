CREATE TABLE tplanificacion
(
  cplanificacion serial PRIMARY KEY,
  komcliente text,
  kominterno text,
  lc text,
  alc text,
  croproyecto text,
  ram text,
  listariesgos text,
  updated_user bigint,
  created_user bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  cproyecto bigint,
  CONSTRAINT fk_cproyecto_planificacion FOREIGN KEY (cproyecto)
      REFERENCES tproyecto (cproyecto) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
