CREATE TABLE tfianzaseguros
(
  cfianzaseguro serial PRIMARY KEY,
  cf text,
  cf_inicio timestamp without time zone,
  cf_vigencia timestamp without time zone,
  srg text,
  srg_inicio timestamp without time zone,
  srg_vigencia timestamp without time zone,
  srp text,
  srp_inicio timestamp without time zone,
  srp_vigencia timestamp without time zone,
  cproyecto bigint,

  CONSTRAINT fk_cproyecto_tfianzaseguros FOREIGN KEY (cproyecto)
      REFERENCES tproyecto (cproyecto) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)