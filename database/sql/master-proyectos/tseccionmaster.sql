CREATE TABLE tseccionmaster
(
  ctseccionmaster serial PRIMARY KEY,
  descripcion varchar(255),
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  estado bigint default 1
)