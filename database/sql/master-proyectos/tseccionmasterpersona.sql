CREATE TABLE tseccionmasterpersona
(
  ctseccionmasterpersona serial PRIMARY KEY,
  cpersona bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  ctseccionmaster serial,
  CONSTRAINT fk_seccionmaster FOREIGN KEY (ctseccionmaster)
  REFERENCES tseccionmaster (ctseccionmaster) MATCH SIMPLE
  ON UPDATE NO ACTION ON DELETE NO ACTION
)
