CREATE TABLE tcierre
(
  ccierre serial PRIMARY KEY,
  acs text,
  acscodigo varchar(255),
  acsfechaenvio timestamp without time zone,
  acsfecharecepcion timestamp without time zone,
  reunioncierre text,
  correofin text,
  updated_user bigint,
  created_user bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  cproyecto bigint,

  CONSTRAINT fk_cproyecto_cierre FOREIGN KEY (cproyecto)
      REFERENCES tproyecto (cproyecto) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)