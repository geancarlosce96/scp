CREATE TABLE public.propuesta_tag_servicio
(
  id serial,  
  propuesta_id integer,
  propuesta_tag_servicio_id integer,
  
  estado integer DEFAULT 1,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,  
  CONSTRAINT propuesta_tag_servicio_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.propuesta_tag_servicio
  OWNER TO postgres;