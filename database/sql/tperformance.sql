-- Tabla PERFORMANCE

CREATE TABLE public.tperformance
(
  cperformance serial primary key,
  semana character varying(100),
  fecha timestamp without time zone,
  porcentaje numeric(10,2),
  cproyectoactividades bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  updated_user bigint,
  created_user bigint,
  periodo character varying(50),
  
  CONSTRAINT fk_cproyectoactividades_tproyectoactividades FOREIGN KEY (cproyectoactividades)
      REFERENCES public.tproyectoactividades (cproyectoactividades) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)


-- Agregar campo orden a la tabla troldespliegue

ALTER TABLE troldespliegue  ADD COLUMN orden character varying(50);


