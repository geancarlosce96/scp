CREATE TABLE public.capacitacion_persona
(
  id serial,
  capacitacion_id integer,
  usuario_id integer,
  estado integer DEFAULT 1,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT capacitacion_persona_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.capacitacion_persona
  OWNER TO postgres;
