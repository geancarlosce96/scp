CREATE TABLE public.propuesta_tecnico
(
  id serial,
  cpropuesta integer,
  persona_id integer,
  
  estado integer DEFAULT 1,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,  
  CONSTRAINT propuesta_tecnico_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.propuesta_tecnico
  OWNER TO postgres;