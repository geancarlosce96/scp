CREATE TABLE public.planificacion_diaria
(
  id serial NOT NULL,
  periodo integer,
  semana integer,
  cproyecto integer,
  cproyectoactividades integer,
  usuario character varying(20),
  lun numeric(10,2) DEFAULT 0,
  mar numeric(10,2) DEFAULT 0,
  mie numeric(10,2) DEFAULT 0,
  jue numeric(10,2) DEFAULT 0,
  vie numeric(10,2) DEFAULT 0,
  sab numeric(10,2) DEFAULT 0,
  dom numeric(10,2) DEFAULT 0,
  comentarios character varying(300),
  estado integer DEFAULT 1,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT planificacion_diaria_pkey PRIMARY KEY (id)
)