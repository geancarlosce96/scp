CREATE TABLE public.propuesta_opciones_log
(
  id serial,
  cpropuesta integer,
  cpersona integer,
  opcion character varying(100),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,  
  CONSTRAINT propuesta_opciones_log_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.propuesta_opciones_log
  OWNER TO postgres;