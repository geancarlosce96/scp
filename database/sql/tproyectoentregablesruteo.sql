CREATE TABLE tproyectoentregablesruteo
(
  cproyectoentregablesruteo serial PRIMARY KEY,
  cproyectoentregable bigint,
  ruteo_actual character(5),
  estado character(5),
  revision character(5),
  fecha timestamp without time zone,
  cpersona bigint,
  comentario character(150),
  CONSTRAINT fk_tproyectoentregablesruteo_tproyectoentregables FOREIGN KEY (cproyectoentregable)
      REFERENCES tproyectoentregables (cproyectoentregables) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION  
)