CREATE TABLE planificacion_log
(
	id serial,
	periodo integer,
	semana integer,
	cproyecto integer,
	mensaje character varying(200),
	created_at timestamp without time zone,
  	updated_at timestamp without time zone,
  	CONSTRAINT planificacion_log_pkey PRIMARY KEY (id)
)
