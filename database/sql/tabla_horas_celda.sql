CREATE TABLE public.tabla_horas_celda
(
	id serial,
	cpropuesta integer,	
	fila_codigo integer,
	columna_codigo integer,	
	valor character varying(200) DEFAULT '',
	estado integer DEFAULT 1,
	updated_at timestamp without time zone,
	created_at timestamp without time zone,
	CONSTRAINT tabla_horas_celda_pkey PRIMARY KEY (id)
)
WITH ( OIDS=FALSE );
