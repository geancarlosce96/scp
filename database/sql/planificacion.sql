CREATE TABLE planificacion
(
	id serial,
	periodo integer,
	semana integer,
	cproyecto integer,
	usuario character varying(20),
	planificadas numeric(10,2) DEFAULT 0,
	planificadas_adm numeric(10,2) DEFAULT 0, 
	disponibles numeric(10,2) DEFAULT 0,
	total numeric(10,2) DEFAULT 0, 
	ejecutadas numeric(10,2) DEFAULT 0, 
	ejecutadas_adm numeric(10,2) DEFAULT 0,
	estado integer DEFAULT 1,
	created_at timestamp without time zone,
  	updated_at timestamp without time zone,
  	CONSTRAINT planificacion_pkey PRIMARY KEY (id)
)
