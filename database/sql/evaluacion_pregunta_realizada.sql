
CREATE TABLE public.evaluacion_pregunta_realizada
(
  id serial,
  evaluacion_persona_id integer,
  pregunta_id integer,  
  descripcion character varying(300),
  respuestas character varying(500),
  estado integer DEFAULT 1, 
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  CONSTRAINT evaluacion_pregunta_realizada_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.evaluacion_pregunta_realizada
  OWNER TO postgres;
COMMENT ON COLUMN public.evaluacion_pregunta_realizada.estado IS '0 pendiente 1 en proceso 2 aprobado 3 desaprobado ';

