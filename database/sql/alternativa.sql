CREATE TABLE public.alternativa
(
  id serial,
  pregunta_id integer,
  descripcion character varying(300),
  correcta integer DEFAULT 0,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  CONSTRAINT alternativa_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.alternativa
  OWNER TO postgres;