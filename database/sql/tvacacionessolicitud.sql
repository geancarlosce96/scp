CREATE TABLE public.tvacacionessolicitud
(
  id SERIAL PRIMARY KEY,
  totalvacionesacumuladas character varying(30),
  diassolicitados character varying(10),
  estadova character varying(50),
  tipovaca character varying(20),
  fdesde character varying(10),
  fhasta character varying(10),
  idempleado bigint,
  idjefeinmediato bigint,
  idjefesuperior bigint,
  aprobado character varying(25),
  created_at timestamp without time zone,
  updated_at timestamp without time zone
)