CREATE TABLE tmotivoenvioentregable
(
  cmotivoenvio serial NOT NULL primary key,
  decripcion character varying(300) DEFAULT ''::character varying,  
  updated_user bigint,
  created_user bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone

)