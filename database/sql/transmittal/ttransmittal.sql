CREATE TABLE ttransmittal
(
  ctransmittal serial NOT NULL primary key,
  cproyecto bigint references tproyecto,
  numero character varying(50),
  tipotransmittal character varying(5), -- AND-CLI o CLI-AND,
  tipoenvio character varying(5), -- TCATALOGO,
  fechaenvio timestamp without time zone,
  comentario character varying(300),
  updated_user bigint,
  created_user bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone
);