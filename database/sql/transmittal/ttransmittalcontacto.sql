CREATE TABLE ttransmittalcontacto
(
  ctransmittalcontacto serial not null primary key,
  cunidadmineracontacto bigint,
  ctransmittal integer references ttransmittal,
  tipocontacto character varying(5),
  updated_at timestamp without time zone,
  created_at timestamp without time zone
)