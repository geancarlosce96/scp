CREATE TABLE ttransmittaldetalle
(
  ctransmittaldetalle serial NOT NULL primary key,
  ctransmittal bigint references ttransmittal, --CABECERA
  cproyectoentregables integer references tproyectoentregables, 
  codigo_entregable character varying(50),
  revision_entregable character varying(10),
  descripcion_entregable character varying(300),
  cmotivoenvio integer references tmotivoenvioentregable,
  updated_user bigint,
  created_user bigint,
  updated_at timestamp without time zone,
  created_at timestamp without time zone
)