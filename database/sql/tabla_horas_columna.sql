CREATE TABLE public.tabla_horas_columna
(
	id serial,
	cpropuesta integer,
	descripcion character varying(200),	
	num_orden integer DEFAULT 1,
	editable integer DEFAULT 1,
	disciplina_id integer DEFAULT 0, --REFERENCIA OPCIONAL
	profesion_id integer, --REFERENCIA OPCIONAL
	estado integer DEFAULT 1,
	updated_at timestamp without time zone,
	created_at timestamp without time zone,
	CONSTRAINT tabla_horas_columna_pkey PRIMARY KEY (id)
)
WITH ( OIDS=FALSE );
