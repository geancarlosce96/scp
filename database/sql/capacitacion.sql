
CREATE TABLE public.capacitacion
(
  id serial,
  codigo character varying(100),
  titulo character varying(200),  
  videos character varying(3000),
  motivo character varying(300) DEFAULT '',
  aprobadores character varying(1000) DEFAULT '',
  personas character varying(1000) DEFAULT '',
  fecha_inicio timestamp without time zone,
  fecha_fin timestamp without time zone,  
  usuario_id integer,
  cantidad_evaluacion integer DEFAULT 0,
  estado integer DEFAULT 0, -- 0 pendiente 1 aprobado 2 finalizado 3 cancelado 4 observado
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.capacitacion
  OWNER TO postgres;
COMMENT ON COLUMN public.capacitacion.estado IS '0 pendiente 1 aprobado 2 finalizado 3 cancelado ';

