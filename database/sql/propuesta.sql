CREATE TABLE public.propuesta
(
	id serial,
	codigo character varying(50),
	propuesta character varying(300),
	revision character varying(10),
	gerencia character varying(50),	
	disciplinas character varying(100),	
	motivo character varying(100),
	emision integer DEFAULT 1,
	
	tipo_servicio_id integer DEFAULT NULL,
	cliente_id integer DEFAULT NULL,
	contacto_id integer DEFAULT NULL,
	unidad_minera_id integer DEFAULT NULL,
	prepropuesta_id integer DEFAULT NULL,
	tipo_registro integer DEFAULT 1, -- 1 propuesta 2 pre propuesta 3 invitacion

	responsable_propuesta_id integer DEFAULT NULL,
	revisor_final_id integer DEFAULT NULL,
	gerente_proyecto_id integer DEFAULT NULL,
	ssa_id integer DEFAULT NULL,
	responsable_tecnico_id integer DEFAULT NULL,
	propuestas_id integer DEFAULT NULL,	
	inicio_propuesta date DEFAULT NULL,
	visita_tecnica date DEFAULT NULL,
	presentacion_consultas date DEFAULT NULL,
	absolucion_consultas date DEFAULT NULL,
	sustentacion date DEFAULT NULL,
	presentacion date DEFAULT NULL,
	comentario character varying(300),
	presentacion_tipo integer DEFAULT 0, --1 fisico 2 digital

	estado integer DEFAULT 1,
	created_at timestamp without time zone,
	updated_at timestamp without time zone,  
	CONSTRAINT propuesta_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
);
ALTER TABLE public.propuesta
	OWNER TO postgres;