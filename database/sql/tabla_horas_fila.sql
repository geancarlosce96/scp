CREATE TABLE public.tabla_horas_fila
(
	id serial,
	cpropuesta integer,
	descripcion character varying(200),
	num_orden integer DEFAULT 1,
	editable integer DEFAULT 1,
	estado integer DEFAULT 1,
	updated_at timestamp without time zone,
	created_at timestamp without time zone,
	CONSTRAINT tabla_horas_fila_pkey PRIMARY KEY (id)
)
WITH ( OIDS=FALSE );
