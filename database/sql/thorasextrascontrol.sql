CREATE TABLE public.thorasextrascontrol
(
  id SERIAL,
  dia character varying(30),
  horarioentrada character varying(10),
  horariosalida character varying(20),
  maxhoras character varying(10),
  idiso character varying(10),
  aprobado character varying(25),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  primary key(id)
)