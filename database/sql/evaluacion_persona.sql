
CREATE TABLE public.evaluacion_persona
(
  id serial,
  evaluacion_id integer,
  usuario_id integer,
  puntaje integer DEFAULT 0,  
  estado integer DEFAULT 0, -- 0 pendiente 1 en proceso 2 aprobado 3 desaprobado
  created_at timestamp without time zone,
  updated_at timestamp without time zone,  
  CONSTRAINT evaluacion_persona_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.evaluacion_persona
  OWNER TO postgres;
COMMENT ON COLUMN public.evaluacion_persona.estado IS '0 pendiente 1 en proceso 2 aprobado 3 desaprobado ';

