-- Table: public.thorasextrasaprobadas

-- DROP TABLE public.thorasextrasaprobadas;

CREATE TABLE public.thorasextrasaprobadas
(
  id SERIAL,
  thorasextras character varying(30),
  useraccion character varying(25),
  accionrealizada character varying(30),
  observacion character(500),
  idhorasextrassolicitudint integer,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  primary key(id)
)