CREATE TABLE public.contacto
(
	id serial,
	nombres character varying(100),
	apellidos character varying(100),
	cargo character varying(100),
	numero character varying(100),
	correo character varying(100),
	fecha_invitacion timestamp without time zone,
	fecha_consultas timestamp without time zone,
	fecha_absolucion timestamp without time zone,
	fecha_presentacion timestamp without time zone,
	
	estado integer DEFAULT 1,
	created_at timestamp without time zone,
	updated_at timestamp without time zone,  
	CONSTRAINT contacto_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
);
ALTER TABLE public.contacto
	OWNER TO postgres;