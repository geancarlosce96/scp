CREATE TABLE public.erp_encuesta_det
(
  id integer NOT NULL,
  id_encuesta integer NOT NULL,
  tipo_respuesta character varying(50),
  grupo character varying(50),
  tipo character varying(50),
  detalle character varying(255),
  orden integer,
  id_respuesta integer, -- 1:A 2:B 3:C 4:D
  texto_respuesta text,
  CONSTRAINT erp_encuesta_det_pkey PRIMARY KEY (id),
  CONSTRAINT fk_erp_encuesta_det_erp_encuesta_cab FOREIGN KEY (id_encuesta)
      REFERENCES public.erp_encuesta_cab (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);