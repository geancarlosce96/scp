CREATE TABLE public.tipo_servicio
(
  id serial,  
  descripcion character varying(100),  
  
  estado integer DEFAULT 1,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,  
  CONSTRAINT tipo_servicio_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tipo_servicio
  OWNER TO postgres;