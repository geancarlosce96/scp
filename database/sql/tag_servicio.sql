CREATE TABLE public.tag_servicio
(
  id serial,  
  servicio_grupal_id integer,
  descripcion character varying(200),
  tag character varying(20),
  comentario character varying(300),  
  
  estado integer DEFAULT 1,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,  
  CONSTRAINT tag_servicio_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.tag_servicio
  OWNER TO postgres;