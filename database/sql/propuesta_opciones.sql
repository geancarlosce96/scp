CREATE TABLE public.propuesta_opciones
(
  id serial,
  cpropuesta integer DEFAULT 0,
  actividades integer DEFAULT 0,
  disciplinas integer DEFAULT 0,
  equipo integer DEFAULT 0,
  gastos integer DEFAULT 0,
  gastosLaboratorios integer DEFAULT 0,
  horas integer DEFAULT 0,
  resumenGlobal integer DEFAULT 0,
  resumenHH integer DEFAULT 0,
  tags integer DEFAULT 0,
  
  estado integer DEFAULT 1,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,  
  CONSTRAINT propuesta_opciones_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.propuesta_opciones
  OWNER TO postgres;
