CREATE TABLE public.prepropuesta
(
	id serial,
	codigo character varying(50),
	propuesta character varying(300),
	unidad character varying(100),
	invitacion_id integer DEFAULT NULL,
	cliente_id integer DEFAULT NULL,

	contacto_id integer DEFAULT NULL,
	estado integer DEFAULT 1,
	created_at timestamp without time zone,
	updated_at timestamp without time zone,  
	CONSTRAINT prepropuesta_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
);
ALTER TABLE public.prepropuesta
	OWNER TO postgres;