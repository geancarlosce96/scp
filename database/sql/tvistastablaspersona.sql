CREATE TABLE tvistastablaspersona
(
  tvistastablaspersona serial NOT NULL,
  cpersona bigint,
  columnas character varying(100),
  ctvistastablas bigint,
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT tvistastablaspersona_pkey PRIMARY KEY (tvistastablaspersona),
  CONSTRAINT fk_tvistastablaspersona_tvistastablas FOREIGN KEY (ctvistastablas)
      REFERENCES tvistastablas (ctvistastablas) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)