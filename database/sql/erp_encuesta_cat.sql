CREATE TABLE public.erp_encuesta_cat
(
  id integer NOT NULL,
  descripcion character varying(255),
  desde character varying(50),
  hasta character varying(50),
  CONSTRAINT erp_encuesta_cat_pkey PRIMARY KEY (id)
);