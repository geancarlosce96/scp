﻿CREATE TABLE public.tvacacionaprobada
(
  id SERIAL PRIMARY KEY,
  idsolicitudint integer,
  useraccion character varying(25),
  accionrealizada character varying(30),
  observacion character(500),		
  created_at timestamp without time zone,
  updated_at timestamp without time zone

)