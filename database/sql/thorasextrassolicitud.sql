CREATE TABLE public.thorasextrassolicitud
(
  id serial,
  fechahorasextras character varying(30),
  horassolicitadas character varying(10),
  estadohe character varying(20),
  cproyecto bigint,
  motivo character varying(500),
  idfechaiso character varying(45),
  idempleado bigint,
  idjefeinmediato bigint,
  idgerenteproyecto bigint,
  aprobado character varying(25),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  primary key(id)
);