
CREATE TABLE public.evaluacion
(
  id serial,  
  capacitacion_id integer,
  titulo character varying(200),
  objetivo character varying(300),
  fecha_inicio timestamp without time zone,
  fecha_fin timestamp without time zone,
  duracion integer DEFAULT 0,
  cantidad_preguntas integer DEFAULT 0,
  cantidad_preguntas_min integer DEFAULT 0,
  cantidad_preguntas_mostrar integer DEFAULT 0,
  estado integer DEFAULT 0, -- 0 pendiente aprobacion 1 aprobado 2 finalizado 3 cancelado
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  CONSTRAINT evaluacion_pk PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.evaluacion
  OWNER TO postgres;
COMMENT ON COLUMN public.evaluacion.estado IS '0 pendiente aprobacion 1 aprobado 2 finalizado 3 cancelado ';

