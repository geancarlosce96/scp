CREATE TABLE public.invitacion
(
	id serial,
	propuesta character varying(300),

	contacto_id integer DEFAULT NULL,
	cliente_id integer DEFAULT NULL,
	estado integer DEFAULT 1,
	created_at timestamp without time zone,
	updated_at timestamp without time zone,  
	CONSTRAINT invitacion_pkey PRIMARY KEY (id)
)
WITH (
	OIDS=FALSE
);
ALTER TABLE public.invitacion
	OWNER TO postgres;