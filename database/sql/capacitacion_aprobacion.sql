CREATE TABLE public.capacitacion_aprobacion
(
  id serial,
  capacitacion_id integer,
  usuario_id integer,
  estado integer DEFAULT 1,
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  CONSTRAINT capacitacion_aprobacion_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.capacitacion_aprobacion
  OWNER TO postgres;