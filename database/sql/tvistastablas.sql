CREATE TABLE tvistastablas
(
  ctvistastablas serial NOT NULL,
  nombre character varying(25),
  columnas character varying(300),
  descripcion character varying(50),
  estado character varying(5),
  created_at timestamp without time zone,
  updated_at timestamp without time zone,
  diccionario character(150),
  CONSTRAINT tvistastablas_pkey PRIMARY KEY (ctvistastablas)
)