CREATE TABLE public.erp_encuesta_cab
(
  id integer NOT NULL,
  dni_evaluado character varying(50),
  dni_evaluador character varying(50),
  tipo_evaluador character varying(50),
  perfil character varying(50),
  id_estado integer, -- 1:  PENDIENTE 2:  TERMINADO
  id_categoria integer,
  CONSTRAINT erp_encuesta_cab_pkey PRIMARY KEY (id)
);