-- Table: public.xx_migracion

-- DROP TABLE public.xx_migracion;

CREATE TABLE public.xx_migracion
(
  proyectoid character varying(255),
  gasto_padre character varying(255),
  gastoid character varying(255),
  nombre_gasto character varying(255),
  asignado character varying(255),
  venta character varying(255),
  monto_total character varying(255),
  monto_igv character varying(255),
  igv character varying(255),
  otro_impuesto character varying(255),
  monto_impuesto character varying(255),
  submonto character varying(255),
  moneda character varying(255),
  fecha_rendicion character varying(255),
  fecha_factura character varying(255),
  estado_rendicion character varying(255),
  categoriaid character varying(255),
  areaid character varying(255),
  bill_cliente character varying(255),
  reembolsable character varying(255),
  num_factura character varying(255),
  proveedor character varying(255),
  laboratorio_cantidad character varying(255),
  laboratorio_pu character varying(255),
  descripcion character varying(8000),
  numero_rendicion character varying(255),
  soles_monto_total character varying(255),
  dolar_monto_total1 character varying(255),
  nombre_rendicion character varying(255),
  monto_total_rendicion character varying(255),
  comentario character varying(255),
  monto_reembolso character varying(255),
  monto_cliente character varying(255),
  numero_completo character varying(255),
  dolar_monto_total character varying(255),
  dolar_monto_cliente character varying(255),
  dolar_monto_reembolso character varying(255),
  gasto_persona_id character varying(255),
  tipo_proyecto character varying(255)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.xx_migracion
  OWNER TO postgres;
