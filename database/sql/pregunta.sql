
CREATE TABLE public.pregunta
(
  id serial,
  evaluacion_id integer,
  descripcion character varying(200),
  opcion_unica integer DEFAULT 0, -- opcion unica/multiple 0 si 1 no
  updated_at timestamp without time zone,
  created_at timestamp without time zone,
  CONSTRAINT pregunta_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE public.pregunta
  OWNER TO postgres;
COMMENT ON COLUMN public.pregunta.opcion_unica IS 'opcion unica/multiple 0 si 1 no ';

