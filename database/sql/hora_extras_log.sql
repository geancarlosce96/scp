CREATE TABLE public.hora_extras_log
(
  id serial primary key,
  id_solicitud_horas_extras integer,
  estado_anterior character varying(20),
  estado_actual character varying(20),
  persona_aprobada integer references tpersona(cpersona),
  updated_at timestamp without time zone,
  created_at timestamp without time zone,

  CONSTRAINT hora_extras_log_id_solicitud_horas_extras_fkey FOREIGN KEY (id_solicitud_horas_extras)
      REFERENCES public.thorasextrassolicitud (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)