intotmenu
-- ----------------------------
-- Permisos en la tabla de Menu 
-- ----------------------------
INSERT INTO "public"."tmenu" VALUES ('0606', 'Evaluaciones ', 'periodoevaluacion', '1', '06');
INSERT INTO "public"."tmenu" VALUES ('0607', 'Auditoria Evaluaciones ', 'auditoriaevaluaciones', '1', '06');
INSERT INTO "public"."tmenu" VALUES ('0608', 'Vacaciones', 'vacaciones', '1', '06');
INSERT INTO "public"."tmenu" VALUES ('0609', 'Aprueba Vacaciones', 'vacacionessuccess', '1', '06');
INSERT INTO "public"."tmenu" VALUES ('0610', 'Horas Extras', 'horasextras', '1', '06');
INSERT INTO "public"."tmenu" VALUES ('0611', 'Aprueba Horas Extras', 'horasextrassuccess', '1', '06');